<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

if(env('APP_ENV')=='production')
{
    return [
        'user'=>'8304deb7-7f54-4470-9b95-1197e5d355352022',
        'role'=>'410ae6dc-a3e6-47c7-9f9c-5528d88405b62022',
        'merchant'=>'8f359ca7-4043-4e01-be12-3c412539fb202022',
        'switch_branch'=>'b8f3e883-3b95-49cd-a7c2-9e97cb74be592022',
        'switch_branch_id'=>'9cd91445-50b4-4b90-83d6-19f65e08177c2022',
        'switch_branch_user_id'=>'5b037148-aeb9-413e-8387-5755c1180dcb2022',
        'menu_merchant'=>'19ecdb09-096d-4c19-8f7b-f450c58c28f62022'
    ];
}else{
    return [
        'user'=>'8304deb7-7f54-4470-9b95-1197e5d35q53520222022',
        'role'=>'410ae6dc-a3e6-47c7-9f9c-5528d8sm8405b620222022',
        'merchant'=>'8f359ca7-4043-4e01-be12-3c412sm539fb2020222022',
        'switch_branch'=>'b8f3e883-3b95-49cd-a7c2-9e9sm7cb74be5920222022',
        'switch_branch_id'=>'9cd91445-50b4-4b90-83d6-1am9f65e08177c20222022',
        'switch_branch_user_id'=>'5b037148-aeb9-413e-8am387-5755c1180dcb20222022',
        'menu_merchant'=>'19ecdb09-096d-4c19-8f7b-f4am50c58c28f620222022'
    ];
}


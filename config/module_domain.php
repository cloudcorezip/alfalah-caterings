<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */
return[
    'domain'=>(env('APP_DOMAIN')==true)?'sennakasir.co.id':'/',
    'blog_domain'=>(env('APP_DOMAIN')==true)?'blog.sennakasir.co.id':'blog',
    'merchant_domain'=>( env('APP_DOMAIN')==true)?'merchant.sennakasir.co.id':'merchant',
];



<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

return [
    'size_limit' => '6000', // in bytes
    'base_path' => '/',
    'exclude_keywords' => []
];

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

return[
    'about'=>'About',
    'service'=>'Service',
    'help'=>'Help Center',
    'carrier'=>'Carrier',
    'term'=>'Term & Condition',
    'contact'=>'Contact',
    'policy'=>'Privacy & Policy',
    'price'=>'Price'
];




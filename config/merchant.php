<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

return [
    'toko'=>'32dd6101-b364-4ceb-8c66-5cce32b6c2a212',
    'jasa'=>'3ddf2e2f-30ba-4dfa-a052-72390b30ade021',
    'pay_sub_wallet'=>(env('APP_ENV')!='production')?['LINKAJA','QRIS']:["DANA","LINKAJA","QRIS","SHOPEEPAY"],
    'pay_sub_va'=>['BNI', 'BRI', 'MANDIRI', 'PERMATA']
];

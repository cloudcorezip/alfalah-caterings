
<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <img src="{{asset('public/backend-v2')}}/img/logo-senna-v2.png" height="55px" style="margin-left: -30px; margin-right: 40px;" />
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0" />

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">

        @if(get_role()==3)
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse3013" aria-expanded="true" aria-controls="collapse3">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
            <div id="collapse3013" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('merchant.toko.sale-order.index')}}">Ringkasa Penjualan</a>
                    <a class="collapse-item" href="{{route('merchant.toko.purchase-order.index')}}">Ringkasan Pembelian & Persediaan</a>
                </div>
            </div>
        @else
            <a class="nav-link" href="{{route(''.dashboard_url().'')}}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        @endif
    </li>
@if(get_role()==5 )
    <!-- Divider -->
        <hr class="sidebar-divider" />

        <!-- Heading -->
        <div class="sidebar-heading">
            Master Data
        </div>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                <i class="fas fa-fw fa-cog"></i>
                <span>Senna</span>
            </a>
            <div id="collapse3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('master-data.app-service.index')}}">Layanan Aplikasi</a>
                    <a class="collapse-item" href="{{route('master-data.subscription.index')}}">Paket Langganan</a>
                    <a class="collapse-item" href="{{route('master-data.type-business.index')}}">Tipe Bisnis</a>
                    <a class="collapse-item" href="{{route('master-data.bank.index')}}">Bank</a>
                    <a class="collapse-item" href="{{route('master-data.avBank.index')}}">Bank Tersedia</a>
                    <a class="collapse-item" href="{{route('master-data.contact.index')}}">Kontak Kami</a>

                    <a class="collapse-item" href="{{route('master-data.transaction-status.index')}}">Status Transaksi</a>
                    <a class="collapse-item" href="{{route('master-data.transaction-type.index')}}">Tipe Transaksi</a>
                    <a class="collapse-item" href="{{route('master-data.province.index')}}">Provinsi</a>
                    <a class="collapse-item" href="{{route('master-data.city.index')}}">Kota</a>
                    <a class="collapse-item" href="{{route('master-data.district.index')}}">District</a>
                    <a class="collapse-item" href="{{route('master-data.approval.index')}}">Pengaturan Approval</a>
                    <a class="collapse-item" href="{{route('master-data.merchant-approval.index')}}">Approval Merchant</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse312" aria-expanded="true" aria-controls="collapse312">
                <i class="fas fa-fw fa-cog"></i>
                <span>Senna</span>
            </a>
            <div id="collapse312" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('master-data.unit.index')}}">Satuan</a>
                    <a class="collapse-item" href="{{route('master-data.currency.index')}}">Mata Uang</a>
                    <a class="collapse-item" href="{{route('master-data.senna-kasir.category.index')}}">Kategori Umum</a>
                    <a class="collapse-item" href="{{route('master-data.senna-kasir.transaction-status.index')}}">Status Transaksi</a>
                    <a class="collapse-item" href="{{route('master-data.shipping-category.index')}}">Kategori Pengiriman</a>
                    <a class="collapse-item" href="{{route('master-data.senna-kasir.order-status.index')}}">Status Order</a>
                    <a class="collapse-item" href="{{route('master-data.inventory-method.index')}}">Metode Persediaan</a>
                    <a class="collapse-item" href="{{route('master-data.product-type.index')}}">Tipe Produk</a>
                    <a class="collapse-item" href="{{route('master-data.cash-type.index')}}">Tipe Cash</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse3121" aria-expanded="true" aria-controls="collapse3121">
                <i class="fas fa-fw fa-cog"></i>
                <span>Senna Jasa</span>
            </a>
            <div id="collapse3121" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('master-data.senna-jasa.category-service.index')}}">Kategori Layanan</a>
                    <a class="collapse-item" href="{{route('master-data.senna-jasa.type-service.index')}}">Tipe Layanan</a>
                    <a class="collapse-item" href="{{route('master-data.senna-jasa.order-status.index')}}">Status Order</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse31211" aria-expanded="true" aria-controls="collapse31211">
                <i class="fas fa-fw fa-cog"></i>
                <span>Senna Pay</span>
            </a>
            <div id="collapse31211" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('master-data.senna-pay.transaction-status.index')}}">Status Transaksi</a>
                    <a class="collapse-item" href="{{route('master-data.senna-pay.transaction-type.index')}}">Tipe Transaksi</a>
                    <a class="collapse-item" href="{{route('master-data.senna-pay.guide-category.index')}}">Kategori Panduan</a>
                    <a class="collapse-item" href="{{route('master-data.senna-pay.guide.index')}}">Panduan</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                <span>Pengguna</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('master-data.user.index')}}">Menu </a>
                    <a class="collapse-item" href="{{route('master-data.role.index')}}">Hak Akses</a>
                    <a class="collapse-item" href="{{route('master-data.user-bank.index')}}">Bank Pengguna</a>
                    <a class="collapse-item" href="{{route('master-data.user-activity-notification.index')}}">Notifikasi Pengguna</a>
                </div>
            </div>
        </li>

@endif

<!-- Nav Item - Utilities Collapse Menu -->
    <!-- Divider -->
    @if(get_role()==5 || get_role()==6)
        <hr class="sidebar-divider" />

        <!-- Heading -->
        <div class="sidebar-heading">
            Transaksi
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages1" aria-expanded="true" aria-controls="collapsePages1">
                <i class="fas fa-fw fa-folder"></i>
                <span>Transfer Masuk</span>
            </a>
            <div id="collapsePages1" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('senna-pay.transaction.manual-transaction-in')}}">Manual <span class="badge badge-danger text-white ml-auto">{{$countTfIn}}</span></a>
                    <a class="collapse-item" href="#">Online</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages2" aria-expanded="true" aria-controls="collapsePages2">
                <i class="fas fa-fw fa-folder"></i>
                <span>Penarikan</span>
            </a>
            <div id="collapsePages2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('senna-pay.transaction.manual-wd')}}">Manual <span class="badge badge-danger text-white ml-auto">{{$countTfOut}}</span></a>
                    <a class="collapse-item" href="#">Online</a>
                </div>
            </div>
        </li>
    @endif @if(get_role()==5 || get_role()==6)
        <hr class="sidebar-divider" />

        <!-- Heading -->
        <div class="sidebar-heading">
            Daftar Merchant
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages11" aria-expanded="true" aria-controls="collapsePages1">
                <i class="fas fa-fw fa-folder"></i>
                <span>Pendaftaran</span>
            </a>
            <div id="collapsePages11" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('merchant.list')}}">Merchant Baru <span class="badge badge-danger text-white ml-auto">{{$countMerchant}}</span></a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages22" aria-expanded="true" aria-controls="collapsePages2">
                <i class="fas fa-fw fa-folder"></i>
                <span>List Merchant</span>
            </a>
            <div id="collapsePages22" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('merchant-list.menu.index')}}">Menu</a>
                    <a class="collapse-item" href="{{route('merchant.list.list')}}">List</a>
                </div>
            </div>
        </li>
    @endif @if(get_role()==3)
        <hr class="sidebar-divider" />

        <!-- Heading -->
        <div class="sidebar-heading">
            Manajemen Kamu
        </div>
        <!-- Multi Dropdown -->
        @if($menuOwner!=[]) @foreach($menuOwner as $key => $item)
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages2022{{$key}}" aria-expanded="true" aria-controls="collapsePages111">
                <i class="{{$item->icons}}"></i>
                <span>{{$item->name}}</span>
            </a>
            <div id="collapsePages2022{{$key}}" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                @foreach($item->getChild->sortBy('id') as $k => $n)
                    @if(searchMenuWithSubscription($n->id)==true)
                        @if($n->getChild->count()>1)
                            <a class="nav-link" data-toggle="collapse" href="#componentsCollapse{{$key}}{{$k}}"><span class="sidebar-normal">{{$n->name}}</span></a>
                            <div class="collapse" id="componentsCollapse{{$key}}{{$k}}">
                                <ul class="nav">
                                    <li class="nav-item bg-white collapse-inner">
                                        @foreach($n->getChild->sortBy('id') as $child)
                                        <a class="collapse-item menu-pop" href="{{($child->url=='#')?'#':route($child->url)}}" title="" data-placement="right" data-toggle="tooltip" data-original-title="" >
                                           {{$child->name}}
                                        </a>
                                        @endforeach
                                    </li>
                                </ul>
                            </div>
                        @else
                            @if ($n->name==="Permintaan Izin")
                            @php
                                $countPermit = \DB::table('md_merchant_staff_permit_submissions')->where('md_merchant_id', merchant_id())
                                                                                                ->where('md_merchant_staff_permit_submissions.is_approved',0)
                                                                                                ->where('md_merchant_staff_permit_submissions.is_canceled',0)->get();
                            @endphp
                            <a class="nav-link" href="{{($n->url=='#')?'#':route($n->url)}}"><span class="sidebar-normal">{{$n->name}} <small class="badge badge-pill badge-light ">{{count($countPermit)}}</small></span></a>
                            @else
                            <a class="nav-link" href="{{($n->url=='#')?'#':route($n->url)}}"><span class="sidebar-normal">{{$n->name}}</span></a>
                            @endif

                        @endif
                    @endif
                @endforeach
            </div>
        </li>
        @endforeach @endif
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages22211" aria-expanded="true" aria-controls="collapsePages2">
                <i class="fa fa-gears"></i>
                <span>Pengaturan</span>
            </a>
            <div id="collapsePages22211" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('merchant.menu')}}">Menu</a>
                </div>
            </div>
        </li>

    @endif
    @if(get_role()==5 || get_role()==6 || get_role()==7)
        <hr class="sidebar-divider" />
        <div class="sidebar-heading">
            Blog
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages1111" aria-expanded="true" aria-controls="collapsePages111">
                <i class="fas fa-fw fa-folder"></i>
                <span>Artikel</span>
            </a>
            <div id="collapsePages1111" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('blog.dashboard.posts')}}">Posts</a>
                    <a class="collapse-item" href="{{route('blog.dashboard.category')}}">Kategori</a>
                    <a class="collapse-item" href="{{route('blog.dashboard.tags')}}">Tags</a>
                    <a class="collapse-item" href="{{route('blog.testimony.index')}}">Testimoni</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages11111" aria-expanded="true" aria-controls="collapsePages111">
                <i class="fas fa-fw fa-folder"></i>
                <span>Panduan</span>
            </a>
            <div id="collapsePages11111" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('senna-pay.transaction.manual-transaction-in')}}">Merchant Baru</a>
                </div>
            </div>
        </li>
    @endif @if(get_role()>=11)

        <hr class="sidebar-divider" />

        <!-- Heading -->
        <div class="sidebar-heading">
            Manajemen Kamu
        </div>

        @if($menu!=[]) @foreach($menu as $key => $item) @if(menu_parent_show(user()->getStaffMerchant->id,$item->id)=='show')
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages20221{{$key}}" aria-expanded="true" aria-controls="collapsePages111">
                    <i class="{{$item->icons}}"></i>
                    <span>{{$item->name}}</span>
                </a>
                <div id="collapsePages20221{{$key}}" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    @foreach($item->getChild->sortBy('id') as $n)
                        @if(menu_show(user()->getStaffMerchant->id,$n->id)=='show')
                            @if($n->getChild->count()>1)
                                <a class="nav-link" data-toggle="collapse" href="#componentsCollapse1"><span class="sidebar-normal">{{$n->name}}</span></a>
                                <div class="collapse" id="componentsCollapse1">
                                    <ul class="nav">
                                        <li class="nav-item bg-white collapse-inner rounded">
                                            @foreach($n->getChild->sortBy('id') as $child)
                                                <a class="collapse-item menu-pop" href="{{($child->url=='#')?'#':route($child->url)}}" title="" data-placement="right" data-toggle="tooltip" href="#" data-original-title="">{{$child->name}}</a>
                                            @endforeach
                                        </li>
                                    </ul>
                                </div>
                            @else
                                <a class="nav-link" href="{{($n->url=='#')?'#':route($n->url)}}"><span class="sidebar-normal">{{$n->name}}</span></a>
                            @endif
                        @endif
                    @endforeach
                </div>
            </li>
        @endif
        @endforeach @endif @endif @if(get_role()==9)
        <hr class="sidebar-divider" />
        <div class="sidebar-heading">
            Marketing
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages1111" aria-expanded="true" aria-controls="collapsePages111">
                <i class="fas fa-fw fa-folder"></i>
                <span>Sales</span>
            </a>
            <div id="collapsePages1111" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('marketing.sales.index')}}">Data Sales</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages111123" aria-expanded="true" aria-controls="collapsePages111">
                <i class="fas fa-fw fa-folder"></i>
                <span>Membership</span>
            </a>
            <div id="collapsePages111123" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{route('marketing.membership.commission.index')}}">Komisi</a>
                    <a class="collapse-item" href="{{route('marketing.membership.redeem-point.index')}}">Pengaturan Point</a>
                    <a class="collapse-item" href="{{route('blog.dashboard.tags')}}">List</a>
                </div>
            </div>
        </li>

@endif
<!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block" />

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>

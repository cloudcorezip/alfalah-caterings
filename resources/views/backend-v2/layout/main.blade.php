<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Senna adalah sebuah layanan multi-platfrom di Indonesia">
    <meta name="author" content="Senna">

    <title>@yield('title')</title>
    <link href="{{asset('public/backend-v2')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="{{asset('public/backend-v2')}}/vendor/fontawesome-free/css/v4-shims.min.css" rel="stylesheet">
    <link href="{{asset('public/backend-v2')}}/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{asset('public/frontend')}}/img/fav.png" />
    <link rel="icon" href="{{asset('public/frontend/')}}/img/fav.png" />
    <link href="{{asset('public/backend-v2')}}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/backend')}}/core/senna.css">
    <link rel="stylesheet" href="{{asset('public/frontend')}}/css/wa.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="{{asset('public/backend-v2')}}/vendor/jquery/jquery.min.js"></script>
    <script src="{{asset('public/backend-v2')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="{{asset('public/backend')}}/core/jquery.datetimepicker.css" />
    <script src="{{asset('public/backend-v2')}}/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="{{asset('public/backend-v2')}}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('public/backend-v2')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='_timezone' class="timezone" value=''>
    <script src="{{asset('public/backend/')}}/core/ajax.js"></script>
    <script src="{{asset('public/backend/')}}/core/datatable.js"></script>
    <script src="{{asset('public/backend/')}}/core/jquery.datetimepicker.js"></script>
    @yield('css')
</head>
<body id="page-top">
<div id="wrapper">
    @include('backend-v2.layout.menu')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
         @include('backend-v2.layout.navbar')
            <div class="container-fluid" id="main-content">
               @yield('content')
            </div>
        </div>
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Senna v0.0.1 2020</span>
                </div>
            </div>
        </footer>

    </div>

</div>

<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

@if(get_role()==5 || get_role()==6)
    <audio id="notifAudio">
        <source src="https://senna.co.id/public/got-it-done.ogg" type="audio/ogg">
    </audio>

    <script src="{{asset('public/backend/')}}/core/manual-tf.js"></script>
<script>
    setInterval(function(){
        getManualTransferNotify("{{url('api/v1/admin/manual-transfer/notify/all')}}");
    }, 5000);

    setInterval(function(){
        getMerchantRegistrationNotif("{{url('api/v1/admin/merchant/regist-notify/all')}}");
    }, 5000);

</script>
@endif
<script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" integrity="sha512-yI2XOLiLTQtFA9IYePU0Q1Wt0Mjm/UkmlKMy4TU4oNXEws0LybqbifbO8t9rEIU65qdmtomQEQ+b3XfLfCzNaw==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" integrity="sha512-qiKM6FJbI5x5+GL5CEbAUK0suRhjXVMRXnH/XQJaaQ6iQPf05XxbFBE4jS6VJzPGIRg7xREZTrGJIZVk1MLclA==" crossorigin="anonymous"></script>
@if(get_role()==2 || get_role()==3 || get_role()>10)

@endif
<script>

    @if(get_role()==2 || get_role()==3)
        $("#branch select").first().find(":selected").removeAttr("selected");
        $("#branch select").find("option").each(function () {
            if ($(this).val() == "{{merchant_id()}}") {
                $(this).attr("selected", true);
            }
        });
    $('#branch select').change(function () {
        $.ajax({
            url: "{{route('merchant.switch-branch')}}?md_merchant_id="+$("#branch select").first().val(),
            type: 'get',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if(data==true)
                {
                    location.reload();
                    toastr.success('Berhasil login ke cabang terkait')
                }
            }
        });

    });
    @endif

</script>
<script>
    $(document).ready(function() {
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $(".timezone").val(timezone);
    });
    $('.menu-pop').tooltip();

</script>
@yield('js')
    <script src="{{asset('public/backend-v2')}}/js/sb-admin-2.min.js"></script>
</body>

</html>

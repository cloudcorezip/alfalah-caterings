<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Senna adalah sebuah layanan multi-platfrom di Indonesia">
    <meta name="author" content="Senna">
    <title>{{$title}}</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link href="{{asset('public/backend')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='_timezone' class="timezone" value=''>
    <script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="{{asset('public')}}{{mix('backend/js/toast-custom.min.js')}}"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    @yield('css')
    <style>
        .bank-logo {
            width:230px;
            height:100px;
            object-fit:contain;
        }

        .merchant-logo {
            width:38px!important;
            object-fit:contain!important;
        }

        .btn:focus,.btn:active {
            outline: none !important;
            box-shadow: none;
        }
        #code_country {
            width:16%;
            border-bottom-right-radius: 0!important;
            border-top-right-radius:0!important;
            border: 1px solid #F0F0F0!important;
        }
        #mobile_number {
            width:84%;
            border-bottom-left-radius: 0!important;
            border-top-left-radius:0!important;
        }
        @media screen and (max-width:768px){
            .bank-logo {
                width: 70px!important;
                height: 40px;
            }
            .d-container {
                margin-top: 5vh;
            }
            .qr-logo {
                width: 90%;
            }
            .bt-name {
                font-size: 12px;
            }

            #code_country {
                width:30%;
            }
            #mobile_number {
                width:70%;
            }
        }

    </style>

</head>
<body class="navigation-toggle-one">
    <div style="background-color:{{is_null($payment_invoice->theme)?'#FFA943':$payment_invoice->theme}}; min-height:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex align-items-center px-4 py-3" id="inv-header">
                        @if(!is_null($merchant->image))
                            <img src="{{env('S3_URL')}}{{$merchant->image}}" alt="logo" class="merchant-logo">
                        @else
                            <img src="{{asset('public/backend/img/no-user.png')}}" alt="foto profil" class="img-fluid img-thumbnail merchant-logo">
                        @endif
                        <span class="d-block ml-3" style="font-size:24px;color:#fff;">
                            {{ucwords($merchant->name)}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="main">
        <main class="main-content poppin">
            <div class="container d-container" id="container-invoice">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="border-radius:10px;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div
                                            class="mb-3 d-flex justify-content-between align-items-center"
                                            style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                        >
                                            <div>
                                                <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                <span style="color:#4F4F4F;font-size:16px;font-weight:bold">
                                                    Pembayaran Transaksi {{$data->code}}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="collapse mb-3" id="virtual-account">
                                            <span>{{$data->code}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body px-0">                                   
                                    @foreach($paymentOption as $item)
                                        @if($item["name"] == "Pembayaran Digital" && count($item["data"]) > 0)
                                            @foreach($item["data"] as $k => $pd)
                                                @if(!empty($pd["data"]))
                                                <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                    <div class="card-body">
                                                        <div>
                                                            <button
                                                                class="mb-3 d-flex justify-content-between align-items-center"
                                                                type="button"
                                                                data-toggle="collapse"
                                                                data-target="#{{str_replace(' ','',$item['name'])}}{{$k}}"
                                                                aria-expanded="false"
                                                                aria-controls="virtual-account"
                                                                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                                            >
                                                                <div>
                                                                    <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                                    <span style="color:#4F4F4F;font-size:16px;">{{$pd['name']}}</span>
                                                                </div>
                                                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                                                            </button>
                                                            <div class="collapse show" id="{{str_replace(' ','',$item['name'])}}{{$k}}">
                                                                <div class="row" id="virtual-account-row">
                                                                    @foreach($pd['data'] as $p)
                                                                    <div class="col-md-6 col-sm-12 card-bank">
                                                                        <a href="#" class="pm-method">
                                                                            <input class="trans_type" type="hidden" name="md_transaction_type_id" value="{{$p['trans_id']}}_{{$p['acc_coa_id']}}_1_{{$p['alias']}}_{{$p['id']}}">
                                                                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                                                <div class="card-body text-center">
                                                                                    <img src="{{env('S3_URL')}}{{$p['icon']}}" alt="{{$p['icon']}}" class="bank-logo">
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endforeach
                                        @endif
                                        @if($item["name"] == "Bank Transfer" && count($item["data"]) > 0)
                                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                <div class="card-body">
                                                    <div>
                                                        <button
                                                            class="mb-3 d-flex justify-content-between align-items-center"
                                                            type="button"
                                                            data-toggle="collapse"
                                                            data-target="#{{str_replace(' ','',$item['name'])}}"
                                                            aria-expanded="false"
                                                            aria-controls="virtual-account"
                                                            style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                                        >
                                                            <div>
                                                                <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                                <span style="color:#4F4F4F;font-size:16px;">{{$item['name']}}</span>
                                                            </div>
                                                            <i class="fas fa-angle-down" style="font-size:16px;"></i>
                                                        </button>
                                                        <div class="collapse show" id="{{str_replace(' ','',$item['name'])}}">
                                                            <div class="row" id="virtual-account-row">
                                                                @foreach($item['data'] as $key =>$other)
                                                                <div class="col-md-6 col-sm-12 card-bank">
                                                                    <a href="#" class="pm-method">
                                                                        <input class="trans_type" type="hidden" name="md_transaction_type_id" value="{{$other->trans_id}}_{{$other->acc_coa_id}}_0_{{$other->id}}">
                                                                        <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                                            <div class="card-body text-center">
                                                                                <h4 class="bt-name">{{$other->name}}</h4>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="coa_json" value="{{$coa_json}}" id="coa_json">
        </main>
    </div>
    <script>
        $(".pm-method").on('click', function(e){
            e.preventDefault();
            var data = new FormData();
            
            let pm = $(this).find('.trans_type').val();

            data.append("md_transaction_type_id",pm);
            data.append("is_debet", 0);
            data.append("sale_id", "{{$data->id}}");
            data.append("coa_json", $("#coa_json").val());

            if(pm.indexOf('ID_OVO') == -1){
                ajaxTransfer("{{route('checkout.create-charge')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            } else {
                var html = `<form class="form-konten"> 
                                <div class="form-group">
                                    <div class="d-flex align-items-center" style="width:100%;">
                                        <input id="code_country" type="text" class="form-control text-center" name="code_country" value="+62" disabled>
                                        <input id="mobile_number" type="text" name="mobile_number" class="form-control" placeholder="No. Handphone OVO">
                                    </div>
                                    <span id="mobile_number_validate" class="text-danger d-none">No Handphone tidak boleh kosong !</span>
                                </div>
                            </form>`;
                modalConfirm("Masukkan No. Handphone OVO", html, function(){
                    var phoneNumberVal = $("#mobile_number").val();
                    var codeCountry = $("#code_country").val();
                    if(!phoneNumberVal){
                        $("#mobile_number_validate").removeClass("d-none").text("No Handphone tidak boleh kosong !");
                        return;
                    } else {
                        var phoneNum = codeCountry + phoneNumberVal;
                        if(!/^(\+62)([8])\d(\d)?[2-9](\d){6,8}$/.test(phoneNum)){
                            $("#mobile_number_validate").removeClass("d-none").text("No Handphone tidak valid !");
                            return;
                        } else {
                            $("#mobile_number_validate").addClass("d-none");
                            data.append("mobile_number", phoneNum);
                            ajaxTransfer("{{route('checkout.create-charge')}}", data, function(response){
                                var data = JSON.parse(response);
                                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                            });
                        }
                        
                    }
                    
                });
            }
            
        })
    </script>
</body>
</html>

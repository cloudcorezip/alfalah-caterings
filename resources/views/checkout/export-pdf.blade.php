<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: Arial, sans-serif; width:100%;margin-right:auto;margin-left:auto">
    <div style="width:100%; margin:0 auto;">
        <div style="background:#FAFAFA;padding:30px;">
            <div style="width:50%;float:left;">
                @if(!is_null($merchant->image))
                <img src="{{env('S3_URL')}}{{$merchant->image}}" alt="logo" style="width:31%;object-fit:contain;">
                @endif
                <h4 style="color:#707070;" class="arial-rounded">{{ucwords($merchant->name)}}</h4>
                @if(!is_null($merchant->address))
                <p style="color:#707070;font-size:12px;">{{$merchant->address}}</p>
                @endif
            </div>
            <div style="width:50%;text-align:right;float:right;">
                @if(!is_null($merchant->email_merchant))
                <p style="color:#707070;font-size:12px;">Email : {{$merchant->email_merchant}}</p>
                @endif

                @if(!is_null($merchant->phone_merchant))
                <p style="color:#707070;font-size:12px;">Phone : {{$merchant->phone_merchant}}</p>
                @endif
            </div>
            <div style="clear:both;"></div>
        </div>
        <br>

        <div style="padding:0 30px 0 30px;">
            <div style="width:50%;float:left;">
                <h3 style="color:#707070;" class="arial-rounded">INVOICE</h3>
            </div>
            <div style="width:50%;text-align:right;float:right;">
                @if($saleOrder->md_sc_transaction_status_id == 1)
                    <h4 style="color:#FF9E0D;">Menunggu Pembayaran</h4>
                @endif
                @if($saleOrder->md_sc_transaction_status_id == 2)
                    <h4 style="color:#62AC29;">Pembayaran Berhasil</h4>
                @endif
            </div>
            <div style="clear:both;"></div>
        </div>

        <div style="padding:0 30px 0 30px;">
            <div style="width:100%;float:left;">
                <p style="color:#707070;font-size:12px;"><strong>Kode Transaksi :</strong> {{$saleOrder->code}}</p>
                <p style="color:#707070;font-size:12px;"><strong>Metode Pembayaran : </strong>{{is_null($data)?'-':strtoupper($data->wallet_name)}}</p>
                <p style="color:#707070;font-size:12px;"><strong>Tanggal : </strong>{{$created_at}}</p>
            </div>
            <div style="clear:both;"></div>
            <br>
            <hr style="color:#E1E1E1;">
        </div>

        <div style="padding:0 30px 0 30px;">
            <div style="width:50%;float:left;">
                <h4 style="color:#FF9743;">BILL FROM :</h4>
                <h5 style="color:#707070;">{{ucwords($merchant->name)}}</h5>
                @if(!is_null($merchant->address))
                <p style="color:#707070;font-size:12px;">{{$merchant->address}}</p>
                @endif
                @if(!is_null($merchant->phone_merchant))
                <p style="color:#707070;font-size:12px;">{{$merchant->phone_merchant}}</p>
                @endif

                @if(!is_null($merchant->email_merchant))
                <p style="color:#707070;font-size:12px;">{{$merchant->email_merchant}}</p>
                @endif

            </div>
            <div style="width:50%;float:left;">
                <h4 style="color:#62AC29;">TO :</h4>
                <h5 style="color:#707070;">{{is_null($customer)?'':ucwords($customer->name)}}</h5>
                <p style="color:#707070;font-size:12px;">{{is_null($customer)? '':$customer->address}}</p>
                <p style="color:#707070;font-size:12px;">{{is_null($customer)? '':$customer->phone_number}}</p>
                <p style="color:#707070;font-size:12px;">{{is_null($customer)? '':$customer->email}}</p>
            </div>
            <div style="clear:both;"></div>
        </div>
        <br>
        <div style="padding:0 30px 0 30px;">
            <table width="100%" cellspacing="0">
                <tr style="background:#F9F9F9;">
                    <th style="padding:0 10px;">
                        <p style="color:#A4A4A4; font-weight:600;font-size:12px; text-align:left;">PRODUK</p>
                    </th>
                    <th>
                        <p style="color:#A4A4A4; font-weight:600;font-size:12px;text-align:center;">QUANTITY</p>
                    </th>
                    <th>
                        <p style="color:#A4A4A4; font-weight:600;font-size:12px;text-align:center;">HARGA</p>
                    </th>
                    <th style="padding:0 10px;">
                        <p style="color:#A4A4A4; font-weight:600;font-size:12px; text-align:right;">SUBTOTAL</p>
                    </th>
                </tr>
                @foreach($saleOrder->getDetail->where('is_deleted',0) as $key =>$item)
                    <tr>
                        <td style="color:#707070;font-size:12px;padding:20px 10px;">{{$item->getProduct->name}}</td>
                        @if ($item->multi_quantity!=0)
                            <td style="color:#707070;font-size:12px;text-align:center;padding:20px 0;">{{$item->multi_quantity}} {{$item->unit_name}}</td>
                        @else
                            <td style="color:#707070;font-size:12px;text-align:center;padding:20px 0;">{{$item->quantity}} {{$item->getProduct->getUnit->name}}</td>
                        @endif
                        @if ($item->is_bonus==0)
                            <td style="color:#707070;font-size:12px;text-align:center;padding:20px 0;">{{rupiah($item->price)}}</td>
                            <td style="color:#707070;font-size:12px;padding:20px 10px;text-align:right;">{{rupiah($item->price*$item->quantity)}}</td>
                        @else
                            <td style="color:#707070;font-size:12px;text-align:center;padding:20px 0;">Bonus Produk</td>
                            <td style="color:#707070;font-size:12px;padding:20px 10px;text-align:right;">0</td>
                        @endif

                        @if ($item->multi_quantity!=0)
                            @if($item->price*$item->multi_quantity!=$item->sub_total)
                            @php
                                $promo_product+=$item->price*$item->quantity-$item->sub_total;
                            @endphp
                            @endif
                        @else
                            @if($item->price*$item->quantity!=$item->sub_total)
                            @php
                                $promo_product+=$item->price*$item->quantity-$item->sub_total;
                            @endphp
                            @endif
                        @endif

                    </tr>
                @endforeach
                <tr>
                    <td colspan="3" style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        Diskon :
                    </td>
                    <td style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        {{rupiah((is_null($saleOrder->promo))?0+$promo_product:$saleOrder->promo+$promo_product)}}
                    </td>
                </tr>
                @if(!is_null($saleOrder->tax))
                <tr>
                    <td colspan="3" style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        Pajak :
                    </td>
                    <td style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        {{rupiah((is_null($saleOrder->tax))?0:$saleOrder->tax)}}
                    </td>
                </tr>
                @endif
                <tr>
                    <td colspan="3" style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        Biaya Admin (Payment Digital) :
                    </td>
                    <td style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        {{rupiah($saleOrder->admin_fee)}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:right;font-weight:bold;color:#707070;padding:10px 0;">
                        Invoice Total :
                    </td>
                    <td style="text-align:right;padding:0 10px;color:#FF8929;padding:10px 0;font-weight:bold;">
                        {{rupiah($saleOrder->total+$saleOrder->admin_fee)}}
                    </td>
                </tr>
            </table>
        </div>

    </div>
<footer>
    <h6>Tautan Pembayaran : <a target="_blank" href="{{route('checkout',['key'=>$saleOrder->id])}}">Klik disini</a> </h6>
</footer>

</body>
</html>

<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{$title}}</title>
    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link href="{{asset('public/backend')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='_timezone' class="timezone" value=''>
    <script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    @yield('css')
    <style>
        .bank-logo {
            width:200px;
            height:80px;
            object-fit:contain;
        }
        .merchant-logo {
            width: 15%;
            object-fit: cover;
        }
        .btn:focus,.btn:active {
            outline: none !important;
            box-shadow: none;
        }
        .link-guide.act {
            background: #F8F8F8;
        }
        .fa-copy {
            color:#D4D4D4;
            cursor:pointer;
        }
        .fa-copy:hover {
            color:#363636;
        }
        @media screen and (max-width:576px){
            .bank-logo {
                width: 120px!important;
                height: 80px;
            }
        }
    </style>

</head>
<body class="navigation-toggle-one">
    <header>
        <div class="card-header px-4 py-3" id="inv-header" style="background-color:{{is_null($payment_invoice->theme)?'#FFA943':$payment_invoice->theme}}; height:50px;"></div>
    </header>
    <div id="main">
        <main class="main-content poppin">
            <div class="container" id="container-invoice">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger text-center" style="background-color:#FFEEEE!important;" role="alert">
                            <i style="color:#FF3D00;" class="fas fa-exclamation-triangle mr-2"></i> 
                            <span style="color:#F25858;">Bayar sebelum {{$expireDate}}</span>
                        </div>  
                    </div>      
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="border-radius:10px;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button
                                            class="mb-3 d-flex justify-content-between align-items-center" 
                                            type="button" 
                                            data-toggle="collapse" 
                                            data-target="#virtual-account" 
                                            aria-expanded="false" 
                                            aria-controls="virtual-account"
                                            style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                        >
                                            <div>
                                                <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                <span style="color:#4F4F4F;font-size:16px;font-weight:bold">Pembayaran Paket Langganan</span>
                                            </div>
                                            <i class="fas fa-angle-down" style="font-size:16px;color:#FF8100;"></i>
                                        </button>
                                        <div class="collapse mb-3" id="virtual-account">
                                            <span>T-0000000032</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card mb-0" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:10px;">
                                            <div class="card-body px-5">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        @if($data->transaction_type_id == 5)
                                                        <h5 class="font-weight-bold" style="color:#323232;">Virtual Account</h5>
                                                        @elseif($data->transaction_type_id == 4)
                                                        <h5 class="font-weight-bold" style="color:#323232;">QRIS</h5>
                                                        @else
                                                        <h5 class="font-weight-bold" style="color:#323232;">E-Wallet</h5>
                                                        @endif

                                                        @if($data->transaction_type_id == 5)
                                                        <h2 style="color:#616161;">
                                                            <span id="va-num">{{$va['account_number']}}</span>
                                                            <i onclick="copyClipboard('va-num')" id="fa-copy" title="copied" class="far fa-copy ml-2"></i>
                                                        </h2>
                                                        @endif
                                                        
                                                    </div>
                                                    <div class="col-md-4">
                                                        <img src="{{env('S3_URL')}}{{$data->icon}}" alt="{{$data->icon}}" class="bank-logo">
                                                        <h5 class="font-weight-bold" style="color:#323232;">Nominal yang dibayarkan</h5>
                                                        <h2 style="color:#62AC29;">{{rupiah(70000)}}</h2>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 offset-md-4">
                                                        <form onsubmit="return false;" class="form-konten" id="form-konten">
                                                            <input type="hidden" name="channel_code" value="{{$channel_code}}">
                                                            <input type="hidden" name="csrf_token" value="{{csrf_token()}}">
                                                            <input type="number" name="phone_number" class="form-control mb-3" placeholder="No Handphone">
                                                            <button style="border-radius:5px;" type="submit" class="btn btn-order__online d-block mx-auto py-2">Bayar Sekarang</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <hr style="border: 1px solid #EDEDED;">
                                    </div>
                                </div>
                                
                                <div class="row mt-3">
                                    <div class="col-md-12 text-right">
                                        <a href="{{route('checkout', ['merchant_id'=>$data->md_merchant_id])}}" class="btn btn-light" style="color:#777777C9;">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script>
        $(document).ready(function(){
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');
                ajaxTransfer("{{route('checkout-ewallet')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });

            });
        });
    </script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Senna adalah sebuah layanan multi-platfrom di Indonesia">
    <meta name="author" content="Senna">
    <title>{{$title}}</title>
    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link href="{{asset('public/backend')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='_timezone' class="timezone" value=''>
    <script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    @yield('css')
    <style>
        .bank-logo {
            width:200px;
            height:80px;
            object-fit:contain;
        }
        .merchant-logo {
            width: 15%;
            object-fit: cover;
        }
        .btn:focus,.btn:active {
            outline: none !important;
            box-shadow: none;
        }
        .link-guide.act {
            background: #F8F8F8;
        }
        .fa-copy {
            color:#D4D4D4;
            cursor:pointer;
        }
        .fa-copy:hover {
            color:#363636;
        }
        @media screen and (max-width:576px){
            .bank-logo {
                width: 120px!important;
                height: 80px;
            }
        }
    </style>

</head>
<body class="navigation-toggle-one">
    <div id="main">
        <main class="main-content poppin">
            <div class="container" id="container-invoice">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <div class="card" style="border-radius:10px;">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col-md-12">
                                        <div>
                                            <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                            <span style="color:#4F4F4F;font-size:16px;font-weight:bold">Pembayaran Penjualan {{$code}}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:10px;">
                                            <div class="card-body">
                                                <div class="row mb-3">
                                                    <div class="col-md-12 text-center">
                                                        <h5 class="font-weight-bold" style="color:#323232;">{{$title}}</h5>
                                                        @if($status == 2)
                                                        <h1 style="color:#00c40d;font-size:80px;">
                                                            <i class="fas fa-check-circle"></i>
                                                        </h1>
                                                        @else
                                                        <h1 style="color:#ff0303;font-size:80px;">
                                                            <i class="fas fa-times-circle"></i>
                                                        </h1>
                                                        @endif
                                                    </div>
                                                </div>
                                                @if($status == 2)
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table style="width:100%;">
                                                            <tr>
                                                                <td class="font-weight-bold" style="color:#b3b3b3;">Kode Transaksi</td>
                                                                <td class="text-right font-weight-bold">{{$code}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="font-weight-bold" style="color:#b3b3b3;">Total Pembayaran</td>
                                                                <td class="text-right font-weight-bold">
                                                                    <h6 class="font-weight-bold" style="color:#62AC29;">{{rupiah($total)}}</h6>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>

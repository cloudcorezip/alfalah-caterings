<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Senna adalah sebuah layanan multi-platfrom di Indonesia">
    <meta name="author" content="Senna">
    <title>{{$title}}</title>
    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link href="{{asset('public/backend')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='_timezone' class="timezone" value=''>
    <script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="{{asset('public')}}{{mix('backend/js/toast-custom.min.js')}}"></script>
    @yield('css')
    <style>
        .row-desc span {
            color:#858796!important;
        }
        .bank-logo {
            width:200px;
            height:80px;
            object-fit:contain;
        }
        .merchant-logo {
            width:38px!important;
            object-fit:contain!important;
        }
        .btn:focus,.btn:active {
            outline: none !important;
            box-shadow: none;
        }
        .link-guide.act {
            background: #F8F8F8;
        }
        .fa-copy {
            color:#D4D4D4;
            cursor:pointer;
        }
        .fa-copy:hover {
            color:#363636;
        }
        .borderless table {
            border-top-style: none;
            border-left-style: none;
            border-right-style: none;
            border-bottom-style: none;
        }
        .qr-logo {
            width:40%;
            image-rendering: pixelated;
        }
        @media screen and (max-width:768px){
            .bank-logo {
                width: 120px!important;
                height: 80px;
            }
            .d-container {
                margin-top: 5vh;
            }
            .qr-logo {
                width: 90%;
            }
        }
    </style>
</head>
<body class="navigation-toggle-one">
    <div style="background-color:{{is_null($payment_invoice->theme)?'#FFA943':$payment_invoice->theme}}" style="min-height:50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex align-items-center px-4 py-3" id="inv-header">
                        @if(!is_null($merchant->image))
                            <img src="{{env('S3_URL')}}{{$merchant->image}}" alt="logo" class="merchant-logo">
                        @else
                            <img src="{{asset('public/backend/img/no-user.png')}}" alt="foto profil" class="img-fluid img-thumbnail merchant-logo">
                        @endif
                        <span class="d-block ml-3" style="font-size:24px;color:#fff;">
                            {{ucwords($merchant->name)}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="main">
        <main class="main-content poppin">
            <div class="container d-container" id="container-invoice">
                @if($saleOrder->md_sc_transaction_status_id == 1)
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger text-center" style="background-color:#FFEEEE!important;" role="alert">
                            <i style="color:#FF3D00;" class="fas fa-exclamation-triangle mr-2"></i>
                            <span style="color:#F25858;"><b>Bayar sebelum {{\Carbon\Carbon::parse($expireDate)->isoFormat(' D MMMM Y  HH:mm:ss')}}</b></span>
                        </div>
                    </div>
                </div>
                @endif
                @if($saleOrder->md_sc_transaction_status_id == 4)
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-light mb-2" href="{{route('checkout', ['key' => $saleOrder->id])}}">Kembali</a>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="border-radius:10px;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div
                                            class="mb-3 d-flex justify-content-between align-items-center"
                                            style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                        >
                                            <div>
                                                <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                <span style="color:#4F4F4F;font-size:16px;font-weight:bold">Pembayaran Penjualan {{$saleOrder->code}} </span>
                                            </div>
                                        </div>
                                        <div class="collapse mb-3" id="virtual-account">
                                            <span>{{$saleOrder->code}}</span>
                                        </div>
                                    </div>
                                </div>

                                @if($data->transaction_type_id == 13)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card mb-0" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:10px;">
                                                <div class="card-body px-5">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            <h3 class="font-weight-bold" style="color:#323232;">{{$data->payment_name}}</h3>
                                                            <h3 style="color:#616161;">
                                                                <span id="va-num">{{$data->bank_account_number}}</span>
                                                                <i onclick="copyClipboard('va-num',1)" title="copied" class="far fa-copy ml-2 copy-1"></i>
                                                            </h3>
                                                            {{---<img src="{{env('S3_URL')}}{{$data->icon}}" alt="{{$data->icon}}" style="width:40%;image-rendering: pixelated;" class="img-fluid"> ---}}
                                                            <h5 class="font-weight-bold" style="color:#323232;">Nominal yang harus dibayarkan</h5>
                                                            <h2 style="color:#62AC29;">{{rupiah($saleOrder->total)}}</h2>
                                                            @if($saleOrder->md_sc_transaction_status_id == "1")
                                                            <h6 style="color:#858796;">Status: Menunggu Pembayaran</h6>
                                                            @elseif($saleOrder->md_sc_transaction_status_id == "2")
                                                            <h6 style="color:#858796;">Status: Pembayaran Berhasil</h6>
                                                            @else
                                                            <h6 style="color:#858796;">Status: Pembayaran Gagal</h6>
                                                            @endif
                                                            <a href="{{route('checkout.export-pdf', ['key'=> encrypt($saleOrder->id)])}}" target="_blank" class="btn btn-outline-warning btn-xs btn-rounded">
                                                                <i class="far fa-file-pdf mr-2"></i>Download Invoice
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($data->transaction_type_id != 4 && $data->transaction_type_id != 5 && $data->transaction_type_id != 13)
                                    @if(strtolower($data->wallet_name) != "ovo")
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card mb-0" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:10px;">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <h5 class="font-weight-bold mb-2" style="color:#323232;">E-Wallet - {{$data->wallet_name}}</h5>
                                                            <div class="row d-flex align-items-center mb-2 row-desc">
                                                                <div class="col-md-4 col-6">
                                                                    <span><b>Nominal Transaksi</b></span>
                                                                </div>
                                                                <div class="col-md-3 col-6" style="text-align: right">
                                                                    <span>
                                                                        {{rupiah($saleOrder->total)}}
                                                                         </span>
                                                                </div>
                                                            </div>
                                                            <div class="row d-flex align-items-center mb-2 row-desc">
                                                                <div class="col-md-4 col-6">
                                                                    <span><b>Biaya Admin</b></span>
                                                                </div>
                                                                <div class="col-md-3 col-6" style="text-align: right">
                                                                    <span>
                                                                        {{rupiah($saleOrder->admin_fee)}}
                                                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="row d-flex align-items-center mb-2 row-desc">
                                                                <div class="col-md-4 col-6">
                                                                    <span><b>Total Transaksi</b></span>
                                                                </div>
                                                                <div class="col-md-3 col-6" style="text-align: right">
                                                                    <span>
                                                                        {{rupiah($saleOrder->total+$saleOrder->admin_fee)}}
                                                                         </span>
                                                                </div>
                                                            </div>
                                                            @if($saleOrder->md_sc_transaction_status_id == "1")
                                                                <div class="row d-flex align-items-center mb-2">
                                                                    <div class="col-md-4 col-6">
                                                                        <span><b>Status</b></span>
                                                                    </div>
                                                                    <div class="col-md-3 col-6" style="color: red;text-align: right">
                                                                        <span>Menunggu Pembayaran</span>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if($saleOrder->md_sc_transaction_status_id == "2")
                                                                <div class="row d-flex align-items-center mb-2 ">
                                                                    <div class="col-md-4 col-6">
                                                                        <span><b>Status</b></span>
                                                                    </div>
                                                                    <div class="col-md-3 col-6" style="color: green;text-align: right">
                                                                        <span>Pembayaran Berhasil</span>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            @if($saleOrder->md_sc_transaction_status_id == "4")
                                                                <div class="row d-flex align-items-center mb-2 ">
                                                                    <div class="col-md-4 col-6">
                                                                        <span><b>Status</b></span>
                                                                    </div>
                                                                    <div class="col-md-3 col-6" style="color: red;text-align: right">
                                                                        <span>Pembayaran Gagal</span>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            
                                                            @if(strtolower($data->wallet_name) == "shopeepay")
                                                            <div class="row d-flex align-items-center mb-2 row-desc">
                                                                <div class="col-md-4 col-6">
                                                                    <span><b>Salin Tautan Pembayaran</b></span>
                                                                </div>
                                                                <div class="col-md-3 col-6" style="text-align: right">
                                                                    <span>
                                                                        <span id="va-num" style="display: none">{{$va->actions->mobile_deeplink_checkout_url}}</span>
                                                                        <i onclick="copyClipboard('va-num',1)" title="copied" class="far fa-copy ml-2 copy-1"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="row d-flex align-items-center mb-2 row-desc">
                                                                <div class="col-md-4 col-6">
                                                                    <span><b>Tautan Pembayaran</b></span>
                                                                </div>
                                                                <div class="col-md-3 col-6" style="text-align: right">
                                                                    <a href="{{$va->actions->mobile_deeplink_checkout_url}}" target="_blank" class="btn btn-outline-success btn-xs text-success btn-rounded">
                                                                        <i class="fa fa-link mr-1"></i> Klik Disini
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            @else
                                                            <div class="row d-flex align-items-center mb-2 row-desc">
                                                                <div class="col-md-4 col-6">
                                                                    <span><b>Salin Tautan Pembayaran</b></span>
                                                                </div>
                                                                <div class="col-md-3 col-6" style="text-align: right">
                                                                    <span>
                                                                        <span id="va-num" style="display: none">{{$va->actions->desktop_web_checkout_url}}</span>
                                                                        <i onclick="copyClipboard('va-num',1)" title="copied" class="far fa-copy ml-2 copy-1"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="row d-flex align-items-center mb-2 row-desc">
                                                                <div class="col-md-4 col-6">
                                                                    <span><b>Tautan Pembayaran</b></span>
                                                                </div>
                                                                <div class="col-md-3 col-6" style="text-align: right">
                                                                    <a href="{{$va->actions->desktop_web_checkout_url}}" target="_blank" class="btn btn-outline-success btn-xs text-success btn-rounded">
                                                                        <i class="fa fa-link mr-1"></i> Klik Disini
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            @endif
                                                            <div class="row d-flex align-items-center mb-2 row-desc">
                                                                <div class="col-md-4 col-6">
                                                                    <span><b>Download Invoice</b></span>
                                                                </div>
                                                                <div class="col-md-3 col-6" style="text-align: right">
                                                                    <a href="{{route('checkout.export-pdf', ['key'=> encrypt($saleOrder->id)])}}" target="_blank" class="btn btn-outline-warning btn-xs btn-rounded">
                                                                        <i class="far fa-file-pdf mr-2"></i>Download Invoice
                                                                    </a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-4">
                                                            <img src="{{env('S3_URL')}}{{$data->icon_wallet}}" alt="{{$data->icon_wallet}}" class="bank-logo">
                                                            <h5 class="font-weight-bold" style="color:#323232;">Nominal yang dibayarkan</h5>
                                                            <h2 style="color:#62AC29;">{{rupiah($saleOrder->total+$saleOrder->admin_fee)}}</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card mb-0" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:10px;">
                                                <div class="card-body px-5">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            <h5 class="font-weight-bold" style="color:#323232;">E-Wallet - {{$data->wallet_name}}</h5>
                                                            <img src="{{env('S3_URL')}}{{$data->icon_wallet}}" alt="{{$data->icon_wallet}}" class="bank-logo">
                                                            <h5 class="font-weight-bold" style="color:#323232;">Nominal yang dibayarkan</h5>
                                                            <h2 style="color:#62AC29;">{{rupiah($saleOrder->total+$saleOrder->admin_fee)}}</h2>
                                                            @if($saleOrder->md_sc_transaction_status_id == "1")
                                                            <h6 style="color:#858796;">Status: Menunggu Pembayaran</h6>
                                                            @endif
                                                            @if($saleOrder->md_sc_transaction_status_id == "2")
                                                            <h6 style="color:#858796;">Status: Pembayaran Berhasil</h6>
                                                            @endif
                                                            @if($saleOrder->md_sc_transaction_status_id == "4")
                                                            <h6 style="color:#858796;">Status: Pembayaran Gagal</h6>
                                                            @endif
                                                            <a href="{{route('checkout.export-pdf', ['key'=> encrypt($saleOrder->id)])}}" target="_blank" class="btn btn-outline-warning btn-xs btn-rounded">
                                                                <i class="far fa-file-pdf mr-2"></i>Download Invoice
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr style="border: 1px solid #EDEDED;">
                                        </div>
                                    </div>
                                    @endif
                                @endif

                                @if($data->transaction_type_id == 4)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card mb-0" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:10px;">
                                                <div class="card-body px-5">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            <h3 class="font-weight-bold" style="color:#323232;">{{$data->wallet_name}}</h3>
                                                            <img src="{{env('S3_URL')}}{{$saleOrder->qr_code}}" alt="{{$saleOrder->qr_code}}" class="img-fluid qr-logo">
                                                            <h5 class="font-weight-bold" style="color:#323232;">Nominal yang dibayarkan</h5>
                                                            <h2 style="color:#62AC29;">{{rupiah($saleOrder->total+$saleOrder->admin_fee)}}</h2>
                                                            @if($saleOrder->md_sc_transaction_status_id == "1")
                                                            <h6 style="color:#858796;">Status: Menunggu Pembayaran</h6>
                                                            @endif
                                                            @if($saleOrder->md_sc_transaction_status_id == "2")
                                                            <h6 style="color:#858796;">Status: Pembayaran Berhasil</h6>
                                                            @endif
                                                            @if($saleOrder->md_sc_transaction_status_id == "4")
                                                            <h6 style="color:#858796;">Status: Pembayaran Gagal</h6>
                                                            @endif
                                                            <a href="{{route('checkout.export-pdf', ['key'=> encrypt($saleOrder->id)])}}" target="_blank" class="btn btn-outline-warning btn-xs btn-rounded mb-2">
                                                                <i class="far fa-file-pdf mr-2"></i>Download Invoice
                                                            </a>
                                                            <a href="{{env('S3_URL')}}{{$saleOrder->qr_code}}" target="_blank" class="btn btn-outline-success btn-xs text-success btn-rounded mb-2">
                                                                <i class="far fa-file-pdf mr-2"></i>Download QR Code
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($data->transaction_type_id == 5)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card mb-0" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:10px;">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <h5 class="font-weight-bold" style="color:#323232;">Virtual Account</h5>
                                                            <h2 style="color:#616161;">
                                                                <span id="va-num">{{$va->account_number}}</span>
                                                                <i onclick="copyClipboard('va-num',1)" title="copied" class="far fa-copy ml-2 copy-1"></i>
                                                            </h2>
                                                            @if($saleOrder->md_sc_transaction_status_id == "1")
                                                            <h6 style="color:#858796;">Status: Menunggu Pembayaran</h6>
                                                            @endif
                                                            @if($saleOrder->md_sc_transaction_status_id == "2")
                                                            <h6 style="color:#858796;">Status: Pembayaran Berhasil</h6>
                                                            @endif
                                                            <a href="{{route('checkout.export-pdf', ['key'=> encrypt($saleOrder->id)])}}" target="_blank" class="btn btn-outline-warning btn-xs btn-rounded">
                                                                <i class="far fa-file-pdf mr-2"></i>Download Invoice
                                                            </a>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <img src="{{env('S3_URL')}}{{$data->icon}}" alt="{{$data->icon}}" class="bank-logo">
                                                            <h5 class="font-weight-bold" style="color:#323232;">Nominal yang dibayarkan</h5>
                                                            <h2 style="color:#62AC29;">{{rupiah($va->expected_amount)}}</h2>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr style="border: 1px solid #EDEDED;">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <ul class="list-group mb-2">
                                                <li class="list-group-item link-guide act">
                                                    <a href="" data-target="atm" class="d-flex align-items-center justify-content-between">
                                                        Petunjuk Transfer ATM
                                                        <i class="fas fa-chevron-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <ul class="list-group mb-2">
                                                <li class="list-group-item link-guide">
                                                    <a href="" data-target="i-bank" class="d-flex align-items-center justify-content-between">
                                                        Petunjuk iBanking
                                                        <i class="fas fa-chevron-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <ul class="list-group mb-2">
                                                <li class="list-group-item link-guide">
                                                    <a href="" data-target="m-bank" class="d-flex align-items-center justify-content-between">
                                                        Petunjuk Transfer M-Banking
                                                        <i class="fas fa-chevron-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        @if(strtoupper($data->bank_code) == 'BNI')
                                            <div class="col-md-8">
                                                <div id="atm" class="card mb-0 card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1 : TEMUKAN ATM TERDEKAT</h5>
                                                        <ol>
                                                            <li>Masukkan kartu ATM anda</li>
                                                            <li>Pilih bahasa</li>
                                                            <li>Masukkan PIN ATM anda</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Pilih "Menu Lainnya"</li>
                                                            <li>Pilih "Transfer"</li>
                                                            <li>Pilih jenis rekening yang akan anda gunakan (contoh: "Dari Rekening Tabungan")</li>
                                                            <li>Pilih "Virtual Account Billing"</li>
                                                            <li>Masukkan Nomor Virtual Account anda <strong>{{$va->account_number}}</strong></li>
                                                            <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi</li>
                                                            <li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div id="i-bank" class="card mb-0 d-none card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1 : MASUK KE AKUN ANDA</h5>
                                                        <ol>
                                                            <li>Buka situs <a href="https://ibank.bni.co.id" target="_blank">https://ibank.bni.co.id</a></li>
                                                            <li>Masukkan User ID dan Password</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : DETAIL PEMBAYARAN</h5>
                                                        <ol>
                                                            <li>Pilih menu "Transfer"</li>
                                                            <li>Pilih menu "Virtual Account Billing"</li>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong></li>
                                                            <li>Lalu pilih rekening debet yang akan digunakan. Kemudian tekan "Lanjut"</li>
                                                            <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : TRANSAKSI BERHASIL</h5>
                                                        <ol>
                                                            <li>Setelah transaksi Anda selesai, simpan bukti pembayaran</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate secara otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div id="m-bank" class="card mb-0 d-none card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1: MASUK KE AKUN ANDA</h5>
                                                        <ol>
                                                            <li>Akses BNI Mobile Banking melalui handphone</li>
                                                            <li>Masukkan User ID dan Password</li>
                                                            <li>Pilih menu "Transfer"</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2: DETAIL PEMBAYARAN</h5>
                                                        <ol>
                                                            <li>Pilih menu "Virtual Account Billing", lalu pilih rekening debet</li>
                                                            <li>Masukkan Nomor Virtual Account anda {{$va->account_number}} pada menu "Input Baru"</li>
                                                            <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi</li>
                                                            <li>Konfirmasi transaksi dan masukkan Password Transaksi</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3: TRANSAKSI BERHASIL</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate secara otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(strtoupper($data->bank_code) == 'BRI')
                                            <div class="col-md-8">
                                                <div id="atm" class="card mb-0 card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1 : TEMUKAN ATM TERDEKAT</h5>
                                                        <ol>
                                                            <li>Masukkan kartu, kemudian pilih bahasa dan masukkan PIN anda</li>
                                                            <li>Pilih "Transaksi Lain" dan pilih "Pembayaran"</li>
                                                            <li>Pilih menu "Lainnya" dan pilih "Briva"</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong> dan jumlah yang ingin anda bayarkan</li>
                                                            <li>Periksa data transaksi dan tekan "YA"</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div id="i-bank" class="card mb-0 d-none card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1: MASUK KE AKUN ANDA</h5>
                                                        <ol>
                                                            <li>Buka situs <a href="https://ib.bri.co.id/ib-bri/" target="_blank">https://ib.bri.co.id/ib-bri/</a>, dan masukkan USER ID dan PASSWORD anda</li>
                                                            <li>Pilih "Pembayaran" dan pilih "Briva"</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong> dan jumlah yang ingin anda bayarkan</li>
                                                            <li>Masukkan password anda kemudian masukkan mToken internet banking</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div id="m-bank" class="card mb-0 d-none card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1: MASUK KE AKUN ANDA</h5>
                                                        <ol>
                                                            <li>Buka aplikasi BRI Mobile Banking, masukkan USER ID dan PIN anda</li>
                                                            <li>Pilih "Pembayaran" dan pilih "Briva"</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong> dan jumlah yang ingin anda bayarkan</li>
                                                            <li>Masukkan PIN Mobile Banking BRI</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(strtoupper($data->bank_code) == 'PERMATA')
                                            <div class="col-md-8">
                                                <div id="atm" class="card mb-0 card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1 : TEMUKAN ATM TERDEKAT</h5>
                                                        <ol>
                                                            <li>Masukkan kartu ATM Permata anda</li>
                                                            <li>Masukkan PIN</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Pilih menu "Transaksi Lainnya"</li>
                                                            <li>Pilih menu "Pembayaran"</li>
                                                            <li>Pilih menu "Pembayaran Lainnya"</li>
                                                            <li>Pilih menu "Virtual Account"</li>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong></li>
                                                            <li>Lalu pilih rekening debet yang akan digunakan</li>
                                                            <li>Konfirmasi detail transaksi anda</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div id="i-bank" class="card mb-0 d-none card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1: MASUK KE AKUN ANDA</h5>
                                                        <ol>
                                                            <li>Buka situs <a href="https://new.permatanet.com" target="_blank">https://new.permatanet.com</a></li>
                                                            <li>Masukkan User ID dan Password</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Pilih "Pembayaran Tagihan"</li>
                                                            <li>Pilih "Virtual Account"</li>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong></li>
                                                            <li>Periksa kembali detail pembayaran anda</li>
                                                            <li>Masukkan otentikasi transaksi/token</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div id="m-bank" class="card mb-0 d-none card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1: MASUK KE AKUN ANDA</h5>
                                                        <ol>
                                                            <li>Buka aplikasi PermataMobile Internet</li>
                                                            <li>Masukkan User ID dan Password</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Pilih "Pembayaran Tagihan"</li>
                                                            <li>Pilih "Virtual Account"</li>
                                                            <li>Masukkan Nomor Virtual Account anda <strong>{{$va->account_number}}</strong></li>
                                                            <li>Masukkan otentikasi transaksi/token</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(strtoupper($data->bank_code) == 'MANDIRI')
                                            <div class="col-md-8">
                                                <div id="atm" class="card mb-0 card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1 : TEMUKAN ATM TERDEKAT</h5>
                                                        <ol>
                                                            <li>Masukkan ATM dan tekan "Bahasa Indonesia"</li>
                                                            <li>Masukkan PIN, lalu tekan "Benar"</li>
                                                            <li>Pilih "Pembayaran", lalu pilih "Multi Payment"</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Masukkan kode perusahaan "{{$va->merchant_code}}", lalu tekan "BENAR"</li>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong>, lalu tekan "BENAR"</li>
                                                            <li>Masukkan nominal yang ingin di transfer, lalu tekan "BENAR"</li>
                                                            <li>Informasi pelanggan akan ditampilkan, pilih nomor 1 sesuai dengan nominal pembayaran kemudian tekan "YA"</li>
                                                            <li>Konfirmasi pembayaran akan muncul, tekan "YES", untuk melanjutkan</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div id="i-bank" class="card mb-0 d-none card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">LANGKAH 1: MASUK KE AKUN ANDA</h5>
                                                        <ol>
                                                            <li>Buka situs Mandiri Internet Banking <a href="https://ibank.bankmandiri.co.id" target="_blank">https://ibank.bankmandiri.co.id</a></li>
                                                            <li>Masuk menggunakan USER ID dan PASSWORD anda</li>
                                                            <li>Buka halaman beranda, kemudian pilih "Pembayaran"</li>
                                                            <li>Pilih "Multi Payment"</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h5>
                                                        <ol>
                                                            <li>Pilih {{$va->merchant_code}} sebagai penyedia jasa</li>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong></li>
                                                            <li>Lalu pilih Lanjut</li>
                                                            <li>Apabila semua detail benar tekan "KONFIRMASI"</li>
                                                            <li>Masukkan PIN / Challenge Code Token</li>
                                                        </ol>
                                                        <h5 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h5>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div id="m-bank" class="card mb-0 d-none card-guide" style="box-shadow:none!important;border: 1px solid #D9D9D9;border-radius:6px;">
                                                    <div class="card-body">
                                                        <h5 class="font-weight-bold">MBANKING BLUE LIVIN APP</h5>
                                                        <h6 class="font-weight-bold">LANGKAH 1: MASUK KE AKUN ANDA</h6>
                                                        <ol>
                                                            <li>Buka aplikasi Mandiri Online, masukkan USERNAME dan PASSWORD anda</li>
                                                            <li>Pilih "Bayar"</li>
                                                            <li>Pilih "Multipayment"</li>
                                                        </ol>
                                                        <h6 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h6>
                                                        <ol>
                                                            <li>Pilih {{$va->merchant_code}} sebagai penyedia jasa</li>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong></li>
                                                            <li>Lalu pilih Lanjut</li>
                                                            <li>Tinjau dan konfirmasi detail transaksi anda, lalu tekan Konfirmasi</li>
                                                            <li>Selesaikan transaksi dengan memasukkan MPIN anda</li>
                                                        </ol>
                                                        <h6 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h6>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                        <hr>
                                                        <h5 class="font-weight-bold">MBANKING YELLOW LIVIN APP</h5>
                                                        <h6 class="font-weight-bold">LANGKAH 1: MASUK KE AKUN ANDA</h6>
                                                        <ol>
                                                            <li>Buka aplikasi Livin by Mandiri, masukkan PASSWORD atau lakukan verifikasi wajah</li>
                                                            <li>Pilih "Bayar"</li>
                                                            <li>Cari "Xendit {{$va->merchant_code}}"</li>
                                                        </ol>
                                                        <h6 class="font-weight-bold">LANGKAH 2 : Detail Pembayaran</h6>
                                                        <ol>
                                                            <li>Pilih {{$va->merchant_code}} sebagai penyedia jasa</li>
                                                            <li>Masukkan Nomor Virtual Account <strong>{{$va->account_number}}</strong></li>
                                                            <li>Nominal pembayaran akan terisi secara otomatis</li>
                                                            <li>Tinjau dan konfirmasi detail transaksi anda, lalu tekan Konfirmasi</li>
                                                            <li>Selesaikan transaksi dengan memasukkan MPIN anda</li>
                                                        </ol>
                                                        <h6 class="font-weight-bold">LANGKAH 3 : Transaksi Berhasil</h6>
                                                        <ol>
                                                            <li>Transaksi Anda telah selesai</li>
                                                            <li>Setelah transaksi anda selesai, invoice ini akan diupdate otomatis. Proses ini mungkin memakan waktu hingga 5 menit</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script>
        $('#fa-copy').tooltip('hide');
        function copyClipboard(target){
            let copyText = document.getElementById('va-num');
            let el = document.createElement('input');
            document.body.appendChild(el);
            el.value = copyText.innerText;
            el.select();
            document.execCommand("copy");
            document.body.removeChild(el);
            $('#fa-copy').tooltip('show');
            setTimeout(() => {
                $('#fa-copy').tooltip('dispose');
            }, 1000);
        }

        function changeGuide(e, target){
            let container = document.querySelectorAll('.card-guide');
            let linkEl = document.querySelectorAll('.link-guide');
            let parent = e.target.parentNode;

            container.forEach(item => {
                item.classList.add('d-none');
            });

            linkEl.forEach(item => {
                item.classList.remove('act');
            });

            parent.classList.add('act');
            document.querySelector('#'+target).classList.remove('d-none');
        }

        $('.link-guide a').on('click', function(e){
            e.preventDefault();
            var target = $(this).attr('data-target');
            changeGuide(e, target);
        });
    </script>

    <script src="//{{ Request::getHost() }}:{{env('LARAVEL_ECHO_PORT')}}/socket.io/socket.io.js"></script>
    <script src="{{asset('public/js/laravel-echo.js')}}" type="text/javascript"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <script type="text/javascript">
        Echo.channel('senna_database_payment-success')
            .listen('PaymentSuccess', e => {
                var order = {{$saleOrder->id}}
                    
                if(order==e.key_1){
                    if(e.key_2==2){
                        toastForSaveData('Pembayaran berhasil dilakukan','success',false,'{{route('checkout-detail', ['key' => encrypt($saleOrder->id)])}}');
                    }
                }
            })
    </script>
</body>
</html>

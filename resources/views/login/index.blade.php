<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi.">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <title>{{$title}}</title>
    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />

    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/backend/css/auth.css')}}">
    <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>
    <meta name="google-signin-client_id" content="{{env('GOOGLE_CLIENT_ID')}}">
    <meta name="base-url" content="https://senna.co.id">
    <meta name="base-domain" content="senna.co.id">
    <meta name="author" content="Senna">

    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi.">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi." />
    <meta name="twitter:url" content="{{route('login')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('login')}}" />
    <meta property="og:description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi." />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />

</head>

<body style="background: white;">
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BHVKPP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<div class="auth-wrapper">
    <div class="row auth-row__wrapper justify-content-center">
        <div class="auth-bg__dark"></div>
        <div class="col-lg-5 col-md-6 auth-right__content" style="align-self: center">
            <div class="card px-2 auth-card__wrapper">
                <div class="card-body">
                    <h2 class="auth-form__title mb-3">Login</h2>
                    <div id="results"></div>
                    <form id="form-konten" action=""  class="user" onsubmit="return false">
                        <div class="form-group" style="position:relative">
                            <i
                                class="fa fa-envelope"
                                style="position:absolute;top:50%; transform:translateY(-50%); left:15px; font-size:14px;color:#C4C4C4;"
                            ></i>
                            <input type="email" name="email" class="form-control border-0 form-input" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="E-mail" style="padding-left:35px!important;" required>
                        </div>
                        <div class="mb-1" style="position:relative">
                            <i
                                class="fa fa-unlock-alt"
                                style="position:absolute;top:50%; transform:translateY(-50%); left:15px; font-size:16px;color:#C4C4C4;"
                            ></i>
                            <input name="password" type="password" class="form-control border-0 form-input" id="password" placeholder="Password" style="padding-left:35px!important;" required/>
                            <i
                                id="input-eye1"
                                style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                                class="fa fa-eye">
                            </i>
                        </div>
                        <button class="btn btn-auth__submit btn-block" type="submit">
                            <span class="bold poppin" style="font-size: 10pt">Login</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12 p-0 d-flex align-items-end justify-content-center">
            <span class="text-center pb-2">
                © {{date('Y')}} All Rights Reserved
            </span>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<input type='hidden' name='_token' value='{{ csrf_token() }}'>
<input type='hidden' name='_timezone' class="timezone" value=''>
<script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>

<script>
    window.onbeforeunload = function(e){
        console.log("tidak ada auth");
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.disconnect();

    };
    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        $.ajax({
            url: "{{route('login.validate')}}?key={{$key}}&is_gmail=1",
            headers: {
                'X-CSRF-TOKEN':"{{csrf_token()}}"
            },
            type: "POST",
            data: {
                "email":profile.getEmail()
            },
            success: function (response) {
                console.log(response);
                if(response.status==true){
                    window.location.replace(response.dashboard);
                }else{
                    window.location.replace(response.dashboard);
                }
            },
        });

    }
</script>

<script type="text/javascript">
    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{url('/login/validate-login?key=')}}{{$key}}&is_gmail=0", data, '#results');
    });

    $('#input-eye1').on('click', function(){
        if($('#input-eye1').hasClass('fa fa-eye')){
            $('#password').attr('type', 'text');
            $('#input-eye1').removeClass('fa fa-eye');
            $('#input-eye1').addClass('fa fa-eye-slash');
        } else {
            $('#password').attr('type', 'password');
            $('#input-eye1').removeClass('fa fa-eye-slash');
            $('#input-eye1').addClass('fa fa-eye');
        }

    });

    $(document).ready(function() {
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $(".timezone").val(timezone);
        @if(isset($is_destroy))
        @if(!is_null($is_destroy))
        localStorage.removeItem('isActiveMenu');
        localStorage.removeItem('isActiveLink');
        @endif
        @endif
    });

</script>
</body>


</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge" /><![endif]-->
    <title>{{$title}}</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{asset('public/frontend')}}/img/fav.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="https://help.senna.co.id/wp-content/uploads/2020/10/Artboard-27hdpi-150x150.png" />
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700,900" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('public/frontend')}}/css/frontend.min.css" />
    <link rel="stylesheet" href="{{asset('public/backend')}}/css/auth.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public')}}/backend/css/senna.css"/>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>

    @if(env('APP_ENV')=='production')
        <meta name="google-site-verification" content="pKkY-T_xZ2bzNn9zrDShMe-nufacfr5RO_eJ9Ww6ENg" />
    @endif
    @yield('meta')
    @if(env('APP_ENV')=='production')
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-04M6PQZHK6"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-04M6PQZHK6');
        </script>
    @endif
</head>
<body style="background-color:#ffffff; min-height:100vh;">
<!-- Google Tag Manager (noscript) -->
<!-- End Google Tag Manager (noscript) -->
@include('frontend.layout.partial.nav')
@if($availableEvent["status"])
    <div class="container my-9">
        <div class="row">
            <div class="col-md-7">
                <div id="results"></div>
                @if(Session::has('message'))
                    <div class="alert alert-warning text-center alert-dismissible fade show" role="alert">
                        {{ Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if(is_null($step))
                    <ul class="progressbar" style="margin-bottom:40px;margin-top:30px;">
                        <div class="connector"></div>
                        <li class="progressbar-item">
                            <a class="nav-link-auth active" style="pointer-events:none;">1</a>
                        </li>
                        <li class="progressbar-item">
                            <a class="nav-link-auth" style="pointer-events:none;">2</a>
                        </li>
                        <li class="progressbar-item">
                            <a class="nav-link-auth" style="pointer-events:none;">3</a>
                        </li>
                    </ul>
                @elseif($step == 2)
                    <ul class="progressbar" style="margin-bottom:40px;margin-top:30px;">
                        <div class="connector"></div>
                        <li class="progressbar-item">
                            <a class="nav-link-auth" style="pointer-events:none;">1</a>
                        </li>
                        <li class="progressbar-item">
                            <a class="nav-link-auth active" style="pointer-events:none;">2</a>
                        </li>
                        <li class="progressbar-item">
                            <a class="nav-link-auth" style="pointer-events:none;">3</a>
                        </li>
                    </ul>
                @else
                    <ul class="progressbar" style="margin-bottom:40px;margin-top:30px;">
                        <div class="connector"></div>
                        <li class="progressbar-item">
                            <a class="nav-link-auth" style="pointer-events:none;">1</a>
                        </li>
                        <li class="progressbar-item">
                            <a class="nav-link-auth" style="pointer-events:none;">2</a>
                        </li>
                        <li class="progressbar-item">
                            <a class="nav-link-auth active" style="pointer-events:none;">3</a>
                        </li>
                    </ul>
                @endif

                @if($step != 3)
                    <h4 class="font-bolder text-dark" style="margin-bottom:40px;">Apakah kamu Ingin Mengikuti Event Kami ?<br>Daftarkan Diri Kamu disini.</h4>
                @endif

                @if(is_null($step))
                    <form id="form-konten" action="{{route('event.register', [$event->slug])}}"  class="user" method="post">
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type='hidden' name='_timezone' class="timezone" value=''>
                        <input type="hidden" name="event_id" value="{{$event->id}}">
                        <input type="hidden" name="slug" value="{{$event->slug}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-4">
                                    <input type="text" name="fullname" class="form-control border-0 form-input" placeholder="Nama Lengkap" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <input type="email" name="email" class="form-control border-0 form-input" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <input type="number" name="phone_number" class="form-control border-0 form-input" placeholder="No HP" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <input type="text" name="address" class="form-control border-0 form-input" placeholder="Alamat" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <select name="gender" id="" class="form-control border-0 form-input" required style="color:#B5B5B5;">
                                        <option value="" disabled selected>Jenis Kelamin</option>
                                        <option value="L">Laki - Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-4">
                                    <select name="province" id="province" class="form-control" required>
                                        <option value="" disabled selected>--- Pilih Provinsi ---</option>
                                        @foreach($provinces as $province)
                                            <option value="{{$province->id}}">{{$province->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="form-group" id="city-container">
                                    <select name="city" id="city" class="form-control form-control-sm" required>
                                        <option value="" disabled selected>--- Pilih Kota ---</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <div class="form-group" id="district-container">
                                    <select name="district" id="district" class="form-control form-control-sm" required>
                                        <option value="" disabled selected>--- Pilih Kecamatan ---</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="wrapper-100">
                            <button type="submit" class="btn btn-warning" style="border:none;">
                                <span class="poppin" style="font-size: 12px">Simpan & Lanjut</span>
                            </button>
                        </div>
                    </form>
                @elseif($step == 2)
                    <form id="form-konten2" action=""  class="user" onsubmit="return false">
                        <input type="hidden" name="step" value="2">
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type='hidden' name='_timezone' class="timezone" value=''>
                        @if(!is_null($stepOne))
                            <input type="hidden" name="fullname" value="{{$stepOne['fullname']}}">
                            <input type="hidden" name="email" value="{{$stepOne['email']}}">
                            <input type="hidden" name="phone_number" value="{{$stepOne['phone_number']}}">
                            <input type="hidden" name="address" value="{{$stepOne['address']}}">
                            <input type="hidden" name="gender" value="{{$stepOne['gender']}}">
                            <input type="hidden" name="district_id" value="{{$stepOne['district']}}">
                        @endif
                        <input type="hidden" name="event_id" value="{{$event->id}}">
                        <input type="hidden" name="slug" value="{{$event->slug}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <input type="text" name="company_name" class="form-control border-0 form-input" placeholder="Nama Usaha" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-4">
                                    <input type="file" name="trans_approve" class="form-control border-0 form-input" placeholder="Upload Bukti Transaksi">
                                    <small>Upload Bukti Transaksi</small>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <select name="type_id" class="form-control font-control-sm" id="type_id" required>
                                    <option></option>
                                    @foreach($type as $t)
                                        <option value="{{$t->id}}">{{$t->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <select class="form-control font-control-sm" name="category_id[]" multiple="multiple" id="category_id" required>
                                    <option></option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="wrapper-100">
                            <button class="btn btn-warning" type="submit" style="border:none;">
                                <span class="poppin" style="font-size: 12px">Simpan & Lanjut</span>
                            </button>
                        </div>
                    </form>
                @else
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="{{asset('public/backend/icons/register/check.png')}}" alt="" class="mb-4">
                            <h5 style="color:#6C6C6C;" class="mb-3">SELESAI</h5>
                            <span style="color:#6C6C6C;">Data yang sudah kamu isikan akan diverifikasi oleh kami. Silahkan lanjutkan konfirmasimu melalui WA.</span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-10 offset-md-2">
                        <div class="text-center py-2 text-white" style="margin-top:30px; background-color:#F48637;">
                            <span>Detail Event</span>
                        </div>
                        <div style="padding-left:10px; padding-right:10px; border:1px solid #F48637;">
                            <table style="width:100%;">
                                <tr>
                                    <td style="color:#848484; font-size:14px; padding:12px;">Nama Event</td>
                                    <td>{{$event->name}}</td>
                                </tr>

                                <tr>
                                    <td style="color:#848484; font-size:14px; padding:12px;">Materi (Link)</td>
                                    <td>
                                        @if(is_null($event->module_url))
                                            -
                                        @else
                                            <a href="{{$event->module_url}}" target="_blank">Klik disini untuk detail materi</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color:#848484; font-size:14px; padding:12px;">Tanggal</td>
                                    <td>{{\Carbon\Carbon::parse($event->start_event)->isoFormat('D MMMM Y')}}-
                                        {{\Carbon\Carbon::parse($event->end_event)->isoFormat('D MMMM Y')}}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="color:#848484; font-size:14px; padding:12px;">Pemateri</td>
                                    <td>{{$event->speaker}} - {{$event->position_speaker}}</td>
                                </tr>
                                <tr>
                                    <td style="color:#848484; font-size:14px; padding:12px;">HTM</td>
                                    <td>{{rupiah($event->amount)}}</td>
                                </tr>
                                <tr>
                                    <td style="color:#848484; font-size:14px; padding:12px;">Lokasi</td>
                                    <td>{!! $event->location !!}
                                        @if(!is_null($event->url_loc))
                                            <br><a href="{{$event->url_loc}}" target="_blank">Klik disini untuk mengetahui map lokasi</a>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-2">
                        <div class="text-center py-2 text-white" style="margin-top:30px; background-color:#F48637;">
                            <span>Mekanisme Pembayaran</span>
                        </div>
                        <div style="padding-left:10px; padding-right:10px; border:1px solid #F48637;">
                            <table style="width:100%;">

                                <tr>
                                    <td>{!! $event->payment_term !!}</td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@else
    <div class="container my-9">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning text-center" role="alert">
                    {{$availableEvent["message"]}}
                </div>
            </div>
        </div>
    </div>

@endif
<!-- Footer -->

<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="background-color:transparent;border:none;">
            <div class="modal-body">
                <button
                    type="button"
                    class="close button-close-modal"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
                <audio controls id="myAudio" style="display:none;">
                    <source src="{{asset('public/frontend')}}/audio/senna.wav" type="audio/wav">
                    Your browser does not support the audio element.
                </audio>
                <a href="{{url('https://play.google.com/store/apps/details?id=com.senna_store')}}">
                    <img src="{{asset('public/frontend')}}/img/banner-popup.png" class="img-fluid" alt="modal" style="border-radius:13px;">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="background-color:transparent;border:none;">
            <div class="modal-body">
                <button type="button" class="close button-close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <a href="{{route('package')}}">
                    <img src="{{asset('public/frontend')}}/img/banner-promo.png" class="img-fluid" alt="modal" style="border-radius:13px;">
                </a>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<footer class="site-footer bg-dark text-contrast pb-4 z13">

    <div class="container">
        <div class="row gap-y text-center text-md-left" style="padding-top: 30px">
            <div class="col-md-4 mr-auto mt-1 ">
                <img src="{{asset('public/frontend')}}/img/logo.png" alt="" class="logo">
                <p class="poppin" style="font-size: 15px">
                    <strong>Senna</strong> memudahkan anda untuk mengontrol segala aktivitas transaksi usaha anda.
                </p>
                <h6>#MudahMenguntungkan</h6>
                <nav class="nav btn-store">
                    <a href="https://play.google.com/store/apps/details?id=com.senna_store" target="_blank"><img src="{{asset('public/frontend')}}/img/home/playstore.png" class="nav-item" width="90%" height="auto" alt=""></a>
                </nav>
            </div>
            <div class="col-md-2" style="margin-top: 6%">
                <h6 class="text-warning">Senna Kasir</h6>
                <nav class="nav flex-column">
                    <a class="nav-item py-2 text-contrast poppin" href="{{route('home')}}"><span>Beranda</span> </a>
                    <a class="nav-item py-2 text-contrast poppin" href="https://help.senna.co.id"><span>FAQ</span> </a>
                    <a class="nav-item py-2 text-contrast poppin" href="{{route('policy')}}"><span>{{config('frontend_'.get_lang().'.policy')}}</span></a>
                    <a class="nav-item py-2 text-contrast poppin" href="https://help.senna.co.id"><span>{{config('frontend_'.get_lang().'.help')}}</span></a>
                    <a class="nav-item py-2 text-contrast poppin" href="{{route('term')}}"><span>{{config('frontend_'.get_lang().'.term')}}</span></a>
                    <a class="nav-item py-2 text-contrast poppin" href="{{route('contact')}}"><span>{{config('frontend_'.get_lang().'.contact')}}</span></a>
                    <a class="nav-item py-2 text-contrast poppin" href="https://drive.google.com/drive/folders/1Ph-LzwRbSjiJNvAwCWGTDYYmSMVr7Hmw?usp=sharing" target="_blank"><span>Manual Book</span></a>
                </nav>
            </div>
            <div class="col-md-2" style="margin-top: 6%">
                <h6 class="text-warning">Hubungi Kami</h6>
                <nav class="nav flex-column">
                    <a class="nav-item py-2 text-contrast poppin"><span>(+62)812-9619-9565</span></a>
                    <a class="nav-item py-2 text-contrast poppin"><span>contact@senna.co.id</span></a>

                </nav>
            </div>
            <div class="col-md-2" style="margin-top: 6%">
                <h6 class="text-warning">Perusahaan</h6>
                <nav class="nav flex-column">
                    <a class="nav-item py-2 text-contrast poppin" href="{{route('about')}}"><span>{{config('frontend_'.get_lang().'.about')}}</span> </a>
                    <a class="nav-item py-2 text-contrast poppin" href="{{route('blog')}}"><span>Blog</span></a>
                    <a class="nav-item py-2 text-contrast poppin">
					<span>Sekarsuli-Berbah No.9, Potorono, Banguntapan,
						Bantul Regency, Special Region of Yogyakarta
					</span>
                    </a>
                </nav>
            </div>
        </div>
        <hr class=" op-5">
        <div class="row small py-2 d-flex align-items-center">
            <div class="col-md-4 d-flex justify-content-md-start justify-content-center">
			<span class=" text-center text-md-left ">
				© {{date('Y')}} <a href="" class="bold text-warning poppin">Senna</a>. All Rights Reserved
			</span>
            </div>
            <div class="col-md-8 d-flex align-items-center justify-content-md-end justify-content-center">
                <span>ikuti kami</span>
                <nav class="nav btn-sosmed">
                    <a href="https://www.facebook.com/idsennaindonesia" target="_blank" class="btn btn-circle text-light nav-item"><i class="fab fa-2x fa-facebook"></i></a>&nbsp;&nbsp;
                    <a href="#" class="btn btn-circle text-light nav-item"><i class="fab fa-2x fa-twitter"></i></a>&nbsp;&nbsp;
                    <a href="https://www.instagram.com/sennaindonesia/" target="_blank" class="btn btn-circle text-light nav-item"><i class="fab fa-2x fa-instagram"></i></a>
                    <a href="https://www.youtube.com/channel/UCooO_WCjQXQQZY5p-jvrjkw" target="_blank" class="btn btn-circle text-light nav-item"><i class="fab fa-2x fa-youtube"></i></a>
                </nav>
            </div>
        </div>
    </div>
</footer>



<script src="{{asset('public/backend')}}/js/backend.min.js"></script>
<div id="fb-root"></div>

<!-- Your Chat Plugin code -->
<div id="fb-customer-chat" class="fb-customerchat">
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" integrity="sha512-yI2XOLiLTQtFA9IYePU0Q1Wt0Mjm/UkmlKMy4TU4oNXEws0LybqbifbO8t9rEIU65qdmtomQEQ+b3XfLfCzNaw==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" integrity="sha512-qiKM6FJbI5x5+GL5CEbAUK0suRhjXVMRXnH/XQJaaQ6iQPf05XxbFBE4jS6VJzPGIRg7xREZTrGJIZVk1MLclA==" crossorigin="anonymous"></script>
<script type="text/javascript">
    $('#form-konten2').submit(function () {
        var data = getFormData('form-konten2');

        data.append('business_category', JSON.stringify($('#category_id').val()));
        ajaxTransfer("{{route('event.save', [$event->slug])}}", data, '#results');
    });
    $(document).ready(function() {
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $(".timezone").val(timezone);
    });
</script>

<script>
    $(document).ready(function(){
        $('#province').select2();
        $('#city').select2();
        $('#district').select2();
        $('#city-container').hide();
        $('#district-container').hide();
        $('#category_id').select2({
            placeholder:"--- Pilih Kategori Usaha ---",
            allowClear:true
        });

        $('#type_id').select2({
            placeholder:"--- Pilih Tipe Usaha ---"
        });

        $('#province').on('select2:select', function(e){
            if(e.target.value == '-1'){
                $('#city-container').hide();
                $('#district-container').hide();
                $('#city').val(-1);
                $('#district').val(-1);
                return;
            }
            const rawUrl = '{{route("event.get-city")}}';
            const url = rawUrl +"?id="+e.target.value;

            $('#city-container').show();
            $('#city option[value!=""]').remove();
            $('#district-container').hide();
            $('#district option[value!=""]').remove();
            $.ajax({
                url: url,
                success:function(response){
                    let html = '';
                    response.map(item => {
                        html += '<option value="'+item.id+'">'+item.name+'</option>';
                    });

                    $('#city').append(html);
                }
            });
        });

        $('#city').on('select2:select', function(e){
            if(e.target.value == '-1'){
                $('#district-container').hide();
                $('#district').val(-1);
                return;
            }
            const rawUrl = '{{route("event.get-district")}}';
            const url = rawUrl +"?id="+e.target.value;

            $('#district-container').show();
            $('#district option[value!=""]').remove();
            $.ajax({
                url: url,
                success:function(response){
                    let html = '';
                    response.map(item => {
                        html += '<option value="'+item.id+'">'+item.name+'</option>';
                    });

                    $('#district').append(html);
                }
            });
        })
    });

</script>

<script>
    var chatbox = document.getElementById('fb-customer-chat');
    chatbox.setAttribute("page_id", "204104350532189");
    chatbox.setAttribute("attribution", "biz_inbox");
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v11.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/id_ID/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html>

@extends('backend-v3.layout.main')
@section('title','Akses Salah')
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">Akses Salah</h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
               <div class="alert alert-warning">
                   <strong>Perhatian!</strong> Akunmu saat ini belum berlangganan,sehingga tidak bisa melakukan tindakan apapun di Dashboard Senna.
                   Silahkan melakukan pembayaran berlangganan melalui aplikasi mobile atau klik <a href="{{route('subscription.index')}}">tautan ini</a>.Terima kasih!
               </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
@endsection

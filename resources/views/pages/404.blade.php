
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>404 | Halaman Tidak Ditemukan  </title>
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/logo.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <style>
        * {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        body {
            padding: 0;
            margin: 0;
            font-family: 'Poppins';
            background-color: #ffffff;
        }

        #notfound {
            position: relative;
            height: 100vh;
        }

        #notfound .notfound {
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        .notfound {
            max-width: 520px;
            width: 100%;
            text-align: center;
            line-height: 1.4;
        }

        .notfound .notfound-404 {
            height: 190px;
        }

        .notfound .notfound-404 h1 {
            
            font-size: 146px;
            font-weight: 700;
            margin: 0px;
            color: #232323;
        }
        
        .notfound .notfound-404 h1>span {
            display: inline-block;
            width: 120px;
            height: 120px;
            background-image: url('https://colorlib.com/etc/404/colorlib-error-404-12/img/emoji.png');
            background-size: cover;
            -webkit-transform: scale(1.4);
            -ms-transform: scale(1.4);
            transform: scale(1.4);
            z-index: -1;
        }

        .notfound h2 {
            
            font-size: 22px;
            font-weight: 700;
            margin: 0;
            text-transform: uppercase;
            color: #000000;
        }

        .notfound p {
            
            color: #8f8484;
            font-weight: 300;
        }

        .notfound a {
            
            display: inline-block;
            padding: 12px 30px;
            font-weight: 700;
            background-color: #f99827;
            color: #ffffff;
            border-radius: 40px;
            text-decoration: none;
            -webkit-transition: 0.2s all;
            transition: 0.2s all;
        }

        .notfound a:hover {
            opacity: 0.8;
        }
        @media only screen and (max-width: 767px) {
            .notfound .notfound-404 {
                height: 115px;
            }
            .notfound .notfound-404 h1 {
                font-size: 86px;
            }
            .notfound .notfound-404 h1>span {
                width: 86px;
                height: 86px;
            }
        }
    </style>
    <!-- Google font -->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>

<body>

<div id="notfound">
    <div class="notfound">
        <img src="{{asset('public/frontend/img/pages/404.png')}}" width="40%" alt="">
        <h2>Halaman Tidak Ditemukan</h2>
        <p>Mohon maaf,halaman yang kamu akses tidak ditemukan,silahkan kembali ke halaman sebelumnya.</p>
        <a href="{{route('home')}}">{{trans('pages.return_back')}}</a>
    </div>
</div>

<!-- Global site tag (gtag.js) - Google Analytics -->
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>


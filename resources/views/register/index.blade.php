<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik">
    <title>{{$title}}</title>

    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/backend/css/auth.css')}}">
    <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>
    <meta name="google-signin-client_id" content="{{env('GOOGLE_CLIENT_ID')}}">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi.">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi." />
    <meta name="twitter:url" content="{{route('register')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('register')}}" />
    <meta property="og:description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi." />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
    @if(env('APP_ENV')=='production')
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-04M6PQZHK6"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-04M6PQZHK6');
        </script>
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TBV6V3B');</script>
<!-- End Google Tag Manager -->
    @endif


</head>

<body style="background: white">
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BHVKPP"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<div class="auth-wrapper">
    <div class="row auth-row__wrapper">
        <div class="auth-bg__dark"></div>
        <div class="col-md-12 mb-3 pt-4 pb-0 auth-logo__wrapper">
            <a title="link to home" href="{{route('home')}}">
                <img src="{{env('S3_URL')}}public/frontend/img/logo.png" alt="logo" width="10%">
            </a>
        </div>
        <div class="col-lg-7 col-md-6 auth-left__content mt-5">
            <div class="mt-5">
                <h1 class="mb-3">Aplikasi Multi-Layanan untuk Kemudahan Pengelolaan Usahamu</h1>
                <p>Kamu bisa merasakan kemudahan Senna  mulai dari pengelolaan usahamu, pengelolaan keuangan usaha, hingga layanan CRM dengan konsumenmu.</p>
                <ul>
                    <li class="d-flex align-items-start font-14 mb-1">
                        <i class="fa fa-check mr-2" style="color: #5DB480;"></i>Pencatatan dan pembukuan laporan keuangan secara mudah dengan sentuhan jari
                    </li>
                    <li class="d-flex align-items-start font-14 mb-1">
                        <i class="fa fa-check mr-2" style="color: #5DB480;"></i>Kelola laporan keuangan secara praktis dan akurat untuk kemudahan pengambilan keputusan
                    </li>
                    <li class="d-flex align-items-start font-14 mb-1">
                        <i class="fa fa-check mr-2" style="color: #5DB480;"></i>Kelola usaha dimanapun dan kapanpun tanpa ribet pakai banyak aplikasi
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-5 col-md-6 auth-right__content">
            <div class="card px-2 auth-card__wrapper regist">
                @if(is_null($step))
                <div class="card-body">
                    <h2 class="auth-form__title mb-3">Register <span class="auth-title__in">Senna</span></h2>
                    <span class="d-block mt-2 mb-3 font-bold-500">Sudah Punya Akun ?  <a href="{{route('login')}}" class="font-or fw-700">Login</a></span>
                    <div id="results"></div>
                    <form id="form-konten" action=""  class="user" onsubmit="return false">
                        <input type="hidden" name="type" value="3">
                        <input type="hidden" name="is_from_google" value="0">

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <input type="text" name="fullname" class="form-control form-input" placeholder="Nama Lengkap" required>
                            </div>
                            <div class="col-md-12 mb-3">
                                <input type="email" name="email" class="form-control form-input" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="E-mail" required>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group mb-3" style="position:relative;">
                                    <input id="password" type="password" name="password" class="form-control form-input" placeholder="Password" required>
                                    <i
                                        id="input-eye1"
                                        style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                                        class="fa fa-eye">
                                    </i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group mb-3" style="position:relative;">
                                    <input id="confirmPassword" type="password" name="confirmPassword" class="form-control form-input" placeholder="Konfirmasi Password" required>
                                    <i
                                        id="input-eye2"
                                        style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                                        class="fa fa-eye">
                                    </i>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <input type="text" name="merchant_name" class="form-control form-input" placeholder="Nama Usaha" required>
                            </div>
                            <div class="col-md-12 mb-3">
                                <select class="form-control font-control-sm" name="md_sc_category_id" id="category_id" required>
                                    <option></option>
                                    @foreach($categories as $category)
                                        <optgroup label="{{$category->name}}" style="font-weight: bold">
                                            @foreach($category->getChild as $c)
                                                <option value="{{$c->id}}">{{$c->name}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <button class="btn btn-auth__submit btn-block" type="submit">
                            <span class="bold poppin" style="font-size: 10pt">Buat Akun</span>
                        </button>
                        <small class="d-block mt-3 text-center">
                            Dengan mendaftar Anda telah menyetujui <a style="color:#FF9344;" href="{{route('term')}}">Syarat Ketentuan & Kebijakan Privasi</a>
                        </small>
                    </form>
                </div>
                @endif
            </div>
        </div>

        <div class="col-md-12 p-0 d-flex align-items-end justify-content-center">
            <span class="text-center pb-2">
                © {{date('Y')}} <a class="font-or fw-700" href="{{route('home')}}">Senna Indonesia</a> All Rights Reserved
            </span>
        </div>
    </div>
</div>


<input type='hidden' name='_token' value='{{ csrf_token() }}'>
<input type='hidden' name='_timezone' class="timezone" value=''>
<script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" integrity="sha512-yI2XOLiLTQtFA9IYePU0Q1Wt0Mjm/UkmlKMy4TU4oNXEws0LybqbifbO8t9rEIU65qdmtomQEQ+b3XfLfCzNaw==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" integrity="sha512-qiKM6FJbI5x5+GL5CEbAUK0suRhjXVMRXnH/XQJaaQ6iQPf05XxbFBE4jS6VJzPGIRg7xREZTrGJIZVk1MLclA==" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){
        $('#category_id').select2({
            placeholder:"--- Pilih Kategori Usaha ---",
            allowClear:true
        });

    })

    window.onbeforeunload = function(e){
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.disconnect();

    };

    function login(profile) {
        $.ajax({
            url: "{{route('login.validate')}}?key={{$key}}&is_gmail=1",
            headers: {
                'X-CSRF-TOKEN':"{{csrf_token()}}"
            },
            type: "POST",
            data: {
                "email":profile.getEmail()
            },
            success: function (response) {
                if(response.status==true){
                    window.location.replace(response.dashboard);
                }
            },
        });

    }

    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        var gmailInfo = {
            "idToken":"{{csrf_token()}}",
            "scopes":[
                "https://www.googleapis.com/auth/userinfo.profile",
                "https://www.googleapis.com/auth/userinfo.email",
            ],
            "serverAuthCode":null,
            "user":{
                "email":profile.getEmail(),
                "familyName":profile.getFamilyName(),
                "givenName":profile.getGivenName(),
                "id":profile.getId(),
                "name": profile.getName(),
                "photo":profile.getImageUrl()
            }
        }

        $.ajax({
            url: "{{route('register.validate')}}",
            headers: {
                'X-CSRF-TOKEN':"{{csrf_token()}}"
            },
            type: "POST",
            data: {
                "email":profile.getEmail(),
                "fullname":profile.getName(),
                "name":profile.getName(),
                "is_from_google":1,
                "type":3,
                "gmail_information":JSON.stringify(gmailInfo)
            },
            success: function (res) {
                if(res.code == 200 && !res.data.merchant_name){
                    let url = "{{url('/register')}}" +"?step=2&key="+res.data.token;
                    window.location.replace(url);
                }
                if(res.code == 200 && res.data.merchant_name && res.data.is_from_google == 1){
                    login(profile);
                }
                if(res.code == 200 && res.data.merchant_name && res.data.is_from_google == 0){
                    window.location.replace("{{route('login')}}");
                }
            },
        });

    }
    $('#input-eye1').on('click', function(){
        if($('#input-eye1').hasClass('fa fa-eye')){
            $('#password').attr('type', 'text');
            $('#input-eye1').removeClass('fa fa-eye');
            $('#input-eye1').addClass('fa fa-eye-slash');
        } else {
            $('#password').attr('type', 'password');
            $('#input-eye1').removeClass('fa fa-eye-slash');
            $('#input-eye1').addClass('fa fa-eye');
        }

    });

    $('#input-eye2').on('click', function(){
        if($('#input-eye2').hasClass('fa fa-eye')){
            $('#confirmPassword').attr('type', 'text');
            $('#input-eye2').removeClass('fa fa-eye');
            $('#input-eye2').addClass('fa fa-eye-slash');
        } else {
            $('#confirmPassword').attr('type', 'password');
            $('#input-eye2').removeClass('fa fa-eye-slash');
            $('#input-eye2').addClass('fa fa-eye');
        }

    });

    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{url('/register/validate-register')}}", data, '#results');
    });

    $(document).ready(function() {
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $(".timezone").val(timezone);
    });
</script>
</body>
</html>

<!-- begin::navigation -->
<div class="navigation">
    <div class="navigation-menu-tab bg-gradient-danger">
        <div>
            <div class="navigation-menu-tab-header" data-toggle="tooltip" title="{{get_user_name()}}" data-placement="right">
                <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                    <figure class="avatar avatar-sm">
                        <img src="{{get_user_profil()}}" class="rounded-circle" alt="avatar">
                    </figure>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-big">
                    <div class="p-3 text-center" data-backround-image="{{asset('public/backend-v3')}}/assets/media/image/image1.jpg">
                        <figure class="avatar mb-3">
                            <img src="{{get_user_profil()}}" class="rounded-circle" alt="image">
                        </figure>
                        <h6 class="d-flex align-items-center justify-content-center">
                            {{get_user_name()}}
                        </h6>
                    </div>
                    <div class="dropdown-menu-body">
                        <div class="list-group list-group-flush">
                            @if(get_role() == 3)
                                <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}" class="list-group-item">Ubah Informasi Tokomu</a>
                            @endif
                            <a href="{{route('logout')}}" class="list-group-item">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="flex-grow-1">
            <ul>
                <li>
                    @if(get_role() == 5 || get_role() == 6 || get_role() == 7)
                        @if(basename(request()->path()) == 'admin' || Request::segment(3) === 'notify')
                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Dashboards" data-nav-target="#dashboard">
                                <i class="fas fa-tachometer-alt"></i>
                            </a>
                        @elseif (basename(request()->path()) == 'dashboard' && Request::segment(1) == 'blog')
                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Dashboards" data-nav-target="#dashboard">
                                <i class="fas fa-tachometer-alt"></i>
                            </a>
                        @else
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Dashboards" data-nav-target="#dashboard">
                                <i class="fas fa-tachometer-alt"></i>
                            </a>
                        @endif
                    @endif
                    @if(get_role() === 9)
                        @if(basename(request()->path()) == 'marketing')
                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Dashboards" data-nav-target="#dashboard">
                                <i class="fas fa-tachometer-alt"></i>
                            </a>
                        @else
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Dashboards" data-nav-target="#dashboard">
                                <i class="fas fa-tachometer-alt"></i>
                            </a>
                        @endif
                    @endif
                </li>
                @if(get_role()==5 )
                    <li>
                        @if(Request::segment(2) === 'master-data')
                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Master Data" data-nav-target="#master-data">
                                <i class="fas fa-layer-group"></i>
                            </a>
                        @else
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Master Data" data-nav-target="#master-data">
                                <i class="fas fa-layer-group"></i>
                            </a>
                        @endif
                    </li>
                @endif

                @if(get_role()==5 || get_role()==6)
                    @if(Request::segment(3) === 'transaction' && Request::segment(2) === 'senna-pay')
                        <li>
                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Transaksi"
                               data-nav-target="#transaksi">
                                <i class="fas fa-wallet"></i>
                            </a>
                        </li>
                    @else
                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Transaksi"
                               data-nav-target="#transaksi">
                                <i class="fas fa-wallet"></i>
                            </a>
                        </li>
                    @endif

                @endif

                @if(get_role()==5 || get_role()==6)
                    @if(Request::segment(2) === 'merchant-list')
                        <li>
                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Daftar Merchant" data-nav-target="#daftar-merchant">
                                <i class="far fa-list-alt"></i>
                            </a>
                        </li>
                    @else
                        <li>
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Daftar Merchant" data-nav-target="#daftar-merchant">
                                <i class="far fa-list-alt"></i>
                            </a>
                        </li>
                    @endif

                @endif

                @if(get_role()==5 || get_role()==6 || get_role()==7)
                    <li>
                        <a href="#" data-toggle="tooltip" data-placement="right" title="Blog" data-nav-target="#blog">
                            <i class="fas fa-paste"></i>
                        </a>
                    </li>
                @endif

                @if(get_role()==9)
                    <li>
                        @if(Request::segment(2) == 'sales' && Request::segment(1) == 'marketing')
                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Sales" data-nav-target="#sales">
                                <i class="fas fa-shopping-cart"></i>
                            </a>
                        @else
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Sales" data-nav-target="#sales">
                                <i class="fas fa-shopping-cart"></i>
                            </a>
                        @endif
                    </li>
                    <li>
                        @if(Request::segment(1) == 'marketing' && Request::segment(2) == 'membership')
                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Membership" data-nav-target="#membership">
                                <i class="fas fa-users"></i>
                            </a>
                        @else
                            <a href="#" data-toggle="tooltip" data-placement="right" title="Membership" data-nav-target="#membership">
                                <i class="fas fa-users"></i>
                            </a>
                        @endif
                    </li>
                @endif

            </ul>
        </div>

        <div>
            <ul>
                <li>
                    <a href="{{route('logout')}}" data-toggle="tooltip" data-placement="right" title="Logout">
                        <i data-feather="log-out"></i>
                    </a>
                </li>
            </ul>
        </div>

    </div>

    <!-- begin::navigation menu -->
    <div class="navigation-menu-body">

        <!-- begin::navigation-logo -->
        <div>
            <div id="navigation-logo">
                <a href="index.html">
                    <img class="logo" src="{{asset('public/backend')}}/img/logo.png" alt="logo" height="40px">
                </a>
            </div>
        </div>
        <!-- end::navigation-logo -->

        <div class="navigation-menu-group">

                        @if(get_role() === 5 || get_role() === 6 || get_role() === 7)
                            @if(basename(request()->path()) == 'admin')
                                <div class="open" id="dashboard">
                                    @elseif (basename(request()->path()) == 'dashboard' && Request::segment(1) == 'blog')
                                        <div class="open" id="dashboard">
                                            @else
                                                <div id="dashboard">
                                                    @endif
                                                    <ul>
                                                        <li>
                                                            @if(basename(request()->path()) == 'admin')
                                                                <a class="active" href="{{route(''.dashboard_url().'')}}">Dashboard</a>
                                                            @else
                                                                <a href="{{route(''.dashboard_url().'')}}">Dashboard</a>
                                                            @endif
                                                        </li>
                                                        @if(get_role() === 5)
                                                            <li>
                                                                <a href="#" data-nav-target="#master-data">Master Data</a>
                                                            </li>
                                                        @endif
                                                        @if(get_role() === 5 || get_role() === 6)
                                                            <li>
                                                                <a href="#" data-nav-target="#transaksi">Transaksi</a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-nav-target="#daftar-merchant">Daftar Merchant</a>
                                                            </li>
                                                        @endif
                                                        @if(get_role() === 5 || get_role() === 6 || get_role() === 7)
                                                            <li>
                                                                <a href="#" data-nav-target="#blog">Blog</a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            @endif

                                            @if(get_role() === 9)
                                                @if(basename(request()->path()) == 'marketing')
                                                    <div class="open" id="dashboard">
                                                        @else
                                                            <div id="dashboard">
                                                                @endif
                                                                <ul>
                                                                    <li>
                                                                        <a class="" href="{{route('marketing.dashboard')}}">Dashboard</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" data-nav-target="#sales">Sales</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" data-nav-target="#membership">Membership</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        @endif

                                                        @if(get_role()==5 )
                                                            @if(Request::segment(2) === "master-data")
                                                                <div class="open" id="master-data">
                                                                    @else
                                                                        <div id="master-data">
                                                                            @endif
                                                                            <ul>
                                                                                <li class="navigation-divider">Master Data</li>
                                                                                <li>
                                                                                    <a href="#">Senna</a>
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.app-service.index')}}">Layanan Aplikasi</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.subscription.index')}}">Paket Langganan</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.type-business.index')}}">Tipe Bisnis</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.bank.index')}}">Bank</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.avBank.index')}}">Bank Tersedia</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.contact.index')}}">Kontak Kami</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.transaction-status.index')}}">Status Transaksi</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.transaction-type.index')}}">Tipe Transaksi</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.province.index')}}">Provinsi</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.city.index')}}">Kota</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.district.index')}}">District</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.approval.index')}}">Pengaturan Approval</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.merchant-approval.index')}}">Approval Merchant</a>
                                                                                        </li>

                                                                                    </ul>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">Senna</a>
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.unit.index')}}">Satuan</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.currency.index')}}">Mata Uang</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-kasir.category.index')}}">Kategori Umum</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-kasir.transaction-status.index')}}">Status Transaksi</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.shipping-category.index')}}">Kategori Pengiriman</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-kasir.order-status.index')}}">Status Order</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.inventory-method.index')}}">Metode Persediaan</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.product-type.index')}}">Tipe Produk</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.cash-type.index')}}">Tipe Cash</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">Senna Jasa</a>
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-jasa.category-service.index')}}">Kategori Layanan</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-jasa.type-service.index')}}">Tipe Layanan</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-jasa.order-status.index')}}">Status Order</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">Senna Pay</a>
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-pay.transaction-status.index')}}">Status Transaksi</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-pay.transaction-type.index')}}">Tipe Transaksi</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-pay.guide-category.index')}}">Kategori Panduan</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.senna-pay.guide.index')}}">Panduan</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">Pengguna</a>
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.user.index')}}">Menu </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.role.index')}}">Hak Akses</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.user-bank.index')}}">Bank Pengguna</a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a href="{{route('master-data.user-activity-notification.index')}}">Notifikasi Pengguna</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    @endif

                                                                    @if(get_role()==5 || get_role()==6)
                                                                        @if(Request::segment(3) === 'transaction' && Request::segment(2) === 'senna-pay')
                                                                            <div class="open" id="transaksi">
                                                                                @else
                                                                                    <div id="transaksi">
                                                                                        @endif
                                                                                        <ul>
                                                                                            <li class="navigation-divider">Transaksi</li>
                                                                                            <li>
                                                                                                <a href="#">Transfer Masuk</a>
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        <a class="collapse-item" href="{{route('senna-pay.transaction.manual-transaction-in')}}">
                                                                                                            <span>Manual</span>
                                                                                                            <span class="badge badge-danger">{{$countTfIn}}</span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a href="#">Online</a>
                                                                                                    </li>

                                                                                                </ul>
                                                                                            </li>
                                                                                            <li>
                                                                                                <a href="#">Penarikan</a>
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        <a href="{{route('senna-pay.transaction.manual-wd')}}">
                                                                                                            <span>Manual</span>
                                                                                                            <span class="badge badge-danger">{{$countTfOut}}</span>
                                                                                                        </a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a class="collapse-item" href="#">Online</a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                @endif

                                                                                @if(get_role()==5 || get_role()==6)
                                                                                    @if(Request::segment(2) === 'merchant-list')
                                                                                        <div class="open" id="daftar-merchant">
                                                                                            @else
                                                                                                <div id="daftar-merchant">
                                                                                                    @endif
                                                                                                    <ul>
                                                                                                        <li class="navigation-divider">Daftar Merchant</li>
                                                                                                        <li>
                                                                                                            <a href="#">Pendaftaran</a>
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    <a class="collapse-item" href="{{route('merchant.list')}}">
                                                                                                                        <span>Merchant Baru</span>
                                                                                                                        <span class="badge badge-danger">{{$countMerchant}}</span>
                                                                                                                    </a>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">List Merchant</a>
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    <a href="{{route('merchant-list.menu.index')}}">Menu</a>
                                                                                                                </li>
                                                                                                                <li>
                                                                                                                    <a href="{{route('merchant.list.list')}}">List</a>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            @endif

                                                                                            @if(get_role()==5 || get_role()==6 || get_role()==7)
                                                                                                @if(basename(request()->path()) !== 'dashboard' && Request::segment(1) == 'blog')
                                                                                                    <div class="open" id="blog">
                                                                                                        @else
                                                                                                            <div id="blog">
                                                                                                                @endif
                                                                                                                <ul>
                                                                                                                    <li class="navigation-divider">Blog</li>
                                                                                                                    <li>
                                                                                                                        <a href="#">Artikel</a>
                                                                                                                        <ul>
                                                                                                                            <li>
                                                                                                                                <a href="{{route('blog.dashboard.posts')}}">Posts</a>
                                                                                                                            </li>
                                                                                                                            <li>
                                                                                                                                <a href="{{route('blog.dashboard.category')}}">Kategori</a>
                                                                                                                            </li>
                                                                                                                            <li>
                                                                                                                                <a href="{{route('blog.dashboard.tags')}}">Tags</a>
                                                                                                                            </li>
                                                                                                                            <li>
                                                                                                                                <a href="{{route('blog.testimony.index')}}">Testimoni</a>
                                                                                                                            </li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                    <li>
                                                                                                                        <a href="#">Panduan</a>
                                                                                                                        <ul>
                                                                                                                            <li>
                                                                                                                                <a href="{{route('senna-pay.transaction.manual-transaction-in')}}">Merchant Baru</a>
                                                                                                                            </li>
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        @endif



                                                                                                                                                                                                    @if(get_role()==9)
                                                                                                                                                                                                        @if(Request::segment(2) == 'sales' && Request::segment(1) == 'marketing')
                                                                                                                                                                                                            <div class="open" id="sales">
                                                                                                                                                                                                                @else
                                                                                                                                                                                                                    <div id="sales">
                                                                                                                                                                                                                        @endif
                                                                                                                                                                                                                        <ul>
                                                                                                                                                                                                                            <li class="navigation-divider">Sales</li>
                                                                                                                                                                                                                            <li>
                                                                                                                                                                                                                                <a class="collapse-item" href="{{route('marketing.sales.index')}}">Data Sales</a>
                                                                                                                                                                                                                            </li>
                                                                                                                                                                                                                        </ul>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                    @if(Request::segment(1) == 'marketing' && Request::segment(2) == 'membership')
                                                                                                                                                                                                                        <div class="open" id="membership">
                                                                                                                                                                                                                            @else
                                                                                                                                                                                                                                <div id="membership">
                                                                                                                                                                                                                                    @endif
                                                                                                                                                                                                                                    <ul>
                                                                                                                                                                                                                                        <li class="navigation-divider">Membership</li>
                                                                                                                                                                                                                                        <li>
                                                                                                                                                                                                                                            <a href="{{route('marketing.membership.commission.index')}}">Komisi</a>
                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                        <li>
                                                                                                                                                                                                                                            <a href="{{route('marketing.membership.redeem-point.index')}}">Pengaturan Point</a>
                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                        <li>
                                                                                                                                                                                                                                            <a href="{{route('blog.dashboard.tags')}}">List</a>
                                                                                                                                                                                                                                        </li>
                                                                                                                                                                                                                                    </ul>
                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                            @endif
                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <!-- end::navigation menu -->

                                                                                                                                                                                                </div>
                                                                                                                                                                                                <!-- end::navigation -->

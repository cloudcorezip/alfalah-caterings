<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Senna adalah sebuah layanan multi-platfrom di Indonesia">
    <meta name="author" content="Senna">

    <title>@yield('title')</title>
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="stylesheet" href="{{asset('public/backend')}}/css/MonthPicker.min.css" />
    <link rel="stylesheet" href="{{asset('public/backend')}}/css/yearpicker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link href="{{asset('public/backend')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='_timezone' class="timezone" value=''>
    <script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdn.rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
    <script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>
    <script src="https://code.iconify.design/2/2.1.2/iconify.min.js"></script>

    @yield('css')
    <style>
        .toast-top-center {
            top: 12px;
            margin: 0 auto;
        }
    </style>

</head>

<body class="navigation-toggle-one" id="page-top">
@include('backend-v3.layout.header')
@include('backend-v3.layout.navbar')
<div id="main">
    @include('backend-v3.layout.menu')
    <main class="main-content poppin">

        @yield('content')
        <footer>
            <div class="container-fluid">
                <div class="copyright">
                    <span>Copyright &copy;</span>
                </div>
            </div>
        </footer>

    </main>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<script src="{{asset('public/backend/vendor/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" integrity="sha512-yI2XOLiLTQtFA9IYePU0Q1Wt0Mjm/UkmlKMy4TU4oNXEws0LybqbifbO8t9rEIU65qdmtomQEQ+b3XfLfCzNaw==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" integrity="sha512-qiKM6FJbI5x5+GL5CEbAUK0suRhjXVMRXnH/XQJaaQ6iQPf05XxbFBE4jS6VJzPGIRg7xREZTrGJIZVk1MLclA==" crossorigin="anonymous"></script>
<script src="{{asset('public/backend/js/vendor/intro/intro.min.js')}}"></script>
<script src="{{asset('public')}}{{mix('backend/js/toast-custom.min.js')}}"></script>
<script>
    $(document).ready(function() {
        @if(is_null(\Illuminate\Support\Facades\Session::get(env('APP_MENU_VERSION').'_'.merchant_id())))
        redirect(1500,'{{route('merchant.dashboard')}}')
        @endif
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $(".timezone").val(timezone);
    });
    var welcomeUrl="{{request()->url()}}";
    toastr.options = {
        timeOut: 3000,
        progressBar: true,
        showMethod: "slideDown",
        hideMethod: "slideUp",
        showDuration: 200,
        hideDuration: 200,
        iconClasses: {
            success: 'btn-bg__orange'
        }
    };
    if(welcomeUrl==="{{url('/')}}/merchant" || welcomeUrl==="{{url('/')}}"){
        toastr.success('Selamat Datang {{get_user_name()}}');
    }
    $('#toggle-menu').click(function(){
        $('#toggle-menu a i').hasClass('fa-arrow-left') ?
            ($('#toggle-menu a i').removeClass('fa-arrow-left'), $('#toggle-menu a i').addClass('fa-arrow-right'))
            :($('#toggle-menu a i').removeClass('fa-arrow-right'), $('#toggle-menu a i').addClass('fa-arrow-left'))
    });

    $(document).click('[data-nav-target]',function(){
        $('#toggle-menu a i').hasClass('fa-arrow-right') ?
            ($('#toggle-menu a i').removeClass('fa-arrow-right'), $('#toggle-menu a i').addClass('fa-arrow-left'))
            :"";

    });
    function changeSearchInputStyle(){
        setTimeout(() => {
            $("#table-data_filter label").contents().filter(function () {
                return this.nodeType === 3;
            }).remove()
            $("#table-data_filter label").prepend('<i class="fas fa-search table-search-icon"></i>');
            $("#table-data_filter label").addClass('table-search-input-wrapper');
            $('#table-data_filter label input').attr('placeholder','Cari Sesuatu Disini...');
            $('#table-data_filter label input').addClass('table-search-input');

        },0);
    }
    const changeEmptyTableMessage = () => {
        setTimeout(() => {
            $(".dataTables_empty").text("Tidak ada data yang tersedia pada tabel ini");
        }, 1000);
    }
    $('.amount_currency').mask("#.##0,00", {reverse: true});
    changeSearchInputStyle();
    changeEmptyTableMessage();
    feather.replace()
    $('.menu-pop').tooltip();

    closeSelect2InModal();
</script>
@yield('js')

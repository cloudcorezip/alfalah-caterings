<!-- begin::header -->
<div class="header">

    <div>
        <ul class="navbar-nav">

            <!-- begin::navigation-toggler -->
            <li class="nav-item navigation-toggler" id="toggle-menu">
                <a href="#" class="nav-link" title="Hide navigation">
                    <i data-feather="menu"></i>
                </a>
            </li>
            <li class="nav-item navigation-toggler mobile-toggler">
                <a href="#" class="nav-link" title="Show navigation">
                    <i data-feather="menu"></i>
                </a>
            </li>
            <!-- end::navigation-toggler -->
        </ul>
    </div>

    <div>
        <ul class="navbar-nav">
            <!-- begin::header search -->
            <li class="nav-item">
                <a href="#" class="nav-link" title="Search" data-toggle="dropdown">
                    <i data-feather="search"></i>
                </a>
                <div class="dropdown-menu p-2 dropdown-menu-right">
                    <form>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-prepend">
                                <button class="btn" type="button">
                                    <i data-feather="search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <!-- end::header search -->

            <!-- begin::header minimize/maximize -->
            <li class="nav-item dropdown">
                <a href="#" class="nav-link" title="Fullscreen" data-toggle="fullscreen">
                    <i class="maximize" data-feather="maximize"></i>
                    <i class="minimize" data-feather="minimize"></i>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link" title="User menu" data-toggle="dropdown">
                    <i data-feather="settings"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-big">
                    <div class="p-4 text-center d-flex justify-content-between"
                         data-backround-image="{{asset('public/backend')}}/media/image/image1.jpg">
                        <h6 class="mb-0">{{merchant_detail()->name}}</h6><br>

                    </div>
                    <div>
                        <ul class="list-group list-group-flush">
                            @if(get_role()==3)
                            <li class="list-group-item">
                                <i class="fa fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}"> Ubah Informasi Tokomu</a>
                            </li>
                            @if(env('APP_ENV')!='production')
                                <li class="list-group-item">
                                    <i class="fa fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    <a href="{{route('merchant.toko.digital-menu.index',['id'=>merchant_id()])}}"> Buat Katalog Digital</a>
                                </li>
                            @endif
                            @endif
                            <li class="list-group-item">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400" aria-hidden="true"></i>
                                <a href="{{route('logout')}}">Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
            <!-- end::user menu -->
        </ul>

        <!-- begin::mobile header toggler -->
        <ul class="navbar-nav d-flex align-items-center">
            <li class="nav-item header-toggler">
                <a href="#" class="nav-link">
                    <i data-feather="arrow-down"></i>
                </a>
            </li>
        </ul>
        <!-- end::mobile header toggler -->
    </div>

</div>
<!-- end::header -->

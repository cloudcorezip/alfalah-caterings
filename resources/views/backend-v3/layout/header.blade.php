<div class="main-header">
    <div class="row">
        <div class="col-md-12">
            <!-- begin::header -->
            <div class="header" style="background-color:transparent; border-bottom:none;">
                <div>
                    <ul class="navbar-nav main-header-menu-btn">

                        <!-- begin::navigation-toggler -->
                        <li class="nav-item navigation-toggler" id="toggle-menu">
                            <a href="#" class="nav-link" title="Hide navigation">
                                <i class="fas fa-list"></i>
                            </a>
                        </li>
                        <li class="nav-item navigation-toggler mobile-toggler">
                            <a id="tes" class="nav-link" data-toggle="tooltip" data-placement="right" title="Tampilkan Menu">
                                <i style="font-size:20px;" class="fas fa-bars text-white"></i>
                            </a>
                        </li>
                        <!-- end::navigation-toggler -->
                    </ul>
                </div>

                <div>
                    <ul class="navbar-nav">
                        @if(get_role() == 3)

                        <li class="nav-item mr-2" id="jual" style="position:relative;">
                            <a
                                href="{{route('merchant.toko.sale-order.data')}}"
                                onclick="activeMenu('penjualan','penjualan-data-penjualan',1)"
                                class="nav-link main-header__btn__link"
                            >
                                <img src="{{asset('public/backend/img/header-jual.png')}}" alt="icon" class="mr-2">
                                <span>Jual</span>
                            </a>
                        </li>
                        <li class="nav-item mr-2" id="beli" style="position:relative;">
                            <a
                                href="{{route('merchant.toko.purchase-order.data')}}"
                                onclick="activeMenu('pembelian','pembelian-data-pembelian',1)"
                                class="nav-link main-header__btn__link"
                            >
                                <img src="{{asset('public/backend/img/header-beli.png')}}" alt="icon" class="mr-2">
                                <span>Beli</span>
                            </a>
                        </li>
                        <li class="nav-item mr-2" id="biaya" style="position:relative;">
                            <a
                                href="{{route('merchant.toko.acc.journal.index')}}"
                                onclick="activeMenu('keuangan', 'keuangan-pencatatan-jurnal---biaya',1)"
                                class="nav-link main-header__btn__link"
                            >
                                <img src="{{asset('public/backend/img/header-biaya.png')}}" alt="icon" class="mr-2">
                                <span>
                                    Jurnal Umum
                                </span>
                            </a>
                        </li>
                        @endif
                        @if(get_role()==2 || get_role()==3)
                        <li class="nav-item dropdown">
                            @if($countNotifyMerchant > 0)
                            <a href="#" class="nav-link nav-link-notify" title="Notifications" data-toggle="dropdown">
                            @else
                            <a href="#" class="nav-link" title="Notifications" data-toggle="dropdown">
                            @endif
                                <i class="far fa-bell menu-header-icon" style="font-size:18px;"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-big dropdown-notif">
                                <div class="p-4 text-center d-flex justify-content-between">
                                    <h6 class="mb-0">Pesan Masuk</h6>
                                    <small class="font-size-11 opacity-7">{{$countNotifyMerchant}} belum dibaca</small>
                                </div>
                                <div>
                                    <ul class="list-group list-group-flush">
                                            @foreach(get_notif() as $key =>$item)
                                            <li>
                                                <a href="{{route('merchant.notify.index')}}" class="list-group-item d-flex hide-show-toggler">
                                                    <div>
                                                        <figure class="avatar avatar-sm m-r-15">
                                                                <span class="avatar-title bg-warning-bright text-warning rounded-circle">
                                                                    <i class="ti-package"></i>
                                                                </span>
                                                        </figure>
                                                    </div>
                                                    <div class="flex-grow-1">
                                                        <span class="text-muted small">{{$item->title}}-{{$item->created_at}}</span>
                                                        <p class="mb-0 line-height-20 d-flex justify-content-between">
                                                            {{$item->body}}
                                                            <i title="Mark as unread" data-toggle="tooltip"
                                                            class="hide-show-toggler-item fa fa-check font-size-11"></i>
                                                        </p>

                                                    </div>
                                                </a>
                                            </li>
                                            @endforeach
                                    </ul>
                                </div>
                                <div class="p-2 text-right">
                                    <ul class="list-inline small">
                                        <li class="list-inline-item">
                                            <a href="{{route('merchant.notify.index')}}">Lihat Semua Pesan</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        @endif
                        <!-- end::header notification dropdown -->

                        <!-- begin::user menu -->
                        <li class="nav-item dropdown" id="user-menu" style="position:relative;margin-left: -20px">
                            <a href="#" class="nav-link" title="User menu" data-toggle="dropdown">
                                <img src="{{asset('public')}}/backend/icons/header/home.png" alt="icon" width="24px">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-big">
                                <div class="p-4" style="border-bottom: 1px solid #ebebeb">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <b class="font-16">{{merchant_detail()->name}}</b>
                                            <small>{{merchant_detail()->email_merchant}}</small>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <button class="btn" style="background: #D0E7DC;color:#3A996C;border: none!important;
    align-items: center;
    justify-content: center">{{(merchant_detail()->is_branch==0)?'Pusat':'Cabang'}}</button>
                                        </div>
                                    </div>

                                </div>
                                <div>
                                    <ul class="list-group list-group-flush">
                                        @if(get_role()==3)
                                        <li class="list-group-item">
                                            <span class="iconify mr-2" data-icon="fa-regular:user" data-width="20" data-height="20"></span>
                                            <a onclick="activeMenu('pengaturan', 'pengaturan-informasi-usaha')" href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}"> Ubah Informasi Usaha</a>
                                        </li>
                                            @if(env('APP_ENV')!='production')
                                            <li class="list-group-item">
                                                <span class="iconify mr-2" data-icon="akar-icons:star" data-width="20" data-height="20"></span>
                                                <a onclick="activeMenu('pengaturan', 'pengaturan-informasi-usaha')" href="{{route('merchant.toko.digital-menu.index',['id'=>merchant_id()])}}">Buat Katalog Digital</a>
                                            </li>
                                            @endif

                                        @endif
                                        <li class="list-group-item">
                                            <span class="iconify mr-2" data-icon="heroicons-outline:logout" data-width="20" data-height="20"></span>
                                            <a id="logout-btn" href="{{route('logout')}}">Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- end::user menu -->
                    </ul>

                    <!-- begin::mobile header toggler -->
                    <ul class="navbar-nav d-flex align-items-center">
                        <li class="nav-item header-toggler">
                            <a href="#" class="nav-link">
                                <i class="text-white" data-feather="arrow-down"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- end::mobile header toggler -->
                </div>

                </div>
                <!-- end::header -->
        </div>

    </div>
</div>

<script>
    $(document).ready(function(){
        $('.mobile-toggler').click(function(e){
            e.preventDefault();
            e.stopPropagation();
            var overlay = document.querySelectorAll('.overlay');
            $('.main-navigation').removeClass('main-navigation-open');
            $('.main-navigation-toggle').removeClass('rotate180');
            $('.main-navigation').toggleClass('dflex');
            (overlay.length == 0)? $('body').append('<div class="overlay show"></div>'):$('.overlay').remove();

        });

        $(document).click(function(e){
            if(e.target.classList.contains('overlay')){
                $('.main-navigation').removeClass('dflex');
                $('.main-navigation').removeClass('main-navigation-open');
            }
        });

        $('#logout-btn').click(function(e){
            localStorage.removeItem('isActiveMenu');
            localStorage.removeItem('isActiveLink');
            localStorage.removeItem('isActiveChildLink');
        });

    });
</script>

<style>
    .child-submenu {
        padding-left: 60px!important;
    }
    .child-submenu a {
        color: #A4A4A4!important;
        width: 100%!important;
    }
    .child-submenu-ul {
        width: 100%!important;
    }
</style>
<div class="main-navigation">
    <ul style="margin-top:100px;">
        <li class="d-flex align-items-start menu-item-li" id="dashboard" style="position:relative;">
            @if(get_role() == 3 || get_role() >= 11)
                <a onclick="activeMenu('dashboard', 'ringkasan-usaha', 1)" href="{{route('merchant.dashboard')}}" data-toggle="tooltip" class="dashboard item-link p-10" data-placement="right" title="Dashboard" data-nav-target="#dashboard">
                    <span class="iconify" data-icon="bx:bxs-dashboard"></span>
                </a>
            @endif
        </li>
        @php
        $menusV3=\Illuminate\Support\Facades\Session::get(env('APP_MENU_VERSION').'_'.merchant_id());
        @endphp
        @if(get_role() == 3 || get_role()>=11)
            @if($menusV3!=[])
                @foreach($menusV3 as $key => $item)
                    @php
                       $plugin=checkPlugin(merchant_id(),$item->is_type);
                    @endphp
                    @if($plugin!=false)
                        @php
                            $menuName = strtolower(str_replace(['&', '/', ' '],'-', $item->name));
                        @endphp
                        @if(count($item->getChild) >0)
                            <li class="d-flex align-items-start menu-item-li" id="{{$menuName}}Db" style="position-relative">
                                <a class="{{$menuName}} item-link p-10"  data-toggle="tooltip" data-placement="right" title="{{replaceMenuName($plugin,$item->name)}}" data-nav-target="#{{$menuName}}">
                                    {!! $item->icons !!}
                                </a>
                            </li>
                        @else
                            @php
                                $linkName = strtolower(str_replace(['&','/',' '], '-', $item->name));
                            @endphp
                            <li class="d-flex align-items-start menu-item-li" id="{{$menuName}}Db" style="position-relative">
                                @php
                                    $url2=(!\Illuminate\Support\Facades\Route::has($item->url))?'#':route($item->url);
                                @endphp
                                <a onclick="activeMenu('{{$menuName}}','{{$menuName}}-{{$linkName}}', 1)" href="{{($item->url=='#')?'#':$url2}}{{(!is_null($item->query_params)?$item->query_params:'')}}" class="{{$menuName}} item-link p-10"  data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#{{$menuName}}">
                                    {!! $item->icons !!}
                                </a>
                            </li>
                        @endif
                        @if($item->name=='Laporan')
                            <hr style="padding: 1px 0!important;">
                        @endif
                    @endif
                @endforeach
            @endif
        @endif
        <li class="d-flex align-items-start menu-item-li" id="pengaturan" style="position:relative;">
            @if(get_role() == 3 || get_role()==11)
                <a onclick="activeMenu('pengaturan', 'pengaturan', 1)" href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}" data-toggle="tooltip" class="pengaturan item-link p-10" data-placement="right" title="Pengaturan" data-nav-target="#pengaturan">
                    <span class="iconify" data-icon="uiw:setting"></span>
                </a>
            @endif
        </li>
    </ul>
</div>
<div class="submenu-collapse">
    @if(get_role() == 3 || get_role()>=11)
        @if($menusV3!=[])
            @foreach($menusV3 as $key => $item)
                @php
                    $plugin=checkPlugin(merchant_id(),$item->is_type);
                @endphp
                    @if($plugin!=false)
                    @php
                        $menuName = strtolower(str_replace(['&', '/', ' '],'-', $item->name));
                    @endphp
                    @if(count($item->getChild) >0)
                        <div class="submenu-row" id="{{$menuName}}">
                            <h5 class="title-combo-menu mb-3 mt-2">{{replaceMenuName($plugin,$item->name)}}</h5>
                            <div class="submenu-wrapper">
                                <ul style="width:100%;">
                                    @foreach($item->getChild->where('is_active',1)->sortBy('id') as $k => $n)
                                        @php
                                            $plugin2=checkPlugin(merchant_id(),$n->is_type);
                                        @endphp
                                          @if($plugin2!=false)
                                            @php
                                                $linkName = strtolower(str_replace(['&','/',' '], '-', $n->name));
                                            @endphp
                                            @if($n->getChild->count()>0)

                                                <li class="d-flex align-items-start submenu-li" style="position:relative;" id="{{$menuName}}-{{$linkName}}">
                                                    <div class="submenu-child-collapse">
                                                        <a
                                                            onclick="activeMenu('{{$menuName}}','{{$menuName}}-{{$linkName}}', 1)"
                                                            class="submenu-link dropdown-toggle"
                                                            href="#sub-{{$linkName}}"
                                                            data-toggle="collapse"
                                                            aria-expanded="true"
                                                        >
                                                            {!! $n->icons !!}
                                                            <span class="span-menu-name">{{replaceMenuName($plugin2,$n->name)}}</span>
                                                        </a>
                                                        <ul class="collapse child-submenu-ul mt-3" id="sub-{{$linkName}}">
                                                            @foreach($n->getChild->where('is_active',1)->sortBy('id') as $cc)
                                                                @php
                                                                    $plugin3=checkPlugin(merchant_id(),$cc->is_type);
                                                                @endphp
                                                                @if($plugin3!=false)
                                                                    <li class="child-submenu mb-2">
                                                                        @php
                                                                            $url3=(!\Illuminate\Support\Facades\Route::has($cc->url))?'#':route($cc->url);
                                                                        @endphp
                                                                        <a href="{{($cc->url=='#')?'#':$url3}}{{(!is_null($cc->query_params)?$cc->query_params:'')}}">{{$cc->name}}</a>
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </li>

                                            @else
                                                <li class="d-flex align-items-start submenu-li" style="position:relative;" id="{{$menuName}}-{{$linkName}}">
                                                    @php
                                                        $url4=(!\Illuminate\Support\Facades\Route::has($n->url))?'#':route($n->url);
                                                    @endphp
                                                    <a
                                                        onclick="activeMenu('{{$menuName}}','{{$menuName}}-{{$linkName}}', 1)"
                                                        href="{{($n->url=='#')?'#':$url4}}{{(!is_null($n->query_params)?$n->query_params:'')}}"
                                                        class="submenu-link"
                                                    >
                                                        {!! $n->icons !!}
                                                        <span>{{replaceMenuName($plugin2,$n->name)}}</span>
                                                    </a>
                                                </li>

                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                @endif
            @endforeach
        @endif
    @endif
</div>
<script>
    $(document).ready(function(){
        if (localStorage.getItem("isActiveMenu") === null) {
            localStorage.setItem('isActiveMenu',"dashboard");
        }
        $('#'+localStorage.getItem('isActiveMenu')+' .menu-list').addClass('open-menu');
        $("."+localStorage.getItem('isActiveMenu')).addClass("active-menu");
        $("#"+localStorage.getItem('isActiveLink')).addClass("active");
        $("#"+localStorage.getItem('isActiveMenu')).addClass("open");

        $('[data-toggle="tooltip"]').tooltip();

    });

    $('.item-link').click(function(e){
        e.stopPropagation();
        let id = $(this).attr('data-nav-target');
        const menus = document.querySelectorAll(".submenu-row");
        const target = document.querySelector(id);
        menus.forEach(item => {
            const otherId = item.getAttribute('id');
            if(otherId !== id.replace('#','')){
                document.querySelector('#'+otherId).classList.remove('open-sub-menu');
            }
        })

        if(target.classList.contains("open-sub-menu")){
            target.classList.remove('open-sub-menu');
        } else {
            target.classList.add('open-sub-menu');
        }

        if(!target.querySelector('.submenu-child-collapse')){
            removeActiveComboMenu();
        }

    });

    const removeActiveComboMenu = () => {
        const bodyElement = document.querySelector('body');
        const comboMenuList = bodyElement.querySelectorAll('.submenu-row');
        bodyElement.addEventListener('click', (event) => {
            if(!event.target.classList.contains('span-menu-name')){
                if(!event.target.classList.contains('submenu-wrapper')
                    && !event.target.classList.contains('title-combo-menu')
                    && !event.target.classList.contains('submenu-row')
                ){
                    comboMenuList.forEach(item => {
                        if(item.classList.contains('open-sub-menu')){
                            item.classList.remove('open-sub-menu');
                        }
                    });
                }
            }

        })
    }

    const activeMenu = (menu, link, isFromMenu = 0) => {
        localStorage.setItem("isActiveMenu", menu);
        if(isFromMenu == 1){
            localStorage.setItem("isActiveLink", link);
        }
    }

    $(".mobile-toggler").on('click', function(){
        $(".open-sub-menu").removeClass('open-sub-menu');
    });
</script>

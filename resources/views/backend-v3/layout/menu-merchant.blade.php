<!-- begin::navigation -->
<div class="navigation">
    <div class="navigation-menu-tab bg-gradient-danger">
        <div>
            <div class="navigation-menu-tab-header" data-toggle="tooltip" title="{{get_user_name()}}" data-placement="right">
                <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                    <figure class="avatar avatar-sm">
                        <img src="{{get_user_profil()}}" class="rounded-circle" alt="avatar">
                    </figure>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-big">
                    <div class="p-3 text-center" data-backround-image="{{asset('public/backend-v3')}}/assets/media/image/image1.jpg">
                        <figure class="avatar mb-3">
                            <img src="{{get_user_profil()}}" class="rounded-circle" alt="image">
                        </figure>
                        <h6 class="d-flex align-items-center justify-content-center">
                            {{get_user_name()}}
                        </h6>
                    </div>
                    <div class="dropdown-menu-body">
                        <div class="list-group list-group-flush">
                            @if(get_role() == 3)

                                <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}" class="list-group-item">Ubah Informasi Tokomu</a>
                            @endif

                            <a href="{{route('logout')}}" class="list-group-item">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-grow-1">
            <ul>
                <li>
                    @if(get_role() == 3)
                        @if(basename(request()->path()) == 'merchant' || Request::segment(3) === 'notify' || basename(request()->path()) == 'purchase-inventory' )

                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Dashboard" data-nav-target="#dashboard">
                                <i class="fa fa-dashboard"></i>
                            </a>
                        @else

                            <a href="#" data-toggle="tooltip" data-placement="right" title="Dashboard" data-nav-target="#dashboard">
                                <i class="fa fa-dashboard"></i>
                            </a>
                        @endif
                    @endif

                </li>
                @if(get_role() == 3)
                    @if($menuOwner!=[])
                        @foreach($menuOwner as $key => $item)

                            <li>
                                @if(Request::segment(4)=== 'master-data' && $item->name === 'Master Data')

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif($item->name==='Penjualan' && preg_match('/sale/i', Request::segment(5)))

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif($item->name==='Penjualan' &&
                                    Request::segment(4) === 'report' &&
                                    !preg_match('/purchase/i', Request::segment(5)) &&
                                    !preg_match('/stock/i', Request::segment(5)) &&
                                    !preg_match('/staff/i', Request::segment(5))
                                )

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif($item->name === 'Pembelian' && preg_match('/purchase/i', Request::segment(5)))

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif(Request::segment(4)==='stok' && $item->name === 'Persediaan')

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif(preg_match('/stock/i', Request::segment(5)) && $item->name === 'Persediaan')

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif(preg_match('/acc/i', Request::segment(4)) && $item->name === 'Keuangan')

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif(Request::segment(4)==='permit-staff' && $item->name === 'Absensi' && $item->name !== 'Master Data')

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif(
                                    preg_match('/staff/i', Request::segment(5)) &&
                                    $item->name === 'Absensi' &&
                                    !preg_match('/master-data/i', Request::segment(4))
                                )

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @elseif(Request::segment(2)==='menu' && $item->name === 'Pengaturan')

                                    <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @else

                                    <a href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                        <i class="{{$item->icons}}"></i>
                                    </a>
                                @endif

                            </li>
                        @endforeach
                    @endif

                    <li>
                        @if(Request::segment(2)==="menu")

                            <a class="active" href="#" data-toggle="tooltip" data-placement="right" title="Pengaturan" data-nav-target="#pengaturan">
                                <i class="fa fa-gears"></i>
                            </a>
                        @else

                            <a href="#" data-toggle="tooltip" data-placement="right" title="Pengaturan" data-nav-target="#pengaturan">
                                <i class="fa fa-gears"></i>
                        @endif

                    </li>
                @endif
                @if(get_role()>=11)
                    @if($menu!=[])
                        @foreach($menu as $key => $item)

                            <li>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role11-{{$key}}">
                                    <i data-feather="copy"></i>
                                </a>
                            </li>
                        @endforeach
                    @endif
                @endif

            </ul>
        </div>
        <div>
            <ul>
                <li>
                    <a href="{{route('logout')}}" data-toggle="tooltip" data-placement="right" title="Logout">
                        <i data-feather="log-out"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- begin::navigation menu -->
    <div class="navigation-menu-body">
        <!-- begin::navigation-logo -->
        <div>
            <div id="navigation-logo">
                <a href="index.html">
                    <img class="logo" src="{{asset('public/backend')}}/img/logo.png" alt="logo" height="40px">
                </a>
            </div>
        </div>
        <!-- end::navigation-logo -->
        <div class="navigation-menu-group">
            @if(get_role() === 3)
                @if(basename(request()->path()) == 'merchant' || basename(request()->path()) == 'purchase-inventory' || Request::segment(3) === 'notify' || Request::segment(2) === 'detail')

                    <div class="open" id="dashboard">
                        @else

                            <div class="" id="dashboard">
                                @endif

                                <ul>
                                    <li>
                                        @if(basename(request()->path()) == 'merchant')

                                            <a class="active" href="{{route(''.dashboard_url().'')}}">Ringkasan Keuangan Usaha</a>
                                        @else

                                            <a href="{{route(''.dashboard_url().'')}}">Ringkasan Keuangan Usaha</a>
                                        @endif

                                    </li>
                                    <li>
                                        @if(basename(request()->path()) == 'purchase-inventory')

                                            <a class="active" href="{{route('merchant.purchase-inventory')}}">Ringkasan Pembelian & Persedian</a>
                                        @else

                                            <a href="{{route('merchant.purchase-inventory')}}">Ringkasan Pembelian & Persediaan </a>
                                        @endif

                                    </li>
                                </ul>
                            </div>
                        @endif

                        @if(get_role() == 3)
                            @if($menuOwner!=[])
                                @foreach($menuOwner as $key => $item)
                                    @if(Request::segment(4) === "master-data" && $item->name === "Master Data")

                                        <div class="open" id="menu-role3-{{$key}}">
                                            @elseif($item->name==='Penjualan' && preg_match('/sale/i', Request::segment(5)))

                                                <div class="open" id="menu-role3-{{$key}}">
                                                    @elseif($item->name==='Penjualan' &&
                                                            Request::segment(4) === 'report' &&
                                                            !preg_match('/purchase/i', Request::segment(5)) &&
                                                            !preg_match('/stock/i', Request::segment(5)) &&
                                                            !preg_match('/staff/i', Request::segment(5))
                                                    )

                                                        <div class="open" id="menu-role3-{{$key}}">
                                                            @elseif($item->name==='Pembelian' && preg_match('/purchase/i', Request::segment(5)))

                                                                <div class="open" id="menu-role3-{{$key}}">
                                                                    @elseif(Request::segment(4)==='stok' && $item->name === 'Persediaan')

                                                                        <div class="open" id="menu-role3-{{$key}}">
                                                                            @elseif(preg_match('/stock/i', Request::segment(5)) && $item->name === 'Persediaan')

                                                                                <div class="open" id="menu-role3-{{$key}}">
                                                                                    @elseif(preg_match('/acc/i', Request::segment(4)) && $item->name === 'Keuangan')

                                                                                        <div class="open" id="menu-role3-{{$key}}">
                                                                                            @elseif(
                                                                                                preg_match('/staff/i', Request::segment(4))
                                                                                                && $item->name === 'Absensi' &&
                                                                                                $item->name !== 'Master Data'
                                                                                            )

                                                                                                <div class="open" id="menu-role3-{{$key}}">
                                                                                                    @elseif(
                                                                                                        preg_match('/staff/i', Request::segment(5)) &&
                                                                                                        $item->name === 'Absensi' &&
                                                                                                        !preg_match('/master-data/i', Request::segment(4))
                                                                                                    )

                                                                                                        <div class="open" id="menu-role3-{{$key}}">
                                                                                                            @else

                                                                                                                <div id="menu-role3-{{$key}}">
                                                                                                                    @endif

                                                                                                                    <ul>
                                                                                                                        <li class="navigation-divider">{{$item->name}}</li>
                                                                                                                        @foreach($item->getChild->sortBy('id') as $k => $n)
                                                                                                                            @if(searchMenuWithSubscription($n->id)==true)
                                                                                                                                @if($n->getChild->count()>1)

                                                                                                                                    <li>
                                                                                                                                        <a href="#">{{$n->name}}</a>
                                                                                                                                        <ul>
                                                                                                                                            @foreach($n->getChild->sortBy('id') as $child)
                                                                                                                                                @if(searchMenuWithSubscription($child->id)==true)

                                                                                                                                                <li>
                                                                                                                                                    <a class="collapse-item menu-pop" href="{{($child->url=='#')?'#':route($child->url)}}" title="" data-placement="right" data-toggle="tooltip" data-original-title="" >
                                                                                                                                                        {{$child->name}}
                                                                                                                                                    </a>
                                                                                                                                                </li>
                                                                                                                                                @endif
                                                                                                                                            @endforeach

                                                                                                                                        </ul>
                                                                                                                                    </li>
                                                                                                                                @else
                                                                                                                                    @if ($n->name==="Permintaan Izin")
                                                                                                                                        @php
                                                                                                                                            $countPermit = \DB::table('md_merchant_staff_permit_submissions')->where('md_merchant_id', merchant_id())
                                                                                                                                                                                                            ->where('md_merchant_staff_permit_submissions.is_approved',0)
                                                                                                                                                                                                            ->where('md_merchant_staff_permit_submissions.is_canceled',0)->get();
                                                                                                                                        @endphp

                                                                                                                                        <li>
                                                                                                                                            <a class="collapse-item" href="{{($n->url=='#')?'#':route($n->url)}}">
                                                                                                                                                <span>{{$n->name}}</span>
                                                                                                                                                <span class="badge badge-pill badge-light">{{count($countPermit)}}</span>
                                                                                                                                            </a>
                                                                                                                                        </li>
                                                                                                                                    @else

                                                                                                                                        <li>
                                                                                                                                            <a href="{{($n->url=='#')?'#':route($n->url)}}">{{$n->name}}</a>
                                                                                                                                        </li>

                                                                                                                                    @endif
                                                                                                                                @endif
                                                                                                                            @endif
                                                                                                                        @endforeach

                                                                                                                    </ul>
                                                                                                                </div>
                                                                                                                @endforeach
                                                                                                            @endif
                                                                                                            @if(Request::segment(2)==="menu")

                                                                                                                <div class="open" id="pengaturan">
                                                                                                                    @else

                                                                                                                        <div id="pengaturan">
                                                                                                                            @endif

                                                                                                                            <ul>
                                                                                                                                <li class="navigation-divider">Pengaturan</li>
                                                                                                                                <li>
                                                                                                                                    <a href="{{route('merchant.reset-data.add')}}">Reset Data</a>
                                                                                                                                </li>
                                                                                                                            </ul>
                                                                                                                        </div>
                                                                                                                    @endif



                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <!-- end::navigation menu -->
                                                                                                </div>
                                                                                                <!-- end::navigation -->

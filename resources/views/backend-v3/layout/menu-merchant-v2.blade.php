<!-- begin::navigation -->
<div class="navigation">
    <div class="navigation-menu-tab bg-gradient-danger">
        <div>
            <div class="navigation-menu-tab-header" data-toggle="tooltip" title="{{get_user_name()}}" data-placement="right">
                <a href="#" class="nav-link" data-toggle="dropdown" aria-expanded="false">
                    <figure class="avatar avatar-sm">
                        <img src="{{get_user_profil()}}" class="rounded-circle" alt="avatar">
                    </figure>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-big">
                    <div class="p-3 text-center" data-backround-image="{{asset('public/backend')}}/media/image/image1.jpg">
                        <figure class="avatar mb-3">
                            <img src="{{get_user_profil()}}" class="rounded-circle" alt="image">
                        </figure>
                        <h6 class="d-flex align-items-center justify-content-center">
                            {{get_user_name()}}
                        </h6>
                    </div>
                    <div class="dropdown-menu-body">
                        <div class="list-group list-group-flush">
                            @if(get_role() == 3)

                                <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}" class="list-group-item">Ubah Informasi Tokomu</a>
                            @endif

                            <a href="{{route('logout')}}" class="list-group-item">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-grow-1">
            <ul>
                <li>
                    @if(get_role() == 3)
                        <a onclick="setActiveMenu('dashboard',1)" data-toggle="tooltip" class="dashboard" data-placement="right" title="Dashboard" data-nav-target="#dashboard">
                            <i data-feather="layers"></i>

                        </a>
                    @endif

                </li>
                @if(get_role() == 3)
                    @if($menuOwner!=[])
                        @foreach($menuOwner as $key => $item)

                            <li>
                                <a onclick="setActiveMenu('menu-role3-{{$key}}',1)" class="menu-role3-{{$key}}"  data-toggle="tooltip" data-placement="right" title="{{$item->name}}" data-nav-target="#menu-role3-{{$key}}">
                                    <i class="{{$item->icons}}"></i>
                                </a>
                            </li>
                        @endforeach
                    @endif
                @endif
                <li>
                    @if(get_role() == 3)
                        <a onclick="setActiveMenu('pengaturan',1)" data-toggle="tooltip" class="pengaturan" data-placement="right" title="Pengaturan" data-nav-target="#pengaturan">
                            <i data-feather="settings"></i>

                        </a>
                    @endif
                </li>
            </ul>
        </div>
        <div>
            <ul>
                <li>
                    <a href="{{route('logout')}}" data-toggle="tooltip" data-placement="right" title="Logout">
                        <i data-feather="log-out"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- begin::navigation menu -->
    <div class="navigation-menu-body">
        <!-- begin::navigation-logo -->
        <div>
            <div id="navigation-logo">
                <a href="{{url('/')}}">
                    <img class="logo" src="{{asset('public/backend')}}/media/image/logo.png" alt="logo" height="40px">
                </a>
            </div>
        </div>
        <!-- end::navigation-logo -->
        <div class="navigation-menu-group">
            @if(get_role() === 3)
                <div id="dashboard">
                    <ul>
                        <li>
                            <a href="{{route(''.dashboard_url().'')}}">Ringkasan Keuangan Usaha</a>
                        </li>
                        <li>
                            <a href="{{route('merchant.purchase-inventory')}}" >Ringkasan Pembelian & Persediaan </a>
                        </li>
                    </ul>
                </div>
                @foreach($menuOwner as $key =>$item)
                    <div  id="menu-role3-{{$key}}">
                        <ul>
                            <li class="navigation-divider">{{$item->name}}</li>
                            @foreach($item->getChild->sortBy('id') as $k => $n)
                                @if(searchMenuWithSubscription($n->id)==true)
                                    @if($n->getChild->count()>1)

                                        <li>
                                            <a href="#">{{$n->name}}</a>
                                            <ul>
                                                @foreach($n->getChild->sortBy('id') as $child)
                                                    @if(searchMenuWithSubscription($child->id)==true)
                                                        @php
                                                            $url=($child->url!='#')?route($child->url):route(dashboard_url());
                                                            $arraOfUrl = explode('/', $url);
                                                            $lastUrl=$arraOfUrl[sizeof($arraOfUrl)-1];
                                                            $beforeLastUrl=$arraOfUrl[sizeof($arraOfUrl)-2];

                                                        @endphp
                                                        <li>
                                                            <a class="collapse-item menu-pop {{$beforeLastUrl}}-{{$lastUrl}}-menu-role3-{{$key}}"  href="{{($child->url=='#')?'#':route($child->url)}}" title="" data-placement="right" data-toggle="tooltip" data-original-title="" >
                                                                {{$child->name}}
                                                            </a>
                                                        </li>

                                                    @endif
                                                @endforeach

                                            </ul>
                                        </li>
                                    @else
                                        @php
                                            $url=($n->url!='#')?route($n->url):route(dashboard_url());
                                            $arraOfUrl = explode('/', $url);
                                            $lastUrl=$arraOfUrl[sizeof($arraOfUrl)-1];
                                            $beforeLastUrl=$arraOfUrl[sizeof($arraOfUrl)-2];

                                        @endphp
                                        @if ($n->name==="Permintaan Izin")
                                            @php
                                                $countPermit = \DB::table('md_merchant_staff_permit_submissions')->where('md_merchant_id', merchant_id())
                                                                                                                ->where('md_merchant_staff_permit_submissions.is_approved',0)
                                                                               ->where('md_merchant_staff_permit_submissions.is_canceled',0)->get();
                                            @endphp

                                            <li>
                                                <a class="collapse-item {{$beforeLastUrl}}-{{$lastUrl}}-menu-role3-{{$key}}" href="{{($n->url=='#')?'#':route($n->url)}}" >
                                                    <span>{{$n->name}}</span>
                                                    <span class="badge badge-pill badge-light">{{count($countPermit)}}</span>
                                                </a>
                                            </li>
                                        @else

                                            <li>
                                                <a href="{{($n->url=='#')?'#':route($n->url)}}" class="{{$beforeLastUrl}}-{{$lastUrl}}-menu-role3-{{$key}}">{{$n->name}}</a>
                                            </li>

                                        @endif
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endforeach
                <div id="pengaturan">
                    <ul>
                        <li class="navigation-divider">Pengaturan</li>
                        @php
                            $url=route('merchant.reset-data.add');
                            $arraOfUrl = explode('/', $url);
                            $lastUrl=$arraOfUrl[sizeof($arraOfUrl)-1];
                            $beforeLastUrl=$arraOfUrl[sizeof($arraOfUrl)-2];

                        @endphp
                        <li>
                            <a href="{{route('merchant.reset-data.add')}}" class={{$beforeLastUrl}}-{{$lastUrl}}-pengaturan">Reset Data</a>
                        </li>
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>



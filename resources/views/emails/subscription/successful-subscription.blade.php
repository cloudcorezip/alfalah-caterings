<html>
<body style="background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;">
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
    Berikut merupakan detail invoice pembayaran berlangganan di Aplikasi Senna</div>

<table style="max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px #f05c1e;">
    <thead>
    <tr>
        <th style="text-align:left;">
            <img style="max-width: 150px;" src="{{asset('public/logo')}}/invoice-logo.png" alt="Senna">
        </th>
        <th style="text-align:right;font-weight:400;">{{\Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="height:35px;"></td>
    </tr>
    <tr>
        <td colspan="2" style="border: solid 1px #ddd; padding:10px 20px;">
            <p style="font-size:14px;margin:0 0 6px 0;">
                <span style="font-weight:bold;display:inline-block;min-width:150px">Status Pembayaran</span>
                <b style="color:rgba(0,0,0,0.98);font-weight:normal;margin:0">Success</b>
            </p>
            <p style="font-size:14px;margin:0 0 6px 0;">
                <span style="font-weight:bold;display:inline-block;min-width:146px">Kode Pembayaran</span> {{$data->transaction->code}}
            </p>
            <p style="font-size:14px;margin:0 0 0 0;">
                <span style="font-weight:bold;display:inline-block;min-width:146px">Total</span> {{rupiah($data->transaction->amount)}}
            </p>
        </td>
    </tr>
    <tr>
        <td style="height:35px;"></td>
    </tr>
    <tr>
        <td style="width:50%;padding:20px;vertical-align:top">
            <p style="margin:0 0 10px 0;padding:0;font-size:14px;">
                <span style="display:block;font-weight:bold;font-size:13px">Nama Merchant</span> {{$data->getMerchant->name}}
            </p>
            <p style="margin:0 0 10px 0;padding:0;font-size:14px;">
                <span style="display:block;font-weight:bold;font-size:13px;">Email</span> {{$email}}
            </p>
            <p style="margin:0 0 10px 0;padding:0;font-size:14px;">
                <span style="display:block;font-weight:bold;font-size:13px;">No Telp</span>  {{$data->getMerchant->phone_mumber}}
            </p>
        </td>
        <td style="width:50%;padding:20px;vertical-align:top">
            <p style="margin:0 0 10px 0;padding:0;font-size:14px;">
                <span style="display:block;font-weight:bold;font-size:13px;">Alamat</span> {{$data->getMerchant->address}}
            </p>
            <p style="margin:0 0 10px 0;padding:0;font-size:14px;">
                <span style="display:block;font-weight:bold;font-size:13px;">Aktif Sampai</span> {{$data->getMerchant->end_subscription}}
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="font-size:20px;padding:30px 15px 0 15px;">Item</td>
    </tr>
    <tr>
        <td colspan="2" style="padding:15px;">
            <p style="font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;">
                <span style="display:block;font-size:13px;font-weight:normal;">{{$data->getSubscription->name}}</span> {{rupiah($data->getSubscription->price)}}
                <b style="font-size:12px;font-weight:300;">{{$data->getSubscription->type}}</b>
            </p>

        </td>
    </tr>
    </tbody>
    <tfooter>
        <tr>
            <td colspan="2" style="font-size:14px;padding:50px 15px 0 15px;">
                <strong style="display:block;margin:0 0 10px 0;">Dari</strong> Senna
                <br> Jalan Sekarsuli Potorono No 9, Banguntapan, Bantul, Yogyakarta
                <br>
                <br>
                <b>No Telp:</b>
                <br>
                <b>Email:</b> contact@senna.co.id

            </td>
        </tr>
    </tfooter>
</table>
</body>
</html>

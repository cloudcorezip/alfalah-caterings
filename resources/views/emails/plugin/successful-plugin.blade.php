<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Pembelian Plugin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            a:link {color: #027ab7; text-decoration:none; outline:none}
            a:visited {color: #027ab7; text-decoration:none; outline:none}
            .a2 a:link {color:#027ab7; text-decoration:none; outline:none}
            .a2 a:visited {color:#027ab7; text-decoration:none; outline:none}
            p{text-align: justify;}
            p img{-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.1);}
            .fitur li{margin: 10px 0 !important;} 
            li{ margin: 5px 0; }
            @media (prefers-color-scheme: light) {
                table tr td{background-color: #fff;}
                table tr td.encapsuled{background-color: #f1f1f1;}
            }
            @media (prefers-color-scheme: dark) {
                table tr td{background-color: #fff;}
                table tr td.encapsuled{background-color: #f1f1f1;}
            }           
        </style>
    </head>
    <body style="background-color: #f1f1f1; margin:0; padding:0; line-height:1.2em; font-family:arial, verdana, helvetica">
    <table width="650" cellpadding="0" cellspacing="0" align="center" border="0" style="background-color:#ffffff">
        <tr>
            <td valign="top" style="background-color: #f1f1f1;" class="encapsuled">
                <!-- header -->
                <table width="650" cellpadding="0" cellspacing="0" align="center" border="0" style="margin-top:18px; margin-bottom:21px; font-size:11px; color: #909090">
                    <tr>
                        <td valign="top" align="right" style="background-color: #f1f1f1;">
                            
                        </td>
                    </tr>
                </table>
                <table width="650" cellpadding="0" cellspacing="0" border="0" align="center" style="background-color:#ffffff">
                    <tr>
                        <td height="30" style="background-color:#fff">
                        </td>
                    </tr>
                    <tr>
                        <td width="275" style="background-color:#fff">
                        </td>
                        <td>
                            <a href="https://www.senna.co.id" target="_blank">
                                <img src="{{env('S3_URL')}}public/frontend/img/logo.png" border="0" width="100" alt="senna" style="display: block;height: auto;" />
                            </a>
                        </td>
                        <td align="right" width="275" style="padding-right:20px; font-size:18px; color: #202020; line-height:1.3em;background-color:#fff">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="25" style="background-color:#fff">
                        </td>
                    </tr>
                </table>
                <!-- end header -->
                <!-- body -->
                <table width="650" cellpadding="0" cellspacing="0" border="0" align="center" style="background-color:#ffffff;">
                    <tr>

                        <td width="25" style="background-color:#fff">&nbsp;
                        </td>
                        <td width="600" style="font-size:18px; color: #333; line-height:1.5em;padding-top: 10px;background-color:#fff">
                            <p style="font-size:14px;font-weight:bold;line-height: 0;">Status Pembayaran</p>
                            <span style="font-size:14px;color:rgba(0,0,0,0.98);">Success</span>

                            <p style="font-size:14px;font-weight:bold;line-height: 0;">Kode Pembayaran</p>
                            <span style="font-size:14px;color:rgba(0,0,0,0.98);">{{$data->transaction->code}}</span>

                            <p style="font-size:14px;font-weight:bold;line-height: 0;">Total</p>
                            <span style="font-size:14px;color:rgba(0,0,0,0.98);">{{$data->transaction->amount}}</span>
                            
                            <p style="font-size:22px;text-align: center;">Halo <b>{{ucwords($data->getMerchant->name)}}</b></p>
                            <p style="font-size:14px;">Selamat, pembayaran plugin "{{$data->getPlugin->name}}" sudah berhasil, nikmati fitur yang telah kami berikan</p>

                            <p style="text-align:center"><b>Senna Indonesia,</b></p>
                            <p style="text-align: center;">Senna Team</p>
                            
                            &nbsp;
                            
                        </td>
                        <td width="25" style="background-color:#fff">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td height="10" style="background-color:#fff">&nbsp;</td>
                    </tr>
                </table>
                <!-- end body -->
                
                <!-- footer -->
                <table width="650" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-spacing:0; border-width:0;">
                    <tr>
                        <td height="10" style="background-color:#f1f1f1;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="25" style="background-color:#f1f1f1;">&nbsp;</td>
                        <td width="600" style="background-color:#f1f1f1;">
                            <p style="text-align: center; color: #737373; font-size:18px;">Pertanyaan? Hubungi Kami di <a href="mailto:contact@senna.co.id" style="text-decoration:none; color: #FF9743;">contact@senna.co.id</a>
                            </p>
                        </td>
                        <td width="25" style="background-color:#f1f1f1;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="25" style="background-color: #f1f1f1;">&nbsp;</td>
                        <td style="padding-top: 15px;padding-bottom: 15px;background-color: #f1f1f1">
                            <table width="600" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-spacing:0; border-width:0;">
                                <tr>
                                    <td width="210" style="background-color:#f1f1f1;">&nbsp;</td>
                                    <td width="180" valign="top"  style="padding:10px 0 0 0; font-size:12px; line-height:15pt; color:#999999;background-color:#f1f1f1;">
                                        <table width="200" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-spacing:0; border-width:0;">
                                            <tr>
                                                <td width="60" style="padding:0 0 0 0;background-color:#f1f1f1;">
                                                    <a href="https://www.facebook.com/idsennaindonesia/" title="Ikuti kami di Facebook">
                                                        <img style="display:block;margin: 0 auto;" border="0" vspace="0" hspace="0" alt="Facebook" height="24" src="{{env('S3_URL')}}public/frontend/img/fb-icon.png" width="24">
                                                    </a>
                                                </td>
                                                <td width="60" style="padding:0 0 0 0;background-color:#f1f1f1;">
                                                    <a href="https://www.instagram.com/sennaindonesia/" title="Ikuti kami di Instagram">
                                                        <img style="display:block;margin: 0 auto;" border="0" vspace="0" hspace="0" alt="Instagram" height="24" src="{{env('S3_URL')}}public/frontend/img/ig-icon.png" width="24">
                                                    </a>
                                                </td>
                                                <td width="60" style="padding:0 0 0 0;background-color:#f1f1f1;">
                                                    <a href="https://www.youtube.com/channel/UCooO_WCjQXQQZY5p-jvrjkw">
                                                        <img style="display:block;margin: 0 auto;" border="0" vspace="0" hspace="0" alt="Instagram" height="24" src="{{env('S3_URL')}}public/frontend/img/yt-icon.png" width="24">
                                                    </a>
                                                </td>    
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="210" style="background-color:#f1f1f1;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td width="25" style="background-color: #f1f1f1;">&nbsp;</td>
                    </tr>
                </table>
                <table width="650" cellpadding="0" cellspacing="0" border="0" align="center" style="font-size:14px; color: #7c7c7c; margin-bottom:20px;margin-top: 10px;">
                    <tr>
                        <td class="a2" align="center" style="background-color:#f1f1f1">
                            Copyright &copy; 2021 Senna Indonesia
                        </td>
                    </tr>       
                </table>
                <!-- end footer -->
            </td>
        </tr>
    </table>
    </body>
</html>
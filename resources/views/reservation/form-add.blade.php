@extends('reservation.main')
@section('content')
<div class="container-fluid px-0">
    <div class="card card-reservation mb-4 px-2 py-4">
        <div class="card-body">
            <div class="row mb-4">
                <div class="col-md-12">
                    <h3 class="text-center font-weight-bold h3-res">{{$title}}</h3>
                    <span class="d-block text-center color-a4a4a4">Isi form reservasi mu secara lengkap dan pastikan simpan kartu untuk bukti pendaftaran</span>
                </div>
            </div>

            <div class="row mb-5">
                <div class="col-md-12">
                    <div class="res-connector d-flex align-items-center justify-content-center">
                        <div class="res-state line text-center active">
                            <span class="iconify" data-icon="bxs:user"></span>
                            <h6 class="font-weight-bold mt-3 color-a4a4a4 h6-res">Data Pasien</h6>
                        </div>
                        <div class="res-state line text-center @if($page=='service'||$page=='print') active @endif">
                            <span class="iconify" data-icon="fluent:book-add-24-filled"></span>
                            <h6 class="font-weight-bold mt-3 color-a4a4a4 h6-res">Layanan</h6>
                        </div>
                        <div class="res-state text-center @if($page=='print') active @endif">
                            <span class="iconify" data-icon="bxs:id-card"></span>
                            <h6 class="font-weight-bold mt-3 color-a4a4a4 h6-res">Simpan Kartu</h6>
                        </div>
                    </div>
                </div>
            </div>
            
            @if($page == 'cust')
            <div class="row d-flex align-items-center mb-4">
                <div class="col-md-6 mb-2">
                    <h5 class="font-weight-bold h5-res">Apakah anda sudah pernah mendaftar ?</h5>
                </div>
                <div class="col-md-6 d-flex justify-content-end mb-2">
                    <a href="{{route('reservation.add', ['merchant_id' => $merchantId, 'page'=>'cust'])}}" class="btn btn-warning text-white mr-2">Sudah</a>
                    <a href="{{route('reservation.create', ['merchant_id' => $merchantId, 'page'=>'cust'])}}" class="btn btn-outline-warning ml-2">Belum</a>
                </div>
            </div>

            <div class="row d-flex align-items-center mb-4">
                <div class="col-lg-6 col-md-12 mb-2">
                    <h5 class="font-weight-bold h5-res">Masukkan kode member</h5>
                </div>
                <div class="col-lg-6 col-md-12 d-flex justify-content-end mb-2">
                    <form class="form-inline">
                        <div class="form-group d-flex">
                            <input type="text" class="form-control" placeholder="Kode" id="code" value="{{$customer->code}}">
                            <input type="hidden" id="user_id" value="{{$userId}}">
                            <button type="button" class="btn btn-warning ml-2" id="search-btn">Cari</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 mb-4">
                    <div class="cust-result" id="cust-result">
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <table class="table-note">
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">No. Member</td>
                                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4 member-no">{{is_null($customer->code)? "-":$customer->code}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-14 font-weight-bold color-323232">Nama</td>
                                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4 member-name">{{is_null($customer->name)? "-":$customer->name}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="w-80-100" style="margin:0 auto;">
                                    <div class="patient-card">
                                        <table class="table-note">
                                            <tr>
                                                <td class="font-weight-bold font-14 color-323232" style="padding-bottom:0!important;">Tanggal Lahir</td>
                                                <td class="text-center font-14 font-weight-bold color-323232" width="40px" style="padding-bottom:0!important;">:</td>
                                                @if(!is_null($customer->date_of_birth))
                                                <td class="font-24 color-a4a4a4 member-birth-date" style="padding-bottom:0!important;">{{\Carbon\Carbon::parse($customer->date_of_birth)->isoFormat('DD MMMM Y')}}</td>
                                                @else
                                                <td class="font-24 color-a4a4a4 member-birth-date" style="padding-bottom:0!important;">-</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold font-14 color-323232" style="padding-bottom:0!important;">Jenis Kelamin</td>
                                                <td class="text-center font-14 font-weight-bold color-323232" width="40px" style="padding-bottom:0!important;">:</td>
                                                @if(!is_null($customer->gender))
                                                    @if(strtolower($customer->gender) == 'male')
                                                    <td class="font-24 color-a4a4a4 member-gender" style="padding-bottom:0!important;">Laki - Laki</td>
                                                    @elseif(strtolower($customer->gender) == 'female')
                                                    <td class="font-24 color-a4a4a4 member-gender" style="padding-bottom:0!important;">Perempuan</td>
                                                    @else
                                                    <td class="font-24 color-a4a4a4 member-gender" style="padding-bottom:0!important;">-</td>
                                                    @endif
                                                @else
                                                <td class="font-24 color-a4a4a4 member-gender" style="padding-bottom:0!important;">-</td>
                                                @endif
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 mb-3">
                    <h6 class="font-weight-bold">Catatan :</h6>
                    <span class="color-a4a4a4">Jika data sudah benar anda dapat melanjutkan pengisian layanan yang akan dibuat</span>
                </div>
                <div class="col-lg-8 col-md-6 col-sm-12 d-flex align-items-center justify-content-end mb-3">
                    @if($data->status != 1)
                    <form onsubmit="return false;" id="form-tosave">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <input type="hidden" name="customer_id" id="customer_id" value="{{$customer->id}}">
                        <input type="hidden" name="merchant_id" value="{{$merchantId}}">
                        <button type="submit" class="btn btn-success">
                            Simpan & Lanjut <span class="iconify ml-2" data-icon="ooui:next-ltr"></span>
                        </button>
                    </form>
                    @endif
                </div>
            </div>

            @endif

            @if($page == 'service')
            <div class="row mb-4">
                <div class="col-md-12">
                    <h5 class="font-weight-bold h5-res">Tambah Layanan</h5>
                    <span class="color-a4a4a4">Tambah data Layanan mu disini dengan benar</span>
                </div>
            </div>
            <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Tanggal Reservasi <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-sm trans_time" name="reservation_date" value="{{$data->reservation_date}}" placeholder="Tanggal Reservasi">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Layanan <span class="text-danger">*</span></label>
                            <select name="sc_product_id" id="sc_product_id" required>
                                <option></option>
                                @foreach($service as $key => $item)
                                <option value="{{$item->id}}" @if($data->sc_product_id == $item->id) selected @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Dokter <span class="text-danger">*</span></label>
                            <select name="md_merchant_staff_id" id="md_merchant_staff_id">
                                <option></option>
                                @foreach($doctor as $key => $item)
                                <option value="{{$item->id}}" @if($data->md_merchant_staff_id == $item->id) selected @endif>{{$item->getUser->fullname}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold d-block">Catatan :</label>
                            <span style="color: #A4A4A4;">Disini anda dapat memilih tanggal reservasi, layanan, & Dokter yang sesuai dengan jadwal reservasi anda. Setelah mengisi pastikan anda mengecek data tersebut sebelum menyimpan kartu reservasi.</span>
                        </div>
                    </div>      
                </div>
                <input type='hidden' name='id' value='{{$data->id }}'>
                <input type='hidden' name='_token' value='{{csrf_token()}}'>
                <input type='hidden' name='is_from_create' value='0'>
                <input type='hidden' name='sc_customer_id' value='{{$customer->id}}'>
                <input type='hidden' name='merchant_id' value='{{$merchantId}}'>

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{route('reservation.add', ['merchant_id'=>$merchantId,'id'=>$data->id,'page'=>'cust', 'member'=>$customerId])}}" class="btn btn-light mr-2" data-dismiss="modal">Kembali</a>
                        @if($data->status != 1)
                        <button type="submit" class="btn btn-success">
                            Simpan
                        </button>
                        @endif
                    </div>
                </div>
            </form>
            @endif

            @if($page == 'print')
            <div class="row mb-4">
                <div class="col-md-12">
                    <h5 class="font-weight-bold h5-res">Simpan Kartu Reservasi</h5>
                    {{---<span class="color-a4a4a4">Simpan kartu untuk bukti reservasi</span>---}}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 mb-3">
                    <div class="print-card mb-2">
                        <h3 class="text-center font-weight-bold">{{ucwords($data->getMerchant->name)}}</h3>
                        <span class="d-block text-center mb-2" style="color:#A4A4A4;">{{$data->getMerchant->address}}</span>
                        
                        <div style="width:100%;">
                            <img style="width:100%; height:250px;object-fit:contain;" src="{{env('S3_URL')}}{{$data->qr_code_file}}" alt="qr_code">
                        </div>
                        
                        <div style="width:70%; margin:0 auto;">
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width:50%;">No Reservasi</dt>
                                <dd class="text-right" style="width:50%;">{{$data->reservation_number}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width:50%;">No Member</dt>
                                <dd class="text-right" style="width:50%;">{{$data->getCustomer->code}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width:50%;">Nama</dt>
                                <dd class="text-right" style="width:50%;">{{$data->getCustomer->name}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width:50%;">Layanan</dt>
                                <dd class="text-right" style="width:50%;">{{$data->getProduct->name}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width:50%;">Dokter</dt>
                                <dd class="text-right" style="width:50%;">{{$data->getStaff->getUser->fullname}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width:50%;">Tanggal</dt>
                                <dd class="text-right" style="width:50%;">{{\Carbon\Carbon::parse($data->reservation_date)->isoFormat(' DD/MM/Y  HH:mm:ss')}}</dd>
                            </dl>
                            <h3 class="font-weight-bold text-center mt-3">TerimaKasih</h3>
                            <span class="d-block text-center" style="color:#A4A4A4;"><i>Powered By Senna</i></span>
                        </div>
                    </div>
                    <i class="d-block text-center" style="color:#747474;">Nomor Reservasi Pasien</i>
                    <div class="d-flex align-items-center justify-content-center mt-4">
                        <a href="{{route('reservation.add', ['merchant_id'=>$merchantId, 'page'=> 'cust'])}}" class="btn btn-light mr-2" data-dismiss="modal">Kembali</a>
                        <a class="btn btn-success ml-2" href="{{route('reservation.print-pdf', ['id'=>$data->id])}}" target="_blank">
                            <span class="iconify mr-2" data-icon="fa-solid:download"></span> Download
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 mb-3">
                    <h4 class="font-weight-bold">Catatan</h4>
                    <span class="color-a4a4a4">Disini anda berhasil menginputkan data reservasi. Kemudian anda bisa dapat menyimpan kartu reservasi disini. Sebelum menyimpan perlu anda catat data yang anda inputkan lengkap dan benar.</span>
                </div>
            </div>
            @endif
            
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $("#sc_product_id").select2({
        placeholder: "--- Pilih Layanan ---"
    });
    $("#md_merchant_staff_id").select2({
        placeholder: "--- Pilih Dokter ---"
    });
    dateTimePicker('.trans_time');

    $("#search-btn").on('click', function(){
        $.ajax({
            type: 'POST',
            data: {
                code: $("#code").val(),
                user_id: "{{$userId}}",
                _token:"{{ csrf_token() }}"
            },
            url: "{{route('reservation.get-customer')}}",
            success:function(response){
                let data = response;
                if(data.id){
                    $(".member-no").text(data.code);
                    $(".member-name").text(data.name);
                    $(".member-birth-date").text(data.date_of_birth);
                    $(".member-gender").text(data.gender);
                    $("#customer_id").val(data.id);
                } else {
                    $(".member-no").text("-");
                    $(".member-name").text("-");
                    $(".member-birth-date").text("-");
                    $(".member-gender").text("-");
                    $("#customer_id").val("");
                    toastForSaveData("Member tidak ditemukan !", "warning", false, "");
                }
            }
        });
    });

    $('#form-tosave').submit(function(){
        var data = getFormData('form-tosave');
        ajaxTransfer("{{route('reservation.redirect-member')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    })

    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{route('reservation.save')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    })
});
</script>
@endsection


<style>
    .main-content {
        margin-top: 130px!important;
        padding-left: 40px!important;
        padding-right: 40px!important;
        width: 100%!important;
    }

    .reservation-header {
        position: fixed;
        top: 0;
        width: 100%;
        height: 340px;
        box-sizing:border-box;
        background: linear-gradient(180deg, #FF961A 0%, #FF8A00 100%);
        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
        padding: 10px 40px;
    }

    .reservation-logo {
        width: 140px;
        object-fit:contain;
    }

    .card-reservation {
        background: #FFFFFF;
        box-shadow: 0px 4px 30px rgba(0, 0, 0, 0.1);
        border-radius: 16px;
        width: 85%;
        margin: 0 auto;
    }

    .color-a4a4a4 {
        color: #A4A4A4;
    }

    .res-connector {
        width: 50%;
        margin: 0 auto;
    }

    .res-state {
        position: relative;
        width: 30%;
    }

    .res-state.line:after {
        content: '';
        position: absolute;
        width: 100%;
        height: 2px;
        background: #C4C4C4;
        top: 19px;
        z-index: 5;
    }

    .res-state .iconify {
        position:relative;
        color: #A4A4A4;
        background: #F0F0F0;
        font-size: 38px;
        border-radius: 50%;
        padding: 8px;
        z-index: 10;
    }

    .res-state.active .iconify {
        color: #FF9743;
        background: #FFEDD9;
    }

    .res-state.line.active:after {
        background: #FF9743;
    }

    .print-card {
        border:1px solid #DDDDDD;
        border-radius:10px;
        width: 60%;
        margin: 0 auto;
        padding: 14px;
    }

    .btn-warning {
        background: #FF9743!important;
    }

    .btn-warning:hover {
        color: #fff!important;
    }

    .cust-result {
        border: 1px dashed #C3C3C3;
        background: #FDFDFD;
        border-radius: 10px;
        padding: 30px 80px;
    }

    @media screen and (max-width: 1300px){
        .print-card {
            width: 70%;
            padding: 10px;
        }
    }

    @media screen and (max-width: 1200px){
        .print-card {
            width: 90%;
            padding: 10px;
        }
        .card-reservation {
            width: 90%;
        }
    }
    
    @media screen and (max-width: 992px){
        .print-card {
            width: 90%;
            padding: 10px;
        }
        .res-connector {
            width: 70%;
        } 
    }

    @media screen and (max-width: 768px){
        .main-content {
            padding-left: 5px!important;
            padding-right: 5px!important;
        }
        .reservation-header {
            padding: 10px 0px;
        }
        .card-reservation {
            width: 100%;
        }

        .res-connector {
            width: 100%;
        }
        .h3-res {
            font-size: 16px;
        }
        
        .h5-res {
            font-size: 14px;
        }

        .h6-res {
            font-size: 11px;
        }

        .cust-result {
            padding: 20px;
        }
    }
</style>
@extends('reservation.main')
@section('content')
<style>
    .font-12 {
        font-size: 12px;
    }
    .font-14 {
        font-size: 14px;
    }
    .font-24 {
        font-size: 24px;
    }
    .color-a4a4a4 {
        color: #A4A4A4;
    }
    .color-323232 {
        color: #323232;
    }
    .col-pad {
        padding: 0 100px!important;
    }
    .side-width {
        width: 60%!important;
    }
    .patient-card {
        border:1px dashed #C3C3C3;
        background: #FDFDFD;
        border-radius:10px;
        margin: 0 auto;
        padding: 20px;
    }
    .status-0 {
        background: #EDD0D0;
        padding: 8px 18px;
        color: #E92222;
        border-radius: 14px;
    }
    .status-1 {
        background: #EAFFDA;
        padding: 8px 18px;
        color: #72BA6C;
        border-radius: 14px;
    }
    @media screen and (max-width: 1300px){
        .print-card {
            width: 70%;
        }
        .side-width {
            width: 70%!important;
        }
    }
    @media screen and (max-width: 1200px){
        .col-pad {
            padding: 0 70px!important;
        }
        .print-card {
            width: 80%;
        }
        .side-width {
            width: 80%!important;
        }
    }
    @media screen and (max-width: 992px){
        .col-pad {
            padding:0 20px!important;
        }
    }
</style>
<div class="container-fluid px-0">
    <div class="card card-reservation mb-4 px-2 py-4">
        <div class="card-body">
            <div class="row mb-4">
                <div class="col-md-12">
                    <h3 class="font-weight-bold h3-res">{{$title}}</h3>
                    <span class="d-block color-a4a4a4">Isi form reservasi mu secara lengkap dan pastikan cetak kartu untuk bukti pendaftaran</span>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-md-12">
                    <h5 class="font-weight-bold mb-3">Detail Pasien</h5>
                    <div class="patient-card">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table-note" style="margin: 0 auto;">
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">No. Member</td>
                                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4">{{$data->getCustomer->code}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-14 font-weight-bold color-323232">No. Reservasi</td>
                                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4">{{$data->reservation_number}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table-note" style="margin:0 auto;">
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">Nama</td>
                                        <td class="text-center font-weight-bold font-14" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4">{{$data->getCustomer->name}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">Status</td>
                                        <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                        @if(!is_null($data->getCustomer->marital_status))
                                        <td class="font-14 color-a4a4a4">{{$maritalStatus[$data->getCustomer->marital_status]}}</td>
                                        @else
                                        <td class="font-14 color-a4a4a4">-</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">Tanggal Lahir</td>
                                        <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4">
                                            @if(!is_null($data->getCustomer->date_of_birth))
                                                {{$data->getCustomer->place_of_birth}}, {{\Carbon\Carbon::parse($data->getCustomer->date_of_birth)->isoFormat('D MMMM Y')}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">Jenis Kelamin</td>
                                        <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                        @if($data->getCustomer->gender == 'male')
                                        <td class="font-14 color-a4a4a4">Laki - Laki</td>
                                        @elseif($data->getCustomer->gender == 'female')
                                        <td class="font-14 color-a4a4a4">Perempuan</td>
                                        @else
                                        <td class="font-14 color-a4a4a4">-</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">Usia</td>
                                        <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                        @if(!is_null($data->getCustomer->age))
                                        <td class="font-14 color-a4a4a4">{{$data->getCustomer->age}} tahun</td>
                                        @else
                                        <td class="font-14 color-a4a4a4">-</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">Agama</td>
                                        <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                        @if(!is_null($data->getCustomer->religion))
                                        <td class="font-14 color-a4a4a4">{{$religion[$data->getCustomer->religion]}}</td>
                                        @else
                                        <td class="font-14 color-a4a4a4">-</td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-md-12">
                    <hr style="width:98%;margin:0 auto; background:#E9E9E9;">
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h5 class="font-weight-bold mb-3">Detail Reservasi</h5>
                    <div class="row">
                        <div class="col-md-6 mb-2">
                            <table class="table-note">
                                <tr>
                                    <td class="font-weight-bold font-14 color-323232">Tanggal Layanan</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">{{\Carbon\Carbon::parse($data->reservation_date)->isoFormat(' D MMMM Y  HH:mm:ss')}}</td>
                                </tr>
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">Layanan</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">{{$data->getProduct->name}}</td>
                                </tr>
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">Dokter</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">{{$data->getStaff->getUser->fullname}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6 mb-2">
                            <div class="w-80-100" style="margin:0 auto;">
                                <div class="patient-card">
                                    <table class="table-note">
                                        <tr>
                                            <td class="font-weight-bold font-14 color-323232" style="padding-bottom:0!important;">No. Reservasi</td>
                                            <td class="text-center font-14 font-weight-bold color-323232" width="40px" style="padding-bottom:0!important;">:</td>
                                            <td class="font-24 color-a4a4a4" style="padding-bottom:0!important;">{{$data->reservation_number}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        
            
        </div>
    </div>
</div>
@endsection

@extends('reservation.main')
@section('content')
<div class="container-fluid px-0">
    <div class="card card-reservation mb-4 px-2 py-4">
        <div class="card-body">
            <div class="row mb-4">
                <div class="col-md-12">
                    <h3 class="text-center font-weight-bold h3-res">{{$title}}</h3>
                    <span class="d-block text-center color-a4a4a4">Isi form reservasi mu secara lengkap dan pastikan simpan kartu untuk bukti pendaftaran</span>
                </div>
            </div>

            <div class="row mb-5">
                <div class="col-md-12">
                    <div class="res-connector d-flex align-items-center justify-content-center">
                        <div class="res-state line text-center active">
                            <span class="iconify" data-icon="bxs:user"></span>
                            <h6 class="font-weight-bold mt-3 color-a4a4a4 h6-res">Data Pasien</h6>
                        </div>
                        <div class="res-state line text-center @if($page=='service'||$page=='print') active @endif">
                            <span class="iconify" data-icon="fluent:book-add-24-filled"></span>
                            <h6 class="font-weight-bold mt-3 color-a4a4a4 h6-res">Layanan</h6>
                        </div>
                        <div class="res-state text-center @if($page=='print') active @endif">
                            <span class="iconify" data-icon="bxs:id-card"></span>
                            <h6 class="font-weight-bold mt-3 color-a4a4a4 h6-res">Simpan Kartu</h6>
                        </div>
                    </div>
                </div>
            </div>
            
            @if($page == 'cust')
            <div class="row d-flex align-items-center mb-3">
                <div class="col-md-6 mb-2">
                    <h5 class="font-weight-bold h5-res">Apakah anda sudah pernah mendaftar ?</h5>
                </div>
                <div class="col-md-6 d-flex justify-content-end mb-2">
                    <a href="{{route('reservation.add', ['merchant_id' => $merchantId, 'page'=>'cust'])}}" class="btn btn-outline-warning mr-2">Sudah</a>
                    <a href="{{route('reservation.create', ['merchant_id' => $merchantId, 'page'=>'cust'])}}" class="btn btn-warning text-white ml-2">Belum</a>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-12">
                    <h5 class="font-weight-bold h5-res">Pendaftaran Pasien Baru</h5>
                    <span class="color-a4a4a4">Isi data anda sesuai dengan KTP</span>
                </div>
            </div>
            <form onsubmit="return false;" id="form-member" class='form-horizontal form-konten' backdrop="">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">No. Pasien Baru</label>
                            <input type="text" class="form-control form-control-sm" name="code" placeholder="No Pasien Baru" value="{{is_null($customer->id)? $code:$customer->code}}" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">NIK <span class="text-danger">*</span></label>
                            <input type="number" class="form-control form-control-sm" name="identity_card_number" placeholder="NIK" value="{{$customer->identity_card_number}}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold" for="exampleInputPassword1">Jenis Kelamin</label>
                            <select name="gender" class="form-control form-control-sm" id="gender" required>
                                <option value="male" @if(strtolower($customer->gender) == 'male') selected @endif>Laki - Laki</option>
                                <option value="female" @if(strtolower($customer->gender) == 'female') selected @endif>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Nama Lengkap <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama Lengkap" value="{{$customer->name}}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold" for="exampleInputPassword1">Tanggal Lahir <span class="text-danger">*</span></label>
                            <input 
                                id="dob" 
                                type="date" 
                                class="form-control form-control-sm" 
                                name="date_of_birth" 
                                placeholder="Tanggal Lahir"
                                @if(!is_null($customer->date_of_birth))
                                    value="{{\Carbon\Carbon::parse($customer->date_of_birth)->format('Y-m-d')}}"
                                @endif  
                                required
                            >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Email</label>
                            <input type="email" class="form-control form-control-sm" name="email" placeholder="Email" value="{{$customer->email}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold" for="exampleInputPassword1">Pekerjaan <span class="text-danger">*</span></label>
                            <select name="md_job_id" class="form-control form-control-sm" id="job" required>
                                <option value="">--- Pilih Pekerjaan ---</option>
                                @foreach($jobs as $key => $item)
                                <option value="{{$item->id}}" @if($item->id == $customer->md_job_id) selected @endif>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold" for="exampleInputPassword1">Agama</label>
                            <select name="religion" class="form-control form-control-sm" id="religion">
                                <option value="">--- Pilih Agama ---</option>
                                @foreach($religions as $key => $item)
                                <option value="{{$key}}" @if($key == $customer->religion) selected @endif>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold" for="exampleInputPassword1">Usia <span class="text-danger">*</span></label>
                            <input id="age" type="number" class="form-control form-control-sm" name="age" value="{{$customer->age}}" placeholder="Usia" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold" for="exampleInputPassword1">No Telephone / Hp <span class="text-danger">*</span></label>
                            <input type="number" class="form-control form-control-sm" name="phone_number" value="{{$customer->phone_number}}" placeholder="Contoh : 081228612373" required>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold" for="exampleInputPassword1">Alamat <span class="text-danger">*</span></label>
                            <textarea style="width:100%;" name="address" class="form-control" placeholder="Alamat" required>{{$customer->address}}</textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold" for="exampleInputPassword1">Status Menikah <span class="text-danger">*</span></label>
                            <select name="marital_status" class="form-control form-control-sm" id="marital_status" required>
                                <option value="">--- Pilih Status ---</option>
                                @foreach($maritalStatus as $key => $item)
                                    <option value="{{$key}}" @if($key == $customer->marital_status) selected @endif>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                </div>

                <div class="modal-footer" style="border-top:none;">
                    @if($data->status != 1)
                    <button class="btn btn-success" type="submit">
                        Simpan & Lanjut <span class="iconify ml-2" data-icon="ooui:next-ltr"></span>
                    </button>
                    @endif
                </div>
                <input type='hidden' name='sc_customer_id' value='{{$customer->id }}'>
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <input type='hidden' name='id' value='{{$data->id}}'>
                <input type='hidden' name='md_user_id' value='{{$userId}}'>
                <input type='hidden' name='merchant_id' value='{{$merchantId}}'>
            </form>
            @endif

            @if($page == 'service')
            <div class="row mb-4">
                <div class="col-md-12">
                    <h5 class="font-weight-bold h5-res">Tambah Layanan Baru</h5>
                    <span class="color-a4a4a4">Tambah data Layanan mu disini dengan benar</span>
                </div>
            </div>
            <form onsubmit="return false;" id="form-service" class='form-horizontal form-konten' backdrop="">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Tanggal Reservasi <span class="text-danger">*</span></label>
                            <input type="text" class="form-control form-control-sm trans_time" name="reservation_date" value="{{$data->reservation_date}}" placeholder="Tanggal Reservasi">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Layanan</label>
                            <select class="form-control" name="sc_product_id" id="sc_product_id" required>
                                <option></option>
                                @foreach($service as $key => $item)
                                <option 
                                    value="{{$item->id}}"
                                    @if($item->id == $data->sc_product_id)
                                    selected
                                    @endif
                                >
                                    {{$item->name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Dokter</label>
                            <select class="form-control" name="md_merchant_staff_id" id="md_merchant_staff_id">
                                <option></option>
                                @foreach($doctor as $key => $item)
                                <option 
                                    value="{{$item->id}}"
                                    @if($item->id == $data->md_merchant_staff_id)
                                        selected
                                    @endif
                                >
                                    {{$item->getUser->fullname}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold d-block">Catatan :</label>
                            <span style="color: #A4A4A4;">Disini anda dapat memilih tanggal reservasi, layanan, & Dokter yang sesuai dengan jadwal reservasi anda. Setelah mengisi pastikan anda mengecek data tersebut sebelum menyimpan kartu reservasi.</span>
                        </div>
                    </div>      
                </div>
                <input type='hidden' name='id' value='{{$data->id }}'>
                <input type='hidden' name='_token' value='{{csrf_token()}}'>
                <input type='hidden' name='sc_customer_id' value='{{$customerId}}'>
                <input type='hidden' name='is_from_create' value='1'>
                <input type='hidden' name='merchant_id' value='{{$merchantId}}'>

                <div class="modal-footer" style="border-top:none;">
                    <a href="{{route('reservation.create', ['merchant_id'=> $merchantId,'id'=> $data->id, 'page'=> 'cust', 'member'=>$customer->id])}}" class="btn btn-light" data-dismiss="modal">Kembali</a>
                    @if($data->status != 1)
                    <button class="btn btn-success">
                        Simpan & Lanjut <span class="iconify ml-2" data-icon="ooui:next-ltr"></span>
                    </button>
                    @endif
                </div>
            </form>
            @endif
            @if($page == 'print')
            <div class="row mb-4">
                <div class="col-md-12">
                    <h5 class="font-weight-bold h5-res">Simpan Kartu Reservasi</h5>
                    {{---<span class="color-a4a4a4">Simpan kartu untuk bukti reservasi</span>---}}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 mb-3">
                    <div class="print-card mb-2">
                        <h3 class="text-center font-weight-bold">{{ucwords($data->getMerchant->name)}}</h3>
                        <span class="d-block text-center mb-2" style="color:#A4A4A4;">{{$data->getMerchant->address}}</span>
                        <div style="width:100%;">
                            <img style="width:100%; height:250px;object-fit:contain;" src="{{env('S3_URL')}}{{$data->qr_code_file}}" alt="qr_code">
                        </div>
                        
                        <div style="width:70%; margin:0 auto;">
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width: 50%;">No Reservasi</dt>
                                <dd class="text-right" style="width: 50%;">{{$data->reservation_number}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width: 50%;">No Member</dt>
                                <dd class="text-right" style="width: 50%;">{{$data->getCustomer->code}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width: 50%;">Nama</dt>
                                <dd class="text-right" style="width: 50%;">{{$data->getCustomer->name}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width: 50%;">Layanan</dt>
                                <dd class="text-right" style="width: 50%;">{{$data->getProduct->name}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width: 50%;">Dokter</dt>
                                <dd class="text-right" style="width: 50%;">{{$data->getStaff->getUser->fullname}}</dd>
                            </dl>
                            <dl class="d-flex justify-content-between mb-0">
                                <dt style="width: 50%;">Tanggal</dt>
                                <dd class="text-right" style="width: 50%;">{{\Carbon\Carbon::parse($data->reservation_date)->isoFormat(' DD/MM/Y  HH:mm:ss')}}</dd>
                            </dl>
                            <h3 class="font-weight-bold text-center mt-3">TerimaKasih</h3>
                            <span class="d-block text-center" style="color:#A4A4A4;"><i>Powered By Senna</i></span>
                        </div>
                    </div>
                    <i class="d-block text-center" style="color:#747474;">Nomor Reservasi Pasien</i>
                    <div class="d-flex align-items-center justify-content-center mt-4">
                        <a href="{{route('reservation.add', ['merchant_id'=>$merchantId, 'page'=> 'cust'])}}" class="btn btn-light mr-2" data-dismiss="modal">Kembali</a>
                        <a class="btn btn-success ml-2" href="{{route('reservation.print-pdf', ['id'=>$data->id])}}" target="_blank">
                            <span class="iconify mr-2" data-icon="fa-solid:download"></span> Download
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 mb-3">
                    <h4 class="font-weight-bold">Catatan</h4>
                    <span class="color-a4a4a4">Disini anda berhasil menginputkan data reservasi. Kemudian anda bisa dapat menyimpan kartu reservasi disini. Sebelum menyimpan perlu anda catat data yang anda inputkan lengkap dan benar.</span>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
$(document).ready(function () {
    let data = @json($data);
    $("#md_merchant_staff_category_id").select2({
        placeholder:'--- Pilih Kategori Spesialis ---'
    });
    $("#marital_status").select2();
    $("#religion").select2();
    $("#last_education").select2();
    $("#gender").select2();
    $("#blood_type").select2();
    $("#job").select2();
    $("#sc_product_id").select2({
        placeholder:'--- Pilih Layanan ---'
    });
    $("#md_merchant_staff_id").select2({
        placeholder: '--- Pilih Dokter ---'
    });
    dateTimePicker('.trans_time');

    $("#dob").on("change", function(){
        $.ajax({
            type: 'POST',
            data: {
                date: $("#dob").val(),
                _token:"{{ csrf_token() }}"
            },
            url: "{{route('reservation.calculate-age')}}",
            success:function(response){
                $("#age").val(response);
            }
        });
    });

    $('#form-member').submit(function () {
        var data = getFormData('form-member');
        ajaxTransfer("{{route('reservation.save-member')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    });

    $('#form-service').submit(function () {
        var data = getFormData('form-service');
        ajaxTransfer("{{route('reservation.save')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    });
});
</script>
@endsection


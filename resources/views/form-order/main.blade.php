<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="description" content="Senna adalah sebuah layanan multi-platfrom di Indonesia">
    <meta name="author" content="Senna">
    <title>{{$title}}</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link href="{{asset('public/backend')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='_timezone' class="timezone" value=''>
    <script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    @yield('css')
    <style>
        .section-wrapper {
            background: #fff;
        }
        .bg-form-overlay {
            position: absolute;
            height: 50%;
            width: 100%;
            background: #FF8929;
        }
        .add-list {
            background-color: #FFF;
            border:none;
            color:#FFA943;
        }
        .add-list:hover {
            color: #FFA943;
        }
        .add-list:focus {
            outline:none;
        }
        #form-konten textarea.form-control.note_value:disabled {
            background-color:#fff!important;
            cursor: default;
        }
    </style>
</head>

<body class="navigation-toggle-one">
    <div id="main">
        <main class="main-content poppin">
            @yield('content')
        </main>
    </div>

    <script>
        $(document).ready(function(){
            var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            $(".timezone").val(timezone);
        })
    </script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" integrity="sha512-yI2XOLiLTQtFA9IYePU0Q1Wt0Mjm/UkmlKMy4TU4oNXEws0LybqbifbO8t9rEIU65qdmtomQEQ+b3XfLfCzNaw==" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" integrity="sha512-qiKM6FJbI5x5+GL5CEbAUK0suRhjXVMRXnH/XQJaaQ6iQPf05XxbFBE4jS6VJzPGIRg7xREZTrGJIZVk1MLclA==" crossorigin="anonymous"></script>
    <script src="{{asset('public')}}{{mix('backend/js/toast-custom.min.js')}}"></script>
    @yield('js')
</body>
</html>


@foreach($transType as $item)
    @if(count($item["data"]) > 0)
    <div 
        class="form-group" 
        data-id="pm-va" 
        id="pm-va"
        data-type="radio"
        data-label="Virtual Account"
        data-name="payment_method"
        data-section-type="metode-pembayaran"
        data-required="true"
        data-container-type="div"
        style="position:relative;"
    >
        @if($item['name']=='Pembayaran Digital')
            @foreach($item['data'] as $d)
                <label class="font-weight-bold">{{$d['name']}}</label>
                @if(!empty($d['data']))
                    @foreach($d['data'] as $c)
                        <div class="form-check">
                            <input type="radio" class="form-check-input" name="payment_method" value="{{$c['trans_id']}}_{{$c['acc_coa_id']}}_1_{{$c['alias']}}_{{$c['id']}}" data-label="{{$c['name']}}">
                            <label>{{$c['name']}}</label>
                        </div>
                    @endforeach
                @endif
            @endforeach
        @endif
    </div>
    @endif
@endforeach
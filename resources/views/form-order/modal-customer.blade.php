<style>
    /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-customer" class='form-horizontal form-konten' backdrop="">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Kode</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$data->code}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Nama Pelanggan</label>
                <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{$data->name}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="email" class="form-control form-control-sm" name="email" value="{{$data->email}}" placeholder="Email" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">No Telp</label>
                <input type="number" class="form-control form-control-sm" name="phone_number" value="{{$data->phone_number}}" placeholder="No. Telp" required>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1">Alamat</label>
                <textarea name="address" class="form-control" placeholder="Alamat">{{$data->address}}</textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Provinsi</label>
                <select name="region" class="form-control form-control-sm" id="region">
                    <option></option>
                    @foreach($provinces as $key => $item)
                        <option value="{{$item->id}}" @if(strtolower($data->region_name) == strtolower($item->name)) selected @endif>{{$item->name}}</option>
                    @endforeach

                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Kota</label>
                <select name="city" class="form-control form-control-sm" id="city">
                    <option></option>
                    @if(!is_null($data->city_name))
                    <option value="{{$data->city_name}}" selected>{{$data->city_name}}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Tipe Alamat</label>
                <select name="type_address" class="form-control form-control-sm" id="type_address">
                    <option></option>
                    <option value="home" @if(strtolower($data->type_address == 'home'))  selected @endif>Rumah</option>
                    <option value="work" @if(strtolower($data->type_address == 'work'))  selected @endif>Kantor</option>
                    <option value="other" @if(strtolower($data->type_address == 'other'))  selected @endif>Lainnya</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Kode Pos</label>
                <input type="text" class="form-control form-control-sm" name="postal_code" placeholder="Nama" value="{{$data->postal_code}}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Jenis Kelamin</label>
                <select name="gender" class="form-control form-control-sm" id="gender">
                    <option></option>
                    <option value="male" @if(strtolower($data->gender) == 'male') selected @endif>Laki - Laki</option>
                    <option value="female" @if(strtolower($data->gender) == 'female') selected @endif>Perempuan</option>
                    <option value="unspecified" @if(strtolower($data->gender) == 'unspecified') selected @endif>Tidak Ditentukan</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Kategori</label>
                <select name="sc_customer_level_id" class="form-control form-control-sm" id="sc_customer_level">
                    <option></option>
                    @foreach($level as $item)
                        <option value="{{$item->id}}" @if($item->id==$data->sc_customer_level_id) selected @endif>{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='is_sale' value='{{$isFromSale }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type="hidden" name="user_id" value="{{$userId}}">
</form>

<script>
    $(document).ready(function () {
        $("#sc_customer_level").select2({
            placeholder: "--- Pilih Kategori ---"
        });
        $("#region").select2({
            placeholder: "--- Pilih Provinsi ---"
        });
        $("#city").select2({
            placeholder:  "--- Pilih Kota ---"
        });
        $("#gender").select2({
            placeholder: "--- Pilih Jenis Kelamin ---"
        });
        $("#type_address").select2({
            placeholder: "--- Pilih Tipe Alamat ---"
        });

        $('#form-customer').submit(function () {
            var data = getFormData('form-customer');
            data.append('region_name', $('#region option:selected').text());
            data.append('city_name', $('#city option:selected').text());

            ajaxTransfer("{{route('form-order.save-customer')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        });
    });


    $('#region').on('change', function(e){
        $('#city option').remove();
        $.ajax({
            url: '{{route("form-order.get-city")}}',
            method: 'POST',
            data: {'_token': "{{csrf_token()}}", 'id': $(this).val()},
            success:function(response){
                let html = '<option></option>';
                response.map(item => {
                    html += '<option value="'+item.id+'">'+item.name+'</option>';
                });
                $('#city').append(html);
            }
        });
    });

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        let id = "{{$data->id}}";
        let str = (!id)? "menambah":"mengedit";

        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk ${str} pelanggan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    $("#modal-target").on("hidden.bs.modal", function () {
        reload(100);
    });


</script>

@extends('form-order.main')
@section('title',$title)
@section('content')
    <style>
        .main-content {
            padding-left: 0!important;
            padding-right: 0!important;
        }
    </style>
    <div class="page-header">
        <div class="container-fluid">
            <div class="py-2 px-4">
                <h4 class="mb-1">Detail Penjualan</h4>
                <span>Berikut merupakan informasi lengkap terkait detail penjualan</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid px-4">
        {{--- <ul class="nav nav-tabs tabs-trans" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link @if($page=='transaksi')active @endif" id="home-tab"  href="{{route('form-order.detail',['id'=>$data->id])}}?page=transaksi" role="tab" aria-controls="transaksi" aria-selected="true">Transaksi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($page=='pembayaran')active @endif " id="home-tab"  href="{{route('form-order.detail',['id'=>$data->id])}}?page=pembayaran" role="tab" aria-controls="pembayaran" aria-selected="true">Pembayaran</a>
            </li>
        </ul> ---}}
        <div class="row">
            <div class="col-md-3">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Kode Transaksi :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->code}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Waktu Transaksi :</span>
                            </dd>
                            <dt>
                                <h5>{{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd class="trans-info">
                                <span>Status Transaksi :</span>
                            </dd>
                            <dt>
                                <h5 class="text-tosca">{{$data->getTransactionStatus->description}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Jatuh Tempo Pembayaran :</span>
                            </dd>
                            <dt>
                                @if($data->is_debet==0)
                                    <h5>-</h5>
                                @else
                                    <h5>{{\Carbon\Carbon::parse($data->ar->due_date)->isoFormat(' D MMMM Y  HH:mm:ss')}}</h5>
                                @endif
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <a class="btn btn-light" href="{{route('form-order.index', ['param' => $param])}}">Kembali</a>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-2">
                                <h6 class="font-weight-bold">Nama Pelanggan</h6>
                            </dt>
                            <dd class="col-sm-10">
                                <span>{{(is_null($data->getCustomer))?'-':$data->getCustomer->name}}</span>
                            </dd>
                        </dl>
                        <h6 class="font-weight-bold mb-4">Item Produk</h6>
                        <div class="table-responsive">
                            <table class="table no-margin text-center table-custom">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Produk</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga Satuan</th>
                                        <th>Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data->getDetail->where('is_deleted',0) as $key =>$item)
                                        <tr>
                                            <td class="center">{{$key+1}}</td>
                                            <td>{{$item->getProduct->name}}</td>
                                            @if ($item->multi_quantity!=0)
                                                <td class="center">{{$item->multi_quantity}}</td>
                                                <td class="center">{{$item->unit_name}}</td>
                                            @else
                                                <td class="center">{{$item->quantity}}</td>
                                                <td class="center">{{$item->getProduct->getUnit->name}}</td>
                                            @endif
                                            @if ($item->is_bonus==0)
                                                <td class="center">{{rupiah($item->price)}}</td>
                                                <td class="center">{{rupiah($item->price*$item->quantity)}}</td>
                                            @else
                                                <td class="center">Bonus Produk</td>
                                                <td class="center">0</td>
                                            @endif

                                            @if ($item->multi_quantity!=0)
                                                @if($item->price*$item->multi_quantity!=$item->sub_total)
                                                @php
                                                    $promo_product+=$item->price*$item->quantity-$item->sub_total;
                                                @endphp
                                                @endif
                                            @else
                                                @if($item->price*$item->quantity!=$item->sub_total)
                                                @php
                                                    $promo_product+=$item->price*$item->quantity-$item->sub_total;
                                                @endphp
                                                @endif
                                            @endif

                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="font-weight-bold" style="border-bottom:none!important;" colspan="5">TOTAL</td>
                                        <td class="font-weight-bold" style="border-bottom:none!important;">{{rupiah($data->getDetail->where('is_deleted',0)->sum('sub_total'))}}</td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <table class="table-note">
                                    <tr>
                                        <td class="text-note">Catatan</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        @if(!is_null($data->note_order))
                                            <td class="text-note">{{$data->note_order}}</td>
                                        @else
                                            <td class="text-note">-</td>
                                        @endif                                        </tr>
                                    <tr>
                                        <td class="text-note">Status Penjualan</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td class="text-note">
                                            @if($data->is_deleted==1)
                                                <div class="badge badge-danger">Terhapus</div>
                                            @else
                                            -
                                            @endif
                                        </td>
                                    </tr>
                                    {{-- <tr>
                                        <td class="text-note">Bukti Penjualan</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td class="text-note">-</td>
                                    </tr> --}}


                                    <tr>
                                        <td class="text-note mb-2">Tautan Pembayaran</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td>
                                            @if($data->md_sc_transaction_status_id==1 && $data->is_payment_gateway==1)
                                            <a href="{{route('checkout', ['key' => $data->id])}}" class="btn btn-outline-warning btn-xs text-warning btn-rounded mb-2" target="_blank">
                                                <i class="fa fa-link mr-1"></i> Klik Disini
                                            </a>
                                                @php
                                                    $link = route('checkout', ['key'=>$data->id]);
                                                @endphp
                                                <a type="button" class="btn btn-outline-warning btn-rounded btn-xs px-2 rounded mr-2 mb-2" onclick="copyClipboard('{{$link}}')" data-placement="bottom" title="Salin URL Order Form">
                                                    <i id="fa-copy" title="copied" class="far fa-copy mr-2"></i>
                                                    <span>Salin URL</span>
                                                </a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                    
                                    @if($data->md_sc_transaction_status_id == 1 && $data->md_transaction_type_id==13)
                                    <tr>
                                        <td class="text-note">Terima Pembayaran Dari Bank Transfer</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td>
                                            <input type="hidden" id="coa_json" value="{{$coaJson}}">
                                            <a id="accept-transfer" class="btn btn-succcess btn-xs text-warning btn-rounded">
                                                <i class="fa fa-link mr-1"></i> Terima Pembayaran
                                            </a>
                                        </td>
                                    </tr>
                                    @endif

                                    @if($data->md_sc_transaction_status_id == 2 && $data->md_transaction_type_id==13)
                                        @if(!is_null($data->other_payment_info))
                                        @php
                                            $other_payment_info = json_decode($data->other_payment_info);
                                        @endphp
                                        <tr>
                                            <td class="text-note">Bank Pembayar</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td>{{$other_payment_info->bank_name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-note">No Rekening Pembayar</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td>{{$other_payment_info->account_number}}</td>
                                        </tr>
                                        @endif
                                    @endif

                                    @if(!is_null($data->md_merchant_payment_method_id))
                                    <tr>
                                        <td class="text-note">Download Invoice</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td>
                                            <a href="{{route('checkout.export-pdf', ['key'=> encrypt($data->id)])}}" target="_blank" class="btn btn-outline-warning btn-xs btn-rounded">
                                                <i class="far fa-file-pdf mr-2"></i>Download Invoice
                                            </a>
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-custom">
                                        <tr>
                                            <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Potongan Harga</td>
                                            <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah((is_null($data->promo))?0+$promo_product:$data->promo+$promo_product)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Pajak</td>
                                            <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah((is_null($data->tax))?0:$data->tax)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Biaya Pengiriman</td>
                                            <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah($data->shipping_cost)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Biaya Admin (Payment Digital)</td>
                                            <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah($data->admin_fee)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Total Tagihan</td>
                                            <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;text-align:right!important;">{{rupiah($data->total+$data->admin_fee)}}</td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('js')
    <script>
        $("#accept-transfer").on('click', function(e){
            e.preventDefault();

            let bank = @json($bank);
            let html = `<div class="form-konten">
                    <div class="form-group">
                        <label>Pilih Bank</label>
                        <select class="form-control" id="bank-select">
                            <option value="-1">--- Pilih Bank ---</option>`;

            bank.forEach(item => {
                html += `<option value="${item.id}">${item.name}</option>`
            })

            html += ` </select>
                </div>
                <div class="form-group">
                    <label>No Rekening</label>
                    <input class="form-control" name="no-rek" id="no-rek" placeholder="No Rekening">
                </div>
                <div class="form-group">
                    <span>Apakah anda yakin untuk menerima pembayaran sebesar <b>{{rupiah($data->total)}}</b> ? 
                        Setelah pembayaran diterima data tidak dapat diubah. Pastikan data yang anda masukkan benar
                    </span>
                </div>
                </div>`;

            setTimeout(() => {
                $("#bank-select").select2();
            }, 1000);

            modalConfirm('Terima Pembayaran', html, function () {
                const data = new FormData();
                data.append('sale_id', "{{$data->id}}");
                data.append('coa_json', $("#coa_json").val());
                data.append('bank_id', $('#bank-select').val());
                data.append('bank_name', $("#bank-select option:selected").text());
                data.append('no_rek', $('#no-rek').val());

                if(!/^[0-9]*$/.test($('#no-rek').val())){
                    toastForSaveData('No Rekening tidak valid','warning','false','');
                } else {
                    ajaxTransfer("{{route('checkout.accept-transfer')}}", data, function(response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                    });
                }
                
            });
        })
    </script>

    <script>
        $('.btn-preview').tooltip();
        $('#fa-copy').tooltip('hide');
        function copyClipboard(url){
            let copyText = url;
            let el = document.createElement('input');
            document.body.appendChild(el);
            el.value = url;
            el.select();
            document.execCommand("copy");
            document.body.removeChild(el);
            $('#fa-copy').tooltip('show');
            setTimeout(() => {
                $('#fa-copy').tooltip('dispose');
            }, 1000);
        }
    </script>
@endsection

<!-- modal add list -->
<div class="modal fade z1600" id="modal-add-list" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-list-label">Tambah List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <input type="hidden" name="list_item_id" id="list_item_id">
                    <div class="form-group">
                        <input class="form-control" type="text" name="list_name" id="list_name" placeholder="Nama List">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-add-list" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Tambah</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal add list -->


<script>
    function calcHeight(value) {
        let numberOfLineBreaks = (value.match(/\n/g) || []).length;
        let newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
        return newHeight + 80;
    }

    $(document).on("keyup", "#note_value", function(){
        let height = calcHeight($(this).val()) + "px!important";
        $(this).attr("style", `height: ${height}`);
    })
</script>
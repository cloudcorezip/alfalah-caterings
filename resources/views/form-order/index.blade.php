@extends('form-order.main')
@section('content')
    <style>
        .delete-row{
            background-color: #fff;
            color:rgba(119, 119, 119, 0.79);
            border:none;
            border-radius: 4px;
        }
        .delete-row:hover {
            background-color: #ff4400;
            color: #fff;
        }
    </style>
    <div class="container-fluid">
        <div class="card shadow mb-4 px-2 py-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <input type="hidden" class="form-control form-control-sm" name="timezone" id="timezone" value="">
                    <input type="hidden" class="form-control form-control-sm" name="paid_nominal" id="paid_nominal" value="0">
                    <input type="hidden" class="form-control form-control-sm" name="change_nominal" id="change_nominal" value="0" >
                    <div class="row mb-4 justify-content-between">
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label>Code</label>
                                <input type="text" class="form-control form-control-sm" name="code" value="{{$code}}" disabled>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Pelanggan</label>
                                <select id="sc_supplier_id" class="form-control form-control-sm" name="sc_customer_id" @if(!is_null($data->id)) disabled @endif>
                                    <option value="-1">--Pilih Pelanggan--</option>
                                    @foreach ($customer as $b)
                                        <option @if ($b->id==$data->sc_customer_id)
                                                selected
                                                @endif value="{{$b->id}}">{{$b->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Metode Pengiriman</label>
                                <select id="trans2" class="form-control form-control-sm" name="md_sc_shipping_category_id" required>
                                    <option value="-1">Pilih Metode Pengiriman</option>
                                    @foreach($shippingMethod as $item)
                                        <optgroup label="{{$item['name']}}">
                                            @if($item['is_group']==0)

                                                <option value="{{$item['data']->id}}_{{$item['data']->key}}">{{$item['data']->name}}</option>

                                            @else
                                                @foreach($item['data'] as $d)
                                                    <option value="{{$d->id}}_{{$d->key}}">{{$d->name}}</option>

                                                @endforeach
                                            @endif

                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Tanggal</label>
                                <input type="text" id="trans_time" class="form-control form-control-sm trans_time" name="created_at" value="{{$data->created_at}}" placeholder="Waktu Transaksi" @if(!is_null($data->id)) disabled @endif required>
                            </div>

                            <div class="form-group" id="tempo">
                                <label class="col-form-label">Tgl Jatuh Tempo</label>
                                <input type="text" class="form-control form-control-sm trans_time" name="due_date" value="{{date('Y-m-d H:i:s')}}" placeholder="Jatuh Tempo Pembayaran" required>
                            </div>



                        </div>

                    </div>

                    <div class="row mb-3" id="dynamic_field">
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            @if(is_null($data->id))
                                <button type="button" name="add" id="add" class="btn btn-darkblue btn-rounded py-2 px-4">
                                    <i class="fa fa-plus mr-2"></i>
                                    <span>Tambah Produk</span>
                                </button>
                            @endif
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Catatan :</label>
                                <textarea name="note" class="form-control form-control-sm" id="" cols="20" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-5 offset-md-2">
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note">SubTotal</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control form-control-sm text-right" id="total1" name="total1" value="0" disabled>
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note">Diskon(<span id="unit-diskon">%</span>)</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control form-control-sm text-right" step="0.01" min="0" max="100" name="discount_percentage" id="discount_percentage" value="{{0+$data->discount_percentage}}" @if (!is_null($data->id)) disabled @endif>
                                    <input type="hidden" class="form-control form-control-sm" name="discount" id="discount" value="{{$data->discount}}" >
                                </div>

                            </div>

                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note">Pajak(%)</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control form-control-sm text-right" name="tax_percentage" id="tax_percentage" step="0.01" min="0" max="100" value="{{0+$data->tax_percentage}}" @if (!is_null($data->id)) disabled @endif>
                                    <input type="hidden" class="form-control form-control-sm" name="tax" id="tax" value="{{$data->tax}}" >
                                </div>
                            </div>



                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note">Total</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control form-control-sm text-right" name="total" id="total" value="{{($data->getDetail->where('is_deleted',0)->count()>0)?$total+$data->tax-$data->discount:0}}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <input type='hidden' name='id' value='{{$data->id}}'>
                            <input type='hidden' name='is_with_load_shipping' value='0' id="beban-pengiriman-1">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="inventory_id" value="{{$inventory_id}}">
                            <input type="hidden" name="coa_json" value="{{$coa_json}}">
                            <input type="hidden" name="step_type" value="0">
                            <input type="hidden" name="warehouse" value="{{$warehouse}}">
                            <input type="hidden" name="param" value="{{$param}}">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            var i=0;
            var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
            var d = new Date();
            sumTotal();
            $("#is_debet").select2();
            $("#tempo").hide();
            $("#timezone").val($('.timezone').val());

            $('#sc_supplier_id').select2().on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_supplier_id-results').append('<a id="add-customer" onclick="loadModalFullScreen(this)" target="{{route("form-order.add-customer")}}?is_sale=1&param={{$merchantId}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Pelanggan Baru</b></a>');
            });

            $(document).on('click','#add-customer', function(e){
                $("#sc_supplier_id").select2('close');
            });
            var codes = [];


            $('#is_debet').change(function(){
                var debet=$("#is_debet").val();
                if(debet==0)
                {
                    $("#tempo").hide();
                    $("#transs").show();
                }else{
                    $("#tempo").show();
                    $("#transs").hide();
                    $("#trans").val("-1");

                }
            });


            $('#trans').change(function(){

                var payment = $("#trans").val().split("_")

                if(payment[2]==1){

                    $("#is_debet").val(0)
                    $("#ar").hide()

                }else{
                    $("#is_debet").val(0)
                    $("#ar").show()
                }

            });

            var sum = 0;
            $(".subtotal").each(function(){
                sum += +$(this).val();
            });
            if(sum==0)
            {
                $("#save-po").attr("disabled", true);
            }
            $("#total1").val(sum);

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            $('#sc_supplier_id').select2();
            $('.sc_product_id').select2();
            $('#trans2').select2();

            function formatState (state) {
                if (!state.id) {
                    return state.text;
                }
                var id = state.text
                var baseUrl;
                var $state;


                if(id.search(/QRIS|Virtual Account|E-Wallet/)==0){
                    if(id.search(/QRIS|Virtual Account/)==0){
                        baseUrl="{{env('S3_URL')}}"+"public/transactionType"
                    }else{
                        baseUrl="{{env('S3_URL')}}"+"public/payment-icon"

                    }
                    var name = state.text.replace(/Virtual Account - |E-Wallet - /,'')

                    if(name=='QRIS (Hanya Label)'){
                        $state = $(
                            '<span>' + state.text + '</span>'
                        );
                    }else{
                        $state = $(
                            '<span><img src="' + baseUrl + '/' + name.replace(' ','').toLowerCase() + '.png" class="img-flag" style="width: 40px" /> ' + state.text + '</span>'
                        );
                    }

                }else{
                    $state = $(
                        '<span>' + state.text + '</span>'
                    );
                }

                return $state;
            }

            $('#trans').select2({
                templateResult: formatState,
                templateSelection: formatState

            })



            $('#add').click(function(){
                i++;
                let html = `<div class="col-lg-4 col-md-6" id="row${i}">
                            <div class="card rounded">
                                <div class="card-body card-prod" style="position:relative;">
                                    <div style="position:absolute;top:10px;right:10px;">
                                        <button class="delete-row" data-id="${i}">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                    <input type ="hidden" value="0" name="sc_purchase_order_detail_id[]">
                                    <div class="form-group">
                                        <label class="col-form-label">Pilih Produk</label>
                                        <select data-row-id="${i}" name="sc_product_id[]" id="sc_product_id${i}" class="form-control"></select>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Harga Jual</label>
                                        <input type="number" name="selling_price[]" min="0" step="0.01" class="form-control form-control-sm selling_price text-right"  id="selling_price${i}"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label">Quantity</label>
                                        <input type="number" name="quantity[]" min="0" step="0.01" id="quantity${i}" class="form-control form-control-sm quantity text-right" value="0"/>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label">Sub Total</label>
                                        <input type="number" name="subtotal[]" id="subtotal${i}" class="form-control form-control-sm subtotal text-right" value="0" disabled/>
                                    </div>
                                    <input type="hidden" name="coa_inv_id[]" id="coa_inv_id${i}" class="form-control form-control-sm"/>
                                    <input type="hidden" name="coa_hpp_id[]" id="coa_hpp_id${i}" class="form-control form-control-sm"/>
                                </div>
                            </div>
                        </div>`
                $('#dynamic_field').append(html);
                $("#sc_product_id" + i).select2({
                    placeholder:"---- Pilih Produk ----",
                    ajax: {
                        type: "GET",
                        user:$("#sc_supplier_id").val(),
                        url: "{{route('api.product.all',['userId'=>$userId])}}",
                        dataType: 'json',
                        delay: 250,
                        headers:{
                            "senna-auth":"{{$token}}"
                        },
                        data: function (params) {
                            return {
                                key: params.term
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    },
                });

                $("#sc_product_id" + i).on('select2:select', function (e) {
                    let data = e.params.data;
                    let id = $(this).attr('data-row-id');
                    let rowId = `row${id}`;
                    $("#selling_price"+id).val(data.selling_price)

                    let cardProd = $(this).closest(".card-prod");
                    $("#quantity"+id).val(1);
                    let quantity=$("#quantity"+id).val();
                    let selling_price=$("#selling_price"+id).val();
                    $("#coa_inv_id"+id).val(data.coa_inv_id)
                    $("#coa_hpp_id"+id).val(data.coa_hpp_id)
                    $("#subtotal"+id).val(quantity*selling_price);

                    let duplicateCode = codes.filter(item => {
                        return item.code == data.code;
                    });
                    
                    if(duplicateCode.length > 0){
                        let row = duplicateCode[0].row;
                        let val = $("#quantity" + row).val();
                        $("#quantity" + row).val(parseInt(val)+1);
                        $("#subtotal" + row).val( $("#quantity" + row).val() *  $("#selling_price" + row).val());
                        $("#quantity" + row).focus();
                        $('#'+rowId).remove();
                        sumTotal();
                        codes = [...codes].filter(item => {
                            return item.row != id
                        });
                    } else {
                        if(codes.findIndex(item => item.row == id) != -1){
                            codes = [...codes].map(item => {
                                if(item.row == id){
                                    return {
                                        ...item,
                                        code:data.code
                                    }
                                } else {
                                    return {
                                        ...item
                                    }
                                }
                            })
                        } else {
                            codes.push({
                                code:data.code,
                                row:id
                            });
                        }
                    }
                    
                    cardProd.find(".subtotal").val(quantity*selling_price);
                    sumTotal();

                });

            });

            $(document).on("change", ".card-prod .form-group input", function() {
                let row = $(this).closest(".card-prod");
                let quantity = parseFloat(row.find(".quantity").val());
                let selling_price=parseFloat(row.find(".selling_price").val());
                let subtotal=quantity*selling_price;
                row.find(".subtotal").val(subtotal);
                sumTotal();
            });

            $( "#discount_percentage").change(function() {
                sumTotal();
            });
            $("#tax_percentage").change(function() {
                sumTotal();
            });
            $("#shipping_cost").change(function() {
                sumTotal();
            });


            $(document).on('click', '.delete-row', function(){
                var button_id = $(this).attr("data-id");
                $('#row'+button_id+'').remove();
                codes = codes.filter(item => {
                    return item.row != button_id
                });
                sumTotal();
            });
        });

        function sumTotal(){
            var sum = 0;
            $(".subtotal").each(function(){
                sum += +$(this).val();
            });
            if(sum==0)
            {
                $("#save-po").attr("disabled", true);
            }else {
                $("#save-po").attr("disabled", false);

            }
            $("#total1").val(sum);
            var subtotal=$("#total1").val();
            var tax_percentage=$("#tax_percentage").val();
            var discount_percentage=$("#discount_percentage").val();
            var shipping_cost=$("#shipping_cost").val();
            if($('#unit-diskon').text()=='%'){
                var discount = subtotal*discount_percentage/100;
            }else{
                var discount = discount_percentage;
            }

            var price = subtotal-discount;
            var tax = price*tax_percentage/100;
            var total=price+tax;

            $("#tax").val(tax)
            $("#discount").val(discount)
            $("#total").val(total);

        }

        $('#form-konten').submit(function () {
            sumTotal();
            var data = getFormData('form-konten');
            var shipping = $("#trans2").val().split("_");

            data.append("md_sc_shipping_category_id",shipping[0]);
            data.append("is_debet", 0);

            ajaxTransfer("{{route('form-order.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        });
    </script>

@endsection


<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi.">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <title>{{$title}}</title>
    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/backend/css/auth.css')}}">
    <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>
    <meta name="google-signin-client_id" content="{{env('GOOGLE_CLIENT_ID')}}">

    <meta name="base-url" content="https://sennakasir.co.id">
    <meta name="base-domain" content="sennakasir.co.id">
    <meta name="author" content="Senna Kasir">
    <meta itemprop="name" content="Senna Kasir">
    <meta itemprop="description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi.">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi." />
    <meta name="twitter:url" content="{{route('forgot.password')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('forgot.password')}}" />
    <meta property="og:description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi." />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna Kasir" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna Kasir" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />

    @if(env('APP_ENV')=='production')
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-0M6J25N7DN"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-0M6J25N7DN');
        </script>

        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5BHVKPP');</script>

        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '382739596758971');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=382739596758971&ev=PageView&noscript=1"
            /></noscript>
    @endif


</head>

<body style="background: white">

<div class="auth-wrapper">
    <div class="row auth-row__wrapper">
        <div class="auth-bg__dark"></div>
        <div class="col-md-12 mb-3 pt-4 pb-0 auth-logo__wrapper">
            <img src="{{env('S3_URL')}}public/frontend/img/logo.png" alt="logo" width="10%">
        </div>
        <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12 col-12 auth-right__content">
            <div class="card px-2 auth-card__wrapper" style="width:80%!important;">
                <div class="card-body">
                    <h2 class="auth-form__title mb-1">Lupa Password ?</h2>
                    <span class="d-block mt-2 mb-3 font-bold-500">Masukkan Emailmu dan Kami Akan Mengirim Tautan Untuk Mereset Passwordmu</span>
                    <div id="results"></div>
                    <form id="form-konten" action=""  class="user" onsubmit="return false">
                        <div class="form-group" style="position:relative">
                            <i 
                                class="fa fa-envelope"
                                style="position:absolute;top:50%; transform:translateY(-50%); left:15px; font-size:14px;color:#C4C4C4;"
                            ></i>
                            <input type="email" name="email" class="form-control border-0 form-input" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="E-mail" style="padding-left:35px!important;">
                        </div>
                        <button class="btn btn-auth__submit btn-block" type="submit">
                            <span class="bold poppin" style="font-size: 10pt">Kirim</span>
                        </button>
                        <span class="d-block mt-2 mb-3 font-bold-500">Sudah Punya Akun ?  <a href="{{route('login')}}" class="font-or fw-700">Login</a></span>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12 p-0 d-flex align-items-end justify-content-center">
            <span class="text-center pb-2">
                © {{date('Y')}} <a class="font-or fw-700" href="{{route('home')}}">Senna Indonesia</a> All Rights Reserved
            </span>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript-->
<input type='hidden' name='_token' value='{{ csrf_token() }}'>
<input type='hidden' name='_timezone' class="timezone" value=''>
<script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>

<script type="text/javascript">
    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{url('/forgot-password/send-link')}}", data, '#results');
    });
    $(document).ready(function() {
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $(".timezone").val(timezone);
    });
</script>
</body>


</html>

<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Monitor Semua Cabang darimanapun dan mudah mengatur penjualan, absensi, stok, keuangan, pelanggan loyalti, promosi.">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">

    <title>{{$title}}</title>
    <!-- Custom fonts for this template-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="{{env('S3_URL')}}public/frontend/img/favicon.png" />
    <link rel="stylesheet" href="{{asset('public')}}{{mix('backend/css/backend.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/backend/css/auth.css')}}">
    <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>
    <meta name="google-signin-client_id" content="{{env('GOOGLE_CLIENT_ID')}}">
</head>

<body style="background: white">

<div class="auth-wrapper">
    <div class="row auth-row__wrapper">
        <div class="auth-bg__dark"></div>
        <div class="col-md-12 mb-3 pt-4 pb-0 auth-logo__wrapper">
            <img src="{{env('S3_URL')}}public/frontend/img/logo.png" alt="logo" width="10%">
        </div>
        
        <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12 col-12 auth-right__content">
            <div class="card px-2 auth-card__wrapper" style="width:80%!important;">
                <div class="card-body">
                    <h2 class="auth-form__title mb-1">Reset Password</h2>
                    <span class="d-block mt-2 mb-3 font-bold-500">Silahkan Masukkan Password Baru</span>
                    <div id="results"></div>
                    <form id="form-konten" action=""  class="user" onsubmit="return false">
                        <input type="hidden" name="key" value="{{$data->token}}">
                        <input type="hidden" name="p" value="{{encrypt($data->email)}}">
                        <div class="mb-2" style="position:relative">
                            <i 
                                class="fa fa-unlock-alt"
                                style="position:absolute;top:50%; transform:translateY(-50%); left:15px; font-size:16px;color:#C4C4C4;"
                            ></i>
                            <input name="password" type="password" class="form-control border-0 form-input" id="password" placeholder="Password" style="padding-left:35px!important;" required/>
                            <i
                                id="input-eye1"
                                style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                                class="fa fa-eye">
                            </i>
                        </div>
                        <div class="mb-2" style="position:relative">
                            <i 
                                class="fa fa-unlock-alt"
                                style="position:absolute;top:50%; transform:translateY(-50%); left:15px; font-size:16px;color:#C4C4C4;"
                            ></i>
                            <input name="confirm_password" type="password" class="form-control border-0 form-input" id="confirm_password" placeholder="Konfirmasi Password" style="padding-left:35px!important;" required/>
                            <i
                                id="input-eye2"
                                style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                                class="fa fa-eye">
                            </i>
                        </div>
                        <button class="btn btn-auth__submit btn-block" type="submit">
                            <span class="bold poppin" style="font-size: 10pt">Simpan</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12 p-0 d-flex align-items-end justify-content-center">
            <span class="text-center pb-2">
                © {{date('Y')}} <a class="font-or fw-700" href="{{route('home')}}">Senna Indonesia</a> All Rights Reserved
            </span>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript-->
<input type='hidden' name='_token' value='{{ csrf_token() }}'>
<input type='hidden' name='_timezone' class="timezone" value=''>
<script src="{{asset('public')}}{{mix('backend/js/backend.min.js')}}"></script>

<script type="text/javascript">
    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{route('forgot.password.save')}}", data, '#results');
    });

    $('#input-eye1').on('click', function(){
        if($('#input-eye1').hasClass('fa fa-eye')){
            $('#password').attr('type', 'text');
            $('#input-eye1').removeClass('fa fa-eye');
            $('#input-eye1').addClass('fa fa-eye-slash');
        } else {
            $('#password').attr('type', 'password');
            $('#input-eye1').removeClass('fa fa-eye-slash');
            $('#input-eye1').addClass('fa fa-eye');
        }

    });

    $('#input-eye2').on('click', function(){
        if($('#input-eye2').hasClass('fa fa-eye')){
            $('#confirm_password').attr('type', 'text');
            $('#input-eye2').removeClass('fa fa-eye');
            $('#input-eye2').addClass('fa fa-eye-slash');
        } else {
            $('#confirm_password').attr('type', 'password');
            $('#input-eye2').removeClass('fa fa-eye-slash');
            $('#input-eye2').addClass('fa fa-eye');
        }

    });

    $(document).ready(function() {
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $(".timezone").val(timezone);
    });
</script>
</body>


</html>

@extends('blog.main')
@section('meta')
    <meta name="description" content="Rumah Senna membantu usahamu makin maju dari segi permodalan,literasi keuangan dan berbagi bersama">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="Rumah Senna membantu usahamu makin maju dari segi permodalan,literasi keuangan dan berbagi bersama">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="Rumah Senna membantu usahamu makin maju dari segi permodalan,literasi keuangan dan berbagi bersama" />
    <meta name="twitter:url" content="{{route('rumah-senna')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('rumah-senna')}}" />
    <meta property="og:description" content="Rumah Senna membantu usahamu makin maju dari segi permodalan,literasi keuangan dan berbagi bersama" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="article" />
    <meta property="article:tag" content="aplikasi toko online"/>
    <meta property="article:tag" content="aplikasi toko online terbaik"/>
    <meta property="article:tag" content="aplikasi toko online lengkap"/>
    <meta property="article:tag" content="aplikasi toko online murah"/>
    <meta property="article:tag" content="aplikasi toko online dropship"/>
    <meta property="article:tag" content="aplikasi toko online reseller"/>
    <meta property="article:tag" content="aplikasi toko online mobile"/>
    <meta property="article:tag" content="aplikasi toko online android"/>
    <meta property="article:tag" content="aplikasi toko online ios"/>
    <meta property="article:tag" content="aplikasi toko online indonesia"/>
    <meta property="article:tag" content="aplikasi toko online agen"/>
    <meta property="article:tag" content="aplikasi toko online grosir"/>
    <meta property="article:tag" content="aplikasi toko online umkm"/>
    <meta property="article:tag" content="aplikasi jual beli online"/>
    <meta property="article:tag" content="aplikasi online shop"/>
    <meta property="article:tag" content="aplikasi online shop terbaik"/>
    <meta property="article:tag" content="aplikasi online shop lengkap"/>
    <meta property="article:tag" content="aplikasi online shop murah"/>
    <meta property="article:tag" content="aplikasi online shop dropship"/>
    <meta property="article:tag" content="aplikasi online shop reseller"/>
    <meta property="article:tag" content="aplikasi online shop mobile"/>
    <meta property="article:tag" content="aplikasi online shop android"/>
    <meta property="article:tag" content="aplikasi online shop ios"/>
    <meta property="article:tag" content="aplikasi online shop indonesia"/>
    <meta property="article:tag" content="aplikasi online shop agen"/>
    <meta property="article:tag" content="aplikasi online shop grosir"/>
    <meta property="article:tag" content="aplikasi online shop umkm"/>
    <meta property="article:tag" content="aplikasi toko"/>
    <meta property="article:tag" content="aplikasi toko terbaik"/>
    <meta property="article:tag" content="aplikasi toko lengkap"/>
    <meta property="article:tag" content="aplikasi toko murah"/>
    <meta property="article:tag" content="aplikasi toko dropship"/>
    <meta property="article:tag" content="aplikasi toko reseller"/>
    <meta property="article:tag" content="aplikasi toko mobile"/>
    <meta property="article:tag" content="aplikasi toko android"/>
    <meta property="article:tag" content="aplikasi toko ios"/>
    <meta property="article:tag" content="aplikasi toko indonesia"/>
    <meta property="article:tag" content="aplikasi toko agen"/>
    <meta property="article:tag" content="aplikasi toko jual beli"/>
    <meta property="article:tag" content="aplikasi toko grosir"/>
    <meta property="article:tag" content="aplikasi toko umkm"/>
    <meta property="article:tag" content="aplikasi beli beli online"/>
    <meta property="article:tag" content="aplikasi pasar online"/>
    <meta property="article:tag" content="aplikasi bakul online"/>
    <meta property="article:tag" content="aplikasi jualan laris"/>
    <meta property="article:tag" content="aplikasi jual laris"/>
    <meta property="article:tag" content="aplikasi ecommerce"/>
    <meta property="article:tag" content="aplikasi ecommerce terbaik"/>
    <meta property="article:tag" content="aplikasi ecommerce indonesia"/>
    <meta property="article:tag" content="aplikasi ecommerce lengkap"/>
    <meta property="article:tag" content="aplikasi ecommerce murah"/>
    <meta property="article:tag" content="aplikasi ecommerce dropship"/>
    <meta property="article:tag" content="aplikasi ecommerce reseller"/>
    <meta property="article:tag" content="aplikasi ecommerce mobile"/>
    <meta property="article:tag" content="aplikasi ecommerce android"/>
    <meta property="article:tag" content="aplikasi ecommerce ios"/>
    <meta property="article:tag" content="aplikasi ecommerce agen"/>
    <meta property="article:tag" content="aplikasi ecommerce grosir"/>
    <meta property="article:tag" content="aplikasi ecommerce umkm"/>
    <meta property="article:tag" content="aplikasi reseller"/>
    <meta property="article:tag" content="aplikasi reseller lengkap"/>
    <meta property="article:tag" content="aplikasi reseller dropship"/>
    <meta property="article:tag" content="aplikasi reseller agen"/>
    <meta property="article:tag" content="aplikasi reseller mobile"/>
    <meta property="article:tag" content="aplikasi reseller android"/>
    <meta property="article:tag" content="aplikasi reseller ios"/>
    <meta property="article:tag" content="aplikasi reseller indonesia"/>
    <meta property="article:tag" content="aplikasi distributor indonesia"/>
    <meta property="article:tag" content="aplikasi distributor lengkap"/>
    <meta property="article:tag" content="aplikasi distributor dropship"/>
    <meta property="article:tag" content="aplikasi distributor agen"/>
    <meta property="article:tag" content="aplikasi distributor mobile"/>
    <meta property="article:tag" content="aplikasi distributor android"/>
    <meta property="article:tag" content="aplikasi distributor ios"/>
    <meta property="article:tag" content="aplikasi dropship indonesia"/>
    <meta property="article:tag" content="aplikasi dropship lengkap"/>
    <meta property="article:tag" content="aplikasi dropship dropship"/>
    <meta property="article:tag" content="aplikasi dropship agen"/>
    <meta property="article:tag" content="aplikasi dropship mobile"/>
    <meta property="article:tag" content="aplikasi dropship android"/>
    <meta property="article:tag" content="aplikasi dropship ios"/>
    <meta property="article:tag" content="aplikasi supplier"/>
    <meta property="article:tag" content="aplikasi supplier indonesia"/>
    <meta property="article:tag" content="aplikasi supplier lengkap"/>
    <meta property="article:tag" content="aplikasi supplier dropship"/>
    <meta property="article:tag" content="aplikasi supplier agen"/>
    <meta property="article:tag" content="aplikasi supplier mobile"/>
    <meta property="article:tag" content="aplikasi supplier android"/>
    <meta property="article:tag" content="aplikasi supplier ios"/>
    <meta property="article:tag" content="aplikasi bisnis online"/>
    <meta property="article:tag" content="aplikasi bisnis mudah"/>
    <meta property="article:tag" content="aplikasi bisnis murah"/>
    <meta property="article:tag" content="aplikasi bisnis digital"/>
    <meta property="article:tag" content="aplikasi bisnis lengkap"/>
    <meta property="article:tag" content="aplikasi digital marketing"/>
    <meta property="article:tag" content="aplikasi bisnis online shop"/>
    <meta property="article:tag" content="aplikasi bisnis ecommerce"/>
    <meta property="article:tag" content="aplikasi bisnis toko online"/>
    <meta property="article:tag" content="aplikasi akuntansi"/>
    <meta property="article:tag" content="aplikasi akuntansi online"/>
    <meta property="article:tag" content="aplikasi akuntansi online shop"/>
    <meta property="article:tag" content="aplikasi akuntansi ecommerce"/>
    <meta property="article:tag" content="aplikasi akuntansi toko online"/>
    <meta property="article:tag" content="aplikasi catat penjualan"/>
    <meta property="article:tag" content="aplikasi catat jualan online"/>
    <meta property="article:tag" content="aplikasi pencatatan jualan"/>
    <meta property="article:tag" content="aplikasi catat order"/>
    <meta property="article:tag" content="aplikasi jualan"/>
    <meta property="article:tag" content="aplikasi pergudangan"/>
    <meta property="article:tag" content="aplikasi catat stok"/>
    <meta property="article:tag" content="aplikasi manajemen stok"/>
    <meta property="article:tag" content="aplikasi agen reseller"/>
    <meta property="article:tag" content="aplikasi manajemen order"/>
    <meta property="article:tag" content="aplikasi manajemen pelanggan"/>
    <meta property="article:tag" content="aplikasi manajemen jualan"/>
    <meta property="article:tag" content="aplikasi manajemen reseller"/>
    <meta property="article:tag" content="aplikasi manajemen agen"/>
    <meta property="article:tag" content="aplikasi manajemen dropship"/>
    <meta property="article:tag" content="aplikasi manajemen pesanan"/>
    <meta property="article:tag" content="aplikasi jual beli umkm"/>
    <meta property="article:tag" content="website toko online"/>
    <meta property="article:tag" content="website toko online terbaik"/>
    <meta property="article:tag" content="website toko online lengkap"/>
    <meta property="article:tag" content="website toko online murah"/>
    <meta property="article:tag" content="website toko online dropship"/>
    <meta property="article:tag" content="website toko online reseller"/>
    <meta property="article:tag" content="website toko online mobile"/>
    <meta property="article:tag" content="website toko online android"/>
    <meta property="article:tag" content="website toko online ios"/>
    <meta property="article:tag" content="website toko online indonesia"/>
    <meta property="article:tag" content="website toko online agen"/>
    <meta property="article:tag" content="website toko online grosir"/>
    <meta property="article:tag" content="website toko online umkm"/>
    <meta property="article:tag" content="website jual beli online"/>
    <meta property="article:tag" content="website online shop"/>
    <meta property="article:tag" content="website online shop terbaik"/>
    <meta property="article:tag" content="website online shop lengkap"/>
    <meta property="article:tag" content="website online shop murah"/>
    <meta property="article:tag" content="website online shop dropship"/>
    <meta property="article:tag" content="website online shop reseller"/>
    <meta property="article:tag" content="website online shop mobile"/>
    <meta property="article:tag" content="website online shop android"/>
    <meta property="article:tag" content="website online shop ios"/>
    <meta property="article:tag" content="website online shop indonesia"/>
    <meta property="article:tag" content="website online shop agen"/>
    <meta property="article:tag" content="website online shop grosir"/>
    <meta property="article:tag" content="website online shop umkm"/>
    <meta property="article:tag" content="website toko"/>
    <meta property="article:tag" content="website toko terbaik"/>
    <meta property="article:tag" content="website toko lengkap"/>
    <meta property="article:tag" content="website toko murah"/>
    <meta property="article:tag" content="website toko dropship"/>
    <meta property="article:tag" content="website toko reseller"/>
    <meta property="article:tag" content="website toko mobile"/>
    <meta property="article:tag" content="website toko android"/>
    <meta property="article:tag" content="website toko ios"/>
    <meta property="article:tag" content="website toko indonesia"/>
    <meta property="article:tag" content="website toko agen"/>
    <meta property="article:tag" content="website toko jual beli"/>
    <meta property="article:tag" content="website toko grosir"/>
    <meta property="article:tag" content="website toko umkm"/>
    <meta property="article:tag" content="website beli beli online"/>
    <meta property="article:tag" content="website pasar online"/>
    <meta property="article:tag" content="website bakul online"/>
    <meta property="article:tag" content="website jualan laris"/>
    <meta property="article:tag" content="website jual laris"/>
    <meta property="article:tag" content="website ecommerce"/>
    <meta property="article:tag" content="website ecommerce terbaik"/>
    <meta property="article:tag" content="website ecommerce indonesia"/>
    <meta property="article:tag" content="website ecommerce lengkap"/>
    <meta property="article:tag" content="website ecommerce murah"/>
    <meta property="article:tag" content="website ecommerce dropship"/>
    <meta property="article:tag" content="website ecommerce reseller"/>
    <meta property="article:tag" content="website ecommerce mobile"/>
    <meta property="article:tag" content="website ecommerce android"/>
    <meta property="article:tag" content="website ecommerce ios"/>
    <meta property="article:tag" content="website ecommerce agen"/>
    <meta property="article:tag" content="website ecommerce grosir"/>
    <meta property="article:tag" content="website ecommerce umkm"/>
    <meta property="article:tag" content="website reseller"/>
    <meta property="article:tag" content="website reseller lengkap"/>
    <meta property="article:tag" content="website reseller dropship"/>
    <meta property="article:tag" content="website reseller agen"/>
    <meta property="article:tag" content="website reseller mobile"/>
    <meta property="article:tag" content="website reseller android"/>
    <meta property="article:tag" content="website reseller ios"/>
    <meta property="article:tag" content="website reseller indonesia"/>
    <meta property="article:tag" content="website distributor indonesia"/>
    <meta property="article:tag" content="website distributor lengkap"/>
    <meta property="article:tag" content="website distributor dropship"/>
    <meta property="article:tag" content="website distributor agen"/>
    <meta property="article:tag" content="website distributor mobile"/>
    <meta property="article:tag" content="website distributor android"/>
    <meta property="article:tag" content="website distributor ios"/>
    <meta property="article:tag" content="website dropship indonesia"/>
    <meta property="article:tag" content="website dropship lengkap"/>
    <meta property="article:tag" content="website dropship dropship"/>
    <meta property="article:tag" content="website dropship agen"/>
    <meta property="article:tag" content="website dropship mobile"/>
    <meta property="article:tag" content="website dropship android"/>
    <meta property="article:tag" content="website dropship ios"/>
    <meta property="article:tag" content="website supplier"/>
    <meta property="article:tag" content="website supplier indonesia"/>
    <meta property="article:tag" content="website supplier lengkap"/>
    <meta property="article:tag" content="website supplier dropship"/>
    <meta property="article:tag" content="website supplier agen"/>
    <meta property="article:tag" content="website supplier mobile"/>
    <meta property="article:tag" content="website supplier android"/>
    <meta property="article:tag" content="website supplier ios"/>
    <meta property="article:tag" content="website bisnis online"/>
    <meta property="article:tag" content="website bisnis mudah"/>
    <meta property="article:tag" content="website bisnis murah"/>
    <meta property="article:tag" content="website bisnis digital"/>
    <meta property="article:tag" content="website bisnis lengkap"/>
    <meta property="article:tag" content="website digital marketing"/>
    <meta property="article:tag" content="website bisnis online shop"/>
    <meta property="article:tag" content="website bisnis ecommerce"/>
    <meta property="article:tag" content="website bisnis toko online"/>
    <meta property="article:tag" content="website akuntansi"/>
    <meta property="article:tag" content="website akuntansi online"/>
    <meta property="article:tag" content="website akuntansi online shop"/>
    <meta property="article:tag" content="website akuntansi ecommerce"/>
    <meta property="article:tag" content="website akuntansi toko online"/>
    <meta property="article:tag" content="website catat penjualan"/>
    <meta property="article:tag" content="website catat jualan online"/>
    <meta property="article:tag" content="website pencatatan jualan"/>
    <meta property="article:tag" content="website catat order"/>
    <meta property="article:tag" content="website jualan"/>
    <meta property="article:tag" content="website pergudangan"/>
    <meta property="article:tag" content="website catat stok"/>
    <meta property="article:tag" content="website manajemen stok"/>
    <meta property="article:tag" content="website agen reseller"/>
    <meta property="article:tag" content="website manajemen order"/>
    <meta property="article:tag" content="website manajemen pelanggan"/>
    <meta property="article:tag" content="website manajemen jualan"/>
    <meta property="article:tag" content="website manajemen reseller"/>
    <meta property="article:tag" content="website manajemen agen"/>
    <meta property="article:tag" content="website manajemen dropship"/>
    <meta property="article:tag" content="website manajemen pesanan"/>
    <meta property="article:tag" content="website jual beli umkm"/>
    <meta property="article:tag" content="sistem toko online"/>
    <meta property="article:tag" content="sistem toko online terbaik"/>
    <meta property="article:tag" content="sistem toko online lengkap"/>
    <meta property="article:tag" content="sistem toko online murah"/>
    <meta property="article:tag" content="sistem toko online dropship"/>
    <meta property="article:tag" content="sistem toko online reseller"/>
    <meta property="article:tag" content="sistem toko online mobile"/>
    <meta property="article:tag" content="sistem toko online android"/>
    <meta property="article:tag" content="sistem toko online ios"/>
    <meta property="article:tag" content="sistem toko online indonesia"/>
    <meta property="article:tag" content="sistem toko online agen"/>
    <meta property="article:tag" content="sistem toko online grosir"/>
    <meta property="article:tag" content="sistem toko online umkm"/>
    <meta property="article:tag" content="sistem jual beli online"/>
    <meta property="article:tag" content="sistem online shop"/>
    <meta property="article:tag" content="sistem online shop terbaik"/>
    <meta property="article:tag" content="sistem online shop lengkap"/>
    <meta property="article:tag" content="sistem online shop murah"/>
    <meta property="article:tag" content="sistem online shop dropship"/>
    <meta property="article:tag" content="sistem online shop reseller"/>
    <meta property="article:tag" content="sistem online shop mobile"/>
    <meta property="article:tag" content="sistem online shop android"/>
    <meta property="article:tag" content="sistem online shop ios"/>
    <meta property="article:tag" content="sistem online shop indonesia"/>
    <meta property="article:tag" content="sistem online shop agen"/>
    <meta property="article:tag" content="sistem online shop grosir"/>
    <meta property="article:tag" content="sistem online shop umkm"/>
    <meta property="article:tag" content="sistem toko"/>
    <meta property="article:tag" content="sistem toko terbaik"/>
    <meta property="article:tag" content="sistem toko lengkap"/>
    <meta property="article:tag" content="sistem toko murah"/>
    <meta property="article:tag" content="sistem toko dropship"/>
    <meta property="article:tag" content="sistem toko reseller"/>
    <meta property="article:tag" content="sistem toko mobile"/>
    <meta property="article:tag" content="sistem toko android"/>
    <meta property="article:tag" content="sistem toko ios"/>
    <meta property="article:tag" content="sistem toko indonesia"/>
    <meta property="article:tag" content="sistem toko agen"/>
    <meta property="article:tag" content="sistem toko jual beli"/>
    <meta property="article:tag" content="sistem toko grosir"/>
    <meta property="article:tag" content="sistem toko umkm"/>
    <meta property="article:tag" content="sistem beli beli online"/>
    <meta property="article:tag" content="sistem pasar online"/>
    <meta property="article:tag" content="sistem bakul online"/>
    <meta property="article:tag" content="sistem jualan laris"/>
    <meta property="article:tag" content="sistem jual laris"/>
    <meta property="article:tag" content="sistem ecommerce"/>
    <meta property="article:tag" content="sistem ecommerce terbaik"/>
    <meta property="article:tag" content="sistem ecommerce indonesia"/>
    <meta property="article:tag" content="sistem ecommerce lengkap"/>
    <meta property="article:tag" content="sistem ecommerce murah"/>
    <meta property="article:tag" content="sistem ecommerce dropship"/>
    <meta property="article:tag" content="sistem ecommerce reseller"/>
    <meta property="article:tag" content="sistem ecommerce mobile"/>
    <meta property="article:tag" content="sistem ecommerce android"/>
    <meta property="article:tag" content="sistem ecommerce ios"/>
    <meta property="article:tag" content="sistem ecommerce agen"/>
    <meta property="article:tag" content="sistem ecommerce grosir"/>
    <meta property="article:tag" content="sistem ecommerce umkm"/>
    <meta property="article:tag" content="sistem reseller"/>
    <meta property="article:tag" content="sistem reseller lengkap"/>
    <meta property="article:tag" content="sistem reseller dropship"/>
    <meta property="article:tag" content="sistem reseller agen"/>
    <meta property="article:tag" content="sistem reseller mobile"/>
    <meta property="article:tag" content="sistem reseller android"/>
    <meta property="article:tag" content="sistem reseller ios"/>
    <meta property="article:tag" content="sistem reseller indonesia"/>
    <meta property="article:tag" content="sistem distributor indonesia"/>
    <meta property="article:tag" content="sistem distributor lengkap"/>
    <meta property="article:tag" content="sistem distributor dropship"/>
    <meta property="article:tag" content="sistem distributor agen"/>
    <meta property="article:tag" content="sistem distributor mobile"/>
    <meta property="article:tag" content="sistem distributor android"/>
    <meta property="article:tag" content="sistem distributor ios"/>
    <meta property="article:tag" content="sistem dropship indonesia"/>
    <meta property="article:tag" content="sistem dropship lengkap"/>
    <meta property="article:tag" content="sistem dropship dropship"/>
    <meta property="article:tag" content="sistem dropship agen"/>
    <meta property="article:tag" content="sistem dropship mobile"/>
    <meta property="article:tag" content="sistem dropship android"/>
    <meta property="article:tag" content="sistem dropship ios"/>
    <meta property="article:tag" content="sistem supplier"/>
    <meta property="article:tag" content="sistem supplier indonesia"/>
    <meta property="article:tag" content="sistem supplier lengkap"/>
    <meta property="article:tag" content="sistem supplier dropship"/>
    <meta property="article:tag" content="sistem supplier agen"/>
    <meta property="article:tag" content="sistem supplier mobile"/>
    <meta property="article:tag" content="sistem supplier android"/>
    <meta property="article:tag" content="sistem supplier ios"/>
    <meta property="article:tag" content="sistem bisnis online"/>
    <meta property="article:tag" content="sistem bisnis mudah"/>
    <meta property="article:tag" content="sistem bisnis murah"/>
    <meta property="article:tag" content="sistem bisnis digital"/>
    <meta property="article:tag" content="sistem bisnis lengkap"/>
    <meta property="article:tag" content="sistem digital marketing"/>
    <meta property="article:tag" content="sistem bisnis online shop"/>
    <meta property="article:tag" content="sistem bisnis ecommerce"/>
    <meta property="article:tag" content="sistem bisnis toko online"/>
    <meta property="article:tag" content="sistem akuntansi"/>
    <meta property="article:tag" content="sistem akuntansi online"/>
    <meta property="article:tag" content="sistem akuntansi online shop"/>
    <meta property="article:tag" content="sistem akuntansi ecommerce"/>
    <meta property="article:tag" content="sistem akuntansi toko online"/>
    <meta property="article:tag" content="sistem catat penjualan"/>
    <meta property="article:tag" content="sistem catat jualan online"/>
    <meta property="article:tag" content="sistem pencatatan jualan"/>
    <meta property="article:tag" content="sistem catat order"/>
    <meta property="article:tag" content="sistem jualan"/>
    <meta property="article:tag" content="sistem pergudangan"/>
    <meta property="article:tag" content="sistem catat stok"/>
    <meta property="article:tag" content="sistem manajemen stok"/>
    <meta property="article:tag" content="sistem agen reseller"/>
    <meta property="article:tag" content="sistem manajemen order"/>
    <meta property="article:tag" content="sistem manajemen pelanggan"/>
    <meta property="article:tag" content="sistem manajemen jualan"/>
    <meta property="article:tag" content="sistem manajemen reseller"/>
    <meta property="article:tag" content="sistem manajemen agen"/>
    <meta property="article:tag" content="sistem manajemen dropship"/>
    <meta property="article:tag" content="sistem manajemen pesanan"/>
    <meta property="article:tag" content="sistem jual beli umkm"/>
    <meta property="article:tag" content="software toko online"/>
    <meta property="article:tag" content="software toko online terbaik"/>
    <meta property="article:tag" content="software toko online lengkap"/>
    <meta property="article:tag" content="software toko online murah"/>
    <meta property="article:tag" content="software toko online dropship"/>
    <meta property="article:tag" content="software toko online reseller"/>
    <meta property="article:tag" content="software toko online mobile"/>
    <meta property="article:tag" content="software toko online android"/>
    <meta property="article:tag" content="software toko online ios"/>
    <meta property="article:tag" content="software toko online indonesia"/>
    <meta property="article:tag" content="software toko online agen"/>
    <meta property="article:tag" content="software toko online grosir"/>
    <meta property="article:tag" content="software toko online umkm"/>
    <meta property="article:tag" content="software jual beli online"/>
    <meta property="article:tag" content="software online shop"/>
    <meta property="article:tag" content="software online shop terbaik"/>
    <meta property="article:tag" content="software online shop lengkap"/>
    <meta property="article:tag" content="software online shop murah"/>
    <meta property="article:tag" content="software online shop dropship"/>
    <meta property="article:tag" content="software online shop reseller"/>
    <meta property="article:tag" content="software online shop mobile"/>
    <meta property="article:tag" content="software online shop android"/>
    <meta property="article:tag" content="software online shop ios"/>
    <meta property="article:tag" content="software online shop indonesia"/>
    <meta property="article:tag" content="software online shop agen"/>
    <meta property="article:tag" content="software online shop grosir"/>
    <meta property="article:tag" content="software online shop umkm"/>
    <meta property="article:tag" content="software toko"/>
    <meta property="article:tag" content="software toko terbaik"/>
    <meta property="article:tag" content="software toko lengkap"/>
    <meta property="article:tag" content="software toko murah"/>
    <meta property="article:tag" content="software toko dropship"/>
    <meta property="article:tag" content="software toko reseller"/>
    <meta property="article:tag" content="software toko mobile"/>
    <meta property="article:tag" content="software toko android"/>
    <meta property="article:tag" content="software toko ios"/>
    <meta property="article:tag" content="software toko indonesia"/>
    <meta property="article:tag" content="software toko agen"/>
    <meta property="article:tag" content="software toko jual beli"/>
    <meta property="article:tag" content="software toko grosir"/>
    <meta property="article:tag" content="software toko umkm"/>
    <meta property="article:tag" content="software beli beli online"/>
    <meta property="article:tag" content="software pasar online"/>
    <meta property="article:tag" content="software bakul online"/>
    <meta property="article:tag" content="software jualan laris"/>
    <meta property="article:tag" content="software jual laris"/>
    <meta property="article:tag" content="software ecommerce"/>
    <meta property="article:tag" content="software ecommerce terbaik"/>
    <meta property="article:tag" content="software ecommerce indonesia"/>
    <meta property="article:tag" content="software ecommerce lengkap"/>
    <meta property="article:tag" content="software ecommerce murah"/>
    <meta property="article:tag" content="software ecommerce dropship"/>
    <meta property="article:tag" content="software ecommerce reseller"/>
    <meta property="article:tag" content="software ecommerce mobile"/>
    <meta property="article:tag" content="software ecommerce android"/>
    <meta property="article:tag" content="software ecommerce ios"/>
    <meta property="article:tag" content="software ecommerce agen"/>
    <meta property="article:tag" content="software ecommerce grosir"/>
    <meta property="article:tag" content="software ecommerce umkm"/>
    <meta property="article:tag" content="software reseller"/>
    <meta property="article:tag" content="software reseller lengkap"/>
    <meta property="article:tag" content="software reseller dropship"/>
    <meta property="article:tag" content="software reseller agen"/>
    <meta property="article:tag" content="software reseller mobile"/>
    <meta property="article:tag" content="software reseller android"/>
    <meta property="article:tag" content="software reseller ios"/>
    <meta property="article:tag" content="software reseller indonesia"/>
    <meta property="article:tag" content="software distributor indonesia"/>
    <meta property="article:tag" content="software distributor lengkap"/>
    <meta property="article:tag" content="software distributor dropship"/>
    <meta property="article:tag" content="software distributor agen"/>
    <meta property="article:tag" content="software distributor mobile"/>
    <meta property="article:tag" content="software distributor android"/>
    <meta property="article:tag" content="software distributor ios"/>
    <meta property="article:tag" content="software dropship indonesia"/>
    <meta property="article:tag" content="software dropship lengkap"/>
    <meta property="article:tag" content="software dropship dropship"/>
    <meta property="article:tag" content="software dropship agen"/>
    <meta property="article:tag" content="software dropship mobile"/>
    <meta property="article:tag" content="software dropship android"/>
    <meta property="article:tag" content="software dropship ios"/>
    <meta property="article:tag" content="software supplier"/>
    <meta property="article:tag" content="software supplier indonesia"/>
    <meta property="article:tag" content="software supplier lengkap"/>
    <meta property="article:tag" content="software supplier dropship"/>
    <meta property="article:tag" content="software supplier agen"/>
    <meta property="article:tag" content="software supplier mobile"/>
    <meta property="article:tag" content="software supplier android"/>
    <meta property="article:tag" content="software supplier ios"/>
    <meta property="article:tag" content="software bisnis online"/>
    <meta property="article:tag" content="software bisnis mudah"/>
    <meta property="article:tag" content="software bisnis murah"/>
    <meta property="article:tag" content="software bisnis digital"/>
    <meta property="article:tag" content="software bisnis lengkap"/>
    <meta property="article:tag" content="software digital marketing"/>
    <meta property="article:tag" content="software bisnis online shop"/>
    <meta property="article:tag" content="software bisnis ecommerce"/>
    <meta property="article:tag" content="software bisnis toko online"/>
    <meta property="article:tag" content="software akuntansi"/>
    <meta property="article:tag" content="software akuntansi online"/>
    <meta property="article:tag" content="software akuntansi online shop"/>
    <meta property="article:tag" content="software akuntansi ecommerce"/>
    <meta property="article:tag" content="software akuntansi toko online"/>
    <meta property="article:tag" content="software catat penjualan"/>
    <meta property="article:tag" content="software catat jualan online"/>
    <meta property="article:tag" content="software pencatatan jualan"/>
    <meta property="article:tag" content="software catat order"/>
    <meta property="article:tag" content="software jualan"/>
    <meta property="article:tag" content="software pergudangan"/>
    <meta property="article:tag" content="software catat stok"/>
    <meta property="article:tag" content="software manajemen stok"/>
    <meta property="article:tag" content="software agen reseller"/>
    <meta property="article:tag" content="software manajemen order"/>
    <meta property="article:tag" content="software manajemen pelanggan"/>
    <meta property="article:tag" content="software manajemen jualan"/>
    <meta property="article:tag" content="software manajemen reseller"/>
    <meta property="article:tag" content="software manajemen agen"/>
    <meta property="article:tag" content="software manajemen dropship"/>
    <meta property="article:tag" content="software manajemen pesanan"/>
    <meta property="article:tag" content="software jual beli umkm"/>
    @foreach($tags as $item)
        <meta property="article:tag" content="{{$item->name}}"/>
    @endforeach
@endsection

@section('search')
    <div class="col-md-8">
        <ul class="list-group list-blog-category">
            <li class="list-group-item border-0 bg-transparent">
                <a href="{{route('rumah-senna')}}" class="blog-category-link arial-rounded active">Semua</a>
            </li>
            @foreach($category as $c)
            <li class="list-group-item border-0 bg-transparent">
                <a href="{{route('rumah-senna.show', ['slug' => $c->slug])}}" class="blog-category-link arial-rounded">{{ucwords($c->name)}}</a>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="col-md-4 mt-sm-28">
        <div class="blog-search-wrapper">
            <input id="keyword" type="text" class="form-control arial-rounded" name="keyword" placeholder="Cari inspirasi disini" autocomplete="off">
            <span class="input-group-btn rounded">
                <button class="btn blog-submit" onclick="search()">
                    <i class="fa fa-lg fa-search text-grey"></i>
                </button>
            </span>
        </div>
        <script>
            function search(){
                var keyword = document.getElementById('keyword').value;
                window.location.replace("{{route('rumah-senna.search')}}?keyword="+keyword);
            }

            var inputKeyword = document.getElementById('keyword');

            inputKeyword.addEventListener('keyup', (e) => {
                if(e.keyCode == '13'){
                    search();
                }
            });
        </script>
    </div>

@endsection

@section('content2')
    @if($data->count() > 0)
    <div class="col-xl-6 col-lg-12 mb-4">
        <h1 class="blog-first-title">
            <a href="{{route('rumah-senna.detail', [
                                    'year'=>$data->first()->created_at->year,
                                    'month'=>$data->first()->created_at->month,
                                    'day'=>$data->first()->created_at->day,
                                    'slug' => $data->first()->slug
                                    ])}}">
            {{$data->first()->title}}</a>
        </h1>
        
        <div class="blog-first-p">
            {!!$data->first()->content!!}
        </div>
    </div>
    <div class="col-xl-6 col-lg-12 mb-4">
        <img src="{{env('S3_URL').$data->first()->image_preview}}" alt="{{$data->first()->title}}" class="blog-first-thumbnail">
    </div>
    @endif
@endsection

@section('content3')

<div class="col-xl-8 col-lg-12 mb-3">
    <div class="row">
        @foreach ($data as $c)
        <div class="col-md-4 col-sm-6 mb-4">
            <a
                href="{{route('rumah-senna.detail', [
                                    'year'=>$c->created_at->year,
                                    'month'=>$c->created_at->month,
                                    'day'=>$c->created_at->day,
                                    'slug' => $c->slug
                                    ])}}"
            >
                <div class="list-card-blog">
                    <img class="list-card-blog__thumbnail" src="{{env('S3_URL').$c->image_preview}}" alt="{{$c->title}}">
                    <div class="px-2" style="height:180px;">
                        <a href="{{route('rumah-senna.detail', [
                                    'year'=>$c->created_at->year,
                                    'month'=>$c->created_at->month,
                                    'day'=>$c->created_at->day,
                                    'slug' => $c->slug
                                    ])}}"
                            class="blog-title-link"
                        >
                            <h5 class="mb-2 mt-3 blog-title">{{$c->title}}</h5>
                        </a>
                        <div class="list-blog-desc">
                            <p>{!! $c->content !!}</p>
                        </div>
                    </div>
                    <div class="px-2">
                        <small class="d-block font-weight-bold" style="color:#454545;">{{ucwords($c->getUser->fullname)}}</small>
                        <small>{{$c->created_at->isoFormat('dddd, D MMMM Y')}}</small>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>


@if($data->count() > 0)
<div class="col-xl-4 col-lg-12 mb-3">
    <div class="card shadow radius-20 border-0 px-2 py-2 mb-4" style="background-color:#FDF5EE;">
        <div class="card-body">
            <h4 class="mb-3">Rumah Senna Terbaru</h4>
            <ul class="list-group list-latest-blog">
                @foreach($content1 as $key => $item)
                <li class="list-group-item border-0 bg-transparent p-0 mb-3">
                    <a 
                        href="{{route('rumah-senna.detail', [
                                    'year'=>$item->created_at->year,
                                    'month'=>$item->created_at->month,
                                    'day'=>$item->created_at->day,
                                    'slug' => $item->slug
                                    ])}}" 
                        class="arial-rounded d-flex align-items-start"
                    >
                        <span class="mr-3 latest-blog-number">{{$key + 1}}</span>
                        <div>
                            <span class="latest-blog-title">{{$item->title}}</span>
                            <small style="color:#ADADAD;">{{$item->created_at->isoFormat('dddd, D MMMM Y')}}</small>
                        </div>
                    </a>
                    
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="card shadow radius-26 border-0 p-0 mb-4">
        <a href="https://play.google.com/store/apps/details?id=com.senna_store" rel="noreferrer" target="_blank">
            <img src="{{env('S3_URL')}}public/frontend/img/blog-side-banner.png" alt="blog banner" width="100%" class="img-fluid">
        </a>
    </div>
</div>
@endif

@if($data->count()<1)
    <div class="col-xl-12 mt-5 text-center">
        <div class="alert alert-danger" role="alert">
            Konten tidak ditemukan!
        </div>
    </div>
@endif


@endsection

@section('pagination')
    <div class="col-md-12 blog-pagination-wrapper">
        <div class="mt-5">{{ $data->links() }}</div>
    </div>
@endsection
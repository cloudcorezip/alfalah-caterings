@include('frontend.layout.partial.head')
@include('frontend.layout.partial.nav')
<section class="section-relative section-padding mt-120 mb-5"  style="background-color:#FDF5EE;">
    <div class="container-fluid py-3">
        <div class="row d-flex align-items-center">
            @yield('search')
        </div>
    </div>    
</section>
<section class="section-relative section-padding"  style="z-index: 100;">
    <div class="container-fluid">
        <div class="row mb-5">
            @yield('content2')
        </div>
        <div class="row">
            @yield('content3')
        </div>
        <div class="row justify-content-center text-center">
            @yield('pagination')
        </div>
    </div>
</section>

@include('frontend.layout.partial.footer')

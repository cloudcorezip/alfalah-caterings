@include('frontend.layout.partial.head')
@include('frontend.layout.partial.nav')
<section class="section-relative section-padding mt-120 mb-5"  style="background-color:#FDF5EE;">
    <div class="container-fluid py-3">
        <div class="row d-flex align-items-center">
            @yield('search')
        </div>
    </div>    
</section>
<section class="section-relative section-padding mb-5">
    <div class="container-fluid">
        <div class="row mt-5">
            @yield('content1')
            @yield('content2')
        </div>
    </div>
</section>
@include('frontend.layout.partial.footer')
@yield('js')
@if ($paginator->hasPages())
    <div class="row">
        <div class="col-md-6">
            @if($paginator->onFirstPage())
                <h6>Menampilkan 1  sampai {{$paginator->perPage()}} dari {{$paginator->total()}} entri</h6>
            @else
                @if($paginator->currentPage()!=$paginator->lastPage())
                    <h6>Menampilkan {{($paginator->perPage()*$paginator->currentPage())-$paginator->perPage()+1}}  sampai {{$paginator->perPage()*$paginator->currentPage()}} dari {{$paginator->total()}} entri</h6>
                 @else
                    <h6>Menampilkan {{($paginator->perPage()*$paginator->currentPage())-$paginator->perPage()+1}}  sampai {{$paginator->total()}} dari {{$paginator->total()}} entri</h6>
                @endif
            @endif
        </div>
        <div class="col-md-6">
            <nav>
                <ul class="pagination" style="justify-content: right">
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                            <span class="page-link" aria-hidden="true">&lsaquo;</span>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                        </li>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                                @else
                                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <li class="page-item">
                            <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                        </li>
                    @else
                        <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                            <span class="page-link" aria-hidden="true">&rsaquo;</span>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

@endif

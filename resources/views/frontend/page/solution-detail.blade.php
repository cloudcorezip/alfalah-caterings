@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('features')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('features')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
@endsection
@section('content')
    @php
        $data = [
            "laundry" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Laundrymu",
                "list" =>[
                    "Monitor status sesanan",
                    "Pembayaran QRIS",
                    "Membership & loyalti pelanggan",
                    "Manajemen aset usaha (mesin cuci, setrika, dll)"
                ],
                "img" => "public/frontend/img/tb-laundry.png"
            ],
            "fnb" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha F & B mu",
                "list" =>[
                    "Tambah produk berdasarkan kategori",
                    "Kelola harga modal dan harga jual",
                    "Mendukung pembayaran non-tunai",
                    "Laporan penjualan dengan penerimaan, biaya, dan laba kotor"
                ],
                "img"=>"public/frontend/img/tb-fnb.png"
            ],
            "barbershop" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Salon dan Barbershopmu",
                "list" =>[
                    "Data produk jasa yang ditawarkan",
                    "Kelola aset usaha (alat cukur rambut, dll)",
                    "Pantau absensi karyawan",
                    "Laporan detail transaksi dan karyawan"
                ],
                "img" => "public/frontend/img/tb-barbershop.png"
            ],
            "fashion" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Fashion dan Accesoriesmu",
                "list" =>[
                    "Simpan data produk dalam jumlah besar",
                    "Multi varian dan harga setiap produk",
                    "Diskon & promo produk",
                    "Temukan barang yang paling laris"
                ],
                "img" => "public/frontend/img/tb-fashion.png"
            ],
            "retail" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Retailmu",
                "list" =>[
                    "Jumlah produk dan transaksi tidak terbatas",
                    "Hitung HPP, multi satuan, multi varian, dan scan barcode",
                    "Mutasi stok antar cabang & gudang",
                    "Notifikasi stok habis"
                ],
                "img" => "public/frontend/img/tb-retail.png"
            ],
            "elektronik" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Elektronikmu",
                "list" =>[
                    "Jumlah produk dan transaksi tidak terbatas",
                    "Hitung HPP, multi satuan, multi varian, dan scan barcode",
                    "Mutasi stok antar cabang & gudang",
                    "Notifikasi stok habis"
                ],
                "img" => "public/frontend/img/tb-elektronik.png"
            ],
            "cafe" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Cafe dan Restoranmu",
                "list" =>[
                    "Tambah produk berdasarkan kategori",
                    "Kelola harga modal dan harga jual",
                    "Mendukung pembayaran non-tunai",
                    "Laporan penjualan dengan penerimaan, biaya, dan laba kotor"
                ],
                "img" => "public/frontend/img/tb-restoran.png"
            ],
            "restoran" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Restoranmu",
                "list" =>[
                    "Tambah produk berdasarkan kategori",
                    "Kelola harga modal dan harga jual",
                    "Mendukung pembayaran non-tunai",
                    "Laporan penjualan dengan penerimaan, biaya, dan laba kotor"
                ],
                "img" => "public/frontend/img/tb-restoran.png"
            ],
            "coffee-shop" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Coffe Shopmu",
                "list" =>[
                    "Tambah produk berdasarkan kategori",
                    "Kelola harga modal dan harga jual",
                    "Mendukung pembayaran non-tunai",
                    "Laporan penjualan dengan penerimaan, biaya, dan laba kotor"
                ]
            ],
            "souvenir" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Souvenirmu",
                "list" =>[
                    "Tambah produk berdasarkan kategori",
                    "Kelola harga modal dan harga jual",
                    "Mendukung pembayaran non-tunai",
                    "Laporan penjualan dengan penerimaan, biaya, dan laba kotor"
                ]
            ],
            "alat-tulis" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Alat Tulismu",
                "list" =>[
                    "Tambah produk berdasarkan kategori",
                    "Kelola harga modal dan harga jual",
                    "Mendukung pembayaran non-tunai",
                    "Laporan penjualan dengan penerimaan, biaya, dan laba kotor"
                ]
            ],
            "otomotif" => [
                "title" => "Yuk Tingkatkan Keuntungan Usaha Otomotifmu",
                "list" =>[
                    "Lihat ketersediaan spare part",
                    "Laporan pembelian stok dan mitra",
                    "Pantau absensi karyawan secara online",
                    "Catat aset usaha yang mengalami penyusutan"
                ],
                "img" => "public/frontend/img/tb-otomotif.png"
            ]
        ];
    @endphp

    @if(!array_key_exists($page, $data))
        @php
            $page = "retail";
        @endphp
        <script>
            window.location.replace("{{route('solution.detail', ['page'=>$page])}}");
        </script>
    @endif
    <section class="section-relative mt-section-header mb-section">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-left.png" alt="background left" class="bg-top__left">
        <div class="container-fluid z-10 section-header-padding">
            <div class="row d-flex align-items-center">
                <div class="col-md-6 pt-5 text-center-main-sm">
                    <h1 class="main-title">
                        {{$data[$page]['title']}}<br><span class="main-title__in with-border">Sekarang!</span>
                    </h1>
                </div>
                <div class="col-md-6 text-center">
                    <img src="{{env('S3_URL')}}public/frontend/img/detail-tipe-bisnis.png" alt="hero image" width="95%" class="img-fluid">
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted.png" alt="background dotted" class="bg-bottom__center">
    </section>
    
    <section class="section-relative mb-120">
        <div class="container-fluid z-10 position-relative section-padding">
            <div class="row d-flex align-items-center">
                <div class="col-md-6">
                    <div class="position-relative text-center" style="position:relative; width:80%;">
                        <img style="position:absolute;top:-20%;left:0;z-index:10;" src="{{asset('public/frontend/img/solution-detail-foreground.png')}}" alt="foreground" class="img-fluid" width="100%;">
                        <img src="{{env('S3_URL')}}{{$data[$page]['img']}}" alt="image business" class="img-fluid mb-4" width="80%">
                    </div>
                </div>
                <div class="col-md-6">
                    <p class="mb-4">Dapatkan kemudahan terhubung dengan data transaksi lebih akurat dan real time, untuk bisa langsung dianalisa sehingga menjadi solusi bisnis yang tepat.</p>
                    <ul class="list-group">
                        @foreach($data[$page]["list"] as $item)
                        <li class="list-group-item d-flex align-items-start p-0 border-0" style="background:transparent;">
                            <div class="text-featured">
                                <i class="fa fa-check-circle font-orange"></i>
                            </div>
                            <div class="ml-2">
                                <span>{{$item}}</span>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted.png" alt="background dotted" class="bg-bottom__center">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-right.png" alt="background icon" class="bg-middle__right">
    </section>
    
@endsection

@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('contact')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('contact')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />

@endsection
@section('content')    
    <section class="section-relative mb-5 mt-section-header">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-left.png" alt="background left" class="bg-top__left">
        <div class="container-fluid z-10">
            <div class="row">
                <div class="col-md-12 text-center pt-5">
                    <h1 class="main-title">
                        <span class="main-title__in with-border__top">Teman Terbaik</span><br>untuk Melangkah ke Depan</h1>
                </div>
            </div>
        </div>
    </section>
    
    <section class="section-relative mb-120 section-padding">
        <div class="container-fluid position-relative z-10">
            <div class="row d-flex align-items-center">
                <div class="col-md-6 text-center mb-3">
                    <img src="{{env('S3_URL')}}public/frontend/img/about-ilust-1.png" alt="image background" class="img-fluid" width="80%">
                </div>
                <div class="col-md-6 mb-3">
                    <h2 class="mb-4">Usaha yang Maju Diawali oleh Langkah Kecil Pengusaha</h2>
                    <p>Langkah kecil yang dilakukan terus menerus oleh UMKM dengan mengelola usaha secara profesional dan keberanian untuk berinovasi akan membawa dampak yang lebih besar.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="section-relative mb-120 section-padding">
        <div class="container-fluid position-relative z-10">
            <div class="row d-flex align-items-center">
                <div class="col-md-6 mb-3">
                    <h2 class="mb-4">Memanfaatkan Teknologi untuk Tumbuh Lebih Pesat</h2>
                    <p>Perkembangan teknologi yang pesat memberikan kemudahan untuk UMKM berkembang lebih jauh tanpa batas</p>
                </div>
                <div class="col-md-6 text-center mb-3">
                    <img src="{{env('S3_URL')}}public/frontend/img/about-ilust-2.png" alt="image background" class="img-fluid" width="80%">
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-right.png" alt="background icon" class="bg-middle__right">
    </section>

    <section class="section-relative mb-120 z-10">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center mb-5">
                    <h3 class="to-h2">Partner Kami</h3>
                </div>
                <div class="col-12 mb-5">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="row d-flex align-items-center">
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/grab-logo.png" alt="grab logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/xendit-logo.png" alt="xendit logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/winpay-logo.png" alt="winpay logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/rajabiller-logo.png" alt="rajabiller logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/lazada-logo.png" alt="lazada logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/tokopedia-logo.png" alt="tokopedia logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/shopee-logo.png" alt="shopee logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/bukalapak-logo.png" alt="bukalapak logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/jdid-logo.png" alt="jdid logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/blibli-logo.png" alt="blibli logo" class="img-fluid partner-logo">
                                </div>
                                <div class="col-md-3 col-6 mb-2">
                                    <img src="{{env('S3_URL')}}public/frontend/img/koinwork-logo.png" alt="koinwork logo" class="img-fluid partner-logo">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('package')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('package')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
@endsection
@section('content')
    <section class="section-relative mb-120 mt-section-header">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-left.png" alt="background left" class="bg-top__left">
        <div class="container-fluid position-relative z-10">
            <div class="row">
                <div class="col-md-12 text-center pt-5">
                    <h1>Solusi Aplikasi Lengkap<br>tanpa biaya Tambahan di<br><span class="main-title__in with-border">Senna</span></h1>
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted.png" alt="background dotted" class="bg-bottom__center">
    </section>
    <section class="section-relative mb-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt-3">
                    <div class="card paket-1 mb-sm-50 mx-auto h-100" style="border-radius: 25px;border: 0;width:85%;">
                        <div class="card-header text-center text-light pt-5 px-0" style="border-radius: 25px 25px 0px 0px;border:none;">
                            <div>
                                <h2 class="paket-name font-weight-bold" style="color:#333333;">NAKULA</h2>
                                <h2 class="mt-2 mb-2" style="color:#a4a4a4;font-size:14pt">
                                    <s>Rp. 150rb</s>
                                </h2>
                                <h2 class="main-price mt-2 mb-2 font-weight-bold"><small style="color:#454545;font-size:12pt;">Rp.</small> 99rb</h2>
                                
                                <small class="d-block" style="color:#000000;">per cabang/bulan</small>
                                {{--                                  <div class="px-5">--}}
                                {{--                                      <hr>--}}
                                {{--                                  </div>--}}
                                {{--                                  <a class="paket-collapse arial-rounded" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1">--}}
                                {{--                                    Lihat Harga Paket Lainnya <i class="fa fa-chevron-down"></i>--}}
                                {{--                                  </a>--}}
                                {{--                                  <div class="collapse mx-auto px-2 py-3 radius-20 bg-white width-80" id="collapse1">--}}
                                {{--                                    <h3 class="to-h6 mt-2 mb-2" style="color:#333333;">Rp. 109rb</h3>--}}
                                {{--                                    <small class="d-block" style="color:#000000;">per cabang/bulan - dibayar 6 Bulan</small>--}}
                                {{--                                    <div class="px-5">--}}
                                {{--                                        <hr>--}}
                                {{--                                    </div>--}}
                                {{--                                    <h3 class="to-h6 mt-2 mb-2" style="color:#333333;">Rp. 129rb</h3>--}}
                                {{--                                    <small class="d-block" style="color:#000000;">per cabang/bulan - dibayar 3 Bulan</small>--}}
                                {{--                                  </div>--}}
                            </div>
                        </div>
                        <div class="card-body px-5">
                            <ul class="list-group">
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span class="poppin" style="font-size:10pt">
                                            <b>Kelola maksimal <strong>1</strong> cabang</b>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Aplikasi Kasir Online</b>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Manajemen Persediaan</b>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Manajemen Karyawan</b>
                                        </span>
                                        <br>
                                        <small>Maksimal 5 Karyawan</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Laporan Lengkap</b>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Akuntansi</b>
                                        </span>
                                        <br>
                                        <small>Jurnal, Pengaturan Akun, Laba / Rugi</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>E-Wallet(QRIS)</b>
                                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="card-footer">
                            <a href="{{url('https://play.google.com/store/apps/details?id=com.senna_store')}}" target="_blank" rel="noreferrer"
                               class="btn btn-paket btn-rounded btn-block text-center arial-rounded">Pilih Paket</a>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt-3">
                    <div class="card paket-2 mb-sm-50 mx-auto h-100" style="border-radius: 25px;border: 0;width:85%;">
                        <img class="badge-package" src="{{env('S3_URL')}}public/frontend/img/badge-populer.png" alt="badge">
                        <div class="card-header text-center text-light pt-5 px-0">
                            <div>
                                <h2 class="paket-name font-weight-bold" style="color:#333333;">SADEWA</h2>
                                <h2 class="mt-2 mb-2" style="color:#a4a4a4;font-size:14pt">
                                    <s>Rp. 249rb</s>
                                </h2>
                                <h2 class="main-price mt-2 mb-2 font-weight-bold"><small style="color:#000000;font-size:12pt;">Rp.</small> 189rb</h2>
                                <small class="d-block" style="color:#000000;">per cabang/bulan</small>
                                {{--                                  <div class="px-5">--}}
                                {{--                                      <hr>--}}
                                {{--                                  </div>--}}
                                {{--                                  <a class="paket-collapse arial-rounded" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse1">--}}
                                {{--                                    Lihat Harga Paket Lainnya <i class="fa fa-chevron-down"></i>--}}
                                {{--                                  </a>--}}

                                {{--                                  <div class="collapse mx-auto px-2 py-3 radius-20 bg-white width-80" id="collapse2">--}}
                                {{--                                    <h3 class="to-h6 mt-2 mb-2" style="color:#333333;">Rp. 199rb</h3>--}}
                                {{--                                    <small class="d-block" style="color:#000000;">per cabang/bulan - dibayar 6 Bulan</small>--}}
                                {{--                                    <div class="px-5">--}}
                                {{--                                        <hr>--}}
                                {{--                                    </div>--}}
                                {{--                                    <h3 class="to-h6 mt-2 mb-2" style="color:#333333;">Rp. 219rb</h3>--}}
                                {{--                                    <small class="d-block" style="color:#000000;">per cabang/bulan - dibayar 3 Bulan</small>--}}
                                {{--                                  </div>--}}
                            </div>
                        </div>
                        <div class="card-body px-5">
                            <ul class="list-group">
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>Kelola maksimal 3 cabang</b>
                                            </span>
                                    </div>
                                </li>

                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>Aplikasi Kasir Online</b>
                                            </span>
                                        <br>
                                        <small>Bahan Baku, Multi Varian</small>
                                    </div>
                                </li>

                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>Manajemen Persediaan</b>
                                            </span>
                                        <br>
                                        <small>HPP, COGS, dan Average</small>
                                        <br>
                                        <small>Mutasi Stok</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>Manajemen Karyawan</b>
                                            </span>
                                        <br>
                                        <small>Maksimal 10 Karyawan</small>
                                        <br>
                                        <small>Absensi Karyawan</small>
                                    </div>
                                </li>

                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>Laporan Lengkap</b>
                                            </span>
                                        <br>
                                        <small>Analisa Bisnis</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>Akuntansi</b>
                                            </span>
                                        <br>
                                        <small class="d-block">Jurnal, Pengaturan Akun, Laba / Rugi, Laporan Keuangan</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>E-Wallet(QRIS)</b>
                                            </span>
                                    </div>
                                </li>

                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>Struk Digital</b>
                                            </span>
                                    </div>
                                </li>

                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                            <span style="font-size:10pt">
                                                <b>Pengelolaan Aset</b>
                                            </span>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="card-footer">
                            <a href="{{url('https://play.google.com/store/apps/details?id=com.senna_store')}}" target="_blank" rel="noreferrer" class="btn btn-paket btn-block text-center arial-rounded">Pilih Paket</a>
                        </div>

                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 col-12 mt-3">
                    <div class="card paket-3 mb-sm-50 mx-auto h-100" style="border-radius: 25px;border: 0;width:85%;">
                        <div class="card-header text-center text-light pt-5 px-0">
                            <div>
                                <h2 class="paket-name font-weight-bold">ANTASENA</h2>
                                <h2 class="main-price mt-2 mb-2 font-weight-bold"><small style="color:#000000;font-size:12pt;">Rp.</small> 300rb</h2>
                                <small class="d-block" style="color:#000000;">per cabang/bulan</small>
                                {{--                                  <div class="px-5">--}}
                                {{--                                      <hr>--}}
                                {{--                                  </div>--}}

                                {{--                                  <a class="paket-collapse arial-rounded" data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse1">--}}
                                {{--                                    Lihat Harga Paket Lainnya <i class="fa fa-chevron-down"></i>--}}
                                {{--                                  </a>--}}
                                {{--                                  <div class="collapse mx-auto px-2 py-3 radius-20 bg-white width-80" id="collapse3">--}}
                                {{--                                    <h3 class="to-h6 mt-2 mb-2" style="color:#333333;">Rp. 249rb</h3>--}}
                                {{--                                    <small class="d-block" style="color:#000000;">per cabang/bulan - dibayar 6 Bulan</small>--}}
                                {{--                                    <div class="px-5">--}}
                                {{--                                        <hr>--}}
                                {{--                                    </div>--}}
                                {{--                                    <h3 class="to-h6 mt-2 mb-2" style="color:#333333;">Rp. 269rb</h3>--}}
                                {{--                                    <small class="d-block" style="color:#000000;">per cabang/bulan - dibayar 3 Bulan</small>--}}
                                {{--                                  </div>--}}
                            </div>
                        </div>
                        <div class="card-body px-5">
                            <ul class="list-group">
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Kelola cabang tanpa batasan</b>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Aplikasi Kasir Online</b>
                                        </span>
                                        <br>
                                        <small>Bahan Baku, Multi Varian, Multi Satuan</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Manajemen Persediaan</b>
                                        </span>
                                        <br>
                                        <small>HPP, COGS, dan Average</small>
                                        <br>
                                        <small>Mutasi Stok, Multi Gudang</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Manajemen Karyawan</b>
                                        </span>
                                        <br>
                                        <small>Unlimited Karyawan</small>
                                        <br>
                                        <small>Absensi Karyawan</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Laporan Lengkap</b>
                                        </span>
                                        <br>
                                        <small>Analisa Bisnis</small>
                                        <br>
                                        <small>Laporan Kinerja Karyawan</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Akuntansi</b>
                                        </span>
                                        <br>
                                        <small>Akuntansi Lengkap</small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>E-Wallet(QRIS)</b>
                                        </span>
                                    </div>
                                </li>

                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Struk Digital</b>
                                        </span>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span style="font-size:10pt">
                                            <b>Pengelolaan Aset</b>
                                        </span>
                                    </div>
                                </li>

                                <li class="list-group-item d-flex align-items-start p-0">
                                    <div class="text-featured">
                                        <i class="fa fa-check-circle"></i>
                                    </div>
                                    <div class="ml-2">
                                        <span class="poppin d-block" style="font-size:10pt">
                                            <b>E-Katalog</b>
                                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="card-footer">
                            <a href="{{url('https://play.google.com/store/apps/details?id=com.senna_store')}}" target="_blank" rel="noreferrer" class="btn btn-paket btn-block text-center arial-rounded">Pilih Paket</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="section-relative mb-120">
        <div class="container">
            <div class="row mt-5 package-header">
                <div class="col-3 text-center" data-aos="fade-down">
                    <h2 class="to-h4 mb-4">Paket Merchant</h2>
                </div>
                <div class="col-3 text-center" data-aos="fade-right">
                    <h2 class="to-h4 mb-4">NAKULA</h2>
                </div>
                <div class="col-3 text-center" data-aos="fade-down">
                    <h2 class="to-h4 mb-4">SADEWA</h2>
                </div>
                <div class="col-3 text-center" data-aos="fade-left">
                    <h2 class="to-h4 mb-4">ANTASENA</h2>
                </div>
            </div>

            <div>
                <div class="row mt-4">
                    <div class="col-12 p-3 rounded" style="background-color: #F6F6F6;">
                        <h3 class="align-middle h5">Aplikasi Kasir Online</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Jumlah Cabang</p>
                    </div>
                    <div class="col-3 text-center">
                        <p class="text-muted my-0">1</p>
                    </div>
                    <div class="col-3 text-center">
                        <p class="text-muted my-0">3</p>
                    </div>
                    <div class="col-3 text-center">
                        <p class="text-muted my-0">Unlimited</p>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Login Karyawan (Maksimal)</p>
                    </div>
                    <div class="col-3 text-center">
                        <p class="text-muted my-0">5</p>
                    </div>
                    <div class="col-3 text-center">
                        <p class="text-muted my-0">10</p>
                    </div>
                    <div class="col-3 text-center">
                        <p class="text-muted my-0">Unlimited</p>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Import Barang Melalui Excel</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Tipe Harga Barang</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Notifikasi Stok Barang Habis / Menipis</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Pembelian Barang dari Supplier</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Bahan Baku, Multi Varian, Multi Satuan</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Manajemen Hutang/Piutang</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Manajemen Diskon dan Pajak</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Manajemen Pelanggan dan Supplier</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 p-3 rounded" style="background-color: #F6F6F6;">
                        <h3 class="align-middle h5">Transaksi</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Struk Digital</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Mencatat Pembayaran Tunai dan Non Tunai</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Mencatat Berbagai Jenis Order (Bungkus, Delivery, Table, dll)</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 p-3 rounded" style="background-color:#F6F6F6;">
                        <h3 class="align-middle h5">Laporan</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Umum</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Katalog Online (Cariapa)</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan lain-lain</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Semua Transaksi</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Penjualan</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Pembelian</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Modal</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Pengunjung</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Melalui Back Office (Web)</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Kinerja Karyawan</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Stok</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Log Perubahan Stok Barang</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 p-3 rounded" style="background-color: #F6F6F6;">
                        <h3 class="align-middle h5">Persediaan</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Mencatat Stok, Harga Modal, dan Harga Jual</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Mencatat HPP, COGS, dan Average</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Mutasi Stok</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Multi Gudang</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Stok Opname</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 p-3 rounded" style="background-color: #F6F6F6;">
                        <h3 class="align-middle h5">Manajemen Karyawan</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Manajemen Shift</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Absensi Digital</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Otorisasi dan Akses Karyawan</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Shift</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laporan Absensi</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 p-3 rounded" style="background-color: #F6F6F6;">
                        <h3 class="align-middle h5">Akuntansi</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p-3 rounded" style="background-color: #ffddab;">
                        <h3 class="align-middle h6">Pencatatan Jurnal dan Biaya</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Kas & Bank</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Pengeluaran Administrasi & Umum</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Jurnal Umum</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Pendapatan di Luar Usaha</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Utang Usaha</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Piutang Usaha</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 p-3 rounded" style="background-color: #ffddab;">
                        <h3 class="align-middle h6">Pengaturan Akun</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Format Arus Kas</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Kategori Akun</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Pemetaan Akun</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Periode Akuntansi</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Daftar Akun</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 p-3 rounded" style="background-color: #ffddab;">
                        <h3 class="align-middle h6">Laporan Keuangan</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Arus Kas</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Buku Besar</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Jurnal</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Neraca Saldo</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Posisi Keuangan</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Laba / Rugi</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 p-3 rounded" style="background-color: #F6F6F6;">
                        <h3 class="align-middle h5">Lain Lain</h3>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Pemindai <i>Barcode</i></p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Generate <i>Barcode</i> Produk Untuk Penjualan</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Sinkronisasi dan <i>BackUp</i> data ke <i>Cloud System</i></p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Filter <i>Persediaan</i> Berdasarkan Waktu</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-muted my-0">Syarat Kredit Ke Bank</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-check-circle text-success"></i>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-3">
                        <p class="text-dark font-weight-bold my-0">Bonus Langganan</p>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <i class="fa-lg fa fa-remove text-danger"></i>
                    </div>
                    <div class="col-3 text-center">
                        <button class="btn btn-warning" style="pointer-events:none; border-radius:10px;">
                            Extra (+) 1 Bulan
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="Privasi Kebijakan Penggunaan Aplikasi Senna">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="Privasi Kebijakan Penggunaan Aplikasi Senna">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="Privasi kebijakan penggunaan aplikasi Senna" />
    <meta name="twitter:url" content="{{route('policy')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('policy')}}" />
    <meta property="og:description" content="Privasi Kebijakan Penggunaan Aplikasi Senna" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
@endsection
@section('content')

    <section class="section-relative mt-section-header mb-5">
        <div class="container">
            <div class="col-md-12">
                <h1 class="pt-5 mb-3">Senna Privacy & Policy</h1>
                <span class="small text-senna poppin"><i>Updated at 12 November 2020</i> </span>
                <p class="poppin text-justify">
                    We would like to explain Privacy Policy and Security regarding user data collection, usage, and disclosure to PT. Senna Kreasi Nusa for Senna (or mentioned as Application later).  This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Senna (or mentioned as Application later) unless otherwise defined in this Privacy Policy.
                </p>
            </div>
            <div class="col-md-12 mt-5">
                <h3 class="text-senna poppin-med mt-5">Information Collection and Use</h3>
                <p class="poppin text-justify">
                    For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to ID Card, Phone Number, Email Address, Fullname. The information that we request will be retained by us and used as described in this privacy policy.
                    <br>
                    <br> &emsp;&emsp;
                     The app does use third party services that may collect information used to identify you.Link to privacy policy of third party service providers used by the app:
<ol class="poppin" type="a" style="padding-left: 10%">
                    <li><a href="https://policies.google.com/terms" target="_blank" rel="noopener noreferrer">Google Play Services</a></li>
                <!---->
                <li><a href="https://firebase.google.com/terms/analytics" target="_blank" rel="noopener noreferrer">Google Analytics for Firebase</a></li>
                <li><a href="https://firebase.google.com/terms/crashlytics" target="_blank" rel="noopener noreferrer">Firebase Crashlytics</a></li>
                </ol>
                </p>

                <h3 class="text-senna poppin-med mt-5">Log Data</h3>
                <p class="poppin text-justify">
                We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.
                </p>


                <h3 class="text-senna poppin-med mt-5">Children’s Privacy</h3>
                <p class="poppin text-justify">
                These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.
                </p>

                <h3 class="text-senna poppin mt-5">Security</h3>
                <p class="poppin text-justify">
                The user data will be stored in our server. We use a backup system periodically, encrypted, and distributed to a different data storage to minimize the errors and risks from our server. We do not sell and distribute the user data to anyone or any parties because we aware of the importance of privacy.
                </p>
                <p class="poppin text-justify">
                In our system, we use multiple security methods from various sies (i.e: application, database, network, and server). The application is also supported by the developer team who have experiences in the field of data security, to ensure the user data safety from data exploitation. The application implements anti brute-force, anti SQL injection, anti remote-injection, system log monitoring user activity, and others.
                </p>
                <p class="poppin text-justify">
                The application is also equipped with a system that detects spam, prevent BOTs-made transactions, and fraud detection with artificial intelligence attachment in the system to detect any interference from outside parties. The application network, we use SSL 256 bit encryption to secure user data from data tapping and manipulation that used MITM mode. Otherwise, user system that uses SSL will be detected as a safe system for all type of browser so it could avoid any ad or script injection that have the possibility to harm the user’s system and also improve user system reputation.
                </p>
                <p class="poppin text-justify">
                The application database implements automatic backup data for every hour and distributes to 4 different storages. Minimizing the risk from hacking or data exploitation, the transaction database is separated from the general database. The application server uses anti-DDOS to secure your system from an unwanted party that possibly distress user system stability.
                </p>
            </div>
            <div class="col-md-12 mt-5">
                <h3 class="text-senna poppin-med">Cookie(s)</h3>
                <p class="poppin text-justify">
                 A cookie is a small file data that place in user’s device and browser. By using the cookie, the feature of application and site that the user access could save user information or remember any actions and preferences from time to time. Most of browser support cookie; but user could adjust the browser to reject some or specific type of cookie. Besides, the user can clear cookie in any time they wish. We use cookie for may purpose, for instance, to remember the user’s preference safe browsing, help the user to access or manage a particular service, and protects user data.
                </p>
            </div>

            <div class="col-md-12 mt-5">
                <h3 class="text-senna poppin-med">Change in Privacy Policy</h3>
                <p class="poppin text-justify">
                 We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.This policy is effective as of 2020-11-12
                </p>
            </div>
           <!--  <div class="col-md-12 mt-5">
                <h3 class="text-senna poppin">Disclaimer</h3>
                <p class="poppin text-justify">
                The site and application are the rightful property of PT. Senna Kreasi Nusa. All the information contained inside the application/site is correctly input. PT. Senna Kreasi Nusa has the full right to load, not to load, edit and/or delete the user information at any time needed. Due to that terms, we urge the user to always update the application and check the site periodically to know the latest update on the application.
                </p>
                <p class="poppin text-justify">
                PT. Senna Kreasi Nusa has no obligation upon non-delivery data/information to the user through every mean of communication due to the unpredicted technical error.
                </p>
                <p class="poppin text-justify">
                If there is any user who responded to the information contained in this application/site, but not limited to question, comment, suggestion, and others, are not considered as privacy and PT. Senna Kreasi Nusa has no rights related to those response. PT. Senna Kreasi Nusa has the capability to produce, use, announce, or share the response without any limitations.
                </p>

            </div> -->
            <div class="col-md-12 mt-5">
                <h3 class="text-senna poppin-med">Contact Us</h3>
                <p class="poppin text-justify">

                <li>Office	: Jl. Sekarsuli-Berbah No. 9 Potorono, Banguntapan, Bantul, D.I.Y </li>
                <li>Phone	: +62812-9619-9565, 62813-2566-6958</li>
                <li>E-mail	: contact@senna.co.id</li>

                </p>


            </div>
        </div>
    </section>

    <section class="section bg-dark foot-term" style="" >
        <img src="{{env('S3_URL')}}public/frontend/img/c.jpg" class="bg-f" alt="background">
        <div class="container">
            <div class="row" style="margin-bottom:150px;">
                <div class="col-md-6">
                    <img src="{{asset('public/frontend/img/hand-and-phone.png')}}" class="img-fluid" width="100%" alt="phone image">
                </div>
                <div class="col-md-6 my-auto centered">
                    <h2 class="text-light">Download App <br> Senna Now!</h2><br>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-6">
                            <a href="https://play.google.com/store/apps/details?id=com.senna_store" target="_blank" rel="noreferrer">
                                <img src="{{asset('public/frontend/img/playstore.png')}}" class="img-fluid" width="80%" alt="playstore logo">
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <a href="https://apps.apple.com/us/app/senna-kasir/id1573939224" target="_blank" rel="noreferrer">
                                <img src="{{asset('public/frontend/img/appstore.png')}}" class="img-fluid" width="80%" alt="appstore logo">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

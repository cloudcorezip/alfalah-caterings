@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('features')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('features')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
@endsection
@section('content')
    <section class="section-relative mt-section-header">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-left.png" alt="background left" class="bg-top__left">
        <div class="container-fluid z-10 section-header-padding position-relative">
            <div class="row d-flex align-items-center">
                <div class="col-lg-6 col-md-12 pt-5 text-center-main-sm">
                    <h1 class="main-title">
                        <span class="main-title__in with-border__top">Aplikasi Bisnis</span> untuk<br>seluruh bidang usaha
                    </h1>
                    <p class="main-p">Kemudahan pengelolaan usaha dengan sistem aplikasi POS dan Akuntansi Terintegrasi terlengkap untuk seluruh bidang usaha.</p>
                </div>
                <div class="col-lg-6 col-md-12 text-center">
                    <img src="{{env('S3_URL')}}public/frontend/img/hero-solution.png" alt="Aplikasi Bisnis untuk seluruh bidang usaha" width="95%" class="img-fluid">
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted.png" alt="background dotted" class="bg-bottom__center">
    </section>

    <section class="section-relative section-padding">
        <div class="container z-10 position-relative">
            <div class="row mt-5">
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img src="{{env('S3_URL')}}public/frontend/img/tb-retail.png" alt="Toko Retail" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'retail'])}}">Toko Retail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img src="{{env('S3_URL')}}public/frontend/img/tb-restoran.png" alt="Cafe Restoran" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'cafe'])}}">Cafe & Restoran</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img src="{{env('S3_URL')}}public/frontend/img/tb-fnb.png" alt="f n b" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'fnb'])}}">F & B</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img src="{{env('S3_URL')}}public/frontend/img/tb-elektronik.png" alt="Elektronik" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'elektronik'])}}">Elektronik</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img src="{{env('S3_URL')}}public/frontend/img/tb-fashion.png" alt="Fashion Accesories" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'fashion'])}}">Fashion & Accesories</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img src="{{env('S3_URL')}}public/frontend/img/tb-otomotif.png" alt="Otomotif" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'otomotif'])}}">Otomotif</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img src="{{env('S3_URL')}}public/frontend/img/tb-laundry.png" alt="Laundry" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'laundry'])}}">Laundry</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img src="{{env('S3_URL')}}public/frontend/img/tb-barbershop.png" alt="Salon Barbershop" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'barbershop'])}}">Salon & Barbershop</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-bottom-rounded.png" alt="background" class="bg-top__rounded" style="top:-100px;">
    </section>

@endsection

@extends('frontend.layout.main')

@section('meta')
<meta name="description" content="Privasi Kebijakan Penggunaan Aplikasi Senna">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="Privasi Kebijakan Penggunaan Aplikasi Senna">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="Privasi kebijakan penggunaan aplikasi Senna" />
    <meta name="twitter:url" content="{{route('policy')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('policy')}}" />
    <meta property="og:description" content="Privasi Kebijakan Penggunaan Aplikasi Senna" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />

@endsection

@section('content')

<section class="section-relative mt-section-header mb-5">
    <div class="container">
        <div class="col-md-12">
            <h1 class="pt-5 mb-3">Kebijakan Privasi dan Keamanan Senna</h1>
            <span class="small text-senna poppin"><i>Tanggal terbit: 12 November 2020</i> </span>
            <p class="poppin text-justify">&emsp;&emsp;Kami akan memberikan penjelasan kepada Anda mengenai Kebijakan kami terkait dengan pengumpulan, penggunaan, dan pengungkapan Data Pribadi Anda yang kami dapat untuk PT. Senna Kreasi Nusa di dalam aplikasi/situs Senna. Halaman ini digunakan untuk memberi tahu pengunjung mengenai kebijakan kami dengan pengumpulan, penggunaan, dan pengungkapan Informasi Pribadi jika ada yang memutuskan untuk menggunakan Layanan kami.Jika Anda memilih untuk menggunakan Layanan kami, maka Anda menyetujui pengumpulan dan penggunaan informasi terkait dengan kebijakan ini. Informasi Pribadi yang kami kumpulkan digunakan untuk menyediakan dan meningkatkan Layanan. Kami tidak akan menggunakan atau membagikan informasi Anda kepada siapa pun kecuali seperti yang dijelaskan dalam Kebijakan Privasi ini. Istilah yang digunakan dalam Kebijakan Privasi ini memiliki arti yang sama seperti dalam Syarat dan Ketentuan kami.</p>
        </div>
        <div class="col-md-12 mt-5">
            <h3 class="text-senna poppin-med">Pengumpulan dan Penggunaan Informasi</h3>
            <p class="poppin text-justify">Untuk pengalaman yang lebih baik, saat menggunakan Layanan Senna, kami mungkin meminta Anda untuk memberikan kami informasi pengenal pribadi tertentu, termasuk namun tidak terbatas pada KTP, Nomor Telepon, Alamat Email, Nama Lengkap. Informasi yang kami minta akan disimpan oleh kami dan digunakan seperti yang dijelaskan dalam kebijakan privasi ini. <br>
               <br>Aplikasi memang menggunakan layanan pihak ketiga yang dapat mengumpulkan informasi yang digunakan untuk mengidentifikasi Anda.Tautan ke kebijakan privasi penyedia layanan pihak ketiga yang digunakan oleh aplikasi:
                <ol class="poppin" type="a" style="padding-left: 10%">
                    <li><a href="https://policies.google.com/terms" target="_blank" rel="noopener noreferrer">Google Play Services</a></li>
                <!---->
                <li><a href="https://firebase.google.com/terms/analytics" target="_blank" rel="noopener noreferrer">Google Analytics for Firebase</a></li>
                <li><a href="https://firebase.google.com/terms/crashlytics" target="_blank" rel="noopener noreferrer">Firebase Crashlytics</a></li>
                  </ol>
            </p>

            <h3 class="text-senna poppin-med mt-5">Log Data</h3>
                <p class="poppin text-justify">
                Kami ingin memberi tahu Anda bahwa setiap kali Anda menggunakan Layanan kami, jika terjadi kesalahan dalam aplikasi, kami mengumpulkan data dan informasi (melalui produk pihak ketiga) di ponsel Anda yang disebut Data Log. Data Log ini dapat mencakup informasi seperti alamat Protokol Internet ("IP") perangkat Anda, nama perangkat, versi sistem operasi, konfigurasi aplikasi saat menggunakan Layanan kami, waktu dan tanggal penggunaan Anda atas Layanan, dan statistik lainnya.
                </p>


                <h3 class="text-senna poppin-med mt-5">Kebijakan Anak Dibawah Umur</h3>
                <p class="poppin text-justify">
                Layanan ini tidak ditujukan kepada siapa pun yang berusia di bawah 13 tahun. Kami tidak dengan sengaja mengumpulkan informasi yang dapat diidentifikasi secara pribadi dari anak-anak di bawah 13 tahun. Jika kami menemukan bahwa seorang anak di bawah 13 tahun telah memberi kami informasi pribadi, kami segera menghapusnya dari server kami. Jika Anda adalah orang tua atau wali dan Anda mengetahui bahwa anak Anda telah memberi kami informasi pribadi, silakan hubungi kami sehingga kami dapat melakukan tindakan yang diperlukan.
                </p>

            <h3 class="text-senna poppin-med mt-5">Keamanan</h3>

                <p class="poppin text-justify">&emsp;&emsp;Data anda tersimpan dengan aman di server kami. Kami menggunakan sistem backup data secara berkala setiap jam, terenkripsi, dan terdistribusi ke beberapa data penyimpanan kami yang terpisah, sehingga dapat meminimalisir terjadinya hal-hal yang tidak diinginkan apabila terjadi kesalahan pada program atau server. Kami tidak memperjual belikan data anda kepada pihak manapun dengan alasan apapun, karena privasi dan keamanan data anda adalah prioritas kami.</p>
                <p class="poppin text-justify">&emsp;&emsp;Di sistem Senna, kami menggunakan metode keamanan yang berlapis dari berbagai sisi, baik sisi aplikasi, database, jaringan, maupun server. Senna juga didukung oleh tim pengembang yang berpengalaman di bidang keamanan data, untuk memastikan data Anda aman dari penyalahgunaan data oleh pihak manapun. Dari sisi aplikasi, Senna mengimplementasikan anti brute-force, anti SQL Injection, anti remote-injection, sistem log monitoring aktivitas user, dan lain-lain.</p>
                <p class="poppin text-justify">&emsp;&emsp;Senna juga dilengkapi sistem yang mendeteksi apakah email yang digunakan merupakan Spam, mencegah transaksi yang dilakukan oleh bot, dan mengimplementasikan fraud detection yang menggunakan kecerdasan buatan yang ditanamkan dalam sistem untuk mendeteksi adanya gangguan dari pihak luar. Dari sisi jaringan, sistem Senna menggunakan SSL 256 bit encryption, sehingga mengamankan data Anda dari penyadapan atau manipulasi data yang menggunakan metode man in the middle (MITM). Disisi lain, dengan menggunakan SSL, sistem Anda akan terdeteksi sebagai sistem yang aman oleh seluruh web browser, sehingga menghindari adanya injeksi iklan atau script yang dapat merugikan sistem Anda, dan juga dapat meningkatkan reputasi sistem Anda dari sudut pandang client Anda.</p>
                <p class="poppin text-justify">&emsp;&emsp;Dari sisi database, sistem Senna mengimplementasikan backup otomatis secara berkala setiap jam, terdistribusi secara otomatis di 4 lokasi penyimpanan yang berbeda. Disamping itu, database transaksi Anda juga terpisah dengan database file Anda, sehingga dapat meminimalisir berbagai resiko yang mungkin timbul akibat dari aktivitas hacking maupun penyalahgunaan Data. Dari sisi server, sistem Senna menggunakan anti DDOS , sehingga akan lebih mengamankan sistem Anda dari pihak-pihak yang bertujuan untuk mengganggu kestabilan sistem Anda.</p>

        </div>
        <div class="col-md-12 mt-5">
            <h3 class="text-senna poppin-med">Cookie(s)</h3>
            <p class="poppin text-justify">&emsp;&emsp;Cookie adalah berkas data kecil yang ditempatkan browser pada perangkat internet Anda. Dengan cookie, fitur Aplikasi/Situs web yang Anda akses dapat menyimpan informasi atau mengingat tindakan dan preferensi Anda dari waktu ke waktu. Sebagian besar browser internet mendukung cookie; namun Anda dapat mengatur browser yang Anda gunakan untuk menolak beberapa jenis cookie tertentu atau cookie spesifik. Di samping itu, Anda dapat menghapus cookie kapan saja. Kami menggunakan cookie untuk berbagai tujuan. Kami menggunakannya, misalnya, untuk mengingat preferensi penelusuran aman Anda, membantu Anda untuk menggunakan Layanan (apabila Anda adalah Pelanggan) atau mengelola Layanan (apabila Anda adalah Penyedia Layanan), dan melindungi data Anda.</p>
        </div>
        <div class="col-md-12 mt-5">
            <h3 class="text-senna poppin-med">Perubahan Kebijakan Privasi</h3>
            <p class="poppin text-justify">&emsp;&emsp;Kami dapat memperbaharui Kebijakan Privasi ini dari waktu ke waktu. Pembaharuan atas Kebijakan Privasi ini akan dipublikasikan di Aplikasi/Situs ini. Oleh karenanya, kami menghimbau Anda untuk mengakses Aplikasi/Situs ini secara berkala atas informasi terkini untuk Layanan Senna.</p>
        </div>
        <div class="col-md-12 mt-5">
                <h3 class="text-senna poppin-med">Kontak Kami</h3>
                <p class="poppin text-justify">

                <li>Office	: Jl. Sekarsuli-Berbah No. 9 Potorono, Banguntapan, Bantul, D.I.Y </li>
                <li>Phone	:+62812-9619-9565, +62813-2566-6958 </li>
                <li>E-mail	: contact@senna.co.id</li>

                </p>


            </div>
    </div>
</section>
@endsection

@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('contact')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('contact')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />

@endsection
@section('content')
    <section class="section-relative mt-section-header mb-section">
        <div class="container-fluid z-10 position-relative">
            <div class="row">
                <div class="col-md-12 text-center pt-5">
                    <h1 class="main-title mb-5">Ada yang bisa kami bantu ?</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card border-0 radius-20 shadow px-4">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="position-relative w-100">
                                    <input id="keyword" type="text" class="form-control input-search-faq radius-10 py-2" name="keyword" placeholder="Cari pertanyaan mu disini" autocomplete="off">
                                    <span class="input-group-btn rounded">
                                        <button class="btn faq-search-submit" onclick="search()">
                                            <i class="fa fa-lg fa-search"></i>
                                        </button>
                                    </span>
                                    <script>
                                        function search(){
                                            var keyword = document.getElementById('keyword').value;

                                            if(keyword == ''){
                                                swal({
                                                    title: "Perhatian !",
                                                    text: "Anda belum mengisi keyword !",
                                                    icon: "warning",
                                                    button: {
                                                        text:"Tutup",
                                                        className:"btn btn-orange"
                                                    },
                                                });
                                                return;
                                            }
                                            
                                            window.location.replace("{{route('faq.search')}}/"+keyword.replace(/[\W_]+/g,"-"));
                                        }

                                        var inputKeyword = document.getElementById('keyword');

                                        inputKeyword.addEventListener('keyup', (e) => {
                                            if(e.keyCode == '13'){
                                                search();
                                            }
                                        });
                                    </script>
                                </div>
                                <button style="font-size:14px;" class="btn btn-orange py-2 radius-10 ml-3" onclick="search()">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted.png" alt="background dotted" class="bg-bottom__center">
        <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/bg-bottom-rounded.png" alt="background" class="bg-bottom__rounded" style="bottom:-120%;">
    </section>
      
    @if(!isset($keyword))
    <section class="section-relative mb-120">
        <div class="container position-relative z-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="card shadow border-0 radius-20 p-5">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 mb-4">
                                    <div class="row">
                                        @foreach($category as $c)
                                        <div class="col-md-12 col-sm-6 mb-4">
                                            <div class="card border-0 faq-card__category py-2 h-100 mx-auto" id="faq-category-{{$c->id}}">
                                                <div class="card-body d-flex align-items-center justify-content-center text-center">
                                                    <button class="faq-btn-active" type="button" onclick="changeView('#faq-category-{{$c->id}}','{{$c->id}}')">
                                                        <img class="faq_icon_category" src="{{env('S3_URL')}}{{$c->icon_active}}" alt="icon">
                                                    </button>
                                                    <button class="faq-btn-nonactive" type="button" onclick="changeView('#faq-category-{{$c->id}}','{{$c->id}}')">
                                                        <img class="faq_icon_category" src="{{env('S3_URL')}}{{$c->icon_nonactive}}" alt="icon">
                                                    </button>
    
                                                </div>
                                                <div class="card-footer border-0 d-flex align-items-center justify-content-center text-center">
                                                    <button type="button" class="@if($c->id == $category_active->id) active @endif" onclick="changeView('#faq-category-{{$c->id}}','{{$c->id}}')">
                                                        {{$c->name}}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-8 mb-4">
                                    @if(count($data) == 0)
                                    <div class="alert text-center alert-warning alert-dismissible fade show" role="alert">
                                        Konten tidak tersedia
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    @else
                                    <div id="faq-result">
                                        @foreach($data as $key => $item)
                                        <div class="w-100 mb-4">
                                            <a class="faq-collapse arial-rounded collapsed d-flex align-items-center" data-toggle="collapse" href="#collapse{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapse{{$item->id}}">
                                                {{$item->title}} <i class="fa fa-chevron-down"></i>
                                            </a>
                                            <div class="collapse py-3" id="collapse{{$item->id}}">
                                                {!!$item->content!!}
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                        
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    @if(isset($keyword))
    <section class="section-relative mb-120">
        <div class="container position-relative z-10">
            <div class="row">
                <div class="col-md-12">
                    <div class="card shadow border-0 radius-20 p-5">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 mb-4">
                                    @if(count($data) == 0)
                                    <script>
                                        window.onload = () => {
                                            swal({
                                                title: "Perhatian !",
                                                text: "Hasil pencarian dengan kata kunci '{{$keyword}}' tidak ditemukan !",
                                                icon: "warning",
                                                button: {
                                                    text:"Tutup",
                                                    className:"btn btn-orange"
                                                },
                                            }).then((value) => {
                                                window.location.replace("{{route('faq')}}");
                                            });
                                        }
                                    </script>
                                    @else
                                        @foreach($data as $key => $item)
                                        <div class="w-100 mb-4">
                                            <a class="faq-collapse arial-rounded collapsed d-flex align-items-center" data-toggle="collapse" href="#collapse{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapse{{$item->id}}">
                                                {{$item->title}} <i class="fa fa-chevron-down"></i>
                                            </a>
                                            <div class="collapse py-3" id="collapse{{$item->id}}">
                                                {!!$item->content!!}
                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        const changeView = (container,id) => {
            $(".faq-card__category:not("+container+") .card-body button.faq-btn-active").hide();
            $(container+' .card-body button.faq-btn-nonactive').hide()
            $.ajax({
                url: "{{route('faq.change-view')}}",
                data: {"category_id" : id},
                success: function(response){
                   $('#faq-result').html(response);
                }
            });

            $(container+' .card-body button.faq-btn-active').show();
            $(".faq-card__category:not("+container+") .card-body button.faq-btn-nonactive").show();

            $(container+' .card-footer button').addClass('active');
            $(".faq-card__category:not("+container+") .card-footer button").removeClass('active');
            
        }
        changeView("#faq-category-{{$category_active->id}}","{{$category_active->id}}");
    </script>

@endsection

@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('contact')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('contact')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />

@endsection
@section('content')
    <section class="section-relative mt-section-header mb-section">
        <div class="container-fluid z-10 section-header-padding position-relative">
            <div class="row d-flex align-items-center">
                <div class="col-lg-6 col-md-12 pt-5 text-center-main-sm">
                    <h1 class="main-title mb-5">
                        Halo<br>
                        <span class="main-title__in with-border">Butuh Bantuan ?</span>
                    </h1>
                    <p class="main-p">Tanyakan kepada kami dan kami akan menjawab pertanyaanmu maksimal 24 jam</p>
                </div>
                <div class="col-lg-6 col-md-12 text-center">
                    <img src="{{env('S3_URL')}}public/frontend/img/contact-header.png" width="70%" alt="contact image" class="img-fluid">
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-right.png" alt="background right" class="bg-top__right">
    </section>

    <section class="section-relative mb-section section-padding z-10">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 mb-4">
                    <div class="card px-4 py-3 border-0 shadow radius-26">
                        <div class="card-body">
                            <h2 class="to-h3">Tanya Senna</h2>
                            <p class="sub-p">Butuh demo? Butuh bantuan teknis? Ada tawaran kerjasama dengan Senna? Atau ada pertanyaan lain?</p>
                            <div id="result-form-konten"></div>
                            <form method="POST" id="form-konten" onsubmit="return false;">
                                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 mb-3">
                                        <input type="text" class="form-control form-contact" id="fullname" name="fullname" placeholder="Nama Lengkap">
                                    </div>
                                    <div class="col-md-6 col-sm-12 mb-3">
                                        <input type="text" class="form-control form-contact" id="email" name="email" placeholder="Email">
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <textarea class="form-control form-contact" name="message" id="message" cols="30" rows="10" placeholder="Pesan"></textarea>
                                    </div>

                                    <div class="col-md-12 text-right mb-3">
                                        <button onclick="contact()" class="btn btn-orange radius-10 arial-rounded px-5" style="font-size:12px;">
                                            Kirim
                                        </button>
                                    </div>
                                </div>

                            </form>
                            <script>
                                function contact(){
                                    if($("#fullname").val()=='' || $("#email").val()=='' || $("#message").val()=='')
                                    {
                                        swal({
                                            title: "Perhatian !",
                                            text: "Isian form wajib diisi semua !",
                                            icon: "warning",
                                            button: {
                                                text:"Tutup",
                                                className:"btn btn-orange"
                                            },
                                        });
                                    }else{
                                        var data = getFormData('form-konten');
                                        ajaxTransfer("{{route('contact.store')}}", data, '#result-form-konten');
                                    }

                                }

                            </script>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-md-12 col-sm-12 mb-4">
                    <div class="card shadow radius-26 border-0 px-4 py-3">
                        <div class="card-body">
                            <h2 class="to-h4 mb-4">Ikuti Senna Indonesia Lebih Lanjut</h2>
                            <ul class="list-group">
                                <li class="list-group-item d-flex align-items-start p-0 border-0">
                                    <div style="width:15%;">
                                        <a title="link to facebook" href="https://www.facebook.com/sennaidn" target="_blank" rel="noreferrer" class="btn btn-ifooter">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div style="width:85%;">
                                        <h3 class="to-h6">Senna Indonesia</h3>
                                        <p class="sub-p">Like page facebook kami untuk ikuti perkembangan usaha serta promo menarik.</p>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0 border-0">
                                    <div style="width:15%;">
                                        <a title="link to youtube" href="https://www.youtube.com/channel/UCooO_WCjQXQQZY5p-jvrjkw" target="_blank" rel="noreferrer" class="btn btn-ifooter">
                                            <i class="fa fa-youtube"></i>
                                        </a>
                                    </div>
                                    <div style="width:85%;">
                                        <h3 class="to-h6">Senna Indonesia</h3>
                                        <p class="sub-p">Lihat beragam video menarik, tutorial bermanfaat yang dapat menginspirasimu dalam menjalankan usaha.</p>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0 border-0">
                                    <div style="width:15%;">
                                        <a title="link to twitter" href="#" class="btn btn-ifooter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                    <div style="width:85%;">
                                        <h3 class="to-h6">Senna Indonesia</h3>
                                        <p class="sub-p">Cuitan tentang bisnis yang dapat kamu ikuti dan bagikan. </p>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-start p-0 border-0">
                                    <div style="width:15%;">
                                        <a title="link to instagram" href="https://www.instagram.com/sennaindonesia/" target="_blank" rel="noreferrer" class="btn btn-ifooter">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </div>
                                    <div style="width:85%;">
                                        <h3 class="to-h6">Senna Indonesia</h3>
                                        <p class="sub-p">Promo, produk baru, diskusi, dan segala informasi yang dapat kamu ikuti untuk perkembangan bisnismu.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{asset('public/backend/js/ajax.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        window.onload = () => {
            document.querySelector('#loading-box').remove();
        }
    </script>
@endsection

@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('demo')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('demo')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
@endsection
@section('content')
<section class="section">
    <img src="{{asset('public/frontend')}}/img/bg.png" style="background-size: cover;height: 120%;width:100% ;position: absolute" alt="background">
        <div class="container bring-to-front">
            <div class="row justify-content-center text-center text-white">
                <div class="col-md-12 mt-5" >
                    <h1 class="poppin">Booking Jadwal Demo Online</h1>
                    <p class="poppin">Mau dibantu tim kami agar bisnis anda menguntungkan & mudah di kontrol pakai Senna? <br> Silahkan lengkapi form permintaan booking jadwal demoo di bawah ini</p>
                    <div class="row justify-content-center">
                        <div class="col-md-6"><br>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{asset('public/frontend')}}/video/senna.mp4" allowfullscreen></iframe>
                              </div>
                              <p class="poppin mt-3">*Tonton Ilustrasi Online Via Google Meet</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

</section>

<section class="section">
<div class="container bring-to-front bg-ight" >
<div class="row justify-content-center px-5 py-5 shadow" style="background-color: white;border-radius: 20px;">
    <div class="col-md-12">

        <br>
    <form method="POST" id="form-konten">
        <div class="row">
        <div class="col-md-4 mt-2"><h5>Nama Lengkap <span class="text-danger">*</span></h5><input type="text" class="form-control form-control-sm" name="fullname" placeholder="Masukan Nama Lengkap"required ></div>
        <div class="col-md-4 mt-2"><h5>No. HP/WA <span class="text-danger">*</span></h5><input type="text" class="form-control form-control-sm" name="telp" placeholder="Masukan No. HP/WA yang aktif.." required=""></div>
        <div class="col-md-4 mt-2"><h5>Email <span class="text-danger">*</span></h5><input type="text" class="form-control form-control-sm" id="email" name="email" placeholder="Email" required=""></div>
        </div>
        <div class="row mt-4">
            <div class="col-md-4 mt-2"><h5>Nama Perusahaan <span class="text-danger">*</span></h5><input type="text" class="form-control form-control-sm" name="company_name" placeholder="Nama Perusahaan"required ></div>
            <div class="col-md-4 mt-2"><h5>Kota <span class="text-danger">*</span></h5><div class="form-group">
            <select id="inputState" class="form-control form-control-sm" name="md_cities_id">
                <option disabled selected>--Pilih Kota--</option>
                @foreach ($city as $c)
                <option value="{{$c->id}}">{{ preg_replace("/Kabupaten/","", $c->name)}}</option>
                @endforeach
            </select></div></div>
            <div class="col-md-4 mt-2"><h5>Bidang Usaha <span class="text-danger">*</span></h5>
            <select id="inputState" class="form-control form-control-sm" name="md_sc_categories_id">
                <option disabled selected>--Pilih Bidang Usaha Anda--</option>
                @foreach ($category as $c)
                <option value="{{$c->id}}">{{$c->name}}</option>
                @endforeach
            </select></div>
        </div>
        <div class="row mt-4">
            <div class="col-md-4 mt-2"><h5>Produk <span class="text-danger">*</span></h5>
                <select id="inputState" class="form-control form-control-sm" name="product">
                <option disabled selected>--Pilih Produk--</option>
                <option value="online" >Senna</option>
                {{-- <option value="offline" >Demo Offline</option> --}}
            </select></div>
            <div class="col-md-4 mt-2"><h5>Pilih Demo <span class="text-danger">*</span></h5>
                <select id="inputState" class="form-control form-control-sm" name="demo_type">
                    {{-- <option disabled selected>--Pilih Demo--</option> --}}
                    <option value="online" >Demo Online</option>
                    {{-- <option value="offline" >Demo Offline</option> --}}
                </select></div>
            <div class="col-md-4 mt-2"><h5>Tanggal Demo <span class="text-danger">*</span></h5><input type="date" class="form-control form-control-sm" name="demo_date" placeholder="Email" required=""></div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 mt-2">
                <h5>Tantangan Apa di Bisnis Anda yg ingin Diselesaikan Pakai Senna? <span class="text-danger">*</span></h5>
                <textarea class="form-control form-control-sm" name="description" cols="30" rows="10" placeholder="Cerita Singkat" required=""></textarea>
            </div>
        </div>

        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <div class="row mt-4">

            <div class="col-md-12">
                <div id="result-form-konten"></div>
            <button onclick="contact()" class="btn text-light text-center" style="width: 100%;background-color: #FF5901;"><h5>Kirim Request</h5> </button>
        </div>
        </div>


        </form>
        <script>
            function contact(){
                if($("#fullname").val()=='' || $("#email").val()=='' || $("#telp").val()=='' || $("#company_name").val()=='' || $("#md_cities_id").val()=='' || $("#md_sc_categories_id").val()=='' || $("#product").val()=='' || $("#demo_type").val()=='' || $("#demo_date").val()=='' || $("#description").val()=='')
                {
                    alert("Isian form wajib diisi semua");
                }else{
                      var data = getFormData('form-konten');
                //window.location.replace ("{{route('contact.store')}}?data="+data);
                ajaxTransfer("{{route('demo.store')}}", data, '#result-form-konten');
                }

            }

        </script>

    </div>
</div>
</div>
</section>
@endsection

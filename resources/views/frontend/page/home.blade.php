@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('home')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('home')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
@endsection
@section('content')
    <section class="section-header d-flex align-items-center">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-left.png" alt="background left" class="bg-top__left">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted-top.png" alt="background left" class="bg-dotted__top__left">
        <div class="container-fluid z-10">
            <div class="row nav-padding">
                <div class="col-sm-12 hero-image__mobile text-center px-0">
                    <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/hero-image-mobile.png" alt="Kelola Usaha jadi Mudah dan Menguntungkan" class="img-fluid">
                </div>
                <div class="col-md-12 col-sm-12 col-12 text-center-main-sm px-0">
                    <h1 class="main-title mb-main-title">Kelola Usaha jadi Mudah<br> dan Menguntungkan<br>
                        <span class="main-title__in with-border">#Coba Aja Dulu !</span>
                    </h1>
                    <a href="{{route('register')}}" class="btn btn-orange arial-rounded radius-10 py-2">Demo Gratis <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted.png" alt="background dotted" class="bg-bottom__center">
        <img src="{{env('S3_URL')}}public/frontend/img/hero-image.png" alt="Kelola Usaha jadi Mudah dan Menguntungkan" class="hero-right__img">
    </section>

    <section class="section-relative z-10 py-5 d-flex align-items-center mt-section-m-0 mb-200">
        <div class="container z-10">
            <div class="row">
                <div class="col-md-6 text-center mb-3 py-3">
                    <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/frame-2.png" alt="Kelola usaha dimanapun dan kapanpun dengan Senna" width="60%">
                </div>
                <div class="col-md-6 mb-3 py-3 px-smm-5">
                    <span class="badge badge-orange mb-4 arial-rounded text-sm-center d-block-sm">Kata siapa kelola usaha itu ribet?</span>
                    <h2 class="mb-4 text-center-sm">Kelola usaha dimanapun dan kapanpun dengan <span class="sub-title__in">Senna</span></h2>
                    <ul class="section-list">
                        <li class="section-list__item mb-2">
                            <i class="fa fa-check-circle mr-2"></i>
                            Tingkatkan keuntungan dengan transaksi penjualan tak terbatas
                        </li>
                        <li class="section-list__item mb-2">
                            <i class="fa fa-check-circle mr-2"></i>
                            Kelola usaha dari berbagai perangkat (Android, IOS, dan PC)
                        </li>
                        <li class="section-list__item mb-2">
                            <i class="fa fa-check-circle mr-2"></i>
                            Integrasi cepat dengan berbagai saluran pembayaran, penjualan dan pengiriman
                        </li>
                        <li class="section-list__item mb-2">
                            <i class="fa fa-check-circle mr-2"></i>
                            Pantau usahamu dari jarak jauh
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/bg-bottom-rounded.png" alt="background" class="bg-bottom__rounded">
    </section>

    <section class="section-relative d-flex align-items-center padding-home__feature mb-120">
        <div class="container-fluid z-10 d-flex align-items-center">
            <div class="row">
                <div class="col-lg-6 col-md-12 mb-3">
                    <div class="row mx-s-auto" style="width:85%;">
                        <div class="col-md-6 col-sm-6">
                            <div class="card card-home__feature active mx-auto" id="card-feature-1" onclick="handleBtnAccordionClick(`{{env('S3_URL')}}public/frontend/img/home-kasir-online.png`, '#card-feature-1')">
                                <div class="card-header">
                                    <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/home-icon-1.png" alt="icon" width="60%" class="card-home__icon">
                                </div>
                                <div class="card-body mt-5">
                                    <h2 class="card-home__feature__title">Kasir Online</h2>
                                    <p>Pembukuan transaksi di berbagai cabang secara otomatis</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="card card-home__feature mx-auto" id="card-feature-2" onclick="handleBtnAccordionClick(`{{env('S3_URL')}}public/frontend/img/home-manajemen-persediaan.png`, '#card-feature-2')">
                                <div class="card-header">
                                    <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/home-icon-2.png" alt="icon" width="60%" class="card-home__icon">
                                </div>
                                <div class="card-body mt-5">
                                    <h2 class="card-home__feature__title">Manajemen Persediaan & Asset</h2>
                                    <p>Kendalikan ketersediaan stok barang & aset yang kamu miliki</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="card card-home__feature mx-auto" id="card-feature-3" onclick="handleBtnAccordionClick(`{{env('S3_URL')}}public/frontend/img/home-manajemen-karyawan.png`, '#card-feature-3')">
                                <div class="card-header">
                                    <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/home-icon-3.png" alt="icon" width="60%" class="card-home__icon">
                                </div>
                                <div class="card-body mt-5">
                                    <h2 class="card-home__feature__title">Manajemen Karyawan</h2>
                                    <p>Pantau kehadiran dan hak akses fitur karyawanmu secara real-time</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="card card-home__feature mx-auto" id="card-feature-4" onclick="handleBtnAccordionClick(`{{env('S3_URL')}}public/frontend/img/home-laporan-keuangan.png`, '#card-feature-4')">
                                <div class="card-header">
                                    <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/home-icon-4.png" alt="fitur senna" width="60%" class="card-home__icon">
                                </div>
                                <div class="card-body mt-5">
                                    <h2 class="card-home__feature__title">Manajemen Keuangan</h2>
                                    <p>Pembuatan laporan yang detail & akurat berstandar akuntansi</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <a class="arial-rounded" href="{{route('features')}}" style="color:#FF9900;">
                                Lihat Fitur Menarik Lainnya
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 text-right mb-3">
                    <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/home-kasir-online.png" alt="fitur senna" class="img-fluid" id="fitur-img">
                </div>
            </div>
        </div>
        <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/bg-top-right.png" alt="background icon" class="bg-home__feature">
    </section>

    <section class="section-relative mb-section">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-12 text-center">
                    <h2>Bisnis Apapun Pasti Bisa</h2>
                    <p>Pilih dulu tipe bisnismu, tentuin kebutuhan sesuai yang kamu mau.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/tb-retail.png" alt="image business" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'retail'])}}">Toko Retail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/tb-restoran.png" alt="image business" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'cafe'])}}">Cafe & Restoran</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/tb-fnb.png" alt="image business" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'fnb'])}}">F & B</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-type__business">
                        <div class="card-body text-center">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/tb-elektronik.png" alt="image business" class="img-fluid mb-4">
                            <a class="btn-white radius-9 arial-rounded" href="{{route('solution.detail', ['page' =>'elektronik'])}}">Elektronik</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right text-center-sm">
                    <a class="arial-rounded" href="{{route('solution')}}" style="color:#FF9900;">
                        Lihat Tipe Bisnis Lainnya
                        <i class="fa fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    @if($testimony->count()>0)
    <section class="section-relative mt-200-sm-0 mb-120 section-pl">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted-top.png" alt="background left" class="bg-dotted__top__left">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 position-relative mb-120 z-10">
                    <h2 class="mt-5 text-center-sm">Apa Kata Pengguna Senna?</h2>
                    {{---<a class="arial-rounded text-center-sm d-block" href="#" style="color:#FF9900;">
                        Lihat Senna Selanjutnya
                        <i class="fa fa-arrow-right"></i>
                    </a>---}}
                </div>
                <div class="col-md-8 position-relative">
                    <div class="slider-bg__icon">
                        <span class="arial-rounded">“</span>
                    </div>
                    <div class="testi-slider__wrapper z-10">
                        <div class="testi-slider__container" id="testi-container">
                            @foreach($testimony as $key => $t)
                                @if($key + 1 == count($testimony))
                                    <div class="testi-slider" id="testi-last-item">
                                        <div class="testi-vid__wrapper">
                                            <div class="embed-responsive embed-responsive-16by9 mb-5">
                                                <iframe title="{{$t->message}}" class="embed-responsive-item position-absolute img-right" src="{{$t->url}}" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        {{---<span class="badge badge-orange-sm mb-4 arial-rounded">Buah dan Sayur</span>---}}
                                        <p>{{$t->message}}</p>
                                        <h2 class="testi-slider__owner">{{$t->owner_name}}</h2>
                                        <p>{{$t->merchant_name}}</p>
                                    </div>
                                @else
                                    <div class="testi-slider">
                                        <div class="testi-vid__wrapper">
                                            <div class="embed-responsive embed-responsive-16by9 mb-5">
                                                <iframe title="{{$t->message}}" class="embed-responsive-item position-absolute img-right" src="{{$t->url}}" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        {{---<span class="badge badge-orange-sm mb-4 arial-rounded">Buah dan Sayur</span>---}}
                                        <p>{{$t->message}}</p>
                                        <h2 class="testi-slider__owner">{{$t->owner_name}}</h2>
                                        <p>{{$t->merchant_name}}</p>
                                    </div>
                                @endif
                            @endforeach

                        </div>

                    </div>
                    <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/bg-center.png" alt="background middle" class="bg-middle__left">

                    <div class="row position-relative z-10">
                        <div class="col-md-12 text-right px-5">
                            <button type="button" class="testi-slider__prev mr-1" aria-label="next slide" onclick="nextSlide('left')">
                                <i class="fa fa-arrow-left"></i>
                            </button>
                            <button type="button" class="testi-slider__next ml-1" aria-label="previous slide" onclick="nextSlide('right')">
                                <i class="fa fa-arrow-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    <section class="section-relative mb-120">
        <div class="container py-5 position-relative z-10">
            <div class="row mb-5">
                <div class="col-md-12 text-center">
                    <h2>Kelola Bisnis Sesuai Kebutuhanmu</h2>
                    <p>Kami tau kebutuhanmu pasti berbeda beda, tapi semuanya sudah kami satukan disini. Yuk jadi bagian dari mitra kami !</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-manage__business cmb-bg1">
                        <div class="card-header">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/cmb-1.png" alt="5000 Bisnis di 230 kota" class="img-fluid">
                        </div>
                        <div class="card-body text-center position-relative z-10">
                            <h3 class="cmb-title">5000 +</h3>
                            <p>Bisnis di 230 kota</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-manage__business cmb-bg2">
                        <div class="card-header">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/cmb-2.png" alt="pengelolaan usaha terintegrasi" class="img-fluid">
                        </div>
                        <div class="card-body text-center position-relative z-10">
                            <h3 class="cmb-title">Pengelolaan Usaha</h3>
                            <p>Terintegrasi</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-manage__business cmb-bg3">
                        <div class="card-header">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/cmb-3.png" alt="multiplatform" class="img-fluid">
                        </div>
                        <div class="card-body text-center position-relative z-10">
                            <h3 class="cmb-title">Multiplatform</h3>
                            <p>Android & IOS</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                    <div class="card card-manage__business cmb-bg4">
                        <div class="card-header">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/cmb-4.png" alt="24 jam pelayanan pelanggan" class="img-fluid">
                        </div>
                        <div class="card-body text-center position-relative z-10">
                            <h3 class="cmb-title">24 Jam</h3>
                            <p>Pelayanan Pelanggan</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/bg-bottom-rounded.png" alt="background" class="bg-top__rounded" style="top:-40px;">
    </section>
    <script>
        $(document).ready(function(){
            $('.testi-slider__prev').click();
        })
        const handleBtnAccordionClick = (srcVal, cardId) => {
            const cards = document.querySelectorAll('.card-home__feature');

            cards.forEach(item => {
                item.classList.remove('active');
            });

            const cardTarget = document.querySelector(cardId);

            cardTarget.classList.add('active');

            const fiturImg = document.querySelector('#fitur-img');
            fiturImg.removeAttribute('src');
            fiturImg.setAttribute('src', srcVal);

        }

        const testiContainer = document.querySelector('#testi-container');
        const testiLast = document.querySelector('#testi-last-item');
        let xPos = 0;

        const nextSlide = (pos) => {
            let rect = testiLast.getBoundingClientRect();

            if(pos === 'left'){
                if(xPos !== 0){
                    xPos += parseInt(rect.width);
                }
            } else {
                if(rect.right > window.innerWidth){
                    xPos -= parseInt(rect.width);
                }
            }

            testiContainer.style.left = xPos +"px";

        }

    </script>
@endsection


@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('contact')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('contact')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />

@endsection
@section('content')
    <section class="section-relative mt-section-header mb-section">
        <div class="container-fluid z-10 position-relative">
            <div class="row">
                <div class="col-md-12 text-center pt-5">
                    <h1 class="main-title mb-5">Pilih Perangkat untuk<br>Usaha mu disini</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card border-0 radius-20 shadow px-4">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="position-relative w-100">
                                    <input id="keyword" type="text" class="form-control input-search-faq radius-10 py-2" name="keyword" placeholder="Cari item" autocomplete="off">
                                    <span class="input-group-btn rounded">
                                        <button class="btn faq-search-submit" onclick="search()">
                                            <i class="fa fa-lg fa-search"></i>
                                        </button>
                                    </span>
                                    <script>
                                        function search(){
                                            var keyword = document.getElementById('keyword').value;

                                            if(keyword == ''){
                                                swal({
                                                    title: "Perhatian !",
                                                    text: "Anda belum mengisi keyword !",
                                                    icon: "warning",
                                                    button: {
                                                        text:"Tutup",
                                                        className:"btn btn-orange"
                                                    },
                                                });
                                                return;
                                            }
                                            
                                            window.location.replace("{{route('hardware.search')}}/"+keyword.replace(/[\W_]+/g,"-"));
                                        }

                                        var inputKeyword = document.getElementById('keyword');

                                        inputKeyword.addEventListener('keyup', (e) => {
                                            if(e.keyCode == '13'){
                                                search();
                                            }
                                        });
                                    </script>
                                </div>
                                <button style="font-size:14px;" class="btn btn-orange py-2 radius-10 ml-3" onclick="search()">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted.png" alt="background dotted" class="bg-bottom__center">
        <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/bg-bottom-rounded.png" alt="background" class="bg-bottom__rounded" style="bottom:-120%;">
    </section>
      
    @if(!isset($keyword))
        <section class="section-relative mb-120">
            <div class="container position-relative z-10">
                <div class="row">
                    @foreach($data as $key => $item)
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 mb-5">
                        <div class="card card-hardware shadow mb-3 border-0 h-100">
                            <div class="card-header border-0" style="background-color: #FFF;border-radius: 24px;">
                                <div class="d-flex align-items-center" style="height:160px;">
                                    @if(!is_null($item->image))
                                    <img src="{{env('S3_URL')}}{{$item->image}}" alt="{{$item->title}}" class="img-fluid mx-auto d-block" style="width:100%;height:100%;object-fit:contain;">
                                    @else
                                    <img src="{{asset('public/frontend/img/no-product-image.png')}}" alt="perangkat kasir" style="width:100%;height:100%;object-fit:cover;">
                                    @endif
                                </div>
                            </div>
                            <div class="card-body" style="padding-bottom:0;">
                                <h2 class="to-h6 mb-2">{{$item->title}}</h2>
                                <div class="card-hardware__content" style="color:#818286; font-size:14px;">
                                    {!!$item->content!!}
                                </div>
                            </div>
                            <div class="card-footer text-center border-0">
                                @if($item->discount_percentage > 0)
                                    @php
                                        $priceAfterDiscount = $item->price - $item->discount_percentage / 100 * $item->price
                                    @endphp
                                    <small style="color: #8d8d8d;font-size: 14px;">
                                        <s>{{rupiah($item->price)}}</s>
                                    </small>
                                    <h3 class="to-h4 card-hardware__price mb-4">{{rupiah($priceAfterDiscount)}}</h3>
                                @else
                                    <h3 class="to-h4 card-hardware__price mb-4">{{rupiah($item->price)}}</h3>
                                @endif
                                <div class="card-hardware__dropdown navbar dropdown mx-auto">
                                    <button class="btn btn-card__hardware py-2 arial-rounded" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Beli
                                    </button>
                                    <div class="dropdown-menu p-3" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-card-hardware__item dropdown-item py-2 text-center mb-3" href="{{$item->shopee_url}}" target="_blank">Shopee</a>
                                        <a class="btn btn-card-hardware__item dropdown-item py-2 text-center mb-3" href="{{$item->tokopedia_url}}" target="_blank">Tokopedia</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
    
        @if(count($dataBundling) > 0)
        <section class="section-relative mb-120" style="background: #FDF5EE;">
            <div class="container position-relative py-5 z-10">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="mb-5 to-h3">Pilih Paketmu Disini</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-8">
                        <div class="row">
                            @foreach($dataBundling as $key => $item)
                            <div class="col-md-12 mb-5">
                                <div class="card card-hardware shadow mb-3 border-0 h-100">
                                    <div class="card-body d-flex justify-content-center align-items-center">
                                        <div class="w-25">
                                            @if(!is_null($item->image))
                                            <img src="{{env('S3_URL')}}{{$item->image}}" alt="perangkat kasir" class="img-fluid" style="width:80%;object-fit:cover;">
                                            @else
                                            <img src="{{asset('public/frontend/img/no-product-image.png')}}" alt="perangkat kasir" style="width:80%;object-fit:cover;">
                                            @endif
                                        </div>
                                        <div class="w-50">
                                            <h2 class="to-h5">{{$item->title}}</h2>
                                            <div class="card-hardware__content">
                                                {!!$item->content!!}
                                            </div>
                                            @if($item->discount_percentage > 0)
                                                @php
                                                    $priceAfterDiscount = $item->price - $item->discount_percentage / 100 * $item->price
                                                @endphp
                                                <small style="color: #8d8d8d;font-size: 14px;">
                                                    <s>{{rupiah($item->price)}}</s>
                                                </small>
                                                <h3 class="to-h4 card-hardware__price mb-4">{{rupiah($priceAfterDiscount)}}</h3>
                                            @else
                                                <h3 class="to-h4 card-hardware__price mb-4">{{rupiah($item->price)}}</h3>
                                            @endif
                                        </div>
                                        <div class="w-25">
                                            <div class="card-hardware__dropdown navbar dropdown mx-auto">
                                                <button class="btn btn-card__hardware py-2 arial-rounded" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Beli
                                                </button>
                                                <div class="dropdown-menu p-3" aria-labelledby="dropdownMenu2">
                                                    <a class="btn btn-card-hardware__item dropdown-item py-2 text-center mb-3" href="{{$item->shopee_url}}" target="_blank">Shopee</a>
                                                    <a class="btn btn-card-hardware__item dropdown-item py-2 text-center mb-3" href="{{$item->tokopedia_url}}" target="_blank">Tokopedia</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-sm-6 offset-xl-0 offset-md-3 offset-sm-3">
                        <div class="card card-hardware shadow mb-3 border-0">
                            <div class="card-header border-0" style="background-color: #FFF;border-radius: 24px;">
                                <div class="d-flex align-items-center" style="height:160px;">
                                    @if(!is_null($dataBundling->first()->image))
                                    <img src="{{env('S3_URL')}}{{$dataBundling->first()->image}}" alt="{{$item->title}}" class="img-fluid mx-auto d-block" style="width:100%;height:100%;object-fit:contain;">
                                    @else
                                    <img src="{{asset('public/frontend/img/no-product-image.png')}}" alt="perangkat kasir" style="width:100%;height:100%;object-fit:cover;">
                                    @endif
                                </div>
                            </div>
                            <div class="card-body d-flex flex-column justify-content-center">
                                <h2 class="to-h5 mt-4">{{$dataBundling->first()->title}}</h2>
                                <div class="card-hardware__content">
                                    {!!$dataBundling->first()->content!!}
                                </div>
                            </div>
                            <div class="card-footer text-center border-0">
                                @if($dataBundling->first()->discount_percentage > 0)
                                    @php
                                        $priceAfterDiscount = $dataBundling->first()->price - $dataBundling->first()->discount_percentage / 100 * $dataBundling->first()->price
                                    @endphp
                                    <small style="color: #8d8d8d;font-size: 14px;">
                                        <s>{{rupiah($dataBundling->first()->price)}}</s>
                                    </small>
                                    <h3 class="to-h4 card-hardware__price mb-4">{{rupiah($priceAfterDiscount)}}</h3>
                                @else
                                    <h3 class="to-h4 card-hardware__price mb-4">{{rupiah($dataBundling->first()->price)}}</h3>
                                @endif
                                <div class="card-hardware__dropdown dropdown navbar mx-auto">
                                    <button class="btn btn-card__hardware py-2 arial-rounded" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Beli
                                    </button>
                                    <div class="dropdown-menu p-3" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-card-hardware__item dropdown-item py-2 text-center mb-3" href="{{$dataBundling->first()->shopee_url}}" target="_blank">Shopee</a>
                                        <a class="btn btn-card-hardware__item dropdown-item py-2 text-center mb-3" href="{{$dataBundling->first()->tokopedia_url}}" target="_blank">Tokopedia</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif


    @endif

    @if(isset($keyword))
    <section class="section-relative mb-120">
        <div class="container position-relative z-10">
            <div class="row">
                @if(count($data) == 0)
                    <div class="col-md-12 mb-4">
                        <script>
                            window.onload = () => {
                                swal({
                                    title: "Perhatian !",
                                    text: "Hasil pencarian dengan kata kunci '{{$keyword}}' tidak ditemukan !",
                                    icon: "warning",
                                    button: {
                                        text:"Tutup",
                                        className:"btn btn-orange"
                                    },
                                }).then((value) => {
                                    window.location.replace("{{route('hardware')}}");
                                });
                            }
                        </script>
                    </div>
                @else
                    @foreach($data as $key => $item)
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 mb-5">
                        <div class="card card-hardware shadow mb-3 border-0 h-100">
                            <div class="card-header border-0 mt-2" style="background-color: #FFF;border-radius: 24px;">
                                <div class="d-flex align-items-center" style="height:160px;">
                                    @if(!is_null($item->image))
                                    <img src="{{env('S3_URL')}}{{$item->image}}" alt="{{$item->title}}" class="img-fluid mx-auto d-block" style="width:100%;height:100%;object-fit:contain;">
                                    @else
                                    <img src="{{asset('public/frontend/img/no-product-image.png')}}" alt="perangkat kasir" style="width:100%;height:100%;object-fit:cover;">
                                    @endif
                                </div>
                            </div>
                            <div class="card-body" style="padding-bottom:0;">
                                <h2 class="to-h6 mb-2">{{$item->title}}</h2>
                                <div class="card-hardware__content" style="color:#818286; font-size:14px;">
                                    {!!$item->content!!}
                                </div>
                            </div>
                            <div class="card-footer text-center border-0">
                                @if($item->discount_percentage > 0)
                                    @php
                                        $priceAfterDiscount = $item->price - $item->discount_percentage / 100 * $item->price
                                    @endphp
                                    <small style="color: #8d8d8d;font-size: 14px;">
                                        <s>{{rupiah($item->price)}}</s>
                                    </small>
                                    <h3 class="to-h4 card-hardware__price mb-4">{{rupiah($priceAfterDiscount)}}</h3>
                                @else
                                    <h3 class="to-h4 card-hardware__price mb-4">{{rupiah($item->price)}}</h3>
                                @endif
                                <div class="card-hardware__dropdown navbar dropdown mx-auto">
                                    <button class="btn btn-card__hardware py-2 arial-rounded" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Beli
                                    </button>
                                    <div class="dropdown-menu p-3" aria-labelledby="dropdownMenu2">
                                        <a class="btn btn-card-hardware__item dropdown-item py-2 text-center mb-3" href="{{$item->shopee_url}}" target="_blank">Shopee</a>
                                        <a class="btn btn-card-hardware__item dropdown-item py-2 text-center mb-3" href="{{$item->tokopedia_url}}" target="_blank">Tokopedia</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    @endif
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@endsection

@extends('frontend.layout.main')
@section('meta')
<meta name="description" content="Syarat dan Ketentuan Penggunaan Aplikasi Senna">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="Syarat dan Ketentuan Penggunaan Aplikasi Senna">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="Syarat dan Ketentuan Penggunaan Aplikasi Senna" />
    <meta name="twitter:url" content="{{route('term')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('term')}}" />
    <meta property="og:description" content="Syarat dan Ketentuan Penggunaan Aplikasi Senna" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
@endsection
@section('content')

<section class="section-relative mt-section-header mb-5">
    <div class="container">
        <div class="col-md-12">
            <h1 class="pt-5 mb-3">Syarat dan Ketentuan Senna</h1>
            <span class="small"><i>Tanggal terbit: 12 November 2020</i> </span>
            <p class="poppin text-justify">
                Dengan mengunduh atau menggunakan aplikasi, persyaratan ini akan secara otomatis berlaku untuk Anda - Anda harus memastikan bahwa Anda membacanya dengan cermat sebelum menggunakan aplikasi. Anda tidak diizinkan untuk
                menyalin, atau memodifikasi aplikasi, bagian mana pun dari aplikasi, atau merek dagang kami dengan cara apa pun. Anda tidak diizinkan mencoba mengekstrak kode sumber aplikasi, dan Anda juga tidak boleh mencoba menerjemahkan
                aplikasi ke dalam bahasa lain, atau membuat versi turunan. Aplikasi itu sendiri, dan semua merek dagang, hak cipta, hak basis data, dan hak kekayaan intelektual lain yang terkait dengannya, masih menjadi milik PT Senna
                Kreasi Nusa.
            </p>

            <p class="poppin text-justify">
                PT Senna Kreasi Nusa berkomitmen untuk memastikan bahwa aplikasi ini berguna dan seefisien mungkin. Oleh karena itu, kami berhak membuat perubahan pada aplikasi atau membebankan biaya atas layanannya, kapan pun dan dengan
                alasan apa pun. Kami tidak akan pernah menagih Anda untuk aplikasi atau layanannya tanpa menjelaskan kepada Anda apa tepatnya yang Anda bayar.
            </p>

            <p class="poppin text-justify">
                Aplikasi Senna  menyimpan dan memproses data pribadi yang telah Anda berikan kepada kami, untuk menyediakan Layanan kami. Merupakan tanggung jawab Anda untuk menjaga keamanan ponsel dan akses ke aplikasi. Oleh karena
                itu kami menyarankan Anda untuk tidak melakukan jailbreak atau me-root ponsel Anda, yang merupakan proses menghapus batasan dan batasan perangkat lunak yang diberlakukan oleh sistem operasi resmi perangkat Anda. Itu bisa
                membuat ponsel Anda rentan terhadap malware / virus / program jahat, membahayakan fitur keamanan ponsel Anda dan itu bisa berarti aplikasi Senna tidak akan berfungsi dengan baik atau tidak akan berfungsi sama sekali.
            </p>

            <p class="poppin text-justify">Aplikasi memang menggunakan layanan pihak ketiga yang menyatakan Syarat dan Ketentuan mereka sendiri.Tautan ke Syarat dan Ketentuan penyedia layanan pihak ketiga yang digunakan oleh aplikasi</p>
            <ol class="poppin" type="a" style="padding-left: 10%;">
                <li><a href="https://policies.google.com/terms" target="_blank" rel="noopener noreferrer">Google Play Services</a></li>
                <!---->
                <li><a href="https://firebase.google.com/terms/analytics" target="_blank" rel="noopener noreferrer">Google Analytics for Firebase</a></li>
                <li><a href="https://firebase.google.com/terms/crashlytics" target="_blank" rel="noopener noreferrer">Firebase Crashlytics</a></li>
            </ol>

            <p class="poppin text-justify">
                Perlu diketahui bahwa ada hal-hal tertentu yang tidak menjadi tanggung jawab PT Senna Kreasi Nusa. Fungsi tertentu dari aplikasi akan memerlukan aplikasi untuk memiliki koneksi internet aktif. Koneksi dapat berupa Wi-Fi,
                atau disediakan oleh penyedia jaringan seluler Anda, tetapi PT Senna Kreasi Nusa tidak dapat bertanggung jawab atas aplikasi yang tidak berfungsi secara penuh jika Anda tidak memiliki akses ke Wi-Fi, dan Anda tidak
                memilikinya sisa kuota data Anda.
            </p>

            <p class="poppin text-justify">
                Jika Anda menggunakan aplikasi di luar suatu area dengan Wi-Fi, Anda harus ingat bahwa persyaratan perjanjian Anda dengan penyedia jaringan seluler akan tetap berlaku. Akibatnya, Anda mungkin dikenai biaya oleh penyedia
                seluler Anda untuk biaya data selama koneksi saat mengakses aplikasi, atau biaya pihak ketiga lainnya. Dalam menggunakan aplikasi, Anda menerima tanggung jawab atas biaya tersebut, termasuk biaya data roaming jika Anda
                menggunakan aplikasi di luar wilayah asal Anda (yaitu wilayah atau negara) tanpa mematikan roaming data. Jika Anda bukan pembayar tagihan untuk perangkat tempat Anda menggunakan aplikasi, perlu diketahui bahwa kami berasumsi
                bahwa Anda telah menerima izin dari pembayar tagihan untuk menggunakan aplikasi tersebut.
            </p>

            <p class="poppin text-justify">
                Sejalan dengan itu, PT Senna Kreasi Nusa tidak dapat selalu bertanggung jawab atas cara Anda menggunakan aplikasi, yaitu Anda perlu memastikan bahwa perangkat Anda tetap terisi - jika kehabisan baterai dan Anda tidak dapat
                menyalakannya untuk memanfaatkan Layanan. , PT Senna Kreasi Nusa tidak bisa menerima tanggung jawab.
            </p>

            <p class="poppin text-justify">
                Sehubungan dengan tanggung jawab PT Senna Kreasi Nusa atas penggunaan aplikasi oleh Anda, ketika Anda menggunakan aplikasi, penting untuk diingat bahwa meskipun kami berusaha untuk memastikan bahwa itu diperbarui dan benar
                setiap saat, kami mengandalkan yang ketiga. pihak untuk memberikan informasi kepada kami sehingga kami dapat menyediakannya untuk Anda. PT Senna Kreasi Nusa tidak bertanggung jawab atas kerugian apa pun, langsung atau tidak
                langsung, yang Anda alami sebagai akibat dari mengandalkan sepenuhnya fungsi aplikasi ini.
            </p>

            <p class="poppin text-justify">
                Suatu saat, kami mungkin ingin memperbarui aplikasi. Aplikasi saat ini tersedia di Android - persyaratan untuk sistem (dan untuk sistem tambahan apa pun yang kami putuskan untuk memperluas ketersediaan aplikasi) dapat
                berubah, dan Anda perlu mengunduh pembaruan jika ingin tetap menggunakan aplikasi. PT Senna Kreasi Nusa tidak berjanji akan selalu mengupdate aplikasinya agar relevan dengan anda dan / atau bekerja dengan versi Android yang
                sudah anda install di device anda. Namun, Anda berjanji untuk selalu menerima pembaruan aplikasi saat ditawarkan kepada Anda, Kami mungkin juga ingin berhenti menyediakan aplikasi, dan dapat menghentikan penggunaannya kapan
                saja tanpa memberikan pemberitahuan penghentian kepada Anda. Kecuali jika kami memberi tahu Anda sebaliknya, setelah penghentian, (a) hak dan lisensi yang diberikan kepada Anda dalam persyaratan ini akan berakhir; (b) Anda
                harus berhenti menggunakan aplikasi, dan (jika perlu) menghapusnya dari perangkat Anda.
            </p>
        </div>

        <div class="col-md-12 mt-5">
            <h3 class="text-senna poppin-med">Perubahan Syarat & Ketentuan</h3>
            <p class="poppin text-justify">
                &emsp;&emsp;Kami dapat memperbaharui Syarat dan Ketentuan dari waktu ke waktu. Pembaharuan tersebut akan dipublikasikan di Aplikasi/Situs ini. Oleh karenanya, kami menghimbau Anda untuk mengakses Aplikasi/Situs ini secara
                berkala atas informasi terkini untuk Layanan Senna.
            </p>
        </div>
        <div class="col-md-12 mt-5">
            <h3 class="text-senna poppin-med">Kontak Kami</h3>
            <p class="poppin text-justify">
                <li>Office : Jl. Sekarsuli-Berbah No. 9 Potorono, Banguntapan, Bantul, D.I.Y</li>
                <li>Phone  : +62812-9619-9565, +62813-2566-6958</li>
                <li>E-mail : contact@senna.co.id</li>
            </p>
        </div>
    </div>
</section>

@endsection

@extends('frontend.layout.main')
@section('meta')
    <meta name="description" content="{{$meta_desc}}">
    <meta name="keywords" content="aplikasi manajemen usaha,point of sale,jasa,logistik,pelaku usaha,umkn,senna,
kasir, senna kasir, aplikasi kasir, aplikasi kasir android, software kasir, program kasir, aplikasi kasir toko, aplikasi pos, kasir online, aplikasi program penjualan,
aplikasi akuntansi,akuntansi online, akuntansi terbaik,software akuntansi, software akuntansi terbaik
">
    <meta itemprop="name" content="Senna">
    <meta itemprop="description" content="{{$meta_desc}}">
    <meta itemprop="image" content="{{asset('public')}}/logo/logo-header.png">
    <meta name="twitter:title" content="{{$title}}" />
    <meta name="twitter:description" content="{{$meta_desc}}" />
    <meta name="twitter:url" content="{{route('features')}}" />
    <meta name="twitter:image" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{route('features')}}" />
    <meta property="og:description" content="{{$meta_desc}}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:site_name" content="Senna" />
    <meta property="og:image" itemprop='image' content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:secure_url" content="{{asset('public')}}/logo/logo-header.png" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Senna" />
    <meta property="og:updated_time" content="{{Carbon\Carbon::now()->isoFormat('dddd, D MMMM Y')}}" />
    <meta property="og:type" content="website" />
@endsection
@section('content')
    <section class="section-relative mt-section-header">
        <img src="{{env('S3_URL')}}public/frontend/img/bg-top-right.png" alt="background left" style="top:-100px;" width="24%" class="bg-top__right">
        <div class="container-fluid z-10 position-relative">
            <div class="row">
                <div class="col-md-12 text-center pt-5">
                    <h1 class="main-title">Aplikasi Super Komplit<br>Buat Bisnismu <span class="main-title__in with-border">Makin Maju</span></h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="feature-slider__wrapper mx-auto">
                        <div class="feature-slider mx-auto">
                            
                            <div class="feature-slider-item text-center" id="inventory-nav" onclick="changeFeature('#inventory-nav', '#inventory-wrapper')">
                                <img src="{{env('S3_URL')}}public/frontend/img/home-icon-5.png" alt="inventory">
                                <h2>Inventory</h2>
                            </div>
                            
                            <div class="feature-slider-item text-center" id="manajemen-nav" onclick="changeFeature('#manajemen-nav', '#manajemen-wrapper')">
                                <img src="{{env('S3_URL')}}public/frontend/img/home-icon-3.png" alt="management karyawan">
                                <h2>Management Karyawan</h2>
                            </div>
                            <div class="feature-slider-item text-center active" id="kasir-nav" onclick="changeFeature('#kasir-nav', '#kasir-wrapper')">
                                <img src="{{env('S3_URL')}}public/frontend/img/home-icon-1.png" alt="kasir online">
                                <h2 class="h2-custom">Kasir Online</h2>
                            </div>
                            <div class="feature-slider-item text-center" id="akuntansi-nav" onclick="changeFeature('#akuntansi-nav', '#akuntansi-wrapper')">
                                <img src="{{env('S3_URL')}}public/frontend/img/home-icon-2.png" alt="akuntansi">
                                <h2 class="h2-custom">Akuntansi</h2>
                            </div>
                            <div class="feature-slider-item text-center" id="laporan-nav" onclick="changeFeature('#laporan-nav', '#laporan-wrapper')">
                                <img src="{{env('S3_URL')}}public/frontend/img/home-icon-4.png" alt="laporan">
                                <h2 class="h2-custom">Laporan</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <img src="{{env('S3_URL')}}public/frontend/img/bg-dotted.png" alt="background dotted" class="bg-bottom__center">
    </section>
    
    <section class="section-relative mb-120 z-10 fitur-section" id="kasir-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="fitur-sidebar">
                        <ul class="list-group features-list p-4">
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pengaturan-dan-pengelolaan-toko')"
                                >
                                    Pengaturan dan Pengelolaan Toko
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pengaturan-dan-pengelolaan-barang-jasa')"
                                >
                                    Pengaturan dan Pengelolaan Barang / Jasa
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pengelolaan-stok')"
                                >
                                    Pengelolaan Stok
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pengelolaan-pembelian-dan-penjualan')"
                                >
                                    Pengelolaan Pembelian dan Penjualan
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#manajemen-hutang-dan-pajak')"
                                >
                                    Manajemen Hutang dan Pajak
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#manajemen-diskon')"
                                >
                                    Manajemen Diskon
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pencatatan-transaksi')"
                                >
                                    Pencatatan Transaksi
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 px-4">
                    <div class="mt-4 mb-3" data-aos="fade-right" id="pengaturan-dan-pengelolaan-toko">
                        <h3 class="poppin font-20">Pengaturan dan Pengelolaan Toko</h3>
                        <p class="poppin text-justify">Transaksi yang dilakukan melalui Senna dilengkapi dengan navigasi yang berbeda untuk setiap jenis usaha sehingga pengguna dapat melakukan pencatatan untuk semua jenis pemesanan (bungkus, delivery, dan lain-lain).  Dilengkapi dengan struk digital yang dapat dikirim melalui email dan whatsapp pengunjung dimana pembayaran dapat dilakukan baik secara tunai maupun non tunai.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-1.png')}}" class="img-features" alt="Pengaturan dan Pengelolaan Toko">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-left" id="pengaturan-dan-pengelolaan-barang-jasa">
                        <h3 class="poppin font-20">Pengaturan dan Pengelolaan Barang/Jasa</h3>
                        <p class="poppin text-justify">Pengguna dapat mengatur bagaimana produknya akan digunakan. Produk dapat berupa barang maupun jasa dengan berbagai model bisnis yang disesuaikan dengan kebutuhan bisnis pengguna.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-2.png')}}" class="img-features" alt="Pengaturan dan Pengelolaan Barang Jasa">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-right" id="pengelolaan-stok">
                        <h3 class="poppin font-20">Pengelolaan Stok</h3>
                        <p class="poppin text-justify">Fitur pengelolaan stok berfungsi untuk mengelola stok barang atau jasa mulai dari pengelolaan pergudangan, pendistribusian barang termasuk juga <i>transfer</i> stok antar cabang.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-3.png')}}" class="img-features" alt="Pengelolaan Stok">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-left" id="pengelolaan-pembelian-dan-penjualan">
                        <h3 class="poppin font-20">Pengelolaan Pembelian dan Penjualan</h3>
                        <p class="poppin text-justify">Dalam fitur ini, pengguna dapat menambahkan stok dengan melakukan pembelian, mengatur transaksi penjualan, melihat daftar riwayat pembelian dan penjualan, dan juga melakukan retur pembelian atau penjualan.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-4.png')}}" class="img-features" alt="Pengelolaan Pembelian dan Penjualan">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-right" id="manajemen-hutang-dan-pajak">
                        <h3 class="poppin font-20">Manajemen Hutang dan Pajak</h3>
                        <p class="poppin text-justify">Lakukan pengelolaan piutang dari pelanggan, dan mengecek utang usaha. Selain itu, pengguna juga dapat mengecek laporan pajak dan mengelola pajak usaha.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-5.png')}}" class="img-features" alt="Manajemen Hutang dan Pajak">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-left" id="manajemen-diskon">
                        <h3 class="poppin font-20">Manajemen Diskon</h3>
                        <p class="poppin text-justify">Pemilik usaha dapat mengatur waktu dimana usahanya akan mengadakan diskon (periode promo), memasukan besaran potongan harga, dan sistem akan menyesuaikan pelaporan transaksi berdasarkan harga diskon.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-6.png')}}" class="img-features" alt="Manajemen Diskon">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-right" id="pencatatan-transaksi">
                        <h3 class="poppin font-20">Pencatatan Transaksi</h3>
                        <p class="poppin text-justify">Pengguna dapat mengakses mengenai semua laporan yang berkaitan dengan transaksi dalam usaha Anda, mulai dari pembelian, penjualan, beserta dengan detail transaksi mengenai jumlah barang, harga setiap item, waktu terjadinya transaksi.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-7.png')}}" class="img-features" alt="Pencatatan Transaksi">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-relative mb-120 z-10 fitur-section d-none" id="laporan-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="fitur-sidebar">
                        <ul class="list-group shadow features-list p-4">
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#laporan-umum')"
                                >
                                    Laporan Umum
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#laporan-pembelian-dan-penjualan')"
                                >
                                    Laporan Pembelian dan Penjualan
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#laporan-melalui-back-office-web')"
                                >
                                    Laporan Melalui Back Office Web
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#laporan-modal')"
                                >
                                    Laporan Modal
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#laporan-pengunjung')"
                                >
                                    Laporan Pengunjung
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-8 px-4">
                    <div class="mt-4 mb-3" data-aos="fade-left" id="laporan-umum">
                        <h3 class="font-20">Laporan Umum</h3>
                        <p class="text-justify">Pengguna akan mendapatkan laporan umum yang terdiri dari laporan pembelian, pendapatan kotor (laba kotor), dan  laporan penjualan.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-8.png')}}" class="img-features" alt="Laporan Umum">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-right" id="laporan-pembelian-dan-penjualan">
                        <h3 class="font-20">Laporan Pembelian & Penjualan</h3>
                        <p class="text-justify">Seluruh transaksi yang dilakukan mulai dari pembelian bahan baku, alat, pembayaran listrik dan air, lalu pembelian lainnya, kemudian transaksi penjualan dengan berbagai macam jenis varian produk dan model akan dijadikan dalam satu laporan transaksi pembelian dan penjualan.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-9.png')}}" class="img-features" alt="Laporan Pembelian Penjualan">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-left" id="laporan-melalui-back-office-web">
                        <h3 class="font-20">Laporan melalui Back Office Web</h3>
                        <p class="text-justify">Pada Aplikasi Senna, semua laporan mulai dari laporan pembelian,  laporan modal, laporan pengunjung, hingga laporan shift yang dapat diakses melalui Back Office Website Senna</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-10.png')}}" class="img-features" alt="Laporan melalui Back Office Web">
                        </div>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-right" id="laporan-modal">
                        <h3 class="font-20">Laporan Modal</h3>
                        <p class="text-justify">Pengguna akan mendapatkan fitur laporan modal yang berisi informasi mengenai modal yang dimiliki oleh usaha pengguna, baik berkurang maupun bertambah yang ditulis hingga periode akuntansi selesai.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-10.png')}}" class="img-features" alt="Laporan Modal">
                        </div>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-left" id="laporan-pengunjung">
                        <h3 class="font-20">Laporan Pengunjung</h3>
                        <p class="text-justify">Pengguna akan mendapatkan laporan mengenai berapa orang yang telah mengunjungi tempat usahamu, beserta data lain seperti data diri, waktu kunjungan, dan data lainnya seperti rata-rata jumlah kunjungan. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-relative mb-120 z-10 fitur-section d-none" id="inventory-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="fitur-sidebar">
                        <ul class="list-group shadow features-list p-4">
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pencatatan-stok-harga-modal-harga-jual')"
                                >
                                    Pencatatan Stok, Harga Modal, Harga Jual
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pencatatan-hpp')"
                                >
                                    Pencatatan HPP
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"  
                                    onclick="scrollInto(event, '#mutasi-stok')"
                                >
                                    Mutasi Stok
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#multi-gudang')"
                                >
                                    Multi Gudang
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#stok-opname')"
                                >
                                    Stok Opname
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 px-4">
                    <div style="margin-bottom:45px;" class="mt-3" data-aos="fade-right" id="pencatatan-stok-harga-modal-harga-jual">
                        <h3 class="font-20">Pencatatan Stok, Harga Modal, Harga Jual</h3>
                        <p class="text-justify">Pengguna dapat melakukan pencatatan stok bahan baku dan produk, pencatatan harga modal dan harga jual yang disesuaikan dengan bahan baku produk.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-11.png')}}" class="img-features" alt="Pencatatan Stok">
                        </div>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-left" id="pencatatan-hpp">
                        <h3 class="font-20">Pencatatan HPP</h3>
                        <p class="text-justify">Pengguna akan mendapatkan fitur pencatatan untuk menggambarkan perkiraan biaya yang digunakan  dalam setiap kegiatan suatu produksi baik barang maupun jasa. Mulai dari persediaan awal dan akhir barang, pembelian, dan masih banyak lagi.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-12.png')}}" class="img-features" alt="Pencatatan HPP">
                        </div>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-right" id="mutasi-stok">
                        <h3 class="font-20">Mutasi Stok</h3>
                        <p class="text-justify">Dalam fitur ini pengguna dapat melakukan pemindahan stok  baik antar cabang maupun antar gudang dengan menggunakan Aplikasi Senna.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-13.png')}}" class="img-features" alt="Mutasi Stok">
                        </div>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-left" id="multi-gudang">
                        <h3 class="font-20">Multi Gudang</h3>
                        <p class="text-justify">Pengguna dapat mengatur berbagai gudang melalui satu aplikasi.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-14.png')}}" class="img-features" alt="Multi Gudang">
                        </div>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-right" id="stok-opname">
                        <h3 class="font-20">Stok Opname</h3>
                        <p class="text-justify">Pengguna dapat melakukan perhitungan persediaan stok barang di gudang sebelum dijual.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-15.png')}}" class="img-features" alt="Stok Opname">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-relative mb-120 z-10 fitur-section d-none" id="manajemen-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="fitur-sidebar">
                        <ul class="list-group shadow features-list p-4">
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#manajemen-shift')"
                                >
                                    Manajemen Shift
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#absensi-digital')"
                                >
                                    Absensi Digital
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#otorisasi-dan-akses')"
                                >
                                    Otorisasi dan Akses
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#laporan-shift-dan-absensi')"
                                >
                                    Laporan Shift dan Absensi
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 px-4">
                    <div class="mt-4" style="margin-bottom:45px;" data-aos="fade-left" id="manajemen-shift">
                        <h3 class="font-20">Manajemen Shift</h3>
                        <p class="text-justify">Pemilik usaha dapat mengatur pembagian jadwal staff dan mengecek kinerja staff berdasarkan waktu yang telah dibagi. Kini membagi shift tidak sesusah yang dibayangkan.</p>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-right" id="absensi-digital">
                        <h3 class="font-20">Absensi Digital</h3>
                        <p class="text-justify">Staff dari usaha pengguna dapat melakukan absensi digital melalui smartphone dengan melampirkan foto dan lokasi terkini sehingga  pengguna tidak perlu bertemu secara fisik untuk mengetahui kehadiran staff.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-16.png')}}" class="img-features" alt="Absensi Digital">
                        </div>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-left" id="otorisasi-dan-akses">
                        <h3 class="font-20">Otorisasi dan Akses</h3>
                        <p class="text-justify">Tidak semua fitur dan data pada Aplikasi Senna dapat diakses oleh semua orang. Oleh karena itu dilakukan otorisasi terhadap akses untuk membuka suatu fitur maupun data.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-17.png')}}" class="img-features" alt="Otorisasi dan Akses">
                        </div>
                    </div>

                    <div style="margin-bottom:45px;" data-aos="fade-right" id="laporan-shift-dan-absensi">
                        <h3 class="font-20">Laporan Shift dan Absensi</h3>
                        <p class="text-justify">Pengguna akan mendapatkan laporan yang berisi absensi staff beserta keterangan shift. Hal tersebut dapat membantu pengguna dalam menghitung payroll untuk gaji staff.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-18.png')}}" class="img-features" alt="Laporan Shift dan Absensi">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-relative mb-120 z-10 fitur-section d-none" id="akuntansi-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="fitur-sidebar">
                        <ul class="list-group shadow features-list p-4">
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pencatatan-jurnal-dan-biaya')"
                                >
                                    Pencatatan Jurnal dan Biaya
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#pengaturan-akun')"
                                >
                                    Pengaturan Akun
                                </a>
                            </li>
                            <li class="list-group-item text-left">
                                <a
                                    href="#"
                                    class="arial-rounded"
                                    onclick="scrollInto(event, '#laporan-keuangan')"
                                >
                                    Laporan Keuangan
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 px-4">
                    <div class="mt-4 mb-3" data-aos="fade-left" id="pencatatan-jurnal-dan-biaya">
                        <h3 class="font-20">Pencatatan Jurnal & Biaya</h3>
                        <p class="text-justify">Laporan yang didapatkan oleh pengguna secara otomatis akan terhubung dengan jurnal akuntansi agar pencatatan laporan keuangan yang didapatkan lebih akurat. Mulai dari  kas dan bank, pengeluaran administrasi dan umum, jurnal umum, pendapatan di luar usaha, utang dan piutang usaha.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-19.png')}}" class="img-features" alt="Pencatatan Jurnal Biaya">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-right" id="pengaturan-akun">
                        <h3 class="font-20">Pengaturan Akun</h3>
                        <p class="text-justify">Pada pengaturan akun, pengguna dapat memasukkan data saldo awal yang dimiliki. Kemudian pengguna juga akan format arus kas, kategori dan pemetaan akun, periode akuntansi,  dan daftar akun.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-20.png')}}" class="img-features" alt="Pengaturan Akun">
                        </div>
                    </div>

                    <div class="mb-3" data-aos="fade-left" id="laporan-keuangan">
                        <h3 class="font-20">Laporan Keuangan</h3>
                        <p class="text-justify">Pengguna akan mendapatkan laporan keuangan yang komprehensif sehingga pengguna dapat mengetahui kondisi keuangan usaha dengan mengetahui laporan keuangan. Mulai dari arus kas,  buku besar, jurnal, neraca saldo, posisi keuangan, dan laba rugi.</p>
                        <div class="text-center">
                            <img loading="lazy" src="{{asset('public/frontend/img/phone-21.png')}}" class="img-features" alt="Laporan Kuangan">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>

        const changeFeature = (id, sectionId) => {
            const featureSlider = document.querySelectorAll('.feature-slider-item');
            const featureSection = document.querySelectorAll('.fitur-section');
            const targetEl = document.querySelector(id);
            const targetSection = document.querySelector(sectionId);

            featureSlider.forEach(item => {
                item.classList.remove('active');
            });
            targetEl.classList.add('active');

            featureSection.forEach(item => {
                item.classList.add('d-none');
                item.classList.remove('d-block');
            });


            targetSection.classList.remove('d-none');
            targetSection.classList.add('d-block');
        }

        const scrollInto = (event, id) => {
            event.preventDefault();
            const targetPage = document.querySelector(id);
            const targetTittle = document.querySelectorAll('h3.font-20');

            targetTittle.forEach(item => {
                item.classList.remove('feature-title-active');
            });

            targetPage.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
            targetPage.querySelector('h3').classList.add('feature-title-active');
        }

        document.querySelector("main").style.overflow = "visible";
        document.querySelector('body').style.overflowX = "visible";
    </script>
@endsection

<nav class="navbar navbar-expand-mx main-nav navigation navigation-fixed sidebar-left">
    <div class="container-fluid nav-padding">
        <button class="navbar-toggler" type="button" aria-label="toggler">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="{{route('home')}}" class="navbar-brand">
            <img src="{{env('S3_URL')}}public/frontend/img/logo.png" alt="Logo"  class="logo logo-sticky">
        </a>
        <div class="collapse navbar-collapse " id="main-navbar">
            <div class="sidebar-brand">
                <a href="{{route('home')}}" class="d-flex align-items-center justify-content-center">
                    <img src="{{env('S3_URL')}}public/frontend/img/logo.png" class="Logo img-fluid" alt="Logo big">
                </a>
            </div>
            <ul class="nav navbar-nav ml-auto d-flex align-items-lg-center">
                <li class="nav-item dropdown">
                    <button class="nav-link dropdown-toggle arial-rounded" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Selangkah Lebih Maju</button>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item arial-rounded" href="{{route('features')}}">Penjelasan Fitur</a>
                        {{--<a class="dropdown-item arial-rounded" href="">Ekosistem Digital</a>---}}
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link arial-rounded" href="{{route('solution')}}">Solusi Usaha</a>
                </li>

                <li class="nav-item dropdown">
                    <button class="nav-link dropdown-toggle arial-rounded" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Inspirasi</button>
                    <div class="dropdown-menu" aria-labelledby="dropdown02">
                        <a class="dropdown-item arial-rounded" href="{{route('blog')}}">Blog</a>
                        <a class="dropdown-item arial-rounded" href="{{route('rumah-senna')}}">Rumah Senna</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link arial-rounded"  href="{{route('package')}}">Harga</a>
                </li>
                <li class="nav-item dropdown">
                    <button class="nav-link dropdown-toggle arial-rounded" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Lainnya</button>
                    <div class="dropdown-menu" aria-labelledby="dropdown03">
                        <a class="dropdown-item arial-rounded" href="{{route('hardware')}}">Perangkat</a>
                        <a class="dropdown-item arial-rounded" href="{{route('contact')}}">Hubungi Kami</a>
                        <a class="dropdown-item arial-rounded" href="{{route('faq')}}">FAQ</a>
                    </div>
                </li>
                
                <li class="nav-item">
                    <a class="btn btn-orange-soft radius-9 arial-rounded" href="{{route('login')}}" style="margin-right:10px">Masuk</a>
                </li>

                 <li class="nav-item">
                    <a href="{{route('register')}}" class="btn btn-orange radius-9 arial-rounded">Coba Aja Dulu</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<main>

<script>
    var e = $(".main-nav");
    $(".navbar-toggler", e).on("click", function () {
        e.toggleClass("navbar-expanded");
    });

    $('.card-home__feature').hover(function(){
        $(this).toggleClass('active');
    });

    $('.card-home__feature').on('click', function(){
        $(this).addClass('active');
    });

    const navContainer = document.querySelector('.navbar');

    const lastSegment = window.location.href.split("/").pop();

    if(lastSegment == ''){
        navContainer.classList.remove("shadow-sm");
        navContainer.classList.remove("border-top");
    }

    window.addEventListener("scroll", function(){
        navContainer.classList.toggle("fixed-top", window.scrollY > 0);
        if(window.scrollY > 0){
            navContainer.classList.add("bg-white");
            navContainer.classList.add("shadow-sm");
        } else {
            navContainer.classList.remove("bg-white");
            navContainer.classList.remove("shadow-sm");
        }
    });

</script>

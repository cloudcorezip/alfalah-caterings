<section class="section-relative mb-5 z-10">
        <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/bg-section-price.png" alt="background icon" class="img-fluid footer-banner">
        <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/bg-section-price-mobile.png" alt="background icon" class="img-fluid footer-banner-mobile">
    </section>
</main>

<!-- Footer -->
<footer>
    <div class="container-fluid section-padding">
        <div class="row">
            <div class="col-lg-4 col-md-12 mb-5">
                <img src="{{env('S3_URL')}}public/frontend/img/logo.png" class="mb-3" alt="footer logo" width="30%">
                <p>Dapatkan <b>OneStop Solutions</b> dalam mengelola usaha dengan fitur aplikasi terintegrasi dengan pembukuan laporan keuangan yang super lengkap</p>
                <p>#CobaAjaDulu</p>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-6">
                        <a href="https://play.google.com/store/apps/details?id=com.senna_store" target="_blank" rel="noreferrer">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/playstore.png" alt="playstore logo" class="img-fluid" width="100%">
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-6">
                        <a href="https://apps.apple.com/id/app/senna/id1573939224?mt=8" target="_blank" rel="noreferrer">
                            <img loading="lazy" src="{{env('S3_URL')}}public/frontend/img/appstore.png" alt="appstore logo" class="img-fluid" width="100%">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 mb-5">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-6 mb-3">
                        <h4 class="to-h6 mb-2">Produk</h4>
                        <ul class="footer-nav">
                            <li>
                                <a class="py-4 pr-2" href="{{route('features')}}">Penjelasan Fitur</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('hardware')}}">Perangkat</a>
                            </li>
                            <li>
                                <a class="py-4 pr-4" href="{{route('package')}}">Harga</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3 col-sm-6 col-6 mb-3">
                        <h4 class="to-h6 mb-2">Sekilas Usaha</h4>
                        <ul class="footer-nav">
                            <li>
                                <a class="py-4 pr-2" href="{{route('solution.detail', ['page' =>'retail'])}}">Toko Retail</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('solution.detail', ['page' =>'cafe'])}}">Cafe & Restoran</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('solution.detail', ['page' =>'fnb'])}}">FnB</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('solution.detail', ['page' =>'elektronik'])}}">Elektronik</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('solution.detail', ['page' =>'fashion'])}}">Fashion & Accesories</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('solution.detail', ['page' =>'otomotif'])}}">Otomotif</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('solution.detail', ['page' =>'laundry'])}}">Laundry</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('solution.detail', ['page' =>'barbershop'])}}">Salon & Barbershop</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3 col-sm-6 col-6 mb-3">
                        <h4 class="to-h6 mb-2">Senna</h4>
                        <ul class="footer-nav">
                            <li>
                                <a class="py-4 pr-2" href="{{route('about')}}">Tentang Kami</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('contact')}}">Kontak</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('policy')}}">Kebijakan Privasi</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="{{route('term')}}">Syarat & Ketentuan</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="https://bit.ly/manualMobileSennaKasir">Manual Book Mobile</a>
                            </li>
                            <li>
                                <a class="py-4 pr-2" href="https://bit.ly/manualWebSennaKasir">Manual Book Web</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3 col-sm-6 col-6 mb-3">
                        <h4 class="to-h6 mb-2">Tetap Terhubung</h4>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <a title="link to facebook" href="https://www.facebook.com/sennaidn" target="_blank" rel="noreferrer" class="btn btn-ifooter footer-i-smd">
                                <i class="fa fa-facebook"></i>
                            </a>&nbsp;&nbsp;
                            <a title="link to twitter" href="#" class="btn btn-ifooter footer-i-smd">
                                <i class="fa fa-twitter"></i>
                            </a>&nbsp;&nbsp;
                            <a title="link to instagram" href="https://www.instagram.com/sennaindonesia/" target="_blank" rel="noreferrer" class="btn btn-ifooter footer-i-smd">
                                <i class="fa fa-instagram"></i>
                            </a>&nbsp;&nbsp;
                            <a title="link to youtube" href="https://www.youtube.com/channel/UCooO_WCjQXQQZY5p-jvrjkw" target="_blank" rel="noreferrer" class="btn btn-ifooter footer-i-smd">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </div>
                        <ul class="footer-nav">
                            <li class="mb-2">(+62)813-2566-6958</li>
                            <li class="mb-2">contact@senna.co.id</li>
                        </ul>
                        <h4 class="to-h6">Kantor Pusat</h4>
                        <p class="sub-p">Sekarsuli-Berbah No.9, Potorono, Banguntapan, Bantul Regency, Special Region of Yogyakarta</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row section-padding" style="background-color: #FDFDFD;">
            <div class="col-md-12 d-flex align-items-center">
                <p style="margin-top: 1rem;">Copyright @ {{date('Y')}} Senna Indonesia. <b>All Rights Reserved</b></p>
            </div>
        </div>
    </div>
    <div id="whatsapp-button"></div>
    <script>
        whatsappchat.multipleUser(
            {
                selector: '#whatsapp-button',
                users: [
                    {
                        name:'Miftah Wulandari',
                        phone: '6281329353576',
                    },
                    {
                        name:'Dina Mahmudi',
                        phone: '6281325666958',

                    },
                ],
                headerMessage: 'Jangan ragu untuk mengajukan pertanyaan!',
                chatBoxMessage: 'Kami akan segera membalasnya',
                color: '#FF9344',
            }
        );
    </script>
</footer>
{{--<style>--}}
{{--    .float{--}}
{{--        position:fixed;--}}
{{--        width:60px;--}}
{{--        height:60px;--}}
{{--        bottom:40px;--}}
{{--        right:40px;--}}
{{--        background-color: #FF9344;--}}
{{--        color:#FFF;--}}
{{--        border-radius:50px;--}}
{{--        text-align:center;--}}
{{--        font-size:30px;--}}
{{--        box-shadow: 2px 2px 3px #999;--}}
{{--        z-index:100;--}}
{{--    }--}}

{{--    .my-float{--}}
{{--        margin-top:16px;--}}
{{--    }--}}
{{--</style>--}}
{{--<a  target="_blank" href="https://api.whatsapp.com/send?phone=6281325666958&text=Saya%20ingin%20menanyakan%20tentang%20layanan%20Senna%20?" class="float">--}}
{{--    <i class="fa fa-whatsapp my-float"></i>--}}
{{--</a>--}}

</body>
</html>

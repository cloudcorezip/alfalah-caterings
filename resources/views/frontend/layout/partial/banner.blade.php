<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="background-color:transparent;border:none;">
            <div class="modal-body">
                <button
                    type="button"
                    class="close button-close-modal"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
                <a href="{{url('https://play.google.com/store/apps/details?id=com.senna_store')}}">
                    <img src="{{asset('public/frontend/img/banner-popup.png')}}" class="img-fluid" alt="banner pop" style="border-radius:13px;">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" style="background-color:transparent;border:none;">
            <div class="modal-body">
                <button type="button" class="close button-close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <a href="{{route('package')}}">
                    <img src="{{asset('public/frontend/img/banner-promo.png')}}" class="img-fluid" alt="modal" style="border-radius:13px;">
                </a>
            </div>
        </div>
    </div>
</div>

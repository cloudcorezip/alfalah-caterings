<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

return [
    'registration_success'=>'Registrasimu berhasil,silahkan cek emailmu atau di folder spam untuk melakukan aktivasi akun!',
    'registration_errors'=>'Mohon maaf telah terjadi kesalahan pada saat registrasi,Tunggu beberapa saat dan ulangi kembali',
    'email_already_use'=>'Emailmu telah digunakan oleh akun lain',
    'account_not_found'=>'Yah,sistem kami tidak menemukan akunmu',
    'account_not_found_fill'=>'Oops maaf sepertinya sistem kami tidak menemukan akun yang kamu tuju',
    'scan_qr_error'=>'Oops maaf sepertinya sepertinya ada kesalahan,periksa lagi saldo akun atau tunggu beberapa saat dan lakukan kembali',
    'login_success'=>'Login berhasil,silahkan memulai sesimu',
    'errors'=>'Telah terjadi kesalahan,tunggu beberapa saat lagi..',
    'add_success'=>'Data telah berhasil ditambahkan',
    'pin_error'=>'Oops,pin kamu salah...',
    'account_email_confirmation'=>'Mohon maaf akunmu belum aktif, silahkan buka emailmu untuk melakukan aktivasi akun',
    'account_is_active'=>'Akunmu sekarang sudah aktif,silahkan login diaplikasi untuk memulai sesi',
    'account_is_active_success'=>'Aktivasi akun telah berhasil,silahkan login diaplikasi untuk memulai sesi',
    'email_not_found'=>'Email yang kamu masukkan tidak terdapat disistem kami',
    'forgot_password_success'=>'Silahkan cek emailmu untuk melakukan perubahan password',
    'forgot_pin_success'=>'Silahkan cek emailmu untuk melakukan perubahan pin',
    'update_profile_success'=>'Pembaharuan profil telah berhasil dilakukan',
    'update_password_success'=>'Pembaharuan password telah berhasil dilakukan',
    'update_pin_success'=>'Pembaharuan pin telah berhasil dilakukan',
    'update_email_success'=>'Pembaharuan email telah berhasil dilakukan,cek emailmu untuk melakukan verifikasi',
    'update_foto_success'=>'Pembaharuan foto telah berhasil dilakukan',
    'active_senna_pay'=>'Selamat akun SennaPay kamu telah aktif,sekarang kamu bisa melakukan topup dan fitur menarik lainnya',
    'manual_topup'=>'Terima kasih sudah melakukan topup secara manual,dana kamu akan segera masuk dan nikmati fitur yang tersedia di aplikasi kami,Jika dana belum masuk kamu bisa hubungi kami di nomor +62812 9619 9565',
    'scan_success'=>'Yey transaksi yang kamu lakukan berhasil',
    'manual_wd'=>'Terima kasih sudah melakukan penarikan secara manual,dana kamu akan segera masuk dan nikmati fitur yang tersedia di aplikasi kami',
    'is_approved_manual'=>'Data sudah di setujui',
    'phone_number_success'=>'Pembaharuan nomor telpon berhasil',
    'not_allowed'=>'Mohon maaf kamu tidak diperkenankan mengakses halaman ini',
    'company_success'=>'Pengisian biodata usaha berhasil disimpan,silahkan memulai usahamu',
    'foto_company_success'=>'Pembaharuan foto usaha berhasil disimpan',
    'merchant_exists'=>'Mohon maaf kamu telah mengisi detail usaha,silahkan menggunakan fitur update untuk bisnismu',
    'upgrade_senna_pay'=>'Pengisian data sudah berhasil,tunggu sistem kami memverifikasi terkait upgrade senna pay premiummu',
    'success_pay_not_repeat'=>'Pembayaran untuk transaksi ini sudah dilakukan',
    'pay_subscription_fee'=>'Selamat,pembayaran berlangganan sudah berhasil,kamu berlangganan sampai dengan :end_date ,nikmati fitur yang telah kami berikan',
    'pay_subscription_is_active'=>'Opps,kamu masih aktif berlangganan',
    'branch_not_found'=>'Cabang yang dituju tidak ditemukan dalam sistem kami',
    'branch_success'=>'Penambahan cabang berhasil,tunggu beberapa saat agar cabang yang dituju mengkonfirmasi',
    'accepted_branch'=>'Selamat konfirmasi menjadi cabang dari :merchant telah berhasil',
    'add_staff_success'=>'Penambahan staff berhasil,staff kamu bisa melakukan login dari aplikasi',
    'max_subs'=>'Oops sepertinya untuk paket ini kamu sudah melewati batas yang tersedia,silahkan upgrade ke paket yang lebih tinggi',
    'pay_subscription_failed'=>'Yah,pembayaran berlanggananmu gagal,coba lagi beberapa saat lagi atau hubungi kami ya',

];


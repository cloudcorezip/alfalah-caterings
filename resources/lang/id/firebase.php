<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */


return[
    'topup_manual'=>'Topup manual dengan kode transaksi :code berhasil dilakukan,saldo akan masuk beberapa saat lagi',
    'wd_manual'=>'Penarikan manual dengan kode transaksi :code berhasil dilakukan,tunggulah proses transfer direkeningmu',
    'topup_manual_confirmation'=>'Topup manual dengan kode transaksi :code  :status ,cek saldomu',
    'wd_manual_confirmation'=>'Penarikan manual dengan kode transaksi :code  :status ,silahkan cek rekeningmu',
    'pay_success'=>'Selamat,pembayaran dengan kode transaksi :code senilai :nominal berhasil dilakukan',
    'pay_success_received'=>'Pembayaran dengan kode transaksi :code senilai :nominal berhasil diterima,silahkan cek saldomu',
    'fill_success'=>'Selamat,kirim saldo dengan kode transaksi :code senilai :nominal berhasil dilakukan',
    'fill_success_received'=>'Kamu baru saja mendapatkan kiriman saldo dari :user senilai :nominal dengan kode transaksi :code',
    'topup_success'=>'Pengisian saldo dengan kode transaksi :code sejumlah :amount berhasil dilakukan,cek saldomu untuk melihatnya',
    'topup_failed'=>'Pengisian saldo dengan kode transaksi :code sejumulah :amount gagal dilakukan',
    'wd_success'=>'Penarikan saldo dengan kode transaksi :code sejumlah :amount berhasil dilakukan',
    'wd_failed'=>'Penarikan saldo dengan kode transaksi :code sejumlah :amount gagal dilakukan',
    'order_is_packing'=>'Pesananmu dengan kode :code sedang dikemas oleh Toko',
    'order_looking_for_driver'=>'Pesananmu sedang dicarikan driver,tunggulah beberapa saat lagi',
    'order_not_approved'=>'Oops,Pesananmu dengan kode :code ditolak,coba order yang lain',
    'order_is_send'=>'Pesananmu dengan kode :code sedang dikirim,pantau terus pesananmu',
    'order_is_waiting_for_take'=>'Pesanan dengan kode :code sedang menunggu pengambilanmu',
    'order_is_send_with_courier'=>'Pesananmu dengan kode :code sedang dikirim dengan nomor resi :resi_code,pantau terus pesananmu',
    'order_is_done'=>'Pesanan dengan kode :code sudah sampai tujuan',
    'order_comission_driver'=>'Komisi dari pengiriman dengan kode :code senilai :amount sudah masuk,cek saldo kamu',
    'order_in'=>'Ada orderan masuk nih dengan kode :code,check segera mungkin atau abaikan jika tidak mau menerima',
    'order_in_to_driver'=>'Ada orderan masuk nih dengan kode :code,check segera mungkin atau abaikan jika tidak mau menerima',
    'order_rejected_user'=>'Opps pesanan dengan kode :code dibatalkan pemesanan',
    'work_is_done'=>'Yey,pekerjaanmu dengan kode :code sudah selesai tunggu beberapa saat untuk mendapatkan upahnya',
    'order_approved'=>'Yey,pesananmu dengan kode :code sudah disetujui penyedia jasa',
    'to_location'=>'Hey, penyedia jasa dengan kode pesanan :code sedang menuju lokasimu',
    'work_is_done_to_user'=>'Yey,pekerjaan dengan kode :code telah selesai dikerjakan cek apakah benar-benar selesai',
    'get_the_driver'=>'Yey pesananmu dengan kode :code berhasil mendapatkan driver',
    'to_location_merchant'=>'Driver sedang menuju ke toko/restoran dengan pesananmu berkode :code',
    'driver_to_location'=>'Hey,driver dengan pesananmu berkode :code sedang menuju lokasimu pastikan kamu standby ya',
    'driver_arrived'=>'Hey,driver sudah sampai di lokasimu nih!,selamat ya',
    'merchant_to_location'=>'Hey pesananmu berkode :code sedang menuju lokasimu pastikan kamu standby ya',
    'merchant_arrive'=>'Hey pesananmu berkode :code telah tiba',
    'ar_due_date'=>'Kasbonmu ditoko :merchant_name dengan kode transaksi :code dengan nominal :residual_amount akan jatuh tempo ditanggal :due_date, Silahkan dibayar ya'




];

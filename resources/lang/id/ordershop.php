<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

return [
    'shop_success'=>'Pembuatan order telah berhasil disimpan,silahkan isi detail order yang baru saja kamu buat',
    'shop_update_success'=>'Update order telah berhasil disimpan,silahkan isi detail order yang baru saja kamu buat',
    'pay_order_success'=>'Selamat pembayaran untuk pemesanan yang kamu lakukan berhasil,Tunggu penyedia untuk menyetujuinya',
    'input_resi'=>'Pengisian resi berhasil,sistem akan mengirimkan informasi ke pengguna',
    'already_pickup'=>'Pengisian resi berhasil,sistem akan mengirimkan informasi ke pengguna',
    'not_shipping'=>'Order bukan termasuk pengantaran ini',
    'out_of_stock'=>'Stok produk tidak mencukupi untuk melakukan transaksi ini',
    'offline_shop_success'=>'Pembuatan order berhasil disimpan,untuk pembayaran melalui senna pay lanjutkan dengan scan qr code pada struk transaksi yang tersedia',
    'cust_not_found'=>"Maaf pelanggan ini tidak memakai Aplikasi Senna",
    'purchase_order_create'=>'Pembelian berhasil disimpan',
    'po_retur'=>'Pembuatan retur pembelian berhasil disimpan',
    'so_retur'=>'Pembuatan retur penjualan berhasil disimpan',
    'so_create'=>'Penjualana berhasil disimpan'
];

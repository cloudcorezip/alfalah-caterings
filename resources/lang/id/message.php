<?php
/**
 * MitraAnakNegeri
 * Copyright (c) 2020.
 */

return[
    '200'=>'Data berhasil disimpan !',
    '201'=>'Pengambilan data berhasil!',
    '404'=>'Data tidak tersedia!',
    '302'=>'Data berhasil di perbaharui !',
    '500'=>'Terjadi kesalahan! Data gagal disimpan!',
    '202'=>'Data berhasil dihapus!',
    '405'=>'Terjadi kesalahan! Data gagal dihapus!',
    '501'=>'Operasi tidak diizinkan! Data digunakan oleh entitas lain',
];




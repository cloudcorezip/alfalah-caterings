<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

return[
  'order_success'=>'Selamat pembayaran untuk pemesanan jasa yang kamu lakukan berhasil,Tunggu penyedia jasa untuk menyetujuinya',
    'order_error'=> 'Oops maaf sepertinya sepertinya ada kesalahan,periksa lagi saldo senna pay kamu atau tunggu beberapa saat dan lakukan kembali',
    'transaction_type_not_available'=>'Mohon maaf tipe pembayaran ini belum bisa digunakan pada layanan ini',
    'approved_confirm_provider'=>'Selamat pemesananmu telah disetujui oleh penyedia jasa,silahkan tunggu penyedia jasa datang ke tempatmu sesuai waktu yang kamu tentukan diawal',
    'rejected_confirm_provider'=>'Yah sepertinya penyedia jasa/toko yang kamu pesan sedang sibuk,coba cari penyedia jasa/toko lainnya',
    'approved_order_provider'=>'Kamu baru saja menyetujui pemesanan,untuk langkah selanjutnya kamu harus datang ke detail pemesanan tersebut',
    'rejected_order_provider'=>'Kamu baru saja membatalkan pemesanan,kami akan memberikan informasi kepada pemesan terkait hal tersebut',
    'user_cannot_canceled_order'=>'Maaf kamu tidak dapat membatalkan pemesananmu karena telah disetujui penyedia jasa',
    'canceled_user_success'=>'Proses pembatalan pemesanan sudah berhasil,tunggu beberapa saat hingga saldomu kembali',
    'provider_to_location'=>'Pembaharuan berhasil,kamu akan menuju tempat pemesan untuk menyelesaikan pekerjaan',
    'done_service_item'=>'Selamat,kamu baru saja menyelesaikan satu item pekerjaan pada order tersebut',
    'rating_done'=>'Terima kasih sudah memberikan rating untuk pesanan terkait',
    'rating_done_exists'=>'Oops kamu sudah memberikan rating pada pesanan ini',
    'job_not_done'=>'Oops pekerjaan ini belum selesai,apakah kamu yakin memberikan nilai?',
    'order_refund_error'=>'Mohon maaf telah terjadi kesalahan terkait proses refund,tunggulah beberapa saat lagi',
    'no_repeat_cancel_user'=>'Mohon maaf proses cancel sudah berjalan tunggulah beberapa saat proses pengembalian saldo diakunmu',
    'check_cod_balance'=>'Pastikan saldo kamu cukup untuk menyelesaikan pekerjaan ini',
    'error_get_payment_from_merchant'=>'Terjadi kesalahan saat menekan tombol pekerjaan selesai,ulangi beberapa saat lagi',
    'error_done_order'=>'Kamu sudah menyelesaikan pekerjaan ini',
    'error_rejected_order_provider'=>'Terjadi kesalahan saat membatalkan pesanan,Tunggulah beberapa saat lagi',
    'no_repeat_cancel'=>'Mohon maaf proses cancel sudah berjalan!',
    'no_repeat_driver'=>'Opps order ini sudah di ambil driver lain,Maaf ya',
    'no_repeat_step'=>'Opps kamu sudah melakukan step ini',
    'approved_driver'=>'Yey kamu berhasil menyetujui pengiriman ini',
    'step_success'=>'Yeh kamu berhasil menyimpan step ini'





];

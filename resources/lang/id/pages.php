<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */
return [
    'not_allowed_title'=>'Halaman Tidak Boleh Di Akses',
    'not_allowed_sub_title'=>'Mohon maaf kamu tidak diperkenankan untuk mengakses halaman tersebut',
    'not_allowed_desc'=>'URL mungkin salah eja atau halaman yang kamu cari tidak lagi tersedia. Jika kamu merasa tiba di sini karena kesalahan kami, mohon kontak kami',
    'return_back'=>'Kembali ke Halaman Sebelumnya'

];

const mix = require('laravel-mix');
require('dotenv').config();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//frontend
mix.styles([
        'public/frontend/css/bootstrap.min.css',
        'public/frontend/css/navbar.css',
        'public/frontend/css/app.css',
    'public/whatsapp-chat/whatsapp-chat.css'
    ],
    'public/frontend/css/frontend.min.css').version();

mix.scripts([
        'public/frontend/js/jquery-3.6.0.min.js',
        'public/frontend/js/bootstrap.min.js',
    'public/whatsapp-chat/whatsapp-chat.js'
    ],
    'public/frontend/js/frontend.min.js').version();

//backend

mix.styles([
        'public/backend/js/vendor/bundle.css',
        'public/backend/css/app.min.css',
        'public/backend/css/auth.css',
        'public/backend/css/google-button.css',
        'public/backend/css/custom.css',
        'public/backend/css/font-awesome.css',
        'public/backend/css/jquery.datetimepicker.css',
        'public/backend/css/vendor/datatables/dataTables.bootstrap4.min.css',
    ],
    'public/backend/css/backend.min.css').version();


mix.styles([
        'public/backend/js/vendor/bundle.css',
        'public/backend/css/app.min.css',
        'public/backend/css/auth.css',
        'public/backend/css/google-button.css',
        'public/backend/css/clinic.css',
        'public/backend/css/font-awesome.css',
        'public/backend/css/jquery.datetimepicker.css',
        'public/backend/css/vendor/datatables/dataTables.bootstrap4.min.css',
    ],
    'public/backend/css/backend-clinic.min.css').version();

mix.scripts([
        'public/backend/js/vendor/bundle.js',
        'public/backend/js/app.min.js',
        'public/backend/js/vendor/jquery-easing/jquery.easing.min.js',
        'public/backend/js/vendor/datatables/jquery.dataTables.min.js',
        'public/backend/js/vendor/datatables/dataTables.bootstrap4.min.js',
        'public/backend/js/ajax.js',
        'public/backend/js/datatable.js',
        'public/backend/js/accounting.js',
        'public/backend/js/jquery.datetimepicker.js',
        'public/backend/js/tour.js'
    ],
    'public/backend/js/backend.min.js').version();

//toast-custom

mix.scripts([
        'public/backend/js/toast-custom.js'
    ],
    'public/backend/js/toast-custom.min.js').version();

mix.js('resources/js/laravel-echo.js', 'public/js/laravel-echo.js')
    .sass('resources/sass/app.scss', 'public/css');

LARAVEL_ECHO_PORT=process.env.LARAVEL_ECHO_PORT


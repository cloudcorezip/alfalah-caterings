var active_modal = "";
var modal_delay = 300;
var baseURL = "";
var fnPositiveButton, fnNegativeButton;
var csrfParam = '_token';
var timezoneParam ='_timezone';

function ajaxTransfer(url, data, callback, asJson, hide = true) {
    asJson = typeof asJson == 'undefined' ? false : true;
    var currentTS = Math.floor(new Date().getTime() / 1000);
    if (!navigator.onLine) {
        if (typeof callback === 'string') {
            $(callback).html(alertDanger('Terjadi kesalahan! Kamu sedang dalam kondisi offline atau tidak terhubung dengan internet'))
        }
        return !1
    }
    if (data instanceof FormData) {
        data.append('browser_url', document.URL);
        data.append('client_timestamp', currentTS)
    } else if (typeof data === 'object') {
        data.browser_url = document.URL;
        data.client_timestamp = currentTS
    } else {
        try {
            data.browser_url = document.URL;
            data.client_timestamp = currentTS
        } catch (e) {
            console.log(e)
        }
    }

    if (asJson) {
        ajaxAsJson(url, data, callback, hide);
    } else {
        ajaxAsXhr(url, data, callback, hide);
    }
}

function alertDanger(msg) {
    return "<div class='alert alert-danger text-center'>" + msg + "</div>"
}

function ajaxAsJson(url, data, callback, hide = true) {
    var response, csrfToken,timezone;
    url = baseURL + url;
    if (url.substring(0, 2) == '//') {
        url = url.substring(1, url.length);
    }

    if ((data instanceof FormData)) {
        data = Object.toFormData(data);
        data = data.serializeArray();
    }

    csrfToken = $('input[name=' + csrfParam + ']').val();
    timezone =$('input[name=' + timezoneParam + ']').val();
    data = {
        json_data: JSON.stringify(data)
    }
    data[csrfParam] = csrfToken;
    data[timezoneParam]=timezone;

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function(response) {
            if (typeof callback == 'string') {
                $(callback).html(response);
            } else {
                callback(response);
            }

            setInputPlaceholder();
            validateRequiredInput();
            if (hide)
                hideLoading();
        },
        error: function(e) {
            if (typeof callback == 'string') {
                $(callback).html(response);
            }

            console.log(e);
            if (hide)
                hideLoading();
        }
    })
}

function ajaxAsXhr(url, data, callback, hide = true) {
    var response, csrfToken,timezone;
    url = baseURL + url;
    if (url.substring(0, 2) == '//') {
        url = url.substring(1, url.length);
    }

    if (!(data instanceof FormData)) {
        data = Object.toFormData(data);
    }

    csrfToken = $('input[name=' + csrfParam + ']').val();
    timezone =$('input[name=' + timezoneParam + ']').val();
    data.append(csrfParam, csrfToken);
    data.append(timezoneParam,timezone);

    showLoading();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function() {
        response = xhr.responseText;
        if (xhr.status === 200) {
            if (typeof callback == 'string') {
                $(callback).html(response);
            } else {
                callback(response);
            }

            setInputPlaceholder();
            validateRequiredInput();
        } else {
            if (typeof callback == 'string') {
                $(callback).html(response);
            }

            console.log('ajax error! status : ' + xhr.status);
            console.log(xhr);
        }
        if (hide)
            hideLoading();
    };
    xhr.send(data);
}


function customAjaxAsXhr(url, data, callback) {
    var response, csrfToken,timezone;
    url = baseURL + url;
    if (url.substring(0, 2) == '//') {
        url = url.substring(1, url.length);
    }

    if (!(data instanceof FormData)) {
        data = Object.toFormData(data);
    }

    csrfToken = $('input[name=' + csrfParam + ']').val();
    timezone =$('input[name=' + timezoneParam + ']').val();
    data.append(csrfParam, csrfToken);
    data.append(timezoneParam,timezone);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function() {
        response = xhr.responseText;
        if (xhr.status === 200) {
            if (typeof callback == 'string') {
                $(callback).html(response);
            } else {
                callback(response);
            }

        } else {
            if (typeof callback == 'string') {
                $(callback).html(response);
            }

            console.log('ajax error! status : ' + xhr.status);
            console.log(xhr);
        }
    };
    xhr.send(data);
}


function modalAlert(title, message) {
    $('.modal-backdrop, #modal-target.modal').remove();

    var modal_box = "";
    modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog' tabindex='-1' id='modal-target' class='modal fade' style='display: none;'>";
    modal_box += "<div class='modal-dialog'>";
    modal_box += "<div class='modal-content'>";
    modal_box += "<div class='modal-header' style='background: #ff5252'>";
    modal_box += "<h4 class='modal-title' style='color: white'>" + title + "</h4>";
    modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='removeModal(this)' rel='modal-target' type='button'><span aria-hidden='true'>&times;</span></button>";
    modal_box += "</div>";
    modal_box += "<div id='modal-output' class='modal-body'>" + message + "</div>";
    modal_box += "</div>";
    modal_box += "</div>";
    modal_box += "</div>";

    $('html').append(modal_box);
    $('#modal-target').modal('show');
}

function modalConfirm(title, message, positiveButton, negativeButton) {
    if (typeof(positiveButton) == 'function') {
        fnPositiveButton = positiveButton;
    }
    if (typeof(negativeButton) == 'function') {
        fnNegativeButton = negativeButton;
    }

    $('.modal-backdrop, #modal-target.modal').remove();

    var modal_box = "";
    modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog' tabindex='-1' id='modal-target' class='modal fade' style='display: none;'>";
    modal_box += "<div class='modal-dialog'>";
    modal_box += "<div class='modal-content'>";
    modal_box += "<div class='modal-header'>";
    modal_box += "<h4 class='modal-title'>" + title + "</h4>";
    modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='removeModal(this)' rel='modal-target' type='button'><span aria-hidden='true'>x</span></button>";modal_box += "</div>";
    modal_box += "<div id='modal-output' class='modal-body'><p>" + message + "</p></div>";
    modal_box += "<div class='modal-footer'><div id='confirm-button-action'><a onclick='negativeButtonClick()' class='btn btn-danger btn-sm'>Cancel</a><a onclick='positiveButtonClick()' class='btn btn-success btn-sm'>Ok</a></div></div>";
    modal_box += "</div>";
    modal_box += "</div>";
    modal_box += "</div>";

    $('html').append(modal_box);
    $('#modal-target').modal('show');
}

function positiveButtonClick() {
    if (typeof(fnPositiveButton) != 'function') {
        return false;
    } else {
        fnPositiveButton();
    }
}

function negativeButtonClick() {
    if (typeof(fnNegativeButton) != 'function') {
        closeModal();
    } else {
        fnNegativeButton();
    }
}

function loadModal(t) {
    // console.log(t);
    t.setAttribute('href', '#modal-target');
    t.setAttribute('data-toggle', 'modal');

    var title = t.getAttribute('title');
    if (title == null) title = t.innerHTML;
    $('.modal-backdrop, #modal-target.modal').remove();

    var modal_box = "";
    modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog'  id='modal-target' class='modal fade' style='display: none;  overflow-y: scroll;'>";
    modal_box += "<div class='modal-dialog modal-lg'>";
    modal_box += "<div class='modal-content'>";
    modal_box += "<div class='modal-header'>";
    modal_box += "<h6 class='modal-title'>" + title + "</h6>";
    modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='removeModal(this)' rel='modal-target' type='button'><span aria-hidden='true'>&times;</span></button>";
    modal_box += "</div>";
    modal_box += "<div id='modal-output' class='modal-body'></div>";
    modal_box += "</div>";
    modal_box += "</div>";
    modal_box += "</div>";

    $('html').append(modal_box);

    var data = t.getAttribute('data');
    var ajaxData = new FormData();
    var ajaxUrl = t.getAttribute('target');

    if (data == null) {
        data = [];
    } else {
        data = data.split(';');
    }

    for (var i = 0; i < data.length; i++) {
        if (data[i].length == 0) continue;
        else {
            var temp = data[i].split('=');
            var key = temp[0];
            var value = temp[1];

            ajaxData.append(key, value);
        }
    }
    ajaxTransfer(ajaxUrl, ajaxData, '#modal-output');
}



function loadModalFullScreen(t) {
    t.setAttribute('href', '#modal-target');
    t.setAttribute('data-toggle', 'modal');

    var title = t.getAttribute('title');
    if (title == null) title = t.innerHTML;
    $('.modal-backdrop, #modal-target.modal').remove();

    var modal_box = "";
    modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog'  id='modal-target' class='modal fade' style='display: none;  overflow-y: scroll;'>";
    modal_box += "<div class='modal-dialog modal-dialog-scrollable modal-lg'>";
    modal_box += "<div class='modal-content'>";
    modal_box += "<div class='modal-header'>";
    modal_box += "<h6 class='modal-title'>" + title + "</h6>";
    modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='removeModal(this)' rel='modal-target' type='button'><span aria-hidden='true'>&times;</span></button>";
    modal_box += "</div>";
    modal_box += "<div id='modal-output' class='modal-body'></div>";
    modal_box += "</div>";
    modal_box += "</div>";
    modal_box += "</div>";

    $('html').append(modal_box);

    var data = t.getAttribute('data');
    var ajaxData = new FormData();
    var ajaxUrl = t.getAttribute('target');

    if (data == null) {
        data = [];
    } else {
        data = data.split(';');
    }

    for (var i = 0; i < data.length; i++) {
        if (data[i].length == 0) continue;
        else {
            var temp = data[i].split('=');
            var key = temp[0];
            var value = temp[1];

            ajaxData.append(key, value);
        }
    }
    ajaxTransfer(ajaxUrl, ajaxData, '#modal-output');
}

function removeModal(t) {
    var modal_id = t.getAttribute('rel');
    $('.modal, .modal-overlay, .modal-backdrop').animate({ opacity: 0 }, modal_delay);
    setTimeout(function() {
        $('#modal-target.modal, .modal-overlay, .modal-backdrop').remove();
        $('body').removeClass('modal-open');
    }, modal_delay);

}


function loadModalPopup(t) {
    // console.log(t);
    t.setAttribute('href', '#modal-target');
    t.setAttribute('data-toggle', 'modal');

    var title = t.getAttribute('title');
    if (title == null) title = t.innerHTML;
    $('.modal-backdrop, #modal-target.modal').remove();

    var modal_box = "";
    modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog'  id='modal-target' class='modal fade' style='display: none;  overflow-y: scroll;'>";
    modal_box += "<div class='modal-dialog modal-lg'>";
    modal_box += "<div class='modal-content'>";
    modal_box += "<div class='modal-header'>";
    modal_box += "<h6 class='modal-title'>" + title + "</h6>";
    modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='removeModalPopup(this)' rel='modal-target' type='button'><span aria-hidden='true'>&times;</span></button>";
    modal_box += "</div>";
    modal_box += "<div id='modal-output' class='modal-body'></div>";
    modal_box += "</div>";
    modal_box += "</div>";
    modal_box += "</div>";

    $('html').append(modal_box);

    var data = t.getAttribute('data');
    var ajaxData = new FormData();
    var ajaxUrl = t.getAttribute('target');

    if (data == null) {
        data = [];
    } else {
        data = data.split(';');
    }

    for (var i = 0; i < data.length; i++) {
        if (data[i].length == 0) continue;
        else {
            var temp = data[i].split('=');
            var key = temp[0];
            var value = temp[1];

            ajaxData.append(key, value);
        }
    }
    ajaxTransfer(ajaxUrl, ajaxData, '#modal-output');
}

function removeModalPopup(t) {
    var modal_id = t.getAttribute('rel');
    $('.modal, .modal-overlay, .modal-backdrop').animate({ opacity: 0 }, modal_delay);
    setTimeout(function() {
        $('#modal-target.modal, .modal-overlay, .modal-backdrop').remove();
        $('body').removeClass('modal-open');
    }, modal_delay);
}

function closeModal(timeout) {
    $('.modal-header .close').click()
}

function showLoading() {
    $('#loading-screen').css({ display: 'block' });
    $('#loading-screen').animate({ opacity: '1' }, 100);
}

function hideLoading() {
    $('#loading-screen').animate({ opacity: '0' }, 100);
    setTimeout(function() {
        $('#loading-screen').css({ display: 'none' });
    }, 100);
}

function reload(timeout) {
    if (timeout == undefined) location.reload();
    else {
        timeout = parseInt(timeout);
        setTimeout(function() {
            location.reload();
        }, timeout);
    }
}

function initiateLoadingBar() {
    var new_element = "";
    new_element += "<div id='loading-screen'>";
    new_element += "<div id='loading-box'>";
    new_element += "<span>Sedang mengolah data, mohon tunggu...</span>";
    new_element += "</div>";
    new_element += "</div>";
    new_element += "<div id='global-temp'></div>";

    $(document).ready(function() {
        $('body').append(new_element);
    });
}

function getFormData(formId, asObject) {
    if (typeof asObject == 'boolean' && asObject) {
        var $form = $("#" + formId);
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i) {
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }

    var formData = new FormData();
    var input = $('#' + formId + ' input');
    var select = $('#' + formId + ' select');
    var textarea = $('#' + formId + ' textarea');
    var ignored = ['submit', 'button', 'reset'];
    var i, j, inputType, inputName, inputValue, file, files;

    for (i = 0; i < input.length; i++) {
        inputType = input[i].getAttribute('type');
        inputName = input[i].getAttribute('name');
        inputValue = input[i].value;

        if (ignored.indexOf(inputType) != -1) {
            continue;
        } else if (inputType == 'checkbox') {
            if (!input[i].checked) {
                inputValue = null;
            }
            formData.append(inputName, inputValue);
        } else if (inputType == 'radio') {
            inputValue = $('input[name="' + inputName + '"]:checked').val();
            formData.append(inputName, inputValue);
        } else if (inputType == 'file') {
            files = input[i].files;
            for (j = 0; j < files.length; j++) {
                file = files[j];
                formData.append(inputName, file, file.name);
            }
        } else {
            formData.append(inputName, inputValue);
        }
    }

    for (i = 0; i < select.length; i++) {
        inputName = select[i].getAttribute('name');
        inputValue = select[i].value;
        formData.append(inputName, inputValue);
    }

    for (i = 0; i < textarea.length; i++) {
        inputName = textarea[i].getAttribute('name');
        inputValue = textarea[i].value;
        formData.append(inputName, inputValue);
    }

    return formData;
}

function setInputPlaceholder() {
    var labels = $('label');
    var label, placeholder;

    for (var i = 0; i < labels.length; i++) {
        label = $(labels[i]).attr('for');
        placeholder = $(labels[i]).html();
        $(labels[i]).parents('form').find('*[name=' + label + ']').attr('placeholder', placeholder);
    }
}

function validateRequiredInput() {
    var required = $(':required');
    for (var i = 0; i < required.length; i++) {
        $(required[i]).blur(function(t) {
            var element = t.currentTarget;
            var value = $(element).val();
            var parent = $(element).parent();

            if (value.length == 0) {
                $(parent).addClass('has-error');
            } else {
                $(parent).removeClass('has-error');
            }
        });
    }
}

function scrollToTop() {
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
}


function isValidDate(dateString) {
    // First check for the pattern
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
};

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function getCsrfToken() {
    return $('input[name=' + csrfParam + ']').val();
}


function redirect(timeout, url) {
    if (timeout == undefined) location.reload();
    else {
        timeout = parseInt(timeout);
        setTimeout(function() {
            window.location = baseURL + url;
        }, timeout);
    }
}


function ajaxTransferOption(url, data, callback) {
    var response, csrfToken,timezone;
    if (typeof baseURL == 'undefined') {
        baseURL = '';
    }
    url = baseURL + url;

    if (!(data instanceof FormData)) {
        temp = data;
        data = new FormData();

        for (var key in temp) {
            if (temp.hasOwnProperty(key)) {
                data.append(key, temp[key]);
            }
        }
    }

    csrfToken = $('input[name=_token]').val();
    timezone =$('input[name=_timezone]').val();

    data.append('_token', csrfToken);
    data.append('_timezone',timezone)
    data.append('ajax-call', true);

    showLoading();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.onload = function() {
        response = xhr.responseText;
        if (xhr.status === 200) {
            if (typeof callback == 'string') {
                $(callback).html(response);
            } else {
                callback(response);
            }

            // labelForInput();
            // inputValidation();
            // toDate('.date');
            // toDatetime('.datetime');
        } else {
            console.log('ajax error! status : ' + xhr.status);
            console.log(xhr);
        }

        hideLoading();
    };
    xhr.send(data);
}




function getCity(urlCity,urlDistrict,urlVillage,acceptNull){
    acceptNull = typeof acceptNull === 'undefined' ? 0 : 1;
    var province_id = $('select[name=ref_provinsi_id]').val();
    ajaxTransferOption(urlCity, {ref_provinsi_id: province_id, accept_null: acceptNull}, function (result) {
        result = JSON.parse(result);
        $('select[name=ref_kabupaten_id] option').remove();

        var option = '';
        for (var key in result) {
            if (result.hasOwnProperty(key)) {
                option += "<option value='"+key+"'>"+result[key]+"</option>";
            }
        }

        $('select[name=ref_kabupaten_id]').html(option).trigger("chosen:updated");
        getDistrict(urlDistrict,urlVillage,acceptNull);
    });
}


function getDistrict(urlDistrict,urlVillage,acceptNull){
    acceptNull = typeof acceptNull === 'undefined' ? 0 : 1;
    var city_id = $('select[name=ref_kabupaten_id]').val();
    ajaxTransferOption(urlDistrict, {ref_kabupaten_id: city_id, accept_null: acceptNull}, function (result) {
        result = JSON.parse(result);
        $('select[name=ref_kecamatan_id] option').remove();

        var option = '';
        for (var key in result) {
            if (result.hasOwnProperty(key)) {
                option += "<option value='"+key+"'>"+result[key]+"</option>";
            }
        }

        $('select[name=ref_kecamatan_id]').html(option).trigger("chosen:updated");
        getVillage(urlVillage,acceptNull);
    });
}

function getVillage(url,acceptNull){
    acceptNull = typeof acceptNull === 'undefined' ? 0 : 1;
    var district_id = $('select[name=ref_kecamatan_id]').val();
    ajaxTransferOption(url, {ref_kecamatan_id: district_id, accept_null: acceptNull}, function (result) {
        result = JSON.parse(result);
        $('select[name=ref_desa_id] option').remove();

        var option = '';
        for (var key in result) {
            if (result.hasOwnProperty(key)) {
                option += "<option value='"+key+"'>"+result[key]+"</option>";
            }
        }

        $('select[name=ref_desa_id]').html(option).trigger("chosen:updated");
    });
}

function formatRupiah(bilangan, prefix)
{
    var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
        split	= number_string.split(','),
        sisa 	= split[0].length % 3,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function limitCharacter(event)
{
    key = event.which || event.keyCode;
    if ( key != 188 // Comma
        && key != 8 // Backspace
        && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
        && (key < 48 || key > 57) // Non digit
        // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    )
    {
        event.preventDefault();
        return false;
    }
}


function dateTime() {
    now = new Date();
    year = "" + now.getFullYear();
    month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
    day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
    hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
    minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
    second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}

function showFilter(btn,container){
    $('#'+btn).toggleClass('hide');
    $('#'+container).toggleClass('show');
}

function isMobileRequest() {
    var isMobileApps = parseInt($("input[name=is_mobile_apps]").val());
    if (isMobileApps === 1) {
        return !0;
    } else {
        return !1;
    }
}
function dateTimePicker(selector) {
    if (isMobileRequest()) {
        try {
            var element = $(selector);
            var siblings = $(element).siblings(".btn-trigger-datetime");
            var finalSelector;
            $(element).attr("autocomplete", "off");
            if (siblings.length === 0) {
                finalSelector = element;
            } else {
                finalSelector = siblings;
                $(element).change(function () {
                    var value = $(element).val();
                    $(siblings).attr("data-date", value);
                    $(siblings).find("input").val(value);
                });
            }
            finalSelector.datetimepicker({
                format: "yyyy-mm-dd hh:ii:ss",
                outputElement: element,
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 1,
                forceParse: 1,
                bootcssVer: 3,
                fontAwesome: !0,
                pickerPosition: "bottom-left",
            });
        } catch (e) {
            console.log(e);
        }
    } else {
        try {
            $.datetimepicker.setDateFormatter({
                parseDate: function (date, format) {
                    var d = moment(date, format);
                    return d.isValid() ? d.toDate() : !1;
                },
                formatDate: function (date, format) {
                    return moment(date).format(format);
                },
                formatMask: function (format) {
                    return format.replace(/Y{4}/g, "9999").replace(/Y{2}/g, "99").replace(/M{2}/g, "19").replace(/D{2}/g, "39").replace(/H{2}/g, "29").replace(/m{2}/g, "59").replace(/s{2}/g, "59");
                },
            });
            var value = $(selector).val();
            if (value.length > 0) {
                value = moment(value).format("YYYY-MM-DD HH:mm:ss");
            }
            $(selector).attr("autocomplete", "off").datetimepicker({ format: "YYYY-MM-DD HH:mm:ss", formatTime: "HH:mm", formatDate: "YYYY-MM-DD" });
        } catch (e) {
            console.log(e);
        }
    }
}

function getMomentNameDate(type)
{
    var array={
        'today': [moment(), moment()],
        'yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'thisweek': [moment().subtract(6, 'days'), moment()],
        'thismonth': [moment().startOf('month'), moment().endOf('month')],
        'lastmonth': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'thisyear':[moment().startOf('year'),moment().endOf('year')],
        'lastyear': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
    }
    return array[type];

}

$(document).ready(function() {
    initiateLoadingBar();
    setInputPlaceholder();
    validateRequiredInput();
    dateTimePicker(".trans_time");

})

function currencyFormat(value, curr) {
    value = isNaN(value) ? 0 : value;
    if (typeof curr === 'undefined') {
        curr = ''
    } else {
        curr = curr.trim().toUpperCase()
    }
    try {
        if (value < 0) {
            value *= -1;
            return '(' + accounting.formatMoney(value, curr + ' ', 2, ".", ",") + ')'
        } else {
            return accounting.formatMoney(value, curr + ' ', 2, ".", ",")
        }
    } catch (e) {}
    if (value < 0) {
        value *= -1;
        value = value.toLocaleString();
        value = value.replace(/\./g, '-');
        value = value.replace(/,/g, '.');
        value = value.replace(/-/g, ',');
        if (curr == '') {
            return '(' + value + ')'
        } else {
            return '(' + curr + ' ' + value + ')'
        }
    } else {
        value = value.toLocaleString();
        value = value.replace(/\./g, '-');
        value = value.replace(/,/g, '.');
        value = value.replace(/-/g, ',');
        if (curr == '') {
            return value
        } else {
            return curr + ' ' + value
        }
    }
}

function closeModalAfterSuccess()
{
    setTimeout(function() {
        $("#modal-target").modal("toggle")
    }, 1000);
}

function closeSelect2InModal(){
    $(document).on('hide.bs.modal','.modal',function (event) {
        $(this).find('select.select2-hidden-accessible').select2('close');
    });
}

function replaceCurrencyPointOrDecimal(val,find,str){
    var matched = false;
    if(val.indexOf(find)!==-1){
        matched = true
        val = val.replace(find,str)
    }
    if (matched) {
        val = replaceCurrencyPointOrDecimal(val,find,str);
    }
    return val;
}







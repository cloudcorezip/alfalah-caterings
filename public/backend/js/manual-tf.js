/*
 * Senna Apps
 * Copyright (c) 2020.
 */

function notify(body,icon,title,link,duration) {
    if ("Notification" in window) {
        var permission = Notification.permission;

        if (permission === "denied") {
            console.log('denied');
        } else if (permission === "granted") {
            return displayNotification(body,icon,title,link,duration)
        }

        Notification.requestPermission().then(function() {
            return displayNotification(body,icon,title,link,duration)
        });
    }
}

function displayNotification(body, icon, title, link, duration) {
    link = link || 0;
    duration = duration || 5000;

    var options = {
        body: body,
        icon: icon
    };

    var n = new Notification(title, options);

    if (link) {
        n.onclick = function () {
            window.open(link);
        };
    }

    setTimeout(n.close.bind(n), duration);
}

function getManualTransferNotify(url) {

    $.ajax({
        url:url,
        method:'GET',
        dataType:"json",
        success:function(data){
            console.log(data.data);
            var countAll= localStorage.getItem("countAll");
            console.log('dariStorage',countAll);
            console.log('dariDB',data.data.countAll);
            if(countAll<data.data.countAll){
                $(".manual-in").text(data.data.in)
                $(".manual-in-1").text(data.data.in)
                $(".manual-out").text(data.data.out)
                $(".manual-out-1").text(data.data.out)
                notify(
                    'Ada Transaksi Baru Dengan Kode '+data.data.latest.transaction_code+' Oleh '+data.data.latest.fullname+'',
                    "https://senna.co.id/public/logo-senna.png",
                    "Approval Topup dan Withdraw",
                    `#`
                );
                var x = document.getElementById("notifAudio");

                x.play();

            }

            localStorage.setItem("countAll", data.data.countAll);
        }

    })

}

function getMerchantRegistrationNotif(url) {
    $.ajax({
        url:url,
        method:'GET',
        dataType:"json",
        success:function(data){
            console.log(data.data);
            var countAll= localStorage.getItem("countMerchantRegAll");
            if(countAll<data.data.countAll){
                $(".merchant-reg").text(data.data.count)
                $(".merchant-reg-1").text(data.data.count)
                notify(
                    'Ada Merchant Baru Baru Dengan Nama '+data.data.latest.name+' Oleh '+data.data.latest.fullname+'',
                    "https://senna.co.id/public/logo-senna.png",
                    "Persetujuan Approval Merchant",
                    `#`
                );
                var x = document.getElementById("notifAudio");

                x.play();

            }

            localStorage.setItem("countMerchantRegAll", data.data.count);
        }

    })
}


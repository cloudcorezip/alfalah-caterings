/*
 * MitraAnakNegeri
 * Copyright (c) 2020.
 */


function getSelectWithModal(selector='',url='', placeholder='') {
    $(selector).select2({
        placeholder: placeholder,
        searchInputPlaceholder: 'Cari',
        dropdownParent: $('#modal-target'),
        ajax: {
            type: "GET",
            url: url,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    key: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
    });
}

function getSelect(selector='',url='', placeholder='') {
    $(selector).select2({
        placeholder: placeholder,
        searchInputPlaceholder: 'Cari',
        ajax: {
            type: "GET",
            url: url,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    key: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
    });
}





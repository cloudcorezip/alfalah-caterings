const dataTour = [
    {
        'selector':"#user-menu",
        'title': "Pengaturan",
        'content':'Melalui pintasan ini, anda diarahkan ke halaman pengaturan dasar dari profil usaha anda seperti identitas usaha,metode persediaan,dan pengaturan lainnya',
        'wrapper_class':'tour-wrapper',
        'arrow_class':'tour-arrow__up'
    },
    {
        'selector':"#biaya",
        'title': "Biaya",
        'content':'Melalui pintasan ini, anda diarahkan ke halaman yang berkaitan tentang pengelolaan keuangan yang lebih detail seperti pengeluaran umum,hutang usaha,piutang usaha dan lainnya',
        'wrapper_class':'tour-wrapper',
        'arrow_class':'tour-arrow__up'
    },
    {
        'selector':"#beli",
        'title': "Beli",
        'content':'Pintasan ini digunakan untuk mengarahkan anda ke halaman input data pembelian',
        'wrapper_class':'tour-wrapper',
        'arrow_class':'tour-arrow__up'
    },
    {
        'selector':"#jual",
        'title': "Jual",
        'content':'Pintasan ini mengarahkan anda ke halaman untuk memasukkan data penjualan',
        'wrapper_class':'tour-wrapper',
        'arrow_class':'tour-arrow__up'
    },
    {
        'selector':"#dashboard",
        'title': "Dashboard",
        'content':'Ikon menu ini mengarahkan anda untuk melihat tampilan ringkasan jual beli dan laporan grafik keuangan dalam kurun waktu tertentu',
        'wrapper_class':'tour-wrapper__left',
        'arrow_class':'tour-arrow__left'
    },
    {
        'selector':"#laporan",
        'title': "Laporan",
        'content':'Ikon menu ini mengarahkan anda untuk melihat laporan keseluruhan terkait keuangan,jual beli dan laporan lainnya secara detail dan dapat ditarik menjadi file excel atau pdf',
        'wrapper_class':'tour-wrapper__left',
        'arrow_class':'tour-arrow__left'
    },
    {
        'selector':"#master-dataDb",
        'title': "Master Data",
        'content':'Ikon ini menampilkan menu terkait pengelolaan data utama usaha anda seperti data produk,pelanggan,supplier dan data lainnya',
        'wrapper_class':'tour-wrapper__left',
        'arrow_class':'tour-arrow__left'
    },
    {
        'selector':"#penjualanDb",
        'title': "Penjualan",
        'content':'Ikon ini menampilkan menu terkait data penjualan maupun data piutang kepada pelanggan anda',
        'wrapper_class':'tour-wrapper__left',
        'arrow_class':'tour-arrow__left'
    },
    {
        'selector':"#pembelianDb",
        'title': "Pembelian",
        'content':'Ikon ini menampilkan menu terkait data pembelian maupun data hutang kepada supplier/pemasok  usaha anda',
        'wrapper_class':'tour-wrapper__left',
        'arrow_class':'tour-arrow__left'
    },
    {
        'selector':"#persediaanDb",
        'title': "Persediaan",
        'content':'Ikon ini mengarahkan anda ke halaman pengelolaan persediaan stok produk dimana anda bisa melakukan mutasi stok,produksi produk, melakukan stok opname jika terjadi selisih stok produk',
        'wrapper_class':'tour-wrapper__left',
        'arrow_class':'tour-arrow__left'
    },
    {
        'selector':"#keuanganDb",
        'title': "Keuangan",
        'content':'Ikon ini menampikan dan mengarahkan anda kehalaman pengaturan akun pada akuntansi usaha anda dan pencatatan jurnal berkaitan uang masuk dan keluar',
        'wrapper_class':'tour-wrapper__left',
        'arrow_class':'tour-arrow__left'
    },
    {
        'selector':"#absensiDb",
        'title': "Absensi",
        'content':'Ikon ini mengarahkan anda untuk melihat absensi karyawan anda dan persetujuan perizinan tidak masuk',
        'wrapper_class':'tour-wrapper__left__top',
        'arrow_class':'tour-arrow__left__bottom'
    },
    ,
    {
        'selector':"#pengelolaan-asetDb",
        'title': "Pengelolaan Aset",
        'content':'Ikon ini mengarahkan anda ke halaman pengelolaan aset pada usaha anda baik yang terdepresiasi maupun yang tidak',
        'wrapper_class':'tour-wrapper__left__top',
        'arrow_class':'tour-arrow__left__bottom'
    },
    // {
    //     'selector':"#integrasiDb",
    //     'title': "Integrasi",
    //     'content':'Ikon ini mengarahkan anda untuk melihat laporan pembayaran digital (QRIS) secara menyeluruh',
    //     'wrapper_class':'tour-wrapper__left__top',
    //     'arrow_class':'tour-arrow__left__bottom'
    // }, 
    {
        'selector':"#pengaturan",
        'title': "Pengaturan",
        'content':'Ikon ini mengarahkan anda ke halaman pengaturan dasar dari profil usaha anda seperti identitas usaha,metode persediaan, dan pengaturan lainnya',
        'wrapper_class':'tour-wrapper__left__top',
        'arrow_class':'tour-arrow__left__bottom'
    }     
];

let tour_step = 0;

const Tour = {
    init(selector, step){
        const element = [];
        selector.forEach(item => {
            element.push({
                'selector': item.selector,
                'title':item.title,
                'content':item.content,
                'wrapper_class':item.wrapper_class,
                'arrow_class':item.arrow_class
            });
        });
        console.log(element.length);
        return this.start(element, step);
    },
    _template(title,content, wrapper_class, arrow_class){
        if(tour_step == 0){
            return `<div class="tour-wrapper__overlay"></div>
                <div class="${wrapper_class} t-wrap">
                    <div class="${arrow_class}"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="text-dark font-weight-bold">${title}</h5>
                            <p class="text-dark">${content}</p>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-end">
                            <button
                                class="btn-tour__skip mr-2"
                                onclick="skipTour()"
                            >
                                Lewati
                            </button>
                            <button
                                class="btn-tour__next"
                                onclick="nextStep(${tour_step + 1})"
                            >
                                Lanjut
                            </button>
                        </div>
                    </div>

                </div>`;
        } else {
            return `<div class="tour-wrapper__overlay"></div>
                <div class="${wrapper_class} t-wrap">
                    <div class="${arrow_class}"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="text-dark font-weight-bold">${title}</h5>
                            <p class="text-dark">${content}</p>
                        </div>
                        <div class="col-sm-12 d-flex justify-content-end">
                            <button
                                class="btn-tour__back mr-2"
                                onclick="backStep(${tour_step - 1})"
                            >
                                Kembali
                            </button>
                            <button
                                class="btn-tour__skip mr-2"
                                onclick="skipTour()"
                            >
                                Lewati
                            </button>
                            <button
                                class="btn-tour__next"
                                onclick="nextStep(${tour_step + 1})"
                            >
                                Lanjut
                            </button>
                        </div>
                    </div>

                </div>`;
        }

    },
    start(element, step){
        $('.tour-wrapper__overlay').remove();
        $('.t-wrap').remove();
        let title = element[step].title;
        let content = element[step].content;
        let wrapper_class = element[step].wrapper_class;
        let arrow_class = element[step].arrow_class;
        $(element[step].selector).append(this._template(title, content, wrapper_class, arrow_class));
    },
    skipTour(){
        $('.tour-wrapper__overlay').remove();
        $('.t-wrap').remove();
    }

}


const nextStep = (step) => {
    tour_step = step;
    if(step >= dataTour.length - 1){
        skipTour();
        return;
    }
    Tour.init(dataTour, step);
}

const backStep = (step) => {
    tour_step = step;
    Tour.init(dataTour, step);
}

const skipTour = () => {
    localStorage.setItem('is_tour', 1);
    Tour.skipTour();
}

const repeatTour = () => {
    if(localStorage.getItem('is_tour')){
        localStorage.removeItem('is_tour');
    }
}

const loadModalTour = () => {
    let html =  `<div class="modal modal-tour" tabindex="-1" id="modalTutor">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="${$('#banner-tutor').attr('src')}" alt="modal" class="img-fluid">
                                <div class="modal-tour__button d-flex justify-content-end">
                                    <button
                                        class="btn-tour__back mr-2"
                                        data-dismiss="modal"
                                        onclick="skipTour()"
                                        style="border-radius:5px;font-size:12px;"
                                    >
                                        Lewati
                                    </button>
                                    <button
                                        class="btn-tour__next"
                                        onclick="nextStep(${tour_step})"
                                        data-dismiss="modal"
                                        style="border-radius:5px;font-size:12px;"
                                    >
                                        Mulai Panduan
                                        <i class="fas fa-chevron-right ml-2"></i>
                                    </button>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>`;
        $('body').append(html);
        $('#modalTutor').modal('show')
}

if(localStorage.getItem('is_tour') != 1 && localStorage.getItem('isActiveMenu') == 'dashboard'){
    $('document').ready(function(){
        loadModalTour();
    });
}



var csrfParam = '_token';

function ajaxDataTable(selector, stateSave, url, collumn, defaultOrder,isReload=0) {
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    if (typeof defaultOrder == 'undefined') {
        defaultOrder = []; // number is always the first column
    }
    if (stateSave === 1) {
        if(isReload===0)
        {
            stateSave = true;
        }else {
            stateSave = false;
        }

    } else {
        stateSave = false;
    }

    csrfToken = $('input[name=' + csrfParam + ']').val();
    var table=$(selector).DataTable({
        responsive: true,
        processing: true,
        destroy:true,
        serverSide: true,
        iDisplayLength: 10,
        language: {
            processing: '<div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div> ',
            paginate: {
                "previous": '<i class="ti-angle-left"></i>',
                "next": '<i class="ti-angle-right"></i>'
            },
            info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            lengthMenu: "Tampilkan _MENU_ entri",
            infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
        },

        stateSave: false,
        ajax: {
            url: url,
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        },
        columns: collumn,
        columnDefs: [
            { orderable: false, targets: 0 }
        ],
        fnDrawCallback: function(x) {
            var currentPage = this.fnPagingInfo().iPage;
            var displayLength = this.fnPagingInfo().iLength;
            var page = (currentPage * displayLength) + 1;
            rearrangeDataTableNumbering(selector, page);
        }
    });

    if(isReload===1)
    {
        table.ajax.reload();
    }
}

function dataTable(selector) {
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        }

        $(selector).DataTable({
            'iDisplayLength': 25,
            order: [
                [1, "asc"]
            ],
            'fnDrawCallback': function(x) {
                var currentPage = this.fnPagingInfo().iPage;
                var displayLength = this.fnPagingInfo().iLength;
                var page = (currentPage * displayLength) + 1;
                rearrangeDataTableNumbering(selector, page);
            }
        });
    });
}


function rearrangeDataTableNumbering(selector, page) {
    var firstField = $(selector).find('th:first-child').html().toLowerCase();
    if (firstField == 'no') {
        var trList = $(selector).find('tbody tr');
        for (var i = 0; i < trList.length; i++) {
            $(trList[i]).find('td:first-child').html(page + i);
        }
    }
}

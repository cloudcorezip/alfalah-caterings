/*
 * Senna Apps
 * Copyright (c) 2021.
 */

function toastSubsSadewaOrAntasena($msg)
{
    toastr.options = {
        timeOut: 3000,
        progressBar: true,
        showMethod: "slideDown",
        hideMethod: "slideUp",
        showDuration: 200,
        hideDuration: 200,
        positionClass : "toast-top-center"
    };
    toastr.success($msg);


}
function toastForSaveData(message,type,is_modal=false,url='',is_with_datatable=true) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "250",
        "hideDuration": "600",
        "timeOut": "1500",
        "extendedTimeOut": "700",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    if (type == 'warning') {

        toastr.warning(message);

    } else if (type == 'danger') {
        toastr.error(message);

    } else {
        toastr.success(message);
        if(is_modal==false)
        {
            redirect(1000,url);
        }else{
            if(is_with_datatable==false)
            {
                closeModalAfterSuccess()
                reload(1500)

            }else{
                reloadDataTable(1)
                closeModalAfterSuccess()

            }
        }
    }
}

function otherMessage(type,message)
{
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "250",
        "hideDuration": "600",
        "timeOut": "1500",
        "extendedTimeOut": "700",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    if (type == 'warning') {

        toastr.warning(message);

    } else if (type == 'danger') {
        toastr.error(message);

    } else {

        toastr.success(message);
    }
}

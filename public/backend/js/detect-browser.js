/*
 * Senna Apps
 * Copyright (c) 2020.
 */

function get_browser(){var e,r,a;return r=void 0,e=(a=navigator.userAgent).match(/(android)\s([0-9\.]*)/i)||a.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i)||[],/android/i.test(e[1])?{name:"android",version:parseInt(e[2])}:/trident/i.test(e[1])?(r=/\brv[ :]+(\d+)/g.exec(a)||[],{name:"ie",version:parseInt(r[1])||""}):"Chrome"===e[1]&&null!==(r=a.match(/\b(OPR|Edge)\/(\d+)/))?{name:r[1].replace("OPR","opera"),version:parseInt(r[2])}:(e=e[2]?[e[1],e[2]]:[navigator.appName,navigator.appVersion,"-?"],null!==(r=a.match(/version\/(\d+)/i))&&e.splice(1,1,r[1]),{name:e[0].toLowerCase(),version:parseInt(e[1])})}

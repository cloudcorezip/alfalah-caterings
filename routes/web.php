<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(env('APP_DOMAIN')==true){
    Route::domain(config('module_domain.domain'))->group(function () {
        $directory = DIRECTORY_SEPARATOR;
        \App\Classes\RouteConfig::getRouteFromController($directory.'Http'.$directory.'Controllers'.$directory);

    });
}else{
    $directory = DIRECTORY_SEPARATOR;
    \App\Classes\RouteConfig::getRouteFromController($directory.'Http'.$directory.'Controllers'.$directory);

}


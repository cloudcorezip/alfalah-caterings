<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Models;


use Illuminate\Database\Eloquent\Model;

class ShiftRoasterChange extends Model
{
    protected $table='hr_merchant_shift_roaster_changes';
    protected $primaryKey='id';
}

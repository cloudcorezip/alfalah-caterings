<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Models;


use Illuminate\Database\Eloquent\Model;

class ShiftRoaster extends Model
{
    protected $table='hr_merchant_shift_roasters';
    protected $primaryKey='id';

}

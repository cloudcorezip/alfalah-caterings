<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Models;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\User;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table='hr_merchant_job_positions';
    protected $primaryKey='id';


    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');

    }

    public function getCreatedBy()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

}

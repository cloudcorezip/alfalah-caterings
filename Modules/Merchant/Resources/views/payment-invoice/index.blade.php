@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">{{$title}}</h4>
                <span>Atur tampilan invoice terhadap pelangganmu disini.</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-12 mb-3 p-0">
                <div class="card">
                    <div class="card-body">
                        <form onsubmit="return false;" class="form-konten" id="form-konten">
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <h5 class="font-weight-bold mb-3" style="color:#535353;font-weight:500;">Pengaturan Invoice</h5>
                            <h6 class="mb-3" style="color:#535353;font-weight:500;"><i class="fas fa-palette mr-2" style="color:#737373;"></i>Kustomisasi Penampilan</h6>
                            <div class="form-group">
                                <label class="font-weight-bold d-block">Warna Tema</label>
                                <div class="d-flex align-items-center">
                                    <input
                                        type="color"
                                        id="theme-pallete"
                                        class="mr-3"
                                        value="{{is_null($data->theme)? '#FFA943': $data->theme}}"
                                    >
                                    <input
                                        type="text"
                                        class="form-control"
                                        style="width:50%;"
                                        name="theme"
                                        id="theme"
                                        value="{{is_null($data->theme)? '#FFA943':$data->theme}}"
                                    >
                                </div>
                            </div>

                            <h6 class="mb-3" style="color:#535353;font-weight:500;"><i class="fas fa-users mr-2" style="color:#737373;"></i>Pengaturan Pelanggan</h6>
                            <div class="form-group">
                                <label class="font-weight-bold">Bahasa Standar</label>
                                <select class="form-control" id="md_language_id" name="md_language_id">
                                    <option value="1">Bahasa Indonesia</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Mata Uang</label>
                                <select class="form-control" id="md_currency_id" name="md_currency_id">
                                    @foreach($currency as $key => $item)
                                    <option
                                        value="{{$item->id}}"
                                        @if($data->md_currency_id == $item->id)
                                        selected
                                        @endif
                                    >
                                        {{$item->name}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
{{--                            <div class="form-group mb-3">--}}
{{--                                <label class="font-weight-bold" for="exampleFormControlSelect1">Durasi Invoice Standar</label>--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-6 mb-3">--}}
{{--                                      --}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-6 mb-3">--}}
{{--                                        <select class="form-control" id="invoice_period">--}}
{{--                                            <option>Hari</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            {{--- @if(count($vaBank) > 0 || count($eWallet) > 0 || !is_null($qris))
                            <h5 class="font-weight-bold mb-3" style="color:#535353;font-weight:500;">Metode Pembayaran</h5>
                            @endif

                            @if(count($vaBank) > 0)
                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                <div class="card-body">
                                    <h6 class="mb-3" style="color:#535353;font-weight:500;"><i style="font-size:16px;" class="fas fa-university mr-2"></i>Virtual Account</h6>

                                    @foreach($vaBank as $key => $item)
                                    <div class="form-check d-flex align-items-center ml-2 mb-1">
                                        <input
                                            class="form-check-input check-bank"
                                            type="checkbox"
                                            value="{{$item->id}}"
                                            style="margin-top:0;"
                                            name="payment_method[]"
                                            data-icon="{{$item->icon}}"
                                            data-target="va-{{$item->id}}"
                                            data-container="virtual-account-row"
                                            @if(in_array($item->id, array_column($jsonInvoice, 'id')))
                                                checked
                                            @endif
                                        >
                                        <label class="form-check-label">
                                            {{$item->name}}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif

                            @if(count($eWallet) > 0)
                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                <div class="card-body">
                                    <h6 class="mb-3" style="color:#535353;font-weight:500;"><i style="font-size:16px;" class="fas fa-university mr-2"></i>E-Wallet</h6>

                                    @foreach($eWallet as $key => $item)
                                    <div class="form-check d-flex align-items-center ml-2 mb-1">
                                        <input
                                            class="form-check-input check-bank"
                                            type="checkbox"
                                            value="{{$item->id}}"
                                            style="margin-top:0;"
                                            name="payment_method[]"
                                            data-icon="{{$item->icon}}"
                                            data-target="ew-{{$item->id}}"
                                            data-container="ewallet-row"
                                            @if(in_array($item->id, array_column($jsonInvoice, 'id')))
                                                checked
                                            @endif
                                        >
                                        <label class="form-check-label">
                                            {{$item->name}}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif

                            @if(!is_null($qris))
                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                <div class="card-body">
                                    <h6 class="mb-3" style="color:#535353;font-weight:500;"><i style="font-size:16px;" class="fas fa-university mr-2"></i>QRIS</h6>
                                    <div class="form-check d-flex align-items-center ml-2 mb-1">
                                        <input
                                            class="form-check-input check-bank"
                                            type="checkbox"
                                            value="{{$qris->id}}"
                                            style="margin-top:0;"
                                            name="payment_method[]"
                                            data-icon="public/transactionType/qris.png"
                                            data-target="qr-{{$qris->id}}"
                                            data-container="qris-row"
                                            @if(in_array($qris->id, array_column($jsonInvoice, 'id')))
                                                checked
                                            @endif
                                        >
                                        <label class="form-check-label">Qris</label>
                                    </div>
                                </div>
                            </div>
                            @endif ---}}


                            <div class="d-flex justify-content-center">
                                <input
                                    type="number"
                                    class="form-control"
                                    name="invoice_duration"
                                    value="{{is_null($data->invoice_duration) ? 1 : $data->invoice_duration}}"
                                    id="invoice_duration"
                                    hidden
                                >
                                <a class="btn btn-light py-2 mr-3" href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}?page=payment-method">Kembali</a>
                                <button style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Simpan</button>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-lg-9 col-md-12 mb-3 p-0">
                <div class="container-fluid">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8 col-md-6">
                                    <h4 class="mb-1 font-weight-bold">Kustomisasi Invoice Kamu</h4>
                                    <span class="d-block mb-2">Kustomisasi tampilan default dan ketentuan pembayaran invoice kamu.</span>
                                </div>
                                <div class="col-lg-4 col-md-6 d-flex justify-content-end align-items-center">
                                    <button class="mr-3 btn btn-outline-warning px-5 switch-view" data-type="mobile">Mobile</button>
                                    <button class="btn btn-bg__orange text-white px-5 switch-view active" data-type="web">Website</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid" id="container-invoice">
                    <div class="row">
                        <div class="col-lg-12 offset-lg-0" id="col-container">
                            <div class="card shadow">
                                <div class="card-header px-4 py-3 d-flex align-items-center" id="inv-header" style="background-color:{{is_null($data->theme)?'#FFA943':$data->theme}}; min-height:50px;">
                                    @if(!is_null($merchant->image))
                                        <img src="{{env('S3_URL')}}{{$merchant->image}}" alt="logo" class="merchant-logo">
                                    @else
                                        <img src="{{asset('public/backend/img/no-user.png')}}" alt="foto profil" class="img-fluid img-thumbnail merchant-logo">
                                    @endif

                                    <span class="d-block ml-3" style="font-size:19px;color:#fff;">
                                        {{ucwords($merchant->name)}}
                                    </span>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button
                                                class="d-flex justify-content-between align-items-center"
                                                type="button"
                                                data-toggle="collapse"
                                                data-target="#view-trans"
                                                aria-expanded="false"
                                                aria-controls="virtual-account"
                                                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                            >
                                                <div>
                                                    <i class="fas fa-book mr-1"></i>
                                                    <span style="color:#4F4F4F;font-size:16px;font-weight:bold">Pembayaran Paket Langganan</span>
                                                </div>
                                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                                            </button>
                                            <div class="collapse" id="view-trans">
                                                <span class="d-block ml-4 mt-2">Reference ID T-0000000002132577938</span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row mb-4">
                                        <div class="col-md-12 text-center">
                                            <h5 class="font-weight-bold" style="color:#747474;">BAYAR SEBELUM <span id="exp_date">{{$expireDate}}</span></h5>
                                            <h1 class="font-weight-bold" style="color:#535353;">{{rupiah(70000)}}</h1>
                                        </div>
                                    </div>

                                    @if(count($vaBank) > 0)
                                    <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                        <div class="card-body">
                                            <div>
                                                <button
                                                    class="mb-3 d-flex justify-content-between align-items-center"
                                                    type="button"
                                                    data-toggle="collapse"
                                                    data-target="#virtual-account"
                                                    aria-expanded="false"
                                                    aria-controls="virtual-account"
                                                    style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                                >
                                                    <div>
                                                        <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                        <span style="color:#4F4F4F;font-size:16px;">Virtual Account</span>
                                                    </div>
                                                    <i class="fas fa-angle-down" style="font-size:16px;"></i>
                                                </button>
                                                <div class="collapse show" id="virtual-account">
                                                    <div class="row" id="virtual-account-row">
                                                    @foreach($vaBank as $key => $item)
                                                        <div class="col-md-6 col-sm-12 card-bank" id="va-{{$item->id}}">
                                                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                                <div class="card-body text-center">
                                                                    <img src="{{env('S3_URL')}}{{$item->icon}}" alt="{{$item->icon}}" class="bank-logo">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(count($eWallet) > 0)
                                    <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                        <div class="card-body">
                                            <div>
                                                <button
                                                    class="mb-3 d-flex justify-content-between align-items-center"
                                                    type="button"
                                                    data-toggle="collapse"
                                                    data-target="#ewallet"
                                                    aria-expanded="false"
                                                    aria-controls="virtual-account"
                                                    style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                                >
                                                    <div>
                                                        <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                        <span style="color:#4F4F4F;font-size:16px;">E-Wallet</span>
                                                    </div>
                                                    <i class="fas fa-angle-down" style="font-size:16px;"></i>
                                                </button>
                                                <div class="collapse show" id="ewallet">
                                                    <div class="row" id="ewallet-row">
                                                    @foreach($eWallet as $key => $item)
                                                        <div class="col-md-6 col-sm-12 card-bank" id="ew-{{$item->id}}">
                                                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                                <div class="card-body text-center">
                                                                    <img src="{{env('S3_URL')}}{{$item->icon}}" alt="{{$item->icon}}" class="bank-logo">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(!is_null($qris))
                                    <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                        <div class="card-body">
                                            <div>
                                                <button
                                                    class="mb-3 d-flex justify-content-between align-items-center"
                                                    type="button"
                                                    data-toggle="collapse"
                                                    data-target="#qris"
                                                    aria-expanded="false"
                                                    aria-controls="qris"
                                                    style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                                >
                                                    <div>
                                                        <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                        <span style="color:#4F4F4F;font-size:16px;">QRIS</span>
                                                    </div>
                                                    <i class="fas fa-angle-down" style="font-size:16px;"></i>
                                                </button>
                                                <div class="collapse show" id="qris">
                                                    <div class="row" id="qris-row">
                                                        <div class="col-md-6 col-sm-12 card-bank" id="qr-{{$qris->id}}">
                                                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                                <div class="card-body text-center">
                                                                    <img src="{{env('S3_URL')}}public/transactionType/qris.png" alt="public/transactionType/qris.png" class="bank-logo">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(!is_null($cash))
                                    <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                        <div class="card-body">
                                            <div>
                                                <button
                                                    class="mb-3 d-flex justify-content-between align-items-center"
                                                    type="button"
                                                    data-toggle="collapse"
                                                    data-target="#cash"
                                                    aria-expanded="false"
                                                    aria-controls="qris"
                                                    style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                                >
                                                    <div>
                                                        <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                        <span style="color:#4F4F4F;font-size:16px;">{{$cash->name}}</span>
                                                    </div>
                                                    <i class="fas fa-angle-down" style="font-size:16px;"></i>
                                                </button>
                                                <div class="collapse show" id="cash">
                                                    <div class="row" id="cash-row">
                                                        <div class="col-md-6 col-sm-12 card-bank" id="cash-{{$cash->id}}">
                                                            <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                                <div class="card-body text-center">
                                                                    <img src="{{env('S3_URL')}}{{$cash->icon}}" alt="{{$cash->icon}}" class="bank-logo">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <script>
        $(document).ready(function(){
            $("#languange").select2();
            $("#md_currency_id").select2();
            $("#md_language_id").select2();
            $("#invoice_period").select2();

            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');
                ajaxTransfer("{{route('payment-invoice.save')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            });


            $('.switch-view').on('click', function(){
                let type = $(this).attr('data-type');

                $(this).addClass('btn-bg__orange text-white active');
                $(this).removeClass('btn-outline-warning');
                $('.switch-view').not(this).removeClass('btn-bg__orange text-white active');
                $('.switch-view').not(this).addClass('btn-outline-warning');

                if(type == 'web'){
                    $('#col-container').addClass('col-lg-12 offset-lg-0 transition-switch');
                    $('#col-container').removeClass('col-md-6 offset-md-3');
                    $('.card-bank').addClass('col-md-6');
                    $('.card-bank').removeClass('col-md-12');

                } else {
                    $('#col-container').removeClass('col-lg-12 offset-lg-0');
                    $('#col-container').addClass('col-md-6 offset-md-3 transition-switch');
                    $('.card-bank').addClass('col-md-12');
                    $('.card-bank').removeClass('col-md-6');
                }
            });

            $('.check-bank').on('change', function(){
                let iconUrl = $(this).attr('data-icon');
                let colTarget= $(this).attr('data-target');
                let containerTarget = $(this).attr('data-container');
                if(this.checked){
                    let html = `<div class="col-md-6 col-sm-12 card-bank" id="${colTarget}">
                                    <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                        <div class="card-body text-center">
                                            <img src="{{env('S3_URL')}}${iconUrl}" alt="{{$item->icon}}" class="bank-logo">
                                        </div>
                                    </div>
                                </div>`;
                    $('#'+containerTarget).append(html);
                } else {
                    $('#'+colTarget).remove();
                }

                $('.switch-view.active').click();
            });

            $('#invoice_duration').on('change', function(){
                $.ajax({
                    type:"POST",
                    url: "{{route('payment-invoice.expire-date')}}",
                    data:{
                            _token:'{{csrf_token()}}',
                            invoice_duration: $(this).val()
                        },
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    success:function(response){
                        $('#exp_date').text(response);
                    }
                });
            });

            $('#theme-pallete').on('change', function(){
                let colorVal = $(this).val();
                $('#theme').val(colorVal);
                $('#inv-header').css('background-color', colorVal);
            });

            $('#theme').on('change', function(){
                let colorVal = $(this).val();
                $('#theme-pallete').val(colorVal);
                $('#inv-header').css('background-color', colorVal);
            });
        });
    </script>
@endsection






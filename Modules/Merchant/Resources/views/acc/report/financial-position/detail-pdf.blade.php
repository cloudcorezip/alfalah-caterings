<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>

    <div id="layout-print-intern">
        <h1 class="text-center" style="font-size: 20px; font-weight: bold; margin: 0 0 10px 0;">{{strtoupper($merchant->name)}}</h1>
        <h2 class="text-center" style="margin: 0 0 10px 0; font-size: 18px; font-weight: bold;">LAPORAN POSISI KEUANGAN PERUSAHAAN</h2>
        <h3 class="text-center" style="margin: 0 0 40px 0; font-size: 14px; font-weight: bold;">{{strtoupper($sub_title)}}</h3>
        <table class="table table-hover">
            <tbody>
            @php
                $asset=0;
                $kewajibanAndEkuitas=0;
            @endphp
            @foreach($data as $key => $item)
                <tr>
                    <td><b>{{strtoupper($item->name)}}</b></td>
                    <td></td>
                </tr>
                @php
                    $subTotal=0;
                    $child=collect($item->json)->whereNotIn('code',['1.3.00'])->sortBy('id');
                @endphp
                @foreach($child as $c)
                    @php
                        $subTotalDetail=0;
                        $det1=collect($c->detail)->sortBy('id');
                    @endphp
                    @foreach($det1 as $dd)
                        @if($dd->type_coa=='Debit')
                            @php
                                $subTotalDetail+=$dd->sum_of_debit-$dd->sum_of_kredit;
                            @endphp
                        @else
                            @php
                                $subTotalDetail+=$dd->sum_of_kredit-$dd->sum_of_debit;
                            @endphp
                        @endif
                    @endforeach
                    @php
                        $subTotal+=$subTotalDetail;
                    @endphp
                    <tr>
                        <td style="padding-left: 20px !important;">{{ucwords($c->name)}}</td>
                        <td class="text-right">
                            @if($subTotalDetail<0)
                                ({{rupiah(abs($subTotalDetail))}})
                            @else
                                {{rupiah($subTotalDetail)}}
                            @endif
                        </td>

                    </tr>
                    @php
                        $child2=collect($c->json)->sortBy('id');
                    @endphp
                    @if($child2->count()>0)
                        @foreach($child2 as $cc)
                            @php
                                $subTotalDetail=0;
                                $det2=collect($cc->detail)->sortBy('id');

                            @endphp
                            @foreach($det2 as $d)
                                @if($d->type_coa=='Debit')
                                    @php
                                        $subTotalDetail+=$d->sum_of_debit-$d->sum_of_kredit;
                                    @endphp
                                @else
                                    @php
                                        $subTotalDetail+=$d->sum_of_kredit-$d->sum_of_debit;
                                    @endphp
                                @endif
                            @endforeach
                            @php
                                $subTotal+=$subTotalDetail;
                            @endphp
                            <tr>
                                <td style="padding-left: 40px !important;">{{ucwords($cc->name)}}</td>
                                <td class="text-right">
                                    @if($subTotalDetail<0)
                                        ({{rupiah(abs($subTotalDetail))}})
                                    @else
                                        {{rupiah($subTotalDetail)}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                @endforeach
                @if($key==3)
                    @php
                        $asset+=$subTotal;
                    @endphp
                    <tr>
                        <td><b>TOTAL {{strtoupper($item->name)}}</b></td>
                        <td class="text-right"><b>
                                @if($subTotal<0)
                                    ({{rupiah(abs($subTotal))}})
                                @else
                                    {{rupiah($subTotal)}}
                                @endif
                            </b></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @else
                    @php
                        $kewajibanAndEkuitas+=$subTotal;
                    @endphp
                @endif
            @endforeach
            <tr>
                <td style="padding-left: 20px !important;">Saldo Laba Tahun Berjalan</td>
                <td class="text-right">
                    @if($asset-$kewajibanAndEkuitas<0)
                        ({{rupiah(abs($asset-$kewajibanAndEkuitas))}})
                    @else
                        {{rupiah($asset-$kewajibanAndEkuitas)}}
                    @endif
                </td>

            </tr>
            <tr>
                <td><b>TOTAL KEWAJIBAN & EKUITAS</b></td>
                <td class="text-right"><b>
                        @if($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas)<0)
                            ({{rupiah(abs($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas)))}})
                        @else
                            {{rupiah($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas))}}
                        @endif
                    </b></td>
            </tr>
            </tbody>
        </table>
    </div>


</body>

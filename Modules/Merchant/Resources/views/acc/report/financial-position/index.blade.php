@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all')}}">Keuangan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>Laporan Posisi Keuangan</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan rangkuman laporan posisi keuangan usahamu setiap periode</span>

            </div>
        </div>
    </div>

    <div class="container-fluid">
        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="80%" style="padding: 0.1rem"><h6><b>{{strtoupper($title)}} </b></h6></td>
                <td rowspan="2" class="text-right" style="padding: 0.1rem">
                    <a class="btn btn-success" onclick="exportData()"><i class="fa fa-download mr-2"></i> Download Laporan</a>

                </td>
            </tr>
            <tr>
                <td style="padding: 0.1rem"><h6><b>{{strtoupper($sub_title)}}</b></h6></td>
            </tr>
        </table>

        @php
            $asset=0;
            $kewajibanAndEkuitas=0;
        @endphp
        @foreach($data as $key =>$item)
            <div class="card shadow" @if($item->name=='aktiva') style="margin-bottom: 0.5rem" @else  style="margin-bottom: 0rem" @endif >
                <div class="card-body" style="padding:0rem">
                    <table class="table table-borderless" style="margin-bottom: 0rem">
                        <tbody>
                        <tr>
                            <td colspan="2" class="text-left" style="border-bottom: 1px solid #EFEFEF"><b>{{strtoupper($item->name)}}</b></td>
                        </tr>
                        @php
                            $subTotal=0;
                            $child=collect($item->json)->whereNotIn('code',['1.3.00'])->sortBy('id');
                        @endphp
                        @foreach($child as $c)
                            @php
                                $subTotalDetail=0;
                                $det1=collect($c->detail)->sortBy('id');
                            @endphp
                            @foreach($det1 as $dd)
                                @if($dd->type_coa=='Debit')
                                    @php
                                        $subTotalDetail+=$dd->sum_of_debit-$dd->sum_of_kredit;
                                    @endphp
                                @else
                                    @php
                                        if(strpos($dd->name,'Akumulasi')!==false){
                                            $subTotalDetail-=$dd->sum_of_kredit-$dd->sum_of_debit;

                                         }else{
                                            $subTotalDetail+=$dd->sum_of_kredit-$dd->sum_of_debit;
                                         }
                                    @endphp
                                @endif
                            @endforeach
                            @php

                                $subTotal+=$subTotalDetail;
                            @endphp
                            <tr>
                                <td style="padding-left: 20px !important;">{{ucwords($c->name)}}</td>
                                <td class="text-right">
                                    @if($subTotalDetail<0)
                                        ({{rupiah(abs($subTotalDetail))}})
                                    @else
                                        {{rupiah($subTotalDetail)}}
                                    @endif
                                </td>

                            </tr>
                            @php
                                $child2=collect($c->json)->sortBy('id');
                            @endphp
                            @if($child2->count()>0)
                                @foreach($child2 as $cc)
                                    @php
                                        $subTotalDetail=0;
                                        $det2=collect($cc->detail)->sortBy('id');

                                    @endphp
                                    @foreach($det2 as $d)
                                        @if($d->type_coa=='Debit')
                                            @php
                                                $subTotalDetail+=$d->sum_of_debit-$d->sum_of_kredit;
                                            @endphp
                                        @else
                                            @php
                                                if(strpos($d->name,'Akumulasi')!==false){
                                             $subTotalDetail-=$d->sum_of_kredit-$d->sum_of_debit;

                                          }else{
                                             $subTotalDetail+=$d->sum_of_kredit-$d->sum_of_debit;
                                          }
                                            @endphp
                                        @endif
                                    @endforeach
                                    @php
                                        $subTotal+=$subTotalDetail;
                                    @endphp
                                    <tr>
                                        <td style="padding-left: 40px !important;">{{ucwords($cc->name)}}</td>
                                        <td class="text-right">
                                            @if($subTotalDetail<0)
                                                ({{rupiah(abs($subTotalDetail))}})
                                            @else
                                                {{rupiah($subTotalDetail)}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                        @if($item->code=='1.0.00')
                            @php
                                $asset+=$subTotal;
                            @endphp
                            <tr style="font-weight: bold;background-color: #FEF2EB">
                                <td><b>TOTAL {{strtoupper($item->name)}}</b></td>
                                <td class="text-right"><b>
                                        @if($subTotal<0)
                                            ({{rupiah(abs($subTotal))}})
                                        @else
                                            {{rupiah($subTotal)}}
                                        @endif
                                    </b></td>
                            </tr>
                            <tr  style="background-color: #EFEFEF;">
                                <td colspan="2" style="padding: 0.2rem"></td>
                            </tr>
                        @else
                            @php
                                $kewajibanAndEkuitas+=$subTotal;
                            @endphp
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <tbody>
                    <tr>
                        <td style="padding-left: 20px !important;">Saldo Laba Tahun Berjalan</td>
                        <td class="text-right">
                            @if($asset-$kewajibanAndEkuitas<0)
                                ({{rupiah(abs($asset-$kewajibanAndEkuitas))}})
                            @else
                                {{rupiah($asset-$kewajibanAndEkuitas)}}
                            @endif
                        </td>

                    </tr>
                    <tr style="font-weight: bold;background-color: #FEF2EB">
                        <td><b>TOTAL KEWAJIBAN & EKUITAS</b></td>
                        <td class="text-right"><b>
                                @if($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas)<0)
                                    ({{rupiah(abs($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas)))}})
                                @else
                                    {{rupiah($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas))}}
                                @endif
                            </b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <!-- DataTales Example -->

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form method="get" class="form-horizontal">
            <div class="row px-30">
            <div class="col-md-12">
               <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="end_date" value="{{$endDate}}" name="end_date">

                    </div>
                </div>

                <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Pilih Cabang</label>
                            <select name="md_merchant_id" id="branch" class="form-control form-control">
                                @foreach(get_cabang() as $key  =>$item)
                                    <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <input class="btn btn-success btn-block" type="submit" value="Tampilkan Data" data-original-title="" title="" autocomplete="off">
                        <a href="{{route('merchant.toko.acc.report.financial-position.index')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>
                        <input name="is_download" type="hidden" value="0" autocomplete="off">

                    </div>
                </div>
            </div>


        </form>

    </div>

@endsection

@section('js')
    <script>
        const reloadDataTable = () => void 0;
        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {

                const data = new FormData();
                data.append('start_date','{{$startDate}}');
                data.append('end_date','{{$endDate}}');
                data.append('md_merchant_id','{{$merchantId}}');

                ajaxTransfer("{{route('merchant.toko.acc.report.financial-position.exportExcel')}}", data, '#modal-output');
            });

        }
        $(document).ready(function() {
            @if(merchant_detail()->is_branch==0)
            $("#branch").select2({});
            @endif
            activeMenu('laporan', 'laporan-all')
            $('#form-export-excel').submit(function () {
                var data = getFormData('form-export-excel');
                ajaxTransfer("{{route('merchant.toko.acc.report.jurnal.exportExcel')}}", data, '#modal-output');
            });
        });
    </script>
@endsection

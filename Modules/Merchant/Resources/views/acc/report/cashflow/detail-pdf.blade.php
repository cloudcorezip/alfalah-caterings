<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>


            <h1 class="text-center" style="font-size: 20px; font-weight: bold; margin: 0 0 10px 0;">{{strtoupper($merchant->name)}}</h1>
            <h2 class="text-center" style="margin: 0 0 10px 0; font-size: 18px; font-weight: bold;">LAPORAN ARUS KAS PERUSAHAAN</h2>
            <h3 class="text-center" style="margin: 0 0 40px 0; font-size: 14px; font-weight: bold;">{{strtoupper($sub_title)}}</h3>
            <table class="table table-hover">
                <tbody>
                @php
                $previousMonth=0;
                $thisMonth=0;
                @endphp
                @foreach($data as $key => $item)
                    <tr>
                        <td colspan="2"><b>Arus kas dari {{$item->key}}</b></td>
                    </tr>
                    @foreach($item->child as $i =>$j)
                    <tr>
                        <td style="padding-left: 20px !important;"> <a title="Rincian Data" style="color: black" rel="noopener noreferrer">
                                {{$j->name}}
                            </a> </td>
                        <td class="align-right">
                            @if($item->key=='Investasi')
                                {{($j->amount>0)?'('.rupiah($j->amount).')':rupiah(abs($j->amount))}}
                            @else
                                @if(strpos($j->name,'Beban')===false && strpos($j->name,'Persediaan')===false )
                                    @if($j->amount<0)
                                        ({{rupiah(abs($j->amount))}})

                                    @else
                                        {{rupiah($j->amount)}}

                                    @endif
                                @else
                                    @if($j->amount<0)
                                        ({{rupiah(abs($j->amount))}})
                                    @else
                                        @if($j->amount==0)
                                            {{rupiah($j->amount)}}
                                        @else
                                           ({{rupiah($j->amount)}})
                                        @endif
                                    @endif

                                @endif

                            @endif


                        </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td><b>Kas bersih yang diperoleh dari Aktivitas {{$item->key}}</b></td>
                        <td class="align-right"><b>
                                @if($item->sum_all<0)
                                    ({{rupiah(abs($item->sum_all))}})

                                @else
                                    @if($item->key=='Investasi')
                                        {{($item->sum_all>0)?'('.rupiah($item->sum_all).')':rupiah(abs($item->sum_all))}}

                                    @else
                                        {{rupiah($item->sum_all)}}

                                    @endif
                                @endif
                    </b></td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    @php
                        if($item->key=='Investasi'){
                            $previousMonth-=$item->sum_before;
                            $thisMonth-=$item->sum_all;

                         }else{
                            $previousMonth+=$item->sum_before;
                            $thisMonth+=$item->sum_all;

                         }
                    @endphp
                @endforeach
                <tr>
                    <td><b>Saldo Kas Awal Bulan</b></td>
                    <td class="align-right"><b>
                            @if($previousMonth<0)
                                ({{rupiah(abs($previousMonth))}})

                            @else
                                {{rupiah($previousMonth)}}

                            @endif

                        </b></td>
                </tr>
                <tr>
                    <td><b>Arus Kas Periode Berjalan</b></td>
                    <td class="align-right"><b>
                            @if($thisMonth<0)
                                ({{rupiah(abs($thisMonth))}})

                            @else
                                {{rupiah($thisMonth)}}

                            @endif
                        </b></td>
                </tr>
                <tr>
                    <td><b>Saldo Kas Akhir Bulan</b></td>
                    <td class="align-right"><b>
                            @if($thisMonth+$previousMonth<0)
                                ({{rupiah(abs($thisMonth+$previousMonth))}})

                            @else
                                {{rupiah($thisMonth+$previousMonth)}}
                            @endif

                        </b></td>
                </tr>
                </tbody>
            </table>





</body>

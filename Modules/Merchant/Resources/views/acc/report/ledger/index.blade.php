@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all')}}">Keuangan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>Buku Besar</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan rangkuman buku besar dari setiap periode</span>

            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="80%" style="padding: 0.1rem"><h6><b>{{strtoupper($title)}} </b></h6></td>
                <td rowspan="2" class="text-right" style="padding: 0.1rem">
                    <a class="btn btn-success" onclick="exportData()"><i class="fa fa-download mr-2"></i> Download Laporan</a>

                </td>
            </tr>
            <tr>
                <td style="padding: 0.1rem"><h6><b>{{strtoupper($sub_title)}}</b></h6></td>
            </tr>
        </table>
        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <thead>
                    <tr>
                        <th width="10%"><b>Tanggal</b></th>
                        <th width="50%"><b>Transaksi</b></th>
                        <th class="text-right" width="10%"><b>Debit</b></th>
                        <th class="text-right" width="10%"><b>Kredit</b></th>
                        <th class="text-right" width="10%"><b>Saldo</b></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        @foreach($data as $key =>$item)
            <div class="card shadow" style="margin-bottom: 0.5rem">
                <div class="card-body" style="padding:0rem">
                    <table class="table table-borderless" style="margin-bottom: 0rem">
                        <thead>
                        <tr>
                            <th width="10%" style="padding: 0rem"><b></b></th>
                            <th width="50%"  style="padding: 0rem"><b></b></th>
                            <th class="text-right" width="10%"  style="padding: 0rem"><b></b></th>
                            <th class="text-right" width="10%"  style="padding: 0rem"><b></b></th>
                            <th class="text-right" width="10%"  style="padding: 0rem"><b></b></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="5" style="font-weight: bold;border-bottom: 1px solid #EFEFEF ">{{$item->parent_code}} - {{str_replace('.','',$item->code)}} {{$item->name}}</td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>Saldo Awal</b></td>
                            <td class="align-right nowrap" style="text-align: right"><b>{{rupiah($item->sum_of_before_debit)}}</b></td>
                            <td class="align-right nowrap" style="text-align: right"><b>{{rupiah($item->sum_of_before_kredit)}}</b></td>
                            <td class="align-right nowrap" style="text-align: right">
                                <b> @if($item->type_coa=='Debit')
                                        @if($item->sum_of_before_debit<$item->sum_of_before_kredit)
                                            ({{rupiah(abs($item->sum_of_before_kredit-$item->sum_of_before_debit))}})
                                        @else
                                            {{rupiah($item->sum_of_before_debit-$item->sum_of_before_kredit)}}
                                        @endif
                                    @else
                                        @if($item->sum_of_before_kredit<$item->sum_of_before_debit)
                                            ({{rupiah(abs($item->sum_of_before_debit-$item->sum_of_before_kredit))}})
                                        @else
                                            {{rupiah($item->sum_of_before_kredit-$item->sum_of_before_debit)}}
                                        @endif
                                    @endif
                                </b>
                            </td>
                        </tr>
                        @php
                            $initialDebit=$item->sum_of_before_debit-$item->sum_of_before_kredit;
                            $temp=$initialDebit;
                            $initialKredit=$item->sum_of_before_kredit-$item->sum_of_before_debit;
                            $tempKredit=$initialKredit;
                        @endphp
                        @foreach($item->child as $k =>$j)
                            <tr>
                                <td class="center">{{\Carbon\Carbon::parse($j->trans_time)->isoFormat('D MMMM Y')}}</td>
                                <td>{{$j->coa_name==''?$j->trans_purpose : $j->coa_name}}</td>
                                <td class="align-right nowrap" style="text-align: right">{{($j->coa_type=='Debit')?rupiah($j->amount):'-'}}</td>
                                <td class="align-right nowrap" style="text-align: right">{{($j->coa_type=='Kredit')?rupiah($j->amount):'-'}}</td>
                                <td class="align-right nowrap" style="text-align: right">

                                    @if($item->type_coa=='Debit')
                                        @php

                                            if($j->coa_type=='Debit'){
                                                $temp+=$j->amount;
                                            }else{
                                                $temp-=$j->amount;
                                            }

                                        echo ($temp<0)?'('.rupiah(abs($temp)).')':rupiah($temp)
                                        @endphp
                                    @else
                                        @php

                                            if($j->coa_type=='Kredit'){
                                                $tempKredit+=$j->amount;
                                            }else{
                                                $tempKredit-=$j->amount;
                                            }

                                        echo ($tempKredit<0)?'('.rupiah(abs($tempKredit)).')':rupiah($tempKredit)
                                        @endphp
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        <tr style="background-color: #FEF2EB;">
                            <td colspan="2" style="font-weight: bold">Saldo Akhir Akun {{$item->parent_code}} - {{str_replace('.','',$item->code)}} {{$item->name}}</td>
                            <td class="align-right nowrap" style="font-weight: bold;text-align: right">{{rupiah($item->sum_of_before_debit+$item->sum_of_debit)}}</td>
                            <td class="align-right nowrap" style="font-weight: bold;text-align: right">{{rupiah($item->sum_of_before_kredit+$item->sum_of_kredit)}}</td>

                            <td class="align-right nowrap" style="font-weight: bold;text-align: right">
                                @if($item->type_coa=='Debit')
                                    @if(($item->sum_of_before_debit+$item->sum_of_debit)-($item->sum_of_before_kredit+$item->sum_of_kredit)<0)
                                        ({{rupiah(abs(($item->sum_of_before_debit+$item->sum_of_debit)-($item->sum_of_before_kredit+$item->sum_of_kredit)))}})
                                    @else
                                        {{rupiah(($item->sum_of_before_debit+$item->sum_of_debit)-($item->sum_of_before_kredit+$item->sum_of_kredit))}}
                                    @endif
                                @else
                                    @if(($item->sum_of_before_kredit+$item->sum_of_kredit)-($item->sum_of_before_debit+$item->sum_of_debit)<0)
                                        ({{rupiah(abs(($item->sum_of_before_kredit+$item->sum_of_kredit)-($item->sum_of_before_debit+$item->sum_of_debit)))}})
                                    @else
                                        {{rupiah(($item->sum_of_before_kredit+$item->sum_of_kredit)-($item->sum_of_before_debit+$item->sum_of_debit))}}
                                    @endif
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

         @endforeach
        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <thead>
                    <tr>
                        <th width="10%"  style="padding: 0rem"><b></b></th>
                        <th width="50%"  style="padding: 0rem"><b></b></th>
                        <th class="text-right" width="10%"  style="padding: 0rem"><b></b></th>
                        <th class="text-right" width="10%"  style="padding: 0rem"><b></b></th>
                        <th class="text-right" width="10%"  style="padding: 0rem"><b></b></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2" style="font-weight: bold"><b>TOTAL</b></td>
                        <td class="align-right nowrap" style="font-weight: bold;text-align: right"><b>{{rupiah($sumOfCoaAmount['sum_of_total_debit'])}}</b></td>
                        <td class="align-right nowrap" style="font-weight: bold;text-align: right"><b>{{rupiah($sumOfCoaAmount['sum_of_total_kredit'])}}</b></td>
                        <td></td> </tr>
                    </tbody>
                </table>
                @if($data->count()>0)
                    <input name="period_name" id="period_name" type="hidden" value="{{request()->period_name}}">
                    <input name="start_date" id="start_date" type="hidden" value="{{request()->start_date}}">
                    <input name="end_date" id="end_date" type="hidden" value="{{request()->end_date}}">
                    <input name="coa" id="coa" type="hidden" value="{{request()->coa}}">
                @endif
            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>

        <form method="get" class="form-horizontal">
            <div class="row px-30">
            <div class="col-md-12">
               <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="end_date" value="{{$endDate}}" name="end_date">

                    </div>
                </div>

                <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Pilih Cabang</label>
                            <select name="md_merchant_id" id="branch" class="form-control form-control" onchange="getCoa()">
                                @foreach(get_cabang() as $key  =>$item)
                                    <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <input class="btn btn-success btn-block" type="submit" value="Tampilkan Data" data-original-title="" title="" autocomplete="off">
                        <a href="{{route('merchant.toko.acc.report.ledger.index')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>
                        <input name="is_download" type="hidden" value="0" autocomplete="off">
                        <input name="today" type="hidden" id="today" value="{{$date}}" autocomplete="off">
                        <input name="coa_value" type="hidden" id="coa_filter" value="{{$coa}}" autocomplete="off">
                    </div>
                </div>

            </div>
        </form>
    </div>
@endsection

@section('js')
    <script>
        const reloadDataTable = () => void 0;
        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {

                const data = new FormData();
                data.append('start_date',$('#start_date').val());
                data.append('end_date',$('#end_date').val());
                data.append('coa', $('#coa').val());
                data.append('md_merchant_id','{{$merchantId}}');

                ajaxTransfer("{{route('merchant.toko.acc.report.ledger.exportExcel')}}", data, '#modal-output');
            });

        }
        $(document).ready(function() {
            $("#branch").select2({});
        });

    </script>
@endsection

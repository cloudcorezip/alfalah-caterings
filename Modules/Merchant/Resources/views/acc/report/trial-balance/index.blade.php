@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all')}}">Keuangan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>Neraca Saldo</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan rangkuman neraca keuangan dari usahamu</span>

            </div>
        </div>
    </div>

    <!-- DataTales Example -->

    <div class="container-fluid">
        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="80%" style="padding: 0.1rem"><h6><b>{{strtoupper($title)}} </b></h6></td>
                <td rowspan="2" class="text-right" style="padding: 0.1rem">
                    <a class="btn btn-success" onclick="exportData()"><i class="fa fa-download mr-2"></i> Download Laporan</a>

                </td>
            </tr>
            <tr>
                <td style="padding: 0.1rem"><h6><b>{{strtoupper($sub_title)}}</b></h6></td>
            </tr>
        </table>
        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <thead>
                    <tr>
                        <th class="text-left" width="75%"><b>Nama Akun</b></th>
                        <th class="text-right" width="25%"><b>Saldo Akhir</b></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        @php
            $endOfTotal=0;
        @endphp

        @php
            $totalDebit=0;
            $totalKredit=0;
        @endphp
        @foreach($data as $key =>$item)

            <div class="card shadow" style="margin-bottom: 0.5rem">
                <div class="card-body" style="padding:0rem">
                    <table class="table table-borderless" style="margin-bottom: 0rem">
                        <thead>
                        <tr>
                            <th width="75%" style="padding: 0rem"><b></b></th>
                            <th width="25%"  style="padding: 0rem"><b></b></th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $totalOfCategory=0;
                        @endphp
                        <tr>
                            <td style="font-weight: bold;border-bottom: 1px solid #EFEFEF">
                                <button class="btn btn-default btn-xs" data-toggle="collapse" data-target="#{{str_replace(' ','',$item->name)}}" aria-expanded="false" aria-controls="collapseExample"><span class="iconify" data-icon="ep:arrow-down-bold"></span>
                                </button>
                                {{$item->parent_code}} - {{str_replace('.','',$item->code)}} {{$item->name}}</td>
                            <td style="border-bottom: 1px solid #EFEFEF"></td>

                        </tr>
                        @if(!empty($item->detail))
                            @php
                                $subOfDebit=0;
                                $subOfKredit=0;
                                $subOfEndBalance=0;
                                $det1=collect($item->detail)->sortBy('id');
                            @endphp
                            @foreach($det1 as $d =>$dd2)
                                <tr class="collapse" id="{{str_replace(' ','',$item->name)}}">
                                    <td><span class="indent-dash" style="width: 20px;"></span>{{$item->parent_code}} - {{str_replace('.','',$dd2->code)}} {{$dd2->name}}</td>
                                    @php
                                        if($dd2->type_coa=='Debit'){
                                            $firstBalance=$dd2->sum_of_before_debit-$dd2->sum_of_before_kredit;
                                        }else{
                                            $firstBalance=$dd2->sum_of_before_kredit-$dd2->sum_of_before_debit;
                                        }
                                    @endphp
                                    <td class="text-right">
                                        @php
                                            if($dd2->type_coa=='Debit')
                                             {
                                                 $endBalance=$firstBalance+$dd2->sum_of_debit-$dd2->sum_of_kredit;

                                             }else{
                                                 $endBalance=$firstBalance+$dd2->sum_of_kredit-$dd2->sum_of_debit;

                                             }
                                        @endphp
                                        @if($endBalance<0)
                                            ({{rupiah(abs($endBalance))}})
                                        @else
                                            {{rupiah($endBalance)}}
                                        @endif
                                    </td>
                                    @php
                                        $subOfDebit+=$dd2->sum_of_debit;
                                        $subOfKredit+=$dd2->sum_of_kredit;
                                        if(strpos($dd2->name,'Akumulasi')!==false)
                                        {
                                            $subOfEndBalance-=$endBalance;

                                        }else{
                                            $subOfEndBalance+=$endBalance;

                                        }
                                    @endphp
                                </tr>
                            @endforeach
                            <tr class="collapse" id="{{str_replace(' ','',$item->name)}}" style="font-weight: bold;background-color: #FEF2EB">
                                <td class="text-left"> <b>Total {{$item->name}}</b></td>
                                <td class="text-right">
                                    @if($subOfEndBalance<0)
                                        ({{rupiah(abs($subOfEndBalance))}})
                                    @else
                                        {{rupiah($subOfEndBalance)}}
                                    @endif
                                </td>
                                @php
                                    $totalDebit+=$subOfDebit;
                                    $totalKredit+=$subOfKredit;
                                    $endOfTotal+=$subOfEndBalance;
                                    $totalOfCategory+=$subOfEndBalance;
                                @endphp
                            </tr>

                        @endif

                        @php
                            $json1=collect($item->json)->sortBy('id');
                        @endphp

                        @foreach($json1 as $n =>$c)
                            <tr class="collapse" id="{{str_replace(' ','',$item->name)}}">
                                <td style="font-weight: bold"><span class="indent-dash" style="width: 10px;"></span>{{$item->parent_code}} - {{str_replace('.','',$c->code)}} {{$c->name}}</td>
                                <td></td>
                            </tr>
                            @php
                                $json2=collect($c->json)->sortBy('id');
                            @endphp
                            @if($json2->count()>0)
                                @foreach($json2 as $c2 =>$cc)
                                    <tr class="collapse" id="{{str_replace(' ','',$item->name)}}">
                                        <td style="font-weight: bold"><span class="indent-dash" style="width: 15px;"></span>{{$item->parent_code}} - {{str_replace('.','',$cc->code)}} {{$cc->name}}</td>
                                        <td></td>
                                    </tr>
                                    @php
                                        $subOfDebit=0;
                                        $subOfKredit=0;
                                        $subOfEndBalance=0;
                                        $det2=collect($cc->detail)->sortBy('id');

                                    @endphp
                                    @foreach($det2 as $d =>$dd)
                                        <tr class="collapse" id="{{str_replace(' ','',$item->name)}}">
                                            <td><span class="indent-dash" style="width: 20px;"></span>{{$item->parent_code}} - {{str_replace('.','',$dd->code)}} {{$dd->name}}</td>
                                            @php
                                                if($dd->type_coa=='Debit'){
                                                    $firstBalance=$dd->sum_of_before_debit-$dd->sum_of_before_kredit;
                                                }else{
                                                    $firstBalance=$dd->sum_of_before_kredit-$dd->sum_of_before_debit;

                                                }
                                            @endphp
                                            <td class="text-right">
                                                @php
                                                    if($dd->type_coa=='Debit')
                                                     {
                                                         $endBalance=$firstBalance+$dd->sum_of_debit-$dd->sum_of_kredit;

                                                     }else{
                                                         $endBalance=$firstBalance+$dd->sum_of_kredit-$dd->sum_of_debit;
                                                     }
                                                @endphp
                                                @if($endBalance<0)
                                                    ({{rupiah(abs($endBalance))}})
                                                @else
                                                    {{rupiah($endBalance)}}
                                                @endif
                                            </td>
                                            @php
                                                $subOfDebit+=$dd->sum_of_debit;
                                                $subOfKredit+=$dd->sum_of_kredit;
                                                if(strpos($dd->name,'Akumulasi')!==false)
                                                {
                                                    $subOfEndBalance-=$endBalance;

                                                }else{
                                                    $subOfEndBalance+=$endBalance;

                                                }
                                            @endphp
                                        </tr>
                                    @endforeach
                                    @if($det2->count()>0)
                                        <tr class="collapse" id="{{str_replace(' ','',$item->name)}}" style="font-weight: bold;background-color: #FEF2EB">
                                            <td class="text-left"><span class="indent-dash" style="width: 15px;"></span><b>Total {{$cc->name}}</b></td>
                                            <td class="text-right">
                                                @if($subOfEndBalance<0)
                                                    ({{rupiah(abs($subOfEndBalance))}})
                                                @else
                                                    {{rupiah($subOfEndBalance)}}
                                                @endif
                                            </td>
                                            @php
                                                $totalDebit+=$subOfDebit;
                                                $totalKredit+=$subOfKredit;
                                                $endOfTotal+=$subOfEndBalance;
                                                $totalOfCategory+=$subOfEndBalance;

                                            @endphp
                                        </tr>

                                    @endif
                                @endforeach
                            @else
                                @php
                                    $det3=collect($c->detail)->sortBy('id');
                                @endphp
                                @if($det3->count()>0)
                                    @php
                                        $subOfDebit=0;
                                        $subOfKredit=0;
                                        $subOfEndBalance=0;
                                    @endphp
                                    @foreach($det3 as $d =>$dd1)
                                        <tr class="collapse" id="{{str_replace(' ','',$item->name)}}">
                                            <td><span class="indent-dash" style=" width: 20px;"></span>{{$item->parent_code}} - {{str_replace('.','',$dd1->code)}} {{$dd1->name}}</td>
                                            @php
                                                if($dd1->type_coa=='Debit'){
                                                    $firstBalance=$dd1->sum_of_before_debit-$dd1->sum_of_before_kredit;
                                                }else{
                                                    $firstBalance=$dd1->sum_of_before_kredit-$dd1->sum_of_before_debit;

                                                }
                                            @endphp

                                            <td class="text-right">
                                                @php
                                                    if($dd1->type_coa=='Debit')
                                                     {
                                                         $endBalance=$firstBalance+$dd1->sum_of_debit-$dd1->sum_of_kredit;

                                                     }else{
                                                         $endBalance=$firstBalance+$dd1->sum_of_kredit-$dd1->sum_of_debit;

                                                     }
                                                @endphp
                                                @if($endBalance<0)
                                                    ({{rupiah(abs($endBalance))}})
                                                @else
                                                    {{rupiah($endBalance)}}
                                                @endif
                                            </td>
                                            @php
                                                $subOfDebit+=$dd1->sum_of_debit;
                                                $subOfKredit+=$dd1->sum_of_kredit;
                                                if(strpos($dd1->name,'Akumulasi')!==false)
                                                {
                                                    $subOfEndBalance-=$endBalance;

                                                }else{
                                                    $subOfEndBalance+=$endBalance;

                                                }
                                            @endphp
                                        </tr>
                                    @endforeach
                                    <tr class="collapse" id="{{str_replace(' ','',$item->name)}}" style="font-weight: bold;background-color: #FEF2EB">
                                        <td class="text-left"><span class="indent-dash" style="width: 10px;"></span><b>Total {{$c->name}}</b></td>

                                        <td class="text-right">
                                            @if($subOfEndBalance<0)
                                                ({{rupiah(abs($subOfEndBalance))}})
                                            @else
                                                {{rupiah($subOfEndBalance)}}
                                            @endif
                                        </td>
                                        @php
                                            $totalDebit+=$subOfDebit;
                                            $totalKredit+=$subOfKredit;
                                            $endOfTotal+=$subOfEndBalance;
                                            $totalOfCategory+=$subOfEndBalance;

                                        @endphp
                                    </tr>

                                @endif
                            @endif
                        @endforeach
                        <tr>
                            <td class="text-left"><b>Total {{$item->name}}</b></td>
                            <td class="text-right">
                                <b>@if($totalOfCategory<0)
                                        ({{rupiah(abs($totalOfCategory))}})
                                    @else
                                        {{rupiah($totalOfCategory)}}
                                    @endif
                                </b>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <tbody>
                    <tr>
                        <td class="text-left" width="75%"><b>TOTAL</b></td>
                        <td class="text-right" width="25%">
                            <b>@if($endOfTotal<0)
                                    ({{rupiah(abs($endOfTotal))}})
                                @else
                                    {{rupiah($endOfTotal)}}
                                @endif
                            </b>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="" method="get" class="form-horizontal">
            <div class="row px-30">
            <div class="col-md-12">
                <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="end_date" value="{{$endDate}}" name="end_date">

                    </div>
                </div>
                <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Pilih Cabang</label>
                            <select name="md_merchant_id" id="branch" class="form-control form-control">
                                @foreach(get_cabang() as $key  =>$item)
                                    <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input class="btn btn-success btn-block" type="submit" value="Tampilkan" data-original-title="" title="" autocomplete="off">
                        <a href="{{route('merchant.toko.acc.report.trial-balance.index')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>
                        <input name="is_download" type="hidden" value="0" autocomplete="off">
                        <input name="period" type="hidden" id="period_filter" value="{{$period}}" autocomplete="off">
                        <input name="today" type="hidden" id="today" value="{{date('Y-m-d')}}" autocomplete="off">

                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('js')
    <script>
         const reloadDataTable = () => void 0;
        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {

                const data = new FormData();
                data.append('start_date','{{$startDate}}');
                data.append('end_date','{{$endDate}}');
                data.append('md_merchant_id',$("#branch").val());
                ajaxTransfer("{{route('merchant.toko.acc.report.trial-balance.exportExcel')}}", data, '#modal-output');
            });

        }
        $(document).ready(function() {
            @if(merchant_detail()->is_branch==0)
            $("#branch").select2({});
            @endif
            // $("#start_date").val(moment($("#today").val(),'YYYY-MM-DD').startOf('year').format('YYYY-MM-DD'))
            // $("#end_date").val(moment($("#today").val(),'YYYY-MM-DD').endOf('year').format('YYYY-MM-DD'))
            $('#form-export-excel').submit(function () {
                var data = getFormData('form-export-excel');
                ajaxTransfer("{{route('merchant.toko.acc.report.jurnal.exportExcel')}}", data, '#modal-output');
            });
        });
    </script>


@endsection

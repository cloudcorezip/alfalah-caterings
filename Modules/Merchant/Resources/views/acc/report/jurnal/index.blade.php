@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all')}}">Keuangan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>Jurnal</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut aktivitas penjurnalan transaksi keluar masuk usahamu</span>

            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="80%" style="padding: 0.1rem"><h6><b>{{strtoupper($title)}}  {{strtoupper($merchant->name)}}</b></h6></td>
                <td rowspan="2" class="text-right" style="padding: 0.1rem">
                    <a class="btn btn-success" onclick="exportData()"><i class="fa fa-download mr-2"></i> Download Laporan</a>

                </td>
            </tr>
            <tr>
                <td style="padding: 0.1rem"><h6><b>{{strtoupper($sub_title)}}</b></h6></td>
            </tr>
        </table>


        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <thead>
                    <tr>
                        <th style="padding: 11px" width="50%" class="font-weight-bold">Akun</th>
                        <th class="text-right font-weight-bold" style="padding: 11px" width="25%">Debit</th>
                        <th class="text-right font-weight-bold" style="padding: 11px" width="25%">Kredit</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        @foreach($data as $n => $item)
            @php
                $first=$item->where('coa_type','Debit')->where('name','!=','Administrasi Bank')->sortBy('ajd_id')->first();
            @endphp
            <div class="card shadow" style="margin-bottom: 0.5rem">
                <div class="card-body" style="padding:0rem">
                    <table class="table table-borderless" style="margin-bottom: 0rem">
                        <thead>
                        <tr>
                            <th width="50%" style="padding: 0rem"><b></b></th>
                            <th width="25%"  style="padding: 0rem"><b></b></th>
                            <th width="25%"  style="padding: 0rem"><b></b></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="background: #FEF2EB">
                            <td colspan="3" style="font-weight: bold;border: none;padding: 10px">{{\Carbon\Carbon::parse($first->trans_time)->isoFormat('D MMMM Y')}} | {{$first->trans_name}}</td>
                        </tr>
                        <tr>
                            <td>{{$first->parent_code}} - {{str_replace('.','',$first->code)}} {{$first->name}} </td>

                            <td class=" text-right"> {{($first->coa_type=='Debit')?rupiah($first->amount):'-'}}</td>
                            <td class=" text-right"> {{($first->coa_type=='Kredit')?rupiah($first->amount):'-'}}  </td>
                        </tr>
                        @foreach($item->where('jurnal_detail_id','!=',$first->jurnal_detail_id)->sortBy('coa_type') as $d)
                            <tr >
                                <td
                                    @if($d->coa_type=='Debit')
                                    @else
                                    style="padding-left: 30px"
                                    @endif
                                > {{$d->parent_code}} - {{str_replace('.','',$d->code)}} {{$d->name}} </td>
                                <td class="text-right"
                                > {{($d->coa_type=='Debit')?rupiah($d->amount):'-'}} </td>
                                <td class="text-right" > {{($d->coa_type=='Kredit')?rupiah($d->amount):'-'}}  </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <thead>
                    <tr>
                        <th width="50%" style="padding: 0rem"><b></b></th>
                        <th width="25%"  style="padding: 0rem"><b></b></th>
                        <th width="25%"  style="padding: 0rem"><b></b></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th style="font-weight: bold">TOTAL</th>
                        <th class="text-right">{{rupiah($sumOfAmount[0]->sum_of_debit)}}</th>
                        <th class="text-right">{{rupiah($sumOfAmount[0]->sum_of_kredit)}}</th>
                    </tbody>
                </table>
                <br>
                <div class="col-md-12">
                    {{$data->withQueryString()->links('vendor.pagination.with-showing-entry')}}
                </div>
                <input name="end_date" id="end_date" type="hidden" value="{{request()->end_date}}">
                <input name="start_date" id="start_date" type="hidden" value="{{request()->start_date}}">
                <input name="period_name" id="period_name" type="hidden" value="{{request()->period_name}}">
                <input name="trans_code" id="trans_code" type="hidden" value="{{request()->trans_code}}">
                <input name="ref_code" id="ref_code" type="hidden" value="{{request()->ref_code}}">
                <br>
            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="" method="get" class="form-horizontal">
            <div class="row px-30">

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="end_date" value="{{$endDate}}" name="end_date">

                    </div>
                </div>

                <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Pilih Cabang</label>
                            <select name="md_merchant_id" id="branch" class="form-control form-control">
                                @foreach(get_cabang() as $key  =>$item)
                                    <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                                @endforeach
                            </select>
                        </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Kode Jurnal</label>
                        <input type="text" class="form-control form-control" name="trans_code">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Kode Referensi</label>
                        <input type="text" class="form-control form-control" name="ref_code">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <input class="btn btn-success btn-block" type="submit" value="Tampilkan Data" data-original-title="" title="" autocomplete="off">
                        <a href="{{route('merchant.toko.acc.report.jurnal.index')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>
                        <input name="is_download" type="hidden" value="0" autocomplete="off">
                        <input name="today" type="hidden" id="today" value="{{$date}}" autocomplete="off">
                    </div>
                </div>
            </div>
        </form>


    </div>
@endsection

@section('js')
    <script>
        const reloadDataTable = () => void 0;
        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                const data = new FormData();
                data.append('start_date',$('#start_date').val());
                data.append('end_date',$('#end_date').val());
                data.append('trans_code', $('#trans_code').val());
                data.append('ref_code', $('#ref_code').val());
                data.append('md_merchant_id','{{$merchantId}}');
                ajaxTransfer("{{route('merchant.toko.acc.report.jurnal.exportExcel')}}", data, '#modal-output');
            });

        }
        $(document).ready(function() {
            $("#branch").select2({});

            $('#form-export-excel').submit(function () {
               var data = getFormData('form-export-excel');
                ajaxTransfer("{{route('merchant.toko.acc.report.jurnal.exportExcel')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            });
        });


    </script>
@endsection

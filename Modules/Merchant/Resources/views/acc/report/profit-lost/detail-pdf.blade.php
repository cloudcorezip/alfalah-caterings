<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
    

    <div id="layout-print-intern">
        <h1 class="text-center" style="font-size: 20px; font-weight: bold; margin: 0 0 10px 0;">{{strtoupper($merchant->name)}}</h1>
        <h2 class="text-center" style="margin: 0 0 10px 0; font-size: 18px; font-weight: bold;">LAPORAN LABA RUGI PERUSAHAAN</h2>
        <h3 class="text-center" style="margin: 0 0 40px 0; font-size: 14px; font-weight: bold;">{{strtoupper($sub_title)}}</h3>
        <table class="table table-hover">
            <tbody>
                @php
                $sumAllIncrement=0;
                $sumAllDecrement=0;
                @endphp
                @foreach($data as $key =>$item)
                    @php
                    $total=0;
                    @endphp
                    <tr>
                        <td><b>{{strtoupper($item->name)}}</b></td>
                        <td></td>
                    </tr>
                    @foreach($item->child as $c =>$child)
                        <tr>
                            <td style="padding-left: 20px !important;"> <a style="color: black" title="Rincian Data" rel="noopener noreferrer">
                                    {{$child->name}}</a>
                            </td>
                            <td class="align-right">
                                @if($child->amount<0)
                                    ({{rupiah(abs($child->amount))}})
    
                                @else
                                    {{rupiah($child->amount)}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td><b>TOTAL {{strtoupper($item->name)}}</b></td>
                        <td class="align-right"><b>
                                @if($item->total<0)
                                    ({{rupiah(abs($item->total))}})
                                @else
                                    {{rupiah($item->total)}}
                                @endif
                            </b>
                        </td>
                        @php
                           $total+=$item->total;
                        @endphp
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    @php
                    if($item->key=='pendapatan'){
                      $sumAllIncrement+=$total;
                     }else{
                        $sumAllDecrement+=$total;
    
                     }
                    @endphp
                @endforeach
    
                <tr>
                    <td><b>TOTAL LABA/RUGI PERUSAHAAN</b></td>
                    <td class="align-right"><b>
                            @if($sumAllIncrement-$sumAllDecrement<0)
                                ({{rupiah(abs($sumAllIncrement-$sumAllDecrement))}})
                            @else
                                {{rupiah($sumAllIncrement-$sumAllDecrement)}}
                            @endif
                        </b>
                    </td>
                </tr>
                </tbody>
        </table>
    </div>
</body>
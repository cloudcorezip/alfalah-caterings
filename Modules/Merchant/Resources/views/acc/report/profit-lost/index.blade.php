@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all')}}">Keuangan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan rangkuman laporan laba rugi  dari usahamu</span>

            </div>
        </div>
    </div>

    <div class="container-fluid">

        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="80%" style="padding: 0.1rem"><h6><b>{{strtoupper($title)}}</b></h6></td>
                <td rowspan="2" class="text-right" style="padding: 0.1rem">
                    <a class="btn btn-success" onclick="exportData()"><i class="fa fa-download mr-2"></i> Download Laporan</a>

                </td>
            </tr>
            <tr>
                <td style="padding: 0.1rem"><h6><b>{{strtoupper($sub_title)}}</b></h6></td>
            </tr>
        </table>
        @php
            $countMerchant=$merchantList->count();
        @endphp
        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <tbody>
                    <tr>
                        <td width="50%"></td>
                        @foreach($merchantList as $mm)
                            <td class="text-right" width="25%"><b>{{strtoupper($mm->name)}}</b></td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @foreach($data as $key =>$item)
            <div class="card shadow" style="margin-bottom: 0.5rem">
                <div class="card-body" style="padding:0rem">
                    <table class="table table-borderless" style="margin-bottom: 0rem">
                        <tbody>
                        <tr>
                            <td colspan="{{$countMerchant+1}}" style="border-bottom: 1px solid #EFEFEF"><b>{{strtoupper($item->name)}}</b></td>
                        </tr>
                        @foreach($item->child as $c =>$child)
                            <tr>
                                <td style="padding-left: 20px !important;" width="50%">
                                    {{$child->name}}
                                </td>
                                @foreach($child->value as $v)
                                    <td class="text-right" width="25%">
                                        @if($v->amount<0)
                                            ({{rupiah(abs($v->amount))}})

                                        @else
                                            {{rupiah($v->amount)}}
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        <tr style="background-color: #FEF2EB">
                            <td width="50%"><b>TOTAL {{strtoupper($item->name)}}</b></td>
                            @foreach($item->totals as $t)
                                <td class="text-right" width="25%"><b>
                                        @if($t->total<0)
                                            ({{rupiah(abs($t->total))}})
                                        @else
                                            {{rupiah($t->total)}}
                                        @endif
                                    </b>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        @endforeach
        <div class="card shadow" style="margin-bottom: 0.5rem">
            <div class="card-body" style="padding:0rem">
                <table class="table table-borderless" style="margin-bottom: 0rem">
                    <tbody>
                    <tr>
                        <td width="50&"><b>TOTAL LABA/RUGI</b></td>
                        @foreach($summary as $ss)
                            <td class="text-right" width="25%"><b>
                                    @if($ss->sumAllIncrement-$ss->sumAllDecrement<0)
                                        ({{rupiah(abs($ss->sumAllIncrement-$ss->sumAllDecrement))}})
                                    @else
                                        {{rupiah($ss->sumAllIncrement-$ss->sumAllDecrement)}}
                                    @endif
                                </b>
                            </td>

                        @endforeach
                    </tr>
                    </tbody>
                </table>
                <br>
                {{$merchantList->withQueryString('vendor.pagination.with-showing-entry')->links()}}
                <br>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>

        <form method="get" class="form-horizontal">
            <div class="row px-30">

               <div class="col-md-12">
               <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="end_date" value="{{$endDate}}" name="end_date">

                    </div>
                </div>

                <div class="col-md-12 mb-3">
                        <label>Pilih Cabang yang ingin dibandingkan</label>
                        <select name="md_merchant_id[]" id="branch" class="form-control" multiple="multiple" required>
                            <option value="-1"  @if(in_array('-1',$merchantId)) selected @endif>Semua Cabang</option>
                            @foreach(get_cabang() as $key  =>$item)
                                <option value="{{$item->id}}" @if(in_array($item->id,$merchantId)) selected @endif>{{$item->nama_cabang}}</option>
                            @endforeach
                        </select>
                    </div>

                 <div class="col-md-12">
                    <div class="form-group">
                        <input class="btn btn-success btn-block" type="submit" value="Tampilkan" data-original-title="" title="" autocomplete="off">
                        <a href="{{route('merchant.toko.acc.report.profit-lost.index')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>
                        <input name="is_download" type="hidden" value="0" autocomplete="off">
                        <input name="today" type="hidden" id="today" value="{{$date}}" autocomplete="off">

                    </div>
                 </div>
            </div>
        </form>
    </div>

@endsection

@section('js')
    <script>
        const reloadDataTable = () => void 0;
        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {

                let selectedMerchantId = $("#branch").select2('data');
                let merchantIds = [];

                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
                const data = new FormData();
                data.append('start_date','{{$startDate}}');
                data.append('end_date','{{$endDate}}');
                data.append('md_merchant_id',JSON.stringify(merchantIds));

                ajaxTransfer("{{route('merchant.toko.acc.report.profit-lost.exportExcel')}}", data, '#modal-output');
            });

        }

        $("#branch").on("select2:select", function(e){
            if(e.params.data.id == "-1"){
                $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
            } else {
                $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
            }
        });
        $(document).ready(function() {
            $("#branch").select2({});

            $('#form-export-excel').submit(function () {
                var data = getFormData('form-export-excel');
                ajaxTransfer("{{route('merchant.toko.acc.report.jurnal.exportExcel')}}", data, '#modal-output');
            });
        });


    </script>
@endsection

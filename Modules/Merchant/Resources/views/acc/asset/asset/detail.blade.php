@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Aset Usaha</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.asset.asset.index')}}">Data Aset</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan detail aset usahamu</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="50%" style="padding: 0.1rem;font-size: 14px"><b>Pembelian Aset</b></td>
                <td class="text-right" width="50%" style="padding: 0.1rem;font-size: 14px"><b>Nilai Aset Terkini</b></td>

            </tr>
            <tr class="mt-2">
                <td style="padding: 0.1rem"><br><h5><b>{{$data->asset_name}}</b></h5></td>
                <td style="padding: 0.1rem" class="text-right"><br><h5><b>{{($data->is_depreciation==1)?rupiah($data->current_asset_value):rupiah($data->asset_value)}}</b></h5></td>

            </tr>
        </table>
        <div class="row">
            @if($data->is_paid==0 || $payCount>0)
            <div class="col-md-4">
             @else
             <div class="col-md-12">
             @endif
                <div class="card shadow mb-4">
                    @if($data->is_paid==0 || $payCount>0)
                        @if($image)
                            <img class="card-img-top" src="{{env('S3_URL')}}{{$data->file_asset}}" alt="Card image cap">

                        @else
                            <img class="card-img-top" src="{{asset('public/backend/img/no-image-found.png')}}" alt="Card image cap">

                        @endif
                    @endif
                    @if($data->is_paid==0 || $payCount>0)
                    <div class="card-header" style="border: none">
                        <table style="border: 0" width="100%">
                            <tr>
                                <td class="text-left"><h5 style="margin-top: 5%"><span class="fa fa-list mr-1"></span>Informasi Aset</h5></td>
                                <td class="text-right">
                                    <a href="{{route('merchant.asset.asset.add',['id'=>$data->id])}}" class="btn btn-success-light-xs btn-xs" data-original-title="" title="" rel="noopener noreferrer"
                                       style="color: #4F8F1D">
                                        <i class="fa fa-pen mr-1"></i> Ubah Data</a>
                                    <a onclick="hapusData({{$data->id}})" class="btn btn-delete-xs btn-xs"  data-original-title="" title="" rel="noopener noreferrer">
                                        <span class="fa fa-trash-alt" style="padding: 3px"></span>
                                    </a>

                                </td>
                            </tr>
                        </table>
                    </div>
                     @endif

                        @if($data->is_paid==0 || $payCount>0)
                            <div class="card-body">
                                <table class="table-note" width="100%">
                                    <tr>
                                        <td class="text-left">Cabang</td>
                                        <td class="text-right font-weight-bold">
                                            {{$data->getMerchant->name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Status Aset</td>
                                        <td class="text-right font-weight-bold">
                                            @if($data->is_paid==0)
                                                <button style="color: #D33434;border-radius: 5px;border: none;background: #FAE7E7; padding:5px 10px 5px 10px" class="font-weight-bold">Belum Lunas</button>

                                            @else
                                                <button style="color: white;border-radius: 5px;border: none;background: #72BA6C; padding:5px 10px 5px 10px" class="font-weight-bold">Lunas</button>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kategori Aset</td>
                                        <td class="text-right font-weight-bold">
                                            {{$data->getCategory->name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Supplier/Vendor</td>
                                        <td class="text-right font-weight-bold">{{is_null($data->getSupplier)?'-':$data->getSupplier->name}}</td>

                                    </tr>
                                    <tr>
                                        <td class="text-left">Waktu Pembelian</td>
                                        <td class="text-right font-weight-bold">{{\Carbon\Carbon::parse($data->record_time)->isoFormat('D MMMM Y')}}</td>

                                    </tr>
                                    <tr>
                                        <td class="text-left">Harga Beli</td>
                                        <td class="text-right font-weight-bold">{{rupiah($data->asset_value)}}</td>
                                    </tr>

                                    <tr>
                                        <td class="text-left">Aset Terdepresiasi</td>
                                        <td class="text-right font-weight-bold">{{($data->is_depreciation==1)?'Ya':'Tidak'}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Deskripsi</td>
                                        <td class="text-right font-weight-bold">
                                            {{$data->desc}}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="text-left">Bukti Transaksi</td>
                                        <td class="text-right font-weight-bold">{!!  is_null($data->trans_proof)?'-':'<a href="'.env('S3_URL').$data->trans_proof.'" target="_blank" style="color:green">Download File</a>'!!}</td>


                                    </tr>

                                </table>
                            </div>

                         @else
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="container">
                                        <table class="table-note" width="100%" style="margin-top: 5%">
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    @if($image)
                                                        <img class="card-img-top" src="{{env('S3_URL')}}{{$data->file_asset}}" style="min-height: 270px" alt="Card image cap">

                                                    @else
                                                        <img class="card-img-top" src="{{asset('public/backend/img/no-image-found.png')}}" style="min-height: 270px" alt="Card image cap">
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                        @if($data->is_intangible_asset==0)
                                            @if($data->is_sell==0)
                                        <button class="btn btn-success btn-block" onclick="loadModalFullScreen(this)"
                                                target="{{route('merchant.asset.asset.add-sell')}}" data="acc_depreciation_id={{$data->id}}"
                                        >
                                            <i class="fa fa-money-bill mr-1"></i>Jual Aset
                                        </button>
                                                @else
                                                <button class="btn btn-danger btn-block" onclick="cancelSell({{$data->id}})">
                                                    <i class="fa fa-remove mr-1"></i>Batalkan Penjualan
                                                </button>
                                                @endif
                                        @endif

                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="container">
                                        <table class="table-note" width="96%">
                                            <tr>
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-left"><h5 style="margin-top: 5%"><span class="fa fa-list mr-1"></span>Informasi Aset</h5></td>
                                                <td class="text-right">
                                                    <a href="{{route('merchant.asset.asset.add',['id'=>$data->id])}}" class="btn btn-success-light-xs btn-xs" data-original-title="" title="" rel="noopener noreferrer"
                                                       style="color: #4F8F1D">
                                                        <i class="fa fa-pen mr-1"></i> Ubah Data</a>
                                                    <a onclick="hapusData({{$data->id}})" class="btn btn-delete-xs btn-xs"  data-original-title="" title="" rel="noopener noreferrer">
                                                        <span class="fa fa-trash-alt" style="padding: 3px"></span>
                                                    </a>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-left">Cabang</td>
                                                <td class="text-right font-weight-bold">
                                                    {{$data->getMerchant->name}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-left">Status Aset</td>
                                                <td class="text-right font-weight-bold">
                                                    @if($data->is_paid==0)
                                                        <button style="color: #D33434;border-radius: 5px;border: none;background: #FAE7E7; padding:5px 10px 5px 10px" class="font-weight-bold">Belum Lunas</button>

                                                    @else
                                                        <button style="color: white;border-radius: 5px;border: none;background: #72BA6C; padding:5px 10px 5px 10px" class="font-weight-bold">Lunas</button>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-left">Kategori Aset</td>
                                                <td class="text-right font-weight-bold">
                                                    {{$data->getCategory->name}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-left">Supplier/Vendor</td>
                                                <td class="text-right font-weight-bold">{{is_null($data->getSupplier)?'-':$data->getSupplier->name}}</td>

                                            </tr>
                                            <tr>
                                                <td class="text-left">Waktu Pembelian</td>
                                                <td class="text-right font-weight-bold">{{\Carbon\Carbon::parse($data->record_time)->isoFormat('D MMMM Y')}}</td>

                                            </tr>
                                            <tr>
                                                <td class="text-left">Harga Beli</td>
                                                <td class="text-right font-weight-bold">{{rupiah($data->asset_value)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-left">Aset Terdepresiasi</td>
                                                <td class="text-right font-weight-bold">{{($data->is_depreciation==1)?'Ya':'Tidak'}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-left">Deskripsi</td>
                                                <td class="text-right font-weight-bold">
                                                    {{$data->desc}}
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="text-left">Bukti Transaksi</td>
                                                <td class="text-right font-weight-bold">{!!  is_null($data->trans_proof)?'-':'<a href="'.env('S3_URL').$data->trans_proof.'" target="_blank" style="color:green">Download File</a>'!!}</td>


                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                </td>
                                            </tr>

                                        </table>

                                    </div>
                                </div>
                            </div>
                        @endif
                </div>
            </div>

            @if($data->is_paid==0 || $payCount>0)
                    <div class="col-md-8">
                        <div class="card shadow mb-4" style="min-height: 619px">
                            <div class="card-header">
                                <table style="border: 0" width="100%">
                                    <tr>
                                        <td class="text-left"><h4>Pembayaran</h4></td>
                                        <td class="text-right">
                                            @if($data->is_paid==0 && $data->is_deleted==0)
                                                <a onclick="loadModalFullScreen(this)" target="{{route('merchant.asset.asset.add-payment')}}" data="acc_depreciation_id={{$data->id}}" class="btn btn-success btn-md" data-original-title="" title="" style="color: white"><span class="fa fa-money-bill mr-1"></span> Setor Pembayaran</a>
                                            @else
                                                @if($data->is_intangible_asset==0)
                                                    @if($data->is_sell==0)
                                                <button class="btn btn-success btn-block" onclick="loadModalFullScreen(this)"
                                                        target="{{route('merchant.asset.asset.add-sell')}}" data="acc_depreciation_id={{$data->id}}">
                                                    <i class="fa fa-money-bill mr-1"></i>Jual Aset
                                                </button>
                                                    @else
                                                        <button class="btn btn-danger btn-block" onclick="cancelSell({{$data->id}})">
                                                            <i class="fa fa-remove mr-1"></i>Batalkan Penjualan
                                                        </button>
                                                     @endif

                                                 @endif

                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="card-header mt-4">
                                <div class="row" style="margin-bottom: 2rem">
                                    <div class="col-md-4">
                                        <div class="card shadow mb-4">
                                            <div class="card-body">
                                                <h5 class="card-title font-weight-bold">Total Tagihan</h5>
                                                <h5 class="card-subtitle mb-2 text-orange text-right font-weight-bold" id="saldo_debit">{{rupiah($data->asset_value)}}</h5>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="card shadow mb-4">
                                            <div class="card-body">
                                                <h5 class="card-title font-weight-bold">Total Pembayaran</h5>
                                                <h5 class="card-subtitle mb-2 text-orange text-right font-weight-bold" id="saldo_debit">{{rupiah($payDetail->sum('paid_nominal'))}}</h5>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="card shadow mb-4">
                                            <div class="card-body">
                                                <h5 class="card-title font-weight-bold">Total Kekurangan</h5>
                                                <h5 class="card-subtitle mb-2 text-orange text-right font-weight-bold" id="saldo_debit">{{rupiah($data->asset_value-$payDetail->sum('paid_nominal'))}}</h5>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="card-body">
                                <table class="table table-custom" id="table-data-1" width="100%" cellpadding="0">
                                    <thead>
                                    <tr>
                                        <th>Kode</th>
                                        <th>Waktu</th>
                                        <th>Metode Pembayaran</th>
                                        <th class="text-right">Jumlah Pembayaran</th>
                                        <th>Opsi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($payCount>0)
                                        @foreach($payDetail as $key =>$item)
                                            <tr>
                                                <td>{{is_null($item->second_code)?$item->code:$item->second_code}}</td>
                                                <td>{{\Carbon\Carbon::parse($item->paid_date)->isoFormat( 'D MMMM Y')}}
                                                    {{(is_null($item->timezone)?getTimeZoneName($data->_timezone):getTimeZoneName($item->timezone))}}
                                                </td>
                                                <td>
                                                    {{($item->getCoa->name=='Kas')?'Tunai':$item->getCoa->name}}
                                                </td>
                                                <td class="text-right">
                                                    {{rupiah($item->paid_nominal)}}
                                                </td>
                                                <td>
                                                    <a title="Hapus Pembayaran" onclick="hapusPembayaran({{$item->id}})" class="btn btn-delete-xs btn-xs" data-original-title="Hapus Pembayaran"><span class="fa fa-trash-alt"></span></a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                <small>Belum ada pembayaran</small>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
            @endif

                    @if($data->is_depreciation==1)
                        <div class="col-md-12">
                            <div class="card shadow mb-4">
                                <table class="table table-borderless" width="100%">
                                    <tr>
                                        <td class="text-center font-weight-bold" id="grafik-td" width="50%" style="border-bottom: 2px solid #FF8100;padding-top: 1.5rem;padding-bottom: 1.5rem">
                                            <a onclick="showTabDepreciaton(1)" id="grafik-name">Grafik Penyusutan</a>
                                        </td>
                                        <td class="text-center font-weight-bold" style="border-bottom: 1px solid #ebebeb" id="rincian-td">
                                            <a onclick="showTabDepreciaton(2)" style="color: #d3d3d3" id="rincian-name">Rincian Data</a>
                                        </td>

                                    </tr>
                                </table>
                                <div class="card-body" style="padding-top: 0.5rem">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="card mb-4" style="border: 1px solid #E5E5E5">
                                                <div class="card-body" style="padding: 0.5rem" >
                                                    <span class="card-subtitle font-weight-bold">Kelompok Harta</span>
                                                    <br>
                                                    <span class="card-subtitle" id="saldo_debit">
                                                        @if($data->is_depreciation==1)
                                                            @if(!is_null($data->getProperty))
                                                            {{$data->getProperty->getParent->name}}-{{$data->getProperty->name}}
                                                            @endif
                                                        @else
                                                        -
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="card mb-4" style="border: 1px solid #E5E5E5">
                                                <div class="card-body" style="padding: 0.5rem" >
                                                    <span class="card-subtitle font-weight-bold">Masa Manfaat</span>
                                                    <br>
                                                    <span class="card-subtitle" id="saldo_debit">
                                                        @if($data->is_depreciation==1)
                                                            {{$data->benefit_period}} Tahun
                                                        @else
                                                            -
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="card mb-4" style="border: 1px solid #E5E5E5">
                                                <div class="card-body" style="padding: 0.5rem" >
                                                    <span class="card-subtitle font-weight-bold">Penyusutan</span>
                                                    <br>
                                                    <span class="card-subtitle" id="saldo_debit">
                                                         @if($data->is_depreciation==1)
                                                            {{$data->value_depreciation*100}} %
                                                        @else
                                                            -
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="card mb-4" style="border: 1px solid #E5E5E5">
                                                <div class="card-body" style="padding: 0.5rem" >
                                                    <span class="card-subtitle font-weight-bold">Akun Penyusutan</span>
                                                    <br>
                                                    <span class="card-subtitle" id="saldo_debit">
                                                        @if($data->is_depreciation==1)
                                                            @if(is_null($data->getCoaDepreciation))
                                                                -
                                                            @else
                                                                {{$data->getCoaDepreciation->name}}

                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="card mb-4" style="border: 1px solid #E5E5E5">
                                                <div class="card-body" style="padding: 0.5rem" >
                                                    <span class="card-subtitle font-weight-bold">Akumulasi Penyusutan</span>
                                                    <br>
                                                    <span class="card-subtitle" id="saldo_debit">
                                                         @if($data->is_depreciation==1)
                                                            @if(is_null($data->getCoaAccumulation))
                                                                -
                                                            @else
                                                                {{$data->getCoaAccumulation->name}}

                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <table class="table table-borderless" width="100%">
                                                <tr>
                                                    <td  style="width: 25%">Total Penyusutan :
                                                    <span class="font-14 font-weight-bold ml-1">{{rupiah($detail->sum('value_depreciation'))}}</span>
                                                    </td>
                                                    <td  width="50%">Metode Penyusutan :
                                                    <span class="font-14 font-weight-bold ml-1">
                                                        @if($data->is_depreciation==1)
                                                            {{$data->getDepreciationMethod->name}}
                                                        @else
                                                            -
                                                        @endif

                                                    </span>
                                                    </td>
                                                    <td>
                                                        <select class="form-control" id="year" onchange="loadDepreciation()">
                                                            @foreach($year as $item)
                                                                <option value="{{$item->id}}">{{$item->text}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-12" id="graphic-depre">
                                            <div id="container-4" style="width: 100%;min-height: 400px"></div>
                                        </div>
                                    </div>
                                    <div id="rincian-table">
                                        <table class="table table-custom" width="100%" id="table-data-2" cellpadding="0">
                                            <thead>
                                            <tr>
                                                <th>Kode</th>
                                                <th>Waktu</th>
                                                <th class="text-right">Nilai Penyusutan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($detail as $key =>$d)
                                                <tr>
                                                    <td>{{$d->code}}</td>
                                                    <td>{{\Carbon\Carbon::parse($d->time)->isoFormat( 'D MMMM Y')}}
                                                        {{(is_null($d->timezone)?getTimeZoneName($data->_timezone):getTimeZoneName($d->timezone))}}
                                                    </td>
                                                    <td class="text-right">
                                                        {{rupiah($d->value_depreciation)}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif
        </div>
    </div>
@endsection

        @section('js')
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>

            <script>
                function hapusPembayaran(id) {
                    var data = new FormData();
                    data.append('id', id);

                    modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                        ajaxTransfer("{{route('merchant.asset.asset.delete-payment')}}", data, function (response){
                            var data = JSON.parse(response);
                            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                        });
                    })
                }

                function hapusData(id) {
                    var data = new FormData();
                    data.append('id', id);

                    modalConfirm("Konfirmasi", "<div class='alert alert-warning'>Menghapus data aset akan menghapus pencatatan jurnal.Apakah anda yakin menghapus data?</div>", function () {
                        ajaxTransfer("{{route('merchant.asset.asset.delete')}}", data, function (response){
                            var data = JSON.parse(response);
                            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                        });
                    })
                }

                function cancelSell(id) {
                    var data = new FormData();
                    data.append('id', id);

                    modalConfirm("Konfirmasi", "<div class='alert alert-warning'>Menghapus data penjualan aset akan menghapus pencatatan jurnal.Apakah anda yakin menghapus data?</div>", function () {
                        ajaxTransfer("{{route('merchant.asset.asset.cancel-sell')}}", data, function (response){
                            var data = JSON.parse(response);
                            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                        });
                    })
                }

                $( document ).ready(function() {
                    loadDepreciation()
                    $("#year").select2();
                    showTabDepreciaton(1)
                    $("#table-data-1").DataTable({
                        iDisplayLength: 5,
                        lengthMenu: [[5, 10, 20,50,100], [5, 10, 20,50,100]],
                        language: {
                            processing: '<div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div> ',
                            paginate: {
                                "previous": '<i class="ti-angle-left"></i>',
                                "next": '<i class="ti-angle-right"></i>'
                            },
                            info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                            lengthMenu: "Tampilkan _MENU_ entri",
                            infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                        },
                    });

                    $("#table-data-2").DataTable({
                        iDisplayLength: 10,
                        language: {
                            processing: '<div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div> ',
                            paginate: {
                                "previous": '<i class="ti-angle-left"></i>',
                                "next": '<i class="ti-angle-right"></i>'
                            },
                            info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                            lengthMenu: "Tampilkan _MENU_ entri",
                            infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                        },
                        "order": [],
                        "columnDefs": [ {
                            "targets"  : 'no-sort',
                            "orderable": false,
                        }]
                    });

                });

                function loadDepreciation()
                {
                    var year = $("#year").val()
                    let data = new FormData();
                    ajaxTransfer("{{route('merchant.asset.asset.json-depreciation')}}?asset_id={{$data->id}}",data,function (result){
                        let data = JSON.parse(result)
                        var assetValue = {{$data->asset_value}};
                        let tempCurrentValue=0;
                        let valueCurrentValue=[],
                            month=[],
                        valueDepreciaton=[];
                        for (let i = 0; i < data.length; i++) {
                            tempCurrentValue+=parseFloat(data[i]['value_depreciation']);
                            if(data[i]['year_name']==year)
                            {
                                let tempValue=parseFloat(assetValue)-tempCurrentValue;
                                valueCurrentValue.push(tempValue);
                                valueDepreciaton.push(parseFloat(data[i]['value_depreciation']))

                                month.push(data[i]['month_name'])
                            }
                        }
                        Highcharts.chart('container-4', {
                            chart: {
                                type: 'spline'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                categories:month,
                                accessibility: {
                                    description: ''
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Harga Jual'
                                },
                                labels: {
                                    formatter: function () {
                                        return currencyFormat(this.value) + '';
                                    }
                                }
                            },
                            tooltip: {
                                shared: false,
                                backgroundColor: null,
                                borderWidth: 1,
                                useHTML:true,
                                formatter: function() {
                                    var series = this.series;
                                    var index = this.series.data.indexOf(this.point);
                                    var s = '<div style="background: white">Harga Jual<br><br>';
                                    s += '<span style="color:#000"><b>' + currencyFormat(this.y) + '</b><br><br>';
                                    $.each(series.options.composition, function(name, value) {
                                        s += '<button class="btn btn-number-down-xs btn-xs" style="color: #E04040;font-size: 10px">-'+ currencyFormat(value[index]) + ' <i class="fa fa-arrow-right ml-1" style="display: inline-block; -webkit-transform: rotate(45deg);transform: rotate(45deg);"></i> </button>';
                                    });
                                    s+='</div>';
                                    return s;

                                }
                            },
                            plotOptions: {
                                spline: {
                                    marker: {
                                        radius: 4,
                                        lineColor: '#E5E5E5',
                                        lineWidth: 1
                                    }
                                }
                            },
                            series: [{
                                name: 'Harga Jual',
                                marker: {
                                    symbol: 'circle'
                                },
                                color: "#E5E5E5",
                                data: valueCurrentValue,
                                composition: {
                                    "data1": valueDepreciaton
                                },
                            }]
                        });


                    })

                }
                function showTabDepreciaton(value)
                {
                    if(value==1)
                    {
                        $("#rincian-table").hide();
                        $("#graphic-depre").show();
                        $("#grafik-td").css({ "border-bottom": "2px solid #FF8100","padding-top": "1.5rem","padding-bottom": "1.5rem" });
                        $("#rincian-td").css({ "border-bottom": "","padding-top": "","padding-bottom": "" });
                        $("#rincian-td").css({ "border-bottom": "1px solid #ebebeb" });
                        $("#grafik-name").css({"color": ""})
                        $("#rincian-name").css({"color": "#d3d3d3"})

                    }else{
                        $("#rincian-table").show();
                        $("#graphic-depre").hide();
                        $("#rincian-td").css({ "border-bottom": "2px solid #FF8100","padding-top": "1.5rem","padding-bottom": "1.5rem" });
                        $("#grafik-td").css({ "border-bottom": "","padding-top": "","padding-bottom": "" });
                        $("#grafik-td").css({ "border-bottom": "1px solid #ebebeb" });
                        $("#rincian-name").css({"color": ""})
                        $("#grafik-name").css({"color": "#d3d3d3"})


                    }

                }
            </script>
@endsection

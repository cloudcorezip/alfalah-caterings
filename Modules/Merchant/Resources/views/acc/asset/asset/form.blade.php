@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Aset Usaha</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.asset.asset.index')}}">Data Aset</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}
                            </a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa mengisi data di bawah ini untuk menambah aset. Pastikan semua kolom sudah terisi dengan benar</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-title" style="border-bottom: 1px solid rgba(0, 0, 0, 0.1)">
                <div class="card-body" style="padding-bottom: 0">
                    <h4>Detail Aset</h4>

                </div>
            </div>
            <div class="card-body">
                <div id="result-form-konten"></div>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                            <div class="row">
                                @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan pencatatan pembelian aset untuk mengetahui secara spesifik pergerakan aset disetiap cabangmu',
'selector'=>'branch',
'with_onchange'=>true,
])
                                <div class="col-md-6">
                                    @include('merchant::component.file-upload',['title'=>'Foto Aset','fieldName'=>'file_asset','allow_all'=>true])
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Kode Aset</label>
                                        <input type="text" class="form-control" name="code" value="{{is_null($data->second_code)?$data->code:$data->second_code}}" placeholder="Kode Aset">

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Nama Aset<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="asset_name" value="{{$data->asset_name}}" required placeholder="Nama Aset">

                                    </div>
                                    <div class="form-group">
                                    <label for="exampleInputPassword1" class="font-weight-bold">Tipe Aset<span class="text-danger">*</span></label>
                                    <select class="form form-control" name="is_intangible_asset" id="is_intangible" required>
                                        <option value="-1">Pilih Tipe Aset</option>
                                        <option value="0" @if($data->is_intangible_asset==0) selected @endif>Berwujud</option>
                                        <option value="1"@if($data->is_intangible_asset==1) selected @endif>Tidak Berwujud</option>
                                    </select>

                                </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Kategori Aset<span class="text-danger">*</span></label>
                                        <select name="acc_asset_category_id" class="form-control" id="category" required>
                                            @foreach($category as $key =>$item)
                                                <option value="{{$item->id}}" {{($item->id==$data->acc_asset_category_id)?'selected':''}}>{{$item->nama_kategori}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Akun Aset<span class="text-danger">*</span></label>
                                        @if(is_null($data->id))
                                            <select name="acc_coa_asset_id" class="form-control" id="coa_asset">
                                                <option value="-1"> Pilih Akun Aset</option>
                                            </select>
                                        @else
                                            <select name="acc_coa_asset_id" class="form-control" id="coa_asset">
                                                @foreach($assetAccount as $key =>$item)
                                                    <option value="{{$item->id}}" {{($item->id==$data->acc_coa_asset_id)?'selected':''}}>{{$item->text}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Status Pembelian<span class="text-danger">*</span></label>
                                        <select class="form form-control" name="is_paid" id="is_paid_pay">
                                            <option value="-1">Pilih Status Pembelian</option>
                                            <option value="1" @if($data->is_paid==1) selected @endif>Lunas</option>
                                            <option value="0"@if($data->is_paid==0) selected @endif>Belum Lunas</option>
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-form-label font-weight-bold">Metode Pembayaran</label>
                                        @if(is_null($data->id))
                                            <select id="trans" class="form-control form-control-sm paid_payment" name="payment_coa_detail_id" required>
                                                <option value="-1">Pilih Metode Pembayaran</option>
                                            </select>
                                        @else
                                            <select id="trans" class="form-control form-control-sm paid_payment" name="payment_coa_detail_id" required>
                                                <option value="-1">Pilih Metode Pembayaran</option>
                                                @foreach($payment as $item)
                                                    <optgroup label="{{$item['name']}}">
                                                        @if(count($item['data'])>0)
                                                            @if($item['name']=='Pembayaran Digital')
                                                                @foreach($item['data'] as $d)
                                                                    <optgroup label="--{{$d['name']}}">
                                                                        @if(!empty($d['data']))
                                                                            @foreach($d['data'] as $c)
                                                                                <option value="{{$c['acc_coa_id']}}" @if($c['acc_coa_id']==$data->payment_coa_detail_id) selected @endif>{{$c['name']}} </option>
                                                                            @endforeach
                                                                        @endif
                                                                    </optgroup>
                                                                @endforeach
                                                            @else
                                                                @foreach($item['data'] as $key =>$other)
                                                                    <option value="{{$other->acc_coa_id}}" @if($other->acc_coa_id==$data->payment_coa_detail_id) selected @endif>{{$other->name}}</option>

                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </optgroup>
                                                @endforeach
                                            </select>

                                        @endif

                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Nilai Aset<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control amount_currency" name="asset_value" id="asset_value" step="0.001" min="0"  value="{{is_null($data->asset_value)?0:rupiah($data->asset_value)}}"  required>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label font-weight-bold">Biaya Administrasi Bank</label>
                                        <input type="text" class="form-control amount_currency paid_payment" name="admin_fee" id="admin_fee" step="0.001" min="0"  value="{{is_null($data->admin_fee)?0:rupiah($data->admin_fee)}}"  required>

                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Bukti Transaksi</label>
                                        <input type="file" class="form-control" name="trans_proof">

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Waktu Pencatatan</label>
                                        <input type="text" class="form-control trans_time" name="record_time" value="{{$data->record_time}}" placeholder="Waktu Pencatatan" required>

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Supplier/Vendor<span class="text-danger">*</span></label>
                                        <select name="sc_supplier_id" class="form-control" id="supplier">
                                            <option value="-1">Tidak memakai supplier/vendor</option>
                                            @foreach($supplier as $key =>$item)
                                                <option value="{{$item->id}}" {{($item->id==$data->sc_supplier_id)?'selected':''}}>{{$item->kode}} {{$item->nama_supplier}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Deskripsi</label>
                                        <textarea class="form-control" name="desc" placeholder="Tambahkan deskripsi">{{$data->desc}}</textarea>

                                    </div>
                                </div>
                                <div class="col-md-12 mt-4">
                                    <div class="form-group row">
                                        <div class="col-md-11"><h5>Aset Terdepresiasi</h5>
                                            <small>Asetmu akan mengalami penyusutan sesuai dengan masa manfaat dan nilai penyusutan yang kamu masukkan</small>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="custom-control custom-switch">
                                                <input
                                                    type="checkbox"
                                                    class="custom-control-input check-bank"
                                                    id="is_depr"
                                                    name="is_depr"
                                                    value="{{($data->is_depreciation==1)?1:0}}"
                                                    style="transform:scale(2)"
                                                    {{($data->is_depreciation==1)?'checked':''}}
                                                >
                                                <label class="custom-control-label" for="is_depr"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row" id="tangible">
                                <div class="col-md-6">
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Kelompok Harta</label>
                                        <select name="acc_md_property_group_id" class="form-control" id="property" onchange="setInformasiKelompokHarta()">
                                            @foreach($propertyGroup as $key =>$item)
                                                <option disabled><b>{{$item->name}}</b>
                                                </option>
                                                @foreach($item->getChild as $c)
                                                    <option value="{{$c->id}}"
                                                        {{($c->id==$data->acc_md_property_group_id)?'selected':''}}>{{$c->name}}
                                                    </option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Akun Penyusutan<span class="text-danger">*</span></label>
                                        @if(is_null($data->id))
                                            <select name="account_depreciation_id" class="form-control acc_depreciation">
                                                <option value="-1">Pilih Akun Penyusutan</option>
                                            </select>
                                        @else
                                            <select name="account_depreciation_id" class="form-control acc_depreciation">
                                                @foreach($depreciationAccount as $key =>$item)
                                                    <option value="{{$item->id}}" {{($item->id==$data->account_depreciation_id)?'selected':''}}>{{$item->text}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Akumulasi Penyusutan <span class="text-danger">*</span></label>
                                        @if(is_null($data->id))
                                            <select name="account_accumulation_id" class="form-control acc_accumulation">
                                                <option value="-1">Pilih Akumulasi Penyusutan</option>
                                            </select>
                                        @else
                                            <select name="account_accumulation_id" class="form-control acc_accumulation">
                                                @foreach($assetAccount as $key =>$item)
                                                    <option value="{{$item->id}}" {{($item->id==$data->account_accumulation_id)?'selected':''}}>{{$item->text}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Metode Penyusutan</label>
                                        <select name="acc_md_depreciation_method_id" class="form-control method" onchange="setBenefitPeriod()">
                                            @if(!is_null($data->id) && $data->is_depreciation==1)
                                                <option value="{{$data->acc_md_depreciation_method_id}}" selected>{{$data->getDepreciationMethod->name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Masa Manfaat</label>
                                        <input type="text" class="form-control"  value="{{is_null($data->benefit_period)?0:$data->benefit_period}}" id="benefit_period"  required disabled>

                                    </div>
                                    <div class="form-group penyusutan" >
                                        <label for="exampleInputPassword1" class="font-weight-bold">Nilai Penyusutan</label>
                                        <input type="text" class="form-control"  value="{{is_null($data->value_depreciation)?0:$data->value_depreciation}}" id="value_depreciation"  required disabled>

                                    </div>
                                </div>
                            </div>
                            <div class="row" id="intangible">
                                <div class="col-md-6">
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Metode Penyusutan</label>
                                        <select name="acc_md_depreciation_method_id_intangible" class="form-control method" onchange="setBenefitPeriod()">
                                            @if(!is_null($data->id) && $data->is_depreciation==1)
                                                <option value="{{$data->acc_md_depreciation_method_id}}" selected>{{$data->getDepreciationMethod->name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Masa Manfaat</label>
                                        <input type="number" class="form-control" name="benefit_period_intangible"  value="{{is_null($data->benefit_period)?0:$data->benefit_period}}"  required onkeypress="return isNumber(event)">

                                    </div>
                                    <div class="form-group penyusutan" >
                                        <label for="exampleInputPassword1" class="font-weight-bold">Nilai Penyusutan (%) / Tahun</label>
                                        <input type="number" class="form-control" name="value_depreciation_intangible"  value="{{is_null($data->value_depreciation*100)?0:$data->value_depreciation*100}}"  required onkeypress="return isNumber(event)">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Akun Penyusutan<span class="text-danger">*</span></label>
                                        @if(is_null($data->id))
                                            <select name="account_depreciation_id_intangible" class="form-control acc_depreciation">
                                                <option value="-1">Pilih Akun Penyusutan</option>
                                            </select>
                                        @else
                                            <select name="account_depreciation_id_intangible" class="form-control acc_depreciation">
                                                @foreach($depreciationAccount as $key =>$item)
                                                    <option value="{{$item->id}}" {{($item->id==$data->account_depreciation_id)?'selected':''}}>{{$item->text}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                    <div class="form-group penyusutan">
                                        <label for="exampleInputPassword1" class="font-weight-bold">Akumulasi Penyusutan <span class="text-danger">*</span></label>
                                        @if(is_null($data->id))
                                            <select name="account_accumulation_id_intangible" class="form-control acc_accumulation">
                                                <option value="-1">Pilih Akumulasi Penyusutan</option>
                                            </select>
                                        @else
                                            <select name="account_accumulation_id_intangible" class="form-control acc_accumulation">
                                                @foreach($assetAccount as $key =>$item)
                                                    <option value="{{$item->id}}" {{($item->id==$data->account_accumulation_id)?'selected':''}}>{{$item->text}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-12 text-right">
                                    <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                                </div>
                            </div>
                            <input type="hidden"  name="is_depreciation" value="{{($data->is_depreciation==1)?1:0}}" id="is_depreciation" >
                            <input type="hidden"  name="is_paid" value="{{($data->is_paid==1)?1:0}}" id="is_paid" >
                            <input type="hidden"  name="benefit_period" value="0" id="benefit" >
                            <input type="hidden"  name="value_depreciation" value="0" id="value" >
                            <input type='hidden' id="id" name='id' value='{{$data->id }}'>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.amount_currency').mask("#.##0,00", {reverse: true});
            $('#supplier').select2();
            $('#property').select2();
            $('#coa_asset').select2();
            $('#category').select2();
            $('.acc_depreciation').select2();
            $('.acc_accumulation,.method').select2();
            $("#trans").select2()
            $("#is_intangible").select2()
            $("#is_paid_pay").select2()
            @if(!is_null($data->id))
                @if($data->is_depreciation==1)
            $(".penyusutan").attr("hidden",false);

            @else
            $(".penyusutan").attr("hidden",true);

            @endif

            @else
            $(".penyusutan").attr("hidden",true);

            @endif

            @if($data->is_intangible_asset==1)
                $("#intangible").show();
                $("#tangible").hide();
            @else
            $("#intangible").hide();
            $("#tangible").show();
            @endif

            @if($data->is_paid==1)
                $(".paid_payment").removeAttr('disabled');
            @else
               $(".paid_payment").attr('disabled',true)
            @endif
            setMethodDepreciation(3)
            setBenefitPeriod()
            if(!$("#id").val()){
                onchangeBranch()
            }
        })

        @if(!is_null($data->id))

            saveEditData()
        @else
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.asset.asset.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
        @endif

        function saveEditData()
        {
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');

                modalConfirm("Konfirmasi", "<div class='alert alert-warning'>Perhatian! Perubahan pada data tersebut akan menyebabkan adanya penghitungan ulang pada data penyusutan.Apakah Anda yakin akan melanjutkan pemrosesan data?</div>", function () {
                    ajaxTransfer("{{route('merchant.asset.asset.save')}}", data, function (response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                    });
                })
            })

        }





        function setInformasiKelompokHarta() {
            var id=$('#property').val();
            $('#inf').attr('data',"id="+id+"");
            setMethodDepreciation(id)
            setBenefitPeriod()
        }

        $("#is_depr").change(function() {
            if(this.checked) {
                $("#is_depreciation").val(1)
                $(".penyusutan").removeAttr("hidden");

            }else{
                $("#is_depreciation").val(0)
                $(".penyusutan").attr("hidden",true);

            }
        });
        $("#is_paid_pay").change(function() {
            if($("#is_paid_pay").val()==1) {
                $("#is_paid").val(1)
                $(".paid_payment").removeAttr('disabled')
            }else{
                $("#is_paid").val(0)
                $(".paid_payment").attr('disabled',true)
                $("#trans").select2("val", "-1");

            }
        });

        $("#is_intangible").change(function() {
            if($("#is_intangible").val()==1) {
                $("#intangible").show();
                $("#tangible").hide()
                setMethodDepreciation()
            }else{
                $("#intangible").hide();
                $("#tangible").show()
            }
        });
        function setMethodDepreciation(id=null)
        {
            let intangible=$("#is_intangible").val()
            if(intangible==1)
            {
                var data =[
                    {
                        id: 1,
                        text: 'Garis Lurus (Straight Line)'
                    },
                    {
                        id: 2,
                        text: 'Saldo Menurun (Double Declining Balance)'
                    },
                ];
                $(".method").select2({
                    data: data
                })
            }else{
                var data =(id>6)? [
                    {
                        id: 1,
                        text: 'Garis Lurus (Straight Line)'
                    },

                ] :[
                    {
                        id: 1,
                        text: 'Garis Lurus (Straight Line)'
                    },
                    {
                        id: 2,
                        text: 'Saldo Menurun (Double Declining Balance)'
                    },
                ];

                $(".method").select2({
                    data: data
                })
            }



        }

        function setBenefitPeriod()
        {
            var id=$('#property').val();
            var methodId=$(".method").val();
            var intangible=$("#is_intangible").val();
            if(intangible==0)
            {
                if(methodId==null)
                {
                    methodId=1;
                }
                $.ajax({
                    type: "GET",
                    url: "{{route('merchant.asset.asset.benefit')}}?id="+id+"&method_id="+methodId,
                    dataType: 'json',
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    success: function(response){
                        $("#value_depreciation").val(response[0].depreciation_value*100+ "%")
                        $("#benefit_period").val(response[0].benefit_period +' Tahun')

                        $("#value").val(response[0].depreciation_value)
                        $("#benefit").val(response[0].benefit_period)

                    }
                });
            }
        }

        function onchangeBranch()
        {
            let branch = $("#branch").val()
            $("#coa_asset").empty()
            $(".acc_depreciation").empty()
            $(".acc_accumulation").empty()
            $("#trans").empty()
            $.ajax({
                type: "GET",
                url: "{{route('merchant.asset.asset.asset-list')}}?md_merchant_id="+branch,
                dataType: 'json',
                success: function(result){

                    $("#coa_asset").select2({
                        data:result
                    });
                    $(".acc_accumulation").select2({
                        data:result
                    });

                }});

            $.ajax({
                type: "GET",
                url: "{{route('merchant.asset.asset.depreciation-list')}}?md_merchant_id="+branch,
                dataType: 'json',
                success: function(result){

                    $(".acc_depreciation").select2({
                        data:result
                    });
                }});
            $.ajax({
                type: "GET",
                url: "{{route('merchant.toko.acc.jurnal.administrative-expense.payment-list')}}?md_merchant_id="+branch,
                dataType: 'json',
                success: function(result){

                    $("#trans").select2({
                        data:result
                    });

                }});
        }
        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endsection

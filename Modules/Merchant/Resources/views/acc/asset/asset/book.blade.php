<style>
    .kelompok-harta-list {
        display: inline-block;
        width: 100%;
        margin-bottom: 10px
    }

    .kelompok-harta-title {
        background-color: #ff7b15;
        color: #fff;
        display: block;
        width: 100%;
        padding: 5px 10px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        text-transform: uppercase;
        font-weight: 500
    }

    .kelompok-harta-deskripsi {
        display: inline-block;
        border: 1px solid #cacaca;
        padding: 5px 10px;
        border-top: 0 none;
        width: 100%
    }

    .kelompok-harta-deskripsi ol {
        margin: 0;
        padding: 0 0 0 15px
    }
    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    @if($request['id']==3)
    <div class="modal-popup-content" id="modal-popup-content-1">
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Semua Jenis Usaha </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Mebel dan peralatan dari kayu atau rotan termasuk meja, bangku, kursi, almari dan yang sejenisnya yang bukan bagian dari bangunan</li>
                    <li>Mesin kantor seperti mesin tik, mesin hitung, duplikator, mesin fotokopi, mesin akunting/pembukuan, komputer, printer, scanner dan sejenisnya</li>
                    <li>Perlengkapan lainnya seperti amplifier, tape/cassette, video recorder, televisi dan sejenisnya</li>
                    <li>Sepeda motor, sepeda dan becak</li>
                    <li>Alat perlengkapan khusus (tools) bagi industri/jasa yang bersangkutan</li>
                    <li>Alat dapur untuk memasak, makanan dan minuman</li>
                    <li>Dies, jigs, dan mould.</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Pertanian, perkebunan, kehutanan, dan perikanan </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Alat yang digerakkan bukan dengan mesin </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Industri makanan dan minuman </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Mesin ringan yang dapat dipindah-pindahkan seperti, huller, pemecah kulit, penyosoh, pengering, pallet, dan sejenisnya </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Perhubungan pergudangan dan komunikasi </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Mobil taksi, bus dan truk yang digunakan sebagai angkutan umum. </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Industri semi konduktor </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Falsh memory tester, writer machine, biporar test system, elimination (PE8-1), pose checker. </div>
        </div>
    </div>
    @endif

    @if($request['id']==4)

        <div class="modal-popup-content" id="modal-popup-content-1">
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Semua Jenis Usaha </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Mebel dan peralatan dari logam temasuk meja, bangku, kursi, almari dan sejenisnya yang bukan merupakan bagian dari bangunan. Alat pengatur udara seperti AC, kipas angin dan sejenisnya.</li>
                    <li>Mobil, bus, truk speed boat dan sejenisnya.</li>
                    <li>Container dan sejenisnya.</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Pertanian, perkebunan, kehutanan, perikanan </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Mesin pertanian / perkebunan seperti traktor dan mesin bajak, penggaruk, penanaman, penebar benih dan sejenisnya.</li>
                    <li>Mesin yang mengolah atau menghasilkan atau memproduksi bahan atau barang pertanian, kehutanan, perkebunan, dan perikanan.</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Industri makanan dan minuman </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Mesin yang mengolah produk asal binatang, unggas dan perikanan, misalnya pabrik susu, pengalengan ikan</li>
                    <li>Mesin yang mengolah produk nabati, misalnya mesin minyak kelapa, magarine, penggilingan kopi, kembang gula, mesin pengolah biji-bijian seperti penggilingan beras, gandum, tapioka.</li>
                    <li>Mesin yang menghasilkan / memproduksi minuman dan bahan-bahan minuman segala jenis.</li>
                    <li>Mesin yang menghasilkan / memproduksi bahan-bahan makanan dan makanan segala jenis.</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Industri mesin </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Mesin yang menghasilkan / memproduksi mesin ringan (misalnya mesin jahit, pompa air). </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Perkayuan </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Mesin dan peralatan penebangan kayu. </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Konstruksi </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Peralatan yang dipergunakan seperti truk berat, dump truck, crane buldozer dan sejenisnya. </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Perhubungan, pergudangan dan komunikasi </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Truck kerja untuk pengangkutan dan bongkar muat, truck peron, truck ngangkang, dan sejenisnya;</li>
                    <li>Kapal penumpang, kapal barang, kapal khusus dibuat untuk pengangkutan barang tertentu (misalnya gandum, batu - batuan, biji tambang dan sebagainya) termasuk kapal pendingin, kapal tangki, kapal penangkap ikan dan sejenisnya, yang mempunyai
                        berat sampai dengan 100 DWT;</li>
                    <li>Kapal yang dibuat khusus untuk menghela atau mendorong kapal-kapal suar, kapal pemadam kebakaran, kapal keruk, keran terapung dan sejenisnya yang mempunyai berat sampai dengan 100 DWT;</li>
                    <li>Perahu layar pakai atau tanpa motor yang mempunyai berat sampai dengan 250 DWT;</li>
                    <li>Kapal balon.</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Telekomunikasi </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br>
                <ol>
                    <li>Perangkat pesawat telepon;</li>
                    <li>Pesawat telegraf termasuk pesawat pengiriman dan penerimaan radio telegraf dan radio telepon.</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Industri semi konduktor </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Auto frame loader, automatic logic handler, baking oven, ball shear tester, bipolar test handler (automatic), cleaning machine, coating machine, curing oven, cutting press, dambar cut machine, dicer, die bonder, die shear
                test, dynamic burn-in system oven, dynamic test handler, eliminator (PGE-01), full automatic handler, full automatic mark, hand maker, individual mark, inserter remover machine, laser marker (FUM A-01), logic test system, marker (mark), memory
                test system, molding, mounter, MPS automatic, MPS manual, O/S tester manual, pass oven, pose checker, re-form machine, SMD stocker, taping machine, tiebar cut press, trimming/forming machine, wire bonder, wire pull tester. </div>
        </div>
    </div>
        @endif





        @if($request['id']==5)

    <div class="modal-popup-content" id="modal-popup-content-1">
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Pertambangan selain minyak dan gas </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Mesin-mesin yang dipakai dalam bidang pertambangan, termasuk mesin - mesin yang mengolah produk pelikan. </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Permintalan, pertenunan dan pencelupan </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Mesin yang mengolah / menghasilkan produk-produk tekstil (misalnya kain katun, sutra, serat-serat buatan, wol dan bulu hewan lainnya, lena rami, permadani, kain-kain bulu, tule).</li>
                    <li>Mesin untuk yang preparation, bleaching, dyeing, printing, finishing, texturing, packaging dan sejenisnya.</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Perkayuan </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Mesin yang mengolah / menghasilkan produk - produk kayu, barang-barang dari jerami, rumput dan bahan anyaman lainnya</li>
                    <li>Mesin dan peralatan penggergajian kayu</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Industri kimia </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Mesin peralatan yang mengolah / menghasilkan produk industri kimia dan industri yang ada hubungannya dengan industri kimia (misalnya bahan kimia anorganis, persenyawaan organis dan anorganis dan logam mulia, elemen radio aktif, isotop,
                        bahan kimia organis, produk farmasi, pupuk, obat celup, obat pewarna, cat, pernis, minyak eteris dan resinoida-resinonida wangi-wangian, obat kecantikan dan obat rias, sabun, detergent dan bahan organis pembersih lainnya, zat albumina,
                        perekat, bahan peledak, produk pirotehnik, korek api, alloy piroforis, barang fotografi dan sinematografi.</li>
                    <li>Mesin yang mengolah / menghasilkan produk industri lainnya (misalnya damar tiruan, bahan plastik, ester dan eter dari selulosa, karet sintetis, karet tiruan, kulit samak, jangat dan kulit mentah).</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Industri mesin </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Mesin yang menghasilkan/memproduksi mesin menengah dan berat (misalnya mesin mobil, mesin kapal). </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Perhubungan, dan komunikasi </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Kapal penumpang, kapal barang, kapal khusus dibuat untuk pengangkutan barang-barang tertentu (misalnya gandum, batu-batuan, biji tambang dan sejenisnya) termasuk kapal pendingin dan kapal tangki, kapal penangkapan ikan dan sejenisnya,
                        yang mempunyai berat di atas 100 DWT sampai dengan 1.000 DWT.</li>
                    <li>Kapal dibuat khusus untuk mengela atau mendorong kapal, kapal suar, kapal pemadam kebakaran, kapal keruk, keran terapung dan sejenisnya, yang mempunyai berat di atas 100 DWT sampai dengan 1.000 DWT.</li>
                    <li>Dok terapung.</li>
                    <li>Perahu layar pakai atau tanpa motor yang mempunyai berat di atas 250 DWT.</li>
                    <li>Pesawat terbang dan helikopter-helikopter segala jenis.</li>
                </ol>
            </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Telekomunikasi </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Perangkat radio navigasi, radar dan kendali jarak jauh. </div>
        </div>
    </div>

        @endif




        @if($request['id']==6)

    <div class="modal-popup-content" id="modal-popup-content-1">
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Konstruksi </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b><br> Mesin berat untuk konstruksi </div>
        </div>
        <div class="kelompok-harta-list">
            <div class="kelompok-harta-title"> Jenis Usaha : Perhubungan dan komunikasi </div>
            <div class="kelompok-harta-deskripsi"> <b>Jenis Harta :</b>
                <ol>
                    <li>Lokomotif uap dan tender atas rel</li>
                    <li>Lokomotif uap atas rel, dijalankan dengan baterai atau dengan tenaga listrik dari sumber luar</li>
                    <li>Lokomotif atas rel lainnya</li>
                    <li>Kereta, gerbong penumpang dan barang, termasuk kontainer khusus dibuat dan diperlengkapi untuk ditarik dengan satu alat atau beberapa alat pengangkutan.</li>
                    <li>Kapal penumpang, kapal barang, kapal khusus dibuat untuk pengangkutan barang-barang tertentu (misalnya gandum, batu-batuan, biji tambang dan sejenisnya) termasuk kapal pendingin dan kapal tangki, kapal penangkap ikan dan sejenisnya, yang
                        mempunyai berat di atas 1.000 DWT.</li>
                    <li>Kapal dibuat khusus untuk menghela atau mendorong kapal, kapal suar, kapal pemadam kebakaran, kapal keruk, keran-keran terapung dan sebagainya, yang mempunyai berat di atas 1.000 DWT.</li>
                    <li>Dok-dok terapung.</li>
                </ol>
            </div>
        </div>
    </div>
            @endif

        @if($request['id']>6)
            <div class="alert alert-warning">Mohon maaf tidak ada keterangan tersedia</div>
        @endif
</form>

<script>
</script>

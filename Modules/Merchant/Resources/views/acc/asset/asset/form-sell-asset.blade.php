
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Kode Penjualan</label>
                <input type="text" class="form-control form-control-sm" name="sell_second_code" placeholder="Kode Penjualan">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Waktu Penjualan</label>
                <input type="text" class="form-control form-control-sm trans_time" name="sell_date" placeholder="Pilih Tanggal" required>
            </div>
        </div>
        <div class="col-md-6 input1">
            <div class="form-group input1" >
                <label id="labelTo" class="font-weight-bold">Metode Penjualan</label>
                <select id="trans" class="form-control form-control-sm" name="sell_coa_detail_id" required>
                    <option value="-1">Pilih Metode Pembayaran</option>
                    @foreach($payment as $item)
                        <optgroup label="{{$item['name']}}">
                            @if(count($item['data'])>0)
                                @if($item['name']=='Pembayaran Digital')
                                    @foreach($item['data'] as $d)
                                        <optgroup label="--{{$d['name']}}">
                                            @if(!empty($d['data']))
                                                @foreach($d['data'] as $c)
                                                    <option value="{{$c['acc_coa_id']}}" @if($c['acc_coa_id']==$asset->sell_coa_detail_id) selected @endif>{{$c['name']}} </option>
                                                @endforeach
                                            @endif
                                        </optgroup>
                                    @endforeach
                                @else
                                    @foreach($item['data'] as $key =>$other)
                                        <option value="{{$other->acc_coa_id}}" @if($other->acc_coa_id==$asset->sell_coa_detail_id) selected @endif>{{$other->name}}</option>

                                    @endforeach
                                @endif
                            @endif
                        </optgroup>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Jumlah Penjualan</label>
                <input type="text" name="sell_price" class="form-control amount_currency" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="{{rupiah($asset->sell_price)}}" required>

            </div>
        </div>

        <div class="col-md-6 admin-fee">
            <div class="form-group admin-fee">
                <label for="exampleInputPassword1" class="admin-fee font-weight-bold">Pajak PPN</label>
                <input type="text" name="sell_ppn_amount" class="form-control amount_currency" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="{{rupiah($asset->sell_ppn_amount)}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Bukti Transaksi</label>
                <input type="file" class="form-control form-control-sm"  name="sell_trans_proof">
                <small class="d-block mt-2" style="color:#EE6767;">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</small>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='md_merchant_id' value='{{$asset->md_merchant_id }}'>
    <input type='hidden' name='acc_asset_depreciation_id' value='{{$asset->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        dateTimePicker(".trans_time");
        $("#trans").select2();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.asset.asset.save-sell')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })

    })


</script>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah penjualan aset, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });
</script>

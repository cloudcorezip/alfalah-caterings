@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.acc.journal.cash-bank.index')}}">Kas & Bank</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Buat transfer kas dalam kas kecil, kas besar, dan juga kas bank</span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal px-4' backdrop="">
                    <div class="row">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>true,

])

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode Transaksi</label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{is_null($data->second_code)?$data->ref_code:$data->second_code}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Transaksi</label>
                                <input type="name" class="form-control form-control-sm" name="trans_name" placeholder="Nama Transaksi" value="{{$data->trans_name}}" required>
                            </div>
                        </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="font-weight-bold">{{($type==1)?'Asal Bank' :'Asal Kas'}}</label>
                                    {{-- @dd(collect(json_decode($bankOption))) --}}
                                    @if(!is_null($data->id))
                                        <select class="form-control" id="from" name="from_acc_coa_detail_id" required>

                                        @if($type==1)
                                            @foreach(collect(json_decode($bankOption))->where('md_merchant_id',$data->md_merchant_id) as $k)
                                                <option value="{{$k->id}}" @if($firstOption->acc_coa_detail_id==$k->id) selected @endif>{{$k->text}}</option>

                                            @endforeach
                                        @else
                                            @foreach(collect(json_decode($cashOption))->where('md_merchant_id',$data->md_merchant_id) as $k)
                                                <option value="{{$k->id}}" @if($firstOption->acc_coa_detail_id==$k->id) selected @endif>{{$k->text}}</option>

                                            @endforeach
                                        @endif
                                        </select>
                                    @else
                                        <select class="form-control" id="from" name="from_acc_coa_detail_id" required>

                                        </select>
                                    @endif

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="font-weight-bold">{{($type==1)?'Tujuan Bank' :'Tujuan Kas'}}</label>
                                    @if(!is_null($data->id))

                                        <select class="form-control" id="to" name="to_acc_coa_detail_id" required>
                                        @if($type==1)
                                            @foreach(collect(json_decode($bankOption))->where('md_merchant_id',$data->md_merchant_id) as $k)
                                                <option value="{{$k->id}}" @if($lastOption->acc_coa_detail_id==$k->id) selected @endif>{{$k->text}}</option>

                                            @endforeach
                                        @else
                                            @foreach(collect(json_decode($cashOption))->where('md_merchant_id',$data->md_merchant_id) as $k)
                                                <option value="{{$k->id}}" @if($lastOption->acc_coa_detail_id==$k->id) selected @endif>{{$k->text}}</option>

                                            @endforeach
                                        @endif

                                        </select>
                                    @else
                                        <select class="form-control" id="to" name="to_acc_coa_detail_id" required>

                                        </select>
                                    @endif
                                </div>
                            </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Jumlah</label>
                                <input type="text" class="form-control form-control-sm amount_currency" min="0" step="0.01" id="trans_amount" name="trans_amount" placeholder="Jumlah" value="{{currency($data->trans_amount)}}" required>
                            </div>
                        </div>
                        @if($type!=2)
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Biaya Administrasi</label>
                                <input type="text" class="form-control form-control-sm amount_currency" min="0" step="0.01" id="admin_fee" name="admin_fee" placeholder="Biaya Administrasi" value="{{currency($data->admin_fee)}}" required>
                            </div>
                        </div>
                        @else
                            <input type="hidden" class="form-control form-control-sm amount_currency" min="0" step="0.01" id="admin_fee" name="admin_fee" placeholder="Biaya Administrasi" value="0" required>
                        @endif



                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="font-weight-bold">Waktu Transaksi</label>
                                <input type="text" class="form-control form-control-sm trans_time" name="trans_time" value="{{$data->trans_time}}" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label font-weight-bold">Bukti Transaksi</label>
                                <input type="file" class="form-control form-control-sm"  name="trans_proof">
                                <small style="color: red">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip <br> Maksimal ukuran file:2.5 MB</small>

                            </div>
                        </div>
                        <div @if($type!=2)class="col-md-12" @else class="col-md-6" @endif>
                            <div class="form-group">
                                <label class="col-form-label font-weight-bold">Catatan</label>
                                <textarea name="trans_note" class="form-control form-control-sm">{{$data->trans_note}}</textarea>
                            </div>
                        </div>
                    </div>



                    <div class="modal-footer" style="border-top: none">
                        <input type='hidden' id="id" name='id' value='{{$data->id }}'>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type="hidden" name="trans_purpose" value="{{$type}}">
                        <input type="hidden" id="md_merchant_id" value="{{!is_null($data->id)?$data->md_merchant_id:'-1'}}">
                        <input type="hidden" id="cash" value="{{$cashOption}}">
                        <input type="hidden" id="bank" value="{{$bankOption}}">
                    @if(!is_null($data->id))

                            <input type="hidden" name='flag_name' value="{{str_replace('Edit ','',$title)}}">

                        @else
                            <input type="hidden" name='flag_name' value="{{str_replace('Tambah ','',$title)}}">

                        @endif
                        <a href="{{route('merchant.toko.acc.journal.cash-bank.index')}}" class="btn btn-light text-light mr-2">
                            <i class="fa fa-arrow-circle-left mr-2"></i>
                            Kembali</a>
                        <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $("#from").select2()
            $("#to").select2()
            if(!$("#id").val()){
                onchangeBranch()
            }
        })


        $('#form-konten').submit(function () {
            let from = $("#from").val()
            let to = $("#to").val()

            if(from==to)
            {
                otherMessage('warning','Asal Akun Bank/Kas tidak boleh sama dengan Tujuan Akun Bank/ Kas')
            }else{
                var data = getFormData('form-konten');
                ajaxTransfer("{{route('merchant.toko.acc.journal.cash-bank.save')}}", data,
                    function (response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                    });
            }
        })

        function onchangeBranch()
        {
            let branch = $("#branch").val()
            let cash = JSON.parse($("#cash").val())
            let bank = JSON.parse($("#bank").val())
            let newCash=[];
            let newBank = [];

            cash.forEach(item=>{
                if(item.md_merchant_id==branch)
                {
                    newCash.push(item)
                }
            })

            bank.forEach(item=>{
                if(item.md_merchant_id==branch)
                {
                    newBank.push(item)
                }
            })

            $("#from").empty()
            $('#to').empty();

            @if($type==1)
            $("#from").select2({
                data:newBank
            });
            $("#to").select2({
                data:newBank
            });
            @else
            $("#from").select2({
                data:newCash
            });
            $("#to").select2({
                data:newCash
            });

            @endif
        }

    </script>
@endsection

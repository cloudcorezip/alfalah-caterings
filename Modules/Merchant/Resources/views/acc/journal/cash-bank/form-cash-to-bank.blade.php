@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.acc.journal.cash-bank.index')}}">Kas & Bank</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Buat transfer kas dalam kas kecil, kas besar, dan juga kas bank</span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal px-4' backdrop="">
                    <div class="row">

                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>true,

])
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode Transaksi</label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{is_null($data->second_code)?$data->ref_code:$data->second_code}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Transaksi</label>
                                <input type="name" class="form-control form-control-sm" name="trans_name" placeholder="Nama Transaksi" value="{{$data->trans_name}}" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Asal Setoran</label>
                                @if(!is_null($data->id))
                                    <select class="form-control" id="from" name="from_acc_coa_detail_id" required>
                                        @foreach(collect(json_decode($cashOption))->where('md_merchant_id',$data->md_merchant_id) as $k)
                                            <option value="{{$k->id}}" @if($firstOption->acc_coa_detail_id==$k->id) selected @endif>{{$k->text}}</option>

                                        @endforeach
                                    </select>
                                @else
                                    <select class="form-control" id="from" name="from_acc_coa_detail_id" required>

                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="font-weight-bold">Waktu Transaksi</label>
                                <input type="text" class="form-control form-control-sm trans_time" name="trans_time" value="{{$data->trans_time}}" required>
                            </div>
                        </div>


                        <div class="col-md-12 mb-4" style="border: 1px dashed #CED4DA;background: #FBFBFB">
                            <table id="dynamic_field" class="table table-borderless mt-2" width="100%" cellpadding="0" cellspacing="0">
                                <thead style="background: #F6F6F6!important">
                                <tr>
                                    <td class="font-weight-bold" style="padding: 20px">Tujuan Setoran</td>
                                    <td class="font-weight-bold">Jumlah</td>
                                    <td class="font-weight-bold text-center">Opsi</td>
                                </tr>
                                </thead>
                                <tbody id="clearTable">
                                @if(!is_null($lastOption))
                                    @foreach($lastOption as $key =>$item)
                                        <tr id="row{{$key+1}}">
                                            <td class="text-left">
                                                <select  name="to_acc_coa_detail_id[]" id="coa-category{{$key+1}}" class="form-control form-control-sm select_coa" >
                                                    @foreach(collect(json_decode($bankOption))->where('md_merchant_id',$data->md_merchant_id) as $cc)
                                                        <option value="{{$cc->id}}"
                                                                @if($item->acc_coa_detail_id==$cc->id) selected @endif

                                                        >{{$cc->text}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td class="text-left">
                                                <input type="text" class="form-control form-control-sm amount_currency" id="amount{{$key+1}}" name="amount[]" placeholder="Jumlah" value="{{rupiah($item->amount)}}" required>
                                            </td>
                                            <td class="text-center"><button type="button" name="remove" id="{{$key+1}}" class="btn btn-xs btn-delete-xs btn_remove">
                                                    <span class="fa fa-trash-alt" style="color: white"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <button type="button" name="add" id="add" class="btn btn-darkblue btn-sm btn-rounded mb-2">
                                <i class="fa fa-plus mr-2"></i>
                                <span>Tambah</span>
                            </button>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label font-weight-bold">Bukti Transaksi</label>
                                <input type="file" class="form-control form-control-sm"  name="trans_proof">
                                <small style="color: red">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip <br> Maksimal ukuran file:2.5 MB</small>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Biaya Administrasi</label>
                                <input type="text" class="form-control form-control-sm amount_currency" min="0" step="0.01" id="admin_fee" name="admin_fee" placeholder="Biaya Administrasi" value="{{currency($data->admin_fee)}}" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label font-weight-bold">Catatan</label>
                                <textarea name="trans_note" class="form-control form-control-sm">{{$data->trans_note}}</textarea>
                            </div>
                        </div>
                    </div>



                    <div class="modal-footer" style="border-top: none">
                        <input type='hidden' id='id' name='id' value='{{$data->id }}'>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type="hidden" name="trans_purpose" value="{{$type}}">
                        <input type="hidden" id="md_merchant_id" value="{{!is_null($data->id)?$data->md_merchant_id:'-1'}}">
                        <input type="hidden" id="cash" value="{{$cashOption}}">
                        <input type="hidden" id="bank" value="{{$bankOption}}">
                        @if(!is_null($data->id))

                            <input type="hidden" name='flag_name' value="{{str_replace('Edit ','',$title)}}">

                        @else
                            <input type="hidden" name='flag_name' value="{{str_replace('Tambah ','',$title)}}">

                        @endif
                        <a href="{{route('merchant.toko.acc.journal.cash-bank.index')}}" class="btn btn-light text-light mr-2">
                            <i class="fa fa-arrow-circle-left mr-2"></i>
                            Kembali</a>
                        <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $("#from").select2()
            $(".select_coa").select2()
            var i={{is_null($lastOption)?0:$lastOption->count()+1}};

            @if(!is_null($data->id))
            var coaSelectedId = {};
            @foreach($lastOption as $j =>$jj)
                coaSelectedId[{{$j+1}}]={{$jj->acc_coa_detail_id}}
            @endforeach
            @else
            var  coaSelectedId= {};
            @endif

            $('#add').click(function(){
                i++;
                let bank = JSON.parse($("#bank").val())
                let status = true
                let branch = $("#branch").val();
                console.log(branch)

                let temp_branch = $("#md_merchant_id").val()
                let newCoa = [];

                if(temp_branch==-1 || temp_branch=='-1')
                {
                    status = false
                    clearTable()
                }

                if(branch!=temp_branch)
                {
                    status = false
                    clearTable()
                }

                $.each(bank,function (i,item){
                    if(i==0)
                    {
                        newCoa.push({
                            id:'-1',
                            text:'Pilih Akun',
                            md_merchant_id:'-'
                        })
                    }
                    if(item.md_merchant_id==branch)
                    {
                        newCoa.push(item)
                    }
                })

                console.log(newCoa)

                if(status==true)
                {
                    $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">' +
                        '<td class="text-left">' +
                        '<select  name="to_acc_coa_detail_id[]" id="coa-category' + i + '" class="form-control form-control-sm" ></select>' +
                        '</td>' +
                        '<td class="text-left">' +
                        '<input type="text" class="form-control form-control-sm amount" id="amount' + i + '" name="amount[]" placeholder="Jumlah" value="0" required>' +
                        '</td>' +
                        '<td class="text-center"><button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs btn_remove">' +
                        '<span class="fa fa-trash-alt" style="color: white"></span>' +
                        '</button>' +
                        '</td>' +
                        '</tr>');
                    $("#coa-category" + i).select2({
                        delay: 250,
                        data:newCoa
                    });

                    $('.amount').mask('#.##0,00', {reverse: true});

                    $("#coa-category" + i).on('select2:select', function (e) {
                        var data = e.params.data;
                        let statusDuplicate = false;
                        Object.keys(coaSelectedId).forEach(key => {
                            if(coaSelectedId[key] === data.id){
                                statusDuplicate=true;
                            }
                        });
                        if(statusDuplicate)
                        {
                            $('#row'+i).remove();
                            delete coaSelectedId[i];
                        }else{
                            coaSelectedId[i] = data.id;
                        }

                    });
                }

            });

            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                delete coaSelectedId[button_id];
                sumTotal();
            });


            $(".select_coa").on('select2:select', function (e) {
                var data = e.params.data;
                var rowId = $(this).closest('tr').attr('id');
                var id = rowId.substr(3);
                let statusDuplicate = false;
                Object.keys(coaSelectedId).forEach(key => {
                    if(coaSelectedId[key] === data.id){
                        statusDuplicate=true;
                    }
                });
                if(statusDuplicate)
                {
                    $('#row'+id).remove();
                    delete coaSelectedId[id];
                }else{
                    coaSelectedId[id] = data.id;
                }

            });
            if(!$("#id").val()){
                onchangeBranch()
            }
        })

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.acc.journal.cash-bank.save')}}", data,
                function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
        })


        function onchangeBranch()
        {
            let branch = $("#branch").val()
            $("#md_merchant_id").val(branch)
            let cash = JSON.parse($("#cash").val())
            let newCash = [];

            cash.forEach(item=>{
                if(item.md_merchant_id==branch)
                {
                    newCash.push(item)
                }
            })

            $("#from").empty()
            clearTable()
            $("#from").select2({
                data:newCash
            });

        }

        function clearTable()
        {
            $("#clearTable").empty();
        }


    </script>
@endsection

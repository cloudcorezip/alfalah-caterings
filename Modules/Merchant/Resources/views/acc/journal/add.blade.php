@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.acc.journal.index')}}">Jurnal Umum</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Catat setiap perubahan jurnal umum keuangan usahamu seperti perubahan modal, kekayaan, dan lainnya</span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal px-4' backdrop="">
                    <div class="row">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan pencatatan jurnal umum',
'selector'=>'branch',
'with_onchange'=>true,

])
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode Transaksi</label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{is_null($data->second_code)?$data->trans_code:$data->second_code}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Transaksi</label>
                                <input type="name" class="form-control form-control-sm" name="trans_name" placeholder="Nama Transaksi" value="{{str_replace(is_null($data->second_code)?$data->trans_code:$data->second_code,'',$data->trans_name)}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="font-weight-bold">Waktu Transaksi</label>
                                <input type="text" class="form-control form-control-sm trans_time" name="trans_time" value="{{$data->trans_time}}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Tujuan Transaksi</label>
                                <select id="trans_purpose" class="form-control form-control-sm init-select2" name="trans_purpose">
                                    <option value="-1">Internal</option>
                                    @foreach ($supplier as $b)
                                        <option @if ($b->nama_supplier==$data->trans_purpose)
                                                selected
                                                @endif value="{{$b->id}}">{{$b->nama_supplier}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <table id="dynamic_field" class="table table-borderless table-add-trans">
                                <thead>
                                <th class="text-center">
                                    <label for="exampleInputPassword1" class="font-weight-bold ">Nama Akun</label>
                                </th>


                                <th class="text-center"><label for="exampleInputPassword1" class="font-weight-bold">Deskripsi</label></th>



                                <th class="text-left">
                                    <label for="exampleInputPassword1" class="font-weight-bold">Debit</label>
                                </th>


                                <th class="text-left">
                                    <label for="exampleInputPassword1" class="font-weight-bold">Kredit</label>
                                </th>

                                <th class="text-center">
                                    <label for="exampleInputPassword1" class="font-weight-bold ">Opsi</label>
                                </th>

                                </thead>
                                <tbody id="clearTable">
                                @if (!is_null($detJurnal))
                                    @foreach ($detJurnal as $n => $j)
                                        <tr id="row{{$n+1}}" style="border-bottom: 1px solid #ebebeb">
                                            <td>
                                                <select class="form-control form-control-sm init-select2" name="acc_coa_category_id[]">
                                                    @foreach(collect(json_decode($getCoa))->where('md_merchant_id',$data->md_merchant_id) as $key =>$item)
                                                        <option value="{{$item->id}}" {{($item->id==$j->acc_coa_detail_id)?'selected':''}} >{{$item->text}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                    <input type="text" class="form-control form-control-sm desc" name="desc[]" value="{{$j->desc}}">
                                            </td>
                                            <td>
                                                    <input type="hidden" value="{{($j->coa_type!='Debit')?0:(float)$j->amount}}" class="amount_debit_original">

                                                    <input type="text" class="form-control form-control-sm amount_debit"  name="amount_debit[]" placeholder="Jumlah" value="{{($j->coa_type!='Debit')?rupiah(0):rupiah($j->amount)}}" required>
                                            </td>
                                            <td>
                                                    <input type="hidden" value="{{($j->coa_type!='Kredit')?0:(float)$j->amount}}" class="amount_kredit_original">
                                                    <input type="text" class="form-control form-control-sm amount_kredit"  name="amount_kredit[]" placeholder="Jumlah" value="{{($j->coa_type!='Kredit')?rupiah(0):rupiah($j->amount)}}" required>

                                            </td>
                                            <td class="text-center">
                                                    <button type="button" name="remove" id="{{$n+1}}" class='btn btn-xs btn-delete-xs btn_remove'>
                                                        <span class='fa fa-trash-alt' style='color: white'></span>
                                                    </button>

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <tfoot>
                                <tr style="border-bottom:1px solid #ebebeb ">
                                    <td colspan="2" class="text-left"><b>TOTAL</b></td>
                                    <td id="sub_debit" class="text-left"><b style="margin-left: 20px">0</b></td>
                                    <td id="sub_kredit" class="text-left"><b style="margin-left: 20px">0</b></td>
                                    <td></td>
                                </tr>
                                <tr style="border-bottom:1px solid #ebebeb ">
                                    <td colspan="2">
                                        <small style="color: red">Selisih debit dan kredit : </small>
                                    </td>
                                    <td id="diff"><b style="margin-left: 20px">0</b></td>
                                    <td></td>
                                </tr>
                                </tfoot>

                            </table>
                            <button type="button" name="add" id="add" class="btn btn-darkblue btn-sm btn-rounded py-2 px-4">
                                <i class="fa fa-plus mr-2"></i>
                                <span>Tambah Jurnal</span>
                            </button>
                            <br>

                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-form-label col-md-12 font-weight-bold">Bukti Transaksi</label>
                                <div class="col-lg-12 col-md-12">
                                    <input type="file" class="form-control form-control-sm"  name="trans_proof">
                                    <small style="color: red">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip <br> Maksimal ukuran file:2.5 MB</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-form-label col-md-6 font-weight-bold">Catatan</label>
                                <div class="col-lg-12 col-md-12">
                                    <textarea name="trans_note" class="form-control form-control-sm">{{$data->trans_note}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>

                    </div>
                    <input type='hidden' id="id" name='id' value='{{$data->id }}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" id="coa" value="{{$getCoa}}">
                    <input type="hidden" id="total_amount" value="0">
                    <input type="hidden" id="md_merchant_id" value="{{!is_null($data->id)?$data->md_merchant_id:'-1'}}">

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        // afterDismissModal()
        $(document).ready(function () {
            var coa= JSON.parse($("#coa").val())

            @if(is_null($data->id))
            $("#save-po").attr("disabled", true);
            @endif

            sumTotal()

            $(".init-select2").select2();
            dateTimePicker(".trans_time");
            $("#new_trans").hide();

            $('.amount_debit,.amount_kredit').mask('#.##0,00', {reverse: true});

            $("#trans_purpose").on('change', function() {
                if ($(this).val() == '-2'){
                    $("#new_trans").show();
                } else {
                    $("#new_trans").hide();
                }
            });
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');
                ajaxTransfer("{{route('merchant.toko.acc.journal.save')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })

            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                sumTotal()
            });
            var i={{!is_null($detJurnal)?(count($detJurnal)+1) : 0}};

            $('#add').click(function(){
                i++;
                let status= true;
                let branch = $("#branch").val();
                let temp_branch = $("#md_merchant_id").val()
                let newCoa = [];

                if(temp_branch==-1 || temp_branch=='-1')
                {
                    status = false
                    clearTable()
                }

                if(branch!=temp_branch)
                {
                    status = false
                    clearTable()
                }

                coa.forEach(item=>{
                    if(item.md_merchant_id==branch)
                    {
                        newCoa.push(item)
                    }
                })
                if(status==true)
                {
                    $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added" style="border-bottom:1px solid #ebebeb">' +
                        '<td class="text-left">' +
                        '<select  name="acc_coa_category_id[]" id="coa-category' + i + '" class="form-control form-control-sm" ></select>' +
                        '</td>' +
                        '<td class="text-center">' +
                        '<input type="text" name="desc[]" class="form-control form-control-sm" id="desc'+i+'" >'+
                        '</td>' +
                        '<td class="text-left">' +
                        '<input type="hidden" value="0" class="amount_debit_original">' +
                        '<input type="text" class="form-control form-control-sm amount_debit" id="debit' + i + '" name="amount_debit[]" placeholder="Jumlah" value="0" required>' +
                        '</td>' +
                        '<td class="text-left">' +
                        '<input type="hidden" value="0" class="amount_kredit_original">' +
                        '<input type="text" class="form-control form-control-sm amount_kredit" id="kredit' + i + '" name="amount_kredit[]" placeholder="Jumlah" value="0" required>' +
                        '</td>' +
                        '<td class="text-center"><button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs btn_remove">' +
                        '<span class="fa fa-trash-alt" style="color: white"></span>' +
                        '</button>' +
                        '</td>' +
                        '</tr>');
                    $("#coa-category" + i).select2({
                        data:newCoa
                    });
                    i++;
                    $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added" style="border-bottom:1px solid #ebebeb">' +
                        '<td class="text-left">' +
                        '<select  name="acc_coa_category_id[]" id="coa-category' + i + '" class="form-control form-control-sm" ></select>' +
                        '</td>' +
                        '<td class="text-center">' +
                        '<input type="text" name="desc[]" class="form-control form-control-sm" id="desc'+i+'" >'+
                        '</td>' +
                        '<td class="text-left">' +
                        '<input type="hidden" value="0" class="amount_debit_original">' +
                        '<input type="text" class="form-control form-control-sm amount_debit" id="debit' + i + '" name="amount_debit[]" placeholder="Jumlah" value="0" required>' +
                        '</td>' +
                        '<td class="text-left">' +
                        '<input type="hidden" value="0" class="amount_kredit_original">' +
                        '<input type="text" class="form-control form-control-sm amount_kredit" id="kredit' + i + '" name="amount_kredit[]" placeholder="Jumlah" value="0" required>' +
                        '</td>' +
                        '<td class="text-center"><button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs btn_remove">' +
                        '<span class="fa fa-trash-alt" style="color: white"></span>' +
                        '</button>' +
                        '</td>' +
                        '</tr>');
                    $("#coa-category" + i).select2({
                        data:newCoa
                    });
                }

                $('.amount_debit,.amount_kredit').mask('#.##0,00', {reverse: true});

            });

            $("table").on("change", "input", function () {
                var row = $(this).closest("tr");
                let debit = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal(row.find(".amount_debit").val(),'.',''),',','.')
                let kredit = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal(row.find(".amount_kredit").val(),'.',''),',','.')
                let isClassChange = row.prevObject[0].className

                if(isClassChange=='form-control form-control-sm amount_kredit'){
                    if(debit>0)
                    {
                        row.find('.amount_debit').val(0)
                    }
                }

                if(isClassChange=='form-control form-control-sm amount_debit'){
                    if(kredit>0)
                    {
                        row.find('.amount_kredit').val(0)
                    }
                }

                sumTotal()
            });
            if(!$("#id").val()){
                onchangeBranch()
            }
        })

        function sumTotal()
        {
            var sumDebit=0;
            var sumKredit=0;
            var count=0;

            $(".amount_debit").each(function(){
                sumDebit += + replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($(this).val(),'.',''),',','.');
                count++
            });

            $(".amount_kredit").each(function(){
                sumKredit += + replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($(this).val(),'.',''),',','.');
            });

            let diff=(sumDebit>sumKredit)?Math.abs(sumDebit-sumKredit):
                Math.abs(sumKredit-sumDebit)

            if(diff==0 && count==0)
            {
                $("#save-po").attr("disabled", true);
            }else if(diff==0 && count>0)
            {
                $("#save-po").attr("disabled", false);
            }else{

                $("#save-po").attr("disabled", true);
            }

            $("#sub_debit").html("<b class='text-left' style='margin-left: 20px'>"+currencyFormat(sumDebit,'')+"</b>")
            $("#sub_kredit").html("<b class='text-left' style='margin-left: 20px'>"+currencyFormat(sumKredit,'')+"</b>")
            $("#diff").html("<b class='text-left' style='margin-left: 20px'>"+currencyFormat(diff,'')+"</b>")
            $("#total_amount").val(sumDebit)
        }

        function clearTable()
        {
            $("#clearTable").empty();
        }

        function onchangeBranch()
        {
            let branch = $("#branch").val();
            $("#md_merchant_id").val(branch)
            clearTable()
        }
    </script>
@endsection

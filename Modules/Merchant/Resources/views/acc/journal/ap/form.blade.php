<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        @include('merchant::component.branch-selection',
    ['withoutModal'=>true,
    'data'=>$data,
    'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
    'selector'=>'branch-ap',
    'with_onchange'=>true,

    ])
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Kode Transaksi</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{is_null($data->second_code)?$data->ap_code:$data->second_code}}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Nama Transaksi</label>
                <input type="text" class="form-control form-control-sm" name="trans_name" placeholder="Nama Transaksi" value="{{$data->ap_name}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Jenis Utang</label>
                @if ($data->trans_purpose==null)
                    @if(is_null($data->id))
                    <select name="acc_coa_category_id" id="selectAp" class="form-control form-control-sm">

                    </select>
                    @else
                    <select name="acc_coa_category_id" id="selectAp" class="form-control form-control-sm" disabled>
                        @foreach($categoryOption as $key =>$item)
                            <option value="{{$item->id}}" {{($item->id==$data->getCoa->acc_coa_detail_id)?'selected':'' }}>{{$item->text}}</option>
                        @endforeach
                    </select>
                    @endif
                @else
                <input type="text" class="form-control form-control-sm acc_coa_category_id" name="acc_coa_category_id" value="{{$data->acc_coa_category_id}}" disabled>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Pemberi Utang</label>
                @if ($data->trans_purpose==null)
                    @if(is_null($data->id))
                        <select name="trans_purpose" id="trans_purpose" class="form-control form-control-sm" required>

                        </select>
                    @else
                        <select name="trans_purpose" class="form-control form-control-sm" disabled>
                            <option value="{{$data->ref_user_id}}" selected>
                                {{$data->ap_purpose}}
                            </option>
                        </select>
                    @endif

                @else
                    <input type="text" class="form-control form-control-sm trans_purpose" name="trans_purpose" value="{{$data->trans_purpose}}" disabled>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Waktu Transaksi</label>
                <input type="text" class="form-control form-control-sm trans_time" name="trans_time" value="{{$data->ap_time}}" placeholder="Waktu Transaksi" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Jatuh Tempo</label>
                <input type="text" class="form-control form-control-sm trans_time" name="due_date" value="{{$data->due_date}}" placeholder="Jatuh Tempo" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Bukti Transaksi</label>
                <input type="file" class="form-control form-control-sm"  name="trans_proof">
                <small class="d-block mt-1" style="color: #EE6767;">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</small>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Catatan</label>
                <textarea name="trans_note" class="form-control form-control-sm" cols="5">{{$data->ap_note}}</textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' id='id' name='id' value='{{$data->id }}'>
    <input type='hidden' name='type' id="type" value='{{!is_null($data->is_from_customer)?$data->is_from_customer:0}}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        dateTimePicker('.trans_time')
        getCustomer()
        $("#selectAp").select2();
        if(!$("#id").val()){
                onchangeBranch()
            }
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.acc.journal.ap.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })

    })
    function getCustomer() {
        $("#trans_purpose").select2({
            ajax: {
                type: "GET",
                url: "{{route('merchant.toko.transaction.sale-order.contact-list')}}",
                dataType: 'json',
                delay: 250,
                headers: {
                    "senna-auth": "{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    console.log(data)
                    return {
                        results: data
                    };
                },
                cache: true
            },
            placeholder:"--Pilih Pemberi Pinjaman--"
        });
        //harusnya name
    }
    $("#trans_purpose").on('select2:select', function (e) {
            var data = e.params.data;

            $('#type').val(data.type)
        });
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah transaksi utang, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    function onchangeBranch()
    {
        let branch = $("#branch-ap").val()
        $("#selectAp").empty()
        $.ajax({
            type: "GET",
            url: "{{route('merchant.toko.acc.journal.ap.ap-list')}}?md_merchant_id="+branch,
            dataType: 'json',
            success: function(result){

                $("#selectAp").select2({
                    data:result
                });

            }});
    }

</script>

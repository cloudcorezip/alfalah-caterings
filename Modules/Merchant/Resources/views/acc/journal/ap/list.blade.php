<div class="table-responsive">

    <table class="table table-custom" id="table-data-ap" width="100%" cellspacing="0">
        <thead>
        <tr>
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<script>

    function reloadDataTable(isReload=0)
    {
        ajaxDataTable('#table-data-ap', 1, "{{route('merchant.toko.acc.journal.ap.datatable')}}?sk={{$searchKey}}&start_date={{$start_date}}&end_date={{$end_date}}&encode={{$encodeMerchant}}&ref_user_id={{$ref_user_id}}&type_person={{$type_person}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ],0);
    }
    $(document).ready(function() {

        reloadDataTable(0);
    })
</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.acc.journal.ap.index')}}">Utang & Piutang</a>
                        </li>
                    </ol>
                </nav>
                <span>Tambah dan lihat detail catatan transaksi utang & piutang usaha yang pernah kamu lakukan</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row" style="margin-bottom: 10px">
                    {!! $add !!}
                </div>
                <div id="output-sale-order">
                    @include('merchant::acc.journal.ap.list')
                </div>

            </div>
        </div>
    </div>
    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>
    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold mt-3 text-dark">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" class="px-30" onsubmit="return false">
            <div class="row">
            <div class="col-md-12 mb-3">
                    <label>Pilih Tanggal Transaksi Mulai :</label>
                    <input type="text" class="form-control form-control startDate" id="start" value="{{$start_date}}" name="startDate" style="background: white" required>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Tanggal Transaksi Sampai :</label>
                    <input type="text" class="form-control form-control endDate" name="endDate" value="{{$end_date}}" style="background: white" required>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pemberi / Penerima Utang</label>
                    <select name="ref_user_id" class="form-control" id="ap_purpose" required>
                        <option value="-1">--- Pilih Pemberi/ Penerima Utang ---</option>
                    </select>
                </div>

                <div class="col-md-12 mb-3">
                    <label for="exampleInputPassword1">Pilih Cabang</label>
                    <select id="branch" class="form-control form-control" multiple required>
                        <option value="-1" @if(in_array('-1',$merchantId)) selected @endif>Semua Cabang</option>
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" @if(in_array($item->id,$merchantId)) selected @endif>{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <input type="hidden" name="search_key" value="{{$searchKey}}">
                <input type="hidden" name="type_person" id="type_person" value="-1">
                <div class="col-md-12 mb-3 mt-3">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a onclick="exportData()" class="btn btn-edit-xs text-primary btn-block py-5 px-4"><i class="fa fa-download mr-2"></i><span>Download</span></a>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('js')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.acc.journal.ap.delete')}}", data,  function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        }

        function deleteDataAr(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.acc.journal.ar.delete')}}", data,  function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        }

        function exportData() {
            var info = $('#table-data-ap_info').html().split('dari');
            if (info.length < 2) {
                return false;
            }

            var dataCount = parseInt(info[1].replace('entri', '').replace(',', '').replace(' ', ''));
            var seconds = parseFloat(dataCount / 66).toFixed(3);
            var estimation = '';

            if (seconds < 60) {
                estimation = seconds + ' detik';
            } else {
                var minute = parseInt(seconds / 60);
                seconds = seconds % 60;
                estimation = minute + ' menit ' + seconds + ' detik';
            }

            modalConfirm('Export Data', 'Proses export data sejumlah ' + dataCount + ' data membutuhkan estimasi waktu ' + estimation + '. Lanjutkan proses export data?', function () {
                var data = getFormData('form-filter-sale-order');
                let selectedMerchantId = $("#branch").select2('data');
                let merchantIds = [];
                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
                data.append('md_merchant_id',JSON.stringify(merchantIds));
                ajaxTransfer("{{route('merchant.toko.acc.journal.ap.exportData')}}", data, '#modal-output');
            });

        }

        $("#branch").on("select2:select", function(e){
            if(e.params.data.id == "-1"){
                $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
            } else {
                $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
            }
        });

        $(document).ready(function() {
            reloadDataTable(0);
            dateTimePicker(".startDate");
            dateTimePicker(".endDate");
            getCustomer()
            $('#branch').select2();
            $('#form-filter-sale-order').submit(function () {
                var startDate = $('.startDate').val();
                var endDate = $('.endDate').val();
                let selectedMerchantId = $("#branch").select2('data');
                let merchantIds = [];
                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
                var data = getFormData('form-filter-sale-order');
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('md_merchant_id',JSON.stringify(merchantIds));

                ajaxTransfer("{{route('merchant.toko.acc.journal.ap.reload')}}", data, '#output-sale-order');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        })

        function getCustomer()
        {
            $("#ap_purpose").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.transaction.sale-order.contact-list')}}",
                    dataType: 'json',
                    delay: 250,
                    headers: {
                        "senna-auth": "{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                placeholder:"--Pilih Penerima --"

            });
        }

        $("#ap_purpose").on('select2:select', function (e) {
            var data = e.params.data;
            $('#type_person').val(data.type)
        });
    </script>
@endsection

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->

    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.acc.journal.ap.index')}}">Utang & Piutang</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan detail utang usahamu terhadap pihak ketiga</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card mb-4">
                    <div class="row">
                        <div class="col-md-5 mt-2" style="border-right: 1px solid #E5E5E5">
                            <div class="card-body">
                                <h5 class="font-weight-bold">{{$data->ap_purpose}}</h5>
                                <h6>Memberi utang kepadamu sebesar :</h6>
                                <h3 class="card-subtitle mb-2 text-orange text-left font-weight-bold" id="residual_amount">{{rupiah($data->ap_amount)}}</h3>

                            </div>
                        </div>
                        <div class="col-md-3 mt-2">
                            <div class="card-body">
                                <h6 class="mt-4 text-center">Sisa Utang</h6>
                                <h5 class="card-subtitle mb-2 text-center font-weight-bold" id="ap_amount">{{rupiah($data->ap_amount-$data->paid_nominal)}}</h5>

                            </div>
                        </div>
                        <div class="col-md-4 mt-2">
                            <div class="card-body">
                                <h6 class="mt-4 text-center">Total Pembayaran</h6>
                                <h5 class="card-subtitle mb-2 text-center font-weight-bold" id="paid_nominal">{{rupiah($data->paid_nominal)}}</h5>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card-body">
                                <table class="table table-borderless" width="100%" cellpadding="0">
                                    <thead>
                                    <tr bgcolor="white">
                                        <th width="20%" class="font-weight-bold">TANGGAL</th>
                                        <th class="text-left font-weight-bold">TERIMA</th>
                                        <th class="text-left font-weight-bold">BERIKAN</th>
                                        <th class="font-weight-bold">BUKTI TRANSAKSI</th>
                                        <th class="font-weight-bold">OPSI</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr bgcolor="#F8F9FC">
                                        <td></td>
                                        <td>
                                            <a class="btn btn-rounded btn-success btn-sm text-white" onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.acc.journal.ap.add-detail',['apId'=>$data->id,'type'=>1])}}"><i class="fa fa-plus mr-1"></i>Terima Uang</a>

                                        </td>
                                        <td>
                                            <a class="btn btn-rounded btn-success btn-sm text-white" onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.acc.journal.ap.add-detail',['apId'=>$data->id,'type'=>2])}}">
                                                <i class="fa fa-plus mr-1"></i>
                                                Bayarkan</a>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @foreach($ap->where('is_deleted',0) as $key =>$item)
                                        <tr @if($key%2==1) bgcolor="#F8F9FC" @endif style="border-bottom: 1px solid #F8F9FC">
                                            <td>{{\Carbon\Carbon::parse($item->paid_date)->isoFormat(' D MMMM Y  HH:mm:ss')}}</td>
                                            <td>
                                                @if($item->type_action!=2)
                                                    {{rupiah($item->paid_nominal)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->type_action==2)
                                                    {{rupiah($item->paid_nominal)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(!is_null($item->trans_proof))
                                                    <a href="{{env('S3_URL').$item->trans_proof}}" target='_blank' class='btn btn-xs btn-rounded btn-info text-white'><i class='fa fa-download mr-2' ></i>Download</a>
                                                @else
                                                    <span class="badge badge-pill badge-warning text-white">Tidak ada bukti transaksi</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->type_action==2)
                                                    <button type="button" class='btn btn-xs btn-delete-xs' onclick="hapusPembayaran({{$item->id}})">
                                                        <span class='fa fa-trash-alt' style='color: white'></span>
                                                    </button>
                                                @else
                                                    <button type="button" class='btn btn-xs btn-delete-xs' onclick="hapusPemberian({{$item->id}})">
                                                        <span class='fa fa-trash-alt' style='color: white'></span>
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                <br>
                                {{$ap->withQueryString()->links('vendor.pagination.with-showing-entry')}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Cabang</span>
                            </dd>
                            <dt>
                                <h5>{{$data->getMerchant->name}}</h5>
                            </dt>
                        </dl>

                        <dl class="trans-info">
                            <dd>
                                <span>Kode Transaksi</span>
                            </dd>
                            <dt>
                                <h5>{{!is_null($data->second_code)?$data->second_code:$data->ap_code}}</h5>
                            </dt>
                        </dl>

                        <dl class="trans-info">
                            <dd>
                                <span>Kode Transaksi</span>
                            </dd>
                            <dt>
                                <h5>{{$data->getCoa->name}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Waktu Transaksi</span>
                            </dd>
                            <dt>
                                <h5>{{\Carbon\Carbon::parse($data->ap_time)->isoFormat(' D MMMM Y  HH:mm:ss')}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd class="trans-info">
                                <span>Status Transaksi</span>
                            </dd>
                            <dt>
                                <h5 class="text-tosca">@if ($data->is_paid_off==0 && $data->ap_amount==0)
                                        Belum Ada Hutang
                                    @elseif($data->is_paid_off==0 && $data->ap_amount>=0)
                                        Belum Lunas
                                    @else
                                        Lunas
                                    @endif</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Jatuh Tempo Pembayaran</span>
                            </dd>
                            <dt>
                                @if(is_null($data->due_date))
                                    <h5>-</h5>
                                @else
                                    <h5>{{\Carbon\Carbon::parse($data->due_date)->isoFormat(' D MMMM Y  HH:mm:ss')}}</h5>
                                @endif

                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Opsi</span>
                            </dd>
                            <dt>

                                <a target="{{route('merchant.toko.acc.journal.ap.add',['id'=>$data->id])}}" onclick="loadModalFullScreen(this)" class="btn btn-sm btn-edit-xs mb-4 text-white">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </dt>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.acc.journal.ap.delete')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                });
            })
        }
        function hapusPemberian(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.acc.journal.ap.delete-giving')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                });
            })
        }

        function hapusPembayaran(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.acc.journal.ap.delete-payment')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                });
            })
        }
    </script>
@endsection

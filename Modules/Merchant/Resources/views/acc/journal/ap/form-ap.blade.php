
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Kode Transaksi</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{is_null($data->second_code)?$data->ap_detail_code:$data->second_code}}">
            </div>
        </div>
        <div class="col-md-6" id="trans_date">
            <div class="form-group">
                <label class="font-weight-bold">Tanggal Penerimaan</label>
                <input type="text" class="form-control form-control-sm trans_time" name="paid_date" value="{{$data->paid_date}}" placeholder="Pilih Tanggal" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Jumlah Penerimaan</label>
                <input type="text" name="amount" id="amount" class="form-control amount_currency" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="{{rupiah($data->paid_nominal)}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Metode Penerimaan</label>
                <select class="form-control form-control-sm" id="from" name="payment_acc_coa_detail_id">
                    @foreach($payment as $item)
                        <optgroup label="{{$item['name']}}">
                            @if(count($item['data'])>0)
                                @if($item['name']=='Pembayaran Digital')
                                    @foreach($item['data'] as $d)
                                        <optgroup label="--{{$d['name']}}">
                                            @if(!empty($d['data']))
                                                @foreach($d['data'] as $c)
                                                    <option value="{{$c->acc_coa_id}}" {{($c->acc_coa_id==-1)?'selected':'' }}>{{$c['name']}} </option>

                                                @endforeach
                                            @endif
                                        </optgroup>
                                    @endforeach
                                @else
                                    @foreach($item['data'] as $key =>$other)

                                        <option value="{{$other->acc_coa_id}}" @if(!is_null($data->id)){{($other->acc_coa_id==-1)?'selected':'' }}@endif>{{$other->name}} @if($item['name']=='Label Pembayaran Lainnya') @endif</option>
                                    @endforeach
                                @endif
                            @endif
                        </optgroup>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Bukti Transaksi</label>
                <input type="file" class="form-control form-control-sm"  name="trans_proof">
                <small class="d-block mt-2" style="color: #EE6767;">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</small>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold" style="margin-top: 10px;font-weight: bold">Catatan</label>
                <textarea name="trans_note" class="form-control form-control-sm" cols="5">{{$data->trans_note}}</textarea>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='ap_id' value='{{$ap->id }}'>
    <input type='hidden' name='md_merchant_id' value='{{$ap->md_merchant_id }}'>
    <input type='hidden' name='ap_acc_coa' value='{{$ap->ap_acc_coa }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        dateTimePicker(".trans_time");
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        $("#from").select2();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.acc.journal.ap.save-detail',['type'=>1])}}", data,
                function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })


</script>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Terima Utang</h4>
                                        <span class="span-text">Untuk menambah penerimaan pinjaman, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });
</script>

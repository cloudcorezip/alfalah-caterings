@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.acc.jurnal.administrative-expense.index')}}">Pendapatan & Biaya</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Catat biaya yang dikeluarkan setiap waktu tertentu pada usahamu</span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal px-4' backdrop="">
                    <div class="row">
                            @include('merchant::component.branch-selection',
    ['withoutModal'=>true,
    'data'=>$data,
    'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
    'selector'=>'branch',
    'with_onchange'=>true,

    ])
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode Transaksi</label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{is_null($data->second_code)?$data->ref_code:$data->second_code}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Transaksi</label>
                                <input type="name" class="form-control form-control-sm" name="trans_name" placeholder="Nama Transaksi" value="{{str_replace(is_null($data->second_code)?$data->ref_code:$data->second_code,'',$data->trans_name)}}" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="font-weight-bold">Waktu Transaksi</label>
                                <input type="text" class="form-control form-control-sm trans_time" name="trans_time" value="{{$data->trans_time}}" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Asal Transaksi</label>
                                <select id="trans_purpose" class="form-control form-control-sm select_custom" name="sc_supplier_id">
                                    <option value="-1">Internal</option>
                                    @foreach ($supplier as $b)
                                        <option @if ($b->nama_supplier==$data->trans_purpose)
                                                selected
                                                @endif value="{{$b->id}}">{{$b->nama_supplier}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Asal Pendapatan</label>
                                @if(is_null($data->id))
                                    <select id="to" class="form-control form-control-sm select_custom" name="acc_coa_detail_id">
                                    </select>
                                @else
                                    <select id="to" class="form-control form-control-sm select_custom" name="acc_coa_detail_id">

                                        @foreach($costList as $key =>$item)

                                            <option value="{{$item->id}}" {{($item->id==$firstOption->acc_coa_detail_id)?'selected':'' }}>{{$item->text}}</option>

                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Metode Penerimaan</label>
                                @if(is_null($data->id))
                                    <select class="form-control form-control-sm" id="from" name="payment_acc_coa_detail_id">
                                    </select>
                                @else
                                    <select class="form-control form-control-sm" id="from" name="payment_acc_coa_detail_id">
                                        @foreach($payment as $item)
                                            <optgroup label="{{$item['name']}}">
                                                @if(count($item['data'])>0)
                                                    @if($item['name']=='Pembayaran Digital')
                                                        @foreach($item['data'] as $d)
                                                            <optgroup label="--{{$d['name']}}">
                                                                @if(!empty($d['data']))
                                                                    @foreach($d['data'] as $c)
                                                                        <option value="{{$c->acc_coa_id}}" {{($c->acc_coa_id==$lastOption->acc_coa_detail_id)?'selected':'' }}>{{$c['name']}} </option>

                                                                    @endforeach
                                                                @endif
                                                            </optgroup>
                                                        @endforeach
                                                    @else
                                                        @foreach($item['data'] as $key =>$other)

                                                            <option value="{{$other->acc_coa_id}}" @if(!is_null($data->id)){{($other->acc_coa_id==$lastOption->acc_coa_detail_id)?'selected':'' }}@endif>{{$other->name}} @if($item['name']=='Label Pembayaran Lainnya') @endif</option>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </optgroup>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Jumlah</label>
                                <input type="text" class="form-control form-control-sm amount_currency" min="0" step="0.01" id="trans_amount" name="trans_amount" placeholder="Jumlah" value="{{currency($data->trans_amount)}}" required>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label font-weight-bold">Bukti Transaksi</label>
                                <input type="file" class="form-control form-control-sm"  name="trans_proof">
                                <small style="color: red">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip <br> Maksimal ukuran file:2.5 MB</small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label font-weight-bold">Catatan</label>
                                <textarea name="trans_note" class="form-control form-control-sm">{{$data->trans_note}}</textarea>

                            </div>
                        </div>
                    </div>



                    <div class="modal-footer" style="border-top: none">
                        <input type='hidden' id='id' name='id' value='{{$data->id }}'>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type="hidden" id="md_merchant_id" value="{{!is_null($data->id)?$data->md_merchant_id:'-1'}}">
                        <a href="{{route('merchant.toko.acc.jurnal.administrative-expense.index')}}" class="btn btn-light text-light mr-2">
                            <i class="fa fa-arrow-circle-left mr-2"></i>
                            Kembali</a>
                        <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $("#branch").select2()
            $("#from").select2()
            $("#to").select2()
            $(".select_custom").select2()
            if(!$("#id").val()){
                onchangeBranch()
            }
        })


        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.acc.jurnal.other-income.save')}}", data,
                function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
        })

        function onchangeBranch()
        {
            let branch = $("#branch").val()
            $("#from").empty()
            $('#to').empty();

            $.ajax({
                type: "GET",
                url: "{{route('merchant.toko.acc.jurnal.other-income.revenue-list')}}?md_merchant_id="+branch,
                dataType: 'json',
                success: function(result){

                    $("#to").select2({
                        data:result
                    });

                }});

            $.ajax({
                type: "GET",
                url: "{{route('merchant.toko.acc.jurnal.administrative-expense.payment-list')}}?md_merchant_id="+branch,
                dataType: 'json',
                success: function(result){

                    $("#from").select2({
                        data:result
                    });

                }});


        }

    </script>
@endsection

<div class="table-responsive">

    <table class="table table-custom" id="table-data-jurnal" width="100%" cellspacing="0">
        <thead>
        <tr>
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<script>

    function reloadDataTable(isReload=0)
    {
        ajaxDataTable('#table-data-jurnal', 1, "{{route('merchant.toko.acc.journal.closing.datatable')}}?sk={{$searchKey}}&start_date={{$start_date}}&end_date={{$end_date}}&encode={{$encodeMerchant}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ],0);
    }
    $(document).ready(function() {

        reloadDataTable(0);
    })
</script>

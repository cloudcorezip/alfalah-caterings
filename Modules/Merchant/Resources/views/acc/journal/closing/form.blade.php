@extends('backend-v3.layout.main')
@section('title',$title)

@section('content')
    <style>
        #progressbar {
            overflow: hidden;
            color: lightgrey;
            width: 30%;
            position: relative;
            justify-content: space-between;
            list-style-type: none;
            display: flex;
        }
        #progressbar .active {
            color: #72BA6C
        }
        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            float: left;
            position: relative;
            font-weight: 400
        }
        #progressbar #step1:before {
            content: "1";
            text-align: center;
        }
        #progressbar #step2:before {
            content: "2";
            text-align: center;
        }
        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 20px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }
        #progressbar li.active:before
        {
            background: #72BA6C
        }
        #progressbar li.active:after {
            background: #72BA6C
        }
        #progressbar li.active:after {
            background: #72BA6C
        }
        #progressbar-connector{
            position: absolute;
            height: 2px;
            background-color: #72BA6C;
            top: 25px;
            transform: translateY(-50%);
            left: 30px;
            right: 40px;
        }
    </style>
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.acc.journal.closing.index')}}">Jurnal Penutup</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Tambah jurnal penutup untuk menutup buku keuangan pada usahamu</span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="d-flex justify-content-center mb-4">
                    <ul id="progressbar">
                        <div id="progressbar-connector"></div>
                        <li @if($step==0) class="active" @endif id="step1">
                            <strong> Periode Waktu </strong>
                        </li>
                        <li  @if($step==1) class="active" @endif id="step2"> <strong> Akun Distribusi</strong> </li>
                    </ul>

                </div>

            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                @if($step==0)
                <div class="container" style="margin: 0 auto;width: 80%">
                    <h5 class="font-weight-bold text-center" style="margin-bottom: 50px">Tentukan tanggal periode keuangan untuk melakukan proses tutup buku</h5>
                    <form onsubmit="return false;" id="form-konten" class='form-horizontal px-4' backdrop="">
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Mulai Tanggal</label>
                                    <input
                                        type="date"
                                        class="form-control form-control-sm"
                                        name="start_date"
                                        placeholder="Tanggal Mulai"
                                        id="start_date"
                                        required
                                        value="{{is_null($data->id)?'':\Carbon\Carbon::parse($data->start_date)->format('Y-m-d')}}"

                                        @if($data->is_distribution==1 || $data->getDetail->count()>0) disabled @endif
                                    >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Sampai Tanggal</label>
                                    <input
                                        type="date"
                                        class="form-control form-control-sm"
                                        name="end_date"
                                        placeholder="Tanggal Selesai"
                                        id="end_date"
                                        required
                                        value="{{is_null($data->id)?'':\Carbon\Carbon::parse($data->end_date)->format('Y-m-d')}}"
                                        @if($data->is_distribution==1 || $data->getDetail->count()>0) disabled @endif


                                    >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pilih Cabang</label>
                                    @if(!is_null($data->id))
                                        <select id="branch" class="form-control form-control-sm select2" name="md_merchant_id" required disabled>
                                            <option value="{{$data->md_merchant_id}}" selected>{{$data->getMerchant->name}}</option>
                                        </select>
                                    @else
                                        <select id="branch" class="form-control form-control-sm select2" name="md_merchant_id" required>
                                            @foreach(get_cabang() as $c)
                                                <option value="{{$c->id}}"@if($c->id==$data->md_merchant_id) selected @endif>{{$c->nama_cabang}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type='hidden' name='id' value='{{$data->id}}'>
                        <input type='hidden' name='step' value='{{$step}}'>
                        <button id="btn-save-hidden" style="display: none"></button>
                    </form>
                </div>

                <div class="row" style="margin-top: 50px">
                    <div class="col-md-12 mb-2 text-right">
                        <a href="{{route('merchant.toko.acc.journal.closing.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                        <button class="btn btn-success" id="btn-save"><i class="fa fa-arrow-circle-right mr-2"></i> Selanjutnya</button>
                    </div>
                </div>
                @else
                    <h6 class="text-left">Tutup Jurnal</h6>
                    <h5 class="font-weight-bold text-left">Ringkasan Laba Rugi</h5>
                    <div class="row" style="margin-top: 30px">
                        <div class="col-md-3">
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">Saldo Debit</h5>
                                    <h5 class="card-subtitle mb-2 text-orange text-right font-weight-bold" id="saldo_debit">{{($data->debit_amount<0)?'('.rupiah($data->kredit_amount).')':rupiah($data->debit_amount)}}</h5>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">Saldo Kredit</h5>
                                    <h5 class="card-subtitle mb-2 text-orange text-right font-weight-bold" id="saldo_kredit">{{($data->kredit_amount<0)?'('.rupiah($data->kredit_amount).')':rupiah($data->kredit_amount)}}</h5>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">Saldo Akhir</h5>
                                    <h5 class="card-subtitle mb-2 text-orange text-right font-weight-bold" id="end_saldo">{{($data->end_amount<0)?'('.rupiah($data->end_amount).')':rupiah($data->end_saldo)}}</h5>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <h5 class="card-title font-weight-bold">Saldo Terdistribusi</h5>
                                    <h5 class="card-subtitle mb-2 text-orange text-right font-weight-bold" id="saldo_distribusi">{{($data->amount_distribution<0)?'('.rupiah($data->amount_distribution).')':rupiah($data->amount_distribution)}}</h5>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <form onsubmit="return false;" id="form-konten-distribution" backdrop="" style="width: 100%">

                            <div class="col-md-12">
                                <div id="accordion">
                                    <div>
                                        <div class="card-header" id="headingOne" style="padding-bottom: 0rem">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <h6 class="font-weight-bold">Ringkasan Akun Laba Rugi</h6>
                                                </div>
                                                <div class="col-md-1 text-right">
                                                    <h5><button class="btn btn-default text-right" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="padding: 0rem">
                                                            <span class="iconify" data-icon="ep:arrow-down-bold"></span>
                                                        </button></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                                <table class="table table-custom" id="table-data-jurnal" width="100%" cellspacing="0">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center font-weight-bold">KATEGORI</th>
                                                        <th class="text-left font-weight-bold">AKUN KEUANGAN</th>
                                                        <th class="text-right font-weight-bold">DEBIT</th>
                                                        <th  class="text-right font-weight-bold">KREDIT</th>
                                                        <th class="text-right font-weight-bold">TOTAL</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php
                                                        $revenueTotal=0;
                                                         $costTotal=0;
                                                         $amountDebit=0;
                                                         $amountKredit=0;
                                                    @endphp
                                                    @if(!empty($accountClosing))
                                                        @foreach($accountClosing as $key =>$item)
                                                            <tr>
                                                                <td rowspan="{{($item->count)+1}}">{{ucfirst($item->key)}}</td>
                                                            </tr>
                                                            @foreach($item->child as $c)
                                                                <tr>

                                                                    <td>{{$c->parent_code}} - {{str_replace('.','',$c->code)}} {{$c->name}}</td>
                                                                    <td class="text-right">{{rupiah($c->amount_debit)}}</td>
                                                                    <td class="text-right">{{rupiah($c->amount_kredit)}}</td>
                                                                    <td class="text-right">
                                                                        @if($c->type_coa=='Debit')
                                                                            @if($c->amount_debit<$c->amount_kredit)

                                                                                ({{rupiah(abs($c->amount_debit-$c->amount_kredit))}})
                                                                             @else

                                                                                {{rupiah($c->amount_debit-$c->amount_kredit)}}

                                                                            @endif

                                                                                <input type="hidden" name="amount_detail[]" value="{{abs($c->amount_debit-$c->amount_kredit)}}">
                                                                                @if($c->amount_debit-$c->amount_kredit<0)
                                                                                    <input type="hidden" name="type_coa_detail[]" value="Kredit">
                                                                                @else
                                                                                    <input type="hidden" name="type_coa_detail[]" value="Debit">
                                                                                @endif


                                                                        @else

                                                                            @if($c->amount_kredit<$c->amount_debit)

                                                                                ({{rupiah(abs($c->amount_kredit-$c->amount_debit))}})
                                                                            @else

                                                                                {{rupiah($c->amount_kredit-$c->amount_debit)}}

                                                                            @endif
                                                                                <input type="hidden" name="amount_detail[]" value="{{abs($c->amount_kredit-$c->amount_debit)}}">
                                                                                @if($c->amount_kredit-$c->amount_debit<0)
                                                                                    <input type="hidden" name="type_coa_detail[]" value="Debit">
                                                                                @else
                                                                                    <input type="hidden" name="type_coa_detail[]" value="Kredit">
                                                                                @endif

                                                                        @endif

                                                                        <input type="hidden" name="coa_id_detail[]" value="{{$c->id}}">



                                                                        @if($c->type_of_category=='pendapatan')

                                                                            @if($c->type_coa=='Debit')
                                                                                @php
                                                                                  $revenueTotal+=$c->amount_debit-$c->amount_kredit;
                                                                                @endphp
                                                                             @else
                                                                                    @php
                                                                                        $revenueTotal+=$c->amount_kredit-$c->amount_debit;
                                                                                    @endphp
                                                                            @endif
                                                                        @else
                                                                                @if($c->type_coa=='Debit')
                                                                                    @php
                                                                                        $costTotal+=$c->amount_debit-$c->amount_kredit;
                                                                                    @endphp
                                                                                @else
                                                                                    @php
                                                                                        $costTotal+=$c->amount_kredit-$c->amount_debit;
                                                                                    @endphp
                                                                                @endif
                                                                        @endif

                                                                            @php
                                                                                $amountDebit+=$c->amount_debit;
                                                                                $amountKredit+=$c->amount_kredit;
                                                                            @endphp

                                                                    </td>

                                                                </tr>

                                                            @endforeach
                                                        @endforeach
                                                     @else
                                                    <tr>
                                                        <td colspan="5" class="text-center">Data tidak tersedia</td>
                                                    </tr>
                                                    @endif

                                                    </tbody>
                                                    <tfoot>
                                                    <tr style="background: #F6F6F6!important">
                                                        <td class="font-weight-bold" colspan="4">TOTAL LABA/RUGI</td>
                                                        <input type="hidden" id="amount_debit_total" value="@if($amountDebit<0) ({{rupiah($amountDebit)}}) @else {{rupiah($amountDebit)}} @endif">
                                                        <input type="hidden" id="amount_kredit_total" value="@if($amountKredit<0) ({{rupiah($amountKredit)}}) @else {{rupiah($amountKredit)}} @endif">
                                                        <td id="total_profit_lost" class="text-right text-orange"><b>
                                                                @if($revenueTotal-$costTotal<0)
                                                                    ({{rupiah($revenueTotal-$costTotal)}})
                                                                 @else
                                                                     {{rupiah($revenueTotal-$costTotal)}}
                                                                @endif
                                                            </b></td>
                                                    </tr>
                                                    </tfoot>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="card-header" id="headingTwo" style="padding-bottom: 0rem">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <h6 class="font-weight-bold">Informasi Akun Distribusi</h6>
                                                </div>
                                                <div class="col-md-1 text-right">
                                                    <h5><button class="btn btn-default text-right" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne" style="padding: 0rem">
                                                            <span class="iconify" data-icon="ep:arrow-down-bold"></span>
                                                        </button></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                                <table class="table table-custom" id="table-data-jurnal" width="100%" cellspacing="0">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-left font-weight-bold">NAMA AKUN</th>
                                                        <th class="text-left font-weight-bold">POSISI AKUN</th>
                                                        <th class="text-left font-weight-bold" width="25%">JUMLAH DISTRIBUSI</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(!empty($accountDistribution))
                                                        @foreach($accountDistribution as $d)
                                                            <tr>
                                                                <input type="hidden" name="coa_distribution_id[]" value="{{$d->id}}">
                                                                <input type="hidden" name="type_coa_distribution[]" value="{{$d->type_coa}}">
                                                                <td> {{$d->parent_code}} - {{str_replace('.','',$d->code)}} {{$d->name}}</td>
                                                                <td>{{ucfirst($d->type_coa)}}</td>
                                                                <td class="text-right">
                                                                    <div class="form-group">
                                                                     <input type="text" value="{{rupiah($d->getAmountCoaDistribution($d->id,$data->code))}}" class="form-control amount_currency" name="amount[]" required>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="3" class="text-center">Data tidak tersedia</td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2 text-right" style="margin-top: 40px">
                                @if($data->is_distribution==0 || $data->getDetail->count()<1)
                                <a href="{{route('merchant.toko.acc.journal.closing.add')}}?id={{$data->id}}&step=0" class="btn btn-light text-light mr-2">
                                    <i class="fa fa-arrow-circle-left mr-2"></i>
                                    Kembali</a>
                               @else
                                    <a href="{{route('merchant.toko.acc.journal.closing.index')}}" class="btn btn-light text-light mr-2">
                                        <i class="fa fa-arrow-circle-left mr-2"></i>
                                        Kembali</a>
                                @endif
                                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                                <input type='hidden' name='id' value='{{$data->id}}'>
                                <input type='hidden' name='step' value='{{$step}}'>
                                <input type='hidden' name='amount_debit_original' value='{{$amountDebit}}'>
                                <input type='hidden' name='amount_kredit_original' value='{{$amountKredit}}'>
                                <input type='hidden' name='end_amount_original' value='{{$revenueTotal-$costTotal}}'>
                                    <input type='hidden' name='md_merchant_id' value='{{$data->md_merchant_id}}'>

                                    <button class="btn btn-success" @if($accountClosing<1) disabled @endif onclick="saveDistribution()"><i class="fa fa-save mr-2"></i> Simpan</button>
                            </div>
                        </form>
                    </div>

                @endif
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.amount_currency').mask("#.##0,00", {reverse: true});
            $("#saldo_debit").text($("#amount_debit_total").val())
            $("#saldo_kredit").text($("#amount_kredit_total").val())
            $("#end_saldo").html($("#total_profit_lost").text())
            $('#btn-save').on('click', function(){
                $('#btn-save-hidden').click();
            })
            $("#branch").select2()
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');
                ajaxTransfer("{{route('merchant.toko.acc.journal.closing.save')}}", data,
                    function (response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                    });
            })
        })
    </script>

@endsection

<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        @include('merchant::component.branch-selection',
    ['withoutModal'=>true,
    'data'=>$data,
    'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
    'selector'=>'branch-ap',
    'with_onchange'=>true,

    ])
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Kode Transaksi</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{is_null($data->second_code)?$data->ar_code:$data->second_code}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Nama Transaksi</label>
                <input type="text" class="form-control form-control-sm" name="trans_name" placeholder="Nama Transaksi" value="{{$data->ar_name}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Jenis Piutang</label>
                @if ($data->trans_purpose==null)
                    @if(is_null($data->id))
                        <select name="acc_coa_category_id" id="selectAp" class="form-control form-control-sm">

                        </select>
                    @else
                        <select name="acc_coa_category_id" id="selectAp" class="form-control form-control-sm" disabled>
                            @foreach($categoryOption as $key =>$item)
                                <option value="{{$item->acc_coa_detail_id}}" {{($item->id==$data->getCoa->acc_coa_detail_id)?'selected':'' }}>{{$item->text}}</option>
                            @endforeach
                        </select>
                    @endif
                    @else
                    <input type="text" class="form-control form-control-sm acc_coa_category_id" name="acc_coa_category_id" value="{{$data->acc_coa_category_id}}" disabled>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Diberikan Kepada</label>
                @if ($data->trans_purpose==null)
                    @if(is_null($data->id))
                        <select name="trans_purpose" id="trans_purpose" class="form-control form-control-sm" required>

                        </select>
                    @else
                        <select name="trans_purpose" class="form-control form-control-sm" disabled>
                            <option value="{{$data->ref_user_id}}" selected>
                                {{$data->ar_purpose}}
                            </option>
                        </select>
                    @endif

                @else
                    <input type="text" class="form-control form-control-sm trans_purpose" name="trans_purpose" value="{{$data->trans_purpose}}" disabled>
                @endif
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Waktu Transaksi</label>
                <input type="text" class="form-control form-control-sm trans_time" name="trans_time" value="{{$data->ar_time}}" placeholder="Waktu Transaksi" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Jatuh Tempo</label>
                <input type="text" class="form-control form-control-sm trans_time" name="due_date" value="{{$data->due_date}}" placeholder="Jatuh Tempo" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Bukti Transaksi</label>
                <input type="file" class="form-control"  name="trans_proof">
                <small style="color: red">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</small>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Catatan</label>
                <textarea name="trans_note" class="form-control form-control-sm">{{$data->ar_note}}</textarea>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' id='id' name='id' value='{{$data->id }}'>
    <input type='hidden' name='type' id="type" value='{{!is_null($data->is_from_supplier)?$data->is_from_supplier:0}}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>


<script>

    $(document).ready(function () {
        getCustomer()
        dateTimePicker(".trans_time");
        $("#selectAp").select2();
        if(!$("#id").val()){
                onchangeBranch()
            }
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.acc.journal.ar.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })

    })

    function getCustomer() {
        $("#trans_purpose").select2({
            ajax: {
                type: "GET",
                url: "{{route('merchant.toko.transaction.sale-order.contact-list')}}",
                dataType: 'json',
                delay: 250,
                headers: {
                    "senna-auth": "{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            placeholder:"--Pilih Penerima --"

        });
        //harusnya name
    }

    $("#trans_purpose").on('select2:select', function (e) {
            var data = e.params.data;

            $('#type').val(data.type)
        });

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah transaksi piutang, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    function onchangeBranch()
    {
        let branch = $("#branch-ap").val()
        $("#selectAp").empty()
        $.ajax({
            type: "GET",
            url: "{{route('merchant.toko.acc.journal.ar.ar-list')}}?md_merchant_id="+branch,
            dataType: 'json',
            success: function(result){

                $("#selectAp").select2({
                    data:result
                });

            }});
    }


</script>

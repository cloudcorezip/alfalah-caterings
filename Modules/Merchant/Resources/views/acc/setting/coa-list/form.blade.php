<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Nama Akun</label>
                <input type="name" class="form-control form-control-sm" name="name" placeholder="Nama Akun" value="{{$data->name}}" @if(!is_null($data->id))@if($data->is_deleted==0) disabled @endif @endif>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Kategori Akun</label>
                <select class="form-control form-control-sm" id="coa-category" name="acc_coa_category_id" @if(!is_null($data->id))@if($data->is_deleted==0) disabled @endif @endif>
                    @foreach($categoryOption as $key =>$item)
                        @if($item->code=='5.0.00' && $item->code=='4.0.00')
                        <option value="{{$item->id}}"  {{($item->id==$data->acc_coa_category_id)?'selected':''}}>{{$item->parent_code}} - {{str_replace('.','',$item->code)}} {{$item->name}}</option>
                        @endif
                    @foreach($item->getChild as $c)
                            @if($c->getChild->count()<1)
                            <option value="{{$c->id}}"  {{($c->id==$data->acc_coa_category_id)?'selected':''}}>{{$c->parent_code}} - {{str_replace('.','',$c->code)}} {{$c->name}}</option>
                            @endif
                        @foreach($c->getChild as $cc)
                                <option value="{{$cc->id}}"  {{($cc->id==$data->acc_coa_category_id)?'selected':''}}>{{$cc->parent_code}} - {{str_replace('.','',$cc->code)}} {{$cc->name}}</option>
                            @endforeach
                        @endforeach
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Tipe Saldo</label>
                <select class="form-control form-control-sm" name="type_coa" id="type_coa" @if(!is_null($data->id))@if($data->is_deleted==0) disabled @endif @endif required>
                    <option value="Debit" {{($data->type_coa=='Debit')?'selected':'' }}>Debit</option>
                    <option value="Kredit" {{($data->type_coa=='Kredit')?'selected':'' }}>Kredit</option>
                </select>
            </div>
        </div>

    </div>







    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-sm btn-success py-2 px-4">Simpan</button>

    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.acc.setting.coa-list.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })

        $('#coa-category').select2();
        $('#currency').select2();
        $('#type_coa').select2();
    })

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah daftar akun, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });




</script>

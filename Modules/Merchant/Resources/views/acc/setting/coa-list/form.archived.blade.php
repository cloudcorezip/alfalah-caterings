<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Nama Akun</label>
                <input type="name" class="form-control form-control-sm" name="name" placeholder="Nama Akun" value="{{$data->name}}" @if(!is_null($data->id))@if($data->is_deleted==0) disabled @endif @endif>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Kategori Akun</label>
                <select class="form-control form-control-sm" id="coa-category" name="acc_coa_category_id" @if(!is_null($data->id))@if($data->is_deleted==0) disabled @endif @endif>
                    @foreach($categoryOption as $key =>$item)
                        @if($item->code=='5.0.00' && $item->code=='4.0.00')
                            <option value="{{$item->id}}"  {{($item->id==$data->acc_coa_category_id)?'selected':''}}>{{$item->parent_code}} - {{str_replace('.','',$item->code)}} {{$item->name}}</option>
                        @endif
                        @foreach($item->getChild as $c)
                            @if($c->getChild->count()<1)
                                <option value="{{$c->id}}"  {{($c->id==$data->acc_coa_category_id)?'selected':''}}>{{$c->parent_code}} - {{str_replace('.','',$c->code)}} {{$c->name}}</option>
                            @endif
                            @foreach($c->getChild as $cc)
                                <option value="{{$cc->id}}"  {{($cc->id==$data->acc_coa_category_id)?'selected':''}}>{{$cc->parent_code}} - {{str_replace('.','',$cc->code)}} {{$cc->name}}</option>
                            @endforeach
                        @endforeach
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Tipe Saldo</label>
                <select class="form-control form-control-sm" name="type_coa" id="type_coa" @if(!is_null($data->id))@if($data->is_deleted==0) disabled @endif @endif required>
                    <option value="Debit" {{($data->type_coa=='Debit')?'selected':'' }}>Debit</option>
                    <option value="Kredit" {{($data->type_coa=='Kredit')?'selected':'' }}>Kredit</option>
                </select>
            </div>
        </div>

        @if(is_null($data->id))
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="font-weight-bold">Masukkan Pada Laporan Arus Kas ? </label>
                    <div class="col-md-2">
                        <input value="0" type="hidden" name="is_include_cashflow" id="cashflow"><label>
                            <input type="checkbox" class="form-control" id="is_include_cashflow"></label>
                    </div>
                </div>
            </div>

            <div class="col-md-6 show-hide-cashflow">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="font-weight-bold">Tipe Arus Kas</label>
                    <select class="form-control form-control-sm" name="type_cashflow" id="type_cashflow" onchange="getListTypeCashflowDetail()">
                        <option value="-1">Pilih Tipe Arus Kas</option>
                        <option value="operasional">Operasional</option>
                        <option value="investasi">Investasi</option>
                        <option value="pendanaan">Pendanaan</option>
                    </select>
                </div>
            </div>

            <div class="col-md-6 show-hide-cashflow">
                <div class="form-group">
                    <label for="exampleInputPassword1" class="font-weight-bold">Tipe Format Arus Kas</label>
                    <select class="form-control form-control-sm" name="type_cashflow_format" id="type_cashflow_format">
                    </select>
                </div>
            </div>

        @else
            @if($data->is_deleted==2)
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="font-weight-bold">Masukkan Pada Laporan Arus Kas ? </label>
                        <div class="col-md-2">
                            <input value="{{!is_null($checkCashflowFormatDetails)?1:0}}" type="hidden" name="is_include_cashflow" id="cashflow"><label>
                                <input type="checkbox" {{!is_null($checkCashflowFormatDetails)?'checked':''}} id="is_include_cashflow" class="form-control"></label>
                        </div>
                    </div>
                </div>
                @if(!is_null($checkCashflowFormatDetails))
                    <div class="col-md-6 show-hide-cashflow">
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="font-weight-bold">Tipe Arus Kas</label>
                            <select class="form-control form-control-sm" name="type_cashflow" id="type_cashflow"  onchange="getListTypeCashflowDetail()">
                                <option value="-1">Pilih Tipe Arus Kas</option>
                                <option value="operasional"@if($checkCashflowFormatDetails->getCashflowFormat->type=='operasional') selected @endif>Operasional</option>
                                <option value="investasi" @if($checkCashflowFormatDetails->getCashflowFormat->type=='investasi') selected @endif>Investasi</option>
                                <option value="pendanaan" @if($checkCashflowFormatDetails->getCashflowFormat->type=='pendanaan') selected @endif>Pendanaan</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6 show-hide-cashflow">
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="font-weight-bold">Tipe Format Arus Kas</label>
                            <select class="form-control form-control-sm" name="type_cashflow_format" id="type_cashflow_format">
                                <option value="{{$checkCashflowFormatDetails->acc_coa_cashflow_format_id}}" selected>{{\Modules\Merchant\Http\Controllers\Acc\Report\CashflowController::replaceNameOfReport($checkCashflowFormatDetails->getCashflowFormat->name)}}</option>
                            </select>
                        </div>
                    </div>
                @else
                    <div class="col-md-6 show-hide-cashflow">
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="font-weight-bold">Tipe Arus Kas</label>
                            <select class="form-control form-control-sm" name="type_cashflow" id="type_cashflow" onchange="getListTypeCashflowDetail()">
                                <option value="-1">Pilih Tipe Arus Kas</option>
                                <option value="operasional">Operasional</option>
                                <option value="investasi">Investasi</option>
                                <option value="pendanaan">Pendanaan</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6 show-hide-cashflow">
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="font-weight-bold">Tipe Format Arus Kas</label>
                            <select class="form-control form-control-sm" name="type_cashflow_format" id="type_cashflow_format">
                            </select>
                        </div>
                    </div>

                @endif
            @endif
        @endif


    </div>







    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-sm btn-success py-2 px-4">Simpan</button>

    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {

        @if(!is_null($data->id))
        @if(!is_null($checkCashflowFormatDetails))
        $(".show-hide-cashflow").attr("hidden",false);

        @else
        $(".show-hide-cashflow").attr("hidden",true);
        @endif

        @else
        $(".show-hide-cashflow").attr("hidden",true);
        @endif

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.acc.setting.coa-list.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })

        $('#coa-category').select2();
        $('#currency').select2();
        $('#type_coa').select2();
        $("#type_cashflow").select2()
        $("#type_cashflow_format").select2();

    })

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah daftar akun, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


    $("#is_include_cashflow").change(function() {
        if(this.checked) {
            $("#cashflow").val(1)
            $(".show-hide-cashflow").removeAttr("hidden");

        }else{
            $("#cashflow").val(0)
            $(".show-hide-cashflow").attr("hidden",true);

        }
    });

    function getListTypeCashflowDetail()
    {
        let cashflow =$("#type_cashflow").val();
        $('#type_cashflow_format').val(null).trigger('change');
        $("#type_cashflow_format").select2({
            ajax: {
                type: "GET",
                url: "{{route('merchant.toko.acc.setting.coa-list.ajax-type-cashflow-format')}}?type="+cashflow+"",
                dataType: 'json',
                delay: 250,
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }



</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.acc.setting.coa-list.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kumpulan akun yang diperlukan untuk pencatatan keuangan. Kamu juga bisa menambahkan akun lain untuk mempermudah pencatatan keuangan.</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')

        <div class="card shadow mb-4">
            <div class="card-body">
                {!! $add !!}
                <div class="table-responsive">
                    <table class="table table-custom" id="table-data" width="100%" cellspacing="0">

                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Akun</th>
                            <th>Saldo Normal</th>
                            <th>Kategori</th>
                            <th>Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($datas as $key =>$item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td><b>{{$item->getCategory->parent_code}} - {{str_replace('.','',$item->code)}}</b> {{$item->name}}</td>
                                <td>{{$item->type_coa}}</td>
                                <td><b>{{$item->getCategory->name}}</b></td>
                                <td>
{{--                                    @if($item->is_deleted==0)--}}
{{--                                        <a  onclick='loadModal(this)' target='{{route('merchant.toko.acc.setting.coa-list.add',['id'=>$item->id])}}' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>--}}
{{--                                            <span class='fa fa-edit' style='color: white'></span>--}}
{{--                                        </a>--}}
{{--                                    @endif--}}
                                    @if($item->is_deleted==2)
                                        <a  onclick='loadModal(this)' target='{{route('merchant.toko.acc.setting.coa-list.add',['id'=>$item->id])}}' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                            <span class='fa fa-edit' style='color: white'></span>
                                        </a>
                                        <a onclick='deleteData({{$item->id}})' class='btn btn-xs btn-delete-xs btn-rounded'>
                                            <span class='fa fa-trash-alt' style='color: white'></span>
                                        </a>

                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.acc.setting.coa-list.delete')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                });
            })
        }

        $( document ).ready(function() {
            $("#table-data").DataTable({
                iDisplayLength: 10,
                language: {
                    processing: '<div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div> ',
                    paginate: {
                        "previous": '<i class="ti-angle-left"></i>',
                        "next": '<i class="ti-angle-right"></i>'
                    },
                    info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    lengthMenu: "Tampilkan _MENU_ entri",
                    infoEmpty: "Menampilkan 0 sampai 0 dari 0 entri",
                },
            });

        });


    </script>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: Arial, sans-serif; width:100%;margin-right:auto;margin-left:auto">
    <div style="width:100%; margin:0 auto;">
        <div style="background:#FAFAFA;padding:30px;">
            <div style="width:50%;float:left;">
                <img src="{{env('S3_URL')}}public/frontend/img/logo.png" alt="logo" style="width:36%;">
                <h4 style="color:#707070;" class="arial-rounded">Senna Indonesia</h4>
                <p style="color:#707070;font-size:12px;">Sekarsuli-Berbah No.9, Potorono, Banguntapan, Bantul Regency, Special Region of Yogyakarta</p>
            </div>
            <div style="width:50%;text-align:right;float:right;">
                <p style="color:#707070;font-size:12px;">Email : contact@senna.co.id</p>
                <p style="color:#707070;font-size:12px;">Website : senna.co.id</p>
                <p style="color:#707070;font-size:12px;">Phone : (+62)813-2566-6958</p>
            </div>
            <div style="clear:both;"></div>
        </div>
        <br>
        <div style="padding:0 30px 0 30px;">
            <div style="width:50%;float:left;">
                <h3 style="color:#707070;" class="arial-rounded">INVOICE</h3>
                <p style="color:#979797;font-size:14px;font-weight:bold;">{{$data->getSubscription->name}}</p>
            </div>
            <div style="width:50%;text-align:right;float:right;">
                @if($data->transaction->md_sp_transaction_status_id == 1)
                    <h2 style="color:#FF9E0D;">PENDING</h2>
                @endif
                @if($data->transaction->md_sp_transaction_status_id == 2)
                    <h2 style="color:#62AC29;">SUCCESS</h2>
                @endif
                @if($data->transaction->md_sp_transaction_status_id == 3)
                    <h2 style="color:#FF9E0D;">CANCELED</h2>
                @endif
                @if($data->transaction->md_sp_transaction_status_id == 4)
                    <h2 style="color:#FF9E0D;">FAILED</h2>
                @endif
                @if($data->transaction->md_sp_transaction_status_id == 5)
                    <h2 style="color:#FF9E0D;">REFUND</h2>
                @endif
            </div>
            <div style="clear:both;"></div>
        </div>
        
        <div style="padding:0 30px 0 30px;">
            <div style="width:50%;float:left;">
                <p style="color:#707070;font-size:12px;"><strong>Date </strong>{{$created_at}}</p>
            </div>
            <div style="width:50%;text-align:right;float:right;">
                <p style="color:#707070;font-size:12px;"><strong>Invoice No.</strong> {{$data->transaction->code}}</p>
            </div>
            <div style="clear:both;"></div>
            <br>
            <hr style="color:#E1E1E1;">
        </div>

        <div style="padding:0 30px 0 30px;">
            <div style="width:50%;float:left;">
                <h4 style="color:#FF9743;">BILL FROM :</h4>
                <h5 style="color:#707070;">Senna Indonesia</h5>
                <p style="color:#707070;font-size:12px;">Sekarsuli-Berbah No.9, Potorono, Banguntapan, Bantul Regency,<br>Special Region of Yogyakarta</p>
                <p style="color:#707070;font-size:12px;">(+62)813-2566-6958</p>
                <p style="color:#707070;font-size:12px;">contact@senna.co.id</p>
            </div>
            <div style="width:50%;float:left;">
                <h4 style="color:#62AC29;">TO :</h4>
                <h5 style="color:#707070;">{{ucwords($data->getMerchant->name)}}</h5>
                <p style="color:#707070;font-size:12px;">{{is_null($data->getMerchant->address)? '':$data->getMerchant->address}}</p>
                <p style="color:#707070;font-size:12px;">{{is_null($data->getMerchant->phone_merchant)? '':$data->getMerchant->phone_merchant}}</p>
                <p style="color:#707070;font-size:12px;">{{$user->email}}</p>
            </div>
            <div style="clear:both;"></div>
        </div>
        <br>
        <div style="padding:0 30px 0 30px;">
            <table width="100%" cellspacing="0">
                <tr style="background:#F9F9F9;">
                    <th style="padding:0 10px;">
                        <p style="color:#A4A4A4; font-weight:600;font-size:12px; text-align:left;">PACKAGE</p>
                    </th>
                    <th>
                        <p style="color:#A4A4A4; font-weight:600;font-size:12px;text-align:center;">PRICE</p>
                    </th>
                    <th>
                        <p style="color:#A4A4A4; font-weight:600;font-size:12px;text-align:center;">DURATION</p>
                    </th>
                    <th style="padding:0 10px;">
                        <p style="color:#A4A4A4; font-weight:600;font-size:12px; text-align:right;">STATUS</p>
                    </th>
                </tr>
                <tr>
                    <td style="color:#707070;font-size:12px;padding:20px 10px;">{{$data->getSubscription->name}}</td>
                    <td style="color:#707070;font-size:12px;text-align:center;padding:20px 0;">{{rupiah($data->getSubscription->price)}}</td>
                    <td style="color:#707070;font-size:12px;text-align:center;padding:20px 0;">{{$duration}}</td>
                    <td style="color:#707070;font-size:12px;padding:20px 10px;text-align:right;">
                        @if($data->transaction->md_sp_transaction_status_id == 1)
                            <i style="color:#FF9E0D;">PENDING</i>
                        @endif
                        @if($data->transaction->md_sp_transaction_status_id == 2)
                            <i style="color:#7EB056;">SUCCESS</i>
                        @endif
                        @if($data->transaction->md_sp_transaction_status_id == 3)
                            <i style="color:#FF9E0D;">CANCELED</i>
                        @endif
                        @if($data->transaction->md_sp_transaction_status_id == 4)
                            <i style="color:#FF9E0D;">FAILED</i>
                        @endif
                        @if($data->transaction->md_sp_transaction_status_id == 5)
                            <i style="color:#FF9E0D;">REFUND</i>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        Diskon :
                    </td>
                    <td style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        {{rupiah($discount)}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        Biaya Admin :
                    </td>
                    <td style="text-align:right;color:#707070;font-size:12px;padding:10px 0;">
                        {{rupiah($admin)}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:right;font-weight:bold;color:#707070;padding:10px 0;">
                        Invoice Total :
                    </td>
                    <td style="text-align:right;padding:0 10px;color:#FF8929;padding:10px 0;font-weight:bold;">
                        {{rupiah($price)}}
                    </td>
                </tr>
            </table>
        </div>





    </div>

</body>
</html>
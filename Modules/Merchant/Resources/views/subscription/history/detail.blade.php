@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<link rel="stylesheet" href="{{asset('public/backend/css/custom.css')}}">
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid d-flex justify-content-between align-items-center">
        <div class="p-5">
            <h4 class="mb-1">{{$title}}</h4>
        </div>
    </div>
</div>

<div class="container">
    <div class="card shadow mb-4">
        <div class="card-body p-4">
            <div class="d-flex justify-content-between p-50" style="background:#FAFAFA;">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{env('S3_URL')}}public/frontend/img/logo.png" alt="logo" class="img-fluid mb-4" style="width:60%;">
                        <h6 style="color:#707070;" class="arial-rounded">Senna Indonesia</h6>
                        <span class="d-block text-span-desc">Banguntapan, Yogyakarta</span>
                        <span class="text-span-desc">23 November 2021</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <span class="d-block text-span-desc">Legal Registration No : 3422</span>
                        <span class="d-block text-span-desc">Email : contact@senna.co.id</span>
                        <span class="d-block text-span-desc">Web : sennakasir.co.id</span>
                        <span class="d-block text-span-desc">Phone : +89 2329 33232 </span>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between p-t-50 p-l-50 p-r-50">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="color:#707070;" class="arial-rounded">INVOICE</h3>
                        <span class="d-block text-span-desc">Paket Nakula 3 Bulan</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <h2 style="color:#62AC29;" class="arial-rounded">LUNAS</h2>
                    </div>
                </div>
            </div>
            <div class="row p-l-50 p-r-50">
                <div class="col-md-4">
                    <span class="d-block text-span-desc">
                        <strong>Date</strong> 23/10/2021
                    </span>
                </div>
                <div class="col-md-4">
                    <span class="d-block text-span-desc text-center">
                        <strong>Due Date</strong> 23/10/2021
                    </span>
                </div>
                <div class="col-md-4">
                    <span class="d-block text-span-desc text-right">
                        <strong>Invoice No.</strong> #2333234
                    </span>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
            </div>
            
            <div class="row p-l-50 p-r-50 pt-2">
                <div class="col-md-6">
                    <h4 class="text-orange arial-rounded">BILL FROM</h4>
                    <h5 style="color:#707070;" class="arial-rounded">Senna Indonesia</h5>
                    <span class="d-block text-span-desc">Banguntapan, Yogyakarta</span>
                    <span class="d-block text-span-desc">+89 2329 33232 </span>
                    <span class="d-block text-span-desc">contact@senna.co.id</span>
                </div>
                <div class="col-md-6">
                    <h4 style="color:#62AC29;" class="arial-rounded">TO FROM</h4>
                    <h5 style="color:#707070;" class="arial-rounded">Chicken Crush</h5>
                    <span class="d-block text-span-desc">Banguntapan, Yogyakarta</span>
                    <span class="d-block text-span-desc">+89 2329 33232 </span>
                    <span class="d-block text-span-desc">contact@senna.co.id</span>
                </div>
            </div>

            <div class="row p-50">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" width="100%" cellspacing="0">
                            <thead style="background:#F9F9F9;">
                                <tr>
                                    <th style="color:#A4A4A4; font-weight:600;">PACKAGE</th>
                                    <th style="color:#A4A4A4; font-weight:600;" class="text-center">PRICE</th>
                                    <th style="color:#A4A4A4; font-weight:600;" class="text-center">DURATION</th>
                                    <th style="color:#A4A4A4; font-weight:600;" class="text-right">STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="border-0">Nakula</td>
                                    <td class="text-center border-0">Rp. 99.000,00</td>
                                    <td class="text-center border-0">3 Bulan</td>
                                    <td class="text-right border-0" style="color:#7EB056;"><i>Sudah Dibayar</i></td>
                                </tr>
                                <tr>
                                    <td class="text-right border-0" colspan="4">
                                        <h6 class="arial-rounded">Invoice Total :</h6>
                                        <h3 class="arial-rounded text-orange">Rp. 99.000,00</h3>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row p-50">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between">
                        <a class="btn btn-light py-2" href="">Kembali</a>
                        <button class="inv-btn__checkout py-2">Cetak & Lihat</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const loadTableView = () => {
        const descLength = $('#data-description ul li').length;
        let checkHtml = "<ul>";
        let stripHtml = "<ul>";

        for(let i = 0; i < descLength; i++){
            checkHtml += "<li><i class='fa fa-check-circle' style='color:#62AC29;'></i></li>";
            stripHtml += "<li style='color:#707070;'>-</li>";
        }

        checkHtml += "</ul>";
        stripHtml += "</ul>";
        $('#check-description').html(checkHtml);
        $('#strip-description').html(stripHtml);
    }

    $(document).ready(function(){
        loadTableView();
    });
</script>

@endsection

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid d-flex justify-content-between align-items-center">
        <div class="p-5">
            <h4 class="mb-1">{{$title}}</h4>
            <span>Disini kamu bisa mengakses fitur layanan berlangganan apabila masa langganan kamu sudah habis.</span>
        </div>
    </div>
</div>

<div class="container-fluid">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link active" href="{{route('subscription.index')}}">Layanan Berlangganan</a>
        </li>
        <li class="nav-item">
            <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link" href="{{route('plugin.index')}}">Layanan Integrasi</a>
        </li>
        <li class="nav-item">
            <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link" href="{{route('subscription.history.index')}}">Riwayat Pembelian Layanan</a>
        </li>
    </ul>
    <div class="card shadow mb-4">
        <div class="card-body">
            <h4 class="font-weight-bold mb-4">{{$title}}</h4>
            <div class="row">
                @foreach($data as $key => $item)
                <div class="col-lg-4 col-md-6 col-sm-12 mb-5">
                    <div class="card card-sub px-3 h-100">
                        <div class="card-body">
                            <h2 class="card-sub__title mb-3 poppin-med">{{$item->name}}</h2>
                            @if($item->discount_percentage > 0)
                                @php
                                    $priceAfterDiscount = $item->price - $item->discount_percentage / 100 * $item->price
                                @endphp
                                <h2 class="card-sub__price arial-rounded">
                                    {{rupiah($priceAfterDiscount)}}
                                </h2>
                                <span class="card-sub__price__discount arial-rounded"><s>{{rupiah($item->price)}}</s></span>
                            @else
                                <h2 class="card-sub__price arial-rounded">
                                    {{rupiah($item->price)}}
                                </h2>
                            @endif
                            <div class="card-sub__description mt-3">
                                {!!$item->description!!}
                            </div>
                        </div>
                        <div class="card-footer py-3 text-right border-0">
                            {{--- @if($paymentSubscription->contains('md_subscription_id', $item->id))
                                @php
                                    $payment = $paymentSubscription->where('md_subscription_id', $item->id)
                                                ->where('is_expired', 0)
                                                ->first();
                                @endphp
                                @if($payment->transaction->md_sp_transaction_status_id == 1)
                                    <a class="card-sub__btn__buy py-2 px-4" href="{{route('subscription.invoice', ['key' => encrypt($payment->id)])}}">BELI</a>
                                @else
                                    <form onsubmit="return false;" class="form-konten" id="form-{{$item->id}}">
                                        <input type="hidden" name="md_subscription_id" value="{{$item->id}}">
                                        <input type="hidden" name="csrf_token" value="{{csrf_token()}}">
                                        <button type="submit" class="card-sub__btn__buy py-2 px-4">BELI</button>
                                    </form>        
                                @endif
                            @else
                                <form onsubmit="return false;" class="form-konten" id="form-{{$item->id}}">
                                    <input type="hidden" name="md_subscription_id" value="{{$item->id}}">
                                    <input type="hidden" name="csrf_token" value="{{csrf_token()}}">
                                    <button type="submit" class="card-sub__btn__buy py-2 px-4">BELI</button>
                                </form>
                            @endif ---}}
                            <form onsubmit="return false;" class="form-konten" id="form-{{$item->id}}">
                                <input type="hidden" name="md_subscription_id" value="{{$item->id}}">
                                <input type="hidden" name="csrf_token" value="{{csrf_token()}}">
                                <button type="submit" class="card-sub__btn__buy py-2 px-4">BELI</button>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>      
        </div>
    </div>
</div>

<script>
     $(document).ready(function () {
        $('.form-konten').submit(function () {
            var data = getFormData($(this).attr('id'));
            ajaxTransfer("{{route('subscription.create-invoice')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
           
        })
    });
</script>

@endsection

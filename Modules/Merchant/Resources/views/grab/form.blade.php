@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">{{$title}}</h4>
                <span>Akun Grabfood Merchant</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="card shadow mb-4 px-4 grab-card__wrapper">
                    <div class="card-body">
                        <h4 class="font-weight-bold mb-4 text-center">Akun Grab Merchant</h4>
                        <ul class="grab-list__step">
                            <li class="{{(!is_null($step)) ?'active':''}}">
                                @if(!is_null($step))
                                    @if($step < 8)
                                        <a href="{{route('grab.activation.create')}}">
                                            <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                            <span>Data Pendaftar</span>
                                        </a>
                                    @else
                                        <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                        <span>Data Pendaftar</span>
                                    @endif
                                @else
                                    <div class="grab-list__step__nonactive mr-3">1</div>
                                    <span>Data Pendaftar</span>
                                @endif
                                
                            </li>
                            <li class="{{(!is_null($step) && $step >2) ?'active':''}}">
                                @if(!is_null($step) && $step >2)
                                    @if($step < 8)
                                        <a href="{{route('grab.activation.create', ['step'=>2])}}">
                                            <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                            <span>Data Pemilik</span>
                                        </a>
                                    @else
                                        <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                        <span>Data Pemilik</span>
                                    @endif
                                @else
                                    <div class="grab-list__step__nonactive mr-3">2</div>
                                    <span>Data Pemilik</span>
                                @endif
                                
                            </li>
                            <li class="{{(!is_null($step) && $step >3) ?'active':''}}">
                                @if(!is_null($step) && $step >3)
                                    @if($step < 8)
                                        <a href="{{route('grab.activation.create', ['step'=>3])}}">
                                            <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                            <span>Data Usaha</span>
                                        </a>
                                    @else
                                        <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                        <span>Data Usaha</span>
                                    @endif
                                @else
                                    <div class="grab-list__step__nonactive mr-3">3</div>
                                    <span>Data Usaha</span>
                                @endif
                                
                            </li>
                            <li class="{{(!is_null($step) && $step >4) ?'active':''}}">
                                @if(!is_null($step) && $step >4)
                                    @if($step < 8)
                                        <a href="{{route('grab.activation.create', ['step'=>4])}}">
                                            <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                            <span>Data Rekening Bank</span>
                                        </a>
                                    @else
                                        <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                        <span>Data Rekening Bank</span>
                                    @endif
                                @else
                                    <div class="grab-list__step__nonactive mr-3">4</div>
                                    <span>Data Rekening Bank</span>
                                @endif
                            </li>
                            <li class="{{(!is_null($step) && $step >5) ?'active':''}}">
                                @if(!is_null($step) && $step >5)
                                    @if($step < 8)
                                        <a href="{{route('grab.activation.create', ['step'=>5])}}">
                                            <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                            <span>Data Restoran dan Menu</span>
                                        </a>
                                    @else
                                        <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                        <span>Data Restoran dan Menu</span>
                                    @endif
                                @else
                                    <div class="grab-list__step__nonactive mr-3">5</div>
                                    <span>Data Restoran dan Menu</span>
                                @endif
                                
                            </li>
                            <li class="{{(!is_null($step) && $step >6) ?'active':''}}">
                                @if(!is_null($step) && $step >6)
                                    @if($step < 8)
                                        <a href="{{route('grab.activation.create', ['step'=>6])}}">
                                            <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                            <span>Data Jadwal Buka</span>
                                        </a>
                                    @else
                                        <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                        <span>Data Jadwal Buka</span>
                                    @endif
                                @else
                                    <div class="grab-list__step__nonactive mr-3">6</div>
                                    <span>Data Jadwal Buka</span>
                                @endif
                            </li>
                            <li class="{{(!is_null($step) && $step >7) ?'active':''}}">
                                @if(!is_null($step) && $step >7)
                                    @if($step < 8)
                                        <a href="{{route('grab.activation.create', ['step'=>7])}}">
                                            <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                            <span>Data Legalitas</span>
                                        </a>
                                    @else
                                        <img src="{{asset('public/backend/img/success-checklist.png')}}" class="mr-3" alt="checklist">
                                        <span>Data Legalitas</span>  
                                    @endif
                                @else
                                    <div class="grab-list__step__nonactive mr-3">7</div>
                                    <span>Data Legalitas</span>
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @if(is_null($step))
            <div class="col-lg-9 col-md-12">
                <div class="card shadow mb-4 px-4 grab-card__wrapper">
                    <div class="card-body">
                        <div id="result-form-konten"></div>
                        <h5 class="font-weight-bold mb-4">Data Pendaftar</h5>
                        <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            @if(!is_null($data))
                                <input type='hidden' name='id' value='{{$data->id}}'>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama Lengkap<span class="text-danger">*</span></label>
                                        <input 
                                            type="text" 
                                            name="registrant_name" 
                                            class="form-control" 
                                            placeholder="Nama Lengkap"
                                            value="{{!is_null($data)?$data->registrant_name:$user->fullname}}" 
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Email<span class="text-danger">*</span></label>
                                        <input 
                                            type="email" 
                                            name="registrant_email" 
                                            class="form-control" 
                                            placeholder="Email"
                                            value="{{!is_null($data)?$data->registrant_email:$user->email}}" 
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>No.Handphone<span class="text-danger">*</span></label>
                                        <input 
                                            type="text" 
                                            name="registrant_phone_number" 
                                            class="form-control" 
                                            placeholder="No.Handphone"
                                            
                                                @if(!is_null($data))
                                                    value="{{$data->registrant_phone_number}}"
                                                @elseif(!is_null($user->phone_number))
                                                    value="{{$user->phone_number}}"
                                                @else
                                                    value=""
                                                @endif
                                            
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-md-12 d-flex justify-content-end">
                                    @if(!is_null($data) && $data->registration_status !== 1)
                                        <a onclick="deleteHistory('{{$data->id}}')" class="btn btn-light mr-3" style="color:#747474;font-size:12px;">
                                            <span>Hapus Riwayat Pendaftaran</span> 
                                        </a>
                                    @endif
                                    <button class="btn btn-grab__submit d-flex align-items-center">
                                        <span class="mr-2">Simpan dan Lanjut</span> 
                                        <i class="fas fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif

            @if($step == 2)
            <div class="col-lg-9 col-md-12">
                <div class="card shadow mb-4 px-4 grab-card__wrapper">
                    <div class="card-body">
                        <div id="result-form-konten"></div>
                        <h5 class="font-weight-bold mb-4">Data Pemilik</h5>
                        <hr>
                        <form onsubmit="return false;" id="form-konten-2" class='form-konten' backdrop="">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Bentuk Badan Usaha</label>
                                        <select name="business_type_name" class="form-control" id="business_type_name" required>
                                            <option value="">--- Pilih Tipe Usaha ---</option>
                                            @foreach($type as $key => $item)
                                                <option 
                                                    @if(strtolower($data->business_type_name) == strtolower($item->name))
                                                        selected
                                                    @endif 
                                                    value="{{$item->name}}">
                                                    {{$item->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Nama Pemilik<span class="text-danger">*</span></label>
                                        <input 
                                            type="text" 
                                            name="owner_name"
                                            id="owner_name" 
                                            class="form-control" 
                                            placeholder="Nama Pemilik"
                                            value="{{!is_null($data->owner_name)? $data->owner_name: ''}}"
                                            required
                                        >
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" onchange="fillData(this, '#owner_name', '{{$data->registrant_name}}')">
                                        <label class="form-check-label">
                                            <small class="text-muted">Samakan dengan nama pendaftar</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Alamat Pemilik<span class="text-danger">*</span></label>
                                        <textarea name="owner_address" class="form-control" required>{{(!is_null($data->owner_address))? $data->owner_address:''}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No.KTP/Paspor Pemilik<span class="text-danger">*</span></label>
                                        <input 
                                            name="owner_identity_card_number" 
                                            type="text" 
                                            class="form-control" 
                                            placeholder="No.KTP/Paspor Pemilik"
                                            value="{{(!is_null($data->owner_identity_card_number))? $data->owner_identity_card_number:''}}" 
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="form-group">
                                        <label>No.Telephone Pemilik<span class="text-danger">*</span></label>
                                        <input 
                                            id="owner_phone_number" 
                                            name="owner_phone_number" 
                                            type="text" 
                                            class="form-control" 
                                            placeholder="No.Telephone Pemilik"
                                            value="{{(!is_null($data->owner_phone_number))? $data->owner_phone_number:''}}"  
                                            required
                                        >
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" onchange="fillData(this, '#owner_phone_number', '{{$data->registrant_phone_number}}')">
                                        <label class="form-check-label">
                                            <small class="text-muted">Samakan dengan No Telephone Pendaftar</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Email Pemilik<span class="text-danger">*</span></label>
                                        <input 
                                            type="email" 
                                            name="owner_email"
                                            id="owner_email" 
                                            class="form-control" 
                                            placeholder="Email Pemilik"
                                            value="{{!is_null($data->owner_email)? $data->owner_email: ''}}"
                                            required
                                        >
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" onchange="fillData(this, '#owner_email', '{{$data->registrant_email}}')">
                                        <label class="form-check-label">
                                            <small class="text-muted">Samakan dengan email pendaftar</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tanggal Lahir<span class="text-danger">*</span></label>
                                        <input 
                                            name="owner_birthdate" 
                                            type="date" 
                                            class="form-control" 
                                            placeholder="No.Telephone Pemilik"
                                            value="{{(!is_null($data->owner_birthdate))? $data->owner_birthdate:''}}" 
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kewarganegaraan<span class="text-danger">*</span></label>
                                        <select name="owner_citizenship" id="owner_citizenship">
                                            <option value="Indonesia">Indonesia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Posisi<span class="text-danger">*</span></label>
                                        <input 
                                            type="text" 
                                            name="job_level" 
                                            class="form-control" 
                                            placeholder="Contoh: Pemilik, Owner, Manajer"
                                            value="{{(!is_null($data->job_level))? $data->job_level:''}}" 
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-12 mb-2">
                                    <small class="text-muted mb-3">( maximum file size : 1mb, format : .png, .jpg, .jpeg)</small>
                                </div>
                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="owner_identity_file" 
                                        id="owner_identity_file" 
                                        style="display: none;"
                                        onchange="previewImage(this, '#owner_identity_file_preview')" 
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#owner_identity_file')">
                                        <span>Upload Foto KTP/Paspor</span>
                                        <div class="d-flex align-items-center" id="owner_identity_file_preview">
                                            @if(is_null($data->owner_file))
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @else
                                                <img src="{{env('S3_URL')}}{{json_decode($data->owner_file)->identity_card}}" alt="upload icon" class="upload-preview__image">
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="owner_identity_face" 
                                        id="owner_identity_face" 
                                        style="display: none;"
                                        onchange="previewImage(this, '#owner_identity_face_preview')" 
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#owner_identity_face')">
                                        <span>Upload Foto Selfie & KTP</span>
                                        <div class="d-flex align-items-center" id="owner_identity_face_preview">
                                            @if(is_null($data->owner_file))
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @else
                                                <img src="{{env('S3_URL')}}{{json_decode($data->owner_file)->identity_card_with_face}}" alt="upload icon" class="upload-preview__image">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 d-flex justify-content-end">
                                    <button class="btn btn-grab__submit d-flex align-items-center">
                                        <span class="mr-2">Simpan dan Lanjut</span> 
                                        <i class="fas fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif

            @if($step == 3)
            <div class="col-lg-9 col-md-12">
                <div class="card shadow mb-4 px-4 grab-card__wrapper">
                    <div class="card-body">
                        <div id="result-form-konten"></div>
                        <h5 class="font-weight-bold mb-4">Data Usaha</h5>
                        <hr>
                        <form onsubmit="return false;" id="form-konten-3" class='form-konten' backdrop="">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Email<span class="text-danger">*</span></label>
                                        <input 
                                            name="business_email" 
                                            id="business_email"
                                            type="email" 
                                            class="form-control" 
                                            placeholder="Email Usaha"
                                            value="{{(!is_null($data->business_email))? $data->business_email:''}}"
                                            required
                                        >
                                    </div>
                                    <div class="form-check">
                                        <input 
                                            class="form-check-input"
                                            type="checkbox" 
                                            onchange="fillData(this, '#business_email', '{{$data->registrant_email}}')"
                                        >
                                        <label class="form-check-label">
                                            <small class="text-muted">Samakan dengan email pendaftar</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>No.Handphone<span class="text-danger">*</span></label>
                                        <input 
                                            name="business_phone_number"
                                            id="business_phone_number"
                                            type="text" 
                                            class="form-control" 
                                            placeholder="No.Handphone"
                                            value="{{(!is_null($data->business_phone_number))? $data->business_phone_number:''}}" 
                                            required
                                        >
                                    </div>
                                    <div class="form-check">
                                        <input 
                                            class="form-check-input" 
                                            type="checkbox"
                                            onchange="fillData(this, '#business_phone_number', '{{$data->registrant_phone_number}}')" 
                                        >
                                        <label class="form-check-label">
                                            <small class="text-muted">Samakan dengan no handphone pendaftar</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Nama Toko Grabfood</label>
                                        <input 
                                            type="text" 
                                            class="form-control" 
                                            placeholder="Nama Toko Grabfood"
                                            value="{{(!is_null($data->grab_food_name))? $data->grab_food_name:$merchant->name}}"
                                            name="grab_food_name"
                                            disabled
                                        >
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Pilih Kategori Bisnis<span class="text-danger">*</span></label>
                                        <select name="business_category" class="form-control" id="business_category" required>
                                            <option value="">--- Pilih Kategori Bisnis ---</option>
                                            @foreach($category as $key => $item)
                                                <option 
                                                    @if(strtolower($data->business_category) == strtolower($item->name))
                                                    selected
                                                    @endif
                                                    value="{{$item->name}}"
                                                >
                                                        {{$item->name}}
                                                    </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Penjualan (IDR / Tahun)<span class="text-danger">*</span></label>
                                        <select class="form-control" id="business_scalable" name="business_scalable" required>
                                            <option value="">--- Pilih Skala Bisnis ---</option>
                                            <option 
                                                value="< 300 Juta (dibawah 300 Juta Rupiah)"
                                                @if(strtolower($data->business_scalable) == strtolower('< 300 Juta (dibawah 300 Juta Rupiah)'))
                                                selected
                                                @endif
                                            >
                                                < 300 Juta (dibawah 300 Juta Rupiah)
                                            </option>
                                            <option 
                                                value="300 Juta - 2.5 Miliar"
                                                @if(strtolower($data->business_scalable) == strtolower('300 Juta - 2.5 Miliar'))
                                                selected
                                                @endif
                                            >
                                                300 Juta - 2.5 Miliar
                                            </option>
                                            <option 
                                                value="2.5 Miliar - 50 Miliar"
                                                @if(strtolower($data->business_scalable) == strtolower('2.5 Miliar - 50 Miliar'))
                                                selected
                                                @endif
                                            >
                                                2.5 Miliar - 50 Miliar
                                            </option>
                                            <option
                                                value="> 50 Miliar (Lebih dari 50 Miliar)"
                                                @if(strtolower($data->business_scalable) == strtolower('> 50 Miliar (Lebih dari 50 Miliar)'))
                                                selected
                                                @endif
                                            >
                                                > 50 Miliar (Lebih dari 50 Miliar)
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Rata Rata Penjualan (IDR / Bulan)<span class="text-danger">*</span></label>
                                        <select class="form-control" id="business_revenue_average" name="business_revenue_average" required>
                                            <option value="">--- Pilih Pendapatan / Bulan ---</option>
                                            <option 
                                                value="< 50.000 (dibawah 50.000)"
                                                @if(strtolower($data->business_revenue_average) == strtolower('< 50.000 (dibawah 50.000)'))
                                                selected
                                                @endif
                                                
                                            >
                                                < 50.000 (dibawah 50.000)
                                            </option>
                                            <option 
                                                value="50.000 - 100.000"
                                                @if(strtolower($data->business_revenue_average) == strtolower('50.000 - 100.000'))
                                                selected
                                                @endif
                                            >
                                                50.000 - 100.000
                                            </option>
                                            <option 
                                                value="100.000 - 250.000"
                                                @if(strtolower($data->business_revenue_average) == strtolower('100.000 - 250.000'))
                                                selected
                                                @endif
                                            >
                                                100.000 - 250.000
                                            </option>
                                            <option 
                                                value="> 250.000 (diatas 250.000)"
                                                @if(strtolower($data->business_revenue_average) == strtolower('> 250.000 (diatas 250.000)'))
                                                selected
                                                @endif
                                            >
                                                > 250.000 (diatas 250.000)
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Alamat<span class="text-danger">*</span></label>
                                        <textarea name="business_address" class="form-control" required>{{(!is_null($data->business_address))? $data->business_address:''}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Link Google Map<span class="text-danger">*</span></label>
                                        <input 
                                            name="google_map_link" 
                                            type="text" 
                                            class="form-control" 
                                            placeholder="Link Google Map"
                                            value="{{(!is_null($data->google_map_link))? $data->google_map_link:''}}" 
                                            required
                                        >
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Negara<span class="text-danger">*</span></label>
                                        <input 
                                            name="countries_name"
                                            type="text" 
                                            class="form-control" 
                                            placeholder="Negara" 
                                            value="Indonesia" 
                                            disabled 
                                            required
                                        >
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Provinsi<span class="text-danger">*</span></label>
                                        <select name="province" class="form-control" id="province" required>
                                            <option value="">--- Pilih Provinsi ---</option>
                                            @foreach($province as $key => $item)
                                                <option 
                                                    value="{{$item->id}}"
                                                    @if(strtolower($item->name) == strtolower($data->province_name))
                                                    selected
                                                    @endif
                                                >
                                                    {{$item->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Kota<span class="text-danger">*</span></label>
                                        <select name="city" class="form-control" id="city" required>
                                            <option value="">--- Pilih Kota ---</option>
                                            @if(!is_null($data->city_name))
                                                <option selected value="{{$data->city_name}}">{{$data->city_name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Kecamatan<span class="text-danger">*</span></label>
                                        <select name="district" class="form-control" id="district" required>
                                            <option value="">--- Pilih Kota ---</option>
                                            @if(!is_null($data->district_name))
                                                <option selected value="{{$data->district_name}}">{{$data->district_name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Kode Pos<span class="text-danger">*</span></label>
                                        <input 
                                            name="postal_code" 
                                            type="text" 
                                            class="form-control" 
                                            placeholder="Kode Pos"
                                            value="{{(!is_null($data->postal_code))? $data->postal_code:''}}"  
                                            required
                                        >
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Jenis Lokasi<span class="text-danger">*</span></label>
                                        <select class="form-control" name="location_type" id="location_type" required>
                                            <option value="">--- Pilih Jenis Lokasi ---</option>
                                            <option 
                                                value="Ruko"
                                                @if(strtolower($data->location_type) == strtolower('Ruko'))
                                                selected
                                                @endif
                                            >
                                                Ruko
                                            </option>
                                            <option 
                                                value="Toko Pribadi"
                                                @if(strtolower($data->location_type) == strtolower('Toko Pribadi'))
                                                selected
                                                @endif
                                            >
                                                Toko Pribadi
                                            </option>
                                            <option 
                                                value="Kios Jalanan"
                                                @if(strtolower($data->location_type) == strtolower('Kios Jalanan'))
                                                selected
                                                @endif
                                            >
                                                Kios Jalanan
                                            </option>
                                            <option 
                                                value="Pasar Tradisional"
                                                @if(strtolower($data->location_type) == strtolower('Pasar Tradisional'))
                                                selected
                                                @endif
                                            >
                                                Pasar Tradisional
                                            </option>
                                            <option 
                                                value="Mall"
                                                @if(strtolower($data->location_type) == strtolower('Mall'))
                                                selected
                                                @endif
                                            >
                                                Mall
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label>Patokan Lokasi<span class="text-danger">*</span></label>
                                        <input 
                                            name="location_benchmark"
                                            type="text" 
                                            class="form-control" 
                                            placeholder="Contoh: Samping Masjid"
                                            value="{{(!is_null($data->location_benchmark))? $data->location_benchmark:''}}"
                                            required
                                        >
                                    </div>
                                </div>

                                <div class="col-12 mb-2">
                                    <small class="text-muted mb-3">( maximum file size : 1mb, format : .png, .jpg, .jpeg)</small>
                                    @php
                                        $outlet_foto = !is_null($data->outlet_foto) ? json_decode($data->outlet_foto):null;
                                    @endphp
                                </div>
                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="front_view_outlet" 
                                        id="front_view_outlet" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#front_view_preview')" 
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#front_view_outlet')">
                                        <span>Foto Tampak Depan Outlet <strong class="text-danger">*</strong></span>
                                        <div class="d-flex align-items-center" id="front_view_preview">
                                            @if(isset($outlet_foto->front_view_outlet))
                                                <img src="{{env('S3_URL')}}{{$outlet_foto->front_view_outlet}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="inside_view_outlet" 
                                        id="inside_view_outlet" 
                                        style="display: none;"
                                        onchange="previewImage(this, '#inside_view_preview')" 
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#inside_view_outlet')">
                                        <span>Foto Tampak Dalam Outlet <strong class="text-danger">*</strong></span>
                                        <div class="d-flex align-items-center" id="inside_view_preview">
                                            @if(isset($outlet_foto->inside_view_outlet))
                                                <img src="{{env('S3_URL')}}{{$outlet_foto->inside_view_outlet}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="side_view_outlet" 
                                        id="side_view_outlet" 
                                        style="display: none;"
                                        onchange="previewImage(this, '#side_view_preview')"  
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#side_view_outlet')">
                                        <span>Foto Tampak Samping Outlet <strong class="text-danger">*</strong></span>
                                        <div class="d-flex align-items-center" id="side_view_preview">
                                            @if(isset($outlet_foto->side_view_outlet))
                                                <img src="{{env('S3_URL')}}{{$outlet_foto->side_view_outlet}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="other_view_outlet" 
                                        id="other_view_outlet" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#other_view_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#other_view_outlet')">
                                        <span>Foto Lainnya Outlet</span>
                                        <div class="d-flex align-items-center" id="other_view_preview">
                                            @if(isset($outlet_foto->other_view_outlet))
                                                <img src="{{env('S3_URL')}}{{$outlet_foto->other_view_outlet}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 d-flex justify-content-end">
                                    <button class="btn btn-grab__submit d-flex align-items-center">
                                        <span class="mr-2">Simpan dan Lanjut</span> 
                                        <i class="fas fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif

            @if($step == 4)
            <div class="col-lg-9 col-md-12">
                <div class="card shadow mb-4 px-4 grab-card__wrapper">
                    <div class="card-body">
                        <div id="result-form-konten"></div>
                        <h5 class="font-weight-bold mb-4">Data Rekening Bank</h5>
                        <hr>
                        <form onsubmit="return false;" id="form-konten-4" class='form-konten' backdrop="">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama Bank<span class="text-danger">*</span></label>
                                        <select class="form-control" id="bank_name" name="bank_name" required>
                                            <option value="">--- Pilih Bank ---</option>
                                            @foreach($bank as $key => $item)
                                                <option 
                                                    value="{{$item->name}}"
                                                    @if(strtolower($item->name) == strtolower($data->bank_name))
                                                        selected
                                                    @endif
                                                >
                                                    {{$item->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nomer Rekening<span class="text-danger">*</span></label>
                                        <input 
                                            name="bank_number"
                                            id="bank_number"
                                            type="text" 
                                            class="form-control" 
                                            placeholder="Nomor Rekening"
                                            value="{{!is_null($data->bank_number)? $data->bank_number: ''}}" 
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama Pemilik Rekening<span class="text-danger">*</span></label>
                                        <input
                                            name="bank_account_name" 
                                            type="text"
                                            id="bank_account_name" 
                                            class="form-control" 
                                            placeholder="Nama Pemilik Rekening"
                                            value="{{!is_null($data->bank_account_name)? $data->bank_account_name: ''}}"  
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Cabang<span class="text-danger">*</span></label>
                                        <input
                                            name="bank_branch"
                                            id="bank_branch" 
                                            type="text" 
                                            class="form-control" 
                                            placeholder="Cabang"
                                            value="{{!is_null($data->bank_branch)? $data->bank_branch: ''}}"  
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-12 mb-2">
                                    <small class="text-muted mb-3">( maximum file size : 1mb, format : .png, .jpg, .jpeg)</small>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="bank_file" 
                                        id="bank_file" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#bank_file_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#bank_file')">
                                        <span>Cover Depan Buku Tabungan <strong class="text-danger">*</strong></span>
                                        <div class="d-flex align-items-center" id="bank_file_preview">
                                            @if(!is_null($data->bank_file))
                                                <img src="{{env('S3_URL')}}{{$data->bank_file}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <h5>Apakah nama pemilik rekening berbeda dengan nama pemilik toko?</h5>
                                            <small class="text-muted mb-3">Jika iya, Anda perlu surat kuasa</small>
                                        </div>
                                        <div class="col-md-2 d-flex align-items-center">
                                            <div class="custom-control custom-switch">
                                                <input 
                                                    type="checkbox" 
                                                    class="custom-control-input" 
                                                    id="is_same_bank_name"
                                                    name="is_same_bank_name"
                                                    @if($data->is_same_bank_name == 0)
                                                        checked
                                                    @endif
                                                    onchange="checkSameBankName(this, '#surat_kuasa_wrapper')"
                                                >
                                                <label class="custom-control-label" for="is_same_bank_name"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div 
                                    class="col-lg-6 col-md-12 mb-3" 
                                    id="surat_kuasa_wrapper"
                                    @if($data->is_same_bank_name == 1)
                                    style="display:none;"
                                    @endif 
                                    
                                >
                                    <input 
                                        type="file" 
                                        name="letter_of_attorney_file" 
                                        id="letter_of_attorney_file" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#letter_of_attorney_file_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#letter_of_attorney_file')">
                                        <span>Surat Kuasa</span>
                                        <div class="d-flex align-items-center" id="letter_of_attorney_file_preview">
                                            @if(!is_null($data->letter_of_attorney_file))
                                                <img src="{{env('S3_URL')}}{{$data->letter_of_attorney_file}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 d-flex justify-content-end">
                                    <button class="btn btn-grab__submit d-flex align-items-center">
                                        <span class="mr-2">Simpan dan Lanjut</span> 
                                        <i class="fas fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif

            @if($step == 5)
            <div class="col-lg-9 col-md-12">
                <div class="card shadow mb-4 px-4 grab-card__wrapper">
                    <div class="card-body">
                        <div id="result-form-konten"></div>
                        <h5 class="font-weight-bold mb-4">Data Restoran & Menu</h5>
                        <hr>
                        <form onsubmit="return false;" id="form-konten-5" class='form-konten' backdrop="">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        @if(!is_null($data->outlet_type_food))
                                            @php
                                                $selectedFoodType = json_decode($data->outlet_type_food); 
                                            @endphp
                                        @endif
                                        <label>Tipe Makanan</label>
                                        <select class="form-control" name="food_type[]" multiple="multiple" id="food_type" required>
                                            <option></option>
                                            @foreach($foodType as $item)    
                                                <option 
                                                    value="{{$item}}"
                                                    @if(!is_null($data->outlet_type_food))
                                                        @if(in_array($item, $selectedFoodType))
                                                            selected
                                                        @endif
                                                    @endif
                                                >
                                                    {{ucwords($item)}}
                                                </option>
                                            @endforeach
                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        @if(!is_null($data->outlet_tag_food))
                                            @php
                                                $selectedTagFood = json_decode($data->outlet_tag_food); 
                                            @endphp
                                        @endif
                                        <label>Tag</label>
                                        <select class="form-control" name="food_tag_type[]" id="food_tag_type" multiple="multiple" required>
                                            <option></option>
                                            @foreach($foodType as $item)
                                                <option 
                                                    value="{{$item}}"
                                                    @if(!is_null($data->outlet_tag_food))
                                                        @if(in_array($item, $selectedTagFood))
                                                            selected
                                                        @endif
                                                    @endif
                                                >
                                                    {{ucwords($item)}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block">Kualifikasi Makanan</label>
                                        <div class="btn-group btn-group-toggle grab-btn__group mb-3" data-toggle="buttons">
                                            <label 
                                                class="btn btn-outline-warning 
                                                    @if(is_null($data->food_classification)) 
                                                        active 
                                                    @endif
                                                    @if(strtolower($data->food_classification) == 'halal')
                                                        active
                                                    @endif
                                                    "
                                            >
                                                <input 
                                                    type="radio" 
                                                    name="food_classification" 
                                                    id="option1" 
                                                    autocomplete="off" 
                                                    value="halal"
                                                    @if(is_null($data->food_classification)) 
                                                        checked 
                                                    @endif
                                                    @if(strtolower($data->food_classification) == 'halal')
                                                        checked
                                                    @endif
                                                >
                                                Halal
                                            </label>
                                            <label 
                                                class="btn btn-outline-warning
                                                    @if(strtolower($data->food_classification) == 'non halal')
                                                        active
                                                    @endif    
                                                "
                                            >
                                                <input 
                                                    type="radio" 
                                                    name="food_classification" 
                                                    id="option2" 
                                                    autocomplete="off" 
                                                    value="non halal"
                                                    @if(strtolower($data->food_classification) == 'non halal')
                                                        checked
                                                    @endif
                                                > 
                                                Non Halal
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block">Harga Menu</label>
                                        <div class="btn-group btn-group-toggle grab-btn__group mb-3" data-toggle="buttons">
                                            <label 
                                                class="btn btn-outline-warning
                                                    @if(is_null($data->is_with_tax)) 
                                                        active 
                                                    @endif
                                                    @if($data->is_with_tax == 1)
                                                        active
                                                    @endif"
                                            >
                                                <input 
                                                    type="radio" 
                                                    name="is_with_tax" 
                                                    id="option1" 
                                                    autocomplete="off"
                                                    value="1" 
                                                    @if(is_null($data->is_with_tax)) 
                                                        checked 
                                                    @endif
                                                    @if($data->is_with_tax == 1)
                                                        checked
                                                    @endif
                                                >
                                                Sudah Termasuk Pajak
                                            </label>
                                            <label class="btn btn-outline-warning
                                                    @if($data->is_with_tax == 0)
                                                        active
                                                    @endif"
                                                    >
                                                <input 
                                                    type="radio" 
                                                    name="is_with_tax" 
                                                    id="option2" 
                                                    autocomplete="off"
                                                    value="0"
                                                    @if($data->is_with_tax == 0)
                                                        checked
                                                    @endif
                                                >
                                                Belum Termasuk Pajak
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Deskripsi Restoran <span class="text-danger">*</span></label>
                                        <textarea name="outlet_detail" class="form-control" required>{{!is_null($data->outlet_detail)? $data->outlet_detail:''}}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Perkiraan waktu Persiapan Makanan (Menit)</label>
                                        <select class="form-control" name="time_pickup" id="time_pickup" required>
                                            <option value="">--- Pilih Persiapan Waktu Makanan Anda ---</option>
                                            @foreach($timePickup as $item)
                                            <option 
                                                value="{{$item}}"
                                                @if(strtolower($item) == strtolower($data->time_pickup))
                                                selected
                                                @endif
                                            >{{$item}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 mb-2">
                                    <span class="d-block">Foto Logo Restoran <span class="text-danger">*</span></span>
                                    <small class="text-muted mb-3">( maximum file size : 1mb, format : .png, .jpg, .jpeg)</small>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="outlet_logo" 
                                        id="outlet_logo" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#outlet_logo_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#outlet_logo')">
                                        <span>Logo Restoran</span>
                                        <div class="d-flex align-items-center" id="outlet_logo_preview">
                                            @if(!is_null($data->outlet_logo))
                                                <img src="{{env('S3_URL')}}{{$data->outlet_logo}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 mb-2">
                                    <span class="d-block">Foto Daftar Menu <span class="text-danger">*</span></span>
                                    <small class="text-muted mb-3">( maximum file size : 1mb, format : .png, .jpg, .jpeg)</small>
                                </div>

                                @php
                                    $menuList = !is_null($data->menu_list) ? json_decode($data->menu_list):null;
                                @endphp

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="menu['menu1']" 
                                        id="menu1" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#menu1_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#menu1')">
                                        <span>Foto Daftar Menu 1</span>
                                        <div class="d-flex align-items-center" id="menu1_preview">
                                            @if(isset($menuList->menu1))
                                                <img src="{{env('S3_URL')}}{{$menuList->menu1}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="menu['menu2']" 
                                        id="menu2" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#menu2_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#menu2')">
                                        <span>Foto Daftar Menu 2</span>
                                        <div class="d-flex align-items-center" id="menu2_preview">
                                            @if(isset($menuList->menu2))
                                                <img src="{{env('S3_URL')}}{{$menuList->menu2}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="menu['menu3']" 
                                        id="menu3" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#menu3_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#menu3')">
                                        <span>Foto Daftar Menu 3</span>
                                        <div class="d-flex align-items-center" id="menu3_preview">
                                            @if(isset($menuList->menu3))
                                                <img src="{{env('S3_URL')}}{{$menuList->menu3}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="menu['menu4']" 
                                        id="menu4" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#menu4_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#menu4')">
                                        <span>Foto Daftar Menu 4</span>
                                        <div class="d-flex align-items-center" id="menu4_preview">
                                            @if(isset($menuList->menu4))
                                                <img src="{{env('S3_URL')}}{{$menuList->menu4}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="menu['menu5']" 
                                        id="menu5" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#menu5_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#menu5')">
                                        <span>Foto Daftar Menu 5</span>
                                        <div class="d-flex align-items-center" id="menu5_preview">
                                            @if(isset($menuList->menu5))
                                                <img src="{{env('S3_URL')}}{{$menuList->menu5}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 mb-2">
                                    <span class="d-block">Foto Makanan Dan Minuman <span class="text-danger">*</span></span>
                                    <small class="text-muted mb-3">Upload Foto Daftar Makanan & Minuman Maksimal 5 Foto dalam format png, jpg, jpeg</small>
                                </div>

                                @php
                                    $foodList = !is_null($data->food_list) ? json_decode($data->food_list):null;
                                @endphp

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="food['food1']" 
                                        id="food1" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#food1_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#food1')">
                                        <span>Foto Makanan Dan Minuman 1</span>
                                        <div class="d-flex align-items-center" id="food1_preview">
                                            @if(isset($foodList->food1))
                                                <img src="{{env('S3_URL')}}{{$foodList->food1}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="food['food2']" 
                                        id="food2" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#food2_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#food2')">
                                        <span>Foto Makanan Dan Minuman 2</span>
                                        <div class="d-flex align-items-center" id="food2_preview">
                                            @if(isset($foodList->food2))
                                                <img src="{{env('S3_URL')}}{{$foodList->food2}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="food['food3']" 
                                        id="food3" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#food3_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#food3')">
                                        <span>Foto Makanan Dan Minuman 3</span>
                                        <div class="d-flex align-items-center" id="food3_preview">
                                            @if(isset($foodList->food3))
                                                <img src="{{env('S3_URL')}}{{$foodList->food3}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="food['food4']" 
                                        id="food4" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#food4_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#food4')">
                                        <span>Foto Makanan Dan Minuman 4</span>
                                        <div class="d-flex align-items-center" id="food4_preview">
                                            @if(isset($foodList->food4))
                                                <img src="{{env('S3_URL')}}{{$foodList->food4}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 mb-3">
                                    <input 
                                        type="file" 
                                        name="food['food5']" 
                                        id="food5" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#food5_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#food5')">
                                        <span>Foto Makanan Dan Minuman 5</span>
                                        <div class="d-flex align-items-center" id="food5_preview">
                                            @if(isset($foodList->food5))
                                                <img src="{{env('S3_URL')}}{{$foodList->food5}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 d-flex justify-content-end">
                                    <button class="btn btn-grab__submit d-flex align-items-center">
                                        <span class="mr-2">Simpan dan Lanjut</span> 
                                        <i class="fas fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif

            @if($step == 6)
            <div class="col-lg-9 col-md-12">
                @php
                    $time_opr = (!is_null($data->time_operational))? json_decode($data->time_operational):null;
                @endphp
                <div class="card shadow mb-4 px-4 grab-card__wrapper">
                    <div class="card-body">
                        <div id="result-form-konten"></div>
                        <h5 class="font-weight-bold mb-5">Data Jadwal Buka</h5>
                        <hr>
                        <form onsubmit="return false;" id="form-konten-6" class='form-konten' backdrop="">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="row">
                                <div class="col-md-12 schedule-wrapper">
                                    <input type="hidden" name="days[]" value="senin">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <label class="font-weight-bold">Senin</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="custom-control custom-switch">
                                                <input 
                                                    type="checkbox" 
                                                    class="custom-control-input" 
                                                    id="mondaySwitch"
                                                    name="openStatus" 
                                                    onchange="changeSchedule(this, '#mondayLabel', '#mondayWrapper')"
                                                    @if(is_null($data->time_operational))
                                                        checked
                                                    @endif
                                                    @if(isset($time_opr->senin->status))
                                                        @if($time_opr->senin->status == 'open')
                                                            checked
                                                        @endif
                                                    @endif
                                                >
                                                <label id="mondayLabel" class="custom-control-label" for="mondaySwitch">
                                                    @if(is_null($data->time_operational))
                                                        Buka
                                                    @endif
                                                    @if(isset($time_opr->senin->status))
                                                        @if($time_opr->senin->status == 'open')
                                                            Buka
                                                        @else
                                                            Tutup
                                                        @endif
                                                    @endif
                                                </label>
                                            </div>
                                        </div>

                                        
                                        <div class="col-sm-8" id="mondayWrapper" 
                                            @if(!is_null($data->time_operational) && $time_opr->senin->status == 'close')
                                                style="display:none;"
                                            @endif
                                        >
                                            <div class="row">
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time" 
                                                        name="startPeriod" 
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->senin->status == 'open')
                                                            value="{{$time_opr->senin->periods->startPeriod}}"
                                                        @else
                                                            value="08:00"
                                                        @endif

                                                    >
                                                </div>
                                                <div class="col-sm-1 d-flex align-items-center">
                                                    <div class="g-time__connect"></div>
                                                </div>
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time" 
                                                        name="endPeriod" 
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->senin->status == 'open')
                                                            value="{{$time_opr->senin->periods->endPeriod}}"
                                                        @else
                                                            value="18:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input 
                                                            type="checkbox" 
                                                            class="form-check-input"
                                                            name="allHour"
                                                            onchange="checkAllHour(this,'#mondayWrapper')"
                                                            @if(isset($time_opr->senin->isOpenAllHours))
                                                                @if($time_opr->senin->isOpenAllHours == 1)
                                                                    checked
                                                                @endif
                                                            @endif

                                                        >
                                                        <label class="form-check-label">24 Jam</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        

                                    </div>
                                    <hr class="mt-5">
                                </div>

                                <div class="col-md-12 schedule-wrapper">
                                    <input type="hidden" name="days[]" value="selasa">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <label class="font-weight-bold">Selasa</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="custom-control custom-switch">
                                                <input 
                                                    type="checkbox" 
                                                    class="custom-control-input" 
                                                    id="tuesdaySwitch"
                                                    name="openStatus" 
                                                    onchange="changeSchedule(this, '#tuesdayLabel', '#tuesdayWrapper')"
                                                    @if(is_null($data->time_operational))
                                                        checked
                                                    @endif
                                                    @if(isset($time_opr->selasa->status))
                                                        @if($time_opr->selasa->status == 'open')
                                                            checked
                                                        @endif
                                                    @endif
                                                >
                                                <label id="tuesdayLabel" class="custom-control-label" for="tuesdaySwitch">
                                                    @if(is_null($data->time_operational))
                                                        Buka
                                                    @endif
                                                    @if(isset($time_opr->selasa->status))
                                                        @if($time_opr->selasa->status == 'open')
                                                            Buka
                                                        @else
                                                            Tutup
                                                        @endif
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" id="tuesdayWrapper"
                                            @if(!is_null($data->time_operational) && $time_opr->selasa->status == 'close')
                                                style="display:none;"
                                            @endif
                                        >
                                            <div class="row">
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="startPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->selasa->status == 'open')
                                                            value="{{$time_opr->selasa->periods->startPeriod}}"
                                                        @else
                                                            value="08:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-1 d-flex align-items-center">
                                                    <div class="g-time__connect"></div>
                                                </div>
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="endPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->selasa->status == 'open')
                                                            value="{{$time_opr->selasa->periods->endPeriod}}"
                                                        @else
                                                            value="18:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input 
                                                            type="checkbox" 
                                                            class="form-check-input"
                                                            name="allHour"
                                                            onchange="checkAllHour(this,'#tuesdayWrapper')"
                                                            @if(isset($time_opr->selasa->isOpenAllHours))
                                                                @if($time_opr->selasa->isOpenAllHours == 1)
                                                                    checked
                                                                @endif
                                                            @endif 
                                                        >
                                                        <label class="form-check-label">24 Jam</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-5">
                                </div>

                                <div class="col-md-12 schedule-wrapper">
                                    <input type="hidden" name="days[]" value="rabu">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <label class="font-weight-bold">Rabu</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="custom-control custom-switch">
                                                <input 
                                                    type="checkbox" 
                                                    class="custom-control-input" 
                                                    id="wednesdaySwitch"
                                                    name="openStatus" 
                                                    onchange="changeSchedule(this, '#wednesdayLabel', '#wednesdayWrapper')"
                                                    @if(is_null($data->time_operational))
                                                        checked
                                                    @endif
                                                    @if(isset($time_opr->rabu->status))
                                                        @if($time_opr->rabu->status == 'open')
                                                            checked
                                                        @endif
                                                    @endif
                                                >
                                                <label id="wednesdayLabel" class="custom-control-label" for="wednesdaySwitch">
                                                    @if(is_null($data->time_operational))
                                                        Buka
                                                    @endif
                                                    @if(isset($time_opr->rabu->status))
                                                        @if($time_opr->rabu->status == 'open')
                                                            Buka
                                                        @else
                                                            Tutup
                                                        @endif
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" id="wednesdayWrapper"
                                            @if(!is_null($data->time_operational) && $time_opr->rabu->status == 'close')
                                                style="display:none;"
                                            @endif
                                        >
                                            <div class="row">
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="startPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->rabu->status == 'open')
                                                            value="{{$time_opr->rabu->periods->startPeriod}}"
                                                        @else
                                                            value="08:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-1 d-flex align-items-center">
                                                    <div class="g-time__connect"></div>
                                                </div>
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="endPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->rabu->status == 'open')
                                                            value="{{$time_opr->rabu->periods->endPeriod}}"
                                                        @else
                                                            value="18:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input 
                                                            type="checkbox" 
                                                            class="form-check-input"
                                                            name="allHour"
                                                            onchange="checkAllHour(this,'#wednesdayWrapper')"
                                                            @if(isset($time_opr->rabu->isOpenAllHours))
                                                                @if($time_opr->rabu->isOpenAllHours == 1)
                                                                    checked
                                                                @endif
                                                            @endif 
                                                        >
                                                        <label class="form-check-label">24 Jam</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-5">
                                </div>

                                <div class="col-md-12 schedule-wrapper">
                                    <input type="hidden" name="days[]" value="kamis">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <label class="font-weight-bold">Kamis</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="custom-control custom-switch">
                                                <input 
                                                    type="checkbox" 
                                                    class="custom-control-input" 
                                                    id="thursdaySwitch"
                                                    name="openStatus" 
                                                    onchange="changeSchedule(this, '#thursdayLabel', '#thursdayWrapper')"
                                                    @if(is_null($data->time_operational))
                                                        checked
                                                    @endif
                                                    @if(isset($time_opr->kamis->status))
                                                        @if($time_opr->kamis->status == 'open')
                                                            checked
                                                        @endif
                                                    @endif
                                                >
                                                <label id="thursdayLabel" class="custom-control-label" for="thursdaySwitch">
                                                    @if(is_null($data->time_operational))
                                                        Buka
                                                    @endif
                                                    @if(isset($time_opr->kamis->status))
                                                        @if($time_opr->kamis->status == 'open')
                                                            Buka
                                                        @else
                                                            Tutup
                                                        @endif
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" id="thursdayWrapper"
                                            @if(!is_null($data->time_operational) && $time_opr->kamis->status == 'close')
                                                style="display:none;"
                                            @endif
                                        >
                                            <div class="row">
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="startPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->kamis->status == 'open')
                                                            value="{{$time_opr->kamis->periods->startPeriod}}"
                                                        @else
                                                            value="08:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-1 d-flex align-items-center">
                                                    <div class="g-time__connect"></div>
                                                </div>
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="endPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->kamis->status == 'open')
                                                            value="{{$time_opr->kamis->periods->endPeriod}}"
                                                        @else
                                                            value="18:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input 
                                                            type="checkbox" 
                                                            class="form-check-input"
                                                            name="allHour"
                                                            onchange="checkAllHour(this,'#thursdayWrapper')"
                                                            @if(isset($time_opr->kamis->isOpenAllHours))
                                                                @if($time_opr->kamis->isOpenAllHours == 1)
                                                                    checked
                                                                @endif
                                                            @endif 
                                                        >
                                                        <label class="form-check-label">24 Jam</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-5">
                                </div>

                                <div class="col-md-12 schedule-wrapper">
                                    <input type="hidden" name="days[]" value="jumat">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <label class="font-weight-bold">Jum`at</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="custom-control custom-switch">
                                                <input 
                                                    type="checkbox" 
                                                    class="custom-control-input" 
                                                    id="fridaySwitch"
                                                    name="openStatus" 
                                                    onchange="changeSchedule(this, '#fridayLabel', '#fridayWrapper')"
                                                    @if(is_null($data->time_operational))
                                                        checked
                                                    @endif
                                                    @if(isset($time_opr->jumat->status))
                                                        @if($time_opr->jumat->status == 'open')
                                                            checked
                                                        @endif
                                                    @endif
                                                >
                                                <label id="fridayLabel" class="custom-control-label" for="fridaySwitch">
                                                    @if(is_null($data->time_operational))
                                                        Buka
                                                    @endif
                                                    @if(isset($time_opr->jumat->status))
                                                        @if($time_opr->jumat->status == 'open')
                                                            Buka
                                                        @else
                                                            Tutup
                                                        @endif
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" id="fridayWrapper"
                                            @if(!is_null($data->time_operational) && $time_opr->jumat->status == 'close')
                                                style="display:none;"
                                            @endif
                                        >
                                            <div class="row">
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="startPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->jumat->status == 'open')
                                                            value="{{$time_opr->jumat->periods->startPeriod}}"
                                                        @else
                                                            value="08:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-1 d-flex align-items-center">
                                                    <div class="g-time__connect"></div>
                                                </div>
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="endPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->jumat->status == 'open')
                                                            value="{{$time_opr->jumat->periods->endPeriod}}"
                                                        @else
                                                            value="18:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input 
                                                            type="checkbox" 
                                                            class="form-check-input"
                                                            name="allHour"
                                                            onchange="checkAllHour(this,'#fridayWrapper')"
                                                            @if(isset($time_opr->jumat->isOpenAllHours))
                                                                @if($time_opr->jumat->isOpenAllHours == 1)
                                                                    checked
                                                                @endif
                                                            @endif 
                                                        >
                                                        <label class="form-check-label">24 Jam</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-5">
                                </div>

                                <div class="col-md-12 schedule-wrapper">
                                    <input type="hidden" name="days[]" value="sabtu">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <label class="font-weight-bold">Sabtu</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="custom-control custom-switch">
                                                <input 
                                                    type="checkbox" 
                                                    class="custom-control-input" 
                                                    id="saturdaySwitch"
                                                    name="openStatus" 
                                                    onchange="changeSchedule(this, '#saturdayLabel', '#saturdayWrapper')"
                                                    @if(is_null($data->time_operational))
                                                        checked
                                                    @endif
                                                    @if(isset($time_opr->sabtu->status))
                                                        @if($time_opr->sabtu->status == 'open')
                                                            checked
                                                        @endif
                                                    @endif
                                                >
                                                <label id="saturdayLabel" class="custom-control-label" for="saturdaySwitch">
                                                    @if(is_null($data->time_operational))
                                                        Buka
                                                    @endif
                                                    @if(isset($time_opr->sabtu->status))
                                                        @if($time_opr->sabtu->status == 'open')
                                                            Buka
                                                        @else
                                                            Tutup
                                                        @endif
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" id="saturdayWrapper"
                                            @if(!is_null($data->time_operational) && $time_opr->sabtu->status == 'close')
                                                style="display:none;"
                                            @endif
                                        >
                                            <div class="row">
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="startPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->sabtu->status == 'open')
                                                            value="{{$time_opr->sabtu->periods->startPeriod}}"
                                                        @else
                                                            value="08:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-1 d-flex align-items-center">
                                                    <div class="g-time__connect"></div>
                                                </div>
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="endPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->sabtu->status == 'open')
                                                            value="{{$time_opr->sabtu->periods->endPeriod}}"
                                                        @else
                                                            value="18:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input 
                                                            type="checkbox" 
                                                            class="form-check-input"
                                                            name="allHour"
                                                            onchange="checkAllHour(this,'#saturdayWrapper')"
                                                            @if(isset($time_opr->sabtu->isOpenAllHours))
                                                                @if($time_opr->sabtu->isOpenAllHours == 1)
                                                                    checked
                                                                @endif
                                                            @endif 
                                                        >
                                                        <label class="form-check-label">24 Jam</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-5">
                                </div>

                                <div class="col-md-12 schedule-wrapper">
                                    <input type="hidden" name="days[]" value="minggu">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <label class="font-weight-bold">Minggu</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="custom-control custom-switch">
                                                <input 
                                                    type="checkbox" 
                                                    class="custom-control-input" 
                                                    id="sundaySwitch"
                                                    name="openStatus" 
                                                    onchange="changeSchedule(this, '#sundayLabel', '#sundayWrapper')"
                                                    @if(is_null($data->time_operational))
                                                        checked
                                                    @endif
                                                    @if(isset($time_opr->minggu->status))
                                                        @if($time_opr->minggu->status == 'open')
                                                            checked
                                                        @endif
                                                    @endif
                                                >
                                                <label id="sundayLabel" class="custom-control-label" for="sundaySwitch">
                                                    @if(is_null($data->time_operational))
                                                        Buka
                                                    @endif
                                                    @if(isset($time_opr->minggu->status))
                                                        @if($time_opr->minggu->status == 'open')
                                                            Buka
                                                        @else
                                                            Tutup
                                                        @endif
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" id="sundayWrapper"
                                            @if(!is_null($data->time_operational) && $time_opr->minggu->status == 'close')
                                                style="display:none;"
                                            @endif
                                        >
                                            <div class="row">
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="startPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->minggu->status == 'open')
                                                            value="{{$time_opr->minggu->periods->startPeriod}}"
                                                        @else
                                                            value="08:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-1 d-flex align-items-center">
                                                    <div class="g-time__connect"></div>
                                                </div>
                                                <div class="col-sm-4 mb-2">
                                                    <input 
                                                        type="time"
                                                        name="endPeriod"  
                                                        class="form-control form-control-sm"
                                                        @if(!is_null($data->time_operational) && $time_opr->minggu->status == 'open')
                                                            value="{{$time_opr->minggu->periods->endPeriod}}"
                                                        @else
                                                            value="18:00"
                                                        @endif
                                                    >
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input 
                                                            type="checkbox" 
                                                            class="form-check-input"
                                                            name="allHour"
                                                            onchange="checkAllHour(this,'#sundayWrapper')"
                                                            @if(isset($time_opr->minggu->isOpenAllHours))
                                                                @if($time_opr->minggu->isOpenAllHours == 1)
                                                                    checked
                                                                @endif
                                                            @endif 
                                                        >
                                                        <label class="form-check-label">24 Jam</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-5">
                                </div>


                                <div class="col-md-12 d-flex justify-content-end">
                                    <button class="btn btn-grab__submit d-flex align-items-center">
                                        <span class="mr-2">Simpan dan Lanjut</span> 
                                        <i class="fas fa-chevron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif

            @if($step == 7)
            <div class="col-lg-9 col-md-12">
                <div class="card shadow px-4 mb-4 grab-card__wrapper position-relative">
                    <div class="card-body">
                        <div id="result-form-konten"></div>
                        <h5 class="font-weight-bold mb-4">Data Legalitas</h5>
                        <hr>
                        <form onsubmit="return false;" id="form-konten-7" class='form-konten' backdrop="">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 mb-3">
                                    <div class="form-group">
                                        <label>Nomor Pokok Wajib Pajak <span class="text-danger">*</span></label>
                                        <input 
                                            type="text" 
                                            class="form-control" 
                                            placeholder="NPWP"
                                            name="npwp_number"
                                            value="{{!is_null($data->npwp_number)? $data->npwp_number:''}}" 
                                            required
                                        >
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 mb-3">
                                    <label>
                                        <small class="text-muted">( maximum file size : 1mb, format : .png, .jpg, .jpeg)</small>
                                    </label>
                                    <input 
                                        type="file" 
                                        name="npwp_file" 
                                        id="npwp_file" 
                                        style="display: none;" 
                                        onchange="previewImage(this, '#npwp_file_preview')"
                                    >
                                    <div class="input-grab__file" onclick="browseImage('#npwp_file')">
                                        <span>NPWP <strong class="text-danger">*</strong></span>
                                        <div class="d-flex align-items-center" id="npwp_file_preview">
                                            @if(!is_null($data->npwp_file))
                                                <img src="{{env('S3_URL')}}{{$data->npwp_file}}" alt="upload icon" class="upload-preview__image">
                                            @else
                                                <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                <span class="font-weight-bold">Upload</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-5 mb-5 py-4" style="background:#FAFAFA; width:100%;">
                                    <div class="col-md-12 mb-2">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" name="agree[]" onchange="checkAgreement()">
                                            <label class="form-check-label">
                                                Dengan ini saya mengajukan pendaftaran Grabfood Merchant serta menyutujui <strong>Syarat & Ketentuannya</strong>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-2">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" name="agree[]" onchange="checkAgreement()">
                                            <label class="form-check-label">
                                                Dengan ini saya mengajukan integerasi outlet dengan Grabfood Merchant serta menyutujui <strong>Syarat & Ketentuannya</strong>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-2">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" name="agree[]" onchange="checkAgreement()">
                                            <label class="form-check-label">
                                                Dengan ini saya mendaftar menyetujui ketentuan dan layanan dari Senna Kasir
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="col-md-12 d-flex justify-content-end">
                                    <a href="{{route('grab.activation.create', ['step'=>6])}}" class="btn btn-light mr-4" style="color:#747474;font-size:12px;">
                                        <span>Batal</span> 
                                    </a>
                                    <button class="btn btn-grab__submit d-flex align-items-center" disabled id="btn-submit">
                                        <span class="mr-2">Simpan Pengajuan</span> 
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif

            @if($step == 8)
            <div class="col-lg-9 col-md-12">
                <div class="card shadow px-4 mb-4 grab-card__wrapper position-relative" style="height:400px;">
                    <div class="card-body d-flex align-items-center justify-content-center">
                        <div class="row">
                            <div class="col-md-6 offset-md-3 text-center">
                                <img 
                                    src="{{asset('public/backend/img/check-success.png')}}" 
                                    alt="success"
                                    class="mb-3"
                                    style="width:120px;height:120px;object-fit:contain;"
                                >
                                <h4 class="text-center font-weight-bold">Data Berhasil Diajukan</h4>
                                <p class="text-center">Integrasi dengan grabfood sedang dalam proses peninjauan. Proses ini membutuhkan waktu 7x24 jam. Apabila status integrasi belum berubah dalam 7x24 jam, silahkan hubungi customer service kami.</p>
                                <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id(),'page'=>'orderOnline'])}}">
                                    <button class="btn btn-grab__submit d-flex align-items-center mx-auto">
                                        Selesai
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#type_of_businness').select2();
            $('#business_type_name').select2();
            $('#province').select2();
            $('#city').select2();
            $('#district').select2();
            $('#bank').select2();
            $('#food_type').select2({
                placeholder:'+ Pilih Tipe Makanan'
            });
            $('#food_tag_type').select2({
                placeholder:'+ Pilih Tag'
            });
            $('#time_pickup').select2();
            $('#owner_citizenship').select2();
            $('#countries_name').select2();
            $('#business_scalable').select2();
            $('#business_category').select2();
            $('#business_revenue_average').select2();
            $('#location_type').select2();
            $('#bank_name').select2();
        })

        const browseImage = (selector) => {
            $(selector).attr('accept', '.jpg, .png, .jpeg');
            $(selector).show();
            $(selector).focus();
            $(selector).click();
            $(selector).hide();
        }

        const previewImage = (input, selector) => {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    let imgElmnt = document.createElement('img');
                    imgElmnt.setAttribute('src', e.target.result);
                    imgElmnt.classList.add('upload-preview__image');
                    $(selector).html(imgElmnt);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        const changeSchedule = (e, label, wrapper) => {
            if(e.checked){
                $(label).text('Buka');
                $(wrapper).show();
            } else {
                $(label).text('Tutup');
                $(wrapper).hide();
            }
        }

        const checkAllHour = (e, selector) => {
            if(e.checked){
                $(selector+' .row .col-sm-4').hide();
                $(selector+' .row .col-sm-1 .g-time__connect').hide();
            } else {
                $(selector+' .row .col-sm-4').show();
                $(selector+' .row .col-sm-1 .g-time__connect').show();
            }
        }

        const fillData = (e, selector, value) => {
            if(e.checked){
                $(selector).val(value);
            } else {
                $(selector).val('');
            }
        }

        const checkSameBankName = (e, selector) => {
            if(e.checked){
                $(selector).show();
            } else {
                $(selector).hide();
            }
        }

        $('#province').on('select2:select', function(e){
            if(e.target.value == ''){
                return;
            }
            const rawUrl = '{{route("grab.activation.get-city")}}';
            const url = rawUrl +"?id="+e.target.value;

            $('#city option[value!=""]').remove();
            $('#district option[value!=""]').remove();
            $.ajax({
                url: url,
                success:function(response){
                    let html = '';
                    response.map(item => {
                        html += '<option value="'+item.id+'">'+item.name+'</option>';
                    });

                    $('#city').append(html);
                }
            });
        });

        $('#city').on('select2:select', function(e){
            if(e.target.value == ''){
                return;
            }
            const rawUrl = '{{route("grab.activation.get-district")}}';
            const url = rawUrl +"?id="+e.target.value;

            $('#district option[value!=""]').remove();
            $.ajax({
                url: url,
                success:function(response){
                    let html = '';
                    response.map(item => {
                        html += '<option value="'+item.id+'">'+item.name+'</option>';
                    });

                    $('#district').append(html);
                }
            });
        });

        $('#bank_name').on('select2:select', function(e){
            $('#bank_number').val('');
            $('#bank_account_name').val('');
            $('#bank_branch').val('');
        });

        const getSchedule = () => {
            let data = {};
            const dayWrapper = [...document.querySelectorAll('.schedule-wrapper')];

            dayWrapper.forEach(item => {
                let day = item.querySelector('input[name="days[]"]');
                let startPeriod = item.querySelector('input[name="startPeriod"]');
                let endPeriod = item.querySelector('input[name="endPeriod"]');
                let openStatus = item.querySelector('input[name="openStatus"]');
                let openAllHours = item.querySelector('input[name="allHour"]');
               
                if(openStatus.checked){
                    if(openAllHours.checked){
                        data[day.value] = {
                            "status": "open",
                            "isOpenAllHours":1,
                            "periods":{
                                "startPeriod":"00:00",
                                "endPeriod":"23:59"
                            }
                        }
                    } else {
                        data[day.value] = {
                            "status": "open",
                            "isOpenAllHours":0,
                            "periods":{
                                "startPeriod":startPeriod.value,
                                "endPeriod":endPeriod.value
                            }
                        }
                    }
                } else {
                    data[day.value] = {
                        "status": "close",
                        "isOpenAllHours":0,
                        "periods":{
                            
                        }
                    }
                } 
                
            })

            return data;
        }


        const checkAgreement = () => {
            let elmnt = [...document.querySelectorAll('input[name="agree[]"]')];
            let check = 0;

            elmnt.forEach(item => {
                if(item.checked){
                    check +=1
                }
            });

            if(check === elmnt.length){
                $('#btn-submit').removeAttr('disabled');
            } else {
                $('#btn-submit').attr('disabled', 'disabled');
            }
        }

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('grab.activation.step-one')}}", data, '#result-form-konten');
        });
        $('#form-konten-2').submit(function () {
            var data = getFormData('form-konten-2');
            ajaxTransfer("{{route('grab.activation.step-two')}}", data, '#result-form-konten');
        });

        $('#form-konten-3').submit(function () {
            var data = getFormData('form-konten-3');

            data.append('province_name', $('#province option:selected').text());
            data.append('city_name', $('#city option:selected').text());
            data.append('district_name', $('#district option:selected').text());

            ajaxTransfer("{{route('grab.activation.step-three')}}", data, '#result-form-konten');
        });

        $('#form-konten-4').submit(function () {
            var data = getFormData('form-konten-4');
            ajaxTransfer("{{route('grab.activation.step-four')}}", data, '#result-form-konten');
        });

        $('#form-konten-5').submit(function () {
            var data = getFormData('form-konten-5');
            data.append('outlet_type_food', $('#food_type').val());
            data.append('outlet_tag_food', $('#food_tag_type').val());
            ajaxTransfer("{{route('grab.activation.step-five')}}", data, '#result-form-konten');
        });

        $('#form-konten-6').submit(function () {
            var data = getFormData('form-konten-6');
            let schedule = getSchedule();
            data.append('time_operational', JSON.stringify(schedule));
            ajaxTransfer("{{route('grab.activation.step-six')}}", data, '#result-form-konten');
        });

        $('#form-konten-7').submit(function () {
            modalConfirm('Perhatian','Pastikan data yang anda masukkan sudah benar. Data tidak bisa dirubah setelah melakukan pengajuan !', function(){
                var data = getFormData('form-konten-7');
                ajaxTransfer("{{route('grab.activation.step-seven')}}", data, '#result-form-konten');
            });
        });

        const deleteHistory = (id) => {
            var data = new FormData();
            data.append('id', id);
            modalConfirm("Konfirmasi", "Apakah anda yakin untuk menghapus riwayat pendaftaran ?", function () {
                ajaxTransfer("{{route('grab.activation.delete')}}", data, "#modal-output");
            });
        }
    </script>
@endsection
@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">{{$title}}</h4>
                <span>Akun Grabfood Merchant</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            @if(is_null($step))
            <div class="col-lg-12 col-md-12">
                <div class="card shadow mb-4 px-4 grab-card__wrapper">
                    <div class="card-body">
                        <div id="result-form-konten"></div>
                        <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="countries_name" value="Indonesia">
                            <input type="hidden" name="province_name" value="{{$merchant->getDistrict->getCity->getProvince->name}}">
                            <input type="hidden" name="district_name" value="{{$merchant->getDistrict->name}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="font-weight-bold mb-4">Info Usaha</h5>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Usaha</label>
                                                <input 
                                                    type="text" 
                                                    class="form-control" 
                                                    placeholder="Nama Usaha"
                                                    value="{{$merchant->name}}" 
                                                    disabled
                                                >
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Jenis Usaha</label>
                                                <input 
                                                    type="text" 
                                                    name="business_type_name" 
                                                    class="form-control" 
                                                    placeholder="Jenis Usaha"
                                                    value="{{$merchant->getTypeOfBusiness->name}}"
                                                    disabled
                                                >
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Alamat Email</label>
                                                <input 
                                                    type="email" 
                                                    name="registrant_email" 
                                                    class="form-control" 
                                                    placeholder="Email"
                                                    value="{{$user->email}}" 
                                                    required
                                                    disabled
                                                >
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Pemilik Usaha</label>
                                                <input 
                                                    type="text" 
                                                    name="registrant_name" 
                                                    class="form-control" 
                                                    placeholder="Email"
                                                    value="{{$user->fullname}}" 
                                                    required
                                                    disabled
                                                >
                                            </div>
                                        </div>
                                        <input 
                                            type="hidden" 
                                            name="registrant_phone_number" 
                                            @if(!is_null($user->phone_number))
                                                value="{{$user->phone_number}}"
                                            @else
                                                value="-"
                                            @endif
                                        >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row"> 
                                        <div class="col-md-12">
                                            <h5 class="font-weight-bold mb-4">Cabang</h5>
                                        </div>
                                    </div>
                                    <div class="row border p-4 mb-3 rounded">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Toko di GrabFood</label>
                                                <input 
                                                    type="text" 
                                                    name="grab_food_name" 
                                                    class="form-control" 
                                                    placeholder="Nama Toko di GrabFood"
                                                    value="{{$merchant->name}}" 
                                                    required
                                                >
                                            </div>
                                        </div>
                                        {{---<div class="col-md-12">
                                            <div class="form-group">
                                                <label>Provinsi</label>
                                                <input 
                                                    type="text" 
                                                    name="province_name" 
                                                    class="form-control" 
                                                    placeholder="Provinsi"
                                                    value="{{$merchant->getDistrict->getCity->getProvince->name}}"
                                                    required
                                                    disabled
                                                >
                                            </div>
                                        </div>---}}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Kota</label>
                                                <input 
                                                    type="text" 
                                                    name="city_name" 
                                                    class="form-control" 
                                                    placeholder="Kota"
                                                    value="{{$merchant->getDistrict->getCity->name}}"
                                                    required
                                                    disabled
                                                >
                                            </div>
                                        </div>
                                        {{---<div class="col-md-12">
                                            <div class="form-group">
                                                <label>Kecamatan</label>
                                                <input 
                                                    type="text" 
                                                    name="district_name" 
                                                    class="form-control" 
                                                    placeholder="Kecamatan"
                                                    value="{{$merchant->getDistrict->name}}"
                                                    required
                                                    disabled
                                                >
                                            </div>
                                        </div>---}}
                                        <div class="col-12 mb-2">
                                            <small class="text-muted mb-3">( maximum file size : 1mb, format : .png, .jpg, .jpeg)</small>
                                        </div>
                                        <div class="col-lg-12 col-md-12 mb-3">
                                            <input 
                                                type="file" 
                                                name="screenshot_file" 
                                                id="screenshot_file" 
                                                style="display: none;" 
                                                onchange="previewImage(this, '#screenshot_file_preview')"
                                            >
                                            <div class="input-grab__file" onclick="browseImage('#screenshot_file')">
                                                <span>Screenshoot Aplikasi Grab Merchant <strong class="text-danger">*</strong></span>
                                                <div class="d-flex align-items-center" id="screenshot_file_preview">
                                                    <img src="{{asset('public/backend/img/upload-icon.png')}}" alt="upload icon" class="mr-2 upload-icon">
                                                    <span class="font-weight-bold">Upload</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12">
                                            <p class="text-muted">Screenshoot halaman profil Grab Merchant anda sesuai dengan outlet yang dipilih dengan langkah-langkah berikut :</p>
                                            <ol class="text-muted">
                                                <li>Buka Apps Grab Merchant</li>
                                                <li>Masuk ke menu Account</li>
                                                <li>Buka profil toko anda</li>
                                                <li>Lakukan screenshoot pada halaman Profil Toko Grab Merchant</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 d-flex justify-content-end">
                                    <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id(),'page'=>'orderOnline'])}}" class="btn btn-light mr-3" style="color:#747474;font-size:12px;">
                                        <span>Batal</span> 
                                    </a>
                                    <button class="btn btn-grab__submit d-flex align-items-center">
                                        <span>Ajukan</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif
            @if($step == 'success')
            <div class="col-lg-12 col-md-12">
                <div class="card shadow px-4 mb-4 grab-card__wrapper position-relative" style="height:400px;">
                    <div class="card-body d-flex align-items-center justify-content-center">
                        <div class="row">
                            <div class="col-md-6 offset-md-3 text-center">
                                <img 
                                    src="{{asset('public/backend/img/check-success.png')}}" 
                                    alt="success"
                                    class="mb-3"
                                    style="width:120px;height:120px;object-fit:contain;"
                                >
                                <h4 class="text-center font-weight-bold">Data Berhasil Diajukan</h4>
                                <p class="text-center">Integrasi dengan grabfood sedang dalam proses peninjauan. Proses ini membutuhkan waktu 7x24 jam. Apabila status integrasi belum berubah dalam 7x24 jam, silahkan hubungi customer service kami.</p>
                                <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id(),'page'=>'orderOnline'])}}">
                                    <button class="btn btn-grab__submit d-flex align-items-center mx-auto">
                                        Selesai
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script>
        const browseImage = (selector) => {
            $(selector).attr('accept', '.jpg, .png, .jpeg');
            $(selector).show();
            $(selector).focus();
            $(selector).click();
            $(selector).hide();
        }

        const previewImage = (input, selector) => {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    let imgElmnt = document.createElement('img');
                    imgElmnt.setAttribute('src', e.target.result);
                    imgElmnt.classList.add('upload-preview__image');
                    $(selector).html(imgElmnt);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('grab.activation.save')}}", data, '#result-form-konten');
        });
    </script>
@endsection
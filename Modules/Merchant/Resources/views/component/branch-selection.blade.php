@if($withoutModal)
    <div class="col-md-12 mb-4" style="background-color: rgba(221, 221, 221, 0.1);border: 2px solid #F0F0F0;border-radius: 10px">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-form-label font-weight-bold">Multi Cabang<br>
                    <small>
                        {{$subTitle}}
                    </small>
                </label>
                @if($with_onchange==true)
                    @if(is_null($data->id))
                        <select class="form-control" id="{{$selector}}" name="md_merchant_id"
                                @if(isset($function_onchange))
                                    onchange="{{$function_onchange}}"
                                    @else
                                onchange="onchangeBranch()"
                                @endif
                                required>
                            <option value="-1">Pilih Cabang</option>
                            @foreach(get_cabang() as $b)
                                <option value="{{$b->id}}" @if($b->id==merchant_id()) selected @endif>{{$b->nama_cabang}}</option>
                            @endforeach
                        </select>
                    @else
                        <select class="form-control" id="branch" name="md_merchant_id" disabled required>
                            <option value="{{$data->md_merchant_id}}" selected>{{$data->getMerchant->name}}</option>
                        </select>
                    @endif
                @else
                    @if(is_null($data->id))
                        <select class="form-control" id="{{$selector}}" name="md_merchant_id" required>
                            <option value="-1">Pilih Cabang</option>
                            @foreach(get_cabang() as $b)
                                <option value="{{$b->id}}" @if($b->id==merchant_id()) selected @endif>{{$b->nama_cabang}}</option>
                            @endforeach
                        </select>
                    @else
                        <select class="form-control" id="branch" name="md_merchant_id" disabled required>
                            <option value="{{$data->md_merchant_id}}" selected>{{$data->getMerchant->name}}</option>
                        </select>
                    @endif
                @endif
            </div>
        </div>

    </div>
@else
    <div class="col-md-6">
        <div class="form-group">
            <label class="font-weight-bold">Multi Cabang</label>
            @if($with_onchange==true)
                @if(is_null($data->id))
                    <select class="form-control" id="{{$selector}}" name="md_merchant_id"
                            @if(isset($function_onchange))
                            onchange="{{$function_onchange}}"
                            @else
                            onchange="onchangeBranch()"
                            @endif
                            required>
                        <option value="-1">Pilih Cabang</option>
                        @foreach(get_cabang() as $b)
                            <option value="{{$b->id}}" @if($b->id==merchant_id()) selected @endif>{{$b->nama_cabang}}</option>
                        @endforeach
                    </select>
                @else
                    <select class="form-control" id="branch" name="md_merchant_id" disabled required>
                        <option value="{{$data->md_merchant_id}}" selected>{{$data->getMerchant->name}}</option>
                    </select>
                @endif
            @else
                @if(is_null($data->id))
                    <select class="form-control" id="{{$selector}}" name="md_merchant_id" required>
                        <option value="-1">Pilih Cabang</option>
                        @foreach(get_cabang() as $b)
                            <option value="{{$b->id}}" @if($b->id==merchant_id()) selected @endif>{{$b->nama_cabang}}</option>
                        @endforeach
                    </select>
                @else
                    <select class="form-control" id="branch" name="md_merchant_id" disabled required>
                        <option value="{{$data->md_merchant_id}}" selected>{{$data->getMerchant->name}}</option>
                    </select>
                @endif
            @endif
        </div>
    </div>
@endif

<script>
    $(document).ready(function () {
        $("#{{$selector}}").select2()
    })
</script>

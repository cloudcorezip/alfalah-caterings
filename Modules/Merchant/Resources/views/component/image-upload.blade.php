<style>
    .file-upload {
        background-color: rgba(221, 221, 221, 0.1);
        margin: 0 auto;
        position: relative;
        border:2px dashed #BFBFBF;
        width: 100%;
        height: 220px;
        border-radius: 5px;
    }

    .file-upload:hover {
        background-color: #BFBFBF;
    }

    .file-upload-content {
        width: 100%;
        height: 100%;
        text-align: center;
    }

    .file-upload-input {
        position: absolute;
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        outline: none;
        opacity: 0;
        cursor: pointer;
        z-index: 2;
    }

    .image-title-wrap {
        padding: 0 15px 15px 15px;
        color: #222;
    }

    .drag-text {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
        color: rgba(0, 0, 0, 0.5);
        z-index: 1;
    }


    .file-upload-image {
        position: absolute;
        width: 80%;
        height: 80%;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        object-fit:contain;
        border-radius: 5px;
    }

    .remove-image {
        margin: 0;
        color: #fff;
        background: #cd4535;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #b02818;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
        position:absolute;
        top: -10px;
        right:-10px;
    }

    .remove-image:hover {
        background: #c13b2a;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
    }

    .remove-image:active {
        border: 0;
        transition: all .2s ease;
    }

    .btn-delete-image {
        background-color: #fff;
        border: 1px solid #ff4400;
        color:rgba(119, 119, 119, 0.79);
        border:none;
        border-radius: 50%;
        position: absolute;
        top: -10px;
        right: -10px;
    }

    .btn-delete-image:hover {
        background-color: #ff4400;
        color: #fff;
    }
</style>

<div class="form-group">
    <label class="font-weight-bold">{{$title}}</label>
    <div class="file-upload" id="file-upload-{{$fieldName}}">
        <div class="image-upload-wrap" @if(!is_null($file)) style="display:none;" @endif>
            <input 
                class="file-upload-input" 
                type='file' 
                name="{{$fieldName}}" 
                onchange="readURL(this);" 
                accept='image/*'
                data-wrapper-id="#file-upload-{{$fieldName}}"
            />
            <div class="drag-text">
                <h3>
                    <i class="fa fa-camera fa-2x"></i>
                </h3>
                <p class="mb-0"><b>No file choosen.</b></p>
                @if(isset($description))
                <span>{{$description}}</span>
                @endif
            </div>
        </div>
        <div class="file-upload-content" @if(is_null($file)) style="display:none;" @endif>
            <img class="file-upload-image" src="{{env('S3_URL')}}{{$file}}" alt="your image" />
            <div class="image-title-wrap">
                <button type="button" onclick="removeUpload('#file-upload-{{$fieldName}}')" class="btn btn-delete-image">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    function readURL(input) {
        const wrapperId = input.getAttribute('data-wrapper-id');

        let fileTypes = [
            'jpg',
            'jpeg',
            'png'
        ];
        
        if (input.files && input.files[0]) {
            let extension = input.files[0].name.split('.').pop().toLowerCase(),
                isSuccess = fileTypes.indexOf(extension) > -1;

            let reader = new FileReader();

            reader.onload = function(e) {
                $(`${wrapperId} .image-upload-wrap`).hide();
                if(isSuccess){
                    $(`${wrapperId} .file-upload-image`).show()
                    $(`${wrapperId} .file-upload-image`).attr('src', e.target.result);

                }else{
                    $(`${wrapperId} .file-upload-image`).hide();
                }
                $(`${wrapperId} .file-upload-content`).show();

                $(`${wrapperId} .image-title`).html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);
        } else {
            removeUpload(wrapperId);
        }
    }

    function removeUpload(selector) {
        const cloneEl = $(`${selector} .file-upload-input`).val('').clone();
        $(`${selector} .file-upload-input`).replaceWith(cloneEl);
        $(`${selector} .file-upload-content`).hide();
        $(`${selector} .image-upload-wrap`).show();
    }
    $('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
    });
</script>

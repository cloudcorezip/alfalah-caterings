<style>

    .file-upload {
        background-color: rgba(221, 221, 221, 0.1);
        margin: 0 auto;
    }

    .file-upload-content {
        display: none;
        text-align: center;
    }

    .file-upload-input {
        position: absolute;
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        outline: none;
        opacity: 0;
        cursor: pointer;
    }

    .image-upload-wrap {
        border:2px dashed #BFBFBF;
        position: relative;
    }

    .image-upload-wrap:hover {
        background-color: #BFBFBF;
        border: 2px dashed #BFBFBF;
    }

    .image-title-wrap {
        padding: 0 15px 15px 15px;
        color: #222;
    }

    .drag-text {
        text-align: center;
        color: rgba(0, 0, 0, 0.5);
        padding: 40px 50px;
    }


    .file-upload-image {
        max-height: 200px;
        max-width: 200px;
        margin: auto;
        padding: 20px;
    }

    .remove-image {
        width: 200px;
        margin: 0;
        color: #fff;
        background: #cd4535;
        border: none;
        padding: 10px;
        border-radius: 4px;
        border-bottom: 4px solid #b02818;
        transition: all .2s ease;
        outline: none;
        text-transform: uppercase;
        font-weight: 700;
    }

    .remove-image:hover {
        background: #c13b2a;
        color: #ffffff;
        transition: all .2s ease;
        cursor: pointer;
    }

    .remove-image:active {
        border: 0;
        transition: all .2s ease;
    }
</style>

<div class="form-group">
<label class="font-weight-bold">{{$title}}</label>
<div class="file-upload">
    <div class="image-upload-wrap">
        <input class="file-upload-input" type='file' name="{{$fieldName}}" onchange="readURL(this);" {{($allow_all==true)?"":"accept='image/*'"}} />
        <div class="drag-text">
            <h3>
                <i class="fa fa-camera fa-2x"></i>
            </h3>
            <p><b>No file choosen.</b><br>Tipe file : jpg, jpeg, png, docx, xlsx, pdf, zip. Maksimal ukuran file : 2.5 MB</p>
        </div>
    </div>
    <div class="file-upload-content">
        <img class="file-upload-image" src="#" alt="your image" />

        <div class="image-title-wrap">
            <span class="image-title ml-2"></span>
            <button type="button" onclick="removeUpload()" class="btn btn-delete-xs btn-xs"><i class="fa fa-trash-alt" style="color: red"></i></button>
        </div>
    </div>
</div>
</div>
<script>
    function readURL(input) {
        let fileTypes = [
            'jpg',
            'jpeg',
            'png'
        ];

        if (input.files && input.files[0]) {
            let extension = input.files[0].name.split('.').pop().toLowerCase(),
                isSuccess = fileTypes.indexOf(extension) > -1;

            let reader = new FileReader();

            reader.onload = function(e) {
                $('.image-upload-wrap').hide();
                if(isSuccess){
                    $('.file-upload-image').show()
                    $('.file-upload-image').attr('src', e.target.result);

                }else{
                    $('.file-upload-image').hide();
                }
                $('.file-upload-content').show();

                $('.image-title').html(input.files[0].name);
            };

            reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }
    $('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
    });
</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<style>
    .select2-container .select2-selection--multiple .select2-selection__rendered  {
        display: grid!important;
        grid-template-columns: 1fr 1fr;
    }
</style>
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a>Laporan</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.report-all', ['page'=>'sale-order'])}}">Penjualan</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a>{{$title}}</a>
                    </li>
                </ol>
            </nav>
            <span>Disini, kamu bisa melihat buka tutup kasir setiap cabangmu</span>

        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <div class="py-3">
                <h4 class="font-weight-bold">{{$title}}</h4>
                <small class="text-muted period-date">-</small>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-custom" id="table-data" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        @foreach($tableColumns as $key =>$item)
                            <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>


        </div>
    </div>
</div>

<div class="side-popup" id="side-popup">
    <div class="row">
        <div class="col-12">
            <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <span class="text-white form-filter3-text-header">Detail Laporan Tutup Kasir</span>
                </div>
                <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter', 'side-popup')">
                    <i style="font-size:14px;" class="fas fa-times text-white"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="side-popup-body">

    </div>
</div>

<a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
    <div class="d-flex align-items-center">
        <i class="fas fa-filter mr-2 text-white"></i>
        <span class="text-white form-filter3-text-header">Filter</span>
    </div>
</a>

<div class="form-filter3" id="form-filter3">
    <div class="row">
        <div class="col-12">
            <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <i class="fas fa-filter mr-2"></i>
                    <span class="text-white form-filter3-text-header">Filter</span>
                </div>
                <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                    <i style="font-size:14px;" class="fas fa-times text-white"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="row mb-3 px-30">
        <div class="col-12">
            <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
            <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
        </div>
    </div>
    <hr>
    <form id="form-filter" onsubmit="return false" class="px-30">
        <div class="row justify-content-end align-items-center">
            <div class="col-md-12 mb-3">
                <div class="form-group">
                    <label for="exampleInputPassword1">Tanggal Awal</label>
                    <input type="text" class="form-control form-control trans_time" id="startDate" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date">
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <div class="form-group">
                    <label for="exampleInputPassword1">Tanggal Akhir</label>
                    <input type="text" class="form-control form-control trans_time" id="endDate" value="{{$endDate}}" style="background: white" name="end_date">
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <label>Pilih Cabang yang ingin dibandingkan</label>
                <select name="md_merchant_id" id="branch" class="form-control" multiple="multiple" data-is_branch="0">
                    <option value="-1">Semua Cabang</option>
                    @foreach(get_cabang() as $key  =>$item)
                        <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-12">
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <button type="submit" class="btn btn-success btn-block" style="padding:10px 20px;">Terapkan</button>
                <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>

            </div>
        </div>
    </form>
</div>

@endsection


@section('js')
<script>
     $("#branch").on("select2:select", function(e){
        if(e.params.data.id == "-1"){
            $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
        } else {
            $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
        }
    });
    const loadData = () => {
        let start = $("#startDate").val();
        let end = $("#endDate").val();

        let md_merchant_id;

        if($("#branch").attr('data-is_branch') == 0){
            let merchantIds = [];
            let selectedMerchantId = $("#branch").select2('data');
            selectedMerchantId.forEach(item => {
                merchantIds.push(parseInt(item.id));
            });

            md_merchant_id = merchantIds.join('_');
        } else {
            md_merchant_id = $("#branch_merchant").val();
        }
        $('.period-date').html(moment(start).format('MMMM D, YYYY') + ' - ' + moment(end).format('MMMM D, YYYY'));
        ajaxDataTable('#table-data', 1, "{{route('merchant.report.sale-order.close-cashier.datatable')}}?sk={{$searchKey}}&start_date="+moment(start).format('YYYY-MM-DD')+"&end_date="+moment(end).format('YYYY-MM-DD')+"&merchant_id="+md_merchant_id, [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ]);
    }

    $(document).ready(function() {
        $('#branch').select2();
        loadData();
    });

    $('#form-filter').submit(function () {
        loadData();
        showFilter('btn-show-filter3', 'form-filter3');
    });
</script>

<script>

    const loadDetail = (value) => {
        const data = JSON.parse(value.json_response)[0];
        const totalTunaiSistem = value.total_tunai_sistem;

        const selisih = parseInt(totalTunaiSistem) - parseInt(data.actual_amount);

        $('#side-popup-body').html('');
        let html =  "<div class='row mb-3 px-30'>"+
                        "<div class='col-12'>"+
                            "<h5 class='font-weight-bold text-dark mt-3 text-center'>Laporan Transaksi</h5>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Nama Kasir</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 style='text-transform:capitalize;' class='text-dark text-right'>"+data.fullname+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Waktu Buka</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+moment(data.start_date).format('DD MMMM YYYY, HH:mm')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Waktu Tutup</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+moment(data.end_date).format('DD MMMM YYYY, HH:mm')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<hr>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Modal Awal</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat(parseFloat(data.start_amount), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<hr>"+
                    "<div class='row px-30'>"+
                        "<div class='col-12'>"+
                            "<h5 class='font-weight-bold text-dark text-center'>Penjualan</h5>"+
                        "</div>"+
                    "</div>"+
                    "<hr>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Tunai</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat(parseFloat(data.cash_sale), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Non Tunai</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat(parseFloat(data.non_cash_sale), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Pendapatan Lain-Lain</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat(parseFloat(data.other_revenue), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<hr>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Total</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat((parseInt(data.cash_sale)+parseInt(data.non_cash_sale)+parseInt(data.other_revenue)) , 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<hr>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Transaksi Selesai</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+data.sale_finish+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Transaksi Belum Selesai</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+data.sale_not_finish+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<hr>"+
                    "<div class='row px-30'>"+
                        "<div class='col-12'>"+
                            "<h5 class='font-weight-bold text-dark text-center'>Pembelian</h5>"+
                        "</div>"+
                    "</div>"+
                    "<hr>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Tunai</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat(parseFloat(data.cash_purchase), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Non Tunai</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat(parseFloat(data.non_cash_purchase), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Pengeluaran Lain-Lain</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat(parseFloat(data.other_cost), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark'>Total</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right'>"+currencyFormat((parseFloat(data.cash_purchase)+parseFloat(data.non_cash_purchase)+parseFloat(data.other_cost)) , 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<hr>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark font-weight-bold'>Total Tunai Sistem</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right font-weight-bold'>"+currencyFormat(parseFloat(totalTunaiSistem), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark font-weight-bold'>Total Tunai Aktual</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right font-weight-bold'>"+currencyFormat(parseFloat(data.actual_amount), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"+
                    "<div class='row mb-3 px-30'>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark font-weight-bold'>Selisih</h6>"+
                        "</div>"+
                        "<div class='col-6'>"+
                            "<h6 class='text-dark text-right font-weight-bold'>"+currencyFormat(parseFloat(selisih), 'rp')+"</h6>"+
                        "</div>"+
                    "</div>"
                    ;
        $('#side-popup-body').append(html);
        $('#side-popup').addClass('show');
    }

const reloadDataTable = () => void 0;
const exportData = () => {
    modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
        var startDate = $('#reportrange').data('daterangepicker').startDate._d;
        var endDate = $('#reportrange').data('daterangepicker').endDate._d;

        const interval = $('#interval').val();

        const data = new FormData();
        data.append('interval', $('#interval').val());
        data.append('md_merchant_id',$('#branch').val());
        data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));

        ajaxTransfer("{{route('merchant.report.sale-order.export-close-cashier')}}", data, '#modal-output');
    });

}
</script>

@endsection

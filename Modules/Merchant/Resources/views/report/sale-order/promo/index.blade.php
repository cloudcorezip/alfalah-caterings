@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')

<style>
    .select2-container .select2-selection--multiple .select2-selection__rendered  {
        display: grid!important;
        grid-template-columns: 1fr 1fr;
    }
</style>
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a>Laporan</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.report-all', ['page'=>'sale-order'])}}">Penjualan</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center"> 
                        <a>{{$title}}</a>
                    </li>   
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <div class="py-3">
                <h4 class="font-weight-bold">{{$title}}</h4>
                <small class="text-muted period-date">-</small>
            </div>
            <div class="py-3">
                <select class="form-control" name="dataPerPage" id="dataPerPage">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="50">50</option>
                </select>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-custom" width="100%" cellspacing="0">
                    <thead>
                        <tr style="background-color:#FFF;">
                            <th>Nama Promo</th>
                            <th>Outlet</th>
                            <th>Total Promo</th>
                        </tr>
                    </thead>
                    <tbody id="data-result">
                        <tr>
                            <td>Promo Hydrangea</td>
                            <td>Outlet A</td>
                            <td>Rp 61.596</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Outlet B</td>
                            <td>Rp 66.128</td>
                        </tr>
                        <tr>
                            <td>Promo Gardenia</td>
                            <td>Outlet A</td>
                            <td>Rp 61.596</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Outlet B</td>
                            <td>Rp 66.128</td>
                        </tr>
                        <tr>
                            <td>Promo Gardenia</td>
                            <td>Outlet A</td>
                            <td>Rp 61.596</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr style="background-color:#F8F9FC;">
                            <td class="font-weight-bold" colspan="2">Total</td>
                            <td class="font-weight-bold">Rp 895.223</td>
                        </tr>
                    </tfoot>
                </table>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end" id="pagination-result">

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>

<a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
    <div class="d-flex align-items-center">
        <i class="fas fa-filter mr-2 text-white"></i>
        <span class="text-white form-filter3-text-header">Filter</span>
    </div>
</a>

<div class="form-filter3" id="form-filter3">
    <div class="row">
        <div class="col-12">
            <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <i class="fas fa-filter mr-2"></i>
                    <span class="text-white form-filter3-text-header">Filter</span>
                </div>
                <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                    <i style="font-size:14px;" class="fas fa-times text-white"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="row mb-3 px-30">
        <div class="col-12">
            <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
            <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
        </div>
    </div>
    <hr>
    <form id="form-filter-sale-order" onsubmit="return false" class="px-30">
        <div class="row justify-content-end align-items-center">
            <div class="col-md-12 mb-3">
                <label>Tanggal</label>
                <div id="reportrange" class="form-control rounded-0 mr-2" name="date-range" style="width:auto;">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <label>Pilih Cabang yang ingin dibandingkan</label>
                <select name="md_merchant_id" id="branch" class="form-control" multiple="multiple">
                    <option value="-1">Semua Cabang</option>
                    @foreach(get_cabang() as $key  =>$item)
                        <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-12">
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <button type="submit" class="btn btn-success btn-block" style="padding:10px 20px!important;">Terapkan</button>
                <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>

            </div>
        </div>
    </form>
</div>

@endsection


@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>

<script>

     $(function() {

        var start = moment().startOf('month');
        var end = moment().endOf('month');

        $('.period-date').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
            }
        }, cb);
        cb(start, end);

    });
</script>

<script>
$(document).ready(function(){
    $('#branch').select2();

    $('#reportrange').on('apply.daterangepicker', (e, picker) => {
        var start = picker.startDate._d;
        var end = picker.endDate._d;
        $('.period-date').html(moment(start,'MMMM D, YYYY').format('MMMM D, YYYY') + ' - ' + moment(end,'MMMM D, YYYY').format('MMMM D, YYYY'));
        loadData();
    });

    $('#dataPerPage').on('change', function(){
        loadData();
    });
    $('#branch').on('change', function(){
        loadData();
    });
    const loadData = (offset=0) => {

        var startDate = $('#reportrange').data('daterangepicker').startDate._d;
        var endDate = $('#reportrange').data('daterangepicker').endDate._d;

        const totalData= parseInt("{{$totalData}}") + 1;
        const totalPage = Math.ceil(totalData / $('#dataPerPage').val());

        const data = new FormData();
        data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('limit', $('#dataPerPage').val());
        data.append('md_merchant_id',$('#branch').val());
        data.append('offset', offset);

        ajaxTransfer("{{route('merchant.report.sale-order.promoReport')}}", data,function(response){
            const responseData = JSON.parse(response);
            console.log(responseData);
            $('#data-result').html('');
            if(responseData.code == 404){
                let html = "<tr>"+
                                "<td class='text-center' colspan='2'>Data laporan tidak ditemukan</td>"+
                            "</tr>";
                $('#data-result').append(html);
                return;
            }

            let html = "";

            responseData.data.forEach(item => {
                html += "<tr>"+
                        "<td>"+item.discount_name+"</td>"+
                        "<td>"+currencyFormat(parseFloat(item.total_promo), 'rp')+"</td>"+
                        "</tr>";
            });

            $('#data-result').append(html);

            let prevOffset;
            let nextOffset;

            if(parseInt(offset) + parseInt($('#dataPerPage').val()) < totalData){
                nextOffset = parseInt(offset) + parseInt($('#dataPerPage').val());
            } else {
                nextOffset = offset;
            }

            if(parseInt(offset) - parseInt($('#dataPerPage').val()) < 0){
                prevOffset = offset;
            } else {
                prevOffset = parseInt(offset) - parseInt($('#dataPerPage').val());
            }

            let pagination = "<li class='page-item'>"+
                                "<a class='page-link' aria-label='Previous' data-offset='"+prevOffset+"'>"+
                                    "<span aria-hidden='true'>&laquo;</span>"+
                                    "<span class='sr-only'>Previous</span>"+
                                "</a>"+
                            "</li>"
            for(let i = 0; i < totalPage; i++){
                if(offset == i * $('#dataPerPage').val()){
                    pagination += "<li class='page-item active'>"+
                                    "<a class='page-link' data-offset='"+i * $('#dataPerPage').val()+"'>"+(i+1)+"</a>"+
                                "</li>";
                } else {
                    pagination += "<li class='page-item'>"+
                                    "<a class='page-link' data-offset='"+i * $('#dataPerPage').val()+"'>"+(i+1)+"</a>"+
                                "</li>";
                }

            }

            pagination += "<li class='page-item'>"+
                            "<a class='page-link' aria-label='Next' data-offset='"+nextOffset+"'>"+
                                "<span aria-hidden='true'>&raquo;</span>"+
                                "<span class='sr-only'>Next</span>"+
                            "</a>"+
                        "</li>";
            $('#pagination-result').html(pagination);
        });
    }


    $(document).on('click','#pagination-result .page-item .page-link', function(){
        const offset = $(this).attr('data-offset');
        loadData(offset);
    })

    loadData();
})

const reloadDataTable = () => void 0;
const exportData = () => {
    modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
        var startDate = $('#reportrange').data('daterangepicker').startDate._d;
        var endDate = $('#reportrange').data('daterangepicker').endDate._d;

        let totalData= parseInt("{{$totalData}}") + 1;

        const data = new FormData();
        data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('md_merchant_id',$('#branch').val());
        data.append('limit', totalData);

        ajaxTransfer("{{route('merchant.report.sale-order.export-promo')}}", data, '#modal-output');
    });

}

</script>


@endsection

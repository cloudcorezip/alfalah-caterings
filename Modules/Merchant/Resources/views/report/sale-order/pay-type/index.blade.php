@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')

<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item d-flex align-items-center">
                    <a>Laporan</a>
                </li>
                <li class="breadcrumb-item d-flex align-items-center">
                    <a href="{{route('merchant.report-all', ['page'=>'sale-order'])}}">Penjualan</a>
                </li>
                <li class="breadcrumb-item active d-flex align-items-center"> 
                    <a>{{$title}}</a>
                </li>   
            </ol>
        </nav>
    </div>
</div>



<div class="container-fluid mb-4">
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <div class="py-3">
                <h4 class="font-weight-bold">{{$title}}</h4>
                <small class="text-muted period-date">-</small>
            </div>
            <div>
                <button
                    class="btn btn-bg__orange rounded-0 text-white btn-type"
                    data-type="penjualan"
                >Penjualan
                </button>
                <button
                    class="btn btn-light rounded-0 text-muted btn-type mr-4"
                    data-type="pembayaran piutang"
                >Pembayaran Piutang
                </button>
                <input type="hidden" id="type" name="type" value="penjualan">

                <button
                    class="btn btn-bg__orange rounded-0 text-white btn-int"
                    data-interval="day"
                >Hari
                </button>
                <button
                    class="btn btn-light rounded-0 text-muted btn-int"
                    data-interval="month"
                >Bulan
                </button>
                <button
                    class="btn btn-light rounded-0 text-muted btn-int"
                    data-interval="year"
                >Tahun
                </button>
                <input type="hidden" id="interval" name="interval" value="day">
            </div>
        </div>
        <div class="card-body" id="myChart-wrapper">
            <canvas id="myChart" width="400" height="80"></canvas>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <div class="py-3">
                <h4 class="font-weight-bold">{{$title}}</h4>
                <small class="text-muted period-date">-</small>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-custom" width="100%" cellspacing="0">
                    <thead>
                        <tr style="background-color:#FFF;">
                            <th>Jenis Bayar</th>
                            <th>Penjualan</th>
                        </tr>
                    </thead>
                    <tbody id="data-result">
                    </tbody>
                    <tfoot>
                        <tr style="background-color:#F8F9FC;">
                            <td class="font-weight-bold">Total</td>
                            <td class="font-weight-bold">Rp. 10.897.252</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
    <div class="d-flex align-items-center">
        <i class="fas fa-filter mr-2 text-white"></i>
        <span class="text-white form-filter3-text-header">Filter</span>
    </div>
</a>

<div class="form-filter3" id="form-filter3">
    <div class="row">
        <div class="col-12">
            <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                    <i class="fas fa-filter mr-2"></i>
                    <span class="text-white form-filter3-text-header">Filter</span>
                </div>
                <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                    <i style="font-size:14px;" class="fas fa-times text-white"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="row mb-3 px-30">
        <div class="col-12">
            <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
            <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
        </div>
    </div>
    <hr>
    <form id="form-filter-sale-order" onsubmit="return false" class="px-30">
        <div class="row justify-content-end align-items-center">
            <div class="col-md-12 mb-3">
                <label>Tanggal</label>
                <div id="reportrange" class="form-control rounded-0 mr-2" name="date-range" style="width:auto;">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <label>Pilih Cabang yang ingin dibandingkan</label>
                <select name="md_merchant_id" id="branch" class="form-control" multiple="multiple">
                    <option value="-1">Semua Cabang</option>
                    @foreach(get_cabang() as $key  =>$item)
                        <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-12">
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <button type="submit" class="btn btn-success btn-block" style="padding:10px 20px!important;">Terapkan</button>
                <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>

            </div>

        </div>

    </form>

</div>

@endsection


@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>

<script>

     $(function() {

        var start = moment().startOf('month');
        var end = moment().endOf('month');

        $('.period-date').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
            }
        }, cb);
        cb(start, end);

    });
</script>

<script>
$(document).ready(function(){
    $('#branch').select2();
    const resetCanvas = () => {
        $('#myChart').remove();
        $('#myChart-wrapper').append('<canvas id="myChart" width="400" height="80"></canvas>');
    }

    $('#reportrange').on('apply.daterangepicker', (e, picker) => {

        var start = picker.startDate._d;
        var end = picker.endDate._d;
        $('.period-date').html(moment(start,'MMMM D, YYYY').format('MMMM D, YYYY') + ' - ' + moment(end,'MMMM D, YYYY').format('MMMM D, YYYY'));

        $('.btn-int').removeAttr('disabled');
        if(picker.chosenLabel == 'Tahun Ini' || picker.chosenLabel == 'Tahun Lalu' || picker.chosenLabel == '90 Hari Terakhir'){
            $('.btn-int[data-interval="day"]').prop('disabled', 'true');
            $('.btn-int[data-interval="month"]').click();
            return;
        }

        resetCanvas();
        loadChart();
        loadData();
    });

    $('#dataPerPage').on('change', function(){
        resetCanvas();
        loadChart();
        loadData();
    });

    $('#branch').on('change', function(){
        resetCanvas();
        loadChart();
        loadData();
    });

    $('.btn-type').on('click', function(){
        const btnTypes = [...document.querySelectorAll('.btn-type')];
        btnTypes.forEach(item => {
            if(item.classList.contains('btn-bg__orange')){
                item.classList.remove('btn-bg__orange', 'text-white');
                item.classList.add('btn-light', 'text-muted');
            }
        });

        $(this).removeClass('btn-light text-muted');
        $(this).addClass('btn-bg__orange text-white');
        const type = $(this).attr('data-type');
        $('#type').val(type);
        resetCanvas();
        loadChart();
        loadData();
    });

    $('.btn-int').on('click', function(){
        const btnIntervals = [...document.querySelectorAll('.btn-int')];
        btnIntervals.forEach(item => {
            if(item.classList.contains('btn-bg__orange')){
                item.classList.remove('btn-bg__orange', 'text-white');
                item.classList.add('btn-light', 'text-muted');
            }
        });

        $(this).removeClass('btn-light text-muted');
        $(this).addClass('btn-bg__orange text-white');
        const interval = $(this).attr('data-interval');
        $('#interval').val(interval);
        resetCanvas();
        loadChart();
        loadData();
    });

    const loadChart = () => {
        var startDate = $('#reportrange').data('daterangepicker').startDate._d;
        var endDate = $('#reportrange').data('daterangepicker').endDate._d;

        const data = new FormData();
        data.append('interval', $('#interval').val());
        data.append('md_merchant_id',$('#branch').val());
        data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        ajaxTransfer("{{route('merchant.report.sale-order.type-buyer')}}", data,function(response){
            const responseData = JSON.parse(response);
            const borderColor = [
                "#ffeb3b6b","#FFD028","#F2B04B","#FFADAD","#fc4949"
            ];
            const labels = [];
            const titleType = $('#type').val();
            let html;
            $('#data-result').html('');
            $('#total-result').html('');
            if(responseData.code != 200 || responseData.data.table.length < 1){
                html = `<tr>
                            <td class="text-center" colspan="3">Data laporan tidak ditemukan</td>
                        </tr>
                        `;
                $('#data-result').append(html);
                return;
            }

            // const timeLabels = responseData.data.table[0].branch[0].get_detail_selling;
            // timeLabels.forEach(item => {
            //     labels.push(item.time);
            // });

            // const datasets = responseData.data.table.map((item, index) => {
            //     let branch = item.branch;
            //     return {
            //         label: item.paytype,
            //         backgroundColor: borderColor[index],
            //         data: labels.map((l) => {
            //             let total = 0;
            //             return {
            //                 time: l,
            //                 branch: branch.map(b => {
            //                     return {
            //                         name: b.branch_name,
            //                         sub_qty: b.get_detail_selling.filter(gd => {
            //                             return gd.time == l;
            //                         }).map(obj => {
            //                             total += obj.trans_amount;
            //                             return obj.trans_amount;
            //                         }).shift()
            //                     }
            //                 }),
            //                 total:total
            //             }
            //         })
            //     }
            // });

            // console.log(datasets);

            // const data = {
            //     labels: labels,
            //     datasets: datasets
            // };

            // const config = {
            //     type: 'bar',
            //     data: data,
            //     options: {
            //         parsing: {
            //             xAxisKey: 'time',
            //             yAxisKey: 'total'
            //         },
            //         plugins: {
            //             legend:{
            //                 display: true,
            //                 position: 'bottom'
            //             },
            //             title: {
            //                 display: true,
            //                 text: titleType.toUpperCase(),
            //                 font: {
            //                     size: 19
            //                 }
            //             },
            //             tooltip:{
            //                 callbacks: {
            //                     label: function(context){
            //                         return "";
            //                     },
            //                     footer:function(cb){
            //                         let text = "";
            //                         cb.forEach(item => {
            //                             item.raw.detail.forEach(i => {
            //                                 text += i.name + " : " +currencyFormat(parseFloat(i.value), 'rp') + "\n";
            //                             });
            //                         });
            //                         return text;
            //                     }
            //                 }
            //             }
            //         }
            //     },
            // };

            // const myChart = new Chart(
            //     document.getElementById('myChart'),
            //     config
            // );

            let total_product = 0;
            let total_price = 0;

            console.log(responseData.data.table);
            responseData.data.table.forEach(item => {
                
                html += `<tr style="background-color:#F8F9FC;">
                            <td class="font-weight-bold" colspan="3">${item.paytype}</td>
                        </tr>`;
                item.branch.forEach(i => {
                    html += `<tr>
                                <td style="padding-left:30px;">${i.branch_name}</td>
                                <td>${currencyFormat(parseFloat(i.trans_amount), '')}</td>
                            </tr>`;
                    total_product += i.qty;
                    total_price += i.price;
                });
            });

            let totalHtml = `<tr style="background-color:#F8F9FC;">
                                <td class="font-weight-bold">TOTAL</td>
                                <td>${total_product}</td>
                                <td>${currencyFormat(parseFloat(total_price), '')}</td>
                            </tr>`;    

            $('#data-result').append(html);
            $('#total-result').html(totalHtml);
        });
    }

    loadChart();
})

const reloadDataTable = () => void 0;
const exportData = () => {
    modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
        var startDate = $('#reportrange').data('daterangepicker').startDate._d;
        var endDate = $('#reportrange').data('daterangepicker').endDate._d;

        const interval = $('#interval').val();

        const data = new FormData();
        data.append('interval', interval);
        data.append('md_merchant_id',$('#branch').val());
        data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));

        ajaxTransfer("{{route('merchant.report.sale-order.export-type-buyer')}}", data, '#modal-output');
    });

}


</script>
@endsection
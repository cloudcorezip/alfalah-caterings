@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .select2-container .select2-selection--multiple .select2-selection__rendered  {
            display: grid!important;
            grid-template-columns: 1fr 1fr;
        }

        .table-responsive {
            max-height: 80vh;
            overflow-y: auto;
        }
        .table-responsive thead {
            position: sticky;
            top: 0;
        }
        .table-responsive tfoot {
            position: sticky;
            bottom: 0;
        }
    </style>
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all', ['page'=>'sale-order'])}}">Penjualan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Disini, kamu bisa melihat detail penjualan berdasarkan customer atau pelangganmu produk dari yang teratas atau sebaliknya</span>

            </div>
        </div>
    </div>



    <div class="container-fluid mb-4">
        <div class="card shadow mb-4">
            <div class="card-header d-flex justify-content-between align-items-center">
                <div class="py-3">
                    <h4 class="font-weight-bold">Laporan Pelanggan</h4>
                    <small class="text-muted period-date">-</small>
                </div>
                <div>
                    <button
                        class="btn btn-bg__orange rounded-0 text-white btn-int"
                        data-interval="day"
                    >Hari
                    </button>
                    <button
                        class="btn btn-light rounded-0 text-muted btn-int"
                        data-interval="month"
                    >Bulan
                    </button>
                    <button
                        class="btn btn-light rounded-0 text-muted btn-int"
                        data-interval="year"
                    >Tahun
                    </button>
                    <input type="hidden" id="interval" name="interval" value="day">
                </div>
            </div>
            <div class="card-body" id="myChart-wrapper">
                <canvas id="myChart" width="400" height="80"></canvas>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card shadow mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="py-3">
                            <h4 class="font-weight-bold">Rincian Laporan</h4>
                            <small class="text-muted period-date">-</small>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-custom" width="100%" cellspacing="0">
                                <thead>
                                <tr style="background-color:#FFF;">
                                    <th class="font-weight-bold">Pelanggan</th>
                                    <th class="font-weight-bold text-center">Total Transaksi</th>
                                    <th class="font-weight-bold text-right">Total Penjualan</th>
                                </tr>
                                </thead>
                                <tbody id="data-result">

                                </tbody>
                                <tfoot id="total-result"></tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="py-3">
                            <h4 class="font-weight-bold">Insight</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <h6 class="font-weight-bold">Pelanggan Teratas</h6>
                            <div id="best-selling-product">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter" onsubmit="return false" class="px-30">
            <div class="row justify-content-end align-items-center">
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="startDate" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date" required>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" style="background: white" id="endDate" value="{{$endDate}}" name="end_date" required>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Cabang yang ingin dibandingkan</label>
                    <select name="md_merchant_id" id="branch" class="form-control" multiple="multiple">
                        <option value="-1">Semua Cabang</option>
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Pelanggan yang ingin ditampilkan</label>
                    <select class="form-control" name="sc_customer_id" id="sc_customer_id" multiple="multiple" required>
                        <option value="-1">Semua Pelanggan</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <button type="submit" class="btn btn-success btn-block" style="padding:10px 20px!important;">Terapkan</button>
                    <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>

                </div>

            </div>

        </form>

    </div>


@endsection


@section('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>
    <script>
        $(document).ready(function(){
            getCustomer()
            $('#branch').select2();
            const resetCanvas = () => {
                $('#myChart').remove();
                $('#myChart-wrapper').append('<canvas id="myChart" width="400" height="80"></canvas>');
            }

            $('.btn-int').on('click', function(){
                const btnIntervals = [...document.querySelectorAll('.btn-int')];
                btnIntervals.forEach(item => {
                    if(item.classList.contains('btn-bg__orange')){
                        item.classList.remove('btn-bg__orange', 'text-white');
                        item.classList.add('btn-light', 'text-muted');
                    }
                });
                $(this).removeClass('btn-light text-muted');
                $(this).addClass('btn-bg__orange text-white');
                const interval = $(this).attr('data-interval');
                $('#interval').val(interval);
                resetCanvas();
                loadChart();
            });

            $("#branch").on("select2:select", function(e){
                if(e.params.data.id == "-1"){
                    $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
                } else {
                    $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
                }
            });

            $("#sc_customer_id").on("select2:select", function(e){
                if(e.params.data.id == "-1"){
                    $("#sc_customer_id > option[value !='-1']").prop("selected",false).trigger("change");
                } else {
                    $("#sc_customer_id > option[value ='-1']").prop("selected",false).trigger("change");
                }
            });



            $('#form-filter').submit(function () {
                resetCanvas();
                loadChart();
                showFilter('btn-show-filter3', 'form-filter3');
            });

            const loadChart = () => {
                let startDate = $('#startDate').val();
                let endDate = $('#endDate').val();
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let selectedProduct = $("#sc_customer_id").select2('data');
                let merchantIds = [];
                let products = [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json($branch);
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }

                // cek jika pilih semua produk
                let md_merchant_id,req_product_code;

                if(selectedProduct.length<1)
                {
                    req_product_code='';

                }else{
                    if(selectedProduct.findIndex(a => a.id == '-1') >= 0){
                        req_product_code='';
                    } else {
                        selectedProduct.forEach(item => {
                            products.push(`'${item.id.trim()}'`);
                        });
                        req_product_code = `and spc.id in(${products.join(',')})`;

                    }
                }


                md_merchant_id = merchantIds.join(',');

                const data = new FormData();
                data.append('interval', $('#interval').val());
                data.append('md_merchant_id',md_merchant_id);
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('customer_id', req_product_code);

                $('.period-date').html(moment(startDate).format('MMMM D, YYYY') + ' - ' + moment(endDate).format('MMMM D, YYYY'));
                ajaxTransfer("{{route('merchant.report.sale-order.cust-sell')}}", data,function(response){
                    const responseData = JSON.parse(response);
                    loadSideGraph(responseData);
                    const borderColor = [
                        "#ffeb3b6b","#FFD028","#F2B04B","#FFADAD","#fc4949"
                    ];
                    const labels = [];
                    let html;
                    $('#data-result').html('');
                    $('#total-result').html('');
                    if(responseData.code != 200 || responseData.data.table.length < 1){
                        html = `<tr>
                            <td class="text-center" colspan="3">Data laporan tidak ditemukan</td>
                        </tr>
                        `;
                        $('#data-result').append(html);
                        return;
                    }

                    const timeLabels = responseData.data.table[0].branch[0].get_detail;
                    timeLabels.forEach(item => {
                        labels.push(item.time);
                    });

                    const datasets = responseData.data.table.map((item, index) => {
                        let branch = item.branch;
                        return {
                            label: item.customer,
                            backgroundColor: borderColor[index],
                            data: labels.map((l) => {
                                let total = 0;
                                let amount = 0;
                                return {
                                    time: l,
                                    branch: branch.map(b => {
                                        return {
                                            name: b.branch_name,
                                            sub_qty: b.get_detail.filter(gd => {
                                                return gd.time == l;
                                            }).map(obj => {
                                                total += obj.trans_count;
                                                amount += obj.trans_amount;

                                                return obj.trans_count;
                                            }).shift()
                                        }
                                    }),
                                    quantity: total,
                                    amount: amount
                                }
                            })
                        }
                    });

                    const data = {
                        labels: labels,
                        datasets: datasets
                    };

                    const config = {
                        type: 'bar',
                        data: data,
                        options: {
                            parsing: {
                                xAxisKey:"time",
                                yAxisKey:"quantity"
                            },
                            plugins: {
                                legend:{
                                    display: true,
                                    position: 'bottom'
                                },
                                tooltip: {
                                    callbacks: {
                                        label: function(context){
                                            return context.raw.name;
                                        },
                                        footer: function(cb){
                                            let text = "";
                                            cb.forEach(item => {
                                                item.raw.branch.forEach(i => {
                                                    text += i.name + " : " + i.sub_qty + "\n";
                                                })
                                            });
                                            return text;
                                        },
                                        afterFooter: function(tooltipItem){
                                            let text="";
                                            tooltipItem.forEach(item => {
                                                text += "Total : " + item.raw.quantity +"\n";
                                                text += "Total Pembelian : " + currencyFormat(item.raw.amount,'');

                                            });

                                            return text;
                                        }
                                    }
                                }
                            }
                        }
                    };

                    const myChart = new Chart(
                        document.getElementById('myChart'),
                        config,
                    );


                    let total_product = 0;
                    let total_price = 0;
                    responseData.data.table.forEach(item => {
                        html += `<tr style="background-color:#F8F9FC;">
                            <td class="font-weight-bold" colspan="3">${item.customer}</td>
                        </tr>`;
                        item.branch.forEach(i => {
                            html += `<tr>
                                <td style="padding-left:30px;">${i.branch_name}</td>
                                <td class="text-center">${i.trans_count}</td>
                                <td class="text-right">${currencyFormat(parseFloat(i.trans_amount), '')}</td>
                            </tr>`;
                            total_product += i.trans_count;
                            total_price += i.trans_amount;
                        });
                    });

                    let totalHtml = `<tr style="background-color:#F8F9FC;">
                                <td class="font-weight-bold">TOTAL</td>
                                <td class="text-center"><b>${total_product}</b></td>
                                <td class="text-right"><b>${currencyFormat(parseFloat(total_price), '')}</b></td>
                            </tr>`;

                    $('#data-result').append(html);
                    $('#total-result').html(totalHtml);
                });
            }

            loadChart();
        })

        const reloadDataTable = () => void 0;
        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                let startDate = $('#startDate').val();
                let endDate = $('#endDate').val();
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let selectedProduct = $("#sc_customer_id").select2('data');
                let merchantIds = [];
                let products = [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json($branch);
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }

                // cek jika pilih semua produk
                let md_merchant_id,req_product_code;

                if(selectedProduct.length<1)
                {
                    req_product_code='';

                }else{
                    if(selectedProduct.findIndex(a => a.id == '-1') >= 0){
                        req_product_code='';
                    } else {
                        selectedProduct.forEach(item => {
                            products.push(`'${item.id.trim()}'`);
                        });
                        req_product_code = `and spc.id in(${products.join(',')})`;

                    }
                }


                md_merchant_id = merchantIds.join(',');

                const data = new FormData();
                data.append('interval', $('#interval').val());
                data.append('md_merchant_id',md_merchant_id);
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('customer_id', req_product_code);

                ajaxTransfer("{{route('merchant.report.sale-order.export-customer-sell')}}", data, '#modal-output');
            });

        }

        const loadSideGraph = (param) => {
            const response = param.data.table;
            const data = [];
            let backgroundColor = ['#FE8F2D', '#939393', '#B2B1B1', '#C4C4C4', '#E0E0E0'];
            let arrQty = [];

            if(param.code != 200){
                $("#not-selling-product").html(`
            <div class="col-md-12">
                <span class="d-block text-center">Tidak ada data yang tersedia</span
            </div>
        `);
                return;
            }

            response.forEach(item => {
                let q = item.branch.reduce((acc, curr) => {
                    return acc + curr.trans_count;
                }, []);
                arrQty.push(q);
            });

            let total = arrQty.reduce((acc, curr) => {
                return acc + parseInt(curr);
            },0);

            response.forEach((item,index) => {
                let qty = item.branch.reduce((acc, curr) => {
                    return acc + curr.trans_count;
                }, 0);
                let width = (Math.floor(qty / total * 100))-20;
                data.push(
                    {
                        product:item.customer,
                        quantity: qty,
                        width:width,
                        background:backgroundColor[index]
                    }
                )
            });

            viewBestSellingProduct(data);
        }

        const viewBestSellingProduct = (param) => {
            const response = [...param].sort((a, b) => b.quantity - a.quantity);
            const data = response.filter(item => {
                return item.quantity > 0;
            }).slice(0,5);

            let html = `<div class='mb-3' style='width:100%;'>`;
            data.forEach((item, index) => {
                html += `<div class="d-flex align-items-center justify-content-between">
                    <div class="py-2 px-4 rounded-right" style="width:${item.width}%; background-color:${item.background}">
                    </div>
                    <span class="py-2">${item.quantity}</span>
                </div>
                <div><small>${item.product}</small></div>

`;
            });
            html += "</div>";

            if(data.length > 0){
                $("#best-selling-product").html(html);
            } else {
                $("#best-selling-product").html(`
            <div class="mb-3" style='width:100%;'>
                <span class="d-block text-center">Tidak ada data yang tersedia</span>
            </div>
        `);
            }
        }

        function getCustomer() {
            $("#sc_customer_id").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.transaction.sale-order.customer-list')}}",
                    dataType: 'json',
                    delay: 250,
                    headers: {
                        "senna-auth": "{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        let datas=[];
                        $.each(data,function(i,v){
                            if(i==0)
                            {
                                datas.push({
                                    id:'-1',
                                    text:'Semua Pelanggan'
                                })
                            }
                            datas.push(v)
                        })
                        return {
                            results: datas
                        };
                    },
                    cache: true
                },
            });
        }

    </script>


@endsection



<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Waktu Pembayaran</label>
                <input type="text" class="form-control form-control-sm trans_time" name="payment_paid_date" placeholder="Pilih Tanggal" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Kode Pembayaran</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Pembayaran">
            </div>
        </div>
        <div class="col-md-6 input1">
            <div class="form-group input1" >
                <label id="labelTo" class="font-weight-bold">Metode Pembayaran</label>
                <select id="trans" class="form-control form-control-sm" name="payment_paid_id" required>
                    <option value="-1">Pilih Metode Pembayaran</option>
                    @foreach($payment as $item)
                        <optgroup label="{{$item['name']}}">
                            @if(count($item['data'])>0)
                                @if($item['name']=='Pembayaran Digital')
                                    @foreach($item['data'] as $d)
                                        <optgroup label="--{{$d['name']}}">
                                            @if(!empty($d['data']))
                                                @foreach($d['data'] as $c)
                                                    <option value="{{$c['acc_coa_id']}}">{{$c['name']}} </option>
                                                @endforeach
                                            @endif
                                        </optgroup>
                                    @endforeach
                                @else
                                    @foreach($item['data'] as $key =>$other)
                                        <option value="{{$other->acc_coa_id}}">{{$other->name}}</option>

                                    @endforeach
                                @endif
                            @endif
                        </optgroup>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Jumlah Pembayaran</label>
                <input type="text" name="paid_nominal" class="form-control amount_currency" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="{{rupiah($totalAmount)}}" required disabled>

            </div>
        </div>

        <div class="col-md-6 admin-fee">
            <div class="form-group admin-fee">
                <label for="exampleInputPassword1" class="admin-fee font-weight-bold">Biaya Administrasi</label>
                <input type="text" name="admin_fee" class="form-control amount_currency" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="0" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold">Bukti Transaksi</label>
                <input type="file" class="form-control form-control-sm"  name="payment_trans_proof">
                <small class="d-block mt-2" style="color:#EE6767;">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</small>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label class="font-weight-bold">Catatan</label>
                <textarea name="desc" class="form-control form-control-sm" cols="5"></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='merchant_id' value='{{$merchantId}}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='opt' value='{{ $opt }}'>
    <input type='hidden' name='ids' value='{{ $ids }}'>
    <input type='hidden' name='coa_commission_id' value='{{ $coaCommissionId}}'>
    <input type='hidden' name='start_date' value='{{ $startDate}}'>
    <input type='hidden' name='end_date' value='{{ $endDate}}'>
    <input type='hidden' name='count_data' value='{{ $countData}}'>
    <input type='hidden' name='merchant_ids' value='{{ $merchantIds}}'>



</form>

<script>
    $(document).ready(function () {
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        dateTimePicker(".trans_time");
        $("#trans").select2();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.report.employee.save-pay-commission')}}", data, function (response){
                var data = JSON.parse(response);
                if(data.type=='success'){
                    otherMessage(data.type,data.message)
                    closeModalAfterSuccess()
                    reload(1000)
                }else{
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)

                }
            });
        })

    })


</script>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah pembayaran komisi, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });
</script>

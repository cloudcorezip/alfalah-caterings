@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all', ['page'=>'employee'])}}">Karyawan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>

                <span>Disini, Kamu bisa melihat komisi setiap karyawanmu selama periode tertentu</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @include('merchant::alert')

        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="80%" style="padding: 0.1rem"><h6><b>Laporan Komisi</b></h6></td>
                <td rowspan="2" class="text-right" style="padding: 0.1rem">
                    <a class="btn btn-success" onclick="exportData()"><i class="fa fa-download mr-2"></i> Download Laporan</a>

                </td>
            </tr>
            <tr>
                <td style="padding: 0.1rem"><h6><b>Periode {{\Carbon\Carbon::parse($startDate)->isoFormat('D MMMM Y')}} - {{\Carbon\Carbon::parse($endDate)->isoFormat('D MMMM Y')}}</b></h6></td>
            </tr>
        </table>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card rounded">
                            <div class="card-body">
                                <h6 class="font-weight-bold">Total Komisi</h6>
                                <h5 style="color:#FA6D1D;" class="text-right font-weight-bold">{{$data->total_amount}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card rounded">
                            <div class="card-body">
                                <h6 class="font-weight-bold">Terbayarkan</h6>
                                <h5 style="color:#FA6D1D;" class="text-right font-weight-bold">{{$data->total_paid}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card rounded">
                            <div class="card-body">
                                <h6 class="font-weight-bold">Belum Terbayarkan</h6>
                                <h5 style="color:#FA6D1D;" class="text-right font-weight-bold">{{$data->total_unpaid}}
                                <input type="hidden" id="total_unpaid" value="{{$data->total_unpaid_original}}">
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-info ml-2" onclick="loadModal(this)" id="pay-commission" data="id=-1" target="{{route('merchant.report.employee.add-pay-commission')}}?merchant_id={{merchant_id()}}" style="color: white">Proses Pembayaran Komisi</a>
                        <a class="btn btn-warning ml-2" onclick="rejectPayCommission()" style="color: white">Batalkan Pembayaran Komisi</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-custom" id="table-data-hpp" width="100%" cellspacing="0">
                        <thead>
                        <tr style="background: white;border-top: 1px solid #ececec" >
                            <th>
                                <input type="checkbox" class="check-all">
                                <input type="hidden" id="is_check_all" value="0">
                            </th>
                            <th style="width: 15%">Nama Karyawan</th>
                            <th>Jabatan</th>
                            <th>Grade</th>
                            <th>Cabang</th>
                            <th>Gaji Pokok</th>
                            <th>Total Komisi</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($data->data->count()<1)
                            <tr>
                                <td colspan="8" class="text-center"><small>Data karyawan tidak tersedia</small></td>
                            </tr>
                        @else
                        @foreach($data->data as $key =>$item)
                           <tr style="background: #ececec">
                               <td></td>
                               <td class="font-weight-bold">{{$item->fullname}}</td>
                               <td class="font-weight-bold">{{$item->job_name}}</td>
                               <td class="font-weight-bold"> {{$item->grade_name}}</td>
                               <td class="font-weight-bold">{{$item->merchant_name}}</td>
                               <td class="font-weight-bold">{{$item->basic_salary}}</td>
                               <td class="font-weight-bold">{{$item->commission_amount}}</td>
                               <td></td>
                           </tr>
                           @if(count($item->details)>0)
                               <tr style="background: #f8f9fa">
                                   <td></td>
                                   <td class="text-right">
                                   </td>
                                   <td colspan="3"><b>Nama Komisi</b></td>
                                   <td><b>Ref. Transaksi</b></td>
                                   <td><b>Nilai Komisi</b></td>
                                   <td></td>
                               </tr>
                               @foreach($item->details as $d)
                                   <tr>
                                       <td></td>
                                       <td data-id="{{$d->id}}" class="text-right">
                                           <input type="checkbox" class="cb-all" value="{{$d->id}}">
                                       </td>
                                       <td colspan="3">
                                           {{$d->commission_name}}
                                       </td>
                                       <td>
                                           <a href="" target="_blank">{{$d->sale_code}}</a>
                                       </td>
                                       <td>
                                           <input type="hidden" id="commission-value{{$d->id}}" value="{{$d->total_original}}">
                                           {{$d->total}}
                                       </td>
                                       <td>@if($d->is_paid==0)
                                               <label class="badge badge-danger"><small>Belum Terbayarkan</small></label>
                                            @else
                                               <label class="badge badge-success"><small>Terbayarkan</small></label>
                                           @endif
                                       </td>
                                   </tr>

                               @endforeach
                           @endif
                        @endforeach

                        @endif
                        </tbody>
                    </table>
                    <br>
                    {{$data->data->withQueryString()->links('vendor.pagination.with-showing-entry')}}
                </div>

            </div>
        </div>
    </div>



    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>
    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold mt-3 text-dark">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-hpp" class="px-30" method="get">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="startDate" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date" required>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" style="background: white" id="endDate" value="{{$endDate}}" name="end_date" required>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Cabang yang ingin dibandingkan</label>
                    <select name="md_merchant_id[]" id="branch" class="form-control" multiple="multiple" required>
                        <option value="-1"  @if(in_array('-1',$merchantId)) selected @endif>Semua Cabang</option>
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" @if(in_array($item->id,$merchantId)) selected @endif>{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3 mt-3">
                    <input type="hidden" name="start_date" id="start_date" value="{{$startDate}}">
                    <input type="hidden" name="end_date" id="end_date" value="{{$endDate}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a href="{{route('merchant.report.employee.commission')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')

    <script>
        function exportData() {

            modalConfirm('Export Data', 'Lanjutkan proses export data?', function () {
                var data = getFormData('form-filter-sale-order');
                let startDate = $('#startDate').val();
                let endDate = $('#endDate').val()
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json(get_cabang());
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }
                data.append('md_merchant_id',merchantIds);
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                ajaxTransfer("{{route('merchant.report.employee.export-commission')}}", data, '#modal-output');
            });

        }

        $("#branch").on("select2:select", function(e){
            if(e.params.data.id == "-1"){
                $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
            } else {
                $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
            }
        });

        $(document).ready(function () {
            $("#branch").select2({});
            $("#checked-action").select2()
            $('#form-filter-hpp').submit(function () {
                let startDate = $('#reportrange').data('daterangepicker').startDate._d;
                let endDate = $('#reportrange').data('daterangepicker').endDate._d;
                $("#start_date").val(moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'))
                $("#end_date").val(moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'))
            });
        });


        $(".check-all").change(function() {
            if(this.checked) {
                $("#is_check_all").val(1);
            }else{
                $("#is_check_all").val(0);
            }
            $(".cb-all").prop('checked', $(this).prop("checked"));
            calculateCheckbox()
        });

        $(".cb-all").change(function() {
           calculateCheckbox()
        })


        function calculateCheckbox()
        {
            let checkbox = $('.cb-all');
            let ids = [];
            let commissionValue=[];
            let i, value;
            let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
            let merchantIds = [];
            if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                let branch = @json(get_cabang());
                branch.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
            } else {
                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
            }

            for (i = 0; i < checkbox.length; i++) {
                value = $(checkbox[i]).val();
                if ($(checkbox[i]).prop('checked')) {
                    ids.push(parseInt(value));
                    commissionValue.push($("#commission-value"+value).val())
                }
            }
            let data = {
                opt:1,
                ids:ids,
                commissionValue:commissionValue,
                totalUnpaid:$("#total_unpaid").val(),
                startDate:'{{$startDate}}',
                endDate:'{{$endDate}}',
                countData:{{$data->count_data}},
                merchantIds:merchantIds

            }
            $('#pay-commission').attr("data","id="+JSON.stringify(data)+"");
        }


        function rejectPayCommission() {
            let data = new FormData();
            let checkbox = $('.cb-all');
            let ids = [];
            let i, value;
            for (i = 0; i < checkbox.length; i++) {
                value = $(checkbox[i]).val();
                if ($(checkbox[i]).prop('checked')) {
                    ids.push(parseInt(value));
                }
            }

            if (ids.length == 0) {
                otherMessage('warning','Terjadi kesalahan! Setidaknya memilih satu data')
                return false;
            }
            let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
            let merchantIds = [];
            if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                let branch = @json(get_cabang());
                branch.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
            } else {
                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
            }
            let id_s = ids.join(',');
            let md_merchant_id = merchantIds.join(',');

            data.append('ids',id_s)
            data.append('is_check_all',$("#is_check_all").val())
            data.append('count_data_paid',{{$data->count_data_paid}})
            data.append('start_date','{{$startDate}}')
            data.append('end_date','{{$endDate}}')
            data.append('merchant_ids',md_merchant_id)
            modalConfirm("Konfirmasi", "Apa anda yakin membatalkan pembayaran komisi dari tanggal {{$startDate}} sd {{$endDate}}?", function () {
                ajaxTransfer("{{route('merchant.report.employee.reject-commission')}}", data,function (response){
                    var data = JSON.parse(response);
                    if(data.type=='success'){
                        otherMessage(data.type,data.message)
                        closeModalAfterSuccess()
                        reload(1000)
                    }else{
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)

                    }
                });
            })
        }


    </script>

@endsection

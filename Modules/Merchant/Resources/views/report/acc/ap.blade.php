@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')

<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="row p-5 d-flex align-items-center">
            <div class="col-lg-4 col-md-4 mb-2">
                <h4>{{$title}}</h4>
            </div>
            <div class="col-lg-8 col-md-8 d-flex align-items-center justify-content-md-end mb-2">
                <div id="reportrange" class="form-control rounded-0 mr-2" name="date-range" style="width:auto;">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                </div>
                <div class="col-md-4">
                    <select name="md_merchant_id" id="branch" class="form-control mr-2">
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-bg__orange rounded-0 text-white text-nowrap" onclick="exportData()">
                    <i class="fa fa-download mr-2"></i>
                    Download Laporan
                </button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mb-4">
    <div class="row">
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <div class="py-3">
                        <h4 class="font-weight-bold">Utang Usaha</h4>
                        <small class="text-muted period-date">-</small>
                    </div>
                </div>
                <div class="card-body" id="myChart-wrapper">
                    <canvas id="myChart" width="400" height="100"></canvas>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <div class="py-3">
                        <h4 class="font-weight-bold">Utang Usaha</h4>
                        <small class="text-muted period-date">-</small>
                    </div>
                    <div class="py-3">
                        <select class="form-control" name="dataPerPage" id="dataPerPage">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Pemberi Hutang</th>
                                    <th>Total Hutang</th>
                                    <th>Hutang Terbayar</th>
                                </tr>
                            </thead>
                            <tbody id="data-result">

                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-end" id="pagination-result">

                            </ul>
                        </nav>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>

<script>

     $(function() {

        var start = moment().startOf('month');
        var end = moment().endOf('month');

        $('.period-date').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
            }
        }, cb);
        cb(start, end);

    });
</script>

<script>
$(document).ready(function(){
    $('#branch').select2();

    const resetCanvas = () => {
        $('#myChart').remove();
        $('#myChart-wrapper').append('<canvas id="myChart" width="400" height="80"></canvas>');

    }

    $('#reportrange').on('apply.daterangepicker', (e, picker) => {

        var start = picker.startDate._d;
        var end = picker.endDate._d;
        $('.period-date').html(moment(start,'MMMM D, YYYY').format('MMMM D, YYYY') + ' - ' + moment(end,'MMMM D, YYYY').format('MMMM D, YYYY'));

        resetCanvas();
        loadChart();
        loadData();
    });

    $('#dataPerPage').on('change', function(){
        resetCanvas();
        loadChart();
        loadData();
    });
    $('#branch').on('change', function(){
        resetCanvas();
        loadChart();
        loadData();
    });

    const loadChart = () => {

        var startDate = $('#reportrange').data('daterangepicker').startDate._d;
        var endDate = $('#reportrange').data('daterangepicker').endDate._d;

        const limit = Math.ceil((endDate-startDate)/1000/60/60/24);

        const data = new FormData();
        data.append('interval', $('#interval').val());
        data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('limit', 5);
        data.append('md_merchant_id',$('#branch').val());
        ajaxTransfer("{{route('merchant.report.acc.ap-report')}}", data,function(response){
            const responseData = JSON.parse(response);
            const borderColor = [
                "#ffeb3b6b","#FFD028","#F2B04B","#FFADAD","#fc4949"
            ];

            const labels = [];
            const datasets = [{
                label: 'Data Hutang',
                data: [],
                backgroundColor: [],
                hoverOffset: 4
            }];

            responseData.data.forEach((item, index) => {
                labels.push(item.ap_purpose);
                datasets[0].data.push(item.ap_amount);
                datasets[0].backgroundColor.push(borderColor[index]);
            });

            const data = {
                labels: labels,
                datasets: datasets
            };

            const config = {
                type: 'pie',
                data: data,
                options: {
                    plugins: {
                        legend:{
                            display: true,
                            position: 'bottom'
                        }
                    }
                }
            };

            const myChart = new Chart(
                document.getElementById('myChart'),
                config,
            );
        });
    }

    loadChart();



    const loadData = (offset=0) => {

        var startDate = $('#reportrange').data('daterangepicker').startDate._d;
        var endDate = $('#reportrange').data('daterangepicker').endDate._d;

        const totalData= "{{$totalData}}";
        const totalPage = Math.ceil(totalData / $('#dataPerPage').val());

        const data = new FormData();
        data.append('interval', $('#interval').val());
        data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('limit', $('#dataPerPage').val());
        data.append('md_merchant_id',$('#branch').val());
        data.append('offset', offset);
        ajaxTransfer("{{route('merchant.report.acc.ap-report')}}", data,function(response){
            const responseData = JSON.parse(response);

            $('#data-result').html('');
            if(responseData.code == 404){
                let html = "<tr>"+
                                "<td class='text-center' colspan='3'>Data laporan tidak ditemukan</td>"+
                            "</tr>";
                $('#data-result').append(html);
                return;
            }

            let html = "";

            responseData.data.forEach(item => {
                html += "<tr>"+
                            "<td>"+item.ap_purpose+"</td>"+
                            "<td>"+currencyFormat(parseFloat(item.ap_amount), 'rp')+"</td>"+
                            "<td>"+currencyFormat(parseFloat(item.paid_nominal), 'rp')+"</td>"+
                        "</tr>";
            });

            $('#data-result').append(html);

            let prevOffset;
            let nextOffset;

            if(parseInt(offset) + parseInt($('#dataPerPage').val()) < totalData){
                nextOffset = parseInt(offset) + parseInt($('#dataPerPage').val());
            } else {
                nextOffset = offset;
            }

            if(parseInt(offset) - parseInt($('#dataPerPage').val()) < 0){
                prevOffset = offset;
            } else {
                prevOffset = parseInt(offset) - parseInt($('#dataPerPage').val());
            }

            let pagination = "<li class='page-item'>"+
                                "<a class='page-link' aria-label='Previous' data-offset='"+prevOffset+"'>"+
                                    "<span aria-hidden='true'>&laquo;</span>"+
                                    "<span class='sr-only'>Previous</span>"+
                                "</a>"+
                            "</li>"
            for(let i = 0; i < totalPage; i++){
                if(offset == i * $('#dataPerPage').val()){
                    pagination += "<li class='page-item active'>"+
                                    "<a class='page-link' data-offset='"+i * $('#dataPerPage').val()+"'>"+(i+1)+"</a>"+
                                "</li>";
                } else {
                    pagination += "<li class='page-item'>"+
                                    "<a class='page-link' data-offset='"+i * $('#dataPerPage').val()+"'>"+(i+1)+"</a>"+
                                "</li>";
                }

            }

            pagination += "<li class='page-item'>"+
                            "<a class='page-link' aria-label='Next' data-offset='"+nextOffset+"'>"+
                                "<span aria-hidden='true'>&raquo;</span>"+
                                "<span class='sr-only'>Next</span>"+
                            "</a>"+
                        "</li>";
            $('#pagination-result').html(pagination);

        });
    }

    $(document).on('click','#pagination-result .page-item .page-link', function(){
        const offset = $(this).attr('data-offset');
        loadData(offset);
    })

    loadData();
})

const reloadDataTable = () => void 0;
const exportData = () => {
    modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
        var startDate = $('#reportrange').data('daterangepicker').startDate._d;
        var endDate = $('#reportrange').data('daterangepicker').endDate._d;

        let totalData = "{{$totalData}}";

        const data = new FormData();
        data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
        data.append('md_merchant_id',$('#branch').val());
        data.append('limit', totalData);

        ajaxTransfer("{{route('merchant.report.acc.export-ap')}}", data, '#modal-output');
    });

}



</script>
@endsection
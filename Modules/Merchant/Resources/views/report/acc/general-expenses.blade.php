@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')

<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a>Laporan</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.report-all')}}">Keuangan</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a href="#">Pengeluaran Umum</a>
                    </li>
                </ol>
            </nav>
            <span>Disini, kamu bisa melihat detail pengeluaran usahamu</span>
        </div>
    </div>
</div>

<div class="container-fluid mb-4">
    <div class="row">
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <div class="py-3">
                        <small class="text-muted period-date">-</small>
                    </div>
                </div>
                <div class="card-body" id="myChart-wrapper">
                    <canvas id="myChart" width="400" height="100"></canvas>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <div class="py-3">
                        <small class="text-muted period-date">-</small>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-custom" id="general-expense" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Jumlah</th>
                                    <th>Cabang</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>

        <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
            <div class="d-flex align-items-center">
                <i class="fas fa-filter mr-2 text-white"></i>
                <span class="text-white form-filter3-text-header">Filter</span>
            </div>
        </a>

        <div class="form-filter3" id="form-filter3" style="padding-bottom: 30px;">
            <div class="row">
                <div class="col-12">
                    <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center">
                            <i class="fas fa-filter mr-2"></i>
                            <span class="text-white form-filter3-text-header">Filter</span>
                        </div>
                        <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                            <i style="font-size:14px;" class="fas fa-times text-white"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="row mb-3 px-30">
                <div class="col-12">
                    <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                    <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
                </div>
            </div>
            <hr>
            <form id="form-filter-stock-opname" class="px-30" onsubmit="return false">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal Awal</label>
                            <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date" >
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal Akhir</label>
                            <input type="text" class="form-control form-control trans_time" style="background: white" id="end_date" value="{{$endDate}}" name="end_date">

                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label>Pilih Cabang yang ingin dibandingkan</label>
                        <select name="md_merchant_id[]" id="branch" class="form-control" multiple="multiple" required>
                            <option value="-1">Semua Cabang</option>
                            @foreach(get_cabang() as $key  =>$item)
                                <option value="{{$item->id}}">{{$item->nama_cabang}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="btn btn-success btn-block" type="submit" value="Terapkan" data-original-title="" title="" autocomplete="off">
                            <input name="is_download" type="hidden" value="0" autocomplete="off">
                        </div>
                    </div>
                </div>

            </form>
            <h4 class="font-weight-bold text-dark" style="margin-left: 20px;">Export</h3>
                
                <div class="" style="display: inline-table;margin: 0 20px 0 20px;">
                    <label>Export data berdasarkan</label>
                    </br>
                    <button 
                        class="btn btn-bg__orange rounded-0 btn_export_type"
                        type="button"
                        >Semua
                    </button>
                    <button 
                        class="btn rounded-0 btn_export_type"
                        type="button"
                        data-export-type="day"
                        >Per hari
                    </button>
                    <button
                        class="btn rounded-0 btn_export_type"
                        type="button"
                        data-export-type="month"
                    >Per Bulan
                    </button>
                    <button
                        class="btn rounded-0 btn_export_type"
                        type="button"
                        data-export-type="year"
                    >Per Tahun
                    </button>
                    
                <div class="row" style="margin: 0 2px 0 2px;" id="export-input-type">
                </div>
                    <a class="btn btn-info btn-block" onclick="exportData()" style="background: #DEF1FF!important;margin-top: 35px"><i class="fa fa-download mr-2"></i> Download Laporan</a>
                    <!-- <div class="form-group">
                        <label for="exampleInputPassword1">Bulan Awal</label>
                        <input type="text" class="form-control form-control trans_time_month" id="startDate" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date" required>
                    </div> -->
                </div>

        </div>
    </div>
</div>

@endsection


@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>

<script>

     $(function() {

        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
            }
        }, cb);
        cb(start, end);

    });
</script>

<script>
$(document).ready(function(){
    $('#branch').select2();
    dataTableGeneralExpense();
    getChart()


    $('#form-filter-stock-opname').submit(function () {
        var startDate =moment($('#start_date').val(),'YYYY-MM-DD').format('YYYY-MM-DD');
        var endDate = moment($('#end_date').val(),'YYYY-MM-DD').format('YYYY-MM-DD');

        let selectedMerchantId = $("#branch").select2('data');
        let merchantIds = [];
        selectedMerchantId.forEach(item => {
            merchantIds.push(parseInt(item.id));
        });
        let md_merchant_id = merchantIds.join(',');

        var data = getFormData('form-filter-stock-opname');
        data.append('start_date',startDate);
        data.append('end_date',endDate);
        data.append('md_merchant_id',md_merchant_id)

        ajaxTransfer("{{route('merchant.report.acc.exp-report')}}?is_datatable=0&start_date="+startDate+"&end_date="+endDate+"&md_merchant_id="+md_merchant_id+"", data, '#output-sale-order');
        dataTableGeneralExpense();
        resetChart()
        getChart()
    });
})

$('.btn_export_type').on('click', function(){
        const btnIntervals = [...document.querySelectorAll('.btn_export_type')];
        btnIntervals.forEach(item => {
            if(item.classList.contains('btn-bg__orange')) {
                item.classList.remove('btn-bg__orange');
            }
        });
        $(this).addClass('btn-bg__orange');
        const export_format = $(this).attr('data-export-type')
        showExportInput(export_format);
});


$("#branch").on("select2:select", function(e){
    if(e.params.data.id == "-1"){
        $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
    } else {
        $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
    }
});

function resetChart()
{
    $('#myChart').remove();
    $('#myChart-wrapper').append('<canvas id="myChart" width="400" height="80"></canvas>');

}

function getChart()
{

    var startDate =moment($('#start_date').val(),'YYYY-MM-DD').format('YYYY-MM-DD');
    var endDate = moment($('#end_date').val(),'YYYY-MM-DD').format('YYYY-MM-DD');

    let selectedMerchantId = $("#branch").select2('data');
    let merchantIds = [];
    selectedMerchantId.forEach(item => {
        merchantIds.push(parseInt(item.id));
    });
    let md_merchant_id = merchantIds.join(',');

    var data = getFormData('form-filter-stock-opname');
    data.append('start_date',startDate);
    data.append('end_date',endDate);
    data.append('md_merchant_id',md_merchant_id)
    ajaxTransfer("{{route('merchant.report.acc.exp-report')}}?is_datatable=0&start_date="+startDate+"&end_date="+endDate+"&md_merchant_id="+md_merchant_id+"", data,function(response){
        const responseData = JSON.parse(response);

        const borderColor = [
            "#ffeb3b6b","#FFD028","#F2B04B","#FFADAD","#fc4949"
        ];

        const labels = [];
        const datasets = [{
            label: 'Data Pengeluaran Umum',
            data: [],
            backgroundColor: [],
            hoverOffset: 4
        }];

        responseData.forEach((item, index) => {
            labels.push(item.nama_pengeluaran);
            datasets[0].data.push(item.total);
            datasets[0].backgroundColor.push(borderColor[index]);
        });

        const data = {
            labels: labels,
            datasets: datasets
        };

        const config = {
            type: 'doughnut',
            data: data,
            options: {
                plugins: {
                    legend:{
                        display: true,
                        position: 'bottom'
                    }
                },
            }
        };

        new Chart(
            document.getElementById('myChart'),
            config,
        );
    });

}

function dataTableGeneralExpense()
{
    var startDate =moment($('#start_date').val(),'YYYY-MM-DD').format('YYYY-MM-DD');
    var endDate = moment($('#end_date').val(),'YYYY-MM-DD').format('YYYY-MM-DD');
    let selectedMerchantId = $("#branch").select2('data');
    let merchantIds = [];

    $('.period-date').html(moment($('#start_date').val(),'YYYY-MM-DD').format('MMMM D, YYYY') + ' - ' + moment($('#end_date').val(),'YYYY-MM-DD').format('MMMM D, YYYY'));


    selectedMerchantId.forEach(item => {
        merchantIds.push(parseInt(item.id));
    });

    let md_merchant_id = merchantIds.join(',');

    ajaxDataTable('#general-expense', 1, "{{route('merchant.report.acc.exp-report')}}?is_datatable=1&start_date="+startDate+"&end_date="+endDate+"&md_merchant_id="+md_merchant_id+"", [
        {
            data: 'nama_pengeluaran',
            name: 'nama_pengeluaran',
            orderable: false,
            searchable: false
        },
        {
            data: 'total',
            name: 'total',
            orderable: false,
            searchable: false
        },
        {
            data: 'outlet',
            name: 'outlet',
            orderable: false,
            searchable: false
        }
    ]);
}

function exportData()
{
    modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
        const data = new FormData();
        const exportType = getExportType();
        var startDate = exportType == "month" ? moment($('#startMonth').val()).startOf('month').format('YYYY-MM-DD') : exportType == "year" ? moment($('#startYear').val()).startOf('year').format('YYYY-MM-DD') : moment($('#start_date').val(),'YYYY-MM-DD').format('YYYY-MM-DD');
        var endDate = exportType == "month" ? moment($('#endMonth').val()).endOf('month').format('YYYY-MM-DD') : exportType == "year" ? moment($('#endYear').val()).endOf('year').format('YYYY-MM-DD')  : moment($('#end_date').val(),'YYYY-MM-DD').format('YYYY-MM-DD');

        let selectedMerchantId = $("#branch").select2('data');
        let merchantIds = [];
        selectedMerchantId.forEach(item => {
            merchantIds.push(parseInt(item.id));
        });
        let md_merchant_id = merchantIds.join(',');

        if(exportType) {
            data.append('interval', exportType);    
        }
        
        ajaxTransfer("{{route("merchant.report.acc.export-exp")}}?is_datatable=0&start_date="+startDate+"&end_date="+endDate+"&md_merchant_id="+md_merchant_id+"", data,"#modal-output");
    });
}

const showExportInput = (exportType = "all") => {
    $("#export-input-type").children().remove();

    if(exportType == "month")  {
        $("#export-input-type").html(
            `
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Bulan Awal</label>
                        <input type="text" class="form-control form-control trans_time_month" id="startMonth" style="margin-bottom: 3px;background: white" name="start_month">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Sampai Bulan</label>
                        <input type="text" class="form-control form-control trans_time_month" id="endMonth" style="margin-bottom: 3px;background: white" name="end_month">
                    </div>
                </div>
            `
        )
        $(".trans_time_month").MonthPicker({
            ShowIcon: false,
            MonthFormat: "yy-mm",
        });
    }

    if (exportType == "year") {
        $("#export-input-type").html(
            `
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tahun Awal</label>
                        <input type="text" class="form-control form-control trans_time_year" id="startYear"style="margin-bottom: 3px;background: white" name="start_year">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Sampai Tahun</label>
                        <input type="text" class="form-control form-control trans_time_year" id="endYear" style="margin-bottom: 3px;background: white" name="end_year">
                    </div>
                </div>
            `
        )
        $(".trans_time_year").yearpicker();
    }
}

const getExportType = () => {
    let export_types = [...document.querySelectorAll('.btn_export_type')];
    let export_type = '';

    export_types.forEach(item => {
        if(item.classList.contains('btn-bg__orange')) {
            export_type = $(item).attr('data-export-type');
        }
    })

    return export_type;

}




</script>
@endsection

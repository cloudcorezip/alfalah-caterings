@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all', ['page'=>'inventory'])}}">Persediaan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>

                <span>Disini, Kamu bisa melihat stok produk yang rusak di gudangmu</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @include('merchant::alert')
        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="80%" style="padding: 0.1rem"><h6><b>Laporan Stok Produk (Cacat,Expired,Rusak)</b></h6></td>
                <td rowspan="2" class="text-right" style="padding: 0.1rem">
                    <a class="btn btn-success" onclick="exportData()"><i class="fa fa-download mr-2"></i> Download Laporan</a>

                </td>
            </tr>
            <tr>
                <td style="padding: 0.1rem"><h6><b>Periode
                            {{\Carbon\Carbon::parse($startDate)->isoFormat('D MMMM Y')}}
                            -
                            {{\Carbon\Carbon::parse($endDate)->isoFormat('D MMMM Y')}}

                        </b></h6></td>
            </tr>
        </table>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-custom" id="table-data-hpp" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th class="text-center">Rusak</th>
                            <th class="text-center">Cacat</th>
                            <th class="text-center">Kadaluarsa</th>
                            <th>Satuan</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($data)>0)
                        @foreach($data as $key =>$item)
                            <tr style="background: #ececec">
                                <td class="font-weight-bold">{{(count($item)<1)?'':$item->first()->nama_produk}}</td>
                                <td class="font-weight-bold text-center">{{(count($item)<1)?rupiah(0):round($item->sum('qty'),2)}}</td>
                                <td class="font-weight-bold text-center">{{(count($item)<1)?rupiah(0):round($item->sum('qty_damage'),2)}}</td>
                                <td class="font-weight-bold text-center">{{(count($item)<1)?rupiah(0):round($item->sum('qty_expired'),2)}}</td>
                                <td class="font-weight-bold">{{(count($item)<1)?'':$item->first()->unit_name}}</td>
                            </tr>
                            @foreach($item->sortBy('merchant_id') as $child)
                                <tr>
                                    <td>
                                        {{$child->outlet_name}} - {{$child->warehouse_name}}
                                    </td>
                                    <td class="text-center">
                                        {{round($child->qty,2)}}
                                    </td>
                                    <td class="text-center">
                                        {{round($child->qty_damage,2)}}
                                    </td>
                                    <td class="text-center">
                                        {{round($child->qty_expired,2)}}
                                    </td>
                                    <td></td>
                                </tr>
                                @endforeach
                        @endforeach
                            @else
                        <tr>
                            <td colspan="5" class="text-center">
                                Data tidak tersedia
                            </td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                    <br>
                </div>
                {{$data->withQueryString()->links('vendor.pagination.with-showing-entry')}}

            </div>
        </div>
    </div>



    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>
    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold mt-3 text-dark">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-hpp" class="px-30" method="get">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="end_date" value="{{$endDate}}" name="end_date" style="background: white">

                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Cabang yang ingin dibandingkan</label>
                    <select name="md_merchant_id[]" id="branch" class="form-control" multiple="multiple">
                        <option value="-1"  @if(in_array('-1',$merchantId)) selected @endif>Semua Cabang</option>
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" @if(in_array($item->id,$merchantId)) selected @endif>{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 mb-3 mt-3">
                    <input type="hidden" name="start_date" id="start_date" value="{{$startDate}}">
                    <input type="hidden" name="end_date" id="end_date" value="{{$endDate}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a href="{{route('merchant.report.inventory.demage-stock')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')
    <script>
        function exportData() {

            modalConfirm('Export Data', 'Lanjutkan proses export data?', function () {
                var data = getFormData('form-filter-sale-order');
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];
                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
                data.append('md_merchant_id',merchantIds);
                data.append('start_date','{{$startDate}}');
                data.append('end_date','{{$endDate}}');
                ajaxTransfer("{{route('merchant.report.inventory.export-demage-stock')}}", data, '#modal-output');
            });

        }

        $("#branch").on("select2:select", function(e){
            if(e.params.data.id == "-1"){
                $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
            } else {
                $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
            }
        });

        $(document).ready(function () {
            $("#branch").select2({});
            $("#type").select2({})

            $('#form-filter-hpp').submit(function () {

            });
        });
    </script>

@endsection

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .select2-container .select2-selection--multiple .select2-selection__rendered  {
            display: grid!important;
            grid-template-columns: 1fr 1fr;
        }
        .table-responsive {
            max-height: 80vh;
            overflow-y: auto;
        }
        .table-responsive thead {
            position: sticky;
            top: 0;
        }
        .table-responsive tfoot {
            position: sticky;
            bottom: 0;
        }
    </style>
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all', ['page'=>'purchase-order'])}}">Pembelian</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Disini, kamu bisa melihat retur pembelianmu</span>

            </div>
        </div>
    </div>

    <div class="container-fluid mb-4">
        <div class="col-md-12">
            <div class="alert alert-warning text-center">
                <b>Perhatian !</b> Laporan retur pembelian dan penerimaan ditampilkan dengan menggunakan satuan default dari setiap produk.
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header d-flex justify-content-between align-items-center">
                <div class="py-3">
                    <h4 class="font-weight-bold">{{$title}}</h4>
                    <small class="text-muted period-date">-</small>
                </div>
                <div class="col-md-6 d-flex justify-content-md-end">
                    <button
                        class="btn btn-bg__orange rounded-0 text-white btn-int"
                        data-interval="day"
                    >Hari
                    </button>
                    <button
                        class="btn btn-light rounded-0 text-muted btn-int"
                        data-interval="month"
                    >Bulan
                    </button>
                    <button
                        class="btn btn-light rounded-0 text-muted btn-int"
                        data-interval="year"
                    >Tahun
                    </button>
                    <input type="hidden" id="interval" name="interval" value="day">
                </div>
            </div>
            <div class="card-body" id="myChart-wrapper">
                <canvas id="myChart" width="400" height="80"></canvas>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="card shadow mb-4">
            <div class="card-header d-flex justify-content-between align-items-center">
                <div class="py-3">
                    <h4 class="font-weight-bold">Rincian Laporan</h4>
                    <small class="text-muted period-date">-</small>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-custom" width="100%" cellspacing="0">
                        <thead>
                        <tr style="background-color:#FFF;">
                            <th>Waktu</th>
                            <th class="text-center">Total Produk Retur</th>
                            <th class="text-center">Total Transaksi Retur</th>
                            <th class="text-right">Retur Pembelian</th>
                            <th class="text-center">Opsi</th>
                        </tr>
                        </thead>
                        <tbody id="data-result">
                        </tbody>
                        <tfoot id="total-result"></tfoot>
                    </table>
                </div>


            </div>
        </div>
    </div>

    <div class="side-popup" id="side-popup">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <span class="text-white form-filter3-text-header" id="title-detail"></span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter', 'side-popup')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div id="side-popup-body">

        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter" onsubmit="return false" class="px-30">
            <div class="row justify-content-end align-items-center">
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="startDate" value="{{$startDate}}" style="margin-bottom: 3px;background: white"  name="start_date">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="endDate" value="{{$endDate}}" style="background: white" name="end_date">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Tipe Retur</label>
                    <select name="reason_id" id="reason_id" class="form-control">
                        <option value="-1">Semua Tipe Retur</option>
                        <option value="1">Barang Rusak</option>
                        <option value="2">Pembatalan Item Transaksi</option>
                    </select>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Cabang yang ingin dibandingkan</label>
                    <select name="md_merchant_id" id="branch" class="form-control" multiple="multiple">
                        <option value="-1">Semua Cabang</option>
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12">
                    <input type="hidden" id="merchant_id" value="{{$merchantId}}">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <button type="submit" class="btn btn-success btn-block" style="padding:10px 20px!important;">Terapkan</button>
                    <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>

                </div>

            </div>

        </form>

    </div>

@endsection


@section('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#branch').select2();
            $("#reason_id").select2()

            const resetCanvas = () => {
                $('#myChart').remove();
                $('#myChart-wrapper').append('<canvas id="myChart" width="400" height="80"></canvas>');
            }

            $('.btn-int').on('click', function(){
                const btnIntervals = [...document.querySelectorAll('.btn-int')];
                btnIntervals.forEach(item => {
                    if(item.classList.contains('btn-bg__orange')){
                        item.classList.remove('btn-bg__orange', 'text-white');
                        item.classList.add('btn-light', 'text-muted');
                    }
                });

                $(this).removeClass('btn-light text-muted');
                $(this).addClass('btn-bg__orange text-white');
                const interval = $(this).attr('data-interval');
                $('#interval').val(interval);
                resetCanvas();
                loadChart();
            });

            $("#branch").on("select2:select", function(e){
                if(e.params.data.id == "-1"){
                    $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
                } else {
                    $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
                }
            });

            $('#form-filter').submit(function () {
                resetCanvas();
                loadChart();
                showFilter('btn-show-filter3', 'form-filter3');
            });

            const loadChart = () => {
                let startDate = $('#startDate').val();
                let endDate = $('#endDate').val()
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json($branch);
                    merchantIds.push(parseInt($("#merchant_id").val()));
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }

                let md_merchant_id = merchantIds.join(',');

                const data = new FormData();
                data.append('interval', $('#interval').val());
                data.append('md_merchant_id',md_merchant_id);
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('reason_id',$("#reason_id").val());

                $('.period-date').html(moment(startDate).format('MMMM D, YYYY') + ' - ' + moment(endDate).format('MMMM D, YYYY'));
                ajaxTransfer("{{route('merchant.report.purchase-order.retur-report')}}", data,function(response){
                    const responseData = JSON.parse(response);
                    const labels = [];
                    const borderColor = [
                        "#ffeb3b6b","#FFD028","#F2B04B","#FFADAD","#fc4949"
                    ];
                    let html;

                    $('#data-result').html('');
                    $('#total-result').html('');
                    if(responseData.code != 200){
                        html = "<tr>"+
                            "<td class='text-center' colspan='7'>Data laporan tidak ditemukan</td>"+
                            "</tr>";
                        $('#data-result').append(html);
                        return;
                    }

                    // for chart
                    responseData.data.table.forEach(item => {
                        labels.push(item.time);
                    });

                    const datasets = responseData.data.chart.map((item, index) => {
                        let details = JSON.parse(item.trans_details);
                        return {
                            id: item.id,
                            label: item.name,
                            backgroundColor:borderColor[index],
                            data: details.map(i => {
                                return {
                                    time: i.time,
                                    total: i.retur_amount,
                                    details: Object.keys(i).map(v => {
                                        return {
                                            name: v,
                                            value: i[v]
                                        }
                                    })
                                }
                            })
                        }
                    });

                    const data = {
                        labels: labels,
                        datasets: datasets
                    };

                    const config = {
                        type: 'bar',
                        data: data,
                        options: {
                            parsing: {
                                xAxisKey: "time",
                                yAxisKey: "total"
                            },
                            plugins: {
                                legend:{
                                    display: true,
                                    position: 'bottom'
                                },
                                tooltip: {
                                    callbacks: {
                                        label:function(context){
                                            return "";
                                        },
                                        footer:function(cb){
                                            let text = "";
                                            cb.forEach(item => {
                                                item.raw.details.forEach(i => {
                                                    let name;
                                                    let value;
                                                    switch(i.name){
                                                        case 'retur_amount':
                                                            name = 'Retur Pembelian';
                                                            value = currencyFormat(parseFloat(i.value), '');
                                                            break;
                                                        case 'retur_count':
                                                            name = 'Transaksi';
                                                            value = i.value;
                                                            break;
                                                        case 'retur_product_qty':
                                                            name = 'Total Produk';
                                                            value = i.value;
                                                            break;
                                                        default:
                                                            name = false;
                                                            value = false;
                                                            break;
                                                    }

                                                    if(name){
                                                        text += name + " : " + value + "\n";
                                                    }
                                                })
                                            });
                                            return text;
                                        }
                                    }
                                }
                            }
                        }
                    };

                    const myChart = new Chart(
                        document.getElementById('myChart'),
                        config,
                    );

                    // for table
                    let total_trans = 0;
                    let total_retur_amount = 0;
                    let total_retur_product = 0;

                    responseData.data.table.forEach(item => {
                        html += `<tr style="background:#F8F9FC;">
                            <td colspan="5" class="font-weight-bold">${item.time}</td>
                        </tr>`;
                        item.branch.forEach(i => {
                            let onDetail=i.id+"_"+item.time.replace(" ","_")+"";

                            html += `<tr>
                                <td class="pl-4">${i.name}</td>
 <td class="text-center">${i.retur_product_qty}</td>
                                <td class="text-center">${i.retur_count}</td>
                                <td class="text-right">${currencyFormat(parseFloat(i.retur_amount), '')}</td>
                                <td class="text-center"><a onclick="loadDetail('${onDetail}')" class="btn btn-xs btn-edit-xs btn-rounded" title="Lihat Detail" style="position: unset">
                                    <i class="fa fa-eye" style="color: #236FC3"></i>
                                </a>
</td>
                            </tr>`;
                            total_trans += i.retur_count;
                            total_retur_amount += i.retur_amount;
                            total_retur_product += i.retur_product_qty;
                        });
                    });

                    let totalHtml = `<tr style="background-color:#F8F9FC;">
                                <td class="font-weight-bold">TOTAL</td>
                                                                <td class="font-weight-bold text-center" id="total_product">${total_retur_product}</td>
                                <td class="font-weight-bold text-center" id="total_trans">${total_trans}</td>
                                <td class="font-weight-bold text-right" id="total_retur">${currencyFormat(parseFloat(total_retur_amount), '')}</td>
                                <td></td>
                            </tr>`;

                    $('#data-result').append(html);
                    $('#total-result').html(totalHtml);
                    // end table
                });
            }

            loadChart();
        })

        const reloadDataTable = () => void 0;
        const exportData = () => {

            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                let startDate = $('#startDate').val();
                let endDate = $('#endDate').val()
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json($branch);
                    merchantIds.push(parseInt($("#merchant_id").val()));
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }

                let md_merchant_id = merchantIds.join(',');

                const data = new FormData();
                data.append('interval', $('#interval').val());
                data.append('md_merchant_id',md_merchant_id);
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('reason_id',$("#reason_id").val());
                data.append('total_trans',$("#total_trans").text());
                data.append('total_retur',$("#total_retur").text());
                data.append('total_product',$("#total_product").text());



                ajaxTransfer("{{route('merchant.report.purchase-order.export-retur')}}", data, '#modal-output');
            });

        }
        const loadDetail = (params)=>{
            let startDate = $('#startDate').val();
            let endDate = $('#endDate').val()

            $.ajax({
                type: 'POST',
                url: '{{route('merchant.report.purchase-order.retur-report-detail')}}',
                data: {
                    'interval':$('#interval').val(),
                    'custom_params':params,
                    'start_date':moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'),
                    'end_date':moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD')
                },
                headers:{
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
                success: function(data){
                    $('#side-popup-body').html('');
                    let html="<div class='row mb-3'>" +
                        "" +
                        "<table class='table table-custom'>";
                    html +="<thead>" +
                        "<tr>" +
                        "<th>Kode</th>"+
                        "<th>Supplier</th>" +
                        "<th>Alasan</th>" +
                        "<th class='text-right'>Total</th>" +
                        "</tr>" +
                        "</thead><tbody>"
                    if(data.length<1)
                    {
                        html+="<tr>" +
                            "<td colspan='4' style='text-align: center'>Data tidak tersedia</td></tr>"

                    }else{
                        $.each(data,function (i,v){
                            if(i==0){

                                $("#title-detail").text("Detail Retur Cabang "+ v.branch_name)
                            }
                            html+= "<tr>" +
                                "<td><button class='btn btn-default btn-xs' onclick='collapseTable("+v.id+")'>" +
                                "<span class='iconify' data-icon='ep:arrow-down-bold'></span> " +
                                "<input type='hidden' value='0' id='"+v.id+"status'> " +
                                "</button>" +
                                ""+v.code+"</td>" +
                                "<td>"+v.customer_name+"</td>" +
                                "<td>"+v.reason_note+"</td>" +
                                "<td class='text-right'>"+currencyFormat(v.total,'')+"</td>" +
                                "</tr>"


                            let details=JSON.parse(v.details)

                            html+="<tr class='"+v.id+"row' style='display: none; background-color: #F8F9FC'>" +
                                "<td rowspan='"+(details.length+1)+"'><b>Detail</b></td>" +
                                "<td colspan='2'><b>Nama Produk</b></td>" +
                                "<td><b>Qty/" +
                                "Satuan</b></td>" +
                                "</tr>"

                            $.each(details,function(j,k){
                                let quantity=(k.is_multi_unit===1)?k.quantity:k.quantity;

                                html+="<tr class='"+v.id+"row' style='display: none'>" +
                                    "<td colspan='2'>"+k.product_name+"</td>" +
                                    "<td>"+quantity+" " +
                                    ""+k.unit_name+"</td>" +
                                    "</tr>"
                            })
                        })
                    }

                    html+="</tbody></table>" +
                        "</div>";
                    $('#side-popup-body').append(html);
                    $('#side-popup').addClass('show');

                }
            });
        }
        const collapseTable=(params)=>{
            let classCollapse =$("#"+params+"status").val();

            if(classCollapse==1){
                $("."+params+"row").hide();
                $("."+params+"row").removeClass('show');
                $("#"+params+"status").val(0)
            }else{
                $("."+params+"row").show();
                $("."+params+"row").addClass('show');
                $("#"+params+"status").val(1)

            }
        }

    </script>


@endsection

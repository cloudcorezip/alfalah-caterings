@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .select2-container .select2-selection--multiple .select2-selection__rendered  {
            display: grid!important;
            grid-template-columns: 1fr 1fr;
        }
        .table-responsive {
            max-height: 80vh;
            overflow-y: auto;
        }
        .table-responsive thead {
            position: sticky;
            top: 0;
        }
        .table-responsive tfoot {
            position: sticky;
            bottom: 0;
        }
    </style>
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.report-all', ['page'=>'purchase-order'])}}">Pembelian</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Disini, kamu bisa melihat detail pembelian secara menyeluruh dari per kategori maupun per produk.</span>

            </div>
        </div>
    </div>


    <div class="container-fluid mb-4">
        <div class="col-md-12">
            <div class="alert alert-warning text-center">
                <b>Perhatian !</b> Laporan stok pembelian produk ditampilkan dengan menggunakan satuan default dari setiap produk.
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header">
                <div class="row d-flex align-items-center">
                    <div class="col-md-6">
                        <div class="py-3">
                            <h4 class="font-weight-bold">Pembelian Produk</h4>
                            <small class="text-muted period-date">-</small>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex justify-content-md-end">
                        <button
                            class="btn btn-bg__orange rounded-0 text-white btn-int"
                            data-interval="day"
                        >Hari
                        </button>
                        <button
                            class="btn btn-light rounded-0 text-muted btn-int"
                            data-interval="month"
                        >Bulan
                        </button>
                        <button
                            class="btn btn-light rounded-0 text-muted btn-int"
                            data-interval="year"
                        >Tahun
                        </button>
                        <input type="hidden" id="interval" name="interval" value="day">
                    </div>
                </div>
            </div>
            <div class="card-body" id="myChart-wrapper">
                <canvas id="myChart" width="400" height="80"></canvas>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card shadow mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="py-3">
                            <h4 class="font-weight-bold">Rincian Laporan</h4>
                            <small class="text-muted period-date">-</small>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-custom" width="100%" cellspacing="0">
                                <thead>
                                <tr style="background-color:#FFF;">
                                    <th>Produk</th>
                                    <th class="text-center">Jumlah Produk</th>
                                    <th class="text-right">Pembelian</th>
                                </tr>
                                </thead>
                                <tbody id="data-result">
                                </tbody>
                                <tfoot id="total-result"></tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="py-3">
                            <h4 class="font-weight-bold">Insight</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <h6 class="font-weight-bold">Pembelian Produk Terbanyak</h6>
                            <div id="best-selling-product">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3" style="padding-bottom: 205px;">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter" onsubmit="return false" class="px-30">
            <div class="row align-items-center">
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="startDate" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date" required>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="endDate" value="{{$endDate}}" name="end_date" style="background: white" required>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Cabang yang ingin dibandingkan</label>
                    <select name="md_merchant_id" id="branch" class="form-control" multiple="multiple" required>
                        <option value="-1">Semua Cabang</option>
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Kategori yang ingin ditampilkan</label>
                    <select class="form-control" name="sc_category_id" id="sc_category_id" multiple="multiple" required onchange="getProduct()">
                        <option value="-1">Semua</option>
                        @foreach($category as $key => $item)
                            <option value="{{$item->id}}" data-name="{{$item->nama_kategori}}">{{$item->nama_kategori}}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="col-md-12 mb-3">
                    <label>Pilih Produk yang ingin ditampilkan</label>
                    <select class="form-control" name="product_id" id="product_id" multiple="multiple" required>
                        <option value="-1">Semua</option>
                    </select>
                </div>

                <div class="col-md-12 mb-4">
                    <button type="submit" class="btn btn-success btn-block" style="padding:10px 20px;">Terapkan</button>
                </div>

                <h4 class="font-weight-bold text-dark" style="margin-left: 14px;">Export</h3>
                
                <div class="col-md-12 mb-3">
                    <label>Export data berdasarkan</label>
                    </br>
                    <button 
                        class="btn btn-bg__orange rounded-0 btn_export_type"
                        type="button"
                        data-export-type="day"
                        >Semua
                    </button>
                    <button
                        class="btn rounded-0 btn_export_type"
                        type="button"
                        data-export-type="month"
                    >Per Bulan
                    </button>
                    <button
                        class="btn rounded-0 btn_export_type"
                        type="button"
                        data-export-type="year"
                    >Per Tahun
                    </button>
                    <!-- <div class="form-group">
                        <label for="exampleInputPassword1">Bulan Awal</label>
                        <input type="text" class="form-control form-control trans_time_month" id="startDate" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date" required>
                    </div> -->
                </div>

                <div class="row" style="margin: 0 2px 0 2px;" id="export-input-type">
                </div>

                <div class="col-md-12">
                    <input type="hidden" id="merchant_id" value="{{$merchantId}}">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>
                </div>
            </div>

        </form>

    </div>
@endsection


@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#branch').select2();
            $('#sc_category_id').select2();
            $('#product_id').select2();

            const resetCanvas = () => {
                $('#myChart').remove();
                $('#myChart-wrapper').append('<canvas id="myChart" width="400" height="80"></canvas>');
            }

            $('.btn-int').on('click', function(){
                const btnIntervals = [...document.querySelectorAll('.btn-int')];
                btnIntervals.forEach(item => {
                    if(item.classList.contains('btn-bg__orange')){
                        item.classList.remove('btn-bg__orange', 'text-white');
                        item.classList.add('btn-light', 'text-muted');
                    }
                });
                $(this).removeClass('btn-light text-muted');
                $(this).addClass('btn-bg__orange text-white');
                const interval = $(this).attr('data-interval');
                $('#interval').val(interval);
                resetCanvas();
                loadChart();
            });

            $('.btn_export_type').on('click', function(){
                const btnIntervals = [...document.querySelectorAll('.btn_export_type')];
                btnIntervals.forEach(item => {
                    if(item.classList.contains('btn-bg__orange')) {
                        item.classList.remove('btn-bg__orange');
                    }
                });
                $(this).addClass('btn-bg__orange');
                const export_format = $(this).attr('data-export-type')
                showExportInput(export_format);
            });

            $("#branch").on("select2:select", function(e){
                if(e.params.data.id == "-1"){
                    $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
                } else {
                    $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
                }
            });


            $("#sc_category_id").on("select2:select", function(e){
                if(e.params.data.id == "-1"){
                    $("#sc_category_id > option[value !='-1']").prop("selected",false).trigger("change");
                } else {
                    $("#sc_category_id > option[value ='-1']").prop("selected",false).trigger("change");
                }
            });

            $("#product_id").on("select2:select", function(e){
                if(e.params.data.id == "-1"){
                    $("#product_id > option[value !='-1']").prop("selected",false).trigger("change");
                } else {
                    $("#product_id > option[value ='-1']").prop("selected",false).trigger("change");
                }
            });



            $('#form-filter').submit(function () {
                resetCanvas();
                loadChart();
                showFilter('btn-show-filter3', 'form-filter3');
            });

            const loadChart = () => {
                let startDate = $('#startDate').val();
                let endDate = $('#endDate').val();
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let selectedProduct = $("#product_id").select2('data');
                let selectedCategories = $("#sc_category_id").select2('data');
                let merchantIds = [];
                let products = [];
                let category= [];


                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json($branch);
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }

                // cek jika pilih semua produk
                let md_merchant_id,req_product_code,req_category_id;

                if(selectedCategories.length<1)
                {
                    req_category_id='';
                }else{

                    if(selectedCategories.findIndex(a => a.id == '-1') >= 0){
                        req_category_id='';
                    } else {
                        selectedCategories.forEach(item => {
                            category.push(`'${item.id.trim()}'`);
                        });
                        req_category_id = `and spc.sc_product_category_id in(${category.join(',')})`;
                    }
                }

                if(selectedProduct.length<1)
                {
                    req_product_code='';

                }else{
                    if(selectedProduct.findIndex(a => a.id == '-1') >= 0){
                        req_product_code='';
                    } else {
                        selectedProduct.forEach(item => {
                            products.push(`'${item.id.trim()}'`);
                        });
                        req_product_code = `and spc.code in(${products.join(',')})`;

                    }
                }


                md_merchant_id = merchantIds.join(',');


                const data = new FormData();
                data.append('interval', $('#interval').val());
                data.append('md_merchant_id',md_merchant_id);
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('category_id', req_category_id);
                data.append('product_code', req_product_code);

                $('.period-date').html(moment(startDate).format('MMMM D, YYYY') + ' - ' + moment(endDate).format('MMMM D, YYYY'));
                ajaxTransfer("{{route('merchant.report.purchase-order.product-report')}}", data,function(response){

                    const responseData = JSON.parse(response);
                    loadSideGraph(responseData);
                    const borderColor = [
                        "#ffeb3b6b","#FFD028","#F2B04B","#FFADAD","#fc4949"
                    ];
                    const labels = [];
                    let html;
                    $('#data-result').html('');
                    $('#total-result').html('');
                    if(responseData.code != 200 || responseData.data.table.length < 1){
                        html = `<tr>
                            <td class="text-center" colspan="3">Data laporan tidak ditemukan</td>
                        </tr>
                        `;
                        $('#data-result').append(html);
                        return;
                    }

                    const timeLabels = responseData.data.table[0].branch[0].get_detail;
                    timeLabels.forEach(item => {
                        labels.push(item.time);
                    });

                    const datasets = responseData.data.table.map((item, index) => {
                        let branch = item.branch;
                        return {
                            label: item.product,
                            backgroundColor: borderColor[index],
                            data: labels.map((l) => {
                                let total = 0;
                                let amount = 0;
                                return {
                                    time: l,
                                    branch: branch.map(b => {
                                        amount+=b.price
                                        return {
                                            name: b.branch_name,
                                            sub_qty: b.get_detail.filter(gd => {
                                                return gd.time == l;
                                            }).map(obj => {
                                                total += obj.quantity;
                                                return obj.quantity;
                                            }).shift(),
                                            unit_name:b.unit_name
                                        }
                                    }),
                                    quantity: total,
                                    amount:amount,
                                    unit_name:item.unit_name
                                }
                            })
                        }
                    });

                    const data = {
                        labels: labels,
                        datasets: datasets
                    };

                    const config = {
                        type: 'bar',
                        data: data,
                        options: {
                            parsing: {
                                xAxisKey:"time",
                                yAxisKey:"quantity"
                            },
                            plugins: {
                                legend:{
                                    display: true,
                                    position: 'bottom'
                                },
                                tooltip: {
                                    callbacks: {
                                        label: function(context){
                                            return context.raw.name;
                                        },
                                        footer: function(cb){
                                            let text = "";
                                            cb.forEach(item => {
                                                item.raw.branch.forEach(i => {
                                                    text += i.name + " : " + i.sub_qty +" "+i.unit_name+"\n";
                                                })
                                            });
                                            return text;
                                        },
                                        afterFooter: function(tooltipItem){
                                            let text="";
                                            tooltipItem.forEach(item => {
                                                text += "Total Produk : " + item.raw.quantity+" "+item.raw.unit_name+"\n";
                                                text += "Total Pembelian : " + currencyFormat(item.raw.amount,'');

                                            });

                                            return text;
                                        }
                                    }
                                }
                            }
                        }
                    };

                    const myChart = new Chart(
                        document.getElementById('myChart'),
                        config,
                    );


                    let total_product = 0;
                    let total_price = 0;

                    responseData.data.table.forEach(item => {
                        html += `<tr style="background-color:#F8F9FC;">
                            <td class="font-weight-bold" colspan="3">${item.product}</td>
                        </tr>`;
                        item.branch.forEach(i => {
                            html += `<tr>
                                <td style="padding-left:30px;">${i.branch_name}</td>
                                <td class="text-center">${i.qty} ${i.unit_name}</td>
                                <td class="text-right">${currencyFormat(parseFloat(i.price), '')}</td>
                            </tr>`;
                            total_product += i.qty;
                            total_price += i.price;
                        });
                    });

                    let totalHtml = `<tr style="background-color:#F8F9FC;">
                                <td class="font-weight-bold">TOTAL</td>
                                <td class="text-center" id="total_product"><b>${total_product}</b></td>
                                <td class="text-right" id="total_price"><b>${currencyFormat(parseFloat(total_price), '')}</b></td>
                            </tr>`;

                    $('#data-result').append(html);
                    $('#total-result').html(totalHtml);
                });
            }

            loadChart();
        })

        const reloadDataTable = () => void 0;
        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {

                const exportType = getExportType();
                let startDate = exportType == "month" ? $('#startMonth').val() : exportType == "year" ? $('#startYear').val() : $('#startDate').val();
                let endDate = exportType == "month" ? moment($('#endMonth').val()).endOf('month') : exportType == "year" ? moment($('#endYear').val()).endOf('year') : $('#endDate').val();
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let selectedProduct = $("#product_id").select2('data');
                let selectedCategories = $("#sc_category_id").select2('data');
                let merchantIds = [];
                let products = [];
                let category= [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json($branch);
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }

                // cek jika pilih semua produk
                let md_merchant_id,req_product_code,req_category_id;

                if(selectedCategories.length<1)
                {
                    req_category_id='';
                }else{

                    if(selectedCategories.findIndex(a => a.id == '-1') >= 0){
                        req_category_id='';
                    } else {
                        selectedCategories.forEach(item => {
                            category.push(`'${item.id.trim()}'`);
                        });
                        req_category_id = `and spc.sc_product_category_id in(${category.join(',')})`;
                    }
                }

                if(selectedProduct.length<1)
                {
                    req_product_code='';

                }else{
                    if(selectedProduct.findIndex(a => a.id == '-1') >= 0){
                        req_product_code='';
                    } else {
                        selectedProduct.forEach(item => {
                            products.push(`'${item.id.trim()}'`);
                        });
                        req_product_code = `and spc.code in(${products.join(',')})`;

                    }
                }


                md_merchant_id = merchantIds.join(',');


                const data = new FormData();
                data.append('interval', exportType);
                data.append('md_merchant_id',md_merchant_id);
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('category_id', req_category_id);
                data.append('product_code', req_product_code);
                data.append('total_product',$("#total_product").text())
                data.append('total_price',$("#total_price").text())

                ajaxTransfer("{{route('merchant.report.purchase-order.export-product')}}", data, '#modal-output');
            });

        }

        const loadSideGraph = (param) => {
            const response = param.data.table;
            const data = [];
            let backgroundColor = ['#FE8F2D', '#939393', '#B2B1B1', '#C4C4C4', '#E0E0E0'];
            let arrQty = [];

            if(param.code != 200){
                $("#not-selling-product").html(`
            <div class="col-md-12">
                <span class="d-block text-center">Tidak ada data yang tersedia</span
            </div>
        `);
                return;
            }

            response.forEach(item => {
                let q = item.branch.reduce((acc, curr) => {
                    return acc + curr.qty;
                }, []);
                arrQty.push(q);
            });

            let total = arrQty.reduce((acc, curr) => {
                return acc + parseInt(curr);
            },0);

            response.forEach((item,index) => {
                let qty = item.branch.reduce((acc, curr) => {
                    return acc + curr.qty;
                }, 0);
                let width = (Math.floor(qty / total * 100))-20;
                data.push(
                    {
                        product:item.product,
                        quantity: qty,
                        width:width,
                        background:backgroundColor[index],
                        unit_name:item.unit_name
                    }
                )
            });

            viewBestSellingProduct(data);
        }

        const viewBestSellingProduct = (param) => {
            const response = [...param].sort((a, b) => b.quantity - a.quantity);
            const data = response.filter(item => {
                return item.quantity > 0;
            }).slice(0,5);

            let html = `<div class='mb-3' style='width:100%;'>`;
            data.forEach((item, index) => {
                html += `<div class="d-flex align-items-center justify-content-between">
                    <div class="py-2 px-4 rounded-right" style="width:${item.width}%; background-color:${item.background}">
                    </div>
                    <span class="py-2">${item.quantity} ${item.unit_name}</span>
                </div>
                <div><small>${item.product}</small></div>

`;
            });
            html += "</div>";

            if(data.length > 0){
                $("#best-selling-product").html(html);
            } else {
                $("#best-selling-product").html(`
            <div class="mb-3" style='width:100%;'>
                <span class="d-block text-center">Tidak ada data yang tersedia</span>
            </div>
        `);
            }
        }

        const showExportInput = (exportType = "all") => {
            $("#export-input-type").children().remove();

            if(exportType == "month")  {
                $("#export-input-type").html(
                    `
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Bulan Awal</label>
                                <input type="text" class="form-control form-control trans_time_month" id="startMonth" style="margin-bottom: 3px;background: white" name="start_month">
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Sampai Bulan</label>
                                <input type="text" class="form-control form-control trans_time_month" id="endMonth" style="margin-bottom: 3px;background: white" name="end_month">
                            </div>
                        </div>
                    `
                )
                $(".trans_time_month").MonthPicker({
                    ShowIcon: false,
                    MonthFormat: "yy-mm",
                });
            }

            if (exportType == "year") {
                $("#export-input-type").html(
                    `
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tahun Awal</label>
                                <input type="text" class="form-control form-control trans_time_year" id="startYear"style="margin-bottom: 3px;background: white" name="start_year">
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Sampai Tahun</label>
                                <input type="text" class="form-control form-control trans_time_year" id="endYear" style="margin-bottom: 3px;background: white" name="end_year">
                            </div>
                        </div>
                    `
                )
                $(".trans_time_year").yearpicker();
            }
        }

        const getExportType = () => {
            let export_types = [...document.querySelectorAll('.btn_export_type')];
            let export_type = '';

            export_types.forEach(item => {
                if(item.classList.contains('btn-bg__orange')) {
                    export_type = $(item).attr('data-export-type');
                }
            })

            return export_type;

        }

        function getProduct()
        {
            let selectedCategories = $("#sc_category_id").select2('data');
            let category= [];

            let req_category_id;

            if(selectedCategories.length<1)
            {
                req_category_id='';
            }else{

                if(selectedCategories.findIndex(a => a.id == '-1') >= 0){
                    req_category_id='';
                } else {
                    selectedCategories.forEach(item => {
                        category.push(`'${item.id.trim()}'`);
                    });
                    req_category_id = `(${category.join(',')})`;
                }
            }

            $("#product_id").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.report.sale-order.ajax-get-product')}}?md_merchant_id={{merchant_id()}}&is_with_stock=1&category="+req_category_id,
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

        }

    </script>


@endsection

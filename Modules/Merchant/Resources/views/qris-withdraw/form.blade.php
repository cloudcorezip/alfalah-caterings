
                <h5 align="center">Detail Permintaan Aktivasi QRIS</h5>
                <table class="table table-bordered text-center table-striped" id="table-data" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <td>Nama Bank</td>
                        <td>Nama Akun Bank</td>
                        <td>Nomor Akun Bank</td>
                        <td>Kartu Identitas</td>
                        <td>Buku Akun</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$data->getBank->name}}</td>
                        <td>{{$data->bank_account_name}}</td>
                        <td>{{$data->bank_account_number}}</td>
                        <td>
                            <a href="{{env('S3_URL').$data->identity_card}}" target="_blank">Download File</a>
                        </td>
                        <td>
                            <a href="{{env('S3_URL').$data->account_book}}" target="_blank">Download File</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                        <td>
                            @if($data->is_agree==0)
                            <a href="{{route('qris.approve',['id'=>$data->id])}}" class="btn btn-success btn-sm text-white">Setuju</a>
                            <a href="{{route('qris.canceled',['id'=>$data->id])}}" class="btn btn-danger btn-sm text-white">Tolak</a>
                            @endif
                            @if($data->is_agree==1)
                            <div class="badge badge-success">Izin telah disetujui</div>
                           @endif

                                @if($data->is_agree==2)
                                    <div class="badge badge-danger">Izin tidak disetujui</div>
                                @endif
                        </td>
                    </tr>
                    </tbody>
                </table>


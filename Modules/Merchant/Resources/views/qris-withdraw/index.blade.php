@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Keuangan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Ketahui ringkasan informasi pembayaran transaksi yang menggunakan Pembayaran Digital.</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @if(is_null($activation))
            <div class="alert alert-info alert-dismissible show text-center" style="z-index: 0;color: #7b7f81" role="alert">
                <strong>Hey</strong> Kamu belum melakukan aktivasi metode pembayaran digital,yuk <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}?page=digital-payment" class="btn btn-outline-info">Aktivasi QRIS disini</a>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @else
            @if($activation->is_active==0)
                <div class="alert alert-primary alert-dismissible show text-center" style="z-index: 0;color: #7b7f81" role="alert">
                    <strong>Hey</strong> Aktivasi Pembayaran Digitalmu lagi diproses tunggulah 2x24 jam untuk dapat kami proses
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @else

            @endif
        @endif
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="dashboard-component info-component">
                    <div class="card border">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 col-8">
                                    <span>Jumlah</span>
                                    <h4><b>Saldo Tertunda</b></h4>
                                </div>
                                <div class="col-md-4 col-4">
                                    <img src="{{asset('public/backend')}}/icons/dashboard/image16.png" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <b><div id="stat-saldo-pending" class="info-number"><img src="{{asset('public/backend/img')}}/loading-horizontal.gif" /></div></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="dashboard-component info-component">
                    <div class="card border">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 col-8">
                                    <span>Jumlah</span>
                                    <h4><b>Saldo Masuk</b></h4>
                                </div>
                                <div class="col-md-4 col-4">
                                    <img src="{{asset('public/backend')}}/icons/dashboard/image16.png" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <b><div id="stat-saldo" class="info-number"><img src="{{asset('public/backend/img')}}/loading-horizontal.gif" /></div></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="dashboard-component info-component">
                    <div class="card border">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 col-8">
                                    <span>Jumlah</span>
                                    <h4><b>Transaksi</b></h4>
                                </div>
                                <div class="col-md-4 col-4">
                                    <img src="{{asset('public/backend')}}/icons/dashboard/image14.png" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <b><div id="stat-total-transaksi" class="info-number"><img src="{{asset('public/backend/img')}}/loading-horizontal.gif" /></div></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="dashboard-component info-component">
                    <div class="card border">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 col-8">
                                    <span>Total</span>
                                    <h4><b>Penarikan</b></h4>
                                </div>
                                <div class="col-md-4 col-4">
                                    <img src="{{asset('public/backend')}}/icons/dashboard/image16.png" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <b><div id="stat-total-penarikan" class="info-number"><img src="{{asset('public/backend/img')}}/loading-horizontal.gif" /></div></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">

                </div>
                <div class="table-responsive">
                    @if(!is_null($activation))
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-lg-2">
                                <button class="btn btn-success btn-rounded btn-sm btn-block" onclick="loadModal(this)" target="{{route('qris-withdraw.add')}}"><i class="fa fa-plus"></i> Buat Penarikan</button>
                            </div>
                        </div>
                    @endif
                    <div id="output-sale-order">
                        @include('merchant::qris-withdraw.list')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" class="px-30" onsubmit="return false">
            <div class="row d-flex align-items-center">
                <div class="col-md-12 mb-3">
                    <label>Pilih Tanggal Transaksi :</label>
                    <div id="reportrange" class="form-control" name="date-range">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>

                <div class="col-md-12 mb-3 mt-3">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                </div>
            </div>

        </form>
    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')

    <script type="text/javascript">

        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                    '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                    '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                    'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
                    //'All Time': 'all-time',
                }
            }, cb);
            cb(start, end);

        });
    </script>
    <script>
        function loadStatistikHeader() {
            ajaxTransfer("{{route('qris-withdraw.load')}}", {}, function(response) {
                response = JSON.parse(response);
                console.log(response['jumlah_transaksi'])
                $('#stat-saldo').html(response['saldo']);
                $('#stat-saldo-pending').html(response['saldo_pending']);
                $('#stat-total-transaksi').html(response['jumlah_transaksi']);
                $('#stat-total-penarikan').html(response['total_penarikan']);
            }, false, false);
        }
        $(document).ready(function () {
            loadStatistikHeader()

            $('#form-filter-sale-order').submit(function () {
                var startDate = $('#reportrange').data('daterangepicker').startDate._d;
                var endDate = $('#reportrange').data('daterangepicker').endDate._d;
                var data = getFormData('form-filter-sale-order');
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                ajaxTransfer("{{route('qris-withdraw.reload.data')}}", data, '#output-sale-order');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        });

    </script>
@endsection

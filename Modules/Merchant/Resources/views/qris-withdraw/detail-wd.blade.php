<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        @if($tipe=='out')
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Nama Pemilik Rekening</label>
                    <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$activation->bank_account_name}}" disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Nama Bank</label>
                    <input type="email" class="form-control form-control-sm" name="email" placeholder="Email" value="{{$activation->getBank->bank_code}}-{{$activation->getBank->name}} ({{$activation->bank_account_number}})" disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Jumlah Penarikan</label>
                    <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{rupiah($wd->amount)}}" required disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Biaya Administrasi</label>
                    <input type="text" class="form-control form-control-sm" name="phone_number" placeholder="No Telp" value="{{rupiah(config('qris_rule.administration_wd'))}}" required disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Waktu Penarikan</label>
                    <input type="text" class="form-control form-control-sm" name="phone_number" placeholder="No Telp" value="{{Carbon\Carbon::parse($wd->created_at)->isoFormat('dddd, D MMMM Y')}}" required disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Status Penarikan</label>
                    @if($wd->transaction_status=='PENDING')
                        <badge class="badge badge-warning" style="color: white">Pending</badge>
                    @elseif($wd->transaction_status=='SUCCESS')
                        <badge class="badge badge-success" style="color: white">Success</badge>
                    @else
                        <badge class="badge badge-danger" style="color: white">Failed</badge>
                    @endif
                </div>
            </div>

        @else
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Kode Penjualan</label>
                    <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$wd->kode_transaksi}}" disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Jumlah</label>
                    <input type="email" class="form-control form-control-sm" name="email" placeholder="Email" value="{{rupiah($wd->)}}" disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Biaya Administrasi (0.70%)</label>
                    <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{rupiah($wd->admin_fee)}}" required disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Jumlah Total</label>
                    <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{rupiah($wd->total-$wd->admin_fee)}}" required disabled>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Waktu Transaksi</label>
                    <input type="text" class="form-control form-control-sm" name="phone_number" placeholder="No Telp" value="{{Carbon\Carbon::parse($wd->created_at)->isoFormat('dddd, D MMMM Y')}}" required disabled>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputPassword1">Status Transaksi</label>
                    @if($wd->md_sc_transaction_status_id==1)
                        <badge class="badge badge-warning" style="color: white">Pending</badge>
                    @elseif($wd->md_sc_transaction_status_id==2)
                        <badge class="badge badge-success" style="color: white">Success</badge>
                    @else
                        <badge class="badge badge-danger" style="color: white">Failed</badge>
                    @endif
                </div>
            </div>
        @endif


        <div class="col-md-12">
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div>
                                         <h5 class="modal-title">{{($tipe=='in')?'Detail Transaksi':'Detail Penarikan'}}</h5>
                                    </div>`)
    });


</script>

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nama Bank</label>
        <input type="text" class="form-control" name="name" placeholder="Nama Kategori" value="{{$data->getBank->name}} {{$data->bank_account_number}}" required disabled>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Biaya Administrasi</label>
        <input type="text" class="form-control" name="name" placeholder="Nama Kategori" value="{{rupiah($admin_fee)}}" required disabled>
    </div>


    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Masukkan Nominal</label>
        <input type="text" class="form-control amount_currency" name="amount" placeholder="Masukkan Nominal" value="0" required>
    </div>


    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='saldo' value='{{$saldo }}'>
    <input type='hidden' name='admin_fee' value='{{$admin_fee }}'>


    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('.amount_currency').mask('#.##0,00', {reverse: true});
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('qris-withdraw.inquiry')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })
    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        @if(merchant_id()==4090)
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Buat Penarikan</h4>
                                        <span class="span-text">Maksimal saldo yang dapat ditarik saat ini adalah {{rupiah($saldo)}} sudah dipotong dengan biaya layanan untuk Senna sebesar 1%</span>
                                    </div>`);
        @else
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Buat Penarikan</h4>
                                        <span class="span-text">Maksimal saldo yang dapat ditarik saat ini adalah {{rupiah($saldo)}}</span>
                                    </div>`);
        @endif
    });


</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table tr td {
            font-size: 11px;
        }
    </style>
</head>
<body style="font-family: Arial, sans-serif; width:100%;margin-right:auto;margin-left:auto">
    <div style="width:100%; margin:0 auto;">
        <div style="border: 1px solid #DDDDDD; border-radius:12px;">
            <div style="width:100%; margin: 0 auto;">
                <h3 style="text-align:center;margin-bottom:7px;">{{$data->getMerchant->name}}</h3>
                <span style="display:block;text-align:center;color:#a4a4a4;font-size:12px;">{{$data->getMerchant->address}}</span>
            </div>
            @if(is_null($data->queue_number))
            <div style="width:100%;margin: 0 auto;">
                <img style="width:100%;object-fit:contain;" src="{{env('S3_URL')}}{{$data->qr_code_file}}" alt="qr_code">
            </div>
            @else
            <div style="width:100%;margin: 0 auto;">
                <h3 style="text-align:center;">{{$data->queue_number}}</h3>
            </div>
            @endif
            <div style="width:80%;margin: 0 auto;">
                <table width="100%" cellspacing="0">
                    <tr>
                        <td style="width:48%;">No. Reservasi</td>
                        <td style="width:4%; text-align:right;">:</td>
                        <td style="width:48%;text-align:right;">{{$data->reservation_number}}</td>
                    </tr>
                    <tr>
                        <td style="width:48%;">No. Member</td>
                        <td style="width:4%; text-align:right;">:</td>
                        <td style="width:48%;text-align:right;">{{$data->getCustomer->code}}</td>
                    </tr>
                    <tr>
                        <td style="width:48%;">Nama</td>
                        <td style="width:4%;text-align:right;">:</td>
                        <td style="width:48%;text-align:right;">{{$data->getCustomer->name}}</td>
                    </tr>
                    <tr>
                        <td style="width:48%;">Layanan</td>
                        <td style="width:4%;text-align:right;">:</td>
                        <td style="width:48%;text-align:right;">{{$data->getProduct->name}}</td>
                    </tr>
                    <tr>
                        <td style="width:48%;">Dokter</td>
                        <td style="width:4%;text-align:right;">:</td>
                        <td style="width:48%;text-align:right;">{{$data->getStaff->getUser->fullname}}</td>
                    </tr>
                    <tr>
                        <td style="width:48%;">Tanggal</td>
                        <td style="width:4%;text-align:right;">:</td>
                        <td style="width:48%;text-align:right;">{{$data->reservation_date}}</td>
                    </tr>
                </table>
            </div>
            <div style="width:100%;margin:0 auto;margin-bottom:10px;">
                <h3 style="text-align:center;margin-bottom:7px;">Terima Kasih</h3>
                <span style="display:block;color:#a4a4a4;text-align:center;font-size:11px;"><i>Powered By Senna</i></span>
            </div>
        </div>
    </div>
</body>
</html>
@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Klinik</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.reservation.index')}}">Reservasi</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa melihat detail reservasi disini</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4 rounded">
            <div class="card-body py-5 px-5">
                <div class="row mb-4">
                    <div class="col-md-12 mb-4">
                        <h4 class="font-weight-bold">Detail Data Reservasi</h4>
                    </div>
                    <div class="col-md-12">
                        <h5 class="font-weight-bold mb-3">Detail Pasien</h5>
                        <div class="patient-card">
                            <div class="row">
                                <div class="col-md-6">
                                    {{---<div class="mb-3" style="width: 120px; height:120px; border:1px solid #ccc; margin:0 auto;">

                                    </div>---}}
                                    <table class="table-note" style="margin: 0 auto;">
                                        <tr>
                                            <td class="font-weight-bold font-14 color-323232">No. Member</td>
                                            <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                            <td class="font-14 color-a4a4a4">{{$data->getCustomer->code}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-14 font-weight-bold color-323232">No. Reservasi</td>
                                            <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                            <td class="font-14 color-a4a4a4">{{$data->reservation_number}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table-note" style="margin:0 auto;">
                                        <tr>
                                            <td class="font-weight-bold font-14 color-323232">Nama</td>
                                            <td class="text-center font-weight-bold font-14" width="40px">:</td>
                                            <td class="font-14 color-a4a4a4">{{$data->getCustomer->name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold font-14 color-323232">Status</td>
                                            <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                            @if(!is_null($data->getCustomer->marital_status))
                                            <td class="font-14 color-a4a4a4">{{$maritalStatus[$data->getCustomer->marital_status]}}</td>
                                            @else
                                            <td class="font-14 color-a4a4a4">-</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold font-14 color-323232">Tanggal Lahir</td>
                                            <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                            <td class="font-14 color-a4a4a4">
                                                @if(!is_null($data->getCustomer->date_of_birth))
                                                    {{$data->getCustomer->place_of_birth}}, {{\Carbon\Carbon::parse($data->getCustomer->date_of_birth)->isoFormat('D MMMM Y')}}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold font-14 color-323232">Jenis Kelamin</td>
                                            <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                            @if($data->getCustomer->gender == 'male')
                                            <td class="font-14 color-a4a4a4">Laki - Laki</td>
                                            @elseif($data->getCustomer->gender == 'female')
                                            <td class="font-14 color-a4a4a4">Perempuan</td>
                                            @else
                                            <td class="font-14 color-a4a4a4">-</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold font-14 color-323232">Usia</td>
                                            <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                            @if(!is_null($data->getCustomer->age))
                                            <td class="font-14 color-a4a4a4">{{$data->getCustomer->age}} tahun</td>
                                            @else
                                            <td class="font-14 color-a4a4a4">-</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold font-14 color-323232">Agama</td>
                                            <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                                            @if(!is_null($data->getCustomer->religion))
                                            <td class="font-14 color-a4a4a4">{{$religion[$data->getCustomer->religion]}}</td>
                                            @else
                                            <td class="font-14 color-a4a4a4">-</td>
                                            @endif
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-12">
                        <hr style="width:98%;margin:0 auto; background:#E9E9E9;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="font-weight-bold mb-3">Detail Reservasi</h5>
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <table class="table-note">
                                    <tr>
                                        <td class="font-weight-bold font-14 color-323232">Tanggal Layanan</td>
                                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4">{{\Carbon\Carbon::parse($data->reservation_date)->isoFormat(' D MMMM Y  HH:mm:ss')}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-14 font-weight-bold color-323232">Layanan</td>
                                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4">{{$data->getProduct->name}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-14 font-weight-bold color-323232">Dokter</td>
                                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                        <td class="font-14 color-a4a4a4">{{$data->getStaff->getUser->fullname}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-14 font-weight-bold color-323232">Status</td>
                                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                        @if($data->status == 0)
                                        <td>
                                            <span class="status-0 font-12">Belum Berkunjung</span>
                                        </td>
                                        @elseif($data->status == 1)
                                        <td>
                                            <span class="status-1 font-12">Disetujui</span>
                                        </td>
                                        @elseif($data->status == 2)
                                        <td>
                                            <span class="status-1 font-12">Sudah Berkunjung</span>
                                        </td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6 mb-2">
                                <div class="w-80-100" style="margin:0 auto;">
                                    <div class="patient-card">
                                        <table class="table-note">
                                            <tr>
                                                <td class="font-weight-bold font-14 color-323232" style="padding-bottom:0!important;">No. Reservasi</td>
                                                <td class="text-center font-14 font-weight-bold color-323232" width="40px" style="padding-bottom:0!important;">:</td>
                                                <td class="font-24 color-a4a4a4" style="padding-bottom:0!important;">{{$data->reservation_number}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <a href="{{route('merchant.toko.reservation.index')}}" class="btn btn-light mr-2" data-dismiss="modal">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{route('merchant.toko.reservation.save')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    })
});
</script>
@endsection

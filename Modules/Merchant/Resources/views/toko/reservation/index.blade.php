@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Klinik</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.reservation.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Jadwal reservasi membermu sudah kami rangkum disini, kamu bisa melihatnya berdasarkan bulanan, mingguan, atau harian.</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a onclick="activeMenu('klinik','klinik-appointment')" class="nav-link" href="{{route('merchant.toko.appointment.index')}}">Appointment</a>
            </li>
            <li class="nav-item">
                <a onclick="activeMenu('klinik','klinik-appointment')" class="nav-link active" href="{{route('merchant.toko.reservation.index')}}">Reservasi</a>
            </li>
        </ul>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-md-6 mb-3">
                        <button 
                            id="button-add" 
                            type="button" 
                            class="btn btn-success btn-rounded btn-sm py-2 px-4" 
                            data-toggle="modal" 
                            data-target="#modal-reservation"
                        >
                            <i class="fa fa-plus mr-2"></i> Tambah Reservasi
                        </button>
                    </div>

                    <div class="col-md-6 mb-3 text-right">
                        @php
                            $link = url('/reservation/add');
                            $link .= '/'.merchant_id().'?page=cust';
                        @endphp
                        <button type="button" class="btn-preview py-1 px-2 rounded mr-2" onclick="copyClipboard('{{$link}}')" data-placement="bottom" title="Salin URL Reservasi Online">
                            <i id="fa-copy" title="copied" class="far fa-copy mr-2"></i>
                            <span>Salin URL Reservasi Online</span>
                        </button>
                    </div>
                </div>
                <div id="output-sale-order">
                    @include('merchant::toko.reservation.list')
                </div>
            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" onsubmit="return false" class="px-30">
            <div class="row justify-content-end align-items-center">
                <div class="col-md-12 mb-3">
                    <label for="exampleInputPassword1">Tanggal Reservasi</label>
                    <br>
                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" id="all_date" name="date_opsi" value="1" checked>
                        <label class="form-check-label" for="inlineCheckbox1">Semua Tanggal</label>
                    </div>
                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" id="period_date" name="date_opsi" value="2">
                        <label class="form-check-label" for="inlineCheckbox1">Rentang Waktu</label>
                    </div>
                    <div class="form-group" id="range_date">
                        <label><small>Tanggal Awal</small></label>
                        <input type="text" class="form-control trans_time2 mb-3" id="start_date" name="start_date">
                        <label><small>Tanggal Akhir</small></label>
                        <input type="text" class="form-control trans_time2" id="end_date" name="end_date">
                    </div>
                </div>
                <div class="col-md-12">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                    <input type="hidden" name="key" value="{{$key_val}}">
                    <input name="today" type="hidden" id="today" value="{{$date}}" autocomplete="off">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>
                </div>

            </div>

        </form>

    </div>


    <div class="modal fade z1600" id="modal-reservation" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content"> 
                <div class="modal-header d-flex justify-content-end align-items-center py-0" style="border-bottom:none!important;">
                    <button style="background:#C9C9C9;color:#fff;" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <h4 class="font-weight-bold" style="color:#747474;">Apakah pasien sudah<br>mempunyai nomor member ?</h4>
                </div>
                <div class="modal-footer" style="border-top:none!important;">
                    <a style="border-radius:0.5rem!important;margin-bottom:0!important;margin-top:0!important;" class="btn btn-success rounded-0 py-2" href="{{route('merchant.toko.reservation.create', ['page'=>'cust'])}}">Belum</a>
                    <a style="border-radius:0.5rem!important;margin-bottom:0!important;margin-top:0!important;" class="btn btn-light py-2" href="{{route('merchant.toko.reservation.add')}}">Sudah</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        $('#range_date').hide();
        $('#all_date').on('click', function(e){
            $('#range_date').hide();
        });
        $('#period_date').on('click', function(e){
            $('#range_date').show();
        });

        $("#start_date").val(moment($("#today").val(),'YYYY-MM-DD').startOf('month').format('YYYY-MM-DD'));
        $("#end_date").val(moment($("#today").val(),'YYYY-MM-DD').endOf('month').format('YYYY-MM-DD'));
        $('.trans_time2').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        $('#form-filter-sale-order').submit(function () {
            var data = getFormData('form-filter-sale-order');
            let date_opsi = data.get('date_opsi');
            if(date_opsi == 1){
                data.delete('start_date');
                data.delete('end_date');
            }
            data.delete('date_opsi');
            ajaxTransfer("{{route('merchant.toko.reservation.reload-data')}}", data,'#output-sale-order');
            showFilter('btn-show-filter3', 'form-filter3');
        });
    });

    $('.btn-preview').tooltip();
    $('#fa-copy').tooltip('hide');
    function copyClipboard(url){
        let copyText = url;
        let el = document.createElement('input');
        document.body.appendChild(el);
        el.value = url;
        el.select();
        document.execCommand("copy");
        document.body.removeChild(el);
        $('#fa-copy').tooltip('show');
        setTimeout(() => {
            $('#fa-copy').tooltip('dispose');
        }, 1000);
    }
</script>
@endsection
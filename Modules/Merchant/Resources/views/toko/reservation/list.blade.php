<div class="table-responsive">
    <table class="table table-custom" id="table-data-cust-level" width="100%" cellspacing="0">
        <thead>
        <tr>
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<script>
    function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
            ajaxTransfer("{{route('merchant.toko.reservation.delete')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    }
    function createQueue(id){
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Buat no antrian untuk reservasi ?", function () {
            ajaxTransfer("{{route('merchant.toko.reservation.create-queue')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })

    }
    function reloadDataTable(isReload=0)
    {
        ajaxDataTable('#table-data-cust-level', 1, "{{route('merchant.toko.reservation.datatable')}}?key_val={{$key_val}}&sk={{$searchKey}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ],0);
    }

    function exportData() {
        modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
            var data = getFormData('form-filter-sale-order');
            ajaxTransfer("{{route('merchant.toko.reservation.export-data')}}", data, '#modal-output');
        });
    }

    $(document).ready(function() {
        reloadDataTable(0)
    })
</script>

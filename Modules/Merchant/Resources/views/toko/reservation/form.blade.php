@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Klinik</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.reservation.index')}}">Reservasi</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Dihalaman ini kamu dapat menambahkan data reservasi</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4 rounded">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Data Member <span class="text-danger">*</span></label>
                                        <select name="sc_customer_id" id="sc_customer_id" required>
                                            <option></option>
                                            @foreach($customer as $key => $item)
                                            <option value="{{$item->id}}" @if($data->sc_customer_id == $item->id) selected @endif>{{$item->code}}-{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Tanggal Reservasi <span class="text-danger">*</span></label>
                                        <input 
                                            type="text" 
                                            class="form-control form-control-sm trans_time" 
                                            name="reservation_date" 
                                            placeholder="Tanggal Reservasi"
                                            @if(is_null($data->reservation_date))
                                            value="{{date('Y-m-d H:i:s')}}" 
                                            @else
                                            value="{{$data->reservation_date}}" 
                                            @endif
                                            disabled
                                        >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Layanan <span class="text-danger">*</span></label>
                                        <select name="sc_product_id" id="sc_product_id" required>
                                            <option></option>
                                            @foreach($service as $key => $item)
                                            <option value="{{$item->id}}" @if($data->sc_product_id == $item->id) selected @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Dokter <span class="text-danger">*</span></label>
                                        <select name="md_merchant_staff_id" id="md_merchant_staff_id">
                                            <option></option>
                                            @foreach($doctor as $key => $item)
                                            <option value="{{$item->id}}" @if($data->md_merchant_staff_id == $item->id) selected @endif>{{$item->getUser->fullname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="font-weight-bold d-block">Catatan :</label>
                                        <span style="color: #A4A4A4;">Disini anda dapat memilih tanggal reservasi, layanan, & Dokter yang sesuai dengan jadwal pasien anda. Setelah mengisi pastikan anda mengecek data tersebut sebelum menyimpan kartu reservasi.</span>
                                    </div>
                                </div>     
                            </div>
                            <input type='hidden' name='id' value='{{$data->id }}'>
                            <input type='hidden' name='_token' value='{{csrf_token()}}'>
                            <input type='hidden' name='is_from_create' value='0'>

                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{route('merchant.toko.reservation.index')}}" class="btn btn-light mr-2" data-dismiss="modal">Kembali</a>
                                    @if($data->status != 1)
                                    <button type="submit" class="btn btn-success">
                                        Simpan
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 mb-3">
                        @if(is_null($data->id))
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="print-card mb-2 d-flex justify-content-center align-items-center" style="height: 400px;">
                                    <span style="color: #A4A4A4;">Belum ada kartu yang dicetak </span>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div class="print-card mb-2">
                                    <h3 class="text-center font-weight-bold">{{ucwords($data->getMerchant->name)}}</h3>
                                    <span class="d-block text-center mb-2" style="color:#A4A4A4;">{{$data->getMerchant->address}}</span>
                                    @if(is_null($data->queue_number))
                                    <div style="width:100%;">
                                        <img style="width:100%; height:250px;object-fit:contain;" src="{{env('S3_URL')}}{{$data->qr_code_file}}" alt="qr_code">
                                    </div>
                                    @else
                                    <h1 class="text-center font-weight-bold" style="font-size:90px;">{{$data->queue_number}}</h1>
                                    @endif
                                    <div style="width:70%; margin:0 auto;">
                                        <dl class="d-flex justify-content-between mb-0">
                                            <dt>No Reservasi</dt>
                                            <dd>{{$data->reservation_number}}</dd>
                                        </dl>
                                        <dl class="d-flex justify-content-between mb-0">
                                            <dt>No Member</dt>
                                            <dd>{{$data->getCustomer->code}}</dd>
                                        </dl>
                                        <dl class="d-flex justify-content-between mb-0">
                                            <dt>Nama</dt>
                                            <dd>{{$data->getCustomer->name}}</dd>
                                        </dl>
                                        <dl class="d-flex justify-content-between mb-0">
                                            <dt>Layanan</dt>
                                            <dd>{{$data->getProduct->name}}</dd>
                                        </dl>
                                        <dl class="d-flex justify-content-between mb-0">
                                            <dt>Dokter</dt>
                                            <dd>{{$data->getStaff->getUser->fullname}}</dd>
                                        </dl>
                                        <dl class="d-flex justify-content-between mb-0">
                                            <dt>Tanggal</dt>
                                            <dd>{{\Carbon\Carbon::parse($data->reservation_date)->isoFormat(' DD/MM/Y  HH:mm:ss')}}</dd>
                                        </dl>
                                        <h3 class="font-weight-bold text-center mt-3">TerimaKasih</h3>
                                        <span class="d-block text-center" style="color:#A4A4A4;"><i>Powered By Senna</i></span>
                                    </div>
                                </div>
                                <i class="d-block text-center" style="color:#747474;">Nomor Reservasi Pasien</i>
                            </div>
                            <div class="col-md-12 d-flex justify-content-center mb-3">
                                <a class="btn btn-success ml-2 d-block side-width" href="{{route('merchant.toko.reservation.print-pdf', ['id'=>$data->id])}}" target="_blank">
                                    <span class="iconify mr-2" data-icon="fluent:print-24-filled"></span> Download
                                </a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $("#sc_product_id").select2({
        placeholder: "--- Pilih Layanan ---"
    });
    $("#md_merchant_staff_id").select2({
        placeholder: "--- Pilih Dokter ---"
    });
    $("#sc_customer_id").select2({
        placeholder: "--- Cari Data Member ---"
    });
    dateTimePicker('.trans_time');

    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{route('merchant.toko.reservation.save')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    })
});
</script>
@endsection

<div class="row">
    <div class="col-md-12">
        <div class="card card-medical-record">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-sm py-2 px-4" data-toggle="modal" data-target="#modal-add-service">
                            <i class="fa fa-plus mr-2"></i> Tambah Tindakan
                        </button>
                    </div>
                </div>

                <div class="row mb-5">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-custom" id="table-data-cust-level" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <td>Tindakan</td>
                                        <td>Harga</td>
                                        <td>Opsi</td>
                                    </tr>
                                </thead>
                                <tbody id="result-services">
                                    @foreach($dataService as $key => $item)
                                    <tr id="row-{{$item->id}}">
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->selling_price}}</td>
                                        <td>
                                            @if($item->id != $reservation->sc_product_id)
                                            <button type="button" name="remove" data-id="{{$item->id}}" class="btn btn-sm btn-remove">
                                                <span class="iconify" data-icon="codicon:trash"></span>
                                            </button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <form onsubmit="return false;" id="form-action" class='form-horizontal form-konten' backdrop="">
                            <input type='hidden' name='_token' value='{{csrf_token()}}'>
                            <input type='hidden' name='id' value='{{$data->id}}'>
                            <input type="hidden" name="clinic_reservation_id" value="{{$data->clinic_reservation_id}}">
                            <div class="col-md-12 text-right">
                                <a href="{{route('merchant.toko.medical-record.add', ['id'=>$data->id, 'page'=>'diagnosis'])}}" class="btn btn-light mr-2">Kembali</a>
                                <button type="submit" class="btn btn-success text-right">
                                    Simpan & Lanjut <span class="iconify ml-2" data-icon="ooui:next-ltr"></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>


<div class="modal fade z1600" id="modal-add-service" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-list-label">Tambah Tindakan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <div class="form-group">
                        <label class="font-weight-bold">Pilih Tindakan</label>
                        <select name="service_id" id="service_id" class="form-control">
                            <option></option>
                            @foreach($services as $key => $item)
                                <option 
                                    value="{{$item->id}}"
                                    data-name="{{$item->name}}"
                                    data-price="{{$item->selling_price}}"
                                >
                                    {{$item->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-custom">
                            <thead>
                                <tr>
                                    <th>Tindakan</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody id="body-item-services">
                                @foreach($dataService as $key => $item)
                                <tr id="modal-row-{{$item->id}}">
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->selling_price}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-add-list" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Tambah</button>
            </div>
        </div>
    </div>
</div>

<script>
    let dataServiceDb = @json($data->service);
    let dataService = [];

    if(dataServiceDb){
        JSON.parse(dataServiceDb).forEach(item => {
            dataService.push({
                "name":item.name,
                "sc_product_id": parseInt(item.sc_product_id),
                "quantity": item.quantity,
                "sub_total": item.sub_total,
                "price":item.price
            })
        });
    }

    $('#modal-add-service').on('shown.bs.modal', function (e) {
        $("#service_id").select2({
            placeholder:'--- Pilih Tindakan ---'
        });
    });

    $('#modal-add-service').on('hidden.bs.modal', function (e) {
        $("#service_id").val('').change();
    });

    $("#service_id").on('change', function(){
        if($(this).val() == ''){
            return false;
        }
        let checkService = dataService.filter(item => {
            return item.sc_product_id == $(this).val();
        });
        if(checkService.length > 0){
            return false;
        }
        let html = `<tr id="modal-row-${$(this).val()}">
                        <td>${$(this).find(":selected").attr('data-name')}</td>
                        <td>${$(this).find(":selected").attr('data-price')}</td>
                    </tr>`
        $("#body-item-services").append(html);
        let resultHtml = ``;
        resultHtml += `<tr id="row-${$(this).val()}">
                        <td>${$(this).find(":selected").attr('data-name')}</td>
                        <td>${$(this).find(":selected").attr('data-price')}</td>
                        <td>
                            <button type="button" name="remove" data-id="${$(this).val()}" class="btn btn-sm btn-remove">
                                <span class="iconify" data-icon="codicon:trash"></span>
                            </button>
                        </td>
                    </tr>`;
        $("#result-services").append(resultHtml);
        dataService.push({
            "name":$(this).find(":selected").attr("data-name"),
            "sc_product_id": parseInt($(this).val()),
            "quantity":1,
            "price":parseInt($(this).find(":selected").attr('data-price')),
            "sub_total":parseInt($(this).find(":selected").attr('data-price')) * 1
        });
    });

    $("#btn-add-list").on('click', function(){
        $("#modal-add-service").modal('hide');
    });

    $(document).on('click', '.btn-remove', function(){
        var button_id = $(this).attr("data-id");
        $('#row-'+button_id+'').remove();
        $('#modal-row-'+button_id+'').remove();
        dataService = dataService.filter(item => {
            return item.sc_product_id != button_id
        });
    });


    $(document).ready(function(){
        $('#form-action').submit(function () {
            var data = getFormData('form-action');

            data.append('services', JSON.stringify(dataService));

            ajaxTransfer("{{route('merchant.toko.medical-record.save-action')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    });

</script>
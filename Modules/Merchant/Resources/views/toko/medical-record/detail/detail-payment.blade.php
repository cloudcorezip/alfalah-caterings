<div class="row">
    <div class="col-md-12">
        <div class="card card-medical-record">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <h5 class="font-weight-bold">{{$merchant->name}}</h5>
                            <span class="color-a4a4a4">No Transaksi {{$saleOrder->code}}</span>
                        </div>
                        <div class="col-md-6">
                            {{---<div class="form-group row d-flex align-items-center">
                                <label class="font-weight-bold col-md-6 text-right">Petugas</label>
                                <div class="col-md-6"> 
                                    <select class="form-control" name="staff_id" id="staff" @if(!is_null($saleOrder)) disabled @endif>
                                        <option></option>
                                        @foreach($staff as $key => $item)
                                        <option 
                                            value="{{$item->id}}"
                                            @if(!is_null($saleOrder))
                                                @if($saleOrder->created_by == $item->id)
                                                selected
                                                @endif
                                            @endif
                                        >
                                            {{$item->fullname}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>---}}
                            <div class="form-group row d-flex align-items-center">
                                <label class="font-weight-bold col-md-6 text-right">Perawat</label>
                                <div class="col-md-6"> 
                                    @if(!is_null($saleOrder))
                                        @php
                                            $userHelper = is_null($saleOrder->assign_to_user_helper)? [] : json_decode($saleOrder->assign_to_user_helper);
                                            $userHelperIds = [];
                                        @endphp
                                        @foreach($userHelper as $k => $i)
                                            @php
                                                array_push($userHelperIds, $i->id);
                                            @endphp
                                        @endforeach
                                    @endif

                                    <select multiple="multiple" class="form-control" name="user_helper[]" id="user_helper" disabled>
                                        <option></option>
                                        @foreach($staff as $key => $item)
                                        <option 
                                            value="{{$item->id}}"
                                            @if(!is_null($saleOrder))
                                                @if(in_array($item->id, $userHelperIds))
                                                    selected
                                                @endif
                                            @endif
                                        >
                                            {{$item->fullname}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <table width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="4">
                                            <hr>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="pt-2 pb-4">Nama Layanan</th>
                                        <th class="pt-2 pb-4 text-center">Harga Jual</th>
                                        <th class="pt-2 pb-4 text-center">Qty</th>
                                        <th class="pt-2 pb-4 text-right">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $promo_product = 0;
                                    @endphp
                                    @foreach($saleOrder->getDetail->where('is_deleted', 0) as $key => $item)
                                        <tr>
                                            <td class="pl-4">
                                                <span class="color-323232 font-14">{{$item->getProduct->name}}</span>
                                            </td>
                                            <td class="text-center">{{rupiah($item->price)}}</td>
                                            <td class="text-center">{{$item->quantity}}</td>
                                            <td class="text-right">
                                            {{rupiah($item->price*$item->quantity)}}
                                            </td>
                                        </tr>
                                        @if ($item->multi_quantity!=0)
                                            @if($item->price*$item->multi_quantity!=$item->sub_total)
                                                @php
                                                    $promo_product+=$item->price*$item->quantity-$item->sub_total;
                                                @endphp
                                            @endif
                                        @else
                                            @if($item->price*$item->quantity!=$item->sub_total)
                                                @php
                                                    $promo_product+=$item->price*$item->quantity-$item->sub_total;
                                                @endphp
                                            @endif
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td colspan="4">
                                            <hr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            {{---<div class="form-check d-flex align-items-center">
                                                <input class="form-check-input mt-0" type="checkbox" id="ar_check" value="1" @if($saleOrder->is_debet == 1) checked @endif disabled>
                                                <label class="form-check-label mt-0 font-weight-bold" for="gridCheck">Hutang</label>
                                            </div>---}}
                                        </td>
                                        <td class="text-right font-14 font-weight-bold">Diskon</td>
                                        <td class="text-right font-14">
                                            <span>Rp {{rupiah((is_null($saleOrder->promo))?0+$promo_product:$saleOrder->promo+$promo_product)}}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                        <td class="text-right font-14 font-weight-bold">Pajak</td>
                                        <td class="text-right font-14">
                                            <span>Rp {{rupiah($saleOrder->tax)}}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                        <td class="text-right font-14 font-weight-bold">Biaya Admin (Payment Digital)</td>
                                        <td class="text-right font-14">
                                            <span>Rp {{rupiah($saleOrder->admin_fee)}}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                        <td class="text-right font-14 font-weight-bold">Total</td>
                                        <td class="text-right font-14">
                                            <span>Rp {{rupiah($saleOrder->total+$saleOrder->admin_fee)}}</span>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="{{route('merchant.toko.medical-record.detail', ['id'=>$data->id, 'page'=>'recipe'])}}" class="btn btn-light mr-2">Kembali</a>
                            <a class="btn btn-success" href="{{route('merchant.toko.sale-order.detail', ['id'=>$saleOrder->id])}}">Lihat Detail Transaksi</a>
                        </div>
                    </div>
                </form>
               
            </div>
        </div>
    </div>
</div>

<script>
     $(document).ready(function(){
        $("#user_helper").select2({
            placeholder: '--- Pilih Perawat ---'
        });
     });
</script>
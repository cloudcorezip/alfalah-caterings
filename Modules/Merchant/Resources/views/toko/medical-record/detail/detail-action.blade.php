<div class="row">
    <div class="col-md-12">
        <div class="card card-medical-record">
            <div class="card-body">
                <div class="row mb-5">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-custom" id="table-data-cust-level" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>Tindakan</td>
                                        <td>Harga</td>
                                    </tr>
                                </thead>
                                <tbody id="result-services">
                                    @foreach($dataService as $key => $item)
                                    <tr id="row-{{$item->id}}">
                                        <td>{{$key + 1}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{rupiah($item->selling_price)}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="{{route('merchant.toko.medical-record.detail', ['id'=>$data->id, 'page'=>'diagnosis'])}}" class="btn btn-light mr-2">Kembali</a>
                        <a href="{{route('merchant.toko.medical-record.detail', ['id'=>$data->id, 'page'=>'recipe'])}}" class="btn btn-success">
                            Selanjutnya <span class="iconify ml-2" data-icon="ooui:next-ltr"></span>
                        </a>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
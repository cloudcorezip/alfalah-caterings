<div class="row">
    <div class="col-md-12">
        <div class="card card-medical-record">
            <div class="card-body">
                <div class="row mb-5">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-custom" id="table-data-cust-level" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <td>No</td>
                                        <td>Nama</td>
                                        <td>Kategori</td>
                                        <td>Satuan</td>
                                        <td>Aturan</td>
                                        <td>Qty</td>
                                    </tr>
                                </thead>
                                <tbody id="result-recipes">
                                    @foreach($dataRecipe as $key => $item)
                                        @php
                                            $recipeById = array_values(
                                                                array_filter(json_decode($data->recipe), function($var) use($item){
                                                                    return $item->id == $var->sc_product_id;
                                                                })
                                                            )[0];
                                            $qty = $recipeById->quantity;
                                        @endphp
                                        <tr id="row-{{$item->id}}">
                                            <td>{{$key + 1}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->getCategory->name}}</td>
                                            <td>{{$item->getUnit->name}}</td>
                                            <td>{!! $item->description !!}</td>
                                            <td width="10%">{{$qty}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="{{route('merchant.toko.medical-record.detail', ['id'=>$data->id, 'page'=>'action'])}}" class="btn btn-light mr-2">Kembali</a>
                        <a href="{{route('merchant.toko.medical-record.detail', ['id'=>$data->id, 'page'=>'payment'])}}" class="btn btn-success">
                            Selanjutnya <span class="iconify ml-2" data-icon="ooui:next-ltr"></span>
                        </a>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
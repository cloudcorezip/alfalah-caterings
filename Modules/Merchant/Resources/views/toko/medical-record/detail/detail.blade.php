@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Klinik</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.medical-record.index')}}">Rekam Medis</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Lakukan tindakan sesuai dengan langkah langkah yang disediakan</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4 rounded py-3 px-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5 class="font-weight-bold mb-3">Identitas Member</h5>
                    </div>
                    <div class="col-lg-6 col-md-12 py-4">
                        <table class="table-note">
                            <tr>
                                <td class="font-weight-bold font-14 color-323232">No. Member</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                <td class="font-14 color-a4a4a4">{{$reservation->getCustomer->code}}</td>
                            </tr>
                            <tr>
                                <td class="font-14 font-weight-bold color-323232">No. Reservasi</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                <td class="font-14 color-a4a4a4">{{$reservation->reservation_number}}</td>
                            </tr>
                            <tr>
                                <td class="font-14 font-weight-bold color-323232">Nama</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                <td class="font-14 color-a4a4a4">{{$reservation->getCustomer->name}}</td>
                            </tr>
                            <tr>
                                <td class="font-14 font-weight-bold color-323232">Tanggal Lahir</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                <td class="font-14 color-a4a4a4">
                                    @if(!is_null($reservation->getCustomer->date_of_birth))
                                    {{\Carbon\Carbon::parse($reservation->getCustomer->date_of_birth)->isoFormat(' D MMMM Y ')}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="font-14 font-weight-bold color-323232">Jenis Kelamin</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                @if($reservation->getCustomer->gender == 'male')
                                <td class="font-14 color-a4a4a4">Laki - Laki</td>
                                @elseif($reservation->status == 'female')
                                <td class="font-14 color-a4a4a4">Perempuan</td>
                                @endif
                            </tr>
                        </table>
                    </div>
                    <div class="col-lg-6 col-md-12 py-4">
                        <table class="table-note">
                            @if(!is_null($reservation->getCustomer->sc_customer_level_id))
                            <tr>
                                <td class="font-weight-bold font-14 color-323232">Kategori Pasien</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                <td class="font-14 color-a4a4a4">{{$reservation->getCustomer->getCustLevel->name}}</td>
                            </tr>
                            @endif
                            <tr>
                                <td class="font-weight-bold font-14 color-323232">Tanggal Reservasi</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                <td class="font-14 color-a4a4a4">{{\Carbon\Carbon::parse($reservation->reservation_date)->isoFormat(' D MMMM Y  HH:mm:ss')}}</td>
                            </tr>
                            <tr>
                                <td class="font-14 font-weight-bold color-323232">Jenis Layanan</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                <td class="font-14 color-a4a4a4">{{$reservation->getProduct->name}}</td>
                            </tr>
                            <tr>
                                <td class="font-14 font-weight-bold color-323232">Dokter</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                <td class="font-14 color-a4a4a4">{{$reservation->getStaff->getUser->fullname}}</td>
                            </tr>
                            <tr>
                                <td class="font-14 font-weight-bold color-323232">Status</td>
                                <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                @if($reservation->status == 0)
                                <td>
                                    <span class="status-0 font-12">Belum Berkunjung</span>
                                </td>
                                @elseif($reservation->status == 1)
                                <td>
                                    <span class="status-1 font-12">Disetujui</span>
                                </td>
                                @elseif($reservation->status == 2)
                                <td>
                                    <span class="status-1 font-12">Sudah Berkunjung</span>
                                </td>
                                @endif
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                
                <div class="row mb-5">
                    <div class="col-md-12">
                        <h5 class="font-weight-bold mb-3">Langkah Tindakan</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="record-step-wrapper">
                            <div class="record-step active py-2 px-2">
                                <span>Diagnosa</span>
                            </div>
                            <div class="record-step py-2 px-2 @if($page=='action' || $page=='recipe' || $page=='payment') active @endif">
                                <span>Tindakan</span>
                            </div>
                            <div class="record-step py-2 px-2 @if($page=='recipe' || $page=='payment') active @endif">
                                <span>Resep</span>
                            </div>
                            <div class="record-step py-2 px-2 @if($page=='payment') active @endif">
                                <span>Pembayaran</span>
                            </div>
                        </div>
                    </div>
                </div>
                
                @if($page == 'diagnosis')
                    @include('merchant::toko.medical-record.detail.detail-diagnosa')
                @endif

                @if($page == 'action')
                    @include('merchant::toko.medical-record.detail.detail-action')
                @endif

                @if($page == 'recipe')
                    @include('merchant::toko.medical-record.detail.detail-recipe')
                @endif
                
                @if($page == 'payment')
                    @include('merchant::toko.medical-record.detail.detail-payment')
                @endif

            </div>
        </div>
        
    </div>
@endsection

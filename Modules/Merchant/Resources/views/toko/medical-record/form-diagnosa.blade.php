<div class="row">
    <div class="col-md-12">
        <div class="card card-medical-record">
            <div class="card-body">
                <h6 class="font-weight-bold">CATATAN HASIL DIAGNOSA</h6>
                <span class="color-a4a4a4">Lihat hasil diagnosa dari pasien mu disini</span>
                <hr>

                <form onsubmit="return false;" id="form-diagnosis" class='form-horizontal form-konten' backdrop="">
                    @php
                        $num = 1;
                    @endphp
                    @foreach($templateDiagnosis as $key => $item)
                        @if($item->type == 'text')
                        <div class="form-group row d-flex align-items-center">
                            <label class="font-weight-bold col-lg-10 col-md-6" style="color:#323232!important;">{{$num}}. {{$item->question}}</label>
                            <div class="col-lg-2 col-md-6 text-right">
                                <input type="text" class="form-control" name="{{$item->name}}" value="{{$item->value}}">
                            </div>
                        </div>
                        @endif

                        @if($item->type == 'file')
                        <div class="form-group row d-flex align-items-center">
                            <label class="font-weight-bold col-lg-8 col-md-6" style="color:#323232!important;">{{$num}}. {{$item->question}}</label>
                            <div class="col-lg-4 col-md-6 text-right">
                                <input type="file" class="form-control" name="{{$item->name}}" accept="image/*">
                            </div>
                            @if(!is_null($item->value))
                            <div class="col-lg-12 mt-4 text-right">
                                <img src="{{env('S3_URL')}}{{$item->value}}" alt="" style="width: 8%;">
                            </div>
                            @endif
                        </div>
                        @endif

                        @if($item->type == 'radio')
                        <div class="form-group">
                            <label class="font-weight-bold" style="color:#323232!important;">{{$num}}. {{$item->question}}</label>
                            <div class="row pl-3">
                                @foreach(json_decode($item->option) as $k => $i)
                                <div class="col-md-auto">
                                    <div class="form-check d-flex align-items-center">
                                        <input 
                                            class="form-check-input mt-0" 
                                            type="radio" 
                                            name="{{$item->name}}" 
                                            value="{{$i}}"
                                            @if($i == $item->value)
                                                checked
                                            @endif
                                        >
                                        <label class="form-check-label mt-0">{{$i}}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endif

                        @php
                            $num++;
                        @endphp


                    @endforeach
                    

                    <input type='hidden' name='_token' value='{{csrf_token()}}'>
                    <input type='hidden' name='id' value='{{$data->id}}'>
                    <input type="hidden" name="clinic_reservation_id" value="{{$data->clinic_reservation_id}}">

                    <div class="row mt-4">
                        <div class="col-md-12 text-right">
                            <a class="btn btn-light mr-2" href="{{route('merchant.toko.medical-record.index')}}">
                                Kembali
                            </a>
                            @if(count($templateDiagnosis) > 0)
                            <button type="submit" class="btn btn-success">
                                Simpan & Lanjut <span class="iconify ml-2" data-icon="ooui:next-ltr"></span>
                            </button>
                            @else
                            <a class="btn btn-success" href="{{route('merchant.toko.template-diagnosis.index')}}">
                                Buat Template Diagnosa
                            </a>
                            @endif
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
    $('#form-diagnosis').submit(function () {
        var data = getFormData('form-diagnosis');
        ajaxTransfer("{{route('merchant.toko.medical-record.save-diagnosis')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
        });
    })
})
</script>
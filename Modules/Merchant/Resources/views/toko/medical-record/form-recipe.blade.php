<div class="row">
    <div class="col-md-12">
        <div class="card card-medical-record">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-sm py-2 px-4" data-toggle="modal" data-target="#modal-add-recipe">
                            <i class="fa fa-plus mr-2"></i> Tambah Resep
                        </button>
                    </div>
                </div>

                <div class="row mb-5">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-custom" id="table-data-cust-level" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <td>Nama</td>
                                        <td>Kategori</td>
                                        <td>Satuan</td>
                                        <td>Aturan</td>
                                        <td>Qty</td>
                                        <td>Opsi</td>
                                    </tr>
                                </thead>
                                <tbody id="result-recipes">
                                    @foreach($dataRecipe as $key => $item)
                                        @php
                                            $recipeById = array_values(
                                                                array_filter(json_decode($data->recipe), function($var) use($item){
                                                                    return $item->id == $var->sc_product_id;
                                                                })
                                                            )[0];
                                            $qty = $recipeById->quantity;
                                        @endphp
                                        <tr id="row-{{$item->id}}">
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->getCategory->name}}</td>
                                            <td>{{$item->getUnit->name}}</td>
                                            <td>{{$item->description}}</td>
                                            <td width="10%">
                                                <input type="number" class="form-control qty" data-id="{{$item->id}}" value="{{$qty}}">    
                                            </td>
                                            <td>
                                                <button type="button" name="remove" data-id="{{$item->id}}" class="btn btn-sm btn-remove">
                                                    <span class="iconify" data-icon="codicon:trash"></span>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <form onsubmit="return false;" id="form-recipe" class='form-horizontal form-konten' backdrop="">
                            <input type='hidden' name='_token' value='{{csrf_token()}}'>
                            <input type='hidden' name='id' value='{{$data->id}}'>
                            <input type="hidden" name="clinic_reservation_id" value="{{$data->clinic_reservation_id}}">
                            <div class="col-md-12 text-right">
                                <a href="{{route('merchant.toko.medical-record.add', ['id'=>$data->id, 'page'=>'action'])}}" class="btn btn-light mr-2">Kembali</a>
                                <button type="submit" class="btn btn-success text-right">
                                    Simpan & Lanjut <span class="iconify ml-2" data-icon="ooui:next-ltr"></span>
                                </button>
                            </div>
                        </form>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</div>


<div class="modal fade z1600" id="modal-add-recipe" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-list-label">Tambah Resep</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <div class="form-group">
                        <label class="font-weight-bold">Pilih Resep</label>
                        <select name="recipe_id" id="recipe_id" class="form-control">
                            <option></option>
                            @foreach($recipe as $key => $item)
                                <option 
                                    value="{{$item->id}}"
                                    data-name="{{$item->name}}"
                                    data-price="{{$item->selling_price}}"
                                    data-unit-name="{{$item->getUnit->name}}"
                                    data-category-name="{{$item->getCategory->name}}"
                                    data-description="{{$item->description}}"
                                >
                                    {{$item->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-custom">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama Item</th>
                                    <th>Satuan</th>
                                    <th>Kategori</th>
                                </tr>
                            </thead>
                            <tbody id="body-item-recipes">
                                @foreach($dataRecipe as $key => $item)
                                    <tr id="modal-row-{{$item->id}}">
                                        <td>{{$item->code}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->getUnit->name}}</td>
                                        <td>{{$item->getCategory->name}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-add-list" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Simpan</button>
            </div>
        </div>
    </div>
</div>

<script>
    let dataRecipeDb = @json($data->recipe);
    let dataRecipe = [];

    if(dataRecipeDb){
        JSON.parse(dataRecipeDb).forEach(item => {
            dataRecipe.push({
                "name":item.name,
                "sc_product_id": parseInt(item.sc_product_id),
                "quantity": item.quantity,
                "price": item.price,
                "sub_total":item.sub_total
            })
        });
    }

    $('#modal-add-recipe').on('shown.bs.modal', function (e) {
        $("#recipe_id").select2({
            placeholder:'--- Pilih Resep ---'
        });
    });

    $('#modal-add-recipe').on('hidden.bs.modal', function (e) {
        $("#recipe_id").val('').change();
    });

    $("#recipe_id").on('change', function(){
        if($(this).val() == ''){
            return false;
        }
        let checkRecipe = dataRecipe.filter(item => {
            return item.sc_product_id == $(this).val();
        });
        if(checkRecipe.length > 0){
            return false;
        }
        let html = `<tr id="modal-row-${$(this).val()}">
                        <td>${$(this).find(":selected").attr('data-name')}</td>
                        <td>${$(this).find(":selected").attr('data-price')}</td>
                        <td>${$(this).find(":selected").attr('data-unit-name')}</td>
                        <td>${$(this).find(":selected").attr('data-category-name')}</td>
                    </tr>`
        $("#body-item-recipes").append(html);
       
        let resultHtml = ``;
        resultHtml += `<tr id="row-${$(this).val()}">
                        <td>${$(this).find(':selected').attr('data-name')}</td>
                        <td>${$(this).find(":selected").attr('data-category-name')}</td>
                        <td>${$(this).find(":selected").attr('data-unit-name')}</td>
                        <td>${$(this).find(":selected").attr('data-description')}</td>
                        <td width="10%">
                            <input type="number" class="form-control qty" data-id="${$(this).val()}" value="1">    
                        </td>
                        <td>
                            <button type="button" name="remove" data-id="${$(this).val()}" class="btn btn-sm btn-remove">
                                <span class="iconify" data-icon="codicon:trash"></span>
                            </button>
                        </td>
                    </tr>`;
        
        $("#result-recipes").append(resultHtml);

        dataRecipe.push({
            "name": $(this).find(":selected").attr("data-name"),
            "sc_product_id": parseInt($(this).val()),
            "quantity":1,
            "price":parseInt($(this).find(":selected").attr('data-price')),
            "sub_total": parseInt($(this).find(":selected").attr('data-price')) * 1
        });
    });

    $("#btn-add-list").on('click', function(){
        $("#modal-add-recipe").modal('hide');
    });

    $(document).on('change','.qty', function(){
        let val = $(this).val();
        let productId = $(this).attr('data-id');

        let tmpRecipe = [...dataRecipe].map(item => {
            if(item.sc_product_id == productId){
                return {
                    ...item,
                    "quantity":parseInt(val),
                    "sub_total": parseInt(val) * parseInt(item.price)
                }
            } else {
                return {
                    ...item
                }
            }
        });

        dataRecipe = [...tmpRecipe];

    });

    $(document).on('click', '.btn-remove', function(){
        var button_id = $(this).attr("data-id");
        $('#row-'+button_id+'').remove();
        $('#modal-row-'+button_id+'').remove();
        dataRecipe = dataRecipe.filter(item => {
            return item.sc_product_id != button_id
        });
    });


    $(document).ready(function(){
        $('#form-recipe').submit(function () {
            var data = getFormData('form-recipe');
            data.append('recipes', JSON.stringify(dataRecipe));

            ajaxTransfer("{{route('merchant.toko.medical-record.save-recipe')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    });

</script>
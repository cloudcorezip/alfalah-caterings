@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Klinik</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.medical-record.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Lihat data rekam medis membermu dan lakukan tindakan sesuai dengan langkah-langkah yang disediakan</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4">
            <div class="card-body">
                <div id="output-sale-order">
                    @include('merchant::toko.medical-record.list')
                </div>
            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" onsubmit="return false" class="px-30">
            <div class="row justify-content-end align-items-center">
                <div class="col-md-12 mb-3">
                    <label for="exampleInputPassword1">Tanggal Reservasi</label>
                    <br>
                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" id="all_date" name="date_opsi" value="1" checked>
                        <label class="form-check-label" for="inlineCheckbox1">Semua Tanggal</label>
                    </div>
                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" id="period_date" name="date_opsi" value="2">
                        <label class="form-check-label" for="inlineCheckbox1">Rentang Waktu</label>
                    </div>
                    <div class="form-group" id="range_date">
                        <label><small>Tanggal Awal</small></label>
                        <input type="text" class="form-control trans_time2 mb-3" id="start_date" name="start_date">
                        <label><small>Tanggal Akhir</small></label>
                        <input type="text" class="form-control trans_time2" id="end_date" name="end_date">
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Status</label>
                    <select name="status" id="status">
                        <option value="-1">--- Pilih Status ---</option>
                        <option value="0">Belum Tindakan</option>
                        <option value="1">Sudah Tindakan</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                    <input type="hidden" name="key" value="{{$key_val}}">
                    <input name="today" type="hidden" id="today" value="{{$date}}" autocomplete="off">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                </div>

            </div>

        </form>

    </div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        $("#status").select2();
        $('#range_date').hide();
        $('#all_date').on('click', function(e){
            $('#range_date').hide();
        });
        $('#period_date').on('click', function(e){
            $('#range_date').show();
        });

        $("#start_date").val(moment($("#today").val(),'YYYY-MM-DD').startOf('month').format('YYYY-MM-DD'));
        $("#end_date").val(moment($("#today").val(),'YYYY-MM-DD').endOf('month').format('YYYY-MM-DD'));
        $('.trans_time2').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        $('#form-filter-sale-order').submit(function () {
            var data = getFormData('form-filter-sale-order');
            let date_opsi = data.get('date_opsi');
            if(date_opsi == 1){
                data.delete('start_date');
                data.delete('end_date');
            }
            data.delete('date_opsi');
            ajaxTransfer("{{route('merchant.toko.medical-record.reload-data')}}", data,'#output-sale-order');
            showFilter('btn-show-filter3', 'form-filter3');
        });
    });
</script>
@endsection
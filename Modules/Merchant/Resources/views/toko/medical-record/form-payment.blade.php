<div class="row">
    <div class="col-md-12">
        <div class="card card-medical-record">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <h5 class="font-weight-bold">{{$merchant->name}}</h5>
                        </div>
                        <div class="col-md-6">
                            {{---<div class="form-group row d-flex align-items-center">
                                <label class="font-weight-bold col-md-6 text-right">Pilih Petugas</label>
                                <div class="col-md-6"> 
                                    <select class="form-control" name="staff_id" id="staff" @if(!is_null($saleOrder)) disabled @endif>
                                        <option></option>
                                        @foreach($staff as $key => $item)
                                        <option 
                                            value="{{$item->id}}"
                                            @if(!is_null($saleOrder))
                                                @if($saleOrder->created_by == $item->id)
                                                selected
                                                @endif
                                            @endif
                                        >
                                            {{$item->fullname}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>---}}
                            
                            <div class="form-group row d-flex align-items-center">
                                <label class="font-weight-bold col-md-6 text-right">Pilih Perawat</label>
                                <div class="col-md-6"> 
                                    <select multiple="multiple" class="form-control" name="user_helper[]" id="user_helper" @if(!is_null($saleOrder)) disabled @endif>
                                        <option></option>
                                        @foreach($staff as $key => $item)
                                        <option 
                                            value="{{$item->id}}"
                                        >
                                            {{$item->fullname}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group" id="trans-wrapper">
                                <div class="row d-flex align-items-center">
                                    <label class="font-weight-bold col-md-6 text-right">Pilih Metode Pembayaran</label>
                                    <div class="col-md-6"> 
                                        <select id="trans" class="form-control form-control-sm" name="md_transaction_type_id" required>
                                            <option value="-1">Pilih Metode Pembayaran</option>
                                            @foreach($transType as $item)
                                                <optgroup label="{{$item['name']}}">
                                                    @if(count($item['data'])>0)
                                                        @if($item['name']=='Pembayaran Digital')
                                                            @foreach($item['data'] as $d)
                                                                <optgroup label="--{{$d['name']}}">
                                                                    @if(!empty($d['data']))
                                                                        @foreach($d['data'] as $c)
                                                                            <option value="{{$c['trans_id']}}_{{$c['acc_coa_id']}}_1_{{$c['alias']}}_{{$c['id']}}">{{$c['name']}} </option>

                                                                        @endforeach
                                                                    @endif
                                                                </optgroup>
                                                            @endforeach
                                                        @else
                                                            @foreach($item['data'] as $key =>$other)
                                                                <option value="{{$other->trans_id}}_{{$other->acc_coa_id}}_0">{{$other->name}} @if($item['name']=='Label Pembayaran Lainnya')(Hanya Label)@endif</option>
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="tempo">
                                <div class="row d-flex align-items-center">
                                    <label class="font-weight-bold col-md-6 text-right">Tgl Jatuh Tempo</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control form-control-sm trans_time" name="due_date" value="{{date('Y-m-d H:i:s')}}" placeholder="Jatuh Tempo Pembayaran" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <table width="100%">
                                <thead>
                                    <tr>
                                        <th colspan="4">
                                            <hr>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="pt-2 pb-4">Nama Layanan</th>
                                        <th class="pt-2 pb-4 text-center">Harga Jual</th>
                                        <th class="pt-2 pb-4 text-center">Qty</th>
                                        <th class="pt-2 pb-4 text-right">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($dataService) > 0)
                                    <tr>
                                        <td>
                                            <span class="font-weight-bold">A. Tindakan</span>
                                        </td>
                                    </tr>
                                    @endif
                                    @foreach($dataService as $key => $service)
                                        @php
                                            $serviceById = array_values(
                                                                array_filter(json_decode($data->service), function($var) use($service){
                                                                    return $service->id == $var->sc_product_id;
                                                                })
                                                            )[0];
                                            $serviceQty = $serviceById->quantity;
                                        @endphp
                                        <tr>
                                            <td class="pl-4">
                                                <span class="color-323232 font-14">{{$service->name}}</span>
                                            </td>
                                            <td class="text-center">{{rupiah($service->selling_price)}}</td>
                                            <td class="text-center">{{$serviceQty}}</td>
                                            <td class="text-right">
                                                {{rupiah($service->selling_price * $serviceQty)}}
                                                <input type="hidden" name="subtotal[]" class="subtotal" value="{{$service->selling_price * $serviceQty}}"/>
                                                <input type="hidden" name="sc_product_id[]" class="sc_product_id" value="{{$service->id}}"/>
                                                <input type="hidden" name="quantity[]" class="quantity" value="{{$serviceQty}}"/>
                                                <input type="hidden" name="selling_price[]" class="selling_price" value="{{$service->selling_price}}"/>
                                                <input type="hidden" name="coa_inv_id[]" value="{{$service->inv_id}}"/>
                                                <input type="hidden" name="coa_hpp_id[]" value="{{$service->hpp_id}}"/>
                                            </td>
                                        </tr>
                                    @endforeach
                                    
                                    @if(count($dataRecipe) > 0)
                                    <tr>
                                        <td>
                                            <span class="font-weight-bold">B. Obat</span>
                                        </td>
                                    </tr>
                                    @endif
                                    @foreach($dataRecipe as $key => $recipe)
                                        @php
                                            $recipeById = array_values(
                                                                array_filter(json_decode($data->recipe), function($var) use($recipe){
                                                                    return $recipe->id == $var->sc_product_id;
                                                                })
                                                            )[0];
                                            $recipeQty = $recipeById->quantity;
                                        @endphp
                                        <tr>
                                            <td class="pl-4">
                                                <span class="color-323232 font-14">{{$recipe->name}}</span>
                                            </td>
                                            <td class="text-center">{{rupiah($recipe->selling_price)}}</td>
                                            <td class="text-center">{{$recipeQty}}</td>
                                            <td class="text-right">
                                                {{rupiah($recipe->selling_price * $recipeQty)}}
                                                <input type="hidden" name="subtotal[]" class="subtotal" value="{{$recipe->selling_price * $recipeQty}}"/>
                                                <input type="hidden" name="sc_product_id[]" class="sc_product_id" value="{{$recipe->id}}"/>
                                                <input type="hidden" name="quantity[]" class="quantity" value="{{$recipeQty}}"/>
                                                <input type="hidden" name="selling_price[]" class="selling_price" value="{{$recipe->selling_price}}"/>
                                                <input type="hidden" name="coa_inv_id[]" value="{{$recipe->inv_id}}"/>
                                                <input type="hidden" name="coa_hpp_id[]" value="{{$recipe->hpp_id}}"/>
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4">
                                            <hr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            {{--<div class="form-check d-flex align-items-center">
                                                <input class="form-check-input mt-0" type="checkbox" id="ar_check" value="1">
                                                <label class="form-check-label mt-0 font-weight-bold" for="gridCheck">Catat Sebagai Hutang</label>
                                            </div>--}}
                                        </td>
                                        <td class="text-right font-14 font-weight-bold">Sub Total</td>
                                        <td class="text-right font-14">
                                            <span id="subtotal-text"></span>
                                            <input type="hidden" name="subtotal_all" value="0" id="subtotal_all">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                        <td class="text-right font-14 font-weight-bold">Diskon</td>
                                        <td class="text-right font-14">
                                            <input type="number" class="form-control form-control-sm w-75 ml-auto mr-0 text-right" step="0.01" min="0" max="100" name="discount_percentage" id="discount_percentage" value="0">
                                            <input type="hidden" class="form-control form-control-sm" name="discount" id="discount" value="0">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                        <td class="text-right font-14 font-weight-bold">Pajak</td>
                                        <td class="text-right font-14">
                                            <input type="number" class="form-control form-control-sm w-75 ml-auto mr-0 text-right" name="tax_percentage" id="tax_percentage" step="0.01" min="0" max="100" value="0">
                                            <input type="hidden" class="form-control form-control-sm" name="tax" id="tax" value="0" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                        <td class="text-right font-14 font-weight-bold">Total</td>
                                        <td class="text-right font-14">
                                            <span id="total-text"></span>
                                            <input type="hidden" name="total" value="0" id="total">
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    @if(is_null($saleOrder))
                    <div class="row">
                        <div class="col-md-12 text-right mb-3">
                            <input type="hidden" name="paid_nominal" id="paid_nominal" value="0">
                            <input type="hidden" name="change_nominal" id="change_nominal" value="0" >
                            <input type="hidden" name="warehouse" value="{{$warehouse}}">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="inventory_id" value="{{$inventory_id}}">
                            <input type="hidden" name="coa_json" value="{{$coa_json}}">
                            <input type="hidden" name="warehouse" value="{{$warehouse}}">
                            <input type="hidden" id="trans_time" name="created_at" value="{{date('Y-m-d H:i:s')}}">
                            <input type="hidden" id="is_debet" name="is_debet" value="0">
                            <input type='hidden' name='medical_record_id' value='{{$data->id}}'>
                            <input type="hidden" name="step_type" value="0">

                            <a href="{{route('merchant.toko.medical-record.add', ['id'=>$data->id, 'page'=>'recipe'])}}" class="btn btn-light mr-2">Kembali</a>
                            <button type="submit" class="btn btn-success">
                                Simpan
                            </button>
                        </div>
                        <div class="col-md-12 text-right">
                            <small class="d-block"><i>Pastikan data yang dimasukkan sudah benar. Data yang sudah disimpan tidak dapat diubah.</i></small>
                        </div>
                    </div>
                    @endif
                </form>
               
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#tempo").hide(); 
        $("#staff").select2({
            placeholder: '--- Pilih Petugas ---'
        });

        $("#user_helper").select2({
            placeholder: '--- Pilih Perawat ---'
        });

        $("#trans").select2();

        $( "#discount_percentage").change(function() {
            sumTotal();
        });

        $("#tax_percentage").change(function() {
            sumTotal();
        });

        function sumTotal(){
            var sum = 0;
            $(".subtotal").each(function(){
                sum += +$(this).val();
            });
            $("#subtotal-text").text(formatRupiah(sum.toString(), 'rp'));
            $("#subtotal_all").val(sum);
            var subtotal=$("#subtotal_all").val();
            var tax_percentage=$("#tax_percentage").val();
            var discount_percentage=$("#discount_percentage").val();
            var discount = subtotal*discount_percentage/100;
            var price = subtotal-discount;
            var tax = price*tax_percentage/100;
            var total=price+tax;
            $("#tax").val(tax)
            $("#discount").val(discount)
            $("#total-text").text(formatRupiah(Math.ceil(total).toString(), 'rp'));
            $("#total").val(Math.ceil(total));
        }

        sumTotal();


        $("#ar_check").on('change', function(){
            if($(this).is(":checked")){
                $('#is_debet').val(1);
                $("#tempo").show();
                $("#trans-wrapper").hide();
                $("#trans").val("-1").change();
                
            } else {
                $('#is_debet').val(0);
                $("#tempo").hide(); 
                $("#trans-wrapper").show();   
            }
        })


        $("#form-konten").on("submit", function(){
            var data = getFormData('form-konten');
            data.append("timezone", $('.timezone').val());
            
            var paymentMethod = $("#trans").val();
            var selectedUserHelper = $("#user_helper").select2('data');
            var userHelper = [];

            selectedUserHelper.forEach(item => {
                userHelper.push({
                    id: parseInt(item.id),
                    fullname: item.text.trim()
                })
            });

            data.append('user_helper', JSON.stringify(userHelper));


            if(paymentMethod.indexOf('ID_OVO') == -1){
                ajaxTransfer("{{route('merchant.toko.medical-record.save-payment')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });  
            } else {
                var html = `<form class="form-konten"> 
                                <div class="form-group">
                                    <div class="d-flex align-items-center" style="width:100%;">
                                        <input id="code_country" type="text" class="form-control" name="code_country" value="+62" disabled style="width:16%;">
                                        <input id="mobile_number" type="text" name="mobile_number" class="form-control" placeholder="No. Handphone OVO" style="width:76%;">
                                    </div>
                                    <span id="mobile_number_validate" class="text-danger d-none">No Handphone tidak boleh kosong !</span>
                                </div>
                            </form>`;

                modalConfirm("Masukkan No. Handphone OVO", html, function(){
                    var phoneNumberVal = $("#mobile_number").val();
                    var codeCountry = $("#code_country").val();
                    if(!phoneNumberVal){
                        $("#mobile_number_validate").removeClass("d-none").text("No Handphone tidak boleh kosong !");
                        return;
                    } else {
                        var phoneNum = codeCountry + phoneNumberVal;
                        if(!/^(\+62)([2-9])\d(\d)?[2-9](\d){6,8}$/.test(phoneNum)){
                            $("#mobile_number_validate").removeClass("d-none").text("No Handphone tidak valid !");
                            return;
                        } else {
                            $("#mobile_number_validate").addClass("d-none");
                            data.append("mobile_number", phoneNum);
                            ajaxTransfer("{{route('merchant.toko.medical-record.save-payment')}}", data, function (response){
                                var data = JSON.parse(response);
                                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                            });
                        }
                        
                    }
                    
                });
            }

            
        });
    })
</script>
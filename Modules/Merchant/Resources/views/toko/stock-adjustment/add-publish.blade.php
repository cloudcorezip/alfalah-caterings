@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock-adjustment.data')}}">Penyesuaian Stok</a>

                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Tambahkan penyesuaian stok sesuai dengan isian dibawah ini dengan baik dan benar</span>

            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="results"></div>
                <form id="form-konten" onsubmit="return false">
                    <div class="row">
                        @include('merchant::component.branch-selection',
 ['withoutModal'=>true,
 'data'=>$data,
 'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
 'selector'=>'branch',
 'with_onchange'=>false,
 'function_onchange'=>''

 ])
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Kode Penyesuaian</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="second_code" value="{{is_null($data->second_code)?$data->code:$data->second_code}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Gudang</label>
                                <label class="col-sm-7 col-form-label">{{$data->getWarehouse->name}}</label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Tanggal Penyesuaian Stok</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control form-control-sm trans_time" name="created_at_by" value="{{$data->created_at_by}}" placeholder="Waktu Transaksi" @if(!is_null($data->id)) disabled @endif required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Publish</label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="is_draf" name="is_draf">
                                        <option value="1" @if($data->is_draf==1) selected @endif>Draf</option>
                                        <option value="0" @if($data->is_draf==0) selected @endif>Publish</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row mb-3">
                        <br>
                        <br>
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="25%">Nama Produk</th>
                                    <th width="15%">Stok Sistem</th>
                                    <th width="12%">Stok Fisik</th>
                                    <th width="16%">Satuan</th>
                                    <th width="14%">Selisih</th>
                                    <th width="23%">Harga Satuan <i class="fa fa-question-circle" onclick="infoUnitPrice()"></i></th>
                                    <th width="5%">
                                        Opsi
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="tbodyid">
                                @foreach($data->getDetail as $key =>$item)
                                    <tr id="row{{$key}}">
                                        <td><input type="hidden" name="sc_product_id[]" id="sc_product_id{{$key}}" value="{{$item->sc_product_id}}"><input class="form-control" value="{{$item->product_name}}" disabled></td>
                                        <td>
                                            <input type="hidden" name=stock[]" id="stock{{$key}}" class="form-control stock" value="{{$item->record_stock}}">
                                            <input type="hidden" name="unit_origin[]" class="form-control form-control-sm" value="{{$item->unit_name_origin_display}}"  id="unit_origin{{$key}}" disabled />
                                            <input type="text" name="stock_display[]" class="form-control form-control-sm stock-display" value="{{$item->record_stock}} {{$item->unit_name_origin_display}}"  id="stock_display{{$key}}" disabled />

                                        </td>
                                        <td>
                                            <input type="text" name="stock_real[]" id="stock_real{{$key}}"  class="form-control form-control-sm stock_real" value="{{$item->physical_count}}" onkeypress="return isNumber(event)"  />
                                        </td>
                                        <td>
                                            <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" value="{{$item->unit_name}}" class="form-control form-control-sm unit_name" disabled />
                                            @if($item->is_multi_unit==0)
                                                <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm nilai_konversi" value="1"/>
                                                <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="0"/>
                                                <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control sc_product_id" onchange="getMultiUnitExistData({{$key}})" required>
                                                    @if(is_null($item->unit_name))
                                                        @if(!is_null($item->getProduct->getUnit))
                                                            <option value="{{$item->getProduct->md_unit_id}}">{{$item->getProduct->getUnit->name}}</option>
                                                        @endif
                                                    @else
                                                        <option value="0">{{$item->unit_name}}</option>
                                                    @endif
                                                </select>
                                            @else
                                                <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="{{$item->json_multi_unit}}"/>
                                                <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="{{$item->multi_unit_id}}"/>
                                                <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm nilai_konversi" value="{{$item->value_conversion}}"/>
                                                <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="1"/>
                                                <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control sc_product_id" onchange="getMultiUnitExistData({{$key}})" required>
                                                    @if(!is_null($item->json_multi_unit))
                                                        @foreach(json_decode($item->json_multi_unit) as $m)
                                                            <option value="{{$m->id}}"
                                                                    @if($m->original_id==$item->multi_unit_id) selected @endif
                                                            >{{$m->text}}</option>
                                                        @endforeach
                                                    @else
                                                        @if(is_null($item->unit_name))
                                                            @if(!is_null($item->getProduct->getUnit))
                                                                <option value="{{$item->getProduct->md_unit_id}}">{{$item->getProduct->getUnit->name}}</option>
                                                            @endif
                                                        @else
                                                            <option value="0">{{$item->unit_name}}</option>
                                                        @endif
                                                    @endif
                                                </select>
                                            @endif

                                        </td>
                                        <td>
                                            <input type="text" name="diff_stock[]" id="diff_stock{{$key}}" class="form-control form-control-sm diff" value="{{$item->difference_amount}}" disabled />
                                        </td>
                                        <td>
                                            <input type="text" name="average_price[]" id="average_price{{$key}}" class="form-control form-control-sm average_price" value="{{rupiah($item->average_price)}}" />
                                        </td>
                                        <td><button type="button" name="remove" id="{{$key}}" class="btn btn-xs btn-delete-xs  btn_remove"><span class="fa fa-trash-alt" style="color: white"></span></button>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                            <button type="button" name="add" id="add" class="btn btn-darkblue btn-rounded py-2 px-4">
                                <i class="fa fa-plus mr-2"></i>
                                <span>Tambah Produk</span>
                            </button>
                            <hr>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan :</label>
                                <textarea name="desc" class="form-control form-control-sm" id="" cols="20" rows="5">{{$data->desc}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-5 offset-md-2">

                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <input type="hidden" id="branch" name="md_merchant_id" value="{{$data->md_merchant_id}}">
                            <input type="hidden" id="count_detail" value="{{$data->getDetail->count()+1}}">
                            <input type="hidden" id="warehouse" name="inv_warehouse_id" value="{{$data->inv_warehouse_id}}">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        var productSelectedId = {};

        @foreach($data->getDetail as $j =>$jj)
            productSelectedId[{{$j}}]={{$jj->sc_product_id}}
            @endforeach

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        $(document).ready(function() {

            $("#is_draf").select2()
            $(".sc_product_id").select2()
            $('.average_price').mask('#.##0,00', {reverse: true});
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            var i=$("#count_detail").val()+1;


            $('#add').click(function(){

                i++;
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">' +
                    '<td>' +
                    '<select  name="sc_product_id[]" id="sc_product_id' + i + '" class="form-control form-control-sm" onchange="productChange('+i+')" required ></select>' +
                    '</td>' +
                    '<td>' +
                    '<input type="hidden" name="stock[]" class="form-control form-control-sm stock"  id="stock'+i+'" disabled />' +
                    '<input type="hidden" name="unit_origin[]" class="form-control form-control-sm"  id="unit_origin'+i+'" disabled />' +
                    '<input type="text" name="stock_display[]" class="form-control form-control-sm stock-display"  id="stock_display'+i+'" disabled />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="stock_real[]" id="stock_real'+i+'" class="form-control form-control-sm stock_real" value="0" onkeypress="return isNumber(event)" />' +
                    '</td>' +
                    '<td>' +
                    '<input type="hidden" name="json_multi_unit[]" id="json_multi_unit'+i+'" class="form-control form-control-sm" value="-1"/>' +
                    '<input type="hidden" name="multi_unit_id[]" id="multi_unit_id'+i+'" class="form-control form-control-sm" value="-1"/>' +
                    '<input type="hidden" name="nilai_konversi[]" id="nilai_konversi'+i+'" class="form-control form-control-sm nilai_konversi" value="1"/>' +
                    '<input type="hidden" name="is_multi_unit[]" id="is_multi_unit'+i+'" class="form-control form-control-sm" value="0"/>' +
                    '<input type="hidden" name="unit_name[]" id="unit_name'+i+'" class="form-control form-control-sm" value=""/>' +
                    '<select  name="unit_list[]" id="unit_list' + i + '" class="form-control" onchange="getMultiUnit('+i+')" required></select>' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="diff_stock[]" id="diff_stock'+i+'" class="form-control form-control-sm diff" disabled />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="average_price[]" id="average_price'+i+'" class="form-control form-control-sm average_price" value="0" />' +
                    '</td>' +
                    '<td><button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs  btn_remove"><span class="fa fa-trash-alt" style="color: white"></span></button>' +
                    '</td>' +
                    '</tr>');
                getProductByWarehouse(i)
                $('.average_price').mask('#.##0,00', {reverse: true});
                $("#unit_list"+i).select2()
                if(status){
                    $("#tbodyid").empty()
                }

            });


            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                delete productSelectedId[button_id];

            });
            // $("table").on("change", "input", function() {
            //     var rowId = $(this).closest('tr').attr('id');
            //     var id = rowId.substr(3);
            //     var stock = parseFloat($("#stock"+id).val());
            //     var stock_real=parseFloat($("stock_real"+id).val());
            //     var diff=stock-stock_real;
            //     $("#diff"+id).val(diff)
            // });

            $("table").on("change", "input", function() {
                let row = $(this).closest("tr");
                let stock = parseFloat(row.find(".stock").val());
                let stock_real=parseFloat(row.find(".stock_real").val());
                let conversation= parseFloat(row.find('.nilai_konversi').val())
                let diff=stock-(stock_real*parseFloat(conversation));
                row.find(".diff").val(diff)
            });

        });

        function getProductByWarehouse(i)
        {
            var warehouse=$("#warehouse").val()
            var branch = $("#branch").val()
            $("#sc_product_id" + i).select2({
                ajax: {
                    type: "GET",
                    url: "{{route('api.product.stockadjustment')}}?merchant_id="+branch+"&warehouse="+warehouse,
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });


        }

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.stock-adjustment.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })

        function getMultiUnit(i)
        {

            let unitList = ($("#unit_list"+i).select2('data'))? $("#unit_list"+i).select2('data'):[];
            if(unitList.length>0)
            {
                let data=unitList[0]
                var real=$("#stock_real"+i).val();

                $("#diff_stock"+i).val((parseFloat( $("#stock"+i).val())-(parseFloat(real)*parseFloat(data['nilai_konversi']))))
                $("#is_multi_unit"+i).val(1)
                $("#unit_name"+i).val(data['text'])
                $("#nilai_konversi"+i).val(data['nilai_konversi'])
                $("#multi_unit_id"+i).val(data['original_id'])
            }
        }


        function getMultiUnitExistData(i)
        {

            let unitVal = $("#unit_list"+i).val()
            let jsonMulti= $("#json_multi_unit"+i).val()
            let isMulti= $("#is_multi_unit"+i).val()
            if(isMulti==1)
            {
                var real=$("#stock_real"+i).val();
                let unitList=JSON.parse(jsonMulti)
                unitList.forEach(element=>{
                    if(element.id==unitVal){
                        $("#diff_stock"+i).val((parseFloat( $("#stock"+i).val())-(parseFloat(real)*parseFloat(element.nilai_konversi))))
                        $("#unit_name"+i).val(element.text)
                        $("#nilai_konversi"+i).val(element.nilai_konversi)
                        $("#multi_unit_id"+i).val(element.original_id)
                    }
                })
            }
        }

        function productChange(i)
        {
            var products = ($("#sc_product_id"+i).select2('data'))? $("#sc_product_id"+i).select2('data'):[];
            if(products.length>0)
            {
                let datas = products[0]
                let real=$("#stock_real"+i).val();
                let unitId=$("#unit_list"+i).val()

                $("#stock"+i).val(datas['stock'])
                $("#stock_display"+i).val(datas['stock']+' '+datas['unit_name'])
                if(unitId!=null){
                    $("#diff_stock"+i).val(parseFloat(datas['stock'])-parseFloat(real))
                }
                $("#unit_name"+i).val(datas['unit_name'])
                $("#coa_inv_id"+i).val(datas['coa_inv_id'])
                $("#coa_hpp_id"+i).val(datas['coa_hpp_id'])
                $("#product_name"+i).val(datas['text'])
                let multi_unit=JSON.parse(datas['multi_unit'])
                let multi_unit_list=[]

                $('#unit_list'+i).empty();
                if(multi_unit.length>0)
                {
                    $.each(multi_unit,function (iteration,value){
                        if(iteration===0)
                        {
                            multi_unit_list.push({
                                id:'-1',
                                text:'Pilih Satuan',
                                selling_price:0,
                                nilai_konversi:0,
                                original_id:'-1',
                            })
                        }

                        multi_unit_list.push({
                            id:value.unit_id,
                            text:value.konversi_ke,
                            selling_price:value.harga_jual,
                            nilai_konversi:value.nilai_konversi,
                            original_id:value.id
                        })
                    })
                    $("#unit_list"+i).select2({
                        data:multi_unit_list
                    })
                    $("#json_multi_unit"+i).val(JSON.stringify(multi_unit_list))

                }else{
                    multi_unit_list.push({
                        id:datas['unit_id'],
                        text:datas['unit_name'],
                        selling_price:datas['selling_price'],
                        nilai_konversi:1,
                        original_id:datas['id']
                    })

                    $("#unit_list"+i).select2({
                        data:multi_unit_list
                    })
                }

                let status = false;
                Object.keys(productSelectedId).forEach(key => {
                    if(productSelectedId[key] === parseInt(datas['id'])){
                        status=true;
                    }
                });

                if(status)
                {
                    $('#row'+i).remove();
                    delete productSelectedId[i];
                }else{
                    productSelectedId[i] = parseInt(datas['id']);
                }
            }
        }

        function infoUnitPrice()
        {
            return modalAlert('Harga Satuan','<div class="alert alert-info">Harga satuan diisi ketika penyesuaian stok yang bersifat <b>menambah</b> dan sesuai dengan harga dari <b>satuan</b> yang dipilih.</div>')
        }

    </script>
@endsection

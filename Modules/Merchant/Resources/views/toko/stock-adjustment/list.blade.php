<div class="table-responsive">
    <table class="table table-custom" id="table-data-r-stock-opname" width="100%" cellspacing="0">
        <thead>
        <tr>
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<script>
    function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
            ajaxTransfer("{{route('merchant.toko.stock-adjustment.delete')}}", data, "#modal-output");
        })
    }
    $(document).ready(function() {
        ajaxDataTable('#table-data-r-stock-opname', 1, "{{route('merchant.toko.stock-adjustment.datatable')}}?sk={{$searchKey}}&start_date={{$start_date}}&end_date={{$end_date}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ]);
    })
</script>

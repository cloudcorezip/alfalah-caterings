<style>
    .font-12 {
        font-size: 12px;
    }
    .status-0 {
        background: #FFF5EC;
        padding: 8px 18px;
        color: #FF8100;
        border-radius: 14px;
    }
    .status-1 {
        background: #EAFFDA;
        padding: 8px 18px;
        color: #72BA6C;
        border-radius: 14px;
    }

    .fc-daygrid-event-harness .bt-status-0 {
        border-top: 8px solid #FF8100!important;
    }

    .fc-daygrid-event-harness .bt-status-1 {
        border-top: 8px solid #72BA6C!important;
    }

    .fc-daygrid-event-harness .card-appointment {
        background: #FFFFFF!important;
        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1)!important;
        padding: 10px 20px!important;
        position: relative!important;
        min-height: 100px!important;
        display:block!important;
        margin-bottom: 10px!important;
    }

    .fc-daygrid-event-harness .card-appointment .fc-daygrid-event-dot {
        display:none!important;
    }

    .fc-daygrid-event-harness .card-appointment .fc-event-time {
        position:absolute!important;
        color: #000!important;
        bottom: 10px!important;
    }

    .fc-daygrid-event-harness .card-appointment .fc-event-title {
        color: #000!important;
        font-size: 16px!important;
        display:block!important;    
    }

    .fc-prev-button, .fc-next-button {
        background: #F6F6F6!important;
        border: 1px solid #F6F6F6!important;
        color: #A4A4A4!important;
    }

    .fc-dayGridMonth-button, .fc-timeGridWeek-button, .fc-listWeek-button {
        background: transparent!important;
        border: none!important;
        color: #767676!important;
    }

    .fc-today-button {
        background: #F6F6F6!important;
        color: #A4A4A4!important;
        border: 1px solid #F6F6F6!important;
    }
    
    .fc-col-header-cell-cushion {
        color: #A4A4A4!important;
    }
    .fc-daygrid-day {
        border: 1px solid #E0E0E0!important;
    }
</style>
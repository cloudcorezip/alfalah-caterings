<div class="container-fluid">
    {{---<div class="row">
        <div class="col-md-12">
            <h3 class="font-weight-bold">{{$data->getProduct->name}}</h3>
            <h6>{{\Carbon\Carbon::parse($data->reservation_date)->isoFormat('DD MMMM Y HH:mm:ss')}}</h6>
            <hr>
        </div>
    </div>---}}
    <div class="row d-flex align-items-center mb-3">
        {{--- <div class="col-md-auto">
            <img src="{{asset('public/backend/img/no-user.png')}}" alt="" class="img-fluid" width="60px;" height="60px;" style="border-radius:50%;object-fit:contain;">
        </div> ---}}
        <div class="col-md-auto">
            <h6 class="d-block">Pasien</h6>
            <h4 class="font-weight-bold">{{$data->getCustomer->name}}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <table class="table-note" style="margin: 0 auto;width:100%;">
                <tr>
                    <td class="font-weight-bold font-14 color-323232">No. Member</td>
                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                    <td class="font-14 color-a4a4a4">{{$data->getCustomer->code}}</td>
                </tr>
                <tr>
                    <td class="font-14 font-weight-bold color-323232">No. Reservasi</td>
                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                    <td class="font-14 color-a4a4a4">{{$data->reservation_number}}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold font-14 color-323232">Nama</td>
                    <td class="text-center font-weight-bold font-14" width="40px">:</td>
                    <td class="font-14 color-a4a4a4">{{$data->getCustomer->name}}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold font-14 color-323232">Tanggal Lahir</td>
                    <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                    <td class="font-14 color-a4a4a4">{{$data->getCustomer->place_of_birth}}, {{\Carbon\Carbon::parse($data->getCustomer->date_of_birth)->isoFormat('D MMMM Y')}}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold font-14 color-323232">Jenis Kelamin</td>
                    <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                    @if($data->getCustomer->gender == 'male')
                    <td class="font-14 color-a4a4a4">Laki - Laki</td>
                    @elseif($data->getCustomer->gender == 'female')
                    <td class="font-14 color-a4a4a4">Perempuan</td>
                    @else
                    <td class="font-14 color-a4a4a4">-</td>
                    @endif
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table-note" style="margin:0 auto;width:100%;">
                <tr>
                    <td class="font-weight-bold font-14 color-323232">Tanggal Reservasi</td>
                    <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                    <td class="font-14 color-a4a4a4">{{\Carbon\Carbon::parse($data->reservation_date)->isoFormat(' DD MMMM Y HH:mm:ss')}}</td>
                </tr>
                <tr>
                    <td class="font-14 font-weight-bold color-323232">Dokter</td>
                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                    <td class="font-14 color-a4a4a4">{{$data->getStaff->getUser->fullname}}</td>
                </tr>
                <tr>
                    <td class="font-weight-bold font-14 color-323232">Status</td>
                    <td class="text-center font-weight-bold font-14 color-323232" width="40px">:</td>
                    @if($data->status == 0)
                    <td class="font-14 color-a4a4a4">
                        <span class="status-0 font-12">Belum Berkunjung</span>
                    </td>
                    @elseif($data->status == 1)
                    <td class="font-14 color-a4a4a4">
                        <span class="status-1 font-12">Disetujui</span>
                    </td>
                    @elseif($data->status == 2)
                    <td class="font-14 color-a4a4a4">
                        <span class="status-1 font-12">Sudah Berkunjung</span>
                    </td>
                    @endif
                </tr>
                <tr>
                    <td class="font-14 font-weight-bold color-323232">Layanan</td>
                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                    <td class="font-14 color-a4a4a4">{{$data->getProduct->name}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h3 class="font-weight-bold">{{$data->getProduct->name}}</h3>
                                        <span class="span-text">{{\Carbon\Carbon::parse($data->reservation_date)->isoFormat('DD MMMM Y HH:mm:ss')}}</span>
                                    </div>`);
    });
</script>
@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')

    @include('merchant::toko.appointment.style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5/main.min.css">
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Klinik</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.appointment.index')}}">Appointment</a>
                        </li>
                    </ol>
                </nav>
                <span>Disini kamu dapat melihat jadwal pasien dan tindakan mu.</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a onclick="activeMenu('klinik','klinik-appointment')" class="nav-link active" href="{{route('merchant.toko.appointment.index')}}">Appointment</a>
            </li>
            <li class="nav-item">
                <a onclick="activeMenu('klinik','klinik-appointment')" class="nav-link" href="{{route('merchant.toko.reservation.index')}}">Reservasi</a>
            </li>
        </ul>
        <div class="card shadow mb-4 rounded py-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="calendar"></div>
                        <a onclick="loadModalFullScreen(this)" id="open-detail"></a>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5/main.min.js"></script>

    <script>
        let calendar;
        let startDate = "{{$startDate}}"
        let endDate = "{{$endDate}}"

        function getData(startDate, endDate)
        {
            let tmp;
            $.ajax({
                url:"{{route('merchant.toko.appointment.get-data')}}",
                type: "POST",
                async: false,
                data:{
                    _token: "{{csrf_token()}}",
                    startDate: startDate,
                    endDate:endDate
                },
                success: function(response){
                    tmp = response;
                }
            });

            return tmp;
        }

        document.addEventListener('DOMContentLoaded', function() {
            let ev = [];
            let data = getData(startDate, endDate);

            if(data){
                data.forEach(item => {
                    ev.push({
                        title: item.customer_name,
                        start: item.reservation_date,
                        end:item.reservation_date,
                        classNames:['card-appointment', (item.status == 0)?'bt-status-0':'bt-status-1'],
                        url:'{{route("merchant.toko.appointment.detail")}}?id='+item.id 
                    })
                })
            }

            var calendarEl = document.getElementById('calendar');
            calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth',
                events:ev,
                locale:'id',
                headerToolbar: {
                    right: 'today prev,next',
                    center: 'title',
                    left: 'listWeek,timeGridWeek,dayGridMonth'
                },
                eventClick: function(info) {
                    info.jsEvent.preventDefault();
                    let url = info.event.url;

                    $("#open-detail").attr("target", url);
                    $("#open-detail").click();
                }
            });

            calendar.render();               
        });

        $(document).ready(function(){
            $("button[title='list view']").click();
        })

    </script>
@endsection

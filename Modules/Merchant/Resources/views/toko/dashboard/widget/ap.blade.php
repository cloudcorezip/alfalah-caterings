<!-- hutang -->
<div class="col-md-6" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header d-flex align-items-center justify-content-between">
            <div>
                <h4 class="font-weight-bold">Utang</h4>
                <h6 style="color:#909090;">Jangan lupa bayar utang usahamu ya</h6>
            </div>
            <div>
                <select class="form-control" name="ap-period" id="ap-period" onchange="loadAp()">
                </select>
            </div>
        </div>
        <div class="card-body">
            <div id="ap-loader" class="col-md-12 justify-content-center">
                <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
            </div>
            <div id="ap-result" style="display:none;">
            </div>
            <div id="ap-nodata" class="text-center" style="display:none;">
                <img class="img-fluid text-center" src="{{asset('public/backend/img/widget-ap-nodata.png')}}">
                <h5 class="font-weight-bold">Yeay! Usahamu bebas utang nih</h5>
                <span>Catat utang usahamu jika ada</span>
            </div>
        </div>
    </div>
</div>
<!-- end hutang -->
<!-- stock hampir abis -->
<div class="col-md-12" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 border-right mb-3">
                    <div class="mb-3">
                        <h4 class="font-weight-bold">Stok Hampir Habis</h4>
                        <span>Daftar stok produk yang menipis disetiap cabang</span>
                        <hr>
                    </div>
                    <div class="p-3">
                        <div class="form-konten position-relative" style="padding: 70px;background:#4B4B4B;border-radius:10px;">
                            <img
                                src="{{asset('public/backend/img/bg-search-stock.png')}}"
                                alt="bg"
                                style="position:absolute;top: 0; right:0;"
                                class="img-fluid"
                            >
                            <div class="form-group">
                                <h4 class="text-white">Cari  Produk</h4>
                                <div class="position-relative">
                                    <input id="search-stock" type="text" class="form-control" style="border-radius:4px;" onkeyup="loadStock()">
                                    <span
                                        style="right: 10px; top:50%; transform:translateY(-50%);color:#333"
                                        class="iconify position-absolute"
                                        data-icon="akar-icons:search"
                                        data-width="18"
                                        data-height="18"
                                    >
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="mb-5 d-flex align-items-center justify-content-between">
                        <h4 class="font-weight-bold mb-0">List Produk</h4>
                        <a style="color:#FF470D; border-bottom:1px solid #FF470D;" href="{{route('merchant.toko.stock.hpp.data')}}" target="_blank">Lihat Semua</a>
                    </div>

                    <div id="stock-result">

                    </div>
                    <div id="stock-loader" class="col-md-12 justify-content-center align-items-center">
                        <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
                    </div>
                    <div id="stock-nodata" class="text-center" style="display:none;">
                        <img class="img-fluid text-center" src="{{asset('public/backend/img/empty-bs-product.png')}}">
                        <h5 class="font-weight-bold">Tidak ada stock hampir habis</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end stock hampir habis -->
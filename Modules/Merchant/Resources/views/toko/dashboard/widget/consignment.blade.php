<!-- produk konsinyasi -->
<div class="col-md-6" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header">
            <h4 class="font-weight-bold">Produk Konsinyasi (Titipan)</h4>
        </div>
        <div class="card-body">
            <div class="mb-3 form-konten position-relative">
                <input type="text" id="search-consignment" class="form-control" placeholder="Cari Produk" style="border-radius:13px!important;" onkeyup="loadConsignment()">
                <span
                    style="right: 10px; top:50%; transform:translateY(-50%);color:#333"
                    class="iconify position-absolute"
                    data-icon="akar-icons:search"
                    data-width="23"
                    data-height="23"
                >
                </span>
            </div>
            <div id="product-consignment-loader" class="col-md-12 justify-content-center">
                <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
            </div>
            <div id="consignment-result">
                <div id="product-consignment-result">

                </div>
                <a style="background:#F95E17!important;" onclick="activeMenu('persediaan','persediaan-penerimaan-konsinyasi')" target="_blank" href="{{route('merchant.toko.consignment.data')}}" class="d-flex align-items-center justify-content-between btn-bg__orange btn-block py-2 px-4 text-white rounded">
                    <span>Lihat semua</span>
                    <span class="iconify" data-icon="akar-icons:chevron-right"></span>
                </a>
            </div>
            <div id="product-consignment-nodata" class="text-center" style="display:none;">
                <img class="img-fluid text-center" src="{{asset('public/backend/img/empty-bs-product.png')}}">
                <h5 class="font-weight-bold">Tidak ada produk konsinyasi</h5>
                <span>Segera tambah produk konsinyasimu di menu persediaan</span>
            </div>
        </div>
    </div>
</div>
<!-- end produk konsinyasi -->
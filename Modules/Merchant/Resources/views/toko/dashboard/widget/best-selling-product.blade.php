<!-- produk terlaris -->
<div class="col-md-4" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header d-flex align-items-center justify-content-between">
            <div>
                <h4 class="font-weight-bold">Produk Terlaris</h4>
                <h6 style="color:#909090;">Berdasarkan total harga penjualan</h6>
            </div>
            <div>
                <select class="form-control" name="" id="bestseller-period" onchange="loadBestSellingProduct()">
                </select>
            </div>
        </div>
        <div class="card-body">
            <div id="bestseller-loader" class="col-md-12 justify-content-center">
                <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
            </div>
            <div id="bestseller" style="display:none;">
                <ul class="mb-3" id="best-selling-result">
                </ul>
                <a style="background:#F95E17!important;" onclick="activeMenu('laporan','laporan-all')" href="{{route('merchant.report.sale-order.product')}}" class="d-flex align-items-center justify-content-between btn-bg__orange btn-block py-2 px-4 text-white rounded">
                    <span>Lihat produk terlaris lainnya</span>
                    <span class="iconify" data-icon="akar-icons:chevron-right"></span>
                </a>
            </div>
            <div id="bestseller-nodata" class="text-center" style="display:none;">
                <img class="img-fluid text-center" src="{{asset('public/backend/img/empty-bs-product.png')}}">
                <h5 class="font-weight-bold">Belum ada produk terlaris</h5>
                <span>Segera tambah penjualanmu</span>
            </div>
        </div>
    </div>
</div>
<!-- end produk terlaris  -->
<!-- stok produk habis pakai -->
<div class="col-md-6" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header">
            <h4 class="font-weight-bold">Stok Produk Habis Pakai</h4>
        </div>
        <div class="card-body">
            <div class="mb-3 form-konten position-relative">
                <input
                     type="text" 
                     class="form-control" 
                     placeholder="Cari Produk" 
                     style="border-radius:13px!important;"
                     id="consumable-product-stock-search"
                     onkeyup="loadConsumableProductStock()"
                    >
                <span
                    style="right: 10px; top:50%; transform:translateY(-50%);color:#333"
                    class="iconify position-absolute"
                    data-icon="akar-icons:search"
                    data-width="23"
                    data-height="23"
                >
                </span>
            </div>

            <div id="consumable-product-stock-loader" class="col-md-12 justify-content-center">
                <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
            </div>
            <div id="consumable-product-stock-result">
            </div>

            <div id="consumable-product-stock-nodata" class="text-center" style="display:none;">
                <img class="img-fluid text-center" src="{{asset('public/backend/img/empty-bs-product.png')}}">
                <h5 class="font-weight-bold">Tidak ada produk habis pakai</h5>
                <span>Segera tambah produk habis pakaimu jika ada</span>
            </div>
        </div>
    </div>
</div>
<!-- end stok produk habis pakai -->
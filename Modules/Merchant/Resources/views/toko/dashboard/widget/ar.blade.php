<!-- piutang -->
<div class="col-md-6" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header d-flex align-items-center justify-content-between">
            <div>
                <h4 class="font-weight-bold">Piutang</h4>
                <h6 style="color:#909090;">Jangan lupa nagih utang pelangganmu ya</h6>
            </div>
            <div>
                <select class="form-control" name="ar-period" id="ar-period" onchange="loadAr()">

                </select>
            </div>
        </div>
        <div class="card-body">
            <div id="ar-loader" class="col-md-12 justify-content-center">
                <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
            </div>
            <div id="ar-result" style="display:none;">
            </div>
            <div id="ar-nodata" class="text-center" style="display:none;">
                <img class="img-fluid text-center" src="{{asset('public/backend/img/widget-ar-nodata.png')}}">
                <h5 class="font-weight-bold">Tidak ada piutang</h5>
                <span>Catat utang pelangganmu jika ada</span>
            </div>
        </div>
    </div>
</div>
<!-- end piutang -->
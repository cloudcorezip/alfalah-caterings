<!-- omset -->
<div class="col-md-4" id="{{$element_id}}">
    <div class="card p-0">
        <div class="card-header p-4 mb-3 d-flex align-items-center justify-content-between">
            <div>
                <h4 class="font-weight-bold">Omzet</h4>
                <h6 class="mb-0" style="color:#909090;">Omzetmu naik atau turun nih?</h6>
            </div>
            <div>
                <select class="form-control" name="omzet-period" id="omzet-period" onchange="loadOmzet()">
                </select>
            </div>
        </div>
        <div class="card-body p-0">
            <div id="omzet-loader" class="col-md-12 justify-content-center">
                <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
            </div>
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="position:relative;">
                <div class="carousel-inner" id="omzet-result">
                </div>
                <a style="width:fit-content;left:20px!important;" class="carousel-control-prev d-block" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="iconify" data-icon="ooui:arrow-next-rtl"></span>
                </a>
                <a style="width:fit-content;right:20px;!important" class="carousel-control-next d-block" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="iconify" data-icon="ooui:arrow-next-ltr"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- end omset -->
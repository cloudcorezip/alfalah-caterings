<!-- distribusi pengeluaran -->
<div class="col-md-6" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header d-flex align-items-center justify-content-between">
            <div>
                <h4 class="font-weight-bold">Distribusi Pengeluaran</h4>
                <h6 style="color:#909090;">Berikut detail pengeluaran setiap cabangmu ya</h6>
            </div>
            <div>
                <select class="form-control" name="cost-period" id="cost-period" onchange="loadCost()">

                </select>
            </div>
        </div>
        <div class="card-body">
            <div id="cost-carousel" class="carousel slide mb-4" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                    <div class="row">
                        <div class="col-md-12" id="cost-exsist-data">
                            <a style="left:0!important;" class="carousel-control-prev d-block" href="#cost-carousel" role="button" data-slide="prev">
                                <span class="iconify" data-icon="ooui:arrow-next-rtl"></span>
                            </a>
                            @foreach(get_cabang() as $key => $item)
                            <div class="carousel-item carousel-cost {{($key == 0)? 'active':''}}" data-id="{{$item->id}}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 style="color:#323232;" class="mb-3 text-center font-weight-bold">{{$item->nama_cabang}}</h3>
                                    </div>
                                </div>
                                <div class="row cost-result">
                                    <div class="col-md-6" id="cost-chart-wrapper_{{$item->id}}">
                                        <canvas id="cost-chart_{{$item->id}}" width="100%" height="100%"></canvas>
                                    </div>
                                    <div class="col-md-6" id="cost-data_{{$item->id}}">
                                        
                                    </div>
                                </div>
                                <div class="row cost-nodata" style="display:none;">
                                    <div class="col-md-6">
                                        <div 
                                            class="mx-auto d-flex align-items-center justify-content-center" 
                                            style="width:180px; height:180px; background:#D1D1D1;border-radius:50%;margin-top:50px;"
                                        >
                                            <div style="width:60%;height:60%;background:#fff;border-radius:50%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <img class="img-fluid text-center" src="{{asset('public/backend/img/widget-cost-nodata.png')}}">
                                            <h5 class="font-weight-bold">Pengeluaran belum ditemukan</h5>
                                            <span style="color:rgba(0, 0, 0, 0.5);">Cek kembali filter analisis pengeluaran</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <a style="right:0;!important;" class="carousel-control-next d-block" href="#cost-carousel" role="button" data-slide="next">
                                <span class="iconify" data-icon="ooui:arrow-next-ltr"></span>
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div id="cost-loader" class="justify-content-center">
                <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
            </div>
        </div>
    </div>
</div>
<!-- end distribusi pengeluaran -->
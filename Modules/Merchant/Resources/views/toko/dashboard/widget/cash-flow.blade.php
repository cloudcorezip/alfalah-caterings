<!-- arus kas -->
<div class="col-md-8" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header d-flex align-items-center justify-content-between">
            <div>
                <h4 class="font-weight-bold">Arus Kas</h4>
                <h6 style="color:#909090;">Berikut perbandingan omset di setiap cabang usahamu</h6>
            </div>
            <div>
                <select class="form-control" name="cash-flow-period" id="cash-flow-period" onchange="loadCashFlow()">
                    <option value="7-month-lastthreemonth" selected>3 Bulan Terakhir</option>
                    <option value="12-month-lastfourmonth">4 Bulan Terakhir</option>
                    <option value="8-month-lastsixmonth">6 Bulan Terakhir</option>
                    <option value="13-month-lasteightmonth">8 Bulan Terakhir</option>
                    <option value="9-month-lastninemonth">9 Bulan Terakhir</option>
                    <option value="10-month-thisyear">Tahun Ini</option>
                    <option value="11-month-lastyear">Tahun Lalu</option>
                </select>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 mb-4 d-flex">
                    <div class="d-flex" id="cash-flow-label">
                        <div class="label-with-close mr-3 position-relative" id="cf_{{merchant_id()}}">
                            <input
                                onchange="changeColorLabel(this, '#span_color_cf_{{merchant_id()}}', 'cf_')"
                                class="color-picker cf_"
                                type="color"
                                value="#FB651D"
                                id="input_color_cf_{{merchant_id()}}"
                                data-id="{{merchant_id()}}"
                                data-name="{{merchant_detail()->name}}"
                            >
                            <span
                                class="circle-dotted mr-3"
                                style="background-color: #FB651D;cursor:pointer;position:relative;"
                                onclick="openPallete('#input_color_cf_{{merchant_id()}}')"
                                id="span_color_cf_{{merchant_id()}}"
                            >
                            </span>
                            <span class="mr-3">{{merchant_detail()->name}}</span>
                            <button type="button">
                                <span class="iconify" data-icon="codicon:chrome-close"></span>
                            </button>
                        </div>
                    </div>
                    <div class="select-branch-wrapper">
                        <select class="form-control select-branch" onchange="addBranch(this,'#cash-flow-label', 'cf_')">
                            <option value="-1">+ Tambah Cabang</option>
                            @foreach(get_cabang() as $key => $item)
                            <option value="{{$item->id}}">{{$item->nama_cabang}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="chart-area loading-bg" id="output-cash-flow">
                    </div>
                    <div id="cash-flow-loader" class="col-md-12 justify-content-center">
                        <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end arus kas -->
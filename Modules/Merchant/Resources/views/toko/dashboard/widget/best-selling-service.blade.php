<!-- jasa terlaris -->
<div class="col-md-4" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header d-flex align-items-center justify-content-between">
            <div>
                <h4 class="font-weight-bold">Jasa Terlaris</h4>
                <h6 style="color:#909090;">Berdasarkan total harga penjualan</h6>
            </div>
            <div>
                <select class="form-control" name="" id="best-selling-service-period" onchange="loadBestSellingService()">
                </select>
            </div>
        </div>
        <div class="card-body">
            <div id="best-selling-service-loader" class="col-md-12 justify-content-center">
                <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
            </div>
            <div id="best-selling-service" style="display:none;">
                <ul class="mb-3" id="best-selling-service-result">
                </ul>
                <a style="background:#F95E17!important;" onclick="activeMenu('penjualan','penjualan-data-penjualan')" href="{{route('merchant.toko.sale-order.data')}}" class="d-flex align-items-center justify-content-between btn-bg__orange btn-block py-2 px-4 text-white rounded">
                    <span>Lihat jasa terlaris lainnya</span>
                    <span class="iconify" data-icon="akar-icons:chevron-right"></span>
                </a>
            </div>
            <div id="best-selling-service-nodata" class="text-center" style="display:none;">
                <img class="img-fluid text-center" src="{{asset('public/backend/img/empty-bs-service.png')}}">
                <h5 class="font-weight-bold">Belum ada jasa terlaris</h5>
                <span>Segera tambah penjualan jasamu</span>
            </div>
        </div>
    </div>
</div>
<!-- end jasa terlaris  -->
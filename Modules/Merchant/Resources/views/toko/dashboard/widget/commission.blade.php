<!-- komisi -->
<div class="col-md-8" id="{{$element_id}}">
    <div class="card card-summary p-3">
        <div class="card-header d-flex align-items-center justify-content-between">
            <div>
                <h4 class="font-weight-bold">Komisi Karyawan</h4>
                <h6 style="color:#909090;">Lihat performa komisi karyawanmu disini</h6>
            </div>
            <div>
                <select class="form-control" name="commission-period" id="commission-period" onchange="loadCommission()">

                </select>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 mb-4 d-flex">
                    <div class="d-flex" id="ec-label">
                        <div class="label-with-close mr-3" id="ec_{{merchant_id()}}">
                            <input
                                onchange="changeColorLabel(this, '#span_color_ec_{{merchant_id()}}', 'ec_')"
                                class="color-picker ec_"
                                type="color"
                                value="#FB651D"
                                id="input_color_ec_{{merchant_id()}}"
                                data-id="{{merchant_id()}}"
                            >
                            <span
                                class="circle-dotted mr-3"
                                style="background-color: #FB651D;cursor:pointer;position:relative;"
                                onclick="openPallete('#input_color_ec_{{merchant_id()}}')"
                                id="span_color_ec_{{merchant_id()}}"
                            >
                            </span>
                            <span class="mr-3">{{merchant_detail()->name}}</span>
                            <button type="button">
                                <span class="iconify" data-icon="codicon:chrome-close"></span>
                            </button>
                        </div>
                    </div>
                    <div class="select-branch-wrapper">
                        <select class="form-control select-branch" onchange="addBranch(this,'#ec-label', 'ec_')">
                            <option value="-1">+ Tambah Cabang</option>
                            @foreach(get_cabang() as $key => $item)
                            <option value="{{$item->id}}">{{$item->nama_cabang}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="chart-area loading-bg" id="output-commission"></div>
                    <div id="commission-loader" class="col-md-12 justify-content-center">
                        <img src="{{asset('public/backend/img')}}/loading-horizontal.gif" style="display: block;margin-left: auto;margin-right: auto;margin-top: 100px ;margin-bottom: 100px" />
                    </div>
                    <div id="commission-nodata" class="text-center" style="display:none;">
                        <img class="img-fluid text-center" src="{{asset('public/backend/img/empty-bs-product.png')}}">
                        <h5 class="font-weight-bold">Belum ada data komisi karyawan</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end komisi -->
@extends('backend-v3.layout.main')
@section('css')
    <style>
        .dashboard-head {
            position: relative;
            width: 100%;
            background: #FDF5EE;
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            border-radius: 10px;
            margin-top: 4%;
        }

        .dashboard-head-ilust {
            position: absolute;
            object-fit: contain;
            right: 0;
            bottom: 0;
            transform: translateX(10px);
        }

        .select2-container--default .select2-selection--single {
            background-color:#F3F3F3!important;
            border:1px solid #F3F3F3!important;
            color:#323232;
            border-radius: 11px!important;
        }
        .select2-container--open .select2-dropdown--below {
        }
        .select2-selection__arrow {
            display:none!important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: normal!important;
            transform: translateY(50%)!important;
        }

        .link-dashboard:hover {
            box-shadow: 0 4px 8px 0 #fe8e2d75, 0 6px 20px 0 rgba(255, 217, 1, 0.253);
        }
        .card-summary {
            box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.05);
            background-color: #FFFFFF;
        }
        .label-with-close {
            display: flex;
            align-items: center;
            justify-content: space-between;
            background-color:#F3F3F3;
            color:#323232;
            border: 1px solid #F3F3F3;
            width: fit-content;
            border-radius: 11px;
            padding: 5px 13px;
        }
        .label-with-close > button {
            background-color:transparent;
            border: none!important;
            cursor: pointer;
            padding: 0!important;

        }
        .label-with-close > .circle-dotted {
            width: 15px;
            height:15px;
            border-radius: 50%;
            border:none!important;
        }

        .btn-border-only {
            border: 1px solid rgba(50, 50, 50, 0.6);
            color:rgba(50, 50, 50, 0.8)!important;
            border-radius: 8px;
        }

        .side-popup {
            color: #323232!important;
            width: 420px!important;
        }
        .selected-widget {
            display: flex;
            align-items:center;
            border: 1px solid #DADCE0;
            border-radius: 6px;
            cursor: grab;
            background-color: #fff;
        }

        .selected-widget .remove-selected-widget {
            cursor: pointer;
        }

        .widget-list {
            background: #fff;
            border: 1px solid #D9D9D9;
            border-radius: 6px;
        }
        .widget-list-body {
            background: #F8F9FA;
            width: 100%;
            height: 130px;
        }

        .list-with-border {
            border: 1px solid #D9D9D9;
            border-radius: 6px;
            padding: 10px;
            width: 100%;
        }

        .bdge-danger {
            background-color:#F25858;
            color: #fff;
            border-radius: 6px;
        }

        .progress-percent-wrapper {
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 100%;
        }
        .progress-percent-outer {
            width: 85%;
            height: 19px;
            border: 2px solid #D9D9D9;
            border-radius: 8px;
        }

        .progress-percent-inner {
            width: 0%;
            height: 15px;
            border: none;
            border-radius: 8px;
            background-color:#72BA6C;
            transition: width 1s ease;
        }
        .btn-transparent {
            background-color: transparent;
            border: 1px solid transparent;
        }
        .btn-transparent:focus {
            outline: none;
        }

        .color-picker {
            position:absolute;
            opacity: 0;
        }
        .card-circle {
            background-color: #ffffff;
            width: 60px;
            height: 60px;
            margin-right: 20px;
        }

        .card-circle .percent {
            position: relative;
        }

        .card-circle svg {
            position: relative;
            width: 70px;
            height: 70px;
            transform: rotate(-90deg);
        }

        .card-circle svg circle {
            width: 100%;
            height: 100%;
            fill: none;
            stroke: #f0f0f0;
            stroke-width: 10;
            stroke-linecap: round;
        }

        .card-circle svg circle:last-of-type {
            stroke-dasharray: 625px;
            stroke-dashoffset: calc(625px - (120px * var(--percent)) / 100);
        }

        .card-circle .number {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .card-circle .number h3 {
            font-weight: 200;
            font-size: 3.5rem;
        }

        .card-circle .number h3 span {
            font-size: 2rem;
        }

        .card-circle .title h2 {
            margin: 25px 0 0;
        }

        .card-circle:nth-child(1) svg circle:last-of-type {
            stroke:var(--color-circle);
        }

    </style>
@endsection
@section('title',$title)
@section('content')

    <div class="container-fluid">
        @include('merchant::alert')
    </div>
    <div id="main-block-content" class="container-fluid">
        <div class="row mb-3">
            <div class="col-md-12 text-right">
                <button type="button" class="btn btn-border-only" onclick="showFilter('btn-show-filter', 'side-popup')">
                    <span class="iconify mr-3" data-icon="bxs:edit"></span>
                    <span>Edit Widget</span>
                </button>
            </div>
        </div>

        <div class="row" id="container-selected-widget">
            @foreach($selectedWidget as $key => $item)
                @include('merchant::toko.dashboard.widget.'.$item->file, ['element_id'=>$item->element_id])
            @endforeach
        </div>
    </div>

    <!-- pop up edit widget -->
    <div class="side-popup" id="side-popup">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <span class="text-white form-filter3-text-header">Edit Widget</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter', 'side-popup')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div id="side-popup-body">
            <div class="row px-4 pt-4 mb-3">
                <div class="col-md-12">
                    <span>TERPILIH {{count($selectedWidget)}} DARI {{$countedWidget}}</span>
                </div>
            </div>
            <!-- selected widget -->
            <div id="widget-sortable" class="row px-4">
                @foreach($selectedWidget as $key => $item)
                <div class="col-md-12 mb-3 sortable-item" id="sort-{{$item->element_id}}" data-value='{{json_encode($item)}}'>
                    <div class="selected-widget px-2 py-2">
                        <span class="iconify mr-3" data-icon="akar-icons:drag-vertical"></span>
                        <span>{{$item->name}}</span>
                        <span class="iconify remove-selected-widget ml-auto" data-icon="codicon:chrome-close" onclick="removeWidget('{{$item->element_id}}')"></span>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- end selected widget -->

            <div class="row px-4">
                <div class="col-md-12 mb-3">
                    <hr>
                </div>
            </div>

            <div class="row px-4 mb-3 d-flex align-items-center">
                <div class="col-md-6">
                    <span>DAFTAR WIDGET ({{count($widgetList)}})</span>
                </div>
            </div>

            <!-- widget list -->
            <div class="row px-4">
                @foreach($widgetList as $key => $item)
                <div class="col-md-12 mb-3">
                    <div class="widget-list py-3">
                        <div class="widget-list-header px-2 mb-3">
                            <div class="form-check form-check-inline">
                                <input
                                    class="form-check-input widget-checkbox"
                                    type="checkbox"
                                    value="{{$item->element_id}}"
                                    data-name="{{$item->name}}"
                                    onclick="checkWidget(this, '{{json_encode($item)}}')"
                                    @if($selectedWidget->pluck('element_id')->contains($item->element_id))
                                    checked
                                    @endif
                                >
                                <label class="form-check-label">{{$item->name}}</label>
                            </div>
                        </div>
                        <div class="widget-list-body mb-3">

                        </div>
                        <div class="widget-list-footer px-2  text-right">
                            <span>8 kolom x 4 baris</span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- end widget list -->

            <div class="row px-4">
                <div class="col-md-12 mb-3">
                    <hr>
                </div>
            </div>

            <div class="row px-4 mb-4">
                <div class="col-md-12">
                    <button
                        style="background:#F95E17!important;"
                        class="btn d-flex align-items-center justify-content-between btn-bg__orange btn-block py-2 px-4 text-white rounded"
                        onclick="showFilter('btn-show-filter', 'side-popup')"
                    >
                        <span class="mr-2">Tutup</span>
                        <span class="iconify" data-icon="akar-icons:chevron-right"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end pop up edit widget -->
@endsection
@section('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>
    <script>

        $(document).ready(function(){
            getRange()
            $(".select-branch").select2();
            $(".progress-percent-inner:first").css('width', '50%');
            $(".progress-percent-inner").css('width', '40%');
            $(".progress-percent-inner:last").css('width', '80%');
        });

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        function invertColor(hex) {
            if (hex.indexOf('#') === 0) {
                hex = hex.slice(1);
            }
            if (hex.length === 3) {
                hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
            }
            if (hex.length !== 6) {
                throw new Error('Invalid HEX color.');
            }
            var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
                g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
                b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
            return '#' + padZero(r) + padZero(g) + padZero(b);
        }

        function padZero(str, len) {
            len = len || 2;
            var zeros = new Array(len).join('0');
            return (zeros + str).slice(-len);
        }


        function getRange()
        {

            $.ajax({
                type: "GET",
                url: "{{route('merchant.ajax.dashboard.range')}}",
                dataType: 'json',
                success: function(result){
                   let apAr=[], costOmzet=[]
                    result.forEach(element=>{
                        apAr.push(element)

                        if(element.id=='6-month-lastmonth'
                            || element.id=='2-day-yesterday'
                            || element.id=='4-day-lastweek'
                            || element.id=='11-month-lastyear' ){
                        }else{
                            costOmzet.push(element)

                        }
                    })
                    let apArOption = ''
                    apAr.forEach(opt =>{
                        if(opt.id=='5-month-thismonth'){
                            apArOption+=`<option value="${opt.id}" selected>${opt.text}</option>`;
                        }else{
                            apArOption+=`<option value="${opt.id}">${opt.text}</option>`;
                        }
                    })
                    let costOption = ''
                    costOmzet.forEach(optCost =>{
                        if(optCost.id=='5-month-thismonth'){
                            costOption+=`<option value="${optCost.id}" selected>${optCost.text}</option>`;
                        }else{
                            costOption+=`<option value="${optCost.id}">${optCost.text}</option>`;
                        }
                    })

                    $("#ar-period").append(apArOption)
                    $("#ap-period").append(apArOption)
                    $("#bestseller-period").append(apArOption)
                    $("#cost-period").append(costOption)
                    $("#omzet-period").append(costOption)
                    $("#commission-period").append(costOption)
                    $("#best-selling-service-period").append(apArOption)
                }});

        }

        function generateRandomColor()
        {
            let randomColor = Math.floor(Math.random()*16777215).toString(16);
            return '#'+randomColor;
        }

        function addBranch(e, target, prefix)
        {
            let val = $(e).val();
            let text = $(e).find('option:selected').text().trim();
            if(val == -1)
            {
                return;
            }
            let check = [...document.querySelectorAll(`#${prefix}${val}`)];
            if(check.length < 1){
                let html = `<div class="label-with-close mr-3 position:relative;" id="${prefix}${val}">
                                <input
                                    onchange="changeColorLabel(this, '#span_color_${prefix}${val}', '${prefix}')"
                                    class="color-picker ${prefix}"
                                    type="color"
                                    value="#FB651D"
                                    id="input_color_${prefix}${val}"
                                    data-id="${val}"
                                    data-name="${text}"

                                >
                                <span
                                    class="circle-dotted mr-3"
                                    style="background-color: #FB651D;cursor:pointer;position:relative;"
                                    onclick="openPallete('#input_color_${prefix}${val}')"
                                    id="span_color_${prefix}${val}"
                                ></span>
                                <span class="mr-3">${text}</span>
                                <button type="button" onclick="removeBranch('#${prefix}${val}', '${prefix}')">
                                    <span class="iconify" data-icon="codicon:chrome-close"></span>
                                </button>
                            </div>`;

                $(target).append(html);
            }
            setTimeout(() => {
                $(e).val('-1').change();
            }, 500);

            if(prefix == 'cf_'){
                loadCashFlow();
            }

            if(prefix == 'ec_')
            {
                loadCommission();
            }
            if(prefix == 'pl_')
            {
                loadProfitLost();
            }
        }

        function openPallete(selector)
        {
            $(selector).click();
        }

        function changeColorLabel(e, selector, prefix)
        {
            let val = $(e).val();
            $(selector).css('background-color', val);

            if(prefix == "cf_"){
                loadCashFlow();
            }

            if(prefix == "ec_")
            {
                loadCommission();
            }
            if(prefix == "pl_"){
                loadProfitLost()
            }
        }

        function removeBranch(target, prefix)
        {
            $(target).remove();

            if(prefix == "cf_")
            {
                loadCashFlow();
            }

            if(prefix == "pl_")
            {
                loadProfitLost();
            }
        }

        function loadCashFlow()
        {
            $("#cash-flow-loader").show();
            $("#output-cash-flow").hide();
            let period = $("#cash-flow-period").val();

            let branchElement = [...document.querySelectorAll(".color-picker.cf_")];
            let branch = [...branchElement].map(item => {
                return {
                    id: $(item).attr('data-id'),
                    name: $(item).attr('data-name'),
                    color: $("#input_color_cf_"+$(item).attr('data-id')).val()
                }
            });
            let merchantId = [...branch].map(item => {
                return item.id;
            }).toString();

            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.cashflow')}}",
                data: {
                    period:period,
                    md_merchant_id: merchantId,
                    _token: "{{csrf_token()}}"
                },
                success:function(response){
                    var data = response;
                    var categories = []
                    var  series = []
                    data.listMonth.forEach(item => {
                        categories.push(item);
                    });

                    branch.forEach(b=>{
                        let valueData = []

                        data.group.forEach(item => {
                            item.data.forEach(val => {
                                if(val.merchant_id==b.id){
                                    valueData.push({
                                        merchant_id:val.merchant_id,
                                        in:parseFloat(val.in),
                                        out:parseFloat(val.out),
                                        netto:parseFloat(val.netto)
                                    })

                                }
                            });
                        });

                        let inCashFlow = []
                        let outCashFlow = []
                        let nettoCashFlow = []

                        valueData.forEach(i=>{
                            if(i.merchant_id==b.id){
                                inCashFlow.push(i.in)
                                outCashFlow.push(i.out)
                                nettoCashFlow.push(i.netto)
                            }
                        })
                        series.push({
                            name: b.name,
                            id:b.id,
                            data: inCashFlow,
                            stack: b.id,
                            color: b.color,
                            alias:'in',
                        })
                        series.push({
                            name: b.name,
                            data:outCashFlow,
                            stack: b.id,
                            linkedTo:b.id,
                            color: invertColor(b.color),
                            alias:'out'
                        })
                        series.push({
                            name: b.name,
                            data:nettoCashFlow,
                            stack: b.id,
                            type: 'line',
                            color: b.color,
                            alias:'netto'
                        });

                    })

                    $('#output-cash-flow').removeClass('.loading-bg').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        xAxis: {
                            categories:categories
                        },
                        yAxis: {
                            title: {
                                text: 'Jumlah Dalam Nominal'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var alias = this.series.userOptions.alias;
                                var name = this.series.userOptions.name;
                                if(alias=='netto'){
                                    return '<b>' + name + '</b><br/>' +
                                        'Kas Bersih' + ': ' + currencyFormat(this.y,'') + '<br/>';
                                }else if(alias=='in'){
                                    return '<b>' + name + '</b><br/>' +
                                        'Kas Masuk' + ': ' + currencyFormat(this.y,'') + '<br/>';

                                }else{
                                    return '<b>' + name + '</b><br/>' +
                                        'Kas Keluar' + ': ' + currencyFormat(this.y,'') + '<br/>';

                                }
                            }
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal'
                            },

                            series: {
                                marker: {
                                    fillColor: '#FFFFFF',
                                    lineWidth: 2,
                                    lineColor: '#FEC208'
                                },
                                borderRadius:'5px',
                                pointWidth: 15
                            }
                        },

                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                        },
                        series: series
                    });

                    $("#cash-flow-loader").hide();
                    $("#output-cash-flow").show();
                }
            })

        }



        function loadAp()
        {
            let period = $("#ap-period").val();
            $("#ap-loader").show();
            $("#ap-result").hide();
            $("#ap-nodata").hide();
            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.ap')}}",
                data: {
                    period:period,
                    _token: "{{csrf_token()}}"
                },
                success: function(response)
                {
                    $("#ap-result").show()
                    const data = response;

                    if(data.data.length < 1){
                        $("#ap-loader").hide();
                        $("#ap-result").hide();
                        $("#ap-nodata").show();
                        return;
                    }

                    let html = `<div class="row mb-3">
                                    <div class="col-md-12 text-center">
                                        <span>Total Hutang</span>
                                        <h2 class="font-weight-bold">${currencyFormat(data.total)}</h2>
                                    </div>
                                </div>`;

                    data.data.forEach(item => {
                        let percent = (item.paid_nominal == 0)? 0:(parseFloat(item.paid_nominal) / parseFloat(item.jumlah)) * 100;

                        let jatuhTempo = moment(item.jatuh_tempo,'YYYY-MM-DD');
                        let current = moment().startOf('day');
                        let diff = moment.duration(jatuhTempo.diff(current)).asDays();

                        html += `<div class="list-with-border mb-3">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <h5 class="font-weight-bold">${item.nama_pemberi_atau_penerima}</h5>
                                        <h4 class="font-weight-bold">${currencyFormat(item.paid_nominal)}</h4>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-between mb-4">
                                        <div>
                                            <small class="bdge-danger p-2 mr-2">H-${diff} jatuh tempo</small>
                                            <small>${moment(item.jatuh_tempo).format('DD MMMM YYYY')}</small>
                                        </div>
                                        <span>dari total ${currencyFormat(item.jumlah)}</span>
                                    </div>
                                    <div class="progress-percent-wrapper">
                                        <div class="progress-percent-outer">
                                            <div style="width: ${percent}%" class="progress-percent-inner" data-toggle="tooltip" data-placement="right" title="terbayar ${currencyFormat(item.paid_nominal)}">
                                            </div>
                                        </div>
                                        <span>${Math.round(percent)}%</span>
                                    </div>
                                </div>`;
                    });

                    $("#ap-result").html(html);
                    $(".progress-percent-inner").tooltip();
                    $("#ap-loader").hide()
                },
                error: function(e) {
                    $("#ap-loader").hide();
                    $("#ap-result").hide();
                    $("#ap-nodata").show();
                    console.log(e);
                }
            })
        }

        function loadAr()
        {
            let period = $("#ar-period").val();
            $("#ar-loader").show();
            $("#ar-result").hide();
            $("#ar-nodata").hide();
            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.ar')}}",
                data: {
                    period:period,
                    _token: "{{csrf_token()}}"
                },
                success: function(response)
                {
                    $("#ar-result").show()
                    const data = response;
                    if(data.data.length < 1){
                        $("#ar-loader").hide();
                        $("#ar-result").hide();
                        $("#ar-nodata").show();
                        return;
                    }

                    let html = `<div class="row mb-3">
                                    <div class="col-md-12 text-center">
                                        <span>Total Piutang</span>
                                        <h2 class="font-weight-bold">${currencyFormat(data.total)}</h2>
                                    </div>
                                </div>`;

                    data.data.forEach(item => {
                        let percent = (item.paid_nominal == 0)? 0:(parseFloat(item.paid_nominal) / parseFloat(item.jumlah)) * 100;
                        let jatuhTempo = moment(item.jatuh_tempo,'YYYY-MM-DD');
                        let current = moment().startOf('day');
                        let diff = moment.duration(jatuhTempo.diff(current)).asDays();

                        html += `<div class="list-with-border mb-3">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <h5 class="font-weight-bold">${item.nama_pemberi_atau_penerima}</h5>
                                        <h4 class="font-weight-bold">${currencyFormat(item.paid_nominal)}</h4>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-between mb-4">
                                        <div>
                                            <small class="bdge-danger p-2 mr-2">H-${diff} jatuh tempo</small>
                                            <small>${moment(item.jatuh_tempo).format('DD MMMM YYYY')}</small>
                                        </div>
                                        <span>dari total ${currencyFormat(item.jumlah)}</span>
                                    </div>
                                    <div class="progress-percent-wrapper">
                                        <div class="progress-percent-outer">
                                            <div style="width: ${percent}%" class="progress-percent-inner" data-toggle="tooltip" data-placement="right" title="terbayar ${currencyFormat(item.paid_nominal)}">
                                            </div>
                                        </div>
                                        <span>${Math.round(percent)}%</span>
                                    </div>
                                </div>`;
                    });

                    $("#ar-result").html(html);
                    $(".progress-percent-inner").tooltip();
                    $("#ar-loader").hide()
                },
                error: function(e) {
                    $("#ar-loader").hide();
                    $("#ar-result").hide();
                    $("#ar-nodata").show();
                    console.log(e);
                }
            })
        }

        function loadBestSellingProduct()
        {
            let period = $("#bestseller-period").val();
            $("#bestseller-loader").show();
            $("#bestseller").hide();
            $("#bestseller-nodata").hide();
            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.best-seller')}}",
                data:{
                    period:period,
                    _token: "{{csrf_token()}}"
                },
                success: function(response)
                {
                    $("#bestseller").show();
                    const data = response;
                    let html = ``;

                    if(data.length < 1){
                        $("#bestseller-loader").hide();
                        $("#bestseller").hide();
                        $("#bestseller-nodata").show();
                        return;
                    }

                    data.forEach(item => {
                        let foto = (item.foto == null)? "{{asset('public/backend/img/no-product-image.png')}}":`{{env('S3_URL')}}${item.foto}`;
                        html += `<li class="mb-3">
                                    <div class="d-flex align-items-center">
                                        <div style="width:30%;">
                                            <img
                                                src="${foto}"
                                                alt="foto produk"
                                                style="width:70px; height: 70px; object-fit:cover;"
                                            >
                                        </div>
                                        <div style="width:50%;">
                                            <h6 stlye="color:background: #323232;" class="font-weight-bold">${item.name}</h6>
                                            <span style="color:#323232;">Total ${item.qty} terjual</span>
                                        </div>
                                        <div style="width:20%;">
                                            <h6 stlye="color:background: #323232;" class="font-weight-bold">${currencyFormat(item.price, "")}</h6>
                                        </div>
                                    </div>
                                </li>`;
                    });

                    $("#best-selling-result").html(html);
                    $("#bestseller-loader").hide()
                },
                error: function(e) {
                    $("#bestseller-loader").hide()
                    $("#bestseller").show()
                    console.log(e);
                }
            })
        }

        function loadConsignment()
        {
            $("#product-consignment-loader").show();
            $("#consignment-result").hide();
            $("#product-consignment-nodata").hide();

            let keyword = $("#search-consignment").val();

            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.consignment')}}",
                data:{
                    _token: "{{csrf_token()}}",
                    keyword: keyword
                },
                success: function(response)
                {
                    $("#consignment-result").show();
                    const data = response;

                    if(data.length < 1){
                        $("#product-consignment-loader").hide();
                        $("#consignment-result").hide();
                        $("#product-consignment-nodata").show();
                        return;
                    }

                    let html = ``;

                    data.forEach(item => {
                        let foto = (item.foto == null)? "{{asset('public/backend/img/no-product-image.png')}}":`{{env('S3_URL')}}${item.foto}`;
                        html += ` <div class="list-with-border mb-3">
                                    <div class="d-flex align-items-center">
                                        <img
                                            src="${foto}"
                                            alt="foto produk"
                                            style="width:70px; height: 70px; object-fit:cover;border-radius: 8px;"
                                            class="mr-3"
                                        >
                                        <div class="mr-2" style="width:50%;">
                                            <h6 stlye="color:#323232;" class="font-weight-bold">${item.name}</h6>
                                            <span class="d-block" style="color:#323232;">Jumlah diterima: ${item.qty} ${item.unit_name}</span>
                                            <span class="d-block" style="color:#323232;">${item.merchant_name}</span>
                                        </div>
                                        <div class="ml-auto">
                                            <h6 stlye="color:#323232;" class="font-weight-bold text-right">Total</h6>
                                            <h4 style="color:#FF470D;" class="text-right">${currencyFormat(item.price,'')}</h4>
                                        </div>
                                    </div>
                                </div>`;
                    });

                    $("#product-consignment-result").html(html);
                    $("#product-consignment-loader").hide();
                    $("#product-consignment-nodata").hide();
                },
                error: function(e) {
                    $("#product-consignment-loader").hide();
                    $("#consignment-result").hide();
                    $("#product-consignment-nodata").show();
                    console.log(e);
                }
            })
        }

        function loadCost()
        {
            $("#cost-loader").show();
            $(".cost-result").hide();
            $(".cost-nodata").hide();
            let period = $("#cost-period").val();
            let merchantId = $('.carousel-cost.active').attr('data-id');
            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.cost')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    period: period,
                    md_merchant_id: merchantId
                },
                success: function(response)
                {
                    $(`#cost-chart-wrapper_${merchantId}`).empty();
                    $(`#cost-chart-wrapper_${merchantId}`).append(`<canvas id="cost-chart_${merchantId}" width="100%" height="100%"></canvas>`);
                    if(response.data.length < 1)
                    {
                        $("#cost-loader").hide();
                        $(".cost-result").hide();
                        $(".cost-nodata").show();
                        return;
                    }

                    const labels = [...response.data].map(item => {
                        return item.name;
                    });

                    const datasets = [{
                        label:"Data",
                        data: [...response.data].map(item => {
                            return item.total
                        }),
                        backgroundColor: [...response.data].map(item => {
                            return generateRandomColor()
                        }),
                        hoverOffset: 20
                    }];

                    const data = {
                        labels: labels,
                        datasets: datasets
                    };
                    const config = {
                        type: 'doughnut',
                        data: data,
                        options: {
                            plugins: {
                                legend:{
                                    display: false
                                }
                            }
                        }
                    };

                    const myChart = new Chart(
                        document.getElementById(`cost-chart_${merchantId}`),
                        config
                    )

                    let html = ``;
                    let backgroundColor = datasets[0].backgroundColor;
                    let periodPreviousName = (response.previousPeriod=='')?'Bulan lalu':response.previousPeriod;
                    response.data.forEach((item, i) => {
                        let increaseType = 'up';
                        if(parseFloat(item.total) > parseFloat(item.total_previous_month)){
                            increaseType = 'up';
                        } else if(parseFloat(item.total) == parseFloat(item.total_previous_month))
                        {
                            increaseType = 'stagnan';
                        } else {
                            increaseType = 'down';
                        }

                        let increaseAmount = parseFloat(item.total) - parseFloat(item.total_previous_month);
                        let increasePercent = 0;
                        let arrow = `<span class="iconify" data-icon="codicon:arrow-small-down" style="color: red;"></span>`;
                        switch(increaseType){
                            case 'up':
                                increasePercent = (item.total_previous_month > 0) ? (Math.abs(increaseAmount)/parseFloat(item.total_previous_month)) * 100 : 100;
                                arrow = `<span class="iconify" data-icon="codicon:arrow-small-up" style="color: green;"></span>`;
                                break;
                            case 'stagnan':
                                increasePercent = 0;
                                arrow = `<span class="iconify" data-icon="codicon:arrow-small-up" style="color: green;"></span>`;
                                break;
                            case 'down':
                                increasePercent = -(Math.abs(increaseAmount)/parseFloat(item.total_previous_month)) * 100;
                                arrow = `<span class="iconify" data-icon="codicon:arrow-small-down" style="color: red;"></span>`;
                                break;
                        }

                        let percentColor = (increaseType != 'down')? "#72BA6C": "#ff0000";
                        let percentageItem = (parseFloat(item.total)/parseFloat(response.total))*100

                        html += `<span id="cost-period-span" class="mb-3 text-right text-light d-block">Perbandingan dengan ${periodPreviousName}</span>`;
                        html += `<div class="list-with-border mb-3" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    <div class="d-flex align-items-center">
                                       <div class="card-circle">
                                            <div class="percent">
                                                <svg>
                                                    <circle cx="40" cy="40" r="20"></circle>
                                                    <circle cx="40" cy="40" r="20" style="--percent:${percentageItem} ; --color-circle:${backgroundColor[i]}"></circle>
                                                </svg>
                                            </div>
                                        </div>

                                        <div>
                                            <h6 class="font-weight-bold mb-0">${item.name}</h6>
                                            <small class="text-light font-weight-bold">${currencyFormat(item.total, '')}</small>
                                        </div>
                                        <div class="ml-auto">
                                            ${arrow}
                                            <small style="color:${percentColor};">${increasePercent}%</small>
                                        </div>
                                    </div>
                                </div>`;
                    });

                    $(`#cost-data_${merchantId}`).html(html);
                    $(".cost-result").show();
                    $("#cost-loader").hide();
                    console.log('ss');
                },
                error: function(e) {
                    $("#cost-loader").hide();
                    $(".cost-result").hide();
                    $(".cost-nodata").show();
                    console.log(e);
                }

            });
        }

        function loadCommission()
        {
            $("#commission-loader").show();
            $("#output-commission").hide();
            $("#commission-nodata").hide();
            let period = $("#commission-period").val();
            let branchElement = [...document.querySelectorAll(".color-picker.cf_")];
            let branch = [...branchElement].map(item => {
                return {
                    id: $(item).attr('data-id'),
                    color: $(item).val()
                }
            });
            let merchantId = [...branch].map(item => {
                return item.id;
            }).toString();

            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.commission')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    md_merchant_id: merchantId,
                    period: period
                },
                success:function(response)
                {
                    const data = response;
                    const categories = [];

                    if(data.length < 1)
                    {
                        $("#commission-loader").hide();
                        $("#output-commission").hide();
                        $("#commission-nodata").show();
                        return;
                    }

                    data.forEach(item => {
                        categories.push(item.fullname);
                    });

                    const series = [...data].map(item => {
                        let foto = (item.foto == null) ? "{{asset('public/backend/img/no-user.png')}}":`{{env('S3_URL')}}${item.foto}`
                        return {
                            type: "scatter",
                            name: item.fullname,
                            data:[{
                                y: item.commission_amount,
                                marker: {
                                    symbol: `url(${foto})`,
                                    width: 40,
                                    height: 40
                                }
                            }]
                        }
                    });

                    $('#output-commission').removeClass('.loading-bg').highcharts({
                        xAxis: {
                            categories: categories
                        },
                        title: "",
                        yAxis: {
                            title: {
                                text: 'Jumlah Dalam Rp'
                            }
                        },
                        plotOptions: {
                            series: {
                                grouping: false
                            }
                        },
                        tooltip: {
                            formatter: function()
                            {
                                return '<b>' + this.x + '</b><br/>' +
                                    'Komisi' + ': ' + currencyFormat(this.y,'') + '<br/>';
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        credits: {
                            enabled: false
                        },
                        series: series
                    });

                    $("#commission-loader").hide();
                    $("#commission-nodata").hide();
                    $("#output-commission").show();
                },
                error:function(er){
                    $("#commission-loader").hide();
                    $("#output-commission").hide();
                    $("#commission-nodata").show();
                    console.log(e);
                }
            })
        }

        function loadOmzet()
        {
            let period = $("#omzet-period").val();
            $("#omzet-loader").show()
            $("#carouselExampleControls").hide()

            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.omzet')}}",
                data: {
                    period: period,
                    _token: "{{csrf_token()}}"
                },
                success:function(response){
                    $("#carouselExampleControls").show()
                    let html = ``;
                    response.data.forEach((item,i) => {
                        let increaseType = 'up';
                        if(parseFloat(item.omzet_this_month) > parseFloat(item.omzet_previous_month)){
                            increaseType = 'up';
                        } else if(parseFloat(item.omzet_this_month) == parseFloat(item.omzet_previous_month))
                        {
                            increaseType = 'stagnan';
                        } else {
                            increaseType = 'down';
                        }

                        let active = (item.id == "{{merchant_id()}}")? "active": "";
                        let imgSource = (increaseType != 'down')? "{{asset('public/backend/img/cost-benefit-up.png')}}":"{{asset('public/backend/img/cost-benefit-down.png')}}";
                        let increaseAmount = parseFloat(item.omzet_this_month) - parseFloat(item.omzet_previous_month);
                        let increasePercent = 0;
                        switch(increaseType){
                            case 'up':
                                increasePercent = (item.omzet_previous_month > 0) ? (Math.abs(increaseAmount)/parseFloat(item.omzet_previous_month)) * 100 : 100;
                                break;
                            case 'stagnan':
                                increasePercent = 0;
                                break;
                            case 'down':
                                increasePercent = -(Math.abs(increaseAmount)/parseFloat(item.omzet_previous_month)) * 100
                                break;
                        }

                        let percentColor = (increaseType != 'down')? "#72BA6C": "#ff0000";
                        let periodName = (response.period=='')?'Bulan ini':response.period;
                        let periodPreviousName = (response.previousPeriod=='')?'Bulan lalu':response.previousPeriod;


                        html += `<div class="carousel-item ${active}">
                                    <div class="px-4">
                                        <h3 style="color:#323232;" class="text-center">${item.name}</h3>
                                    </div>
                                    <div class="p-4" style="background:linear-gradient(180deg, rgba(249, 249, 249, 0) 0%, #FCFAE6 100%)!important;position:relative;">
                                        <h5>Omset ${periodName}</h5>
                                        <h1 class="font-weight-bold">${currencyFormat(parseFloat(item.omzet_this_month), '')}</h1>
                                        <h4 style="color:${percentColor};" class="font-weight-bold">${increasePercent}% <span style="color:#323232;font-size:10px!important;"> dari ${periodPreviousName}</span></h4>
                                        <img
                                            src="${imgSource}"
                                            alt="img"
                                            style="position:absolute;top:50%; right: 30px; transform:translateY(-50%);z-index:1;"
                                        >
                                    </div>
                                    <div class="p-4">
                                        <ul>
                                            <li class="d-flex align-items-center justify-content-between">
                                                <h5>${capitalizeFirstLetter(periodPreviousName)}</h5>
                                                <h5>${currencyFormat(parseFloat(item.omzet_previous_month), '')}</h5>
                                            </li>
                                            <hr>
                                            <li class="d-flex align-items-center justify-content-between">
                                                <h5>Kenaikan ${periodName}</h5>
                                                <h5 style="color: ${percentColor}">${currencyFormat(parseFloat(increaseAmount), '')}</h5>
                                            </li>
                                        </ul>
                                    </div>
                                </div>`;

                    });
                    $("#omzet-loader").hide()
                    $("#omzet-result").html(html);
                },
                error: function(e) {
                    $("#omzet-loader").hide()
                    $("#carouselExampleControls").show()
                    console.log(e);
                }
            })
        }

        function loadProfitLost()
        {
            $("#profit-lost-loader").show()
            $("#output-profit-lost").hide()
            let period = $("#profit-lost-period").val();
            let branchElement = [...document.querySelectorAll(".color-picker.pl_")];
            let branch = [...branchElement].map(item => {
                return {
                    id: $(item).attr('data-id'),
                    name: $(item).attr('data-name'),
                    color: $("#input_color_pl_"+$(item).attr('data-id')).val()
                }
            });

            let merchantId = [...branch].map(item => {
                return item.id;
            }).toString();

            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.profit-lost')}}",
                data: {
                    period:period,
                    md_merchant_id: merchantId,
                    _token: "{{csrf_token()}}"
                },
                success:function(response){
                    const data = response;
                    const categories = [];
                    const series = [];

                    data.listMonth.forEach(item => {
                        categories.push(item);
                    });

                    branch.forEach(b=>{
                        let valueData = []
                        data.group.forEach(item => {
                            item.data.forEach(val => {
                                if(val.merchant_id==b.id){
                                    valueData.push({
                                        merchant_id:val.merchant_id,
                                        revenue:parseFloat(val.revenue),
                                        cost:parseFloat(val.cost),
                                        profit:parseFloat(val.profitLost)
                                    })

                                }
                            });
                        });

                        let revCashFlow = []
                        let costCashFlow = []
                        let profitCashFlow = []

                        valueData.forEach(i=>{
                            if(i.merchant_id==b.id){
                                revCashFlow.push(i.revenue)
                                costCashFlow.push(i.cost)
                                profitCashFlow.push(i.profit)
                            }
                        })
                        series.push({
                            name: b.name,
                            id:b.id,
                            data: revCashFlow,
                            stack: b.id,
                            color: b.color,
                            alias:'rev',
                        })
                        series.push({
                            name: b.name,
                            data:costCashFlow,
                            stack: b.id,
                            linkedTo:b.id,
                            color: invertColor(b.color),
                            alias:'cost'
                        })
                        series.push({
                            name: b.name,
                            data:profitCashFlow,
                            stack: b.id,
                            type: 'line',
                            color: b.color,
                            alias:'profit'
                        });

                    })


                    $('#output-profit-lost').removeClass('.loading-bg').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        subtitle:{
                            text: data.title
                        },
                        xAxis: {
                            categories: categories
                        },
                        yAxis: {
                            title: {
                                text: 'Jumlah Dalam Nominal'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var alias = this.series.userOptions.alias;
                                var name = this.series.userOptions.name;
                                if(alias=='rev'){
                                    return '<b>' + name + '</b><br/>' +
                                        'Pendapatan' + ': ' + currencyFormat(this.y,'') + '<br/>';
                                }else if(alias=='cost'){
                                    return '<b>' + name + '</b><br/>' +
                                        'Biaya' + ': ' + currencyFormat(this.y,'') + '<br/>';

                                }else{
                                    return '<b>' + name + '</b><br/>' +
                                        'Laba Rugi' + ': ' + currencyFormat(this.y,'') + '<br/>';

                                }
                            }
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal'
                            },

                            series: {
                                marker: {
                                    fillColor: '#FFFFFF',
                                    lineWidth: 2,
                                    lineColor: '#FEC208'
                                },
                                borderRadius:'5px',
                                pointWidth: 15
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                        },
                        series: series
                    });

                    $("#profit-lost-loader").hide();
                    $("#output-profit-lost").show();
                },
                error: function(e) {
                    $("#profit-lost-loader").hide()
                    $("#output-profit-lost").show()
                    console.log(e);
                }
            })
        }

        function loadBestSellingService()
        {
            let period = $("#best-selling-service-period").val();
            $("#best-selling-service-loader").show()
            $("#best-selling-service").hide();
            $("#best-selling-service-nodata").hide();
            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.best-seller')}}",
                data:{
                    period:period,
                    _token: "{{csrf_token()}}",
                    is_service: 1
                },
                success: function(response)
                {
                    $("#best-selling-service").show()
                    const data = response;

                    if(data.length < 1){
                        $("#best-selling-service-loader").hide();
                        $("#best-selling-service").hide();
                        $("#best-selling-service-nodata").show();
                        return;
                    }

                    let html = ``;
                    data.forEach(item => {
                        let foto = (item.foto == null)? "{{asset('public/backend/img/no-product-image.png')}}":`{{env('S3_URL')}}${item.foto}`;
                        html += `<li class="mb-3">
                                    <div class="d-flex align-items-center">
                                        <div style="width:30%;">
                                            <img
                                                src="${foto}"
                                                alt="foto produk"
                                                style="width:70px; height: 70px; object-fit:cover;"
                                            >
                                        </div>
                                        <div style="width:50%;">
                                            <h6 stlye="color:background: #323232;" class="font-weight-bold">${item.name}</h6>
                                            <span style="color:#323232;">Total ${item.qty} terjual</span>
                                        </div>
                                        <div style="width:20%;">
                                            <h6 stlye="color:background: #323232;" class="font-weight-bold">${currencyFormat(item.price, "")}</h6>
                                        </div>
                                    </div>
                                </li>`;
                    });

                    $("#best-selling-service-result").html(html);
                    $("#best-selling-service-loader").hide()
                },
                error: function(e) {
                    $("#best-selling-service-loader").hide()
                    $("#best-selling-service").show()
                    console.log(e);
                }
            })
        }

        function loadStock()
        {
            $("#stock-loader").show();
            $("#stock-result").hide();
            $("#stock-nodata").hide();

            let keyword = $("#search-stock").val();

            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.stock')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    keyword: keyword
                },
                success: function(response)
                {
                    const data = response;

                    if(data.length < 1){
                        $("#stock-loader").hide();
                        $("#stock-result").hide();
                        $("#stock-nodata").show();
                        return;
                    }

                    let html = ``;
                    data.forEach(item => {
                        let foto = (item.foto == null)? `{{asset('public/backend/img/no-product-image.png')}}`:`{{env('S3_URL')}}${item.foto}`;
                        html += `<div class="d-flex align-items-center mb-3">
                                    <img
                                        src="${foto}"
                                        alt="foto produk"
                                        style="width:100px; height: 100px; object-fit:cover;border-radius: 8px;"
                                        class="mr-4"
                                    >
                                    <div class="mr-2" style="width:50%;">
                                        <h4 style="color:#323232;" class="font-weight-bold">${item.nama_produk}</h4>
                                        <h6 style="color:#323232;">Total ${item.quantity} terjual bulan ini</h6>
                                        <h6 class="d-block" style="color:#323232;">${capitalizeFirstLetter(item.nama_cabang)}</h6>
                                    </div>
                                    <div class="ml-auto">
                                        <h5 style="color:#323232;" class="font-weight-bold text-right">Stok Sisa</h5>
                                        <h3 style="color:#FF470D;" class="text-right font-weight-bold">${item.residual_stock}</h3>
                                    </div>
                                </div>`;
                    });

                    $("#stock-result").html(html);

                    $("#stock-loader").hide();
                    $("#stock-result").show();
                    $("#stock-nodata").hide();
                },
                error: function(err){
                    $("#stock-loader").hide();
                    $("#stock-result").hide();
                    $("#stock-nodata").show();
                }
            })
        }

        function loadConsumableProductStock()
        {
            $("#consumable-product-stock-loader").show();
            $("#consumable-product-stock-result").hide();
            $("#consumable-product-stock-nodata").hide();

            let keyword = $("#consumable-product-stock-search").val();

            $.ajax({
                type: "POST",
                url: "{{route('merchant.ajax.dashboard.stock')}}",
                data:{
                    _token: "{{csrf_token()}}",
                    keyword: keyword,
                    product_type: 3
                },
                success: function(response)
                {
                    $("#consumable-product-stock-result").show();
                    const data = response;

                    if(data.length < 1){
                        $("#consumable-product-stock-loader").hide();
                        $("#consumable-product-stock-result").hide();
                        $("#consumable-product-stock-nodata").show();
                        return;
                    }

                    let html = ``;

                    data.forEach(item => {
                        let foto = (item.foto == null)? "{{asset('public/backend/img/no-product-image.png')}}":`{{env('S3_URL')}}${item.foto}`;
                        html += ` <div class="list-with-border mb-3">
                                    <div class="d-flex align-items-center">
                                        <img
                                            src="${foto}"
                                            alt="foto produk"
                                            style="width:70px; height: 70px; object-fit:cover;border-radius: 8px;"
                                            class="mr-3"
                                        >
                                        <div class="mr-2" style="width:50%;">
                                            <h6 stlye="color:#323232;" class="font-weight-bold">${item.nama_produk}</h6>
                                            <span class="d-block" style="color:#323232;">${capitalizeFirstLetter(item.nama_cabang)}</span>
                                        </div>
                                        <div class="ml-auto">
                                            <h6 stlye="color:#323232;" class="font-weight-bold text-right">Stock Sisa</h6>
                                            <h4 style="color:#FF470D;" class="text-right">${item.residual_stock}</h4>
                                        </div>
                                    </div>
                                </div>`;
                    });

                    $("#consumable-product-stock-result").html(html);
                    $("#consumable-product-stock-loader").hide();
                    $("#consumable-product-stock-nodata").hide();

                },
                error: function(e) {
                    $("#product-consignment-loader").hide();
                    $("#consignment-result").hide();
                    $("#product-consignment-nodata").show();
                    console.log(e);
                }
            })
        }

        $(document).ready(function() {
            @foreach($selectedWidget as $w)
            {{$w->function}}
            @endforeach
        });

        $(document).on('slid.bs.carousel','#cost-carousel', function (ev) {
            loadCost();
        });

        function checkWidget(e, val)
        {
            const data = JSON.parse(val);
            const name = $(e).attr('data-name');
            let html = `<div class="col-md-12 mb-3 sortable-item" id="sort-${data.element_id}" data-value='${val}'>
                            <div class="selected-widget px-2 py-2">
                                <span class="iconify mr-3" data-icon="akar-icons:drag-vertical"></span>
                                <span>${data.name}</span>
                                <span class="iconify remove-selected-widget ml-auto" data-icon="codicon:chrome-close"></span>
                            </div>
                        </div>`;

            if(e.checked){
                $("#widget-sortable").append(html);
                saveWidget()
            } else {
                $(`#sort-${data.element_id}`).remove();
                saveWidget()
            }
        }

        function removeWidget(selector)
        {
            $(`#sort-${selector}`).remove();
            $(`.widget-checkbox[value="${selector}"]`).prop('checked', false);
            saveWidget()
        }

        function saveWidget()
        {
            const data = new FormData();
            const selectedWidget = [...document.querySelectorAll('.sortable-item')];
            const widgetList = [...selectedWidget].map(item => {
                return JSON.parse(item.getAttribute('data-value'));
            });

            data.append('widget_list', JSON.stringify(widgetList));

            ajaxTransfer("{{route('merchant.dashboard.save-widget')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        }

        $('#widget-sortable').sortable({
            items: '.sortable-item',
            stop: function()
            {
                dropWidget()
            }
        });

        function dropWidget()
        {
            const selectedWidget = [...document.querySelectorAll('.sortable-item')];
            const widgetList = [...selectedWidget].map(item => {
                return JSON.parse(item.getAttribute('data-value'));
            });

            widgetList.forEach(item => {
                let cloneEl = $(`#${item.element_id}`).clone();
                $(`#${item.element_id}`).remove();
                $('#container-selected-widget').append(cloneEl);
                $(`#${item.element_id}`).find('span.select2-container').remove();
                $(`#${item.element_id}`).find('.select-branch').select2();
                eval(item.function);
            });

            $.ajax({
                type: "POST",
                url: "{{route('merchant.dashboard.save-widget')}}",
                data: {
                    _token: "{{csrf_token()}}",
                    widget_list: JSON.stringify(widgetList)
                },
                success: function(response)
                {
                    console.log(response);
                },
                error: function(err){
                    console.log(err);
                    otherMessage(response.type,response.message);
                }
            });
        }

    </script>
@endsection

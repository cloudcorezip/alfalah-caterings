@extends('backend-v3.layout.main')
@section('css')
    <style>
        .link-dashboard:hover {
            box-shadow: 0 4px 8px 0 #fe8e2d75, 0 6px 20px 0 rgba(255, 217, 1, 0.253);
        }
        .btn-FFF1E2{
            background: #FFF1E2;
            color: #FF8100!important;
            padding: 8px 20px;
            border-radius: 11px;
            width:60%!important;
        }
        .btn-interval-wrapper {
            background: #F3F3F3;
            border-radius: 4px;
            padding: 6px 10px;
        }
        .btn-interval {
            background: #F3F3F3;
            border: none!important;
            border-radius: 4px;
            padding: 4px 10px;
            color:#9D9D9D;
        }
        .btn-interval.active {
            background:#FF8C2C;
            color: #fff;
        }
        .btn-interval:focus {
            outline: none!important;
        }
    </style>
@endsection
@section('title',$title)
@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h4 class="mb-1">Ringkasan Klinikmu</h4>
                        <span>Kamu dapat melihat aktivitas pasien dan keuanganmu disini.</span>
                    </div>
                    <div>
                        <a href="" onclick="repeatTour()" data-toggle="tooltip" data-placement="right" title="Ulangi Tour">
                            <i style="color:#747474;" class="fas fa-question-circle h5"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @include('merchant::alert')
    </div>
    <style>
        .card-summary {
            border-radius: 12px;
            box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.05);
            background-color: #FFFFFF;
        }
    </style>
    <div id="main-block-content">
        <div class="row mb-4">
            <div class="col-lg-6 col-md-6">
                <div class="card card-summary h-100">
                    <div class="card-body">
                        <div class="w-100 d-flex align-items-center justify-content-between">
                            <h4 class="font-weight-bold">Aktivitas Appointment</h4>
                            <div class="btn-interval-wrapper">
                                <button class="interval btn-interval" data-value="year">Tahun</button>
                                <button class="interval btn-interval active" data-value="month">Bulan</button>
                            </div>
                        </div>
                        <div class="w-100" id="myChart-wrapper">
                            <canvas id="myChart" width="100%" height="40"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card card-summary">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="font-weight-bold">Jadwal<br>Appointment</h4>
                                <h1 class="font-weight-bold" style="font-size:40px;color:#FF8100;">{{$reservation->total}}</h1>
                                <span class="d-block mb-4" style="color:#7F7F7F;">Jadwal appointment bulan ini</span>
                                <a class="d-flex align-items-center justify-content-between btn-FFF1E2" href="{{route('merchant.toko.reservation.index')}}">
                                    <span>Selebihnya</span>
                                    <span class="iconify" data-icon="akar-icons:circle-chevron-right"></span>
                                </a>
                            </div>
                            <div class="col-6 border-left">
                                <h4 class="font-weight-bold">Total<br>Pasien</h4>
                                <h1 class="font-weight-bold" style="font-size:40px;color:#FF8100;">{{$customer->total}}</h1>
                                <span class="d-block mb-4" style="color:#7F7F7F;">Total yang terdaftar bulan ini</span>
                                <a class="d-flex align-items-center justify-content-between btn-FFF1E2" href="{{route('merchant.toko.customer.index')}}">
                                    <span>Selebihnya</span>
                                    <span class="iconify" data-icon="akar-icons:circle-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 mb-5">
                <div class="card card-summary h-100">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8">
                                <div class="h-75">
                                    <h6 style="color:#909090;">Ringkasan</h6>
                                    <h4 class="font-weight-bold">Penjualan</h4>
                                </div>
                                <div class="h-25">
                                    <br>
                                    <a onclick="activeMenu('penjualan','penjualan-data-penjualan')" href="{{route('merchant.toko.sale-order.data')}}" class="btn-bg__orange py-2 px-4 text-white justify-self-end text-center text-nowrap rounded">
                                        Lihat Selengkapnya
                                    </a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="h-100 d-flex align-items-center justify-content-center">
                                    <img src="{{asset('public/backend')}}/icons/dashboard/amico.png" class="img-fluid"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 mb-5">
                <div class="card card-summary h-100">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8">
                                <div class="h-75">
                                    <h6 style="color:#909090;">Ringkasan</h6>
                                    <h4 class="font-weight-bold">Pembelian</h4>
                                </div>
                                <div class="h-25">
                                    <br>
                                    <a onclick="activeMenu('pembelian','pembelian-data-pembelian')" href="{{route('merchant.toko.purchase-order.data')}}" class="btn-bg__orange py-2 px-4 text-white justify-self-end text-center rounded text-nowrap">
                                        Lihat Selengkapnya
                                    </a>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="h-100 d-flex align-items-center justify-content-center">
                                    <img src="{{asset('public/backend')}}/icons/dashboard/amico-2.png" class="img-fluid"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-12 mb-5">
                <div class="dashboard-component info-table">
                    <div class="card card-summary">
                        <div class="card-body">
                            <h5 class="font-weight-bold mb-4" style="color:#474747;">Stok Obat Dalam Perhatian</h5>
                            <ul class="dashboard-info__list">
                                @if($stockAlert->count()>0)
                                    @foreach($stockAlert as $i)
                                        <li>
                                            <a
                                                class="d-flex justify-content-between align-items-center text-capitalize"
                                                href="{{route('merchant.toko.product.index')}}"
                                                onclick="activeMenu('master-data','master-data-produk')"
                                            >
                                                <div>
                                                    <span class="dashboard-info__code mr-2">[{{$i->code}}]</span>
                                                    <span class="dashboard-info__name">{{$i->name}}</span>
                                                </div>
                                                <span class="dashboard-info__alert__warning" style="color: white">
                                                    {{$i->stock}} {{$i->unit_name}}
                                                </span>
                                            </a>
                                        </li>
                                    @endforeach
                                @else
                                    <li class="text-center">
                                        <span>Tidak ada stok dalam perhatian</span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="dashboard-component info-charts">
                    <div class="card card-summary">
                        <div class="card-body">
                            <div class="info-title" id="title-1"></div>
                            <div class="chart-area loading-bg" id="output-grafik-laba-rugi">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="dashboard-component info-charts">
                    <div class="card card-summary">
                        <div class="card-body">
                            <div class="info-title" id="title-1"></div>
                            <div class="chart-area loading-bg" id="output-grafik-arus-kas">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="dashboard-component info-charts">
                    <div class="card card-summary">
                        <div class="card-body">
                            <div class="info-title" id="title-3"></div>
                            <div class="chart-area loading-bg" id="output-grafik-transaksi">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="dashboard-component info-charts">
                    <div class="card card-summary">
                        <div class="card-body">
                            <div class="info-title" id="title-1"></div>
                            <div class="chart-area loading-bg" id="container-1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>
    <script>
        function loadGrafikLabaRugi() {
            ajaxTransfer("{{route('merchant.toko.laba-rugi')}}", {}, function(response) {
                var data = JSON.parse(response);
                var categories = [],
                    labaRugi = [],
                    pendapatan = [],
                    biaya = [],
                    i;
                var baseCurrency = 'Rp ';
                for (i = 0; i < data.data.length; i++) {
                    categories.push(data.data[i]['nama']);
                    labaRugi.push(data.data[i]['labaRugi']);
                    pendapatan.push(data.data[i]['pendapatan']);
                    biaya.push(-Math.abs(data.data[i]['biaya']));
                }
                $('#output-grafik-laba-rugi').removeClass('.loading-bg').highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    title: {
                        text: 'Laba Rugi'
                    },
                    subtitle:{
                        text: data.title
                    },
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Dalam ' + baseCurrency
                        }
                    },
                    tooltip: {
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal'
                        },
                        series: {
                            pointWidth: 20
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                    },
                    series: [{
                        name: 'Pemasukan',
                        type: 'column',
                        stack: 1,
                        color: "#FEC208",
                        data: pendapatan,
                        tooltip: {
                            valueSuffix: ' '
                        }

                    }, {
                        name: 'Biaya',
                        type: 'column',
                        stack: 1,
                        color: "#FE8F2D",
                        data: biaya,
                        tooltip: {
                            valueSuffix: ' '
                        }

                    }, {
                        name: 'Margin Laba Bersih',
                        type: 'line',
                        color: "#555555",
                        data: labaRugi,
                        tooltip: {
                            valueSuffix: ' '
                        }
                    }]
                });
            }, false, false)
        }

        function loadGrafikArusKas() {
            ajaxTransfer('{{route('merchant.toko.arus-kas')}}', {}, function(response) {
                var data = JSON.parse(response);
                var categories = [],
                    total = [],
                    debit = [],
                    kredit = [],
                    i;
                var baseCurrency = 'Rp ';
                for (i = 0; i < data.data.length; i++) {
                    categories.push(data.data[i]['nama']);
                    total.push(data.data[i]['total']);
                    debit.push(data.data[i]['debit']);
                    kredit.push(data.data[i]['kredit']);
                }
                $('#output-grafik-arus-kas').removeClass('.loading-bg').highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    title: {
                        text: 'Arus Kas'
                    },
                    subtitle:{
                        text: data.title
                    },
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Dalam ' + baseCurrency
                        }
                    },
                    tooltip: {
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal'
                        },
                        series: {
                            pointWidth: 20
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {

                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                    },
                    series: [{
                        name: 'Kas Masuk',
                        type: 'column',
                        stack: 1,
                        color: "#FEC208",
                        data: debit,
                        tooltip: {
                            valueSuffix: ' '
                        }

                    }, {
                        name: 'Kas Keluar',
                        type: 'column',
                        stack: 1,
                        color: "#FE8F2D",
                        data: kredit,
                        tooltip: {
                            valueSuffix: ' '
                        }

                    }, {
                        name: 'Perpindahan Kas Bersih',
                        type: 'line',
                        color: "#555555",
                        data: total,
                        tooltip: {
                            valueSuffix: ' '
                        }
                    }]
                });
            }, false, false)
        }

        function loadTransaction()
        {
            ajaxTransfer('{{route('merchant.toko.load-transaksi')}}', {}, function(response) {
                var data = JSON.parse(response);
                var categories = [],
                    arusKas = [],
                    i;
                var baseCurrency = 'Rp ';
                for (i = 0; i < data.data.length; i++) {
                    categories.push(data.data[i]['nama']);
                    arusKas.push(data.data[i]['jumlah']);
                }
                $('#output-grafik-transaksi').removeClass('.loading-bg').highcharts({
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Grafik Perkembangan Penjualan'
                    },
                    subtitle:{
                        text: data.title
                    },
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Transaksi'
                        }
                    },
                    tooltip: {
                        formatter: function() {
                            return this.y;
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                fillColor: '#FFFFFF',
                                lineWidth: 2,
                                lineColor: '#FEC208' // inherit from series
                            }
                        }
                    },
                    series: [{
                        name: 'Jumlah Transaksi',
                        color: '#FE8F2D',
                        data: arusKas
                    }]
                });
            }, false, false)

        }
        function getFilterDate(filterName=null,customDate=null)
        {
            var momentDate =getMomentNameDate(filterName);
            $("#filter-day").val(filterName);

            $.ajax({
                url: "{{route('toko.report.purchase-order.general',['merchantId'=>merchant_id()])}}",
                type: 'post',
                data:{
                    "period_filter":filterName,
                    "date_custom":customDate
                },
                headers: {
                    'senna-auth':"{{get_user_token()}}"
                },
                dataType: 'json',
                success: function (data) {
                    if(data.code==200)
                    {
                        getChart(data.data.chart);
                    }

                }
            });
        }

        function getChart(data)
        {
            var income=[];
            var profit=[];
            var first=[];
            var last=[];
            var transaction=[];
            var categories=[];
            var total=0;
            $.each(data,function (i,v) {
                total+=v.transaction_amount;
                income.push(parseFloat(v.total));
                profit.push(parseFloat(v.debt));
                categories.push(moment(v.time).format('D MMM Y'));
                transaction.push(parseFloat(v.transaction_amount));

                if(i==0){
                    first.push(moment(v.time).format('D MMM Y'));
                }else if(i==7){
                    last.push(moment(v.time).format('D MMM Y'));
                }
            })
            Highcharts.chart('container-1', {

                title: {
                    text: 'Grafik Pembelian dan Hutang'
                },

                subtitle: {
                    text: 'Periode'+first+'-'+last
                },
                tooltip: {
                    shared: true
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },

                xAxis: {
                    categories: categories
                },


                legend: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                },

                plotOptions: {
                    line: {
                        stacking: 'normal'
                    },
                    series: {
                        pointWidth: 20
                    }
                },

                series: [{
                    name: 'Pembelian',
                    color: "#FEC208",
                    type: 'line',
                    data: income
                }, {
                    name: 'Hutang',
                    color: "#FE8F2D",
                    type: 'line',
                    data: profit
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
        }

        $(document).ready(function() {
            loadGrafikLabaRugi()
            loadGrafikArusKas()
            loadTransaction()
            getFilterDate('thismonth');
        });

    </script>
    <script>
        $('.dropdown-item').click(function(){
            $('.dropdown-menu').removeClass('show');
        });
    </script>

    <script>
        const resetCanvas = () => {
            $('#myChart').remove();
            $('#myChart-wrapper').append('<canvas id="myChart" width="100%" height="40"></canvas>');
        }

        const loadChart = (param) => {
            const data = new FormData();
            data.append('interval', param.interval);
            ajaxTransfer("{{route('merchant.load-reservation')}}", data,function(response){
                const responseData = JSON.parse(response);
                const labels = [];
                const transCount = [];

                responseData.data.forEach(item => {
                    labels.push(item.time);
                    transCount.push(item.total_reservation);
                });

                const data = {
                    labels: labels,
                    datasets: [
                                {
                                    label: 'Total Reservasi',
                                    backgroundColor: '#FF9900',
                                    borderColor: '#FF9900',
                                    data: transCount
                                }
                            ]
                };

                const config = {
                    type: 'bar',
                    data: data,
                    options: {
                        responsive:true,
                        plugins: {
                            legend:{
                                display: false,
                                position: 'bottom'
                            }
                        }
                    }
                };

                const myChart = new Chart(
                    document.getElementById('myChart'),
                    config,
                );
            });
        }

        loadChart({interval:'month'});

        $(".interval").on('click', function(){
            let btnInterval = document.querySelectorAll('.interval');
            btnInterval.forEach(item => {
                item.classList.remove('active');
            });
            $(this).addClass('active');
            let value = $(this).attr('data-value');
            loadChart({interval:value});
            resetCanvas();
        });

    </script>
@endsection

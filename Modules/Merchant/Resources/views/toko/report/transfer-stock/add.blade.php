@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    {{-- @dd($branch) --}}
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.transfer-stock.data')}}">Mutasi Stok</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa menambah data mutasi stok antar gudang dalam usahamu.</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div id="form-konten-result"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode Mutasi Stok</label>
                                <input type="text" name="second_code" class="form-control form-control-sm" value="">

                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Cabang Asal:</label>
                                <select name="from_merchant_id" id="from_merchant_id" class="form-control form-control-sm"  required onchange="clearTable()">
                                    <option disabled selected>Pilih Cabang</option>
                                    @foreach (get_cabang() as $b)
                                        <option value="{{$b->id}}">{{$b->nama_cabang}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Gudang Asal:</label>
                                <select name="inv_warehouse_id" id="inv_warehouse_id" class="form-control form-control-sm" required onchange="clearTable()">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Tanggal Mutasi Stok</label>
                                <input type="text"  class="form-control form-control-sm trans_time" name="created_at" value="{{$data->created_at}}" placeholder="Waktu Transaksi" @if(!is_null($data->id)) disabled @endif required>

                            </div>

                            <div class="form-group">
                                <label class="font-weight-bold">Cabang Tujuan:</label>
                                <select name="to_merchant_id" id="to_merchant_id" class="form-control form-control-sm" required>
                                    <option disabled selected>Pilih Cabang</option>
                                    @foreach (get_cabang() as $b)
                                        <option value="{{$b->id}}">{{$b->nama_cabang}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Gudang Tujuan:</label>
                                <select name="to_inv_warehouse_id" id="to_inv_warehouse_id" class="form-control form-control-sm" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <br>
                        <br>
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="25%">Produk</th>
                                    <th>Stok Asal</th>
                                    <th>Jumlah Transfer</th>
                                    <th width="15%">Satuan</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody id="tbodyid">
                                </tbody>
                            </table>
                            <button type="button" name="add" id="add" class="btn btn-darkblue btn-rounded py-2 px-4">
                                <i class="fa fa-plus mr-2"></i>
                                <span>Tambah Produk</span>
                            </button>
                            <hr>
                        </div>
                    </div>

                    <br>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        var productSelectedId = {};

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        $(document).ready(function() {
            $("#from_merchant_id").select2();
            $("#to_merchant_id").select2();
            $("#inv_warehouse_id").select2();
            $("#to_inv_warehouse_id").select2();
            var i=0;

            $('#add').click(function(){

                var warehouse=$("#inv_warehouse_id").val()
                var branch = $("#from_merchant_id").val()
                var status = false
                if(branch==null){

                    otherMessage('warning','Kamu belum memilih cabang asal !')
                    status = true
                }

                if(warehouse==null){
                    otherMessage('warning','Kamu belum memilih gudang asal !')
                    status = true

                }

                i++;
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">' +
                    '<td>' +
                    '<select  name="sc_product_id[]" id="sc_product_id' + i + '" class="form-control" onchange="productChange('+i+')" ></select>' +
                    '</td>' +
                    '<td>' +
                    '<input type="hidden" id="original_stock'+i+'" name="original_stock[]" class="form-control form-control-sm original_stock" value="0" disabled/>'+
                    '<input type="hidden" name="unit_origin[]" class="form-control form-control-sm"  id="unit_origin'+i+'" disabled />' +
                    '<input type="text" name="stock_display[]" class="form-control form-control-sm stock_display"  id="stock_display'+i+'" disabled />' +
                    '</td>' +
                    '<td>' +
                    '<input type="hidden" name="product_name[]" id="product_name'+i+'" class="form-control form-control-sm" value=""/>' +
                    '<input type="hidden" name="unique_code[]" id="unique_code'+i+'" class="form-control form-control-sm unique_code" />' +
                    '<input type="hidden" name="inv_id[]" id="inv_id'+i+'" class="form-control form-control-sm inv_id" />' +
                    '<input type="hidden" name="hpp_id[]" id="hpp_id'+i+'" class="form-control form-control-sm hpp_id" />' +
                    '<input type="text" name="quantity[]" id="quantity'+i+'" class="form-control form-control-sm quantity" value="0" onkeypress="return isNumber(event)" />' +
                    '</td>' +
                    '<td>' +
                    '<input type="hidden" name="json_multi_unit[]" id="json_multi_unit'+i+'" class="form-control form-control-sm" value="-1"/>' +
                    '<input type="hidden" name="multi_unit_id[]" id="multi_unit_id'+i+'" class="form-control form-control-sm" value="-1"/>' +
                    '<input type="hidden" name="nilai_konversi[]" id="nilai_konversi'+i+'" class="form-control form-control-sm nilai_konversi" value="1"/>' +
                    '<input type="hidden" name="is_multi_unit[]" id="is_multi_unit'+i+'" class="form-control form-control-sm" value="0"/>' +
                    '<input type="hidden" name="unit_name[]" id="unit_name'+i+'" class="form-control form-control-sm" value=""/>' +
                    '<select  name="unit_list[]" id="unit_list' + i + '" class="form-control" onchange="getMultiUnit('+i+')" required></select>' +
                    '</td>' +
                    '<td><button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs  btn_remove"><span class="fa fa-trash-alt" style="color: white"></span></button>' +
                    '</td>' +
                    '</tr>');

                getProductByWarehouse(i)
                $("#unit_list"+i).select2()
                if(status === true)
                {
                    $("#tbodyid").empty();

                }

            });


            $( "#from_merchant_id" ).change(function() {
                var cabang="/"+$( "#from_merchant_id" ).val()+"%29";
                var route="{{route('api.getWarehouse',['merchantId'=>0])}}";
                var route=route.replace(/\/[^\/]*$/,cabang);
                clearTable()
                productSelectedId = {}
                $("#inv_warehouse_id").select2(
                    {
                        ajax: {
                            type: "GET",
                            url: route,
                            dataType: 'json',
                            delay: 250,
                            headers:{
                                "senna-auth":"{{get_user_token()}}"
                            },
                            data: function (params) {
                                return {
                                    key: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: data
                                };
                            },
                            cache: true
                        }
                    }
                );
            });
            $( "#to_merchant_id" ).change(function() {
                var cabang="/"+$( "#to_merchant_id" ).val()+"%29";
                var route="{{route('api.getWarehouse',['merchantId'=>0])}}";
                var route=route.replace(/\/[^\/]*$/,cabang);
                $("#to_inv_warehouse_id").select2(
                    {
                        ajax: {
                            type: "GET",
                            url: route,
                            dataType: 'json',
                            delay: 250,
                            headers:{
                                "senna-auth":"{{get_user_token()}}"
                            },
                            data: function (params) {
                                return {
                                    key: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: data
                                };
                            },
                            cache: true
                        }
                    }
                );
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                delete productSelectedId[button_id];


            });

            $("#inv_warehouse_id").on('select2:select', function (e) {
                productSelectedId = {}
            });

            $("#from_merchant_id").on('select2:select', function (e) {
                productSelectedId = {}
            });

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        })

        function getProductByWarehouse(i)
        {
            var warehouse=$("#inv_warehouse_id").val()
            var branch = $("#from_merchant_id").val()

            $("#sc_product_id" + i).select2({
                ajax: {
                    type: "GET",
                    url: "{{route('api.product.stockadjustment')}}?merchant_id="+branch+"&warehouse="+warehouse,
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });


        }

        function clearTable()
        {
            $("#tbodyid").empty();
        }


        function getMultiUnit(i)
        {

            let unitList = ($("#unit_list"+i).select2('data'))? $("#unit_list"+i).select2('data'):[];
            if(unitList.length>0)
            {
                let data=unitList[0]
                $("#is_multi_unit"+i).val(1)
                $("#unit_name"+i).val(data['text'])
                $("#nilai_konversi"+i).val(data['nilai_konversi'])
                $("#multi_unit_id"+i).val(data['original_id'])
            }
        }

        function productChange(i)
        {
            var products = ($("#sc_product_id"+i).select2('data'))? $("#sc_product_id"+i).select2('data'):[];
            if(products.length>0)
            {
                let datas = products[0]
                $("#unique_code"+i).val(datas['code'])
                $("#original_stock"+i).val(datas['stock'])
                $("#stock_display"+i).val(datas['stock']+' '+datas['unit_name'])
                $("#unit_name"+i).val(datas['unit_name'])
                $("#unit_origin"+i).val(datas['unit_name'])
                $("#coa_inv_id"+i).val(datas['coa_inv_id'])
                $("#coa_hpp_id"+i).val(datas['coa_hpp_id'])
                $("#product_name"+i).val(datas['text'])
                let multi_unit=JSON.parse(datas['multi_unit'])
                let multi_unit_list=[]

                $('#unit_list'+i).empty();
                if(multi_unit.length>0)
                {
                    $.each(multi_unit,function (iteration,value){
                        if(iteration===0)
                        {
                            multi_unit_list.push({
                                id:'-1',
                                text:'Pilih Satuan',
                                selling_price:0,
                                nilai_konversi:0,
                                original_id:'-1',
                            })
                        }

                        multi_unit_list.push({
                            id:value.unit_id,
                            text:value.konversi_ke,
                            selling_price:value.harga_jual,
                            nilai_konversi:value.nilai_konversi,
                            original_id:value.id
                        })
                    })
                    $("#unit_list"+i).select2({
                        data:multi_unit_list
                    })
                    $("#json_multi_unit"+i).val(JSON.stringify(multi_unit_list))

                }else{
                    multi_unit_list.push({
                        id:datas['unit_id'],
                        text:datas['unit_name'],
                        selling_price:datas['selling_price'],
                        nilai_konversi:1,
                        original_id:datas['id']
                    })

                    $("#unit_list"+i).select2({
                        data:multi_unit_list
                    })
                }

                let status = false;
                Object.keys(productSelectedId).forEach(key => {
                    if(productSelectedId[key] === parseInt(datas['id'])){
                        status=true;
                    }
                });

                if(status)
                {
                    $('#row'+i).remove();
                    delete productSelectedId[i];
                }else{
                    productSelectedId[i] = parseInt(datas['id']);
                }
            }
        }


        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.transfer-stock.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                hideLoading()
            });

        })

    </script>
@endsection

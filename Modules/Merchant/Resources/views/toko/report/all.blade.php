@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')

    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Laporan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.report-all', ['page'=>$page])}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berbagai macam laporan usahamu tersedia lengkap disini</span>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a onclick="activeMenu('laporan', 'laporan-all')" class="nav-link active" id="home-tab"  href="#" role="tab" aria-controls="home" aria-selected="true"><h5>{{$subTitle}}</h5> </a>
            </li>
        </ul>
        <div class="card shadow mb-4 pb-3">
            <div class="card-body">
            </div>
            <div class="tab-content px-5 py-3" id="myTabContent">
                <div class="tab-pane fade  @if(is_null($page)) show active @endif" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="container-fluid">
                        <div class="row">
                            @php
                                $subs=\App\Utils\Merchant\MerchantUtil::checkUrl(merchant_id());
                            @endphp
                            <div class="col-md-6">
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.cashflow.index')}}"
                                >

                                    <h5 class="font-weight-bold">Arus Kas</h5>
                                </a>
                                <span>Laporan ini mengukur kas yang telah dihasilkan atau digunakan oleh suatu perusahaan dan menunjukkan detail pergerakannya dalam suatu periode.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.cashflow.index')}}"

                                >
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                                <a
                                    @if($subs['status']==true)
                                    onclick="otherMessage('warning','{{$subs['message']}}')"
                                    @else
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.ledger.index')}}"
                                    @endif
                                >
                                    <h5 class="font-weight-bold">Buku Besar</h5>
                                </a>
                                <span>Laporan ini menampilkan semua transaksi yang telah dilakukan untuk suatu periode. Laporan ini bermanfaat jika Anda memerlukan daftar kronologis untuk semua transaksi yang telah dilakukan oleh perusahaan Anda.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')" href="{{route('merchant.toko.acc.report.ledger.index')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.profit-lost.index')}}">
                                    <h5 class="font-weight-bold">Laba Rugi</h5>
                                </a>
                                <span>Memungkinkan perusahaan untuk membandingkan anggaran keuangan yang sebenarnya.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.profit-lost.index')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.acc.expense')}}">
                                    <h5 class="font-weight-bold">Laporan Pengeluaran Umum</h5>
                                </a>
                                <span>Kelola daftar pengeluaran lainnya</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.acc.expense')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a
                                    @if($subs['status']==true)
                                    onclick="otherMessage('warning','{{$subs['message']}}')"
                                    @else
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.jurnal.index')}}"
                                    @endif
                                >
                                    <h5 class="font-weight-bold">Jurnal</h5>
                                </a>
                                <span>Daftar semua jurnal per transaksi yang terjadi dalam periode waktu. Hal ini berguna untuk melacak di mana transaksi Anda masuk ke masing-masing rekening</span>
                                <br>
                                <a
                                    @if($subs['status']==true)
                                    onclick="otherMessage('warning','{{$subs['message']}}')"
                                    @else
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.jurnal.index')}}"
                                    @endif
                                >
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    @if($subs['status']==true)
                                    onclick="otherMessage('warning','{{$subs['message']}}')"
                                    @else
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.trial-balance.index')}}"
                                    @endif
                                >
                                    <h5 class="font-weight-bold">Neraca</h5>
                                </a>
                                <span>Menampilan apa yang anda miliki (aset), apa yang anda hutang (liabilitas), dan apa yang anda sudah investasikan pada perusahaan anda (ekuitas).</span>
                                <br>
                                <a
                                    @if($subs['status']==true)
                                    onclick="otherMessage('warning','{{$subs['message']}}')"
                                    @else
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.trial-balance.index')}}"
                                    @endif
                                >
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    @if($subs['status']==true)
                                    onclick="otherMessage('warning','{{$subs['message']}}')"
                                    @else
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.financial-position.index')}}"
                                    @endif
                                >
                                    <h5 class="font-weight-bold">Posisi Keuangan</h5>
                                </a>
                                <span>
                                 Laporan ini memberikan informasi mengenai besarnya aset atau harta lembaga dan sumber dari perolehan aset tadi (bisa dari utang atau dari aset bersih) pada satu titik waktu tertentu.
                             </span><br>
                                <a
                                    @if($subs['status']==true)
                                    onclick="otherMessage('warning','{{$subs['message']}}')"
                                    @else
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.acc.report.financial-position.index')}}"
                                    @endif
                                >
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>



                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade @if($page=='sale-order') show active @endif" id="time" role="tabpanel" aria-labelledby="time-tab">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.sale-order.data')}}">
                                    <h5 class="font-weight-bold">Data Penjualan</h5>
                                </a>
                                <span>Menunjukkan daftar dari semua penjualan Anda untuk rentang tanggal yang dipilih.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.sale-order.data')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.index')}}">
                                    <h5 class="font-weight-bold">Penjualan Harian</h5>
                                </a>
                                <span>Ketahui data transaksi penjualan yang terjadi di setiap harinya.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.index')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.category')}}">
                                    <h5 class="font-weight-bold">Penjualan Kategori</h5>
                                </a>
                                <span>Ketahui data transaksi penjualan yang terjadi pada setiap kategori.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.category')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.product')}}">
                                    <h5 class="font-weight-bold">Penjualan Produk</h5>
                                </a>
                                <span>Ketahui informasi mengenai berbagai hal tentang penjualan produk.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.product')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all)"
                                    href="{{route('merchant.report.sale-order.void')}}">
                                    <h5 class="font-weight-bold">Laporan Void</h5>
                                </a>
                                <span>Ketahui informasi transaksi yang dibatalkan.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.void')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.refund')}}">
                                    <h5 class="font-weight-bold">Laporan Refund</h5>
                                </a>
                                <span>Ketahui informasi pengembalian dana karena pembatalan dari pembelian produk.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.refund')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.retur')}}">
                                    <h5 class="font-weight-bold">Laporan Retur</h5>
                                </a>
                                <span>Ketahui informasi jumlah pengembalian barang karena barang tidak sesuai.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.retur')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan','laporan-all')"
                                    href="{{route('merchant.report.sale-order.busiest-selling-time')}}">
                                    <h5 class="font-weight-bold">Waktu Teramai Penjualan</h5>
                                </a>
                                <span>Ketahui waktu teramai terjadinya transaksi penjualan.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan','laporan-all')"
                                    href="{{route('merchant.report.sale-order.busiest-selling-time')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.close-cashier')}}">
                                    <h5 class="font-weight-bold">Laporan Tutup Kasir</h5>
                                </a>
                                <span>Ketahui akumulasi jumlah transaksi pada hari tersebut.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.close-cashier')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.customer-sell')}}">
                                    <h5 class="font-weight-bold">Laporan Pelanggan</h5>
                                </a>
                                <span>Ketahui jumlah transaksi pembelian pelanggan.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.customer-sell')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                                <a
                                    onclick="activeMenu('penjualan','penjualan-data-penjualan')"
                                    href="{{route('merchant.toko.ar.data')}}">
                                    <h5 class="font-weight-bold">Laporan Piutang</h5>
                                </a>
                                <span>Ketahui jumlah hutang yang kamu berikan kepada pihak tertentu.</span>
                                <br>
                                <a
                                    onclick="activeMenu('penjualan','penjualan-data-penjualan')"
                                    href="{{route('merchant.toko.ar.data')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.tax')}}">
                                    <h5 class="font-weight-bold">Laporan Pajak Penjualan</h5>
                                </a>
                                <span>Ketahui jumlah pajak penjualan yang harus dibayarkan kepada negara.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.tax')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.busiest-product-time')}}">
                                    <h5 class="font-weight-bold">Waktu Teramai Produk</h5>
                                </a>
                                <span>Ketahui waktu teramai terjadinya transaksi penjualan per produk.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.sale-order.busiest-product-time')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                            </div>



                        </div>
                    </div>
                </div>
                <div class="tab-pane fade @if($page=='purchase-order') show active @endif" id="time" role="tabpanel" aria-labelledby="time-tab">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <a
                                    onclick="activeMenu('pembelian','pembelian-data-pembelian')"
                                    href="{{route('merchant.toko.purchase-order.data')}}">
                                    <h5 class="font-weight-bold">Data Pembelian</h5>
                                </a>
                                <span>Menunjukkan daftar dari semua pembelian Anda untuk rentang tanggal yang dipilih.</span>
                                <br>
                                <a
                                    onclick="activeMenu('pembelian','pembelian-data-pembelian')"
                                    href="{{route('merchant.toko.purchase-order.data')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.purchase-order.product')}}">
                                    <h5 class="font-weight-bold">Laporan Pembelian</h5>
                                </a>
                                <span>Ketahui laporan pembelian barang berdasarkan kategori dan produk secara fleksibel</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.purchase-order.product')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                            </div>

                            <div class="col-md-6">
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.purchase-order.retur')}}">
                                    <h5 class="font-weight-bold">Laporan Retur</h5>
                                </a>
                                <span>Ketahui daftar retur pengembalian barang karena barang tidak sesuai.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.purchase-order.retur')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.ap.data')}}">
                                    <h5 class="font-weight-bold">Laporan Utang Pembelian</h5>
                                </a>
                                <span>Ketahui jumlah hutang yang kamu dapatkan dari pihak tertentu.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.ap.data')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade @if($page=='inventory') show active @endif" id="time" role="tabpanel" aria-labelledby="time-tab">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.stock.hpp.data')}}">
                                    <h5 class="font-weight-bold">Data Persediaan</h5>
                                </a>
                                <span>Kelola informasi kondisi persediaan barang.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.toko.stock.hpp.data')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.inventory.turnover-stock')}}">
                                    <h5 class="font-weight-bold">Laporan Stok Per Gudang</h5>
                                </a>
                                <span>Kelola informasi kondisi persediaan barang  yang masuk, keluar, tersedia atau tersisa pada setiap gudang.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.inventory.turnover-stock')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                            </div>

                            <div class="col-md-6">
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.inventory.demage-stock')}}">
                                    <h5 class="font-weight-bold">Laporan Stok Produk Rusak</h5>
                                </a>
                                <span>Laporan yang memperlihatkan stok-stok produk yang mengalami kerusakan.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan', 'laporan-all')"
                                    href="{{route('merchant.report.inventory.demage-stock')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade @if($page=='employee') show active @endif" id="inv" role="tabpanel" aria-labelledby="inv-tab">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">

                                <a
                                    onclick="activeMenu('absensi','absensi-laporan')"
                                    href="{{route('merchant.toko.report.staff-attendance.data')}}">
                                    <h5 class="font-weight-bold">Absensi</h5>
                                </a>
                                <span>Menunjukkan daftar dari semua absensi karyawanmu untuk rentang tanggal yang dipilih.</span>
                                <br>
                                <a
                                    onclick="activeMenu('absensi','absensi-laporan')"
                                    href="{{route('merchant.toko.report.staff-attendance.data')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                                <br>

                                <a
                                    onclick="activeMenu('laporan','komisi')"
                                    href="{{route('merchant.report.employee.commission')}}">
                                    <h5 class="font-weight-bold">Komisi Karyawan</h5>
                                </a>
                                <span>Menunjukkan daftar komisi dari semua karyawanmu untuk rentang tanggal yang dipilih.</span>
                                <br>
                                <a
                                    onclick="activeMenu('laporan','komisi')"
                                    href="{{route('merchant.report.employee.commission')}}">
                                    <button class="btn btn-outline-warning mt-3 mb-3">Lihat Laporan</button>
                                </a>
                                <br>
                            </div>
                            <div class="col-md-6">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Pembelian</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.retur-purchase-order.data')}}">Retur</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Disini kamu bisa membuat retur pembelianmu</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row mb-4">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>new \App\Models\SennaToko\ReturPurchaseOrder(),
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>true
])
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode Retur</label>
                                <input type="text" class="form-control form-control-sm" name="second_code"  placeholder="Kode Retur">
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Supplier <span class="text-danger">*</span></label>
                                <select id="sc_supplier_id" class="form-control form-control-sm select22" name="sc_supplier_id" >
                                    <option disabled selected>Pilih Supplier</option>
                                    @foreach ($supplier as $b)
                                        <option value="{{$b->id}}">{{$b->nama_supplier}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Retur dari <span class="text-danger">*</span></label>
                                <select id="retur_from" name="retur_from" class="form-control form-control-sm select22">
                                    <option disabled selected>Pilih Retur dari</option>
                                    <option value="0">Faktur</option>
                                    <option value="3">Penerimaan</option>
                                </select>
                                <input type="hidden" id="step_type" value="">
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Kode Transaksi <span class="text-danger">*</span></label>
                                <select id="code" class="form-control form-control-sm select22" name="code">
                                    <option disabled selected>Pilih Kode Transaksi</option>

                                </select>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Gudang</label>
                                <input type="text" class="form-control form-control-sm" name="inv_warehouse_id" id="inv_warehouse_id" disabled>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Hutang</label>
                                <input type="text" class="form-control form-control-sm" id="is_debet" disabled>
                                <input type="hidden" class="form-control form-control-sm" name="is_debet" id="is_debet_id" disabled>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Alasan Retur</label>
                                <div id="reason">
                                    <select class="form-control form-control-sm select22">
                                        <option disabled selected>Pilih Alasan Retur</option>
                                    </select>
                                </div>
                                <div id="reason_no_debet">
                                    <select id="form_reason_no_debet" class="form-control form-control-sm select22 reason" onchange="changeReason()">
                                        <option disabled>Pilih Alasan Retur</option>
                                        <option value="1">Tukar Barang(Mengganti barang yang rusak dengan yang baru)</option>
                                        <option value="2">Retur Dengan Pembatalan Item Transaksi</option>
                                    </select>
                                </div>
                                <div id="reason_with_debet">
                                    <select id="form_reason_with_debet" class="form-control form-control-sm select22 reason" onchange="changeReason()">
                                        <option disabled>Pilih Alasan Retur</option>
                                        <option value="1">Tukar Barang(Mengganti barang yang rusak dengan yang baru)</option>
                                        <option value="3">Retur Dengan Mengurangi Hutang</option>
                                    </select>
                                </div>
                                <input type="hidden" class="form-control form-control-sm" name="reason_id" id="reason_hidden" value="1" disabled>

                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Tanggal Pengajuan Retur</label>
                                <input type="text" class="form-control form-control-sm trans_time" name="created_at"  placeholder="Waktu Pengajuan Retur" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-custom" id="table-product" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="25%">Produk</th>
                                    <th>Max Dikembalikan</th>
                                    <th width="15%">Satuan</th>
                                    <th>Dikembalikan</th>
                                    <th width="20%">Satuan</th>
                                </tr>
                                </thead>
                                <tbody id="table-body">

                                </tbody>

                            </table>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1" class="font-weight-bold">Catatan</label>
                                <textarea name="note" class="form-control form-control-sm" id="" cols="20" rows="5"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 text-right">
                        <button class="btn btn-success"><i class="fa fa-save mr-2"></i> Simpan</button>
                    </div>
                    <input type='hidden' name='is_deliv' value=''>
                    <input type='hidden' id='id' name='id' value=''>
                    <input type='hidden' id="is_debet_val" value='0'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                </form>

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $("#reason_no_debet").hide();
            $("#reason_with_debet").hide();
            $( ".select22").select2();
            onchangeBranch()
        });

        $( "#sc_supplier_id").change(function() {
            $('#table-body').html('');
            $('#code').val('');
            $('#inv_warehouse_id').val('');
            $('#is_debet').val('');
            $('#reason_with_debet').hide();
            $('#reason_no_debet').hide();
            $("#reason").show();
            getPurchase();
        });

        $( "#retur_from").change(function() {
            getPurchase();
        });

        function getPurchase(){
            let branch = $("#branch").val()
            if(branch=='-1' || branch==-1)
            {
                otherMessage('warning','Silahkan memilih cabang terlebih dahulu!')
            }else{
                $("#code").select2({
                    ajax: {
                        type: "GET",
                        url: "{{route('api.purchase.all')}}?sc_supplier_id="+$("#sc_supplier_id").val()+"&step_type="+$("#retur_from").val()+"&merchant_id="+branch,
                        dataType: 'json',
                        data: {
                            sc_supplier_id: $("#sc_supplier_id").val(),
                            step_type: $("#retur_from").val()
                        },
                        delay: 250,
                        headers:{
                            "senna-auth":"{{get_user_token()}}"
                        },
                        data: function (params) {
                            return {
                                key: params.term
                            };
                        },
                        processResults: function (data) {

                            return {

                                results: data
                            };
                        },
                        cache: true
                    },
                    placeholder:"Pilih Kode Transaksi"
                });

            }
            $( "#code").change(function() {
                $.ajax({
                    type:"POST",
                    url:"{{route('api.purchase.detail')}}",
                    headers:{
                        "Senna-auth":"{{get_user_token()}}"
                    },
                    data: {
                        sc_purchase_order_id: $("#code").val()
                    },
                    dataType:"JSON",
                    success: function (data) {
                        let html = "";
                        let is_debet = 0;
                        let warehouse = 0;
                        let purchase_id = 0;
                        let is_paid_off=0;
                        $('#table-body').empty();

                        $.each(data,function (i,item){
                            let unit =(item.unit_name_2==='')?item.unit_name:item.unit_name_2
                            let product = (item.product_name_2==='')?item.product_name+" "+item.product_code:item.product_name_2
                            let unitList=[];
                            if(item.is_multi_unit==0){
                                unitList.push({
                                    id:0,
                                    text:unit
                                })
                            }else{
                               if(item.json_multi_unit!=='-1' || item.json_multi_unit!=-1){
                                   JSON.parse(item.json_multi_unit).forEach(multi=>{
                                       unitList.push({
                                           id:multi.id,
                                           text:multi.text
                                       })
                                   })
                               }else{
                                   unitList.push({
                                       id:0,
                                       text:unit
                                   })
                               }
                            }

                            html += "<tr>"+
                                "<td class='text-center'>"+"<input type='hidden' name='sc_product_id[]' value='"+item.product_id+"'><input class='form-control form-control-sm' type='text' value='"+item.product_name+" "+item.product_code+" ' disabled></td>"+
                                "<td class='text-center'>"+"<input type='hidden' name='purchase_order_detail_id[]' value='"+item.id_detail+"'><input class='form-control form-control-sm' type='text' name='quantity[]' value='"+item.quantity+"' disabled></td>"+
                                "<td class='text-center'>"+
                                "<input type='hidden' name='original_unit_name[]' value='"+unit+"'>" +
                                "<input type='hidden' name='product_name[]' value='"+product+"'>" +
                                "<input class='form-control form-control-sm' type='text' value='"+unit+"' disabled>" +
                                "</td>"+
                                "<td class='text-center'>"+"" +
                                "<input class='form-control form-control-sm' type='text' name='quantity_retur[]' value='0' required onkeypress='isNumber(event)'></td>"+
                                "<td class='text-center'>"+"" +
                                "<input class='form-control form-control-sm' type='hidden' name='is_multi_unit[]' id='multi_unit"+i+"' value='"+item.is_multi_unit+"'>"+
                                "<input class='form-control form-control-sm' type='hidden' name='multi_unit_id[]' id='multi_unit_id"+i+"' value='"+item.multi_unit_id+"'>"+
                                "<input class='form-control form-control-sm' type='hidden' name='value_conversation[]' id='value_conversation"+i+"' value='"+((item.value_conversation===0)?1:item.value_conversation)+"'>"+
                                "<input class='form-control form-control-sm' type='hidden' name='multi_quantity[]' id='multi_quantity"+i+"' value='"+item.multi_quantity+"'>"+
                                "<input class='form-control form-control-sm' type='hidden' name='origin_multi_quantity[]' id='origin_multi_quantity"+i+"' value='"+item.origin_multi_quantity+"'>"+
                                "<input class='form-control form-control-sm' type='hidden' name='json_multi_unit[]' id='json_multi_unit"+i+"' value='"+item.json_multi_unit+"'>"+
                                "<input type='hidden' name='unit_name[]' id='unit_name"+i+"' value='"+unit+"'>";
                            html+="<select name='unit_po_id[]' id='unit_po_id"+i+"' class='form-control unit_po_id' onchange='getMultiUnit("+i+")'>";
                            unitList.forEach(ul=>{
                                if(ul.text.toLowerCase()==unit.toLowerCase()){
                                    html+="<option value='"+ul.id+"' selected>"+ul.text+"</option>"

                                }else{
                                    html+="<option value='"+ul.id+"'>"+ul.text+"</option>"

                                }
                            })
                            html+="</select></td></tr>";

                            is_debet=item.is_debet;
                            warehouse=item.warehouse;
                            purchase_id=item.purchase_id;
                            is_paid_off=item.is_paid_off;
                        })

                        $("#table-body").append(html)

                        $('#id').val(purchase_id);
                        $("#is_debet_val").val(is_debet);
                        $('#inv_warehouse_id').val(warehouse);

                        if(is_debet==0){
                            $('#is_debet').val("Tidak");

                        }else{
                            $('#is_debet').val("Ya");

                        }
                        if(is_debet==0){
                            $('#reason_no_debet').show();
                            $('#reason_with_debet').hide();
                            $("#reason").hide();
                        }else{
                            if(is_paid_off==1){
                                $('#reason_no_debet').show();
                                $('#reason_with_debet').hide();
                                $("#reason").hide();
                            }else{
                                $('#reason_no_debet').hide();
                                $('#reason_with_debet').show();
                                $("#reason").hide();
                            }
                        }
                    }

                });
            });

        }


        function getMultiUnit(i)
        {
            let isMulti=$("#multi_unit"+i).val()
            let unitList=$("#json_multi_unit"+i).val()
            let unit_id= $("#unit_po_id"+i).val()
            if(isMulti==1)
            {
                if(unitList!=='null' || unitList!==null){
                    JSON.parse(unitList).forEach(ul=>{
                        if(parseInt(unit_id)==parseInt(ul.id)){
                            $("#value_conversation"+i).val(ul.nilai_konversi)
                            $("#multi_unit_id"+i).val(ul.original_id)
                            $("#unit_name"+i).val(ul.text)
                        }
                    })
                }
            }
        }


        function changeReason()
        {
            if($("#is_debet_val").val()==0)
            {
                var value=$( "#form_reason_no_debet").val();

            }else{
                var value=$( "#form_reason_with_debet").val();
            }
            $("#reason_hidden").val(value);
        }

        $("#reason_id").select2();

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            if($("#retur_from").val()==0){
                ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.save-retur')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable);
                });
            }else{
                ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.save-retur-deliv')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable);
                });
            }

        });
        function onchangeBranch()
        {
            $("#table-body").empty()
            $('#id').val(null);
            $("#is_debet_val").val(null);
            $("#is_debet").val(null);
            $('#inv_warehouse_id').val(null);
            $("#reason").show();
            $('#reason_with_debet').hide();
            $('#reason_no_debet').hide();
            $("#code").empty()
            getPurchase()
        }
        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endsection





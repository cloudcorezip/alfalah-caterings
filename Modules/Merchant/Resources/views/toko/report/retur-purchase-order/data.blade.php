@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Pembelian</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="">Retur</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa mengetahui informasi lengkap data retur pembelian</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @include('merchant::alert')

        </div>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row mb-3">
                    @if(\App\Utils\BranchConfig\BranchConfigUtil::getAccess('purchase',merchant_id())==true)
                    <div class="col-lg-auto col-md-12">
                        <a class="btn btn-success btn-rounded btn-sm py-2 px-4"  href="{{route('merchant.toko.retur-purchase-order.add')}}"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>
                    @endif
                </div>
                <div id="output-sale-order">
                    @include('merchant::toko.report.retur-purchase-order.list')
                </div>
            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>
    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" class="px-30" onsubmit="return false">
            <div class="row d-flex align-items-center">

                <div class="col-md-12 mb-3">
                    <label for="exampleInputPassword1">Pilih Cabang</label>
                    <select name="md_merchant_id" id="branch" class="form-control form-control">
                        <option value="-1">Semua Cabang</option>
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" >{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12 mb-3">
                    <label>Pilih Supplier :</label>
                    <select name="sc_supplier_id" id="sc_supplier_id" class="form-control form-control-sm">
                        <option value="-1">Semua Supplier</option>
                        @foreach($supplier as $key =>$item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12 mb-3">
                    <label>Pilih Tanggal Transaksi :</label>
                    <div id="reportrange" class="form-control form-control" name="date-range" style="background: white">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
                <div class="col-md-12 mt-3">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>
                </div>
            </div>

        </form>
    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')
    <script type="text/javascript">
        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                    '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                    '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                    'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
                    //'All Time': 'all-time',
                }
            }, cb);
            cb(start, end);

        });
    </script>
    <script>
        $('#sc_supplier_id').select2();
        $('#branch').select2();

        function exportData() {
            var info = $('#table-data-r-rpo_info').html().split('dari');
            if (info.length < 2) {
                return false;
            }

            var dataCount = parseInt(info[1].replace('entri', '').replace(',', '').replace(' ', ''));
            var seconds = parseFloat(dataCount / 66).toFixed(3);
            var estimation = '';

            if (seconds < 60) {
                estimation = seconds + ' detik';
            } else {
                var minute = parseInt(seconds / 60);
                seconds = seconds % 60;
                estimation = minute + ' menit ' + seconds + ' detik';
            }

            modalConfirm('Export Data', 'Proses export data sejumlah ' + dataCount + ' data membutuhkan estimasi waktu ' + estimation + '. Lanjutkan proses export data?', function () {
                var data = getFormData('form-filter-sale-order');
                var startDate = $('#reportrange').data('daterangepicker').startDate._d;
                var endDate = $('#reportrange').data('daterangepicker').endDate._d;
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                ajaxTransfer("{{route('merchant.toko.retur-purchase-order.export-data')}}", data, '#modal-output');
            });

        }
        function hapusRetur(id,step_type) {
            var data = new FormData();
            data.append('id', id);
            if(step_type==0){
                modalConfirm("Konfirmasi", "Apa anda yakin menghapus retur dari faktur pembeliaan ?", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.delete-retur')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable);
                });
                })
            }else{
                modalConfirm("Konfirmasi", "Apa anda yakin menghapus data retur dari penerimaan pembeliaan?", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.delete-retur-deliv')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable);
                });
            })
            }


        }
        $(document).ready(function () {
            $('#form-filter-sale-order').submit(function () {
                var startDate = $('#reportrange').data('daterangepicker').startDate._d;
                var endDate = $('#reportrange').data('daterangepicker').endDate._d;
                var data = getFormData('form-filter-sale-order');
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                ajaxTransfer("{{route('merchant.toko.retur-purchase-order.reload-data')}}", data, '#output-sale-order');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        });

    </script>

@endsection

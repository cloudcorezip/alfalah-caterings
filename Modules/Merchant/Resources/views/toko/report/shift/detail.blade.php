@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <h4>{{$title}}</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route(''.dashboard_url().'')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                </ol>
            </nav>
        </div>
    </div>

    <!-- DataTales Example -->

    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    @if(!is_null($first))
                        <div class="row">
                            <div class="col-md-7">
                                <table>
                                    <tr>
                                        <td>Petugas Shift</td>
                                        <td>:</td>
                                        <td>{{$first->getShift->getUser->fullname}}</td>
                                    </tr>
                                    <tr>
                                        <td>Posisi</td>
                                        <td>:</td>
                                        <td>{{$first->getShift->getUser->getRole->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Kas Awal</td>
                                        <td>:</td>
                                        <td>{{rupiah($first->getShift->starting_cash)}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-5">
                                <table>
                                    <tr>
                                        <td>Mulai Shift</td>
                                        <td>:</td>
                                        <td>{{ date( 'd-m-Y H:i', strtotime($first->getShift->start_shift))}}</td>
                                    </tr>
                                    <tr>
                                        <td>Akhir Shift</td>
                                        <td>:</td>
                                        <td>{{ date( 'd-m-Y H:i', strtotime($first->getShift->end_shift))}}</td>
                                    </tr>
                                    <tr>
                                        <td>Cetak</td>
                                        <td>:</td>
                                        @php
                                            $id=$first->md_merchant_shift_id;
                                        @endphp
                                        <td><a target="_blank" href="{{route('merchant.toko.shift.cetakExcel',['id'=>$id])}}" class="btn btn-xs btn-success btn-rounded text-white"><i class="fa fa-file-excel"></i> Excel</a>
                                            <a target="_blank" href="{{route('merchant.toko.shift.cetakPDF',['id'=>$id])}}" class="btn btn-xs btn-danger btn-rounded text-white"><i class="fa fa-file-pdf"></i> PDF</a></td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                        <br>
                        <table class="table table-bordered" id="table-data" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Tipe Transaksi</td>
                                <td>Keterangan</td>
                                <td>Jumlah</td>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $penjualan=0;$pembelian=0;$pemasukan_lain=0;$pengeluaran_lain=0;
                            @endphp
                            @foreach($data as $key => $item)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->getCashType->name}}</td>
                                    <td>{{$item->note}}</td>
                                    <td>{{rupiah($item->amount)}}</td>
                                </tr>
                                @php
                                    if ($item->md_sc_cash_type_id==1){
                                         $penjualan+=$item->amount;
                                    }else if($item->md_sc_cash_type_id==2){
                                         $pembelian+=$item->amount;
                                    }else if($item->md_sc_cash_type_id==3){
                                         $pemasukan_lain+=$item->amount;
                                    }else if($item->md_sc_cash_type_id==4){
                                         $pengeluaran_lain+=$item->amount;
                                    }
                                @endphp

                            @endforeach
                            <tr>
                                <td colspan="2" class="border-0"></td>
                                <td>Penjualan</td>
                                <td><b>{{rupiah($penjualan)}}</b> </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="border-0"></td>
                                <td>Pemasukan Lain</td>
                                <td><b>{{rupiah($pemasukan_lain)}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="border-0"></td>
                                <td>Pembelian</td>
                                <td><b>{{rupiah($pembelian)}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="border-0"></td>
                                <td>Pengeluaran Lain</td>
                                <td><b>{{rupiah($pengeluaran_lain)}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="border-0"></td>
                                <td>Penerimaan Aktual</td>
                                <td><b>{{rupiah($result=$penjualan+$pemasukan_lain+$data[0]->getShift->starting_cash-$pembelian+$pengeluaran_lain)}}</b></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="border-0"></td>
                                <td>Selisih</td>
                                <td><b>{{rupiah($result-($penjualan+$pemasukan_lain+$data[0]->starting_cash))}}</b></td>
                            </tr>
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info">Data tidak tersedia</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

</head>

<body>

    <div class="row">
        <div class="col-xs-12 text-center">
            <h4 >Detail Shift</h4>
        </div>
    </div>

    <div class="table-responsive">
        <div class="row">
            <div class="col-xs-6">
                <table class="table-striped">
                    <tr>
                        <td>Petugas Shift</td>
                        <td>:</td>
                        <td>{{$data[0]->getShift->getUser->fullname}}</td>
                    </tr>
                    <tr>
                        <td>Posisi</td>
                        <td>:</td>
                        <td>{{$data[0]->getShift->getUser->getRole->name}}</td>
                    </tr>
                    <tr>
                        <td>Kas Awal</td>
                        <td>:</td>
                        <td>{{rupiah($data[0]->getShift->starting_cash)}}</td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-6">
                <table>
                    <tr>
                        <td>Mulai Shift</td>
                        <td>:</td>
                        <td>{{ date( 'd-m-Y H:i', strtotime($data[0]->getShift->start_shift))}}</td>
                    </tr>
                    <tr>
                        <td>Akhir Shift</td>
                        <td>:</td>
                        <td>{{ (is_null($data[0]->getShift->end_shift))?'-':date( 'd-m-Y H:i', strtotime($data[0]->getShift->end_shift))}}</td>
                    </tr>
                </table>

            </div>
        </div>
        <br>
        <table class="table table-bordered" id="table-data" width="100%" cellspacing="0">
            <thead>
            <tr>
                <td>No</td>
                <td>Tipe Transaksi</td>
                <td>Keterangan</td>
                <td>Jumlah</td>
            </tr>
            </thead>
            <tbody>
            @php
                $penjualan=0;$pembelian=0;$pemasukan_lain=0;$pengeluaran_lain=0;
            @endphp
            @foreach($data as $key => $item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->getCashType->name}}</td>
                    <td>{{$item->note}}</td>
                    <td>{{rupiah($item->amount)}}</td>
                </tr>
                @php
                   if ($item->md_sc_cash_type_id==1){
                        $penjualan+=$item->amount;
                   }else if($item->md_sc_cash_type_id==2){
                        $pembelian+=$item->amount;
                   }else if($item->md_sc_cash_type_id==3){
                        $pemasukan_lain+=$item->amount;
                   }else if($item->md_sc_cash_type_id==4){
                        $pengeluaran_lain+=$item->amount;
                   }
                @endphp

            @endforeach
            <tr>
                <td colspan="2" class="border-0"></td>
                <td>Penjualan</td>
                <td><b>{{rupiah($penjualan)}}</b> </td>
            </tr>
            <tr>
                <td colspan="2" class="border-0"></td>
                <td>Pemasukan Lain</td>
                <td><b>{{rupiah($pemasukan_lain)}}</b></td>
            </tr>
            <tr>
                <td colspan="2" class="border-0"></td>
                <td>Pembelian</td>
                <td><b>{{rupiah($pembelian)}}</b></td>
            </tr>
            <tr>
                <td colspan="2" class="border-0"></td>
                <td>Pengeluaran Lain</td>
                <td><b>{{rupiah($pengeluaran_lain)}}</b></td>
            </tr>
            <tr>
                <td colspan="2" class="border-0"></td>
                <td>Penerimaan Aktual</td>
                <td><b>{{rupiah($result=$penjualan+$pemasukan_lain+$data[0]->getShift->starting_cash-$pembelian+$pengeluaran_lain)}}</b></td>
            </tr>
            <tr>
                <td colspan="2" class="border-0"></td>
                <td>Selisih</td>
                <td><b>{{rupiah($result-($penjualan+$pemasukan_lain+$data[0]->starting_cash))}}</b></td>
            </tr>
            </tbody>
        </table>
    </div>

</body>
</html>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <h6>{{$title}}</h6>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route(''.dashboard_url().'')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                </ol>
            </nav>
        </div>
    </div>

    <!-- DataTales Example -->

    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">{{$title}}</h6>
            </div>
            <div class="card-body">
                <form id="form-filter-shift" onsubmit="return false">
                <div class="row">
                    <div class="col-md-5">
                        <div id="reportrange" class="form-control form-control-sm" name="date-range" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <select name="md_user_id" id="" class="form-control form-control-sm">
                            <option value="-1">Semua Kasir</option>
                            @foreach($staff as $key =>$item)
                                <option value="{{$item->id}}">{{$item->fullname}}-({{$item->role_name}})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="is_active" id="" class="form-control form-control-sm">
                            <option value="-1">Status</option>
                                <option value="1">Aktif</option>
                                <option value="0">Non-Aktif</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div class="text-left">
                            <button type="submit" class="btn btn-sm btn-primary btn-md"><i class="fa fa-filter"></i> Filter Data</button>
                        </div>
                    </div>
                </div>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                </form>
                <br>
                <br>
                <hr>
                <div id="output-shift">
                    @include('merchant::toko.report.shift.list')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')

    <script type="text/javascript">
        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                    '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                    '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                    'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
                    //'All Time': 'all-time',
                }
            }, cb);
            cb(start, end);

        });
    </script>
    <script>
        $(document).ready(function () {
            $('#form-filter-shift').submit(function () {
                var startDate = $('#reportrange').data('daterangepicker').startDate._d;
                var endDate = $('#reportrange').data('daterangepicker').endDate._d;
                var data = getFormData('form-filter-shift');
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                ajaxTransfer("{{route('merchant.toko.shift.reload-data')}}", data, '#output-shift');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        });

    </script>
@endsection

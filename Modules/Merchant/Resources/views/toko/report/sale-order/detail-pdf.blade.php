<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

</head>

<body>

    <div class="row">
        <div class="col-xs-6">
            <h4>{{$data->getMerchant->name}}</h4>
                <span>{{$data->getMerchant->address}} </span><br>
                <span>Email:{{$data->getMerchant->email_merchant}}</span><br>
                <span>Kontak:{{$data->getMerchant->phone_merchant}}</span><br><br><br>
            </div>
        <div class="col-xs-6">
            <h4 style="padding-left: 15%;padding-top: 12%"><u>INVOICE</u> </h4>
        </div>
    </div>
<div class="row">
    <div class="col-xs-6">
        <div style="border-style: solid;width: 80%;padding: 10px">
            <div class="d-flex">
                <h5>Customer:</h5>
                <span>{{is_null($data->getCustomer)?'-':$data->getCustomer->name}}</span>
            </div>

            <h6>Alamat:</h6>
            <span>{{is_null($data->getCustomer)?'-':$data->getCustomer->address}}</span>
        </div>
    </div>
    <div class="col-xs-6">
        <table>
            <tr>
                <td>No.Invoice </td>
                <td>:  </td>
                <td>{{$data->code}}</td>
            </tr>
            <tr>
                <td>Tanggal </td>
                <td>:  </td>
                <td>{{date("d-m-Y", strtotime($data->created_at))}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:  </td>
                <td><label style="background-color: rgb(0, 195, 230);margin-top: 10px " class="badge">{{($data->is_debet==0)?'Lunas':'Kredit'}}</label></td>
            </tr>
            <tr>
                <td>Dibuat</td>
                <td>:  </td>
                <td>{{is_null($data->getStaff)?'-':$data->getStaff->fullname}}</td>
            </tr>

        </table>
    </div>
</div>

        @if($data->is_with_retur==1)
            <hr>
            <p align="center">
            <h4 align="center">Detail Order Sebelumnya</h4>
            </p>
            <table class="table table-striped" id="table-data" width="100%" cellspacing="0">
                <thead class="text-bold">
                <tr>
                    <td>No</td>
                    <td>Nama Produk</td>
                    <td>Qty</td>
                    <td style="width: 20%">Harga</td>
                    <td>Satuan</td>
                </tr>
                </thead>
                <tbody>
                @foreach($data->getOldStruck as $key =>$item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->product_name}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>{{rupiah($item->price)}}</td>
                        <td>{{$item->unit_name}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3" ></td>
                    <td>Sub Total</td>
                    <td>{{rupiah($item->sub_total)}}</td>
                </tr>
                <tr>
                    <td colspan="3" ></td>
                    <td class="text-center">Promo/Diskon</td>
                    <td>{{rupiah($data->promo)}}</td>
                </tr>
                <tr>
                    <td colspan="3" ></td>
                    <td class="text-center">Biaya Pengiriman</td>
                    <td>{{rupiah($data->shipping_cost)}}</td>
                </tr>
                <tr>
                    <td colspan="3" ></td>
                    <td class="text-center">Total</td>
                    <td>{{rupiah($data->total+$data->shipping_cost)}}</td>
                </tr>
                </tbody>
            </table>

        @endif
        <hr>

        <p align="center">
            <h4 align="center">{{($data->is_with_retur==1)?'Detail Order Terbaru':'Detail Order'}}</h4>
        </p>
        <table class="table table-striped table-bordered" id="table-data" style="border-collapse: collapse;border: 1px solid black;" width="100%" cellspacing="1">
            <thead>
            <tr>
                <td>No</td>
                <td>Nama Produk</td>
                <td>Satuan</td>
                <td>Qty</td>
                <td style="width: 20%">Harga</td>

            </tr>
            </thead>
            <tbody>
            @foreach($data->getDetail as $key =>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    @if(is_null($item->product_name))
                        <td style="text-align: center;">{{$item->getProduct->name}} {{$item->getProduct->code}}</td>
                    @else
                        <td>{{$item->product_name}}</td>
                    @endif

                    @if ($item->is_multi_unit==1)
                        <td style="text-align: center;">{{$item->multi_quantity}}</td>
                        <td style="text-align: center;">{{$item->unit_name}}</td>
                    @else
                        <td style="text-align: center;">{{$item->quantity}}</td>
                        @if(!is_null($item->unit_name))
                            <td style="text-align: center;">{{$item->unit_name}}</td>

                        @else
                            <td style="text-align: center;">{{(is_null($item->getProduct->getUnit))?'':$item->getProduct->getUnit->name}}</td>
                        @endif

                    @endif
                    <td>{{rupiah($item->price)}}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="3" ></td>
                <td class="text-center">Sub Total</td>
                <td>{{rupiah($item->sub_total)}}</td>
            </tr>
            <tr>
                <td colspan="3" ></td>
                <td class="text-center">Promo/Diskon</td>
                <td>{{rupiah($data->promo)}}</td>
            </tr>
            <tr>
                <td colspan="3" ></td>
                <td class="text-center">Biaya Pengiriman</td>
                <td>{{rupiah($data->shipping_cost)}}</td>
            </tr>
            <tr>
                <td colspan="3" ></td>
                <td class="text-center">Total</td>
                <td>{{rupiah($data->total+$data->shipping_cost)}}</td>
            </tr>
            </tbody>
        </table>
        @if($data->is_debet==1)
            <hr>
            <p align="center">
            <h5 align="center">Riwayat Piutang</h5>
            </p>
                    <table>
                        <tr>
                            <td>Jatuh Tempo</td>
                            <td>:</td>
                            <td>{{$data->ar->due_date}}</td>
                        </tr>
                        <tr>
                            <td>Terbayarkan</td>
                            <td>:</td>
                            <td>{{rupiah($data->ar->paid_nominal)}}</td>
                        </tr>
                        <tr>
                            <td>Sisa Piutang</td>
                            <td>:</td>
                            <td>{{rupiah($data->ar->residual_amount)}}</td>
                        </tr>
                    </table>

            <table class="table table-striped" id="table-data" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <td>No</td>
                    <td>Tanggal Pembayaran</td>
                    <td>Nominal</td>
                </tr>
                </thead>
                <tbody>
                @foreach($data->ar->getDetail as $key =>$item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->paid_date}}</td>
                        <td>{{rupiah($item->paid_nominal)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        @endif
</body>
</html>

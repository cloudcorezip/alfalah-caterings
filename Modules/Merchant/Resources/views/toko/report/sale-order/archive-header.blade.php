<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="dashboard-component info-component">
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 col-8">
                            <span>Jumlah</span>
                            <h4><b>Transaksi</b></h4>

                        </div>
                        <div class="col-md-4 col-4">
                            <img src="{{asset('public/backend')}}/icons/dashboard/total.png" class="img-fluid"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <b><div id="stat-jumlah-transaksi" class="info-number"><img src="{{asset('public/backend/img')}}/loading-horizontal.gif" /></div></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="dashboard-component info-component">
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 col-8">
                            <span>Total</span>
                            <h4><b>Penjualan</b></h4>
                        </div>
                        <div class="col-md-4 col-4">
                            <img src="{{asset('public/backend')}}/icons/dashboard/penjualan.png" class="img-fluid"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <b><div id="stat-total-pembayaran" class="info-number"><img src="{{asset('public/backend/img')}}/loading-horizontal.gif" /></div></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="dashboard-component info-component">
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 col-8">
                            <span>Total</span>
                            <h4><b>Piutang</b></h4>
                        </div>
                        <div class="col-md-4 col-4">
                            <img src="{{asset('public/backend')}}/icons/dashboard/piutang.png" class="img-fluid"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <b><div id="stat-total-kekurangan" class="info-number"><img src="{{asset('public/backend/img/')}}/loading-horizontal.gif" /></div></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

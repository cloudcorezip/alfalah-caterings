<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: Arial, sans-serif; width:100%;">

    <div style="width:30%; float:left;">
        <h4>{{$data->getMerchant->name}}</h4>
        <p>{{$data->getMerchant->address}}</p>
        <p>{{$data->getMerchant->phone_merchant}}</p>
    </div>
    <div style="width:40%; float:left;">
        <h2 style="text-align:center;">Laporan Pembayaran</h2>
    </div>
    <div style="width:30%; float:right;">
    </div>
    <br style="clear:both;">

    <div style="width:100%; border:1px solid black; margin:0 auto; padding:20px 5px; float:left;">
        <div style="width:40%; float:left;">
            <table>
                <tr>
                    <td>Customer</td>
                    <td>:</td>
                    <td>@if(is_null($data->getCustomer)) 
                            Pelanggan Umum
                        @else 
                            {{$data->getCustomer->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>@if(is_null($data->getCustomer)) 
                            -
                        @else 
                            {{$data->getCustomer->address}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Kontak</td>
                    <td>:</td>
                    <td>@if(is_null($data->getCustomer)) 
                            -
                        @else 
                            {{$data->getCustomer->phone_number}}
                        @endif
                    </td>
                </tr>
            </table>
        </div>
        <div style="width:60%; float:right;">
            <table style="width:80%; float:right;">

                @if($data->is_debet === 1)
                <tr>
                    <td>Jatuh Tempo</td>
                    <td width="20px">:</td>
                    <td width="60%" style="text-align:right;">{{\Carbon\Carbon::parse($data->ar->due_date)->isoFormat(' D MMMM Y')}}</td>
                </tr>
                <tr>
                    <td>Kode Transaksi</td>
                    <td width="20px">:</td>
                    <td width="60%" style="text-align:right;">{{$data->code}}</td>
                </tr>
                <tr>
                    <td>Status Transaksi</td>
                    <td width="20px">:</td>
                    <td width="60%" style="text-align:right">{{$data->getTransactionStatus->description}}</td>
                </tr>
                @else
                <tr>
                    <td>Status Transaksi</td>
                    <td width="20px">:</td>
                    <td width="60%" style="text-align:right;">{{$data->getTransactionStatus->description}}</td>
                </tr>
                @endif

                
            </table>
           
        </div>
        <br style="clear:both;">
        <br>
        <div style="width:100%;">
            @if($data->is_debet === 1)
            <table style="width:100%;" border="1" cellspacing="0" cellpadding="5">
                <tr>
                    <th style="text-align:center; font-weight:bold;">Kode</th>
                    <th style="text-align:center; font-weight:bold;">Metode Pembayaran</th>
                    <th style="text-align:center; font-weight:bold;">Waktu Pembayaran</th>
                    <th style="text-align:center; font-weight:bold;">Jumlah Pembayaran</th>

                </tr>

                @if(count($data->ar->getDetail) > 0)
                    @foreach($data->ar->getDetail as $key => $item)
                    <tr>
                        <td style="text-align:center;">{{$item->ar_detail_code}}</td>
                        <td style="text-align:center;">{{($item->getCoa->name=='Kas')?'Tunai':$item->getCoa->name}}</td>
                        <td style="text-align:center">{{\Carbon\Carbon::parse($item->paid_date)->isoFormat(' D MMMM Y  HH:mm:ss')}}</td>
                        <td style="text-align:center">{{rupiah($item->paid_nominal)}}</td>
                    </tr>
                    @endforeach;
                @else
                    <tr>
                        <td colspan="4" style="text-align:center">Belum melakukan pembayaran untuk tagihan ini</td>
                    </tr>
                @endif

                
            </table>
            @else
            <table style="width:100%;" border="1" cellspacing="0" cellpadding="5">
                <tr>
                    <th style="text-align:center; font-weight:bold;">Kode Transaksi</th>
                    <th style="text-align:center;font-weight:bold">Waktu Pembayaran</th>
                    <th>Jumlah Pembayaran</th>

                </tr>
                <tr>
                    <td style="text-align:center">{{$data->code}}</td>
                    <td style="text-align:center;"> {{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')}}</td>
                    <td style="text-align:center;">{{rupiah($data->total)}}</td>
                </tr>
            </table>
            @endif
        </div>
        
        <br>
        <div style="width:60%; float:left;">
        </div>
        <table style="width:40%; float:right;">
            
            @if($data->is_debet==0)
                <tr>
                    <td>Total Tagihan</td>
                    <td>:</td>
                    <td>{{rupiah($data->total - $data->getRetur->where('is_deleted',0)->where('reason_id','!=',1)->sum('total'))}}</td>
                </tr>
                
                <tr>
                    <td>Total Pembayaran</td>
                    <td>:</td>
                    <td>{{rupiah($data->total - $data->getRetur->where('is_deleted',0)->where('reason_id','!=',1)->sum('total'))}}</td>
                </tr>
                <tr>
                    <td>Total Kekurangan Pembayaran</td>
                    <td>:</td>
                    <td>{{rupiah(0)}}</td>
                </tr>
            @else
                <tr>
                    <td>Total Tagihan</td>
                    <td>:</td>
                    <td>{{rupiah($data->total - $data->getRetur->where('is_deleted',0)->where('reason_id','!=',1)->sum('total'))}}</td>
                </tr>
                <tr>
                    <td>Total Pembayaran</td>
                    <td>:</td>
                    <td>{{rupiah($data->ar->paid_nominal)}}</td>
                </tr>
                <tr>
                    <td>Total Kekurangan Pembayaran</td>
                    <td>:</td>
                    <td>{{rupiah($data->ar->residual_amount)}}</td>
                </tr>
            @endif
        </table>

        <div style="clear:both;"></div>

    </div>

    <div style="clear:both;"></div>


</body>
</html>
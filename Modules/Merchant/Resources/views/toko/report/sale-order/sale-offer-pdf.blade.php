<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: Arial, sans-serif; width:100%;margin-right:auto;margin-left:auto;">
<div style="width:100%;margin:0 auto;">
    <table style="width:100%;position:relative;page-break-inside:avoid;">
        <tr>
            <td style="width:50%;">
                <h4>PENAWARAN PENJUALAN</h4>
            </td>
            <td style="width:50%;">
                <h4 style="text-align:right;"><?php echo $data->getMerchant->name; ?></h4>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border: 2px solid #FF8929">
                <br>
            </td>
        </tr>
        <tr>
            <td style="width:60%;vertical-align:baseline;">
                <table style="width:100%;" cellspacing="0">
                    <tr style="font-size: 13px">
                        <td>Kode Transaksi</td>
                        <td>:</td>
                        <td>{{is_null($data->second_code)?str_replace('S','SQ',$data->code):$data->second_code}}</td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>{{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')}} {{getTimeZoneName($data->timezone)}}</td>
                    </tr>
                </table>
            </td>
            <td style="width:40%; vertical-align:baseline;">
                <table style="width:100%;" cellspacing="0">
                    <tr style="font-size: 13px">
                        <td style="text-align:left;">Pelanggan</td>
                        <td style="text-align:right;">:</td>
                        <td style="text-align:right;">
                            {{is_null($data->getCustomer)?'Pelanggan Umum':$data->getCustomer->name}}
                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="text-align:left;">Alamat</td>
                        <td style="text-align:right;">:</td>
                        <td style="text-align:right;">
                            {{is_null($data->getCustomer)?'-':$data->getCustomer->address}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width:100%;" border="0" cellspacing="0" cellpadding="5">

                    <tr style="background:#FF8929;font-size: 13px">
                        <th style="text-align:left; font-weight:bold;color:white;padding:15px; ">No</th>
                        <th style="text-align:left; font-weight:bold;color:white;padding:15px; ">Produk</th>
                        <th style="text-align:center; font-weight:bold;color:white; ">Jumlah</th>
                        <th style="text-align:center; font-weight:bold;color:white; ">Satuan</th>
                        <th style="text-align:right; font-weight:bold;color:white; ">Harga</th>
                        <th style="text-align:right; font-weight:bold;color:white; ">Sub Total</th>
                    </tr>
                    @php
                        $promo_product=0;
                    @endphp
                    @foreach($data->getDetail->where('is_deleted', 0) as $key => $item)
                        <tr
                            @if($key%2==1) style="background:#fff8e1;font-size: 12px" @else style="font-size: 12px" @endif
                        >
                            <td>{{$key+1}}</td>
                            @if(is_null($item->product_name))
                                <td style="text-align: center;">{{$item->getProduct->name}} {{$item->getProduct->code}}</td>
                            @else
                                <td>{{$item->product_name}}</td>
                            @endif
                            @if ($item->is_multi_unit==1)
                                @if(!is_null($item->unit_name))
                                <td style="text-align: center;">{{$item->quantity}}</td>
                                <td style="text-align: center;">{{$item->unit_name}}</td>
                               @else
                                    <td style="text-align: center;">{{$item->multi_quantity}}</td>
                                    <td style="text-align: center;">{{(is_null($item->getProduct->getUnit))?'':$item->getProduct->getUnit->name}}</td>
                                @endif
                            @else
                                <td style="text-align: center;">{{$item->quantity}}</td>
                                @if(!is_null($item->unit_name))
                                    <td style="text-align: center;">{{$item->unit_name}}</td>

                                @else
                                    <td style="text-align: center;">{{(is_null($item->getProduct->getUnit))?'':$item->getProduct->getUnit->name}}</td>
                                @endif

                            @endif
                            @if ($item->is_bonus==0)
                                <td style="text-align: right">{{rupiah($item->price)}}</td>
                                <td style="text-align: right">{{rupiah($item->sub_total)}}</td>
                            @else
                                <td style="text-align: right">Bonus Produk</td>
                                <td style="text-align: right">0</td>
                            @endif

                            @if ($item->multi_quantity!=0)
                                @if($item->price*$item->multi_quantity!=$item->sub_total)
                                    @php
                                        $promo_product+=$item->price*$item->quantity-$item->sub_total;
                                    @endphp
                                @endif
                            @else
                                @if($item->price*$item->quantity!=$item->sub_total)
                                    @php
                                        $promo_product+=$item->price*$item->quantity-$item->sub_total;
                                    @endphp
                                @endif
                            @endif
                        </tr>
                    @endforeach

                    <tr>
                        <td style="border:1px solid #fff;" colspan="6">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:1px solid #fff;" colspan="3"></td>
                        <td style="border:1px solid #fff;" colspan="3">
                            <table style="width:100%;">
                                <tr style="font-size: 13px">
                                    <td style="border:1px solid #fff;"></td>
                                    <td style="text-align:right;border:1px solid #fff;">Potongan Harga :</td>
                                    <td style="text-align:right;border:1px solid #fff;">{{rupiah((is_null($data->promo_offer))?0+$promo_product:$data->promo_offer+$promo_product)}}</td>
                                </tr>

                                <tr style="font-size: 13px">
                                    <td style="border:1px solid #fff;"></td>
                                    <td style="text-align:right;border:1px solid #fff;">Pajak :</td>
                                    <td style="text-align:right;border:1px solid #fff;">{{rupiah((is_null($data->tax_offer))?0:$data->tax_offer)}}</td>
                                </tr>
{{--                                <tr style="font-size: 13px">--}}
{{--                                    <td style="border:1px solid #fff;"></td>--}}
{{--                                    <td style="text-align:right;border:1px solid #fff;">Biaya Pengiriman :</td>--}}
{{--                                    <td style="text-align:right;border:1px solid #fff;">{{rupiah($data->shipping_cost)}}</td>--}}
{{--                                </tr>--}}
                                @if ($data->md_transaction_type_id==4)
                                    <tr style="font-size: 13px">
                                        <td style="border:1px solid #fff;"></td>
                                        <td style="text-align:right;border:1px solid #fff;">Total Harga :</td>
                                        <td style="text-align:right;border:1px solid #fff;">{{rupiah($data->total_offer-$data->admin_fee)}}</td>
                                    </tr>
                                @else
                                    <tr style="font-size: 13px">
                                        <td style="border:1px solid #fff;"></td>
                                        <td style="text-align:right;border:1px solid #fff;">Total Harga :</td>
                                        <td style="text-align:right;border:1px solid #fff;">{{rupiah($data->total_offer)}}</td>
                                    </tr>
                                @endif

                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td style="vertical-align:baseline;" colspan="2">
                            @if(is_null($data->getCustomer))
                                <h4 style="text-align:center;">Pelanggan Umum</h4>
                            @else
                                <h4 style="text-align:center;">{{$data->getCustomer->name}}</h4>
                            @endif
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="border-bottom:1px solid black; width:30%; margin:0 auto;"></div>
                        </td>
                        <td colspan="1"></td>
                        <td style="vertical-align:baseline;" colspan="3">
                            <h4 style="text-align:center;">{{$data->getMerchant->name}}</h4>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="border-bottom:1px solid black; width:30%; margin:0 auto;"></div>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>

</div>

</body>
</html>

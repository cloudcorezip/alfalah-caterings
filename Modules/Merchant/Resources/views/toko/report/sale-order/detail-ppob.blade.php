@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">Detail Penjualan PPOB</h4>
                <span>Berikut merupakan informasi lengkap terkait detail penjualan</span>

            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @if(Session::get('error-export'))
            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                {{Session::get('error-export')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <ul class="nav nav-tabs tabs-trans" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link @if($page=='transaksi')active @endif" id="home-tab"  href="{{route('merchant.toko.sale-order.detail',['id'=>$data->id])}}?page=transaksi" role="tab" aria-controls="transaksi" aria-selected="true">Transaksi</a>
            </li>

            {{-- <li class="nav-item">
                <a class="nav-link @if($page=='retur') active @endif" id="home-tab"  href="{{route('merchant.toko.sale-order.detail',['id'=>$data->id])}}?page=retur" role="tab" aria-controls="retur" aria-selected="true">Retur Penjualan</a>
            </li> --}}
        </ul>
        <div class="row">
            <div class="col-md-9">
                @if($page == 'transaksi')
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-2">
                                    <h6 class="font-weight-bold">Nomor Pelanggan</h6>
                                </dt>
                                <dd class="col-sm-10">
                                    <span>{{$data->number_id}}</span>
                                </dd>
                            </dl>
                            <h6 class="font-weight-bold mb-4">Item Produk</h6>
                            <div class="table-responsive">
                                <table class="table no-margin text-center table-custom">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Produk</th>
                                            <th>Kategori Produk</th>
                                            <th>Harga</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                <td class="center">1</td>
                                                <td>{{$data->trans_name}}</td>
                                                <td>
                                                    @if($data->sub_group=='PULSA' || $data->sub_group=='PAKET_DATA')
                                                    {{ucfirst(strtolower($data->sub_group)).' '.ucfirst(strtolower($data->parent_group))}}
                                                    @else 
                                                    {{ucfirst(strtolower($data->parent_group)).' '.ucfirst(strtolower($data->sub_group))}}
                                                    @endif
                                                </td>
                                                <td>{{rupiah($data->selling_price+$data->discount_price)}}</td>
                                            </tr>
                                        <tr>
                                            <td class="font-weight-bold" style="border-bottom:none!important;" colspan="3">TOTAL</td>
                                            <td class="font-weight-bold" style="border-bottom:none!important;">{{rupiah($data->selling_price+$data->discount_price)}}</td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-8">
                                    <table class="table-note">
                                        <tr>
                                            <td class="text-note">Catatan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">-</td>
                                        </tr>
                                        <tr>
                                            <td class="text-note">Status Penjualan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">
                                                @if($data->is_deleted==1)
                                                    <div class="badge badge-danger">Terhapus</div>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        {{-- <tr>
                                            <td class="text-note">Bukti Penjualan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">-</td>
                                        </tr> --}}
                                        <tr>
                                            <td class="text-note">Tautan Pembayaran</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td>
                                                -
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="text-note">Pilihan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td>
                                                
                                            </td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-4">
                                    <div class="table-responsive">
                                        <table class="table table-custom">
                                            <tr>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Potongan Harga</td>
                                                <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah($data->discount_price)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Biaya Admin (Payment Digital)</td>
                                                <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah($data->admin_fee)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Total Penjualan</td>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah($data->selling_price)}}</td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                @endif
            </div>

            <div class="col-md-3">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Kode Transaksi :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->code}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Waktu Transaksi :</span>
                            </dd>
                            <dt>
                                <h5>{{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd class="trans-info">
                                <span>Status Transaksi :</span>
                            </dd>
                            <dt>
                                <h5 class="text-tosca">{{(is_null($data->status))?'PENDING':$data->status}}</h5>
                            </dt>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function hapusPembayaran(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.sale-order.delete-payment')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        function voidTransaction(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "<div class='alert alert-warning'>Menghapus data penjualan akan mengembalikan stok dan menghapus pencatatan jurnal.Apakah anda yakin menghapus data penjualan?</div>", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.sale-order.void')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }
        function hapusRetur(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.sale-order.delete-retur')}}", data, "#modal-output");
            })
        }

        const exportFakturExcel = (id) => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                const data = new FormData();
                data.append('id', id);

                ajaxTransfer("{{route('merchant.toko.sale-order.export-faktur-excel')}}", data, '#modal-output');
            });

        }
    </script>
@endsection

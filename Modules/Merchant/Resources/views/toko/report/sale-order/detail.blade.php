@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Penjualan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.sale-order.data')}}">Faktur Penjualan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="#">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan informasi lengkap terkait detail penjualan</span>

            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @if(Session::get('error-export'))
            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                {{Session::get('error-export')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <ul class="nav nav-tabs tabs-trans" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link @if($page=='transaksi')active @endif" id="home-tab"  href="{{route('merchant.toko.sale-order.detail',['id'=>$data->id])}}?page=transaksi" role="tab" aria-controls="transaksi" aria-selected="true">Transaksi</a>
            </li>
            @if($data->is_debet==1)
                <li class="nav-item">
                    <a class="nav-link @if($page=='pembayaran')active @endif " id="home-tab"  href="{{route('merchant.toko.sale-order.detail',['id'=>$data->id])}}?page=pembayaran" role="tab" aria-controls="pembayaran" aria-selected="true">Pembayaran</a>
                </li>
            @endif

        </ul>
        <div class="row">
            <div class="col-md-9">
                @if($page == 'transaksi')
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-4">
                                    <h6 class="font-weight-bold">Cabang</h6>
                                </dt>
                                <dd class="col-sm-8">
                                    <span>{{$data->getMerchant->name}}</span>
                                </dd>
                                <dt class="col-sm-4">
                                    <h6 class="font-weight-bold">Pelanggan</h6>
                                </dt>
                                <dd class="col-sm-8">
                                    <span>{{(is_null($data->getCustomer))?'-':$data->getCustomer->name}}</span>
                                </dd>
                                <dt class="col-sm-4">
                                    <h6 class="font-weight-bold">Metode Pengiriman</h6>
                                </dt>
                                <dd class="col-sm-8">
                                    <span>{{is_null($data->getShippingCategory)?'-':$data->getShippingCategory->name}}</span>
                                </dd>
                            </dl>
                            <h6 class="font-weight-bold mb-4">Item Penjualan</h6>
                            <div class="table-responsive">
                                <table class="table no-margin text-center table-custom">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Produk</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga Satuan</th>
                                        <th>Sub Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data->getDetail->where('is_deleted',0) as $key =>$item)
                                        <tr>
                                            <td class="center">{{$key+1}}</td>
                                            @if(is_null($item->product_name))
                                                <td class="center">{{$item->getProduct->name}} {{$item->getProduct->code}}</td>
                                            @else
                                                <td class="center">{{$item->product_name}}</td>
                                            @endif
                                            @if ($item->is_multi_unit==1)
                                                @if(is_null($item->unit_name))
                                                    <td class="text-center">{{$item->multi_quantity}}</td>
                                                    <td class="center">{{(is_null($item->getProduct->getUnit))?'':$item->getProduct->getUnit->name}}</td>
                                                @else
                                                    <td class="center">{{$item->quantity}}</td>
                                                    <td class="center">{{$item->unit_name}}</td>
                                                @endif
                                            @else
                                                <td class="center">{{$item->quantity}}</td>
                                                @if(!is_null($item->unit_name))
                                                    <td class="center">{{$item->unit_name}}</td>

                                                @else
                                                    <td class="center">{{(is_null($item->getProduct->getUnit))?'':$item->getProduct->getUnit->name}}</td>

                                                @endif

                                            @endif
                                            @if ($item->is_bonus==0)
                                                <td class="center">{{rupiah($item->price)}}</td>
                                                <td class="center">{{rupiah($item->price*$item->quantity)}}</td>
                                            @else
                                                <td class="center">Bonus Produk</td>
                                                <td class="center">0</td>
                                            @endif

                                            @if ($item->multi_quantity!=0)
                                                @if($item->price*$item->multi_quantity!=$item->sub_total)
                                                    @php
                                                        $promo_product+=$item->price*$item->quantity-$item->sub_total;
                                                    @endphp
                                                @endif
                                            @else
                                                @if($item->price*$item->quantity!=$item->sub_total)
                                                    @php
                                                        $promo_product+=$item->price*$item->quantity-$item->sub_total;
                                                    @endphp
                                                @endif
                                            @endif

                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="font-weight-bold" style="border-bottom:none!important;" colspan="5">TOTAL</td>
                                        <td class="font-weight-bold" style="border-bottom:none!important;">{{rupiah($data->getDetail->where('is_bonus',0)->where('is_deleted',0)->sum('sub_total'))}}</td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-8">
                                    <table class="table-note">
                                        <tr>
                                            <td class="text-note font-weight-bold">Catatan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            @if(!is_null($data->note_order))
                                                <td class="text-note">{{$data->note_order}}</td>
                                            @else
                                                <td class="text-note">-</td>
                                            @endif                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Status Penjualan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">
                                                @if($data->is_deleted==1)
                                                    <div class="badge badge-danger">Terhapus</div>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Tautan Pembayaran</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td>
                                                @if(env('APP_ENV')!='production')
                                                    @if($data->md_sc_transaction_status_id==1 && $data->is_payment_gateway==1)
                                                        <a href="{{route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($data->id),
                                ])}}" class="btn btn-outline-warning btn-xs text-warning btn-rounded">
                                                            <i class="fa fa-link mr-1"></i> Klik Disini
                                                        </a>
                                                    @else
                                                        -
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @if($data->is_from_form_order == 1)
                                            <tr>
                                                <td class="text-note font-weight-bold">Detail Form Order</td>
                                                <td class="text-note text-center" width="40px">:</td>
                                                <td>
                                                    <a target="_blank" href="{{route('form-order.detail', ['id'=>$data->id])}}" class="btn btn-outline-warning btn-xs text-warning btn-rounded">
                                                        <i class="fa fa-link mr-1"></i>Klik Disini
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td class="text-note font-weight-bold">Pilihan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td>
                                                @if($data->is_deleted==0)
                                                    <a title="" onclick="voidTransaction({{$data->id}})" class="btn btn-xs btn-delete-xs btn-rounded text-danger" data-original-title="Hapus Data Penjualan"><i class="fa fa-times-circle mr-1"></i>
                                                        Hapus Data Penjualan
                                                    </a>
                                                @endif
                                                <button type="button" class="btn btn-success-light-xs btn-xs text-primary btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-download mr-1"></i>
                                                    Cetak
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="{{route('merchant.toko.sale-order.export-data-faktur',['id'=>$data->id])}}">PDF</a>
                                                    <a class="dropdown-item" onclick="exportData({{$data->id}})">Excel</a>
                                                </div>
                                            </td>
                                        </tr>


                                    </table>
                                </div>
                                <div class="col-4">
                                    <div class="table-responsive">
                                        <table class="table table-custom">
                                            <tr>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Potongan Harga</td>
                                                <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah((is_null($data->promo))?0+$promo_product:$data->promo+$promo_product)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Pajak</td>
                                                <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah((is_null($data->tax))?0:$data->tax)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Biaya Pengiriman</td>
                                                <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah($data->shipping_cost)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Biaya Admin (Payment Digital)</td>
                                                <td style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah($data->admin_fee)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;">Total Tagihan</td>
                                                <td class="font-weight-bold" style="border-bottom:none!important;padding-bottom:0!important;" class="text-right">{{rupiah($data->total+$data->admin_fee)}}</td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                @endif

                @if($page == 'pembayaran')
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            @if($data->is_debet==1)
                                @if ($data->ar->residual_amount<0)
                                    <span class="text-danger font-weight-bold text-center">*Silahkan update pembayaran anda agar tidak minus dikarenakan telah adanya retur mengurangi hutang.</span>
                                @endif
                            @endif
                                @if($data->is_debet==1 && $data->ar->residual_amount!=0 && $data->is_deleted==0)

                                    <a onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.transaction.sale-order.add-payment')}}" data="ar_id={{$data->ar->id}}" class="btn btn-success mb-4" data-original-title="" title="" style="color: white"><i class="fa fa-money-bill mr-1"></i> Setor Pembayaran</a>
                                @endif
                            @if($data->is_debet==1)
                                <div class="table-responsive">
                                    <table class="table table-custom text-center">
                                        <thead>
                                        <tr>
                                            <th>Metode Pembayaran</th>
                                            <th>Jumlah</th>
                                            <th>Jumlah Diterima</th>
                                            <th>Status</th>
                                            <th>Waktu</th>
                                            <th>Opsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data->ar->getDetail as $key =>$item)
                                            <tr>
                                                <td>{{($item->getCoa->name=='Kas' || $item->getCoa->name=='Kas Kasir Outlet')?'Tunai':$item->getCoa->name}}</td>
                                                <td class="center"> {{rupiah($item->paid_nominal+$item->admin_fee)}}</td>
                                                <td class="center"> {{rupiah($item->paid_nominal)}}</td>

                                                <td class="center">
                                                    @if($item->md_transaction_status_id==1)
                                                        <label class="badge badge-warning" style="color: white">Menunggu Pembayaran</label>
                                                        <a href="{{route('merchant.toko.transaction.sale-order-ar.checkout', [
                            'key' => encrypt($item->id),

                        ])}}" class="btn btn-outline-danger btn-xs text-danger btn-rounded">
                                                            <i class="fa fa-link mr-1"></i> Tautan Pembayaran
                                                        </a>
                                                    @else
                                                        <label class="badge badge-success">Terbayar</label>

                                                    @endif

                                                </td>
                                                <td class="center">
                                                    @if($item->md_transaction_status_id==1)
                                                        -
                                                    @else
                                                        {{\Carbon\Carbon::parse($item->paid_date)->isoFormat(' D MMMM Y  HH:mm:ss')." ".getTimeZoneName($item->timezone)}}

                                                    @endif
                                                </td>

                                                <td class="center">
                                                    @if($data->is_deleted==0)
                                                        <a title="Hapus Pembayaran" onclick="hapusPembayaran({{$item->id}})" class="btn btn-danger btn-xs text-danger btn-rounded" data-original-title="Hapus Pembayaran"><i class="fa fa-times-circle"></i></a>
                                                        <a title="Cetak Tagihan" href="{{route('merchant.toko.sale-order.export-data-bill',['id' => $data->id, 'arId' => $item->id])}}" class="btn btn-success btn-xs text-white btn-rounded"><i class="fa fa-download text-white"></i></a>

                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            @endif
                            <div class="row">
                                <div class="col-lg-6">
                                    @if($data->is_debet==1)
                                        @if ($data->ar->residual_amount<0)
                                            <span class="text-danger font-weight-bold">*Silahkan update pembayaran anda agar tidak minus dikarenakan telah adanya retur mengurangi hutang.</span>
                                        @endif
                                    @endif
                                </div>
                                <div class="col-lg-6">
                                    <table class="table-note mt-2">
                                        <tbody>
                                        @if($data->is_debet==1)
                                            <tr>
                                                <td style="width: 275px;" class="left font-weight-bold">Total Tagihan Pembayaran</td>
                                                <td class="text-right">{{rupiah($data->total-$data->getRetur->where('is_deleted',0)->where('reason_id','!=',1)->sum('total'))}}</td>
                                            </tr>
                                            <tr>
                                                <td class="left font-weight-bold">Total Pembayaran Diterima</td>
                                                <td class="text-right">{{rupiah($data->ar->paid_nominal)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="left font-weight-bold">Total Kekurangan Pembayaran </td>
                                                @if ($data->ar->residual_amount<0)
                                                    <td class="text-danger">{{rupiah($data->ar->residual_amount)}}</td>
                                                @else
                                                    <td class="text-right">{{rupiah($data->ar->residual_amount)}}</td>
                                                @endif
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                @endif

            </div>

            <div class="col-md-3">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Kode Transaksi </span>
                            </dd>
                            <dt>
                                <h5>{{(is_null($data->second_code))?$data->code:$data->second_code}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Waktu Transaksi</span>
                            </dd>
                            <dt>
                                <h5>{{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')." ".getTimeZoneName($data->timezone)}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd class="trans-info">
                                <span>Status Transaksi </span>
                            </dd>
                            <dt>
                                <h5 class="text-tosca">{{$data->getTransactionStatus->description}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Jatuh Tempo Pembayaran</span>
                            </dd>
                            <dt>
                                @if($data->is_debet==0)
                                    <h5>-</h5>
                                @else
                                    <h5>{{\Carbon\Carbon::parse($data->ar->due_date)->isoFormat(' D MMMM Y  HH:mm:ss')." ".getTimeZoneName($data->timezone)}}</h5>
                                @endif
                            </dt>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function hapusPembayaran(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.sale-order.delete-payment')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        function voidTransaction(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "<div class='alert alert-warning'>Menghapus data penjualan akan mengembalikan stok dan menghapus pencatatan jurnal.Apakah anda yakin menghapus data penjualan?</div>", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.sale-order.void')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        const exportData = (id) => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                const data = new FormData();
                data.append('id', id);

                ajaxTransfer("{{route('merchant.toko.sale-order.export-faktur-excel')}}", data, '#modal-output');
            });

        }
    </script>
@endsection

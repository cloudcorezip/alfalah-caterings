@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock-opname.data')}}">Stok Opname</a>
                        </li>
                    </ol>
                </nav>
                <span>Daftar dan penghitungan stok produk yang tersedia dalam gudang atau persediaan</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-6">
                        {!! $add !!}
                    </div>
                </div>
                <div id="output-sale-order">
                    @include('merchant::toko.report.stock-opname.list')
                </div>
            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>
    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold mt-3 text-dark">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-stock-opname" class="px-30" onsubmit="return false">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$start_date}}" style="margin-bottom: 3px;background: white" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="end_date" value="{{$end_date}}" style="background: white" name="end_date">

                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <select name="md_merchant_id" id="branch" class="form-control form-control" onchange="getWarehouse()">
                        <option value="-1">Semua Cabang</option>
                        @foreach(get_cabang() as $key =>$item)
                            <option value="{{$item->id}}">{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 mb-3">
                    <select name="inv_warehouse_id" id="warehouse" class="form-control form-control">
                        <option value="-1">Semua Gudang</option>

                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3 mt-3">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')

    <script type="text/javascript">

        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data dan menghapus atau mengembalikan stok ke persediaan ?", function () {
                ajaxTransfer("{{route('merchant.toko.stock-opname.delete')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        }

    </script>
    <script>
        function exportData() {
            var info = $('#table-data-r-stock-opname_info').html().split('dari');
            if (info.length < 2) {
                return false;
            }

            var dataCount = parseInt(info[1].replace('entri', '').replace(',', '').replace(' ', ''));
            var seconds = parseFloat(dataCount / 66).toFixed(3);
            var estimation = '';

            if (seconds < 60) {
                estimation = seconds + ' detik';
            } else {
                var minute = parseInt(seconds / 60);
                seconds = seconds % 60;
                estimation = minute + ' menit ' + seconds + ' detik';
            }

            modalConfirm('Export Data', 'Proses export data sejumlah ' + dataCount + ' data membutuhkan estimasi waktu ' + estimation + '. Lanjutkan proses export data?', function () {
                var data = getFormData('form-filter-stock-opname');
                data.append('start_date',$('#start_date').val());
                data.append('end_date',$('#end_date').val());
                ajaxTransfer("{{route('merchant.toko.stock-opname.export-data')}}", data, '#modal-output');
            });

        }

        $(document).ready(function () {
            $('#branch').select2();
            $('#warehouse').select2();

            $('#form-filter-stock-opname').submit(function () {
                var data = getFormData('form-filter-stock-opname');
                data.append('start_date',$('#start_date').val());
                data.append('end_date',$('#end_date').val());
                ajaxTransfer("{{route('merchant.toko.stock-opname.reload-data')}}", data, '#output-sale-order');
            });
        });


        function getWarehouse()
        {
            var branch=$("#branch").val()

            $('#warehouse')
                .find('option')
                .remove()
                .end()
                .append('<option value="-1">Semua Gudang</option>')
                .val('-1')

            $("#warehouse").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.stock-adjustment.get-warehouse')}}?merchant_id="+branch,
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

        }

    </script>
@endsection

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock-opname.data')}}">Stok Opname</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan informasi lengkap terkait detail stok opname yang kamu lakukan</span>

            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        @if(Session::get('error-export'))
            <div class="alert alert-warning text-center" role="alert">
                {{Session::get('error-export')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <ul class="nav nav-tabs tabs-trans" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" role="tab" aria-controls="transaksi" aria-selected="true">Detail Stok  Opname</a>
            </li>
        </ul>
        <div class="row">
            <div class="col-md-9">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-4">
                                <h6 class="font-weight-bold">Cabang</h6>
                            </dt>
                            <dd class="col-sm-8">
                                <span>{{$data->getMerchant->name}}</span>
                            </dd>
                            <dt class="col-sm-4">
                                <h6 class="font-weight-bold">Gudang</h6>
                            </dt>
                            <dd class="col-sm-8">
                                <span>{{$data->getWarehouse->name}}</span>
                            </dd>

                        </dl>
                        <h6 class="font-weight-bold mb-4">Item Stok Opname</h6>
                        <div class="table-responsive">
                            <table class="table no-margin text-center table-custom">
                                <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Produk</td>
                                    <td>Satuan</td>
                                    <td>Stok Di Aplikasi</td>
                                    <td>Stok Fisik</td>
                                    <td>Selisih</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data->getDetail as $key =>$item)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        @if(is_null($item->product_name))
                                            <td>{{$item->getProduct->name.' '.$item->getProduct->code}}</td>
                                        @else
                                            <td>{{$item->product_name}}</td>

                                        @endif
                                        @if(is_null($item->unit_name))
                                        <td>{{(is_null($item->getProduct->getUnit)?'':$item->getProduct->getUnit->name)}}</td>
                                         @else
                                          <td>{{$item->unit_name}}</td>
                                         @endif
                                        <td>{{$item->record_stock}}</td>
                                        <td>{{$item->physical_count}}</td>
                                        <td>{{$item->difference_amount}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>

            <div class="col-md-3">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Kode Stok Opname :</span>
                            </dd>
                            <dt>
                                <h5>{{(!is_null($data->second_code))?$data->second_code:$data->code}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Waktu Stok Opname :</span>
                            </dd>
                            <dt>
                                <h5>{{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd class="trans-info">
                                <span>Nama Pencatat :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->getUserCreated->fullname}}</h5>

                            </dt>
                        </dl>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection

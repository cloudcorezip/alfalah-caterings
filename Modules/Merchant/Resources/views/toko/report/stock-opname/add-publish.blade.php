@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock-opname.data')}}">Stok Opname</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Tambahkan stok opname sesuai dengan isian dibawah ini dengan baik dan benar</span>

            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="results"></div>
                <form id="form-konten" onsubmit="return false">
                    <div class="row">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>false,
'function_onchange'=>''

])
                        <div class="col-md-6">

                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Kode Stok Opname</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="second_code" value="{{is_null($data->second_code)?$data->code:$data->second_code}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Gudang</label>
                                <label class="col-sm-7 col-form-label">{{$data->getWarehouse->name}}</label>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Tanggal Stok Opname</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control form-control-sm trans_time" name="created_at_by" value="{{$data->created_at_by}}" placeholder="Waktu Transaksi" @if(!is_null($data->id)) disabled @endif required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Publish</label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="is_draf" name="is_draf">
                                        <option value="1" @if($data->is_draf==1) selected @endif>Draf</option>
                                        <option value="0" @if($data->is_draf==0) selected @endif>Publish</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row mb-3">
                        <br>
                        <br>
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="20%">Produk</th>
                                    <th width="10%">Stok Sistem</th>
                                    <th width="10%">Stok Fisik</th>
                                    <th width="10%">Satuan</th>
                                    <th width="10%">Selisih</th>
                                    <th width="10%">Cacat</th>
                                    <th width="10%">Expired</th>
                                    <th width="15%">Harga Rerata</th>
                                    <th width="5%">
                                        Opsi
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="tbodyid">
                                @foreach($data->getDetail as $key =>$item)
                                    <tr id="row{{$key+1}}">
                                        <td><input type="hidden" name="sc_product_id[]" id="sc_product_id{{$key+1}}" value="{{$item->sc_product_id}}"><input class="form-control" value="{{$item->product_name}}" disabled></td>
                                        <td><input type="hidden" name=stock[]" id="stock{{$key+1}}" class="form-control stock" value="{{$item->record_stock}}"><input class="form-control" value="{{$item->record_stock}}" disabled></td>
                                        <td>
                                            <input type="text" name="stock_real[]" id="stock_real{{$key+1}}"  class="form-control form-control-sm stock_real" value="{{$item->physical_count}}" onkeypress="return isNumber(event)"  />
                                        </td>
                                        <td>
                                            <input type="text" name="unit_name[]" id="unit_name{{$key+1}}" value="{{$item->unit_name}}" class="form-control form-control-sm unit_name" disabled />
                                        </td>
                                        <td>
                                            <input type="text" name="diff_stock[]" id="diff_stock{{$key+1}}" class="form-control form-control-sm diff" value="{{$item->difference_amount}}" disabled />
                                            </td>
                                        <td>
                                            <input type="text" name="stock_broken[]" id="stock_broken{{$key+1}}" class="form-control form-control-sm stock_broken" value="{{$item->qty_damage}}" onkeypress="return isNumber(event)"  />
                                            </td>
                                        <td>
                                            <input type="text" name="stock_expired[]" id="stock_expired{{$key+1}}" class="form-control form-control-sm stock_expired" value="{{$item->qty_expired}}" onkeypress="return isNumber(event)"  />
                                            </td>
                                        <td>
                                            <input type="text" name="average_price[]" id="average_price{{$key+1}}" class="form-control form-control-sm average_price" value="{{rupiah($item->average_price)}}" />
                                            </td>
                                        <td><button type="button" name="remove" id="{{$key+1}}" class="btn btn-xs btn-delete-xs  btn_remove"><span class="fa fa-trash-alt" style="color: white"></span></button>
                                            </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                            <button type="button" name="add" id="add" class="btn btn-darkblue btn-rounded py-2 px-4">
                                <i class="fa fa-plus mr-2"></i>
                                <span>Tambah Produk</span>
                            </button>
                            <hr>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan :</label>
                                <textarea name="desc" class="form-control form-control-sm" id="" cols="20" rows="5">{{$data->desc}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-5 offset-md-2">

                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <input type="hidden" id="branch" name="md_merchant_id" value="{{$data->md_merchant_id}}">
                            <input type="hidden" id="count_detail" value="{{$data->getDetail->count()}}">
                            <input type="hidden" id="warehouse" name="inv_warehouse_id" value="{{$data->inv_warehouse_id}}">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        $(document).ready(function() {

            $("#is_draf").select2()
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            var i=$("#count_detail").val()+1;
            var productSelectedId = {};

            @foreach($data->getDetail as $j =>$jj)
                productSelectedId[{{$j+1}}]={{$jj->sc_product_id}}
            @endforeach

            $('#add').click(function(){

                i++;
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">' +
                    '<td>' +
                    '<select  name="sc_product_id[]" id="sc_product_id' + i + '" class="form-control form-control-sm" ></select>' +
                    '</td>' +
                    '<td>' +
                    '<input type="number" name="stock[]" class="form-control form-control-sm stock"  id="stock'+i+'" disabled />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="stock_real[]" id="stock_real'+i+'" class="form-control form-control-sm stock_real" value="0" onkeypress="return isNumber(event)"  />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="unit_name[]" id="unit_name'+i+'" class="form-control form-control-sm unit_name" disabled />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="diff_stock[]" id="diff_stock'+i+'" class="form-control form-control-sm diff" disabled />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="stock_broken[]" id="stock_broken'+i+'" class="form-control form-control-sm stock_broken" value="0" onkeypress="return isNumber(event)"  />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="stock_expired[]" id="stock_expired'+i+'" class="form-control form-control-sm stock_expired" value="0" onkeypress="return isNumber(event)"  />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="average_price[]" id="average_price'+i+'" class="form-control form-control-sm average_price" value="0" />' +
                    '</td>' +
                    '<td><button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs  btn_remove"><span class="fa fa-trash-alt" style="color: white"></span></button>' +
                    '</td>' +
                    '</tr>');

                getProductByWarehouse(i)
                $('.average_price').mask('#.##0,00', {reverse: true});
                $("#sc_product_id" + i).on('select2:select', function (e) {
                    var data = e.params.data;
                    var rowId = $(this).closest('tr').attr('id');
                    var real=$("#stock_real"+i).val();
                    $("#stock"+i).val(data.stock)
                    $("#diff_stock"+i).val(data.stock-real)
                    $("#unit_name"+i).val(data.unit_name)
                    let statusDuplicate = false;
                    Object.keys(productSelectedId).forEach(key => {
                        if(productSelectedId[key] === data.id){
                            statusDuplicate=true;
                        }
                    });
                    if(statusDuplicate)
                    {
                        $('#row'+i).remove();
                        delete productSelectedId[i];
                    }else{
                        productSelectedId[i] = data.id;
                    }
                });

            });


            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                delete productSelectedId[button_id];

            });
            $("table").on("change", "input", function() {
                var row = $(this).closest("tr");
                var stock = parseFloat(row.find(".stock").val());
                var stock_real=parseFloat(row.find(".stock_real").val());
                var diff=stock-stock_real;
                row.find(".diff").val(diff)
            });

        });

        function getProductByWarehouse(i)
        {
            var warehouse=$("#warehouse").val()
            var branch = $("#branch").val()

            $("#sc_product_id" + i).select2({
                ajax: {
                    type: "GET",
                    url: "{{route('api.product.stockadjustment')}}?merchant_id="+branch+"&warehouse="+warehouse,
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });


        }

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.stock-opname.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    </script>
@endsection

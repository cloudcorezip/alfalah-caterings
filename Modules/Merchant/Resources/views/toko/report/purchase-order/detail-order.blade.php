@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->

    <!-- DataTales Example -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Pembelian</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.purchase-order.data-order')}}">Pemesanan Pembelian</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="#">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan detail pemesanan pembelian ke suppliermu</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @if(Session::get('error-export'))
            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                {{Session::get('error-export')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <ul class="nav nav-tabs tabs-trans" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link @if($page=='transaksi')active @endif" id="home-tab"  href="{{route('merchant.toko.purchase-order.detail',['id'=>$data->id])}}?page=transaksi" role="tab" aria-controls="transaksi" aria-selected="true">Transaksi</a>
            </li>
        </ul>

        <div class="row">
            <div class="col-md-9">
                @if($page == "transaksi")
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-2">
                                    <h6 class="font-weight-bold">Cabang</h6>
                                </dt>
                                <dd class="col-sm-10">
                                    <span>{{$data->getMerchant->name}}</span>
                                </dd>
                                <dt class="col-sm-2">
                                    <h6 class="font-weight-bold">Supplier</h6>
                                </dt>
                                <dd class="col-sm-10">
                                    <span>{{$data->getSupplier->name}}</span>
                                </dd>
                            </dl>
                            <h6 class="font-weight-bold mb-4">Item Pemesanan Pembelian</h6>
                            <div class="table-responsive">
                                <table class="table table-custom no-margin text-center table-detail-trans">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Produk</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga Satuan</th>
                                        <th>Sub Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data->getDetail->where('is_deleted',0) as $key =>$item)
                                        <tr>
                                            <td class="center">{{$key+1}}</td>
                                            @if(is_null($item->product_name))
                                                <td>{{$item->getProduct->name}} {{$item->getProduct->code}}</td>
                                            @else
                                                <td>{{$item->product_name}}</td>
                                            @endif
                                            @if ($item->is_multi_unit==1)
                                                @if(is_null($item->unit_name))
                                                    <td class="text-center">{{$item->multi_quantity}}</td>
                                                    <td class="center">{{(is_null($item->getProduct->getUnit))?'':$item->getProduct->getUnit->name}}</td>
                                                @else
                                                    <td class="center">{{$item->quantity}}</td>
                                                    <td class="center">{{$item->unit_name}}</td>
                                                @endif
                                            @else
                                                <td class="center">{{$item->quantity}}</td>
                                                @if(!is_null($item->unit_name))
                                                    <td class="center">{{$item->unit_name}}</td>

                                                @else
                                                    <td class="center">{{is_null($item->getProduct->getUnit)?'':$item->getProduct->getUnit->name}}</td>
                                                @endif
                                            @endif
                                            <td class="text-right">{{rupiah($item->price)}}</td>
                                            <td class="text-right">{{rupiah($item->price*$item->quantity)}}</td>

                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="font-weight-bold" colspan="5" style="border-bottom:none!important;">TOTAL</td>
                                        <td class="font-weight-bold text-right" style="border-bottom:none!important;">{{rupiah($data->getDetail->where('is_deleted',0)->sum('sub_total'))}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-8">
                                    <table class="table-note" width="100%">
                                        <tr>
                                            <td class="text-note font-weight-bold">Catatan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            @if(!is_null($data->note))
                                                <td class="text-note">{{$data->note}}</td>
                                            @else
                                                <td class="text-note">-</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Status Pemesanan Pembelian</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">
                                                @if($data->is_deleted==1)
                                                    <div class="badge badge-danger">Terhapus</div>
                                                @endif
                                                @if($data->is_step_close==1)
                                                    <div class="badge badge-danger">closed</div>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Bukti Pemesanan Pembelian</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">-</td>
                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Pilihan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">
                                                @if($data->is_step_close==0)
                                                    @if($data->is_deleted==0)
                                                        <a title="" onclick="voidTransaction({{$data->id}})" class="btn btn-xs btn-delete-xs btn-rounded text-danger" data-original-title="Hapus Retur"><i class="fa fa-times-circle mr-1"></i>
                                                            Hapus Data
                                                        </a>
                                                        <button type="button" class="btn btn-xs btn-edit-xs btn-rounded text-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Tindakan
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="{{route('merchant.toko.transaction.purchase-order.add',['page'=>"pemesanan",'id'=>$data->id])}}">Ubah Transaksi</a>
                                                            <a class="dropdown-item" href="{{route('merchant.toko.transaction.purchase-order.add',['page'=>"penerimaan",'id'=>$data->id])}}">Buat Penerimaan</a>
                                                            <a class="dropdown-item" href="{{route('merchant.toko.transaction.purchase-order.add',['page'=>"faktur",'id'=>$data->id])}}">Buat Faktur</a>
                                                        </div>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Cetak Data</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">
                                                <button type="button" class="btn btn-success btn-xs text-primary btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-download mr-1"></i>
                                                    Cetak Pemesanan
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="{{route('merchant.toko.purchase-order.export-data-faktur-pdf',['id'=>$data->id])}}">PDF</a>
                                                    <a class="dropdown-item" onclick="exportData({{$data->id}})">Excel</a>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-4">
                                    <table class="table-note" width="100%">
                                        <tr>
                                            <td class="font-weight-bold">Diskon</td>
                                            <td class="text-right">{{rupiah($data->promo_order)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold" >Pajak</td>
                                            <td class="text-right">{{rupiah($data->tax_order)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold" >Total Tagihan</td>
                                            <td class="font-weight-bold text-right">{{rupiah($data->total_order)}}</td>
                                        </tr>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Kode Transaksi :</span>
                            </dd>
                            <dt>
                                <h5>{{(!is_null($data->second_code))?$data->second_code:str_replace('P','PO',$data->code)}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Waktu Transaksi</span>
                            </dd>
                            <dt>
                                <h5>{{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')." ".getTimeZoneName($data->timezone)}}</h5>
                            </dt>
                        </dl>

                        <dl class="trans-info">
                            <dd>
                                <span>Ref Penawaran :</span>
                            </dd>
                            <dt>
                                <h5>{{(!is_null($data->ref_code))?$data->ref_code:''}}</h5>
                            </dt>
                        </dl>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>

        function voidTransaction(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "<div class='alert alert-warning'>Menghapus data pembelian akan mengurangi stok dan menghapus pencatatan jurnal.Apakah anda yakin menghapus data pembelian?</div>", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.delete')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        const exportData = (id) => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                const data = new FormData();
                data.append('id', id);

                ajaxTransfer("{{route('merchant.toko.purchase-order.export-faktur-excel')}}", data, '#modal-output');
            });

        }



    </script>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: Arial, sans-serif; width:100%;margin-right:auto;margin-left:auto;">
<div style="width:100%;margin:0 auto;">
    <table style="width:100%;position:relative;page-break-inside:avoid;">
        <tr>
            <td style="width:50%;">
                <h4>PEMESANAN PEMBELIAN</h4>
            </td>
            <td style="width:50%;">
                <h4 style="text-align:right;"><?php echo $data->getMerchant->name; ?></h4>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border: 2px solid #FF8929">
                <br>
            </td>
        </tr>
        <tr>
            <td style="width:60%;vertical-align:baseline;">
                <table style="width:100%;" cellspacing="0">
                    <tr style="font-size: 13px">
                        <td>Kode Transaksi</td>
                        <td>:</td>
                        <td><?php echo (!is_null($data->second_code))?$data->second_code:str_replace('P','PO',$data->code); ?></td>
                    </tr>

                    <tr style="font-size: 13px">
                        <td>Ref. Penawaran</td>
                        <td>:</td>
                        <td><?php echo (!is_null($data->ref_code))?$data->ref_code:'-'; ?></td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td>Tanggal</td>
                        <td>:</td>
                        <td><?php echo \Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss') ?></td>
                    </tr>
                </table>
            </td>
            <td style="width:40%; vertical-align:baseline;">
                <table style="width:100%;" cellspacing="0">
                    <tr style="font-size: 13px">
                        <td style="text-align:right;">Kepada</td>
                        <td style="text-align:right;">:</td>
                        <td style="text-align:right;"><?php echo (is_null($data->getSupplier))?'-':$data->getSupplier->name ?></td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="text-align:right;">No Telp</td>
                        <td style="text-align:right;">:</td>
                        <td style="text-align:right;"><?php echo (is_null($data->getSupplier))?'-':$data->getSupplier->phone_number ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width:100%;" border="0" cellspacing="0" cellpadding="5">
                    <tr style="background:#FF8929;font-size: 13px">
                        <th style="text-align:left; font-weight:bold;color:white;padding:15px; ">Kode</th>
                        <th style="text-align:center; font-weight:bold;color:white; " width="30%">Produk</th>
                        <th style="text-align:center; font-weight:bold;color:white; ">Jumlah</th>
                        <th style="text-align:center; font-weight:bold;color:white; ">Satuan</th>
                        <th style="text-align:right; font-weight:bold;color:white; ">Harga</th>
                        <th style="text-align:right; font-weight:bold;color:white; ">Sub Total</th>
                    </tr>

                    <?php foreach($data->getDetail->where('is_deleted', 0) as $key => $item): ?>
                    <tr
                        @if($key%2==1) style="background:#fff8e1;font-size: 12px" @else style="font-size: 12px" @endif
                    >
                        <td><?php echo $item->getProduct->code;?></td>
                        <td style="text-align:center;"><?php echo $item->getProduct->name; ?></td>
                        @if ($item->is_multi_unit==1)
                            @if(is_null($item->unit_name))
                                <td style="text-align:center;"><?php echo $item->multi_quantity; ?></td>
                                <td style="text-align:center;"><?php echo (is_null($item->unit_name))?$item->getProduct->getUnit->name:$item->unit_name; ?></td>
                            @else
                                <td style="text-align:center;"><?php echo $item->quantity; ?></td>
                                <td style="text-align:center;"><?php echo $item->unit_name; ?></td>
                            @endif
                        @else
                            <td style="text-align:center;"><?php echo $item->quantity; ?></td>
                            <td style="text-align:center;"><?php echo (is_null($item->unit_name))?$item->getProduct->getUnit->name:$item->unit_name; ?></td>
                        @endif
                        <td style="text-align:right;"><?php echo rupiah($item->price); ?></td>
                        <td style="text-align:right;"><?php echo rupiah($item->price * $item->quantity); ?></td>
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td style="border:1px solid #fff;" colspan="6">
                            <br>
                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="border-bottom:1px solid #FF8929;text-align: left" colspan="3">
                            Catatan
                        </td>
                        <td colspan="3" style="border:1px solid #fff">

                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="border:1px solid #FF8929; border-radius: 5px" colspan="3" rowspan="3">{{$data->note}}</td>
                        <td style="text-align:right;border:1px solid #fff;" colspan="2">Diskon :</td>
                        <td style="text-align:right;border:1px solid #fff;"><?php echo rupiah($data->promo_order); ?></td>
                    </tr>

                    <tr style="font-size: 13px">
                        <td style="text-align:right;border:1px solid #fff;"colspan="2">Pajak :</td>
                        <td style="text-align:right;border:1px solid #fff;"><?php echo rupiah($data->tax_order); ?></td>
                    </tr>

                    <tr style="font-size: 13px">
                        <td style="text-align:right;border:1px solid #fff;" colspan="2">Total Harga :</td>
                        <td style="text-align:right;border:1px solid #fff;"><?php echo rupiah($data->total_order); ?></td>
                    </tr>

                    <tr>
                        <td colspan="6">
                            <br>
                            <br>
                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="width:50%;vertical-align:baseline;" colspan="2"></td>
                        <td colspan="2"></td>
                        <td style="width:50%;vertical-align:baseline;" colspan="2">
                            <h4 style="text-align:center;"><?php echo $data->getMerchant->name; ?></h4>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="border-bottom:1px solid black; width:30%; margin:0 auto;"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</div>

</body>
</html>

<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="dashboard-component info-component">
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 col-8">
                            <span>Jumlah</span>
                            <h4><b>Transaksi</b></h4>

                        </div>
                        <div class="col-md-4 col-4">
                            <img src="{{asset('public/backend')}}/icons/dashboard/total.png" class="img-fluid"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <b><div id="transaction" class="info-number"><img src="{{asset('public/backend/img')}}/loading-horizontal.gif" /></div></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="dashboard-component info-component">
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 col-8">
                            <span>Total</span>
                            <h4><b>Pembelian</b></h4>
                        </div>
                        <div class="col-md-4 col-4">
                            <img src="{{asset('public/backend')}}/icons/dashboard/pembelian.png" class="img-fluid"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <b><div id="purchase" class="info-number"><img src="{{asset('public/backend/img')}}/loading-horizontal.gif" /></div></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="dashboard-component info-component">
            <div class="card border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 col-8">
                            <span>Total</span>
                            <h4><b>Hutang</b></h4>
                        </div>
                        <div class="col-md-4 col-4">
                            <img src="{{asset('public/backend')}}/icons/dashboard/hutang.png" class="img-fluid"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <b><div id="hutang" class="info-number"><img src="{{asset('public/backend/img/')}}/loading-horizontal.gif" /></div></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    function loadStatistikHeader() {
        ajaxTransfer("{{route('merchant.toko.get-statistik-header-inv')}}", {}, function(response) {
            response = JSON.parse(response);
            $('#transaction').html(response['jumlah_transaksi']);
            $('#hutang').html(response['total_hutang']);
            $('#purchase').html(response['total_pembelian']);
        }, false, false);
    }
</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->

    <!-- DataTales Example -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Pembelian</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.purchase-order.data')}}">Faktur Pembelian</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="#">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan detail pembelian dari suppliermu</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @if(Session::get('error-export'))
            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                {{Session::get('error-export')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <ul class="nav nav-tabs tabs-trans" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link @if($page=='transaksi')active @endif" id="home-tab"  href="{{route('merchant.toko.purchase-order.detail',['id'=>$data->id])}}?page=transaksi" role="tab" aria-controls="transaksi" aria-selected="true">Transaksi</a>
            </li>
            @if($data->is_debet==1)
            <li class="nav-item">
                <a class="nav-link @if($page=='pembayaran')active @endif " id="home-tab"  href="{{route('merchant.toko.purchase-order.detail',['id'=>$data->id])}}?page=pembayaran" role="tab" aria-controls="pembayaran" aria-selected="true">Pembayaran</a>
            </li>
            @endif

        </ul>

        <div class="row">
            <div class="col-md-9">
                @if($page == "transaksi")
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-2">
                                    <h6 class="font-weight-bold">Cabang</h6>
                                </dt>
                                <dd class="col-sm-10">
                                    <span>{{$data->getMerchant->name}}</span>
                                </dd>
                                <dt class="col-sm-2">
                                    <h6 class="font-weight-bold">Gudang</h6>
                                </dt>
                                <dd class="col-sm-10">
                                    <span>{{$data->getWarehouse->name}}</span>
                                </dd>
                                <dt class="col-sm-2">
                                    <h6 class="font-weight-bold">Supplier</h6>
                                </dt>
                                <dd class="col-sm-10">
                                    <span>{{$data->getSupplier->name}}</span>
                                </dd>
                            </dl>
                            <h6 class="font-weight-bold mb-4">Item Pembelian</h6>
                            <div class="table-responsive">
                                <table class="table table-custom no-margin text-center table-detail-trans">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Produk</th>
                                        <th>Jumlah</th>
                                        <th>Satuan</th>
                                        <th>Harga Satuan</th>
                                        <th>Sub Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data->getDetail->where('is_deleted',0) as $key =>$item)
                                        <tr>
                                            <td class="center">{{$key+1}}</td>
                                            @if(is_null($item->product_name))
                                                <td>{{$item->getProduct->name}} {{$item->getProduct->code}}</td>
                                            @else
                                                <td>{{$item->product_name}}</td>
                                            @endif
                                            @if ($item->is_multi_unit==1)
                                                @if(is_null($item->unit_name))
                                                    <td class="text-center">{{$item->multi_quantity}}</td>
                                                    <td class="center">{{(is_null($item->getProduct->getUnit))?'':$item->getProduct->getUnit->name}}</td>
                                                @else
                                                    <td class="center">{{$item->quantity}}</td>
                                                    <td class="center">{{$item->unit_name}}</td>
                                                @endif
                                            @else
                                                <td class="center">{{$item->quantity}}</td>
                                                @if(!is_null($item->unit_name))
                                                    <td class="center">{{$item->unit_name}}</td>

                                                @else
                                                    <td class="center">{{is_null($item->getProduct->getUnit)?'':$item->getProduct->getUnit->name}}</td>
                                                @endif
                                            @endif
                                            <td class="text-right">{{rupiah($item->price)}}</td>
                                            <td class="text-right">{{rupiah($item->price*$item->quantity)}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="font-weight-bold" colspan="5" style="border-bottom:none!important;">TOTAL</td>
                                        <td class="font-weight-bold text-right" style="border-bottom:none!important;">{{rupiah($data->getDetail->where('is_deleted',0)->sum('sub_total'))}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">
                                <div class="col-8">
                                    <table class="table-note">
                                        <tr>
                                            <td class="text-note font-weight-bold">Catatan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            @if(!is_null($data->note))
                                                <td class="text-note">{{$data->note}}</td>
                                            @else
                                                <td class="text-note">-</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Status Pembelian</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">
                                                @if($data->is_deleted==1)
                                                    <div class="badge badge-danger">Terhapus</div>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Bukti Pembelian</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">-</td>
                                        </tr>
                                        <tr>
                                            <td class="text-note font-weight-bold">Pilihan</td>
                                            <td class="text-note text-center" width="40px">:</td>
                                            <td class="text-note">
                                                @if($data->is_deleted==0)
                                                    <a title="" onclick="voidTransaction({{$data->id}})" class="btn btn-xs btn-delete-xs btn-rounded text-danger" data-original-title="Hapus Retur"><i class="fa fa-times-circle mr-1"></i>
                                                        Hapus Data
                                                    </a>
                                                @endif
                                                        <button type="button" class="btn btn-edit-xs btn-xs text-primary btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-download mr-1"></i>
                                                            Cetak Faktur
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="{{route('merchant.toko.purchase-order.export-data-faktur-pdf',['id'=>$data->id])}}">PDF</a>
                                                            <a class="dropdown-item" onclick="exportData({{$data->id}})">Excel</a>
                                                        </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-4">
                                    <table class="table-note" width="100%">
                                        <tr>
                                            <td class="font-weight-bold">Diskon</td>
                                            <td  class="text-right">{{rupiah($data->discount)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">Pajak</td>
                                            <td  class="text-right">{{rupiah($data->tax)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold" >Total Tagihan</td>
                                            <td class="font-weight-bold text-right">{{rupiah($data->total)}}</td>
                                        </tr>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                @endif

                @if($page == 'pembayaran')
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            @if($data->is_debet==1)
                                @if ($data->ap->residual_amount<0)
                                    <span class="text-danger font-weight-bold text-center">*Silahkan update pembayaran anda agar tidak minus dikarenakan telah adanya retur mengurangi hutang.</span>
                                @endif
                            @endif
                                @if($data->is_debet==1 && $data->ap->is_paid_off==0 && $data->is_deleted==0)
                                    <a onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.transaction.purchase-order.add-payment')}}" data="ap_id={{$data->ap->id}}" class="btn btn-success mb-4" data-original-title="" title="" style="color: white"><i class="fa fa-money-bill mr-1"></i> Setor Pembayaran</a>
                                @endif
                            @if($data->is_debet==1)
                                <div class="table-responsive">
                                    <table class="table table-custom text-center">
                                        <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Metode Pembayaran</th>
                                            <th>Jumlah</th>
                                            <th>Waktu Pembayaran</th>
                                            <th>Opsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($data->ap->getDetail as $key =>$item)
                                            <tr>
                                                <td class="center">{{is_null($item->second_code)?$item->ap_code:$item->second_code}}</td>
                                                <td>{{($item->getCoa->name=='Kas' || $item->getCoa->name=='Kas Kasir Outlet')?'Tunai':$item->getCoa->name}}</td>
                                                <td class="center"> {{rupiah($item->paid_nominal)}}</td>
                                                <td class="center">                                            {{\Carbon\Carbon::parse($item->paid_date)->isoFormat(' D MMMM Y  HH:mm:ss')." ".getTimeZoneName($item->timezone)}}
                                                </td>
                                                <td class="center">
                                                    @if($data->is_deleted==0)
                                                        <a title="Hapus Pembayaran" onclick="hapusPembayaran({{$item->id}})" class="btn btn-delete-xs btn-xs text-danger btn-rounded" data-original-title="Hapus Pembayaran"><i class="fa fa-times-circle"></i></a>
                                                    @endif
                                                        <a href="{{route('merchant.toko.purchase-order.export-data-bill',['id' => $data->id, 'apId' => $item->id])}}" title="Cetak Tagihan" class="btn btn-edit-xs btn-xs text-primary btn-rounded"><i class="fa fa-download mr-1"></i></a>

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-lg-6">
                                </div>
                                <div class="col-lg-6">
                                    <table class="table-note" width="100%">
                                        <tbody>
                                        @if($data->is_debet==0)
                                            <tr>
                                                <td style="width: 275px;" class="left font-weight-bold">Total Tagihan</td>
                                                <td class="text-right">{{rupiah($data->total)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="left font-weight-bold">Total Pembayaran</td>
                                                <td class="text-right">{{rupiah($data->total)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="left font-weight-bold">Total Kekurangan</td>
                                                <td class="text-right">{{rupiah(0)}}</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td style="width: 275px;" class="left font-weight-bold">Total Tagihan</td>
                                                <td class="text-right">{{rupiah($data->total-$data->getRetur->where('is_deleted',0)->where('reason_id','!=',1)->sum('total'))}}</td>
                                            </tr>
                                            <tr>
                                                <td class="left font-weight-bold">Total Pembayaran</td>
                                                <td class="text-right">{{rupiah($data->ap->paid_nominal)}}</td>
                                            </tr>
                                            <tr>
                                                <td class="left font-weight-bold">Total Kekurangan</td>
                                                @if ($data->ap->residual_amount<0)
                                                    <td class="text-danger">{{rupiah($data->ap->residual_amount)}}</td>
                                                @else
                                                    <td class="text-right">{{rupiah($data->ap->residual_amount)}}</td>
                                                @endif
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <div class="col-md-3">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Kode Transaksi :</span>
                            </dd>
                            <dt>
                                <h5>{{(!is_null($data->second_code))?$data->second_code:$data->code}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Waktu Transaksi</span>
                            </dd>
                            <dt>
                                <h5>{{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')." ".getTimeZoneName($data->timezone)}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Status Transaksi</span>
                            </dd>
                            <dt>
                                <h5 class="text-tosca">{{$data->getTransactionStatus->description}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Jatuh Tempo Pembayaran</span>
                            </dd>
                            <dt>
                                @if($data->is_debet==0)
                                    <h5>-</h5>
                                @else
                                    <h5>{{\Carbon\Carbon::parse($data->ap->due_date)->isoFormat(' D MMMM Y  HH:mm:ss')." ".getTimeZoneName($data->timezone)}}</h5>
                                @endif
                            </dt>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        function hapusPembayaran(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.delete-payment')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        function voidTransaction(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "<div class='alert alert-warning'>Menghapus data pembelian akan mengurangi stok dan menghapus pencatatan jurnal.Apakah anda yakin menghapus data pembelian?</div>", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.void')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }


        const exportData = (id) => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                const data = new FormData();
                data.append('id', id);

                ajaxTransfer("{{route('merchant.toko.purchase-order.export-faktur-excel')}}", data, '#modal-output');
            });

        }


    </script>
@endsection

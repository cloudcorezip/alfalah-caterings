<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: Arial, sans-serif; width:100%;margin-right:auto;margin-left:auto;">
<div style="width:100%;margin:0 auto;">
    <table style="width:100%;position:relative;page-break-inside:avoid;">
        <tr>
            <td style="width:50%;">
                @if($data->step_type==3)
                    <h4>RETUR PENERIMAAN PEMBELIAN</h4>
                @else
                    <h4>RETUR PEMBELIAN</h4>
                @endif
            </td>
            <td style="width:50%;">
                <h4 style="text-align:right;">{{$data->getMerchant->name}}</h4>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="border: 2px solid #FF8929">
                <br>
            </td>
        </tr>
        <tr>
            <td style="width:60%;vertical-align:baseline;">
                <table style="width:100%;" cellspacing="0">
                    <tr style="font-size: 13px">
                        <td>Kode Transaksi</td>
                        <td>:</td>
                        <td>{{is_null($retur->second_code)?$retur->code:$retur->second_code}}</td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td>Ref. Pembelian</td>
                        <td>:</td>
                        <td>
                            @if($data->step_type==0)
                                {{is_null($data->second_code)?$data->code:$data->second_code}}
                            @else
                                {{is_null($data->second_code)?str_replace('P','PD',$data->code):$data->second_code}}
                            @endif
                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td>Keterangan</td>
                        <td>:</td>
                        <td>
                            @if ($retur->reason_id!=3)
                                {{\App\Models\Accounting\ArrayOfAccounting::searchReason($retur->reason_id)}}
                            @else
                                Retur Dengan Mengurangi Hutang
                            @endif
                        </td>

                    </tr>
                </table>
            </td>
            <td style="width:40%; vertical-align:baseline;">
                <table style="width:100%;" cellspacing="0">
                    <tr style="font-size: 13px">
                        <td style="text-align:right;">Tanggal</td>
                        <td style="text-align:right;">:</td>
                        <td style="text-align:right;">
                            <?php echo \Carbon\Carbon::parse($retur->created_at)->isoFormat(' D MMMM Y  HH:mm:ss') ?>

                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="text-align:right;">Kepada</td>
                        <td style="text-align:right;">:</td>
                        <td style="text-align:right;"><?php echo (is_null($data->getSupplier))?'-':$data->getSupplier->name ?></td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="text-align:right;">No Telp</td>
                        <td style="text-align:right;">:</td>
                        <td style="text-align:right;"><?php echo (is_null($data->getSupplier))?'-':$data->getSupplier->phone_number ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width:100%;" border="0" cellspacing="0" cellpadding="5">
                    <tr style="background:#FF8929;font-size: 13px">
                        <th style="text-align:left; font-weight:bold;color:white;padding:15px; ">Kode</th>
                        <th style="text-align:center; font-weight:bold;color:white; " width="25%">Produk</th>
                        <th style="text-align:center; font-weight:bold;color:white; ">Jumlah</th>
                        <th style="text-align:center; font-weight:bold;color:white; ">Satuan</th>
                        <th style="text-align:right; font-weight:bold;color:white; " width="20%">Harga</th>
                        <th style="text-align:right; font-weight:bold;color:white; ">Sub Total</th>
                    </tr>

                    <?php foreach($retur->getDetail as $key => $item): ?>
                    <tr
                        @if($key%2==1) style="background:#fff8e1;font-size: 12px" @else style="font-size: 12px" @endif
                    >
                        <td><?php echo $item->getProduct->code;?></td>
                        <td style="text-align:center;"><?php echo $item->getProduct->name; ?></td>
                        @if ($item->is_multi_unit==1)
                            @if(is_null($item->unit_name))
                                <td style="text-align:center;"><?php echo $item->multi_quantity; ?></td>
                                <td style="text-align:center;"><?php echo (is_null($item->unit_name))?$item->getProduct->getUnit->name:$item->unit_name; ?></td>
                            @else
                                <td style="text-align:center;"><?php echo $item->quantity; ?></td>
                                <td style="text-align:center;"><?php echo $item->unit_name; ?></td>
                            @endif
                        @else
                            <td style="text-align:center;"><?php echo $item->quantity; ?></td>
                            <td style="text-align:center;"><?php echo (is_null($item->unit_name))?$item->getProduct->getUnit->name:$item->unit_name; ?></td>
                        @endif
                        <td style="text-align:right;"><?php echo rupiah($item->getPurchaseOrderDetail->price); ?> / {{$item->getPurchaseOrderDetail->unit_name}}</td>
                        <td style="text-align:right;"><?php echo rupiah($item->sub_total); ?></td>
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td style="border:1px solid #fff;" colspan="6">
                            <br>
                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="border-bottom:1px solid #FF8929;text-align: left" colspan="3">
                            Catatan
                        </td>
                        <td colspan="3" style="border:1px solid #fff">

                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="border:1px solid #FF8929; border-radius: 5px" colspan="3" rowspan="3"> {{$retur->note}}</td>
                        <td style="text-align:right;border:1px solid #fff;" colspan="2">Total :</td>
                        <td style="text-align:right;border:1px solid #fff;">{{rupiah($retur->getDetail->sum('sub_total'))}}</td>
                    </tr>

                    <tr style="font-size: 13px">
                        <td style="text-align:right;border:1px solid #fff;"colspan="2"></td>
                        <td style="text-align:right;border:1px solid #fff;"></td>
                    </tr>

                    <tr style="font-size: 13px">
                        <td style="text-align:right;border:1px solid #fff;" colspan="2"></td>
                        <td style="text-align:right;border:1px solid #fff;"></td>
                    </tr>

                    <tr>
                        <td colspan="6">
                            <br>
                            <br>
                        </td>
                    </tr>
                    <tr style="font-size: 13px">
                        <td style="width:50%;vertical-align:baseline;" colspan="2">
                            <h4 style="text-align:center;"><?php echo $data->getSupplier->name; ?></h4>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="border-bottom:1px solid black; width:30%; margin:0 auto;"></div>
                        </td>
                        <td colspan="2"></td>
                        <td style="width:50%;vertical-align:baseline;" colspan="2">
                            <h4 style="text-align:center;"><?php echo $data->getMerchant->name; ?></h4>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div style="border-bottom:1px solid black; width:30%; margin:0 auto;"></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</div>

</body>
</html>



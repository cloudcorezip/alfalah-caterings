<div class="table-responsive">
    <table class="table table-custom" id="table-data" width="100%" cellspacing="0">
        <thead>
        <tr class="text-nowrap">
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<script>
    $(document).ready(function() {
        ajaxDataTable('#table-data', 1, "{{route('merchant.toko.purchase-order.datatable-offer')}}?sk={{$searchKey}}&start_date={{$start_date}}&end_date={{$end_date}}&merchant_id={{$merchantId}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ]);
    })
</script>

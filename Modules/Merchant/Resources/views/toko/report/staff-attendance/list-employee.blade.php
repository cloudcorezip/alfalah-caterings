<div class="table-responsive">
    <table class="table table-custom" id="table-employee-attendance" width="100%" cellspacing="0">
        <thead>
        <tr>
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<script>
function reloadDataTable(isReload=0)
{ ajaxDataTable('#table-employee-attendance', 1, "{{route('merchant.toko.report.staff-attendance.datatable-employee')}}?sk={{$searchKey}}&start_date={{$start_date}}&end_date={{$end_date}}&merchant_id={{$merchantId}}&staff_user_id={{$staff_user_id}}", [
        @foreach($tableColumns as $key =>$item)
        @if($tableColumns[$key]=='action')
    {
        data: '{{$tableColumns[$key]}}',
        name: '{{$tableColumns[$key]}}',
        orderable: false,
        searchable: false
    },
        @else
    {
        data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
        orderable: false,
        searchable: false
    },
    @endif
    @endforeach
],0);
}
$(document).ready(function() {
    reloadDataTable(0)
})
</script>

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <tbody><tr>
                <td width="20%" style="font-weight: bold" class="font-weight-bold">Hari</td>
                <td>{{\Carbon\Carbon::parse($data->created_at)->isoFormat('D MMMM Y')}}</td>
                <td style="width: 40%; text-align: center!important;" rowspan="3">
                        @if(is_null($data->start_work_file))
                            <div class="foto-barang" style="background-image: url({{env('S3_URL')}}public/backend-v2/img/no-product-image.png); width: 160px; height: 160px; margin-bottom: 0;"></div>
                        @else
                            <div class="foto-barang" style="background-image: url({{env('S3_URL').$data->start_work_file}}); width: 160px; height: 160px; margin-bottom: 0;"></div>
                        @endif

                </td>
            </tr>
            <tr>
                <td style="font-weight: bold" class="font-weight-bold">Jam Masuk</td>
                <td>                {{(is_null($data->start_work))?'-':\Carbon\Carbon::parse($data->start_work)->isoFormat('dddd, D MMMM Y, HH:mm:ss')}}</td>
            </tr>
            <tr>
                <td style="font-weight: bold" class="font-weight-bold">Jam Pulang</td>
                <td>


                    {{(is_null($data->end_work))?'-':\Carbon\Carbon::parse($data->end_work)->isoFormat('dddd, D MMMM Y, HH:mm:ss')}}
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold" class="font-weight-bold">Lokasi</td>
                <td colspan="2">
                    <div id="mapid" style="height: 300px;width:100%"></div>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold" class="font-weight-bold">Status Absensi</td>
                <td colspan="2">
                    @if($data->type_of_attendance=='A')
                        <div class="badge badge-danger">Alasan</div>

                    @elseif($data->type_of_attendance=='H')
                        <div class="badge badge-success"> Hadir</div>
                    @else
                        <div class="badge badge-info">Izin</div>
                    @endif
                </td>
            </tr>

            </tbody>
        </table>
    </div>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        var mymap = L.map('mapid').setView([{{$data->start_latitude}}, {{$data->start_longitude}}], 13);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1
        }).addTo(mymap);

        L.marker([{{$data->start_latitude}}, {{$data->start_longitude}}]).addTo(mymap)
            .bindPopup("<p>Lokasi Absen</p>");

    })

</script>

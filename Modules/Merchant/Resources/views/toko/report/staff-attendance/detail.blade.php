<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        <div class="col-md-6" style="border-right: 1px solid rgba(186, 186, 186, 0.32);">
            <div class="row px-4">
                <div class="col-md-12 mb-3">
                    <h6>Nama Cabang</h6>
                    <h4 class="font-weight-bold">{{$data->merchant_name}}</h4>
                </div>
                <div class="col-md-12 mb-3">
                    <h6>Nama Karyawan</h6>
                    <h4 class="font-weight-bold">{{$data->staff_name}}</h4>
                </div>

                @if(!is_null($data->shift_roaster_id))
                <div class="col-md-12 mb-3">
                    <h6>Jadwal Shift</h6>
                    <h4 class="font-weight-bold">{{$data->shift_name}}</h4>
                </div>
                @endif
                <div class="col-md-12 mb-3">
                    <h6>Catatan</h6>
                    <p>{{$data->note}}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex align-items-center mb-4">
                        <img 
                            style="height:120px;width:120px;object-fit:contain;border-radius: 10px;"
                            src="{{env('S3_URL')}}{{$data->start_work_file}}" 
                            alt="foto presensi masuk"
                        >

                        <div class="ml-4">
                            <h6>Waktu Presensi Masuk</h6>
                            <span style="color:#62AC29;">{{\Carbon\Carbon::parse($data->start_work)->isoFormat('HH:mm:ss')}}</span>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-4">
                        <div style="width: 120px;">
                            <div 
                                id="green-dote" 
                                style="width: 20px; height:20px; border-radius:50%;background:#72BA6C;border:4px solid #D9D9D9;"
                                class="mx-auto"
                            >
                            </div>
                        </div>
                        <div class="ml-4">
                            <span>({{$diffHour}}) jam kerja</span>
                        </div>
                    </div>
                    <div class="d-flex align-items-center mb-4">
                        @if(!is_null($data->end_work))
                        <img 
                            style="height:120px;width:120px;object-fit:contain;border-radius: 10px;"
                            src="{{env('S3_URL')}}{{$data->end_work_file}}" 
                            alt="foto presensi pulang"
                        >
                        <div class="ml-4">
                            <h6>Waktu Presensi Pulang</h6>
                            <span style="color:#62AC29;">{{\Carbon\Carbon::parse($data->end_work)->isoFormat('HH:mm:ss')}}</span>
                        </div>
                        @else
                        <div style="height:120px;width:120px;"></div>
                        <div class="ml-4">
                            <h6>Waktu Presensi Pulang</h6>
                            <span>-</span>
                        </div>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer" style="display:block;">
        <div class="alert alert-warning text-center text-dark">
            <span class="iconify" data-icon="emojione:warning"></span>
            <span>Absensi yang sudah tercatat tidak dapat diubah kembali.</span>
        </div>
    </div>
</form>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div>
                                         <h4 class="modal-title">Detail Presensi</h4>
                                        <span class="span-text">Berikut rincian abesensimu pada tanggal {{\Carbon\Carbon::parse($data->created_at)->isoFormat('DD MMMM YYYY')}}</span>
                                    </div>`);
    });


</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.toko.report.staff-attendance.data')}}">Human Resources</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a class="font-weight-bold">{{$title}}</a>
                    </li>
                </ol>
            </nav>
            <span>Untuk menambah absensi, kamu harus mengisi data di bawah ini</span>
        </div>
    </div>
</div>

<!-- DataTales Example -->

<div class="container-fluid">
    <form onsubmit="return false;" id="form-konten" class="form-horizontal" backdrop="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="font-weight-bold mb-3">Informasi Presensi</h4>
                        <hr>
                    </div>
                    @include('merchant::component.branch-selection',
                    ['withoutModal'=>true,
                        'data'=>$data,
                        'subTitle'=>'Tentukan cabang mana yang akan mencatat absensi dari karyawanmu dan kamu dapat dengan mudah mengetahui riwayat absensi karyawanmu',
                        'selector'=>'branch',
                        'with_onchange'=>true,
                    ])
                    
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                @include('merchant::component.image-upload',
                                ['title'=>'Foto Masuk Kerja',
                                'fieldName'=>'start_work_file',
                                'file' => $data->start_work_file,
                                'description' => 'Tipe file : jpg, jpeg, png, docx, xlsx, pdf, zip. Maksimal ukuran file : 2.5 MB'
                                ])
                            </div>
                            <div class="col-md-6">
                                @include('merchant::component.image-upload',
                                ['title'=>'Foto Akhir Kerja',
                                'fieldName'=>'end_work_file',
                                'file' => $data->end_work_file,
                                'description' => 'Tipe file : jpg, jpeg, png, docx, xlsx, pdf, zip. Maksimal ukuran file : 2.5 MB'
                                ])
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="font-weight-bold">Catatan</label>
                            <textarea
                                class="form-control"
                                rows="3"
                                name="note"
                                cols="50"
                                placeholder="Catatan"
                                @if(!is_null($data->id)) disabled @endif
                            >{{$data->note}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Hari</label>
                            <input 
                                type="text" 
                                class="form-control form-control-sm trans_time" 
                                name="created_at" 
                                id="created_at" 
                                placeholder="Hari Absensi" 
                                value="{{$data->created_at}}" 
                                disabled
                                required
                            >
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Karyawan</label>
                            <select disabled class="form-control form-control-sm" id="staff" name="md_staff_user_id" required>
                                <option></option>
                                <option selected value="{{$data->md_staff_user_id}}">{{$data->text}}</option>
                            </select>
                        </div>
                        @if(!is_null($data->shift_roaster_id))
                        <div class="form-group" id="shift-wrapper">
                            <label class="font-weight-bold">Shift</label>
                            <select class="form-control" name="shift" id="shift" disabled>
                                <option selected value="{{$data->shift_roaster_id}}">{{$data->shift_roaster_name}}</option>
                            </select>
                        </div>
                        @endif
                        
                        <div class="form-group">
                            <label class="font-weight-bold">Waktu Mulai Kerja</label>
                            <input 
                                type="text" 
                                class="form-control form-control-sm trans_time" 
                                name="start_work" 
                                id="start_work" placeholder="Waktu Mulai Kerja" 
                                required
                                value="{{$data->start_work}}"
                                disabled
                            >
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Waktu Akhir Kerja</label>
                            <input 
                                type="text" 
                                class="form-control form-control-sm trans_time" 
                                name="end_work" 
                                placeholder="Waktu Akhir Kerja" 
                                id="end_work"
                                value="{{$data->end_work}}"
                                @if(!is_null($data->end_work))
                                disabled
                                @endif
                            >
                            <small class="d-block mt-1" style="color: #EE6767;">Isikan jika anda menginputkan sekaligus jam mulai dan jam selesai kerja karyawan</small>
                        </div>

                        <div class="form-group">
                            <label class="font-weight-bold">Terlambat</label>
                            <br>
                            <div class="form-check form-check-inline">
                                <input 
                                    id="is_late_0" 
                                    class="form-check-input" 
                                    type="radio" 
                                    value="0" 
                                    name="is_late"
                                    @if($data->is_late == 0)
                                    checked
                                    @endif
                                    disabled
                                >
                                <label class="form-check-label">Tidak</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input 
                                    id="is_late_1" 
                                    class="form-check-input" 
                                    type="radio" 
                                    value="1" 
                                    name="is_late"
                                    @if($data->is_late == 1)
                                    checked
                                    @endif
                                    disabled
                                >
                                <label class="form-check-label">Ya</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 text-right">
                <input type="hidden" name="id" value="{{$data->id}}">
                <a href="{{route('merchant.toko.report.staff-attendance.data')}}" class="btn btn-light text-light mr-2">Kembali</a>
                <button class="btn btn-success"><i class="fa fa-save mr-2"></i> Simpan</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    $("#staff").select2({
        placeholder: '--- Pilih Karyawan ---'
    });
    $("#shift").select2();

    function onchangeBranch(){
        let branch = $("#branch").val();
        $("#staff").select2({
            placeholder:'--- Pilih Karyawan ---',
            ajax: {
                type: "GET",
                url: "{{route('merchant.ajax.share-data.employee-by-merchant')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        key:params.term,
                        md_merchant_id:branch,
                        is_non_employee: 0
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }

    $(document).ready(function () {
        dateTimePicker(".trans_time");
        onchangeBranch();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.report.staff-attendance.save')}}", data,function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    });
</script>
@endsection

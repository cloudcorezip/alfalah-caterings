
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                <h5 align="center">Detail Absensi Periode {{$startDate}}-{{$endDate}}</h5>
                <hr>
                <table class="table table-bordered" id="table-data" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Karyawan</td>
                        <td>Jam Bekerja</td>
                        <td>Pulang Bekerja</td>
                        <td>Keterangan</td>
                    </tr>
                    </thead>
                    <tbody> 
                        @php
                            $alpha=0;$hadir=0;$izin=0;
                        @endphp         
                    @foreach($data as $key =>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->getStaff->fullname}}</td>
                            <td>{{$item->start_work}}</td>
                            <td>{{$item->end_work}}</td>
                            <td>{{$item->type_of_attendance}}</td>
                            @php
                                $alpha+=$item->where('type_of_attendance', 'A')->count();
                                $hadir+=$item->where('type_of_attendance', 'H')->count();
                                $izin+=$item->where('type_of_attendance', 'I')->count();
                            @endphp
                        </tr>
                    @endforeach
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td>Hadir:</td>
                        <td>{{$hadir}}</td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td>Alpha:</td>
                        <td>{{$alpha}}</td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td>Izin:</td>
                        <td>{{$izin}}</td>
                    </tr>
                    </tbody>
                </table>
        </form>
            
        </div>
    </div>

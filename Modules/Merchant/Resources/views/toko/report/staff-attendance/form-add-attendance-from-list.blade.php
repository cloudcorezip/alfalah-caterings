@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.toko.report.staff-attendance.data')}}">Human Resources</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a class="font-weight-bold">Tambah Absensi Masuk</a>
                    </li>
                </ol>
            </nav>
            <span>Untuk menambah absensi, kamu harus mengisi data di bawah ini</span>
        </div>
    </div>
</div>

<!-- DataTales Example -->

<div class="container-fluid">
    <form onsubmit="return false;" id="form-konten" class="form-horizontal" backdrop="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="font-weight-bold mb-3">Informasi Presensi</h4>
                        <hr>
                    </div>
                    <div class="col-md-12 mb-4" style="background-color: rgba(221, 221, 221, 0.1);border: 2px solid #F0F0F0;border-radius: 10px">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label font-weight-bold">Multi Cabang<br>
                                    <small>
                                    Tentukan cabang mana yang akan mencatat absensi dari karyawanmu dan kamu dapat dengan mudah mengetahui riwayat absensi karyawanmu
                                    </small>
                                </label>
                                <select disabled class="form-control" id="branch" name="md_merchant_id" onchange="onchangeBranch()" required>
                                    <option value="-1">Pilih Cabang</option>
                                    @foreach(get_cabang() as $b)
                                        <option value="{{$b->id}}" @if($b->id==$merchantId) selected @endif>{{$b->nama_cabang}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                @include('merchant::component.image-upload',
                                ['title'=>'Foto Masuk Kerja',
                                'fieldName'=>'start_work_file',
                                'file' => $data->start_work_file,
                                'description' => 'Tipe file : jpg, jpeg, png, docx, xlsx, pdf, zip. Maksimal ukuran file : 2.5 MB'
                                ])
                            </div>
                            <div class="col-md-6">
                                @include('merchant::component.image-upload',
                                ['title'=>'Foto Akhir Kerja',
                                'fieldName'=>'end_work_file',
                                'file' => $data->end_work_file,
                                'description'=> 'Tipe file : jpg, jpeg, png, docx, xlsx, pdf, zip. Maksimal ukuran file : 2.5 MB'
                                ])
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="font-weight-bold">Catatan</label>
                            <textarea
                                class="form-control"
                                rows="3"
                                name="note"
                                cols="50"
                                placeholder="Catatan"
                                @if(!is_null($data->id)) disabled @endif
                            ></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Hari</label>
                            <input 
                                type="text" 
                                class="form-control form-control-sm trans_time" 
                                name="created_at" 
                                id="created_at" 
                                placeholder="Hari Absensi" 
                                value="{{$date}}" 
                                onchange="checkShift()" 
                                required
                                disabled
                            >
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Karyawan</label>
                            <select disabled class="form-control form-control-sm" id="staff" name="md_staff_user_id" onchange="checkShift()">
                                <option></option>
                                <option 
                                    selected 
                                    value="{{$staff->id}}"
                                >{{$staff->text}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group" id="shift-wrapper" style="display:none;">
                            <label class="font-weight-bold">Shift</label>
                            <select class="form-control" name="shift" id="shift" onchange="changeShift()">
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Waktu Mulai Kerja</label>
                            <input type="text" class="form-control form-control-sm trans_time" name="start_work" id="start_work" placeholder="Waktu Mulai Kerja" onchange="checkStartWork()" required>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Waktu Akhir Kerja</label>
                            <input type="text" class="form-control form-control-sm trans_time" name="end_work" placeholder="Waktu Akhir Kerja" id="end_work">
                            <small class="d-block mt-1" style="color: #EE6767;">Isikan jika anda menginputkan sekaligus jam mulai dan jam selesai kerja karyawan</small>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Terlambat</label>
                            <br>
                            <div class="form-check form-check-inline">
                                <input id="is_late_0" class="form-check-input" type="radio" value="0" name="is_late" checked>
                                <label class="form-check-label">Tidak</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input id="is_late_1" class="form-check-input" type="radio" value="1" name="is_late">
                                <label class="form-check-label">Ya</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 text-right">
                <input type="hidden" name="id" value="{{$data->id}}">
                <a href="{{route('merchant.toko.report.staff-attendance.data')}}" class="btn btn-light text-light mr-2">Kembali</a>
                <button class="btn btn-success"><i class="fa fa-save mr-2"></i> Simpan</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    $("#staff").select2({
        placeholder:'--- Pilih Karyawan ---'
    });
    $("#shift").select2();
    $("#branch").select2();

    function checkShift()
    {
        let staffUserId = $("#staff").val();
        let date = $("#created_at").val();

        $.ajax({
            type:"POST",
            url: "{{route('merchant.toko.report.staff-attendance.check-shift')}}",
            data: {
                staff_user_id: staffUserId,
                date:date,
                _token: "{{csrf_token()}}"
            },
            success:function(response){
                $("#shift").empty();
                if(response.length > 0){
                    $("#shift-wrapper").show();
                    option = ``;
                    response.forEach(item => {
                        option += `<option
                                        value="${item.id}"
                                        start-shift="${item.start_shift}"
                                        end-shift="${item.end_shift}"
                                        half-shift="${item.half_shift}"
                                        max-late="${item.max_late}"
                                        is_allowed_shift_other_time="${item.is_allowed_shift_other_time}"
                                    >
                                    ${item.text}
                                    </option>`
                    });

                    $("#shift").append(option);

                } else {
                    $("#shift-wrapper").hide();
                }

                changeShift();
                
            }
        })
    }

    function checkStartWork(){
        let startWork = $("#start_work").val();
        let val = moment(startWork).format('HH:mm:ss');

        let shift = {
            id: $("#shift").val(),
            start_shift: $("#shift").find('option:selected').attr('start-shift'),
            end_shift: $("#shift").find('option:selected').attr('end-shift'),
            max_late: $("#shift").find('option:selected').attr('max-late')
        }


        if(shift.id != null){
            $("#is_late_1").prop("disabled", true);
            $("#is_late_0").prop("disabled", true);
            if(val > shift.start_shift){
                $("#is_late_1").prop('checked', true);
            } else {
                $("#is_late_0").prop('checked', true);
            }
        } else {
            $("#is_late_1").prop("disabled", false);
            $("#is_late_0").prop("disabled", false);
        }
        
    }

    function changeShift(){
        $("#start_work").val("");
        $("#end_work").val("");
        $("#is_late_1").prop("disabled", false);
        $("#is_late_0").prop("disabled", false);
    }

    function onchangeBranch(){
        let branch = $("#branch").val();
        checkShift();
        $("#staff").select2({
            placeholder:'--- Pilih Karyawan ---',
            ajax: {
                type: "GET",
                url: "{{route('merchant.ajax.share-data.employee-by-merchant')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        key:params.term,
                        md_merchant_id:branch,
                        is_non_employee: 0
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }

    $(document).ready(function () {
        dateTimePicker(".trans_time");
        onchangeBranch();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.report.staff-attendance.save')}}", data,function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    });
</script>
@endsection

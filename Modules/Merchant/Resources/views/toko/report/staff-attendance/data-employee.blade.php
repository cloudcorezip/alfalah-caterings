@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Human Resource</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Ketahui kehadiran karyawanmu dari manapun dan kapanpun</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a onclick="activeMenu('human-resources','shift-roaster')" class="nav-link" href="{{route('merchant.toko.report.staff-attendance.data')}}">Presensi</a>
            </li>
            <li class="nav-item">
                <a onclick="activeMenu('master-data','kupon-diskon')" class="nav-link active" href="">Presensi Per Karyawan</a>
            </li>
        </ul>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row" id="profile-header">
                </div>
                <div class="row" id="header-data">
                </div>
                <div id="output-discount-product">
                @include('merchant::toko.report.staff-attendance.list-employee')
                </div>
            </div>
        </div>
        <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
            <div class="d-flex align-items-center">
                <i class="fas fa-filter mr-2 text-white"></i>
                <span class="text-white form-filter3-text-header">Filter</span>
            </div>
        </a>
        <div class="form-filter3" id="form-filter3">
            <div class="row">
                <div class="col-12">
                    <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center">
                            <i class="fas fa-filter mr-2"></i>
                            <span class="text-white form-filter3-text-header">Filter</span>
                        </div>
                        <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                            <i style="font-size:14px;" class="fas fa-times text-white"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row mb-3 px-30">
                <div class="col-12">
                    <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                    <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
                </div>
            </div>
            <hr>
            <form id="form-filter-discount-product" class="px-30" onsubmit="return false">
                <div class="row d-flex align-items-center">
                    <div class="col-md-12 mb-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal Awal</label>
                            <input type="text" class="form-control form-control trans_time" id="startDate" value="{{$start_date}}" style="margin-bottom: 3px;background: white" name="start_date" required>
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal Akhir</label>
                            <input type="text" class="form-control form-control trans_time" style="background: white" id="endDate" value="{{$end_date}}" name="end_date" required>
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Pilih Karyawan</label>
                            <select class="form-control" name="md_staff_user_id" id="md_staff_user_id">
                                <option></option>
                                @if(!is_null($staff))
                                <option selected value="{{$staff->id}}">{{$staff->text}}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Pilih Cabang</label>
                            <select class="form-control" name="md_merchant_id" id="md_merchant_id" onchange="onchangeBranch()">
                            @foreach(get_cabang() as $key => $item)
                                <option
                                    value="{{$item->id}}"
                                    @if($item->id == $merchantId)
                                    selected
                                    @endif
                                >{{$item->nama_cabang}}
                                </option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <button id="btn-submit" type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                        <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type="hidden" name="search_key" value="{{$searchKey}}">
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection

@section('js')
    <script>
        $("#md_staff_user_id").select2();
        $("#md_merchant_id").select2();
        $(document).ready(function() {
            $("#status").select2();
            $('#form-filter-discount-product').submit(function () {
                var startDate = $("#startDate").val();
                var endDate = $("#endDate").val();
                var data = getFormData('form-filter-discount-product');
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                loadHeaderEmployee($("#md_staff_user_id").val());
                ajaxTransfer("{{route('merchant.toko.report.staff-attendance.reload-data-employee')}}", data, '#output-discount-product');
                showFilter('btn-show-filter3', 'form-filter3');
            });
            loadHeaderEmployee({{$staff_user_id}});
        });

        function loadHeaderEmployee(staffUserId)
        {
            let merchantId = $("#md_merchant_id").val();
            $.ajax({
                type:"POST",
                url: "{{route('merchant.toko.report.staff-attendance.load-header-employee')}}",
                data:{
                    md_merchant_id:merchantId,
                    md_staff_user_id:staffUserId,
                    _token: "{{csrf_token()}}"
                },
                success:function(response){
                    $("#header-data").empty();
                    $("#profile-header").empty();
                    if(response.length > 0){
                        let html = `<div class="col-md-3">
                                        <div class="card rounded">
                                            <div class="card-body">
                                                <h6 class="font-weight-bold">Kehadiran</h6>
                                                <h4 style="color:#FA6D1D;" class="text-right font-weight-bold">${response[0].kehadiran} Hari</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="card rounded">
                                            <div class="card-body">
                                                <h6 class="font-weight-bold">Terlambat</h6>
                                                <h4 style="color:#FA6D1D;" class="text-right font-weight-bold">${response[0].terlambat} Hari</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="card rounded">
                                            <div class="card-body">
                                                <h6 class="font-weight-bold">Izin</h6>
                                                <h4 style="color:#FA6D1D;" class="text-right font-weight-bold">${response[0].izin} Hari</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="card rounded">
                                            <div class="card-body">
                                                <h6 class="font-weight-bold">Sakit</h6>
                                                <h4 style="color:#FA6D1D;" class="text-right font-weight-bold">${response[0].sakit} Hari</h4>
                                            </div>
                                        </div>
                                    </div>`;

                        let foto = (response[0].foto == null)?  '{{asset('public/backend/img/no-user.png')}}':'{{env('S3_URL')}}'+response[0].foto;
                        let profileHtml = `<div class="col-md-6 d-flex align-items-center">
                                            <img
                                                src="${foto}"
                                                alt=""
                                                style="width: 80px; height:80px; object-fit:cover;border-radius: 50%;"
                                                class="mr-4"
                                            >
                                            <div>
                                                <h4 class="font-weight-bold">${response[0].fullname}</h4>
                                                <span class="text-light">${response[0].job_name}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-12"><hr></div>`
                        $("#header-data").append(html);
                        $("#profile-header").append(profileHtml);
                    }
                }
            });
        }

        function getStaff()
        {
            let branch = $("#md_merchant_id").val();
            $("#md_staff_user_id").select2({
                placeholder:'--- Pilih Karyawan ---',
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.ajax.share-data.employee-by-merchant')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key:params.term,
                            md_merchant_id:branch,
                            is_non_employee: 0
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            })
        }

        function onchangeBranch(){
            $("#md_staff_user_id").val("").change();
            getStaff()
        }

        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                const data = new FormData();
                data.append('start_date',$("#startDate").val());
                data.append('end_date',$("#endDate").val());
                data.append('md_merchant_id',{{$merchantId}});
                data.append('md_staff_user_id', $("#md_staff_user_id").val());
                ajaxTransfer("{{route('merchant.toko.report.staff-attendance.cetakPDF')}}", data, '#modal-output');
            });

        }

        getStaff();

    </script>
@endsection

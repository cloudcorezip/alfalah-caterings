<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal form-konten' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1">Status</label>
        <select name="is_active" id="is_active" class="form-control">
            <option value="1" @if($data->is_active == 1) selected @endif>Aktif</option>
            <option value="0" @if($data->is_active == 0) selected @endif>Tidak Aktif</option>
        </select>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>

    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $("#is_active").select2();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('setting.ecommerce.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Ubah Data</h4>
                                        <span class="span-text">Ubah Status</span>
                                    </div>`);
    });


</script>

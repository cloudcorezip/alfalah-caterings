@if(count($empdShopee) < $empShopee->max_account)
<div class="row">
    <div class="col-lg-auto mb-4">

        <button class="btn btn-success btn-rounded btn-sm py-2 px-4">
            Hubungkan Toko
        </button>
    </div>
</div>
@endif

<div class="table-responsive">
    <table class="table table-custom" id="table-data-ecommerce" width="100%" cellspacing="0">
        <thead>
        <tr>
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

@if(session()->has('status'))
    @php
        $status = session()->get('status');
    @endphp
    <script>
        $(document).ready(function(){
            toastForSaveData('{{$status["message"]}}','{{$status["type"]}}',false,'');
        });
    </script>
@endif

<script>
    function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
            ajaxTransfer("{{route('setting.ecommerce.delete')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    }
    function reloadDataTable(isReload=0)
    {
        ajaxDataTable('#table-data-ecommerce', 1, "{{route('setting.ecommerce.datatable')}}?key_val={{$key_val}}&subType={{$sub}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ],0);
    }
    $(document).ready(function() {
        reloadDataTable(0);
    });
</script>

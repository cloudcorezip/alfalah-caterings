@section('css')
<style>
    .btn-trigger-collapse {
        display: flex!important;
        align-items: center;
        justify-content:space-between;
        background-color: #fff;
        border: 1px solid #fff;
        border-bottom: 1px solid #E5E5E5;
        background: #fff;
        width: 100%;
        padding: 0!important;
    }
    .btn-trigger-collapse:focus {
        outline: none;
    }
    .btn-trigger-collapse .iconify {
        transition: transform .4s ease;
    }
    .card-preview-custom-code {
        background: #FFFFFF;
        border: 1px solid rgba(228, 231, 234, 0.5);
        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15)!important;
        border-radius: 10px;
    }
    .formula-element {
        background: #FAFAFA;
        border: 1px dashed #E4E7EA;
        border-radius: 8px;
        padding: 10px;
    }
    .formula-item {
        position:relative;
    }
    .formula-item .iconify {
        cursor: pointer;
    }
    .nav-tabs-orange {
        border-bottom: none!important;
    }
    .nav-tabs-orange .nav-link {
        position: relative;
        color: #A8A8A8!important;
        border: none!important;
        font-size: 16px;
    }
    .nav-tabs-orange .nav-link.active {
        color:#FF8A00!important;
        font-weight: 600!important;
        background: transparent!important;
        box-shadow: none!important;
        position: relative;
        margin-bottom: 10px;
    }
    .nav-tabs-orange .nav-link.active::after {
        content: "";
        position: absolute;
        width: 100%;
        height: 2px;
        background:#FF8A00;
        left: 50%;
        transform: translateX(-50%);
        bottom: 10%;
    }
    @media screen and (max-width: 768px){
        .nav-tabs-orange .nav-link.active::after {
            width: 0;
        }
    }
    .rotate {
        transform: rotate(90deg);
        transition: transform .4s ease-in;
    }
    select.is_continue_head + span.select2-container .select2-selection__rendered {
        margin-left: 30px;
    }
    .explanation-no {
        position:absolute;
        top: 100%;
        bottom: 0;
        left: 0;
        width: 100%;
        border-radius: 10px;
        background: #FFFFFF;
        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
        padding: 10px;
        height:fit-content;
        z-index: 100;
    }
    .explanation-no thead {
        background: rgba(217, 217, 217, 0.3)!important;
    }

    .explanation-no tr td, .explanation-no tr th {
        padding: 10px;
    }

</style>
@endsection
<div class="card-body">
    <ul class="nav nav-tabs mb-4 nav-tabs-orange">
        <li class="nav-item">
            <a 
                onclick="activeMenu('pengaturan','pengaturan')" 
                class="nav-link px-0 mr-5 {{is_null($sub)?'active':''}}" 
                href="{{route('merchant.toko.profile.detail',['id'=>$data->id, 'page'=>'custom-code'])}}"
            >
                Penjualan
            </a>
        </li>
        <li class="nav-item">
            <a 
                onclick="activeMenu('pengaturan','pengaturan')" 
                class="nav-link px-0 mr-5 {{$sub=='purchase'?'active':''}}" 
                href="{{route('merchant.toko.profile.detail',['id'=>$data->id, 'page'=>'custom-code', 'sub'=>'purchase'])}}"
            >
                Pembelian
            </a>
        </li>
        <li class="nav-item">
            <a 
                onclick="activeMenu('pengaturan','pengaturan')" 
                class="nav-link px-0 mr-5 {{$sub=='stock'?'active':''}}" 
                href="{{route('merchant.toko.profile.detail',['id'=>$data->id, 'page'=>'custom-code', 'sub'=>'stock'])}}"
            >
                Persediaan
            </a>
        </li>
        <li class="nav-item">
            <a 
                onclick="activeMenu('pengaturan','pengaturan')" 
                class="nav-link px-0 mr-5 {{$sub=='master-data'?'active':''}}" 
                href="{{route('merchant.toko.profile.detail',['id'=>$data->id, 'page'=>'custom-code','sub'=>'master-data'])}}"
            >
                Master Data
            </a>
        </li>
        <li class="nav-item">
            <a 
                onclick="activeMenu('pengaturan','pengaturan')" 
                class="nav-link px-0 mr-5 {{$sub=='asset'?'active':''}}" 
                href="{{route('merchant.toko.profile.detail',['id'=>$data->id, 'page'=>'custom-code', 'sub'=>'asset'])}}"
            >
                Asset
            </a>
        </li>
        <li class="nav-item">
            <a 
                onclick="activeMenu('pengaturan','pengaturan')" 
                class="nav-link px-0 mr-5 {{$sub=='acc'?'active':''}}" 
                href="{{route('merchant.toko.profile.detail',['id'=>$data->id, 'page'=>'custom-code', 'sub'=>'acc'])}}"
            >
                Keuangan
            </a>
        </li>
    </ul>
    
    @if(is_null($sub))
        @include('merchant::toko.profile.custom-code.sale.index')
    @endif

    @if($sub == "purchase")
        @include('merchant::toko.profile.custom-code.purchase.index')
    @endif

    @if($sub == "stock")
        @include('merchant::toko.profile.custom-code.stock.index')
    @endif

    @if($sub == "master-data")
        @include('merchant::toko.profile.custom-code.master-data.index')
    @endif

    @if($sub == "asset")
        @include('merchant::toko.profile.custom-code.asset.index')
    @endif

    @if($sub == "acc")
        @include('merchant::toko.profile.custom-code.acc.index')
    @endif
    
</div>


@section('js')
<script>
    $(".is_continue_head").select2();
    function tooltipOrder(val){
        if(val == 1){
            return `<div class="explanation-no">
                    <h6 class="font-weight-bold">Contoh</h6>
                    <table width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width:40%;">Pusat</th>
                                <th style="idth: 60%;">No Urut</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Cabang A</td>
                                <td>01</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>02</td>
                            </tr>
                            <tr>
                                <td>Cabang B</td>
                                <td class="d-flex align-items-center" style="color:#72BA6C;"><span class="mr-4">03</span> <small>(melanjutkan urutan sebelumnya walaupun berbeda cabang) </small></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>04</td>
                            </tr>
                        </tbody>
                    </table>
                </div>`;
        } else {
            return `<div class="explanation-no">
                        <h6 class="font-weight-bold">Contoh</h6>
                        <table width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width:40%;">Pusat</th>
                                    <th style="idth: 60%;">No Urut</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Cabang A</td>
                                    <td>01</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>02</td>
                                </tr>
                                <tr>
                                    <td>Cabang B</td>
                                    <td class="d-flex align-items-center" style="color:#72BA6C;"><span class="mr-4">01</span> <small>(setiap cabang mulai dari urutan 1)</small></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>02</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>`;
        }
        
                
    }

    function tooltipHover(e, type){
        const val = $(e).closest('.formula-col').find('.is_continue_head').val();
        if(type == 1)
        {
            $(e).closest('.formula-col').find('.formula-element .no-tooltip-wrapper').append(tooltipOrder(val));
        } else {
            $(e).closest('.formula-col').find('.formula-element .no-tooltip-wrapper').empty();
        }
    }

    function rotateArrow(e, selector)
    {
        const icon = $(e).find(selector);
        icon.toggleClass('rotate');
    }

    function checkExsistBranchCode()
    {
        const formElement = [...document.querySelectorAll('form.form-konten')];
        formElement.forEach(item => {
            const formId = item.getAttribute('id');
            const branchCodeElement = document.querySelectorAll(`#${formId} .formula-col .kode-cabang`);
            if(branchCodeElement.length < 1){
                $(item).find('.is_continue_head').prop('disabled', true);
                $(item).find('.is_continue_head').val(1).change();
            }
        }); 
    } 
    
    function getCodeSetting()
    {
        $(".formula").select2({
            placeholder: "--- Pilih Formula ---",
            ajax: {
                type: "GET",
                url: "{{route('merchant.ajax.share-data.parent-code-setting')}}",
                dataType: 'json',
                delay: 250,
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    data.unshift({
                        id: -1,
                        text: "--- Pilih Formula ---"
                    })
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }

    function fillChildSelect(selector, id, placeholder, isLoadPage)
    {
        $.ajax({
            type: "GET",
            url:"{{route('merchant.ajax.share-data.child-code-setting')}}",
            data: {
                parent_id: id
            },
            success: function(response){
                let option = ``;
                if(isLoadPage == 0)
                {
                    response.forEach(item => {
                        option += `<option data-id="${item.id}" value="${item.value}">${item.name}</option>`;
                    });
                    $(selector).last().append(option);
                } else {
                    let selectedVal = $(selector).find('option:selected').val();
                    response.forEach(item => {
                        if(selectedVal != item.value){
                            option += `<option data-id="${item.id}" value="${item.value}">${item.name}</option>`;
                        }
                    });
                    $(selector).append(option);
                }
            } 
        });
        $(selector).select2();
    }

    function appendFormula(id, selector)
    {
        // id disini adalah parent_id
        // id 1 = kode
        // id 2 = pemisah
        // id 3 = tanggal
        // id 4 = kode cabang
        // id 5 = no urut
        let html = ``;
        if(id == 1){
            html = `<div class="col-md-3 mb-3 formula-col">
                        <label>Kode</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <input 
                                    type="text" 
                                    class="form-control formula-input kode mr-3" 
                                    placeholder="Kode" 
                                    data-type="text" 
                                    data-id="${id}"
                                    onchange="reviewCode('${selector}')"
                                >
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '${selector}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                        </div>
                    </div>`;
            $(selector).find('.formula-content').append(html);
            return;
        }

        if(id == 2)
        {
            html = `<div class="col-md-3 mb-3 formula-col">
                        <label>Pemisah</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <select onchange="reviewCode('${selector}')" class="form-control formula-input mr-2" data-type="select" parent-id="${id}">
                                </select>
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '${selector}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                        </div>
                    </div>`;

            $(selector).find('.formula-content').append(html);
            fillChildSelect(`${selector} select[parent-id="${id}"]`, id, '--- Pilih Pemisah ---', 0);
            return;
        }
        if(id == 3)
        {
            html = `<div class="col-md-3 mb-3 formula-col">
                        <label>Tanggal</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <select onchange="reviewCode('${selector}')" class="form-control formula-input mr-2" data-type="select" parent-id="${id}">
                                </select>
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '${selector}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                        </div>
                    </div>`;
            $(selector).find('.formula-content').append(html);
            fillChildSelect(`${selector} select[parent-id="${id}"]`, id, '--- Pilih Tanggal ---', 0);
            return;
        }

        if(id ==  4)
        {
            html = `<div class="col-md-3 mb-3 formula-col">
                        <label>Kode Cabang</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <input onchange="reviewCode('${selector}')" disabled value="[KODE CABANG]" type="text" class="form-control formula-input kode-cabang mr-3" placeholder="Kode Cabang" data-type="text" data-id="${id}">
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '${selector}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                        </div>
                    </div>`;
            $(selector).find('.formula-content').append(html);
            return;
        }

        if(id == 5)
        {
            const check = $(selector).find('.kode-cabang');
            let isDisable = (check.length == 0)? "disabled":"";
            html = `<div class="col-md-4 mb-3 formula-col">
                        <label>No Urut</label>
                        <div class="formula-element position-relative">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <input 
                                    onchange="reviewCode('${selector}')" 
                                    type="number" 
                                    class="form-control formula-input no-urut mr-1" 
                                    placeholder="Mulai Dari" 
                                    data-type="number" 
                                    data-id="${id}"
                                    style="width: 40%;"
                                >
                                <div class="position-relative" style="width: 50%;">
                                    <span 
                                        data-width="20" data-height="20" 
                                        class="iconify" 
                                        data-icon="akar-icons:question"
                                        style="position:absolute;top: 50%;left: 6px;transform:translateY(-50%);z-index:100;"
                                        onmouseover="tooltipHover(this, 1)"
                                        onmouseout="tooltipHover(this, 0)"
                                    >
                                    </span>
                                    <select class="form-control is_continue_head" name="is_continue_head" ${isDisable}>
                                        <option selected value="1">Urut Pusat</option>
                                        <option value="0">Urut Per Cabang</option>
                                    </select>
                                </div>
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '${selector}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                            <div class="no-tooltip-wrapper"></div>
                        </div>
                    </div>`;
            $(selector).find('.formula-content').append(html);
            $(selector).find('select.is_continue_head').select2();
            return;
        }

    }

    function removeFormula(e, selector)
    {
        const checkVal = $(e).closest('.formula-col').find('.formula-input').attr('data-id');

        if(checkVal == 4){
            $(selector).find('.is_continue_head').prop('disabled', true);
            $(selector).find('.is_continue_head').val(1).change();
        }

        $(e).closest('.formula-col').remove();
        reviewCode(selector);
    }

    function selectFormula(e, selector)
    {
        const val = $(e).val();
        
        if(val == -1){
            return;
        }

        if(val == 4){
            let check = $(selector).find('.kode-cabang');
            if(check.length > 0){
                toastForSaveData('Kode cabang sudah dipilih !','warning','false','',0);
                setTimeout(() => {
                    $(e).val(-1).change();
                }, 500);
                return;
            }

            $(selector).find('.is_continue_head').prop('disabled', false);
            $(selector).find('.is_continue_head').val(1).change();
        }
        if(val == 5){
            let checkNoUrut = $(selector).find('.no-urut');
            if(checkNoUrut.length > 0){
                toastForSaveData('No urut sudah dipilih !','warning','false','',0);
                setTimeout(() => {
                    $(e).val(-1).change();
                }, 500);
                return;
            }
        }

        appendFormula(val, selector, name);
        setTimeout(() => {
            $(e).val(-1).change();
        }, 500);
    }

    function reviewCode(selector)
    {   
        const inputElement = [...document.querySelectorAll(`${selector} .formula-input`)];
        var text = "";
        inputElement.forEach(item => {
            const parentId = item.getAttribute('parent-id');
            if(parentId == 3){
                switch(item.value){
                    case "date('Y')":
                        text += moment().format('YYYY');
                        break;
                    case "date('M')":
                        text += moment().format('MMM');
                        break;
                    case "date('y')":
                        text += moment().format('YY');
                        break;
                    case "date('m')":
                        text += moment().format('MM');
                        break;
                    case "date('d')":
                        text += moment().format('DD');
                        break;
                    default:
                        text += "";
                        break;
                }
            } else {
                text += item.value
            }
        });

        if(inputElement.length < 1){
            $(`${selector} .preview-text`).text('A-000000');
        } else {
            $(`${selector} .preview-text`).text(text);
        }
    }

    $(document).ready(function(){
        getCodeSetting();
        checkExsistBranchCode();
        const selectElement = [...document.querySelectorAll('select.formula-input[data-type="select"]')];
        selectElement.forEach(item => {
            const parentId = item.getAttribute('parent-id');
            fillChildSelect(item, parentId, "", 1);
        });

        const previewCode = [...document.querySelectorAll('.preview-text')];

        previewCode.forEach(item => {
            const selector = `${item.getAttribute('form-id')}`;
            reviewCode(`#${selector}`);
        });

        $(".form-konten").submit(function(){
            const id = $(this).attr('id');
            const data = getFormData(id);
            const columnElement = document.getElementById(id);
            const inputElement = [...columnElement.querySelectorAll('.formula-input')];
            const is_continue_head = id;

            const codeConfig = [...inputElement].map(item => {
                let type = item.getAttribute('data-type');
                if(type == "select"){
                    let configId = $(item).find('option:selected').attr('data-id');
                    let val = $(item).val();
                    return {
                        id: parseInt(configId),
                        value: val
                    }
                } else if(type == "number"){
                    return {
                        id: parseInt($(item).attr('data-id')),
                        value: parseInt($(item).val())
                    }
                }else {
                    return {
                        id: parseInt($(item).attr('data-id')),
                        value: $(item).val()
                    }
                }
            });

            data.append('code_config', JSON.stringify(codeConfig));
            ajaxTransfer("{{route('merchant.toko.setting.custom-code.save')}}", data,function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })
</script>
@endsection
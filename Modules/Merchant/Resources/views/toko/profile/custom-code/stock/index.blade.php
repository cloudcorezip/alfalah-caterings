@foreach($list as $key => $item)
    @if($item->type == "stock")
        <div class="mb-4">
            @include('merchant::toko.profile.custom-code.stock.form', [
                "form_id" => $item->id,
                "form_title" => $item->title,
                "form_type" => $item->type,
                "form_sub_type" => $item->sub_type
            ])
        </div>
    @endif
@endforeach
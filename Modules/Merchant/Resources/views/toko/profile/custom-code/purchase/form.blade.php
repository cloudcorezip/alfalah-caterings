<form class="form-konten" id="{{$form_id}}" onsubmit="return false" backdrop="">
    <input type="hidden" name="type" value="{{$form_type}}">
    <input type="hidden" name="sub_type" value="{{$form_sub_type}}">
    <button 
        class="btn-trigger-collapse" 
        type="button" 
        data-toggle="collapse" 
        data-target="#{{$form_id}}-collapse" 
        aria-expanded="false" 
        aria-controls="{{$form_id}}-collapse"
        type="button"
        onclick="rotateArrow(this,'.iconify')"
    >
        <h4 class="font-weight-bold">{{$form_title}}</h4>
        <span class="iconify" data-icon="akar-icons:chevron-right"></span>
    </button>
    <div class="collapse" id="{{$form_id}}-collapse">
        <div class="row py-4">
            <div class="col-md-6">
                <div class="form-group">
                    <h6 style="color:rgba(79, 79, 79, 0.79);">Pilih formula sesuai kebutuhanmu yuk, kamu tinggal pilih case formulanya lalu tata urutannya sesuka hatimu.</h6>
                    <div class="w-80">
                        <select class="form-control formula" onchange="selectFormula(this, '#{{$form_id}}')">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-preview-custom-code">
                    <div class="card-body">
                        <h5 class="font-weight-bold">Preview</h5>
                        <span class="d-block mb-3">Ini loh hasilnya dari pengaturan di bawah :</span>
                        <h2 form-id="{{$form_id}}" class="preview-text" style="color:rgba(79, 79, 79, 0.79);">000000</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="row form-konten">
            <div class="col-md-12">
                <h4 class="font-weight-bold">Formula</h4>
            </div>
        </div>
        <div class="row formula-content mb-4">
            @foreach($codeConfig as $key => $item)
                @if($item->type == $form_type && $item->sub_type == $form_sub_type)
                    @if($item->parent_id == 1)
                    <div class="col-md-3 mb-3 formula-col">
                        <label>Kode</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <input 
                                    type="text" 
                                    class="form-control formula-input mr-3 kode" 
                                    placeholder="Kode" 
                                    data-type="text" 
                                    data-id="{{$item->id}}"
                                    value="{{$item->value}}"
                                    onchange="reviewCode('#{{$form_id}}')"
                                >
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '#{{$form_id}}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if($item->parent_id == 2)
                    <div class="col-md-3 mb-3 formula-col">
                        <label>Pemisah</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <select 
                                    class="form-control formula-input mr-2 separator" 
                                    data-type="select" 
                                    parent-id="{{$item->parent_id}}"
                                    form-id="{{$form_id}}"
                                    onchange="reviewCode('#{{$form_id}}')"
                                >
                                    <option selected data-id="{{$item->id}}" value="{{$item->value}}">{{$item->name}}</option>
                                </select>
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '#{{$form_id}}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if($item->parent_id == 3)
                    <div class="col-md-3 mb-3 formula-col">
                        <label>Tanggal</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <select 
                                    class="form-control formula-input mr-2 date" 
                                    data-type="select" 
                                    parent-id="{{$item->parent_id}}"
                                    form-id="{{$form_id}}"
                                    onchange="reviewCode('#{{$form_id}}')"
                                >
                                    <option selected data-id="{{$item->id}}" value="{{$item->value}}">{{$item->name}}</option>
                                </select>
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '#{{$form_id}}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if($item->parent_id == 4)
                    <div class="col-md-3 mb-3 formula-col">
                        <label>Kode Cabang</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <input 
                                    disabled 
                                    value="[KODE CABANG]" 
                                    type="text" 
                                    class="form-control formula-input kode-cabang mr-3" 
                                    placeholder="Kode Cabang" 
                                    data-type="text" 
                                    data-id="{{$item->id}}"
                                    onchange="reviewCode('#{{$form_id}}')"
                                >
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '#{{$form_id}}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if($item->parent_id == 5)
                    <div class="col-md-4 mb-3 formula-col">
                        <label>No Urut</label>
                        <div class="formula-element">
                            <div class="formula-item d-flex align-items-center justify-content-between">
                                <input 
                                    type="number" 
                                    class="form-control formula-input no-urut mr-1" 
                                    placeholder="Mulai Dari" 
                                    data-type="number" 
                                    data-id="{{$item->id}}"
                                    value="{{$item->value}}"
                                    onchange="reviewCode('#{{$form_id}}')"
                                    style="width:40%;"
                                >
                                <div class="position-relative" style="width: 50%;">
                                    <span 
                                        data-width="20" data-height="20" 
                                        class="iconify" 
                                        data-icon="akar-icons:question"
                                        style="position:absolute;top: 50%;left: 6px;transform:translateY(-50%);z-index:100;"
                                        onmouseover="tooltipHover(this, 1)"
                                        onmouseout="tooltipHover(this, 0)"
                                    >
                                    </span>
                                    <select class="form-control is_continue_head" name="is_continue_head">
                                        <option @if($item->is_continue_head == 1) selected @endif value="1">Urut Pusat</option>
                                        <option @if($item->is_continue_head == 0) selected @endif value="0">Urut Per Cabang</option>
                                    </select>
                                </div>
                                <span style="color:#C9C9C9;" onclick="removeFormula(this, '#{{$form_id}}')" data-width="40" data-height="40" class="iconify" data-icon="ci:off-close"></span>
                            </div>
                            <div class="no-tooltip-wrapper"></div>
                        </div>
                    </div>
                    @endif
                @endif
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-success py-2 px-4">Simpan</button>
            </div>
        </div>
    </div>
</form>
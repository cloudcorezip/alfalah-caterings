<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">{{$title}}</h4>
    <span style="color:#979797">Atur konfigurasi {{strtolower($title)}} pada form dibawah ini.</span>
    <hr>
</div>
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="row">

        <div class="col-lg-12">
            <div class="row mt-3">

                <div class="col-lg-12 mb-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                               @if(!is_null($sfa))
                               @if($sfa->allow_change_sale_price==1)
                               checked
                               @endif
                               @endif
                               id="allow_change_sale_price">
                        <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                            Izinkan sales merubah harga jual produk
                        </label>
                    </div>
                </div>


                <div class="col-lg-12 mb-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                               @if(!is_null($sfa))
                               @if($sfa->allow_add_discount==1)
                               checked
                               @endif
                               @endif

                               id="allow_add_discount" autocomplete="off">
                        <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                            Izinkan sales memberikan diskon
                        </label>
                    </div>
                </div>

                <div class="col-lg-12 mb-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                               @if(!is_null($sfa))
                               @if($sfa->is_use_sale_target==1)
                               checked
                               @endif
                               @endif

                               id="use_sale_target" autocomplete="off">
                        <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                            Gunakan pengaturan target masing-masing sales
                        </label>
                    </div>
                </div>
                <div class="col-lg-12 mb-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                               @if(!is_null($sfa))
                               @if($sfa->is_use_visit_schedule==1)
                               checked
                               @endif
                               @endif
                               id="use_visit_schedule" autocomplete="off">
                        <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                            Gunakan atau buat jadwal kunjungan masing-masing sales
                        </label>
                    </div>
                </div>
                <div class="col-lg-12 mb-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                               @if(!is_null($sfa))
                               @if($sfa->allow_created_invoice==1)
                               checked
                               @endif
                               @endif
                               id="allow_created_invoice" autocomplete="off">
                        <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                            Izinkan sales membuat faktur penjualan
                        </label>
                    </div>
                </div>
                <div class="col-lg-12 mb-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                               @if(!is_null($sfa))
                               @if($sfa->allow_created_po==1)
                               checked
                               @endif
                               @endif
                               id="allow_created_po" autocomplete="off">
                        <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                            Izinkan sales membuat PO (Pre Order) Penjualan
                        </label>
                    </div>
                </div>

                <div class="col-lg-12 mb-2">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox"
                               @if(!is_null($sfa))
                               @if($sfa->allow_change_customer==1)
                               checked
                               @endif
                               @endif
                               id="allow_change_customer" autocomplete="off">
                        <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                            Izinkan sales mengubah data customer
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-left">
            <hr>
            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
            @if(is_null($sfa))
            <input type="hidden" value="0"  name="allow_change_sale_price" class="allow_change_sale_price">
            <input type="hidden" value="0"  name="allow_add_discount" class="allow_add_discount">
            <input type="hidden" value="0"  name="is_use_sale_target" class="is_use_sale_target">
            <input type="hidden" value="0" name="is_use_visit_schedule" class="is_use_visit_schedule">
            <input type="hidden" value="0" name="allow_created_po" class="allow_created_po">
            <input type="hidden" value="0" name="allow_created_invoice" class="allow_created_invoice">
            <input type="hidden" value="0" name="allow_change_customer" class="allow_change_customer">
            @else
                <input type="hidden" value="{{$sfa->allow_change_sale_price}}"  name="allow_change_sale_price" class="allow_change_sale_price">
                <input type="hidden" value="{{$sfa->allow_add_discount}}"  name="allow_add_discount" class="allow_add_discount">
                <input type="hidden" value="{{$sfa->is_use_sale_target}}"  name="is_use_sale_target" class="is_use_sale_target">
                <input type="hidden" value="{{$sfa->is_use_visit_schedule}}" name="is_use_visit_schedule" class="is_use_visit_schedule">
                <input type="hidden" value="{{$sfa->allow_created_po}}" name="allow_created_po" class="allow_created_po">
                <input type="hidden" value="{{$sfa->allow_created_invoice}}" name="allow_created_invoice" class="allow_created_invoice">
                <input type="hidden" value="{{$sfa->allow_change_customer}}" name="allow_change_customer" class="allow_change_customer">
            @endif

            <button class="btn btn-success" type="submit">
                <i class="fa fa-save mr-1"></i>
                Simpan</button>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');

                ajaxTransfer("{{route('merchant.toko.setting.save-sfa-config')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })

        })

        $("#allow_change_sale_price").change(function (){
            if(this.checked) {
                $(".allow_change_sale_price").val(1)


            }else{
                $(".allow_change_sale_price").val(0)
            }
        })
        $("#allow_add_discount").change(function (){
            if(this.checked) {
                $(".allow_add_discount").val(1)


            }else{
                $(".allow_add_discount").val(0)
            }
        })
        $("#allow_change_sale_price").change(function (){
            if(this.checked) {
                $(".allow_change_sale_price").val(1)


            }else{
                $(".allow_change_sale_price").val(0)
            }
        })
        $("#use_sale_target").change(function (){
            if(this.checked) {
                $(".is_use_sale_target").val(1)


            }else{
                $(".is_use_sale_target").val(0)
            }
        })

        $("#use_visit_schedule").change(function (){
            if(this.checked) {
                $(".is_use_visit_schedule").val(1)


            }else{
                $(".is_use_visit_schedule").val(0)
            }
        })

        $("#allow_created_invoice").change(function (){
            if(this.checked) {
                $(".allow_created_invoice").val(1)


            }else{
                $(".allow_created_invoice").val(0)
            }
        })

        $("#allow_created_po").change(function (){
            if(this.checked) {
                $(".allow_created_po").val(1)


            }else{
                $(".allow_created_po").val(0)
            }
        })

        $("#allow_change_customer").change(function (){
            if(this.checked) {
                $(".allow_change_customer").val(1)


            }else{
                $(".allow_change_customer").val(0)
            }
        })


    </script>
</form>


<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">Profil Usaha</h4>
    <span style="color:#979797">Masukan informasi usahamu seperti nama usaha,kontak,alamat dan detail lainnya.</span>
    <hr>
</div>
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12 text-center">
                    @if(!is_null($data->image))
                        <img src="{{env('S3_URL').$data->image}}" width="50%" alt="foto profil" style="max-height:200px; object-fit:cover;" class="img-fluid img-thumbnail">
                    @else
                        <img src="{{asset('public/backend/img/no-user.png')}}" alt="foto profil" class="img-fluid img-thumbnail">
                    @endif
                    <div class="form-group text-left mt-3">
                        <label class="font-weight-bold">Foto</label>
                        <input type="file" class="form-control form-control-sm" name="image">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="font-weight-bold">Nama Usaha Kamu</label>
                        <input type="text" class="form-control" name="name"  value="{{$data->name}}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="font-weight-bold">Email</label>
                        <input type="text" class="form-control" name="email_merchant"  value="{{$data->email_merchant}}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="font-weight-bold">No Telephone</label>
                        <input type="number" class="form-control" name="phone_merchant"  value="{{$data->phone_merchant}}" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="font-weight-bold">Kategori Usaha</label>
                        <select id="typeBisnis" class="form-control form-control-sm" name="md_business_category_id" required>
                            @foreach($type as $category)
                                <optgroup label="{{$category->name}}" style="font-weight: bold">
                                    @foreach($category->getChild as $c)
                                        <option value="{{$c->id}}" @if($data->md_business_category_id==$c->id) selected @endif>{{$c->name}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="font-weight-bold">Deskripsi</label>
                        <textarea name="description" class="form-control">{{$data->description}}</textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="font-weight-bold">Alamat</label>
                        <textarea name="address" class="form-control">{{$data->address}}</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="font-weight-bold">Provinsi</label>
                        <select id="province" class="form-control" name="province" required>
                            <option disabled @if (is_null($data->md_district_id)) selected @endif value="">--Pilih Provinsi--</option>
                            @foreach ($province as $b)
                                <option
                                    @if (!is_null($data->md_district_id))
                                    @if ($b->id==$data->getDistrict->getCity->getProvince->id)
                                    selected
                                    @endif
                                    @endif
                                    value="{{$b->id}}">{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="form-group">
                        <label class="font-weight-bold">Kabupaten</label>
                        <select id="city" class="form-control" name="city" required>
                            <option></option>
                            <option disabled @if (is_null($data->md_district_id)) selected @endif value="">--Pilih Kabupaten--</option>
                            @foreach ($city as $b)

                                @if (!is_null($data->md_district_id))
                                    <option
                                        @if ($b->id==$data->getDistrict->getCity->id)
                                        selected
                                        @endif
                                        value="{{$b->id}}">{{$b->name}}</option>
                                @endif

                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="font-weight-bold">Kecamatan</label>
                        <select id="district" class="form-control" name="md_district_id" required>
                            <option></option>
                            <option disabled @if (is_null($data->md_district_id)) selected @endif value="">--Pilih Kecamatan--</option>
                            @foreach ($district as $b)

                                @if (!is_null($data->md_district_id))
                                    <option
                                        @if ($b->id==$data->getDistrict->id)
                                        selected
                                        @endif
                                        value="{{$b->id}}">{{$b->name}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-right">
            @if(!is_null($data->form_order_url))
                @php
                    $link = url('/form-order');
                    $link .= '/'.$data->form_order_url;
                @endphp
                <button type="button" class="btn-preview py-1 px-2 rounded mr-2" onclick="copyClipboard('{{$link}}')" data-placement="bottom" title="Salin URL Order Form">
                    <i id="fa-copy" title="copied" class="far fa-copy mr-2"></i>
                    <span>Salin URL</span>
                </button>
            @endif
            <button class="btn btn-success" type="submit" >
                <i class="fa fa-save mr-1"></i>
                Simpan</button>
        </div>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='md_user_id' value='{{$data->md_user_id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>

    $(document).ready(function () {

        $('#province').select2();
        $('#typeBisnis').select2();

        $('#city').select2({
            placeholder: '--- Pilih Kabupaten ---'
        });
        $('#district').select2({
            placeholder: '--- Pilih Kecamatan ---'
        });
        $('#province').on('select2:select', function(e){
            if(e.target.value !== ""){
                const rawUrl = "{{route('api.city.get')}}";
                const url = rawUrl +"?province="+e.target.value;

                $('#city option').remove();
                $('#district option').remove();
                $('#district').attr('disabled', 'disabled');
                $.ajax({
                    type:"GET",
                    url: url,
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    success:function(response){
                        let html = '';
                        response.map(item => {
                            if(item.id == -1){
                                html += '<option value="" disabled selected>'+item.text+'</option>';
                            } else {
                                html += '<option value="'+item.id+'">'+item.text+'</option>';
                            }
                        });

                        $('#city').append(html);
                    }
                });
            }
        });

        $('#city').on('select2:select', function(e){
            if(e.target.value !== -1){
                const rawUrl = "{{route('api.district.get')}}";
                const url = rawUrl +"?city="+e.target.value;

                $('#district').removeAttr('disabled');
                $('#district option').remove();
                $.ajax({
                    type:"GET",
                    url: url,
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    success:function(response){
                        let html = '';
                        response.map(item => {
                            if(item.id == -1){
                                html += '<option value="" disabled selected>'+item.text+'</option>';
                            } else {
                                html += '<option value="'+item.id+'">'+item.text+'</option>';
                            }

                        });
                        $('#district').append(html);
                    }
                });
            }
        })

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.profile.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })

    });

    const openConfirm = () => {
        $('#modalSubmit').modal('show')
    }

    $('.btn-preview').tooltip();
    $('#fa-copy').tooltip('hide');
    function copyClipboard(url){
        let copyText = url;
        let el = document.createElement('input');
        document.body.appendChild(el);
        el.value = url;
        el.select();
        document.execCommand("copy");
        document.body.removeChild(el);
        $('#fa-copy').tooltip('show');
        setTimeout(() => {
            $('#fa-copy').tooltip('dispose');
        }, 1000);
    }
</script>

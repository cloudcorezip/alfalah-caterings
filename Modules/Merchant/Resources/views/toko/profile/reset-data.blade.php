<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">Reset Data</h4>
    <span style="color:#979797">Kembalikan pengaturan aplikasi dan data seperti semula</span>
    <hr>
</div>
<div class="card-body">
    <div id="results"></div>
    <form id="form-konten" onsubmit="return false">
        <div class="alert alert-warning text-dark">Perhatian! Jika Anda ingin mereset data disistem kami,maka semua data yang berhubungan dengan keuangan seperti pembelian, persedian dan penjualan akan terhapus.Pastikan anda berhati-hati</div>

        <div class="row">
            <div class="col-md-6 mt-4">
                <h5 class="font-weight-bold" style="color:#515151;">Transaksi Penjualan</h5>
                <span class="text-black-50">Data Transaksi penjualan yang akan direset yaitu data penjualan dan retur penjualan</span>
                <h6 class="font-weight-bold mt-2 ml-4" style="color:#515151;">
                    <i style="color:#72BA6C;" class="fas fa-check-circle mr-2"></i>Penjualan
                </h6>
                <h6 class="font-weight-bold mt-2 ml-4" style="color:#515151;">
                    <i style="color:#72BA6C;" class="fas fa-check-circle mr-2"></i>Retur Penjualan
                </h6>
            </div>
            <div class="col-md-6 mt-4">
                <h5 class="font-weight-bold" style="color:#515151;">Transaksi Pembelian</h5>
                <span class="text-black-50">Data Transaksi penjualan yang akan direset yaitu data pembelian dan retur pembelian</span>
                <h6 class="font-weight-bold mt-2 ml-4" style="color:#515151;">
                    <i style="color:#72BA6C;" class="fas fa-check-circle mr-2"></i>Pembelian
                </h6>
                <h6 class="font-weight-bold mt-2 ml-4" style="color:#515151;">
                    <i style="color:#72BA6C;" class="fas fa-check-circle mr-2"></i>Retur Pembelian
                </h6>
            </div>
            <div class="col-md-6 mt-4">
                <h5 class="font-weight-bold" style="color:#515151;">Keuangan</h5>
                <span class="text-black-50">Data Transaksi penjualan yang akan direset yaitu data jurnal, utang, dan piutang</span>
                <h6 class="font-weight-bold mt-2 ml-4" style="color:#515151;">
                    <i style="color:#72BA6C;" class="fas fa-check-circle mr-2"></i>Jurnal
                </h6>
                <h6 class="font-weight-bold mt-2 ml-4" style="color:#515151;">
                    <i style="color:#72BA6C;" class="fas fa-check-circle mr-2"></i>Utang
                </h6>
                <h6 class="font-weight-bold mt-2 ml-4" style="color:#515151;">
                    <i style="color:#72BA6C;" class="fas fa-check-circle mr-2"></i>Piutang
                </h6>
            </div>
            <div class="col-md-6 mt-4">
                <h5 class="font-weight-bold" style="color:#515151;">Aset</h5>
                <span class="text-black-50">Data Aset yang akan direset yaitu data pengelolaan aset</span>
                <h6 class="font-weight-bold mt-2 ml-4" style="color:#515151;">
                    <i style="color:#72BA6C;" class="fas fa-check-circle mr-2"></i>Pengelolaan Aset
                </h6>
            </div>
        </div>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <div class="row">
            <div class="col-12 text-right">
                <button class="btn btn-danger"><i class="fa fa-trash-alt mr-2"></i> Reset Data</button>
            </div>
        </div>
    </form>
</div>


<script>
    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');

        const isFromGoogle = "{{$user->is_from_google}}"
        const isNullPass = "{{is_null($user->password) ? 1:0}}";

        let html = "";
        if(isFromGoogle == 1){
            if(isNullPass == 0){
                html += `<div class="row">
             <div class="col-md-10 offset-md-1">
                 <div class="col-md-8 offset-md-2 text-center">
                     <img class="mb-3" src="{{asset('public/backend/img/confirm.png')}}" style="width:150px;height:150px;object-fit:contain;"/>
                     <h6 class="text-black-50">Apa anda yakin untuk menghapus/mereset data disistem kami ?</h6>
                 </div>
                 <div class="form-group" id="pass-wrapper">
                     <label>Masukkan Password</label>
                     <div style="position:relative">
                         <input type="password" class="form-control" id="password" placeholder="Password"/>
                         <i
                             id="input-eye1"
                             style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                             class="fas fa-eye">
                         </i>
                     </div>
                     <span id="pass-notif" class="text-danger"></span>
                 </div>
             </div>
         </div>`;
            } else {
                html += `<div class="row">
             <div class="col-md-10 offset-md-1">
                 <div class="col-md-8 offset-md-2 text-center">
                     <img class="mb-3" src="{{asset('public/backend/img/confirm.png')}}" style="width:150px;height:150px;object-fit:contain;"/>
                     <h6 class="text-black-50">Untuk keamanan, silahkan buat password terlebih dahulu !</h6>
                 </div>
                 <div class="form-group" id="pass-wrapper">
                     <label>Buat Password</label>
                     <div class="mb-1" style="position:relative">
                         <input type="password" class="form-control" id="password" placeholder="Password"/>
                         <i
                             id="input-eye1"
                             style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                             class="fas fa-eye">
                         </i>
                     </div>
                 </div>
                 <div class="form-group" id="conf-pass-wrapper">
                     <label>Konfirmasi Password</label>
                     <div style="position:relative">
                         <input type="password" class="form-control" id="confirm_password" placeholder="Konfirmasi Password"/>
                         <i
                             id="input-eye2"
                             style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                             class="fas fa-eye">
                         </i>
                     </div>
                 </div>
             </div>
         </div>`;
            }
        } else {
            html += `<div class="row">
         <div class="col-md-10 offset-md-1">
             <div class="col-md-8 offset-md-2 text-center">
                 <img class="mb-3" src="{{asset('public/backend/img/confirm.png')}}" style="width:150px;height:150px;object-fit:contain;"/>
                 <h6 class="text-black-50">Apa anda yakin untuk menghapus/mereset data disistem kami ?</h6>
             </div>
             <div class="form-group" id="pass-wrapper">
                 <label>Masukkan Password</label>
                 <div style="position:relative">
                     <input type="password" class="form-control" id="password" placeholder="Password"/>
                     <i
                         id="input-eye1"
                         style="position:absolute;top:50%; transform:translateY(-50%); right:20px; font-size:14px;cursor:pointer;"
                         class="fas fa-eye">
                     </i>
                 </div>
                 <span id="pass-notif" class="text-danger"></span>
             </div>
         </div>
     </div>`;
        }

        modalConfirm("Konfirmasi", html, function () {
            const errorValidation = [];

            if($('#password').val() == ''){
                errorValidation.push({
                    field:"#pass-wrapper",
                    message:'<span class="text-danger validate-notif">Password tidak boleh kosong !</span>'
                });
            }

            if(isNullPass == 1){
                if($('#confirm_password').val() == ''){
                    errorValidation.push({
                        field:"#conf-pass-wrapper",
                        message:'<span class="text-danger validate-notif">Konfirmasi password tidak boleh kosong !</span>'
                    });
                } else if($('#password').val() != $('#confirm_password').val()){
                    errorValidation.push({
                        field:"#conf-pass-wrapper",
                        message:'<span class="text-danger validate-notif">Konfirmasi password tidak sesuai !</span>'
                    });
                }
            }

            if(errorValidation.length > 0){
                $('.validate-notif').remove();
                errorValidation.forEach(item => {
                    $(item.field).append(item.message);
                })
            } else {
                data.append('password', $('#password').val());
                ajaxTransfer("{{route('merchant.reset-data.reset')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            }
        })
    });

    $(document).on('click', '#input-eye1', function(){
        if($('#input-eye1').hasClass('fas fa-eye')){
            $('#password').attr('type', 'text');
            $('#input-eye1').removeClass('fas fa-eye');
            $('#input-eye1').addClass('far fa-eye-slash');
        } else {
            $('#password').attr('type', 'password');
            $('#input-eye1').removeClass('far fa-eye-slash');
            $('#input-eye1').addClass('fas fa-eye');
        }

    });

    $(document).on('click', '#input-eye2', function(){
        if($('#input-eye2').hasClass('fas fa-eye')){
            $('#confirm_password').attr('type', 'text');
            $('#input-eye2').removeClass('fas fa-eye');
            $('#input-eye2').addClass('far fa-eye-slash');
        } else {
            $('#confirm_password').attr('type', 'password');
            $('#input-eye2').removeClass('far fa-eye-slash');
            $('#input-eye2').addClass('fas fa-eye');
        }

    });

</script>

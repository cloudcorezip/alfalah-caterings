<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">Metode Pembayaran</h4>
    <span style="color:#979797">Pilih metode pembayaran yang digunakan.</span>
    <hr>
</div>
<div class="row mb-3">
    <div class="col-md-12">
        <h5 class="font-weight-bold" style="color:#323232;">E-Wallet </h5>
        @if(is_null($row))
            <a href="{{route('qris.add')}}" class="btn btn-xs btn-outline-success text-success "> Aktifkan Pembayaran Digital</a>
        @endif
        <i>(Biaya administrasi per transaksi sebesar 1,6%)</i>
    </div>
</div>
<div class="row">
    @if(is_null($row))

    @else
        @foreach($eWallet as $key => $item)
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-7">
                                <img src="{{env('S3_URL')}}{{$item->icon}}" alt="{{$item->icon}}" style="width:230px;height:100px;object-fit:contain;">
                            </div>
                            <div class="col-md-5 d-flex align-items-center justify-content-end">
                                <div class="custom-control custom-switch">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input check-ewallet"
                                        id="{{$item->id}}_{{$item->id}}"
                                        name="bank_id"
                                        value="{{$item->id}}"
                                        style="transform:scale(2)"
                                        @if($row->is_active==0) disabled @endif

                                    >
                                    <label class="custom-control-label" for="{{$item->id}}_{{$item->id}}"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif

</div>
<div class="row mb-3">
    <div class="col-md-12">
        <h5 class="font-weight-bold" style="color:#323232;">Virtual Account
            @if(is_null($row))
                <a href="{{route('qris.add')}}" class="btn btn-xs btn-outline-success text-success "> Aktifkan Pembayaran Digital</a>
            @endif
        </h5>
        <i>(Biaya administrasi per transaksi sebesar Rp 5000)</i>
    </div>
</div>
<div class="row">
    @if(is_null($row))

    @else
        @foreach($vaBank as $key => $item)
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            {{-- @dd($vaBank) --}}
                            <div class="col-md-7">
                                <img src="{{env('S3_URL')}}{{$item->getBank->icon}}" alt="{{$item->getBank->icon}}" style="width:230px;height:100px;object-fit:contain;">
                            </div>
                            <div class="col-md-5 d-flex align-items-center justify-content-end">
                                <div class="custom-control custom-switch">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input check-bank"
                                        id="{{$item->id}}_5"
                                        name="bank_id"
                                        value="{{$item->id}}"
                                        style="transform:scale(2)"
                                        @if($row->is_active==0) disabled @endif
                                    >
                                    <label class="custom-control-label" for="{{$item->id}}_5"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
<div class="row mb-3">
    <div class="col-md-12">
        <h5 class="font-weight-bold" style="color:#323232;">QRIS</h5>
        @if(is_null($row))
            <a href="{{route('qris.add')}}" class="btn btn-xs btn-outline-success text-success "> Aktifkan Pembayaran Digital</a>
        @endif
        <i>(Biaya administrasi per transaksi sebesar 0,8%)</i>
    </div>
</div>
<div class="row">
    @if(is_null($qris))

    @else
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        {{-- @dd($qris) --}}
                        <div class="col-md-7">
                            <img src="{{env('S3_URL')}}public/transactionType/qris.png" alt="public/transactionType/qris.png" style="width:230px;height:100px;object-fit:contain;">
                        </div>
                        <div class="col-md-5 d-flex align-items-center justify-content-end">
                            <div class="custom-control custom-switch">
                                <input
                                    type="checkbox"
                                    class="custom-control-input check-qris"
                                    id="{{$qris->id}}_4"
                                    name="bank_id"
                                    value="{{$qris->id}}"
                                    style="transform:scale(2)"

                                    @if($row->is_active==0) disabled @endif

                                >
                                <label class="custom-control-label" for="{{$qris->id}}_4"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>


<div class="row mb-3">
    <div class="col-md-12">
        <h5 class="font-weight-bold" style="color:#323232;">Bank Transfer</h5>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3><a onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.bank-transfer.add')}}"><i class="fas fa-plus"></i> Tambah Bank Transfer </a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($bankTransfer as $key => $item)
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9 d-flex">
                            <h2 class="text-primary"><a onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.bank-transfer.add',['id'=>$item->id])}}">{{$item->nama_bank."-".$item->bank_account_number}}</a></h2>
                        </div>
                        <div class="col-md-3 d-flex align-items-center justify-content-end">
                            <div class="custom-control custom-switch">
                                <input
                                    type="checkbox"
                                    class="custom-control-input check-bank-tf"
                                    id="{{$item->md_va_bank_id.'_13_'.$item->id}}"
                                    name="bank_id"
                                    value="{{$item->id}}"
                                    style="transform:scale(2)"
                                >
                                <label class="custom-control-label" for="{{$item->md_va_bank_id.'_13_'.$item->id}}"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="row mb-3">
    <div class="col-md-12">
        <h5 class="font-weight-bold" style="color:#323232;">Tunai</h5>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-7">
                        <h2>Tunai</h2>
                    </div>
                    <div class="col-md-5 d-flex align-items-center justify-content-end">
                        <div class="custom-control custom-switch">
                            <input
                                type="checkbox"
                                class="custom-control-input check-cash"
                                id="2_2"
                                name="bank_id"
                                value="{{2}}"
                                style="transform:scale(2)"
                            >
                            <label class="custom-control-label" for="2_2"></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        getPayment()
    })

    function getPayment() {
        ajaxTransfer('{{route('merchant.toko.payment-method.get-payment')}}', {}, function(response) {

            if(response.data.paymentMethod.length>0)
            {
                $("#add-bank").show()
                var data=response.data.paymentMethod

                data.forEach(item=>{
                    if(item.transaction_type_id==13){
                        if(item.status==1){
                            $("#"+item.md_va_bank_id+"_"+item.transaction_type_id+"_"+item.id).prop('checked', true)
                        }else{
                            $("#"+item.md_va_bank_id+"_"+item.transaction_type_id+"_"+item.id).prop('checked', false)
                        }
                    }else{
                        if(item.status==1){
                            $("#"+item.md_va_bank_id+"_"+item.transaction_type_id).prop('checked', true)
                        }else{
                            $("#"+item.md_va_bank_id+"_"+item.transaction_type_id).prop('checked', false)
                        }
                    }
                })
            }else{
                $("#add-bank").hide()
            }
        }, false, false)
    }
</script>
<script>

    $(".check-bank").change(function() {
        if(this.checked) {
            $.ajax({
                type: 'POST',
                data: {
                    md_va_bank_id: $(this).val(),
                    is_active:1,
                    type:5,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.save')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }else{
            $.ajax({
                type: 'POST',
                data: {
                    md_va_bank_id: $(this).val(),
                    is_active:0,
                    type:5,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.save')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }
    });
    $(".check-cash").change(function() {
        if(this.checked) {
            $.ajax({
                type: 'POST',
                data: {
                    md_va_bank_id: $(this).val(),
                    is_active:1,
                    type:2,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.save')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }else{
            $.ajax({
                type: 'POST',
                data: {
                    md_va_bank_id: $(this).val(),
                    is_active:0,
                    type:2,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.save')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()                        }
            })
        }
    });
    $(".check-qris").change(function() {
        if(this.checked) {
            $.ajax({
                type: 'POST',
                data: {
                    md_va_bank_id: $(this).val(),
                    is_active:1,
                    type:4,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.save')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }else{
            $.ajax({
                type: 'POST',
                data: {
                    md_va_bank_id: $(this).val(),
                    is_active:0,
                    type:4,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.save')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }
    });
    $(".check-bank-tf").change(function() {
        if(this.checked) {
            $.ajax({
                type: 'POST',
                data: {
                    id: $(this).val(),
                    is_active:1,
                    type:13,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.update-bank-status')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }else{
            $.ajax({
                type: 'POST',
                data: {
                    id: $(this).val(),
                    is_active:0,
                    type:13,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.update-bank-status')}}",
                success:function(response){

                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }
    });
    $(".check-ewallet").change(function() {
        if(this.checked) {
            $.ajax({
                type: 'POST',
                data: {
                    md_va_bank_id: $(this).val(),
                    is_active:1,
                    type:$(this).val(),
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.save')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }else{
            $.ajax({
                type: 'POST',
                data: {
                    md_va_bank_id: $(this).val(),
                    is_active:0,
                    type:$(this).val(),
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.payment-method.save')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getPayment()
                }
            })
        }
    });
</script>

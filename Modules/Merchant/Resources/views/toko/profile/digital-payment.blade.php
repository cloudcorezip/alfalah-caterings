<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">Akun Bank Penarikan</h4>
    <span style="color:#979797">Masukan nama dan nomor rekening serta bank untuk penarikan.</span>
    <hr>
</div>
<div class="card-body">
    @if($row <= 0)
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="{{asset('public/backend/img/credit-wallet-icon.png')}}" alt="wallet" class="img-fluid d-block mb-4 mx-auto">
                <button style="border-radius:5px;" href="" class="btn btn-order__online">
                    <a style="color:inherit" href="{{route('qris.add')}}">Aktivasi Pembayaran Digital</a>
                </button>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    @if ($row<=0)
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-lg-2">
                                <a class="btn btn-success btn-rounded btn-sm btn-block" href="{{route('qris.add')}}"><i class="fa fa-plus"></i> Ajukan QRIS</a>
                            </div>

                        </div>
                    @endif

                    <div id="output-sale-order">
                        @include('merchant::qris.list')
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

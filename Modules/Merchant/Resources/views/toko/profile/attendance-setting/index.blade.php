<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">{{$title}}</h4>
    <span style="color:#979797">Atur konfigurasi {{strtolower($title)}} pada form dibawah ini.</span>
    <hr>
</div>
  @if($page=='attendance')
      <div id="result-form-konten"></div>
      <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

      <div class="row">

            <div class="col-lg-12">
                <div class="row mt-3">

                    <div class="col-lg-12 mb-2">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox"
                                   @if(!is_null($presensi))
                                        @if($presensi->is_allowed_shift_change==1)
                                            checked
                                        @endif
                                  @endif
                            id="allow_shift_change">
                            <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                                Izinkan permintaan pergantian shift
                            </label>
                        </div>
                    </div>


                    <div class="col-lg-12 mb-2">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox"
                                   @if(!is_null($presensi))
                                       @if($presensi->is_allowed_shift_other_time==1)
                                       checked
                                       @endif
                                   @endif

                                   id="allow_shift_other_time" autocomplete="off">
                            <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                                Izinkan presensi diluar shift
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-12 mb-2">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox"
                                   @if(!is_null($presensi))
                                   @if($presensi->is_count_leave_from_date_joining==1)
                                   checked
                                   @endif
                                   @endif

                                   id="count_leave_form_date_joining" autocomplete="off">
                            <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                                Hitung cuti dari tanggal bergabung
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-12 mb-2">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox"
                                   @if(!is_null($presensi))
                                   @if($presensi->is_use_rule_leave==1)
                                   checked
                                   @endif
                                   @endif

                                   id="count_is_use_rule_leave" autocomplete="off">
                            <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 text-wrap">
                                Gunakan perhitungan izin/cuti di pengaturan
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-group my-3 mr-0 mr-lg-12">
                    <label class="f-14 text-dark-grey mb-12" data-label="" for="alert_after_status">Status Pengingat Presensi

                    </label>

                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input check-bank"
                            id="is_active_reminder_status1"
                            name="is_active_reminder_status1"
                            style="transform:scale(2)"
                            @if(!is_null($presensi))
                                @if($presensi->is_active_reminder_status==1)
                                checked
                                @endif
                            @endif
                        >
                        <label class="custom-control-label" for="is_active_reminder_status1"></label>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 alert_after_box" id="hidden_remember_time">
                <div class="form-group my-3 mr-0 mr-lg-2 mr-md-2">
                    <label class="f-14 text-dark-grey mb-12" data-label="true" for="alert_after"> Pengingat Presensi (menit)
                    </label>
                    <input type="number" class="form-control height-35 f-14" placeholder=""
                           @if(!is_null($presensi))
                           @if($presensi->is_active_reminder_status==1)
                               value="{{$presensi->reminder_attendance}}"
                           @else
                           value="0"
                            @endif

                           @else
                               value="0"
                               @endif
                           name="reminder_attendance" min="0" autocomplete="off">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-left">
                <hr>
                @if(!is_null($presensi))
                    <input type="hidden" name="is_allowed_shift_change" id="is_allowed_shift_change" value="{{$presensi->is_allowed_shift_change}}">
                    <input type="hidden" name="is_allowed_shift_other_time" id="is_allowed_shift_other_time" value="{{$presensi->is_allowed_shift_other_time}}">
                    <input type="hidden" name="is_active_reminder_status" id="is_active_reminder_status" value="{{$presensi->is_active_reminder_status}}">
                    <input type="hidden" name="is_count_leave_form_date_joining" id="is_count_leave_form_date_joining" value="{{$presensi->is_count_leave_from_date_joining}}">
                    <input type="hidden" name="is_use_rule_leave" id="is_count_use_rule_leave" value="{{is_null($presensi->is_use_rule_leave)?0:$presensi->is_use_rule_leave}}">


                @else
                    <input type="hidden" name="is_allowed_shift_change" id="is_allowed_shift_change" value="0">
                    <input type="hidden" name="is_allowed_shift_other_time" id="is_allowed_shift_other_time" value="0">
                    <input type="hidden" name="is_active_reminder_status" id="is_active_reminder_status" value="0">
                    <input type="hidden" name="is_count_leave_form_date_joining" id="is_count_leave_form_date_joining" value="0">
                    <input type="hidden" name="is_use_rule_leave" id="is_count_use_rule_leave" value="0">




                @endif
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <button class="btn btn-success" type="submit">
                    <i class="fa fa-save mr-1"></i>
                    Simpan</button>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                @if(is_null($presensi))
                $("#hidden_remember_time").hide();

                @else
                    @if($presensi->is_active_reminder_status==1)
                        $("#hidden_remember_time").show();
                     @else
                     $("#hidden_remember_time").hide();
                    @endif
                @endif
                $('#form-konten').submit(function () {
                    var data = getFormData('form-konten');

                    ajaxTransfer("{{route('merchant.toko.setting.save-attendance')}}", data, function (response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                    });
                })

            })

            $("#is_active_reminder_status1").change(function (){
                if(this.checked) {
                    $("#hidden_remember_time").show()
                    $("#is_active_reminder_status").val(1)


                }else{
                    $("#hidden_remember_time").hide()
                    $("#is_active_reminder_status").val(0)
                }
            })

            $("#allow_shift_change").change(function (){
                if(this.checked) {
                    $("#is_allowed_shift_change").val(1)

                }else{
                    $("#is_allowed_shift_change").val(0)
                }
            })


            $("#allow_shift_other_time").change(function (){
                if(this.checked) {
                    $("#is_allowed_shift_other_time").val(1)

                }else{
                    $("#is_allowed_shift_other_time").val(0)
                }
            })

            $("#count_leave_form_date_joining").change(function (){
                if(this.checked) {
                    $("#is_count_leave_form_date_joining").val(1)

                }else{
                    $("#is_count_leave_form_date_joining").val(0)
                }
            })

            $("#count_is_use_rule_leave").change(function (){
                if(this.checked) {
                    $("#is_count_use_rule_leave").val(1)

                }else{
                    $("#is_count_use_rule_leave").val(0)
                }
            })


        </script>
      </form>
  @else
      <div class="row">
          <div class="col-lg-auto col-md-12">
              <a class="btn btn-success btn-rounded btn-sm py-2 px-4 btn-block mb-4" onclick="loadModal(this)" target="{{route('merchant.toko.setting.add-shift')}}"><i class="fa fa-plus mr-2"></i> Tambah</a>
          </div>
          <div class="col-md-12">
              <div class="table table-responsive">
                  <table class="table table-custom" width="100%" cellspacing="0">
                      <thead>
                      <tr>
                          <th>Nama Shift</th>
                          <th>Waktu</th>
                          <th>Keterangan</th>
                          <th>Opsi</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($presensi as $item)
                          <tr>
                              <td>
                                  <span class="badge badge-info f-12 p-1" style="background-color: {{$item->color}}">{{$item->name}}</span>
                              </td>
                              <td>
                                  Awal Shift : {{$item->start_shift}}<br>
                                  Setengah Hari: {{ (is_null($item->half_shift))?'-':$item->half_shift  }}<br>
                                  Akhir Shift: {{$item->end_shift}}
                              </td>
                              <td>
                                  Maksimal Keterlambatan : {{$item->max_late}}<br>
                                  Maksimal Presensi per Hari : {{$item->max_allowed_attendance_per_day}}<br>
                                  Berlaku pada Hari:
                                  @if(!empty(json_decode($item->work_day)))
                                      @foreach(json_decode($item->work_day) as $i =>$w)
                                          @if($w->is_selected==true)
                                              @if($i==0)
                                                  {{$w->day}}
                                              @else
                                                  ,{{$w->day}}
                                              @endif

                                          @endif
                                      @endforeach
                                  @endif
                              </td>
                              <td>
                                  @if($item->is_default==1)
                                      <a  onclick='loadModal(this)' target='{{route('merchant.toko.setting.add-shift',['id'=>$item->id])}}' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                          <span class='fa fa-edit' style='color: white'></span>
                                      </a>

                                  @else
                                      <a  onclick='loadModal(this)' target='{{route('merchant.toko.setting.add-shift',['id'=>$item->id])}}' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                          <span class='fa fa-edit' style='color: white'></span>
                                      </a>

                                      <a onclick='deleteData({{$item->id}})' class='btn btn-xs btn-delete-xs btn-rounded'>
                                          <span class='fa fa-trash-alt' style='color: white'></span>
                                      </a>
                                  @endif
                              </td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      <script>
          function deleteData(id) {
              var data = new FormData();
              data.append('id', id);

              modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                  ajaxTransfer("{{route('merchant.toko.setting.delete-shift')}}", data, function (response){
                      var data = JSON.parse(response);
                      toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                  });
              })
          }
      </script>
    @endif


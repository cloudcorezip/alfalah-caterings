<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal form-konten' backdrop="">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Keterangan <small style="color: red">*</small></label>
                <input type="text" class="form-control" name="name" placeholder="Keterangan" value="{{$data->name}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold d-block">Label Warna  <small style="color: red">*</small></label>
                <div class="d-flex align-items-center">
                    <input type="color" id="theme-pallete" class="mr-3" style="height: 40px;border: 1px solid #fefefe" value="{{$data->color}}">
                    <input type="text" class="form-control" name="color" id="theme" value="{{$data->color}}" required>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Max Hari <small style="color: red">*</small></label>
                <input type="number" class="form-control" name="max_day" placeholder="Max Hari" value="{{is_null($data->max_day)?0:$data->max_day}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Per Bulan/Tahun <small style="color: red">*</small></label>
                <select class="form-control selectalldata" name="is_per_month" >
                    <option value="1" @if($data->is_per_month==1) selected @endif>Per Bulan</option>
                    <option value="0" @if($data->is_per_month==0) selected @endif>Per Tahun</option>
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Mendapatkan Gaji ? <small style="color: red">*</small></label>
                <select class="form-control selectalldata" name="is_with_paid" id="is_paid">
                    <option value="1" @if($data->is_with_paid==1) selected @endif>Ya</option>
                    <option value="0" @if($data->is_with_paid==0) selected @endif>Tidak</option>
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Status Presensi<small style="color: red">*</small></label>
                <select class="form-control selectalldata" name="type_of_attendance" >
                    <option value="I" @if($data->type_of_attendance=='I') selected @endif>Izin</option>
                    <option value="S" @if($data->type_of_attendance=='S') selected @endif>Sakit</option>
                </select>
            </div>
        </div>

        <div class="col-md-6" id="tipe-cut">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Tipe Potong Gaji<small style="color: red">*</small></label>
                <select class="form-control selectalldata" name="type_of_salary_cut" id="salary-cut">
                    <option value="1" @if($data->type_of_salary_cut==1) selected @endif>Flat</option>
                    <option value="2"@if($data->type_of_salary_cut==2) selected @endif>Persentase</option>
                </select>
            </div>
        </div>

        <div class="col-md-6" id="persentase">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Nilai Pemotongan Gaji</label>
                <input type="number" class="form-control" name="salary_cut_value" placeholder="Max Hari" value="{{is_null($data->salary_cut_value)?0:$data->salary_cut_value}}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Berlaku untuk karyawan yg bergabung lebih dari 1 tahun<small style="color: red">*</small></label>
                <select class="form-control selectalldata" name="is_for_employee_over_one_year" >
                    <option value="0" @if($data->is_for_employee_over_one_year==0) selected @endif>Tidak</option>
                    <option value="1" @if($data->is_for_employee_over_one_year==1) selected @endif>Ya</option>
                </select>
            </div>
        </div>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>

    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {

        $(".selectalldata").select2()
        @if($data->is_with_salary_cut==1 && $data->type_of_salary_cut==2)
            $("#persentase").show();
        @else
        $("#persentase").hide();
        @endif
        @if($data->is_with_paid==1)
        $("#tipe-cut").hide();
        @else
        $("#tipe-cut").show();
        @endif
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.hr.leave-setting.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah atau mengubah data izin/cuti silahkan isi inputan dibawah ini!</span>
                                    </div>`);
    });

    $('#theme-pallete').on('change', function(){
        let colorVal = $(this).val();
        $('#theme').val(colorVal);
    });

    $('#theme').on('change', function(){
        let colorVal = $(this).val();
        $('#theme-pallete').val(colorVal);
    });


    $('#salary-cut').on('change', function(){
      if($("#salary-cut").val()==2){
          $("#persentase").show();
      }else{
          $("#persentase").hide();
      }
    });

    $('#is_paid').on('change', function(){
        if($("#is_paid").val()==1){
            $("#persentase").hide();
            $("#tipe-cut").hide();
        }else{
            $("#tipe-cut").show();
        }
    });


</script>

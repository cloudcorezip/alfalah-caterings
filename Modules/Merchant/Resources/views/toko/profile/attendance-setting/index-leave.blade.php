<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">{{$title}}</h4>
    <span style="color:#979797">Atur konfigurasi {{strtolower($title)}} pada form dibawah ini.</span>
    <hr>
</div>

<div class="row">
    <div class="col-lg-auto col-md-12">
        <a class="btn btn-success btn-rounded btn-sm py-2 px-4 btn-block mb-4" onclick="loadModal(this)" target="{{route('merchant.hr.leave-setting.add')}}"><i class="fa fa-plus mr-2"></i> Tambah</a>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-custom table-hover" id="table-data-asset-category" width="100%" cellspacing="0">
                <thead>
                <tr>
                    @foreach($tableColumns as $key =>$item)
                        <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
            ajaxTransfer("{{route('merchant.hr.leave-setting.delete')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    }
    function reloadDataTable(isReload=0)
    {
        ajaxDataTable('#table-data-asset-category', 1, "{{route('merchant.hr.leave-setting.datatable')}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ],0);
    }
    $(document).ready(function() {
        reloadDataTable(0);
    })

</script>

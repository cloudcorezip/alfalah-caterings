<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal form-konten' backdrop="">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Nama Shift <small style="color: red">*</small></label>
                <input type="text" class="form-control" name="name" placeholder="Nama Shift" value="{{$data->name}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="font-weight-bold d-block">Label Warna  <small style="color: red">*</small></label>
                <div class="d-flex align-items-center">
                    <input type="color" id="theme-pallete" class="mr-3" style="height: 40px;border: 1px solid #fefefe" value="{{$data->color}}">
                    <input type="text" class="form-control" name="color" id="theme" value="{{$data->color}}" required>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Awal Shift<small style="color: red">*</small></label>
                <input type="text" class="form-control time_shift" name="start_shift" placeholder="Awal Shift" value="{{\Carbon\Carbon::parse($data->start_shift)->format('H:i')}}" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Akhir Shift <small style="color: red">*</small></label>
                <input type="text" class="form-control time_shift" name="end_shift" placeholder="Akhir Shift" value="{{\Carbon\Carbon::parse($data->end_shift)->format('H:i')}}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Shift Setengah Hari</label>
                <input type="text" class="form-control time_shift" name="half_shift" placeholder="Shift Setengah Hari" value="{{is_null($data->half_shift)?'':\Carbon\Carbon::parse($data->half_shift)->format('H:i')}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Maksimal Keterlambatan <small style="color: red">*</small></label>
                <input type="number" class="form-control" name="max_late" placeholder="Max Kertelambatan" value="{{is_null($data->max_late)?0:$data->max_late}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Maksimal Presensi<small style="color: red">*</small></label>
                <input type="number" class="form-control" name="max_allowed_attendance_per_day" placeholder="Max Presensi" value="{{is_null($data->max_allowed_attendance_per_day)?0:$data->max_allowed_attendance_per_day}}" required>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group my-3">
                <label class="f-14 text-dark-grey mb-12 font-weight-bold" data-label="true" for="office_open_days">Hari Kerja
                    <sup class="f-14 mr-1">*</sup>
                </label>
                <div class="d-lg-flex d-sm-block justify-content-between ">
                    @foreach($days as $d)
                        @if(!empty($data->id))
                            @foreach(json_decode($data->work_day) as $w)
                                @if($d['day']==$w->day)
                                    <div class="mr-3 mb-2">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox"
                                                   @if($w->is_selected==true) checked @endif
                                                   name="is_selected_days[]" id="open_mon">
                                            <input type="hidden" name="days_name[]" value="{{$d['day']}}">
                                            <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 pt-2 text-wrap" for="open_mon">
                                                {{$d['day']}}
                                            </label>
                                        </div>
                                    </div>
                                @endif

                            @endforeach
                       @else
                            <div class="mr-3 mb-2">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="is_selected_days[]" id="open_mon">
                                    <input type="hidden" name="days_name[]" value="{{$d['day']}}">
                                    <label class="form-check-label form_custom_label text-dark-grey pl-2 mr-3 justify-content-start cursor-pointer checkmark-20 pt-2 text-wrap" for="open_mon">
                                        {{$d['day']}}
                                    </label>
                                </div>
                            </div>
                      @endif
                    @endforeach

                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>

    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $("#is_active").select2();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.setting.save-shift')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
        $('.time_shift').datetimepicker({
            dateFormat: '',
            datepicker:false,
            pickDate: false,
            format: "HH:mm",
            formatTime: 'HH:mm',
            timeOnly:true
        });
    })

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah atau mengubah data shift silahkan isi inputan dibawah ini!</span>
                                    </div>`);
    });

    $('#theme-pallete').on('change', function(){
        let colorVal = $(this).val();
        $('#theme').val(colorVal);
    });

    $('#theme').on('change', function(){
        let colorVal = $(this).val();
        $('#theme-pallete').val(colorVal);
    });


</script>

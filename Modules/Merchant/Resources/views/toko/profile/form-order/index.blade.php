@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    @include('merchant::toko.profile.form-order.style')
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">{{$title}}</h4>
                <span>Masukan informasi usahamu, atur metode pembayaran, jam operasional toko,  metode pengiriman, dan reset data.</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-lg-4 col-md-12 mb-3 p-0 left-sidebar">
                @include('merchant::toko.profile.form-order.sidebar')
            </div>
            <!-- end sidebar -->

            <!-- result form drag -->
            <div class="col-lg-8 col-md-12 mb-3 p-0"> 
                @include('merchant::toko.profile.form-order.drag-result')
            </div>
            <!-- end result form drag -->
        </div>

    </div>
    
    <!-- modal -->
    @include('merchant::toko.profile.form-order.modal')
    <!-- end modal -->

    <!-- drag form -->
    <script>
        const drag = (e) => {
            if(e.target.id){
                e.dataTransfer.setData("text/html", e.target.id);
            } else {
                e.dataTransfer.setData("text/html", e.target.parentNode.id);
            }
        }

        const allowDrop = (e) => {
            e.preventDefault();
        }

        const drop = (e) => {
            e.preventDefault();
            let data = e.dataTransfer.getData("text/html");
            let containerType = e.target.getAttribute('data-container-type');
            let elmnt = document.getElementById(data);

            if(!elmnt){
                return false;
            }

            if(
                ($(e.target).parents('#payment-method-section').length > 0 && !elmnt.classList.contains("section-container")) ||
                ($(e.target).parents('#shipping-method-section').length > 0 && !elmnt.classList.contains("section-container"))
            ){
                return false;
            }

            if($(e.target).parents('#payment-method').length > 0 && !elmnt.classList.contains("section-container")){
                return false;
            }

            if(elmnt.classList.contains("section-container")){
                dragSection({
                    "e":e,
                    "element":elmnt
                });
                return;
            }

            if(elmnt.id == "payment-method" || e.target.getAttribute('data-id') == "drag-notif"){
                addPaymentSection({
                    "e":e,
                    "element":elmnt
                });
                return;   
            }

            if(
                e.target.type == 'checkbox' ||
                e.target.type == 'radio' || 
                e.target.tagName == 'LABEL' || 
                e.target.classList.contains('form-check')
            ){
                return false;
            }

            if(containerType == "section"){
                e.target.appendChild(elmnt);
                return; 
            }

            if(containerType != "form"){
                if(e.target.type == 'button'){
                    let btnId = e.target.id;
                    $(elmnt).insertBefore($(`#${btnId}`));
                } else {
                    if(containerType){
                        let parentNode = e.target.parentNode;
                        let siblingNode = e.target.nextSibling;
                        parentNode.insertBefore(elmnt, siblingNode);
                    } else {
                        let parentNode = e.target.parentNode.parentNode;
                        let siblingNode = e.target.parentNode.nextSibling;
                        parentNode.insertBefore(elmnt, siblingNode);
                    }
                    
                }
            } else {
                let btnId = e.target.getAttribute('data-button-id');
                if(btnId){
                    $(elmnt).insertBefore($(`#${btnId}`));
                } else {
                    e.target.appendChild(elmnt);
                }
            }

        }

        const dragSection = (param) => {
            const { e, element } = param;
            let targetId = e.target.getAttribute("data-id");
            if(!$("#"+targetId).hasClass("section-container")){
                return false;
            }
            $(element).insertBefore($(`#${targetId}`));

        }

        const addPaymentSection = (param) => {
            const { e, element } = param;
            let targetId = e.target.getAttribute("data-id");
            let sectionContainer = [...document.querySelectorAll(".section-container")];
            if(element.id != "payment-method"){
                return false;   
            }
            
            
            if(targetId != "form-payment-method"){
                if(!$("#"+targetId).hasClass("section-container") && targetId != "drag-notif"){
                    return false;
                } else if($("#"+element.id).parents('#form-payment-method').length == 0){
                    return false;
                }
            }

            let html = `<div draggable="true" ondragstart="drag(event)" class="row section-container" id="payment-method-section" data-section-type="Metode Pembayaran">
                            <div class="col-md-12">
                                <div class="position-relative">
                                    <h5 class="font-weight-bold section-title mb-3">Metode Pembayaran</h5>
                                    <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="payment-method-section">
                                        <button class="edit-section" type="button" data-id="payment-method-section">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                        <button class="delete-section" type="button" data-id="payment-method-section">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                                <div
                                    class="p-3 section-wrapper mb-4"
                                    data-container-type="section-pm"
                                    data-section-type="Metode Pembayaran"
                                    data-id="payment-method-section"
                                    style="border:1px dashed #eee;"
                                    ondrop="drop(event)"
                                    ondragover="allowDrop(event)"
                                >
                                </div>
                            </div>
                        </div>`;
            $("#drag-notif").addClass("d-none");
            
            if(targetId != "form-payment-method"){
                $("#target").append(html);
                $("#payment-method-section .section-wrapper").append(element);
            } else {
                $("#form-payment-method").append(element);
                $("#payment-method-section").remove();
                if(sectionContainer.length == 1){
                    $("#drag-notif").removeClass("d-none");
                }
            }
            
        }
    </script>
    <!-- end drag form -->

    <!-- section -->
    <script>
        /* add new section */
        $('#modal-add-section').on('shown.bs.modal', function(e){
            if(e.relatedTarget){
                // tambah section
                $("#modal-add-section-label").text("Tambah Section");
                $("#is_edit_section").val(0);
                $("#section_id").val("");
            } else {
                // edit section
                $("#modal-add-section-label").text("Edit Section");
            }
        });

        $('#modal-add-section').on('hidden.bs.modal', function(){
            $("#modal-add-section-label").text("");
            $("#is_edit_section").val(0);
            $("#section_id").val("");
            $("#section_name").val("");
        });

        $("#save-section").on("click", function(){
            let section_name = $("#section_name").val();

            $("#drag-notif").addClass("d-none");

            if(section_name == ""){
                $('#modal-add-section').modal('hide');
                return;
            }

            let id = Date.now();
            let isEdit = $("#is_edit_section").val();

            if(isEdit == 1){
                let sectionId = $('#section_id').val();
                let param = {
                    "value": section_name
                }

                editSection(sectionId, param);
                return;
            }

            let html = `<div draggable="true" ondragstart="drag(event)" class="row section-container" id="${id}" data-section-type="${section_name}">
                            <div class="col-md-12">
                                <div class="position-relative">
                                    <h5 class="font-weight-bold section-title mb-3">${section_name}</h5>
                                    <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="${id}">
                                        <button class="edit-section" type="button" data-id="${id}">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                        <button class="delete-section" type="button" data-id="${id}">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                                <div
                                    class="p-3 section-wrapper mb-4" 
                                    ondrop="drop(event)"
                                    ondragover="allowDrop(event)"
                                    data-container-type="section"
                                    data-section-type="${section_name}"
                                    data-id="${id}"
                                    style="min-height:50vh; border:1px dashed #eee;"
                                >
                                </div>
                            </div>
                        </div>`;

            $("#section_name").val("");
            $("#target").append(html);
            $('#modal-add-section').modal('hide');
        });

        /* end add new section */

        /* edit section */
        $(document).on('click', '.edit-section', function(){
            let id = $(this).attr('data-id');
            let elmnt = document.getElementById(id);
            let section_name = elmnt.getAttribute("data-section-type");
            $("#section_name").val(section_name);
            $("#is_edit_section").val(1);
            $("#section_id").val(id);
            $("#modal-add-section").modal("show"); 
        });

        const editSection = (id, param) => {
            $(`#${id}`).attr('data-section-type', param.value);
            $(`#${id} .section-wrapper`).attr('data-section-type', param.value);
            $(`#${id} .section-title`).text(param.value);
            $("#modal-add-section").modal("hide");
            $("#section_name").val("");
        }

        /* end edit section */

        /* delete section */
        $(document).on('click', '.delete-section', function(){
            let id = $(this).attr('data-id');
            $("#section_delete_id").val(id);
            $("#modal-delete-section").modal("show");
        });

        $("#btn-delete-section").on('click', function(){
            let id = $("#section_delete_id").val();
            let elmnt = document.getElementById(id);
            let dragField = [...elmnt.querySelectorAll('.drag-element')];
            let sectionContainer = [...document.querySelectorAll(".section-container")];
        
            if(sectionContainer.length == 1){
                $("#drag-notif").removeClass("d-none");
            }

            if(id == "payment-method-section"){
                dragField.forEach(item => {
                    $("#form-payment-method").append(item);
                });
            } else if(id == "shipping-method-section") {
                $("#shipping_method").val([]).change();
            } else {
                dragField.forEach(item => {
                    $(item).insertBefore("#show-add-field-1");
                });
            }

            
            elmnt.remove();

            $("#modal-delete-section").modal("hide");
        });

        /* end delete section */

    </script>
    <!-- end section  -->

    <!-- add new field -->
    <script>
        let modalField = [];

        $("#input-type").on('change', function(){
            let value = $(this).val();
            let isAddAble = $(this).find(":selected").attr('data-add-able');
            let fieldContainer = [...document.querySelectorAll('.field-container')];
            fieldContainer.forEach(item => {
                item.classList.add('d-none');
            });
            
            if(isAddAble == 1){
                $("#is_add_able").val(1);
            } else {
                $("#is_add_able").val(0);
            }

            $(`#${value}`).removeClass('d-none');
        });

        $(document).on('click','#add-opt', function(){
            let elmnt = `<div class="col-md-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control custom-field" name="checkbox_val">
                                 </div>
                            </div>`;
            $(elmnt).insertBefore($(this).parent());
        });
        
        $("#save-field").on('click', function(){
            let inputType = $("#input-type").val();
            if(inputType == "-1"){
                $('#exampleModal').modal('hide');
                return;
            }

            let customFields = [...document.querySelectorAll(`#${inputType} .custom-field`)];
            let sectionType = $('#section_type').val();
            let btnTargetId = $('#target_button_id').val();
            let isEdit = $("#is_edit").val();
            let isAddAble = $("#is_add_able").val();

            if(inputType == "text" || inputType == "textarea"){
                let label, required;
                let id = Date.now();
                let type = inputType;
                let name = Date.now();
                customFields.forEach(item => {
                    if(item.type == "text" && item.getAttribute('name') == "label"){
                        label = item.value;
                    }
                    if(item.type == "checkbox" && item.getAttribute('name') == "required"){
                       required = (item.checked)? "true":"false";
                    }
                });

                if(label == ""){
                    $('#exampleModal').modal('hide');
                    return;        
                }

                if(isEdit == 1){
                    let elementId = $('#element_id').val();
                    let param = {
                        "required":required,
                        "label":label,
                        "is_add_able": isAddAble
                    }
                    editField(elementId,type,param);
                    return;
                }

                let inputElmnt = (type == "text")? 
                                    `<input ${required == "true" ? "required":""} type="${type}" class="form-control" name="${name}" placeholder="${label}">`:
                                    `<textarea ${required == "true" ? "required":""} class="form-control" name="${name}" placeholder="${label}"></textarea>`;

                let body = `<div 
                                class="form-group drag-element" 
                                data-id="${id}" 
                                id="${id}"
                                data-type="${type}"
                                data-label="${label}"
                                data-name="${name}"
                                data-section-type="${sectionType}"
                                data-required="${required}" 
                                draggable="true" 
                                ondragstart="drag(event)" 
                                data-container-type="div"
                                style="position:relative;"
                            >
                                <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="${id}">
                                    <button class="edit-field" type="button" data-id="${id}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                </div>
                                <label class="font-weight-bold">${label}</label>
                                ${inputElmnt}
                            </div>`;
                $(body).insertBefore(`#${btnTargetId}`);
                modalField.push({
                    "id":id,
                    "type":type,
                    "label":label,
                    "name":name,
                    "section_type":sectionType,
                    "required":required
                });
            }

            if(inputType == "checkbox" || inputType == "radio"){
                let label, section_type, required;
                let id = Date.now();
                let type = inputType;
                let name = (inputType == "checkbox") ? `${Date.now()}[]`:Date.now();

                customFields.forEach(item => {
                    if(item.type == "text" && item.getAttribute("name") == "label"){
                        label = item.value;
                    }
                    if(item.type == "checkbox" && item.getAttribute('name') == "required"){
                       required = (item.checked)? "true":"false";
                    }
                });

                if(label == ''){
                    $('#exampleModal').modal('hide');
                    return;        
                }

                if(isEdit == 1){
                    let elementId = $('#element_id').val();
                    let value = [];
                    
                    customFields.forEach(item => {
                        if(item.type == "text" && item.getAttribute("name") == "checkbox_val"){
                            if(item.value != ''){
                                value.push({
                                    "value":item.value
                                });
                            }
                        }
                    });

                    let param = {
                        "label":label,
                        "value":value,
                        "required":required,
                        "is_add_able":isAddAble
                    }

                    editField(elementId,type,param);
                    return;
                }

                let body = `<div 
                                class="drag-element mb-2" 
                                data-id="${id}" 
                                id="${id}"
                                data-type="${type}"
                                data-label="${label}"
                                data-name="${name}"
                                data-section-type="${sectionType}"
                                data-required="${required}"
                                data-add-able="${isAddAble}" 
                                draggable="true" 
                                ondragstart="drag(event)"
                                style="position:relative;" 
                                data-container-type="div"
                            >
                            <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="${id}">
                                <button class="edit-field" type="button" data-id="${id}">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                            </div>
                            <label class="font-weight-bold">${label}</label>`;

                customFields.forEach(item => {
                    if(item.type == "text" && item.getAttribute("name") == "checkbox_val"){
                        if(item.value != ''){
                            body += `<div class="form-check">
                                    <input ${required == "true" ? "required":""} type="${type}" class="form-check-input" name="${name}" value="${item.value}" data-label="${item.value}">
                                    <label>${item.value}</label>
                                </div>`;
                        }
                    }
                });

                if(isAddAble == 1){
                    body += `<button class="add-list d-block" type="button" data-id="${id}">
                                + Lainnya
                            </button>
                        </div>`;
                } else {
                    body += `</div>`;
                }
                
                $(body).insertBefore(`#${btnTargetId}`);

                modalField.push({
                    "id":id,
                    "type":type,
                    "label":label,
                    "name":name,
                    "section_type":sectionType,
                    "required":required,
                    "is_add_able":isAddAble,
                    "option": customFields.filter(item => {
                                    return item.type == "text" && item.getAttribute("name") == "checkbox_val" && item.value != "";
                                }).map(i => {
                                    return {
                                        "label":i.value,
                                        "value":i.value
                                    }
                                })
                });
            }

            if(inputType == "note"){
                let label = $("#note_value").val();
                let id = Date.now();
                let type = inputType;
                let name = Date.now();

                if(label == ""){
                    $('#exampleModal').modal('hide');
                    return;        
                }

                if(isEdit == 1){
                    let elementId = $('#element_id').val();
                    let param = {
                        "required":"false",
                        "label":label,
                        "is_add_able": isAddAble
                    }
                    editField(elementId,type,param);
                    return;
                }

                let noteElmnt = `<textarea disabled class="form-control note_value" name="${name}">${label}</textarea>`;

                let body = `<div 
                                class="form-group drag-element" 
                                data-id="${id}" 
                                id="${id}"
                                data-type="${type}"
                                data-label="${label}"
                                data-name="${name}"
                                data-section-type="${sectionType}"
                                data-required="false" 
                                draggable="true" 
                                ondragstart="drag(event)" 
                                data-container-type="div"
                                style="position:relative;"
                            >
                                <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="${id}">
                                    <button class="edit-field" type="button" data-id="${id}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                </div>
                                <label class="font-weight-bold">Catatan</label>
                                ${noteElmnt}
                            </div>`;
                $(body).insertBefore(`#${btnTargetId}`);
                modalField.push({
                    "id":id,
                    "type":type,
                    "label":label,
                    "name":name,
                    "section_type":sectionType,
                    "required":"false"
                });
            }

            $('#exampleModal').modal('hide');
        });

        let wrapperTypeText, wrapperTypeCheckbox, wrapperTypeRadio, wrapperTypeTextarea, wrapperNote;
        $(document).ready(function(){
            wrapperTypeText = $('#text').html();
            wrapperTypeTextarea = $('#textarea').html();
            wrapperTypeCheckbox = $('#checkbox').html();
            wrapperTypeRadio = $('#radio').html();
            wrapperNote = $("#note").html();
        });

        $('#exampleModal').on('shown.bs.modal', function(e){
            if(e.relatedTarget){
                // tambah field
                $("#exampleModalLabel").text("Tambah Kolom");
                $("#section_type").val($(e.relatedTarget).attr('data-section'));
                $("#target_button_id").val($(e.relatedTarget).attr('id'));
                $("#is_edit").val(0);
                $("#element_id").val("");
                $("#input-type-wrapper").show();
                $("#text").html("").append(wrapperTypeText);
                $("#textarea").html("").append(wrapperTypeTextarea);
                $("#checkbox").html("").append(wrapperTypeCheckbox);
                $("#radio").html("").append(wrapperTypeRadio);
                $("#note").html("").append(wrapperNote);
            } else {
                // edit field
                $("#exampleModalLabel").text("Edit Kolom");
            }
        });

        $('#exampleModal').on('hidden.bs.modal', function(){
            $("#input-type").val('-1').change();
            $('#section_type').val('');
            $("#target_button_id").val('');
            $("#is_edit").val(0);
            $("#element_id").val('');
            $("#exampleModalLabel").text("");
            $("#note_value").val("");
        });
    </script>
    <!-- end add new field -->
    
    <!-- edit field -->
    <script>
        $(document).on('click', '.edit-field', function(){
            let id = $(this).attr('data-id');
            let elmnt = document.getElementById(id);
            let type = elmnt.getAttribute('data-type');
            let label = elmnt.getAttribute('data-label');
            let required = elmnt.getAttribute('data-required');
            let isAddAble = elmnt.getAttribute('data-add-able');
            
            $("#input-type").val(type).change();
            $("#input-type-wrapper").hide();
            $("#is_edit").val(1);
            $("#element_id").val(id);
            $("#is_add_able").val(isAddAble);

            let field = modalField.filter(item => {
                return item.id == id;
            })[0];

            let html = ``;
            
            if(type == "checkbox" || type == "radio"){
                html += `<div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Label</label>
                                <input value="${field.label}" type="text" name="label" class="form-control custom-field" placeholder="Label">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mb-0">
                                <label class="font-weight-bold">Pilihan</label>
                            </div>
                        </div>
                        `;
                
                field.option.forEach(i => {
                    html += `<div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="checkbox_val" class="form-control custom-field" value="${i.value}">
                                </div>
                            </div>`;
                });

                html += `<div class="col-md-12">
                            <button class="btn add-opt" id="add-opt" type="button">
                                <i class="fas fa-plus-circle"></i>
                            </button>
                        </div>
                        <div class="col-md-12 mt-2">
                            <div class="form-check d-flex align-items-center ml-2 mb-1">
                                <input 
                                    class="form-check-input custom-field" 
                                    type="checkbox" 
                                    value="1" 
                                    style="margin-top:0;"
                                    name="required"
                                    ${(required == "true")? "checked":""}
                                >
                                <label class="form-check-label">
                                    Required
                                </label>
                            </div>
                        </div>
                        `;
            }

            if(type == "text" || type =="textarea"){
                html += `<div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Label</label>
                                <input value="${field.label}" type="text" name="label" class="form-control custom-field" placeholder="Label">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-check d-flex align-items-center ml-2 mb-1">
                                <input 
                                    class="form-check-input custom-field" 
                                    type="checkbox" 
                                    value="1" 
                                    style="margin-top:0;"
                                    name="required"
                                    ${(required == "true")? "checked":""}
                                >
                                <label class="form-check-label">
                                    Required
                                </label>
                            </div>
                        </div>`;
            }

            if(type == "note"){
                html += `<div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan</label>
                                <textarea id="note_value" name="note_value" class="form-control note_value" placeholder="Tulis Catatan">${field.label}</textarea>
                            </div>
                        </div>`;
            }

            $("#"+type).html("").append(html); 
            $("#exampleModal").modal("show"); 
        });

        const editField = (elementId, type, param) => {
            let elmnt = document.getElementById(elementId);
            let name = elmnt.getAttribute('data-name');
            if(type == "text" || type == "textarea"){
                $(`#${elementId}`).attr('data-label', param.label);
                $(`#${elementId}`).attr('data-required', param.required);
                $(`#${elementId} label`).text(param.label);
                $(`#${elementId} input`).attr('placeholder', param.label);
                $(`#${elementId} textarea`).attr('placeholder', param.label);

                if(param.required == "true"){
                    $(`#${elementId} input`).prop('required', true);
                    $(`#${elementId} textarea`).prop('required', true);
                } else {
                    $(`#${elementId} input`).prop('required', false);
                    $(`#${elementId} textarea`).prop('required', false);
                }

                modalField = [...modalField].map(item => {
                    if(item.id == elementId){
                        return {
                            ...item,
                            "label":param.label,
                            "required":param.required
                        }
                    } else {
                        return {
                            ...item
                        }
                    }
                });
            }

            if(type == "checkbox" || type == "radio"){
                $(`#${elementId}`).attr('data-label', param.label);
                $(`#${elementId} > label`).text(param.label);
                $(`#${elementId}`).attr('data-required', param.required);
                $(`#${elementId} input[name="required"]`).attr("checked", param.required);

                let html = ``;
                
                if(param.required == "true"){
                    param.value.forEach(item => {
                        html += `<div class="form-check">
                                        <input required type="${type}" class="form-check-input" name="${name}" value="${item.value}" data-label="${item.value}">
                                        <label>${item.value}</label>
                                    </div>`;
                    });
                } else {
                    param.value.forEach(item => {
                        html += `<div class="form-check">
                                        <input type="${type}" class="form-check-input" name="${name}" value="${item.value}" data-label="${item.value}">
                                        <label>${item.value}</label>
                                    </div>`;
                    });
                }

                $(`#${elementId} .form-check`).remove();

                if(param.is_add_able == 1){
                    $(html).insertBefore($(`#${elementId} .add-list`));
                } else {
                    $(`#${elementId}`).append(html);
                }

                modalField = [...modalField].map(item => {
                    if(item.id == elementId){
                        return {
                            ...item,
                            "label":param.label,
                            "required":param.required,
                            "option":param.value.map(i => {
                                return {
                                    "label": i.value,
                                    "value": i.value
                                }
                            })
                        }
                    } else {
                        return {
                            ...item
                        }
                    }
                });
            }

            if(type == "note"){
                $(`#${elementId} textarea`).val(param.label);
                $(`#${elementId} textarea`).text(param.label);

                modalField = [...modalField].map(item => {
                    if(item.id == elementId){
                        return {
                            ...item,
                            "label":param.label
                        }
                    } else {
                        return {
                            ...item
                        }
                    }
                });
            }
            
            $('#exampleModal').modal('hide');
        }

    </script>
    <!-- end edit field -->
    
    <!-- add list item form builder -->
    <script>
        $(document).on('click', '.add-list', function(){
            let id = $(this).attr('data-id');
            $("#list_item_id").val(id);
            $("#modal-add-list").modal("show");
        });

        $("#btn-add-list").on('click', function(){
            let id = $("#list_item_id").val();
            let value = $("#list_name").val();
            
            let name = $(`#${id}`).attr("data-name");
            let type = $(`#${id}`).attr("data-type");

            let html = `<div class="form-check">
                                    <input type="${type}" class="form-check-input" name="${name}" value="${value}" data-label="${value}">
                                    <label>${value}</label>
                                </div>`;
            $(html).insertBefore($(`#${id} .add-list`));

            modalField.forEach(item => {
                if(item.id == id){
                    item.option.push({
                        "label": value,
                        "value": value
                    })
                }
            });

            $("#list_name").val("");
            $("#modal-add-list").modal("hide");
        });

        
    </script>
    <!-- end add list item form builder -->

    <!-- add coa biaya -->
    <script>
        $(document).on('click', '.add-list', function(){
            let id = $(this).attr('data-id');
            $("#list_item_id").val(id);
            $("#modal-add-list").modal("show");
        });

        $("#btn-add-coa-biaya").on('click', function(){
            let id = Date.now();
            let name = Date.now();

            let data = $("#coa_biaya").select2("data");
            let label = data[0].element.getAttribute('data-label');
            let coaCode = $("#coa_biaya").val();

            if(label == "" || coaCode == -1){
                $('#modal-add-coa-biaya').modal('hide');
                return;        
            }

            if(modalField.filter(item => item.label == label).length > 0){
                $('#modal-add-coa-biaya').modal('hide');
                return;
            }

            let inputElmnt = `<input data-coa-code="${coaCode}" class="form-control coa_custom" name="${name}" placeholder="${label}">`;

            let body = `<div 
                            class="form-group drag-element" 
                            data-id="${id}" 
                            id="${id}"
                            data-type="text"
                            data-label="${label}"
                            data-name="${name}"
                            data-section-type="data-biaya"
                            data-required="false" 
                            draggable="true" 
                            ondragstart="drag(event)" 
                            data-container-type="div"
                            style="position:relative;"
                        >
                            <label class="font-weight-bold">${label}</label>
                            ${inputElmnt}
                        </div>`;
            $(body).insertBefore(`#biaya-button`);
            modalField.push({
                "id":id,
                "type":"text",
                "label":label,
                "name":name,
                "section_type":'data-biaya',
                "required":"false",
            });

            $('#modal-add-coa-biaya').modal('hide');
        });


        $("#btn-add-coa-pendapatan").on('click', function(){
            let id = Date.now();
            let name = Date.now();

            let data = $("#coa_pendapatan").select2("data");
            let label = data[0].element.getAttribute('data-label');
            let coaCode = $("#coa_pendapatan").val();


            if(label == "" || coaCode == -1){
                $('#modal-add-coa-pendapatan').modal('hide');
                return;        
            }

            let inputElmnt = `<input data-coa-code="${coaCode}" class="form-control coa_custom" name="${name}" placeholder="${label}">`;

            let body = `<div 
                            class="form-group drag-element" 
                            data-id="${id}" 
                            id="${id}"
                            data-type="text"
                            data-label="${label}"
                            data-name="${name}"
                            data-section-type="data-biaya"
                            data-required="false" 
                            draggable="true" 
                            ondragstart="drag(event)" 
                            data-container-type="div"
                            style="position:relative;"
                        >
                            <label class="font-weight-bold">${label}</label>
                            ${inputElmnt}
                        </div>`;
            $(body).insertBefore(`#pendapatan-button`);
            modalField.push({
                "id":id,
                "type":"text",
                "label":label,
                "name":name,
                "section_type":'data-biaya',
                "required":"false",
            });

            $('#modal-add-coa-pendapatan').modal('hide');
            $("#coa_pendapatan").val(-1).change();
        });

        
    </script>
    <!-- end add coa biaya -->

    <!-- add shipping category -->
    <script>
        $(document).ready(function(){
            $("#shipping_method").select2({
                placeholder:'--- Pilih Metode Pengiriman ---'
            });
        });

        $("#shipping_method").on("change", function(){
            let selectedShippingMethod = $("#shipping_method").select2("data");
            addShippingMethod({
                "data":selectedShippingMethod
            });
        });

        const addShippingMethod = (param) => {
            const { data } = param;
            let shippingMethodSection = document.querySelector("#shipping-method-section");
            
            let element = `<div 
                                class="form-group drag-element" 
                                data-id="shipping-method-section" 
                                id="shipping-method"
                                data-type="radio"
                                data-label="Metode Pengiriman"
                                data-name="shipping_method"
                                data-section-type="metode-pengiriman"
                                data-required="false"
                                data-container-type="div"
                                data-add-able="0" 
                                style="position:relative;"
                            >
                                <label class="font-weight-bold">Metode Pengiriman</label>`;

            data.forEach(item => {
                element += `<div class="form-check">
                                        <input type="radio" class="form-check-input" name="shipping_method" value="${item.id}" data-label="${item.text.trim()}">
                                        <label>${item.text.trim()}</label>
                                    </div>`;
            });

            element += `</div>`;

            let html = `<div draggable="true" ondragstart="drag(event)" class="row section-container" id="shipping-method-section" data-section-type="Metode Pengiriman">
                            <div class="col-md-12">
                                <div class="position-relative">
                                    <h5 class="font-weight-bold section-title mb-3">Metode Pengiriman</h5>
                                    <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="shipping-method-section">
                                        <button class="edit-section" type="button" data-id="shipping-method-section">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                        <button class="delete-section" type="button" data-id="shipping-method-section">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </div>
                                <div
                                    class="p-3 section-wrapper mb-4"
                                    data-container-type="section-sm"
                                    data-section-type="Metode Pengiriman"
                                    data-id="shipping-method-section"
                                    style="border:1px dashed #eee;"
                                    ondrop="drop(event)"
                                    ondragover="allowDrop(event)"
                                >
                                </div>
                            </div>
                        </div>`;
                        
            $("#drag-notif").addClass("d-none");
            
            if(shippingMethodSection){
                $("#shipping-method-section .section-wrapper").html("").append(element);
            } else {
                $("#target").append(html);
                $("#shipping-method-section .section-wrapper").append(element);
            }

            if(data.length == 0){
                $("#shipping-method-section").remove();
            }

            let sectionContainer = [...document.querySelectorAll(".section-container")];

            if(sectionContainer.length == 0){
                $("#drag-notif").removeClass("d-none");
            }
            
        }
    </script>
    <!-- end add shipping category -->

    <!-- add product -->
    <script>
        $("#list_product").on("change", function(){
            let data = $("#list_product").select2("data");
            let label = ($("#input-product-label").val())? $("#input-product-label").val():"Pilih Layanan Penjemputan";
            if(data.length == 0){
                $("#section-product").html("");
                return;
            }

            $("#section-product").html("");

            let body = `<div class="custom-field" style="position:absolute; top:0;right:0;" data-id="section-product">
                                <button class="edit-fiel" type="button" id="btn-edit-section-product" data-id="section-product">
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                            </div>
                            <label id="section-product-label" class="font-weight-bold">${label}</label>
                            <div class="form-group">
                                <input type="text" id="input-product-label" class="form-control d-none" value="${label}">
                            </div>
                             `;
            data.map(item => {
                body += `<div class="form-check">
                            <input type="radio" class="form-check-input" name="product_id" value="${item.id}" data-label="${item.text.trim()}">
                            <label>${item.text.trim()}</label>
                        </div>`;
            });

            $("#section-product").append(body);
        });

        $(document).on("click","#btn-edit-section-product", function(){
            $("#section-product-label").addClass("d-none");
            $("#input-product-label").removeClass("d-none");
        });
        $(document).on("focusout", "#input-product-label", function(){
            $("#input-product-label").addClass("d-none");
            $("#section-product-label").removeClass("d-none");

            if($(this).val() == ""){
                return;
            }
            $("#section-product-label").text($(this).val());
            $("#section-product").attr('data-label', $(this).val());

        });
    </script>
    <!-- end product -->

    <!-- save template -->
    <script>
        $(document).ready(function(){
            $('#input-type').select2({
                placeholder:'--- Pilih Tipe Kolom ---'
            });

            $("#list_product").select2({
                placeholder:'--- Pilih Produk ---'
            });

            $("#coa_biaya").select2();

            $("#coa_pendapatan").select2();

            $('#btn-submit').on('click', function(){
                $('#target').submit();
            });

            let form_template = @json($form_template);
            let data = @json($data);

            if(data){
                JSON.parse(data.template_json).forEach(item => {
                    item.value.forEach(i => {
                        modalField.push(i);
                    })
                });
            }

            form_template.forEach(item => {
                if(modalField.findIndex(i => i.id == item.id) < 0){
                    modalField.push(item);
                }
            });

            $('#target').submit(function(){
                let data = new FormData();
                let sectionWrapper = [...document.querySelectorAll('#target .section-wrapper')]
                let dragElement = [...document.querySelectorAll('#target .section-wrapper .drag-element')];
                let form_ids = [];
                let customFields = [...document.querySelectorAll('#target .custom-field')];

                let productSection = document.querySelector("#target #section-product");
                if(!productSection){
                    toastForSaveData("Tarik data produk ke area section !","warning","false","");
                    return;    
                }
                

                // customFields.forEach(item => {
                //     item.remove();
                // });

                let coaCustom = [];
                let coa = [...document.querySelectorAll(".coa_custom")];
                
                coa.forEach(item => {
                    coaCustom.push(item.getAttribute("data-coa-code"));
                });
                
                let selectedProduct = $("#list_product").select2("data");
                let selectedShipping = $("#shipping_method").select2("data");
                let dataProduct = [];
                let dataShipping = [];

                sectionWrapper.forEach(item => {
                    form_ids.push({
                        "id": item.getAttribute('data-id'),
                        "section":item.getAttribute('data-section-type'),
                        "value": [...item.querySelectorAll('.drag-element')].map(i => {
                            if(i.getAttribute('data-type') == "text" || i.getAttribute('data-type') == "textarea"){
                                return {
                                    "id":i.getAttribute('id'),
                                    "name":i.getAttribute('data-name'),
                                    "label":i.getAttribute('data-label'),
                                    "type":i.getAttribute('data-type'),
                                    "section_type":item.getAttribute('data-section-type'),
                                    "required":i.getAttribute('data-required'),
                                    "is_add_able": i.getAttribute('data-add-able'),
                                    "body":i.innerHTML.trim()
                                }
                            } else if(i.getAttribute('data-type') == "checkbox" || i.getAttribute('data-type') == "radio") {
                                return {
                                    "id":i.getAttribute('id'),
                                    "name":i.getAttribute('data-name'),
                                    "label":i.getAttribute('data-label'),
                                    "type":i.getAttribute('data-type'),
                                    "section_type":item.getAttribute('data-section-type'),
                                    "required":i.getAttribute('data-required'),
                                    "is_add_able":i.getAttribute('data-add-able'),
                                    "body":i.innerHTML.trim(),
                                    "option":[...i.querySelectorAll(`input[name="${i.getAttribute('data-name')}"]`)].map(o => {
                                                return {
                                                    "label":o.getAttribute('data-label'),
                                                    "value":o.value
                                                }
                                            })
                                }
                            } else {
                                return {
                                    "id":i.getAttribute('id'),
                                    "name":i.getAttribute('data-name'),
                                    "label":i.getAttribute('data-label'),
                                    "type":i.getAttribute('data-type'),
                                    "section_type":item.getAttribute('data-section-type'),
                                    "required":i.getAttribute('data-required'),
                                    "is_add_able": i.getAttribute('data-add-able'),
                                    "body":i.innerHTML.trim()
                                }
                            }
                        })
                    })
                });
                
                dataProduct = selectedProduct.map(item => {
                    return {
                        "id":item.id,
                        "name":item.text.trim(),
                        "inv_id":item.element.getAttribute("data-inv-id"),
                        "hpp_id":item.element.getAttribute("data-hpp-id"),
                        "image":item.element.getAttribute("data-image")
                    }
                });

                dataShipping = selectedShipping.map(item => {
                    return {
                        "id":item.id,
                        "name":item.text.trim()
                    }
                });

                if(dataProduct.length == 0){
                    toastForSaveData("Silahkan pilih produk terlebih dahulu !","warning","false","");
                    return;
                }

                data.append("coa_custom", JSON.stringify(coaCustom));
                data.append("unique_code", $("#unique_code").val());
                data.append('form_ids', JSON.stringify(form_ids));
                data.append("product_json", JSON.stringify(dataProduct));
                data.append("shipping_json", JSON.stringify(dataShipping));
                ajaxTransfer("{{route('setting.form-order.save')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            });
        });
    </script>
    <!-- end save template -->
@endsection
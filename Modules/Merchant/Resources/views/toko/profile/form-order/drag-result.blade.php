<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 offset-lg-0" id="col-container">
            <div class="card shadow">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Form Builder</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            @if(!is_null($data))
                            <button class="btn-preview py-1 px-2 rounded mr-2" onclick="copyClipboard('{{$link}}')" data-placement="bottom" title="Salin URL untuk melihat hasil">
                                <i id="fa-copy" title="copied" class="far fa-copy mr-2"></i>
                                <span>Copy Link</span>
                            </button>
                            @endif
                            <button 
                                class="add-section py-1 px-2 rounded" 
                                type="button" 
                                id="add-section"
                                data-toggle="modal"
                                data-target="#modal-add-section"
                            >
                                <i class="fas fa-plus-circle mr-2"></i>Tambah Section
                            </button>
                        </div>
                    </div>
                    <div id="builder" class="panel panel-default">
                        <hr/>
                        <div class="panel-body">
                            <div
                                id="drag-notif"  
                                style="height:500px;border:1px dashed #eee;"
                                ondrop="drop(event)" 
                                ondragover="allowDrop(event)"
                                data-id="drag-notif"
                                class="@if(!is_null($data) && count(json_decode($data->template_json)) > 0) d-none @endif" 
                            >
                                <div data-id="drag-notif" class="d-flex align-items-center justify-content-center" style="height:500px;">
                                    <h5 data-id="drag-notif">Buat section terlebih dahulu kemudian seret komponen dari sisi kiri ke area section</h5>
                                </div>
                            </div>
                            <form
                                onsubmit="return false;" 
                                id="target" 
                                class="form-konten mb-3" 
                                data-container-type="form"
                            >
                                <input type="hidden" name="unique_code" id="unique_code" value="{{$code}}">
                            @if(!is_null($data))
                                @foreach(json_decode($data->template_json) as $key => $item)
                                <div draggable="true" ondragstart="drag(event)" class="row section-container" data-section-type="{{$item->section}}" id="{{$item->id}}">
                                    <div class="col-md-12">
                                        <div class="position-relative">
                                            <h5 class="font-weight-bold section-title mb-3">{{$item->section}}</h5>
                                            <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="${id}">
                                                <button class="edit-section" type="button" data-id="{{$item->id}}">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </button>
                                                <button class="delete-section" type="button" data-id="{{$item->id}}">
                                                    <i class="fas fa-times"></i>
                                                </button>
                                            </div>
                                        </div>
                                        @if($item->id != "payment-method-section")
                                        <div
                                            class="p-3 section-wrapper mb-4" 
                                            ondrop="drop(event)"
                                            ondragover="allowDrop(event)"
                                            data-container-type="section"
                                            data-section-type="{{$item->section}}"
                                            data-id="{{$item->id}}"
                                            style="min-height:50vh; border:1px dashed #eee"
                                        >
                                        
                                            @foreach($item->value as $k => $i)
                                                <div 
                                                    class="form-group drag-element" 
                                                    data-id="{{$i->id}}" 
                                                    id="{{$i->id}}"
                                                    data-type="{{$i->type}}"
                                                    data-label="{{$i->label}}"
                                                    data-name="{{$i->name}}"
                                                    data-section-type="{{$i->section_type}}"
                                                    data-required="{{$i->required}}"
                                                    data-add-able="{{$i->is_add_able}}"
                                                    draggable="true" 
                                                    ondragstart="drag(event)" 
                                                    data-container-type="div"
                                                    style="position:relative"
                                                >
                                                    {!! $i->body !!}
                                                </div>
                                            @endforeach
                                        </div>
                                        @else
                                            @include('merchant::toko.profile.form-order.payment-method')
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                            @endif
                            </form>
                            <button id="btn-submit" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.btn-preview').tooltip();
    $('#fa-copy').tooltip('hide');
    function copyClipboard(url){
        let copyText = url;
        let el = document.createElement('input');
        document.body.appendChild(el);
        el.value = url;
        el.select();
        document.execCommand("copy");
        document.body.removeChild(el);
        $('#fa-copy').tooltip('show');
        setTimeout(() => {
            $('#fa-copy').tooltip('dispose');
        }, 1000);
    }
</script>
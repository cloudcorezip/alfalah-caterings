<style>
    .card-left-sidebar {
        position:sticky;
        top:80px;
        max-height: 90vh;
        overflow-y: auto;
    }
    .card-left-sidebar::-webkit-scrollbar {
        width: 10px;
    }
    .card-left-sidebar::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px #f5f3f3;
        border-radius: 10px;
    }
    .card-left-sidebar::-webkit-scrollbar-thumb {
        background: #eee;
        cursor: pointer;
    }
    .card-left-sidebar::-webkit-scrollbar-thumb:hover {
        background: #c4c3c3;
    }
    .drag-element, .drag-element .form-control, .drag-element .form-check, .section-container {
        cursor: move!important;
    }
    .bank-logo {
        width:230px;
        height:100px;
        object-fit:contain;
    }
    .btn:focus,.btn:active {
        outline: none !important;
        box-shadow: none;
    }
    .btn-field {
        border:1px solid #FFA943;
        border-radius: 4px;
        color: #FFA943;   
    }
    .btn-field:hover {
        background: #FFA943;
        color: #FFFFFF;
    }
    .edit-field, .edit-fiel, .edit-section, .delete-section{
        background-color: #fff;
        color:rgba(119, 119, 119, 0.79);
        border:none;
        border-radius: 4px;
    }
    .edit-fiel:hover, .edit-field:hover, .edit-section:hover {
        background-color:#72BA6C;
        color: #fff;
    }
    .delete-section:hover {
        background-color: #ff4400;
        color: #fff;
    }
    .edit-fiel:focus, .edit-field:focus, .edit-section:focus, .delete-section:focus {
        outline:none;
    }
    .add-opt {
        background-color:#F7F7F7;
        color:#C6C6C6;
    }
    .add-opt:hover {
        color: #FFA943;
    }
    .add-list {
        background-color: #FFF;
        border:none;
        color:#FFA943;
    }
    .add-list:hover {
        color: #FFA943;
    }
    .add-section {
        border: 1px solid #FF8100;
        color: #FF8100;
        background:transparent;
    }
    .add-section:hover {
        background-color: #FF8100;
        color: #FFFFFF;
    }
    .add-section:focus, .add-list:focus {
        outline:none;
    }
    .product-remove {
        cursor: pointer;
    }

    textarea.note_value.form-control {
        resize: vertical!important;
    }
    .btn-preview {
        background-color: #FF8A00;
        color: #FFF;
        border: 1px solid #FF8A00;
    }
    .btn-add-coa {
        border:1px solid #FFA943;
        border-radius: 4px;
        color: #FFA943;   
    }
    .btn-add-coa:hover {
        background: #FFA943;
        color: #FFFFFF;
    }
</style>
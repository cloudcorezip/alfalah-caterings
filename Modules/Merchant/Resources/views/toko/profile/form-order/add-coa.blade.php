<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Nama Akun</label>
                <input type="name" class="form-control form-control-sm" name="name" placeholder="Nama Akun" value="{{$data->name}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Kategori Akun</label>
                <select class="form-control form-control-sm" id="coa-category" name="acc_coa_category_id" disabled>
                    @foreach($categoryOption as $key =>$item)
                        <option value="{{$item->id}}" style="font-weight: bold" {{($item->id==$data->acc_coa_category_id)?'selected':''}}
                        {{($item->getChild->count()>0)?'disabled':''}}
                        >{{$item->code}} {{$item->name}}</option>
                        @foreach($item->getChild as $c)
                            <option value="{{$c->id}}"  {{($c->id==$data->acc_coa_category_id)?'selected':''}}
                                {{($c->getChild->count()>0)?'disabled':''}}
                            >--{{$c->code}} {{$c->name}}</option>
                            @foreach($c->getChild as $cc)
                                @if($cc->code == "6.1.02")
                                <option value="{{$cc->id}}" selected>----{{$cc->code}} {{$cc->name}}</option>
                                @endif
                            @endforeach
                        @endforeach
                    @endforeach
                </select>
            </div>
        </div>
        

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Tipe</label>
                <select class="form-control form-control-sm" name="type_coa" id="type_coa" required>
                    <option value="Debit">Debit</option>
                    <option value="Kredit">Kredit</option>
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Kurs</label>
                <select class="form-control form-control-sm" id="currency" name="md_sc_currency_id" required>
                    @foreach($currencyOption as $key =>$item)
                        <option value="{{$item->id}}" {{($item->id==$data->md_sc_currency_id)?'selected':''}}>{{$item->name}}</option>

                    @endforeach
                </select>
            </div>

        </div>

    </div>







    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-sm btn-success py-2 px-4">Simpan</button>

    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('setting.form-order.save-coa')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })

        $('#coa-category').select2();
        $('#currency').select2();
        $('#type_coa').select2();

    })

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Daftar Akun</h4>
                                        <span class="span-text">Untuk menambah daftar akun, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

<div class="card card-left-sidebar">
    <div class="card-body">
        <div class="alert" style="background-color:#FFEFDD;" role="alert">
            <span style="color:#7F7F7F;">Perhatian ! Untuk menggunakan form builder anda harus drag & drop kolom isian yang akan digunakan.</span>
        </div>
        <div class="mb-3">
            <button
                class="d-flex justify-content-between align-items-center mb-1" 
                type="button" 
                data-toggle="collapse" 
                data-target="#data-pelanggan" 
                aria-expanded="false" 
                aria-controls="data-penjemputan"
                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
            >
                <h6 class="font-weight-bold">Data Pelanggan</h6>
                <i class="fas fa-angle-down" style="font-size:16px;"></i>
            </button>
            <div class="collapse show" id="data-pelanggan">
                <form 
                    class="form-konten p-3" 
                    ondrop="drop(event)" 
                    ondragover="allowDrop(event)" 
                    style="min-height:100px;border:1px solid #DFDFDF; border-radius:10px;" 
                    data-container-type="form"
                    data-button-id="show-add-field-1"
                >
                    @foreach($form_template as $key => $item)
                        @if(!in_array($item->id, $form_ids))
                            <div 
                                class="form-group drag-element" 
                                data-id="{{$item->id}}" 
                                id="{{$item->id}}"
                                data-type="{{$item->type}}"
                                data-label="{{$item->label}}"
                                data-name="{{$item->name}}"
                                data-section-type="{{$item->section_type}}"
                                data-required="false"
                                draggable="true" 
                                ondragstart="drag(event)" 
                                data-container-type="div"
                                style="position:relative;"
                            >
                                <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="{{$item->id}}">
                                    <button class="edit-field" type="button" data-id="{{$item->id}}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                </div>
                                <label class="font-weight-bold">{{$item->label}}</label>
                                {!! $item->body !!}
                            </div>
                        @endif
                    @endforeach
                    <button 
                        id="show-add-field-1" 
                        type="button" 
                        class="btn btn-block btn-field" 
                        data-toggle="modal" 
                        data-target="#exampleModal" 
                        data-section="data-penjemputan"
                    >
                        <i class="fas fa-plus-circle mr-2"></i> Tambah Kolom Inputan
                    </button>
                </form>
                
            </div>
        </div>

        <div class="mb-3">
            <button
                class="d-flex justify-content-between align-items-center mb-1" 
                type="button" 
                data-toggle="collapse" 
                data-target="#data-biaya" 
                aria-expanded="false" 
                aria-controls="data-penjemputan"
                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
            >
                <h6 class="font-weight-bold">Data Biaya</h6>
                <i class="fas fa-angle-down" style="font-size:16px;"></i>
            </button>
            <div class="collapse show" id="data-biaya">
                <form 
                    class="form-konten p-3" 
                    ondrop="drop(event)" 
                    ondragover="allowDrop(event)" 
                    style="min-height:100px;border:1px solid #DFDFDF; border-radius:10px;" 
                    data-container-type="form"
                    data-button-id="button-coa"
                >
                    <button 
                        id="biaya-button" 
                        type="button" 
                        class="btn btn-block btn-field" 
                        data-toggle="modal" 
                        data-target="#modal-add-coa-biaya" 
                        data-section="data-biaya"
                    >
                        <i class="fas fa-plus-circle mr-2"></i> Tambah Biaya
                    </button>
                </form>
                
            </div>
        </div>

        <div class="mb-3">
            <button
                class="d-flex justify-content-between align-items-center mb-1" 
                type="button" 
                data-toggle="collapse" 
                data-target="#data-pendapatan" 
                aria-expanded="false" 
                aria-controls="data-pendapatan"
                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
            >
                <h6 class="font-weight-bold">Data Pendapatan</h6>
                <i class="fas fa-angle-down" style="font-size:16px;"></i>
            </button>
            <div class="collapse show" id="data-pendapatan">
                <form 
                    class="form-konten p-3" 
                    ondrop="drop(event)" 
                    ondragover="allowDrop(event)" 
                    style="min-height:100px;border:1px solid #DFDFDF; border-radius:10px;" 
                    data-container-type="form"
                    data-button-id="button-coa-pendapatan"
                >
                    <button 
                        id="pendapatan-button" 
                        type="button" 
                        class="btn btn-block btn-field" 
                        data-toggle="modal" 
                        data-target="#modal-add-coa-pendapatan" 
                        data-section="data-biaya"
                    >
                        <i class="fas fa-plus-circle mr-2"></i> Tambah Pendapatan
                    </button>
                </form>
                
            </div>
        </div>
        
        <div class="mb-3">
            <button
                class="d-flex justify-content-between align-items-center mb-1" 
                type="button" 
                data-toggle="collapse" 
                data-target="#product-list-collapse" 
                aria-expanded="false" 
                aria-controls="product-list-collapse"
                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
            >
                <h6 class="font-weight-bold">Pilih Layanan Penjemputan</h6>
                <i class="fas fa-angle-down" style="font-size:16px;"></i>
            </button>
            <div class="collapse" id="product-list-collapse">
                <div style="border:1px solid #DFDFDF; border-radius:10px;" class="p-3">
                    <div class="form-group mb-3">
                        <select name="list_product" id="list_product" multiple="multiple">
                            <option></option>
                            @foreach($product as $key => $item)
                            <option 
                                value="{{$item->id}}"
                                data-inv-id="{{$item->inv_id}}"
                                data-hpp-id="{{$item->hpp_id}}"
                                data-image="{{$item->foto}}"
                                @if(in_array($item->id, array_column($product_json, 'id')))
                                    selected
                                @endif
                            >
                                {{$item->name}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    @if(count($product_json) == 0)
                    <div 
                        class="drag-element" 
                        data-id="section-product" 
                        id="section-product"
                        data-type="radio"
                        data-label="Pilih Layanan Penjemputan"
                        data-name="product_id"
                        data-section-type="section-product"
                        data-required="true"
                        data-add-able="false" 
                        draggable="true" 
                        ondragstart="drag(event)"
                        style="position:relative;" 
                        data-container-type="div"
                    >     
                    </div>
                    @endif
                </div>
                
            </div>
        </div>

        <div class="mb-3">
            <button
                class="d-flex justify-content-between align-items-center mb-1" 
                type="button" 
                data-toggle="collapse" 
                data-target="#shipping-method-collapse" 
                aria-expanded="false" 
                aria-controls="shipping-method-collapse"
                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
            >
                <h6 class="font-weight-bold">Metode Pengiriman</h6>
                <i class="fas fa-angle-down" style="font-size:16px;"></i>
            </button>
            <div class="collapse" id="shipping-method-collapse">
                <div style="border:1px solid #DFDFDF; border-radius:10px;" class="p-3">
                    <div class="form-group mb-0">
                        <select name="shipping_method" id="shipping_method" multiple="multiple">
                            <option></option>
                            <option 
                                value="27"
                                @if(in_array(27, array_column($shipping_json, 'id')))
                                    selected
                                @endif
                                >Tidak Memakai Pengiriman</option>
                            <option 
                                value="28"
                                @if(in_array(28, array_column($shipping_json, 'id')))
                                    selected
                                @endif
                                >Intenal Pengiriman
                            </option>
                            @foreach($shipping_category as $key => $item)
                            <option 
                                value="{{$item->id}}"
                                @if(in_array($item->id, array_column($shipping_json, 'id')))
                                    selected
                                @endif
                            >
                                {{$item->name}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="mb-3">
            <button
                class="d-flex justify-content-between align-items-center mb-1" 
                type="button" 
                data-toggle="collapse" 
                data-target="#metode-pembayaran" 
                aria-expanded="false" 
                aria-controls="metode-pembayaran"
                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
            >
                <h6 class="font-weight-bold">Metode Pembayaran</h6>
                <i class="fas fa-angle-down" style="font-size:16px;"></i>
            </button>
            <div class="collapse" id="metode-pembayaran">
                <form
                    id="form-payment-method" 
                    data-id="form-payment-method"
                    class="form-konten p-3" 
                    ondrop="drop(event)" 
                    ondragover="allowDrop(event)" 
                    style="min-height:100px;border:1px solid #DFDFDF; border-radius:10px;" 
                    data-container-type="form"
                >
                    @if(!in_array("payment-method", $form_ids))
                    <div 
                        class="form-group drag-element" 
                        data-id="payment-method-section" 
                        id="payment-method"
                        data-section-type="metode-pembayaran"
                        draggable="true" 
                        ondragstart="drag(event)" 
                        data-container-type="div"
                        style="position:relative;"
                    >
                        @if(count($vaBank) > 0)
                            @if(!in_array("pm-va", $form_ids))
                            <div 
                                class="form-group" 
                                data-id="pm-va" 
                                id="pm-va"
                                data-type="radio"
                                data-label="Virtual Account"
                                data-name="payment_method"
                                data-section-type="metode-pembayaran"
                                data-required="false"
                                data-container-type="div"
                                style="position:relative;"
                            >
                                <label class="font-weight-bold">Virtual Account</label>
                                @foreach($vaBank as $key => $item)
                                    <div class="form-check">
                                        <input type="radio" class="form-check-input" name="payment_method" value="{{$item->id}}" data-label="{{$item->name}}">
                                        <label>{{$item->name}}</label>
                                    </div>
                                @endforeach
                            </div>
                            @endif
                        @endif
                        
                        @if(count($eWallet) > 0)
                            @if(!in_array("pm-ewallet", $form_ids))
                            <div 
                                class="form-group" 
                                data-id="payment-method-section" 
                                id="pm-ewallet"
                                data-type="radio"
                                data-label="E Wallet"
                                data-name="payment_method"
                                data-section-type="metode-pembayaran"
                                data-required="false"
                                data-container-type="div"
                                style="position:relative;"
                            >
                                <label class="font-weight-bold">E-Wallet</label>
                                @foreach($eWallet as $key => $item)
                                    <div class="form-check">
                                        <input type="radio" class="form-check-input" name="payment_method" value="{{$item->id}}" data-label="{{$item->name}}">
                                        <label>{{$item->name}}</label>
                                    </div>
                                @endforeach
                            </div>
                            @endif
                        @endif
                        
                        @if(!is_null($qris))
                            @if(!in_array('pm-qris', $form_ids))
                            <div 
                                class="form-group" 
                                data-id="payment-method-section" 
                                id="pm-qris"
                                data-type="radio"
                                data-label="QRIS"
                                data-name="payment_method"
                                data-section-type="metode-pembayaran"
                                data-required="false" 
                                data-container-type="div"
                                style="position:relative;"
                            >
                                <label class="font-weight-bold">QRIS</label>
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="payment_method" value="{{$qris->id}}" data-label="Qris">
                                    <label>Qris</label>
                                </div>
                            </div>
                            @endif
                        @endif

                        @if(!is_null($cash))
                            @if(!in_array('pm-cash', $form_ids))
                            <div>
                                <label>Tunai</label>
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="payment_method" value="{{$cash->id}}" data-label="Tunai">
                                    <label>{{$cash->name}}</label>
                                </div>
                            </div>
                            @endif
                        @endif
                    
                    </div>
                    @endif
                    
                    
                </form>
                                                
            </div>
        </div>
        
    </div>
</div>
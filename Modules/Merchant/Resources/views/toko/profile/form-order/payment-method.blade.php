<div
    class="p-3 section-wrapper mb-4"
    data-container-type="section-pm"
    data-section-type="Metode Pembayaran"
    data-id="payment-method-section"
    style="border:1px dashed #eee;"
    ondrop="drop(event)"
    ondragover="allowDrop(event)"
>
    <div 
        class="form-group drag-element" 
        data-id="payment-method-section" 
        id="payment-method"
        data-section-type="metode-pembayaran"
        draggable="true" 
        ondragstart="drag(event)" 
        data-container-type="div"
        style="position:relative;"
    >
    @if(count($vaBank) > 0)
        <div 
            class="form-group" 
            data-id="pm-va" 
            id="pm-va"
            data-type="checkbox"
            data-label="Virtual Account"
            data-name="payment_method"
            data-section-type="metode-pembayaran"
            data-required="false"
            data-container-type="div"
            style="position:relative;"
        >
            <label class="font-weight-bold">Virtual Account</label>
            @foreach($vaBank as $key => $item)
                <div class="form-check">
                    <input type="radio" class="form-check-input" name="payment_method" value="{{$item->id}}" data-label="{{$item->name}}">
                    <label>{{$item->name}}</label>
                </div>
            @endforeach
        </div>
        @endif
        
        @if(count($eWallet) > 0)
        <div 
            class="form-group" 
            data-id="payment-method-section" 
            id="pm-ewallet"
            data-type="checkbox"
            data-label="E Wallet"
            data-name="payment_method"
            data-section-type="metode-pembayaran"
            data-required="false"
            data-container-type="div"
            style="position:relative;"
        >
            <label class="font-weight-bold">E-Wallet</label>
            @foreach($eWallet as $key => $item)
                <div class="form-check">
                    <input type="radio" class="form-check-input" name="payment_method" value="{{$item->id}}" data-label="{{$item->name}}">
                    <label>{{$item->name}}</label>
                </div>
            @endforeach
        </div>
        @endif
        
        @if(!is_null($qris))
        <div 
            class="form-group" 
            data-id="payment-method-section" 
            id="pm-qris"
            data-type="checkbox"
            data-label="QRIS"
            data-name="payment_method"
            data-section-type="metode-pembayaran"
            data-required="false" 
            data-container-type="div"
            style="position:relative;"
        >
            <label class="font-weight-bold">QRIS</label>
            <div class="form-check">
                <input type="radio" class="form-check-input" name="payment_method" value="{{$qris->id}}" data-label="Qris">
                <label>Qris</label>
            </div>
        </div>
        @endif
    </div>

</div>


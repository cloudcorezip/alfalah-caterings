<!-- modal add new field -->
<div class="modal fade z1600" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Kolom</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <input type="hidden" name="section_type" id="section_type">
                    <input type="hidden" name="target_button_id" id="target_button_id">
                    <input type="hidden" name="is_edit" id="is_edit">
                    <input type="hidden" name="element_id" id="element_id">
                    <input type="hidden" name="is_add_able" id="is_add_able">
                    <!-- select input type -->
                    <div class="row" id="input-type-wrapper">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Pilih Tipe Kolom</label>
                                <select class="form-control" id="input-type">
                                    <option value="-1" selected>--- Pilih Tipe Kolom ---</option>
                                    <option value="text">Text Field</option>
                                    <option value="textarea">Text Area</option>
                                    <option value="checkbox">CheckBox</option>
                                    <option value="radio">Radio</option>
                                    <option value="checkbox" data-add-able="1">List Item</option>
                                    <option value="note">Catatan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end select input type -->

                    <!-- add input text field -->
                    <div class="row d-none field-container" id="text">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Label</label>
                                <input type="text" name="label" class="form-control custom-field" placeholder="Label">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-check d-flex align-items-center ml-2 mb-1">
                                <input 
                                    class="form-check-input custom-field" 
                                    type="checkbox" 
                                    value="1" 
                                    style="margin-top:0;"
                                    name="required"
                                >
                                <label class="form-check-label">
                                    Required
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- end add input text field -->

                    <!-- add textarea field -->
                    <div class="row d-none field-container" id="textarea">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Label</label>
                                <input type="text" name="label" class="form-control custom-field" placeholder="Label">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-check d-flex align-items-center ml-2 mb-1">
                                <input 
                                    class="form-check-input custom-field" 
                                    type="checkbox" 
                                    value="1" 
                                    style="margin-top:0;"
                                    name="required"
                                >
                                <label class="form-check-label">
                                    Required
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- end add textarea field -->

                    <!-- add checkbox field -->
                    <div class="row d-none field-container" id="checkbox">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Label</label>
                                <input type="text" name="label" class="form-control custom-field" placeholder="Label">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mb-0">
                                <label class="font-weight-bold">Pilihan</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                <input type="text" name="checkbox_val" class="form-control custom-field">
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="checkbox_val" class="form-control custom-field">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn add-opt" id="add-opt" type="button">
                                <i class="fas fa-plus-circle"></i>
                            </button>
                        </div>
                        <div class="col-md-12 mt-2">
                            <div class="form-check d-flex align-items-center ml-2 mb-1">
                                <input 
                                    class="form-check-input custom-field" 
                                    type="checkbox" 
                                    value="1" 
                                    style="margin-top:0;"
                                    name="required"
                                >
                                <label class="form-check-label">
                                    Required
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- end add checkbox field -->

                    <!-- add radio field -->
                    <div class="row d-none field-container" id="radio">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Label</label>
                                <input type="text" name="label" class="form-control custom-field" placeholder="Label">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group mb-0">
                                <label class="font-weight-bold">Pilihan</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                <input type="text" name="checkbox_val" class="form-control custom-field">
                                </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="checkbox_val" class="form-control custom-field">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn add-opt" id="add-opt" type="button">
                                <i class="fas fa-plus-circle"></i>
                            </button>
                        </div>
                        <div class="col-md-12 mt-2">
                            <div class="form-check d-flex align-items-center ml-2 mb-1">
                                <input 
                                    class="form-check-input custom-field" 
                                    type="checkbox" 
                                    value="1" 
                                    style="margin-top:0;"
                                    name="required"
                                >
                                <label class="form-check-label">
                                    Required
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- end add radio field -->

                    <!-- add note field -->
                    <div class="row d-none field-container" id="note">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan</label>
                                <textarea id="note_value" name="note_value" class="form-control note_value" placeholder="Tulis Catatan"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- end add note field -->
                </form>
            </div>
            <div class="modal-footer">
                <button id="save-field" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal add new field -->

<!-- modal add section -->
<div class="modal fade z1600" id="modal-add-section" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-section-label">Tambah Section</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <input type="hidden" name="section_id" id="section_id">
                    <input type="hidden" name="is_edit_section" id="is_edit_section">

                    <!-- add input text field -->
                    <div class="row" id="section">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Section</label>
                                <input type="text" id="section_name" name="section_name" class="form-control custom-section" placeholder="Nama Section">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="save-section" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal add section -->

<!-- modal delete section -->
<div class="modal fade z1600" id="modal-delete-section" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="modal-delete-section-label">Perhatian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <input type="hidden" name="section_delete_id" id="section_delete_id">
                </form>
                <h6>Apakah anda yakin untuk menghapus section ini ?</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light py-2" data-dismiss="modal">Batal</button>
                <button id="btn-delete-section" style="border-radius:0.5rem!important;" class="btn btn-danger rounded-0 py-2">Hapus</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal delete section -->

<!-- modal add list -->
<div class="modal fade z1600" id="modal-add-list" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-list-label">Tambah List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <input type="hidden" name="list_item_id" id="list_item_id">
                    <div class="form-group">
                        <input class="form-control" type="text" name="list_name" id="list_name" placeholder="Nama List">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-add-list" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Tambah</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal add list -->

<!-- modal add coa biaya -->
<div class="modal fade z1600" id="modal-add-coa-biaya" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-list-coa-biaya">Tambah Biaya</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <input type="hidden" name="coa_biaya_id" id="coa_biaya_id">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Pilih Akun</label>
                        <select name="coa_biaya" id="coa_biaya" class="form-control form-control-sm">
                            <option value="-1">--- Pilih Biaya ---</option>
                            @foreach($coa as $key => $item)
                                @if($item->code == "6.0.00")
                                    @foreach($item->getChild->whereIn('is_deleted',[0,2]) as $n =>$c)
                                        @foreach($c->getChild->whereIn('is_deleted',[0,2]) as $c2 =>$cc)
                                            @foreach($cc->getDetail->whereIn('is_deleted',[0,2]) as $d =>$dd)
                                                <option data-label="{{$dd->name}}" value="{{$dd->code}}">----{{$dd->code}} {{$dd->name}}</option>
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                @endif
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-add-coa-biaya" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Tambah</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal coa biaya -->

<!-- modal add coa pendapatan -->
<div class="modal fade z1600" id="modal-add-coa-pendapatan" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-list-coa-pendapatan">Tambah Pendapatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-konten">
                    <input type="hidden" name="coa_pendapatan_id" id="coa_pendapatan_id">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Pilih Akun</label>
                        <select name="coa_pendapatan" id="coa_pendapatan" class="form-control form-control-sm">
                            <option value="-1">--- Pilih Pendapatan ---</option>
                            @foreach($coa as $key => $item)
                                @foreach($item->getChild->whereIn('is_deleted',[0,2]) as $n =>$c)
                                    @if($c->getDetail->whereIn('is_deleted',[0,2])->count()>0)
                                        @foreach($c->getDetail->whereIn('is_deleted',[0,2]) as $d =>$dd1)
                                            @if($dd1->code[0] == "4")
                                            <option data-label="{{$dd1->name}}" value="{{$dd1->code}}">----{{$dd1->code}} {{$dd1->name}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="form-group">
                        <label>Nama Pendapatan</label>
                        <input type="text" name="field_pendapatan_label" id="field_pendapatan_label" class="form-control" placeholder="Nama Pendapatan">
                    </div> --}}
                </form>
            </div>
            <div class="modal-footer">
                <button id="btn-add-coa-pendapatan" style="border-radius:0.5rem!important;" class="btn btn-order__online rounded-0 py-2">Tambah</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal coa biaya -->


<script>
    function calcHeight(value) {
        let numberOfLineBreaks = (value.match(/\n/g) || []).length;
        let newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
        return newHeight + 80;
    }

    $(document).on("keyup", "#note_value", function(){
        let height = calcHeight($(this).val()) + "px!important";
        $(this).attr("style", `height: ${height}`);
    })
</script>

<script>
    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });
</script>
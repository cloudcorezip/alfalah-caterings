@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">{{$title}}</h4>
                <span>Masukan informasi usahamu, atur metode pembayaran, izin/cuti, reset data dan pengaturan lainnya.</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4 py-4 px-4">
            <div class="row">
                <div class="col-lg-3 col-md-12 mb-3">

                    <ul class="list-group setting-sidebar-ul">
                        <li class="list-group-item @if(is_null($page))active @endif">
                            <a onclick="activeMenu('pengaturan','pengaturan')"  href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}" class="font-weight-bold">Profil Usaha</a>
                        </li>
                        <li class="list-group-item">
                            <a  href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle font-weight-bold">Data Pembayaran</a>
                            <ul @if($page!='digital-payment' && $page!='payment-method') class="collapse" @endif  id="pageSubmenu">
                                <li class="list-group-item @if($page=='digital-payment') active @endif">
                                    <a href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=digital-payment">Akun Bank Penarikan</a>
                                </li>
                                <li class="list-group-item @if($page=='payment-method') active @endif">
                                    <a href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=payment-method">Metode Pembayaran</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{route('payment-invoice.index')}}">Template Invoice</a>
                                </li>
                            </ul>
                        </li>

                        @if(count(\App\Utils\Merchant\MerchantUtil::getBranch($data->id))>1)
                        <li class="list-group-item @if($page=='multi-branch') active @endif">
                            <a href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=multi-branch" class="font-weight-bold">Akses Cabang</a>
                        </li>
                        @elseif(merchant_detail_multi_branch($data->id)->is_branch==0)
                            <li class="list-group-item @if($page=='multi-branch') active @endif">
                                <a href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=multi-branch" class="font-weight-bold">Akses Cabang</a>
                            </li>
                        @else

                        @endif

                        <li class="list-group-item @if($page=='role-access') active @endif">
                            <a href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=role-access" class="font-weight-bold">Akses Menu Karyawan</a>
                        </li>

                        <li class="list-group-item @if($page=='inventory') active @endif">
                            <a onclick="activeMenu('pengaturan','pengaturan')"  href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=inventory" class="font-weight-bold">Metode Persediaan</a>
                        </li>
                        @if(merchant_detail_multi_branch($data->id)->is_branch==0)
                            <li class="list-group-item">
                                <a  href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle font-weight-bold">Mekanisme Presensi</a>
                                <ul @if($page!='shift' && $page!='attendance') class="collapse" @endif  id="pageSubmenu2">
                                    <li class="list-group-item @if($page=='attendance') active @endif">
                                        <a href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=attendance">Presensi</a>
                                    </li>
                                    <li class="list-group-item @if($page=='shift') active @endif">
                                        <a href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=shift">Shift</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="list-group-item @if($page=='leave') active @endif">
                                <a onclick="activeMenu('pengaturan','pengaturan')"  href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=leave" class="font-weight-bold">Cuti / Izin</a>
                            </li>

                            @if(env('APP_ENV') != 'production')
                            <li class="list-group-item @if($page=='custom-code') active @endif">
                                <a onclick="activeMenu('pengaturan','pengaturan')"  href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=custom-code" class="font-weight-bold">Custom Kode Transaksi</a>
                            </li>
                            @endif
                        @endif

                        @if(merchant_detail_multi_branch($data->id)->is_branch==0)
                            @if(checkPlugin(merchant_id(),'sfa')!=false)
                                <li class="list-group-item @if($page=='sfa') active @endif">
                                    <a href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=sfa" class="font-weight-bold">Aplikasi Sales</a>
                                </li>
                            @endif
                        @endif

                        <li class="list-group-item @if($page=='reset-data') active @endif">
                            <a onclick="activeMenu('pengaturan','pengaturan')"  href="{{route('merchant.toko.profile.detail',['id'=>$data->id])}}?page=reset-data" class="font-weight-bold">Reset Data</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 col-lg-9 col-md-12 mb-3" style="border-left:1px solid rgba(186, 186, 186, 0.32);">
                    @if(is_null($page))
                        @include('merchant::toko.profile.company',['page'=>$page,'title'=>$title])
                    @endif


                    @if($page=='attendance' || $page=='shift')
                        @include('merchant::toko.profile.attendance-setting.index',['page'=>$page,'presensi'=>$presensi,'title'=>($page=='attendance')?'Presensi':'Shift',
                   ])
                    @endif

                        @if($page=='sfa')
                            @include('merchant::toko.profile.plugin.sales',['page'=>$page,'sfa'=>$sfa,'title'=>'Aplikasi Sales',
                       ])
                        @endif


                    @if($page=='leave')
                        @include('merchant::toko.profile.attendance-setting.index-leave',['page'=>$page,'title'=>'Cuti/Izin',])
                    @endif

                    @if($page=='digital-payment')
                     @include('merchant::toko.profile.digital-payment')
                    @endif
                    @if($page == 'payment-method')
                        @include('merchant::toko.profile.payment-method')
                    @endif
                    @if($page == 'inventory')
                       @include('merchant::toko.profile.inventory')
                    @endif
                    @if(get_role()==3)
                        @if($page=='role-access')
                           @include('merchant::toko.profile.role-access')
                        @endif
                        @if($page=='reset-data')
                           @include('merchant::toko.profile.reset-data')
                        @endif

                        @if($page=='multi-branch')
                           @include('merchant::toko.profile.multi-branch')
                        @endif
                    @endif

                    @if(env('APP_ENV') != 'production')
                        @if(merchant_detail_multi_branch($data->id)->is_branch == 0)
                            @if($page == 'custom-code')
                                @include('merchant::toko.profile.custom-code.index')
                            @endif
                        @endif
                    @endif
                </div>
            </div>
        </div>

    </div>
    @include('backend-v2.layout.daterangepicker')

    @if($page=='p-gcontact')
        <script>
            $(document).ready(function () {
                $('#form-konten-p-gcontact').submit(function () {
                    var data = getFormData('form-konten-p-gcontact');
                    ajaxTransfer("{{route('plugin-gcontact.save')}}", data, function (response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                    });
                })
            })
        </script>
    @endif

@endsection






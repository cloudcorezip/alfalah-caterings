<div id="result-form-konten-time"></div>

<form onsubmit="return false;" id="form-konten-time" class='form-horizontal' backdrop="">
        <div class="form-group">
            @if ($isCreate==0)
                <h4>{{$data->day_name}}</h4>
                <input type="hidden" name="day_name" value="{{$data->day_name}}">
            @else 
            <label> Hari:</label>
            <select id="inputState" class="form-control form-control-sm" name="day_name">
                <option value="Senin">Senin</option>
                <option value="Selasa">Selasa</option>
                <option value="Rabu">Rabu</option>
                <option value="Kamis">Kamis</option>
                <option value="Jumat">Jum'at</option>
                <option value="Sabtu">Sabtu</option>
                <option value="Minggu">Minggu</option>
            </select>
                
            @endif 

        </div>
        <div class="form-group">
            <label> Buka:</label>
            <input type="time" class="form-control form-control-sm" name="start_time" @if ($isCreate==0)
                value="{{$data->service_time['start_time']}}"
            @endif  required>
        </div>
        <div class="form-group">
            <label> Tutup:</label>
            <input type="time" class="form-control form-control-sm" name="end_time"  @if ($isCreate==0) 
            value="{{$data->service_time['end_time']}}"
            @endif required>
        </div>

            <div class="form-group">
                <label >Status:</label>
                <select id="inputState" class="form-control form-control-sm" name="status">
                    <option @if ($data->status=='false')
                        selected
                    @endif value="false">Nonaktif</option>
                    <option @if ($data->status=='true')
                        selected
                    @endif value="true">Aktif</option>
                </select>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
            <button class="btn btn-success">Simpan</button>
        </div>
        <input type='hidden' name='id' value='{{$data->id }}'>
        <input type='hidden' name='md_user_id' value='{{$data->md_user_id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    </form>
    <script>
    $(document).ready(function () {
        $('#form-konten-time').submit(function () {
            var data = getFormData('form-konten-time');
            ajaxTransfer("{{route('merchant.toko.profile.save-time')}}", data, '#result-form-konten-time');
        })
    })
    </script>

    <script>

        $(document).ready(function(){
            $('.modal-dialog').addClass('modal-dialog-centered');
            $('.modal-dialog').removeClass('modal-lg');
        })

        $(document).ready(function(){
            $('.modal-header').find('.modal-title').remove();
            $('.modal-header').addClass('py-3');
            $('.modal-header').prepend(`<div>
                                            <h5 class="modal-title">Tambah Jam Operasional</h5>
                                        </div>`)
        });
    </script>

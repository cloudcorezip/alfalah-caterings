<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">Akses Menu Karyawan</h4>
    <span style="color:#979797">Atur hak akses menu setiap karyawanmu disini</span>
    <hr>
</div>
<div class="card-body">
    <form onsubmit="return false;" id="form-konten-access" class='form-horizontal' backdrop="">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="font-weight-bold">Pilih Karyawan</label>
                    <select id="staff_user_id" class="form-control"  name="staff_user_id" onchange="getMenu(1)"  required>
                        <option disabled>--Pilih Karyawan--</option>
                        @foreach ($employee as $b)
                            <option  value="{{$b->id}}">{{$b->fullname}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @php
            $no=0;
            @endphp
            <div class="col-md-12 ml-4">
                <div class="row">
                    @foreach($menuList as $key => $value)
                        @if(checkPlugin(merchant_id(),$value->is_type)==true)
                            <div class="col-md-4 mt-4">
                                <h5 class="font-weight-bold" style="color:#515151;">{{$value->name}}</h5>
                                @if($value->getChild->count()>0)
                                    @foreach($value->getChild->where('is_active',1)->sortBy('id') as $key =>$child)
                                        @if(checkPlugin(merchant_id(),$child->is_type)==true)
                                            @if($child->getChild->count()>0)
                                                <h6 class="font-weight-bold" style="color:#515151;">
                                                    <input type="checkbox" class="form-check-input menu-check2 menu-check{{$child->id}}" name="is_active[]" value="{{$child->id}}">
                                                    {{$child->name}}
                                                </h6>
                                                @foreach($child->getChild->where('is_active',1)->sortBy('id') as $key2 =>$child2)
                                                    @if(checkPlugin(merchant_id(),$child2->is_type))
                                                        <h6 class="mt-2 ml-4" style="color:#515151;">
                                                            <input type="checkbox" class="form-check-input menu-check2 menu-check{{$child2->id}}" name="is_active[]" value="{{$child2->id}}">
                                                            {{$child2->name}}
                                                        </h6>
                                                    @endif
                                                @endforeach
                                            @else
                                                <h6 class="font-weight-bold" style="color:#515151;">
                                                    <input type="checkbox" class="form-check-input menu-check2 menu-check{{$child->id}}" name="is_active[]" value="{{$child->id}}">
                                                    {{$child->name}}
                                                </h6>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif

                            </div>
                        @endif
                    @endforeach
                </div>

            </div>

        </div>
        <hr>
        <div class="row">
            <div class="col-sm-12 text-right">
                <button class="btn btn-success" type="submit" ><i class="fa fa-save mr-1"></i> Simpan</button>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function () {
        $("#staff_user_id").select2();
        getMenu(0);
    })

    $('#form-konten-access').submit(function () {
        var data = getFormData('form-konten-access');
        ajaxTransfer("{{route('merchant.menu.save')}}", data, function(response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
        });
    })

    function getMenu(isRefresh=0)
    {
        var staffId=$("#staff_user_id").val();
        if(isRefresh==1){
            $(".menu-check2").prop('checked', false);
        }

        ajaxTransfer('{{route('merchant.menu.staff')}}?staff_id='+staffId, {}, function(response) {
            if(response.length>0){
                response.forEach(item=>{
                    $(".menu-check"+item.menu_merchant_id).prop('checked', true)
                })
            }else{
                $(".menu-check2").prop('checked', false)

            }
        }, false, false)
    }

</script>

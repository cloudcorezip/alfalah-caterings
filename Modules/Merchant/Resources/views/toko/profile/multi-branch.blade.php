<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">Akses Cabang</h4>
    <span style="color:#979797">Atur hak akses Cabangmu agar bisa melakukan tambah,edit dan hapus data disini</span>
    <hr>
</div>
<div class="card-body">
    <form onsubmit="return false;" id="form-konten-multi-branch" class='form-horizontal' backdrop="">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Master Data
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk mengelola master data meliputi data kategori, merk, produk dan gudang )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="master_data"
                            name="menus"
                            value="master_data"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="master_data"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Kategori Pelanggan & Supplier
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menghapus, mengubah dan menambah data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="category"
                            name="menus"
                            value="category"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="category"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Pelanggan
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menghapus, mengubah dan menambah data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="customer"
                            name="menus"
                            value="customer"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="customer"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Supplier
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menghapus, mengubah dan menambah data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="supplier"
                            name="menus"
                            value="supplier"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="supplier"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Promo, Kupon , Loyalti
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="crm"
                            name="menus"
                            value="crm"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="crm"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Komisi
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="commission"
                            name="menus"
                            value="commission"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="commission"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Grade, Karyawan, Jabatan dan Departemen
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="employee"
                            name="menus"
                            value="employee"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="employee"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Tipe & Data Gudang
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="inventory"
                            name="menus"
                            value="inventory"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="inventory"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Pembelian
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="purchase"
                            name="menus"
                            value="purchase"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="purchase"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Mutasi dan Produksi
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="mutation"
                            name="menus"
                            value="mutation"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="mutation"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Penyesuaian Stok dan Stok Opname
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="stock_adjustment"
                            name="menus"
                            value="stock_adjustment"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="stock_adjustment"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Keuangan
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus menu daftar akun dan pengaturannya )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="finance"
                            name="menus"
                            value="finance"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="finance"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Aset Usaha
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah dan menghapus data )</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="asset"
                            name="menus"
                            value="asset"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="asset"></label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="font-weight-bold" style="color:#515151;">
                        Mekanisme Presensi, Master Data : Shift , Izin/Cuti, Hari Libur
                    </h6>
                    <h6 class="mt-2 ml-4" style="color:#515151;">
                        <i>(Izinkan cabang untuk menambah, mengubah  dan menghapus data)</i>
                    </h6>
                </div>
                <div class="col-md-2">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input access-branch"
                            id="attendance_master"
                            name="menus"
                            value="attendance_master"
                            style="transform:scale(2)"
                        >
                        <label class="custom-control-label" for="attendance_master"></label>
                    </div>
                </div>
            </div>
        </div>



        <hr>
    </form>
</div>
<script>

    $(document).ready(function () {
        getList()
    })

    function getList() {
        ajaxTransfer('{{route('merchant.toko.setting.multi-branch.list')}}', {}, function(response) {

            if(response.data.length>0)
            {
                var data=response.data

                data.forEach(item=>{
                    if(item.is_available_add==1){
                        $("#"+item.menu_name).prop('checked', true)
                    }else{
                        $("#"+item.menu_name).prop('checked', false)
                    }
                })
            }
        }, false, false)
    }
</script>
<script>

    $(".access-branch").change(function() {
        if(this.checked) {
            $.ajax({
                type: 'POST',
                data: {
                    menu: $(this).val(),
                    is_active:1,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.setting.multi-branch.update-status')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getList()
                }
            })
        }else{
            $.ajax({
                type: 'POST',
                data: {
                    menu: $(this).val(),
                    is_active:0,
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.setting.multi-branch.update-status')}}",
                success:function(response){
                    otherMessage(response.type,response.message)
                    getList()
                }
            })
        }
    });

</script>

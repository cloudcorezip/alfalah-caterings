<style>
    /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Bank</label>
        <select id="bank_id" class="form-control form-control-sm" name="bank_id" required>
            <option value="-1" disabled selected>--Pilih Bank--</option>
            @foreach($bank as $b)
            <option value="{{$b->id.'_'.$b->name}}">{{$b->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nomor Rekening</label>
        <input type="number" id="bank_account_number" class="form-control form-control-sm" name="bank_account_number" placeholder="No.Rek" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nama Pemilik Akun Rekening</label>
        <input type="text" id="bank_account_name" class="form-control form-control-sm" name="bank_account_name" placeholder="Nama Akun Rekening" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nama Akun Akuntansi</label>
        <input type="text" id="coa_name" class="form-control form-control-sm" name="coa_name" placeholder="Nama Akun Akuntansi">
    </div>
    <div class="form-check d-flex align-items-center mb-3">
         <input type="checkbox" name="add_new_coa" class="form-check-input" id="add_new_coa">
        <label class="form-check-label" for="add_new_coa">Pakai akun akuntansi yang sudah ada</label>
    </div>
    <div id="div_coa_old">
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Akun Akuntansi</label>
            <select id="acc_coa_id" class="form-control form-control-sm" name="acc_coa_id">
                <option value="-1" disabled selected>--Pilih Akun Akuntansi--</option>
                @foreach($acc_coa as $b)
                <option value="{{$b->id.'_'.$b->name}}">{{$b->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='type' value='13'>
    <input type='hidden' name='md_merchant_id' value='{{merchant_id() }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#bank_id').select2();
        $('#acc_coa_id').select2();
        $('#div_coa_old').hide();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.payment-method.save-bank')}}", data,function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })
    $('#add_new_coa').click(function() {
        if ($(this).is(':checked')) {
            $('#div_coa_old').show()
        }else{
            $('#div_coa_old').hide()
        }
    });
    $( "#acc_coa_id" ).change(function() {
        var coa= $(this).val();
        var coa_split = coa.split("_");
        $('#coa_name').val(coa_split[1]);
    });
    $( "#bank_id" ).change(function() {
        var bank_name= $(this).val();
        var coa_split = bank_name.split("_");

        if($(this).val()!=null && $("#bank_account_number").val()!=null){
            let name = coa_split[1];
            let number = $("#bank_account_number").val();
            let result = name.concat("-", number);
            $('#coa_name').val(result);

        }
    });
    $( "#bank_account_number" ).change(function() {
        var bank_name= $("#bank_id").val();
        var coa_split = bank_name.split("_");

        let name = coa_split[1];
        let number = $("#bank_account_number").val();
        let result = name.concat("-", number);
        $('#coa_name').val(result);

    });
    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Bank Transfer</h4>
                                        <span class="span-text">Untuk menambah bank transfer, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

</script>

<div class="mb-3">
    <h4 class="font-weight-bold" style="color:#323232;">Metode Persediaan</h4>
    <span style="color:#979797">Atur metode persediaan untuk mempermudah pengelolaan stok produk.</span>
    <hr>
</div>
<div class="card-body">
    <div class="row">
        <div class="col-md-8">
            <form onsubmit="return false;" id="form-konten-inv" class='form-horizontal' backdrop="">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="font-weight-bold">Metode Persediaan</label>
                            <select id="invId" class="form-control"  name="md_inventory_method_id">
                                <option disabled @if ($data->md_inventory_method_id==null)
                                selected
                                    @endif >--Pilih Metode Persediaan--</option>
                                @foreach ($inventory as $b)
                                    <option @if ($b->id==$data->md_inventory_method_id)
                                            selected
                                            @endif value="{{$b->id}}">{{$b->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-success" type="submit" >
                            <i class="fa fa-save mr-1"></i>
                            Simpan</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>


<script>
    $(document).ready(function () {
        $('#invId').select2();

    })

    $('#form-konten-inv').submit(function () {
        var data = getFormData('form-konten-inv');
        ajaxTransfer("{{route('merchant.toko.profile.save-inv')}}", data, function(response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
        });
    })

</script>

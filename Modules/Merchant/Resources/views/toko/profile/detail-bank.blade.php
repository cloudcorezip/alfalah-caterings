<div id="result-form-konten"></div>
{{-- @dd($data) --}}
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Bank</label>
        <select id="bank_id" class="form-control form-control-sm" name="bank_id">
            @foreach($bank as $b)
            <option @if($b->id==$data->md_va_bank_id) selected @endif value="{{$b->id.'_'.$b->name}}">{{$b->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nomor Rekening</label>
        <input type="text" class="form-control form-control-sm" id="bank_account_number" name="bank_account_number" value="{{$data->bank_account_number}}" placeholder="No.Rek" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nama Pemilik Akun Rekening</label>
        <input type="text" class="form-control form-control-sm" name="bank_account_name" value="{{$data->bank_account_name}}" placeholder="Nama Pemilik Akun Rekening" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Akun Akuntansi</label>
        <select id="acc_coa_id" class="form-control form-control-sm" name="acc_coa_id">
            @foreach($acc_coa as $b)
            <option @if($b->id==$data->acc_coa_id) selected @endif value="{{$b->id.'_'.$b->name}}">{{$b->name}}</option>
            @endforeach
        </select>
    </div>
    <div id="div_coa_name">
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Nama Akun Akuntansi</label>
            <input type="text" id="coa_name" class="form-control form-control-sm" name="coa_name" value="{{$data->name}}" placeholder="Nama Akun Akuntansi" required>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id}}'>
    <input type='hidden' name='md_merchant_id' value='{{merchant_id() }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#bank_id').select2();
        // $('#div_coa_name').hide();
        $('#acc_coa_id').select2();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.payment-method.update-bank-detail')}}", data,function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })
    $( "#acc_coa_id" ).change(function() {
        var coa= $(this).val();
        var coa_split = coa.split("_");
        $('#coa_name').val(coa_split[1]);
    });
    $( "#bank_id" ).change(function() {
        var bank_name= $(this).val();
        var coa_split = bank_name.split("_");

        if($(this).val()!=null && $("#bank_account_number").val()!=null){
            let name = coa_split[1];
            let number = $("#bank_account_number").val();
            let result = name.concat("-", number);
            $('#coa_name').val(result);

        }
    });
    $( "#bank_account_number" ).change(function() {
        var bank_name= $("#bank_id").val();
        var coa_split = bank_name.split("_");

        let name = coa_split[1];
        let number = $("#bank_account_number").val();
        let result = name.concat("-", number);
        $('#coa_name').val(result);

    });
    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Ubah Bank Transfer</h4>
                                        <span class="span-text">Untuk mengubah bank transfer, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

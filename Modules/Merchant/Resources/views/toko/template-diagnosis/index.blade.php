@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.template-diagnosis.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa mengatur template diagnosamu sesuai yang ingin dicantukam dihalaman ini.</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4 rounded py-4">
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="background:#FFF4E8!important;z-index:1!important;">
                            <strong class="d-block color-323232">Catatan :</strong>
                            <span class="color-535353">Kamu bisa mengatur template diagnosamu sesuai yang ingin dicantukam dihalaman ini.</span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mx-auto" style="width:80%;">
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <h4 class="font-weight-bold mb-0 color-323232">{{ucwords($merchant->name)}}</h4>
                            <span class="color-a4a4a4"><i>{{$merchant->address}}</i></span>
                        </div>
                        <div class="col-md-6 text-right">
                            @if(!is_null($merchant->image))
                            <img src="{{env('S3_URL')}}{{$merchant->image}}" alt="logo" class="img-fluid" style="width:13%;">
                            @else
                            <div class="text-center px-4 py-3" style="background-color: #F0F0F0;width: 20%; margin-left:auto; margin-right:0;">
                                <span>Logo</span>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="font-weight-bold mb-3">IDENTITAS MEMBER</h5>
                        </div>
                        <div class="col-lg-6 col-md-12 py-4">
                            <table class="table-note">
                                <tr>
                                    <td class="font-weight-bold font-14 color-323232">No. Member</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">PS-0112120-002</td>
                                </tr>
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">No. Reservasi</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">R-001</td>
                                </tr>
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">Nama</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">Armah</td>
                                </tr>
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">Status</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">Belum Menikah</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6 col-md-12 py-4">
                            <table class="table-note">
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">Tanggal Lahir</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">Lebak, 10/20/ 2022</td>
                                </tr>
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">Jenis Kelamin</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">Laki - Laki</td>
                                </tr>
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">Usia</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">18 Tahun</td>
                                </tr>
                                <tr>
                                    <td class="font-14 font-weight-bold color-323232">Agama</td>
                                    <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                                    <td class="font-14 color-a4a4a4">Islam</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn btn-sm py-2 px-4 btn-outline-warning dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Buat Pertanyaan
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.template-diagnosis.add', ['type'=>'text'])}}">Buat Pertanyaan</a>
                                <a class="dropdown-item" onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.template-diagnosis.add', ['type' => 'radio'])}}">Buat Pilihan Ganda</a>
                                <a class="dropdown-item" onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.template-diagnosis.add', ['type'=>'file'])}}">Buat File</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="font-weight-bold color-323232 mb-4">PERTANYAAN DIAGNOSA</h5>
                        </div>

                        @foreach($data as $key => $item)
                            @if($item->type == 'text')
                            <div class="col-md-12 position-relative mb-3">
                                <h6 class="font-weight-bold">{{$key + 1}}. {{$item->question}}</h6>
                                <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="{{$item->id}}">
                                    <button class="edit-section" type="button" onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.template-diagnosis.add', ['id'=>$item->id,'type'=>$item->type])}}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                    <button onclick="deleteData('{{$item->id}}')" class="delete-section" type="button" >
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            @endif

                            @if($item->type == 'radio')
                            <div class="col-md-12 mb-3">
                                <h6 class="font-weight-bold">{{$key + 1}}. {{$item->question}}</h6>
                                <div class="row ml-3">
                                    @php
                                        $num = 'A';
                                    @endphp
                                    @foreach(json_decode($item->option) as $k => $i)
                                        <div class="col-md-3 mb-2">
                                            <h6>{{$num}}. {{$i}}</h6>
                                        </div>
                                        @php
                                            $num++;
                                        @endphp
                                    @endforeach
                                </div>
                                <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="{{$item->id}}">
                                    <button class="edit-section" type="button" onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.template-diagnosis.add', ['id'=>$item->id,'type'=>$item->type])}}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                    <button onclick="deleteData('{{$item->id}}')" class="delete-section" type="button" >
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            @endif

                            @if($item->type == 'file')
                            <div class="col-md-12 mb-3">
                                <h6 class="font-weight-bold">{{$key + 1}}. {{$item->question}}</h6>
                                <div class="custom-field" style="position:absolute; top:0;right:0;" data-id="{{$item->id}}">
                                    <button class="edit-section" type="button" onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.template-diagnosis.add', ['id'=>$item->id,'type'=>$item->type])}}">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                    <button onclick="deleteData('{{$item->id}}')" class="delete-section" type="button" >
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            @endif
                        @endforeach

                        <div class="col-md-12 mt-3 text-right">
                            <button 
                                class="btn btn-success py-2 px-4"
                                onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.template-diagnosis.preview')}}"
                            >
                                Preview
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@section('js')
<script>
    function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apa anda yakin menghapus pertanyaan ?", function () {
            ajaxTransfer("{{route('merchant.toko.template-diagnosis.delete')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    }
</script>
@endsection

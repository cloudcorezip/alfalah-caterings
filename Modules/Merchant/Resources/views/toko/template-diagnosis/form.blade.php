@if($type == 'text')
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal form-konten' backdrop="">
    <div class="form-group">
        <label class="font-weight-bold" for="exampleInputPassword1">Pertanyaan</label>
        <input type="name" class="form-control" name="question" placeholder="Pertanyaan" value="{{$data->question}}" required>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type="hidden" name="type" value="text">
</form>
@endif


@if($type == 'radio')
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal form-konten' backdrop="">
    <div class="form-group">
        <label class="font-weight-bold" for="exampleInputPassword1">Pertanyaan</label>
        <input type="name" class="form-control" name="question" placeholder="Pertanyaan" value="{{$data->question}}" required>
    </div>

    <div class="form-group">
        <label class="font-weight-bold">Pilihan Ganda</label>
        <div class="row" id="option-wrapper">
            @if(is_null($data->id))
            <div class="col-md-6 mb-2">
                <input type="text" name="option[]" class="form-control" placeholder="Pilihan">
            </div>
            <div class="col-md-6 mb-2">
                <input type="text" name="option[]" class="form-control" placeholder="Pilihan">
            </div> 
            @else
                @foreach(json_decode($data->option) as $item)
                <div class="col-md-6 mb-2">
                    <input type="text" name="option[]" class="form-control" placeholder="Pilihan" value="{{$item}}">
                </div>
                @endforeach
            @endif 
        </div>

        <div class="row">
            <div class="col-md-6 mb-2">
                <button id="btn-add-option" type="button" class="text-center btn btn-block" style="background:#F7F7F7;">
                    <span class="iconify" data-icon="ant-design:plus-outlined"></span>
                </button>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type="hidden" name="type" value="radio">
</form>
@endif

@if($type == 'file')
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal form-konten' backdrop="">
    <div class="form-group">
        <label class="font-weight-bold" for="exampleInputPassword1">Nama File</label>
        <input type="name" class="form-control" name="question" placeholder="Nama File" value="{{$data->question}}" required>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type="hidden" name="type" value="file">
</form>
@endif

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.template-diagnosis.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah pertanyaan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


    $("#btn-add-option").on('click', function(){
        let html = `<div class="col-md-6 mb-2">
                        <input type="text" name="option[]" class="form-control" placeholder="Pilihan">
                    </div>`;
        $("#option-wrapper").append(html);
    });


</script>

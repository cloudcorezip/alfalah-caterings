<div class="container-fluid">
    <div class="mx-auto">
        <div class="row mb-3">
            <div class="col-md-6">
                <h4 class="font-weight-bold mb-0 color-323232">{{ucwords($merchant->name)}}</h4>
                <span class="color-a4a4a4"><i>{{$merchant->address}}</i></span>
            </div>
            <div class="col-md-6 text-right">
                @if(!is_null($merchant->image))
                <img src="{{env('S3_URL')}}{{$merchant->image}}" alt="logo" class="img-fluid" style="width:30%;">
                @else
                <div class="text-center px-4 py-3" style="background-color: #F0F0F0;width: 40%; margin-left:auto; margin-right:0;">
                    <span>Logo</span>
                </div>
                @endif
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5 class="font-weight-bold mb-3">IDENTITAS MEMBER</h5>
            </div>
            <div class="col-lg-6 col-md-12 py-4">
                <table class="table-note">
                    <tr>
                        <td class="font-weight-bold font-14 color-323232">No. Member</td>
                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                        <td class="font-14 color-a4a4a4">PS-0112120-002</td>
                    </tr>
                    <tr>
                        <td class="font-14 font-weight-bold color-323232">No. Reservasi</td>
                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                        <td class="font-14 color-a4a4a4">R-001</td>
                    </tr>
                    <tr>
                        <td class="font-14 font-weight-bold color-323232">Nama</td>
                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                        <td class="font-14 color-a4a4a4">Armah</td>
                    </tr>
                    <tr>
                        <td class="font-14 font-weight-bold color-323232">Status</td>
                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                        <td class="font-14 color-a4a4a4">Belum Menikah</td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-6 col-md-12 py-4">
                <table class="table-note">
                    <tr>
                        <td class="font-14 font-weight-bold color-323232">Tanggal Lahir</td>
                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                        <td class="font-14 color-a4a4a4">Lebak, 10/20/ 2022</td>
                    </tr>
                    <tr>
                        <td class="font-14 font-weight-bold color-323232">Jenis Kelamin</td>
                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                        <td class="font-14 color-a4a4a4">Laki - Laki</td>
                    </tr>
                    <tr>
                        <td class="font-14 font-weight-bold color-323232">Usia</td>
                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                        <td class="font-14 color-a4a4a4">18 Tahun</td>
                    </tr>
                    <tr>
                        <td class="font-14 font-weight-bold color-323232">Agama</td>
                        <td class="text-center font-14 font-weight-bold color-323232" width="40px">:</td>
                        <td class="font-14 color-a4a4a4">Islam</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h5 class="font-weight-bold color-323232 mb-4">PERTANYAAN DIAGNOSA</h5>
            </div>

            @foreach($data as $key => $item)
                @if($item->type == 'text')
                <div class="col-md-12 position-relative mb-3">
                    <h6 class="font-weight-bold">{{$key + 1}}. {{$item->question}}</h6>
                </div>
                @endif

                @if($item->type == 'radio')
                <div class="col-md-12 mb-3">
                    <h6 class="font-weight-bold">{{$key + 1}}. {{$item->question}}</h6>
                    <div class="row ml-3">
                        @php
                            $num = 'A';
                        @endphp
                        @foreach(json_decode($item->option) as $k => $i)
                            <div class="col-md-3 mb-2">
                                <h6>{{$num}}. {{$i}}</h6>
                            </div>
                            @php
                                $num++;
                            @endphp
                        @endforeach
                    </div>
                </div>
                @endif

                @if($item->type == 'file')
                <div class="col-md-12 mb-3">
                    <h6 class="font-weight-bold">{{$key + 1}}. {{$item->question}}</h6>
                </div>
                @endif
            @endforeach

        </div>

        <div class="row">
            <div class="col-md-6">
                <h6 class="font-weight-bold">Catatan :</h6>
                <div class="p-3" style="border: 1px solid #c3c3c3; border-radius:10px;">
                    <span class="color-a4a4a4">Setelah anda melakukan diagnosis pada pasien anda bisa langsung menginputkan data diagnosa mu </span>
                </div>
            </div>
        </div>
    </div>
    
</div>
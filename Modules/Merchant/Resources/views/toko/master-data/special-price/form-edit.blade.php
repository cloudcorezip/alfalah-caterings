@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Marketing</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.special-price.index')}}">Harga Khusus</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Untuk menambah harga khusus, kamu harus mengisi data di bawah ini</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="font-weight-bold mb-3">Informasi Harga</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label font-weight-bold">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="btn-group btn-group-toggle grab-btn__group mb-3" data-toggle="buttons">
                                    <label
                                        class="btn btn-outline-warning @if($data->is_active == 1) active @endif"
                                    >
                                        <input
                                            type="radio"
                                            name="is_active"
                                            id="option1"
                                            autocomplete="off"
                                            value="1"
                                            @if($data->is_active == 1)
                                            checked
                                            @endif
                                        >
                                        <span>Aktif</span>
                                    </label>
                                    <label
                                        class="btn btn-outline-warning @if($data->is_active == 0) active @endif"
                                    >
                                        <input
                                            type="radio"
                                            name="is_active"
                                            id="option2"
                                            autocomplete="off"
                                            value="0"
                                            @if($data->is_active == 0)
                                            checked
                                            @endif
                                        >
                                        <span>Tidak Aktif</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Judul <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" name="name" value="{{$data->name}}">
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-top">
                        <label class="col-sm-2 col-form-label font-weight-bold">Durasi Harga Khusus <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="start_date">Tanggal Mulai</label>
                                        <input
                                            type="date"
                                            class="form-control form-control-sm"
                                            name="start_date"
                                            placeholder="Tanggal Mulai"
                                            id="start_date"
                                            value="{{\Carbon\Carbon::parse($data->start_date)->format('Y-m-d')}}"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="end_date">Tanggal Berakhir</label>
                                        <input
                                            type="date"
                                            class="form-control form-control-sm"
                                            name="end_date"
                                            placeholder="Tanggal Selesai"
                                            id="end_date"
                                            value="{{\Carbon\Carbon::parse($data->end_date)->format('Y-m-d')}}"
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Apakah berlaku selama 24 jam ?</label>
                                <div class="col-sm-9">
                                    <div class="custom-control custom-switch">
                                        <input
                                            type="checkbox"
                                            class="custom-control-input check-bank"
                                            id="is_daily"
                                            name="is_daily"
                                            value="{{$data->is_daily}}"
                                            style="transform:scale(2)"
                                            @if($data->is_daily == 1)
                                            checked
                                            @endif
                                        >
                                        <label class="custom-control-label" for="is_daily"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="time-wrapper" @if($data->is_daily == 1) style="display:none;" @endif>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="start_clock">Jam Mulai</label>
                                        <input
                                            type="time"
                                            class="form-control form-control-sm"
                                            name="start_hour"
                                            id="start_hour"
                                            value="{{$data->start_hour}}"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="end_clock">Jam Berakhir</label>
                                        <input
                                            type="time"
                                            class="form-control form-control-sm"
                                            name="end_hour"
                                            id="end_hour"
                                            value="{{$data->end_hour}}"
                                        >
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Hari Aktif <span class="text-danger">*</span></label>
                        <div class="col-sm-10" id="result_day">

                        </div>
                    </div>

                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <button
                                class="d-flex justify-content-between align-items-center pl-0"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse-terms"
                                aria-expanded="false"
                                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                            >
                                <h4 class="font-weight-bold mb-3">Ketentuan Harga Khusus</h4>
                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                            </button>
                        </div>
                    </div>


                    <div class="collapse show" id="collapse-terms">
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-sm-12 col-form-label font-weight-bold d-block mb-0">Produk <span class="text-danger">*</span></label>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <small>Harga khusus akan berlaku jika pelanggan membeli produk apa saja?</small>
                            </div>
                            <div class="col-sm-10">
                                <select name="product_details" class="form-control js-example-basic-single" id="product_details">
                                    <option value="-1">--- Pilih Produk ---</option>
                                    @foreach($product as $key => $item)
                                        <option
                                            value="{{$item->id}}"
                                            id="product_detail_{{$item->id}}"
                                            data-name="{{$item->name}}"
                                            data-code="{{$item->code}}"
                                        >
                                            {{$item->name}}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="mt-3" id="selected-product-wrapper">
                                    <table id="selected-product" class="table table-borderless" style="width:100%;">
                                        <tr>
                                            <th style="width:50%;">Nama Produk</th>
                                            <th style="width:40%;">Harga Khusus</th>
                                            <th style="width:10%;" class="text-center">Hapus</th>
                                        </tr>

                                        @foreach($details as $key => $item)
                                            @if($item->md_merchant_id == merchant_id())
                                            <tr id="details-{{$item->sc_product_id}}">
                                                <td>
                                                    <span>{{$item->name}}</span>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center increment-wrapper">
                                                        <input
                                                            type="text"
                                                            class="form-control product-detail-input amount_currency"
                                                            value="{{number_format((float)$item->special_price, 2, '.', '')}}"
                                                            id="price_{{$item->sc_product_id}}"
                                                            data-id="{{$item->sc_product_id}}"
                                                            data-code="{{$item->code}}"
                                                        >
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <button
                                                        type="button"
                                                        name="remove"
                                                        data-id="{{$item->sc_product_id}}"
                                                        data-target="#details-{{$item->sc_product_id}}"
                                                        class="btn btn-xs btn-delete-xs btn-rounded btn_remove">
                                                            <span class='fa fa-trash-alt' style='color: white'></span>
                                                    </button>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(count(get_cabang()) > 1)
                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <button
                                class="d-flex justify-content-between align-items-center pl-0"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse-additional-terms"
                                aria-expanded="false"
                                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                            >
                                <h4 class="font-weight-bold mb-3">Ketentuan Tambahan</h4>
                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                            </button>
                        </div>
                    </div>

                    <div class="collapse" id="collapse-additional-terms">
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>

                            <div class="row mt-4">
                                <div class="col-sm-12">
                                    <h5 class="font-weight-bold mb-3">Cabang</h5>
                                </div>
                            </div>

                            <div class="form-group row d-flex align-items-center">
                                <div class="col-sm-2">
                                    <label class="col-form-label font-weight-bold d-block">Berlakukan untuk Semua Cabang ?</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="custom-control custom-switch">
                                        <input
                                            type="checkbox"
                                            class="custom-control-input check-bank"
                                            id="is_all_branch"
                                            name="is_all_branch"
                                            value="{{$data->is_all_branch}}"
                                            style="transform:scale(2)"
                                            @if($data->is_all_branch == 1)
                                            checked
                                            @endif
                                        >
                                        <label class="custom-control-label" for="is_all_branch"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="branch-wrapper" @if($data->is_all_branch == 1) style="display:none;" @endif>
                                <div class="col-sm-2">
                                    <label class="col-form-label font-weight-bold d-block">Berlaku untuk cabang ?</label>
                                </div>
                                <div class="col-sm-10">
                                    <select name="branch_id" class="form-control" id="branch_id" multiple="multiple">
                                        @foreach(get_cabang() as $key => $item)
                                            @if($item->id != merchant_id())
                                            <option
                                                value="{{$item->id}}"
                                                data-name="{{$item->nama_cabang}}"
                                                @if(in_array($item->id, $assign_to_branch))
                                                selected
                                                @endif
                                            >
                                                {{$item->nama_cabang}}
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12 mb-4 text-right">
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <a href="{{route('merchant.toko.special-price.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                            <button class="btn btn-success" id="btn-save"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $('.amount_currency').mask("#.##0,00", {reverse: true});
    $("#promo_type").select2({
        placeholder: "--- Pilih Tipe Promo ---"
    });
    $("#product_details").select2({
        placeholder: "Masukkan nama produk"
    });
    $("#customer").select2();
    $("#category_customer").select2({
        placeholder: "--- Pilih Kategori Pelanggan ---"
    });
    $("#branch_id").select2();
    $("#bonus_product").select2();

    $("#is_daily").on('change', function(){
        if($(this).prop('checked')==true){
           $(this).val(1);
           $("#time-wrapper").hide();
        } else {
            $(this).val(0);
            $("#time-wrapper").show();
        }
    });

    $("#is_all_branch").on("change", function(){
        if($(this).prop("checked") == true){
            $("#branch-wrapper").hide();
            $(this).val(1);
        } else {
            $("#branch-wrapper").show();
            $(this).val(0);
        }
    });

    let detailProductIdDb = @json($detailProductIdDb);
    let detailId = [];

    detailProductIdDb.forEach(item => {
        detailId.push(item.sc_product_id);
    });

    $(document).on('change','#product_details', function(){
        let productId = $('#product_details option:selected').val();
        let products =($(".js-example-basic-single").select2("data"))?$(".js-example-basic-single").select2("data"):[];
        let productCode='';
        if(products.length>0){
            productCode=products[0]['code']
        }
        if(productId ==  '-1'){
            return;
        }
        detailId.push(productId);

        var duplicateId = detailId.filter(item => {
            return item == productId;
        });

        if(duplicateId.length <= 1){
            let html = `<tr id="details-${productId}">
                            <td>
                                <span>${$('#product_details option:selected').text()}</span>
                            </td>
                            <td>
                                <div class="d-flex align-items-center increment-wrapper">
                                    <input
                                        type="text"
                                        class="form-control product-detail-input amount_currency"
                                        value="0"
                                        id="price_${productId}"
                                        data-id="${productId}"
                                        data-code="${productCode}"
                                    >
                                </div>
                            </td>
                            <td class="text-center">
                                <button
                                    type="button"
                                    name="remove"
                                    data-id="${productId}"
                                    data-target="#details-${productId}"
                                    class="btn btn-xs btn-delete-xs btn-rounded btn_remove">
                                        <span class='fa fa-trash-alt' style='color: white'></span>
                                </button>'
                            </td>
                        </tr>`;
            $("#selected-product").append(html);
            $("#selected-product").find(".amount_currency").mask("#.##0,00", {reverse: true});
        }
    });

    $(document).on('click', '.btn_remove', function(){
        var target = $(this).attr("data-target");
        var targetId = $(this).attr("data-id");
        detailId = detailId.filter(item => {
            return item != parseInt(targetId);
        });
        $(target).remove();
    });

    let startDateRange = new Date('{{$data->start_date}}');
    let endDateRange = new Date('{{$data->end_date}}');

    $("#start_date").on("change", function(){
        startDateRange = new Date($(this).val());
        getRangeDate(startDateRange, endDateRange);
    });

    $("#end_date").on("change", function(){
        endDateRange = new Date($(this).val());
        getRangeDate(startDateRange, endDateRange);
    });

    let checkVal = [];
    function getRangeDate(startDate, endDate)
    {
        checkVal = [];
        $("#result_day").html("");
        if(!startDate, !endDate){
            return;
        }

        let jsonDayDb = @json($data->days);
        let dayDb = JSON.parse(jsonDayDb);

        const date = new Date(startDate.getTime());
        const dayName = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
        let dayRange = [];

        const dates = [];
        while (date <= endDate) {
            dates.push(new Date(date).getDay());
            date.setDate(date.getDate() + 1);
        }

        if(dates.length >= 7){
            dayRange = [...dayName];
        } else {
            dates.forEach((val) => {
                dayRange.push(dayName[val]);
            });
        }

        let html = `<div class="form-check form-check-inline mr-5">
                        <input class="form-check-input" type="checkbox" id="check_all_day" onchange="checkAllDay(this, 1)">
                        <label class="form-check-label border-bottom">Pilih Semua</label>
                    </div>
                    `;

        dayRange.forEach(item => {
            if(dayDb.findIndex(i => i.hari == item) ==  -1){
                html += `<div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="${item}" name="days[]" onchange="checkAllDay(this, 0)">
                            <label class="form-check-label">${item}</label>
                        </div>
                        `;
            } else {
                html += `<div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="${item}" name="days[]" checked onchange="checkAllDay(this, 0)">
                            <label class="form-check-label">${item}</label>
                        </div>
                        `;
                checkVal.push(item);
            }


        });

        if(dayRange.length > 0)
        {
            $("#result_day").append(html);
        }

    }

    getRangeDate(startDateRange, endDateRange);

    const checkAllDay = (e, isAll) => {
        const inputDay = document.querySelectorAll('input[name="days[]"]');
        if(isAll == 1){
            if(e.checked){
                inputDay.forEach(item => {
                    item.checked = true;
                    const index = checkVal.indexOf(item.value);
                    if(index === -1 ){
                        checkVal.push(item.value);
                    }

                });
            } else {
                inputDay.forEach(item => {
                    item.checked = false;
                });
                checkVal = [];
            }
        } else {
            if(e.checked){
                checkVal.push(e.value);
            } else {
                checkVal = checkVal.filter(item => {
                    return item !== e.value;
                });
            }

            if(checkVal.length === inputDay.length){
                document.querySelector('#check_all_day').checked = true;
            } else if(checkVal.length === inputDay.length - 1){
                document.querySelector('#check_all_day').checked = false;
            }
        }
    }

    $(document).ready(function () {
        getProduct()
        $('#form-konten').submit(function () {
            let data = getFormData('form-konten');
            let is_daily = $("#is_daily").val();
            let branch = ($("#branch_id").select2("data"))?$("#branch_id").select2("data"):[];
            let is_all_branch = ($("#is_all_branch").val())?$("#is_all_branch").val():0;
            let productInput = [...document.querySelectorAll('.product-detail-input')];
            let daysForm = data.getAll('days[]').filter(item => {
                return item != "null";
            });

            let day = daysForm.map(item => {
                return {
                    "day_name":item
                }
            });

            let detailProduct = productInput.map(item => {
                return {
                    "sc_product_id":parseInt(item.getAttribute('data-id')),
                    "product_code":item.getAttribute('data-code'),
                    "special_price":item.value
                }
            });

            let assign_to_branch = branch.map(item => {
                return parseInt(item.id);
            });

            data.set('is_daily', is_daily);
            data.set('is_all_branch', is_all_branch);
            data.append('day', JSON.stringify(day));
            data.append("assign_to_branch", JSON.stringify(assign_to_branch));
            data.append("detail_product", JSON.stringify(detailProduct));
            ajaxTransfer("{{route('merchant.toko.special-price.save')}}", data,function (response){
                let data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    });
    function getProduct()
    {
        $(".js-example-basic-single").select2({
            placeholder:"--Pilih Produk--",
            ajax: {
                type: "GET",
                url: "{{route('api.product.all-v2')}}?md_merchant_id={{merchant_id()}}",
                dataType: 'json',
                delay: 250,
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    //    console.log(data)
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }
</script>
@endsection

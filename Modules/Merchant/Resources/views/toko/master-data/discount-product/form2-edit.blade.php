@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Marketing</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.discount-product.index')}}">Promo</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Untuk menambah promosi, kamu harus mengisi data di bawah ini !</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="col-md-12">
            <div class="alert alert-warning text-center">
                <b>Perhatian !</b> Promo berdasarkan produk dan bonus produk menggunakan perhitungan satuan default dari setiap produk.
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-body">

                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="font-weight-bold mb-3">Informasi Promo</h4>
                            <hr>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label font-weight-bold">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="btn-group btn-group-toggle grab-btn__group mb-3" data-toggle="buttons">
                                    <label
                                        class="btn btn-outline-warning {{($data->is_active == 1)? 'active':''}}"
                                    >
                                        <input
                                            type="radio"
                                            name="is_active"
                                            id="option1"
                                            autocomplete="off"
                                            value="1"
                                            @if($data->is_active == 1)
                                            checked
                                            @endif
                                        >
                                        <span>Aktif</span>
                                    </label>
                                    <label
                                        class="btn btn-outline-warning {{($data->is_active == 0)? 'active':''}}"
                                    >
                                        <input
                                            type="radio"
                                            name="is_active"
                                            id="option2"
                                            autocomplete="off"
                                            @if($data->is_active == 0)
                                            checked
                                            @endif
                                            value="0"
                                        >
                                        <span>Tidak Aktif</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Judul <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" name="name" value="{{$data->name}}">
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Kode <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" name="code" value="{{$data->code}}" disabled>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Deskripsi <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea required name="desc" class="form-control form-control-sm">{!! $data->desc !!}</textarea>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-top">
                        <label class="col-sm-2 col-form-label font-weight-bold">Durasi Promo <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="start_date">Tanggal Mulai</label>
                                        <input
                                            type="date"
                                            class="form-control form-control-sm"
                                            name="start_date"
                                            placeholder="Tanggal Mulai"
                                            id="start_date"
                                            value="{{\Carbon\Carbon::parse($data->start_date)->format('Y-m-d')}}"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="end_date">Tanggal Berakhir</label>
                                        <input
                                            type="date"
                                            class="form-control form-control-sm"
                                            name="end_date"
                                            placeholder="Tanggal Selesai"
                                            id="end_date"
                                            value="{{\Carbon\Carbon::parse($data->end_date)->format('Y-m-d')}}"
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Apakah berlaku selama 24 jam ?</label>
                                <div class="col-sm-9">
                                    <div class="custom-control custom-switch">
                                        <input
                                            type="checkbox"
                                            class="custom-control-input check-bank"
                                            id="is_daily"
                                            name="is_daily"
                                            value="{{$data->is_daily}}"
                                            style="transform:scale(2)"
                                            @if($data->is_daily == 1)
                                                checked
                                            @endif
                                        >
                                        <label class="custom-control-label" for="is_daily"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row {{($data->is_daily==1)?'d-none':''}}" id="clock-wrapper">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="start_clock">Jam Mulai</label>
                                        <input
                                            type="time"
                                            class="form-control form-control-sm"
                                            name="start_clock"
                                            id="start_clock"
                                            value="{{is_null($data->start_clock)? '00:00':$data->start_clock}}"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="end_clock">Jam Berakhir</label>
                                        <input
                                            type="time"
                                            class="form-control form-control-sm"
                                            name="end_clock"
                                            id="end_clock"
                                            value="{{is_null($data->end_clock)? '23:59':$data->end_clock}}"
                                        >
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Hari Aktif Promo <span class="text-danger">*</span></label>
                        <div class="col-sm-10" id="result_day">
                            <div class="form-check form-check-inline mr-5">
                                <input class="form-check-input" type="checkbox" id="check_all_day" onchange="checkAllDay(this)">
                                <label class="form-check-label border-bottom">Pilih Semua</label>
                            </div>
                            @php
                                $dayDb = json_decode($data->day);
                            @endphp
                            @foreach($dayDb as $day)
                            <div class="form-check form-check-inline">
                                <input
                                    class="form-check-input"
                                    type="checkbox"
                                    value="{{$day->hari}}"
                                    name="days[]"
                                    checked
                                >
                                <label class="form-check-label">{{$day->hari}}</label>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <h4 class="font-weight-bold mb-3">Ketentuan Promo</h4>
                            <hr>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Tipe Promo <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <select class="form-control" name="promo_type" id="promo_type" disabled>
                                <option></option>
                                @foreach($promoType as $key => $item)
                                <option
                                    value="{{$item['promo_type']}}"
                                    data-bonus-type="{{$item['bonus_type']}}"
                                    @if($data->promo_type == $item['promo_type'] && $data->bonus_type == $item['bonus_type'])
                                        selected
                                    @endif
                                >
                                    {{$item['description']}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div id="promo_value">
                        @if($data->promo_type == 1)
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-sm-2 col-form-label font-weight-bold">Min total transaksi <span class="text-danger">*</span></label>
                            <div class="col-sm-10">
                                <input
                                    type="text"
                                    class="form-control form-control-sm amount_currency"
                                    name="promo_value"
                                    value="{{is_null($data->promo_value)?0:number_format((float)$data->promo_value, 2, '.', '')}}"
                                    disabled
                                >
                            </div>
                        </div>
                        @endif
                        @if($data->promo_type == 2)
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="product-by-wrapper">
                                    <div>
                                        <button
                                            data-id="1"
                                            type="button"
                                            class="btn btn-product-by {{($data->select_product_by == 1)? 'active':''}}"
                                            disabled
                                        >
                                            Produk
                                        </button>
                                    </div>
                                    <div>
                                        <button
                                            data-id="2"
                                            type="button"
                                            class="btn btn-product-by {{($data->select_product_by == 2)? 'active':''}}"
                                            disabled
                                        >
                                            Kategori
                                        </button>
                                    </div>
                                    <div>
                                        <button
                                            data-id="3"
                                            type="button"
                                            class="btn btn-product-by {{($data->select_product_by == 3)? 'active':''}}"
                                            disabled
                                        >
                                            Merk
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-start">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Produk <span class="text-danger">*</span></label>
                                <small>Promo akan berlaku jika pelanggan membeli produk apa saja?</small>
                            </div>
                            <div class="col-sm-10">

                                @if($data->select_product_by == 0)
                                <div class="table-responsive">
                                    <table id="details" class="table table-borderless" style="width:100%;">
                                        <thead style="background-color:#fff;">
                                            <tr>
                                                <th style="width:50%;">Nama Produk</th>
                                                <th style="width:50%;">Kuantitas</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($detailProductPromo as $key => $item)
                                                @if($item->md_merchant_id == merchant_id())
                                                <tr id="details-{{$item->sc_product_id}}">
                                                    <td>
                                                        <span>{{$item->name}}</span>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex align-items-center increment-wrapper">
                                                            <input
                                                                type="number"
                                                                class="form-control text-right product-detail-input"
                                                                value="{{$data->promo_value}}"
                                                                id="quantity_{{$item->sc_product_id}}"
                                                                data-id="{{$item->sc_product_id}}"
                                                                disabled
                                                            >
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endif

                                @if($data->select_product_by == 1)
                                <div class="table-responsive">
                                    <table id="details" class="table table-borderless" style="width:100%;">
                                        <thead style="background-color:#fff;">
                                            <tr>
                                                <th style="width:50%;">Nama Produk</th>
                                                <th style="width:50%;">Kuantitas</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($detailProductPromo as $key => $item)
                                                @if($item->md_merchant_id == merchant_id())
                                                <tr id="details-{{$item->sc_product_id}}">
                                                    <td>
                                                        <span>{{$item->name}}</span>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex align-items-center increment-wrapper">
                                                            <input
                                                                type="number"
                                                                class="form-control text-right product-detail-input"
                                                                value="{{$item->quantity}}"
                                                                id="quantity_{{$item->sc_product_id}}"
                                                                data-id="{{$item->sc_product_id}}"
                                                                disabled
                                                            >
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endif

                                @if($data->select_product_by == 2)
                                <div class="table-responsive">
                                    <table id="details" class="table table-borderless" style="width:100%;">
                                        <thead style="background-color:#fff;">
                                            <tr>
                                                <th style="width:50%;">Nama Kategori</th>
                                                <th style="width:50%;">Kuantitas</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($selectProductByCategory as $key => $item)
                                            <tr id="details-{{$item->id}}">
                                                <td>
                                                    <span>{{$item->name}}</span>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center increment-wrapper">
                                                        <input
                                                            type="number"
                                                            class="form-control text-right product-detail-input"
                                                            value="{{$item->quantity}}"
                                                            id="quantity_{{$item->id}}"
                                                            data-id="{{$item->id}}"
                                                            disabled
                                                        >
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @endif

                                @if($data->select_product_by == 3)
                                <div class="table-responsive">
                                    <table id="details" class="table table-borderless" style="width:100%;">
                                        <thead style="background-color:#fff;">
                                            <tr>
                                                <th style="width:50%;">Nama Merk</th>
                                                <th style="width:50%;">Kuantitas</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($selectProductByMerk as $key => $item)
                                            <tr id="details-{{$item->id}}">
                                                <td>
                                                    <span>{{$item->name}}</span>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center increment-wrapper">
                                                        <input
                                                            type="number"
                                                            class="form-control text-right product-detail-input"
                                                            value="{{$item->quantity}}"
                                                            id="quantity_{{$item->id}}"
                                                            data-id="{{$item->id}}"
                                                            disabled
                                                        >
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                @endif

                            </div>
                        </div>
                        @endif

                    </div>

                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <button
                                class="d-flex justify-content-between align-items-center pl-0"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse-customer"
                                aria-expanded="false"
                                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                            >
                                <h4 class="font-weight-bold mb-3">Ketentuan Tambahan</h4>
                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                            </button>
                        </div>

                    </div>

                   <div class="collapse" id="collapse-customer">
                        <div class="row mt-4">
                            <div class="col-sm-12">
                                <h5 class="font-weight-bold mb-3">Pelanggan</h5>
                            </div>
                        </div>

                        <div class="form-group row d-flex align-items-center">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Berlakukan untuk semua pelanggan ?</label>
                            </div>
                            <div class="col-sm-10">
                                <div class="custom-control custom-switch">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input"
                                        id="all_customer"
                                        name="all_customer"
                                        value="{{$data->is_all_customer}}"
                                        style="transform:scale(2)"
                                        @if($data->is_all_customer == 1)
                                        checked
                                        @endif
                                    >
                                    <label class="custom-control-label" for="all_customer"></label>
                                </div>
                            </div>
                        </div>

                        <div id="customer_criteria_wrapper" @if($data->is_all_customer == 1) style='display:none;' @endif>
                            <div id="customer_name_wrapper" style="background-color:#FBFBFB;border:1px dashed #C4C4C4;border-radius:10px;" class="p-4 mb-4">
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-sm-2">
                                        <label class="col-form-label font-weight-bold d-block">Pilih dari daftar pelanggan</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="custom-control custom-switch">
                                            <input
                                                type="checkbox"
                                                class="custom-control-input"
                                                id="is_specific_customer"
                                                name="is_specific_customer"
                                                value="{{$data->is_specific_customer}}"
                                                style="transform:scale(2)"
                                                @if($data->is_specific_customer == 1)
                                                checked
                                                @endif
                                            >
                                            <label class="custom-control-label" for="is_specific_customer"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" id="customer_keyword" @if($data->is_specific_customer == 1) style="display:none;" @endif>
                                    <div class="col-sm-2">
                                        <label class="col-form-label font-weight-bold d-block">Promo akan berlaku pada pelanggan yang memiliki nama ?</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="customer_name"
                                            id="customer_name"
                                            data-role="tagsinput"
                                            value="{{$custNameVal}}"
                                        >
                                    </div>
                                </div>

                                <div class="form-group row" id="customer_list" @if($data->is_specific_customer == 0) style="display:none;" @endif>
                                    <div class="col-sm-2">
                                        <label class="col-form-label font-weight-bold d-block">Pilih Pelanggan</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="sc_customer_id" id="sc_customer_id" multiple>
                                            @foreach($customers as $key => $item)
                                                <option
                                                    value="{{$item->id}}"
                                                    selected
                                                >
                                                    {{$item->nama_pelanggan}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="customer_birth_date_wrapper" style="background-color:#FBFBFB;border:1px dashed #C4C4C4;border-radius:10px; @if($data->is_specific_customer == 1) display:none; @endif" class="p-4">
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-sm-2">
                                        <label class="col-form-label font-weight-bold d-block">Promo akan berlaku pada pelanggan yang sedang berulang tahun tanggal?</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input
                                            type="date"
                                            class="form-control form-control-sm"
                                            name="customer_birth_date"
                                            placeholder="Tanggal Lahir Pelanggan"
                                            @if($customerTerms->birth_date != "-1")

                                                @if(isset($customerTerms->birth_year))
                                                value="{{\Carbon\Carbon::parse($customerTerms->birth_year.'-'.$customerTerms->birth_date)->format('Y-m-d')}}"
                                                @else
                                                value="{{\Carbon\Carbon::parse('1990-'.$customerTerms->birth_date)->format('Y-m-d')}}"
                                                @endif
                                            @endif
                                            id="customer_birth_date"
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="row">
                            <div class="col-sm-12">
                                <hr>
                            </div>
                        </div>

                        @if(count(get_cabang()) > 1)
                            <div class="row mt-4">
                                <div class="col-sm-12">
                                    <h5 class="font-weight-bold mb-3">Cabang</h5>
                                </div>
                            </div>

                            <div class="form-group row d-flex align-items-start">
                                <div class="col-sm-2">
                                    <label class="col-form-label font-weight-bold d-block">Berlakukan untuk Semua Cabang ?</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="custom-control custom-switch">
                                        <input
                                            type="checkbox"
                                            class="custom-control-input check-bank"
                                            id="all_branch"
                                            name="all_branch"
                                            value="{{$data->is_all_branch}}"
                                            style="transform:scale(2)"
                                            @if($data->is_all_branch == 1)
                                            checked
                                            @endif
                                            disabled
                                        >
                                        <label class="custom-control-label" for="all_branch"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" @if($data->is_all_branch == 1) style="display:none;" @endif>
                                <div class="col-sm-2">
                                    <label class="col-form-label font-weight-bold d-block">Berlaku untuk cabang ?</label>
                                </div>
                                <div class="col-sm-10">
                                    <select disabled name="branch_id" class="form-control js-example-basic-single" id="branch_id" multiple="multiple">
                                        @foreach(get_cabang() as $key => $item)
                                            @if($item->id != merchant_id())
                                            <option
                                                value="{{$item->id}}"
                                                data-name="{{$item->nama_cabang}}"
                                                @if(in_array($item->id, $assignToBranch))
                                                selected
                                                @endif
                                            >
                                                {{$item->nama_cabang}}
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                   </div>

                <div id="bonus_value">
                    @if($data->bonus_type != 3)
                    <div id="bonus_type_price">
                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <h4 class="font-weight-bold mb-3">Diskon Harga</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Harga <span class="text-danger">*</span></label>
                                <small>Berapa diskon yang didapatkan pelanggan?</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <div class="btn-group btn-group-toggle grab-btn__group mb-3" data-toggle="buttons">
                                                @if($data->bonus_type == 1)
                                                <label
                                                    class="btn btn-outline-warning active"
                                                    style="color:white!important;border-radius:9px!important;"
                                                    id="typePercent-input"
                                                >
                                                    <input
                                                        type="radio"
                                                        name="bonus_type"
                                                        id="typePercent"
                                                        autocomplete="off"
                                                        value="1"
                                                        checked
                                                    >
                                                    Persen (%)
                                                </label>
                                                @endif
                                                @if($data->bonus_type == 2)
                                                <label
                                                    class="btn btn-outline-warning active"
                                                    style="color:white!important;border-radius:9px!important;"
                                                    id="typeRp-input"

                                                >
                                                    <input
                                                        type="radio"
                                                        name="bonus_type"
                                                        id="typeRp"
                                                        autocomplete="off"
                                                        value="2"
                                                        checked
                                                    >
                                                    Nominal (Rp)
                                                </label>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        @php
                                            $bonusVal = json_decode($data->bonus_value)
                                        @endphp

                                        @if($data->bonus_type == 1)
                                        <input disabled type="number" id="bonus_val_percent" class="form-control form-control-sm" name="bonus_val[]" value="{{$bonusVal[0]->value}}" placeholder="Nilai Potongan (%)">
                                        @endif

                                        @if($data->bonus_type == 2)
                                        <input
                                            disabled
                                            type="text"
                                            id="bonus_val_rp"
                                            class="form-control form-control-sm amount_currency"
                                            name="bonus_val[]"
                                            value="{{is_null($bonusVal[0]->value)?0:number_format((float)$bonusVal[0]->value, 2, '.', '')}}"
                                            placeholder="Nilai Potongan (Rp)"
                                        >
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>

                        @if($data->bonus_type == 1)
                        <div class="form-group row d-flex align-items-center" id="max_promo_value">
                            <label class="col-sm-2 col-form-label font-weight-bold">Maksimal Jumlah Diskon <span class="text-danger">*</span></label>
                            <div class="col-sm-10">
                                <input
                                    disabled
                                    type="text"
                                    class="form-control form-control-sm amount_currency"
                                    name="max_promo_value"
                                    value="{{is_null($data->max_promo_value)?0:number_format((float)$data->max_promo_value, 2, '.', '')}}"
                                    required
                                >
                            </div>
                        </div>
                        @endif

                        @if($data->bonus_type == 2)
                        <div class="form-group row d-flex align-items-center" id="is_apply_multiply">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Berlaku Kelipatan ?</label>
                            </div>
                            <div class="col-sm-10">
                                <div class="custom-control custom-switch">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input check-bank"
                                        id="is_applies_multiply"
                                        name="is_applies_multiply"
                                        value="1"
                                        style="transform:scale(2)"
                                        @if($data->is_applies_multiply == 1)
                                        checked
                                        @endif
                                        disabled
                                    >
                                    <label class="custom-control-label" for="is_applies_multiply"></label>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    @endif

                    @if($data->bonus_type == 3)
                    <div id="bonus_type_product">
                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <h4 class="font-weight-bold mb-3">Bonus Produk</h4>
                                <hr>
                            </div>
                        </div>

                        <div class="form-group row d-flex align-items-center">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Produk <span class="text-danger">*</span></label>
                                <small>Apa saja bonus produk yang akan didapatkan pelanggan ?</small>
                            </div>
                            <div class="col-sm-10">
                                <table id="bonus_value_product_dynamic" class="table table-borderless" style="width:100%;">
                                    <tr>
                                        <th style="width:50%;">Nama Produk</th>
                                        <th style="width:50%;">Kuantitas</th>
                                    </tr>
                                    @foreach($bonusProduct as $key => $item)
                                        @if($item->md_merchant_id == merchant_id())
                                        <tr id="details-2-{{$item->value}}">
                                            <td>
                                                <input type="hidden" name="bonus_val[]" value="{{$item->value}}">
                                                <span>{{$item->name}}</span>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center increment-wrapper">
                                                    <input
                                                        type="number"
                                                        name="bonus_val_qty[]"
                                                        value="{{$item->quantity}}"
                                                        class="form-control text-right bonus-product-input"
                                                        required
                                                        id="bonus_qty_{{$item->value}}"
                                                        data-id="{{$item->value}}"
                                                        disabled
                                                    >
                                                </div>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>

                        <div class="form-group row d-flex align-items-center" id="is_apply_multiply">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Berlaku Kelipatan ?</label>
                            </div>
                            <div class="col-sm-10">
                                <div class="custom-control custom-switch">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input check-bank"
                                        id="is_applies_multiply"
                                        name="is_applies_multiply"
                                        value="1"
                                        style="transform:scale(2)"
                                        @if($data->is_applies_multiply == 1)
                                        checked
                                        @endif
                                        disabled
                                    >
                                    <label class="custom-control-label" for="is_applies_multiply"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-md-12 mb-4 text-right">
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <input type="hidden" id="is_type" name="is_type" value="{{$data->is_type}}">
                        <input type="hidden" id="bonus_type" name="bonus_type" value="{{$data->bonus_type}}">
                        <input type="hidden" name="select_product_by" value="{{$data->select_product_by}}">
                        <input type="hidden" name="promo_value" value="{{$data->promo_value}}">
                        <a href="{{route('merchant.toko.discount-product.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                        <button class="btn btn-success" id="btn-save"><i class="fa fa-save mr-2"></i> Simpan Promo</button>
                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script>
    $('.amount_currency').mask("#.##0,00", {reverse: true});
    $("#promo_type").select2({
        placeholder: "--- Pilih Tipe Promo ---"
    });
    $("#product_details").select2({
        placeholder: "Masukkan nama produk"
    });
    $("#customer").select2();
    $("#category_customer").select2({
        placeholder: "--- Pilih Kategori Pelanggan ---"
    });
    $("#branch_id").select2();
    $("#bonus_product").select2();
    // bonus tipe 1 %
    // bonus tipe 2 rp
    // bonus tipe 3 bonus produk

    $("#all_customer").on('change', function(){
        if($(this).prop('checked')==true){
           $(this).val(1);
           $("#customer_criteria_wrapper").hide();
        } else {
            $(this).val(0);
            $("#customer_criteria_wrapper").show();
        }
    });

    $("#is_daily").on('change', function(){
        if($(this).prop('checked')==true){
           $(this).val(1);
           $("#clock-wrapper").addClass('d-none');
        } else {
            $(this).val(0);
            $("#clock-wrapper").removeClass('d-none');
        }
    });

    $("#is_specific_customer").on('change', function(){
        if($(this).prop('checked') == true)
        {
            $(this).val(1);
            $("#customer_keyword").hide();
            $("#customer_list").show();
            $("#customer_birth_date_wrapper").hide();

        } else {
            $(this).val(0);
            $("#customer_keyword").show();
            $("#customer_list").hide();
            $("#customer_birth_date_wrapper").show();
        }
    });

    let startDateRange = new Date('{{$data->start_date}}');
    let endDateRange = new Date('{{$data->end_date}}');

    $("#start_date").on("change", function(){
        startDateRange = new Date($(this).val());
        getRangeDate(startDateRange, endDateRange);
    });

    $("#end_date").on("change", function(){
        endDateRange = new Date($(this).val());
        getRangeDate(startDateRange, endDateRange);
    });

    let checkVal = [];
    function getRangeDate(startDate, endDate)
    {
        checkVal = [];
        $("#result_day").html("");
        if(!startDate, !endDate){
            return;
        }

        let jsonDayDb = @json($data->day);
        let dayDb = JSON.parse(jsonDayDb);

        const date = new Date(startDate.getTime());
        const dayName = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
        let dayRange = [];

        const dates = [];
        while (date <= endDate) {
            dates.push(new Date(date).getDay());
            date.setDate(date.getDate() + 1);
        }

        if(dates.length >= 7){
            dayRange = [...dayName];
        } else {
            dates.forEach((val) => {
                dayRange.push(dayName[val]);
            });
        }

        let html = `<div class="form-check form-check-inline mr-5">
                        <input class="form-check-input" type="checkbox" id="check_all_day" onchange="checkAllDay(this, 1)">
                        <label class="form-check-label border-bottom">Pilih Semua</label>
                    </div>
                    `;

        dayRange.forEach(item => {
            if(dayDb.findIndex(i => i.hari == item) ==  -1){
                html += `<div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="${item}" name="days[]" onchange="checkAllDay(this, 0)">
                            <label class="form-check-label">${item}</label>
                        </div>
                        `;
            } else {
                html += `<div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="${item}" name="days[]" checked onchange="checkAllDay(this, 0)">
                            <label class="form-check-label">${item}</label>
                        </div>
                        `;
                checkVal.push(item);
            }


        });

        if(dayRange.length > 0)
        {
            $("#result_day").append(html);
        }

    }

    getRangeDate(startDateRange, endDateRange);

    const checkAllDay = (e, isAll) => {
        const inputDay = document.querySelectorAll('input[name="days[]"]');
        if(isAll == 1){
            if(e.checked){
                inputDay.forEach(item => {
                    item.checked = true;
                    const index = checkVal.indexOf(item.value);
                    if(index === -1 ){
                        checkVal.push(item.value);
                    }

                });
            } else {
                inputDay.forEach(item => {
                    item.checked = false;
                });
                checkVal = [];
            }
        } else {
            if(e.checked){
                checkVal.push(e.value);
            } else {
                checkVal = checkVal.filter(item => {
                    return item !== e.value;
                });
            }

            if(checkVal.length === inputDay.length){
                document.querySelector('#check_all_day').checked = true;
            } else if(checkVal.length === inputDay.length - 1){
                document.querySelector('#check_all_day').checked = false;
            }
        }
    }

    $(document).ready(function () {
        getCustomer()
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            var days = data.getAll('days[]').filter(item => {
                return item != "null";
            });
            var day = days.map(item => {
                return {
                    "day_name":item
                }
            });

            var promoType = data.get('promo_type');
            var bonusType = data.get('bonus_type');
            var promoProductDetailElement = [...document.querySelectorAll('.product-detail-input')];
            var bonusVal = (bonusType == 3)? [...document.querySelectorAll('.bonus-product-input')] : data.getAll('bonus_val[]');
            var bonusValQty = data.getAll('bonus_val_qty[]');
            var details = [];

            if(promoType == 2){
                promoProductDetailElement.forEach(item => {
                    details.push({
                        "id":parseInt(item.getAttribute('data-id')),
                        "quantity": parseInt(item.value)
                    })
                });
            }

            if(bonusType == 3){
                var bonus_value = bonusVal.map(item => {
                    return {
                        "value":parseInt(item.getAttribute('data-id')),
                        "quantity":parseInt(item.value)
                    }
                });

            } else {
                var bonus_value = bonusVal.map(item => {
                    return {
                        "value":parseInt(item)
                    }
                });
            }

           var applyMultiply = data.get('is_applies_multiply');
           var is_daily = data.get('is_daily');
           var is_not_know_end = data.get('is_not_know_end');
           var all_branch = data.get('all_branch');

           if(applyMultiply === "null" || applyMultiply === null){
               data.set('is_applies_multiply', 0);
           }

           if(is_daily === "null" || is_daily === null){
               data.set('is_daily', 0);
           }

           if(is_not_know_end === "null" || is_not_know_end === null){
               data.set('is_not_know_end', 0);
           }

           if(all_branch === "null" || all_branch === null){
               data.set('all_branch', 0);
           }

           var is_all_customer = ($("#all_customer").val() == 1) ? 1:0;
           var is_specific_customer = ($("#is_specific_customer").val() == 1)? 1:0;

           var customer_ids = ($("#sc_customer_id").select2('data'))?$("#sc_customer_id").select2('data'):[];
           var specific_customer_id = [...customer_ids].map(item => {
                return {
                    "sc_customer_id":parseInt(item.id)
                }
           });

           var customer_name_val = ($("#customer_name").val())? $("#customer_name").val().split(","):[];

           var customer_name = [...customer_name_val].map(item => {
                return {
                    "name":item
                }
           });

           var reqCustomerTerms = {
               "birth_date": ($("#all_customer").val() == 1)? "-1":($("#customer_birth_date").val())? moment($("#customer_birth_date").val()).format('MM-DD'):"-1",
               "birth_year":($("#all_customer").val() == 1)? "-1":($("#customer_birth_date").val())? moment($("#customer_birth_date").val()).format('YYYY'):"-1",
               "customer_name": ($("#all_customer").val() == 1)? []:customer_name,
               "specific_customer_id": ($("#all_customer").val() == 1)? []:specific_customer_id
           }

           var branch = ($("#branch_id").select2("data"))? $("#branch_id").select2("data"):[];

           var assign_to_branch = branch.map(item => {
               return item.id
           });

            data.append('day', JSON.stringify(day));
            data.append('bonus_value', JSON.stringify(bonus_value));
            data.append('details', JSON.stringify(details));
            data.append('activation_type', 1);
            data.append("req_customer_terms", JSON.stringify(reqCustomerTerms));
            data.append('assign_to_branch', JSON.stringify(assign_to_branch));
            data.append('is_all_customer', is_all_customer);
            data.set('is_specific_customer', is_specific_customer);

            ajaxTransfer("{{route('merchant.toko.discount-product.save')}}", data,function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    })

    function getCustomer()
    {
        $("#sc_customer_id").select2({
            ajax: {
                type: "GET",
                url: "{{route('merchant.toko.transaction.sale-order.customer-list')}}",
                dataType: 'json',
                delay: 250,
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }

</script>
@endsection

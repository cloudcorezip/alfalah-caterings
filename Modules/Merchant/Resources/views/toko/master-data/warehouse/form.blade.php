<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label class="font-weight-bold" for="exampleInputPassword1">Cabang</label>
        <select name="md_merchant_id" id="branch" class="form-control" required>
            @if($data->is_to_all==1)
                <option value="-1" selected>Untuk Semua Cabang</option>
            @else
                <option value="-1">Untuk Semua Cabang</option>
            @endif
            @foreach(get_cabang() as $key  =>$item)
                <option value="{{$item->id}}" @if($item->id==$data->md_merchant_id) selected @endif>{{$item->nama_cabang}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label class="font-weight-bold" for="exampleInputPassword1">Nama Gudang</label>
        <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama Gudang" value="{{$data->name}}" required>
    </div>

    <div class="form-group">
        <label class="font-weight-bold" for="exampleInputPassword1">Tipe Gudang</label>
        <select name="type_id" id="type" class="form-control" required>
            @foreach($typeOption as $key  =>$item)
                <option value="{{$item->id}}" @if($item->id==$data->type_id) selected @endif>{{$item->tipe_gudang}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label class="font-weight-bold" for="exampleInputPassword1">Deskripsi</label>
        <textarea name="desc" class="form-control" placeholder="Deskripsi">{{$data->desc}}</textarea>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $("#branch").select2()
        $("#type").select2()

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.warehouse.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    })


</script>

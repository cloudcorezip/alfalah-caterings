@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Marketing</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.discount-product.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Daftar loyalti point dan pengaturannya untuk meningkatkan penjualanmu</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row" style="margin-bottom: 10px">
                    {!! $add !!}
                </div>
                <div id="output-discount-product">
                    @include('merchant::toko.master-data.loyalty.list')
                </div>
            </div>
        </div>
        <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
            <div class="d-flex align-items-center">
                <i class="fas fa-filter mr-2 text-white"></i>
                <span class="text-white form-filter3-text-header">Filter</span>
            </div>
        </a>
        <div class="form-filter3" id="form-filter3">
            <div class="row">
                <div class="col-12">
                    <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center">
                            <i class="fas fa-filter mr-2"></i>
                            <span class="text-white form-filter3-text-header">Filter</span>
                        </div>
                        <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                            <i style="font-size:14px;" class="fas fa-times text-white"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row mb-3 px-30">
                <div class="col-12">
                    <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                    <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
                </div>
            </div>
            <hr>
            <form id="form-filter-discount-product" class="px-30" onsubmit="return false">
                <div class="row d-flex align-items-center">
                    <div class="col-md-12 mb-3">
                        <div id="reportrange" class="form-control form-control-sm" name="date-range" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type="hidden" name="search_key" value="{{$searchKey}}">
                    </div>
                </div>

            </form>

        </div>
    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')

    <script type="text/javascript">
        $(function() {

            var start = moment().startOf('month');
            var end = moment().endOf('month');

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                    '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                    '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                    'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
                    //'All Time': 'all-time',
                }
            }, cb);
            cb(start, end);

        });
    </script>
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.loyalty.delete')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        }
        $(document).ready(function() {
            $("#status").select2();
            $('#form-filter-discount-product').submit(function () {
                var startDate = $('#reportrange').data('daterangepicker').startDate._d;
                var endDate = $('#reportrange').data('daterangepicker').endDate._d;
                var data = getFormData('form-filter-discount-product');
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                ajaxTransfer("{{route('merchant.toko.discount-coupon.reload-data')}}", data, '#output-discount-product');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        })
    </script>
@endsection

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">

        <div class="col-lg-6 col-md-12">
            <div class="form-group">
                <label class="font-weight-bold">Cara mendapatkan point <span class="text-danger">*</span></label>
                <select class="form-control" name="get_point_method" id="get_point_method">
                    <option
                        value="1"
                        @if($data->get_point_method == 1)
                        selected
                        @endif
                    >
                        Minimal total transaksi
                    </option>
                    <option
                        value="2"
                        @if($data->get_point_method == 2)
                        selected
                        @endif
                    >
                        Pembelian Produk
                    </option>
                </select>
            </div>
        </div>

        <div class="col-lg-6 col-md-12">
            <div
                class="form-group"
                id="min-transaction-wrapper"
                @if($data->get_point_method != 1)
                style="display:none;"
                @endif
            >
                <label class="font-weight-bold">Min. Total Transaksi <span class="text-danger">*</span></label>
                <input
                    type="text"
                    name="min_transaction"
                    class="form-control amount_currency"
                    value="{{(!is_null($data->min_transaction))?number_format((float)$data->min_transaction, 2, '.', ''):0}}">
            </div>

            <div
                id="option-product-wrapper"
                class="form-group"
                @if($data->get_point_method != 2)
                style="display:none;"
                @endif
            >
                <label class="font-weight-bold">Pilih Produk <span class="text-danger">*</span></label>
                <select class="form-control" name="sc_product_id" id="sc_product_id">
                    @if(!is_null($data->sc_product_id))
                    <option value="{{$data->sc_product_id}}" selected>{{$data->getProduct->name}}</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="col-lg-6 col-md-12">
            <div class="form-group">
                <label class="font-weight-bold">Point yang diperoleh <span class="text-danger">*</span></label>
                <input type="number" class="form-control" name="point_earned" value="{{$data->point_earned}}">
            </div>
        </div>

        <div class="col-lg-6 col-md-12">
            <div class="form-group">
                <label class="font-weight-bold">Berlaku kelipatan?</label>
                <div class="custom-control custom-switch mb-3">
                    <input
                        type="checkbox"
                        class="custom-control-input"
                        id="is_apply_multiple"
                        name="is_apply_multiple"
                        style="transform:scale(2)"
                        value="{{$data->is_apply_multiple}}"
                        @if($data->is_apply_multiple == 1)
                        checked
                        @endif
                    >
                    <label class="custom-control-label" for="is_apply_multiple"></label>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-12">
            <div class="form-group d-flex justify-content-between align-items-center">
                <label class="font-weight-bold">Berlaku untuk semua tipe pelanggan?</label>
                <div class="custom-control custom-switch mb-3">
                    <input
                        type="checkbox"
                        class="custom-control-input"
                        id="is_all_category"
                        name="is_all_category"
                        style="transform:scale(2)"
                        value="{{$customerCriteria->is_all_category}}"
                        @if($customerCriteria->is_all_category == 1)
                        checked
                        @endif
                    >
                    <label class="custom-control-label" for="is_all_category"></label>
                </div>
            </div>

            <div
                class="form-group"
                id="customer-category-wrapper"
                @if($customerCriteria->is_all_category == 1)
                style="display:none;"
                @endif
            >
                <select multiple class="form-control" name="customer_category" id="customer_category">
                    @foreach($categoryCustomer as $key => $item)
                    <option
                        value="{{$item->id}}"
                        @if(in_array($item->id, $customerCriteria->category_id))
                        selected
                        @endif
                    >
                        {{$item->nama_kategori}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        @if(count(get_cabang()) > 1)
        <div class="col-lg-6 col-md-12">
            <div class="form-group d-flex justify-content-between align-items-center">
                <label class="font-weight-bold">Berlaku untuk semua cabang?</label>
                <div class="custom-control custom-switch mb-3">
                    <input
                        type="checkbox"
                        class="custom-control-input"
                        id="is_all_branch"
                        name="is_all_branch"
                        style="transform:scale(2)"
                        value="{{$data->is_all_branch}}"
                        @if($data->is_all_branch == 1)
                        checked
                        @endif
                    >
                    <label class="custom-control-label" for="is_all_branch"></label>
                </div>
            </div>
            <div
                class="form-group"
                id="branch-wrapper"
                @if($data->is_all_branch == 1)
                style="display:none;"
                @endif
            >
                <select multiple class="form-control" name="branch" id="branch">
                    @foreach(get_cabang() as $key => $item)
                        @if($item->id != merchant_id())
                        <option
                            value="{{$item->id}}"
                            @if(in_array($item->id, $branch))
                            selected
                            @endif
                        >
                            {{$item->nama_cabang}}
                        </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        @endif

        <div class="col-lg-6 col-md-12">
            <div class="form-group">
                <label class="col-form-label font-weight-bold mb-3">Status <span class="text-danger">*</span></label>
                <div class="btn-group-toggle input-radio-group mb-3" data-toggle="buttons">
                    <label
                        class="btn btn-input-radio d-block @if($data->is_active == 1) active @endif">
                        <input
                            type="radio"
                            name="is_active"
                            id="is_active_true"
                            autocomplete="off"
                            value="1"
                            class="is_active"
                            @if($data->is_active == 1)
                            checked
                            @endif
                        >
                        <span class="text-center">Aktif</span>
                    </label>
                    <label
                        class="btn btn-input-radio d-block @if($data->is_active == 0) active @endif">
                        <input
                            type="radio"
                            name="is_active"
                            id="is_active_false"
                            value="0"
                            class="is_active"
                            @if($data->is_active == 0)
                            checked
                            @endif
                        >
                        <span class="text-center">Tidak Aktif</span>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $("#get_point_method").select2({
        placeholder: "--- Pilih cara mendapatkan point ---"
    });
    $("#customer_category").select2();
    $("#branch").select2();

    $('.amount_currency').mask("#.##0,00", {reverse: true});

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })
    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Tentukan bagaimana pelanggan akan mendapatkan poinnya</span>
                                    </div>`);
    });




    $("#get_point_method").on("change", function(){
        let val = $(this).val();
        if(val == 1){
            $("#min-transaction-wrapper").show();
            $("#option-product-wrapper").hide();
        } else {
            $("#min-transaction-wrapper").hide();
            $("#option-product-wrapper").show();
        }
    });


    $("#is_all_category").on("change", function(){
        if($(this).prop("checked") == true){
            $("#customer-category-wrapper").hide();
            $(this).val(1);
            $('#customer_category').val(null).trigger('change');
        } else {
            $("#customer-category-wrapper").show();
            $(this).val(0);
        }
    });

    $("#is_all_branch").on("change", function(){
        if($(this).prop("checked") == true){
            $("#branch-wrapper").hide();
            $(this).val(1);
        } else {
            $("#branch-wrapper").show();
            $(this).val(0);
        }
    });


    $(document).ready(function () {
        getProduct()
        $('#form-konten').submit(function () {
            let data = getFormData('form-konten');

            let selectedCustomerCategory = $("#customer_category").select2("data");
            let is_all_branch = ($("#is_all_branch").val())?$("#is_all_branch").val():0;
            let is_apply_multiple = $("#is_apply_multiple").val();

            let branch = ($("#branch").select2("data"))?$("#branch").select2("data"):[];

            let customerCriteria = {
                "is_all_category": $("#is_all_category").val(),
                "category_id": [...selectedCustomerCategory].map(item => {
                    return item.id
                })
            }

            let assign_to_branch = branch.map(item => {
                return parseInt(item.id);
            });

            data.set("is_all_branch", is_all_branch);
            data.set("is_apply_multiple", is_apply_multiple);
            data.append("customer_criteria", JSON.stringify(customerCriteria));
            data.append("assign_to_branch", JSON.stringify(assign_to_branch));

            ajaxTransfer("{{route('merchant.toko.loyalty.save')}}", data,function (response){
                let data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    });

    function getProduct()
    {
        $("#sc_product_id").select2({
            placeholder:"--Pilih Produk--",
            ajax: {
                type: "GET",
                url: "{{route('api.product.all-v2')}}?md_merchant_id={{merchant_id()}}",
                dataType: 'json',
                delay: 250,
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    //    console.log(data)
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }

</script>

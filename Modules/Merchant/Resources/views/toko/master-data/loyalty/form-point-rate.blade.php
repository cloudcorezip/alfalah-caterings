<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="font-weight-bold">1 poin sebanding dengan <span class="text-danger">*</span></label>
                <input 
                    type="text" 
                    name="value_per_point" 
                    class="form-control amount_currency" 
                    value="{{(!is_null($data->value_per_point))?number_format((float)$data->value_per_point, 2, '.', ''):0}}"
                >
            </div>
        </div>
      
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $('.amount_currency').mask("#.##0,00", {reverse: true});
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
        $('.modal-header').find('.modal-title').remove();
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Kamu bisa mengatur nilai loyalty poinmu disini</span>
                                    </div>`);
    });

    $(document).ready(function () {
        $('#form-konten').submit(function () {
            let data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.loyalty.save-point-rate')}}", data,function (response){
                let data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    });

</script>

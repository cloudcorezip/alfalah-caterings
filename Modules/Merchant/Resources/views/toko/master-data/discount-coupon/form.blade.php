@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    @php
        $days = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"];
    @endphp
    <link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Marketing</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.discount-coupon.index')}}">Kupon</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Untuk menambah kupon, kamu harus mengisi data di bawah ini !</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="col-md-12">
            <div class="alert alert-warning text-center">
                <b>Perhatian !</b> Kupon berdasarkan produk dan bonus produk menggunakan perhitungan satuan default dari setiap produk.
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-body">

                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="font-weight-bold mb-3">Informasi Kupon</h4>
                            <hr>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label font-weight-bold">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="btn-group btn-group-toggle grab-btn__group mb-3" data-toggle="buttons">
                                    <label
                                        class="btn btn-outline-warning active"
                                    >
                                        <input
                                            type="radio"
                                            name="is_active"
                                            id="option1"
                                            autocomplete="off"
                                            value="1"
                                            checked
                                        >
                                        <span>Aktif</span>
                                    </label>
                                    <label
                                        class="btn btn-outline-warning"
                                    >
                                        <input
                                            type="radio"
                                            name="is_active"
                                            id="option2"
                                            autocomplete="off"
                                            value="0"
                                        >
                                        <span>Tidak Aktif</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Judul <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" name="name" value="{{$data->name}}">
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Kode <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" name="code" value="{{$data->code}}">
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Deskripsi <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <textarea name="desc" class="form-control form-control-sm">{!! $data->description !!}</textarea>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-top">
                        <label class="col-sm-2 col-form-label font-weight-bold">Durasi Kupon <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            {{---<div class="form-group row">
                                <label class="col-sm-3 col-form-label">Belum tahu kapan berakhirnya</label>
                                <div class="col-sm-9">
                                    <div class="custom-control custom-switch">
                                        <input
                                            type="checkbox"
                                            class="custom-control-input check-bank"
                                            id="is_not_know_end"
                                            name="is_not_know_end"
                                            style="transform:scale(2)"
                                            value="0"
                                        >
                                        <label class="custom-control-label" for="is_not_know_end"></label>
                                    </div>
                                </div>
                            </div>---}}
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="start_date">Tanggal Mulai</label>
                                        <input
                                            type="date"
                                            class="form-control form-control-sm"
                                            name="start_date"
                                            placeholder="Tanggal Mulai"
                                            id="start_date"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="end_date">Tanggal Berakhir</label>
                                        <input
                                            type="date"
                                            class="form-control form-control-sm"
                                            name="end_date"
                                            placeholder="Tanggal Selesai"
                                            id="end_date"
                                        >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Apakah berlaku selama 24 jam ?</label>
                                <div class="col-sm-9">
                                    <div class="custom-control custom-switch">
                                        <input
                                            type="checkbox"
                                            class="custom-control-input check-bank"
                                            id="is_daily"
                                            name="is_daily"
                                            value="0"
                                            style="transform:scale(2)"
                                        >
                                        <label class="custom-control-label" for="is_daily"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="clock-wrapper">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="start_clock">Jam Mulai</label>
                                        <input
                                            type="time"
                                            class="form-control form-control-sm"
                                            name="start_clock"
                                            id="start_clock"
                                        >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="end_clock">Jam Berakhir</label>
                                        <input
                                            type="time"
                                            class="form-control form-control-sm"
                                            name="end_clock"
                                            id="end_clock"
                                        >
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Hari Aktif Kupon <span class="text-danger">*</span></label>
                        <div class="col-sm-10" id="result_day">

                        </div>
                    </div>

                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <h4 class="font-weight-bold mb-3">Ketentuan Kupon</h4>
                            <hr>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Tipe Kupon <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <select class="form-control" name="promo_type" id="promo_type">
                                <option></option>
                                <option value="1" data-bonus-type="1">Diskon harga (%), jika memenuhi total transaksi</option>
                                <option value="1" data-bonus-type="2">Diskon harga (Rp), jika memenuhi total transaksi</option>
                                <option value="1" data-bonus-type="3">Bonus Produk, jika memenuhi total transaksi</option>
                                <option value="2" data-bonus-type="1">Beli produk tertentu, dapat diskon harga (%)</option>
                                <option value="2" data-bonus-type="2">Beli produk tertentu, dapat diskon harga (Rp)</option>
                                <option value="2" data-bonus-type="3">Beli produk tertentu, bonus produk tertentu</option>
                            </select>
                        </div>
                    </div>

                    <div id="promo_value">

                    </div>

                    <div id="bonus_value"></div>
                    <div class="row mt-1">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label class="col-form-label font-weight-bold">Penggunaan Terbatas <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <div class="custom-control custom-switch mb-3">
                                        <input
                                            type="checkbox"
                                            class="custom-control-input"
                                            id="is_limited"
                                            name="is_limited"
                                            value="0"
                                            style="transform:scale(2)"
                                        >
                                        <label class="custom-control-label" for="is_limited"></label>
                                    </div>
                                    <input id="max_coupon_use" type="number" name="max_coupon_use" class="form-control d-none" value="0">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold d-block">Kupon Per Pelanggan</label>
                                <div class="col-sm-8">
                                    <div class="btn-group-toggle input-radio-group mb-3" data-toggle="buttons">
                                        <label
                                            class="btn btn-input-radio d-block active"
                                        >
                                            <input
                                                type="radio"
                                                name="is_more_than_one"
                                                class="is_more_than_one"
                                                value="1"
                                            >
                                            <span class="text-center">1 Kali</span>
                                        </label>
                                        <label
                                            class="btn btn-input-radio d-block"
                                        >
                                            <input
                                                type="radio"
                                                name="is_more_than_one"
                                                class="is_more_than_one"
                                                value="2"
                                            >
                                            <span class="text-center">Lebih dari 1 Kali</span>
                                        </label>
                                    </div>
                                    <input type="number" id="max_coupon_use_per_customer" name="max_coupon_use_per_customer" class="form-control d-none" value="1">
                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="row mt-5">
                        <div class="col-sm-12">
                            <button
                                class="d-flex justify-content-between align-items-center pl-0"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse-customer"
                                aria-expanded="false"
                                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                            >
                                <h4 class="font-weight-bold mb-3">Ketentuan Tambahan</h4>
                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                            </button>
                        </div>

                    </div>

                    <div class="collapse" id="collapse-customer">
                        <div class="row mt-4">
                            <div class="col-sm-12">
                                <h5 class="font-weight-bold mb-3">Pelanggan</h5>
                            </div>
                        </div>

                        <div class="form-group row d-flex align-items-center">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Berlakukan untuk semua pelanggan ?</label>
                            </div>
                            <div class="col-sm-10">
                                <div class="custom-control custom-switch">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input"
                                        id="all_customer"
                                        name="all_customer"
                                        value="1"
                                        style="transform:scale(2)"
                                        checked
                                    >
                                    <label class="custom-control-label" for="all_customer"></label>
                                </div>
                            </div>
                        </div>


                        <div id="customer_criteria_wrapper" style="display:none;">
                            <div id="customer_name_wrapper" style="background-color:#FBFBFB;border:1px dashed #C4C4C4;border-radius:10px;" class="p-4 mb-4">
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-sm-2">
                                        <label class="col-form-label font-weight-bold d-block">Pilih dari daftar pelanggan</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="custom-control custom-switch">
                                            <input
                                                type="checkbox"
                                                class="custom-control-input"
                                                id="is_specific_customer"
                                                name="is_specific_customer"
                                                value="0"
                                                style="transform:scale(2)"
                                            >
                                            <label class="custom-control-label" for="is_specific_customer"></label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row" id="customer_keyword">
                                    <div class="col-sm-2">
                                        <label class="col-form-label font-weight-bold d-block">Kupon akan berlaku pada pelanggan yang memiliki nama ?</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="customer_name" id="customer_name" data-role="tagsinput" disabled>
                                    </div>
                                </div>

                                <div class="form-group row" id="customer_list" style="display:none;">
                                    <div class="col-sm-2">
                                        <label class="col-form-label font-weight-bold d-block">Pilih Pelanggan</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="sc_customer_id" id="sc_customer_id" multiple disabled>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="customer_birth_date_wrapper" style="background-color:#FBFBFB;border:1px dashed #C4C4C4;border-radius:10px;" class="p-4">
                                <div class="form-group row d-flex align-items-center">
                                    <div class="col-sm-2">
                                        <label class="col-form-label font-weight-bold d-block">Kupon akan berlaku pada pelanggan yang sedang berulang tahun tanggal?</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input
                                            type="date"
                                            class="form-control form-control-sm"
                                            name="customer_birth_date"
                                            placeholder="Tanggal Lahir Pelanggan"
                                            id="customer_birth_date"
                                            disabled
                                        >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <hr>
                            </div>
                        </div>

                        @if(count(get_cabang()) > 1)
                            <div class="row mt-4">
                                <div class="col-sm-12">
                                    <h5 class="font-weight-bold mb-3">Cabang</h5>
                                </div>
                            </div>

                            <div class="form-group row d-flex align-items-center">
                                <div class="col-sm-2">
                                    <label class="col-form-label font-weight-bold d-block">Berlakukan untuk Semua Cabang ?</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="custom-control custom-switch">
                                        <input
                                            type="checkbox"
                                            class="custom-control-input check-bank"
                                            id="all_branch"
                                            name="all_branch"
                                            value="0"
                                            style="transform:scale(2)"
                                        >
                                        <label class="custom-control-label" for="all_branch"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="option-branch-wrapper">
                                <div class="col-sm-2">
                                    <label class="col-form-label font-weight-bold d-block">Berlaku untuk cabang ?</label>
                                </div>
                                <div class="col-sm-10">
                                    <select name="branch_id" class="form-control js-example-basic-single" id="branch_id" multiple="multiple">
                                        @foreach(get_cabang() as $key => $item)
                                            @if($item->id != merchant_id())
                                                <option value="{{$item->id}}"  data-name="{{$item->nama_cabang}}">{{$item->nama_cabang}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-12 mb-4 text-right">
                            <input type="hidden" id="is_type" name="is_type">
                            <input type="hidden" id="bonus_type" name="bonus_type">
                            <a href="{{route('merchant.toko.discount-coupon.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                            <button class="btn btn-success" id="btn-save"><i class="fa fa-save mr-2"></i> Simpan Kupon</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script>
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        $("#promo_type").select2({
            placeholder: "--- Pilih Tipe Kupon ---"
        });

        $("#customer").select2();
        $("#category_customer").select2({
            placeholder: "--- Pilih Kategori Pelanggan ---"
        });
        $("#branch_id").select2();

        // bonus tipe 1 %
        // bonus tipe 2 rp
        // bonus tipe 3 bonus produk
        $("#promo_type").on("change", function(){
            let promoType = $("#promo_type").val();
            let bonusType = $("#promo_type").find(":selected").data("bonus-type");

            $("#bonus_type").val(bonusType);

            if(promoType == 1){
                let promoValueHtml = `
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-sm-2 col-form-label font-weight-bold">Min total transaksi <span class="text-danger">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm amount_currency" name="promo_value" value="0">
                            </div>
                        </div>`;
                $("#promo_value").html(promoValueHtml).find('.amount_currency').mask("#.##0,00", {reverse: true});
                $("#is_type").val(2);

            } else {
                let promoValueHtml = `
                        <div class="row">
                            <div class="col-md-12">
                                <div class="product-by-wrapper">
                                    <div>
                                        <button data-id="1" type="button" class="btn btn-product-by active">Produk</button>
                                    </div>
                                    <div>
                                        <button data-id="2" type="button" class="btn btn-product-by">Kategori</button>
                                    </div>
                                    <div>
                                        <button data-id="3" type="button" class="btn btn-product-by">Merk</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Produk <span class="text-danger">*</span></label>
                                <small>Kupon akan berlaku jika pelanggan membeli produk apa saja?</small>
                            </div>
                            <div class="col-sm-10" id="select-product-wrapper">
                                <select name="product_details" class="form-control select_ajax_sc_product_id" id="product_details">

                </select>
                <input type="hidden" name="select_product_by" value="1">
            </div>
            <div class="col-sm-10 offset-2">
                <table id="details" class="table table-borderless" style="width:100%;">
                    <tr>
                        <th style="width:50%;">Nama Produk</th>
                        <th style="width:40%;">Kuantitas</th>
                        <th style="width:10%;" class="text-center">Hapus</th>
                    </tr>
                </table>
            </div>
            <input type="hidden" name="promo_value" value="0">
        </div>`;
                $("#promo_value").html(promoValueHtml);
                $("#promo_value").find(".select_ajax_sc_product_id").select2({
                    placeholder:"--Pilih Produk--",
                    ajax: {
                        type: "GET",
                        url: "{{route('api.product.all-v2')}}?md_merchant_id={{merchant_id()}}",
                        dataType: 'json',
                        delay: 250,
                        headers:{
                            "senna-auth":"{{get_user_token()}}"
                        },
                        data: function (params) {
                            return {
                                key: params.term
                            };
                        },
                        processResults: function (data) {
                            //    console.log(data)
                            return {
                                results: data
                            };
                        },
                        cache: true
                    },
                });

                $("#is_type").val(1);
            }

            let bonusValueHtml = `
                    <div id="bonus_type_product">
                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <h4 class="font-weight-bold mb-3">Bonus Produk</h4>
                                <hr>
                            </div>
                        </div>

                        <div class="form-group row d-flex align-items-center">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Produk <span class="text-danger">*</span></label>
                                <small>Apa saja bonus produk yang akan didapatkan pelanggan ?</small>
                            </div>
                            <div class="col-sm-10">
                                <select name="bonus_value_product" class="form-control select_ajax_sc_product_id" id="bonus_value_product">
                                    <option value="-1">--- Pilih Produk ---</option>

            </select>
        </div>
        <div class="col-sm-10 offset-2">
            <table id="bonus_value_product_dynamic" class="table table-borderless" style="width:100%;">
                <tr>
                    <th style="width:50%;">Nama Produk</th>
                    <th style="width:40%;">Kuantitas</th>
                    <th style="width:10%;" class="text-center">Hapus</th>
                </tr>
            </table>
        </div>
    </div>
    <div class="form-group row d-flex align-items-center" id="is_apply_multiply">
        <div class="col-sm-2">
            <label class="col-form-label font-weight-bold d-block">Berlaku Kelipatan ?</label>
        </div>
        <div class="col-sm-10">
            <div class="custom-control custom-switch">
                <input
                    type="checkbox"
                    class="custom-control-input check-bank"
                    id="is_applies_multiply"
                    name="is_applies_multiply"
                    value="1"
                    style="transform:scale(2)"
                >
                <label class="custom-control-label" for="is_applies_multiply"></label>
            </div>
        </div>
    </div>
</div>
<div id="bonus_type_price">
    <div class="row mt-5">
        <div class="col-sm-12">
            <h4 class="font-weight-bold mb-3">Diskon Harga</h4>
            <hr>
        </div>
    </div>
    <div class="form-group row d-flex align-items-center">
        <div class="col-sm-2">
            <label class="col-form-label font-weight-bold d-block">Harga <span class="text-danger">*</span></label>
            <small>Berapa diskon yang didapatkan pelanggan?</small>
        </div>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <div class="btn-group btn-group-toggle grab-btn__group mb-3" data-toggle="buttons">
                            <label
                                class="btn btn-outline-warning active"
                                style="color:white!important;border-radius:9px!important;"
                                id="typeRp-input"

                            >
                                <input
                                    type="radio"
                                    name="bonus_type"
                                    id="typeRp"
                                    autocomplete="off"
                                    value="1"
                                    checked
                                >
                                Nominal (Rp)
                            </label>
                            <label
                                class="btn btn-outline-warning active"
                                style="color:white!important;border-radius:9px!important;"
                                id="typePercent-input"
                            >
                                <input
                                    type="radio"
                                    name="bonus_type"
                                    id="typePercent"
                                    autocomplete="off"
                                    value="2"
                                    checked
                                >
                                Persen (%)
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <input type="text" id="bonus_val_rp" class="form-control form-control-sm amount_currency" name="bonus_val[]" placeholder="Nilai Potongan (Rp)" required>
                    <input type="number" id="bonus_val_percent" step="any" class="form-control form-control-sm" name="bonus_val[]" placeholder="Nilai Potongan (%)" required>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group row d-flex align-items-center" id="max_promo_value">
        <label class="col-sm-2 col-form-label font-weight-bold">Maksimal Jumlah Diskon <span class="text-danger">*</span></label>
        <div class="col-sm-10">
            <input type="text" class="form-control form-control-sm amount_currency" name="max_promo_value" value="0" required>
        </div>
    </div>

    <div class="form-group row d-flex align-items-center" id="is_apply_multiply">
        <div class="col-sm-2">
            <label class="col-form-label font-weight-bold d-block">Berlaku Kelipatan ?</label>
        </div>
        <div class="col-sm-10">
            <div class="custom-control custom-switch">
                <input
                    type="checkbox"
                    class="custom-control-input check-bank"
                    id="is_applies_multiply"
                    name="is_applies_multiply"
                    value="1"
                    style="transform:scale(2)"
                >
                <label class="custom-control-label" for="is_applies_multiply"></label>
            </div>
        </div>
    </div>
</div>`;

            $("#bonus_value").html(bonusValueHtml);
            $("#bonus_value").find(".select_ajax_sc_product_id").select2({
                placeholder:"--Pilih Produk--",
                ajax: {
                    type: "GET",
                    url: "{{route('api.product.all-v2')}}?md_merchant_id={{merchant_id()}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        //    console.log(data)
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
            $("#bonus_value").find(".amount_currency").mask("#.##0,00", {reverse: true});
            if(bonusType == 1){
                // jika bonus tipe persen
                $("#bonus_value").find("#bonus_val_rp").remove();
                $("#bonus_value").find("#typeRp-input").remove();
                $("#bonus_value").find("#bonus_type_product").remove();
                $("#bonus_value").find("#is_apply_multiply").remove();
            }

            if(bonusType == 2){
                // jika bonus tipe rupiah / nominal
                $("#bonus_value").find("#max_promo_value").remove();
                $("#bonus_value").find("#bonus_val_percent").remove();
                $("#bonus_value").find("#typePercent-input").remove();
                $("#bonus_value").find("#bonus_type_product").remove();
            }

            if(bonusType == 3){
                // jika bonus tipe produk
                $("#bonus_value").find("#bonus_type_price").remove();
            }
        });

        let detailId = [];
        $(document).on('click', '.btn-product-by', function(){
            let btnEl = [...document.querySelectorAll('.btn-product-by')];
            btnEl.forEach(item => {
                $(item).removeClass('active');
            });
            $(this).addClass('active');

            let val = $(this).attr('data-id');

            if(val == 1){
                let html = `<select name="product_details" class="form-control select_ajax_sc_product_id" id="product_details" >
                            <option value="-1">--- Pilih Produk ---</option>

                </select>
                <input type="hidden" name="select_product_by" value="1">`;
                let detailHtml = `<tr>
                                <th style="width:50%;">Nama Produk</th>
                                <th style="width:40%;">Kuantitas</th>
                                <th style="width:10%;" class="text-center">Hapus</th>
                            </tr>`;

                $("#select-product-wrapper").html(html);
                $("#select-product-wrapper").find(".select_ajax_sc_product_id").select2({
                    placeholder:"--Pilih Produk--",
                    ajax: {
                        type: "GET",
                        url: "{{route('api.product.all-v2')}}?md_merchant_id={{merchant_id()}}",
                        dataType: 'json',
                        delay: 250,
                        headers:{
                            "senna-auth":"{{get_user_token()}}"
                        },
                        data: function (params) {
                            return {
                                key: params.term
                            };
                        },
                        processResults: function (data) {
                            //    console.log(data)
                            return {
                                results: data
                            };
                        },
                        cache: true
                    },
                });

                $("#details").html(detailHtml);
            }

            if(val == 2){
                let html = `<select name="product_details" class="form-control js-example-basic-single" id="product_details">
                            <option value="-1">--- Pilih Kategori ---</option>
                            @foreach($category as $key => $item)
                <option value="{{$item->id}}" id="product_detail_{{$item->id}}" data-name="{{$item->name}}">{{$item->name}}</option>
                            @endforeach
                </select>
                <input type="hidden" name="select_product_by" value="2">`;
                let detailHtml = `<tr>
                        <th style="width:50%;">Nama Kategori</th>
                        <th style="width:40%;">Kuantitas</th>
                        <th style="width:10%;" class="text-center">Hapus</th>
                    </tr>`;
                $("#select-product-wrapper").html(html);
                $("#details").html(detailHtml);
            }

            if(val == 3){
                let html = `<select name="product_details" class="form-control js-example-basic-single" id="product_details">
                            <option value="-1">--- Pilih Merk ---</option>
                            @foreach($merk as $key => $item)
                <option value="{{$item->id}}" id="product_detail_{{$item->id}}" data-name="{{$item->name}}">{{$item->name}}</option>
                            @endforeach
                </select>
                <input type="hidden" name="select_product_by" value="3">`;
                let detailHtml = `<tr>
                        <th style="width:50%;">Nama Merk</th>
                        <th style="width:40%;">Kuantitas</th>
                        <th style="width:10%;" class="text-center">Hapus</th>
                    </tr>`;
                $("#select-product-wrapper").html(html);
                $("#details").html(detailHtml);
            }
            detailId = [];
            $(document).find(".js-example-basic-single").select2();

        });

        $(document).on('change','#product_details', function(){
            let productId = $('#product_details option:selected').val();
            if(productId ==  '-1'){
                return;
            }
            detailId.push(productId);

            var duplicateId = detailId.filter(item => {
                return item == productId;
            });

            if(duplicateId.length <= 1){
                let html = `<tr id="details-${productId}">
                                <td>
                                    <span>${$('#product_details option:selected').text()}</span>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center increment-wrapper">
                                        <button type="button" class="btn btn-increment btn-min" onclick="addProductQty('min', quantity_${productId})">
                                            -
                                        </button>
                                        <input
                                            type="number"
                                            class="form-control text-center product-detail-input"
                                            value="1"
                                            id="quantity_${productId}"
                                            data-id="${productId}"
                                        >
                                        <button type="button" class="btn btn-increment btn-plus" onclick="addProductQty('plus', quantity_${productId})">
                                            +
                                        </button>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        name="remove"
                                        data-id="${productId}"
                                        data-target="#details-${productId}"
                                        class="btn btn-xs btn-delete-xs btn-rounded btn_remove">
                                            <span class='fa fa-trash-alt' style='color: white'></span>
                                    </button>
                                </td>
                            </tr>`;
                $("#details").append(html);
            }
        });

        const addProductQty = (type, selector)=> {
            let val = parseInt( $(selector).val());

            if(type == "plus"){
                $(selector).val(val+1);
            } else {
                if((val - 1) >= 0){
                    $(selector).val(val-1);
                }
            }
        }

        $(document).on('click', '.btn_remove', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");
            detailId = detailId.filter(item => {
                return item != parseInt(targetId);
            });
            $(target).remove();
        });

        var bonusId = [];
        $(document).on('change','#bonus_value_product', function(){
            let val = $(this).find(':selected').val();
            if(val ==  '-1'){
                return;
            }
            bonusId.push(val);

            var duplicateBonusId = bonusId.filter(item => {
                return item == val;
            });

            if(duplicateBonusId.length <= 1){
                let html = `<tr id="details-2-${val}">
                            <td>
                                <input type="hidden" name="bonus_val[]" value="${val}">
                                <span>${$(this).find(':selected').text()}</span>
                            </td>
                            <td>
                                <div class="d-flex align-items-center increment-wrapper">
                                    <button type="button" class="btn btn-increment btn-min" onclick="addProductQty('min', bonus_qty_${val})">
                                        -
                                    </button>
                                    <input
                                        type="number"
                                        name="bonus_val_qty[]"
                                        value="1"
                                        class="form-control text-center bonus-product-input"
                                        required
                                        id="bonus_qty_${val}"
                                        data-id="${val}"
                                    >
                                    <button type="button" class="btn btn-increment btn-plus" onclick="addProductQty('plus', bonus_qty_${val})">
                                        +
                                    </button>
                                </div>
                            </td>
                            <td class="text-center">
                                <button
                                    type="button"
                                    name="remove"
                                    data-id="${val}"
                                    data-target="#details-2-${val}"
                                    class="btn btn-xs btn-delete-xs btn-rounded btn_remove_2"
                                >
                                    <span class='fa fa-trash-alt' style='color: white'></span>
                                </button>
                            </td>
                        </tr>`;
                $('#bonus_value_product_dynamic').append(html);
            }
        });

        $(document).on('click', '.btn_remove_2', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");
            bonusId = bonusId.filter(item => {
                return item != parseInt(targetId);
            });
            $(target).remove();
        });


        $("#all_customer").on('change', function(){
            if($(this).prop('checked')==true){
                $(this).val(1);
                $("#customer_name").prop("disabled", true);
                $("#customer_birth_date").prop("disabled", true);
                $("#sc_customer_id").prop("disabled", true);
                $("#customer_criteria_wrapper").hide();
            } else {
                $(this).val(0);
                $("#customer_name").prop("disabled", false);
                $("#customer_birth_date").prop("disabled", false);
                $("#sc_customer_id").prop("disabled", false);
                $("#customer_criteria_wrapper").show();
            }
        });

        $("#is_not_know_end").on('change', function(){
            if($(this).prop('checked')==true){
                $(this).val(1);
                $("#end_date").prop("disabled", true);
            } else {
                $(this).val(0);
                $("#end_date").prop("disabled", false);
            }
        });

        $("#is_daily").on('change', function(){
            if($(this).prop('checked')==true){
                $(this).val(1);
                $("#clock-wrapper").hide();
            } else {
                $(this).val(0);
                $("#clock-wrapper").show();
            }
        });

        $("#all_branch").on('change', function(){
            if($(this).prop('checked')==true){
                $(this).val(1);
                $("#branch_id").prop("disabled", true);
                $("#option-branch-wrapper").hide();
            } else {
                $(this).val(0);
                $("#option-branch-wrapper").show();
                $("#branch_id").prop("disabled", false);
            }
        });

        $("#is_specific_customer").on('change', function(){
            if($(this).prop('checked') == true)
            {
                $(this).val(1);
                $("#customer_keyword").hide();
                $("#customer_list").show();
                $("#customer_birth_date_wrapper").hide();

            } else {
                $(this).val(0);
                $("#customer_keyword").show();
                $("#customer_list").hide();
                $("#customer_birth_date_wrapper").show();
            }
        })

        $("#is_limited").on('change', function(){
            if($(this).prop('checked') == true){
                $(this).val(1);
                $("#max_coupon_use").removeClass("d-none");

            } else {
                $(this).val(0);
                $("#max_coupon_use").addClass("d-none");
            }
        });

        $(".is_more_than_one").on("change", function(){
            let val = $(this).val();
            if(val == 1){
                $("#max_coupon_use_per_customer").addClass("d-none");
            } else {
                $("#max_coupon_use_per_customer").removeClass("d-none");
            }
        });



        $(document).on("keyup", "#bonus_val_percent", function(){
            let val = $(this).val();

            if(val > 100){
                $(this).val(100);
            }

            if(val < 0){
                $(this).val(0);
            }
        })

        let startDateRange = null;
        let endDateRange = null;
        $("#start_date").on("change", function(){
            startDateRange = new Date($(this).val());
            getRangeDate(startDateRange, endDateRange);
        });

        $("#end_date").on("change", function(){
            endDateRange = new Date($(this).val());
            getRangeDate(startDateRange, endDateRange);
        });

        let checkVal = [];
        function getRangeDate(startDate, endDate)
        {
            checkVal = [];
            $("#result_day").html("");
            if(!startDate, !endDate){
                return;
            }

            const date = new Date(startDate.getTime());
            const dayName = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
            let dayRange = [];

            const dates = [];
            while (date <= endDate) {
                dates.push(new Date(date).getDay());
                date.setDate(date.getDate() + 1);
            }

            if(dates.length >= 7){
                dayRange = [...dayName];
            } else {
                dates.forEach((val) => {
                    dayRange.push(dayName[val]);
                });
            }

            let html = `<div class="form-check form-check-inline mr-5">
                        <input class="form-check-input" type="checkbox" id="check_all_day" onchange="checkAllDay(this, 1)">
                        <label class="form-check-label border-bottom">Pilih Semua</label>
                    </div>
                    `;

            dayRange.forEach(item => {
                html += `<div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" value="${item}" name="days[]" onchange="checkAllDay(this, 0)">
                            <label class="form-check-label">${item}</label>
                        </div>
                        `;
            });

            if(dayRange.length > 0)
            {
                $("#result_day").append(html);
            }

        }

        const checkAllDay = (e, isAll) => {
            const inputDay = document.querySelectorAll('input[name="days[]"]');
            if(isAll == 1){
                if(e.checked){
                    inputDay.forEach(item => {
                        item.checked = true;
                        const index = checkVal.indexOf(item.value);
                        if(index === -1 ){
                            checkVal.push(item.value);
                        }

                    });
                } else {
                    inputDay.forEach(item => {
                        item.checked = false;
                    });
                    checkVal = [];
                }
            } else {
                if(e.checked){
                    checkVal.push(e.value);
                } else {
                    checkVal = checkVal.filter(item => {
                        return item !== e.value;
                    });
                }

                if(checkVal.length === inputDay.length){
                    document.querySelector('#check_all_day').checked = true;
                } else if(checkVal.length === inputDay.length - 1){
                    document.querySelector('#check_all_day').checked = false;
                }
            }
        }

        $(document).ready(function () {
            getCustomer()
            $("#is_limited").val(0)
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');
                var days = data.getAll('days[]').filter(item => {
                    return item != "null";
                });
                var day = days.map(item => {
                    return {
                        "day_name":item
                    }
                });

                var promoType = data.get('promo_type');
                var bonusType = data.get('bonus_type');
                var promoProductDetailElement = [...document.querySelectorAll('.product-detail-input')];
                var bonusVal = (bonusType == 3)? [...document.querySelectorAll('.bonus-product-input')] : data.getAll('bonus_val[]');
                var details = [];

                if(promoType == 2){
                    promoProductDetailElement.forEach(item => {
                        details.push({
                            "id":parseInt(item.getAttribute('data-id')),
                            "quantity": parseInt(item.value)
                        })
                    });
                }

                if(bonusType == 3){
                    var bonus_value = bonusVal.map(item => {
                        return {
                            "value":parseInt(item.getAttribute('data-id')),
                            "quantity":parseInt(item.value)
                        }
                    });

                } else {
                    var bonus_value = bonusVal.map(item => {
                        return {
                            "value":item
                        }
                    });
                }

                var applyMultiply = data.get('is_applies_multiply');
                var is_daily = data.get('is_daily');
                var is_not_know_end = data.get('is_not_know_end');
                var all_branch = data.get('all_branch');

                if(applyMultiply === "null" || applyMultiply === null){
                    data.set('is_applies_multiply', 0);
                }

                if(is_daily === "null" || is_daily === null){
                    data.set('is_daily', 0);
                }

                if(is_not_know_end === "null" || is_not_know_end === null){
                    data.set('is_not_know_end', 0);
                }

                if(all_branch === "null" || all_branch === null){
                    data.set('all_branch', 0);
                }

                var is_all_customer = ($("#all_customer").val() == 1) ? 1:0;
                var is_specific_customer = ($("#is_specific_customer").val() == 1)? 1:0;

                var customer_ids = ($("#sc_customer_id").select2('data'))?$("#sc_customer_id").select2('data'):[];
                var specific_customer_id = [...customer_ids].map(item => {
                    return {
                        "sc_customer_id":parseInt(item.id)
                    }
                });

                var customer_name_val = ($("#customer_name").val())? $("#customer_name").val().split(","):[];

                var customer_name = [...customer_name_val].map(item => {
                    return {
                        "name":item
                    }
                });

                var reqCustomerTerms = {
                    "birth_date": ($("#all_customer").val() == 1)? "-1":($("#customer_birth_date").val())? moment($("#customer_birth_date").val()).format('MM-DD'):"-1",
                    "birth_year":($("#all_customer").val() == 1)? "-1":($("#customer_birth_date").val())? moment($("#customer_birth_date").val()).format('YYYY'):"-1",
                    "customer_name": ($("#all_customer").val() == 1)? []:customer_name,
                    "specific_customer_id": ($("#all_customer").val() == 1)? []:specific_customer_id
                }

                var branch = ($("#branch_id").select2("data"))?$("#branch_id").select2("data"):[];

                var assign_to_branch = branch.map(item => {
                    return parseInt(item.id);
                });

                data.append('day', JSON.stringify(day));
                data.append('bonus_value', JSON.stringify(bonus_value));
                data.append('details', JSON.stringify(details));
                data.append('activation_type', 1);
                data.append("req_customer_terms", JSON.stringify(reqCustomerTerms));
                data.append('assign_to_branch', JSON.stringify(assign_to_branch));
                data.append('is_all_customer', is_all_customer);
                data.set('is_specific_customer', is_specific_customer);

                ajaxTransfer("{{route('merchant.toko.discount-coupon.save')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        })

        function getCustomer()
        {
            $("#sc_customer_id").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.transaction.sale-order.customer-list')}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }


    </script>
@endsection

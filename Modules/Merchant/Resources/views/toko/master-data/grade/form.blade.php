<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-category" class='form-horizontal form-konten' backdrop="">

    <div class="row">

    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Nama Grade</label>
            <input type="text" class="form-control" name="grade_name" placeholder="Nama Grade" id="grade" value="{{$data->grade_name}}" required>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Gaji Pokok</label>
            <input type="text" class="form-control amount_currency" name="basic_salary" placeholder="Gaji Pokok" id="basic-salary" value="{{rupiah($data->basic_salary)}}" required>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Sub Grade dari:</label>
            <select class="form-control" name="parent_id" required id="parent">
                <option value="-1">Bukan Sub Grade</option>
                @foreach($option as $item)
                    @if($item->id!=$data->id)
                    <option value="{{$item->id}}"
                            @if($item->id==$data->parent_id) selected @endif
                    >{{$item->nama_grade}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Berlaku untuk Cabang:</label>
            <select name="md_merchant_id" id="branch" class="form-control" multiple="multiple" required>
                <option value="-1"  @if(in_array('-1',$merchantId)) selected @endif>Semua Cabang</option>
                @foreach(get_cabang() as $key  =>$item)
                    <option value="{{$item->id}}" @if(in_array($item->id,$merchantId)) selected @endif>{{$item->nama_cabang}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Deskripsi</label>
            <textarea name="desc" class="form-control" placeholder="Deskripsi" id="desc">{{$data->desc}}</textarea>
        </div>
    </div>
    </div>




    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}' id="id">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-category').submit(function () {
            let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
            let merchantIds = [];

            // cek jika pilih semua cabang
            if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                merchantIds.push(-1);

            } else {
                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
            }

            let md_merchant_id = merchantIds.join(',');
            var data = new FormData();
            data.append('id',$("#id").val())
            data.append('md_merchant_id',md_merchant_id)
            data.append('grade_name',$("#grade").val())
            data.append('desc',$("#desc").val())
            data.append('basic_salary',$("#basic-salary").val())
            data.append('parent_id',$("#parent").val())
            ajaxTransfer("{{route('merchant.toko.grade.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    })
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('#type-category').select2();
        $("#branch").select2()
        $("#parent").select2()
        $('.amount_currency').mask("#.##0,00", {reverse: true});

        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Grade Karyawan</h4>
                                        <span class="span-text">Untuk menambah grade karyawan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    $("#branch").on("select2:select", function(e){
        if(e.params.data.id == "-1"){
            $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
        } else {
            $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
        }
    });


</script>

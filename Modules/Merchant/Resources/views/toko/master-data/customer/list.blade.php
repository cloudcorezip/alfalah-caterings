<div class="table-responsive">
    <table class="table table-custom" id="table-data-customer" width="100%" cellspacing="0">
        <thead>
        <tr>
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<script>
    function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
            ajaxTransfer("{{route('merchant.toko.customer.delete')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    }
    function reloadDataTable(isReload=0)
    {
        ajaxDataTable('#table-data-customer', 1, "{{route('merchant.toko.customer.datatable')}}?key_val={{$key_val}}&sk={{$searchKey}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ],0);
    }
    $(document).ready(function() {
       reloadDataTable(0)
    })

    function deleteSyncData(formData){
        modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
            const DeleteSyncData = {
                async start(formData){
                    const response = await this._deleteSync(formData);
                    const code = await response.code;
                    if(code !== 200){
                        toastForSaveData('Terjadi kesalahan, coba beberapa saat lagi !','warning',true,'');
                        closeModal(3000);
                        return;
                    } 
                    this.deleteData(formData.id);
                },
                async _deleteSync(formData){
                    let dataSend = {
                        'merchant_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_merchant_id}}'),
                        'plugin_id':  parseInt('{{($activePlugin==false)?0:$activePlugin->md_plugin_id}}'),
                        'resource_name': formData.resource_name
                    };
                    let formBody = [];
                    for (let property in dataSend) {
                        let encodedKey = encodeURIComponent(property);
                        let encodedValue = encodeURIComponent(dataSend[property]);
                        formBody.push(encodedKey + "=" + encodedValue);
                    }
                    formBody = formBody.join("&");

                    // tes get google contact in api.dev
                    return $.ajax({
                        method: 'POST',
                        url:'{{$apiUrl}}/api/google/contact/delete',
                        async: true,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            "senna-auth": "{{get_user_token()}}",
                            'Access-Control-Allow-Origin': '*',
                        },
                        data: formBody
                    })    
                },

                deleteData(id){
                    let data = new FormData();
                    data.append('id', id);
                    ajaxTransfer("{{route('merchant.toko.customer.delete')}}", data, function (response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                    });
                }
            }
            
            DeleteSyncData.start(formData);
        });
        
        
    }

</script>

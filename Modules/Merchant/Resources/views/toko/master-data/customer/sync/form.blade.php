<style>
    .table td {
        vertical-align: middle;
        line-height: 23px!important;
        white-space: normal!important;
    }
</style>

@if(is_null($activePlugin->email) || is_null($activePlugin->client_id) || is_null($activePlugin->client_secret) || is_null($activePlugin->client_token))
<div class="container-fluid" style="position:relative; width: 100%;">
    <div class="row">
        <div class="col-md-12 d-flex align-items-center justify-content-center" style="height: 42vh;">
            <div class="text-center">
                <h1 class="mb-4"><i class="fas fa-cogs" style="font-size:93px; color: #FF9900;"></i></h1>
                <a class="btn btn-sm btn-bg__orange py-2 px-4 btn-rounded text-white"
                    href="{{route('merchant.toko.profile.detail', ['id'=>merchant_id(), 'page'=>'p-gcontact'])}}">
                    Silahkan atur plugin terlebih dahulu
                </a>
            </div>
        </div>
    </div>
</div>
@else
<div class="container-fluid" style="position:relative; width: 100%;">
    <div class="row mb-4">
        <div class="d-flex align-items-center justify-content-between" style="padding-left:15px;padding-right:15px;">
            <select class="form-control" name="dataPerPage" id="dataPerPage">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="50">50</option>
                <option value="100">100</option>
            </select>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-md-12">
            <div clas="table-responsive">
                <table class="table table-custom" id="table-data" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="select-item all">
                            </th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No Handphone</th>
                        </tr>
                    </thead>
                    <tbody id="data-result">

                    </tbody>
                </table>
            </div>

            <div class="d-flex justify-content-end">
                <ul class="pagination">
                    <li class="page-item mr-2">
                        <a class="page-link" style="border-radius:0;" onclick="GetContact.prevPage(localStorage.getItem('page'))">
                            <i class="fas fa-chevron-left"></i>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" style="border-radius:0;" onclick="GetContact.nextPage(localStorage.getItem('page'))">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <div
        id="spinner"
        style="position:absolute; top: 50%; left:50%;border:2px solid #FFA943; border-right-color:transparent;"
        class="spinner-border">
        <span class="sr-only"></span>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
    <button class="btn btn-success" id="btn-save">Simpan</button>
</div>
@endif



<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Silahkan pilih kontak yang ingin anda masukkan ke data pelanggan</span>
                                    </div>`);
    });
</script>

<script>
    const GetContact = {
        data: @json($data),
        async start(config, page, pageSize, type){
            const fetchData = await this.fetchData(config, page, pageSize, type);
            const data = await fetchData.data.list;
            const code = await fetchData.code;
            const nextPageToken = await fetchData.data.nextPageToken;
            if(code != 200){
                toastForSaveData('Terjadi kesalahan saat pengambilan data, coba beberapa saat lagi !','warning',true,'');
                closeModal(3000);
                return;
            }

            localStorage.setItem('next', nextPageToken);
            if(page == 0){
                localStorage.setItem('config', JSON.stringify(config));
                localStorage.setItem('page',page);
                localStorage.setItem('total_items', fetchData.data.totalItems);
            }

            const responseCompare = await this._compareData();
            const compareData = await responseCompare.data;
            this._renderData(data, compareData);
            const checkbox = [...document.querySelectorAll('.select-item')];
            checkbox.forEach(item => {
                item.addEventListener('change', (event) => {
                    this._checkData(event, data, checkbox);
                })
            })
        },
        async fetchData(config, page, pageSize, type){
            let dataSend;
            let dataConfig = config.filter(item => {
                return item.page == page
            })[0];


            if(type == 'prev'){
                dataSend = {
                    'merchant_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_merchant_id}}'),
                    'plugin_id':  parseInt('{{($activePlugin==false)?0:$activePlugin->md_plugin_id}}'),
                    'page_size': pageSize,
                    'next_page_token':-1,
                    'previous_page_token': dataConfig.prev
                };
            } else {
                dataSend = {
                    'merchant_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_merchant_id}}'),
                    'plugin_id':  parseInt('{{($activePlugin==false)?0:$activePlugin->md_plugin_id}}'),
                    'page_size': pageSize,
                    'next_page_token':dataConfig.next,
                    'previous_page_token': -1
                };
            }

            let formBody = [];
            for (let property in dataSend) {
                let encodedKey = encodeURIComponent(property);
                let encodedValue = encodeURIComponent(dataSend[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");

            // tes get google contact in api.dev
            return $.ajax({
                method: 'POST',
                url:'{{$apiUrl}}/api/google/contact/all',
                async: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "senna-auth": "{{get_user_token()}}",
                    'Access-Control-Allow-Origin': '*',
                },
                data: formBody,
                beforeSend: function(){
                    $('#spinner').show();
                },
                success: function(response){
                    $('#spinner').hide();
                },
                error: function(error){
                    toastForSaveData('Terjadi ksesalahan, coba beberapa saat lagi !','warning',true,'');
                    $('#spinner').hide();
                }
            })
        },
        async _compareData(){
            return $.ajax({
                method: 'POST',
                url:"{{route('merchant.toko.customer.contact-sync-compare')}}",
                async: true,
                headers: {
                    "senna-auth": "{{get_user_token()}}",
                    'Access-Control-Allow-Origin': '*',
                },
                data: {'_token':'{{csrf_token()}}'},
            });
        },
        _renderData(data, compareData){
            const dataView = data.map(item => {
                return {
                    id: item.resourceName.replace('people/', ''),
                    name : (item.names)?item.names[0].displayName:'-',
                    email: (item.emailAddresses)?item.emailAddresses[0].value:'-',
                    phone_number: (item.phoneNumbers)?item.phoneNumbers[0].value:'-',
                    resource_name: item.resourceName,
                    etag: item.etag,
                    address: (item.addresses)?item.addresses[0].streetAddress:'-',
                    genders: (item.genders)? item.genders[0].value:'-',
                    city_name: (item.addresses)? ((item.addresses)[0].city? item.addresses[0].city:'-'):'-',
                    region_name: (item.addresses)? ((item.addresses)[0].region? item.addresses[0].region:'-'):'-',
                    postal_code: (item.addresses)? ((item.addresses)[0].postalCode? item.addresses[0].postalCode:'-'):'-',
                    type_address: (item.addresses)? ((item.addresses)[0].type? item.addresses[0].type:'-'):'-'
                }
            });

            let html = "";
            dataView.forEach(item => {
                let id = item.resource_name.replace('people/', '');

                if(this.data.findIndex(i => i.resource_name == item.resource_name) > -1){
                    html += `<tr>
                            <td>
                                <input id="${id}" type="checkbox" class="select-item" value='' checked>
                                <input type="hidden" id="${id}_name" value="${item.name}">
                                <input type="hidden" id="${id}_email" value="${item.email}">
                                <input type="hidden" id="${id}_phone_number" value="${item.phone_number}">
                                <input type="hidden" id="${id}_resource" value="${item.resource_name}">
                                <input type="hidden" id="${id}_etag" value="${item.etag}">
                                <input type="hidden" id="${id}_address" value="${item.address}">
                                <input type="hidden" id="${id}_genders" value="${item.genders}">
                                <input type="hidden" id="${id}_city_name" value="${item.city_name}">
                                <input type="hidden" id="${id}_region_name" value="${item.region_name}">
                                <input type="hidden" id="${id}_postal_code" value="${item.postal_code}">
                                <input type="hidden" id="${id}_type_address" value="${item.type_address}">
                            </td>
                            <td>${item.name}</td>
                            <td>${item.email}</td>
                            <td>${item.phone_number}</td>
                        </tr>`;
                } else {
                    html += `<tr>
                            <td>
                                <input id="${id}" type="checkbox" class="select-item" value=''>
                                <input type="hidden" id="${id}_name" value="${item.name}">
                                <input type="hidden" id="${id}_email" value="${item.email}">
                                <input type="hidden" id="${id}_phone_number" value="${item.phone_number}">
                                <input type="hidden" id="${id}_resource" value="${item.resource_name}">
                                <input type="hidden" id="${id}_etag" value="${item.etag}">
                                <input type="hidden" id="${id}_address" value="${item.address}">
                                <input type="hidden" id="${id}_genders" value="${item.genders}">
                                <input type="hidden" id="${id}_city_name" value="${item.city_name}">
                                <input type="hidden" id="${id}_region_name" value="${item.region_name}">
                                <input type="hidden" id="${id}_postal_code" value="${item.postal_code}">
                                <input type="hidden" id="${id}_type_address" value="${item.type_address}">
                            </td>
                            <td>${item.name}</td>
                            <td>${item.email}</td>
                            <td>${item.phone_number}</td>
                        </tr>`;
                }

            });
            $('#data-result').html('');
            $('#data-result').append(html);
        },

        async _checkData(event, allData, checkbox){
            const checkAllBox = checkbox.filter(item => {
                return item.classList.contains('all');
            })
            const checkedBox = checkbox.filter(item => {
                return item.checked == true && !item.classList.contains('all');
            });

            const unCheckedBox = checkbox.filter(item => {
                return item.checked == false && !item.classList.contains('all');
            });

            if(event.target.classList.contains('all')){
                if(event.target.checked){
                    let plugin = @json($activePlugin);
                    let page = localStorage.getItem('page');
                    let maxQuota = 0;
                    let config = JSON.parse(localStorage.getItem('config'));

                    if(plugin.payment_type.toLowerCase() == 'quota'){
                        maxQuota = (parseInt(plugin.quota_available) - parseInt(plugin.quota_used)) < 1000 ? parseInt(plugin.quota_available) - parseInt(plugin.quota_used) : 1000 ;
                    } else {
                        maxQuota = 1000;
                    }

                    let response = await this.fetchData(config, page,maxQuota,'next');
                    let responseData = await response.data.list;
                    let tmpData = responseData.map(item => {
                        return {
                            id: item.resourceName.replace('people/', ''),
                            name : (item.names)?item.names[0].displayName:'-',
                            email: (item.emailAddresses)?item.emailAddresses[0].value:'-',
                            phone_number: (item.phoneNumbers)?item.phoneNumbers[0].value:'-',
                            resource_name: item.resourceName,
                            address: (item.addresses)?item.addresses[0].streetAddress:'-',
                            etag: item.etag,
                            genders: (item.genders)? item.genders[0].value:'-',
                            city_name: (item.addresses)? ((item.addresses)[0].city? item.addresses[0].city:'-'):'-',
                            region_name: (item.addresses)? ((item.addresses)[0].region? item.addresses[0].region:'-'):'-',
                            postal_code: (item.addresses)? ((item.addresses)[0].postalCode? item.addresses[0].postalCode:'-'):'-',
                            type_address: (item.addresses)? ((item.addresses)[0].type? item.addresses[0].type:'-'):'-'
                        }
                    });

                    this.data = [...tmpData];
                    checkbox.forEach(item => {
                        item.checked = true;
                    });

                } else {
                    this.data = [];
                    checkbox.forEach(item => {
                        item.checked = false;
                    });
                }

            } else {
                const id = event.target.getAttribute('id');
                const data = {
                    "id": id,
                    "name": document.querySelector('#'+id+"_name").value,
                    "email": document.querySelector('#'+id+"_email").value,
                    "phone_number": document.querySelector('#'+id+"_phone_number").value,
                    "etag": document.querySelector('#'+id+"_etag").value,
                    "resource_name": document.querySelector('#'+id+"_resource").value,
                    "address": document.querySelector('#'+id+"_address").value ,
                    "genders": document.querySelector('#'+id+"_genders").value ,
                    "city_name": document.querySelector('#'+id+"_city_name").value ,
                    "region_name": document.querySelector('#'+id+"_region_name").value ,
                    "postal_code": document.querySelector('#'+id+"_postal_code").value ,
                    "type_address": document.querySelector('#'+id+"_type_address").value

                }

                if(event.target.checked){
                    this.data.push(data);
                    (checkedBox.length == checkbox.length - 1) ? checkAllBox[0].checked = true:'';
                } else {
                    const tmpData = this.data.filter(item => {
                        return item.id != id;
                    });
                    this.data = [...tmpData];
                    (unCheckedBox.length < checkbox.length - 1) ? checkAllBox[0].checked = false:'';
                }
            }
        },

        nextPage(page){
            let tmpConfig = [...JSON.parse(localStorage.getItem('config'))];
            let nextToken = localStorage.getItem('next');
            let newPage = parseInt(page) + 1;
            let perPage = $('#dataPerPage').val();

            let checkPage = tmpConfig.filter(item => {
                return item.page == newPage;
            });

            if(checkPage.length == 0){
                let nextConfig = {
                    "page": newPage,
                    "next": nextToken,
                    "prev": tmpConfig[page].next
                }
                tmpConfig.push(nextConfig);
                localStorage.setItem('config', JSON.stringify(tmpConfig));
            }

            localStorage.setItem('page', newPage);
            let newConfig = JSON.parse(localStorage.getItem('config'));

            GetContact.start(newConfig, newPage, perPage,'next');
        },

        prevPage(page){
            let prevConfig = JSON.parse(localStorage.getItem('config'));
            let perPage = $('#dataPerPage').val();
            localStorage.setItem('page', parseInt(page) -1);
            GetContact.start(prevConfig, page, perPage,'prev');
        },
    }

    $(document).ready(() => {

        @if(!is_null($activePlugin->email) || !is_null($activePlugin->client_id) || !is_null($activePlugin->client_secret) || !is_null($activePlugin->client_token))
            $('.modal-content').css('width', '120%');
            let perPage = $('#dataPerPage').val();
            let config = [
                {
                    "page": 0,
                    "next": -1,
                    "prev": -1
                }
            ];
            GetContact.start(config, 0, perPage,'next');
        @endif

        $('#btn-save').on('click', function(){
            let data = new FormData();
            data.append('contacts', JSON.stringify(GetContact.data));

            ajaxTransfer("{{route('merchant.toko.customer.contact-sync-save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        });

        $('#dataPerPage').on('change', function(){
            let perPage = $(this).val();
            let config = [
                {
                    "page": 0,
                    "next": -1,
                    "prev": -1
                }
            ];

            GetContact.start(config, 0, perPage,'next');
        });

    });

    $("#modal-target").on("hidden.bs.modal", function () {
        localStorage.removeItem("config");
        localStorage.removeItem("next");
        localStorage.removeItem("page");
        localStorage.removeItem("pageSize");
        localStorage.removeItem('total_items');
        reload(100);
    });
    var result=''
</script>

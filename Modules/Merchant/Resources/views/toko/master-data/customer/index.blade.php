@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
@php
header('Access-Control-Allow-Origin: *');
@endphp
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.customer.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kelola daftar pelangganmu dan ketahui siapa saja konsumenmu</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')

        @if($activePlugin)
            @if($activePlugin->status == 0)
                @if(strtolower($activePlugin->payment_type) != 'quota')
                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                    <strong>Perhatian!</strong>
                    Masa berlanggan plugin anda sudah habis, silahkan lakukan pembelian plugin untuk dapat menggunakan fitur !
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @else
                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                    <strong>Perhatian!</strong>
                    Kuota plugin yang anda gunakan sudah mencapai batas, silahkan lakukan pembelan plugin untuk dapat menggunakan fitur !
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            @endif
        @endif
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a onclick="activeMenu('master-data','master-data-produk')" class="nav-link active" href="{{route('merchant.toko.customer.index')}}">Pelanggan</a>
            </li>
            <li class="nav-item">
                <a onclick="activeMenu('master-data','master-data-kategori')" class="nav-link" href="{{route('merchant.toko.cust-level.index')}}">Kategori Pelanggan</a>
            </li>
        </ul>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row mb-4 d-flex justify-content-between">
                    {!! $add !!}
                    @if($activePlugin)
                        @if($activePlugin->status == 1)
                        <div class="col-lg-auto col-md-12">
                            <button onclick="loadModalFullScreen(this)" target="{{route('merchant.toko.customer.contact-sync-form', ['id' => 1])}}" class="btn btn-sm btn-bg__orange py-2 px-4 btn-rounded text-white">
                                <i class="fas fa-sync mr-2"></i> Sinkronisasi Google Contact
                            </button>
                        </div>
                        @endif
                    @endif
                </div>

                <div id="output-sale-order">
                    @include('merchant::toko.master-data.customer.list')
                </div>
            </div>
        </div>

    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" onsubmit="return false" class="px-30">
            <div class="row justify-content-end align-items-center">
                <div class="col-md-12 mb-3">
                    <label class="font-weight-bold">Kategori :</label>
                    <select name="sc_customer_level_id" id="sc_customer_level_id" class="form-control">
                        <option value="-1">Semua Kategori</option>
                        @foreach($level as $key =>$item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>

                @if(count(get_cabang()) > 1))
                <div class="col-md-12 mb-3">
                    <label class="font-weight-bold">Cabang :</label>
                    <select name="branch[]" id="branch" class="form-control" multiple="multiple" required>
                        <option selected value="-1">Semua Cabang</option>
                        @foreach(get_cabang() as $key=>$item)
                            <option value="{{$item->id}}">{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>
                @endif

                <div class="col-md-12">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                    <input type="hidden" name="key" value="{{$key_val}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>
                </div>

            </div>

        </form>

    </div>

@endsection

@section('js')
    <script>
        $('#sc_customer_level_id').select2();
        $('#branch').select2();
        $("#branch").on("select2:select", function(e){
            if(e.params.data.id == "-1"){
                $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
            } else {
                $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
            }
        });

        function exportData() {
            var info = $('#table-data-customer_info').html().split('dari');
            if (info.length < 2) {
                return false;
            }

            var dataCount = parseInt(info[1].replace('entri', '').replace(',', '').replace(' ', ''));
            var seconds = parseFloat(dataCount / 66).toFixed(3);
            var estimation = '';

            if (seconds < 60) {
                estimation = seconds + ' detik';
            } else {
                var minute = parseInt(seconds / 60);
                seconds = seconds % 60;
                estimation = minute + ' menit ' + seconds + ' detik';
            }

            modalConfirm('Export Data', 'Proses export data sejumlah ' + dataCount + ' data membutuhkan estimasi waktu ' + estimation + '. Lanjutkan proses export data?', function () {
                var data = getFormData('form-filter-sale-order');
                const search = document.querySelector('#table-data-customer_filter label input[type="search"]');

                if(search.value) {
                    data.append('search', search.value);
                }

                ajaxTransfer("{{route('merchant.toko.customer.export-data')}}", data, '#modal-output');
            });

        }

        $(document).ready(function () {
            $('#form-filter-sale-order').submit(function () {
                var data = getFormData('form-filter-sale-order');

                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];

                // cek jika pilih semua cabang
                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });
                console.log(merchantIds);
                data.append('md_merchant_id',JSON.stringify(merchantIds));
                ajaxTransfer("{{route('merchant.toko.customer.reload-data')}}", data,'#output-sale-order');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        });
    </script>
@endsection

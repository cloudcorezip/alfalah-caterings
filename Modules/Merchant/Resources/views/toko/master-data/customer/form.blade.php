<style>
    /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-customer" class='form-horizontal form-konten' backdrop="">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Kode</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$data->code}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Nama Pelanggan</label>
                <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{$data->name}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Email</label>
                <input type="email" class="form-control form-control-sm" name="email" value="{{$data->email}}" placeholder="Email">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">No Telp</label>
                <input type="number" class="form-control form-control-sm" name="phone_number" value="{{$data->phone_number}}" placeholder="Contoh : 081228612373" required>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Alamat</label>
                <textarea name="address" class="form-control" placeholder="Alamat">{{$data->address}}</textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Provinsi</label>
                <select name="region" class="form-control form-control-sm" id="region">
                    <option></option>
                    @foreach($provinces as $key => $item)
                        <option value="{{$item->id}}" @if(strtolower($data->region_name) == strtolower($item->name)) selected @endif>{{$item->name}}</option>
                    @endforeach

                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Kota</label>
                <select name="city" class="form-control form-control-sm" id="city">
                    <option></option>
                    @if(!is_null($data->city_name))
                    <option value="{{$data->city_name}}" selected>{{$data->city_name}}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Tipe Alamat</label>
                <select name="type_address" class="form-control form-control-sm" id="type_address">
                    <option></option>
                    <option value="home" @if(strtolower($data->type_address == 'home'))  selected @endif>Rumah</option>
                    <option value="work" @if(strtolower($data->type_address == 'work'))  selected @endif>Kantor</option>
                    <option value="other" @if(strtolower($data->type_address == 'other'))  selected @endif>Lainnya</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Kode Pos</label>
                <input type="text" class="form-control form-control-sm" name="postal_code" placeholder="Kode Pos" value="{{$data->postal_code}}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Jenis Kelamin</label>
                <select name="gender" class="form-control form-control-sm" id="gender">
                    <option></option>
                    <option value="male" @if(strtolower($data->gender) == 'male') selected @endif>Laki - Laki</option>
                    <option value="female" @if(strtolower($data->gender) == 'female') selected @endif>Perempuan</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Kategori</label>
                <select name="sc_customer_level_id" class="form-control form-control-sm" id="sc_customer_level">
                    <option></option>
                    @foreach($level as $item)
                        <option value="{{$item->id}}" @if($item->id==$data->sc_customer_level_id) selected @endif>{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        {{---@if(is_null($data->id) && $activePlugin)
            @if($activePlugin->status == 1)
                @if(!is_null($activePlugin->email) || !is_null($activePlugin->client_id) || !is_null($activePlugin->client_secret) || !is_null($activePlugin->client_token))
                <div class="col-md-12 mb-3">
                    <div class="form-check form-check-inline d-flex align-items-center">
                        <input class="form-check-input" type="checkbox" value="1" id="is_google_contact">
                        <label class="form-check-label" for="flexCheckChecked">
                            Sinkronkan dengan google contact
                        </label>
                    </div>
                </div>
                @endif
            @endif
        @endif---}}
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='is_sale' value='{{$isFromSale }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $("#sc_customer_level").select2({
            placeholder: "--- Pilih Kategori ---"
        });
        $("#region").select2({
            placeholder: "--- Pilih Provinsi ---"
        });
        $("#city").select2({
            placeholder:  "--- Pilih Kota ---"
        });
        $("#gender").select2({
            placeholder: "--- Pilih Jenis Kelamin ---"
        });
        $("#type_address").select2({
            placeholder: "--- Pilih Tipe Alamat ---"
        });
        $('#form-customer').submit(function () {
            var data = getFormData('form-customer');
            data.append('region_name', $('#region option:selected').text());
            data.append('city_name', $('#city option:selected').text());

            if($('#is_google_contact').is(':checked')){
                AddContact.start(data);
            } else {
                ajaxTransfer("{{route('merchant.toko.customer.save')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            }

        })
    });


    $('#region').on('change', function(e){
        $('#city option').remove();
        $.ajax({
            url: '{{route("merchant.toko.customer.get-city")}}',
            method: 'POST',
            data: {'_token': "{{csrf_token()}}", 'id': $(this).val()},
            success:function(response){
                let html = '<option></option>';
                response.map(item => {
                    html += '<option value="'+item.id+'">'+item.name+'</option>';
                });
                $('#city').append(html);
            }
        });
    });

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        let id = "{{$data->id}}";
        let str = (!id)? "menambah":"mengedit";

        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk ${str} pelanggan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    const AddContact = {
        async start(formData){
            let plugin = @json($activePlugin);
            if(plugin.payment_type.toLowerCase() == 'quota'){
                if((parseInt(plugin.quota_used) + 1) > plugin.quota_available){
                    toastForSaveData('Kuota anda tidak cukup, data gagal disimpan !','warning',true,'');
                    return;
                }
            }

            const response = await this._addSync(formData);
            const data = await response.data;
            const code = await response.code;
            if(code !== 200){
                toastForSaveData('Terjadi kesalahan, coba beberapa saat lagi !','warning',true,'');
                closeModal(3000);
                return;
            }
            this._addData(formData, data);
        },
        async _addSync(formData){
            let dataSend = {
                'merchant_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_merchant_id}}'),
                'plugin_id':  parseInt('{{($activePlugin==false)?0:$activePlugin->md_plugin_id}}'),
                'name': formData.get('name'),
                'email': formData.get('email'),
                'phone_number':formData.get('phone_number'),
                'street_address': formData.get('address'),
                'region_name': formData.get('region_name'),
                'city_name': formData.get('city_name'),
                'postal_code': formData.get('postal_code'),
                'gender': formData.get('gender'),
                'type_address': formData.get('type_address')
            };

            let formBody = [];
            for (let property in dataSend) {
                let encodedKey = encodeURIComponent(property);
                let encodedValue = encodeURIComponent(dataSend[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");

            // tes get google contact in api.dev
            return $.ajax({
                method: 'POST',
                url:'{{$apiUrl}}/api/google/contact/add',
                async: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "senna-auth": "{{get_user_token()}}",
                    'Access-Control-Allow-Origin': '*',
                },
                data: formBody
            })
        },

        _addData(formData, data){
            formData.append('is_google_contact', 1);
            formData.append('etag', data.etag);
            formData.append('resource_name', data.resourceName);
            ajaxTransfer("{{route('merchant.toko.customer.save')}}", formData, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        }
    }

    $("#modal-target").on("hidden.bs.modal", function () {
        reload(100);
    });


</script>

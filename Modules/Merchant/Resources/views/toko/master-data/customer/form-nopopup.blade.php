@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.customer.index')}}">Pelanggan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="#">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kelola daftar pelangganmu dan tambah atau ubah data disini!</span>
            </div>
        </div>
    </div>
    
    <div class="container-fluid">
        <form onsubmit="return false;" id="form-customer" class="form-horizontal form-konten" backdrop="">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <button
                                class="d-flex justify-content-between align-items-center pl-0"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse-customer-profil"
                                aria-expanded="false"
                                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                            >
                                <h4 class="font-weight-bold mb-3">Profil Pelanggan</h4>
                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                            </button>
                        </div>
                    </div>
                        
                    <div id="collapse-customer-profil" class="collapse show">
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                         <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Nama <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{$data->name}}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Kode Pelanggan <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$code}}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Kategori Pelanggan</label>
                                    <div class="col-sm-8">
                                        <select name="sc_customer_level_id" class="form-control form-control-sm" id="sc_customer_level">
                                            <option></option>
                                            @foreach($level as $item)
                                                <option value="{{$item->id}}" @if($item->id==$data->sc_customer_level_id) selected @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Tanggal Lahir</label>
                                    <div class="col-sm-8">
                                        <input
                                            id="dob" 
                                            type="date" 
                                            class="form-control form-control-sm" 
                                            name="date_of_birth"
                                            placeholder="Tanggal Lahir"
                                            @if(!is_null($data->date_of_birth))
                                            value="{{\Carbon\Carbon::parse($data->date_of_birth)->format('Y-m-d')}}"
                                            @endif
                                        >
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">No. Telepon <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input 
                                            type="number" 
                                            class="form-control form-control-sm" 
                                            name="phone_number" 
                                            value="{{$data->phone_number}}" 
                                            placeholder="Contoh : 081228612373" 
                                            required
                                        >
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Email</label>
                                    <div class="col-sm-8">
                                        <input 
                                            type="email" 
                                            class="form-control form-control-sm" 
                                            name="email" 
                                            value="{{$data->email}}" 
                                            placeholder="Email" 
                                        >
                                    </div>  
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Jenis Kelamin</label>
                                    <div class="col-sm-8">
                                        <div class="form-check form-check-inline">
                                            <input
                                                class="form-check-input type"
                                                type="radio"
                                                value="male"
                                                name="gender"
                                                @if(strtolower($data->gender) == "male")
                                                checked
                                                @endif

                                                @if(is_null($data->id))
                                                checked
                                                @endif
                                            >
                                            <label class="form-check-label">Laki-Laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input
                                                class="form-check-input type"
                                                type="radio"
                                                value="female"
                                                name="gender"
                                                @if(strtolower($data->gender) == "female")
                                                checked
                                                @endif
                                            >
                                            <label class="form-check-label">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                         </div>
                    </div>
                </div>
            </div>

            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <button
                                class="d-flex justify-content-between align-items-center pl-0"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse-additional-info"
                                aria-expanded="false"
                                style="background:transparent!important;border:none!important;outline:none!important;width:100%;"
                            >
                                <h4 class="font-weight-bold mb-3">Info Tambahan</h4>
                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                            </button>
                        </div>
                    </div>

                    <div id="collapse-additional-info" class="collapse show">
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">NIK</label>
                                    <div class="col-sm-8">
                                        <input 
                                            type="number" 
                                            class="form-control form-control-sm" 
                                            name="identity_card_number" 
                                            placeholder="No Kartu Identitas" 
                                            value="{{$data->identity_card_number}}"
                                        >
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Agama</label>
                                    <div class="col-sm-8">
                                        <select name="religion" class="form-control form-control-sm" id="religion">
                                            <option value="">--- Pilih Agama ---</option>
                                            @foreach($religions as $key => $item)
                                            <option value="{{$key}}" @if($key == $data->religion) selected @endif>{{$item}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Pekerjaan</label>
                                    <div class="col-sm-8">
                                        <select name="md_job_id" class="form-control form-control-sm" id="job">
                                            <option value="">--- Pilih Pekerjaan ---</option>
                                            @foreach($jobs as $key => $item)
                                            <option value="{{$item->id}}" @if($item->id == $data->md_job_id) selected @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row d-flex align-items-center">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Usia</label>
                                    <div class="col-sm-8">
                                        <input id="age" type="number" class="form-control form-control-sm" name="age" value="{{(is_null($data->age) ? 0:$data->age)}}" placeholder="Usia" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <button
                                class="d-flex justify-content-between align-items-center pl-0"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse-address-info"
                                aria-expanded="false"
                                style="background:transparent!important;border:none!important;outline:none!important;width:100%;"
                            >
                                <h4 class="font-weight-bold mb-3">Alamat Pelanggan</h4>
                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                            </button>
                        </div>
                    </div>
                    <div id="collapse-address-info" class="collapse show">
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Tipe Alamat</label>
                                    <div class="col-sm-8">
                                        <select name="type_address" class="form-control form-control-sm" id="type_address">
                                            <option value="">--- Pilih Tipe Alamat ---</option>
                                            <option value="home" @if(strtolower($data->type_address == 'home'))  selected @endif>Rumah</option>
                                            <option value="work" @if(strtolower($data->type_address == 'work'))  selected @endif>Kantor</option>
                                            <option value="other" @if(strtolower($data->type_address == 'other'))  selected @endif>Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Alamat</label>
                                    <div class="col-sm-8">
                                        <textarea name="address" class="form-control" placeholder="Alamat">{{$data->address}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Provinsi</label>
                                    <div class="col-sm-8">
                                        <select name="region" class="form-control form-control-sm" id="region">
                                            <option></option>
                                            @foreach($provinces as $key => $item)
                                                <option value="{{$item->id}}" @if(strtolower($data->region_name) == strtolower($item->name)) selected @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Kota</label>
                                    <div class="col-sm-8">
                                        <select name="city" class="form-control form-control-sm" id="city">
                                            <option></option>
                                            @if(!is_null($data->city_name))
                                            <option value="{{$data->city_name}}" selected>{{$data->city_name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Kode Pos</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm" name="postal_code" placeholder="Kode Pos" value="{{$data->postal_code}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 mb-4 text-right">
                    <input type='hidden' name='id' value='{{$data->id }}'>
                    <input type='hidden' name='is_sale' value='1'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <a href="{{route('merchant.toko.customer.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                    <button class="btn btn-success" id="btn-save"><i class="fa fa-save mr-2"></i> Simpan</button>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        $("#sc_customer_level").select2({
            placeholder: "--- Pilih Kategori ---"
        });
        $("#region").select2({
            placeholder: "--- Pilih Provinsi ---"
        });
        $("#city").select2({
            placeholder:  "--- Pilih Kota ---"
        });
        $("#gender").select2();
        $("#job").select2();
        $("#media_info").select2();
        $("#religion").select2();
        $("#marital_status").select2();
        $("#type_address").select2();

        $("#dob").on("change", function(){
            $.ajax({
                type: 'POST',
                data: {
                    date: $("#dob").val(),
                    _token:"{{ csrf_token() }}"
                },
                url: "{{route('merchant.toko.customer.calculate-age')}}",
                success:function(response){
                    $("#age").val(response);
                }
            });
        });

        $('#form-customer').submit(function () {
            var data = getFormData('form-customer');
            data.append('region_name', $('#region option:selected').text());
            data.append('city_name', $('#city option:selected').text());

            if($('#is_google_contact').is(':checked')){
                AddContact.start(data);
            } else {
                ajaxTransfer("{{route('merchant.toko.customer.save')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            }

        })
    });


    $('#region').on('change', function(e){
        $('#city option').remove();
        $.ajax({
            url: '{{route("merchant.toko.customer.get-city")}}',
            method: 'POST',
            data: {'_token': "{{csrf_token()}}", 'id': $(this).val()},
            success:function(response){
                let html = '<option></option>';
                response.map(item => {
                    html += '<option value="'+item.id+'">'+item.name+'</option>';
                });
                $('#city').append(html);
            }
        });
    });

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        let id = "{{$data->id}}";
        let str = (!id)? "menambah":"mengedit";

        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk ${str} pelanggan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    const AddContact = {
        async start(formData){
            let plugin = @json($activePlugin);
            if(plugin.payment_type.toLowerCase() == 'quota'){
                if((parseInt(plugin.quota_used) + 1) > plugin.quota_available){
                    toastForSaveData('Kuota anda tidak cukup, data gagal disimpan !','warning',true,'');
                    return;
                }
            }

            const response = await this._addSync(formData);
            const data = await response.data;
            const code = await response.code;
            if(code !== 200){
                toastForSaveData('Terjadi kesalahan, coba beberapa saat lagi !','warning',true,'');
                closeModal(3000);
                return;
            }
            this._addData(formData, data);
        },
        async _addSync(formData){
            let dataSend = {
                'merchant_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_merchant_id}}'),
                'plugin_id':  parseInt('{{($activePlugin==false)?0:$activePlugin->md_plugin_id}}'),
                'name': formData.get('name'),
                'email': formData.get('email'),
                'phone_number':formData.get('phone_number'),
                'street_address': formData.get('address'),
                'region_name': formData.get('region_name'),
                'city_name': formData.get('city_name'),
                'postal_code': formData.get('postal_code'),
                'gender': formData.get('gender'),
                'type_address': formData.get('type_address')
            };

            let formBody = [];
            for (let property in dataSend) {
                let encodedKey = encodeURIComponent(property);
                let encodedValue = encodeURIComponent(dataSend[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");

            // tes get google contact in api.dev
            return $.ajax({
                method: 'POST',
                url:'{{$apiUrl}}/api/google/contact/add',
                async: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "senna-auth": "{{get_user_token()}}",
                    'Access-Control-Allow-Origin': '*',
                },
                data: formBody
            })
        },

        _addData(formData, data){
            formData.append('is_google_contact', 1);
            formData.append('etag', data.etag);
            formData.append('resource_name', data.resourceName);
            ajaxTransfer("{{route('merchant.toko.customer.save')}}", formData, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        }
    }

    $("#modal-target").on("hidden.bs.modal", function () {
        reload(100);
    });


</script>
@endsection

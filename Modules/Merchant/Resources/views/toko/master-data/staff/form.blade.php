<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nama Karyawan</label>
        <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{(!is_null($data->id))?$data->getUser->fullname:''}}" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Email</label>
        <input type="email" class="form-control form-control-sm" name="email" value="{{$data->email}}" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Password</label>
        <input type="password" class="form-control form-control-sm" name="password" value="" {{is_null($data->id)?"required":''}}>
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Hak Akses</label>
        <select name="md_role_id" class="form-control form-control-sm" id="md_role_id">
            @foreach($role as $key => $item)
                <option value="{{$item->id}}"
                        @if(!is_null($data->id))
                            @if($item->id==$data->getUser->md_role_id)
                selected
                        @endif
                    @endif>{{$item->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.staff.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    });

    $(document).ready(function(){
        $("#md_role_id").select2();
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Karyawan</h4>
                                        <span class="span-text">Untuk menambah Karyawan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

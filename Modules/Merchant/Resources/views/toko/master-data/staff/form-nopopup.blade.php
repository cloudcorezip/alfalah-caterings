@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Human Resources</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.staff.index')}}">Karyawan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.staff.add')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Di halaman ini, kamu dapat menambah data karyawan</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4 rounded">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row py-4">
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="code">Kode</label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$data->code}}">
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Hak Akses <span class="text-danger">*</span></label>
                                <select name="md_role_id" class="form-control form-control-sm" id="md_role_id" required>
                                    @foreach($role as $key => $item)
                                        <option value="{{$item->id}}"
                                                @if(!is_null($data->id))
                                                    @if($item->id==$data->getUser->md_role_id)
                                        selected
                                                @endif
                                            @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Nama Karyawan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{(!is_null($data->id))?$data->getUser->fullname:''}}" required>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="marital_status">Status Pernikahan</label>
                                <select name="marital_status" class="form-control form-control-sm" id="marital_status">
                                    <option value="">--- Pilih Status Pernikahan ---</option>
                                    @foreach($maritalStatus as $key => $item)
                                    <option value="{{$key}}" @if($key == $data->marital_status) selected @endif>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="birth_place">Tempat Lahir</label>
                                <input type="text" name="birth_place" class="form-control form-control-sm" placeholder="Tempat Lahir" value="{{(!is_null($data->id))?$data->getUser->birth_place:''}}">
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="religion">Agama</label>
                                <select name="religion" class="form-control form-control-sm" id="religion">
                                    <option value="">--- Pilih Agama ---</option>
                                    @foreach($religion as $key => $item)
                                        <option value="{{$key}}" @if($data->religion == $key) selected @endif>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Tanggal Lahir</label>
                                <input type="date" class="form-control form-control-sm" name="birth_date" value="{{(!is_null($data->id))?$data->getUser->birth_date:''}}" placeholder="Tanggal Lahir" required>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="gender">Jenis Kelamin</label>
                                <select name="gender" class="form-control form-control-sm" id="gender">
                                    <option
                                        value="0"
                                        @if(!is_null($data->id))
                                            @if($data->getUser->gender == 0)
                                            selected
                                            @endif
                                        @endif
                                    >
                                        Laki - Laki
                                    </option>
                                    <option
                                        value="1"
                                        @if(!is_null($data->id))
                                            @if($data->getUser->gender == 1)
                                            selected
                                            @endif
                                        @endif

                                    >
                                        Perempuan
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="phone_number">No Telephone</label>
                                <input type="number" class="form-control form-control-sm" name="phone_number" value="{{!is_null($data->id)?$data->getUser->phone_number:''}}">
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Tanggal Bergabung</label>
                                <input type="date" class="form-control form-control-sm" name="join_date" value="{{$data->join_date}}" placeholder="Tanggal Bergabung" required>
                            </div>
                        </div>

                        <div class="col-md-12 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="address">Alamat</label>
                                <textarea name="address" class="form-control" placeholder="Alamat">{{$data->address}}</textarea>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="blood_type">Golongan Darah</label>
                                <select name="blood_type" class="form-control form-control-sm" id="blood_type">
                                    <option value="">--- Pilih Golongan Darah ---</option>
                                    @foreach($bloodType as $item)
                                    <option value="{{$item}}" @if($data->blood_type == $item) selected @endif>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Grade Karyawan <span class="text-danger">*</span></label>
                                <select name="merchant_grade_id" class="form-control form-control-sm" id="merchant_grade_id" required>
                                    @foreach($gradeOption as $key => $item)
                                        <option value="{{$item->id}}"
                                                @if(!is_null($data->id))
                                                @if($item->id==$data->merchant_grade_id)
                                                selected
                                            @endif
                                            @endif>{{$item->nama_grade}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Jabatan </label>
                                <select name="job_position_id" class="form-control form-control-sm selectalldata" >
                                    @foreach($jobOption as $key => $item)
                                        <option value="{{$item->id}}"
                                                @if(!is_null($data->id))
                                                @if($item->id==$data->job_position_id)
                                                selected
                                            @endif
                                            @endif>{{$item->jabatan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Departemen</label>
                                <select id="departement_id" name="departement_id" class="form-control form-control-sm selectalldata" >
                                    <option></option>
                                    @if(!is_null($data->departement_id))
                                    <option selected value="{{$data->departement_id}}">{{$data->department_name}}</option>
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="blood_group">Pendidikan Terakhir</label>
                                <select name="last_education" class="form-control form-control-sm" id="last_education">
                                    <option value="">--- Pilih Pendidikan Terakhir ---</option>
                                    @foreach($education as $key => $item)
                                    <option value="{{$key}}" @if($data->last_education == $key) selected @endif>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Cabang <span class="text-danger">*</span></label>
                                <select name="md_merchant_id" class="form-control form-control-sm" id="md_merchant_id" required>
                                    @foreach(get_cabang() as $key => $item)
                                        <option value="{{$item->id}}"
                                                @if(!is_null($data->id))
                                                @if($item->id==$data->md_merchant_id)
                                                selected
                                            @endif
                                            @endif>{{$item->nama_cabang}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <label class="font-weight-bold" for="is_active">Status</label>
                            <br>
                            <div class="form-group py-3 px-3" style="border: 1px solid #DADADA; background-color:#F8F8F8;border-radius:12px;">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="is_active" value="1" @if($data->is_active == 1 || is_null($data->id)) checked @endif>
                                    <label class="form-check-label">Aktif</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="is_active" value="0" @if($data->is_active == 0) checked @endif>
                                    <label class="form-check-label">Tidak Aktif</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Foto</label>
                                <input type="file" name="image" class="form-control" accept="image/*">
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <label class="font-weight-bold" for="is_active">Apakah ingin dibuat username dan password ?</label>
                            <br>
                            <div class="form-group py-3 px-3 d-flex justify-content-between" style="border: 1px dashed #DADADA; background-color:#F8F8F8;border-radius:12px;width:40%;">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input is_create_email" type="radio" name="is_create_email" value="1" @if($data->is_create_email == 1 || is_null($data->id)) checked @endif>
                                    <label class="form-check-label">Ya</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input is_create_email" type="radio" name="is_create_email" value="0" @if($data->is_create_email === 0) checked @endif>
                                    <label class="form-check-label">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad" id="mail_pass" @if($data->is_create_email === 0) style="display:none;" @endif>
                            <div class="form-group">
                                <label class="font-weight-bold">Email</label>
                                <input
                                    id="email"
                                    type="email"
                                    class="form-control form-control-sm"
                                    placeholder="Email"
                                    name="email"
                                    value="{{$data->email}}"
                                    @if(is_null($data->id) || $data->is_create_email === 1)
                                        required
                                    @endif
                                >
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Password</label>
                                <input
                                    id="password"
                                    type="password"
                                    class="form-control form-control-sm"
                                    placeholder="Password"
                                    name="password"
                                    value=""
                                    @if(is_null($data->id))
                                        required
                                    @elseif(is_null($data->getUser->password) && $data->is_create_email == 1)
                                        required
                                    @endif
                                >
                            </div>
                        </div>

                    </div>


                    <div class="modal-footer">
                        <a href="{{route('merchant.toko.staff.index')}}" class="btn btn-light" data-dismiss="modal">Kembali</a>
                        <button class="btn btn-success">Simpan</button>
                    </div>
                    <input type='hidden' name='id' value='{{$data->id }}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('js')
<script>
$(document).ready(function () {
    let data = @json($data);
    $("#md_role_id").select2();
    $("#md_merchant_id").select2();
    $("#merchant_grade_id").select2();
    $("#marital_status").select2();
    $("#religion").select2();
    $("#last_education").select2();
    $("#gender").select2();
    $("#blood_type").select2();
    $(".selectalldata").select2();

    $("#department_id").select2({
        placeholder: '--- Pilih Departemen ---'
    });


    $(".is_create_email").on('change', function(){
        let val = $(this).val();
        if(val == 1){
            $("#mail_pass").show();
            $("#email").prop('required', true);
            if(!data.id){
                $("#password").prop('required', true);
            }
        } else {
            $("#mail_pass").hide();
            $("#email").prop('required', false);
            $("#password").prop('required', false);
        }
    });

    function getDepartment()
    {
        $("#departement_id").select2({
            placeholder:'--- Pilih Departement ---',
            ajax: {
                type: "GET",
                url: "{{route('merchant.ajax.share-data.department-list')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }

    getDepartment();

    $('#departement_id').on('select2:open', () => {
        $(".select2-results:not(:has(a))").has('#select2-departement_id-results').append('<a id="add-department" onclick="loadModalFullScreen(this)" target="{{route("merchant.hr.departement.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Departement Baru</b></a>');
    });

    $(document).on('click','#add-department', function(e){
        $("#departement_id").select2('close');
    })

    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{route('merchant.toko.staff.save')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    })
});
</script>
@endsection

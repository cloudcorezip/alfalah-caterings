<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="form-group">
                <label class="font-weight-bold" for="exampleInputPassword1">Kode</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$data->code}}">
            </div>
            <div class="form-group">
                <label class="font-weight-bold" for="exampleInputPassword1">Nama Kategori</label>
                <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama Kategori" value="{{$data->name}}" required>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="form-group">
                <label class="font-weight-bold" for="exampleInputPassword1">Deskripsi</label>
                <textarea name="description" class="form-control" placeholder="Deskripsi">{{$data->description}}</textarea>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.cust-level.save')}}", data,function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Kategori Pelanggan</h4>
                                        <span class="span-text">Lengkapi data di bawah ini untuk menambah kategori pelanggan</span>
                                    </div>`);
    });


</script>

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    @if(is_null($data->id))
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Nama Usaha</label>
            <input type="text" class="form-control form-control-sm" name="branch_name" placeholder="Nama Usaha"  required>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Nama Owner</label>
            <input type="text" class="form-control form-control-sm" name="branch_owner" placeholder="Nama Owner"  required>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Email Cabang</label>
            <input type="email" class="form-control form-control-sm" name="email_branch" placeholder="Email"  required>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Password</label>
            <input type="password" class="form-control form-control-sm" name="password" placeholder="Password"  required>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Kategori Usaha</label>
            <select class="form-control font-control-sm" name="category_id" id="category_id" required>
                <option></option>
                @foreach($categories as $category)
                    <optgroup label="{{$category->name}}" style="font-weight: bold">
                        @foreach($category->getChild as $c)
                            <option value="{{$c->id}}">{{$c->name}}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Kode Cabang</label>
            <input type="text" class="form-control form-control-sm" name="branch_code" placeholder="Kode Cabang">
        </div>
    @else
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Nama Usaha</label>
            <input type="text" class="form-control form-control-sm" name="branch_name" placeholder="Nama Usaha" value="{{$data->getMerchant->name}}"  required>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Nama Owner</label>
            <input type="text" class="form-control form-control-sm" name="branch_owner" placeholder="Nama Owner" value="{{$data->getMerchant->getUser->fullname}}"  required>
        </div>

        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Email Cabang</label>
            <input type="email" class="form-control form-control-sm" name="email_branch" placeholder="Email" value="{{$data->getMerchant->email_merchant}}"  required>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Kategori Usaha</label>
            <select class="form-control font-control-sm" name="category_id" id="category_id" required>
                <option></option>
                @foreach($categories as $category)
                    <optgroup label="{{$category->name}}" style="font-weight: bold">
                        @foreach($category->getChild as $c)
                            <option value="{{$c->id}}" @if($data->getMerchant->md_business_category_id==$c->id) selected @endif>{{$c->name}}</option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1" class="font-weight-bold">Kode Cabang</label>
            <input type="text" class="form-control form-control-sm" name="branch_code" placeholder="Kode Cabang" value="{{$data->getMerchant->branch_code}}">
        </div>
    @endif

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>

    $(document).ready(function(){
        $('#category_id').select2({
            placeholder:"--- Pilih Kategori Usaha ---",
            allowClear:true
        });

    })

    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.branch.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    })
    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Cabang</h4>
                                        <span class="span-text">Untuk menambah cabang. Kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

</script>

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-category" class='form-horizontal form-konten' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nama Kategori</label>
        <input type="text" class="form-control" name="name" placeholder="Nama Kategori" value="{{$data->name}}" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Jenis Kategori</label>
        <select name="is_service" class="form-control form-control-sm" id="type-category">
            <option value="0" @if($data->is_service==0) selected @endif>Barang</option>
            <option value="1" @if($data->is_service==1) selected @endif>Jasa</option>
        </select>
    </div>
    <br>
    <br>
    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Deskripsi</label>
        <textarea name="description" class="form-control" placeholder="Deskripsi">{{$data->description}}</textarea>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-category').submit(function () {
            var data = getFormData('form-category');
            ajaxTransfer("{{route('merchant.toko.product-category.save')}}", data, function (response){
                var data = JSON.parse(response);
                    if(data.type=='danger' || data.type=='warning'){
                        otherMessage(data.type,data.message)
                    }else{
                        otherMessage(data.type,data.message)
                        closeModalAfterSuccess()
                        getCategory()
                    }
            });
        })
    })
    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('#type-category').select2();
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Kategori Produk</h4>
                                        <span class="span-text">Untuk menambah kategori produk, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.product-category.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Daftar kategori produk untuk mempermudah pengelompokkan produkmu</span>
            </div>
        </div>

    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row" style="margin-bottom: 10px">
                {!! $add !!}
                </div>
                <div id="output-discount-product">
                    @include('merchant::toko.master-data.category.list')
                </div>

            </div>
        </div>
        <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
            <div class="d-flex align-items-center">
                <i class="fas fa-filter mr-2 text-white"></i>
                <span class="text-white form-filter3-text-header">Filter</span>
            </div>
        </a>
        <div class="form-filter3" id="form-filter3">
            <div class="row">
                <div class="col-12">
                    <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center">
                            <i class="fas fa-filter mr-2"></i>
                            <span class="text-white form-filter3-text-header">Filter</span>
                        </div>
                        <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                            <i style="font-size:14px;" class="fas fa-times text-white"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row mb-3 px-30">
                <div class="col-12">
                    <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                    <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
                </div>
            </div>
            <hr>
            <form id="form-filter-category" class="px-30" onsubmit="return false">
                <div class="row d-flex align-items-center">
                    <div class="col-md-12 mt-3">
                        <label>Pilih Tipe Kategori:</label>
                        <select name="is_service" id="is_service" class="form-control form-control-sm">
                            <option value="-1">Semua Tipe</option>
                            <option value="0">Barang</option>
                            <option value="1">Jasa</option>
                        </select>
                    </div>
                    <div class="col-md-12 mt-3">
                        <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type="hidden" name="search_key" value="{{$searchKey}}">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection

@section('js')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.product-category.delete')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        }

        $(document).ready(function() {
           reloadDataTable(0);
           $('#is_service').select2()
           $('#form-filter-category').submit(function () {
                var data = getFormData('form-filter-category');
                ajaxTransfer("{{route('merchant.toko.category.reload-data')}}", data, '#output-discount-product');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        })

    </script>
@endsection

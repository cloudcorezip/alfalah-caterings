@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Human Resources</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.doctor.index')}}">Dokter</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Tambahkan data doktermu disini !</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4 rounded">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row py-4">
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode</label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{is_null($data->id)? $code:$data->code}}">
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Spesialis <span class="text-danger">*</span></label>
                                <select name="md_merchant_staff_category_id" class="form-control form-control-sm" id="md_merchant_staff_category_id" required>
                                    @foreach($category as $key => $item)
                                        <option></option>
                                        <option
                                            value="{{$item->id}}"
                                            @if($item->id==$data->md_merchant_staff_category_id)
                                                selected
                                            @endif
                                        >
                                            {{$item->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Dokter <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama Dokter" value="{{(!is_null($data->id))?$data->getUser->fullname:''}}" required>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Status Pernikahan</label>
                                <select name="marital_status" class="form-control form-control-sm" id="marital_status">
                                    <option value="">--- Pilih Status Pernikahan ---</option>
                                    @foreach($maritalStatus as $key => $item)
                                    <option value="{{$key}}" @if($key == $data->marital_status) selected @endif>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Tempat Lahir</label>
                                <input type="text" name="birth_place" class="form-control form-control-sm" placeholder="Tempat Lahir" value="{{(!is_null($data->id))?$data->getUser->birth_place:''}}">
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Agama</label>
                                <select name="religion" class="form-control form-control-sm" id="religion">
                                    <option value="">--- Pilih Agama ---</option>
                                    @foreach($religion as $key => $item)
                                        <option value="{{$key}}" @if($data->religion == $key) selected @endif>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Jenis Kelamin</label>
                                <select name="gender" class="form-control form-control-sm" id="gender">
                                    <option
                                        value="0"
                                        @if(!is_null($data->id))
                                        @if($data->getUser->gender == 0)
                                        selected
                                        @endif
                                        @endif
                                    >
                                        Laki - Laki
                                    </option>
                                    <option
                                        value="1"
                                        @if(!is_null($data->id))
                                        @if($data->getUser->gender == 1)
                                        selected
                                        @endif
                                        @endif

                                    >
                                        Perempuan
                                    </option>
                                </select>
                            </div>
                        </div>



                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Tanggal Lahir</label>
                                <input type="date" class="form-control form-control-sm" name="birth_date" value="{{(!is_null($data->id))?$data->getUser->birth_date:''}}" placeholder="Tanggal Lahir">
                            </div>
                        </div>


                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">No Telephone</label>
                                <input type="number" placeholder="No Telephone" class="form-control form-control-sm" name="phone_number" value="{{!is_null($data->id)?$data->getUser->phone_number:''}}">
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Tanggal Bergabung</label>
                                <input type="date" class="form-control form-control-sm" name="join_date" value="{{$data->join_date}}" placeholder="Tanggal Bergabung" required>
                            </div>
                        </div>

                        <div class="col-md-12 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Alamat</label>
                                <textarea name="address" class="form-control" placeholder="Alamat">{{$data->address}}</textarea>
                            </div>
                        </div>


                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">No SIP <span class="text-danger">*</span></label>
                                <input type="text" placeholder="No SIP" class="form-control form-control-sm" name="license_number" value="{{$data->license_number}}" required>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Grade Karyawan <span class="text-danger">*</span></label>
                                <select name="merchant_grade_id" class="form-control form-control-sm selectalldata" required>
                                    @foreach($gradeOption as $key => $item)
                                        <option value="{{$item->id}}"
                                                @if(!is_null($data->id))
                                                @if($item->id==$data->merchant_grade_id)
                                                selected
                                            @endif
                                            @endif>{{$item->nama_grade}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Jabatan </label>
                                <select name="job_position_id" class="form-control form-control-sm selectalldata" >
                                    @foreach($jobOption as $key => $item)
                                        <option value="{{$item->id}}"
                                                @if(!is_null($data->id))
                                                @if($item->id==$data->job_position_id)
                                                selected
                                            @endif
                                            @endif>{{$item->jabatan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold" for="exampleInputPassword1">Departemen </label>
                                <select name="departement_id" class="form-control form-control-sm selectalldata" >
                                    @foreach($departementOption as $key => $item)
                                        <option value="{{$item->id}}"
                                                @if(!is_null($data->id))
                                                @if($item->id==$data->departement_id)
                                                selected
                                            @endif
                                            @endif>{{$item->departemen}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Pendidikan Terakhir <span class="text-danger">*</span></label>
                                <select name="last_education" class="form-control form-control-sm" id="last_education" required>
                                    <option value="">--- Pilih Pendidikan Terakhir ---</option>
                                    @foreach($education as $key => $item)
                                    <option value="{{$key}}" @if($data->last_education == $key) selected @endif>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Foto</label>
                                <input type="file" name="image" class="form-control" accept="image/*">
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <div class="form-group">
                                <label class="font-weight-bold">Golongan Darah</label>
                                <select name="blood_type" class="form-control form-control-sm" id="blood_type">
                                    <option value="">--- Pilih Golongan Darah ---</option>
                                    @foreach($bloodType as $item)
                                    <option value="{{$item}}" @if($data->blood_type == $item) selected @endif>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad">
                            <label class="font-weight-bold">Status</label>
                            <br>
                            <div class="form-group py-3 px-3" style="border: 1px solid #DADADA; background-color:#F8F8F8;border-radius:12px;">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="is_active" value="1" @if($data->is_active == 1 || is_null($data->id)) checked @endif>
                                    <label class="form-check-label">Aktif</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="is_active" value="0" @if($data->is_active === 0) checked @endif>
                                    <label class="form-check-label">Tidak Aktif</label>
                                </div>
                            </div>
                            <label class="font-weight-bold">Apakah ingin dibuat username dan password ?</label>
                            <br>
                            <div class="form-group py-3 px-3 d-flex justify-content-between" style="border: 1px dashed #DADADA; background-color:#F8F8F8;border-radius:12px;width:40%;">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input is_create_email" type="radio" name="is_create_email" value="1" @if($data->is_create_email == 1 || is_null($data->id)) checked @endif>
                                    <label class="form-check-label">Ya</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input is_create_email" type="radio" name="is_create_email" value="0" @if($data->is_create_email === 0) checked @endif>
                                    <label class="form-check-label">Tidak</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-pad" id="mail_pass" @if($data->is_create_email === 0) style="display:none;" @endif>
                            <div class="form-group">
                                <label class="font-weight-bold">Email</label>
                                <input
                                    id="email"
                                    type="email"
                                    class="form-control form-control-sm"
                                    placeholder="Email"
                                    name="email"
                                    value="{{$data->email}}"
                                    @if(is_null($data->id) || $data->is_create_email === 1)
                                        required
                                    @endif
                                >
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Password</label>
                                <input
                                    id="password"
                                    type="password"
                                    class="form-control form-control-sm"
                                    placeholder="Password"
                                    name="password"
                                    value=""
                                    @if(is_null($data->id))
                                        required
                                    @elseif(is_null($data->getUser->password) && $data->is_create_email == 1)
                                        required
                                    @endif
                                >
                            </div>
                        </div>

                    </div>


                    <div class="modal-footer">
                        <a href="{{route('merchant.toko.doctor.index')}}" class="btn btn-light" data-dismiss="modal">Kembali</a>
                        <button class="btn btn-success">Simpan</button>
                    </div>
                    <input type='hidden' name='id' value='{{$data->id }}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('js')
<script>
$(document).ready(function () {
    let data = @json($data);
    $("#md_merchant_staff_category_id").select2({
        placeholder:'--- Pilih Kategori Spesialis ---'
    });
    $("#marital_status").select2();
    $("#religion").select2();
    $("#last_education").select2();
    $("#gender").select2();
    $("#blood_type").select2();
    $(".selectalldata").select2();

    $(".is_create_email").on('change', function(){
        let val = $(this).val();
        if(val == 1){
            $("#mail_pass").show();
            $("#email").prop('required', true);
            if(!data.id){
                $("#password").prop('required', true);
            }
        } else {
            $("#mail_pass").hide();
            $("#email").prop('required', false);
            $("#password").prop('required', false);
        }
    });

    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{route('merchant.toko.doctor.save')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    })
});
</script>
@endsection

<style>
    /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>

@if($activePlugin)
    @if($activePlugin->status == 1)
        <div id="result-form-konten"></div>

        <form onsubmit="return false;" id="form-supplier" class='form-horizontal form-konten' backdrop="">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Kode</label>
                        <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$data->code}}">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email</label>
                        <input type="email" class="form-control form-control-sm" name="email" placeholder="Email" value="{{$data->email}}">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama Supplier</label>
                        <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{$data->name}}" required>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1">No Telp</label>
                        <input type="number" class="form-control form-control-sm" name="phone_number" placeholder="No Telp" value="{{$data->phone_number}}" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Alamat</label>
                        <textarea class="form-control" name="address" placeholder="Alamat">{{$data->address}}</textarea>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Provinsi</label>
                        <select name="region" class="form-control form-control-sm" id="region">
                            <option></option>
                            @foreach($provinces as $key => $item)
                                <option value="{{$item->id}}" @if(strtolower($data->region_name) == strtolower($item->name)) selected @endif>{{$item->name}}</option>
                            @endforeach
                            
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Kota</label>
                        <select name="city" class="form-control form-control-sm" id="city">
                            <option></option>
                            @if(!is_null($data->city_name))
                            <option value="{{$data->city_name}}" selected>{{$data->city_name}}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tipe Alamat</label>
                        <select name="type_address" class="form-control form-control-sm" id="type_address">
                            <option></option>
                            <option value="home" @if(strtolower($data->type_address == 'home'))  selected @endif>Rumah</option>
                            <option value="work" @if(strtolower($data->type_address == 'work'))  selected @endif>Kantor</option>
                            <option value="other" @if(strtolower($data->type_address == 'other'))  selected @endif>Lainnya</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Kode Pos</label>
                        <input type="text" class="form-control form-control-sm" name="postal_code" placeholder="Nama" value="{{$data->postal_code}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Jenis Kelamin</label>
                        <select name="gender" class="form-control form-control-sm" id="gender">
                            <option></option>
                            <option value="male" @if(strtolower($data->gender) == 'male') selected @endif>Laki - Laki</option>
                            <option value="female" @if(strtolower($data->gender) == 'female') selected @endif>Perempuan</option>
                            <option value="unspecified" @if(strtolower($data->gender) == 'unspecified') selected @endif>Tidak Ditentukan</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Kategori</label>
                        <select name="sc_supplier_categories_id" class="form-control form-control-sm" id="sc_supplier_categories">
                            <option></option>
                            @foreach($category as $key => $item)
                                <option value="{{$item->id}}" @if($item->id==$data->sc_supplier_categories_id) selected @endif>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                        <button class="btn btn-success">Simpan</button>
                    </div>
                </div>

                <input type='hidden' name='id' value='{{$data->id }}'>
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        </form>
    @else
        <div class="container-fluid" style="position:relative; width: 100%;">
            <div class="row">
                <div class="col-md-12 d-flex align-items-center justify-content-center" style="height: 42vh;">
                    <div class="text-center">
                        <h1 class="mb-4"><i class="fas fa-exclamation-circle" style="font-size:93px; color: #FF9900;"></i></h1>
                        <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                        <strong>Perhatian!</strong> 
                        Masa berlanggan plugin anda sudah habis, silahkan lakukan pembelian plugin untuk dapat menggunakan fitur !
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@else
    <div class="container-fluid" style="position:relative; width: 100%;">
        <div class="row">
            <div class="col-md-12 d-flex align-items-center justify-content-center" style="height: 42vh;">
                <div class="text-center">
                    <h1 class="mb-4"><i class="fas fa-exclamation-circle" style="font-size:93px; color: #FF9900;"></i></h1>
                    <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                    <strong>Perhatian!</strong> 
                    Masa berlanggan plugin anda sudah habis, silahkan lakukan pembelian plugin untuk dapat menggunakan fitur !
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                </div>
            </div>
        </div>
    </div>
@endif

<script>
    $(document).ready(function(){
        $("#sc_supplier_categories").select2({
            placeholder: "--- Pilih Kategori ---"
        });
        $("#region").select2({
            placeholder: "--- Pilih Provinsi ---"
        });
        $("#city").select2({
            placeholder:  "--- Pilih Kota ---"
        });
        $("#gender").select2({
            placeholder: "--- Pilih Jenis Kelamin ---"
        });
        $("#type_address").select2({
            placeholder: "--- Pilih Tipe Alamat ---"
        });
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div>
                                         <h5 class="modal-title">Tambah Supplier</h5>
                                        <span class="span-text">Untuk menambah supplier baru, kamu harus mengisi data di bawah ini</span>
                                    </div>`)
    });


    $("#modal-target").on("hidden.bs.modal", function () {
        reload(100);
    });


</script>

<script>
     const UpdateContact = {
        async start(formData){
            const response = await this._updateSync(formData);
            const data = await response.data;
            const code = await response.code;
            if(code !== 200){
                toastForSaveData('Terjadi kesalahan, coba beberapa saat lagi !','warning',true,'');
                closeModal(3000);
                return;
            }
            this._updateData(formData, data);
        },
        async _updateSync(formData){

            let dataSend = {
                'merchant_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_merchant_id}}'),
                'plugin_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_plugin_id}}'),
                'resource_name': '{{$data->resource_name}}',
                'etag': '{{$data->etag}}',
                'resource_name': '{{$data->resource_name}}',
                'name': formData.get('name'),
                'email': formData.get('email'),
                'phone_number': formData.get('phone_number'),
                'street_address': formData.get('address'),
                'region_name': formData.get('region_name'),
                'city_name': formData.get('city'),
                'postal_code': formData.get('postal_code'),
                'gender': formData.get('gender'),
                'type_address': formData.get('type_address')
            };
            let formBody = [];
            for (let property in dataSend) {
                let encodedKey = encodeURIComponent(property);
                let encodedValue = encodeURIComponent(dataSend[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");

            // tes get google contact in api.dev
            return $.ajax({
                method: 'POST',
                url:'{{$apiUrl}}/api/google/contact/update',
                async: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "senna-auth": "{{get_user_token()}}",
                    'Access-Control-Allow-Origin': '*',
                },
                data: formBody
            })
        },

        _updateData(formData, data){
            formData.append('etag', data.etag);
            formData.append('resource_name', data.resourceName);
            ajaxTransfer("{{route('merchant.toko.supplier.contact-sync-update-save')}}", formData, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        }
    }

    $(document).ready(function () {
        $('#form-supplier').submit(function () {
            var data = getFormData('form-supplier');
            data.append('region_name', $('#region option:selected').text());
            data.append('city_name', $('#city option:selected').text());
            UpdateContact.start(data);
        })
    })
</script>

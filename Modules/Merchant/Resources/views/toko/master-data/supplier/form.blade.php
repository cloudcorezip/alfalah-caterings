<style>
  /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-supplier" class='form-horizontal form-konten' backdrop="">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Kode</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$data->code}}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="email" class="form-control form-control-sm" name="email" placeholder="Email" value="{{$data->email}}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">Nama Supplier</label>
                <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{$data->name}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1">No Telp</label>
                <input type="number" class="form-control form-control-sm" name="phone_number" placeholder="No Telp" value="{{$data->phone_number}}" required>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1">Alamat</label>
                <textarea class="form-control" name="address" placeholder="Alamat">{{$data->address}}</textarea>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Provinsi</label>
                <select name="region" class="form-control form-control-sm" id="region">
                    <option></option>
                    @foreach($provinces as $key => $item)
                        <option value="{{$item->id}}" @if(strtolower($data->region_name) == strtolower($item->name)) selected @endif>{{$item->name}}</option>
                    @endforeach

                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Kota</label>
                <select name="city" class="form-control form-control-sm" id="city">
                    <option></option>
                    @if(!is_null($data->city_name))
                    <option value="{{$data->city_name}}" selected>{{$data->city_name}}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Tipe Alamat</label>
                <select name="type_address" class="form-control form-control-sm" id="type_address">
                    <option></option>
                    <option value="home" @if(strtolower($data->type_address == 'home'))  selected @endif>Rumah</option>
                    <option value="work" @if(strtolower($data->type_address == 'work'))  selected @endif>Kantor</option>
                    <option value="other" @if(strtolower($data->type_address == 'other')) selected @endif>Lainnya</option>
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Kode Pos</label>
                <input type="number" class="form-control form-control-sm" name="postal_code" placeholder="Kode Pos" value="{{$data->postal_code}}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Jenis Kelamin</label>
                <select name="gender" class="form-control form-control-sm" id="gender">
                    <option></option>
                    <option value="male" @if(strtolower($data->gender) == 'male') selected @endif>Laki - Laki</option>
                    <option value="female" @if(strtolower($data->gender) == 'female') selected @endif>Perempuan</option>
                    <option value="unspecified" @if(strtolower($data->gender) == 'unspecified') selected @endif>Tidak Ditentukan</option>
                </select>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1">Kategori</label>
                <select name="sc_supplier_categories_id" class="form-control form-control-sm" id="sc_supplier_categories">
                    <option></option>
                    @foreach($category as $key => $item)
                        <option value="{{$item->id}}" @if($item->id==$data->sc_supplier_categories_id) selected @endif>{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        @if(is_null($data->id) && $activePlugin)
            @if($activePlugin->status == 1)
                @if(!is_null($activePlugin->email) || !is_null($activePlugin->client_id) || !is_null($activePlugin->client_secret) || !is_null($activePlugin->client_token))
                    <div class="col-md-12 mb-3">
                        <div class="form-check form-check-inline d-flex align-items-center">
                            <input class="form-check-input" type="checkbox" value="1" id="is_google_contact">
                            <label class="form-check-label" for="flexCheckChecked">
                                Sinkronkan dengan Google Contact
                            </label>
                        </div>
                    </div>
                @endif
            @endif
        @endif

        <div class="col-md-12">
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                <button class="btn btn-success">Simpan</button>
            </div>
        </div>

        <input type='hidden' name='id' value='{{$data->id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $("#sc_supplier_categories").select2({
            placeholder: "--- Pilih Kategori ---"
        });
        $("#region").select2({
            placeholder: "--- Pilih Provinsi ---"
        });
        $("#city").select2({
            placeholder:  "--- Pilih Kota ---"
        });
        $("#gender").select2({
            placeholder: "--- Pilih Jenis Kelamin ---"
        });
        $("#type_address").select2({
            placeholder: "--- Pilih Tipe Alamat ---"
        });

        $('#form-supplier').submit(function () {
            var data = getFormData('form-supplier');
            data.append('region_name', $('#region option:selected').text());
            data.append('city_name', $('#city option:selected').text());

            if($('#is_google_contact').is(':checked')){
                AddContact.start(data);
            } else {
                ajaxTransfer("{{route('merchant.toko.supplier.save')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            }

        })
    });

    $(document).ready(function(){
        $("#sc_supplier_categories_id").select2();
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div>
                                         <h5 class="modal-title">Tambah Supplier</h5>
                                        <span class="span-text">Untuk menambah supplier baru, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


    $('#region').on('change', function(e){
        $('#city option').remove();
        $.ajax({
            url: '{{route("merchant.toko.supplier.get-city")}}',
            method: 'POST',
            data: {'_token': "{{csrf_token()}}", 'id': $(this).val()},
            success:function(response){
                let html = '<option></option>';
                response.map(item => {
                    html += '<option value="'+item.id+'">'+item.name+'</option>';
                });
                $('#city').append(html);
            }
        });
    });

    const AddContact = {
        async start(formData){
            let plugin = @json($activePlugin);
            if(plugin.payment_type.toLowerCase() == 'quota'){
                if((parseInt(plugin.quota_used) + 1) > plugin.quota_available){
                    toastForSaveData('Kuota anda tidak cukup, data gagal disimpan !','warning',true,'');
                    return;
                }
            }

            const response = await this._addSync(formData);
            const data = await response.data;
            const code = await response.code;
            if(code !== 200){
                toastForSaveData('Terjadi kesalahan, coba beberapa saat lagi !','warning',true,'');
                return;
            }
            this._addData(formData, data);
        },
        async _addSync(formData){
            let dataSend = {
                'merchant_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_merchant_id}}'),
                'plugin_id': parseInt('{{($activePlugin==false)?0:$activePlugin->md_plugin_id}}'),
                'name': formData.get('name'),
                'email': formData.get('email'),
                'phone_number':formData.get('phone_number'),
                'street_address': formData.get('address'),
                'region_name': formData.get('region_name'),
                'city_name': formData.get('city_name'),
                'postal_code': formData.get('postal_code'),
                'gender': formData.get('gender'),
                'type_address': formData.get('type_address')
            };

            let formBody = [];
            for (let property in dataSend) {
                let encodedKey = encodeURIComponent(property);
                let encodedValue = encodeURIComponent(dataSend[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");

            // tes get google contact in api.dev
            return $.ajax({
                method: 'POST',
                url:'{{$apiUrl}}/api/google/contact/add',
                async: true,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "senna-auth": "{{get_user_token()}}",
                    'Access-Control-Allow-Origin': '*',
                },
                data: formBody
            })
        },

        _addData(formData, data){
            formData.append('is_google_contact', 1);
            formData.append('etag', data.etag);
            formData.append('resource_name', data.resourceName);
            ajaxTransfer("{{route('merchant.toko.supplier.save')}}", formData, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        }
    }

    $("#modal-target").on("hidden.bs.modal", function () {
        reload(100);
    });


</script>

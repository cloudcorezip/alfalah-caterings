@extends('backend-v3.layout.main')
@section('title', $title)
@section('content')
<style>
    /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  
  /* Firefox */
  input[type=number] {
    -moz-appearance: textfield;
  }
  </style>
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a>Master Data</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.toko.supplier.index')}}">Supplier</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a href="#">{{$title}}</a>
                    </li>
                </ol>
            </nav>
            <span>Kelola daftar suppliermu dan ketahui siapa saja pemasok produk-produkmu</span>
        </div>
    </div>
</div>

<div class="container-fluid">
    <form onsubmit="return false;" id="form-konten" class="form-horizontal">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <button
                            class="d-flex justify-content-between align-items-center pl-0"
                            type="button"
                            data-toggle="collapse"
                            data-target="#collapse-supplier-profil"
                            aria-expanded="false"
                            style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                        >
                            <h4 class="font-weight-bold mb-3">Profil Supplier</h4>
                            <i class="fas fa-angle-down" style="font-size:16px;"></i>
                        </button>
                    </div>
                </div>
                <div id="collapse-supplier-profil" class="collapse show">
                    <div class="row"> 
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">Nama Supplier <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm" name="name" placeholder="Nama" value="{{$data->name}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">Kode Supplier <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$data->code}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">Kategori Supplier <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <select name="sc_supplier_categories_id" class="form-control form-control-sm" id="sc_supplier_categories" required>
                                        <option></option>
                                        @foreach($category as $key => $item)
                                            <option value="{{$item->id}}" @if($item->id==$data->sc_supplier_categories_id) selected @endif>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">No Telepon <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control form-control-sm" name="phone_number" placeholder="No Telp" value="{{$data->phone_number}}" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">Email <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control form-control-sm" name="email" placeholder="Email" value="{{$data->email}}" required>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                </div>

            </div>
        </div>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <button
                            class="d-flex justify-content-between align-items-center pl-0"
                            type="button"
                            data-toggle="collapse"
                            data-target="#collapse-supplier-address"
                            aria-expanded="false"
                            style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                        >
                            <h4 class="font-weight-bold mb-3">Alamat Supplier</h4>
                            <i class="fas fa-angle-down" style="font-size:16px;"></i>
                        </button>
                    </div>
                </div>
                <div id="collapse-supplier-address" class="collapse show">
                    <div class="row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row mb-3"> 
                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">Tipe Alamat</label>
                                <div class="col-sm-8">
                                    <select name="type_address" class="form-control form-control-sm" id="type_address">
                                        <option></option>
                                        <option value="home" @if(strtolower($data->type_address == 'home'))  selected @endif>Rumah</option>
                                        <option value="work" @if(strtolower($data->type_address == 'work'))  selected @endif>Kantor</option>
                                        <option value="other" @if(strtolower($data->type_address == 'other')) selected @endif>Lainnya</option>
                                    </select>
                                </div>  
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-start">
                                 <label class="col-sm-4 col-form-label font-weight-bold">Alamat</label>
                                 <div class="col-sm-8">
                                    <textarea class="form-control" name="address" placeholder="Alamat">{{$data->address}}</textarea>
                                 </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">Provinsi</label>
                                <div class="col-sm-8">
                                    <select name="region" class="form-control form-control-sm" id="region">
                                        <option></option>
                                        @foreach($provinces as $key => $item)
                                            <option value="{{$item->id}}" @if(strtolower($data->region_name) == strtolower($item->name)) selected @endif>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">Kota</label>
                                <div class="col-sm-8">
                                    <select name="city" class="form-control form-control-sm" id="city">
                                        <option></option>
                                        @if(!is_null($data->city_name))
                                        <option value="{{$data->city_name}}" selected>{{$data->city_name}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row d-flex align-items-center">
                                <label class="col-sm-4 col-form-label font-weight-bold">Kode Pos</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control form-control-sm" name="postal_code" placeholder="Kode Pos" value="{{$data->postal_code}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <input type='hidden' name='id' value='{{$data->id }}'>
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <a href="{{route('merchant.toko.supplier.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                <button class="btn btn-success" id="btn-save"><i class="fa fa-save mr-2"></i> Simpan</button>
            </div>
        </div>
    </form>
</div>


@endsection

@section('js')
<script>
 $(document).ready(function () {
    $("#sc_supplier_categories").select2({
        placeholder: "--- Pilih Kategori ---"
    });
    $("#region").select2({
        placeholder: "--- Pilih Provinsi ---"
    });
    $("#city").select2({
        placeholder:  "--- Pilih Kota ---"
    });
    $("#gender").select2({
        placeholder: "--- Pilih Jenis Kelamin ---"
    });
    $("#type_address").select2({
        placeholder: "--- Pilih Tipe Alamat ---"
    });

    $('#region').on('change', function(e){
        $('#city option').remove();
        $.ajax({
            url: '{{route("merchant.toko.supplier.get-city")}}',
            method: 'POST',
            data: {'_token': "{{csrf_token()}}", 'id': $(this).val()},
            success:function(response){
                let html = '<option></option>';
                response.map(item => {
                    html += '<option value="'+item.id+'">'+item.name+'</option>';
                });
                $('#city').append(html);
            }
        });
    });

    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        data.append('region_name', $('#region option:selected').text());
        data.append('city_name', $('#city option:selected').text());

        ajaxTransfer("{{route('merchant.toko.supplier.save')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
        });
    })
});

</script>
@endsection
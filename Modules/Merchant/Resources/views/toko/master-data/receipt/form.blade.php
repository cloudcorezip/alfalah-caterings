@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .w-90 {
            width: 90%;
        }
        .w-70 {
            width: 80%;
        }
        .btn-remove {
            background:none!important;
            border:none!important;
            position:absolute;
            right:0;
            top:50%;
            transform: translateY(-50%);
        }
        .btn-remove .iconify {
            width: 25px;
            height: 250px;
        }
        .btn-remove-cost {
            background:none!important;
            border:none!important;
            position:absolute;
            right:0;
            top:50%;
            transform: translateY(-50%);
        }
        .btn-remove-cost .iconify {
            width: 25px;
            height: 250px;
        }
    </style>
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.master-data.receipt.index')}}">Resep</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="#">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Di halaman ini, kamu dapat menambah atau merubah data resep</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 py-4 px-5">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row mb-4">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode  <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{is_null($data->id)? $code:$data->code}}" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Resep  <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-sm" name="name" value="{{$data->name}}" required placeholder="Nama Resep">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Harga Jual <span class="text-danger">*</span></label>
                                <input type="text" class="form-control amount_currency" name="selling_price" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:number_format((float)$data->selling_price, 2, '.', '')}}" id="harga-jual" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kategori Resep <span class="text-danger">*</span></label>
                                <select name="sc_product_category_id" class="form-control" id="sc_product_category_id" required>
                                    @if(!is_null($data->id))
                                        <option value="{{$data->sc_product_category_id}}" selected>{{$data->getCategory->name}}</option>

                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Satuan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="md_unit_id" name="md_unit_id" value="{{is_null($data->md_unit_id)?'':$data->getUnit->name}}" required placeholder="Contoh satuan : buah, pcs, bungkus">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Duplikasi ke Cabang Lain</label>

                                <select name="md_merchant_id" id="branch" class="form-control mr-2 js-example-responsive js-states" multiple="multiple">
                                    @foreach (get_cabang() as $key => $b)
                                        @if($b->id!=merchant_id())
                                            <option value="{{$b->id}}">{{$b->nama_cabang}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-12 p-4 rounded" style="background: #FDFDFD; border: 1px dashed #D6D6D6;">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="font-weight-bold">Isi Resep</label>
                                </div>
                                <div class="col-md-3">
                                    <label class="font-weight-bold">Jumlah</label>
                                </div>
                                <div class="col-md-3">
                                    <label class="font-weight-bold">Satuan</label>
                                </div>
                                <div class="col-md-3">
                                    <label class="font-weight-bold">Total</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12" id="dynamic_field">

                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group position-relative">
                                        <label class="font-weight-bold text-center">Sub Total</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group position-relative">
                                        <label class="font-weight-bold ml-3" id="sub_total_item">{{rupiah(0)}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="row-add-product">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-light btn-block" id="add-product">Tambah Isi Resep</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-12 p-4 rounded" style="background: #FDFDFD; border: 1px dashed #D6D6D6;">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="font-weight-bold">Biaya Lainnya</label>
                                </div>
                                <div class="col-md-6">
                                    <label class="font-weight-bold">Nominal</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12" id="dynamic_field_cost">
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-3">
                                    <div class="form-group position-relative">
                                        <label class="font-weight-bold text-center">Sub Total</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group position-relative">
                                        <label class="font-weight-bold ml-3" id="sub_total_cost">{{rupiah(0)}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="row-add-cost">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-light btn-block" id="add-cost">Tambah Biaya</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-3">
                            <div class="form-group position-relative">
                                <label class="font-weight-bold text-center">TOTAL</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group position-relative">
                                <label class="font-weight-bold text-center ml-3" id="total_all_item_cost">{{rupiah(0)}}</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Deskripsi Resep</label>
                                <textarea class="form-control" style="min-height: 200px" name="description" id="desc" placeholder="Tambahkan deskripsi produk">{{$data->description}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan</label>
                                <span class="d-block" style="color:#A4A4A4">Isi keterangan merupakan tanggung jawab penginput data</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 offset-md-6">
                        </div>
                        <div class="col-md-12 text-right">
                            <a href="{{route('merchant.toko.master-data.receipt.index')}}" class="btn btn-light" data-dismiss="modal">Kembali</a>
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="md_sc_product_type_id" value="4">
                    <input type='hidden' name='id' value='{{ $data->id }}' id="id-prod">
                    <input type='hidden' name='inv_method_id' value='{{ $merchant->md_inventory_method_id }}' id="id-prod">
                    <input type="hidden" name="purchase_price" value="0">
                    <input type="hidden" name="is_with_stock" value="0">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#sc_product_category_id').select2({
                placeholder:'--- Pilih Kategori Produk ---',
            }).on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_product_category_id-results').append('<a id="add-category" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.product-category.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;" class="text-danger"><b>+ Tambah Kategori Baru</b></a>');
            });

            $(document).on('click','#add-category', function(e){
                $("#sc_product_category_id").select2('close');
            })
        });
    </script>
    <script>
        var i = 0;
        var productSelectedId={}
        $('#add-product').click(function(){
            i++;
            if(i>999){
                $('#row-add-product').hide();
            }
            let html = `<div class="row product-row" id="row${i}">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <select data-row-id="${i}" name="sc_product_id[]" class="form-control data-product" id="sc_product_id${i}" onchange="productChange(${i})">
                                        <option value="-1">--- Pilih Item ---</option>
                                    </select>
                                    <input type="hidden" id="price${i}" value="0">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group position-relative">
                                    <input data-row-id="${i}" id="quantity${i}" name="quantity[]" min="0" step="0.001" type="number" onkeypress="return isNumberKey(event)" class="form-control form-control-sm  qty" placeholder="Quantity" value="0">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group position-relative">
                                    <input type="hidden" id="is_multi_unit${i}" name="is_multi_unit[]" value="0">
                                    <input type="hidden" id="code${i}" name="product_unit_code[]" value="0">
                                    <input type="hidden" id="multi_unit_id${i}" name="multi_unit_id[]" value="-1">
                                     <input type="hidden" id="value_conversion${i}" name="value_conversion[]" value="1">
                                     <input type="hidden" id="json_multi_unit${i}" name="json_multi_unit[]" value="-1">
                                     <input type="hidden" id="unit_name${i}" name="unit_name[]" value="-1">
                                    <select data-row-id="${i}" name="unit[]" class="form-control" id="unit${i}" onchange="getMultiUnit(${i})">
                                        <option value="-1">--- Pilih Satuan ---</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group position-relative">
                                    <input data-row-id="${i}" id="total${i}" type="hidden" class="w-70 form-control form-control-sm total_origin" value="0">
                                    <input data-row-id="${i}" id="total_show${i}" type="text" name="total[]" class="w-70 form-control form-control-sm total" placeholder="Total" value="0">
                                    <button type="button" class="btn btn-remove" id="${i}">
                                        <span class="iconify" data-icon="clarity:window-close-line"></span>
                                    </button>
                                </div>

                            </div>
                        </div>`;

            $('#dynamic_field').append(html)

            $("#sc_product_id" + i).select2({
                placeholder:"---- Pilih Isian Resep ----",
                ajax: {
                    type: "GET",
                    url: "{{route('api.product.non-package')}}?userId={{user_id()}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: false
                },
            });
            $('.total').mask('#.##0,00', {reverse: true});
            $("#unit"+i).select2()
        });


        function sumSubTotalItem()
        {
            var sum = 0;
            $(".total_origin").each(function(){
                sum += +$(this).val();
            });
            var sumCost = 0;
            $(".total_hidden").each(function(){
                sumCost += +$(this).val();
            });
            $("#sub_total_item").text(currencyFormat(sum,''));
            $("#sub_total_cost").text(currencyFormat(sumCost,''));
            $("#total_all_item_cost").text(currencyFormat(sum+sumCost,''));


        }

        function getMultiUnit(i)
        {

            let unitList = ($("#unit"+i).select2('data'))? $("#unit"+i).select2('data'):[];
            if(unitList.length>0)
            {
                let data=unitList[0]
                $("#is_multi_unit"+i).val(1)
                $("#unit_name"+i).val(data['text'])
                $("#value_conversion"+i).val(data['nilai_konversi'])
                $("#multi_unit_id"+i).val(data['original_id'])
                $("#price"+i).val(data['purchase_price'])
                let quantity=parseFloat($("#quantity"+i).val())
                let price=parseFloat(data['purchase_price'])
                $('#total_show'+i).val(currencyFormat(quantity*price))
                $('#total'+i).val(quantity*price)
                sumSubTotalItem()
            }
        }
        function productChange(i)
        {
            let products = ($("#sc_product_id"+i).select2('data'))? $("#sc_product_id"+i).select2('data'):[];

            if(products.length>0)
            {
                let datas = products[0]
                let multi_unit=JSON.parse(datas['multi_unit'])
                let multi_unit_list=[]
                $('#unit'+i).empty();
                if(multi_unit.length>0)
                {
                    $.each(multi_unit,function (iteration,value){
                        if(iteration===0)
                        {
                            multi_unit_list.push({
                                id:'-1',
                                text:'Pilih Satuan',
                                purchase_price:0,
                                nilai_konversi:0,
                                original_id:'-1',
                                selling_price:0,
                                is_with_stock:datas['is_with_stock']
                            })
                        }

                        multi_unit_list.push({
                            id:value.unit_id,
                            text:value.konversi_ke,
                            purchase_price:value.harga_jual,
                            nilai_konversi:value.nilai_konversi,
                            original_id:value.id,
                            selling_price:0,
                            is_with_stock:datas['is_with_stock']
                        })
                    })
                    $("#unit"+i).select2({
                        data:multi_unit_list
                    })
                    $("#json_multi_unit"+i).val(JSON.stringify(multi_unit_list))
                    $("#code"+i).val(datas['code'])
                    if(datas['is_with_stock']==0){
                        $("#price"+i).val(datas['selling_price'])
                        let quantity=parseFloat($("#quantity"+i).val())
                        let price=parseFloat(datas['selling_price'])
                        $('#total_show'+i).val(currencyFormat(quantity*price))
                        $('#total'+i).val(quantity*price)
                    }else{
                        $("#price"+i).val(datas['purchase_price'])
                        let quantity=parseFloat($("#quantity"+i).val())
                        let price=parseFloat(datas['purchase_price'])
                        $('#total_show'+i).val(currencyFormat(quantity*price))
                        $('#total'+i).val(quantity*price)

                    }
                sumSubTotalItem()
                }else{
                    $("#code"+i).val(datas['code'])
                    $("#is_multi_unit"+i).val(0)
                    $("#unit_name"+i).val(datas['unit_name'])
                    if(datas['is_with_stock']==0){
                        $("#price"+i).val(datas['selling_price'])
                        let quantity=parseFloat($("#quantity"+i).val())
                        let price=parseFloat(datas['selling_price'])
                        $('#total_show'+i).val(currencyFormat(quantity*price))
                        $('#total'+i).val(quantity*price)
                    }else{
                        $("#price"+i).val(datas['purchase_price'])
                        let quantity=parseFloat($("#quantity"+i).val())
                        let price=parseFloat(datas['purchase_price'])
                        $('#total_show'+i).val(currencyFormat(quantity*price))
                        $('#total'+i).val(quantity*price)

                    }
                    multi_unit_list.push({
                        id:datas['unit_id'],
                        text:datas['unit_name'],
                        purchase_price:datas['purchase_price'],
                        selling_price:datas['selling_price'],
                        nilai_konversi:1,
                        original_id:datas['id'],
                        is_with_stock:datas['is_with_stock']
                    })
                    $("#unit"+i).select2({
                        data:multi_unit_list
                    })
                    sumSubTotalItem()
                }

                let status = false;
                Object.keys(productSelectedId).forEach(key => {
                    if(productSelectedId[key] === parseInt(datas['id'])){
                        status=true;
                    }
                });
                if(status)
                {
                    $('#row'+i).remove();
                    delete productSelectedId[i];
                }else{
                    productSelectedId[i] = parseInt(datas['id']);

                }


            }

        }

        $(document).on("change", ".product-row input.qty", function() {
            var row = $(this).attr('data-row-id');
            let sub_total=parseFloat($("#quantity"+row).val())*parseFloat($('#price'+row).val())
            $('#total_show'+row).val(currencyFormat(sub_total))
            $("#total"+row).val(sub_total)
            sumSubTotalItem()

        });
        $(document).on("change", ".product-row input.total", function() {
            var row = $(this).attr('data-row-id');
            let total_show = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#total_show"+row).val(),'.',''),',','.');
            $("#total"+row).val(total_show)
            sumSubTotalItem()

        });



        $(document).on('click', '.btn-remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });

        $(document).on('click', '.btn-remove-cost', function(){
            var button_id = $(this).attr("id");
            $('#row-cost'+button_id+'').remove();
            u-=1
            if(u<1000){
                $('#row-add-cost').show();
            }
        });

        var u = 0;
        var cost_id = [];
        $('#add-cost').click(function(){
            u++;
            let html = `<div class="row cost-row" id="row-cost${u}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select cost-row-id="${u}" name="cost_id[]" class="form-control data-product" id="cost_id${u}">
                                        <option value="-1">--- Pilih Item ---</option>
                                    </select>
                                    <input type="hidden" id="id_cost_hide${u}"  cost-row-id="${u}">
                                    <input type="hidden" name="cost_name[]" id="cost_name${u}" cost-row-id="${u}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <input cost-row-id="${u}" id="amount_hidden${u}"type="hidden" class="form-control form-control-sm w-90 qty total_hidden" value="0">
                                    <input cost-row-id="${u}" id="amount${u}" name="amount[]" type="text" class="form-control form-control-sm w-90 qty amount_currency total" placeholder="Jumlah Biaya" value="0">
                                    <button type="button" class="btn btn-remove-cost" id="${u}">
                                        <span class="iconify" data-icon="clarity:window-close-line"></span>
                                    </button>
                                </div>
                            </div>
                        </div>`;

            $('#dynamic_field_cost').append(html);
            $('.amount_currency').mask("#.##0,00", {reverse: true});

            $("#cost_id" + u).select2({
                placeholder:"---- Pilih Biaya ----",
                ajax: {
                    type: "GET",
                    url: "{{route('api.cost.all',['merchantId'=>merchant_id()])}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        //    console.log(data)
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

            $(document).on("change", ".cost-row input.total", function() {
                var row = $(this).attr('cost-row-id');
                let total_show = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#amount"+row).val(),'.',''),',','.');
                $("#amount_hidden"+row).val(total_show)
                sumSubTotalItem()

            });


            $("#cost_id"+ u).on('select2:select', function (e) {
                let data = e.params.data;
                $('#cost_name'+ u).val(data.text);
            });

            $("#cost_id" + u).on('select2:open', () => {
                $(".select2-results:not(:has(a))").append('<a id="add-customer" onclick="loadModal(this)" target="{{route("merchant.toko.master-data.receipt.add-cost")}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Biaya Baru</b></a>');
            });
            $(document).on('click','#add-customer', function(e){
                $("#cost_id" + u).select2('close');
            });
        });


        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>



    <script>

        $(document).ready(function () {
            $('#branch').select2();
            getCategory()
            $("#form-konten").submit(function () {
                var data = getFormData("form-konten");
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json(get_cabang());
                    merchantIds.push(parseInt($("#merchant_id").val()));
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }
                let md_merchant_id = merchantIds.join(',');
                data.append('md_merchant_id',md_merchant_id);
                ajaxTransfer("{{route('merchant.toko.master-data.receipt.save')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            });
        });

        function getCategory()
        {
            $("#sc_product_category_id").select2({
                placeholder:'--- Pilih Kategori Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.product.category')}}?merchant_id={{merchant_id()}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }

    </script>
@endsection

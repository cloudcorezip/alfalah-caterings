@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .w-90 {
            width: 90%;
        }
        .btn-remove {
            background:none!important;
            border:none!important;
            position:absolute;
            right:0;
            top:50%;
            transform: translateY(-50%);
        }
        .btn-remove .iconify {
            width: 25px;
            height: 250px;
        }
        .btn-remove-cost {
            background:none!important;
            border:none!important;
            position:absolute;
            right:0;
            top:50%;
            transform: translateY(-50%);
        }
        .btn-remove-cost .iconify {
            width: 25px;
            height: 250px;
        }
    </style>
    <!-- Page Heading -->
    <div class="page-header">
    <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.master-data.receipt.index')}}">Resep</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="#">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Di halaman ini, kamu dapat menambah atau merubah data resep</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 py-4 px-5">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row mb-4">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Kode</label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{$data->code}}" disabled>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Resep</label>
                                <input type="text" class="form-control form-control-sm" name="name" value="{{$data->name}}" required placeholder="Nama Resep">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Harga Jual <span class="text-danger">*</span></label>
                                <input type="text" class="form-control amount_currency" name="selling_price" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:number_format((float)$data->selling_price, 2, '.', '')}}" id="harga-jual" required>
                            </div>
                        </div>
                        {{-- @dd($category) --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kategori Produk <span class="text-danger">*</span></label>
                                <select name="sc_product_category_id" class="form-control select2" id="sc_product_category_id" required>
                                    @if(!is_null($data->id))
                                        <option value="{{$data->sc_product_category_id}}" selected>{{$data->getCategory->name}}</option>

                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Satuan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="md_unit_id" name="md_unit_id" value="{{is_null($data->md_unit_id)?'':$data->getUnit->name}}" required placeholder="Contoh satuan : buah, pcs, bungkus">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 col-form-label font-weight-bold">Duplikasi ke Cabang Lain</label>
                                    <select name="md_merchant_id" id="branch" class="form-control mr-2 js-example-responsive js-states select2" multiple="multiple">
                                        @foreach (get_cabang() as $key => $b)
                                        @if($b->id!=merchant_id())
                                            <option value="{{$b->id}}"

                                            @if(!is_null($data->assign_to_branch))
                                                @if(in_array($b->id, json_decode($data->assign_to_branch)))
                                                    selected
                                                @endif
                                        @endif
                                            >{{$b->nama_cabang}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-12 p-4 rounded" style="background: #FDFDFD; border: 1px dashed #D6D6D6;">
                            <div class="row">
                                <div class="col-md-12" id="dynamic_field">
                                    <div>
                                        <div class="row d-flex align-items-center">
                                            <div class="col-md-6">
                                                <h6 class="font-weight-bold">Isi Resep</h6>
                                                <span style="color:#A4A4A4;">Tambahkan Isi Resep.</span>
                                            </div>
                                            <div class="col-md-6 text-right">

                                            </div>
                                        </div>
                                        <div class="collapse show" id="bahanBaku">
                                            <hr>
                                            <div class="row" style="margin-bottom: 10px">
                                                @if($data->md_user_id==user_id())
                                                    {!! $addItem !!}
                                                @endif
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-custom" id="table-data-3" width="100%" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            @foreach($tableColumnsItem as $key =>$item)
                                                                <th>{{($tableColumnsItem[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumnsItem[$key]))}}</th>
                                                            @endforeach
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-12 p-4 rounded" style="background: #FDFDFD; border: 1px dashed #D6D6D6;">
                            <div class="row">
                                <div class="col-md-12" >
                                    <div>
                                        <div class="row d-flex align-items-center">
                                            <div class="col-md-6">
                                                <h6 class="font-weight-bold">Biaya Lainnya</h6>
                                                <span style="color:#A4A4A4;">Tambahkan Biaya Lainnya.</span>
                                            </div>
                                            <div class="col-md-6 text-right">

                                            </div>
                                        </div>
                                        <div class="collapse show" id="biaya">
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12" id="dynamic_field_cost">
                                                    @if(!is_null($data->cost_other))
                                                    @foreach (json_decode($data->cost_other) as $item)

                                                    <div class="row cost-row" id="row{{$item->id}}">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <select cost-row-id="{{$item->id}}" name="cost_id[]" class="form-control select2" id="cost_id{{$item->id}}">
                                                                    @foreach ($cost_other as $a)
                                                                         <option
                                                                         @if($a->id==$item->id)
                                                                            selected
                                                                         @endif
                                                                         value="{{$a->id}}">{{$a->text}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group position-relative">
                                                                <input cost-row-id="{{$item->id}}" id="amount{{$item->id}}" name="amount[]" type="text" class="form-control form-control-sm w-90 qty amount_currency" placeholder="Quantity" value="{{number_format((float)$item->amount, 2, '.', '')}}">
                                                                <input cost-row-id="{{$item->id}}" id="cost_name{{$item->id}}" name="cost_name[]" type="hidden" value="{{$item->name}}">
                                                                <button type="button" class="btn btn-remove" id="{{$item->id}}">
                                                                    <span class="iconify" data-icon="clarity:window-close-line"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                    @endif

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    @if($data->md_user_id==user_id())
                                                    <button type="button" class="btn btn-light btn-block" id="add-cost">Tambah Biaya Lainnya</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Deskripsi Resep</label>
                                <textarea class="form-control" style="min-height: 200px" name="description" id="desc" placeholder="Tambahkan deskripsi resep">{{$data->description}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan</label>
                                <span class="d-block" style="color:#A4A4A4">Isi keterangan merupakan tanggung jawab penginput data</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 offset-md-6">

                        </div>
                        <div class="col-md-12 text-right">
                            <a href="{{route('merchant.toko.master-data.receipt.index')}}" class="btn btn-light" data-dismiss="modal">Kembali</a>
                            @if($data->md_user_id==user_id())
                                <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                            @endif
                        </div>
                    </div>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="md_sc_product_type_id" value="4">
                    <input type='hidden' name='id' value='{{ $data->id }}' id="id-prod">
                    <input type='hidden' name='inv_method_id' value='{{ $merchant->md_inventory_method_id }}' id="id-prod">
                    <input type="hidden" name="purchase_price" value="0">
                    <input type="hidden" name="is_with_stock" value="0">
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#sc_product_category_id').select2({
                placeholder:'--- Pilih Kategori Produk ---',
            }).on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_product_category_id-results').append('<a id="add-category" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.product-category.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;" class="text-danger"><b>+ Tambah Kategori Baru</b></a>');
            });

            $(document).on('click','#add-category', function(e){
                $("#sc_product_category_id").select2('close');
            })
            $('.select2').select2();
            ajaxDataTable('#table-data-3', 1, "{{route('merchant.toko.master-data.receipt.datatable-item')}}?id={{$data->id}}", [
                    @foreach($tableColumnsItem as $key =>$item)
                    @if($tableColumnsItem[$key]=='action')
                {
                    data: '{{$tableColumnsItem[$key]}}',
                    name: '{{$tableColumnsItem[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumnsItem[$key]}}', name: '{{$tableColumnsItem[$key]}}'
                },
                @endif
                @endforeach
            ]);
        });
    </script>
    <script>
        function deleteDataItem(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.master-data.receipt.delete-item')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        }

        $('.amount_currency').mask("#.##0,00", {reverse: true});


        $(document).on('click', '.btn-remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
            codes = codes.filter(item => {
                return item.row != button_id
            });
        });

        $(document).on('click', '.btn-remove-cost', function(){
            var button_id = $(this).attr("id");
            $('#row-cost'+button_id+'').remove();
            u-=1
            console.log(u)
            if(u<1000){
                $('#row-add-cost').show();
            }
        });

        var u = 0;
        var cost_id = [];
        $('#add-cost').click(function(){
            u++;
            let html = `<div class="row cost-row" id="row${u}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select cost-row-id="${u}" name="cost_id[]" class="form-control" id="cost_id${u}">
                                        <option value="-1">--- Pilih Item ---</option>
                                    </select>
                                    <input type="hidden" name="cost_name[]" id="cost_name${u}" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <input cost-row-id="${u}" id="amount${u}" name="amount[]" type="text" class="form-control form-control-sm w-90 qty amount_currency" placeholder="Quantity" value="0">
                                    <button type="button" class="btn btn-remove-cost" id="${u}">
                                        <span class="iconify" data-icon="clarity:window-close-line"></span>
                                    </button>
                                </div>
                            </div>
                        </div>`;

            $('#dynamic_field_cost').append(html);
            $('.amount_currency').mask("#.##0,00", {reverse: true});

            $("#cost_id" + u).select2({
                placeholder:"---- Pilih Produk ----",
                ajax: {
                    type: "GET",
                    url: "{{route('api.cost.all',['merchantId'=>merchant_id()])}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

            $("#cost_id"+ u).on('select2:select', function (e) {
                let data = e.params.data;
                $('#cost_name'+ u).val(data.text);
            });
        });

        $(document).ready(function () {
            getCategory()
            $("#form-konten").submit(function () {
                var data = getFormData("form-konten");
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json(get_cabang());
                    merchantIds.push(parseInt($("#merchant_id").val()));
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }
                let md_merchant_id = merchantIds.join(',');
                data.append('md_merchant_id',md_merchant_id);
                ajaxTransfer("{{route('merchant.toko.master-data.receipt.save')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            });
        });
        function getCategory()
        {
            $("#sc_product_category_id").select2({
                placeholder:'--- Pilih Kategori Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.product.category')}}?merchant_id={{merchant_id()}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }
    </script>
@endsection

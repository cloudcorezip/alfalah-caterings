@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Marketing</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.affiliate.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Daftar data affiliator unuk membantu memasarkan produkmu</span>
            </div>
        </div>

    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row" style="margin-bottom: 10px">
                    {!! $add !!}
                </div>
                <div id="output-discount-product">
                    @include('merchant::toko.master-data.affiliate.list')
                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.affiliate.delete')}}", data, function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        }

    </script>
@endsection

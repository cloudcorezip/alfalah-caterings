<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-category" class='form-horizontal form-konten' backdrop="">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Nama Affiliator<span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name" placeholder="Nama" value="{{(is_null($data->id))?'':$data->getUser->fullname}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Email <span class="text-danger">*</span></label>
                <input type="email" class="form-control" name="email" placeholder="Email" value="{{$data->email}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">No Telp<span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="phone_number" placeholder="No Telp" value="{{(is_null($data->id))?'':$data->getUser->phone_number}}" onkeypress="isNumber(event)" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Alamat</label>
                <textarea name="address" class="form-control" placeholder="Alamat">{{$data->address}}</textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-category').submit(function () {
            var data = getFormData('form-category');
            ajaxTransfer("{{route('merchant.toko.affiliate.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    })
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Affiliator</h4>
                                        <span class="span-text">Untuk menambah BA, Affiliate Marketing, dll. Kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    function isNumber(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        if (key.length == 0) return;
        var regex = /^[0-9.\b]+$/;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }


</script>

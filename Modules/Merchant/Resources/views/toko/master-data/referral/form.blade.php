@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Marketing</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.referral.index')}}">Referral</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Untuk menambah referral, kamu harus mengisi data di bawah ini !</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
            <div class="card shadow mb-4">
                <div class="card-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="font-weight-bold mb-3">Informasi Referral</h4>
                            <hr>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label font-weight-bold">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="btn-group btn-group-toggle grab-btn__group mb-3" data-toggle="buttons">
                                    <label
                                        class="btn btn-outline-warning active"
                                    >
                                        <input
                                            type="radio"
                                            name="is_active"
                                            id="option1"
                                            autocomplete="off"
                                            value="1"
                                            checked
                                        >
                                        <span>Aktif</span>
                                    </label>
                                    <label
                                        class="btn btn-outline-warning"
                                    >
                                        <input
                                            type="radio"
                                            name="is_active"
                                            id="option2"
                                            autocomplete="off"
                                            value="0"
                                        >
                                        <span>Tidak Aktif</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Kode <span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-control-sm" name="code" value="{{$data->code}}">
                        </div>
                        <label class="col-sm-2 col-form-label font-weight-bold">Affiliator <span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <select class="form-control" id="affiliator_id" name="staff_user_id" required onchange="getCommission()">
                                <option value="-1">Pilih Affiliator</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group row d-flex align-items-top">
                        <label class="col-sm-2 col-form-label font-weight-bold">Tanggal Mulai <span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <input
                                type="date"
                                class="form-control form-control-sm"
                                name="start_date"
                                placeholder="Tanggal Mulai"
                                id="start_date"
                            >
                        </div>
                        <label class="col-sm-2 col-form-label font-weight-bold">Tanggal Berakhir <span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <input
                                type="date"
                                class="form-control form-control-sm"
                                name="end_date"
                                placeholder="Tanggal Selesai"
                                id="end_date"
                            >
                        </div>
                    </div>
                </div>
            </div>

            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <button
                                class="d-flex justify-content-between align-items-center pl-0"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapse-customer"
                                aria-expanded="false"
                                style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                            >
                                <h4 class="font-weight-bold mb-3">Ketentuan Referral</h4>
                                <i class="fas fa-angle-down" style="font-size:16px;"></i>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                    <div class="collapse show" id="collapse-customer">
                        <div class="form-group row d-flex align-items-center">
                            <label class="col-sm-2 col-form-label font-weight-bold">Tipe Komisi <span class="text-danger">*</span></label>
                            <div class="col-sm-4">
                                <select class="form-control" id="commission_id" name="commission_id" required>
                                    <option value="-1">Pilih Tipe Komisi</option>
                                </select>

                            </div>
                            <label class="col-sm-2 col-form-label font-weight-bold">Benefit Pelanggan <span class="text-danger">*</span></label>
                            <div class="col-sm-4">
                                <select class="form-control" id="benefit_customer_id" name="promo_id" required>
                                    <option value="-1">Pilih Benefit Pelanggan</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><hr></div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="col-form-label font-weight-bold">Penggunaan Terbatas <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="custom-control custom-switch mb-3">
                                            <input
                                                type="checkbox"
                                                class="custom-control-input"
                                                id="is_limited"
                                                name="is_limited"
                                                value="0"
                                                style="transform:scale(2)"
                                            >
                                            <label class="custom-control-label" for="is_limited"></label>
                                        </div>
                                        <input id="max_coupon_use" type="number" name="max_coupon_use" class="form-control d-none" value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold d-block">Kupon Per Pelanggan</label>
                                    <div class="col-sm-8">
                                        <div class="btn-group-toggle input-radio-group mb-3" data-toggle="buttons">
                                            <label
                                                class="btn btn-input-radio d-block active"
                                            >
                                                <input
                                                    type="radio"
                                                    name="is_more_than_one"
                                                    class="is_more_than_one"
                                                    value="1"
                                                >
                                                <span class="text-center">1 Kali</span>
                                            </label>
                                            <label
                                                class="btn btn-input-radio d-block"
                                            >
                                                <input
                                                    type="radio"
                                                    name="is_more_than_one"
                                                    class="is_more_than_one"
                                                    value="2"
                                                >
                                                <span class="text-center">Lebih dari 1 Kali</span>
                                            </label>
                                        </div>
                                        <input type="number" id="max_coupon_use_per_customer" name="max_coupon_use_per_customer" class="form-control d-none" value="1">
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="col-form-label font-weight-bold d-block">Berlakukan untuk semua pelanggan ?</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="custom-control custom-switch">
                                            <input
                                                type="checkbox"
                                                class="custom-control-input"
                                                id="all_customer"
                                                name="all_customer"
                                                value="1"
                                                style="transform:scale(2)"
                                                checked
                                            >
                                            <label class="custom-control-label" for="all_customer"></label>
                                        </div>
                                    </div>
                                    <div id="customer_criteria_wrapper" style="display:none;">
                                        <div id="customer_name_wrapper" style="background-color:#FBFBFB;border:1px dashed #C4C4C4;border-radius:10px;" class="p-4 mb-4">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="col-form-label font-weight-bold">Pilih dari daftar pelanggan</label>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="custom-control custom-switch">
                                                        <input
                                                            type="checkbox"
                                                            class="custom-control-input"
                                                            id="is_specific_customer"
                                                            name="is_specific_customer"
                                                            value="0"
                                                            style="transform:scale(2)"
                                                        >
                                                        <label class="custom-control-label" for="is_specific_customer"></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mt-1">

                                                </div>
                                                <div class="col-md-6 customer_keyword">
                                                    <label class="col-form-label font-weight-bold">Referral akan berlaku pada pelanggan yang memiliki nama ?</label>
                                                </div>
                                                <div class="col-md-6 customer_keyword">
                                                    <input type="text" class="form-control customer_name" name="customer_name" data-role="tagsinput" disabled>
                                                </div>
                                                <div class="col-md-12 mt-1">

                                                </div>
                                                <div class="col-md-6 customer_list" style="display: none">
                                                    <label class="col-form-label font-weight-bold">Pilih pelanggan dari data yang ada didalam sistem</label>
                                                </div>
                                                <div class="col-md-6 customer_list" style="display: none">
                                                    <select class="form-control" name="sc_customer_id" id="sc_customer_id" multiple disabled>
                                                    </select>
                                                </div>
                                                <div class="col-md-12 mt-1">

                                                </div>
                                                <div class="col-md-6 customer_birth_date_wrapper">
                                                    <label class="col-form-label font-weight-bold d-block">Referral akan berlaku pada pelanggan yang sedang berulang tahun tanggal?</label>
                                                </div>
                                                <div class="col-md-6 customer_birth_date_wrapper">
                                                    <input
                                                        type="date"
                                                        class="form-control form-control-sm customer_birth_date"
                                                        name="customer_birth_date"
                                                        placeholder="Tanggal Lahir Pelanggan"
                                                        disabled
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    @if(count(get_cabang())>1)
                                        <div class="col-md-4">
                                            <label class="col-form-label font-weight-bold d-block">Berlakukan untuk Semua Cabang ?</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="custom-control custom-switch">
                                                <input
                                                    type="checkbox"
                                                    class="custom-control-input check-bank"
                                                    id="all_branch"
                                                    name="all_branch"
                                                    value="0"
                                                    style="transform:scale(2)"
                                                >
                                                <label class="custom-control-label" for="all_branch"></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 option-branch-wrapper">
                                            <label class="col-form-label font-weight-bold d-block">Berlaku untuk cabang ?</label>
                                        </div>
                                        <div class="col-sm-8 option-branch-wrapper">
                                            <select name="branch_id" class="form-control js-example-basic-single" id="branch_id" multiple="multiple">
                                                @foreach(get_cabang() as $key => $item)
                                                    @if($item->id != merchant_id())
                                                        <option value="{{$item->id}}"  data-name="{{$item->nama_cabang}}">{{$item->nama_cabang}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="row mt-5">
                        <div class="col-md-12 mb-4 text-right">
                            <input type="hidden" id="is_type" name="is_type" value="0">
                            <input type="hidden" id="bonus_type" name="bonus_type" value="0">
                            <a href="{{route('merchant.toko.referral.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                            <button class="btn btn-success" id="btn-save"><i class="fa fa-save mr-2"></i> Simpan Referral</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('js')
    <script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script>
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        $("#customer").select2();
        $("#category_customer").select2({
            placeholder: "--- Pilih Kategori Pelanggan ---"
        });
        $("#branch_id").select2();
        $("#all_customer").on('change', function(){
            if($(this).prop('checked')==true){
                $(this).val(1);
                $(".customer_name").prop("disabled", true);
                $(".customer_birth_date").prop("disabled", true);
                $("#sc_customer_id").prop("disabled", true);
                $("#customer_criteria_wrapper").hide();
            } else {
                $(this).val(0);
                $(".customer_name").prop("disabled", false);
                $(".customer_birth_date").prop("disabled", false);
                $("#sc_customer_id").prop("disabled", false);
                $("#customer_criteria_wrapper").show();
            }
        });

        $("#all_branch").on('change', function(){
            if($(this).prop('checked')==true){
                $(this).val(1);
                $("#branch_id").prop("disabled", true);
                $(".option-branch-wrapper").hide();
            } else {
                $(this).val(0);
                $(".option-branch-wrapper").show();
                $("#branch_id").prop("disabled", false);
            }
        });

        $("#is_specific_customer").on('change', function(){
            if($(this).prop('checked') == true)
            {
                $(this).val(1);
                $(".customer_keyword").hide();
                $(".customer_list").show();
                $(".customer_birth_date_wrapper").hide();

            } else {
                $(this).val(0);
                $(".customer_keyword").show();
                $(".customer_list").hide();
                $(".customer_birth_date_wrapper").show();
            }
        })

        $("#is_limited").on('change', function(){
            if($(this).prop('checked') == true){
                $(this).val(1);
                $("#max_coupon_use").removeClass("d-none");

            } else {
                $(this).val(0);
                $("#max_coupon_use").addClass("d-none");
            }
        });

        $(".is_more_than_one").on("change", function(){
            let val = $(this).val();
            if(val == 1){
                $("#max_coupon_use_per_customer").addClass("d-none");
            } else {
                $("#max_coupon_use_per_customer").removeClass("d-none");
            }
        });




        $(document).ready(function () {
            getCustomer()
            getNonEmployee()
            getCommission()
            getPromo()
            $("#is_limited").val(0)
            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');

                var all_branch = data.get('all_branch');


                if(all_branch === "null" || all_branch === null){
                    data.set('all_branch', 0);
                }

                var is_all_customer = ($("#all_customer").val() == 1) ? 1:0;
                var is_specific_customer = ($("#is_specific_customer").val() == 1)? 1:0;

                var customer_ids = ($("#sc_customer_id").select2('data'))?$("#sc_customer_id").select2('data'):[];
                var specific_customer_id = [...customer_ids].map(item => {
                    return {
                        "sc_customer_id":parseInt(item.id)
                    }
                });

                var customer_name_val = ($(".customer_name").val())? $(".customer_name").val().split(","):[];

                var customer_name = [...customer_name_val].map(item => {
                    return {
                        "name":item
                    }
                });

                var reqCustomerTerms = {
                    "birth_date": ($("#all_customer").val() == 1)? "-1":($(".customer_birth_date").val())? moment($(".customer_birth_date").val()).format('MM-DD'):"-1",
                    "birth_year":($("#all_customer").val() == 1)? "-1":($(".customer_birth_date").val())? moment($(".customer_birth_date").val()).format('YYYY'):"-1",
                    "customer_name": ($("#all_customer").val() == 1)? []:customer_name,
                    "specific_customer_id": ($("#all_customer").val() == 1)? []:specific_customer_id
                }

                var branch = ($("#branch_id").select2("data"))?$("#branch_id").select2("data"):[];

                var assign_to_branch = branch.map(item => {
                    return parseInt(item.id);
                });

                data.append('activation_type', 1);
                data.append("req_customer_terms", JSON.stringify(reqCustomerTerms));
                data.append('assign_to_branch', JSON.stringify(assign_to_branch));
                data.append('is_all_customer', is_all_customer);
                data.set('is_specific_customer', is_specific_customer);

                ajaxTransfer("{{route('merchant.toko.referral.save')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        })

        function getCustomer()
        {
            $("#sc_customer_id").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.ajax.share-data.customer-list')}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }

        function getNonEmployee()
        {

            $.ajax({
                url: "{{route('merchant.ajax.share-data.employee-list')}}?md_merchant_id={{merchant_id()}}&is_non_employee=1",
                type: 'get',
                dataType: 'json',
                success: function (jsonObject){
                    $("#affiliator_id").select2({
                        placeholder:"Pilih Affiliator",
                        data:jsonObject
                    });
                }
            });
        }

        function getCommission()
        {
            $("#commission_id").empty()
            let staffId= $("#affiliator_id").val()

            $.ajax({
                url: "{{route('merchant.ajax.share-data.commission-list')}}?md_merchant_id={{merchant_id()}}&is_for_employee=0&staff_id="+staffId,
                type: 'get',
                dataType: 'json',
                success: function (jsonObject){
                    $("#commission_id").select2({
                        placeholder:"Pilih Tipe Komisi",
                        data:jsonObject
                    });
                }
            });

        }

        function getPromo()
        {
            $.ajax({
                url: "{{route('merchant.ajax.share-data.promo-list')}}?md_merchant_id={{merchant_id()}}&is_coupon=0&is_affiliate=0",
                type: 'get',
                dataType: 'json',
                success: function (jsonObject){
                    $("#benefit_customer_id").select2({
                        placeholder:"Pilih Benefit Pelanggan",
                        data:jsonObject
                    });
                }
            });
        }


        $("#affiliator_id").on('select2:open', () => {
            $(".select2-results:not(:has(a))").append('<a id="add-other" onclick="loadModal(this)" target="{{route("merchant.toko.staff.add-non-employee")}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Affiliator</b></a>');
        });
        $(document).on('click','#add-other', function(e){
            $("#affiliator_id").select2('close');
        });


    </script>
@endsection

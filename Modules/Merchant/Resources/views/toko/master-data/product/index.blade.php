@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.product.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Lihat produk secara rinci sesuai dengan kategori dan merek produk usahamu</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4">
            <div class="card-body">
                <form id="form-product" onsubmit="return false">
                <div class="row" style="margin-bottom: 10px">
                    {!! $add !!}
                </div>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                </form>
                <div id="output-sale-order">
                    @include('merchant::toko.master-data.product.list')
                </div>
            </div>
        </div>
    </div>
    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" class="px-30" onsubmit="return false">
            <div class="row d-flex align-items-center">
                <div class="col-md-12 mb-3">
                    <label>Pilih Jenis Produk :</label>
                    <select name="is_with_stock" id="is_with_stock" class="form-control form-control-sm">
                        <option value="-1">Semua Jenis Produk</option>
                        <option value="1">Barang</option>
                        <option value="0">Jasa</option>
                    </select>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Tipe Produk :</label>
                    <select name="md_sc_product_type_id" id="md_sc_product_type_id" class="form-control form-control-sm">
                        <option value="-1">Semua Tipe Produk</option>
                        @foreach($productType as $key =>$item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Kategori :</label>
                    <select name="sc_product_category_id" id="sc_product_category_id" class="form-control form-control-sm">
                        <option value="-1">Semua Kategori</option>
                        @foreach($sc_product_category_id as $key =>$item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12 mt-3">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                    <input type="hidden" name="key" value="{{$key_val}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a onclick="exportData()" class="btn btn-light text-primary btn-block py-2 px-4" style="background: #DEF1FF!important"><i class="fa fa-download mr-2"></i><span>Download</span></a>

                </div>
            </div>

        </form>

    </div>

@endsection

@section('js')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.product.delete')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }
        function reloadDataTable(isReload=0) {
            ajaxDataTable('#table-data-product', 1, "{{route('merchant.toko.product.datatable')}}?key_val={{$key_val}}", [
                    @foreach($tableColumns as $key =>$item)
                    @if($tableColumns[$key]=='action')
                {
                    data: '{{$tableColumns[$key]}}',
                    name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                @endif
                @endforeach
            ],0);
        }
        $(document).ready(function() {
            $('#is_with_stock').select2();
            $('#sc_product_category_id').select2();
            $('#md_sc_product_type_id').select2();
            $('#form-filter-sale-order').submit(function () {
                var data = getFormData('form-filter-sale-order');
                ajaxTransfer("{{route('merchant.toko.product.reload-data')}}", data, '#output-sale-order');
                showFilter('btn-show-filter3', 'form-filter3');
            });
           reloadDataTable(0);
        })
        function exportData() {
            var info = $('#table-data-product_info').html().split('dari');
            if (info.length < 2) {
                return false;
            }

            var dataCount = parseInt(info[1].replace('entri', '').replace(',', '').replace(' ', ''));
            var seconds = parseFloat(dataCount / 66).toFixed(3);
            var estimation = '';

            if (seconds < 60) {
                estimation = seconds + ' detik';
            } else {
                var minute = parseInt(seconds / 60);
                seconds = seconds % 60;
                estimation = minute + ' menit ' + seconds + ' detik';
            }

            modalConfirm('Export Data', 'Proses export data sejumlah ' + dataCount + ' data membutuhkan estimasi waktu ' + estimation + '. Lanjutkan proses export data?', function () {
                const search = document.querySelector('#table-data-product_filter label input[type="search"]');
                var data = getFormData('form-product');
                if(search.value){
                    data.append('search', search.value);
                }
                ajaxTransfer("{{route('merchant.toko.product.export-data')}}", data, '#modal-output');
            });

        }

        function exportDataGlobalCategory() {
            modalConfirm('Export Data', 'Lanjutkan proses export data kategori umum?', function () {
                var data = getFormData('form-product');
                ajaxTransfer("{{route('merchant.toko.product.export-global-category')}}", data, '#modal-output');
            });
        }
    </script>
@endsection

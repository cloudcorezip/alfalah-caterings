<div class="table-responsive">
    <table class="table table-custom" id="table-data-product" width="100%" cellspacing="0">
        <thead>
        <tr>
            @foreach($tableColumns as $key =>$item)
                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<script>
    function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
            ajaxTransfer("{{route('merchant.toko.product.delete')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    }
    function reloadDataTable(isReload=0)
    {
        ajaxDataTable('#table-data-product', 1, "{{route('merchant.toko.product.datatable')}}?key_val={{$key_val}}&sk={{$searchKey}}", [
                @foreach($tableColumns as $key =>$item)
                @if($tableColumns[$key]=='action')
            {
                data: '{{$tableColumns[$key]}}',
                name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
                @else
            {
                data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                orderable: false,
                searchable: false
            },
            @endif
            @endforeach
        ],0);
    }
    $(document).ready(function() {
       reloadDataTable(0)
    })
</script>

<div id="result-form-konten-multi-varian"></div>
<form onsubmit="return false;" id="form-konten-multi-varian" class='form-horizontal form-konten' backdrop="">

    <div id="type_varian_wrapper">
        
    </div>
    <div class="row mb-3">
        <div class="col-md-12 py-2 px-4">
            <a class="btn btn-outline-warning btn-block rounded-0" onclick="showVarianType(this)">+ Tambah Varian Lain</a>
        </div>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Harga Beli</label>
        <input type="text" class="form-control amount_currency" id="purchase_price" name="purchase_price" value="{{currency($data->purchase_price)}}">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Harga Jual</label>
        <input type="text" class="form-control amount_currency" id="selling_price" name="selling_price" value="{{currency($data->selling_price)}}">
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='sc_product_id' value='{{$product->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>

    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}} Varian</h4>
                                        <span class="span-text">Untuk menambah multi satuan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

<script>
    let id = "{{$data->id}}";

    let dataMultiVarian = (!id) ? [] : JSON.parse(@json($data->multi_varian));
    let dataOptionVarian = @json($varian);
    let tmpOptionVarian = @json($varian);
    let removedMultiId = [];
    let iMultiVarian = 0;
  
    const loadDataVarian = () => {
        $('#type_varian_wrapper').empty();

        let html = "";
        
        dataMultiVarian = dataMultiVarian.map((item, index) => {
            return {
                multi_id:'smv-'+(index+1),
                ...item
            }
        });

        dataMultiVarian.forEach((item, index) => {
            html += '<div class="row mb-3 type_varian_row" id="mvw-'+(index + 1)+'">'+
                        '<div class="col-md-12 py-2 px-4" style="background-color:#f6f6f6;">'+
                            '<div class="form-group">'+
                                '<label class="font-weight-bold" for="exampleInputPassword1">Tipe Varian</label>'+
                                '<button data-mv="MV-0" data-id="smv-'+(index + 1)+'" data-row="'+(index + 1)+'" data-target="#mvw-'+(index + 1)+'" type="button" class="close btn-remove-mvw" aria-label="Close">'+
                                    '<span aria-hidden="true">&times;</span>'+
                                '</button>'+
                                '<select data-id="smv-'+(index + 1)+'" class="form-control form-control-sm tipe_varian" name="tipe_varian" id="tipe_varian'+(index + 1)+'" required>'+
                                    '<option></option>'+
                                    dataOptionVarian.map(i => {
                                        if(i.id == item.id){
                                            return '<option selected value="'+item.id+'">'+item.varian_type+'</option>';
                                        } else {
                                            return '<option value="'+i.id+'">'+i.name+'</option>';
                                        }
                                    })+
                                    
                                '</select>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="font-weight-bold" for="exampleInputPassword1">Pilihan</label>'+
                                '<input data-id="smv-'+(index + 1)+'" type="text" class="form-control varian_name" name="varian_name" placeholder="Masukkan Pilihan Variasi, contoh: merah, dll" value="'+item.varian_name+'" required>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

            $('#type_varian_wrapper').append(html);
            $('#tipe_varian'+(index + 1)).select2({
                placeholder:'--- Pilih Tipe Varian ---'
            });
            html="";
        });
    }

    const resetVarianForm = () => {
        $('#type_varian_wrapper').empty();
        const typeVarianRow = [...document.querySelectorAll('.type_varian_row')];

        dataOptionVarian = [...tmpOptionVarian];
        selectedTypeVarianId = [];
        let countRow = typeVarianRow.length + 1;

        let html = '<div class="row mb-3 type_varian_row" id="mvw-'+countRow+'">'+
                        '<div class="col-md-12 py-2 px-4" style="background-color:#f6f6f6;">'+
                            '<div class="form-group">'+
                                '<label class="font-weight-bold" for="exampleInputPassword1">Tipe Varian '+countRow+'</label>'+
                                '<button data-mv="MV-0" data-id="smv-'+countRow+'" data-row="'+countRow+'" data-target="#mvw-'+countRow+'" type="button" class="close btn-remove-mvw" aria-label="Close">'+
                                    '<span aria-hidden="true">&times;</span>'+
                                '</button>'+
                                '<select data-id="smv-'+countRow+'" class="form-control form-control-sm tipe_varian" name="tipe_varian" id="tipe_varian'+countRow+'" required>'+
                                    '<option></option>'+
                                    dataOptionVarian.map(item => {
                                        return '<option value="'+item.id+'">'+item.name+'</option>'
                                    })+
                                    
                                '</select>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="font-weight-bold" for="exampleInputPassword1">Pilihan</label>'+
                                '<input data-id="smv-'+countRow+'" type="text" class="form-control varian_name" name="varian_name" placeholder="Masukkan Pilihan Variasi, contoh: merah, dll" required>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

        $('#type_varian_wrapper').append(html);
        $('#tipe_varian'+countRow).select2({
            placeholder:'--- Pilih Tipe Varian ---'
        });
    }

    
    if(!id){
        resetVarianForm();
    } else {
        loadDataVarian();
    }

    const showVarianType = (e) => {

        const typeVarianRow = [...document.querySelectorAll('.type_varian_row')];

        const maxRow = "{{count($varian)}}";
        let selectRow = typeVarianRow.length + 1;

        let countRow;
        if(removedMultiId.length == 0){
            countRow = typeVarianRow.length + 1;
        } else {
            countRow = removedMultiId.shift();
        }

        if(selectRow > parseInt(maxRow)){
            return;
        }

        let html = '<div class="row mb-3 type_varian_row" id="mvw-'+countRow+'">'+
                        '<div class="col-md-12 py-2 px-4" style="background-color:#f6f6f6;">'+
                            '<div class="form-group">'+
                                '<label class="font-weight-bold" for="exampleInputPassword1">Tipe Varian</label>'+
                                '<button data-mv="MV-'+iMultiVarian+'" data-id="smv-'+countRow+'" data-row="'+countRow+'" data-target="#mvw-'+countRow+'" type="button" class="close btn-remove-mvw" aria-label="Close">'+
                                    '<span aria-hidden="true">&times;</span>'+
                                '</button>'+
                                '<select data-id="smv-'+countRow+'" class="form-control form-control-sm tipe_varian" name="tipe_varian" id="tipe_varian'+countRow+'" required>'+
                                    '<option></option>'+
                                    dataOptionVarian.map(item => {
                                        return '<option value="'+item.id+'">'+item.name+'</option>'
                                    })+
                                    
                                '</select>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="font-weight-bold" for="exampleInputPassword1">Pilihan</label>'+
                                '<input data-id="smv-'+countRow+'" type="text" class="form-control varian_name" name="varian_name" placeholder="Masukkan Pilihan Variasi, contoh: merah, dll" required>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

        $('#type_varian_wrapper').append(html);
        $('#tipe_varian'+countRow).select2({
            placeholder:'--- Pilih Tipe Varian ---'
        });
    }

    $(document).on('change','.tipe_varian', function(){
        let value = $(this).find(':selected').val();
        let textValue = $(this).find(':selected').text();
        let dataId = $(this).attr('data-id');

        const indexRow =  dataMultiVarian.findIndex(item => item.multi_id == dataId);

        if(indexRow < 0){
            dataMultiVarian.push({   
                multi_id: dataId,
                id: value,
                varian_type: textValue
            });

        } else {
            dataMultiVarian = dataMultiVarian.map(item => {
                if(item.multi_id == dataId){
                    return {
                        ...item,
                        multi_id: dataId,
                        id: value,
                        varian_type: textValue
                    }
                } else {
                    return {
                        ...item
                    }
                }
            });
            
        }

    });

    $(document).on('change', '.varian_name', function(){
        let value = $(this).val();
        let dataId = $(this).attr('data-id');

        const index =  dataMultiVarian.findIndex(item => item.multi_id == dataId);

        if(index < 0){
            dataMultiVarian.push({
                multi_id: dataId,
                varian_name: value
            });
        } else {
            dataMultiVarian = dataMultiVarian.map(item => {
                if(item.multi_id == dataId){
                    return {
                        ...item,
                        multi_id: dataId,
                        varian_name: value
                    }
                } else {
                    return {
                       ...item
                    }
                }
            });
            
        }

    });

    $(document).on('click', '.btn-remove-mvw', function(){
        const typeVarianRow = [...document.querySelectorAll('.type_varian_row')];
        let target = $(this).attr("data-target");
        let row = $(this).attr('data-row');
        let dataId = $(this).attr("data-id");

        removedMultiId.push(row);

        if(dataMultiVarian.length > 0){
            dataMultiVarian.forEach((item, index) => {
                if(item.multi_id == dataId){
                    dataMultiVarian.splice(index, 1);
                }
            });
        }

        $(target).remove();
    });

    $(document).ready(function () {
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        $('#form-konten-multi-varian').submit(function () {
            var data = getFormData('form-konten-multi-varian');

            let reqMultiVarian = dataMultiVarian.map(item => {
                return {...item};
            });
            
            let label = '';
            reqMultiVarian.forEach(item => {
                delete item.multi_id;
                label += item.varian_type +' '+ item.varian_name + ', ';
            });

            data.append('multi_varian', JSON.stringify(reqMultiVarian));
            data.append('label', label);

            ajaxTransfer("{{route('merchant.toko.product.multi-varian.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })

    });
</script>
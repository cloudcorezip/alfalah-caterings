<div id="result-form-konten-price-type"></div>

<form onsubmit="return false;" id="form-konten-price-type" class='form-horizontal form-konten' backdrop="">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1">Tipe harga</label>
                <input type="text" class="form-control" name="level_name" placeholder="Tipe Harga" value="{{$data->level_name}}" required>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1">Harga Jual</label>
                <input type="text" class="form-control amount_currency" name="selling_price" value="{{currency($data->selling_price)}}">
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='sc_product_id' value='{{$sc_product_id }}'>

    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten-price-type').submit(function () {
            var data = getFormData('form-konten-price-type');
            ajaxTransfer("{{route('merchant.toko.product.selling-price.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    });


    $(document).ready(function(){
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Tipe Harga</h4>
                                        <span class="span-text">Untuk menambah tipe harga, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });



</script>

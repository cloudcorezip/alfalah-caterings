<div class="alert alert-warning text-center" role="alert">
    Perhatian,jika kamu ingin memasukkan data multi satuan,mulailah dari yang terkecil agar sistem dengan mudah melakukan pengambilan data saat penjualan
  </div>
<div id="result-form-konten-multi-unit"></div>
<form onsubmit="return false;" id="form-konten-multi-unit" class='form-horizontal form-konten' backdrop="">

    <div class="form-group">
        <div class="form-group">
            <label for="exampleInputPassword1">Tipe Multi Satuan</label>
            <select  class="form-control form-control-sm" name="md_unit_id" id="satuan" required>
                <option></option>
                @foreach ($unit as $b)
                <option @if ($b->id==$data->md_unit_id)
                    selected
                @endif value="{{$b->id}}">{{$b->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1" id="konversi"></label>
        @if($data->md_unit_id == $product->md_unit_id)
        <input type="text" class="form-control" id="quantity" name="quantity" value="{{$data->quantity}}" required disabled onkeypress="return isNumber(event)">
        @else
        <input type="text" class="form-control" id="quantity" name="quantity" value="{{$data->quantity}}" required onkeypress="return isNumber(event)">
        @endif
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Harga Jual</label>
        <input type="text" class="form-control amount_currency" id="price" name="price" value="{{currency($data->price)}}" required>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' type="number" id="selling_price" name='selling_price' value='{{currency($product->selling_price)}}'>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='sc_product_id' value='{{$product->id }}'>

    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    @if($data->md_unit_id == $product->md_unit_id)
        <input type="hidden" class="form-control" name="is_default_unit" value="1">
    @endif
</form>

<script>
    $(document).ready(function () {
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        $("#satuan").select2({
            placeholder:'--- Pilih Satuan ---'
        });


        $( "#quantity" ).change(function() {
            var price =parseFloat($("#selling_price").val());
            var quantity =parseFloat($("#quantity").val());
            var result = price*quantity;
            $( "#price" ).val(currencyFormat(result));
        });
        $('#form-konten-multi-unit').submit(function () {
            var data = getFormData('form-konten-multi-unit');
            ajaxTransfer("{{route('merchant.toko.product.multi-unit.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })


    });

    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');

    });

    $(document).ready(function(){
        let units = @json($unit);
        let data = @json($data);

        let product_unit_id = "{{$product->md_unit_id}}";
        let unit_id = "{{$data->md_unit_id}}";

        const unitName = "{{$product->getUnit->name}}";
        let unit = units.filter(item => {
            return item.name.toLowerCase().trim() != unitName.toLowerCase().trim();
        });

        let html = "<option></option>";

        unit.forEach(item => {
            if(item.id == data.md_unit_id){
                html += "<option selected value="+item.id+">"+item.name+"</option>";
            } else {
                html += "<option value="+item.id+">"+item.name+"</option>";
            }
        });


        if(product_unit_id != unit_id){
            $('#satuan option').remove();
            $('#satuan').append(html);
        }
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Multi Satuan</h4>
                                        <span class="span-text">Untuk menambah multi satuan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);

        let typeMulti = ($("#satuan").select2('data'))? $("#satuan").select2('data'):[];

        if(typeMulti.length>0)
        {
            $("#konversi").text("Konversi dari  "+typeMulti[0]['text']+" ke {{is_null($product->getUnit)?'':$product->getUnit->name}}")

        }else{
            $("#konversi").text("Konversi dari   kr{{is_null($product->getUnit)?'':$product->getUnit->name}}")

        }

    });

    $('#satuan').change(function(){
        let typeMulti = ($("#satuan").select2('data'))? $("#satuan").select2('data'):[];

        if(typeMulti.length>0)
        {
            $("#konversi").text("Konversi dari  "+typeMulti[0]['text']+" ke {{is_null($product->getUnit)?'':$product->getUnit->name}}")

        }else{
            $("#konversi").text("Konversi dari   ke {{is_null($product->getUnit)?'':$product->getUnit->name}}")

        }

    });

    function isNumber(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        if (key.length == 0) return;
        var regex = /^[0-9.\b]+$/;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }

</script>

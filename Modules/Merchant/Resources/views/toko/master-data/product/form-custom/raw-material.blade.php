<div id="result-form-konten-raw-material"></div>

<form onsubmit="return false;" id="form-konten-raw-material" class='form-horizontal form-konten' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1">Quantity/Jumlah(untuk decimal/koma memakai titik)</label>
        <input type="number" class="form-control form-control-sm" name="quantity" step="0.001" min="0" placeholder="Qty" value="{{is_null($data->quantity)?0:$data->quantity}}" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Bahan Baku</label>
        <select name="child_sc_product_id" class="form-control form-control-sm" id="product">
            @foreach($raw as $key =>$item)
             <option value="{{$item->id}}" @if($item->id==$data->child_sc_product_id) selected @endif>{{$item->name}}/{{$item->unit_name}}</option>
            @endforeach
        </select>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='sc_product_id' value='{{$sc_product_id }}'>

    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $("#product").select2();
        $('#form-konten-raw-material').submit(function () {
            var data = getFormData('form-konten-raw-material');
            ajaxTransfer("{{route('merchant.toko.product.raw-material.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    })


</script>

<script>
     $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Bahan Baku</h4>
                                        <span class="span-text">Untuk menambah bahan baku, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });
</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>

        .fileinput-button {
            position: relative;
            overflow: hidden;
        }

        .fileinput-button input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            opacity: 0;
            -ms-filter: "alpha(opacity=0)";
            font-size: 200px;
            direction: ltr;
            cursor: pointer;
        }
        .thumb {
            height: 80px;
            width: 100px;
            border: 1px solid #ddd;
        }

        ul.thumb-Images li {
            width: 120px;
            float: left;
            display: inline-block;
            vertical-align: top;
            height: 120px;
        }

        .img-wrap {
            position: relative;
            display: inline-block;
            font-size: 0;
        }

        .img-wrap .close {
            position: absolute;
            top: 2px;
            right: 2px;
            z-index: 100;
            background-color: #d0e5f5;
            padding: 5px 2px 2px;
            color: #000;
            font-weight: bolder;
            cursor: pointer;
            opacity: 0.5;
            font-size: 23px;
            line-height: 10px;
            border-radius: 50%;
        }

        .img-wrap:hover .close {
            opacity: 1;
            background-color: #ff0000;
        }

        .FileNameCaptionStyle {
            font-size: 12px;
        }
    </style>

    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.product.index')}}">Produk</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>Edit Data Produk</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa mengisi data di bawah ini untuk menambah produk. Pastikan semua kolom sudah terisi dengan benar</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

                    <h4 class="font-weight-bold mb-3">{{$title}}</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="result-form-konten"></div>
                        </div>
                        <div class="col-md-12 @if($data->md_sc_product_type_id != 2) d-none @endif" id="alert-barang-produksi">
                            <div class="alert alert-warning" role="alert">
                                Perhatian ! Untuk menambahkan harga jual, dapat dilakukan di menu produksi produk.
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label class="col-form-label font-weight-bold d-block">Foto Produk</label>
                                    <small style="color: #A4A4A4;">Pilih beberapa gambar! Format gambar jpg. jpeg. png dan ukuran minimum 500px x 500px</small>
                                </div>
                                <div class="col-sm-10">
                                    <input
                                        style="display: none"
                                        type="file"
                                        class="form-control"
                                        name="files[]" id="files" multiple  accept="image/jpeg, image/png, image/gif">
                                    <div class="d-flex align-items-center preview-image__product__wrapper" id="foto-preview" onclick="browseImage('#files')">
                                        <div class="product-plus__icon"></div>
                                    </div>
                                    <br>
                                    <output id="Filelist" style="margin-top: 10px;margin-bottom: 40px">
                                        <ul class="thumb-Images" id="imgList">
                                            @if(!is_null($data->product_photos))
                                                @foreach(json_decode($data->product_photos) as $key => $item)
                                                    <li>
                                                        <div class="img-wrap">
                                                            <span class="close">×</span>
                                                            <img class="thumb" src="{{env('S3_URL')}}{{$item->fileName}}" title="{{explode('/',$item->fileName)[5]}}" data-id="{{explode('/',$item->fileName)[5]}}" data-num="{{$item->fileName}}">
                                                        </div>
                                                        <div class="FileNameCaptionStyle">{{explode('/',$item->fileName)[5]}}</div>
                                                        <input type="hidden" name="old_files[]" value="{{$item->fileName}}">
                                                    </li>
                                                @endforeach
                                            @else
                                                @if(!is_null($data->foto))
                                                    <li>
                                                        <div class="img-wrap">
                                                            <span class="close">×</span>
                                                            <img class="thumb" src="{{env('S3_URL')}}{{$data->foto}}" title="{{explode('/',$data->foto)[5]}}" data-id="{{explode('/',$data->foto)[5]}}" data-num="{{$data->foto}}">
                                                        </div>
                                                        <div class="FileNameCaptionStyle">{{explode('/',$data->foto)[5]}}</div>
                                                        <input type="hidden" name="old_files[]" value="{{$data->foto}}">
                                                    </li>
                                                @endif
                                            @endif
                                        </ul>


                                    </output>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"></div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Nama Produk <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" value="{{$data->name}}" required placeholder="Nama Produk">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Kode Produk <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="code" value="{{(is_null($data->id))?$code:$data->code}}" required>
                                </div>
                            </div>
                        </div>
                        @if($is_goods==1)
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Tipe Produk <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <select
                                            name="md_sc_product_type_id"
                                            class="form-control"
                                            id="product_type"
                                            required
                                            disabled
                                        >
                                            <option selected value="{{$data->md_sc_product_type_id}}">{{$data->getType->name}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Kategori Produk <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <select name="sc_product_category_id" class="form-control" id="sc_product_category_id">
                                        <option selected value="{{$data->sc_product_category_id}}">{{$data->getCategory->name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @if($is_goods==1)
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Merk</label>
                                    <div class="col-sm-8">
                                        <select name="sc_merk_id" class="form-control" id="sc_merk_id">
                                            <option></option>
                                            @if(!is_null($data->sc_merk_id))
                                            <option selected value="{{$data->sc_merk_id}}">{{$data->getMerk->name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($is_goods==1)
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Harga Beli <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control amount_currency" name="purchase_price" step="0.001" min="0"  value="{{is_null($data->purchase_price)?0:number_format((float)$data->purchase_price, 2, '.', '')}}"  id="harga-beli" required>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Harga Jual <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control amount_currency" name="selling_price" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:number_format((float)$data->selling_price, 2, '.', '')}}" id="harga-jual" required>
                                </div>
                            </div>
                            @if ($is_goods==1)
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Barang Titipan (Konsinyasi) ? <span class="text-danger"></span></label>
                                    <div class="col-md-2">
                                        <input value="{{($data->is_consignment==1)?1:0}}" type="hidden" name="is_consignment" id="consignment">
                                        <label>
                                            <input type="checkbox" {{($data->is_consignment==1)?'checked':''}} id="is_consignment"></label>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Satuan <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <select name="md_unit_id" class="form-control" id="md_unit_id">
                                        <option selected value="{{$data->md_unit_id}}">{{$data->getUnit->name}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Duplikasi ke Cabang Lain</label>
                                <div class="col-sm-8">
                                    <select name="md_merchant_id" id="branch" class="form-control mr-2 js-example-responsive js-states" multiple="multiple">
                                        @foreach (get_cabang() as $key => $b)
                                        @if($b->id!=merchant_id())
                                            <option value="{{$b->id}}"
                                            @if(!is_null($data->assign_to_branch))
                                                @if(in_array($b->id, json_decode($data->assign_to_branch)))
                                                    selected
                                                @endif
                                            @endif
                                            >{{$b->nama_cabang}}</option>
                                        @endif

                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label font-weight-bold">Deskripsi</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" style="min-height: 250px" name="description" id="desc" placeholder="Tambahkan deskripsi produk">{!!  $data->description !!}</textarea>
                                    <input type="hidden" value="{{$is_goods}}" name="is_with_stock">
                                    @if($is_goods==0)
                                        <input type="hidden" value="0" name="purchase_price">
                                        <input type="hidden" value="1" name="md_sc_product_type_id">

                                    @endif
                                    <input type='hidden' name='id' value='{{$data->id }}'>
                                    <input type='hidden' name='is_with_purchase_price' value='0'>
                                    <input type="hidden" name="is_with_stock" value="{{$is_goods}}">
                                    <input type="hidden" name='is_unit' value='{{$is_unit}}'>
                                    <input type="hidden" name='is_price_type' value='{{$is_price_type}}'>
                                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                                </div>
                            </div>
                        </div>
                        @if($is_goods==1)
                            <div class="col-md-12">
                                <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;">
                                    <label class="col-sm-3 col-form-label font-weight-bold"><h5 style="font-weight: bold">Info Pengiriman</h5></label>
                                    <div class="col-sm-7"></div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label font-weight-bold">Berat (dalam gr)</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control amount_currency" name="weight" step="0.001" min="0"  value="{{is_null($data->weight)?0:$data->weight}}" required>
                                            </div>
                                            <label class="col-sm-2 col-form-label font-weight-bold">Panjang (dalam cm)</label>

                                            <div class="col-sm-4">
                                                <input type="text" class="form-control amount_currency" name="long" step="0.001" min="0"  value="{{is_null($data->long)?0:$data->long}}"  required>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label font-weight-bold">Tinggi (dalam cm)</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control amount_currency" name="hight" step="0.001" min="0"  value="{{is_null($data->hight)?0:$data->hight}}"  required>
                                            </div>
                                            <label class="col-sm-2 col-form-label font-weight-bold">Lebar (dalam cm)</label>

                                            <div class="col-sm-4">
                                                <input type="text" class="form-control amount_currency" name="wide" step="0.001" min="0"  value="{{is_null($data->wide)?0:$data->wide}}"  required>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label font-weight-bold">Isi</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" style="min-height: 250px" name="package_contents"  placeholder="Tambahkan isi paket">{{$data->package_contents}}</textarea>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif
                    </div>
                    <div class="col-md-12 text-right" style="display:none;">
                        <button class="btn btn-success" id="btn-submit-konten"><i class="fa fa-save mr-2"></i> Simpan Produk</button>
                    </div>

                </form>
                @if(($data->md_sc_product_type_id==1 && $is_unit == 0) || ($data->md_sc_product_type_id==2 && $data->selling_price > 0 && $is_unit == 0))
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">

                                <div class="row d-flex align-items-center">
                                    <div class="col-md-6">
                                        <h4 class="font-weight-bold">Tipe Harga</h4>
                                        <span style="color:#A4A4A4;">Tambahkan tipe harga untuk penjualan grosir,eceran atau harga order online (Gojek,Grab)</span>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                           data-toggle="collapse"
                                           href="#tipeHarga"
                                           role="button"
                                           aria-expanded="false"
                                           aria-controls="tipeHarga"
                                        >
                                            Tampilkan
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse" id="tipeHarga">
                                    <hr>
                                    <div class="row">
                                        {!! $add !!}
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-custom" id="table-data-2" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                @foreach($tableColumnsPriceType as $key =>$item)
                                                    <th>{{($tableColumnsPriceType[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumnsPriceType[$key]))}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif


                @if($data->md_sc_product_type_id==3)
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">

                                <div class="row d-flex align-items-center">
                                    <div class="col-md-6">
                                        <h4 class="font-weight-bold">Tipe Harga</h4>
                                        <span style="color:#A4A4A4;">Tambahkan tipe harga untuk penjualan grosir,eceran atau harga order online (Gojek,Grab)</span>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                           data-toggle="collapse"
                                           href="#tipeHarga"
                                           role="button"
                                           aria-expanded="false"
                                           aria-controls="tipeHarga"
                                        >
                                            Tampilkan
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse" id="tipeHarga">
                                    <hr>
                                    <div class="row">
                                        {!! $add !!}
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-custom" id="table-data-2" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                @foreach($tableColumnsPriceType as $key =>$item)
                                                    <th>{{($tableColumnsPriceType[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumnsPriceType[$key]))}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif


            @if(($data->is_with_stock == 1 && $data->md_sc_product_type_id == 1 && $is_price_type == 0) || ($data->md_sc_product_type_id==2 && $data->selling_price > 0 && $is_price_type == 0))
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">

                                <div class="row d-flex align-items-center">
                                    <div class="col-md-6">
                                        <h4 class="font-weight-bold">Multi Satuan</h4>
                                        <span style="color:#A4A4A4;">Perhatian,jika kamu ingin memasukkan data multi satuan,mulailah dari yang terkecil agar sistem dengan mudah melakukan pengambilan data saat penjualan</span>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                           data-toggle="collapse"
                                           href="#multiSatuan"
                                           role="button"
                                           aria-expanded="false"
                                           aria-controls="multiSatuan"
                                        >
                                            Tampilkan
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse" id="multiSatuan">
                                    <hr>
                                    <div class="row" style="margin-bottom: 10px">
                                        {!! $addUnit !!}
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table table-custom" id="table-data-4" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                @foreach($tableColumnsMultiUnit as $key =>$item)
                                                    <th>{{($tableColumnsMultiUnit[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumnsMultiUnit[$key]))}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif


                @if(($data->is_with_stock == 1 && $data->md_sc_product_type_id == 3 && $is_price_type == 0))
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">

                                <div class="row d-flex align-items-center">
                                    <div class="col-md-6">
                                        <h4 class="font-weight-bold">Multi Satuan</h4>
                                        <span style="color:#A4A4A4;">Perhatian,jika kamu ingin memasukkan data multi satuan,mulailah dari yang terkecil agar sistem dengan mudah melakukan pengambilan data saat penjualan</span>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                           data-toggle="collapse"
                                           href="#multiSatuan"
                                           role="button"
                                           aria-expanded="false"
                                           aria-controls="multiSatuan"
                                        >
                                            Tampilkan
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse" id="multiSatuan">
                                    <hr>
                                    <div class="row" style="margin-bottom: 10px">
                                        {!! $addUnit !!}
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table table-custom" id="table-data-4" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                @foreach($tableColumnsMultiUnit as $key =>$item)
                                                    <th>{{($tableColumnsMultiUnit[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumnsMultiUnit[$key]))}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            @if($data->md_sc_product_type_id==\App\Models\MasterData\SennaCashier\ProductType::RAW_MATERIAL || $data->md_sc_product_type_id==2)
                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">

                                <div class="row d-flex align-items-center">
                                    <div class="col-md-6">
                                        <h4 class="font-weight-bold">Bahan Baku</h4>
                                        <span style="color:#A4A4A4;">Tambahkan bahan baku untuk memproduksi produk.</span>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                           data-toggle="collapse"
                                           href="#bahanBaku"
                                           role="button"
                                           aria-expanded="false"
                                           aria-controls="bahanBaku"
                                        >
                                            Tampilkan
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse" id="bahanBaku">
                                    <hr>
                                    <div class="row" style="margin-bottom: 10px">
                                        {!! $addRaw !!}
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-custom" id="table-data-3" width="100%" cellspacing="0">
                                            <thead>
                                            <tr>
                                                @foreach($tableColumnsRawMaterial as $key =>$item)
                                                    <th>{{($tableColumnsRawMaterial[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumnsRawMaterial[$key]))}}</th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                @endif

                @if($is_goods==0)
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-12">
                        <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">

                            <div class="row d-flex align-items-center">
                                <div class="col-md-6">
                                    <h4 class="font-weight-bold">Multi Satuan</h4>
                                    <span style="color:#A4A4A4;">Perhatian,jika kamu ingin memasukkan data multi satuan,mulailah dari yang terkecil agar sistem dengan mudah melakukan pengambilan data saat penjualan</span>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                       data-toggle="collapse"
                                       href="#multiSatuan"
                                       role="button"
                                       aria-expanded="false"
                                       aria-controls="multiSatuan"
                                    >
                                        Tampilkan
                                    </a>
                                </div>
                            </div>
                            <div class="collapse" id="multiSatuan">
                                <hr>
                                <div class="row" style="margin-bottom: 10px">
                                    {!! $addUnit !!}
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-custom" id="table-data-4" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            @foreach($tableColumnsMultiUnit as $key =>$item)
                                                <th>{{($tableColumnsMultiUnit[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumnsMultiUnit[$key]))}}</th>
                                            @endforeach
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <form onsubmit="return false;" id="form-biaya-tambahan" class='form-horizontal form-konten form-other' backdrop="">
                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">
                                    <div class="row d-flex align-items-center">
                                        <div class="col-md-6">
                                            <h4 class="font-weight-bold">Biaya Tambahan</h4>
                                            <span style="color:#A4A4A4;">Masukan data biaya tambahan jika terdapat biaya lain yang dikeluarkan ketika melakukan penjualan produk tersebut</span>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                               data-toggle="collapse"
                                               href="#biayaTambahan"
                                               role="button"
                                               aria-expanded="false"
                                               aria-controls="biayaTambahan"
                                               data-id="form-biaya-tambahan"
                                               data-var="dataBiayaTambahan"
                                            >
                                                + Tambah
                                            </a>
                                        </div>
                                    </div>
                                    <div class="collapse" id="biayaTambahan">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="font-weight-bold" for="exampleInputPassword1">Biaya Tambahan</label>
                                                    <select class="form-control form-control-sm" name="cost_id" id="cost_id" required>
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="font-weight-bold" for="exampleInputPassword1">Nominal</label>
                                                    <input type="text" class="form-control amount_currency" id="amount" name="amount" placeholder="0" required>
                                                    <input type="hidden" class="form-control" id="cost_name" name="cost_name">
                                                </div>
                                            </div>

                                            <div class="col-md-12 text-right mb-3">
                                                <button class="btn btn-success" id="save-satuan">Terapkan</button>
                                            </div>

                                            <div class="col-md-12">
                                                <table id="table-biaya-tambahan" class="table table-custom" style="width:100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Biaya</th>
                                                        <th>Nominal</th>
                                                        <th>Opsi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="body-table-biaya-tambahan">
                                                    @if (!is_null($data->cost_other) && $data->cost_other!="[]")
                                                        @foreach (json_decode($data->cost_other) as $item)
                                                            @php
                                                                $i=$cost->where('acc_coa_detail_id',$item->id)->first();
                                                            @endphp
                                                            <tr id="BT-{{$item->id}}">
                                                                <td >{{$i->name}}</td>
                                                                <td >{{$item->amount}}</td>
                                                                <td ><button type="button" data-target="#BT-{{$item->id}}" data-id="{{$item->id}}" class="btn btn-xs btn-delete-xs btn-rounded btn_remove_bt"><span class="fa fa-trash-alt" style="color: white"></span></button></td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                @endif
                <div class="col-md-12 text-right" style="margin-top: 20px">
                    <a href="{{route('merchant.toko.product.index')}}" class="btn btn-light text-light">Kembali</a>
                    @if($data->md_user_id==user_id())
                        <button class="btn btn-success" id="btn-form-konten"><i class="fa fa-save mr-2"></i> Simpan Produk</button>
                    @endif
                </div>
            </div>
        </div>

    </div>

@endsection

@section('js')
    <script>
        let dataBiayaTambahan = [];

        function browseImage(selector){
            $(selector).attr('accept', '.jpg, .png, .jpeg');
            $(selector).show();
            $(selector).focus();
            $(selector).click();
            $(selector).hide();
        }

        document.addEventListener("DOMContentLoaded", init, false);

        @if(!is_null($data->product_photos))

        var AttachmentArray = [];

        var arrCounter = {{count(json_decode($data->product_photos))}};
        @else

        var AttachmentArray=[];
        var arrCounter = 0;
        @endif

        var filesCounterAlertStatus = false;

        var ul = document.getElementById("imgList");

        function init() {

            document
                .querySelector("#files")
                .addEventListener("change", handleFileSelect, false);
        }

        function handleFileSelect(e) {
            if (!e.target.files) return;

            var files = e.target.files;

            for (var i = 0, f; (f = files[i]); i++) {

                var fileReader = new FileReader();

                fileReader.onload = (function(readerEvt) {
                    return function(e) {
                        ApplyFileValidationRules(readerEvt);

                        RenderThumbnail(e, readerEvt);

                        FillAttachmentArray(e, readerEvt);
                    };
                })(f);
                fileReader.readAsDataURL(f);
            }
            document
                .getElementById("files")
                .addEventListener("change", handleFileSelect, false);
        }



        jQuery(function($) {
            $("div").on("click", ".img-wrap .close", function() {
                var id = $(this)
                    .closest(".img-wrap")
                    .find("img")
                    .data("id");
                var id2 = $(this)
                    .closest(".img-wrap")
                    .find("img")
                    .data("num");

                if(typeof id2!== "undefined"){
                    var data = new FormData();
                    data.append('fileName',id2)
                    data.append('id',{{$data->id}})

                    ajaxTransfer("{{route('merchant.toko.product.delete-product-photo')}}", data, function(response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                    });
                }

                var elementPos = AttachmentArray.map(function(x) {
                    return x.FileName;
                }).indexOf(id);
                if (elementPos !== -1) {
                    AttachmentArray.splice(elementPos, 1);
                }

                $(this)
                    .parent()
                    .find("img")
                    .not()
                    .remove();

                $(this)
                    .parent()
                    .find("div")
                    .not()
                    .remove();


                $(this)
                    .parent()
                    .parent()
                    .find("div")
                    .not()
                    .remove();

                var lis = document.querySelectorAll("#imgList li");
                for (var i = 0; (li = lis[i]); i++) {
                    console.log(lis[i]);

                    if (li.innerHTML == "") {
                        li.parentNode.removeChild(li);
                    }
                }
            });
        });

        function ApplyFileValidationRules(readerEvt) {

            if (CheckFileType(readerEvt.type) == false) {
                alert(
                    "The file (" +
                    readerEvt.name +
                    ") does not match the upload conditions, You can only upload jpg/png/gif files"
                );
                e.preventDefault();
                return;
            }


            if (CheckFileSize(readerEvt.size) == false) {
                alert(
                    "The file (" +
                    readerEvt.name +
                    ") does not match the upload conditions, The maximum file size for uploads should not exceed 300 KB"
                );
                e.preventDefault();
                return;
            }

            //To check files count according to upload conditions
            if (CheckFilesCount(AttachmentArray) == false) {
                if (!filesCounterAlertStatus) {
                    filesCounterAlertStatus = true;
                    alert(
                        "You have added more than 10 files. According to upload conditions you can upload 10 files maximum"
                    );
                }
                e.preventDefault();
                return;
            }
        }

        function CheckFileType(fileType) {
            if (fileType == "image/jpeg") {
                return true;
            } else if (fileType == "image/png") {
                return true;
            } else if (fileType == "image/gif") {
                return true;
            } else {
                return false;
            }
            return true;
        }

        function CheckFileSize(fileSize) {
            if (fileSize < 1000000) {
                return true;
            } else {
                return false;
            }
            return true;
        }

        function CheckFilesCount(AttachmentArray) {

            var len = 0;
            for (var i = 0; i < AttachmentArray.length; i++) {
                if (AttachmentArray[i] !== undefined) {
                    len++;
                }
            }
            if (len > 9) {
                return false;
            } else {
                return true;
            }
        }

        function RenderThumbnail(e, readerEvt) {
            var li = document.createElement("li");
            ul.appendChild(li);
            li.innerHTML = [
                '<div class="img-wrap"> <span class="close">&times;</span>' +
                '<img class="thumb" src="',
                e.target.result,
                '" title="',
                escape(readerEvt.name),
                '" data-id="',
                readerEvt.name,
                '"/>' + "</div>"
            ].join("");

            var div = document.createElement("div");
            div.className = "FileNameCaptionStyle";
            li.appendChild(div);
            div.innerHTML = [readerEvt.name].join("");
            document.getElementById("Filelist").insertBefore(ul, null);
        }

        function FillAttachmentArray(e, readerEvt) {
            AttachmentArray[arrCounter] = {
                AttachmentType: 1,
                ObjectType: 1,
                FileName: readerEvt.name,
                FileDescription: "Attachment",
                NoteText: "",
                MimeType: readerEvt.type,
                Content: e.target.result.split("base64,")[1],
                FileSizeInBytes: readerEvt.size
            };
            arrCounter = arrCounter + 1;
        }

    </script>

    <script>
        $('.amount_currency').mask("#.##0,00", {reverse: true});

        let prodTypeOpt = $('#product_type option[value!="1"]');

        $('.btn-collapse-product').on('click', function(){
            if($(this).attr('aria-expanded') == 'false'){
                $(this).text('Tutup');
            } else {
                $(this).text('Tampilkan');
            }
        })

        $("#product_type").change(function(){
            var t=$("#product_type").val();
            var isWithStock = $("#is_with_stock").val();
            if(t==2)
            {
                $("#harga-jual").prop("disabled",true).val(0);
                $("#harga-beli").prop("disabled",true).val(0);
                $('#alert-barang-produksi').removeClass('d-none');
            }else if(t == 1){
                if(isWithStock == 0){
                    $("#harga-beli").prop("disabled",true).val(0);
                } else {
                    $("#harga-beli").prop("disabled",false).val('{{$data->purchase_price}}');
                }
                $("#harga-jual").prop("disabled",false).val('{{$data->selling_price}}');
                $('#alert-barang-produksi').addClass('d-none');

            }else if(t == 3){
                $("#harga-jual").prop("disabled",false).val('{{$data->selling_price}}');
                $("#harga-beli").prop("disabled",false).val('{{$data->purchase_price}}');
                $('#alert-barang-produksi').addClass('d-none');
            }
        })

        $("#is_with_stock").change(function(){
            var t=$("#is_with_stock").val();
            if(t==0)
            {
                $("#harga-beli").prop("disabled",true).val(0);
                $("#harga-jual").prop("disabled",false);
                $('#product_type').find('option[value!="1"]').remove();
                $("#product_type").val('1').trigger('change');
                $("#form-multi-satuan").hide();
            }else{
                $('#product_type').append(prodTypeOpt);
                if($("#product_type").val()==2){
                    $("#harga-beli").prop("disabled",true).val(0);
                    $("#harga-jual").prop("disabled",true).val(0);
                    $('#alert-barang-produksi').removeClass('d-none');
                }else if($('#product_type').val() == 3){
                    $("#harga-beli").prop("disabled",false).val('{{$data->purchase_price}}');;
                    $("#harga-jual").prop("disabled", true).val(0);
                } else if($('#product_type').val() == 1){
                    $("#harga-beli").prop("disabled",false).val('{{$data->purchase_price}}');
                }
                $("#form-multi-satuan").show();
            }
        })
        function getCategory()
        {
            $("#sc_product_category_id").select2({
                placeholder:'--- Pilih Kategori Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.product.category')}}?merchant_id={{merchant_id()}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }

        function getMerk()
        {
            $("#sc_merk_id").select2({
                placeholder:'--- Pilih Merk Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.merk')}}?merchant_id={{merchant_id()}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }

        function getUnit()
        {

            $("#md_unit_id").select2({
                placeholder:'--- Pilih Satuan Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.unit')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        console.log(data);
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

            $("#satuan").select2({
                placeholder:'--- Pilih Satuan Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.unit')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }

        $(document).ready(function() {
            $('#branch').select2()
            if($("#is_with_stock").val()==0){
                $("#harga-beli").prop("disabled",true);
                $('#product_type option[value!="1"]').remove();
                // $('#form-multi-varian').hide();
            }else{
                if($("#product_type").val()==2){
                    $("#harga-jual").prop("disabled",true);
                    $("#harga-beli").prop("disabled",true);
                    // $("#form-multi-varian").hide();

                } else if($("#product_type").val() == 1) {
                    $("#harga-jual").prop("disabled",false);
                    $("#harga-beli").prop("disabled",false);
                    // $("#form-bahan-baku").hide();
                } else if($("#product_type").val() == 3){
                    $("#harga-jual").prop("disabled",false);
                    $("#harga-beli").prop("disabled",false);
                    // $("#form-bahan-baku").hide();
                    // $("#form-tipe-harga").hide();
                    // $("#form-multi-varian").hide()
                }
            }


            $('.amount_currency').mask("#.##0,00", {reverse: true});

            $('#sc_product_category_id').select2(
                {
                    placeholder:'--- Pilih Kategori Produk ---',
                }
            ).on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_product_category_id-results').append('<a id="add-category" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.product-category.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;" class="text-danger"><b>+ Tambah Kategori Baru</b></a>');
            });
            $(document).on('click','#add-category', function(e){
                $("#sc_product_category_id").select2('close');
            })

            $('#sc_merk_id').select2({
                placeholder:'--- Pilih Merk ---',
            }).on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_merk_id-results').append('<a id="add-merk" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.merk.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Merk Baru</b></a>');
            });

            $(document).on('click','#add-merk', function(e){
                $("#sc_merk_id").select2('close');
            })


            //unit
            $('#md_unit_id').on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-md_unit_id-results').append('<a id="add-unit" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.unit.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Satuan Baru</b></a>');
            });

            $(document).on('click','#add-unit', function(e){
                $("#md_unit_id").select2('close');
            })

                        //unit2
            $('#satuan').on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-satuan-results').append('<a id="add-satuan" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.unit.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Satuan Baru</b></a>');
            });

            $(document).on('click','#add-satuan', function(e){
                $("#satuan").select2('close');
            })

            getCategory()
            getMerk()
            getUnit()
            $('#md_sc_category_id').select2();
            $('#product_type').select2();
            $('#is_with_stock').select2();
            ajaxDataTable('#table-data-2', 1, "{{route('merchant.toko.product.selling-price.datatable')}}?id={{$data->id}}", [
                    @foreach($tableColumnsPriceType as $key =>$item)
                    @if($tableColumnsPriceType[$key]=='action')
                {
                    data: '{{$tableColumnsPriceType[$key]}}',
                    name: '{{$tableColumnsPriceType[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumnsPriceType[$key]}}', name: '{{$tableColumnsPriceType[$key]}}'
                },
                @endif
                @endforeach
            ]);

            ajaxDataTable('#table-data-3', 1, "{{route('merchant.toko.product.raw-material.datatable')}}?id={{$data->id}}", [
                    @foreach($tableColumnsRawMaterial as $key =>$item)
                    @if($tableColumnsRawMaterial[$key]=='action')
                {
                    data: '{{$tableColumnsRawMaterial[$key]}}',
                    name: '{{$tableColumnsRawMaterial[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumnsRawMaterial[$key]}}', name: '{{$tableColumnsRawMaterial[$key]}}'
                },
                @endif
                @endforeach
            ]);
            ajaxDataTable('#table-data-4', 1, "{{route('merchant.toko.product.multi-unit.datatable')}}?id={{$data->id}}", [
                    @foreach($tableColumnsMultiUnit as $key =>$item)
                    @if($tableColumnsMultiUnit[$key]=='action')
                {
                    data: '{{$tableColumnsMultiUnit[$key]}}',
                    name: '{{$tableColumnsMultiUnit[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumnsMultiUnit[$key]}}', name: '{{$tableColumnsMultiUnit[$key]}}'
                },
                @endif
                @endforeach
            ]);

        })
        $('#btn-form-konten').on('click', function(){
            $('#btn-submit-konten').click();
        })
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            if(data.get('is_with_stock') == 0){
                    data.append('cost_other', JSON.stringify(dataBiayaTambahan));
                }
            let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json(get_cabang());
                    merchantIds.push(parseInt($("#merchant_id").val()));
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }
                let md_merchant_id = merchantIds.join(',');
                data.append('md_merchant_id',md_merchant_id);
            data.append('content_1',CKEDITOR.instances['desc'].getData());

            ajaxTransfer("{{route('merchant.toko.product.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })

        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.product.selling-price.delete')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        function deleteDataMultiUnit(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.product.multi-unit.delete')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        function deleteDataRawMaterial(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.product.raw-material.delete')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        function deleteDataMultiVarian(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.product.multi-varian.delete')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

    </script>
<script>
        // form biaya lainnya
        $("#cost_id").select2({
                placeholder:"---- Pilih Biaya ----",
                ajax: {
                    type: "GET",
                    url: "{{route('api.cost.all',['merchantId'=>merchant_id()])}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        console.log(data)
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

            $("#cost_id").on('select2:select', function (e) {
                let data = e.params.data;
                $('#cost_name').val(data.text);
            });

            $("#cost_id").on('select2:open', () => {
                $(".select2-results:not(:has(a))").append('<a id="add-customer" onclick="loadModal(this)" target="{{route("merchant.toko.clinic.services-pack.add-cost")}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Biaya Baru</b></a>');
            });
            $(document).on('click','#add-customer', function(e){
                $("#cost_id").select2('close');
            });

            let iBiaya = 0;
            $('#form-biaya-tambahan').submit(function(){
                let data = getFormData('form-biaya-tambahan');

                let cost_id = data.get('cost_id');
                let amount = data.get('amount');
                let cost_name = data.get('cost_name');


                $('#form-biaya-tambahan input').val('');

                dataBiayaTambahan.push({
                    iBiaya:iBiaya,
                    cost_id: cost_id,
                    amount: amount,
                    cost_name: cost_name,

                });

                $('#body-table-biaya-tambahan .no-data').remove();

                let html = '<tr id="BT-'+iBiaya+'">'+
                    '<td>'+ cost_name +'</td>'+
                    '<td>'+ amount +'</td>'+
                    '<td>'+
                    '<button type="button" data-target="#BT-'+iBiaya+'" data-id="'+iBiaya+'" class="btn btn-xs btn-delete-xs btn-rounded btn_remove_bt">'+
                    '<span class="fa fa-trash-alt" style="color: white"></span>'+
                    '</button>'+
                    '</td>'+
                    '</tr>';
                $('#body-table-biaya-tambahan').append(html);
                iBiaya++;
        });

        $(document).on('click', '.btn_remove_bt', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");
            dataBiayaTambahan = dataBiayaTambahan.filter(item => {
                return item.iBiaya != parseInt(targetId);
            });
            $(target).remove();
        });
    </script>
    <script>
        $(document).ready(function(){
            CKEDITOR.replace( 'desc', {
                height:"250",
            } );
            let is_unit = "{{$is_unit}}";
            if(is_unit == 1){
                $('#md_unit_id').prop('disabled', true);
            } else {
                $('#md_unit_id').prop('disabled', false);
            }
            @if(!is_null($data->cost_other))
            for(let v of JSON.parse(@json($data->cost_other))){
                dataBiayaTambahan.push({
                        iBiaya:v.id,
                        cost_id: v.id,
                        amount: v.amount,
                        cost_name: "",

                    });
            }
            @endif
            // console.log(dataBiayaTambahan)

        });
        $("#is_consignment").change(function() {
            if(this.checked) {
                $("#consignment").val(1)

            }else{
                $("#consignment").val(0)

            }
        });
    </script>
@endsection

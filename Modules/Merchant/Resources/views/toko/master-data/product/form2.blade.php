@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .thumb {
            height: 80px;
            width: 100px;
            border: 1px solid #ddd;
        }

        ul.thumb-Images li {
            width: 120px;
            float: left;
            display: inline-block;
            vertical-align: top;
            height: 120px;
        }

        .img-wrap {
            position: relative;
            display: inline-block;
            font-size: 0;
        }

        .img-wrap .close {
            position: absolute;
            top: 2px;
            right: 2px;
            z-index: 100;
            background-color: #d0e5f5;
            padding: 5px 2px 2px;
            color: #000;
            font-weight: bolder;
            cursor: pointer;
            opacity: 0.5;
            font-size: 23px;
            line-height: 10px;
            border-radius: 50%;
        }

        .img-wrap:hover .close {
            opacity: 1;
            background-color: #ff0000;
        }

        .FileNameCaptionStyle {
            font-size: 12px;
        }
        /* .select2-container .select2-selection--multiple .select2-selection__rendered  {
            display: grid!important;
            grid-template-columns: 1fr 1fr;
        } */
    </style>
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.product.index')}}">Produk</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}} Produk</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa mengisi data di bawah ini untuk menambah produk. Pastikan semua kolom sudah terisi dengan benar</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="card shadow mb-4">
            <div class="card-body">
                <h4 class="font-weight-bold mb-3">Tambah Produk</h4>
                <hr>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div id="result-form-konten"></div>
                    @if($is_goods==1)
                        <div class="col-md-12 d-none" id="alert-barang-produksi">
                            <div class="alert alert-warning" role="alert">
                                Perhatian ! Untuk menambahkan harga jual, dapat dilakukan di menu produksi produk.
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label class="col-form-label font-weight-bold d-block">Foto Produk</label>
                                    <small style="color: #A4A4A4;">Pilih beberapa gambar! Format gambar jpg. jpeg. png dan ukuran minimum 500px x 500px</small>
                                </div>
                                <div class="col-sm-10">
                                    <input
                                        style="display: none"
                                        type="file"
                                        class="form-control"
                                        name="files[]" id="files" multiple accept="image/jpeg, image/png, image/gif">
                                    <div class="d-flex align-items-center preview-image__product__wrapper" id="foto-preview" onclick="browseImage('#files')">
                                        <div class="product-plus__icon"></div>
                                    </div>
                                    <br>
                                    <output id="Filelist" style="margin-top: 10px;margin-bottom: 40px"></output>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"></div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Nama Produk <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" value="{{$data->name}}" required placeholder="Nama Produk">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Kode Produk <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="code" value="{{(is_null($data->id))?$code:$data->code}}" required>
                                </div>
                            </div>
                        </div>
                        @if($is_goods==1)
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Tipe Produk <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="md_sc_product_type_id" class="form-control" id="product_type" required>
                                            <option></option>
                                            @foreach($productType as $key => $item)
                                                <option value="{{$item->id}}" @if($item->id==$data->md_sc_product_type_id) selected @endif>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Kategori Produk <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    @php
                                        if($is_goods==0){
                                            $c=$category->where('is_service',1);
                                        }else{
                                            $c=$category->where('is_service',0);
                                        }
                                    @endphp
                                    <select name="sc_product_category_id" class="form-control" id="sc_product_category_id" required>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @if($is_goods==1)
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Merk</label>
                                    <div class="col-sm-8">
                                        <select name="sc_merk_id" class="form-control" id="sc_merk_id">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($is_goods==1)
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Harga Beli <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control amount_currency" name="purchase_price" step="0.001" min="0"  value="{{is_null($data->purchase_price)?0:$data->purchase_price}}"  id="harga-beli" required>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Harga Jual <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control amount_currency" name="selling_price" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:$data->selling_price}}" id="harga-jual" required>
                                </div>
                            </div>
                            @if ($is_goods==1)
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Barang Titipan (Konsinyasi) ? <span class="text-danger"></span></label>
                                    <div class="col-md-2">
                                        <input value="{{($data->is_consignment==1)?1:0}}" type="hidden" name="is_consignment" id="consignment">
                                        <label>
                                            <input type="checkbox" {{($data->is_consignment==1)?'checked':''}} id="is_consignment"></label>
                                    </div>
                                </div>
                            @endif
                        </div>


                        <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Satuan <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <select name="md_unit_id" class="form-control" id="md_unit_id">
                                            <option></option>
                                        </select>
                                    </div>
                                </div>

                            @if(count(get_cabang())>1)
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label font-weight-bold">Duplikasi ke Cabang Lain </label>
                                    <div class="col-sm-8">
                                        <select name="md_merchant_id" id="branch" class="form-control mr-2 js-example-responsive js-states" multiple="multiple">
                                            @foreach (get_cabang() as $key => $b)
                                                @if($b->id!=merchant_id())
                                                    <option value="{{$b->id}}">{{$b->nama_cabang}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>

                        @if($is_goods==1)
                            {{--                        <div class="col-md-6">--}}
                            {{--                            <div class="form-group row">--}}
                            {{--                                <label class="col-sm-4 col-form-label font-weight-bold">Jual di Ecommerce ? </label>--}}
                            {{--                                <div class="col-sm-8">--}}
                            {{--                                    <input value="0" type="hidden" name="is_sell_to_ecommerce" id="ecommerce">--}}
                            {{--                                    <label><input type="checkbox" id="is_sell_to_ecommerce"></label>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                            {{--                            <div class="col-md-12">--}}
                            {{--                                <div class="form-group row ecommerce">--}}
                            {{--                                    <label class="col-sm-2 col-form-label font-weight-bold"></label>--}}
                            {{--                                    <div class="col-sm-3">--}}
                            {{--                                        <select class="form-control" id="epc1" required>--}}
                            {{--                                            <option></option>--}}
                            {{--                                        </select>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="col-sm-3">--}}
                            {{--                                        <select class="form-control" id="epc2" required>--}}
                            {{--                                            <option></option>--}}
                            {{--                                        </select>--}}
                            {{--                                    </div>--}}
                            {{--                                    <div class="col-sm-3">--}}
                            {{--                                        <select name="ecommerce_product_category_id" class="form-control" id="ecommerce_product_category_id" required>--}}
                            {{--                                            <option></option>--}}
                            {{--                                        </select>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                        @endif

                        <div class="col-md-12">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label font-weight-bold">Deskripsi</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" style="min-height: 250px" name="description" id="desc" placeholder="Tambahkan deskripsi produk">{{$data->description}}</textarea>
                                    <input type="hidden" value="{{$is_goods}}" name="is_with_stock">
                                    @if($is_goods==0)
                                        <input type="hidden" value="0" name="purchase_price">
                                        <input type="hidden" value="1" name="md_sc_product_type_id">

                                    @endif
                                </div>
                            </div>
                        </div>

                        @if($is_goods==1)
                            <div class="col-md-12">
                                <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;">
                                    <label class="col-sm-3 col-form-label font-weight-bold"><h5 style="font-weight: bold">Info Pengiriman</h5></label>
                                    <div class="col-sm-7"></div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label font-weight-bold">Berat (dalam gr)</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="weight" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:$data->selling_price}}" required>
                                            </div>
                                            <label class="col-sm-2 col-form-label font-weight-bold">Panjang (dalam cm)</label>

                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="long" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:$data->selling_price}}" required>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label font-weight-bold">Tinggi (dalam cm)</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="hight" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:$data->selling_price}}"  required>
                                            </div>
                                            <label class="col-sm-2 col-form-label font-weight-bold">Lebar (dalam cm)</label>

                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" name="wide" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:$data->selling_price}}"  required>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label font-weight-bold">Isi</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" style="min-height: 250px" name="package_contents"  placeholder="Tambahkan isi paket">{{$data->package_contents}}</textarea>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif
                    </div>
                    <div class="col-md-12 text-right" style="display:none;">
                        <button class="btn btn-success" id="btn-submit-konten"><i class="fa fa-save mr-2"></i> Simpan Produk</button>
                    </div>

                </form>

                <form onsubmit="return false;" id="form-tipe-harga" class='form-horizontal form-konten form-other' backdrop="">

                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">
                                <div class="row d-flex align-items-center">
                                    <div class="col-md-6">
                                        <h5 class="font-weight-bold">Tipe Harga</h5>
                                        @if($is_goods==1)
                                            <span style="color:#A4A4A4;">Tambahkan tipe harga untuk penjualan grosir,eceran atau harga order online (Gojek,Grab)</span>
                                        @else
                                            <span style="color:#A4A4A4;">Tambahkan tipe harga untuk penjualan jasa/layanan yang ada berikan seperti harga member atau customer tertentu</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                           data-toggle="collapse"
                                           href="#tipeHarga"
                                           role="button"
                                           aria-expanded="false"
                                           aria-controls="tipeHarga"
                                           data-id="form-tipe-harga"
                                           data-var="dataTipeHarga"
                                        >
                                            + Tambah
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse" id="tipeHarga">
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="font-weight-bold" for="exampleInputPassword1" >Tipe harga</label>
                                                <input type="text" class="form-control" name="level_name" placeholder="Tipe Harga" required>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputPassword1" class="font-weight-bold">Harga Jual</label>
                                                <input type="text" class="form-control amount_currency" name="selling_price" placeholder="0" required>
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-right mb-3">
                                            <button class="btn btn-success" id="save-po">Terapkan</button>
                                        </div>

                                        <div class="col-md-12">
                                            <table id="table-tipe-harga" class="table table-custom" style="width:100%;">
                                                <thead>
                                                <tr>
                                                    <th>Tipe Harga</th>
                                                    <th>Harga Jual</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody id="body-table-tipe-harga">
                                                <tr class="no-data">
                                                    <td class="text-center" colspan="3">Tidak ada data !</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>

                <form onsubmit="return false;" id="form-multi-satuan" class='form-horizontal form-konten form-other' backdrop="">

                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">
                                <div class="row d-flex align-items-center">
                                    <div class="col-md-6">
                                        <h5 class="font-weight-bold">Multi Satuan</h5>
                                        <span style="color:#A4A4A4;">Perhatian,jika kamu ingin memasukkan data multi satuan,mulailah dari yang terkecil agar sistem dengan mudah melakukan pengambilan data saat penjualan</span>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0"
                                           data-toggle="collapse"
                                           href="#multiSatuan"
                                           role="button"
                                           aria-expanded="false"
                                           aria-controls="multiSatuan"
                                           data-id="form-multi-satuan"
                                           data-var="dataMultiSatuan"
                                        >
                                            + Tambah
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse" id="multiSatuan">
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="font-weight-bold" for="exampleInputPassword1">Tipe Multi Satuan</label>
                                                <select class="form-control form-control-sm" name="md_unit_id" id="satuan" required>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="font-weight-bold"  id="jumlah-multi" for="exampleInputPassword1">Jumlah</label>
                                                <input type="text" class="form-control" id="quantity" name="quantity" placeholder="0" required onkeypress="return isNumber(event)">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="font-weight-bold" for="exampleInputPassword1">Harga Jual</label>
                                                <input type="text" class="form-control amount_currency" id="price" name="price" placeholder="0" required>
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-right mb-3">
                                            <button class="btn btn-success" disabled="disabled" id="save-satuan">Terapkan</button>
                                        </div>

                                        <div class="col-md-12">
                                            <table id="table-multi-satuan" class="table table-custom" style="width:100%;">
                                                <thead>
                                                <tr>
                                                    <th>Nama Satuan</th>
                                                    <th>Nilai Konversi</th>
                                                    <th>Harga Jual</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody id="body-table-multi-satuan">
                                                <tr class="no-data">
                                                    <td class="text-center" colspan="4">Tidak ada data !</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>



                            </div>
                        </div>

                    </div>
                </form>
                @if($is_goods==1)
                    <form onsubmit="return false;" id="form-bahan-baku" class='form-horizontal form-konten' backdrop="" style="display:none;">

                        <div class="row" style="margin-top: 20px">
                            <div class="col-md-12">
                                <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">
                                    <div class="row d-flex align-items-center">
                                        <div class="col-md-6">
                                            <h5 class="font-weight-bold">Bahan Baku</h5>
                                            <span style="color:#A4A4A4;">Tambahkan bahan baku untuk memproduksi produk.</span>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <a class="btn btn-outline-warning btn-collapse-product px-4 rounded-0 btn-bahan-baku"
                                               data-toggle="collapse"
                                               href="#bahanBaku"
                                               role="button"
                                               aria-expanded="false"
                                               aria-controls="bahanBaku"
                                            >
                                                + Tambah
                                            </a>
                                        </div>
                                    </div>
                                    <div class="collapse" id="bahanBaku">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Quantity/Jumlah(untuk decimal/koma memakai titik)</label>
                                                    <input type="number" class="form-control form-control-sm" name="quantity" step="0.001" min="0" placeholder="Qty" value="0" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Bahan Baku</label>
                                                    <select name="child_sc_product_id" class="form-control form-control-sm" id="child_sc_product_id" required>
                                                        <option></option>
                                                        @foreach($raw as $key =>$item)
                                                            <option value="{{$item->id}}">{{$item->name}} / {{$item->unit_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12 text-right mb-3">
                                                <button class="btn btn-success" id="save-po">Terapkan</button>
                                            </div>

                                            <div class="col-md-12">
                                                <table id="table-bahan-baku" class="table table-custom" style="width:100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Bahan Baku</th>
                                                        <th>Jumlah</th>
                                                        <th>Opsi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="body-table-bahan-baku">
                                                    <tr class="no-data">
                                                        <td class="text-center" colspan="4">Tidak ada data !</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>


                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>

                @endif
                @if ($is_goods==0)
                <form onsubmit="return false;" id="form-biaya-tambahan" class='form-horizontal form-konten' backdrop="">

                    <div class="row" style="margin-top: 20px">
                        <div class="col-md-12">
                            <div style="border: 1px solid #E0E0E0;box-sizing: border-box;border-radius: 5px;padding: 10px">
                                <div class="row d-flex align-items-center">
                                    <div class="col-md-6">
                                        <h5 class="font-weight-bold">Biaya Tambahan</h5>
                                        <span style="color:#A4A4A4;">Masukan data biaya tambahan jika terdapat biaya lain yang dikeluarkan ketika melakukan penjualan produk tersebut</span>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn btn-outline-warning btn-collapse-product btn-bahan-baku px-4 rounded-0"
                                           data-toggle="collapse"
                                           href="#biayaTambahan"
                                           role="button"
                                           aria-expanded="false"
                                           aria-controls="biayaTambahan"
                                           data-id="form-biaya-tambahan"
                                           data-var="dataBiayaTambahan"
                                        >
                                            + Tambah
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse" id="biayaTambahan">
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="font-weight-bold" for="exampleInputPassword1">Biaya Tambahan</label>
                                                <select class="form-control form-control-sm" name="cost_id" id="cost_id" required>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="font-weight-bold" for="exampleInputPassword1">Nominal</label>
                                                <input type="text" class="form-control amount_currency" id="amount" name="amount" placeholder="0" required>
                                                <input type="hidden" class="form-control" id="cost_name" name="cost_name">
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-right mb-3">
                                            <button class="btn btn-success" >Terapkan</button>
                                        </div>

                                        <div class="col-md-12">
                                            <table id="table-biaya-tambahan" class="table table-custom" style="width:100%;">
                                                <thead>
                                                <tr>
                                                    <th>Nama Biaya</th>
                                                    <th>Nominal</th>
                                                    <th>Opsi</th>
                                                </tr>
                                                </thead>
                                                <tbody id="body-table-biaya-tambahan">
                                                <tr class="no-data">
                                                    <td class="text-center" colspan="4">Tidak ada data !</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
                @endif

            </div>
        </div>


        <div class="col-md-12 mb-4 text-right">
            <a href="{{route('merchant.toko.product.index')}}" class="btn btn-light text-light">Kembali</a>

            <button class="btn btn-success" id="btn-save-product"><i class="fa fa-save mr-2"></i> Simpan Produk</button>
        </div>

    </div>
@endsection

@section('js')
    <script>
        const browseImage = (selector) => {
            $(selector).attr('accept', '.jpg, .png, .jpeg');
            $(selector).show();
            $(selector).focus();
            $(selector).click();
            $(selector).hide();
        }


        document.addEventListener("DOMContentLoaded", init, false);

        var AttachmentArray = [];

        var arrCounter = 0;

        var filesCounterAlertStatus = false;

        var ul = document.createElement("ul");
        ul.className = "thumb-Images";
        ul.id = "imgList";

        function init() {
            document
                .querySelector("#files")
                .addEventListener("change", handleFileSelect, false);
        }

        function handleFileSelect(e) {
            if (!e.target.files) return;

            var files = e.target.files;

            for (var i = 0, f; (f = files[i]); i++) {

                var fileReader = new FileReader();

                fileReader.onload = (function(readerEvt) {
                    return function(e) {
                        ApplyFileValidationRules(readerEvt);

                        RenderThumbnail(e, readerEvt);

                        FillAttachmentArray(e, readerEvt);
                    };
                })(f);
                fileReader.readAsDataURL(f);
            }
            document
                .getElementById("files")
                .addEventListener("change", handleFileSelect, false);
        }

        jQuery(function($) {
            $("div").on("click", ".img-wrap .close", function() {
                var id = $(this)
                    .closest(".img-wrap")
                    .find("img")
                    .data("id");

                var elementPos = AttachmentArray.map(function(x) {
                    return x.FileName;
                }).indexOf(id);
                if (elementPos !== -1) {
                    AttachmentArray.splice(elementPos, 1);
                }

                $(this)
                    .parent()
                    .find("img")
                    .not()
                    .remove();

                $(this)
                    .parent()
                    .find("div")
                    .not()
                    .remove();


                $(this)
                    .parent()
                    .parent()
                    .find("div")
                    .not()
                    .remove();

                var lis = document.querySelectorAll("#imgList li");
                for (var i = 0; (li = lis[i]); i++) {
                    if (li.innerHTML == "") {
                        li.parentNode.removeChild(li);
                    }
                }
            });
        });

        function ApplyFileValidationRules(readerEvt) {

            if (CheckFileType(readerEvt.type) == false) {
                alert(
                    "The file (" +
                    readerEvt.name +
                    ") does not match the upload conditions, You can only upload jpg/png/gif files"
                );
                e.preventDefault();
                return;
            }


            if (CheckFileSize(readerEvt.size) == false) {
                alert(
                    "The file (" +
                    readerEvt.name +
                    ") does not match the upload conditions, The maximum file size for uploads should not exceed 300 KB"
                );
                e.preventDefault();
                return;
            }

            //To check files count according to upload conditions
            if (CheckFilesCount(AttachmentArray) == false) {
                if (!filesCounterAlertStatus) {
                    filesCounterAlertStatus = true;
                    alert(
                        "You have added more than 10 files. According to upload conditions you can upload 10 files maximum"
                    );
                }
                e.preventDefault();
                return;
            }
        }

        function CheckFileType(fileType) {
            if (fileType == "image/jpeg") {
                return true;
            } else if (fileType == "image/png") {
                return true;
            } else if (fileType == "image/gif") {
                return true;
            } else {
                return false;
            }
            return true;
        }

        function CheckFileSize(fileSize) {
            if (fileSize < 1000000) {
                return true;
            } else {
                return false;
            }
            return true;
        }

        function CheckFilesCount(AttachmentArray) {

            var len = 0;
            for (var i = 0; i < AttachmentArray.length; i++) {
                if (AttachmentArray[i] !== undefined) {
                    len++;
                }
            }
            if (len > 9) {
                return false;
            } else {
                return true;
            }
        }

        function RenderThumbnail(e, readerEvt) {
            var li = document.createElement("li");
            ul.appendChild(li);
            li.innerHTML = [
                '<div class="img-wrap"> <span class="close">&times;</span>' +
                '<img class="thumb" src="',
                e.target.result,
                '" title="',
                escape(readerEvt.name),
                '" data-id="',
                readerEvt.name,
                '"/>' + "</div>"
            ].join("");

            var div = document.createElement("div");
            div.className = "FileNameCaptionStyle";
            li.appendChild(div);
            div.innerHTML = [readerEvt.name].join("");
            document.getElementById("Filelist").insertBefore(ul, null);
        }

        function FillAttachmentArray(e, readerEvt) {
            AttachmentArray[arrCounter] = {
                AttachmentType: 1,
                ObjectType: 1,
                FileName: readerEvt.name,
                FileDescription: "Attachment",
                NoteText: "",
                MimeType: readerEvt.type,
                Content: e.target.result.split("base64,")[1],
                FileSizeInBytes: readerEvt.size
            };
            arrCounter = arrCounter + 1;
        }

    </script>
    <script>
        let dataMultiSatuan = [];
        let dataTipeHarga = [];
        let dataMultiVarian = [];
        let dataBahanBaku = [];
        let dataBiayaTambahan = [];

        $(document).ready(function(){
            CKEDITOR.replace( 'desc', {
                height:"250",
            } );
            $("#form-bahan-baku").hide();
            $(".ecommerce").attr("hidden",true);

        })

        $("#product_type").change(function(){
            var t=$("#product_type").val();
            var isWithStock = $("#is_with_stock").val();
            if(t==2)
            {
                $("#harga-jual").prop("disabled",true);
                $("#harga-beli").prop("disabled",true);
                $("#harga-jual").val(0);
                $("#harga-beli").val(0);
                $("#form-multi-satuan").hide();
                $("#form-tipe-harga").hide();
                $("#form-multi-varian").hide();
                $("#form-bahan-baku").show();
                $('#alert-barang-produksi').removeClass('d-none');
            }else if(t==1){

                if(isWithStock == 0){
                    $("#harga-beli").prop("disabled",true);
                } else {
                    $("#harga-beli").prop("disabled",false);
                }
                $("#harga-jual").prop("disabled",false);
                $("#form-multi-satuan").show();
                $("#form-tipe-harga").show();
                $("#form-multi-varian").show();
                $("#form-bahan-baku").hide();
                $('#alert-barang-produksi').addClass('d-none');
            }

            else if(t==3){
                $("#harga-jual").val(0);
                $("#harga-beli").prop("disabled",false);
                // $("#form-multi-satuan").hide();
                // $("#form-tipe-harga").hide();
                $("#form-multi-varian").hide();
                $("#form-bahan-baku").hide();
                $('#alert-barang-produksi').addClass('d-none');
            }
        })

        let prodTypeOpt = $('#product_type option[value!="1"]');

        $("#is_with_stock").change(function(){

            var t=$("#is_with_stock").val();
            if(t==0)
            {
                $("#harga-beli").prop("disabled",true);
                $("#harga-jual").prop("disabled",false);
                $("#harga-beli").val(0);
                $('#product_type option[value!="1"]').remove();
                $("#product_type").val('1').trigger('change');
                $("#form-multi-satuan").hide();
                dataMultiSatuan = [];
                $('#body-table-multi-satuan tr').remove();
                $('#body-table-multi-satuan').append(`<tr class="no-data">
                                                        <td class="text-center" colspan="4">Tidak ada data !</td>
                                                    </tr>`);
                $("#form-tipe-harga").removeClass('d-none');

            }else{
                $('#product_type').append(prodTypeOpt);
                $("#product_type").val('1').trigger('change');
                $("#form-multi-satuan").show();
            }


        })


        function getCategory()
        {
            $("#sc_product_category_id").select2({
                placeholder:'--- Pilih Kategori Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.product.category')}}?merchant_id={{merchant_id()}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }

        function getMerk()
        {
            $("#sc_merk_id").select2({
                placeholder:'--- Pilih Merk Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.merk')}}?merchant_id={{merchant_id()}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }

        function getUnit()
        {

            $("#md_unit_id").select2({
                placeholder:'--- Pilih Satuan Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.unit')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

            $("#satuan").select2({
                placeholder:'--- Pilih Satuan Produk ---',
                ajax: {
                    type: "GET",
                    url: "{{route('get.unit')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }

        $(document).ready(function () {
            $('#branch').select2();
            $('.amount_currency').mask("#.##0,00", {reverse: true});
            $('#sc_product_category_id').on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_product_category_id-results').append('<a id="add-category" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.product-category.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;" class="text-danger"><b>+ Tambah Kategori Baru</b></a>');
            });
            

            $(document).on('click','#add-category', function(e){
                $("#sc_product_category_id").select2('close');
            })

            $('#md_sc_category_id').select2();
            $('#product_type').select2({
                placeholder:'--- Pilih Tipe Produk ---'
            });
            $('#is_with_stock').select2();
            $("#child_sc_product_id").select2({
                placeholder:'--- Pilih Bahan Baku ---'
            });

            //merk
            $('#sc_merk_id').on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_merk_id-results').append('<a id="add-merk" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.merk.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Merk Baru</b></a>');
            });

            $(document).on('click','#add-merk', function(e){
                $("#sc_merk_id").select2('close');
            })

            //unit
            $('#md_unit_id').on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-md_unit_id-results').append('<a id="add-unit" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.unit.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Satuan Baru</b></a>');
            });

            $(document).on('click','#add-unit', function(e){
                $("#md_unit_id").select2('close');
            })

            //unit2
            $('#satuan').on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-satuan-results').append('<a id="add-satuan" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.unit.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Satuan Baru</b></a>');
            });

            $(document).on('click','#add-satuan', function(e){
                $("#satuan").select2('close');
            })

            $('#tipe_varian').select2({
                placeholder:'--- Pilih Tipe Varian ---'
            })
            
            getMerk()
            getUnit()
            getCategory()
            $('#btn-save-product').on('click', function(){
                $('#btn-submit-konten').click();
            })

            $('#form-konten').submit(function () {
                var data = getFormData('form-konten');
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];

                // cek jika pilih semua cabang
                if(selectedMerchantId.findIndex(a => a.id == '-1') >= 0){
                    let branch = @json(get_cabang());
                    merchantIds.push(parseInt($("#merchant_id").val()));
                    branch.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                } else {
                    selectedMerchantId.forEach(item => {
                        merchantIds.push(parseInt(item.id));
                    });
                }
                let md_merchant_id = merchantIds.join(',');
                data.append('md_merchant_id',md_merchant_id);

                data.append('tipe_harga', JSON.stringify(dataTipeHarga));
                if(data.get('is_with_stock') == 1 && data.get('md_sc_product_type_id') == 1){
                    data.append('multi_satuan', JSON.stringify(dataMultiSatuan));
                }
                if(data.get('is_with_stock') == 0){
                    data.append('cost_other', JSON.stringify(dataBiayaTambahan));
                    data.append('multi_satuan', JSON.stringify(dataMultiSatuan));

                }
                if(data.get('md_sc_product_type_id') == 2){
                    data.append('bahan_baku', JSON.stringify(dataBahanBaku));
                    data.delete('tipe_harga');
                    data.delete('multi_satuan');
                }

                if(data.get('md_sc_product_type_id') == 3){
                    data.append('multi_satuan', JSON.stringify(dataMultiSatuan));
                    data.delete('bahan_baku');
                }
                data.append('content_1',CKEDITOR.instances['desc'].getData());

                ajaxTransfer("{{route('merchant.toko.product.save')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        })
    </script>

    <script>
        $('.amount_currency').mask("#.##0,00", {reverse: true});

        $('.btn-collapse-product').on('click', function(){
            if($(this).attr('aria-expanded') == 'false'){
                $(this).text('Tutup');
            } else {
                $(this).text('+ Tambah');
            }

        });

        $('.btn-collapse-product:not(.btn-bahan-baku)').on('click', function(){
            let containerId = $(this).attr('data-id');
            let nameVar = eval($(this).attr('data-var'));

            if($(this).attr('aria-expanded') == 'false'){
                $(this).text('Tutup');
                $('.form-other:not(#'+containerId+')').addClass('d-none');
            } else {
                $(this).text('+ Tambah');
                if(nameVar.length == 0){
                    $('.form-other:not(#'+containerId+')').removeClass('d-none');
                }
            }

        });
    </script>

    <script>
        // form multi satuan
        let units = @json($unit);

        $('#md_unit_id').on('change', function(){
            const unitName = $(this).val();
            if(unitName == ''){
                $('#save-satuan').prop('disabled', true);
            } else {
                $('#save-satuan').prop('disabled', false);
            }
            let unit = units.filter(item => {
                return item.name.toLowerCase().trim() != unitName.toLowerCase().trim();
            });

            let html = "<option></option>";

            unit.forEach(item => {
                html += "<option value="+item.id+">"+item.name+"</option>";
            });


            $('#satuan option').remove();
            $('#satuan').append(html);
        })


        $('#quantity').on('keyup', function(){
            var price = parseFloat($("#harga-jual").val().replaceAll('.', ''));
            var quantity =parseFloat($("#quantity").val());
            var result = price*quantity;
            $("#price").val(currencyFormat(result));
        });

        $('#form-multi-satuan').submit(function(){
            let data = getFormData('form-multi-satuan');

            let price = data.get('price');
            let quantity = data.get('quantity');
            let md_unit_id = data.get('md_unit_id');
            let md_unit_name = $('#satuan').find(':selected').text();

            let duplicateSatuanId = dataMultiSatuan.filter(item => {
                return item.md_unit_id == $(this).find(':selected').val();
            });


            $("#satuan").val('').trigger('change');
            $('#form-multi-satuan input').val('');

            if(duplicateSatuanId.length > 0){
                return;
            }

            dataMultiSatuan.push({
                price: price,
                quantity:quantity,
                md_unit_id: md_unit_id
            });

            $('#body-table-multi-satuan .no-data').remove();

            let html = '<tr id="MS-'+md_unit_id+'">'+
                '<td>'+ md_unit_name +'</td>'+
                '<td>'+ quantity +'</td>'+
                '<td>'+ price +'</td>'+
                '<td>'+
                '<button type="button" data-target="#MS-'+md_unit_id+'" data-id="'+md_unit_id+'" class="btn btn-xs btn-delete-xs btn-rounded btn_remove_ms">'+
                '<span class="fa fa-trash-alt" style="color: white"></span>'+
                '</button>'+
                '</td>'+
                '</tr>';
            $('#body-table-multi-satuan').append(html);
        });

        $(document).on('click', '.btn_remove_ms', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");
            dataMultiSatuan = dataMultiSatuan.filter(item => {
                return item.md_unit_id != parseInt(targetId);
            });
            $(target).remove();
        });
    </script>

    <script>
        // form tipe harga
        let iTipeHarga = 0;
        $('#form-tipe-harga').submit(function(){
            let data = getFormData('form-tipe-harga');

            let level_name = data.get('level_name');
            let selling_price = data.get('selling_price');

            $('#form-tipe-harga input').val('');

            dataTipeHarga.push({
                iTipeHarga:iTipeHarga,
                level_name: level_name,
                selling_price: selling_price,
            });

            $('#body-table-tipe-harga .no-data').remove();

            let html = '<tr id="TH-'+iTipeHarga+'">'+
                '<td>'+ level_name +'</td>'+
                '<td>'+ selling_price +'</td>'+
                '<td>'+
                '<button type="button" data-target="#TH-'+iTipeHarga+'" data-id="'+iTipeHarga+'" class="btn btn-xs btn-delete-xs btn-rounded btn_remove_th">'+
                '<span class="fa fa-trash-alt" style="color: white"></span>'+
                '</button>'+
                '</td>'+
                '</tr>';
            $('#body-table-tipe-harga').append(html);
            iTipeHarga++;
        });

        $(document).on('click', '.btn_remove_th', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");

            dataTipeHarga = dataTipeHarga.filter(item => {
                return item.iTipeHarga != parseInt(targetId);
            });

            $(target).remove();
        });
    </script>

    <script>
        // form biaya lainnya
        $("#cost_id").select2({
                placeholder:"---- Pilih Biaya ----",
                ajax: {
                    type: "GET",
                    url: "{{route('api.cost.all',['merchantId'=>merchant_id()])}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        console.log(data)
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

            $("#cost_id").on('select2:select', function (e) {
                let data = e.params.data;
                $('#cost_name').val(data.text);
            });

            $("#cost_id").on('select2:open', () => {
                $(".select2-results:not(:has(a))").append('<a id="add-customer" onclick="loadModal(this)" target="{{route("merchant.toko.clinic.services-pack.add-cost")}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Biaya Baru</b></a>');
            });
            $(document).on('click','#add-customer', function(e){
                $("#cost_id").select2('close');
            });

            let iBiaya = 0;
            $('#form-biaya-tambahan').submit(function(){
                let data = getFormData('form-biaya-tambahan');

                let cost_id = data.get('cost_id');
                let amount = data.get('amount');
                let cost_name = data.get('cost_name');


                $('#form-biaya-tambahan input').val('');

                dataBiayaTambahan.push({
                    iBiaya:iBiaya,
                    cost_id: cost_id,
                    amount: amount,
                    cost_name: cost_name,

                });

                $('#body-table-biaya-tambahan .no-data').remove();

                let html = '<tr id="TH-'+iBiaya+'">'+
                    '<td>'+ cost_name +'</td>'+
                    '<td>'+ amount +'</td>'+
                    '<td>'+
                    '<button type="button" data-target="#TH-'+iBiaya+'" data-id="'+iBiaya+'" class="btn btn-xs btn-delete-xs btn-rounded btn_remove_th">'+
                    '<span class="fa fa-trash-alt" style="color: white"></span>'+
                    '</button>'+
                    '</td>'+
                    '</tr>';
                $('#body-table-biaya-tambahan').append(html);
                iBiaya++;
        });

        $(document).on('click', '.btn_remove_th', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");

            dataBiayaTambahan = dataBiayaTambahan.filter(item => {
                return item.iBiaya != parseInt(targetId);
            });

            $(target).remove();
        });
    </script>

    <script>
        // form bahan baku
        $('#form-bahan-baku').submit(function(){
            let data = getFormData('form-bahan-baku');

            let child_product_id = data.get('child_sc_product_id');
            let quantity = data.get('quantity');
            let child_product_name = $('#child_sc_product_id').find(':selected').text();

            let duplicateBahanBakuId = dataBahanBaku.filter(item => {
                return item.child_sc_product_id == $(this).find(':selected').val();
            });


            $("#child_sc_product_id").val('').trigger('change');
            $('#form-bahan-baku input').val('');

            if(duplicateBahanBakuId.length > 0){
                return;
            }

            dataBahanBaku.push({
                child_sc_product_id: child_product_id,
                quantity:quantity,
            });

            $('#body-table-bahan-baku .no-data').remove();

            let html = '<tr id="BB-'+child_product_id+'">'+
                '<td>'+ child_product_name+'</td>'+
                '<td>'+ quantity +'</td>'+
                '<td>'+
                '<button type="button" data-target="#BB-'+child_product_id+'" data-id="'+child_product_id+'" class="btn btn-xs btn-delete-xs btn-rounded btn_remove_bb">'+
                '<span class="fa fa-trash-alt" style="color: white"></span>'+
                '</button>'+
                '</td>'+
                '</tr>';
            $('#body-table-bahan-baku').append(html);
        });

        $(document).on('click', '.btn_remove_bb', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");
            dataBahanBaku = dataBahanBaku.filter(item => {
                return item.child_sc_product_id != parseInt(targetId);
            });
            $(target).remove();
        });
    </script>
    <script>
        $("#is_sell_to_ecommerce").change(function() {
            if(this.checked) {
                $("#ecommerce").val(1)
                $(".ecommerce").removeAttr("hidden");

            }else{
                $("#ecommerce").val(0)
                $(".ecommerce").attr("hidden",true);

            }
        });

        $("#is_consignment").change(function() {
            if(this.checked) {
                $("#consignment").val(1)

            }else{
                $("#consignment").val(0)

            }
        });
        $('#md_unit_id').on('input',function(e){
            let typeMulti = ($("#satuan").select2('data'))? $("#satuan").select2('data'):[];
            let unit = ($("#md_unit_id").select2(['data']))?$("#md_unit_id").select2('data'):[]
            if(typeMulti.length>0)
            {
                if(unit.length>0){
                    $("#jumlah-multi").text("Konversi jumlah dari "+typeMulti[0]['text']+" ke "+unit[0]['text']+"")

                }else{
                    $("#jumlah-multi").text("Konversi jumlah  dari "+typeMulti[0]['text']+" ke ")

                }

            }else{
                if(unit.length>0){
                    $("#jumlah-multi").text("Konversi jumlah  dari  ke "+unit[0]['text']+"")

                }else{
                    $("#jumlah-multi").text("Konversi jumlah  dari  ke  ")

                }

            }

        });

        $('#satuan').change(function(){
            let typeMulti = ($("#satuan").select2('data'))? $("#satuan").select2('data'):[];
            let unit = ($("#md_unit_id").select2(['data']))?$("#md_unit_id").select2('data'):[]
            if(typeMulti.length>0)
            {
                if(unit.length>0){
                    $("#jumlah-multi").text("Konversi jumlah dari "+typeMulti[0]['text']+" ke "+unit[0]['text']+"")

                }else{
                    $("#jumlah-multi").text("Konversi jumlah  dari "+typeMulti[0]['text']+" ke ")

                }

            }else{
                if(unit.length>0){
                    $("#jumlah-multi").text("Konversi jumlah ke "+unit[0]['text']+" ")

                }else{
                    $("#jumlah-multi").text("Konversi jumlah ke ")

                }

            }

        });

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endsection

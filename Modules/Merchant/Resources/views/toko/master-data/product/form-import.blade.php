<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <b>Langkah Upload Barang/Jasa</b>
        <li>Untuk <b class="text-danger">format excel</b> download disini <a href="{{asset('public/example/format_produk.xlsx')}}" style="color: #0d5ff5"><b>Download</b> </a> </li>
        <li>Masukkan data sesuai format (.xls) yang bertanda merah wajib diisi (kode_barang,
            harga_jual dan
            nama_barang/jasa)</li>
        <li>Pastikan Tipe Data anda TEXT</li>
        <li>Pastikan data anda tidak mengandung karakter seperti (, : ; ')</li>
        <li>Isilah pakai_stok dengan nilai 1 untuk <b class="text-danger">barang</b>  yang mempunyai stok dan 0 untuk <b class="text-danger">jasa atau layanan</b> yang stoknya unlimited</li>
        <li>Isi kategori dengan huruf kecil, bila belum ada kategori maka akan menambahkan sesuai namanya</li>
        <li>Isi tipe_produk 1 untuk tipe <b class="text-danger">Produk Jadi</b> dan tipe 2 untuk <b class="text-danger">Barang produksi</b> dan 8 untuk <b class="text-danger">Bahan Baku</b> </li>
        <li>Unggah file excel anda dibawah ini lalu klik <b style="color: #00c21a">simpan</b></li>

        <input type="file" class="form-control" name="file"required>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.product.save-import')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });


        })
    })
</script>

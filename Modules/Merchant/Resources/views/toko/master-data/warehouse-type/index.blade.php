@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Master Data</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.warehouse-type.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kelola gudang dengan berbagai tipe gudang</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a onclick="activeMenu('master-data','master-data-manajemen')" class="nav-link active" href="{{route('merchant.toko.warehouse-type.index')}}">Tipe Gudang</a>
            </li>
            <li class="nav-item">
                <a onclick="activeMenu('master-data','master-data-manajemen')" class="nav-link" href="{{route('merchant.toko.warehouse.index')}}">Gudang</a>
            </li>
        </ul>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row" style="margin-bottom: 10px">
                    {!! $add !!}
                </div>
                <div class="table-responsive">
                    <table class="table table-custom" id="table-data-warehouse" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($tableColumns as $key =>$item)
                                <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.warehouse-type.delete')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        }
        function reloadDataTable(isReload=0){
            ajaxDataTable('#table-data-warehouse', 1, "{{route('merchant.toko.warehouse-type.datatable')}}?key_val={{$key_val}}", [
                    @foreach($tableColumns as $key =>$item)
                    @if($tableColumns[$key]=='action')
                {
                    data: '{{$tableColumns[$key]}}',
                    name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                @endif
                @endforeach
            ],0);
        }
        $(document).ready(function() {
            reloadDataTable(0)

        })
    </script>
@endsection

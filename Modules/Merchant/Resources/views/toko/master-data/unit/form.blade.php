<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-unit" class='form-horizontal form-konten' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Nama Satuan</label>
        <input type="name" class="form-control" name="name" placeholder="Satuan" value="{{$data->name}}" required>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-unit').submit(function () {
            var data = getFormData('form-unit');
            ajaxTransfer("{{route('merchant.toko.unit.save')}}", data, function (response){
                var data = JSON.parse(response);
                    if(data.type=='danger' || data.type=='warning'){
                        otherMessage(data.type,data.message)
                    }else{
                        otherMessage(data.type,data.message)
                        closeModalAfterSuccess()
                        getUnit()
                    }
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Satuan Produk</h4>
                                        <span class="span-text">Untuk menambah satuan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-category" class='form-horizontal form-konten' backdrop="">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Nama Target Penjualan</label>
                <input type="name" class="form-control" name="name" placeholder="Nama Target Penjualan" value="{{$data->name}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Pilih Sales</label>
                <select name="staff_user_id" class="form-control form-control-sm" id="md_role_id" required>
                    @if(!is_null($data->id))
                        <option value="{{$data->staff_user_id}}" selected>{{$data->getStaffUser->fullname}}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-4">

            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Target Kunjungan</label>
                <input type="number" class="form-control" name="visit_count" placeholder="Target Kunjungan" value="{{$data->visit_count}}" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Target Penjualan</label>
                <input type="number" class="form-control" name="min_sale" placeholder="Target Penjualan" value="{{$data->min_sale}}" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Target Pelanggan Baru</label>
                <input type="number" class="form-control" name="min_new_customer" placeholder="Target Pelanggan Baru" value="{{$data->min_new_customer}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputEmail4" class="font-weight-bold"> Periode Mulai Target</label>
                <input type="text" class="form-control trans_time" name="start_date"  value="{{$data->start_date}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputPassword4" class="font-weight-bold">Periode Akhir Target</label>
                <input type="text" class="form-control trans_time" name="end_date"  value="{{$data->end_date}}">
            </div>
        </div>

    </div>







    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-category').submit(function () {
            var data = getFormData('form-category');
            ajaxTransfer("{{route('merchant.toko.sfa.kpi.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    })
    $(document).ready(function(){
        dateTimePicker(".trans_time");
        getStaff()
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah KPI Target Penjualan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    function getStaff()
    {
        $("#md_role_id").select2({
            placeholder:"Pilih Karyawan",
            ajax: {
                type: "GET",
                url: "{{route('merchant.ajax.share-data.employee-by-merchant')}}?is_non_employee=0&md_merchant_id={{merchant_id()}}",
                dataType: 'json',
                delay: 250,
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });

    }


</script>

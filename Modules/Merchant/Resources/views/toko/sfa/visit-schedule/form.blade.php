@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->

    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Penjualan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.sfa.schedule.index')}}">Jadwal Kunjungan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa menambah jadwal kunjungan setiap salesmu dengan mengisi kolom di bawah ini.</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <input type="hidden" class="form-control form-control-sm" name="timezone" id="timezone" value="" >
                    <div class="row justify-content-center mb-4">
                        <div class="col-md-5">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Pilih Sales</label>
                                <div class="col-sm-8">
                                    <select name="staff_user_id" class="form-control form-control-sm" id="sc_supplier_id"
                                            @if(!is_null($staffId)) disabled @endif required>
                                        @if(!is_null($staffId))
                                            <option value="{{$data->first()->staff_user_id}}" selected>{{$data->first()->getStaffUser->fullname}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-5 offset-md-2">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label font-weight-bold">Tanggal Kunjungan</label>
                                <div class="col-sm-8">
                                    @if(!is_null($staffId))
                                        <input type="text" class="form-control form-control-sm trans_time" name="visit_schedule" value="{{$data->first()->visit_schedule}}" placeholder="Tanggal Kunjungan" required>
                                    @else
                                        <input type="text" class="form-control form-control-sm trans_time" name="visit_schedule" value="{{$data->visit_schedule}}" placeholder="Tanggal Kunjungan" required>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-12">
                            <table class="table table-custom text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="70%">Pelanggan</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!is_null($staffId))
                                    @foreach ($data as $key => $d)
                                        @if($d->is_deleted==0)
                                            <tr id="row{{$key}}">
                                                <td>
                                                    <select class="form-control form-control-sm sc_product_id" name="sc_customer_id[]" id="sc_product_id{{$key}}"
                                                            @if(!is_null($staffId)) disabled @endif
                                                    >
                                                        <option selected value="{{$d->sc_customer_id}}">
                                                            {{$d->getCustomer->name}}
                                                            @if(!is_null($d->getCustomer->address))
                                                                - {{$d->getCustomer->address}}
                                                            @endif
                                                            @if(!is_null($d->getCustomer->phone_number))
                                                                - {{$d->getCustomer->phone_number}}
                                                            @endif
                                                        </option>
                                                        <input type="hidden" value="{{$d->id}}" name="id[]">
                                                    </select>
                                                </td>
                                                <td>
                                                    <button type="button" name="remove" id="{{$key}}" class="btn btn-xs btn-delete-xs btn_remove">
                                                        <span class="fa fa-trash-alt" style="color: white"></span> </button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>

                            </table>

                            <button type="button" name="add" id="add" class="btn btn-darkblue btn-rounded py-2 px-4">
                                <i class="fa fa-plus mr-2"></i>
                                <span>Tambah Kunjungan</span>
                            </button>
                            <hr>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type='hidden' name='original_date' value='{{ $date }}'>

                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            getStaff()
            $('.sc_product_id').select2();
            var i={{is_null($staffId)?0:$data->count()-1}};
            @if(is_null($staffId))
            var productSelectedId = {};
            @else
            var productSelectedId = {};

            @foreach($data as $k =>$dd)
                @if($dd->is_deleted==0)
                productSelectedId['row{{$k}}']={{$dd->sc_customer_id}};
                @endif
            @endforeach
            @endif
            $('#add').click(function(){
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">' +
                    '<td>' +
                    '<select  name="sc_customer_id[]" id="sc_product_id' + i + '" class="form-control" ></select>' +
                    '</td>' +
                    '<td>' +
                    '<button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs btn_remove"> '+
                    '<span class="fa fa-trash-alt" style="color: white"></span> </button>'+
                    '</td>' +
                    '</tr>');
                    $("#sc_product_id"+i).select2({
                        placeholder:"Pilih Customer",
                        ajax: {
                            type: "GET",
                            url: "{{route('merchant.toko.transaction.sale-order.customer-list')}}",
                            dataType: 'json',
                            delay: 250,
                            headers:{
                                "senna-auth":"{{get_user_token()}}"
                            },
                            data: function (params) {
                                return {
                                    key: params.term
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: data
                                };
                            },
                            cache: true
                        },
                    });
                $("#sc_product_id" + i).on('select2:select', function (e) {

                    var data = e.params.data;
                    var rowId = $(this).closest('tr').attr('id');
                    productSelectedId[rowId] = data.id;
                    Object.keys(productSelectedId).forEach(key => {
                        if(productSelectedId[key] === data.id && rowId !== key){
                            $('#'+rowId).remove();
                            delete productSelectedId[rowId];
                        }
                    });

                });

            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                @if(!is_null($staffId))
                var data =new FormData();
                data.append('id',productSelectedId['row'+button_id])
                ajaxTransfer("{{route('merchant.toko.sfa.schedule.delete-detail')}}?staff_id={{$staffId}}&date={{$date}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
                @endif
                    delete productSelectedId['row'+button_id];

            });
        });

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.sfa.schedule.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })

        function getStaff()
        {
            $("#sc_supplier_id").select2({
                placeholder:"Pilih Karyawan",
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.ajax.share-data.employee-by-merchant')}}?is_non_employee=0&md_merchant_id={{merchant_id()}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

        }

    </script>
@endsection





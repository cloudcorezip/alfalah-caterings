
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                <h5 align="center">Detail Absensi Periode {{$startDate}}-{{$endDate}}</h5>
                <hr>
                <table class="table table-bordered" id="table-data" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Karyawan</td>
                        <td>Jam Bekerja</td>
                        <td>Pulang Bekerja</td>
                        <td>Keterangan</td>
                    </tr>
                    </thead>
                    <tbody>          
                    @foreach($data as $key =>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->getStaff->fullname}}</td>
                            <td>{{$item->start_work}}</td>
                            <td>{{$item->end_work}}</td>
                            <td>{{$item->type_of_attendance}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </form>
            
        </div>
    </div>

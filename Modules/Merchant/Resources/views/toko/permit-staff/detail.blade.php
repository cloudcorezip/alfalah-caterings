<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        <div class="col-md-6" style="border-right: 1px solid rgba(186, 186, 186, 0.32);">
            <div class="row px-4">
                <div class="col-md-12 mb-3">
                    <h6>Staff</h6>
                    <h4 class="font-weight-bold">{{$data->fullname}}</h4>
                </div>
                <div class="col-md-12 mb-3">
                    <h6>Tanggal Izin</h6>
                    <h4 class="font-weight-bold">{{\Carbon\Carbon::parse($data->start_date)->isoFormat('DD MMMM YYYY')}} - {{\Carbon\Carbon::parse($data->end_date)->isoFormat('DD MMMM YYYY')}}</h4>
                </div>

                <div class="col-md-12 mb-3">
                    <h6>Kategori Izin</h6>
                    @if(!is_null($data->permit_category_name))
                    <h4 class="font-weight-bold">{{$data->permit_category_name}}</h4>
                    @else
                    <h4 class="font-weight-bold">{{$data->hr_category_name}}</h4>
                    @endif
                </div>
                <div class="col-md-12 mb-3">
                    <h6>Keterangan</h6>
                    @if(is_null($data->desc))
                    <p>-</p>
                    @else
                    <p>{{$data->desc}}</p>
                    @endif
                </div>

                @if($data->is_approved == 0)
                <div class="col-md-12 mb-3">
                    <h6>Setujui / tolak izin</h6>
                    <a href="{{route('merchant.toko.permit-staff.approve',['id'=>$data->id])}}" class="btn btn-success btn-sm text-white">Setuju</a>
                    <a href="{{route('merchant.toko.permit-staff.canceled',['id'=>$data->id])}}" class="btn btn-danger btn-sm text-white">Tolak</a>
                </div>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <img 
                        style="height:100%;width:100%;max-height:600px;object-fit:cover;border-radius: 10px;"
                        src="{{env('S3_URL')}}{{$data->supporting_file}}" 
                        alt="foto pendukung"
                        class="mx-auto"
                    >
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer" style="display:block;">
        @if($data->is_approved == 0)
        <div class="alert alert-warning text-center text-dark">
            <span>Izin belum disetujui</span>
        </div>
        @endif
        @if($data->is_approved == 1)
        <div class="alert alert-success text-center text-dark">
            <span>Izin telah disetujui</span>
        </div>
        @endif

        @if($data->is_approved == 2)
        <div class="alert alert-danger text-center text-dark">
            <span>Izin telah ditolak</span>
        </div>
        @endif
    </div>
</form>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div>
                                         <h4 class="modal-title">Detail Izin</h4>
                                        <span class="span-text">Berikut rincian izinmu</span>
                                    </div>`);
    });


</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.toko.report.staff-attendance.data')}}">Human Resources</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a class="font-weight-bold">Tambah Izin/Cuti</a>
                    </li>
                </ol>
            </nav>
            <span>Untuk menambah izin/cuti kerja, kamu harus mengisi data di bawah ini</span>
        </div>
    </div>
</div>

<!-- DataTales Example -->

<div class="container-fluid">
    <form onsubmit="return false;" id="form-konten" class="form-horizontal" backdrop="">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="font-weight-bold mb-3">Informasi Absen</h4>
                        <hr>
                    </div>
                    @include('merchant::component.branch-selection',
                    ['withoutModal'=>true,
                        'data'=>$data,
                        'subTitle'=>'Tentukan cabang mana yang akan mencatat absensi dari karyawanmu dan kamu dapat dengan mudah mengetahui riwayat absensi karyawanmu',
                        'selector'=>'branch',
                        'with_onchange'=>true,
                    ])

                    <div class="col-md-6 mb-2">
                        <label class="font-weight-bold">Karyawan</label>
                        <select class="form-control form-control-sm select-custom" id="staff" name="md_staff_user_id" required>
                            <option></option>
                        </select>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="form-group">
                            <label class="font-weight-bold">Keperluan Izin</label>
                            <select class="form-control form-control-sm select-custom" id="hr_leave_id" name="hr_leave_id"  required>
                                <option></option>
                                @foreach($leaveOption as $key =>$item)
                                    <option value="{{$item->id}}">{{$item->keterangan}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6 mb-2">
                        <div class="form-group">
                            <label class="font-weight-bold">Tanggal Mulai Izin</label>
                            <input type="text" class="form-control form-control-sm trans_time" name="start_date"  placeholder="Tanggal Mulai Izin" required>
                        </div>
                    </div>

                    <div class="col-md-6 mb-2">
                        <div class="form-group">
                            <label class="font-weight-bold">Tanggal Selesai Izin</label>
                            <input type="text" class="form-control form-control-sm trans_time" name="end_date"  placeholder="Tanggal Selesai Izin" required>
                        </div>
                    </div>

                    <div class="col-md-6 mb-2">
                        <div class="form-group">
                            <label class="font-weight-bold">Catatan</label>
                            <textarea
                                class="form-control"
                                rows="3"
                                name="note"
                                cols="50"
                                placeholder="Catatan"
                                @if(!is_null($data->id)) disabled @endif
                            ></textarea>
                        </div>
                    </div>

                    <div class="col-md-6 mb-2">
                        <div class="row">
                            <div class="col-md-4">
                                @include('merchant::component.image-upload',
                                ['title'=>'Dokumen Pendukung',
                                'fieldName'=>'support_file',
                                'file' => $data->end_work_file,
                                'description' => ''
                                ])
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 text-right">
                <input type="hidden" name="id" value="{{$data->id}}">
                <a href="{{route('merchant.toko.permit-staff.data')}}" class="btn btn-light text-light mr-2">Kembali</a>
                <button class="btn btn-success"><i class="fa fa-save mr-2"></i> Simpan</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    $("#staff").select2({
        placeholder: '--- Pilih Karyawan ---'
    });
    $("#hr_leave_id").select2({
        placeholder: '--- Pilih Keperluan Izin ---'
    })

    function onchangeBranch(){
        let branch = $("#branch").val();

        $("#staff").find('option').not(':first').remove();
        $("#staff").select2({
            placeholder:'--- Pilih Karyawan ---',
            ajax: {
                type: "GET",
                url: "{{route('merchant.ajax.share-data.employee-by-merchant')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        key:params.term,
                        md_merchant_id:branch,
                        is_non_employee: 0
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });

    }

    $(document).ready(function () {
        dateTimePicker(".trans_time");
        onchangeBranch();

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.permit-staff.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    });
</script>
@endsection

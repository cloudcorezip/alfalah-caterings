@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.consignment.data')}}">Penerimaan Konsinyasi</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Tambahkan penerimaan konsinyasi sesuai dengan isian dibawah ini dengan baik dan benar</span>

            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">

        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="results"></div>
                <form id="form-konten" onsubmit="return false">
                    <div class="row">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>true,
'function_onchange'=>'getWarehouse()'

])
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Kode Penerimaan</label>
                                <div class="col-sm-7">
                                    <input type="text" name="second_code" class="form-control" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Supplier</label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="supplier" name="sc_supplier_id" required>
                                        <option value="-1">Pilih Supplier</option>
                                    @foreach($supplier as $w)
                                            <option value="{{$w->id}}" @if($w->id==$data->sc_supplier_id) selected @endif>{{$w->nama_supplier}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label font-weight-bold">Tanggal Penerimaan</label>
                                <div class="col-sm-7">
                                    <input type="text"  class="form-control form-control-sm trans_time" name="time_received" value="{{$data->time_received}}" placeholder="Waktu Penerimaan" @if(!is_null($data->id)) disabled @endif required>
                                </div>
                            </div>

                                <div class="form-group row">
                                    <label class="col-sm-5 col-form-label font-weight-bold">Gudang</label>
                                    <div class="col-sm-7">
                                        <select class="form-control" id="warehouse" name="inv_warehouse_id" required onchange="clearTable()">
                                        </select>
                                    </div>
                                </div>

                        </div>

                    </div>

                    <div class="row mb-3">
                        <br>
                        <br>
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="30%">Nama Produk</th>
                                    <th width="15%">Diterima</th>
                                    <th width="20%">Harga</th>
                                    <th width="15%">Satuan</th>
                                    <th width="15%">Sub Total</th>
                                    <th width="10%">
                                        Opsi
                                    </th>
                                </tr>
                                </thead>
                                <tbody id="tbodyid">

                                </tbody>

                            </table>
                            <button type="button" name="add" id="add" class="btn btn-darkblue btn-rounded py-2 px-4">
                                <i class="fa fa-plus mr-2"></i>
                                <span>Tambah Produk</span>
                            </button>
                            <hr>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="font-weight-bold">Bukti Penerimaan</label>
                                <input type="file" class="form-control" name="attachment_file">
                                <small style="color: red">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</small>
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan :</label>
                                <textarea name="note" class="form-control form-control-sm" id="" cols="20" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-5 offset-md-2">
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right total" name="total" id="total" value="{{$data->total}}" disabled>
                                    <input type="hidden" name="original_total" class="form-control form-control-sm text-right"  id="original_total" disabled>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        var productSelectedId = {};

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        $(document).ready(function() {

            $("#warehouse").select2();
            $("#branch").select2();
            $("#supplier").select2();
            $('.total').mask('#.##0,00', {reverse: true});
            getWarehouse()

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            var i=0;

            $('#add').click(function(){

                var warehouse=$("#warehouse").val()
                var branch = $("#branch").val()
                var status = false
                if(branch==null){

                    otherMessage('warning','Kamu belum memilih cabang penerimaan !')
                    status = true
                }

                if(warehouse==null){
                    otherMessage('warning','Kamu belum memilih gudang penerimaan!')
                    status = true

                }
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">' +
                    '<td>' +
                    '<select  name="sc_product_id[]" id="sc_product_id' + i + '" class="form-control" onchange="productChange('+i+')" required ></select>' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="quantity[]" min="0" step="0.01" id="quantity'+i+'" class="form-control form-control-sm quantity" value="0" required onkeypress="return isNumber(event)" />' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="price[]" class="form-control form-control-sm price text-right"  id="price'+i+'" required/>' +
                    '</td>' +
                    '<td>' +
                    '<input type="hidden" name="json_multi_unit[]" id="json_multi_unit'+i+'" class="form-control form-control-sm" value="-1"/>' +
                    '<input type="hidden" name="multi_unit_id[]" id="multi_unit_id'+i+'" class="form-control form-control-sm" value="-1"/>' +
                    '<input type="hidden" name="nilai_konversi[]" id="nilai_konversi'+i+'" class="form-control form-control-sm" value="1"/>' +
                    '<input type="hidden" name="is_multi_unit[]" id="is_multi_unit'+i+'" class="form-control form-control-sm" value="0"/>' +
                    '<input type="hidden" name="unit_name[]" id="unit_name'+i+'" class="form-control form-control-sm" value=""/>' +
                    '<select  name="unit_list[]" id="unit_list' + i + '" class="form-control" onchange="getMultiUnit('+i+')" required></select>' +
                    '</td>' +
                    '<td>' +
                    '<input type="text" name="subtotal[]" id="subtotal'+i+'" class="form-control form-control-sm sub_total text-right" value="0" disabled/>' +
                    '<input type="hidden" name="subtotal_original[]" id="subtotal_original'+i+'" class="form-control form-control-sm sub_total_original" value="0" disabled/>' +
                    '<input type="hidden" name="product_name[]" id="product_name'+i+'" class="form-control form-control-sm product_name" value="" disabled/>' +
                    '</td>' +
                    '<input type="hidden" name="coa_inv_id[]" id="coa_inv_id'+i+'" class="form-control form-control-sm"/>' +
                    '<input type="hidden" name="coa_hpp_id[]" id="coa_hpp_id'+i+'" class="form-control form-control-sm"/>' +
                    '<td>' +
                    '<button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs btn_remove"> '+
                    '<span class="fa fa-trash-alt" style="color: white"></span> </button>'+
                    '</td>' +
                    '</tr>');

                getProductByWarehouse(i)
                $('.price').mask('#.##0,00', {reverse: true});
                $("#unit_list"+i).select2()
                if(status === true)
                {
                    $("#tbodyid").empty();

                }
            });


            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                sumTotal();
                delete productSelectedId[button_id];

            });
            $("table").on("change", "input", function() {
                var row = $(this).closest("tr");
                var quantity = parseFloat(row.find(".quantity").val());
                var selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal(row.find('.price').val(),'.',''),',','.');
                var subtotal=quantity*selling_price;
                row.find(".sub_total").val(currencyFormat(subtotal,''));
                row.find(".sub_total_original").val(subtotal);
                sumTotal()
            });

            $("#warehouse").on('select2:select', function (e) {
                productSelectedId = {}
            });

        });

        function sumTotal(){
            var sum = 0;
            $(".sub_total_original").each(function(){
                sum += +$(this).val();
            });
            $("#total").val(currencyFormat(sum,''))
            $("#original_total").val(sum)

        }

        function getProductByWarehouse(i)
        {
            var warehouse=$("#warehouse").val()
            var branch = $("#branch").val()

            $("#sc_product_id" + i).select2({
                ajax: {
                    type: "GET",
                    url: "{{route('api.product.stockadjustment')}}?merchant_id="+branch+"&warehouse="+warehouse+"&is_consignment=1",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

        }

        function getWarehouse()
        {
            var branch=$("#branch").val()

            $("#warehouse").empty()
            $("#warehouse").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.stock-adjustment.get-warehouse')}}?merchant_id="+branch,
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

            $("#tbodyid").empty();

        }

        function clearTable()
        {
            $("#tbodyid").empty();
        }



        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            if(data.sc_supplier_id=='-1'){

                otherMessage('warning','Kamu belum memilih supplier !')
            }

            ajaxTransfer("{{route('merchant.toko.consignment.save')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })

        function getMultiUnit(i)
        {

            let unitList = ($("#unit_list"+i).select2('data'))? $("#unit_list"+i).select2('data'):[];
            console.log(unitList)
            if(unitList.length>0)
            {
                let data=unitList[0]
                $("#is_multi_unit"+i).val(1)
                $("#unit_name"+i).val(data['text'])
                $("#nilai_konversi"+i).val(data['nilai_konversi'])
                $("#multi_unit_id"+i).val(data['original_id'])
                $("#price"+i).val(currencyFormat(data['selling_price'],''))
                let quantity=$("#quantity"+i).val();
                let selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#price"+i).val(),'.',''),',','.')
                $("#subtotal"+i).val(currencyFormat(quantity*selling_price,''))
                $("#subtotal_original"+i).val(quantity*selling_price)
            }

            sumTotal();
        }

        function productChange(i)
        {
            let products = ($("#sc_product_id"+i).select2('data'))? $("#sc_product_id"+i).select2('data'):[];

            if(products.length>0)
            {
                let datas = products[0]
                $("#coa_inv_id"+i).val(datas['inv_id'])
                $("#coa_hpp_id"+i).val(datas['hpp_id'])
                $("#product_name"+i).val(datas['text'])
                let multi_unit=JSON.parse(datas['multi_unit'])
                let multi_unit_list=[]
                $('#unit_list'+i).empty();
                if(multi_unit.length>0)
                {
                    $("#price"+i).val(currencyFormat(0,''))
                    let quantity=$("#quantity"+i).val();
                    let selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#price"+i).val(),'.',''),',','.')
                    $("#subtotal"+i).val(currencyFormat(quantity*selling_price,''))
                    $("#subtotal_original"+i).val(quantity*selling_price)

                    $.each(multi_unit,function (iteration,value){
                        if(iteration===0)
                        {
                            multi_unit_list.push({
                                id:'-1',
                                text:'Pilih Satuan',
                                selling_price:0,
                                nilai_konversi:0,
                                original_id:'-1'
                            })
                        }

                        multi_unit_list.push({
                            id:value.unit_id,
                            text:value.konversi_ke,
                            selling_price:value.harga_jual,
                            nilai_konversi:value.nilai_konversi,
                            original_id:value.id
                        })
                    })
                    $("#unit_list"+i).select2({
                        data:multi_unit_list
                    })
                    $("#json_multi_unit"+i).val(JSON.stringify(multi_unit_list))
                    sumTotal();
                }else{
                    $("#is_multi_unit"+i).val(0)
                    $("#unit_name"+i).val(datas['unit_name'])

                    multi_unit_list.push({
                        id:datas['unit_id'],
                        text:datas['unit_name'],
                        selling_price:datas['purchase_price'],
                        nilai_konversi:1,
                        original_id:datas['id']
                    })
                    $("#unit_list"+i).select2({
                        data:multi_unit_list
                    })
                    $("#price"+i).val(currencyFormat(datas['purchase_price'],''))
                    let quantity=$("#quantity"+i).val();
                    let selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#price"+i).val(),'.',''),',','.');
                    $("#subtotal"+i).val(currencyFormat(quantity*selling_price,''))
                    $("#subtotal_original"+i).val(quantity*selling_price)
                    sumTotal();

                }

                let status = false;
                Object.keys(productSelectedId).forEach(key => {
                    if(productSelectedId[key] === parseInt(datas['id'])){
                        status=true;
                    }
                });
                if(status)
                {
                    $('#row'+i).remove();
                    delete productSelectedId[i];
                }else{
                    productSelectedId[i] = parseInt(datas['id']);

                }
            }


        }
    </script>
@endsection

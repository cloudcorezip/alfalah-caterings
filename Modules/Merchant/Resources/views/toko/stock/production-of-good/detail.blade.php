@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock.production.index')}}">Produksi Produk</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan informasi lengkap terkait detail aktivitas produksi produk</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->

    <div class="container-fluid">
        <ul class="nav nav-tabs tabs-trans" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" role="tab" aria-controls="transaksi" aria-selected="true">Item Produksi</a>
            </li>

        </ul>
        <div class="row">
            <div class="col-md-3">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Kode Produksi :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->code}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Produk Produksi :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->getProduct->name}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd class="trans-info">
                                <span>Gudang Produksi :</span>
                            </dd>
                            <dt>
                                <h5 class="text-tosca">{{$data->getWarehouse->name}}</h5>
                            </dt>
                        </dl>

                        <dl class="trans-info">
                            <dd>
                                <span>Waktu Produksi :</span>
                            </dd>
                            <dt>
                                <h5>{{\Carbon\Carbon::parse($data->time_of_production)->isoFormat(' D MMMM Y  HH:mm:ss')}}</h5>
                            </dt>
                        </dl>

                        <dl class="trans-info">
                            <dd>
                                <span>Jumlah Produksi :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->amount}} {{(is_null($data->getProduct->getUnit))?'-':$data->getProduct->getUnit->name}}</h5>

                            </dt>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card shadow mb-4">
                    <div class="card-body">

                        <h6 class="font-weight-bold mb-4">Komponen Produk</h6>
                        <div class="table-responsive">
                            <table class="table no-margin text-center table-custom">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Barang</th>
                                    <th>Jumlah/Unit</th>
                                    <th>Jumlah Penggunaan</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($material as $key =>$item)
                                    <tr>
                                        <td class="center">{{$key+1}}</td>
                                        <td>[{{$item->code}}] {{$item->name}}</td>
                                        <td class="center">{{$item->quantity}} {{$item->unit_name}}</td>
                                        <td class="center">
                                            {{$data->getDetail->where('sc_product_id',$item->id)->sum('amount')}}
                                            {{$item->unit_name}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <h6 class="font-weight-bold mb-4">Detail Biaya Produksi</h6>
                        <div class="table-responsive">
                            <table class="table no-margin text-center table-custom">

                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Biaya</th>
                                    <th>Tujuan Biaya</th>
                                    <th>Asal Biaya</th>
                                    <th>Total Biaya</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($bom as $key =>$item)

                                    <tr>
                                        <td class="center">{{$key+1}}</td>
                                        <td>{{$item->name}}</td>
                                        <td class="center">{{$item->payment_to }}</td>
                                        <td class="center">Melalui {{$item->getCoa->name}}</td>
                                        <td class="center">{{rupiah($item->amount_of_bom)}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                <tr>
                                    <td class="font-weight-bold" style="border-bottom:none!important;" colspan="4">TOTAL</td>
                                    <td class="font-weight-bold" style="border-bottom:none!important;">{{rupiah($bom->sum('amount_of_bom'))}}</td>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-8">
                                <table class="table-note">
                                    <tr>
                                        <td class="text-note">Harga Pokok Produksi</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td class="text-note">                                {{rupiah($data->total/$data->amount)}}/ {{(is_null($data->getProduct->getUnit))?'-':$data->getProduct->getUnit->name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-note">Harga Jual</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td class="text-note">
                                            {{rupiah($data->selling_price)}} / {{(is_null($data->getProduct->getUnit))?'-':$data->getProduct->getUnit->name}}
                                            @if($data->is_deleted==0)
                                                <a onclick="loadModal(this)" target="{{route('merchant.toko.stock.production.add-harga-jual')}}" data="id={{$data->id}}" class="btn btn-purple btn-xs btn-rounded text-white" data-original-title="" title="" rel="noopener noreferrer" href="#modal-target" data-toggle="modal">Ubah Harga Jual</a>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-note">Catatan</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td class="text-note">{{$data->note}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-note">Pilihan</td>
                                        <td class="text-note text-center" width="40px">:</td>
                                        <td>
                                            @if($data->is_use==0 && $data->is_deleted==0)
                                                <a onclick="hapusProduksi({{$data->id}})" class="btn btn-danger btn-xs btn-rounded" data-original-title="" title="" rel="noopener noreferrer">Hapus Data</a>
                                                <a href="{{route('merchant.toko.stock.production.add',['id'=>$data->id])}}" class="btn btn-primary btn-xs btn-rounded text-white" data-original-title="" title="" rel="noopener noreferrer">Ubah Data</a>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@section('js')
    <script>
        function hapusProduksi(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.stock.production.delete')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }
    </script>

@endsection

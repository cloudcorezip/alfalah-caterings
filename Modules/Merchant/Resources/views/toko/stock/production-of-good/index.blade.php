@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock.production.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Daftar aktivitas produksi produk usahamu secara rinci mulai dari tempat, jumlah, hingga waktu produksi</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row mb-3">
                    {!! $add !!}
                </div>
                <div id="output-discount-product">
                    @include('merchant::toko.stock.production-of-good.list')
                </div>
            </div>
        </div>

        <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
            <div class="d-flex align-items-center">
                <i class="fas fa-filter mr-2 text-white"></i>
                <span class="text-white form-filter3-text-header">Filter</span>
            </div>
        </a>

        <div class="form-filter3" id="form-filter3">
            <div class="row">
                <div class="col-12">
                    <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center">
                            <i class="fas fa-filter mr-2"></i>
                            <span class="text-white form-filter3-text-header">Filter</span>
                        </div>
                        <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                            <i style="font-size:14px;" class="fas fa-times text-white"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row mb-3 px-30">
                <div class="col-12">
                    <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                    <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
                </div>
            </div>
            <hr>
            <form id="form-filter-production-of-good" class="px-30" onsubmit="return false">
                <div class="row d-flex align-items-center">
                    <div class="col-md-12 mb-3">
                        <label for="exampleInputPassword1">Pilih Cabang</label>
                        <select name="md_merchant_id" id="branch" class="form-control form-control" multiple required>
                            <option value="-1" selected>Semua Cabang</option>
                            @foreach(get_cabang() as $key  =>$item)
                                <option value="{{$item->id}}">{{$item->nama_cabang}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12 mt-3">
                        <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                        <input type="hidden" name="search_key" value="{{$searchKey}}">
                    </div>
                </div>

            </form>

        </div>

    </div>
@endsection

@section('js')
<script>
    $("#branch").select2();
    $("#branch").on("select2:select", function(e){
        if(e.params.data.id == "-1"){
            $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
        } else {
            $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
        }
    });

    $(document).ready(function(){
        $('#form-filter-production-of-good').submit(function () {
            let data = getFormData('form-filter-production-of-good');

            let selectedMerchantId = $("#branch").select2('data');
            let merchantIds = [];

            selectedMerchantId.forEach(item => {
                merchantIds.push(parseInt(item.id));
            });

            data.append('md_merchant_id',JSON.stringify(merchantIds));

            ajaxTransfer("{{route('merchant.toko.stock.production.reload-data')}}", data, '#output-discount-product');
            showFilter('btn-show-filter3', 'form-filter3');
        });
    });
</script>
@endsection

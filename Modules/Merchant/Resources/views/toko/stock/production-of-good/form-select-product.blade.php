@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<style>
    #progressbar {
        overflow: hidden;
        color: lightgrey;
        width: 40%;
        position: relative;
        justify-content: space-between;
        list-style-type: none;
        display: flex;
    }
    #progressbar .active {
        color: #72BA6C
    }
    #progressbar li {
        list-style-type: none;
        font-size: 15px;
        float: left;
        position: relative;
        font-weight: 400
    }
    #progressbar #step1:before {
        content: "1";
        text-align: center;
    }
    #progressbar #step2:before {
        content: "2";
        text-align: center;
    }

    #progressbar #step3:before {
        content: "3";
        text-align: center;
    }
    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 20px;
        color: #ffffff;
        background: lightgray;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px
    }
    #progressbar li.active:before
    {
        background: #72BA6C
    }
    #progressbar li.active:after {
        background: #72BA6C
    }
    #progressbar li.active:after {
        background: #72BA6C
    }
    #progressbar-connector{
        position: absolute;
        height: 2px;
        background-color: #72BA6C;
        width:70%;
        top: 25px;
        transform: translateY(-50%);
        transform: translateX(-50%);
        left: 50%;
    }
    .btn-remove {
        background:none!important;
        border:none!important;
    }
    .btn-remove .iconify {
        width: 30px;
        height: 30px;
    }
</style>
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a>Persediaan</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.toko.stock.production.index')}}">Produksi Produk</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a>{{$title}}</a>
                    </li>
                </ol>
            </nav>
            <span>Lengkapi data produksi sebelum melakukan proses produksi</span>
        </div>
    </div>
</div>

<!-- DataTales Example -->

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="d-flex justify-content-center mb-4">
                <ul id="progressbar">
                    <div id="progressbar-connector"></div>
                    <li class="active" id="step1">
                        <strong>Informasi Produk</strong>
                    </li>
                    <li id="step2"> 
                        <strong>Biaya Produksi</strong> 
                    </li>
                    <li id="step3">
                        <strong>Ringkasan Produk</strong>
                    </li>
                </ul>

            </div>

        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="alert alert-warning text-center text-dark">
        <span class="iconify" data-icon="emojione:warning"></span>
        <span>Data produksi tidak dapat diubah setelah disimpan. Pastikan data yang kamu masukkan sudah benar !</span>
    </div>
    <form onsubmit="return false;" id="form-konten" class="form-horizontal" backdrop="">
        <div class="row">
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-bold mb-3">Informasi Produk</h4>
                                <hr>
                            </div>
                        </div>
                        @if(is_null($data->id))
                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Tipe Produksi <span class="text-danger">*</span></label>
                            <div class="btn-group-toggle input-radio-group mb-3">
                                <label
                                    class="btn btn-input-radio d-block"
                                >
                                    <a href="{{route('merchant.toko.stock.production.add', ['type'=>'create'])}}">
                                        <span class="text-center">Produksi Baru</span>
                                    </a>
                                </label>
                                <label
                                    class="btn btn-input-radio d-block active"
                                >
                                    <a href="{{route('merchant.toko.stock.production.add', ['type'=>'select'])}}">
                                        <span class="text-center">Produksi Ulang</span>
                                    </a>
                                </label>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Pilih Cabang</label>
                            <select @if(!is_null($data->id)) disabled @endif class="form-control" name="md_merchant_id" id="md_merchant_id" onchange="onchangeBranch()">
                                <option value="-1">--- Pilih Cabang ---</option>
                                @foreach(get_cabang() as $key => $item)
                                <option 
                                    value="{{$item->id}}"
                                    @if($item->id == $data->md_merchant_id)
                                    selected
                                    @endif
                                >
                                    {{$item->nama_cabang}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Pilih Produk</label>
                            <select @if(!is_null($data->id)) disabled @endif class="form-control" name="sc_product_id" id="sc_product_id" onchange="getDataBarang()">
                                <option></option>
                                @if(!is_null($data->id))
                                    <option selected value="{{$data->sc_product_id}}">{{$data->getProduct->name}}</option>
                                @endif
                            </select>
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Waktu Produksi <span class="text-danger">*</span></label>
                            <input
                                class="form-control"
                                name="time_of_production"
                                type="text"
                                value="{{$data->time_of_production}}"
                                placeholder="Waktu Produksi"
                                autocomplete="off"
                                @if(!is_null($data->id)) disabled @endif
                            />
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Jumlah Produksi <span class="text-danger">*</span></label>
                            <div class="position-relative d-flex align-items-center justify-content-between">
                                <div style="width:70%;">
                                    <input 
                                        type="number" 
                                        name="amount" 
                                        class="form-control" 
                                        value="{{$data->amount}}"
                                        @if(!is_null($data->id))
                                        disabled
                                        @endif
                                    >
                                </div>
                                <div style="width:28%;">
                                    <input class="form-control text-center" type="text" id="satuan" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Catatan</label>
                            <textarea
                                class="form-control"
                                rows="3"
                                name="note"
                                cols="50"
                                placeholder="Catatan"
                                @if(!is_null($data->id)) disabled @endif
                            >{{$data->note}}</textarea>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-bold mb-3">Komponen Produksi</h4>
                                <span class="text-light" id="span-komponen"></span>
                                <hr>
                            </div>
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Gudang Produksi</label>
                            <select @if(!is_null($data->id)) disabled @endif class="form-control" name="inv_warehouse_id" id="inv_warehouse_id">
                                <option></option>
                                @if(!is_null($data->id))
                                    @foreach($warehouse as $w)
                                        <option value="{{$w->id}}" @if($data->inv_warehouse_id==$w->id) selected @endif>{{$w->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div id="raw-material-wrapper" class="mb-3">
                
                        </div>

                        @if(is_null($data->id))
                        <div class="col-md-12 mt-4">
                            <div class="form-group">
                                <label>
                                    <input required name="" type="checkbox" value="1" autocomplete="off" /> Data yang saya masukkan adalah benar dan telah diperiksa dengan baik
                                </label>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 text-right">
                @if(!is_null($data->id))
                    <input type='hidden' name='id' value='{{ $data->id }}'>
                @endif
                <input type="hidden" name="type" value="select">
                <a href="{{route('merchant.toko.stock.production.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                <button class="btn btn-success"><i class="fa fa-save mr-2"></i> Simpan</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    $("#md_merchant_id").select2();
    $("#sc_product_category_id").select2();
    $("#sc_merk_id").select2();
    $("#inv_warehouse_id").select2({
        placeholder: "--- Pilih Gudang ---"
    });
    $(".raw-material").select2();
    $("#sc_product_id").select2({
        placeholder: "--- Pilih Produk ---"
    });
    dateTimePicker("input[name=time_of_production]");

    function getDataBarang() {
        var data = new FormData();
        var product=$("#sc_product_id").val()
        ajaxTransfer("{{route('merchant.toko.stock.production.component')}}?id="+product, data, function (response) {
            var response = JSON.parse(response);
            $("#raw-material-wrapper").html('');
            $("#raw-material-wrapper").append(response.html).find(".raw-material").select2({
                placeholder:'--- Pilih Bahan Baku ---'
            });
            $("#satuan").val(response.satuan);
            $("#span-komponen").text(`Tentukan komponen produksimu untuk setiap 1 ${response.satuan}`);
        });
    }

    function onchangeBranch()
    {
        let branch = $("#md_merchant_id").val();
        $("#raw-material-wrapper").empty();
        $("#sc_product_id").find('option').not(':first').remove();
        $("#inv_warehouse_id").empty();

        $("#sc_product_id").select2({
            placeholder: "--- Pilih Produk ---",
            ajax: {
                type: "POST",
                url: "{{route('merchant.toko.stock.production.get-product')}}",
                dataType: 'json',
                delay: 250,
                data: function(params){
                    return {
                        key: params.term,
                        md_merchant_id: branch,
                        _token: "{{csrf_token()}}"
                    }
                },
                processResults: function(data){
                    return {
                        results:data
                    }
                },
                cache: true
            }
        })

        $.ajax({
            type: "POST",
            url: "{{route('merchant.toko.stock.production.get-warehouse')}}",
            data: {
                md_merchant_id: branch,
                _token: "{{csrf_token()}}"
            },
            dataType: 'json',
            success: function(result){
                $("#inv_warehouse_id").select2({
                    data:result
                });
            }
        });
    }

    $(document).ready(function () {
        $("#product").select2();
        $("#gudang").select2();

        @if(!is_null($data->id))
        getDataBarang();
        @endif

        // reloadBiayaProduksi();
        $("#form-konten").submit(function () {

            modalConfirm("Konfirmasi Tindakan", "<div class='alert alert-warning'>Apakah data yang Anda masukkan sudah benar? Data tidak dapat diubah maupun dihapus setelah barang produksi digunakan</div>", function () {
                let data = getFormData("form-konten");
                ajaxTransfer("{{route('merchant.toko.stock.production.save')}}", data, function(response){
                    let data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            });

            
        });
    });



</script>
@endsection

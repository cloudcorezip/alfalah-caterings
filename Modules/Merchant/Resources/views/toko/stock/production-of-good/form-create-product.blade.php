@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<style>
    #progressbar {
        overflow: hidden;
        color: lightgrey;
        width: 40%;
        position: relative;
        justify-content: space-between;
        list-style-type: none;
        display: flex;
    }
    #progressbar .active {
        color: #72BA6C
    }
    #progressbar li {
        list-style-type: none;
        font-size: 15px;
        float: left;
        position: relative;
        font-weight: 400
    }
    #progressbar #step1:before {
        content: "1";
        text-align: center;
    }
    #progressbar #step2:before {
        content: "2";
        text-align: center;
    }

    #progressbar #step3:before {
        content: "3";
        text-align: center;
    }
    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 20px;
        color: #ffffff;
        background: lightgray;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px
    }
    #progressbar li.active:before
    {
        background: #72BA6C
    }
    #progressbar li.active:after {
        background: #72BA6C
    }
    #progressbar li.active:after {
        background: #72BA6C
    }
    #progressbar-connector{
        position: absolute;
        height: 2px;
        background-color: #72BA6C;
        width:70%;
        top: 25px;
        transform: translateY(-50%);
        transform: translateX(-50%);
        left: 50%;
    }
    .btn-remove {
        background:none!important;
        border:none!important;
    }
    .btn-remove .iconify {
        width: 30px;
        height: 30px;
    }
    .delete-row{
        background-color: #fff;
        color:rgba(119, 119, 119, 0.79);
        border:none;
        border-radius: 4px;
    }
    .delete-row:hover {
        background-color: #ff4400;
        color: #fff;
    }
</style>
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a>Persediaan</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.toko.stock.production.index')}}">Produksi Produk</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a>{{$title}}</a>
                    </li>
                </ol>
            </nav>
            <span>Lengkapi data produksi sebelum melakukan proses produksi</span>
        </div>
    </div>
</div>

<!-- DataTales Example -->

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="d-flex justify-content-center mb-4">
                <ul id="progressbar">
                    <div id="progressbar-connector"></div>
                    <li class="active" id="step1">
                        <strong>Informasi Produk</strong>
                    </li>
                    <li id="step2"> 
                        <strong>Biaya Produksi</strong> 
                    </li>
                    <li id="step3">
                        <strong>Ringkasan Produk</strong>
                    </li>
                </ul>

            </div>

        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="alert alert-warning text-center text-dark">
        <span class="iconify" data-icon="emojione:warning"></span>
        <span>Data produksi tidak dapat diubah setelah disimpan. Pastikan data yang kamu masukkan sudah benar !</span>
    </div>
    <form onsubmit="return false;" id="form-konten" class="form-horizontal" backdrop="">
        <div class="row">
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-bold mb-3">Informasi Produk</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Tipe Produksi <span class="text-danger">*</span></label>
                            <div class="btn-group-toggle input-radio-group mb-3" >
                                <label
                                    class="btn btn-input-radio d-block active"
                                >
                                    <a href="{{route('merchant.toko.stock.production.add', ['type'=>'create'])}}">
                                        <span class="text-center">Produksi Baru</span>
                                    </a>
                                </label>
                                <label
                                    class="btn btn-input-radio d-block"
                                >
                                    <a href="{{route('merchant.toko.stock.production.add', ['type'=>'select'])}}">
                                        <span class="text-center">Produksi Ulang</span>
                                    </a>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Pilih Cabang</label>
                            <select class="form-control" name="md_merchant_id" id="md_merchant_id" onchange="onchangeBranch()">
                                <option value="-1">--- Pilih Cabang ---</option>
                                @foreach(get_cabang() as $key => $item)
                                <option value="{{$item->id}}">{{$item->nama_cabang}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Nama <span class="text-danger">*</span></label>
                            <input type="text" id="name" class="form-control" name="name" placeholder="Masukkan nama produk">
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Kategori <span class="text-danger">*</span></label>
                            <select class="form-control" name="sc_product_category_id" id="sc_product_category_id">
                                @foreach($category->where('is_service',0) as $key => $item)
                                <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Satuan <span class="text-danger">*</span></label>
                            <select name="md_unit_id" class="form-control" id="md_unit_id" required>
                                <option></option>
                            </select>
                        </div>
                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Merk</label>
                            <select class="form-control" name="sc_merk_id" id="sc_merk_id">
                                @foreach($merk as $key => $item)
                                <option value="{{$item->id}}">{{$item->nama}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Waktu Produksi <span class="text-danger">*</span></label>
                            <input
                                class="form-control"
                                name="time_of_production"
                                type="text"
                                value="{{$data->time_of_production}}"
                                placeholder="Waktu Produksi"
                                autocomplete="off"
                                @if(!is_null($data->id)) disabled @endif
                            />
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Jumlah Produksi <span class="text-danger">*</span></label>
                            <div class="position-relative d-flex align-items-center justify-content-between">
                                <div style="width:70%;">
                                    <input type="number" name="amount" class="form-control" value="0">
                                </div>
                                <div style="width:28%;">
                                    <input class="form-control text-center" type="text" id="satuan" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Catatan</label>
                            <textarea
                                class="form-control"
                                rows="3"
                                name="note"
                                cols="50"
                                placeholder="Catatan"
                                @if(!is_null($data->id)) disabled @endif
                            ></textarea>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-bold mb-3">Komponen Produksi</h4>
                                <span class="text-light" id="span-komponen"></span>
                                <hr>
                            </div>
                        </div>

                        <div class="col-md-12 mb-4">
                            <label class="font-weight-bold">Gudang Produksi</label>
                            <select class="form-control" name="inv_warehouse_id" id="inv_warehouse_id">
                                <option></option>
                                @foreach($warehouse as $w)
                                    <option value="{{$w->id}}" @if($data->inv_warehouse_id==$w->id) selected @endif>{{$w->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div id="raw-material-wrapper" class="mb-3">
                            
                        </div>

                        <div class="row mb-4" id="row-add-material">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-light btn-block" id="add-material">
                                    <span class="iconify" data-icon="carbon:add"></span>
                                    <span>Tambah Bahan Baku</span>
                                </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>
                                        <input required name="" type="checkbox" value="1" autocomplete="off" /> Data yang saya masukkan adalah benar dan telah diperiksa dengan baik
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12 text-right">
                <input type="hidden" name="code" value="{{$code}}">
                <input type="hidden" name="selling_price" value="0">
                <input type="hidden" name="purchase_price" value="0">
                <input type="hidden" name="md_sc_product_type_id" value="2">
                <input type="hidden" name="is_with_stock" value="1">
                <input type="hidden" name="type" value="create">
                <a href="{{route('merchant.toko.stock.production.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                <button class="btn btn-success"><i class="fa fa-save mr-2"></i> Simpan</button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    $("#md_merchant_id").select2();
    $("#sc_product_category_id").select2();
    $("#sc_merk_id").select2();
    $("#inv_warehouse_id").select2({
        placeholder: "--- Pilih Gudang ---"
    });
    $(".raw-material").select2();
    $("#sc_product_id").select2({
        placeholder: "--- Pilih Produk ---"
    });
    dateTimePicker("input[name=time_of_production]");

    $("#md_unit_id").on("change", function(){
        let val = $(this).find('option:selected').text();
        $("#satuan").val(val);
        $("#span-komponen").text(`Tentukan komponen produksimu untuk setiap 1 ${val}`);
    })

    $('#sc_merk_id').select2({
        placeholder:'--- Pilih Merk ---',
    }).on('select2:open', () => {
        $(".select2-results:not(:has(a))").has('#select2-sc_merk_id-results').append('<a id="add-merk" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.merk.add")}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Merk Baru</b></a>');
    });

    $(document).on('click','#add-merk', function(e){
        $("#sc_merk_id").select2('close');
    })
    let rowMaterialId = [...document.querySelectorAll('.row-raw-material')].map(item => {
        return {
            "row": item.getAttribute('id'),
            "code":null,
            "sc_product_id":null
        };
    });

    let iMaterial = rowMaterialId.length;

    const deleteRow = (rowId) => {
        $("#"+rowId).remove();
        rowMaterialId = [...rowMaterialId].filter(item => {
            return item.row != rowId
        });
    }

    const addMaterial = (row) => {
        $.ajax({
            type: 'POST',
            data: {
                md_merchant_id: $("#md_merchant_id").val(),
                _token:"{{ csrf_token() }}"
            },
            url: "{{route('merchant.toko.stock.production.load-all-material')}}",
            success:function(response){
                const material = response;
                let html = ``;
                let select=``;
                select += `<select name="raw-material" class="form-control raw-material" data-row="rm-${row}">
                                <option></option>
                            `;
                material.forEach(val => {
                    select += `<option
                                    data-code="${val.code}"
                                    value="${val.id}"
                                >
                                    ${val.name}
                                </option>`;
                });
                select += `</select>`;
                
                html += `<div id="rm-${row}" class="col-md-12 mb-4 row-raw-material" style="border:1px dashed #C4C4C4;background:#FBFBFB;border-radius:3px;">
                            <div style="position:absolute;top:10px;right:10px;">
                                <button type="button" class="delete-row" onclick="deleteRow('rm-${row}')">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                            <div class="form-group py-3">
                                <div class="d-flex align-items-center justify-content-between">
                                    <label class="font-weight-bold mb-3">Bahan Baku</label>
                                </div>
                                ${select}
                            </div>
                            <div class="form-group">
                                <label class="font-weight-bold">Jumlah (Kuantitas)</label>
                                <input type="number" class="form-control input-quantity" value="0" data-row="rm-${row}">
                            </div>
                        </div>`;
                $("#raw-material-wrapper").append(html).find(".raw-material").select2({
                    placeholder:'--- Pilih Bahan Baku ---'
                });
            }
        });
    }

    addMaterial(iMaterial++);
    $("#add-material").on('click', function(){
        addMaterial(iMaterial++);
    });
    

    function onchangeBranch()
    {
        let branch = $("#md_merchant_id").val();
        $("#raw-material-wrapper").empty();
        $("#sc_product_id").find('option').not(':first').remove();
        $("#inv_warehouse_id").empty();
        $.ajax({
            type: "POST",
            url: "{{route('merchant.toko.stock.production.get-product')}}",
            data: {
                md_merchant_id: branch,
                _token: "{{csrf_token()}}"
            },
            dataType: 'json',
            success: function(result){
                $("#sc_product_id").select2({
                    placeholder:'--- Pilih Produk ---',
                    data:result
                });
            }
        });

        $.ajax({
            type: "POST",
            url: "{{route('merchant.toko.stock.production.get-warehouse')}}",
            data: {
                md_merchant_id: branch,
                _token: "{{csrf_token()}}"
            },
            dataType: 'json',
            success: function(result){
                $("#inv_warehouse_id").select2({
                    data:result
                });
            }
        });
        addMaterial(iMaterial++);
    }


    function getUnit()
    {
        $("#md_unit_id").select2({
            placeholder:'--- Pilih Satuan Produk ---',
            ajax: {
                type: "GET",
                url: "{{route('get.unit')}}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }

    getUnit();

    $(document).on("change", ".raw-material", function(e){
        let productId = $(this).find('option:selected').val();
        let productCode = $(this).find('option:selected').attr('data-code');
        let rowId = $(this).attr('data-row');
        let checkDuplicate = [...rowMaterialId].filter(item => {
            return item.code == productCode;
        });
        if(checkDuplicate.length > 0){
            $("#"+rowId).remove();
            rowMaterialId = [...rowMaterialId].filter(item => {
                return item.row != rowId;
            });
        } else {
            if(rowMaterialId.findIndex(item => item.row == rowId) != -1){
                rowMaterialId = [...rowMaterialId].map(item => {
                    if(item.row == rowId){
                        return {
                            ...item,
                            code:productCode
                        }
                    } else {
                        return {
                            ...item
                        }
                    }
                    
                });
            } else {
                rowMaterialId.push({
                    "row":rowId,
                    "code":productCode
                });
            }
        }
    }); 

    $(document).ready(function () {
        $("#product").select2();
        $("#gudang").select2();
        
        $('#md_unit_id').on('select2:open', () => {
            $(".select2-results:not(:has(a))").has('#select2-md_unit_id-results').append('<a id="add-unit" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.unit.add")}}?no_reload=1" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Satuan Baru</b></a>');
        });

        $(document).on('click','#add-unit', function(e){
            $("#md_unit_id").select2('close');
        })

        $("#form-konten").submit(function () {
            modalConfirm("Konfirmasi Tindakan", "<div class='alert alert-warning'>Apakah data yang Anda masukkan sudah benar? Data tidak dapat diubah maupun dihapus setelah barang produksi digunakan</div>", function () {
                let data = getFormData("form-konten");
                let selectedMaterial = document.querySelectorAll('.row-raw-material');
                let dataMaterial = [...selectedMaterial].map(item => {
                    return {
                        "child_sc_product_id":item.querySelector('.raw-material').value,
                        "product_code": $(item).find('option:selected').attr('data-code'),
                        "quantity":item.querySelector('.input-quantity').value
                    }
                }).filter(item => {
                    return !!item.product_code;
                });

                data.append('bahan_baku', JSON.stringify(dataMaterial));
                ajaxTransfer("{{route('merchant.toko.stock.production.save')}}", data, function(response){
                    let data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            });

            
        });
        onchangeBranch()
    });



</script>
@endsection

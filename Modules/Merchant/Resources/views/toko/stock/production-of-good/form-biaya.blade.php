<div id="result-form-konten-biaya"></div>

<form onsubmit="return false;" id="form-konten-biaya" class='form-horizontal' backdrop="">

    <div class="form-group row">
        <div class="col-md-2">
            <label for="exampleInputPassword1" style="margin-top: 10px;font-weight: bold">Nama Biaya</label>
        </div>
        <div class="col-md-9">
            <input type="text" class="form-control" name="name" placeholder="Nama Biaya" required>

        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <label for="exampleInputPassword1" style="margin-top: 10px;font-weight: bold">Tujuan Biaya</label>
        </div>
        <div class="col-md-9">
            <input type="text" class="form-control" name="payment_to" placeholder="Tujuan Biaya" required>

        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <label for="exampleInputPassword1" style="margin-top: 10px;font-weight: bold">Metode Pembayaran</label>
        </div>
        <div class="col-md-9">

            <select class="form-control form-control-sm" id="payment" name="trans_coa_detail_id">
                @foreach($paymentOption as $key =>$item)
                    @foreach($item->getDetail as $d)
                        @if(is_null($data->id))
                            <option value="{{$d->id}}">{{$d->name}}{{$d->id}}</option>
                        @else
                            <option value="{{$d->id}}">{{$d->name}}</option>
                        @endif
                    @endforeach
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <label for="exampleInputPassword1" style="margin-top: 10px;font-weight: bold">Waktu Transaksi</label>
        </div>
        <div class="col-md-9">
            <input type="text" class="form-control trans_time" name="created_at" placeholder="Waktu Transaksi" required>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-2">
            <label for="exampleInputPassword1" style="margin-top: 10px;font-weight: bold">Jumlah Biaya</label>
        </div>
        <div class="col-md-9">
            <input type="text" class="form-control" min="0" step="0.01" id="amount_of_bom" name="amount_of_bom" placeholder="Jumlah Biaya"  required>

        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-2">
            <label for="exampleInputPassword1" style="margin-top: 10px;font-weight: bold">Bukti Transaksi</label>
        </div>
        <div class="col-md-9">
            <input type="file" class="form-control"  name="payment_proof">
            <small style="color: red"><i>File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</i></small>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn  btn-primary">Simpan</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#amount_of_bom').mask('#.##0,00', {reverse: true});
        dateTimePicker(".trans_time");
        $('#payment').select2();
        $('#form-konten-biaya').submit(function () {
            var data = getFormData('form-konten-biaya');
            ajaxTransfer("{{route('merchant.toko.stock.production.save-biaya')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Biaya Produksi</h4>
                                        <span class="span-text">Untuk menambah biaya  produksi ,kamu harus mengisi isian dibawah ini</span>
                                    </div>`);
    });


</script>

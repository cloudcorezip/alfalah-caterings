@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<style>
    #progressbar {
        overflow: hidden;
        color: lightgrey;
        width: 40%;
        position: relative;
        justify-content: space-between;
        list-style-type: none;
        display: flex;
    }
    #progressbar .active {
        color: #72BA6C
    }
    #progressbar li {
        list-style-type: none;
        font-size: 15px;
        float: left;
        position: relative;
        font-weight: 400
    }
    #progressbar #step1:before {
        content: "1";
        text-align: center;
    }
    #progressbar #step2:before {
        content: "2";
        text-align: center;
    }

    #progressbar #step3:before {
        content: "3";
        text-align: center;
    }
    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 20px;
        color: #ffffff;
        background: lightgray;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px
    }
    #progressbar li.active:before
    {
        background: #72BA6C
    }
    #progressbar li.active:after {
        background: #72BA6C
    }
    #progressbar li.active:after {
        background: #72BA6C
    }
    #progressbar-connector{
        position: absolute;
        height: 2px;
        background-color: #72BA6C;
        width:70%;
        top: 25px;
        transform: translateY(-50%);
        transform: translateX(-50%);
        left: 50%;
    }
    .btn-remove {
        background:none!important;
        border:none!important;
    }
    .btn-remove .iconify {
        width: 30px;
        height: 30px;
    }
    .delete-row{
        background-color: #fff;
        color:rgba(119, 119, 119, 0.79);
        border:none;
        border-radius: 4px;
    }
    .delete-row:hover {
        background-color: #ff4400;
        color: #fff;
    }
</style>
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a>Persediaan</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.toko.stock.production.index')}}">Produksi Produk</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a>{{$title}}</a>
                    </li>
                </ol>
            </nav>
            <span>Lengkapi data produksi sebelum melakukan proses produksi</span>
        </div>
    </div>
</div>

<!-- DataTales Example -->

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="d-flex justify-content-center mb-4">
                <ul id="progressbar">
                    <div id="progressbar-connector"></div>
                    <li class="active" id="step1">
                        <strong>Informasi Produk</strong>
                    </li>
                    <li class="active" id="step2"> 
                        <strong>Biaya Produksi</strong> 
                    </li>
                    <li class="active" id="step3">
                        <strong>Ringkasan Produk</strong>
                    </li>
                </ul>

            </div>

        </div>
    </div>
</div>

<div class="container-fluid">
    <form onsubmit="return false;" id="form-konten" class="form-horizontal" backdrop="">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <span class="text-light">{{$data->code}}</span>
                        <h5 class="font-weight-bold">{{$data->getProduct->name}}</h5>
                    </div>
                    <div class="col-md-3">
                         <span class="text-light text-center d-block">Waktu Produksi</span>
                         <h5 class="font-weight-bold text-center">{{\Carbon\Carbon::parse($data->time_of_production)->isoFormat(' D MMMM Y  HH:mm:ss')}}</h5>
                    </div>
                    <div class="col-md-3">
                        <span class="text-light text-center d-block">Jumlah Produksi</span>
                        <h5 class="font-weight-bold text-center">{{$data->amount}} {{(is_null($data->getProduct->getUnit))?'-':$data->getProduct->getUnit->name}}</h5>
                    </div>
                    <div class="col-md-3">
                        <span class="text-light text-right d-block">Harga Pokok Produksi (HPP)</span>
                        <h5 class="font-weight-bold text-right">{{rupiah($data->total/$data->amount)}}/ {{(is_null($data->getProduct->getUnit))?'-':$data->getProduct->getUnit->name}}</h5>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive table-borderless">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td colspan="3">
                                            <h6 class="font-weight-bold">Produk Produksi</h6>
                                        </td>
                                    </tr>

                                    @foreach($material as $key => $item)
                                    <tr>
                                        <td style="padding-left:40px;">{{$item->name}}</td>
                                        <td class="text-center">{{$item->quantity * $data->amount}} {{$item->unit_name}}</td>
                                        <td class="text-right">{{rupiah($item->purchase_price * $item->quantity * $data->amount)}}</td>
                                    </tr>
                                    @endforeach
                                    
                                    @if(count($bom) > 0)
                                    <tr>
                                        <td colspan="3">
                                            <h6 class="font-weight-bold">Biaya Tambahan</h6>
                                        </td>
                                    </tr>
                                    @endif

                                    @foreach($bom as $key => $item)
                                    <tr>
                                        <td colspan="2" style="padding-left:40px;">{{$item->name}}</td>
                                        <td class="text-right">{{rupiah($item->amount_of_bom + $item->admin_fee)}}</td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                                <tfoot style="background:#F7F7F7;">
                                    <tr>
                                        <td colspan="2">
                                            <h6 class="font-weight-bold">Total</h6>
                                        </td>
                                        <td>
                                            <h6 class="font-weight-bold text-right">{{rupiah($data->total)}}</h6>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6 px-3" style="border-left: 1px solid rgba(0, 0, 0, 0.1);">
                        <form onsubmit="return false;" id="form-konten" class="form-horizontal" backdrop="">
                            <div class="form-group">
                                <label class="font-weight-bold">Harga Penjualan</label>
                                <input type="text" class="form-control" id="selling_price" name="selling_price" value="{{number_format((float)$data->selling_price, 2, '.', '')}}" placeholder="Harga Jual">
                            </div>
                        </form>
                    </div>
                </div>
                  
                <div class="row">
                    <div class="col-md-12 text-right">
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <a href="{{route('merchant.toko.stock.production.add-cost', ['id'=>$data->id])}}" class="btn btn-light text-light mr-2">Kembali</a>
                        <button class="btn btn-success"><i class="fa fa-save mr-2"></i> Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    $('#selling_price').mask('#.##0,00', {reverse: true});
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.stock.production.save-harga-jual')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    });



</script>
@endsection

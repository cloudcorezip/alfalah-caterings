@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock.production.index')}}">Produksi Produk</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Lengkapi data produksi sebelum melakukan proses produksi</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 py-4 px-5">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Produk</label>
                                <select class="form-control form-control-sm" id="product" required="" onchange="getDataBarang()" name="sc_product_id" placeholder="Barang Produksi"
                                        @if(!is_null($data->id))
                                        disabled @endif>
                                    @foreach($product as $item)
                                        <option value="{{$item->id}}" @if($data->sc_product_id==$item->id) selected @endif>{{$item->code}}-{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Komponen Produk</label>
                                <div id="result-komponen-produksi"></div>
                            </div>

                            <div class="form-group">
                                <label>Gudang Produksi</label>
                                <select class="form-control form-control-sm" id="gudang" required="" name="inv_warehouse_id" placeholder="Gudang Produksi"
                                        @if(!is_null($data->id))
                                        disabled @endif
                                >
                                    @foreach($warehouse as $w)
                                        <option value="{{$w->id}}" @if($data->inv_warehouse_id==$w->id) selected @endif>{{$w->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Detail Biaya Produksi</label>
                                @if(is_null($data->id))
                                    <div class="form-control-static">
                                        <small class="text-warning">Detail biaya produksi dapat ditambahkan setelah data disimpan</small>
                                    </div>
                                @else
                                    <div id="output-biaya-produksi">
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Jumlah Produksi</label>
                                <div class="row">
                                    <div class="col-8">
                                        <input
                                            class="form-control form-control-sm"
                                            min="1"
                                            required
                                            name="amount"
                                            type="number"
                                            value="{{$data->amount}}"
                                            placeholder="Jumlah Produksi"
                                            autocomplete="off"
                                            @if(!is_null($data->id)) disabled @endif
                                        />
                                    </div>
                                    <div class="col-4">
                                        <input type="text" class="form-control form-control-sm"  id="satuan" disabled/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Waktu Produksi</label>
                                <input
                                    class="form-control"
                                    name="time_of_production"
                                    type="text"
                                    value="{{$data->time_of_production}}"
                                    placeholder="Waktu Produksi"
                                    autocomplete="off"
                                    required
                                    @if(!is_null($data->id)) disabled @endif
                                />
                            </div>

                            <div class="form-group">
                                <label>Catatan</label>
                                <textarea
                                    class="form-control"
                                    rows="3"
                                    name="note"
                                    cols="50"
                                    placeholder="Catatan"
                                    @if(!is_null($data->id)) disabled @endif
                                ></textarea>
                            </div>

                            @if(is_null($data->id))
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label><input required name="" type="checkbox" value="1" autocomplete="off" /> Data yang saya masukkan adalah benar dan telah diperiksa dengan baik</label>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12 text-right">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>

                        </div>

                    </div>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    @if(!is_null($data->id))
                        <input type='hidden' name='id' value='{{ $data->id }}' id="id-prod">
                        <input type='hidden' name='inv_method_id' value='{{ $merchant->md_inventory_method_id }}' id="id-prod">

                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function getDataBarang() {
            var data = new FormData();
            var product=$("#product").val()
            ajaxTransfer("{{route('merchant.toko.stock.production.component')}}?id="+product, data, function (response) {
                response = JSON.parse(response);
                console.log(response);
                var komponen=response['html'];
                var satuan = response["satuan"];
                $("#result-komponen-produksi").html(komponen);
                $("#satuan").val(satuan);
            });
        }
        {{--function reloadBiayaProduksi() {--}}
        {{--    var data=new FormData();--}}
        {{--    var product=$("#product").val()--}}
        {{--    ajaxTransfer("{{route('merchant.toko.stock.production.component-biaya')}}?id="+product, data,--}}
        {{--        function (response){--}}
        {{--            response = JSON.parse(response);--}}
        {{--            console.log(response);--}}
        {{--            $("#output-biaya-produksi").html(response);--}}
        {{--        });--}}
        {{--}--}}

        function hapusBiayaProduksi(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.toko.stock.production.delete-biaya')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }
        $(document).ready(function () {

            $("#product").select2();
            $("#gudang").select2();
            getDataBarang();
            // reloadBiayaProduksi();
            dateTimePicker("input[name=time_of_production]");
            $("#form-konten").submit(function () {
                modalConfirm("Konfirmasi Tindakan", "<div class='alert alert-warning'>Apakah data yang Anda masukkan sudah benar? Data tidak dapat diubah maupun dihapus setelah barang produksi digunakan</div>", function () {
                    var data = getFormData("form-konten");
                    ajaxTransfer("{{route('merchant.toko.stock.production.save')}}", data, function(response){
                        var data = JSON.parse(response);
                        toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                    });
                });
            });
        });



    </script>
@endsection

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<style>
    #progressbar {
        overflow: hidden;
        color: lightgrey;
        width: 40%;
        position: relative;
        justify-content: space-between;
        list-style-type: none;
        display: flex;
    }
    #progressbar .active {
        color: #72BA6C
    }
    #progressbar li {
        list-style-type: none;
        font-size: 15px;
        float: left;
        position: relative;
        font-weight: 400
    }
    #progressbar #step1:before {
        content: "1";
        text-align: center;
    }
    #progressbar #step2:before {
        content: "2";
        text-align: center;
    }

    #progressbar #step3:before {
        content: "3";
        text-align: center;
    }
    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 20px;
        color: #ffffff;
        background: lightgray;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px
    }
    #progressbar li.active:before
    {
        background: #72BA6C
    }
    #progressbar li.active:after {
        background: #72BA6C
    }
    #progressbar li.active:after {
        background: #72BA6C
    }
    #progressbar-connector{
        position: absolute;
        height: 2px;
        background-color: #72BA6C;
        width:70%;
        top: 25px;
        transform: translateY(-50%);
        transform: translateX(-50%);
        left: 50%;
    }
    .btn-remove {
        background:none!important;
        border:none!important;
    }
    .btn-remove .iconify {
        width: 30px;
        height: 30px;
    }
    .delete-row{
        background-color: #fff;
        color:rgba(119, 119, 119, 0.79);
        border:none;
        border-radius: 4px;
    }
    .delete-row:hover {
        background-color: #ff4400;
        color: #fff;
    }
</style>
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid">
        <div class="p-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a>Persediaan</a>
                    </li>
                    <li class="breadcrumb-item d-flex align-items-center">
                        <a href="{{route('merchant.toko.stock.production.index')}}">Produksi Produk</a>
                    </li>
                    <li class="breadcrumb-item active d-flex align-items-center">
                        <a>{{$title}}</a>
                    </li>
                </ol>
            </nav>
            <span>Lengkapi data produksi sebelum melakukan proses produksi</span>
        </div>
    </div>
</div>

<!-- DataTales Example -->

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="d-flex justify-content-center mb-4">
                <ul id="progressbar">
                    <div id="progressbar-connector"></div>
                    <li class="active" id="step1">
                        <strong>Informasi Produk</strong>
                    </li>
                    <li class="active" id="step2"> 
                        <strong>Biaya Produksi</strong> 
                    </li>
                    <li id="step3">
                        <strong>Ringkasan Produk</strong>
                    </li>
                </ul>

            </div>

        </div>
    </div>
</div>

<div class="container-fluid">
    <form onsubmit="return false;" id="form-konten" class="form-horizontal" backdrop="">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <h4 class="font-weight-bold">Biaya Tambahan</h4>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="cost-result-wrapper">
                        @foreach($detail as $key => $item)
                        <div 
                            id="row-{{$key}}" 
                            class="p-4 mb-3 row-cost" 
                            style="border:1px dashed #D6D6D6;background-color:#FDFDFD;border-radius:6px;position:relative;"
                        >
                            <div style="position:absolute;top:10px;right:10px;">
                                <button type="button" class="delete-row" onclick="hapusBiayaProduksi('{{$item->id}}')">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Biaya Lainnya</label>
                                        <select disabled data-row="row-{{$key}}" class="form-control cost_coa_id" name="cost_coa_id">
                                            @foreach($costList as $k => $i)
                                            <option 
                                                value="{{$i->acc_coa_detail_id}}"
                                                @if($i->acc_coa_detail_id == $item->cost_coa_id)
                                                selected
                                                @endif
                                            >
                                                {{$i->name}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Metode Pembayaran</label>
                                        <select disabled data-row="row-{{$key}}" class="form-control trans_coa_detail_id" name="trans_coa_detail_id">
                                            @foreach($paymentList as $k => $i)
                                            <optgroup label="{{$i['name']}}">
                                                @if(count($i['data']) > 0)
                                                    @foreach($i['data'] as $r => $v)
                                                    <option 
                                                        value="{{$v->trans_id}}_{{$v->acc_coa_id}}"
                                                        @if($item->md_transaction_type_id == $v->trans_id && $item->trans_coa_detail_id == $v->acc_coa_id)
                                                        selected
                                                        @endif
                                                    >
                                                        {{$v->name}}
                                                    </option>
                                                    @endforeach
                                                @endif
                                            </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="font-weight-bold">Biaya Administrasi</label>
                                        <input disabled data-row="row-{{$key}}" type="text" class="form-control admin_fee amount_currency" value="{{is_null($item->admin_fee)?0:number_format((float)$item->admin_fee, 2, '.', '')}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <diV class="form-group">
                                        <label class="font-weight-bold">Total</label>
                                        <input disabled data-row="row-{{$key}}" type="text" class="form-control amount_of_bom amount_currency" value="{{is_null($item->amount_of_bom)?0:number_format((float)$item->amount_of_bom, 2, '.', '')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 mb-3">
                        <button type="button" class="btn btn-light btn-block" id="btn-add-cost">
                            <span class="iconify" data-icon="carbon:add"></span>
                            <span>Tambah Biaya</span>
                        </button>
                    </div>
                </div>
                  
                <div class="row">
                    <div class="col-md-12 text-right">
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <a href="{{route('merchant.toko.stock.production.add', ['type'=>'select', 'id'=>$data->id])}}" class="btn btn-light text-light mr-2">Kembali</a>
                        <button class="btn btn-success"><i class="fa fa-save mr-2"></i> Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    $(".cost_coa_id").select2({
        placeholder: "--- Pilih Biaya ---"
    });
    $(".trans_coa_detail_id").select2({
        placeholder: "--- Pilih Metode Pembayaran ---"
    });
    dateTimePicker("input[name=time_of_production]");

    let rowCostId = [...document.querySelectorAll('.row-cost')].map(item => {
        return {
            row: $(item).attr('id'),
            cost_coa_id: $(item).find('.cost_coa_id').val(),
            cost_coa_name:$(item).find('.cost_coa_id option:selected').text().trim(),
            trans_coa_detail_id:$(item).find('.trans_coa_detail_id').val(),
            amount_of_bom:$(item).find('.amount_of_bom').val(),
            admin_fee: $(item).find('.admin_fee').val()
        }
    });

    let iCost = rowCostId.length;

    const deleteRow = (rowId) => {
        $("#"+rowId).remove();
        rowCostId = [...rowCostId].filter(item => {
            return item.row != rowId
        });
    }


    function hapusBiayaProduksi(id) {
        var data = new FormData();
        data.append('id', id);
        modalConfirm("Konfirmasi", "Apa anda yakin menghapus biaya ?", function () {
            ajaxTransfer("{{route('merchant.toko.stock.production.delete-biaya')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    }

    const addCost = (row) => {
        let html = `<div 
                        id="row-${row}" 
                        class="p-4 mb-3 row-cost new" 
                        style="border:1px dashed #D6D6D6;background-color:#FDFDFD;border-radius:6px;position:relative;"
                    >
                        <div style="position:absolute;top:10px;right:10px;">
                            <button type="button" class="delete-row" data-row="row-${row}" onclick="deleteRow('row-${row}')">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="font-weight-bold">Biaya Lainnya</label>
                                    <select data-row="row-${row}" class="form-control cost_coa_id" name="cost_coa_id">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="font-weight-bold">Metode Pembayaran</label>
                                    <select data-row="row-${row}" class="form-control trans_coa_detail_id" name="trans_coa_detail_id">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="font-weight-bold">Biaya Administrasi</label>
                                    <input data-row="row-${row}" type="text" class="form-control admin_fee amount_currency" value="0">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <diV class="form-group">
                                    <label class="font-weight-bold">Total</label>
                                    <input data-row="row-${row}" type="text" class="form-control amount_of_bom amount_currency" value="0">
                                </div>
                            </div>
                        </div>
                    </div>`;

        $("#cost-result-wrapper").append(html).find('.cost_coa_id').select2({
            placeholder:"---- Pilih Biaya ----",
            ajax: {
                type: "GET",
                url: "{{route('api.cost.all',['merchantId'=>$data->md_merchant_id])}}",
                dataType: 'json',
                delay: 250,
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        $("#cost-result-wrapper").find('.trans_coa_detail_id').select2({
            placeholder: "--- Pilih Metode Pembayaran ---",
            ajax: {
                type: "GET",
                url: "{{route('merchant.toko.transaction.purchase-order.payment-list')}}?type=2&md_merchant_id={{$data->md_merchant_id}}",
                dataType: 'json',
                delay: 250,  
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true  
            }
        });

        $("#cost-result-wrapper").find('.amount_currency').mask("#.##0,00", {reverse: true});
        
    }

    addCost(iCost++);
    
    $("#btn-add-cost").on('click', function(){
        addCost(iCost++);
    });


    $(document).on("change", ".cost_coa_id", function(e){
        let costCoaId = $(this).find('option:selected').val();
        let costCoaName = $(this).find('option:selected').text();
        let rowId = $(this).attr('data-row');
        let checkDuplicate = [...rowCostId].filter(item => {
            return item.cost_coa_id == costCoaId;
        });

        if(checkDuplicate.length > 0){
            $("#"+rowId).remove();
            rowCostId = [...rowCostId].filter(item => {
                return item.row != rowId;
            });
        } else {
            if(rowCostId.findIndex(item => item.row == rowId) != -1){
                rowCostId = [...rowCostId].map(item => {
                    if(item.row == rowId){
                        return {
                            ...item,
                            cost_coa_id:costCoaId,
                            cost_coa_name:costCoaName
                        }
                    } else {
                        return {
                            ...item
                        }
                    }
                });
            } else {
                rowCostId.push({
                    row:rowId,
                    cost_coa_id:costCoaId,
                    cost_coa_name: costCoaName
                });
            }
        }
    });
    

    $(document).ready(function () {
        $("#form-konten").submit(function () {
            let data = getFormData("form-konten");
            
            let selectedCost = document.querySelectorAll('.row-cost.new');
            let dataCost = [...selectedCost].map(item => {
                return {
                    cost_coa_id: $(item).find('.cost_coa_id').val(),
                    cost_coa_name:$(item).find('.cost_coa_id option:selected').text().trim(),
                    trans_coa_detail_id:$(item).find('.trans_coa_detail_id').val(),
                    amount_of_bom:$(item).find('.amount_of_bom').val(),
                    admin_fee: $(item).find('.admin_fee').val()
                }
            }).filter(item => {
                return !!item.cost_coa_id;
            });

            data.append('data_cost', JSON.stringify(dataCost));

            ajaxTransfer("{{route('merchant.toko.stock.production.save-biaya')}}", data, function(response){
                let data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
            
        });
    });



</script>
@endsection

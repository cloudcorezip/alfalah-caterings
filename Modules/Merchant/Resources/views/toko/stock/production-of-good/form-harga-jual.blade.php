<div id="result-form-konten-hj"></div>

<form onsubmit="return false;" id="form-konten-hj" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1">Harga Jual</label>
        <input type="text" class="form-control" min="0" id="selling_price" step="0.01" name="selling_price" value="{{number_format((float)$data->selling_price, 2, '.', '')}}" placeholder="Harga Jual"  required>
    </div>

    <div class="modal-footer">
        <button class="btn btn-success">Simpan</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
<script>
    $(document).ready(function () {
        $('#selling_price').mask('#.##0,00', {reverse: true});
        $('#form-konten-hj').submit(function () {
            var data = getFormData('form-konten-hj');
            ajaxTransfer("{{route('merchant.toko.stock.production.save-harga-jual')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Harga Jual</h4>
                                        <span class="span-text">Untuk menambah harga jual produk produksi ,kamu harus mengisi isian dibawah ini</span>
                                    </div>`);
    });



</script>

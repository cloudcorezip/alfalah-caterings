@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock.hpp.data')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa melihat detail dan status produk yang tersedia di gudangmu</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @include('merchant::alert')

        <table class="table table-borderless">
            <tr>
                <td class="text-left" width="80%" style="padding: 0.1rem"><h6><b>{{strtoupper('Laporan Stok Persediaan')}}</b></h6></td>
                <td rowspan="2" class="text-right" style="padding: 0.1rem">
                    <a class="btn btn-success" onclick="exportData()"><i class="fa fa-download mr-2"></i> Download Laporan</a>

                </td>
            </tr>
            <tr>
                <td style="padding: 0.1rem"><h6><b>Periode
                            {{\Carbon\Carbon::parse($startDate)->isoFormat('D MMMM Y')}}
                        -
                            {{\Carbon\Carbon::parse($endDate)->isoFormat('D MMMM Y')}}

                        </b></h6></td>
            </tr>
        </table>


        <div class="card shadow mb-4">
            <div class="card-body">

                <div id="output-sale-order">
                    @include('merchant::toko.stock.hpp.list2')

                </div>
            </div>
        </div>
    </div>

    <div class="side-popup" id="side-popup">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <span class="text-white form-filter3-text-header" id="title-detail"></span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter', 'side-popup')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div id="side-popup-body">

        </div>
    </div>


    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>
    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold mt-3 text-dark">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-hpp" class="px-30" method="get">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="text" class="form-control form-control trans_time" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="text" class="form-control form-control trans_time" id="end_date" value="{{$endDate}}" name="end_date" style="background: white">

                    </div>
                </div>
                <div class="col-md-12 mb-3">
                        <label>Pilih Cabang yang ingin dibandingkan</label>
                        <select name="md_merchant_id[]" id="branch" class="form-control" multiple="multiple">
                            <option value="-1"  @if(in_array('-1',$merchantId)) selected @endif>Semua Cabang</option>
                            @foreach(get_cabang() as $key  =>$item)
                                <option value="{{$item->id}}" @if(in_array($item->id,$merchantId)) selected @endif>{{$item->nama_cabang}}</option>
                            @endforeach
                        </select>
                    </div>

                <div class="col-md-12 mb-3">
                    <label>Tampilkan Semua ?</label>
                    <div class="col-md-2">
                        <input value="{{($is_show_all==1)?1:0}}" type="hidden" name="is_show_all" id="show_all">
                        <label>
                            <input type="checkbox" {{($is_show_all==1)?'checked':''}} id="is_show_all"></label>
                    </div>

                </div>
                <div class="col-md-12 mb-3">
                    <label>Barang Konsinyasi ?</label>
                    <div class="col-md-2">
                        <input value="{{($is_consignment==1)?1:0}}" type="hidden" name="is_consignment" id="consignment">
                    <label>
                        <input type="checkbox" {{($is_consignment==1)?'checked':''}} id="is_consignment"></label>
                    </div>

                </div>
                <div class="col-md-12 mb-3">
                    <label>Ketik Kode atau Nama Produk</label>
                    <input type="text" name="search" class="form-control" id="searchKey" style="background: white">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-3 mt-3">
                    <input type="hidden" name="search_product" id="search_product" value="{{$searchProduct}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a href="{{route('merchant.toko.stock.hpp.data')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')

    <script>
        function exportData() {

            modalConfirm('Export Data', 'Lanjutkan proses export data?', function () {
                var data = getFormData('form-filter-sale-order');
                let selectedMerchantId = ($("#branch").select2('data'))? $("#branch").select2('data'):[];
                let merchantIds = [];
                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });

                data.append('md_merchant_id',merchantIds);
                data.append('start_date','{{$startDate}}');
                data.append('end_date','{{$endDate}}');
                data.append('is_consignment',$("#consignment").val())
                data.append('search_product',$("#search_product").val())

                ajaxTransfer("{{route('merchant.toko.stock.hpp.export-data')}}", data, '#modal-output');
            });

        }

        $("#branch").on("select2:select", function(e){
            if(e.params.data.id == "-1"){
                $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
            } else {
                $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
            }
        });

        $(document).ready(function () {
            $("#branch").select2({});
            $('#form-filter-hpp').submit(function () {
                let searchKey = $("#searchKey").val()
                $("#search_product").val(searchKey)
            });
        });
    </script>

    <script>

        function  loadDetail(value)
        {
            let params =value.split('_')
            let startDate = $("#start_date").val();
            let endDate = $("#end_date").val();
            $.ajax({
                type: 'POST',
                url: '{{route('merchant.toko.stock.hpp.ajax-detail')}}',
                data: {
                    'id': params[0],
                    'merchant_id':params[1],
                    'start_date':moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'),
                    'end_date':moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD')
                },
                headers:{
                    'X-CSRF-TOKEN': "{{csrf_token()}}",
                },
                success: function(data){
                    $('#side-popup-body').html('');
                    let html="<div class='row mb-3'>" +
                        "" +
                        "<table class='table table-custom'>";
                    html +="<thead>" +
                        "<tr>" +
                        "<th>Item</th>" +
                        "<th>Stok</th>" +
                        "<th>Satuan</th>" +
                        "<th>Gudang</th>" +
                        "</tr>" +
                        "</thead><tbody>"
                    $("#title-detail").text("Stok Bahan "+ data.product.name +" ("+data.product.merchant_name+")")
                    if(data.product){
                        $.each(data.materialStock,function (i,v){
                            html+= "<tr>" +
                                "<td colspan='4' class='font-weight-bold'>"+v[0].name+"</td>" +
                                "</tr>"
                            $.each(v,function (j,k){
                                html+= "<tr>" +
                                    "<td colspan='2' class='text-center'>" +
                                    ""+k.residual_stock+"</td>" +
                                    "<td>"+k.unit_name+"</td>" +
                                    "<td>"+k.warehouse_name+"</td>" +
                                    "</tr>"
                            })
                        })

                    }else{
                        html+="<tr>" +
                            "<td colspan='4' style='text-align: center'>Data tidak tersedia</td></tr>"
                    }
                    html+="</tbody></table>" +
                        "</div>";

                    $('#side-popup-body').append(html);
                    $('#side-popup').addClass('show');

                }
            });

        }

        $("#is_consignment").change(function() {
            if(this.checked) {
                $("#consignment").val(1)

            }else{
                $("#consignment").val(0)

            }
        });

        $("#is_show_all").change(function() {
            if(this.checked) {
                $("#show_all").val(1)

            }else{
                $("#show_all").val(0)

            }
        });


    </script>
@endsection

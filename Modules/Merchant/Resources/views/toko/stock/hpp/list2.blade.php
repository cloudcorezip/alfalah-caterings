<div class="table-responsive">
    <table class="table table-custom" id="table-data-hpp" width="100%" cellspacing="0">
        <thead>
        <tr style="background: white">
            <th>Nama Produk</th>
            <th>Tipe Gudang</th>
            <th>Stok Tersedia</th>
            <th>HPP</th>
            <th>Nilai Persediaan</th>
            <th>Satuan</th>
            <th>Stok Masuk</th>
            <th>Stok Keluar</th>
            <th>Opsi</th>
        </tr>
        </thead>
        <tbody>
        @if(count($data)>0)
        @foreach($data as $key =>$item)
            <tr style="background-color: #ececec">
                <td colspan="2" style="font-weight: bold">{{$item->nama_produk}}</td>
                <td style="font-weight: bold">{{$item->stok_tersedia}}</td>
                <td style="font-weight: bold">{{$item->hpp}}</td>
                <td style="font-weight: bold">{{$item->nilai_persediaan}}</td>
                <td style="font-weight: bold">{{$item->satuan}}</td>
                <td style="font-weight: bold">{{$item->stok_masuk}}</td>
                <td style="font-weight: bold">{{$item->stok_keluar}}</td>
                <td></td>
            </tr>
            @foreach($item->merchant as $c =>$b)
                <tr style="background-color: #f8f6f6">
                    <td colspan="2" style="font-weight: bold;padding-left: 30px">  {{$b->merchant_name}}</td>
                    <td style="font-weight: bold">{{$b->stok_tersedia}}</td>
                    <td style="font-weight: bold"></td>
                    <td style="font-weight: bold">{{$b->nilai_persediaan}}</td>
                    <td style="font-weight: bold"></td>
                    <td style="font-weight: bold">{{$b->stok_masuk}}</td>
                    <td style="font-weight: bold">{{$b->stok_keluar}}</td>
                    <td>{!! $b->opsi !!}</td>
                </tr>
                @foreach($b->warehouse as $w =>$g)
                    <tr>
                        <td style="padding-left: 50px">{{$g->warehouse_name}}</td>
                        <td>{{$g->warehouse_type}}</td>
                        <td>{{$g->stok_tersedia}}</td>
                        <td></td>
                        <td>{{$g->nilai_persediaan}}</td>
                        <td></td>
                        <td>{{$g->stok_masuk}}</td>
                        <td>{{$g->stok_keluar}}</td>
                        <td>
                        </td>
                    </tr>
                @endforeach
            @endforeach

        @endforeach
            @else
        <tr style="background:#ececec ">
            <td colspan="9" class="text-center">Data tidak tersedia</td>
        </tr>
        @endif

        </tbody>
    </table>
    <br>
</div>
{{$data->withQueryString()->links('vendor.pagination.with-showing-entry')}}

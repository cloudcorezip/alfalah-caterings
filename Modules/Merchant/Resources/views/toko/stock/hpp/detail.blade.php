@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Persediaan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.stock.hpp.data')}}">Lihat Stok</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Berikut merupakan informasi lengkap terkait detail harga,stok,arus stok masuk keluar setiap produk</span>

            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning text-center">
                    <b>Perhatian !</b> Pencatatan stok masuk dan keluar menggunakan satuan default dari produk.
                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <dl class="trans-info">
                            <dd>
                                <span>Nama Produk :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->name}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Harga Rerata Produk :</span>
                            </dd>
                            <dt>
                                <h5>{{($inventory->value_inventory<1)?rupiah(0):rupiah($inventory->value_inventory/$inventory->stock)}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Kategori Produk :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->getCategory->name}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Satuan :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->getUnit->name}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Tipe Produk :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->getType->name}}</h5>
                            </dt>
                        </dl>
                        <dl class="trans-info">
                            <dd>
                                <span>Cabang :</span>
                            </dd>
                            <dt>
                                <h5>{{$data->merchant_name}}</h5>
                            </dt>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-custom" id="table-data" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    @foreach($tableColumns as $key =>$item)
                                        <th class="text-nowrap">{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            ajaxDataTable('#table-data', 1, "{{route('merchant.toko.product.datatable-stock')}}?id={{$data->id}}&warehouse_id={{$warehouseId}}", [
                    @foreach($tableColumns as $key =>$item)
                    @if($tableColumns[$key]=='action')
                {
                    data: '{{$tableColumns[$key]}}',
                    name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                @endif
                @endforeach
            ]);

        })
        $('#form-konten-stock').submit(function () {
            var data = getFormData('form-konten-stock');
            ajaxTransfer("{{route('merchant.toko.product.save-stock')}}", data, '#result-form-konten-stock');
        })
    </script>
@endsection

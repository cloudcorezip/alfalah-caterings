@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    @include('merchant::toko.commission.style')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Marketing</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.commission.index')}}">Komisi</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Untuk menambah komisi, kamu harus mengisi data di bawah ini</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="col-md-12">
            <div class="alert alert-warning text-center">
                <b>Perhatian !</b> Komisi berdasarkan qty produk  menggunakan perhitungan satuan default dari setiap produk.
            </div>
        </div>
        <div class="card shadow mb-4">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="font-weight-bold mb-3">Info Komisi</h4>
                            <hr>
                        </div>
                    </div>

                    <div class="form-group row  d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Status Komisi <span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                            <div class="btn-group-toggle input-radio-group mb-3" data-toggle="buttons">
                                <label
                                    class="btn btn-input-radio d-block active"
                                >
                                    <input
                                        type="radio"
                                        name="status"
                                        class="status"
                                        value="1"
                                        checked
                                    >
                                    <span class="text-center">Aktif</span>
                                </label>
                                <label
                                    class="btn btn-input-radio d-block"
                                >
                                    <input
                                        type="radio"
                                        name="status"
                                        class="status"
                                        value="0"
                                    >
                                    <span class="text-center">Tidak Aktif</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Nama Komisi <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" name="name" value="{{$data->name}}">
                        </div>
                    </div>
                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Tipe Komisi <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <div class="form-check form-check-inline">
                                <input
                                    class="form-check-input type"
                                    type="radio"
                                    value="product"
                                    name="type"
                                    checked
                                >
                                <label class="form-check-label">Produk</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="form-check-input type"
                                    type="radio"
                                    value="transaction"
                                    name="type"
                                >
                                <label class="form-check-label">Transaksi</label>
                            </div>
                        </div>
                    </div>

                    <div id="product_list_wrapper">
                        <div class="form-group row d-flex align-items-start">
                            <label class="col-sm-2 col-form-label font-weight-bold">Pilih Produk <span class="text-danger">*</span></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="product_list" id="product_list">
                                    <option value="-1">Pilih Produk</option>
                                    @foreach($product as $key => $item)
                                        <option data-code="{{$item->code}}" value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>

                                <div id="product-result-wrapper" class="mt-3">
                                    <table class="table table-borderless table-custom" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th style="width:60%;">Nama Produk</th>
                                            <th style="width:40%;" class="text-center">Hapus</th>
                                        </tr>
                                        </thead>
                                        <tbody id="product-result-body">

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group d-flex align-items-start">
                                <label class="col-md-2 col-form-label font-weight-bold" style="padding: 0">Nilai Komisi <span class="text-danger">*</span></label>

                                <div class="btn-group-toggle btn-group col-md-3" style="border: 1px solid #F3F3F3;padding: 0" data-toggle="buttons">
                                    <label
                                        class="btn btn-input-radio d-block active"
                                    >
                                        <input
                                            class="form-check-input value_type"
                                            type="radio"
                                            value="nominal"
                                            name="value_type"
                                            checked
                                        >
                                        <span class="text-center">Nominal</span>
                                    </label>
                                    <label
                                        class="btn btn-input-radio d-block"
                                    >
                                        <input
                                            class="form-check-input value_type"
                                            type="radio"
                                            value="percentage"
                                            name="value_type"
                                        >
                                        <span class="text-center">Persentase(%)</span>
                                    </label>

                                </div>
                                <div class="col-md-1"></div>
                                <div class="btn-group-toggle btn-group col-md-6" style="border: 1px solid #F3F3F3;padding: 0" data-toggle="buttons">
                                    <label
                                        class="btn btn-input-radio d-block active"
                                    >
                                        <input
                                            type="radio"
                                            name="type_of_value_commissions"
                                            class="type_of_value_commissions"
                                            value="flat"
                                            checked
                                        >
                                        <span class="text-center">Flat</span>
                                    </label>
                                    <label
                                        class="btn btn-input-radio d-block"
                                    >
                                        <input
                                            type="radio"
                                            name="type_of_value_commissions"
                                            class="type_of_value_commissions"
                                            value="multiple"
                                        >
                                        <span class="text-center">Kelipatan</span>
                                    </label>
                                    <label
                                        class="btn btn-input-radio d-block"
                                    >
                                        <input
                                            type="radio"
                                            name="type_of_value_commissions"
                                            class="type_of_value_commissions"
                                            value="tiered"
                                        >
                                        <span class="text-center">Berjenjang</span>
                                    </label>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <small>Atur nilai komisi yang didapatkan karyawan dari setiap penjualan</small>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group mt-3" id="value-wrapper">
                                <input type="text" class="form-control amount_currency" name="value[]" value="0" placeholder>
                            </div>
                        </div>
                    </div>



                    <div class="form-group row d-flex align-items-center">
                        <label class="col-sm-2 col-form-label font-weight-bold">Minimal Transaksi <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control amount_currency" name="min_transaction" placeholder="Nilai Rupiah" value="0">
                        </div>
                    </div>

                    @if(count(get_cabang()) > 1)
                        <div class="form-group row d-flex align-items-start">
                            <div class="col-sm-2">
                                <label class="col-form-label font-weight-bold d-block">Berlakukan untuk Semua Cabang ?</label>
                            </div>
                            <div class="col-sm-10">
                                <div class="custom-control custom-switch mb-4">
                                    <input
                                        type="checkbox"
                                        class="custom-control-input check-bank"
                                        id="is_all_branch"
                                        name="is_all_branch"
                                        value="0"
                                        style="transform:scale(2)"
                                    >
                                    <label class="custom-control-label" for="is_all_branch"></label>
                                </div>

                                <div id="branch-wrapper">
                                    <select name="branch" class="form-control js-example-basic-single" id="branch" multiple="multiple">
                                        @foreach(get_cabang() as $key => $item)
                                            @if($item->id != merchant_id())
                                                <option value="{{$item->id}}"  data-name="{{$item->nama_cabang}}">{{$item->nama_cabang}}</option>
                                            @endif

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="form-group row d-flex align-items-start">
                        <label class="col-sm-2 col-form-label font-weight-bold">Penerima Komisi <span class="text-danger">*</span></label>
                        <div class="col-sm-7">
                            <div class="form-check form-check-inline">
                                <input
                                    class="form-check-input is_for_employee"
                                    type="radio"
                                    value="1"
                                    name="is_for_employee"
                                    checked
                                >
                                <label class="form-check-label">Karyawan</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input
                                    class="form-check-input is_for_employee"
                                    type="radio"
                                    value="0"
                                    name="is_for_employee"
                                >
                                <label class="form-check-label">Lainnya (Brand Ambassador,Affiliate Marketing)</label>
                            </div>

                        </div>
                    </div>
                    <div id="employee">
                        <div class="form-group row d-flex align-items-start">
                            <label class="col-sm-2 col-form-label font-weight-bold">Pilih Karyawan</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="staff_list" id="staff_list">

                                </select>

                                <div id="staff-result-wrapper" class="mt-3">
                                    <table class="table table-borderless table-custom" style="width:100%;">
                                        <thead>
                                        <tr>
                                            <th style="width:60%;">Nama Karyawan</th>
                                            <th style="width:40%;" class="text-center">Hapus</th>
                                        </tr>
                                        </thead>
                                        <tbody id="staff-result-body">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="non_employee">
                        <div class="form-group row d-flex align-items-start" id="non_employee">
                            <label class="col-sm-2 col-form-label font-weight-bold">Pilih Penerima Lainnya</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="other_staff_list" id="other_staff_list" multiple>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 mb-4 text-right">
                            <input type="hidden" name="is_manual" value="0">
                            <input type="hidden" id="increment_value_commission" value="0">
                            <a href="{{route('merchant.toko.commission.index')}}" class="btn btn-light text-light mr-2">Kembali</a>
                            <button class="btn btn-success" id="btn-save"><i class="fa fa-save mr-2"></i> Simpan Komisi</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('.amount_currency').mask("#.##0,00", {reverse: true});
        $('#product_list').select2();
        $('#branch').select2();
        $('#staff_list').select2();
        $("#other_staff_list").select2();


        $(".type").on("change", function(){
            let val = $(this).val();
            if(val == 'product')
            {
                $("#product_list_wrapper").show();
            } else {
                $("#product_list_wrapper").hide();
            }
        });

        $(".is_for_employee").on("change", function(){
            let val = $(".is_for_employee:checked").val();
            if(val == '1' || val==1)
            {
                $("#employee").show();
                $("#non_employee").hide();
            } else {
                $("#employee").hide();
                $("#non_employee").show();
            }
        });

        $(document).on("keyup", ".amount_percent", function(){
            let val = $(this).val();
            if(val > 100){
                $(this).val(100);
            }
            if(val < 0){
                $(this).val(0);
            }
        });

        let detailProductId = [];
        $("#product_list").on("change", function(){
            let productId = $('#product_list option:selected').val();
            let productName = $("#product_list option:selected").text();
            let productCode =$("#product_list option:selected").attr('data-code');

            if(productId ==  '-1'){
                return;
            }

            detailProductId.push(productId);

            let duplicateId = detailProductId.filter(item => {
                return item == productId;
            });

            if(duplicateId.length <= 1){
                let html = `<tr id="product-selected-${productId}">
                            <td>${productName}</td>
                            <td class="text-center">
                                <input
                                    type="hidden"
                                    value="${productId}"
                                    data-name="${productName}"
                                    data-code="${productCode}"
                                    class="product-input"
                                >
                                <button
                                    type="button"
                                    name="remove"
                                    data-id="${productId}"
                                    data-target="#product-selected-${productId}"
                                    class="btn btn-xs btn-delete-xs btn-rounded btn-remove-product text-center"
                                >
                                        <span class='fa fa-trash-alt' style='color: white'></span>
                                </button>
                            </td>
                        </tr>`

                $("#product-result-body").append(html);
            }

            setTimeout(() => {
                $('#product_list').val(-1).change();
            }, 500);
        });


        $(document).on('click', '.btn-remove-product', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");
            detailProductId = detailProductId.filter(item => {
                return item != parseInt(targetId);
            });
            $(target).remove();
        });

        let staffDetailId = [];
        let valueMultiple =[];

        $("#staff_list").on("change", function(){
            let staffId = $('#staff_list option:selected').val();
            let staffName = $('#staff_list option:selected').text();

            if(staffId ==  '-1'){
                return;
            }

            staffDetailId.push(staffId);

            let duplicateId = staffDetailId.filter(item => {
                return item == staffId;
            });

            if(duplicateId.length <= 1){
                let html = `<tr id="staff-selected-${staffId}">
                            <td>${staffName}</td>
                            <td class="text-center">
                                <input
                                    type="hidden"
                                    value="${staffId}"
                                    data-name="${staffName}"
                                    class="staff-input"
                                >
                                <button
                                    type="button"
                                    name="remove"
                                    data-id="${staffId}"
                                    data-target="#staff-selected-${staffId}"
                                    class="btn btn-xs btn-delete-xs btn-rounded btn-remove-staff text-center"
                                >
                                        <span class='fa fa-trash-alt' style='color: white'></span>
                                </button>
                            </td>
                        </tr>`

                $("#staff-result-body").append(html);
            }

        });

        $(document).on('click', '.btn-remove-staff', function(){
            var target = $(this).attr("data-target");
            var targetId = $(this).attr("data-id");
            staffDetailId = staffDetailId.filter(item => {
                return item != parseInt(targetId);
            });
            $(target).remove();
        });

        $("#is_all_branch").on("change", function(){
            if($(this).prop("checked") == true){
                $("#branch-wrapper").hide();
                $(this).val(1);
            } else {
                $("#branch-wrapper").show();
                $(this).val(0);
            }
        });

        $(".type_of_value_commissions,.type,.value_type").on("change", function(){
            let val = $('.type_of_value_commissions:checked').val();
            let type =$(".type:checked").val()
            let valueType=$(".value_type:checked").val()

            let html = "";
            $("#value-wrapper").empty();
            if(type=='product' && valueType=='nominal' && val=='flat'){

                html += `<input class="form-control amount_currency" type="text" name="value[]" value="0">`;
                $("#value-wrapper").append(html);
                $("#value-wrapper").find(".amount_currency").mask("#.##0,00", {reverse: true});

            }else if(type=='product' && valueType=='nominal' && val=='multiple')
            {
                html+="<div class='row'><div class='col-md-4 font-weight-bold'>Kelipatan Qty</div><div class='col-md-4 font-weight-bold'>Komisi Didapat</div></div>"
                html+="<div class='row'><div class='col-md-4'>" +
                    "<input type='text' class='form-control' value='0' name='value_other[]' onkeypress='isNumber(event)'>" +
                    "</div>" +
                    "<div class='col-md-4'>" +
                    "<input type='text' class='form-control amount_currency' name='value[]' value='0'>" +
                    "</div>" +
                    "</div>"
                $("#value-wrapper").append(html);
                $("#value-wrapper").find(".amount_currency").mask("#.##0,00", {reverse: true});

            }else if(type=='product' && valueType=='nominal' && val=='tiered'){
                html+="<div class='row'>" +
                    "<div class='col-md-4 font-weight-bold'>Min Qty</div>" +
                    "<div class='col-md-4 font-weight-bold'>Komisi Didapat</div>" +
                    "<div class='col-md-2 font-weight-bold'>Opsi</div></div>"
                html+="<div class='row mt-2' id='added_value_commission'></div>"
                html+="<div class='row mt-2'><div class='col-md-3'>" +
                    "<button class='btn btn-success-xs btn-xs' onclick='addValueCommission()' style='color: white' type='button' id='add_item_commission'>" +
                    "<i class='fa fa-plus mr-2'></i>Tambah</button>" +
                    "</div>" +
                    "</div>"
                $("#value-wrapper").append(html);
                $("#value-wrapper").find(".amount_currency").mask("#.##0,00", {reverse: true});
            }else if(type=='product' && valueType=='percentage' && val=='flat'){
                html += `<input class="form-control amount_percent" type="text" name="value[]" value="0" min="0" step="0.001" onkeypress="isNumber(event)">`;
                $("#value-wrapper").append(html);

            }else if(type=='product' && valueType=='percentage' && val=='multiple')
            {
                html+="<div class='row'>" +
                    "<div class='col-md-4 font-weight-bold'>Kelipatan Qty</div>" +
                    "<div class='col-md-4 font-weight-bold'>Komisi Didapat(%)</div>" +
                    "</div>"
                html+="<div class='row'>" +
                    "<div class='col-md-4'>" +
                    "<input type='text' class='form-control' onkeypress='isNumber(event)' value='0' name='value_other[]'>" +
                    "</div>" +
                    "<div class='col-md-4'>" +
                    "<input type='text' class='form-control amount_percent' name='value[]' value='0' onkeypress='isNumber(event)'>" +
                    "</div>" +
                    "</div>"
                $("#value-wrapper").append(html);

            }else if(type=='product' && valueType=='percentage' && val=='tiered'){
                html+="<div class='row'>" +
                    "<div class='col-md-4 font-weight-bold'>Min Qty</div>" +
                    "<div class='col-md-4 font-weight-bold'>Persentase Didapat</div>" +
                    "<div class='col-md-2 font-weight-bold'>Opsi</div></div>"
                html+="<div class='row mt-2' id='added_value_commission'></div>"
                html+="<div class='row mt-2'>" +
                    "<div class='col-md-3'>" +
                    "<button class='btn btn-success-xs btn-xs' onclick='addValueCommission()' style='color: white' type='button' id='add_item_commission'>" +
                    "<i class='fa fa-plus mr-2'></i>Tambah</button>" +
                    "</div>" +
                    "</div>"
                $("#value-wrapper").append(html);
            }else if(type=='transaction' && valueType=='percentage' && val=='flat'){

                html += `<input class="form-control amount_percent" type="text" name="value[]" value="0" min="0" step="0.001" onkeypress="isNumber(event)">`;
                $("#value-wrapper").append(html);

            }else if(type=='transaction' && valueType=='percentage' && val=='multiple')
            {
                html+="<div class='row'>" +
                    "<div class='col-md-4 font-weight-bold'>Kelipatan Transaksi</div>" +
                    "<div class='col-md-4 font-weight-bold'>Komisi Didapat(%)</div></div>"
                html+="<div class='row'>" +
                    "<div class='col-md-4'>" +
                    "<input type='text' class='form-control amount_currency' value='0' name='value_other[]'>" +
                    "</div>" +
                    "<div class='col-md-4'>" +
                    "<input type='text' class='form-control amount_percent' value='0' name='value[]' onkeypress='isNumber(event)'>" +
                    "</div>" +
                    "</div>"
                $("#value-wrapper").append(html);
                $("#value-wrapper").find(".amount_currency").mask("#.##0,00", {reverse: true});
            }else if(type=='transaction' && valueType=='percentage' && val=='tiered'){

                html+="<div class='row'>" +
                    "<div class='col-md-4 font-weight-bold'>Min Nilai Transaksi</div>" +
                    "<div class='col-md-4 font-weight-bold'>Persentase Didapat</div>" +
                    "<div class='col-md-2 font-weight-bold'>Opsi</div></div>"
                html+="<div class='row mt-2' id='added_value_commission'></div>"
                html+="<div class='row mt-2'>" +
                    "<div class='col-md-3'>" +
                    "<button class='btn btn-success-xs btn-xs' onclick='addValueCommission()' style='color: white' type='button' id='add_item_commission'>" +
                    "<i class='fa fa-plus mr-2'></i>Tambah</button>" +
                    "</div>" +
                    "</div>"
                $("#value-wrapper").append(html);

            }else if(type=='transaction' && valueType=='nominal' && val=='flat'){

                html += `<input class="form-control amount_currency" type="text" name="value[]" value="0">`;
                $("#value-wrapper").append(html);
                $("#value-wrapper").find(".amount_currency").mask("#.##0,00", {reverse: true});

            }else if(type=='transaction' && valueType=='nominal' && val=='multiple')
            {
                html+="<div class='row'>" +
                    "<div class='col-md-4 font-weight-bold'>Kelipatan Transaksi</div>" +
                    "<div class='col-md-4 font-weight-bold'>Komisi Didapat</div></div>"
                html+="<div class='row'>" +
                    "<div class='col-md-4'>" +
                    "<input type='text' class='form-control amount_currency' value='0' name='value_other[]'>" +
                    "</div>" +
                    "<div class='col-md-4'>" +
                    "<input type='text' class='form-control amount_currency' value='0' name='value[]'>" +
                    "</div>" +
                    "</div>"
                $("#value-wrapper").append(html);
                $("#value-wrapper").find(".amount_currency").mask("#.##0,00", {reverse: true});

            }else if(type=='transaction' && valueType=='nominal' && val=='tiered') {
                html += "<div class='row'>" +
                    "<div class='col-md-4 font-weight-bold'>Min Nilai Transaksi</div>" +
                    "<div class='col-md-4 font-weight-bold'>Komisi Didapat</div>" +
                    "<div class='col-md-2 font-weight-bold'>Opsi</div></div>"
                html += "<div class='row mt-2' id='added_value_commission'></div>"
                html += "<div class='row mt-2'>" +
                    "<div class='col-md-3'>" +
                    "<button class='btn btn-success-xs btn-xs' onclick='addValueCommission()' style='color: white' type='button' id='add_item_commission'>" +
                    "<i class='fa fa-plus mr-2'></i>Tambah</button>" +
                    "</div>" +
                    "</div>"
                $("#value-wrapper").append(html);
            }
        });


        $(document).ready(function () {
            $("#non_employee").hide()
            getEmployee()
            getNonEmployee()
            $('#form-konten').submit(function () {
                let data = getFormData('form-konten');

                let is_all_branch = ($("#is_all_branch").val())?$("#is_all_branch").val():0;
                let selectedBranch = ($("#branch").select2("data"))? $("#branch").select2("data"):[];
                let selectedOtherStaff = ($("#other_staff_list").select2("data"))? $("#other_staff_list").select2("data"):[];


                let selectedProduct = document.querySelectorAll(".product-input");
                let selectedStaff = document.querySelectorAll('.staff-input');


                let productList = [...selectedProduct].map(item => {
                    return {
                        "id": item.value,
                        "name": item.getAttribute('data-name').trim(),
                        "code": item.getAttribute('data-code').trim()
                    }
                });

                let staffList = [...selectedStaff].map(item => {
                    return {
                        "id":item.value,
                        "name": item.getAttribute('data-name').trim()
                    }
                });

                let branch = [...selectedBranch].map(item => {
                    return parseInt(item.id)
                });
                let otherStaff=  [...selectedOtherStaff].map(item => {
                    return {
                        "id":item.id,
                        "name":item.text
                    }
                });

                data.append("product_list", JSON.stringify(productList));
                data.append("staff_list", JSON.stringify(staffList));
                data.append("other_staff_list", JSON.stringify(otherStaff));
                data.append("is_all_branch", is_all_branch);
                data.append("branch", JSON.stringify(branch));
                ajaxTransfer("{{route('merchant.toko.commission.save')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
                });
            })
        })

        function getEmployee()
        {
            $.ajax({
                url: "{{route('merchant.ajax.share-data.employee-list')}}?md_merchant_id={{merchant_id()}}&is_non_employee=0",
                type: 'get',
                dataType: 'json',
                success: function (jsonObject){
                    $("#staff_list").select2({
                        placeholder:"Pilih Karyawan",
                        data:jsonObject
                    });
                }
            });
        }


        function getNonEmployee()
        {

            $.ajax({
                url: "{{route('merchant.ajax.share-data.employee-list')}}?md_merchant_id={{merchant_id()}}&is_non_employee=1",
                type: 'get',
                dataType: 'json',
                success: function (jsonObject){
                    $("#other_staff_list").select2({
                        placeholder:"Pilih Affiliator",
                        data:jsonObject
                    });
                }
            });
        }
        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function addValueCommission()
        {

            let i=parseInt($("#increment_value_commission").val())
            let val = $('.type_of_value_commissions:checked').val();
            let type =$(".type:checked").val()
            let valueType=$(".value_type:checked").val()
            let html=""
            if(type=='product' && valueType=='nominal' && val=='tiered'){
                html+="<div class='col-md-4 mb-1 item_list_value"+i+"'>" +
                    "<input type='text' class='form-control' value='0' name='value_other[]' onkeypress='isNumber(event)'>" +
                    "</div>" +
                    "<div class='col-md-4 mb-1 item_list_value"+i+"'>" +
                    "<input type='text' class='form-control amount_currency' name='value[]' value='0'>" +
                    "</div>" +
                    "<div class='col-md-2 mb-1 item_list_value"+i+"'>" +
                    "<button type='button' onclick='removeItem("+i+")' class='btn btn-xs btn-delete-xs btn-rounded btn_remove'><span class='fa fa-trash-alt'></span></button></div>"
            }else if(type=='product' && valueType=='percentage' && val=='tiered'){
                html+="<div class='col-md-4 mb-1 item_list_value"+i+"'>" +
                    "<input type='text' class='form-control' value='0' name='value_other[]' onkeypress='isNumber(event)'>" +
                    "</div>" +
                    "<div class='col-md-4 mb-1 item_list_value"+i+"'>" +
                    "<input type='text' class='form-control amount_percent' name='value[]' value='0' onkeypress='isNumber(event)'>" +
                    "</div>" +
                    "<div class='col-md-2 mb-1 item_list_value"+i+"'>" +
                    "<button type='button' onclick='removeItem("+i+")' class='btn btn-xs btn-delete-xs btn-rounded btn_remove'><span class='fa fa-trash-alt'></span></button></div>"
            }else if(type=='transaction' && valueType=='percentage' && val=='tiered'){
                html+="<div class='col-md-4 mb-1 item_list_value"+i+"'>" +
                    "<input type='text' class='form-control amount_currency' value='0' name='value_other[]'>" +
                    "</div>" +
                    "<div class='col-md-4 mb-1 item_list_value"+i+"'>" +
                    "<input type='text' class='form-control amount_percent' name='value[]' value='0' onkeypress='isNumber(event)'>" +
                    "</div>" +
                    "<div class='col-md-2 mb-1 item_list_value"+i+"'>" +
                    "<button type='button' onclick='removeItem("+i+")' class='btn btn-xs btn-delete-xs btn-rounded btn_remove'><span class='fa fa-trash-alt'></span></button></div>"

            }else if(type=='transaction' && valueType=='nominal' && val=='tiered'){
                html+="<div class='col-md-4 mb-1 item_list_value"+i+"'>" +
                    "<input type='text' class='form-control amount_currency' value='0' name='value_other[]'>" +
                    "</div>" +
                    "<div class='col-md-4 mb-1 item_list_value"+i+"'>" +
                    "<input type='text' class='form-control amount_currency' name='value[]' value='0'>" +
                    "</div>" +
                    "<div class='col-md-2 mb-1 item_list_value"+i+"'>" +
                    "<button type='button' onclick='removeItem("+i+")' class='btn btn-xs btn-delete-xs btn-rounded btn_remove'><span class='fa fa-trash-alt'></span></button></div>"

            }
            $('#added_value_commission').append(html)
            $("#added_value_commission").find(".amount_currency").mask("#.##0,00", {reverse: true});
            $("#increment_value_commission").val(i+1)

        }


        function removeItem(i){
            $('.item_list_value'+i+'').remove();
        }

        $("#other_staff_list").on('select2:open', () => {
            $(".select2-results:not(:has(a))").append('<a id="add-other" onclick="loadModal(this)" target="{{route("merchant.toko.staff.add-non-employee")}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Penerima Lainnya</b></a>');
        });
        $(document).on('click','#add-other', function(e){
            $("#other_staff_list").select2('close');
        });


    </script>
@endsection

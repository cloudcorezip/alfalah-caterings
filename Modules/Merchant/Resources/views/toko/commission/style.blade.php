<style>
.custom-switch .custom-control-label::before {
    width:40px;
    height: 1.4rem;
    border:none;
    background:#D6D6D6;
    border-radius: 18px;
}
.custom-switch .custom-control-label::after {
    width: calc(1.4rem - 4px);
    height: calc(1.4rem - 4px);
    background: #FFFFFF;
    cursor: pointer;
}
.custom-switch .custom-control-input:checked~.custom-control-label::after {
    transform: translateX(1.1rem);
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input[type=number] {
    -moz-appearance: textfield;
}

.btn-increment {
    background:#fff!important;
    border: 1px solid #F96822!important;
    border-radius: 6px;
    color: #F96822!important;
}

.increment-wrapper {
    position: relative;
}

.increment-wrapper .btn-min {
    position: absolute;
    left: 0;
}

.increment-wrapper .btn-plus {
    position: absolute;
    right: 0;
}

.product-by-wrapper {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    width:100%;
    background:#F7F7F7;
    padding: 10px 12px;
}

.btn-product-by {
    background-color:#F7F7F7;
    color: #ACACAC;
    border-radius:5px;
    display: block;
    width:100%;
}

.btn-product-by.active {
    background-color: #FF8A00;
    color: #fff;
}

.btn-outline-warning.active span{
    color: #fff!important;
}

.table-responsive {
    max-height: 60vh;
    overflow-y: auto;
}
.table-responsive .table-borderless thead{
    position: sticky;
    top: 0;
    z-index: 100!important;
}

.input-radio-group {
    display:grid;
    grid-template-columns: 1fr 1fr;
    border:1px solid #F3F3F3;
    padding:4px;
}

.btn-input-radio {
    border-radius: 4px;
}

.btn-input-radio.active {
    background-color: #FF8A00;
}

.btn-input-radio span {
    color: #ACACAC;
}

.btn-input-radio.active span {
    color: #fff;
}

</style>
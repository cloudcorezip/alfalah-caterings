<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-cost" class='form-horizontal form-konten' backdrop="">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1">Nama Biaya</label>
                <input type="name" class="form-control form-control-sm" name="name" placeholder="Nama Biaya" value="">
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-sm btn-success py-2 px-4">Simpan</button>

    </div>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-cost').submit(function () {
            var data = getFormData('form-cost');
            ajaxTransfer("{{route('merchant.toko.clinic.services-pack.save-cost')}}", data, function (response){
                var data = JSON.parse(response);
                var data = JSON.parse(response);
                if(data.type=='danger' || data.type=='warning'){
                    otherMessage(data.type,data.message)
                }else{
                    otherMessage(data.type,data.message)
                    closeModalAfterSuccess()
                }
            });
        })

    })

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Biaya Baru</h4>
                                    </div>`);
    });



</script>

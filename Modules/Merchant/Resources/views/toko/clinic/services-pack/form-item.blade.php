<div id="result-form-konten-raw-material"></div>

<form onsubmit="return false;" id="form-konten-raw-material" class='form-horizontal form-konten' backdrop="">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Jumlah</label>
                <input type="number" class="form-control form-control-sm qty_product" id="quantity" name="quantity" step="0.001" min="0" placeholder="Qty" value="{{is_null($data->quantity)?0:$data->quantity}}" onkeypress="isNumberKey(event)" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Isian Paket</label>
                <select name="child_sc_product_id" class="form-control form-control-sm" id="product" @if(is_null(!$data->id)) disabled @else onchange="productChange()" @endif>
                    <option value="-1" disabled @if(is_null($data->id)) selected @endif>--Pilih Isian Paket--</option>
                    @if(!is_null($data->child_sc_product_id))
                        <option value="{{$data->child_sc_product_id}}" selected>{{$data->getChild->name}} {{$data->getChild->code}}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Satuan</label>
                @if(!is_null($data->id))
                    @if($data->is_multi_unit==0)
                        <input type="hidden" id="is_multi_unit" name="is_multi_unit" value="0">
                        <input type="hidden" id="multi_unit_id" name="multi_unit_id" value="-1">
                        <input type="hidden" id="value_conversion" name="value_conversion" value="1">
                        <input type="hidden" id="json_multi_unit" name="json_multi_unit" value="-1">
                        <input type="hidden" id="unit_name" name="unit_name" value="{{$data->unit_name}}">
                        <select name="unit_id" class="form-control form-control-sm" id="unit_id">
                            <option value="0" selected>{{$data->unit_name}}</option>
                        </select>
                    @else
                        <input type="hidden" id="is_multi_unit" name="is_multi_unit" value="1">
                        <input type="hidden" id="multi_unit_id" name="multi_unit_id" value="{{$data->multi_unit_id}}">
                        <input type="hidden" id="value_conversion" name="value_conversion" value="{{$data->value_conversion}}">
                        <input type="hidden" id="json_multi_unit" name="json_multi_unit" value="{{$data->json_multi_unit}}">
                        <input type="hidden" id="unit_name" name="unit_name" value="{{$data->multi_unit_name}}">
                        @php
                         $jsonMulti=json_decode($data->json_multi_unit);
                        @endphp
                        <select name="unit_id" class="form-control form-control-sm" id="unit_id" onchange="getMultiUnit()">
                            @foreach($jsonMulti as $j)
                            <option value="{{$j->id}}"
                            @if($j->original_id==$data->multi_unit_id) selected @endif
                            >{{$j->text}}</option>
                            @endforeach
                        </select>
                    @endif
                @else
                    <input type="hidden" id="is_multi_unit" name="is_multi_unit" value="0">
                    <input type="hidden" id="multi_unit_id" name="multi_unit_id" value="-1">
                    <input type="hidden" id="value_conversion" name="value_conversion" value="1">
                    <input type="hidden" id="json_multi_unit" name="json_multi_unit" value="-1">
                    <input type="hidden" id="unit_name" name="unit_name" value="-1">
                    <select name="unit_id" class="form-control form-control-sm" id="unit_id" onchange="getMultiUnit()" required>
                    </select>
                @endif
            </div>
        </div>
        <div class="col-md-6">

            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Total</label>
                <input type="text" class="form-control form-control-sm amount_currency" id="total_price" name="total_price" placeholder="" value="{{is_null($data->total)?rupiah(0):rupiah($data->total)}}" required>
            </div>
        </div>
    </div>


    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='sc_product_id' value='{{$sc_product_id }}'>
    <input type='hidden' name='price' id="price" value='{{$price }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $("#unit_id").select2()
        $('#form-konten-raw-material').submit(function () {
            var data = getFormData('form-konten-raw-material');
            ajaxTransfer("{{route('merchant.toko.clinic.services-pack.save-item')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    })


</script>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
        getProduct()
    });

    function getProduct()
    {
        $("#product").select2({
            placeholder:"---- Pilih Isian Paket ----",
            ajax: {
                type: "GET",
                url: "{{route('api.product.non-package')}}?userId={{user_id()}}",
                dataType: 'json',
                delay: 250,
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: false
            },
        });
    }

    $('.amount_currency').mask("#.##0,00", {reverse: true});

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">Tambah Isian Paket</h4>
                                        <span class="span-text">Untuk menambah isian paket, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function getMultiUnit()
    {

        let unitList = ($("#unit_id").select2('data'))? $("#unit_id").select2('data'):[];
        if(unitList.length>0)
        {

            let data=unitList[0]
            if(data['nilai_konversi']===undefined){
                let jsonMulti= $("#json_multi_unit").val()
                let unitVal = $("#unit_id").val()
                let unitList2 =JSON.parse(jsonMulti)
                unitList2.forEach(element=>{
                    if(element.id==unitVal){
                        $("#is_multi_unit").val(1)
                        $("#unit_name").val(element.text)
                        $("#value_conversion").val(element.nilai_konversi)
                        $("#multi_unit_id").val(element.original_id)
                        let quantity=parseFloat($("#quantity").val())
                        let price=parseFloat(element.purchase_price)
                        $('#total_price').val(currencyFormat(quantity*price))
                    }
                })
            }else{
                $("#is_multi_unit").val(1)
                $("#unit_name").val(data['text'])
                $("#value_conversion").val(data['nilai_konversi'])
                $("#multi_unit_id").val(data['original_id'])
                let quantity=parseFloat($("#quantity").val())
                let price=parseFloat(data['purchase_price'])
                $('#total_price').val(currencyFormat(quantity*price))
            }
        }
    }
    function productChange()
    {
        let products = ($("#product").select2('data'))? $("#product").select2('data'):[];

        if(products.length>0) {
            let datas = products[0]
            let multi_unit = JSON.parse(datas['multi_unit'])
            let multi_unit_list = []
            $('#unit_id').empty();
            if (multi_unit.length > 0) {
                $.each(multi_unit, function (iteration, value) {
                    if (iteration === 0) {
                        multi_unit_list.push({
                            id: '-1',
                            text: 'Pilih Satuan',
                            purchase_price: 0,
                            nilai_konversi: 0,
                            original_id: '-1',
                            selling_price: 0,
                            is_with_stock: datas['is_with_stock']
                        })
                    }

                    multi_unit_list.push({
                        id: value.unit_id,
                        text: value.konversi_ke,
                        purchase_price: value.harga_jual,
                        nilai_konversi: value.nilai_konversi,
                        original_id: value.id,
                        selling_price: 0,
                        is_with_stock: datas['is_with_stock']
                    })
                })
                $("#unit_id").select2({
                    data: multi_unit_list
                })
                $("#json_multi_unit").val(JSON.stringify(multi_unit_list))
                $("#quantity").val(1)
                if (datas['is_with_stock'] == 0) {
                    let quantity = parseFloat($("#quantity").val())
                    let price = parseFloat(datas['selling_price'])
                    $('#total_price').val(currencyFormat(quantity * price))
                } else {
                    let quantity = parseFloat($("#quantity").val())
                    let price = parseFloat(datas['purchase_price'])
                    $('#total_price').val(currencyFormat(quantity * price))

                }
            } else {
                $("#is_multi_unit").val(0)
                $("#quantity").val(1)
                $("#unit_name").val(datas['unit_name'])
                if (datas['is_with_stock'] == 0) {
                    let quantity = parseFloat($("#quantity").val())
                    let price = parseFloat(datas['selling_price'])
                    $('#total_price').val(currencyFormat(quantity * price))
                } else {
                    let quantity = parseFloat($("#quantity").val())
                    let price = parseFloat(datas['purchase_price'])
                    $('#total_price').val(currencyFormat(quantity * price))
                }
                multi_unit_list.push({
                    id: datas['unit_id'],
                    text: datas['unit_name'],
                    purchase_price: datas['purchase_price'],
                    selling_price: datas['selling_price'],
                    nilai_konversi: 1,
                    original_id: datas['id'],
                    is_with_stock: datas['is_with_stock']
                })
                $("#unit_id").select2({
                    data: multi_unit_list
                })
            }
        }

    }

</script>

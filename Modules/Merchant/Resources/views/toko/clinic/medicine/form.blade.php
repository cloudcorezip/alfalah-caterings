@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .col-pad {
            padding: 0 100px!important;
        }
        @media screen and (max-width: 1200px){
            .col-pad {
                padding: 0 70px!important;
            }
        }
        @media screen and (max-width: 992px){
            .col-pad {
                padding:0 20px!important;
            }
        }
    </style>
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <div class="p-5">
                <h4 class="mb-1">{{$title}}</h4>
                <span>Tambahkan data obat disini !</span>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="card shadow mb-4 rounded">
            <div class="card-body">
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Kode</label>
                                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode" value="{{is_null($data->id)? $code:$data->code}}" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Obat</label>
                                <input type="text" class="form-control form-control-sm" name="name" value="{{$data->name}}" required placeholder="Nama Obat">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Nama Kategori <span class="text-danger">*</span></label>
                                <select name="sc_product_category_id" class="form-control" id="sc_product_category_id" required>
                                    <option></option>
                                    @foreach($category as $key => $item)
                                        <option value="{{$item->id}}" @if($item->id==$data->sc_product_category_id) selected @endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Satuan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="md_unit_id" name="md_unit_id" value="{{is_null($data->md_unit_id)?'':$data->getUnit->name}}" required placeholder="Contoh satuan : buah, pcs, bungkus">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Harga Beli <span class="text-danger">*</span></label>
                                <input type="text" class="form-control amount_currency" name="purchase_price" step="0.001" min="0"  value="{{is_null($data->purchase_price)?0:number_format((float)$data->purchase_price, 2, '.', '')}}" id="harga-beli" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Harga Jual <span class="text-danger">*</span></label>
                                <input type="text" class="form-control amount_currency" name="selling_price" step="0.001" min="0"  value="{{is_null($data->selling_price)?0:number_format((float)$data->selling_price, 2, '.', '')}}" id="harga-jual" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Aturan Pakai</label>
                                <textarea class="form-control" style="min-height: 200px" name="description" id="desc" placeholder="Tambahkan deskripsi produk">{{$data->description}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan</label>
                                <div style="width:100%;height:250px;color:#A4A4A4;">
                                    Isi keterangan sesuai dengan nama obat yang akan ditampilkan.<br>Agar bisa dimengerti oleh pasien
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="modal-footer">
                        <a href="{{route('merchant.toko.clinic.medicine.index')}}" class="btn btn-light" data-dismiss="modal">Kembali</a>
                        <button class="btn btn-success">Simpan</button>
                    </div>
                    <input type='hidden' name='id' value='{{$data->id }}'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="is_with_stock" value="1">
                    <input type="hidden" name="md_sc_product_type_id" value="1">
                </form>
            </div>
        </div>
        
    </div>
@endsection

@section('js')
<script>
$(document).ready(function () {
    $('.amount_currency').mask("#.##0,00", {reverse: true});
    $('#sc_product_category_id').select2({
        placeholder:'--- Pilih Kategori Produk ---',
    }).on('select2:open', () => {
        $(".select2-results:not(:has(a))").has('#select2-sc_product_category_id-results').append('<a id="add-category" onclick="loadModalFullScreen(this)" target="{{route("merchant.toko.product-category.add")}}" style="padding: 6px;height: 20px;display: inline-table;" class="text-danger"><b>+ Tambah Kategori Baru</b></a>');
    });

    $(document).on('click','#add-category', function(e){
        $("#sc_product_category_id").select2('close');
    });

    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        ajaxTransfer("{{route('merchant.toko.clinic.medicine.save')}}", data, function (response){
            var data = JSON.parse(response);
            toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            hideLoading();
        });
    })
});
</script>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: Arial, sans-serif; width:100%;margin-right:auto;margin-left:auto">
    <div style="width:90%; margin:0 auto;">
        <div style="background:#ff9422;width: 100%;padding-top: 25px;padding-left: 25px;padding-right: 25px;padding-bottom: 10px;border-radius: 16px;">
            <div style="width:100%;">
                @if(is_null($merchant->image))
                    <img style="width: 50px;
                        height: 50px;
                        border-radius: 100%;
                        object-fit: cover;
                        float:left;
                        margin-right:20px;" 
                        src="{{asset('public/backend/img/no-user.png')}}" 
                        alt="logo">
                @else
                    <img style="width: 50px;
                            height: 50px;
                            border-radius: 100%;
                            object-fit: cover;
                            margin-right:20px;
                            float:left;"  
                            src="{{env('S3_URL')}}{{$merchant->image}}" 
                            alt="logo">
                @endif
                    <h3 style="text-transform: capitalize;
                            color:white;
                            font-weight:bolder;
                            white-space:nowrap;
                            overflow: hidden;
                            margin-top: 0;
                            margin-bottom: 0;
                            ">
                            {{$merchant->name}}
                    </h3>
                    <h6 style="text-transform: capitalize;
                            color:white;
                            font-weight:bolder;
                            white-space:nowrap;
                            overflow: hidden;
                            margin-top: 0;
                            ">
                            {{$merchant->address}}
                    </h6>
            </div>
            <div style="clear:both;"></div>
            <div style="width:100%;">
                <h2 style="font-weight:bold;color:white;text-align:center;margin-bottom:5px; margin-top:0;">Scan QR Code</h2>
                <span style="display:block;color:white;text-align:center;">Untuk melihat katalog produk</span>
            </div>
            <br>
            <div style="width:100%;position: relative;border-radius: 12px;background-color: #e2e2e2;padding:20px;">
                <img style="width: 100%;border-radius: 12px;" src="{{env('S3_URL')}}{{$data->qr_code}}">
            </div>
            <div 
                style="text-align:center;
                width: 0;
                height: 0;
                border-left: 20px solid transparent;
                border-right: 20px solid transparent;
                border-top: 20px solid #e2e2e2;
                margin: 0 auto;
                transform: translateY(-3px);
                ">
            </div>
            <div style="width:100%;">
                <h5 style="text-align:center;color:white;font-weight:bold;margin-bottom:10px;">Atau Kunjungi Link di Bawah Ini</h5>
                <a style="display:block;text-align:center;color:yellow;text-decoration:none;margin-bottom:6px;" href="{{$data->url}}" target="_blank">{{$data->url}}</a>
                <small style="display:block;text-align:center;color:white;">Powered by 
                        <img 
                            src="{{asset('public/backend/img/logo-white.png')}}" 
                            alt=""
                            style="width:50px"
                        >
                </small>
            </div>
        </div>
    </div>

</body>
</html>
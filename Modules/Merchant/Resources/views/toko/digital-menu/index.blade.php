@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->

    <!-- DataTales Example -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">{{$title}}</h4>
                <span>Buat katalog digital untuk produk yang kamu jual disini</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->

    <div class="container-fluid">
        @if(Session::get('error-download'))
            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                {{Session::get('error-download')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">
            <div class="col-md-4">
                @if(is_null($data))
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="alert alert-warning">
                            <p class="text-center">Anda belum membuat QR Code katalog menu,silahkan klik tombol dibawah ini</p>
                        </div>
                        <center>
                            <button onclick="generate()" class="btn btn-warning btn-rounded">
                                <i class="fa fa-refresh"></i> Buat QR Code
                            </button>
                        </center>
                    </div>
                </div>
                @else
                <div class="card shadow mb-4">
                    <div class="card-body">
                            <div class="row px-2 pt-2 pb-4">
                                <div class="col-md-12">
                                    <h2 style="color:#323232;" class="font-weight-bold text-center">Kartu QRCode</h2>
                                    <p stlye="color:#808080;" class="text-center">Scan QR Code di bawah ini untuk melihat katalog produk</p>
                                    <div class="store-qr__wrapper mb-4">
                                        <div class="d-flex align-items-start mb-2">
                                            @if(is_null($merchant->image))
                                                <img class="store-qr__merchant__img mr-3" src="{{asset('public/backend/img/no-user.png')}}" alt="logo">
                                            @else
                                                <img class="store-qr__merchant__img mr-3" src="{{env('S3_URL')}}{{$merchant->image}}" alt="logo">
                                            @endif
                                            <div class="store-qr__merchant__info">
                                                <h5 class="text-white font-weight-bold">{{$merchant->name}}</h5>
                                                <p class="text-white">{{$merchant->address}}</p>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col-12 text-center">
                                                <h1 class="text-white font-weight-bold mb-3" style="line-height:0;">Scan QR Code</h1>
                                                <span class="text-white">Untuk melihat katalog produk</span>
                                            </div>
                                        </div>
                                        <div class="store-qr__img__wrapper p-3 mb-4">
                                            <img src="{{env('S3_URL')}}{{$data->qr_code}}">
                                            <div class="store-qr__triangle"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <h5 class="text-center text-white font-weight-bold">Atau Kunjungi Link di Bawah Ini</h5>
                                                <a class="d-block text-center" style="color:yellow;" href="{{$data->url}}" target="_blank">{{$data->url}}</a>
                                                <small class="d-block text-center text-white mt-3">Powered by
                                                        <img
                                                            src="{{asset('public/backend/img/logo-white.png')}}"
                                                            alt=""
                                                            style="width:50px"
                                                        >
                                                </small>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-5">
                                        <div class="col-12 text-center">
                                            <a
                                                href='{{route("merchant.toko.digital-menu.download-qr",["id" => $data->md_merchant_id])}}'
                                                class="btn text-white btn-block"
                                                style="background: #FF9922;box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);"
                                                target="_blank"
                                            >
                                                Download PDF
                                                </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="card shadow mb-4">
                    <div class="row px-4 pt-4 pb-4">
                        <div class="col-md-12 mb-3">
                            <h5 style="color:#323232;" class="font-weight-bold">Atur Nilai Pajak dan Tipe Pemesanan</h5>
                        </div>
                        <div class="col-md-12">
                            <form onsubmit="return false;" id="form-konten-config" class='form-horizontal' backdrop="">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Pakai Pajak(%)</label>
                                    <input type="number" class="form-control" name="tax_percentage" value="{{$data->tax_percentage}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Tipe Order</label>
                                    <select class="form-control" id="tipe" name="order_type" onchange="hidePayment()">
{{--                                        <option value="1" {{($data->order_type==1)?'selected':''}} disabled >Bayar di Kasir</option>--}}
                                        <option value="2" {{($data->order_type==2)?'selected':''}} > Langsung Bayar</option>
                                    </select>
                                </div>
                                <div id="payment">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Metode Pembayaran</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" name="payment_type[]" type="checkbox" id="inlineCheckbox1" value="4"
                                            @if(!is_null($data->payment_type) && in_array(4, json_decode($data->payment_type)))
                                            checked
                                            @endif
                                        >
                                        <label class="form-check-label" for="inlineCheckbox1">QRIS</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="-1" disabled>
                                        <label class="form-check-label" for="inlineCheckbox1">Virtual Account <i>(Belum tersedia)</i></label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-warning py-2 px-4">Simpan</button>
                                </div>
                                <input type='hidden' name='id' value='{{$data->id }}'>
                                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            </form>
                        </div>
                    </div>
                </div>
                @endif
            </div>

            <div class="col-md-8">
                <div class="card shadow mb-4">

                    <div class="card-body">
                        <div class="card-title">
                            <h4 style="color:#323232;" class="font-weight-bold">Data Produk</h4>
                            <hr>
                        </div>
                        @if(is_null($data))
                            <div class="alert alert-warning">
                                <p class="text-center">Anda belum membuat QR Code katalog menu,silahkan klik tombol <b>Buat QR Code</b></p>
                            </div>
                        @else
                            @if(count($dataProduct) == 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-warning fade show text-center" role="alert">
                                            Semua produk sudah dimasukkan ke Katalog !
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-md-12 mb-3">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" onchange="refineCheckboxProduct(this)">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Pilih semua produk
                                            </label>
                                        </div>
                                    </div>
                                    @foreach($dataProduct as $key => $item)
                                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div class="card shadow card-store__product">
                                                <div class="card-header p-0 img-card__store__wrapper">
                                                    @if(!is_null($item->foto_produk))
                                                        <img src="{{env('S3_URL')}}{{$item->foto_produk}}" class="img-card__store" alt="">
                                                    @else
                                                        <img src="{{asset('public/backend/img/no-product-image.png')}}" class="img-card__store" alt="">
                                                    @endif

                                                </div>
                                                <div class="card-body px-3">
                                                    <h5 class="card-store__product__title">{{$item->nama_produk}}</h5>
                                                    <h6 class="card-store__product__price">
                                                        {{rupiah($item->harga)}}
                                                        <span style="color:#939393;">/</span>
                                                        <small style="color:#939393;">{{ucfirst($item->nama_satuan)}}</small>
                                                    </h6>
                                                    <small class="d-block card-store__product__stock">Stok {{$item->stok}} {{ucfirst($item->nama_satuan)}}</small>
                                                    <small class="card-store__product__stock">Kategori : {{ucfirst($item->nama_kategori_produk)}}</small>
                                                </div>
                                                <div class="card-footer">
                                                    <input
                                                        class="cb-product"
                                                        value="{{$item->id}}_{{$item->category_id}}"
                                                        type="checkbox"
                                                        autocomplete="off"
                                                        style="visibility:hidden; position:absolute;"
                                                        id="{{$item->id}}_{{$item->category_id}}"
                                                    >
                                                    <button
                                                        class="btn btn-store btn-sm btn-block btn-product"
                                                        onclick="selectProduct(this,'{{$item->id}}_{{$item->category_id}}')"
                                                        data-checked="0"
                                                    >
                                                        Tambahkan
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                {{$dataProduct->withQueryString()->links('vendor.pagination.with-showing-entry')}}
                                @if($dataProduct->count()>0)
                                    <br>
                                    <a onclick="catatDataProduct()" class="btn btn-warning" data-original-title="" title="" rel="noopener noreferrer" style="color: white">Proses Pencatatan Katalog</a>
                                @endif
                            @endif
                        @endif
                    </div>
                </div>

                <div class="card shadow mb-4">

                    <div class="card-body">
                        <div class="card-title">
                            <h4 style="color:#323232;" class="font-weight-bold">Data Katalog Ditampilkan</h4>
                        </div>
                        <hr>
                        @if(is_null($data))
                            <div class="alert alert-warning">
                                <p class="text-center">Anda belum membuat QR Code katalog menu,silahkan klik tombol <b>Buat QR Code</b></p>
                            </div>
                        @else
                            @if(count($dataCatalog) == 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-warning fade show text-center" role="alert">
                                            Belum ada katalog produk yang dipilih !
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-9">
                                        <select class="form-control-sm" id="action">
                                            <option value="2">Sembunyikan</option>
                                            <option value="3">Tampilkan</option>
                                            <option value="1">Hapus dari Katalog</option>
                                            <option value="4">Atur Diskon</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-3">
                                        <a onclick="prosecCatalog()" class="btn btn-warning" data-original-title="" title="" rel="noopener noreferrer" style="color: white">Proses</a>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-12 mb-3">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" onchange="refineCheckboxCatalog(this)">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Pilih semua produk
                                            </label>
                                        </div>
                                    </div>

                                    @foreach($dataCatalog as $key => $item)
                                        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                                            <div class="card shadow card-store__product card-store__catalog">
                                                <div class="card-header p-0 img-card__store__wrapper">
                                                    @if(!is_null($item->foto_produk))
                                                        <img src="{{env('S3_URL')}}{{$item->foto_produk}}" class="img-card__store" alt="">
                                                    @else
                                                        <img src="{{asset('public/backend/img/no-product-image.png')}}" class="img-card__store" alt="">
                                                    @endif
                                                    @if($item->diskon > 0)
                                                        <div class="card-store__discount">
                                                            <span>{{$item->diskon}}%</span>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="card-body px-3">
                                                    <h5 class="card-store__product__title">{{$item->nama_produk}}</h5>
                                                    <h6 class="card-store__product__price">
                                                        {{rupiah($item->harga)}}
                                                        <span style="color:#939393;">/</span>
                                                        <small style="color:#939393;">{{ucfirst($item->nama_satuan)}}</small>
                                                    </h6>
                                                    <small class="d-block card-store__product__stock">Stok {{$item->stok}} {{ucfirst($item->nama_satuan)}}</small>
                                                    <small class="d-block mb-2 card-store__product__stock">Kategori : {{ucfirst($item->nama_kategori_produk)}}</small>
                                                    <small class="d-block card-store__product__stock">
                                                        @if($item->is_show==0)
                                                            <label style="border-radius:20px;font-size:10px;" class="badge badge-danger rounded">Tidak Tampil</label>
                                                        @else
                                                            <label style="border-radius:20px;font-size:10px;" class="badge badge-success">Tampil</label>

                                                        @endif
                                                    </small>
                                                </div>
                                                <div class="card-footer">
                                                    <input
                                                        class="cb-catalog"
                                                        value="{{$item->catalog_id}}"
                                                        type="checkbox"
                                                        autocomplete="off"
                                                        style="visibility:hidden; position:absolute;"
                                                        id="{{$item->catalog_id}}"
                                                    >
                                                    <button
                                                        class="btn btn-store btn-sm btn-block btn-catalog"
                                                        onclick="selectCatalog(this,'{{$item->catalog_id}}')"
                                                        data-checked="0"
                                                    >
                                                        Pilih
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                @if(!empty($dataCatalog))
                                    {{$dataCatalog->withQueryString()->links('vendor.pagination.with-showing-entry')}}
                                @endif
                                <input type="hidden" id="discount_percentage" value="0">
                                <br>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $("#tipe").select2({});
            $("#action").select2({});
            @if(!is_null($data))
                @if($data->order_type==1)
                    $("#payment").hide();

                @elseif($data->order_type==2)
                $("#payment").show();

                @else
                $("#payment").show();

                  @endif
            @endif

        });
        function generate() {
            var data = new FormData();

            modalConfirm("Konfirmasi", "Apa anda yakin membuat QR Code Katalog Menu ?", function () {
                ajaxTransfer("{{route('merchant.toko.digital-menu.generate',['id'=>merchant_id()])}}", data, "#modal-output");
            })
        }

        $('#form-konten-config').submit(function () {
            var data = getFormData('form-konten-config');
            modalConfirm("Konfirmasi", "Apa anda yakin menyimpan pengaturan tipe order dan metode pembayaran ?", function () {
                ajaxTransfer("{{route('merchant.toko.digital-menu.save-order-type')}}", data, "#modal-output");
            })
        })


        function copyUrl() {
            var copyText = document.getElementById("url");
            copyText.select();
            document.execCommand("copy");
            alert("Copied!");

        }

        function refineCheckboxProduct(t) {
            $('.cb-product').prop('checked', $(t).prop('checked'));
            selectProduct(null,null,true, $(t).prop('checked'));
        }

        function refineCheckboxCatalog(t) {
            $('.cb-catalog').prop('checked', $(t).prop('checked'));
            selectCatalog(null, null, true,$(t).prop('checked'));
        }

        function selectProduct(event=null,selector=null, checkAll=false, isCheck=null){
            if(!checkAll){
                const isCheck = event.getAttribute('data-checked');
                if(isCheck == 0){
                    $('#'+selector).prop('checked', 'checked');
                    event.setAttribute('data-checked', 1);
                    event.classList.remove('btn-store');
                    event.classList.add('btn-store__checked');
                    event.innerText = "Batalkan";
                } else {
                    $('#'+selector).prop('checked', '');
                    event.setAttribute('data-checked', 0);
                    event.classList.remove('btn-store__checked');
                    event.classList.add('btn-store');
                    event.innerText = "Tambahkan";
                }
            } else {
                const btnEl = [...document.querySelectorAll('.btn-product')];
                const isBtnCheck = isCheck? 1:0;
                const removeClass = isCheck? 'btn-store':'btn-store__checked';
                const addClass = isCheck? 'btn-store__checked':'btn-store';
                const btnText = isCheck? 'Batalkan':'Tambahkan';
                btnEl.forEach(item => {
                    item.setAttribute('data-checked', isBtnCheck);
                    item.classList.remove(removeClass);
                    item.classList.add(addClass);
                    item.innerText = btnText;
                });
            }

        }

        function selectCatalog(event=null,selector=null, checkAll=false, isCheck=null){
            if(!checkAll){
                const isCheck = event.getAttribute('data-checked');
                if(isCheck == 0){
                    $('#'+selector).prop('checked', 'checked');
                    event.setAttribute('data-checked', 1);
                    event.classList.remove('btn-store');
                    event.classList.add('btn-store__checked');
                    event.innerText = "Batal";
                } else {
                    $('#'+selector).prop('checked', '');
                    event.setAttribute('data-checked', 0);
                    event.classList.remove('btn-store__checked');
                    event.classList.add('btn-store');
                    event.innerText = "Pilih";
                }
            } else {
                const btnEl = [...document.querySelectorAll('.btn-catalog')];
                const isBtnCheck = isCheck? 1:0;
                const removeClass = isCheck? 'btn-store':'btn-store__checked';
                const addClass = isCheck? 'btn-store__checked':'btn-store';
                const btnText = isCheck? 'Batal':'Pilih';
                btnEl.forEach(item => {
                    item.setAttribute('data-checked', isBtnCheck);
                    item.classList.remove(removeClass);
                    item.classList.add(addClass);
                    item.innerText = btnText;
                });
            }
        }

        function catatDataProduct() {
            modalConfirm('Konfirmasi Tindakan', 'Apakah Anda yakin akan mencatat data ke katalog?', function() {
                var ids = [];
                var checkbox = $('.cb-product');
                var i, value;
                for (i = 0; i < checkbox.length; i++) {
                    value = $(checkbox[i]).val();
                    if ($(checkbox[i]).prop('checked')) {
                        ids.push(value);
                    }
                }


                if (ids.length === 0) {
                    $('#modal-output').html(alertDanger('Terjadi kesalahan! Silahkan memilih setidaknya satu data'));
                    return false;
                }
                var data=new  FormData();
                data.append('config_id',{{(is_null($data))?0:$data->id}})
                data.append('ids',ids)
                ajaxTransfer('{{route('merchant.toko.digital-menu.save-to-catalog')}}', data, '#modal-output');
            });
        }

        function prosecCatalog() {
            var ids = [];
            var checkbox = $('.cb-catalog');
            var action=$("#action").val();
            var i, value;
            for (i = 0; i < checkbox.length; i++) {
                value = $(checkbox[i]).val();
                if ($(checkbox[i]).prop('checked')) {
                    ids.push(value);
                }
            }

            if(action==4){
                modalConfirm('Atur Diskon', '<div class="form-group"> ' +
                    '<label for="exampleInputPassword1">Atur Diskon Produk(%)</label> ' +
                    '<input type="number" class="form-control" name="discount_percentage" id="discount" placeholder="Diskon" value="0" max="100" required onchange="changeDiscount()"> ' +
                '</div>', function() {

                    if (ids.length === 0) {
                        $('#modal-output').html(alertDanger('Terjadi kesalahan! Silahkan memilih setidaknya satu data'));
                        return false;
                    }
                    var data=new  FormData();
                    data.append('action',action)
                    data.append('ids',ids)
                    data.append('discount',$("#discount_percentage").val())
                    ajaxTransfer('{{route('merchant.toko.digital-menu.proses-catalog')}}', data, '#modal-output');
                });
            }else{
                modalConfirm('Konfirmasi Tindakan', 'Apakah Anda yakin akan melakukan hal ini?', function() {

                    if (ids.length === 0) {
                        $('#modal-output').html(alertDanger('Terjadi kesalahan! Silahkan memilih setidaknya satu data'));
                        return false;
                    }
                    var data=new  FormData();
                    data.append('action',action)
                    data.append('ids',ids)
                    ajaxTransfer('{{route('merchant.toko.digital-menu.proses-catalog')}}', data, '#modal-output');
                });
            }
        }

        function hidePayment()
        {
            if($("#tipe").val()==1)
            {
                $("#payment").hide();
            }else{
                $("#payment").show();

            }
        }
        function changeDiscount()
        {
            if( $("#discount").val() > 100){
                $("#discount").val(100)
            } else if($("#discount").val() < 0){
                $("#discount").val(0)
            }
            $("#discount_percentage").val($("#discount").val());
        }

    </script>
@endsection

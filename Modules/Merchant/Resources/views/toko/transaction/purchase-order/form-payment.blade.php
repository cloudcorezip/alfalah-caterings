<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Kode Transaksi</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{!is_null($data->second_code)?$data->second_code:$data->ap_code}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Waktu Transaksi</label>
                <input type="text" class="form-control form-control-sm trans_time" name="paid_date" placeholder="Waktu Transaksi" value="{{$data->paid_date}}" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1"  class="font-weight-bold">Metode Pembayaran</label>
                <select class="form-control form-control-sm" id="payment" name="trans_coa_detail_id">
                    @foreach($paymentOption as $item)
                        <optgroup label="{{$item['name']}}">
                            @if(count($item['data'])>0)
                                @if($item['name']=='Pembayaran Digital')
                                    @foreach($item['data'] as $d)
                                        <optgroup label="--{{$d['name']}}">
                                            @if(!empty($d['data']))
                                                @foreach($d['data'] as $c)
                                                    <option value="{{$c['acc_coa_id']}}" {{($c['acc_coa_id']==$data->trans_coa_detail_id)?'selected':''}}>{{$c['name']}} </option>

                                                @endforeach
                                            @endif
                                        </optgroup>

                                    @endforeach
                                @else
                                    @foreach($item['data'] as $key =>$other)
                                        <option value="{{$other->acc_coa_id}}" {{($other->acc_coa_id==$data->trans_coa_detail_id)?'selected':''}}>{{$other->name}} @if($item['name']=='Label Pembayaran Lainnya')@endif</option>
                                    @endforeach
                                @endif
                            @endif
                        </optgroup>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Jumlah Pembayaran</label>
                <input type="text" id="paid_nominal" class="form-control form-control-sm amount_currency" min="0" step="0.01" name="paid_nominal" placeholder="Jumlah Pembayaran" value="{{$data->trans_amount}}" required>
            </div>
        </div>

        <div class="col-md-6 admin-fee">
            <div class="form-group admin-fee">
                <label class="font-weight-bold" class="admin-fee">Biaya Administrasi</label>
                <input type="text" name="admin_fee" id="admin_fee" class="form-control admin-fee amount_currency" aria-label="Small" aria-describedby="inputGroup-sizing-sm" value="{{rupiah(0)}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" style="margin-top: 10px" class="font-weight-bold">Bukti Transaksi</label>
                <input type="file" class="form-control form-control-sm"  name="trans_proof">
                <small style="color: red">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</small>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Catatan</label>
                <textarea name="trans_note" class="form-control form-control-sm" cols="5">{{$data->note}}</textarea>
            </div>
        </div>

    </div>


    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='ap_id' value='{{$ap_id }}'>
    <input type="hidden" name="md_merchant_id" value="{{$ap->md_merchant_id}}">

    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        dateTimePicker('input[name=paid_date]')
        $('.amount_currency').mask('#.##0,00', {reverse: true});
        $('#coa-detail').select2();
        $('#payment').select2();
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.save-payment')}}", data,
                function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })

    })


</script>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-3');
        $('.modal-header').prepend(`<div>
                                        <h5 class="modal-title">{{$title}}</h5>
                                                                                <span class="span-text">Untuk menambah pembayaran hutang pembelian, kamu harus mengisi data di bawah ini</span>

                                    </div>`)
    });
</script>

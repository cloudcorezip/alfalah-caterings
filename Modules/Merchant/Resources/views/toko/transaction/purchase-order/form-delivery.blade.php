@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Pembelian</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.purchase-order.data')}}">Penerimaan Pembelian</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="#">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa menambah data penerimaan pembelian baru dengan mengisi kolom di bawah ini.</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <input type="hidden" class="form-control form-control-sm" name="timezone" id="timezone" value="" >

                    <div class="row justify-content-center mb-4">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>false,
'function_onchange'=>''
])
                        <div class="col-md-5">
                            @if(!is_null($data->id))
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Ref. Pemesanan</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-sm" name="ref_code" value="{{(!is_null($data->second_code))?$data->second_code:str_replace('P','PO',$data->code)}}" disabled>
                                    </div>
                                </div>
                            @endif
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Kode Penerimaan</label>
                                    <div class="col-sm-6">
                                        @if($data->step_type==2)
                                            <input type="text" class="form-control form-control-sm" name="second_code" value="{{(!is_null($data->second_code))?'':str_replace('P','PD',$data->code)}}">
                                        @else
                                            <input type="text" class="form-control form-control-sm" name="second_code" value="{{(!is_null($data->second_code))?$data->second_code:str_replace('P','PD',$data->code)}}">
                                        @endif
                                    </div>
                                </div>

                            @if(!is_null($data->id))
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Supplier</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-sm" value="{{$data->getSupplier->name}}" disabled>
                                    </div>
                                </div>
                            @else
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Supplier</label>
                                    <div class="col-sm-6">
                                        <select id="sc_supplier_id" class="form-control form-control-sm" name="sc_supplier_id" required>
                                            <option disabled>--Pilih Supplier--</option>
                                            @foreach ($supplier as $b)
                                                <option @if ($b->id==$data->sc_supplier_id)
                                                        selected
                                                        @endif value="{{$b->id}}">{{$b->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="col-md-5 offset-md-2">
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Pilih Gudang</label>
                                <div class="col-sm-6">
                                    <select id="warehouse" class="form-control form-control-sm" name="inv_warehouse_id"  required>
                                        @foreach ($warehouse as $b)
                                            <option @if ($b->id==$data->inv_warehouse_id)
                                                    selected
                                                    @endif value="{{$b->id}}">{{$b->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Tanggal Penerimaan</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-sm trans_time" name="created_at" value="{{$data->created_at}}" placeholder="Waktu Transaksi" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="25%">Produk</th>
                                    <th width="20%">Harga Beli</th>
                                    <th width="10%">Dipesan</th>
                                    <th width="10%">Diterima</th>
                                    <th width="15%">Satuan</th>
                                    <th width="20%">Sub Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total=0;
                                @endphp
                                @foreach ($data->getDetail->where('is_deleted',0) as $key => $d)
                                    <tr id="row{{$key}}">
                                        <td>
                                            <select class="form-control form-control-sm sc_product_id" name="sc_product_id[]" id="sc_product_id{{$key}}" disabled>
                                                @if(is_null($d->product_name))
                                                    <option value="{{$d->sc_product_id}}">{{$d->getProduct->name}} {{$d->getProduct->code}}</option>
                                                @else
                                                    <option value="{{$d->sc_product_id}}">{{$d->product_name}}</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td class="text-right"><input type="text" name="selling_price[]" class="form-control form-control-sm selling_price text-right" value="{{rupiah($d->price)}}" id="selling_price{{$key}}" required disabled/></td>
                                        <td><input type="text" name="max_quantity[]" class="form-control form-control-sm" value="{{$d->quantity}}" id="max-quantity{{$key}}" disabled /></td>
                                        <td><input type="text" name="quantity[]" class="form-control form-control-sm quantity" value="{{$d->quantity}}" id="quantity{{$key}}" required onkeypress="return isNumber(event)" /></td>
                                        <td>
                                            @if(is_null($d->unit_name))
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{is_null($d->getProduct->getUnit)?'':$d->getProduct->getUnit->name}}">

                                            @else
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{$d->unit_name}}" disabled>

                                            @endif
                                            @if($d->is_multi_unit==0)
                                                <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="1"/>
                                                <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="0"/>
                                                <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control selectalldata" required disabled>
                                                    @if(is_null($d->unit_name))
                                                        @if(!is_null($d->getProduct->getUnit))
                                                            <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                        @endif
                                                    @else
                                                        <option value="0">{{$d->unit_name}}</option>
                                                    @endif
                                                </select>
                                            @else
                                                <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="{{$d->json_multi_unit}}"/>
                                                <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="{{$d->multi_unit_id}}"/>
                                                <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="{{$d->value_conversation}}"/>
                                                <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="1"/>
                                                <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control" required disabled>
                                                    @if(!is_null($d->json_multi_unit))
                                                        @foreach(json_decode($d->json_multi_unit) as $m)
                                                            <option value="{{$m->id}}"
                                                                    @if($m->original_id==$d->multi_unit_id) selected @endif
                                                            >{{$m->text}}</option>
                                                        @endforeach
                                                    @else
                                                        @if(is_null($d->unit_name))
                                                            @if(!is_null($d->getProduct->getUnit))
                                                                <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                            @endif
                                                        @else
                                                            <option value="0">{{$d->unit_name}}</option>
                                                        @endif
                                                    @endif
                                                </select>
                                            @endif


                                        </td>
                                        <td class="text-right">
                                            <input type="text" name="subtotal[]" class="form-control form-control-sm subtotal text-right" value="{{rupiah($d->sub_total)}}" id="subtotal{{$key}}" disabled/>
                                            <input type="hidden" name="subtotal_original[]" class="form-control form-control-sm subtotal_original" value="{{$d->sub_total}}" id="subtotal_original{{$key}}" disabled/>

                                        </td>
                                        <input type="hidden" name="coa_inv_id[]" class="form-control form-control-sm coa_inv_id" value="{{$d->coa_inv_id}}" id="coa_inv_id{{$key}}" disabled/>
                                        <input type="hidden" name="coa_hpp_id[]" class="form-control form-control-sm coa_hpp_id" value="{{$d->coa_hpp_id}}" id="coa_hpp_id{{$key}}" disabled/>
                                        <input type="hidden" name="multi_quantity[]" class="form-control form-control-sm coa_hpp_id" value="{{$d->multi_quantity}}" id="multi_quantity{{$key}}" disabled/>


                                    @if(is_null($d->product_name))
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->getProduct->code}} {{$d->getProduct->code}}" id="product_name{{$key}}" disabled/>

                                        @else
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->product_name}}" id="product_name{{$key}}" disabled/>

                                        @endif
                                    </tr>
                                    <input type="hidden" value="{{$d->id}}" name="sc_purchase_order_detail_id[]">
                                    <input type="hidden" value="{{$d->id}}" name="sc_purchase_order_detail_id[]">
                                    @php
                                        $total+=$d->sub_total;
                                    @endphp
                                @endforeach
                                </tbody>

                            </table>

                            <hr>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="text-note font-weight-bold">Catatan : </label>
                                <textarea name="note" class="form-control" id="" cols="20" rows="5">{{$data->note}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-5 offset-md-2">
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Sub Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" id="total1" name="total1" value="0" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" id="total2" value="0" disabled>

                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Diskon(%)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="promo_percentage" id="promo_percentage" value="{{0+$data->promo_percentage_order}}" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" name="promo" id="promo" value="{{$data->promo_order}}" >
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Pajak(%)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="tax_percentage" id="tax_percentage" value="{{0+$data->tax_percentage_order}}" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" name="tax" id="tax" value="{{$data->tax_order}}" >
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="total" id="total" value="0" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" name="total" id="total3" value="0" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                    <input type='hidden' name='id' value='{{$data->id}}'>
                    <input type='hidden' name='step_type' value='3'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
        $(document).ready(function() {
            sumTotal()
            $("#warehouse").select2();
            $("#timezone").val($('.timezone').val())
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            $("table").on("change", "input", function() {
                var row = $(this).closest("tr");
                var quantity = parseFloat(row.find(".quantity").val());
                var selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal(row.find('.selling_price').val(),'.',''),',','.');
                var subtotal=quantity*selling_price;
                row.find(".subtotal_original").val(subtotal);
                row.find(".subtotal").val(currencyFormat(subtotal,''));
                sumTotal();
            });

            $('#sc_supplier_id').select2().on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_supplier_id-results').append('<a id="add-supplier" onclick="loadModal(this)" target="{{route("merchant.toko.supplier.add")}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Supplier Baru</b></a>');
            });

            $(document).on('click','#add-supplier', function(e){
                $("#sc_supplier_id").select2('close');
            });

        });
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.save-delivery')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })

        function sumTotal(){
            var sum = 0;
            $(".subtotal_original").each(function(){
                sum += +$(this).val();
            });
            if(sum==0)
            {
                $("#save-po").attr("disabled", true);
            }else {
                $("#save-po").attr("disabled", false);

            }
            $("#total1").val(currencyFormat(sum,''));
            $("#total2").val(sum);
            var subtotal=$("#total2").val();
            var discount_percentage=$("#promo_percentage").val();
            var tax_percentage=$("#tax_percentage").val();
            var discount = subtotal*discount_percentage/100;
            var subtotalAfterDiscount=subtotal-discount;
            var tax = subtotalAfterDiscount*tax_percentage/100;
            var price = subtotalAfterDiscount+tax;
            $("#promo").val(discount)
            $("#tax").val(tax)
            $("#total").val(currencyFormat(price,''))
            $("#total3").val(price)
        }
    </script>
@endsection





@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Pembelian</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.purchase-order.data')}}">Faktur Pembelian</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="#">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa menambah data faktur pembelian baru dengan mengisi kolom di bawah ini.</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <input type="hidden" class="form-control form-control-sm" name="timezone" id="timezone" value="" >
                    <input type="hidden" class="form-control form-control-sm" name="paid_nominal" id="paid_nominal" value="0" >
                    <input type="hidden" class="form-control form-control-sm" name="change_nominal" id="change_nominal" value="0" >

                    <div class="row justify-content-center mb-4">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>false,
'function_onchange'=>''
])
                        <div class="col-md-6">
                            @if(!is_null($data->id))
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Ref. Penerimaan</label>
                                    <div class="col-sm-6">
                                        @if($data->step_type==3)
                                            <input type="text" class="form-control form-control-sm" name="ref_code" value="{{(!is_null($data->second_code))?$data->second_code:str_replace('P','PD',$data->code)}}" disabled>
                                        @else
                                            <input type="text" class="form-control form-control-sm" name="ref_code" value="{{(!is_null($data->ref_code))?$data->ref_code:str_replace('P','P',$data->code)}}" disabled>

                                        @endif
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Faktur Pembelian</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-sm" name="second_code" value="">

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Supplier</label>
                                <div class="col-sm-6">
                                    @if (is_null($data->id))
                                        <select id="sc_supplier_id" class="form-control form-control-sm" name="sc_supplier_id" required>
                                            <option disabled>--Pilih Supplier--</option>
                                            @foreach ($supplier as $b)
                                                <option @if ($b->id==$data->sc_supplier_id)
                                                        selected
                                                        @endif value="{{$b->id}}">{{$b->name}}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <label class="col-form-label"><b>{{$data->getSupplier->name}}</b> </label>
                                        <input type="hidden" name="sc_supplier_id" id="" value="{{$data->sc_supplier_id}}">
                                    @endif

                                </div>
                            </div>
                            <div class="form-group row" id="trans-1">
                                <label class="col-sm-6 col-form-label font-weight-bold">Metode Pembayaran</label>
                                <div class="col-sm-6">
                                    <select id="trans" class="form-control form-control-sm" name="md_transaction_type_id" required>
                                        <option value="-1">Pilih Metode Pembayaran</option>
                                        @foreach($transType as $item)
                                            <optgroup label="{{$item['name']}}">
                                                @if(count($item['data'])>0)
                                                    @if($item['name']=='Pembayaran Digital')
                                                        @foreach($item['data'] as $d)
                                                            <optgroup label="--{{$d['name']}}">
                                                                @if(!empty($d['data']))
                                                                    @foreach($d['data'] as $c)
                                                                        <option value="{{$c['trans_id']}}_{{$c['acc_coa_id']}}">{{$c['name']}} </option>

                                                                    @endforeach
                                                                @endif
                                                            </optgroup>

                                                        @endforeach
                                                    @else
                                                        @foreach($item['data'] as $key =>$other)
                                                            <option value="{{$other->trans_id}}_{{$other->acc_coa_id}}_0">{{$other->name}} @if($item['name']=='Label Pembayaran Lainnya')@endif</option>

                                                        @endforeach
                                                    @endif
                                                @endif
                                            </optgroup>
                                        @endforeach

                                    </select>
                                </div>

                            </div>




                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Gudang</label>
                                <div class="col-sm-6">
                                    @if (is_null($data->id))
                                        <select id="warehouse" class="form-control form-control-sm" name="inv_warehouse_id" required>
                                            @foreach ($warehouse as $b)
                                                <option @if ($b->id==$data->inv_warehouse_id)
                                                        selected
                                                        @endif value="{{$b->id}}">{{$b->name}}</option>
                                            @endforeach
                                        </select>
                                    @elseif(!is_null($data->id) && is_null($data->inv_warehouse_id))
                                        <select id="warehouse" class="form-control form-control-sm" name="inv_warehouse_id" required>
                                            @foreach ($warehouse as $b)
                                                <option @if ($b->id==$data->inv_warehouse_id)
                                                        selected
                                                        @endif value="{{$b->id}}">{{$b->name}}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <label class="col-form-label"><b>{{$data->getWarehouse->name}}</b> </label>
                                        <input type="hidden" name="inv_warehouse_id" id="warehouse" value="{{$data->inv_warehouse_id}}">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Tanggal</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-sm trans_time" name="created_at" value="{{$data->created_at}}" placeholder="Waktu Transaksi" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Hutang</label>
                                <div class="col-sm-6">
                                    <select id="is_debet" class="form-control form-control-sm" name="is_debet" required>
                                        <option @if ($data->is_debet==0)
                                                selected
                                                @endif value="0">Tidak</option>
                                        <option @if ($data->is_debet==1)
                                                selected
                                                @endif value="1">Ya</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="tempo">
                                <label class="col-sm-6 col-form-label font-weight-bold">Jatuh Tempo</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-sm trans_time" name="due_date" value="{{date('Y-m-d H:i:s')}}" placeholder="Jatuh Tempo Pembayaran" required>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="30%">Produk</th>
                                    <th>Harga Beli</th>
                                    <th>Jumlah</th>
                                    <th width="15%">Satuan</th>
                                    <th>Sub Total</th>
                                    @if (is_null($data->id))
                                        <th>Opsi</th>
                                    @endif

                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total=0;
                                @endphp
                                @foreach ($data->getDetail->where('is_deleted',0) as $key => $d)
                                    @php
                                    $sumOfRetur=$d->getReturAmount($data->step_type,$d);
                                    @endphp
                                    <tr id="row{{$key}}">
                                        <td>
                                            <select class="form-control form-control-sm sc_product_id" name="sc_product_id[]" id="sc_product_id{{$key}}" disabled>
                                                @if(is_null($d->product_name))
                                                    <option value="{{$d->sc_product_id}}">{{$d->getProduct->name}} {{$d->getProduct->code}}</option>
                                                @else
                                                    <option value="{{$d->sc_product_id}}">{{$d->product_name}}</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td><input type="text" name="selling_price[]" class="form-control form-control-sm selling_price text-right" value="{{rupiah($d->price)}}" id="selling_price{{$key}}" required disabled/></td>
                                        <td><input type="text" name="quantity[]" class="form-control form-control-sm quantity" value="{{$d->quantity-$sumOfRetur}}" id="quantity{{$key}}"  required onkeypress="return isNumber(event)" disabled /></td>

                                        <td>
                                            @if(is_null($d->unit_name))
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{is_null($d->getProduct->getUnit)?'':$d->getProduct->getUnit->name}}">

                                            @else
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{$d->unit_name}}" disabled>

                                            @endif
                                            @if($d->is_multi_unit==0)
                                                <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="1"/>
                                                <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="0"/>
                                                <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control selectalldata" required disabled>
                                                    @if(is_null($d->unit_name))
                                                    @if(!is_null($d->getProduct->getUnit))
                                                        <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                    @endif
                                                   @else
                                                        <option value="0">{{$d->unit_name}}</option>
                                                    @endif
                                                </select>
                                            @else
                                                <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="{{$d->json_multi_unit}}"/>
                                                <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="{{$d->multi_unit_id}}"/>
                                                <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="{{$d->value_conversation}}"/>
                                                <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="1"/>
                                                <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control" required disabled>
                                                    @if(!is_null($d->json_multi_unit))
                                                        @foreach(json_decode($d->json_multi_unit) as $m)
                                                            <option value="{{$m->id}}"
                                                                    @if($m->original_id==$d->multi_unit_id) selected @endif
                                                            >{{$m->text}}</option>
                                                        @endforeach
                                                    @else
                                                        @if(is_null($d->unit_name))
                                                            @if(!is_null($d->getProduct->getUnit))
                                                                <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                            @endif
                                                        @else
                                                            <option value="0">{{$d->unit_name}}</option>
                                                        @endif
                                                    @endif
                                                </select>
                                            @endif


                                        </td>
                                        <td>
                                            <input type="text" name="subtotal[]" class="form-control form-control-sm subtotal text-right" value="{{rupiah($d->sub_total-($sumOfRetur*$d->price))}}" id="subtotal{{$key}}" disabled/>
                                            <input type="hidden" name="subtotal_original[]" class="form-control form-control-sm subtotal_original" value="{{$d->sub_total-($sumOfRetur*$d->price)}}" id="subtotal_original{{$key}}" disabled/>

                                        </td>
                                        <input type="hidden" name="coa_inv_id[]" class="form-control form-control-sm coa_inv_id" value="{{$d->coa_inv_id}}" id="coa_inv_id{{$key}}" disabled/>
                                        <input type="hidden" name="coa_hpp_id[]" class="form-control form-control-sm coa_hpp_id" value="{{$d->coa_hpp_id}}" id="coa_hpp_id{{$key}}" disabled/>

                                        @if(is_null($d->product_name))
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->getProduct->code}} {{$d->getProduct->code}}" id="product_name{{$key}}" disabled/>

                                        @else
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->product_name}}" id="product_name{{$key}}" disabled/>

                                        @endif
                                    </tr>
                                    <input type="hidden" value="{{$d->id}}" name="sc_purchase_order_detail_id[]">

                                    @php
                                        $total+=$d->sub_total;
                                    @endphp
                                @endforeach
                                </tbody>

                            </table>
                            @if (is_null($data->id))
                                <button type="button" name="add" id="add" class="btn btn-darkblue btn-rounded py-2 px-4">
                                    <i class="fa fa-plus mr-2"></i>
                                    <span>Tambah Produk</span>
                                </button>
                            @endif

                            <hr>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="text-note font-weight-bold">Catatan : </label>
                                <textarea name="note" class="form-control" id="" cols="20" rows="5">{{$data->note}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-5 offset-2">
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Sub Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" id="total1" value="0" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" id="total2" name="total1" value="0" disabled>

                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Diskon(%)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="promo_percentage" id="promo_percentage" value="{{0+$data->promo_percentage_delivery}}" onkeypress="return isNumber(event)" required >
                                    <input type="hidden" class="form-control form-control-sm text-right" name="promo" id="promo" value="{{$data->promo_delivery}}" >
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Pajak(%)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="tax_percentage" id="tax_percentage" value="{{0+$data->tax_percentage_delivery}}" onkeypress="return isNumber(event)" required >
                                    <input type="hidden" class="form-control form-control-sm text-right" name="tax" id="tax" value="{{$data->tax_delivery}}" >
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 text-note font-weight-bold">Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" id="total" value="{{rupiah($data->total_delivery)}}" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" name="total" id="total3" value="{{$data->total_delivery}}" disabled>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <input type='hidden' name='step_type' value='0'>
                            <input type='hidden' name='id' value='{{$data->id}}'>
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function sumTotal(){
            var sum = 0;
            $(".subtotal_original").each(function(){
                sum += +$(this).val();
            });
            if(sum==0)
            {
                $("#save-po").attr("disabled", true);
            }else {
                $("#save-po").attr("disabled", false);

            }
            $("#total1").val(currencyFormat(sum,''));
            $("#total2").val(sum);
            var subtotal=$("#total2").val();
            var discount_percentage=$("#promo_percentage").val();
            var tax_percentage=$("#tax_percentage").val();
            var discount = subtotal*discount_percentage/100;
            var subtotalAfterDiscount=subtotal-discount;
            var tax = subtotalAfterDiscount*tax_percentage/100;
            var price = subtotalAfterDiscount+tax;
            $("#promo").val(discount)
            $("#tax").val(tax)
            $("#total").val(currencyFormat(price,''))
            $("#total3").val(price)
        }

        $(document).ready(function() {
            $("#trans").select2()
            $("#branch").select2()

            sumTotal()
            $("#is_debet").select2();
            if($('#is_debet').val()==1){
                $("#tempo").show();
                $("#trans-1").hide();
            }else{
                $("#tempo").hide();
            }

            $('#is_debet').change(function(){
                var debet=$("#is_debet").val();
                if(debet==0)
                {
                    $("#tempo").hide();
                    $("#trans-1").show();
                }else{
                    $("#tempo").show();
                    $("#trans-1").hide();
                }
            });

            $("#timezone").val($('.timezone').val())
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $('#sc_supplier_id').select2().on('select2:open', () => {
                $(".select2-results:not(:has(a))").has('#select2-sc_supplier_id-results').append('<a id="add-supplier" onclick="loadModal(this)" target="{{route("merchant.toko.supplier.add")}}" style="padding: 6px;height: 20px;display: inline-table;">+ Tambah Supplier Baru</a>');
            });

            $(document).on('click','#add-supplier', function(e){
                $("#sc_supplier_id").select2('close');
            });

            $( "#promo_percentage").change(function() {
                sumTotal();
            });
            $( "#tax_percentage").change(function() {
                sumTotal();
            });

        });

        $('#form-konten').submit(function () {
            sumTotal()
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.transaction.purchase-order.save-invoice')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    </script>
@endsection





<div class="form-horizontal">
    <div class="form-group row">
        <label class="col-sm-3 col-xs-12 control-label" style="font-weight: bold">Kode Retur</label>
        <div class="col-sm-8 col-xs-12 form-control-static">
            {{$data->code}}
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-xs-12 control-label" style="font-weight: bold">Alasan Retur</label>
        <div class="col-sm-8 col-xs-12 form-control-static">
            @if ($data->reason_id!=3)
            {{\App\Models\Accounting\ArrayOfAccounting::searchReason($data->reason_id)}}
            @else
            Retur Dengan Mengurangi Piutang
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-xs-12 control-label" style="font-weight: bold">Waktu Retur</label>
        <div class="col-sm-8 col-xs-12 form-control-static">
            {{\Carbon\Carbon::parse($data->created_at)->isoFormat(' D MMMM Y  HH:mm:ss')}}

        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-xs-12 control-label" style="font-weight: bold">Daftar Retur</label>
        <div class="col-sm-8 col-xs-12">
            <table class="table table-bordered table-striped table-hover no-margin">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th>Sub Tutal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data->getDetail as $key =>$item)
                    <tr>
                        <td>{{$key+1}}</td>
                        @if(is_null($item->product_name))
                            <td>{{$item->getProduct->name}} {{$item->getProduct->code}}</td>
                        @else
                            <td>{{$item->product_name}}</td>
                        @endif
                        @if($item->is_multi_unit==1)
                            @if(!is_null($item->unit_name))
                                <td>{{$item->quantity}}</td>

                                <td>{{$item->unit_name}}</td>
                            @else
                                <td>{{$item->multi_quantity}}</td>
                                <td>{{is_null($item->getProduct->getUnit)?'':$item->getProduct->getUnit->name}}</td>
                            @endif
                        @else
                            <td>{{$item->quantity}}</td>
                            @if(!is_null($item->unit_name))
                                <td>{{$item->unit_name}}</td>
                            @else
                                <td>{{is_null($item->getProduct->getUnit)?'':$item->getProduct->getUnit->name}}</td>
                            @endif
                        @endif

                         <td>{{rupiah($item->sub_total)}}</td>
                    </tr>

                @endforeach
                <tr>
                    <td colspan="4" style="font-weight: bold">Total</td>
                    <td>{{rupiah($data->getDetail->sum('sub_total'))}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-xs-12 control-label" style="font-weight: bold">Catatan</label>
        <div class="col-sm-8 col-xs-12 form-control-static">
            {{$data->note}}
        </div>
    </div>
</div>

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Kode Transaksi</label>
                <input type="text" class="form-control form-control-sm" name="code" placeholder="Kode Transaksi" value="{{!is_null($data->second_code)?$data->second_code:$data->ap_code}}">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" style="margin-top: 10px;font-weight: bold">Waktu Transaksi</label>
                <input type="text" class="form-control form-control-sm trans_time" name="paid_date" placeholder="Waktu Transaksi" value="{{$data->paid_date}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Metode Pembayaran</label>
                <select class="form-control form-control-sm" id="payment" name="trans_coa_detail_id">
                    @foreach($paymentOption as $item)
                        <optgroup label="{{$item['name']}}">
                            @if(count($item['data'])>0)
                                @if($item['name']=='Pembayaran Digital')
                                    @foreach($item['data'] as $d)
                                        <optgroup label="{{$d['name']}}">
                                            @if(!empty($d['data']))
                                                @foreach($d['data'] as $c)
                                                    <option value="{{$c['trans_id']}}_{{$c['acc_coa_id']}}_1_{{$c['alias']}}_{{$c['id']}}">{{$c['name']}} </option>

                                                @endforeach
                                            @endif
                                        </optgroup>
                                    @endforeach
                                @else
                                    @foreach($item['data'] as $key =>$other)
                                        <option value="{{$other->trans_id}}_{{$other->acc_coa_id}}_0">{{$other->name}}</option>
                                    @endforeach
                                @endif
                            @endif
                        </optgroup>
                    @endforeach

                </select>
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword" class="font-weight-bold">Jumlah Pembayaran</label>
                <input type="text" id="paid_nominal" class="form-control form-control-sm" min="0" step="0.01" name="paid_nominal" placeholder="Jumlah Pembayaran" value="{{$data->trans_amount}}" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Bukti Transaksi</label>
                <input type="file" class="form-control form-control-sm"  name="trans_proof">
                <small style="color: red">File yang diperbolehkan : jpg, jpeg, png, docx, xlsx, pdf, zip</small>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Catatan</label>
                <textarea name="trans_note" class="form-control form-control-sm" cols="5">{{$data->note}}</textarea>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='ar_id' value='{{$ar_id }}'>
    <input type="hidden" name="md_merchant_id" value="{{$ap->md_merchant_id}}">
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#paid_nominal').mask('#.##0,00', {reverse: true});
        dateTimePicker('input[name=paid_date]')
        $('#coa-detail').select2();
        $('#payment').select2({
            templateResult: formatState,
            templateSelection: formatState
        });
        function formatState (state) {
            if (!state.id) {
                return state.text;
            }
            var id = state.text
            var baseUrl;
            var $state;


            if(id.search(/QRIS|Virtual Account|E-Wallet/)==0){
                if(id.search(/QRIS|Virtual Account/)==0){
                    baseUrl="{{env('S3_URL')}}"+"public/transactionType"
                }else{
                    baseUrl="{{env('S3_URL')}}"+"public/payment-icon"

                }
                var name = state.text.replace(/Virtual Account - |E-Wallet - /,'')

                if(name=='QRIS (Hanya Label)'){
                    $state = $(
                        '<span>' + state.text + '</span>'
                    );
                }else{
                    $state = $(
                        '<span><img src="' + baseUrl + '/' + name.replace(' ','').toLowerCase() + '.png" class="img-flag" style="width: 40px" /> ' + state.text + '</span>'
                    );
                }

            }else{
                $state = $(
                    '<span>' + state.text + '</span>'
                );
            }

            return $state;
        }

        $("#payment").on("change", function(){
            let value = $(this).val();
            if(value.indexOf('ID_OVO') == -1){
                $("#col-mobile-number").remove();
            } else {
                let html = `<div class="col-md-12" id="col-mobile-number">
                                <div class="form-group">
                                    <div class="d-flex align-items-center" style="width:100%;">
                                        <input id="code_country" type="text" class="form-control" name="code_country" value="+62" disabled style="width:16%;">
                                        <input id="mobile_number" type="text" name="mobile_number" class="form-control" placeholder="No. Handphone OVO" style="width:84%;">
                                    </div>
                                    <span id="mobile_number_validate" class="text-danger d-none">No Handphone tidak boleh kosong !</span>
                                </div>
                            </div>`;
                $(this).closest('.col-md-12').after(html);
            }
        });

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            var paymentMethod = $("#payment").val();

            if(paymentMethod.indexOf('ID_OVO') != -1){
                var phoneNumberVal = $("#mobile_number").val();
                var codeCountry = $("#code_country").val();
                var phoneNum = codeCountry + phoneNumberVal;
                if(!/^(\+62)([2-9])\d(\d)?[2-9](\d){6,7}$/.test(phoneNum)){
                    toastForSaveData(
                        "No Handphone yang anda masukkan tidak valid !",
                        "warning",
                        "false",
                        ""
                    );
                    return;
                } else {
                    data.append("mobile_number", phoneNum);
                }
            }

            ajaxTransfer("{{route('merchant.toko.transaction.sale-order.save-payment')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    })


</script>

<script>
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-3');
        $('.modal-header').prepend(`<div>
                                        <h5 class="modal-title">{{$title}}</h5>
                                                                                                                        <span class="span-text">Untuk menambah pembayaran piutang penjualan, kamu harus mengisi data di bawah ini</span>

                                    </div>`)
    });
</script>

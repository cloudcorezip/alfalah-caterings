@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Penjualan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.sale-order.data')}}">Faktur Penjualan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa menambah data faktur penjualan seperti detail produk, nama pelanggan, metode pembayaran hingga pengiriman produk</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <input type="hidden" class="form-control form-control-sm" name="timezone" id="timezone" value="">
                    <input type="hidden" class="form-control form-control-sm" name="paid_nominal" id="paid_nominal" value="0">
                    <input type="hidden" class="form-control form-control-sm" name="change_nominal" id="change_nominal" value="0" >
                    <div class="row justify-content-center mb-4">

                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>false,
'function_onchange'=>''
])
                        <div class="col-md-6">
                            @if(!is_null($data->id))
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Ref. Pengiriman</label>
                                    <div class="col-sm-6">
                                        @if($data->step_type==3)
                                            <input type="text" class="form-control form-control-sm" name="ref_code" value="{{(!is_null($data->second_code))?$data->second_code:str_replace('S','SD',$data->code)}}" disabled>
                                        @else
                                            <input type="text" class="form-control form-control-sm" name="ref_code" value="{{(!is_null($data->ref_code))?$data->ref_code:str_replace('S','S',$data->code)}}" disabled>

                                        @endif
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Faktur Penjualan</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-sm" name="second_code" value="">

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Pelanggan</label>
                                <div class="col-sm-6">
                                    <select id="sc_customer_id" class="form-control form-control-sm" name="sc_customer_id" @if(!is_null($data->id)) disabled @endif>
                                        <option value="-1">--Pilih Pelanggan--</option>
                                        @if(!is_null($data->id))
                                            <option selected value="{{$data->sc_customer_id}}">{{$data->getCustomer->code}} {{$data->getCustomer->name}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="transs">
                                <label class="col-sm-6 col-form-label font-weight-bold">Metode Pembayaran</label>
                                <div class="col-sm-6">
                                    <select id="trans" class="form-control form-control-sm" name="md_transaction_type_id" required>
                                        <option value="-1">Pilih Metode Pembayaran</option>
                                        @foreach($transType as $item)
                                            <optgroup label="{{$item['name']}}">
                                                @if(count($item['data'])>0)
                                                    @if($item['name']=='Pembayaran Digital')
                                                        @foreach($item['data'] as $d)
                                                            <optgroup label="--{{$d['name']}}">
                                                                @if(!empty($d['data']))
                                                                    @foreach($d['data'] as $c)
                                                                        <option value="{{$c['trans_id']}}_{{$c['acc_coa_id']}}_1_{{$c['alias']}}_{{$c['id']}}">{{$c['name']}} </option>

                                                                    @endforeach
                                                                @endif
                                                            </optgroup>
                                                        @endforeach
                                                    @else
                                                        @foreach($item['data'] as $key =>$other)
                                                            <option value="{{$other->trans_id}}_{{$other->acc_coa_id}}_0">{{$other->name}} @if($item['name']=='Label Pembayaran Lainnya')(Hanya Label)@endif</option>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </optgroup>
                                        @endforeach


                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Metode Pengiriman</label>
                                <div class="col-sm-6">
                                    <select id="trans2" class="form-control form-control-sm select22" name="md_sc_shipping_category_id" disabled>
                                        <option value="-1">--Pilih Metode Pengiriman--</option>
                                        @foreach($shippingMethod as $item)
                                            <optgroup label="{{$item['name']}}">
                                                @if($item['is_group']==0)

                                                    <option value="{{$item['data']->id}}_{{$item['data']->key}}" {{($data->md_sc_shipping_category_id==$item['data']->id)?'selected':''}}>{{$item['data']->name}}</option>

                                                @else
                                                    @foreach($item['data'] as $d)
                                                        <option value="{{$d->id}}_{{$d->key}}" {{($data->md_sc_shipping_category_id==$d->id)?'selected':''}}>{{$d->name}}</option>

                                                    @endforeach
                                                @endif

                                            </optgroup>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Tanggal</label>
                                <div class="col-sm-6">
                                    <input type="text" id="trans_time" class="form-control form-control-sm trans_time" name="created_at" value="{{$data->created_at}}" placeholder="Waktu Transaksi" @if(!is_null($data->id)) disabled @endif required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Pilih Karyawan</label>
                                <div class="col-sm-6">
                                    <select id="staff_id" class="form-control form-control-sm" name="assign_to_user_helper" multiple>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="ar">
                                <label class="col-sm-6 col-form-label font-weight-bold">Hutang</label>
                                <div class="col-sm-6">
                                    <select id="is_debet" class="form-control form-control-sm" name="is_debet" required>
                                        <option @if ($data->is_debet==0)
                                                selected
                                                @endif value="0">Tidak</option>
                                        <option @if ($data->is_debet==1)
                                                selected
                                                @endif value="1">Ya</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="tempo">
                                <label class="col-sm-6 col-form-label font-weight-bold">Tgl Jatuh Tempo</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-sm trans_time" name="due_date" value="{{date('Y-m-d H:i:s')}}" placeholder="Jatuh Tempo Pembayaran" required>
                                </div>
                            </div>

                        </div>

                    </div>


                    <div class="row mb-3">
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="25%">Produk</th>
                                    <th>Harga Jual</th>
                                    <th>Jumlah</th>
                                    <th width="15%">Satuan</th>
                                    <th width="20%">Sub Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total=0;
                                @endphp
                                @foreach ($data->getDetail->where('is_deleted',0) as $key => $d)
                                    @php
                                        $sumOfRetur=$d->getReturAmount($data->step_type,$d);
                                    @endphp
                                    <tr id="row{{$key}}">
                                        <td>
                                            <select class="form-control form-control-sm sc_product_id" name="sc_product_id[]" id="sc_product_id{{$key}}" disabled>
                                                @if(is_null($d->product_name))
                                                    <option value="{{$d->sc_product_id}}">{{$d->getProduct->name}} {{$d->getProduct->code}}</option>
                                                @else
                                                    <option value="{{$d->sc_product_id}}">{{$d->product_name}}</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td><input type="text" name="selling_price[]" class="form-control form-control-sm selling_price text-right" value="{{rupiah($d->price)}}" id="selling_price{{$key}}" disabled/></td>
                                        <td><input type="text" name="quantity[]" class="form-control form-control-sm quantity text-center" value="{{$d->quantity-$sumOfRetur}}" id="quantity{{$key}}" disabled/></td>
                                        <td>
                                            @if(is_null($d->unit_name))
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{is_null($d->getProduct->getUnit)?'':$d->getProduct->getUnit->name}}" disabled>

                                            @else
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{$d->unit_name}}" disabled>

                                            @endif
                                                @if($d->is_multi_unit==0)
                                                    <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                    <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                    <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="1"/>
                                                    <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="0"/>
                                                    <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control sc_product_id" required disabled>
                                                        @if(is_null($d->unit_name))
                                                            @if(!is_null($d->getProduct->getUnit))
                                                                <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                            @endif
                                                        @else
                                                            <option value="0">{{$d->unit_name}}</option>
                                                        @endif
                                                    </select>
                                                @else
                                                    <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="{{$d->json_multi_unit}}"/>
                                                    <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="{{$d->multi_unit_id}}"/>
                                                    <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="{{$d->value_conversation}}"/>
                                                    <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="1"/>
                                                    <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control sc_product_id" required disabled>
                                                        @if(!is_null($d->json_multi_unit))
                                                            @foreach(json_decode($d->json_multi_unit) as $m)
                                                                <option value="{{$m->id}}"
                                                                        @if($m->original_id==$d->multi_unit_id) selected @endif
                                                                >{{$m->text}}</option>
                                                            @endforeach
                                                        @else
                                                            @if(is_null($d->unit_name))
                                                                @if(!is_null($d->getProduct->getUnit))
                                                                    <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                                @endif
                                                            @else
                                                                <option value="0">{{$d->unit_name}}</option>
                                                            @endif
                                                        @endif
                                                    </select>
                                                @endif
                                        </td>

                                        <td><input type="text" name="subtotal[]" class="form-control form-control-sm subtotal text-right" value="{{rupiah($d->sub_total-($sumOfRetur*$d->price))}}" id="subtotal" disabled/>
                                            <input type="hidden" name="subtotal_original[]" class="form-control form-control-sm subtotal_original" value="{{$d->sub_total-($sumOfRetur*$d->price)}}" id="subtotal_original" disabled/>
                                        </td>
                                        <input type="hidden" name="coa_inv_id[]" class="form-control form-control-sm coa_inv_id" value="{{$d->coa_inv_id}}" id="coa_inv_id" disabled/>
                                        <input type="hidden" name="coa_hpp_id[]" class="form-control form-control-sm coa_hpp_id" value="{{$d->coa_hpp_id}}" id="coa_hpp_id" disabled/>
                                        @if(is_null($d->product_name))
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->getProduct->code}} {{$d->getProduct->code}}" id="product_name" disabled/>

                                        @else
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->product_name}}" id="product_name" disabled/>

                                        @endif
                                    </tr>
                                    <input type="hidden" value="{{$d->id}}" name="sc_sale_order_detail_id[]">
                                    @php
                                        $total+=$d->sub_total;
                                    @endphp
                                @endforeach
                                </tbody>

                            </table>

                            <hr>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan :</label>
                                <textarea name="note" class="form-control form-control-sm" id="" cols="20" rows="5">{{$data->note_order}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-5 offset-md-2">
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Sub Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" id="total1" value="{{rupiah($data->getDetail->where('is_deleted',0)->sum('sub_total'))}}" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" id="total2" name="total1" value="{{$data->getDetail->where('is_deleted',0)->sum('sub_total')}}" disabled>

                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Diskon(<span id="unit-diskon">%</span>)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="discount_percentage" id="discount_percentage" onkeypress="return isNumber(event)" required  min="0" max="100" value="{{0+$data->promo_percentage_delivery}}">
                                    <input type="hidden" class="form-control form-control-sm" name="discount" id="discount" value="{{$data->promo_delivery}}" >
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Pajak(%)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="tax_percentage" id="tax_percentage" onkeypress="return isNumber(event)" required  min="0" max="100" value="{{0+$data->tax_percentage_delivery}}">
                                    <input type="hidden" class="form-control form-control-sm" name="tax" id="tax" value="{{$data->tax_delivery}}" >
                                </div>
                            </div>
                            {{-- @dd($data->md_sc_shipping_category_id) --}}
                            @if (!in_array($data->md_sc_shipping_category_id,[3,27]))
                                <div class="form-group row align-items-center" id="pengiriman">
                                    <label class="col-sm-4 col-form-label text-note font-weight-bold">Biaya Pengiriman</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm text-right" id="shipping_cost" value="{{rupiah(0+$data->shipping_cost)}}" step="0.01" disabled>
                                        <input type="hidden" class="form-control form-control-sm text-right" name="shipping_cost" id="shipping_cost2" value="{{0+$data->shipping_cost}}" step="0.01" disabled>

                                    </div>
                                </div>
                            @else
                                <input type="hidden" class="form-control form-control-sm text-right" id="shipping_cost" value="{{rupiah(0+$data->shipping_cost)}}" step="0.01" disabled>
                                <input type="hidden" class="form-control form-control-sm text-right" name="shipping_cost" id="shipping_cost2" value="{{0+$data->shipping_cost}}" step="0.01" disabled>

                            @endif
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right"  id="total" value="{{rupiah($data->total_delivery)}}" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" name="total" id="total3" value="{{$data->total_delivery}}" disabled>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <input type='hidden' name='id' value='{{$data->id}}'>
                            <input type='hidden' name='is_with_load_shipping' value='false' id="beban-pengiriman-1">
                            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                            <input type="hidden" name="step_type" value="0">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>


                </form>
            </div>
        </div>

    </div>


@endsection

@section('js')
    <script>

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function sumTotal(){
            var sum = 0;
            $(".subtotal_original").each(function(){
                sum += +$(this).val();
            });
            if(sum==0)
            {
                $("#save-po").attr("disabled", true);
            }else {
                $("#save-po").attr("disabled", false);

            }
            $("#total1").val(currencyFormat(sum,''));
            $("#total2").val(sum);
            var subtotal=$("#total2").val();
            var discount_percentage=$("#discount_percentage").val();
            var tax_percentage=$("#tax_percentage").val();
            var shipping_cost = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#shipping_cost").val(),'.',''),',','.');

            if($('#unit-diskon').text()=='%'){
                var discount = subtotal*discount_percentage/100;
            }else{
                var discount = discount_percentage;
            }

            var price = (subtotal-discount);
            var tax = price*tax_percentage/100;
            var total=price+tax+parseFloat(shipping_cost);

            var total_with_shipping=total

            $("#tax").val(tax)
            $("#discount").val(discount)
            $("#shipping_cost2").val(shipping_cost)
            $("#total").val(currencyFormat(total_with_shipping))
            $("#total3").val(total_with_shipping)
        }


        $(document).ready(function() {
            $('#shipping_cost').mask('#.##0,00', {reverse: true});
            sumTotal()
            getCustomer()
            getStaff()
            var i=0;
            $('.select22').select2();
            $('#is_debet').select2();


            var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
            var d = new Date();
            $("#tempo").hide();
            // $("#pengiriman").hide();
            $("#timezone").val($('.timezone').val())

            var codes = [];


            $('#is_debet').change(function(){
                var debet=$("#is_debet").val();
                if(debet==0)
                {
                    $("#tempo").hide();
                    $("#transs").show();
                }else{
                    $("#tempo").show();
                    $("#transs").hide();
                    $("#trans").val("-1");

                }
            });


            $('#trans').change(function(){

                var payment = $("#trans").val().split("_")

                if(payment[2]==1){

                    $("#is_debet").val(0)
                    $("#ar").hide()

                }else{
                    $("#is_debet").val(0)
                    $("#ar").show()
                }

            });

            $('#trans2').change(function(){
                var debet=$("#trans2").val();
                if(debet==1)
                {
                    $("#pengiriman").show();
                }else{
                    $("#pengiriman").hide();

                }
            });


            $('.sc_product_id').select2();

            function formatState (state) {
                if (!state.id) {
                    return state.text;
                }
                var id = state.text
                var baseUrl;
                var $state;


                if(id.search(/QRIS|Virtual Account|E-Wallet/)==0){
                    if(id.search(/QRIS|Virtual Account/)==0){
                        baseUrl="{{env('S3_URL')}}"+"public/transactionType"
                    }else{
                        baseUrl="{{env('S3_URL')}}"+"public/payment-icon"

                    }
                    var name = state.text.replace(/Virtual Account - |E-Wallet - /,'')

                    if(name=='QRIS (Hanya Label)'){
                        $state = $(
                            '<span>' + state.text + '</span>'
                        );
                    }else{
                        $state = $(
                            '<span><img src="' + baseUrl + '/' + name.replace(' ','').toLowerCase() + '.png" class="img-flag" style="width: 40px" /> ' + state.text + '</span>'
                        );
                    }

                }else{
                    $state = $(
                        '<span>' + state.text + '</span>'
                    );
                }

                return $state;
            }

            $('#trans').select2({
                templateResult: formatState,
                templateSelection: formatState

            })
            $('#trans2').select2();

            $( "#discount_percentage").change(function() {
                sumTotal();
            });
            $( "#tax_percentage").change(function() {
                sumTotal();
            });

        });


        $('#form-konten').submit(function () {
            sumTotal()
            var data = getFormData('form-konten');

            var paymentMethod = $("#trans").val();

            let staffData = ($("#staff_id").select2('data'))? $("#staff_id").select2('data'):[];
            let staffList = [];
            let staffIds=[];
            staffData.forEach(item => {
                staffList.push({
                    'id':parseInt(item.id),
                    'fullname':item.text
                });
                staffIds.push(`'${item.id.trim()}'`);

            });

            let staffId = staffIds.join(',');

            data.append('user_helpers',JSON.stringify(staffList))
            data.append('user_helper_ids',staffId)

            if(paymentMethod.indexOf('ID_OVO') == -1){
                ajaxTransfer("{{route('merchant.toko.transaction.sale-order.save-invoice')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            } else {
                var html = `<form class="form-konten">
                                <div class="form-group">
                                    <div class="d-flex align-items-center" style="width:100%;">
                                        <input id="code_country" type="text" class="form-control" name="code_country" value="+62" disabled style="width:16%;">
                                        <input id="mobile_number" type="text" name="mobile_number" class="form-control" placeholder="No. Handphone OVO" style="width:76%;">
                                    </div>
                                    <span id="mobile_number_validate" class="text-danger d-none">No Handphone tidak boleh kosong !</span>
                                </div>
                            </form>`;

                modalConfirm("Masukkan No. Handphone OVO", html, function(){
                    var phoneNumberVal = $("#mobile_number").val();
                    var codeCountry = $("#code_country").val();
                    if(!phoneNumberVal){
                        $("#mobile_number_validate").removeClass("d-none").text("No Handphone tidak boleh kosong !");
                        return;
                    } else {
                        var phoneNum = codeCountry + phoneNumberVal;
                        if(!/^(\+62)([2-9])\d(\d)?[2-9](\d){6,7}$/.test(phoneNum)){
                            $("#mobile_number_validate").removeClass("d-none").text("No Handphone tidak valid !");
                            return;
                        } else {
                            $("#mobile_number_validate").addClass("d-none");
                            data.append("mobile_number", phoneNum);
                            ajaxTransfer("{{route('merchant.toko.transaction.sale-order.save-invoice')}}", data, function(response){
                                var data = JSON.parse(response);
                                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                            });
                        }

                    }

                });
            }


        })
    </script>
    <script>
        function getCustomer()
        {
            $("#sc_customer_id").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.transaction.sale-order.customer-list')}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

        }

        function getStaff()
        {
            $("#staff_id").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.staff.ajax-list')}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

        }

        function hapusDetail(id) {
            var data = new FormData();
            data.append('id', id);
            modalConfirm("Konfirmasi", "<div class='alert alert-warning'>Perhatian,penghapusan data item penjualan akan merubah nilai persedian.Pastikan setelah menghapus data anda melakukan penyimpanan dan mengisi nilai penjualan baru dengan benar!</div>", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.sale-order.delete-detail')}}", data, "#modal-output");
            })
        }
    </script>
@endsection

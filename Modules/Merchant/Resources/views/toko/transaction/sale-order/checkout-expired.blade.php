@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .bank-logo {
            width:200px;
            height:80px;
            object-fit:contain;
        }

        .btn:focus,.btn:active {
            outline: none !important;
            box-shadow: none;
        }
        .borderless table {
            border-top-style: none;
            border-left-style: none;
            border-right-style: none;
            border-bottom-style: none;
        }
        @media screen and (max-width:576px){
            .bank-logo {
                width: 120px!important;
                height: 80px;
            }
        }
    </style>
    <div class="container" id="container-invoice">
        <div class="row">
            <div class="col-md-12">

                <div class="card" style="border-radius:10px;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div
                                    class="mb-3 d-flex justify-content-between align-items-center"
                                    style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                >
                                    <div>
                                        <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                        <span style="color:#4F4F4F;font-size:16px;font-weight:bold">

                                             Pembayaran Transaksi {{$data->code}} </span>
                                    </div>
                                    @if($type='order')
                                    <a href="{{route('merchant.toko.sale-order.detail', ['id' => $data->id])}}" class="btn btn-outline-light btn-xs text-light btn-rounded">
                                        Kembali
                                    </a>
                                        @else
                                        <a href="{{route('merchant.toko.sale-order.detail', ['id' => $data->getAr->arable_id,'page'=>'pembayaran'])}}" class="btn btn-outline-light btn-xs text-light btn-rounded">
                                            Kembali
                                        </a>
                                    @endif
                                </div>
                                <div class="collapse mb-3" id="virtual-account">
                                    <span>{{$data->code}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @foreach($paymentOption as $item)
                                @if(count($item['data'])>0)
                                    <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                        <div class="card-body">
                                            <div>
                                                <button
                                                    class="mb-3 d-flex justify-content-between align-items-center"
                                                    type="button"
                                                    data-toggle="collapse"
                                                    data-target="#{{str_replace(' ','',$item['name'])}}"
                                                    aria-expanded="false"
                                                    aria-controls="virtual-account"
                                                    style="background:transparent!important;border:none!important;outline:none!important; width:100%;"
                                                >
                                                    <div>
                                                        <i style="color:#FF923A;font-size:16px;" class="fas fa-university mr-2"></i>
                                                        <span style="color:#4F4F4F;font-size:16px;">{{$item['name']}}</span>
                                                    </div>
                                                    <i class="fas fa-angle-down" style="font-size:16px;"></i>
                                                </button>
                                                <div class="collapse show" id="{{str_replace(' ','',$item['name'])}}">
                                                    <div class="row" id="virtual-account-row">
                                                        @if($item['name']=='Pembayaran Digital')
                                                            @foreach($item['data'] as $d)
                                                                @if(!empty($d['data']))
                                                                    @foreach($d['data'] as $c)
                                                                        <div class="col-md-6 col-sm-12 card-bank">
                                                                            <a href="#">
                                                                                <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                                                    <div class="card-body text-center">
                                                                                        <img src="{{env('S3_URL')}}{{$c['icon']}}" alt="{{$c['icon']}}" class="bank-logo">
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                        </div>
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @foreach($item['data'] as $key =>$other)
                                                                <div class="col-md-6 col-sm-12 card-bank">
                                                                    <a href="#">
                                                                        <div class="card" style="box-shadow:none!important;border: 1px solid #D2D2D2;border-radius:10px;">
                                                                            <div class="card-body text-center">
                                                                                <h4>{{$other->name}}</h4>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection


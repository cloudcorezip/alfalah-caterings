@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Penjualan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.sale-order.data')}}">Pengiriman Penjualan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa menambah data pengiriman penjualan baru dengan mengisi kolom di bawah ini.</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <input type="hidden" class="form-control form-control-sm" name="timezone" id="timezone" value="" >

                    <div class="row justify-content-center mb-4">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>false,
'function_onchange'=>''
])
                        <div class="col-md-6">

                            @if(!is_null($data->id))
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Ref. Pemesanan</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-sm" name="ref_code" value="{{(!is_null($data->second_code))?$data->second_code:str_replace('S','SO',$data->code)}}" disabled>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Kode Pengiriman</label>
                                <div class="col-sm-6">
                                    @if($data->step_type==2)
                                        <input type="text" class="form-control form-control-sm" name="second_code" value="{{(!is_null($data->second_code))?'':str_replace('S','SD',$data->code)}}">
                                    @else
                                        <input type="text" class="form-control form-control-sm" name="second_code" value="{{(!is_null($data->second_code))?$data->second_code:str_replace('S','SD',$data->code)}}">
                                    @endif
                                </div>
                            </div>

                            @if(!is_null($data->id))
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Pelanggan</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-sm" value="{{$data->getCustomer->name}}" disabled>
                                    </div>
                                </div>
                            @else
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Pelanggan</label>
                                    <div class="col-sm-6">
                                        <select id="sc_customer_id" class="form-control form-control-sm" name="sc_customer_id" required>
                                            <option disabled>Pilih Pelanggan</option>
                                            @if(!is_null($data->id))
                                                <option selected value="{{$data->sc_customer_id}}">{{$data->getCustomer->code}} {{$data->getCustomer->name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Metode Pengiriman</label>
                                <div class="col-sm-6">
                                    <select id="trans2" class="form-control form-control-sm" name="md_sc_shipping_category_id" required>
                                        <option value="-1">Pilih Metode Pengiriman</option>
                                        @foreach($shippingMethod as $item)
                                            <optgroup label="{{$item['name']}}">
                                                @if($item['is_group']==0)

                                                    <option value="{{$item['data']->id}}_{{$item['data']->key}}">{{$item['data']->name}}</option>

                                                @else
                                                    @foreach($item['data'] as $d)
                                                        <option value="{{$d->id}}_{{$d->key}}">{{$d->name}}</option>

                                                    @endforeach
                                                @endif

                                            </optgroup>
                                        @endforeach

                                    </select>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Tanggal Pengiriman</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-sm trans_time" name="created_at" value="{{$data->created_at}}" placeholder="Waktu Transaksi" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="25%">Produk</th>
                                    <th width="16%">Harga Jual</th>
                                    <th>DiPesan</th>
                                    <th>Dikirim</th>
                                    <th width="15%">Satuan</th>
                                    <th width="20%">Sub Total</th>

                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total=0;
                                @endphp
                                @foreach ($data->getDetail->where('is_deleted',0) as $key => $d)
                                    <tr id="row{{$key}}">
                                        <td>
                                            <select class="form-control form-control-sm sc_product_id text-center" name="sc_product_id[]" id="sc_product_id{{$key}}" disabled>
                                                @if(is_null($d->product_name))
                                                    <option value="{{$d->sc_product_id}}">{{$d->getProduct->name}} {{$d->getProduct->code}}</option>
                                                @else
                                                    <option value="{{$d->sc_product_id}}">{{$d->product_name}}</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td><input type="text" name="selling_price[]" class="form-control form-control-sm selling_price text-right" value="{{rupiah($d->price)}}" id="selling_price{{$key}}" disabled/></td>
                                        <td><input type="text" name="max_quantity[]" class="form-control form-control-sm" value="{{$d->quantity}}" id="quantity{{$key}}" disabled /></td>
                                        <td><input type="text" name="quantity[]" class="form-control form-control-sm quantity" value="{{$d->quantity}}" id="quantity{{$key}}" /></td>

                                        <td>
                                            @if(is_null($d->unit_name))
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{is_null($d->getProduct->getUnit)?'':$d->getProduct->getUnit->name}}" disabled>

                                            @else
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{$d->unit_name}}" disabled>

                                            @endif
                                                @if($d->is_multi_unit==0)
                                                    <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                    <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                    <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="1"/>
                                                    <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="0"/>
                                                    <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control sc_product_id" required disabled>
                                                        @if(is_null($d->unit_name))
                                                            @if(!is_null($d->getProduct->getUnit))
                                                                <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                            @endif
                                                        @else
                                                            <option value="0">{{$d->unit_name}}</option>
                                                        @endif
                                                    </select>
                                                @else
                                                    <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="{{$d->json_multi_unit}}"/>
                                                    <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="{{$d->multi_unit_id}}"/>
                                                    <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="{{$d->value_conversation}}"/>
                                                    <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="1"/>
                                                    <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control sc_product_id" required disabled>
                                                        @if(!is_null($d->json_multi_unit))
                                                            @foreach(json_decode($d->json_multi_unit) as $m)
                                                                <option value="{{$m->id}}"
                                                                        @if($m->original_id==$d->multi_unit_id) selected @endif
                                                                >{{$m->text}}</option>
                                                            @endforeach
                                                        @else
                                                            @if(is_null($d->unit_name))
                                                                @if(!is_null($d->getProduct->getUnit))
                                                                    <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                                @endif
                                                            @else
                                                                <option value="0">{{$d->unit_name}}</option>
                                                            @endif
                                                        @endif
                                                    </select>
                                                @endif
                                        </td>

                                        <td><input type="text" name="subtotal[]" class="form-control form-control-sm subtotal text-right" value="{{rupiah($d->sub_total)}}" id="subtotal{{$key}}" disabled/>
                                            <input type="hidden" name="subtotal_original[]" class="form-control form-control-sm subtotal_original" value="{{$d->sub_total}}" id="subtotal_original{{$key}}" disabled/>

                                        </td>
                                        <input type="hidden" name="coa_inv_id[]" class="form-control form-control-sm coa_inv_id" value="{{$d->coa_inv_id}}" id="coa_inv_id{{$key}}" disabled/>
                                        <input type="hidden" name="coa_hpp_id[]" class="form-control form-control-sm coa_hpp_id" value="{{$d->coa_hpp_id}}" id="coa_hpp_id{{$key}}" disabled/>
                                        <input type="hidden" name="multi_quantity[]" class="form-control form-control-sm coa_hpp_id" value="{{$d->multi_quantity}}" id="multi_quantity{{$key}}" disabled/>
                                    @if(is_null($d->product_name))
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->getProduct->code}} {{$d->getProduct->code}}" id="product_name{{$key}}" disabled/>

                                        @else
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->product_name}}" id="product_name{{$key}}" disabled/>

                                        @endif
                                    </tr>
                                    <input type="hidden" value="{{$d->id}}" name="sc_sale_order_detail_id[]">
                                    @php
                                        $total+=$d->sub_total;
                                    @endphp
                                @endforeach
                                </tbody>

                            </table>

                            <hr>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="text-note font-weight-bold">Catatan : </label>
                                <textarea name="note" class="form-control" id="" cols="20" rows="5">{{$data->note_order}}</textarea>
                            </div>
                        </div>

                        <div class="col-md-5 offset-md-2">
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Sub Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" id="total1" value="0" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" id="total2" name="total1" value="0" disabled>

                                </div>
                            </div>
                            <div class="form-group row align-items-center" id="pengiriman">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Biaya Pengiriman</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right"  id="shipping_cost" value="{{0+$data->shipping_cost}}" step="0.01" >
                                    <input type="hidden" class="form-control form-control-sm text-right" name="shipping_cost" id="shipping_cost2" value="{{0+$data->shipping_cost}}" step="0.01" >

                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Diskon(<span id="unit-diskon">%</span>)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="discount_percentage" id="discount_percentage" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" min="0" max="100" value="{{0+$data->promo_percentage_order}}" @if (!is_null($data->id)) disabled @endif>
                                    <input type="hidden" class="form-control form-control-sm" name="discount" id="discount" value="{{$data->promo_order}}" >
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Pajak(%)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="tax_percentage" id="tax_percentage" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==3) return false;" min="0" max="100" value="{{0+$data->tax_percentage_order}}" @if (!is_null($data->id)) disabled @endif>
                                    <input type="hidden" class="form-control form-control-sm" name="tax" id="tax" value="{{$data->tax_order}}" >
                                </div>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right"  id="total" value="0" disabled>
                                    <input type="hidden" class="form-control form-control-sm text-right" name="total" id="total3" value="0" disabled>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>
                    <input type='hidden' name='id' value='{{$data->id}}'>
                    <input type='hidden' name='step_type' value='3'>
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            getCustomer()
            sumTotal()
            $('#shipping_cost').mask('#.##0,00', {reverse: true});
            $('#trans2').select2();
            $("#pengiriman").hide();
            $('.sc_product_id').select2();

            $("#timezone").val($('.timezone').val())
            $('#trans2').change(function(){
                var debet=$("#trans2").val().split("_")
                if(debet[1]==1 || debet[1]==28)
                {
                    $("#pengiriman").show();
                }else{
                    $("#pengiriman").hide();

                }
            });
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            $('#trans').select2();
        });
        $("#shipping_cost").change(function() {
            sumTotal();
        });
        $("table").on("change", "input", function() {
            var row = $(this).closest("tr");
            var quantity = parseFloat(row.find(".quantity").val());
            var selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal(row.find('.selling_price').val(),'.',''),',','.');
            var subtotal=quantity*selling_price;
            row.find(".subtotal_original").val(subtotal);
            row.find(".subtotal").val(currencyFormat(subtotal,''));
            sumTotal();
        });
        function sumTotal(){
            var sum = 0;
            $(".subtotal_original").each(function(){
                sum += +$(this).val();
            });
            if(sum==0)
            {
                $("#save-po").attr("disabled", true);
            }else {
                $("#save-po").attr("disabled", false);

            }
            $("#total1").val(currencyFormat(sum,''));
            $("#total2").val(sum);
            var subtotal=$("#total2").val();
            var tax_percentage=$("#tax_percentage").val();
            var discount_percentage=$("#discount_percentage").val();
            var shipping_cost = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($('#shipping_cost').val(),'.',''),',','.');
            if($('#unit-diskon').text()=='%'){
                var discount = subtotal*discount_percentage/100;
            }else{
                var discount = discount_percentage;
            }

            var price = subtotal-discount;
            var tax = price*tax_percentage/100;
            var total=price+tax+parseFloat(shipping_cost);
            var total_with_shipping=total
            $("#tax").val(tax)
            $("#discount").val(discount)
            $("#shipping_cost2").val(shipping_cost)
            $("#total").val(currencyFormat(total_with_shipping))
            $("#total3").val(total_with_shipping)

        }
        function getCustomer()
        {
            $("#sc_customer_id").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.transaction.sale-order.customer-list')}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

        }
        $('#form-konten').submit(function () {
            sumTotal()
            var data = getFormData('form-konten');
            var shipping =$("#trans2").val().split("_")
            data.append("md_sc_shipping_category_id",shipping[0])
            ajaxTransfer("{{route('merchant.toko.transaction.sale-order.save-delivery')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })
    </script>
@endsection





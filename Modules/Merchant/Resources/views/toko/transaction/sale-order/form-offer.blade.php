@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Penjualan</a>
                        </li>
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a href="{{route('merchant.toko.sale-order.data')}}">Penawaran Penjualan</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a>{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kamu bisa menambah data penawaran penjualan seperti detail produk, nama pelanggan, metode pembayaran hingga pengiriman produk</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
                <div id="result-form-konten"></div>
                <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                    <input type="hidden" class="form-control form-control-sm" name="timezone" id="timezone" value="">
                    <input type="hidden" class="form-control form-control-sm" name="paid_nominal" id="paid_nominal" value="0">
                    <input type="hidden" class="form-control form-control-sm" name="change_nominal" id="change_nominal" value="0" >
                    <div class="row mb-4">
                        @include('merchant::component.branch-selection',
['withoutModal'=>true,
'data'=>$data,
'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
'selector'=>'branch',
'with_onchange'=>false,
'function_onchange'=>''

])
                        <div class="col-md-6">
                            @if(!is_null($data->id))
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Kode Penawaran</label>
                                    <div class="col-sm-6">
                                        <input type="hidden" class="form-control form-control-sm" name="code" value="{{str_replace('P','',$data->code)}}">
                                        <input type="text" class="form-control form-control-sm" name="second_code" value="{{is_null($data->second_code)?str_replace('P','PQ',$data->code):$data->second_code}}">

                                    </div>
                                </div>
                            @else
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label font-weight-bold">Kode Penawaran</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-sm" name="second_code" value="">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Pelanggan</label>
                                <div class="col-sm-6">
                                    <select id="sc_customer_id" class="form-control form-control-sm" name="sc_customer_id">
                                        <option value="-1">Pilih Pelanggan</option>
                                        @if(!is_null($data->id))
                                            <option selected value="{{$data->sc_customer_id}}">{{$data->getCustomer->code}} {{$data->getCustomer->name}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-6 col-form-label font-weight-bold">Tanggal</label>
                                <div class="col-sm-6">
                                    <input type="text" id="trans_time" class="form-control form-control-sm trans_time" name="created_at" value="{{$data->created_at}}" placeholder="Waktu Transaksi" required>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <table class="table table-bordered text-center table-add-trans" id="dynamic_field" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="25%">Produk</th>
                                    <th>Harga Jual</th>
                                    <th>Jumlah</th>
                                    <th width="15%">Satuan</th>
                                    <th>Sub Total</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody id="tbodyid">
                                @php
                                    $total=0;
                                @endphp
                                @foreach ($data->getDetail->where('is_deleted',0) as $key=> $d)
                                    <tr id="row{{$key}}">
                                        <td>
                                            <select class="form-control form-control-sm sc_product_id" name="sc_product_id[]" id="sc_product_id{{$key}}" disabled>
                                                @if(is_null($d->product_name))
                                                    <option value="{{$d->sc_product_id}}">{{$d->getProduct->name}} {{$d->getProduct->code}}</option>
                                                @else
                                                    <option value="{{$d->sc_product_id}}">{{$d->product_name}}</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td><input type="text" name="selling_price[]" class="form-control form-control-sm selling_price" value="{{rupiah($d->price)}}" id="selling_price{{$key}}"/></td>
                                        <td><input type="text" name="quantity[]" class="form-control form-control-sm quantity" value="{{$d->quantity}}" id="quantity{{$key}}" /></td>
                                        <td>
                                            @if(is_null($d->unit_name))
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{is_null($d->getProduct->getUnit)?'':$d->getProduct->getUnit->name}}" disabled>

                                            @else
                                                <input type="hidden" name="unit_name[]" id="unit_name{{$key}}" class="form-control form-control-sm unit_name" value="{{$d->unit_name}}" disabled>
                                            @endif
                                                @if($d->is_multi_unit==0)
                                                    <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                    <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="-1"/>
                                                    <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="1"/>
                                                    <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="0"/>
                                                    <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control selectalldata" required onchange="getMultiUnit({{$key}})">
                                                        @if(is_null($d->unit_name))
                                                            @if(!is_null($d->getProduct->getUnit))
                                                                <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                            @endif
                                                        @else
                                                            <option value="0">{{$d->unit_name}}</option>
                                                        @endif
                                                    </select>
                                                @else
                                                    <input type="hidden" name="json_multi_unit[]" id="json_multi_unit{{$key}}" class="form-control form-control-sm" value="{{$d->json_multi_unit}}"/>
                                                    <input type="hidden" name="multi_unit_id[]" id="multi_unit_id{{$key}}" class="form-control form-control-sm" value="{{$d->multi_unit_id}}"/>
                                                    <input type="hidden" name="nilai_konversi[]" id="nilai_konversi{{$key}}" class="form-control form-control-sm" value="{{$d->value_conversation}}"/>
                                                    <input type="hidden" name="is_multi_unit[]" id="is_multi_unit{{$key}}" class="form-control form-control-sm" value="1"/>
                                                    <select  name="unit_list[]" id="unit_list{{$key}}" class="form-control" required onchange="getMultiUnitExistData({{$key}})">
                                                        @if(!is_null($d->json_multi_unit))
                                                            @foreach(json_decode($d->json_multi_unit) as $m)
                                                                <option value="{{$m->id}}"
                                                                        @if($m->original_id==$d->multi_unit_id) selected @endif
                                                                >{{$m->text}}</option>
                                                            @endforeach
                                                        @else
                                                            @if(is_null($d->unit_name))
                                                                @if(!is_null($d->getProduct->getUnit))
                                                                    <option value="{{$d->getProduct->md_unit_id}}">{{$d->getProduct->getUnit->name}}</option>
                                                                @endif
                                                            @else
                                                                <option value="0">{{$d->unit_name}}</option>
                                                            @endif
                                                        @endif
                                                    </select>
                                                @endif
                                        </td>
                                        <td>

                                            <input type="text" name="subtotal[]" class="form-control form-control-sm subtotal" value="{{rupiah($d->sub_total)}}" id="subtotal{{$key}}" disabled/>
                                            <input type="hidden" name="subtotal_original[]" class="form-control form-control-sm subtotal_original" value="{{$d->sub_total}}" id="subtotal_original{{$key}}" disabled/>
                                        </td>
                                        <input type="hidden" name="coa_inv_id[]" class="form-control form-control-sm coa_inv_id" value="{{$d->coa_inv_id}}" id="coa_inv_id{{$key}}" disabled/>
                                        <input type="hidden" name="coa_hpp_id[]" class="form-control form-control-sm coa_hpp_id" value="{{$d->coa_hpp_id}}" id="coa_hpp_id{{$key}}" disabled/>
                                        @if(is_null($d->product_name))
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->getProduct->code}} {{$d->getProduct->code}}" id="product_name{{$key}}" disabled/>

                                        @else
                                            <input type="hidden" name="product_name[]" class="form-control form-control-sm product_name" value="{{$d->product_name}}" id="product_name{{$key}}" disabled/>

                                        @endif

                                        <td><button type="button" name="remove" id="{{$key}}"  onclick="hapusDetail({{$d->id}})" class="btn btn-xs btn-delete-xs btn_remove">
                                                <span class='fa fa-trash-alt' style='color: white'></span>

                                            </button>
                                        </td>
                                    </tr>
                                    <input type="hidden" value="{{$d->id}}" name="sc_sale_order_detail_id[]">
                                    @php
                                        $total+=$d->sub_total;
                                    @endphp
                                @endforeach
                                </tbody>

                            </table>
                            <button type="button" name="add" id="add" class="btn btn-darkblue btn-rounded py-2 px-4">
                                <i class="fa fa-plus mr-2"></i>
                                <span>Tambah Produk</span>
                            </button>
                            <hr>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="font-weight-bold">Catatan :</label>
                                <textarea name="note" class="form-control form-control-sm" id="" cols="20" rows="5">{{$data->note_order}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">SubTotal</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" id="total1" value="0" disabled>
                                    <input type="hidden" class="form-control form-control-sm" id="total2" name="total1" value="0" disabled>
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Diskon(<span id="unit-diskon">%</span>)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="discount_percentage" id="discount_percentage" step="0.01" min="0" max="100" value="{{0+$data->promo_percentage_offer}}" onkeypress="return isNumber(event)" required >
                                    <input type="hidden" class="form-control form-control-sm" name="discount" id="discount" value="{{$data->promo_offer}}" >
                                </div>

                            </div>

                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Pajak(%)</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right" name="tax_percentage" id="tax_percentage" step="0.01" min="0" max="100" value="{{0+$data->tax_percentage_offer}}" onkeypress="return isNumber(event)" required >
                                    <input type="hidden" class="form-control form-control-sm" name="tax" id="tax" value="{{$data->tax_offer}}" >
                                </div>
                            </div>

                            <div class="form-group row align-items-center" id="pengiriman">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Biaya Pengiriman</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control form-control-sm text-right" name="shipping_cost" id="shipping_cost" value="{{0+$data->shipping_cost}}" >
                                </div>
                            </div>

                            <div class="form-group row align-items-center">
                                <label class="col-sm-4 col-form-label text-note font-weight-bold">Total</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control form-control-sm text-right"  id="total" value="{{($data->getDetail->where('is_deleted',0)->count()>0)?$total+$data->tax-$data->discount:0}}" disabled>
                                    <input type="hidden" class="form-control form-control-sm" name="total" id="total3" value="{{($data->getDetail->where('is_deleted',0)->count()>0)?$total+$data->tax-$data->discount:0}}" disabled>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-success" id="save-po"><i class="fa fa-save mr-2"></i> Simpan</button>
                        </div>
                    </div>

                    <input type='hidden' name='id' value='{{$data->id}}'>
                    <input type='hidden' name='is_with_load_shipping' value='false' id="beban-pengiriman-1">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>

                </form>
            </div>
        </div>

    </div>


@endsection

@section('js')
    <script>

        @if(!is_null($data->id))

        var productSelectedId = {};

        @foreach($data->getDetail->where('is_deleted',0) as $j =>$jj)
            productSelectedId[{{$j}}]={{$jj->sc_product_id}}
            $("#unit_list{{$j}}").select2()
        @endforeach
        @else
        var productSelectedId = {};
        @endif

        function isNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            if (key.length == 0) return;
            var regex = /^[0-9.\b]+$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        $(document).ready(function() {

            getCustomer()
            @if(!is_null($data->id))
            var i={{$data->getDetail->where('is_deleted',0)->count()+1}};
            @else
            var i=0;
            @endif

            var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
            var d = new Date();
            sumTotal();
            $("#tempo").hide();
            $("#pengiriman").hide();
            var codes = [];

            $('#sc_customer_id').on('select2:open', () => {
                $(".select2-results:not(:has(a))").append('<a id="add-customer" onclick="loadModal(this)" target="{{route("merchant.toko.customer.add")}}" style="padding: 6px;height: 20px;display: inline-table;"class="text-danger"><b>+ Tambah Pelanggan Baru</b></a>');
            });

            $(document).on('click','#add-customer', function(e){
                $("#sc_customer_id").select2('close');
            });

            $("#timezone").val($('.timezone').val())
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            $('.sc_product_id').select2();
            $('#trans').select2();
            $('#trans2').select2();

            $('#add').click(function(){

                let branch = $("#branch").val()

                if(branch=='-1' || branch==-1)
                {
                    otherMessage('warning','Cabang belum terpilih !')
                }else {
                    i++;
                    $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added">' +
                        '<td>' +
                        '<input type ="hidden" value="0" name="sc_sale_order_detail_id[]"><select  name="sc_product_id[]" id="sc_product_id' + i + '" class="form-control" onchange="productChange('+i+')" required ></select>' +
                        '</td>' +
                        '<td>' +
                        '<input type="text" name="selling_price[]" class="form-control form-control-sm selling_price"  id="selling_price'+i+'" value="0" required/>' +
                        '</td>' +
                        '<td>' +
                        '<input type="text" name="quantity[]" id="quantity'+i+'" class="form-control form-control-sm quantity" value="0" required onkeypress="return isNumber(event)" />' +
                        '</td>' +
                        '<td>' +
                        '<input type="hidden" name="json_multi_unit[]" id="json_multi_unit'+i+'" class="form-control form-control-sm" value="-1"/>' +
                        '<input type="hidden" name="multi_unit_id[]" id="multi_unit_id'+i+'" class="form-control form-control-sm" value="-1"/>' +
                        '<input type="hidden" name="nilai_konversi[]" id="nilai_konversi'+i+'" class="form-control form-control-sm" value="1"/>' +
                        '<input type="hidden" name="is_multi_unit[]" id="is_multi_unit'+i+'" class="form-control form-control-sm" value="0"/>' +
                        '<input type="hidden" name="unit_name[]" id="unit_name'+i+'" class="form-control form-control-sm" value=""/>' +
                        '<select  name="unit_list[]" id="unit_list' + i + '" class="form-control" onchange="getMultiUnit('+i+')" required></select>' +
                        '</td>' +
                        '<td>' +
                        '<input type="text" name="subtotal[]" id="subtotal'+i+'" class="form-control form-control-sm subtotal" value="0" disabled/>' +
                        '<input type="hidden" name="subtotal_original[]" id="subtotal_original'+i+'" class="form-control form-control-sm subtotal_original" value="0" disabled/>' +
                        '<input type="hidden" name="product_name[]" id="product_name'+i+'" class="form-control form-control-sm product_name" value="" disabled/>' +
                        '</td>' +
                        '<input type="hidden" name="coa_inv_id[]" id="coa_inv_id'+i+'" class="form-control form-control-sm"/>' +
                        '<input type="hidden" name="coa_hpp_id[]" id="coa_hpp_id'+i+'" class="form-control form-control-sm"/>' +
                        '<td>' +
                        '<button type="button" name="remove" id="'+i+'" class="btn btn-xs btn-delete-xs btn_remove"> '+
                        '<span class="fa fa-trash-alt" style="color: white"></span> </button>'+
                        '</td>' +
                        '</tr>');


                    $("#sc_product_id" + i).select2({
                        placeholder:"Pilih Produk",
                        ajax: {
                            type: "GET",
                            url: "{{route('api.product.all-v2')}}?md_merchant_id="+branch,
                            dataType: 'json',
                            delay: 250,
                            headers:{
                                "senna-auth":"{{get_user_token()}}"
                            },
                            data: function (params) {
                                return {
                                    key: params.term
                                };
                            },
                            processResults: function (data) {
                                // console.log(data)
                                return {
                                    results: data
                                };
                            },
                            cache: true
                        },
                    });

                    $('.selling_price').mask('#.##0,00', {reverse: true});
                    $("#unit_list"+i).select2()
                }


            });

            $("#branch").change(function(){
                $("#tbodyid").empty()
                productSelectedId={};
            })
            $( "#tax_percentage").change(function() {
                sumTotal();
            });
            $( "#discount_percentage").change(function() {
                sumTotal();
            });
            $("table").on("change", "input", function() {
                var row = $(this).closest("tr");
                var quantity = parseFloat(row.find(".quantity").val());
                var selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal(row.find('.selling_price').val(),'.',''),',','.');
                var subtotal=quantity*selling_price;
                row.find(".subtotal_original").val(subtotal);
                row.find(".subtotal").val(currencyFormat(subtotal,''));
                sumTotal();
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                delete productSelectedId[button_id];
                sumTotal();
            });
        });


        function sumTotal(){
            var sum = 0;
            $(".subtotal_original").each(function(){
                sum += +$(this).val();
            });
            if(sum==0)
            {
                $("#save-po").attr("disabled", true);
            }else {
                $("#save-po").attr("disabled", false);

            }

            $("#total1").val(currencyFormat(sum,''));
            $("#total2").val(sum);
            var subtotal=$("#total2").val();
            var tax_percentage=$("#tax_percentage").val();
            var discount_percentage=$("#discount_percentage").val();
            var shipping_cost=$("#shipping_cost").val();
            if($('#unit-diskon').text()=='%'){
                var discount = subtotal*discount_percentage/100;
            }else{
                var discount = discount_percentage;
            }
            var price = subtotal-discount;
            var tax = price*tax_percentage/100;
            var total=price+tax;
            var total_with_shipping=total+parseFloat(shipping_cost)
            $("#tax").val(tax)
            $("#discount").val(discount)
            $("#total").val(currencyFormat(total_with_shipping))
            $("#total3").val(total_with_shipping)
        }


        $('#form-konten').submit(function () {
            sumTotal();
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.toko.transaction.sale-order.save-offer')}}", data, function(response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
            });
        })

        function getCustomer()
        {
            $("#sc_customer_id").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.toko.transaction.sale-order.customer-list')}}",
                    dataType: 'json',
                    delay: 250,
                    headers:{
                        "senna-auth":"{{get_user_token()}}"
                    },
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });

        }
        function hapusDetail(id) {
            var data = new FormData();
            data.append('id', id);
            modalConfirm("Konfirmasi", "Apakah kamu yakin menghapus item penawaran penjualan", function () {
                ajaxTransfer("{{route('merchant.toko.transaction.sale-order.delete-detail')}}", data, function(response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url);
                });
            })
        }

        function getMultiUnit(i)
        {

            let unitList = ($("#unit_list"+i).select2('data'))? $("#unit_list"+i).select2('data'):[];
            if(unitList.length>0)
            {
                let data=unitList[0]
                $("#is_multi_unit"+i).val(1)
                $("#unit_name"+i).val(data['text'])
                $("#nilai_konversi"+i).val(data['nilai_konversi'])
                $("#multi_unit_id"+i).val(data['original_id'])
                $("#selling_price"+i).val(currencyFormat(data['selling_price'],''))
                let quantity=$("#quantity"+i).val();
                let selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#selling_price"+i).val(),'.',''),',','.')
                $("#subtotal"+i).val(currencyFormat(quantity*selling_price,''))
                $("#subtotal_original"+i).val(quantity*selling_price)
            }

            sumTotal();
        }


        function getMultiUnitExistData(i)
        {

            let unitVal = $("#unit_list"+i).val()
            let jsonMulti= $("#json_multi_unit"+i).val()
            let isMulti= $("#is_multi_unit"+i).val()
            if(isMulti==1)
            {
                let unitList=JSON.parse(jsonMulti)
                unitList.forEach(element=>{
                    if(element.id==unitVal){
                        $("#unit_name"+i).val(element.text)
                        $("#nilai_konversi"+i).val(element.nilai_konversi)
                        $("#multi_unit_id"+i).val(element.original_id)
                        $("#selling_price"+i).val(currencyFormat(element.selling_price,''))
                        let quantity=$("#quantity"+i).val();
                        let selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#selling_price"+i).val(),'.',''),',','.')
                        $("#subtotal"+i).val(currencyFormat(quantity*selling_price,''))
                        $("#subtotal_original"+i).val(quantity*selling_price)
                    }
                })
            }else{
                let quantity=$("#quantity"+i).val();
                let selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#selling_price"+i).val(),'.',''),',','.')
                $("#subtotal"+i).val(currencyFormat(quantity*selling_price,''))
                $("#subtotal_original"+i).val(quantity*selling_price)
            }
            sumTotal();
        }

        function productChange(i)
        {
            let products = ($("#sc_product_id"+i).select2('data'))? $("#sc_product_id"+i).select2('data'):[];

            if(products.length>0)
            {
                let datas = products[0]
                $("#coa_inv_id"+i).val(datas['coa_inv_id'])
                $("#coa_hpp_id"+i).val(datas['coa_hpp_id'])
                $("#product_name"+i).val(datas['text'])
                let multi_unit=JSON.parse(datas['multi_unit'])
                let multi_unit_list=[]
                $('#unit_list'+i).empty();
                if(multi_unit.length>0)
                {
                    $("#selling_price"+i).val(currencyFormat(0,''))
                    let quantity=$("#quantity"+i).val();
                    let selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#selling_price"+i).val(),'.',''),',','.')
                    $("#subtotal"+i).val(currencyFormat(quantity*selling_price,''))
                    $("#subtotal_original"+i).val(quantity*selling_price)

                    $.each(multi_unit,function (iteration,value){
                        if(iteration===0)
                        {
                            multi_unit_list.push({
                                id:'-1',
                                text:'Pilih Satuan',
                                selling_price:0,
                                nilai_konversi:0,
                                original_id:'-1'
                            })
                        }

                        multi_unit_list.push({
                            id:value.unit_id,
                            text:value.konversi_ke,
                            selling_price:value.harga_jual,
                            nilai_konversi:value.nilai_konversi,
                            original_id:value.id
                        })
                    })
                    $("#unit_list"+i).select2({
                        data:multi_unit_list
                    })
                    $("#json_multi_unit"+i).val(JSON.stringify(multi_unit_list))
                    sumTotal();
                }else{
                    $("#is_multi_unit"+i).val(0)
                    $("#unit_name"+i).val(datas['satuan'])
                    multi_unit_list.push({
                        id:datas['satuan_id'],
                        text:datas['satuan'],
                        selling_price:datas['selling_price'],
                        nilai_konversi:1,
                        original_id:datas['id']
                    })
                    $("#unit_list"+i).select2({
                        data:multi_unit_list
                    })
                    $("#selling_price"+i).val(currencyFormat(datas['selling_price'],''))
                    let quantity=$("#quantity"+i).val();
                    let selling_price = replaceCurrencyPointOrDecimal(replaceCurrencyPointOrDecimal($("#selling_price"+i).val(),'.',''),',','.');
                    $("#subtotal"+i).val(currencyFormat(quantity*selling_price,''))
                    $("#subtotal_original"+i).val(quantity*selling_price)
                    sumTotal();

                }

                let status = false;
                Object.keys(productSelectedId).forEach(key => {
                    if(productSelectedId[key] === parseInt(datas['id'])){
                        status=true;
                    }
                });
                if(status)
                {
                    $('#row'+i).remove();
                    delete productSelectedId[i];
                }else{
                    productSelectedId[i] = parseInt(datas['id']);

                }
            }


        }

    </script>
@endsection





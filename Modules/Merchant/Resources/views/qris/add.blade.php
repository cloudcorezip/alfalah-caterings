@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')

    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1"> {{$title}}</h4>
                <span>Kamu bisa menambah data pengajuan aktivasi pembayaran digital dengan form berikut</span>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4 px-5 py-4">
            <div class="card-body">
            <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
                <div id="result-form-konten"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="font-weight-bold">Nama Pemilik</label>
                        <input type="text" class="form-control form-control-sm" name="bank_account_name" placeholder="Nama Rekening" value="{{$data->bank_account_name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="font-weight-bold">Nomor Rekening</label>
                        <input type="text" class="form-control form-control-sm" name="bank_account_number" placeholder="Nomor Rekening" value="{{$data->bank_account_number}}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="font-weight-bold">Bank</label>
                        <select id="rajabiller_bank" class="form-control form-control-sm" name="md_rajabiller_bank_id"  required>
                            @foreach ($bank as $b)
                            <option value="{{$b->id}}" @if ($data->md_rajabiller_bank_id==$b->id) selected @endif >{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="font-weight-bold">Unggah Foto KTP</label>
                        <input type="file" class="form-control" name="identity_card"  required  >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="font-weight-bold">Unggah Buku Rekening</label>
                        <input type="file" class="form-control" name="account_book"  required >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="font-weight-bold">Nomor Telepon</label>
                        <input type="text" class="form-control form-control-sm" name="phone_number" placeholder="Nomor Telepon" value="{{$data->phone_number}}" required>
                        <span class="text-danger">*Pastikan nomor anda valid</span>
                    </div>
                        <div class="form-check">
                        <input type="checkbox" id="policy" class="form-check-input" name="policy">
                        <label for="policy">Dengan masuk dan mendaftar, kamu menyetujui</label><a onclick="loadModalFullScreen(this)" target="{{route('qris.policy')}}" class="text-danger"><b> Syarat & Ketentuan Layanan</b></a>.
                    </div>
                </div>
            </div>


                <div class="modal-footer">
                    <button id="submit-qris" class="btn btn-sm btn-success py-2 px-4" disabled>Ajukan</button>
                </div>
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <input type='hidden' name='md_user_id' value='{{ user_id() }}'>
                <input type='hidden' name='id' value='{{ $data->id }}'>
            </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $(document).ready(function () {
        $("#rajabiller_bank").select2();
        $(function() {
            enable_cb();
            $("#policy").click(enable_cb);
            });

            function enable_cb() {
                if (this.checked) {
                    $("#submit-qris").removeAttr("disabled");
                } else {
                    $("#submit-qris").attr("disabled", true);
                }
            }
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('qris.create')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })


</script>
@endsection

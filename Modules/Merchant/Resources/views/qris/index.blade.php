@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <h4>{{$title}}</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route(''.dashboard_url().'')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                </ol>
            </nav>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    @if ($row<=0)
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-lg-2">
                                <a class="btn btn-success btn-rounded btn-sm btn-block" href="{{route('qris.add')}}"><i class="fa fa-plus"></i> Ajukan QRIS</a>
                            </div>

                        </div>
                    @endif

                    <div id="output-sale-order">
                        @include('merchant::qris.list')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>

    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold text-dark mt-3">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" class="px-30" onsubmit="return false">
            <div class="row d-flex align-items-center">
                <div class="col-md-12 mb-3">
                    <label>Pilih Tanggal Transaksi :</label>
                    <div id="reportrange" class="form-control" name="date-range">
                        <i class="fa fa-calendar"></i>&nbsp;
                        <span></span> <i class="fa fa-caret-down"></i>
                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label>Pilih Status :</label>
                    <select name="is_agree" id="" class="form-control form-control-sm">
                        <option value="-1">Semua Status</option>
                        <option value="0">Menunggu Persetujuan</option>
                        <option value="1">Disetujui</option>
                        <option value="2">Ditolak</option>
                    </select>
                </div>
                <div class="col-md-12 mb-3 mt-3">
                    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                    <input type="hidden" name="search_key" value="{{$searchKey}}">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('js')
    @include('backend-v2.layout.daterangepicker')

    <script type="text/javascript">
        $(function() {

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                    '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                    '90 Hari Terakhir': [moment().subtract(89, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Tahun Ini':[moment().startOf('year'),moment().endOf('year')],
                    'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1,'year').endOf('year')],
                    //'All Time': 'all-time',
                }
            }, cb);
            cb(start, end);

        });
    </script>
    <script>

        $(document).ready(function () {
            $('#form-filter-sale-order').submit(function () {
                var startDate = $('#reportrange').data('daterangepicker').startDate._d;
                var endDate = $('#reportrange').data('daterangepicker').endDate._d;
                var data = getFormData('form-filter-sale-order');
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                ajaxTransfer("{{route('qris.reload.data')}}", data, '#output-sale-order');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        });

    </script>

@endsection

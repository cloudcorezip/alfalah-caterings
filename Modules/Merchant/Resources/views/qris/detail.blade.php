@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">{{$title}}</h4>
                <span>Berikut merupakan detail dari data pengajuan QRIS</span>

            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row mb-3 px-5 py-4">
                    <div class="col-md-4">
                        <dl class="trans-info">
                            <dd>
                                <span>Nama Bank</span>
                            </dd>
                            <dt>
                                <h3 class="font-weight-bold">{{$data->getBank->name}}</h3>
                            </dt>
                        </dl>
                    </div>
                    <div class="col-md-4">
                        <dl class="trans-info">
                            <dd>
                                <span>Nama Pemilik</span>
                            </dd>
                            <dt>
                                <h3 class="font-weight-bold">{{$data->bank_account_name}}</h3>
                            </dt>
                        </dl>
                    </div>
                    <div class="col-md-4">
                        <dl class="trans-info">
                            <dd>
                                <span>Nomor Rekening</span>
                            </dd>
                            <dt>
                                <h3 class="font-weight-bold">{{$data->bank_account_number}}</h3>
                            </dt>
                        </dl>
                    </div>
                    <div class="col-md-4">
                        <dl class="trans-info">
                            <dd>
                                <span>Kartu Identitas</span>
                            </dd>
                            <dt>
                                <a class="text-tosca" href="{{env('S3_URL').$data->identity_card}}" target="_blank">Download</a>
                            </dt>
                        </dl>
                    </div>
                    <div class="col-md-4">
                        <dl class="trans-info">
                            <dd>
                                <span>Buku Rekening</span>
                            </dd>
                            <dt>
                                <a class="text-tosca" href="{{env('S3_URL').$data->account_book}}" target="_blank">Download</a>
                            </dt>
                        </dl>
                    </div>
                    <div class="col-md-4">
                        <dl class="trans-info">
                            <dd>
                                <span>Status</span>
                            </dd>
                            <dt>
                                @if($data->is_agree == 0)
                                    <span class="text-warning">Menunggu Persetujuan</span>
                                @elseif($data->is_agree == 1)
                                    <span class="text-success">Disetujui</span>
                                @else
                                    <span class="text-danger">Ditolak</span>
                                @endif
                            </dt>
                        </dl>
                    </div>
                </div>


                    <div class="row px-5 py-4">
                        <div class="col-md-8">
                            <a href="{{route('merchant.toko.profile.detail',['id'=>merchant_id()])}}?page=digital-payment" class="d-flex align-items-center">
                                <img src="{{asset('public/backend/img/back.png')}}" alt="" width="18px" class="mr-2">
                                <span style="color:#A4A4A4;">Kembali</span>
                            </a>
                        </div>
                        <div class="col-md-4">
                            @if($data->is_agree == 2)
                                <a class="btn btn-success btn-rounded btn-sm" href="{{route('qris.add',['id'=>$data->id])}}"> Ajukan Lagi</a>
                            @endif
                        </div>
                    </div>

            </div>
        </div>
    </div>

@endsection


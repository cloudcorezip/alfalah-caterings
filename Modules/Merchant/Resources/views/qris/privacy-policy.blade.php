<style>
    .kelompok-harta-list {
        display: inline-block;
        width: 100%;
        margin-bottom: 10px
    }

    .kelompok-harta-title {
        background-color: #ff7b15;
        color: #fff;
        display: block;
        width: 100%;
        padding: 5px 10px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        text-transform: uppercase;
        font-weight: 500
    }

    .kelompok-harta-deskripsi {
        display: inline-block;
        border: 1px solid #cacaca;
        padding: 5px 10px;
        border-top: 0 none;
        width: 100%
    }

    .kelompok-harta-deskripsi ol {
        margin: 0;
        padding: 0 0 0 15px
    }
    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
    }
</style>
</div>
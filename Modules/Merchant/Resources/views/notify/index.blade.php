@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <h4 class="mb-1">Pemberitahuan</h4>
                <span>Kamu bisa melihat informasi pemberitahuan tentang layanan dan update fitur Senna disini</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-custom" id="table-data" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($tableColumns as $key =>$item)
                                <th>{{($tableColumns[$key]=="row_number")?"No":str_replace('_',' ',strtoupper($tableColumns[$key]))}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menandai telah dibaca data ini ?", function () {
                ajaxTransfer("{{route('merchant.notify.read')}}", data, "#modal-output");
            })
        }
        $(document).ready(function() {
            ajaxDataTable('#table-data', 1, "{{route('merchant.notify.datatable')}}", [
                    @foreach($tableColumns as $key =>$item)
                    @if($tableColumns[$key]=='action')
                {
                    data: '{{$tableColumns[$key]}}',
                    name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}'
                },
                @endif
                @endforeach
            ]);
        })
    </script>
@endsection

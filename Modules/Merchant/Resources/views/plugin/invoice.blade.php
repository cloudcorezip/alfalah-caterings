@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid d-flex justify-content-between align-items-center">
        <div class="p-5">
            <h4 class="mb-1">{{$title}}</h4>
        </div>
    </div>
</div>

<div class="container">
    <div class="card shadow mb-4">
        <div class="card-body p-4">
            <div class="d-flex justify-content-between p-50" style="background:#FAFAFA;">
                <div class="row">
                    <div class="col-md-12">
                        <img src="{{env('S3_URL')}}public/frontend/img/logo.png" alt="logo" class="img-fluid mb-4" style="width:60%;">
                        <h6 style="color:#707070;" class="arial-rounded">Senna Indonesia</h6>
                        <span class="d-block text-span-desc">Sekarsuli-Berbah No.9, Potorono,<br>Banguntapan, Bantul Regency,<br>Special Region of Yogyakarta</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <span class="d-block text-span-desc">Email : contact@senna.co.id</span>
                        <span class="d-block text-span-desc">Website : senna.co.id</span>
                        <span class="d-block text-span-desc">Phone : (+62)813-2566-6958</span>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between p-t-50 p-l-50 p-r-50">
                <div class="row">
                    <div class="col-md-12">
                        <h3 style="color:#707070;" class="arial-rounded">INVOICE</h3>
                        <span class="d-block text-span-desc">{{$data->getPlugin->name}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        @if($data->transaction->md_sp_transaction_status_id == 1)
                            <h2 style="color:#FF9E0D;" class="arial-rounded">PENDING</h2>
                        @endif
                        @if($data->transaction->md_sp_transaction_status_id == 2)
                            <h2 style="color:#62AC29;" class="arial-rounded">SUCCESS</h2>
                        @endif
                        @if($data->transaction->md_sp_transaction_status_id == 3)
                            <h2 style="color:#FF9E0D;" class="arial-rounded">CANCELED</h2>
                        @endif
                        @if($data->transaction->md_sp_transaction_status_id == 4)
                            <h2 style="color:#FF9E0D;" class="arial-rounded">FAILED</h2>
                        @endif
                        @if($data->transaction->md_sp_transaction_status_id == 5)
                            <h2 style="color:#FF9E0D;" class="arial-rounded">REFUND</h2>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row p-l-50 p-r-50">
                <div class="col-md-4">
                    <span class="d-block text-span-desc">
                        <strong>Date</strong> {{$created_at}}
                    </span>
                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <span class="d-block text-span-desc text-right">
                        <strong>Invoice No.</strong> {{$data->transaction->code}}
                    </span>
                </div>
                <div class="col-md-12">
                    <hr>
                </div>
            </div>

            <div class="row p-l-50 p-r-50 pt-2">
                <div class="col-md-6">
                    <h4 class="text-orange arial-rounded">BILL FROM</h4>
                    <h5 style="color:#707070;" class="arial-rounded">Senna Indonesia</h5>
                    <span class="d-block text-span-desc">Sekarsuli-Berbah No.9, Potorono, Banguntapan, Bantul Regency,<br>Special Region of Yogyakarta</span>
                    <span class="d-block text-span-desc">(+62)813-2566-6958</span>
                    <span class="d-block text-span-desc">contact@senna.co.id</span>
                </div>
                <div class="col-md-6">
                    <h4 style="color:#62AC29;" class="arial-rounded">TO</h4>
                    <h5 style="color:#707070;" class="arial-rounded">{{ucwords($data->getMerchant->name)}}</h5>
                    <span class="d-block text-span-desc">{{is_null($data->getMerchant->address)? '':$data->getMerchant->address}}</span>
                    <span class="d-block text-span-desc">{{is_null($data->getMerchant->phone_merchant)? '':$data->getMerchant->phone_merchant}}</span>
                    <span class="d-block text-span-desc">{{$user->email}}</span>
                </div>
            </div>

            <div class="row p-50">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" width="100%" cellspacing="0">
                            <thead style="background:#F9F9F9;">
                                <tr>
                                    <th style="color:#A4A4A4; font-weight:600;">PLUGIN</th>
                                    <th style="color:#A4A4A4; font-weight:600;" class="text-center">PRICE</th>
                                    <th style="color:#A4A4A4; font-weight:600;" class="text-right">DURATION</th>
                                    <th style="color:#A4A4A4; font-weight:600;" class="text-right">STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="border-0">{{$data->getPlugin->name}}</td>
                                    <td class="text-center border-0">{{rupiah($data->getPlugin->price)}}</td>
                                    <td class="text-right border-0">{{$duration}}</td>
                                    <td class="text-right border-0">
                                        @if($data->transaction->md_sp_transaction_status_id == 1)
                                            <i style="color:#FF9E0D;">PENDING</i>
                                        @endif
                                        @if($data->transaction->md_sp_transaction_status_id == 2)
                                            <i style="color:#7EB056;">SUCCESS</i>
                                        @endif
                                        @if($data->transaction->md_sp_transaction_status_id == 3)
                                            <i style="color:#FF9E0D;">CANCELED</i>
                                        @endif
                                        @if($data->transaction->md_sp_transaction_status_id == 4)
                                            <i style="color:#FF9E0D;">FAILED</i>
                                        @endif
                                        @if($data->transaction->md_sp_transaction_status_id == 5)
                                            <i style="color:#FF9E0D;">REFUND</i>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-0"></td>
                                    @if(!is_null($data->md_transaction_type_id))
                                        <td class="text-right border-0" colspan="2">
                                            <h6 class="arial-rounded">Diskon:</h6>
                                            <h6 class="arial-rounded">Biaya Admin :</h6>
                                            <h6 class="arial-rounded">Invoice Total :</h6>
                                        </td>
                                        <td class="text-right border-0">
                                            <h6 id="promo" class="arial-rounded promo">{{rupiah($discount)}}</h6>
                                            <h6 id="admin_fee" class="arial-rounded">{{rupiah($admin)}}</h6>
                                            <h3 id="total" class="arial-rounded text-orange">{{rupiah($price)}}</h3>
                                        </td>
                                    @else
                                        <td class="text-right border-0" colspan="2">
                                            <h6 class="arial-rounded">Diskon:</h6>
                                            <h6 style="display:none;" class="arial-rounded admin_fee">Biaya Admin :</h6>
                                            <h6 class="arial-rounded">Invoice Total :</h6>
                                        </td>
                                        <td class="text-right border-0">
                                            <h6 id="promo" class="arial-rounded promo">{{rupiah($discount)}}</h6>
                                            <h6 style="display:none;" id="admin_fee" class="arial-rounded admin_fee"></h6>
                                            <h3 id="total" class="arial-rounded text-orange">{{rupiah($price)}}</h3>
                                        </td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row p-50">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between">
                        <a class="btn btn-light py-2" href="{{route('subscription.history.index')}}">Kembali</a>

                        @if($data->transaction->md_sp_transaction_status_id == 1 && $data->is_from_web == 1)
                            <form onsubmit="return false;" class="form-konten" id="form-konten" style="width:35%;">
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="csrf_token" value="{{csrf_token()}}">
                                <div class="row">
                                    <div class="col-md-8">
                                        <select name="md_transaction_type_id" id="md_transaction_type_id" class="form-control" @if(!is_null($data->md_transaction_type_id)) disabled @endif>
                                            <option></option>
                                            <option value="5" @if(!is_null($data->md_transaction_type_id) && $data->md_transaction_type_id == 5) selected @endif>Virtual Account</option>
                                            <option value="4" @if(!is_null($data->md_transaction_type_id) && $data->md_transaction_type_id == 4) selected @endif >QRIS</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <button id="btn-checkout" type="submit" class="inv-btn__checkout py-2" @if(is_null($data->md_transaction_type_id)) disabled @endif>CHECKOUT</button>
                                    </div>

                                </div>
                            </form>
                        @endif

                        @if($data->transaction->md_sp_transaction_status_id == 2)
                            <a target="_blank" class="inv-btn__checkout text-white py-2" href="{{route('plugin.history.export-pdf',['key' => encrypt($data->id)])}}">Cetak dan Lihat</a>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const loadTableView = () => {
        const descLength = $('#data-description ul li').length;
        let checkHtml = "<ul>";
        let stripHtml = "<ul>";

        for(let i = 0; i < descLength; i++){
            checkHtml += "<li><i class='fa fa-check-circle' style='color:#62AC29;'></i></li>";
            stripHtml += "<li style='color:#707070;'>-</li>";
        }

        checkHtml += "</ul>";
        stripHtml += "</ul>";
        $('#check-description').html(checkHtml);
        $('#strip-description').html(stripHtml);
    }

    $(document).ready(function(){
        $('#md_transaction_type_id').select2({
            placeholder:'Pilih Metode Pembayaran'
        });
        loadTableView();


        $('#md_transaction_type_id').on('change', function(){
            $('#btn-checkout').prop('disabled', false);
            $('.admin_fee').show();
            $.ajax({
                type:"POST",
                url: "{{route('plugin.check-fee')}}",
                data:{id:'{{$data->id}}',
                        _token:'{{csrf_token()}}',
                        referral_code:$('#referal_code').val(),
                        md_transaction_type_id: $(this).val()
                    },
                headers:{
                    "senna-auth":"{{get_user_token()}}"
                },
                success:function(response){
                    if(response){
                        $('#admin_fee').text(response.adminFee);
                        $('#total').text(response.amountRupiah);
                    } else {
                        $('#admin_fee').text('-');
                        $('#total').text('{{rupiah($price)}}');
                    }
                }
            });

        });

        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('plugin.checkout')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });

        });
    });

</script>

@endsection

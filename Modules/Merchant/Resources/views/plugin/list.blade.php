@foreach($data->getChild as $key => $item)
    <div class="card p-4">
        <div class="row no-gutters">
            <div class="col-md-2 mb-3 d-flex justify-content-center align-items-center">
                <div class="order-online__upper text-center">
                    <img src="{{env('S3_URL')}}{{$item->icon}}" class="img-fluid mb-2" alt="{{$item->name}}">
                    {{---@if($item->status == 1)
                    <span class="d-block mb-4">Status: Aktif</span>
                    @else
                    <span class="d-block mb-4">Status: Belum Aktif</span>
                    @endif---}}
                </div>
            </div>
            <div class="col-md-8 d-flex align-items-center px-4 mb-3">
                <div>
                    <h4 style="color:#747474;" class="font-weight-bold">{{$item->name}}</h4>
                    <div style="color:#959595!important; font-size:12px; margin-bottom:10px;" class="p-desc">
                        {!!$item->description!!}
                    </div>

                    @if($item->discount_percentage > 0)
                    @php
                        $priceAfterDiscount = $item->price - $item->discount_percentage / 100 * $item->price
                    @endphp
                        <h2 class="card-sub__price arial-rounded" style="font-size:24px;">
                            {{rupiah($priceAfterDiscount)}}
                        </h2>
                        <span class="card-sub__price__discount arial-rounded" style="font-size:12px;"><s>{{rupiah($item->price)}}</s></span>
                    @else
                        <h2 class="card-sub__price arial-rounded" style="font-size:21px;">
                            {{rupiah($item->price)}}
                            @if($item->payment_type=='yearly')
                                / tahun
                            @elseif($item->payment_type=='monthly')
                                / 1 bulan
                            @elseif($item->payment_type=='quota')

                            @elseif($item->payment_type=='sixmonth')
                            / 6 bulan
                            @else
                            / 3 bulan
                            @endif
                        </h2>
                    @endif
                </div>
            </div>
            <div class="col-md-2 d-flex align-items-end justify-content-center mb-3">
                <form onsubmit="return false;" class="form-konten" id="form-{{$item->id}}">
                    <input type="hidden" name="md_plugin_id" value="{{$item->id}}">
                    <input type="hidden" name="csrf_token" value="{{csrf_token()}}">
                    <button style="border-radius:5px;" type="submit" class="btn btn-order__online d-block mx-auto py-2">BELI</button>
                </form>
            </div>
        </div>
    </div>
@endforeach

<script>
    $(document).ready(function(){
        $('.p-desc ul li').css('line-height', '19px');
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Daftar paket yang tersedia</span>
                                    </div>`);
    });

     $(document).ready(function () {
        $('.form-konten').submit(function () {
            var data = getFormData($(this).attr('id'));

            ajaxTransfer("{{route('plugin.create-invoice')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    });
</script>

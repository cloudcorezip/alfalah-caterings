@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<link rel="stylesheet" href="{{asset('public/backend/css/custom.css')}}">
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid d-flex justify-content-between align-items-center">
        <div class="p-5">
            <h4 class="mb-1">{{$title}}</h4>
            <span>Disini kamu bisa mengakses fitur layanan berlangganan apabila masa langganan kamu sudah habis.</span>
        </div>
    </div>
</div>

<div class="container-fluid">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link" href="{{route('plugin.index')}}">Daftar Plugin</a>
        </li>
        <li class="nav-item">
            <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link active" href="{{route('plugin.history.index')}}">Riwayat Pembelian</a>
        </li>
    </ul>
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-custom" id="table-data-history" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        @foreach($tableColumns as $key =>$item)
                            <th>{{($tableColumns[$key]=="row_number")?"No":ucwords(str_replace('_',' ',$tableColumns[$key]))}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    function reloadDataTable(isReload=0)
        {
            ajaxDataTable('#table-data-history', 1, "{{route('plugin.history.datatable')}}?key_val={{$key_val}}", [
                    @foreach($tableColumns as $key =>$item)
                    @if($tableColumns[$key]=='action')
                {
                    data: '{{$tableColumns[$key]}}',
                    name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                @endif
                @endforeach
            ],0);
        }
        $(document).ready(function() {
            reloadDataTable(0)
        })
</script>
@endsection
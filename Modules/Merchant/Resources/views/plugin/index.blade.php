@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid d-flex justify-content-between align-items-center">
        <div class="p-5">
            <h4 class="mb-1">{{$title}}</h4>
            <span>Berikut adalah daftar integrasi yang kami berikan untuk menunjang usahamu.</span>
        </div>
    </div>
</div>

<div class="container-fluid">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link" href="{{route('subscription.index')}}">Layanan Berlangganan</a>
        </li>
        <li class="nav-item">
            <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link active" href="{{route('plugin.index')}}">Layanan Integrasi</a>
        </li>
        <li class="nav-item">
            <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link" href="{{route('subscription.history.index')}}">Riwayat Pembelian Layanan</a>
        </li>
    </ul>
    <div class="card shadow mb-4">
        <div class="card-body">
        <div class="row">
            @foreach($data as $key => $item)
            <div class="col-md-6">
                <div class="card p-4">
                    <div class="row no-gutters">
                      <div class="col-md-3 mb-3">
                            <div class="order-online__upper text-center">
                                <img src="{{env('S3_URL')}}{{$item->icon}}" class="img-fluid mb-2" alt="{{$item->name}}">
                             
                            </div>
                        </div>
                        <div class="d-flex align-items-center px-2 mb-3">
                            <div>
                                <h4 style="color:#747474;" class="font-weight-bold">{{$item->name}}
                                @if(is_null($item->getEnableMerchantPlugin(merchant_id(),$item->id)))
                                <label class="badge badge-pill badge-danger">Belum Aktif</label></h4>
                                @else
                                @php
                                $pluginActivation=$item->getEnableMerchantPlugin(merchant_id(),$item->id);
                                @endphp
                                @if(date('Y-m-d H:i:s')<$pluginActivation->end_period)
                                <label class="badge badge-pill badge-info">Aktif sd {{Carbon\Carbon::parse($pluginActivation->end_period)->isoFormat('D MMMM Y')}} </label></h4>
                                @else 
                                <label class="badge badge-pill badge-warning">Masa Layanan Habis </label></h4>
                                @endif
                                @endif
                                <div style="color:#959595!important; font-size:12px; margin-bottom:10px;" class="p-desc">
                                    {!!$item->description!!}
                                </div>

                                @if($item->discount_percentage > 0)
                                @php
                                    $priceAfterDiscount = $item->price - $item->discount_percentage / 100 * $item->price
                                @endphp
                                    <h2 class="card-sub__price arial-rounded" style="font-size:24px;">
                                        {{rupiah($priceAfterDiscount)}}
                                    </h2>
                                    <span class="card-sub__price__discount arial-rounded" style="font-size:12px;"><s>{{rupiah($item->price)}}</s></span>
                                @else
                                    <h2 class="card-sub__price arial-rounded" style="font-size:21px;">
                                        {{rupiah($item->price)}}
                                        @if($item->payment_type=='yearly')
                                            / tahun
                                        @elseif($item->payment_type=='monthly')
                                            / 1 bulan
                                        @elseif($item->payment_type=='quota')

                                        @elseif($item->payment_type=='sixmonth')
                                        / 6 bulan
                                        @else
                                        / 3 bulan
                                        @endif
                                    </h2>
                                @endif
                            </div>
                        </div>
                        @if(is_null($item->getEnableMerchantPlugin(merchant_id(),$item->id)))
                        <div class="col-md-4 d-flex align-items-end">
                            <form onsubmit="return false;" class="form-konten" id="form-{{$item->id}}">
                                <input type="hidden" name="md_plugin_id" value="{{$item->id}}">
                                <input type="hidden" name="csrf_token" value="{{csrf_token()}}">
                                <button style="border-radius:5px;" type="submit" class="btn btn-order__online d-block mx-auto py-2 px-2">Beli Layanan</button>
                            </form>
                        </div>
                        @else 
                    
                        <div class="col-md-4 d-flex align-items-end">
                            <form onsubmit="return false;" class="form-konten" id="form-{{$item->id}}">
                                <input type="hidden" name="md_plugin_id" value="{{$item->id}}">
                                <input type="hidden" name="csrf_token" value="{{csrf_token()}}">
                                <button style="border-radius:5px;" type="submit" class="btn btn-order__online d-block mx-auto py-2 px-2">Perpanjang</button>
                            </form>
                        </div>
                    
                        @endif
                    </div>
                </div>
                    </div>
            @endforeach   
          </div>  
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.form-konten').submit(function () {
            var data = getFormData($(this).attr('id'));

            ajaxTransfer("{{route('plugin.create-invoice')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    });
</script>

@endsection

@extends('backend-v3.layout.main')
@section('title',$title)
@section('css')
<style>
.setting-sidebar-ul .list-group-item {
    border: none!important;
}
.setting-sidebar-ul .list-group-item.active {
    background: #FFE6D1!important;
}

.setting-sidebar-ul .list-group-item a {
    color: #4F4F4F!important;
    font-size: 14px;
    font-weight: 500;
}

.setting-sidebar-ul .list-group-item.active a {
    color: #FF9743!important;
}

</style>
@endsection
@section('content')
<!-- Page Heading -->
<div class="page-header">
    <div class="container-fluid d-flex justify-content-between align-items-center">
        <div class="p-5">
            <h4 class="mb-1">{{$title}}</h4>
            <span>Disini kamu bisa mengatur plugin yang tersedia </span>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="card shadow mb-4 py-4 px-4">
        <div class="row">
            <div class="col-lg-3 col-md-12 mb-3">
                <ul class="list-group setting-sidebar-ul">
                    <li class="list-group-item">
                        <a class="nav-link" id="home-tab"  href="{{route('merchant.toko.profile.detail',['id'=>$merchant_id])}}" role="tab" aria-controls="home" aria-selected="true">Info Usaha Kamu</a>
                    </li>
                    <li class="list-group-item">
                        <a class="nav-link" id="qris-tab"  href="{{route('merchant.toko.profile.detail',['id'=>$merchant_id])}}?page=digital-payment" role="tab" aria-controls="qris" aria-selected="true">Aktivasi Metode Pembayaran</a>
                    </li>
                    <li class="list-group-item">
                        <a class="nav-link" id="inv-tab"  href="{{route('merchant.toko.profile.detail',['id'=>$merchant_id])}}?page=inventory" role="tab" aria-controls="inv" aria-selected="true">Metode Persediaan</a>
                    </li>
                    {{--<li class="list-group-item">
                        <a class="nav-link" id="inv-tab"  href="{{route('merchant.toko.profile.detail',['id'=>$merchant_id])}}?page=orderOnline" role="tab" aria-controls="inv" aria-selected="true">Order Online</a>
                    </li>--}}
                    <li class="list-group-item">
                        <a onclick="activeMenu('pengaturan','pengaturan-reset-data')" class="nav-link" href="{{route('merchant.reset-data.add')}}">Reset Data</a>
                    </li>
                    <li class="list-group-item active">
                        <a onclick="activeMenu('pengaturan','pengaturan')" class="nav-link" href="{{route('plugin-gcontact.index')}}">Google </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-9 col-md-12 mb-3" style="border-left:1px solid rgba(186, 186, 186, 0.32);">
                <div class="mb-3">
                    <h4 class="font-weight-bold" style="color:#323232;">Pengaturan Plugin</h4>
                    <span style="color:#979797">Konfigurasi plugin yang tersedia</span>
                    <hr>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($data as $key => $item)
                        <div class="col-md-12 mt-3">
                            <div class="card p-4">
                                <div class="row no-gutters">
                                    <div class="col-xl-2 col-lg-12 mb-3 d-flex justify-content-center align-items-center">
                                        <div class="order-online__upper text-center">
                                            <img src="{{env('S3_URL')}}{{$item->getPlugin->icon}}" class="img-fluid mb-2" alt="{{$item->getPlugin->name}}">
                                            {{---@if($item->status == 1)
                                            <span class="d-block mb-4">Status: Aktif</span>
                                            @else
                                            <span class="d-block mb-4">Status: Belum Aktif</span>
                                            @endif---}}
                                        </div>
                                    </div>
                                    <div class="col-xl-7 col-lg-12 d-flex align-items-center px-4 mb-3">
                                        <div>
                                            <h4 style="color:#747474;" class="font-weight-bold">{{$item->getPlugin->name}}</h4>
                                            <div style="color:#959595!important; font-size:12px; margin-bottom:10px;" class="p-desc">
                                                {!!$item->getPlugin->description!!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-lg-12 d-flex align-items-end justify-content-center">
                                        <button onclick='loadModalFullScreen(this)' target="{{route('plugin-setting.configure',['id'=>$item->id])}}" class='btn btn-order__online d-block mx-auto' title='Konfigurasi'>
                                            Konfigurasi
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

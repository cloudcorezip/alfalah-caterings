<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal form-konten' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1">Email</label>
        <input type="name" class="form-control" name="email" placeholder="Email" value="{{$data->email}}">
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Client ID</label>
        <input type="name" class="form-control" name="client_id" placeholder="client id" value="{{$data->client_id}}">
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Client Secret</label>
        <input type="name" class="form-control" name="client_secret" placeholder="client secret" value="{{$data->client_secret}}">
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Refresh Token</label>
        <input type="name" class="form-control" name="client_token" placeholder="client token" value="{{$data->client_token}}">
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('plugin-setting.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url)
            });
        })
    })

    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Konfigurasi plugin sebelum mulai digunakan</span>
                                    </div>`);
    });


</script>

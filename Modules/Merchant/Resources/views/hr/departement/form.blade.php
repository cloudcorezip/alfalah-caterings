<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-department" class='form-horizontal form-konten' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Departemen</label>
        <input type="name" class="form-control" name="name" placeholder="Departemen" value="{{$data->name}}" required>
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Sub Departemen dari:</label>
        <select class="form-control" name="parent_id" required id="parent">
            <option value="-1">Bukan Sub Departemen</option>
            @foreach($option as $item)
                @if($item->id!=$data->id)
                <option value="{{$item->id}}"
                @if($item->id==$data->parent_id) selected @endif
                >{{$item->departemen}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='no_reload' value='{{$no_reload}}'>
</form>

<script>
    $(document).ready(function () {
        $("#parent").select2()
        $('#form-department').submit(function () {
            var data = getFormData('form-department');
            ajaxTransfer("{{route('merchant.hr.departement.save')}}", data, function (response){
                var data = JSON.parse(response);
                if(data.no_reload == 1){
                    otherMessage(data.type,data.message);
                    closeModalAfterSuccess();
                } else {
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                }
            });
        })
    })
    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah departemen, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

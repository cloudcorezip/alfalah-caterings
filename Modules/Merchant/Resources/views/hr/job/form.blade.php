<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Jabatan</label>
        <input type="name" class="form-control" name="name" placeholder="Jabatan" value="{{$data->name}}" required>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.hr.job.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })
    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah jabatan, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

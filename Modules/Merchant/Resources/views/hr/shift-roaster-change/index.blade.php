@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .table.table-other thead th {
            border-top: none!important;
            border-left: none!important;
            border-bottom: none!important;
            color: #515151!important;
            padding: 18px;
            text-transform: capitalize;
        }

        .table.table-other td {
            border-top: none!important;
            border-left: none!important;
            border-bottom: 1px solid rgba(186, 186, 186, 0.32)!important;
            color: #7F7F7F;
            font-size: 14px;
            padding: 18px;
        }

        .table.table-other thead {
            color: #858796!important;
            background: #F6F6F6!important;
        }
        .text-grey-custom{
            color: #616e80;
            font-size: 10px;
        }

    </style>
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Human Resources</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.hr.job.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kelola jadwal dan pergantian shift karyawanmu disini</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a onclick="activeMenu('human-resources','shift-roaster')" class="nav-link" href="{{route('merchant.hr.shift-roaster.index')}}">{{$title}}</a>
            </li>
            <li class="nav-item">
                <a onclick="activeMenu('human-resources','shift-roaster-change')" class="nav-link active" href="{{route('merchant.hr.shift-roaster-change.index')}}">Pergantian Shift</a>
            </li>
        </ul>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row" style="margin-bottom: 10px">
                    {!! $add !!}
                </div>
                <div id="output-sale-order">
                    @include('merchant::hr.shift-roaster-change.list')
                </div>
            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>
    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold mt-3 text-dark">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" class="px-30" onsubmit="return false">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="date" class="form-control form-control trans_time_custom" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="date" class="form-control form-control trans_time_custom" id="end_date" value="{{$endDate}}" style="background: white" name="end_date" required>

                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="exampleInputPassword1">Pilih Cabang</label>
                    <select name="md_merchant_id" id="branch" class="form-control form-control"required multiple>
                        <option value="-1" @if(in_array('-1',$merchantId)) selected @endif>Semua Cabang</option>
                    @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}">{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="row">
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <div class="col-md-12 mb-3 mt-3">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $("#branch").select2()
            $(".selectalldata").select2()

            $("#from_init").select2({})

            $('#form-filter-sale-order').submit(function () {
                var startDate = $('#start_date').val();
                var endDate = $('#end_date').val();

                let selectedMerchantId = $("#branch").select2('data');
                let merchantIds = [];

                selectedMerchantId.forEach(item => {
                    merchantIds.push(parseInt(item.id));
                });

                var data = getFormData('form-filter-sale-order');
                data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
                data.append('md_merchant_id',JSON.stringify(merchantIds));
                ajaxTransfer("{{route('merchant.hr.shift-roaster-change.reload-data')}}", data, '#output-sale-order');
                showFilter('btn-show-filter3', 'form-filter3');
            });
        })


        $("#branch").on("select2:select", function(e){
            if(e.params.data.id == "-1"){
                $("#branch > option[value !='-1']").prop("selected",false).trigger("change");
            } else {
                $("#branch > option[value ='-1']").prop("selected",false).trigger("change");
            }
        });


        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('merchant.hr.shift-roaster-change.delete')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                });
            })
        }

        function acceptOrRejectData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin memproses data ini ?", function () {
                ajaxTransfer("{{route('merchant.hr.shift-roaster-change.accept-or-reject')}}", data,function (response){
                    var data = JSON.parse(response);
                    toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
                });
            })
        }

        {{--const exportData = () => {--}}
        {{--    modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {--}}
        {{--        const data = new FormData();--}}
        {{--        data.append('start_date','{{$startDate}}');--}}
        {{--        data.append('end_date','{{$endDate}}');--}}
        {{--        ajaxTransfer("{{route('merchant.hr.shift-roaster.export-data')}}", data, '#modal-output');--}}
        {{--    });--}}

        {{--}--}}
    </script>
@endsection

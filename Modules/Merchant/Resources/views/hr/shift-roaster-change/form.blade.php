
<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info " role="alert">
                <i class="fa fa-info-circle mr-2"></i> Perhatian! Shift yang sudah ada akan digantikan.
            </div>
        </div>
        @include('merchant::component.branch-selection',
    ['withoutModal'=>false,
    'data'=>new \Modules\Merchant\Models\ShiftRoaster(),
    'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
    'selector'=>'branch-shift',
    'with_onchange'=>true,
    'function_onchange'=>'getEmployee()'

    ])
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Departemen</label>
                <select class="form-control selectalldata" name="departement_id" required id="depid" onchange="getEmployee()">
                    <option value="-1">Pilih Departemen</option>
                    @foreach($departement as $item)
                        <option value="{{$item->id}}"
                        >{{$item->departemen}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold"> Pindah ke Shift</label>
                <select class="form-control selectalldata" name="shift_id" required id="shiftid">
                    @foreach($shift as $item)
                        <option value="{{$item->id}}"
                        >{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Tanggal</label>
                <input type="date" class="form-control" name="shift_change_date" placeholder="Pilih Tanggal" required>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Pilih Karyawan</label>
                <select class="form-control selectalldata" id="staffid" required>
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Karyawan Pengganti</label>
                <select class="form-control selectalldata"  id="staffchangeid">
                </select>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Alasan</label>
               <textarea class="form-control" name="reason">

               </textarea>
            </div>
        </div>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>

</form>

<script>
    $(document).ready(function () {
        $(".selectalldata").select2()
        getEmployee()
        getEmployeeChange()
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            let selectedStaff = ($("#staffid").select2('data'))? $("#staffid").select2('data'):[];
            let selectedStaffChange = ($("#staffchangeid").select2('data'))? $("#staffchangeid").select2('data'):[];

            if(selectedStaff.length>0)
            {
                data.append('staff_id',selectedStaff[0]['other_id']);
            }

            console.log(selectedStaffChange)

            if(selectedStaffChange.length>0){
                data.append('to_staff_id',selectedStaffChange[0]['other_id']);
            }else{
                data.append('to_staff_id','-1')
            }

            ajaxTransfer("{{route('merchant.hr.shift-roaster-change.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah pergantian jadwal shift, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });

    $("#branch-shift").change(function (){
        $('#staffid').val(null).trigger('change');
        $('#staffchangeid').val(null).trigger('change');
        getEmployee()
        getEmployeeChange()
    })

    $("#depid").change(function (){
        $('#staffid').val(null).trigger('change');
        $('#staffchangeid').val(null).trigger('change');
        getEmployee()
        getEmployeeChange()
    })

    function getEmployee()
    {
        let branch = $("#branch-shift").val()
        let dep = $("#depid").val()
        $("#staffid").select2({
            ajax: {
                type: "GET",
                url: "{{route('merchant.hr.shift-roaster.employee-list')}}?md_merchant_id="+branch+"&departement_id="+dep,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }

    function getEmployeeChange()
    {
        let branch = $("#branch-shift").val();
        if(branch=='-1')
        {
            otherMessage('Silahkan pilih cabang terlebih dahulu')
        }else{
            let dep = $("#depid").val()
            $("#staffchangeid").select2({
                ajax: {
                    type: "GET",
                    url: "{{route('merchant.hr.shift-roaster.employee-list')}}?md_merchant_id="+branch+"&departement_id="+dep,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            key: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                },
            });
        }
    }





</script>

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Hari Libur <small style="color: red">*</small></label>
                <input type="date" class="form-control" name="holiday_date" placeholder="Departemen" value="{{$data->holiday_date}}" required>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Keterangan</label>
                <input type="text" class="form-control" name="occasion" placeholder="Keterangan" value="{{$data->occasion}}" required>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group row">
                <label for="exampleInputPassword1" class="font-weight-bold col-md-7">Libur Nasional?</label>
               <div class="col-md-5">
                   <div class="custom-control custom-switch">
                       <input
                           type="checkbox"
                           class="custom-control-input check-bank"
                           id="is_national_holiday"
                           name="is_national_holiday"
                           value="{{($data->is_national_holiday==1)?1:0}}"
                           style="transform:scale(2)"
                           {{($data->is_national_holiday==1)?'checked':''}}
                       >
                       <label class="custom-control-label" for="is_national_holiday"></label>
                   </div>
               </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group row">
                <label for="exampleInputPassword1" class="font-weight-bold col-md-7">Cuti Bersama?</label>
                <div class="col-md-5">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input check-bank"
                            id="is_leave"
                            name="is_leave"
                            value="{{($data->is_leave==1)?1:0}}"
                            style="transform:scale(2)"
                            {{($data->is_leave==1)?'checked':''}}
                        >
                        <label class="custom-control-label" for="is_leave"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group row">
                <label for="exampleInputPassword1" class="font-weight-bold col-md-7">Tetap Bekerja?</label>
                <div class="col-md-5">
                    <div class="custom-control custom-switch">
                        <input
                            type="checkbox"
                            class="custom-control-input check-bank"
                            id="is_work"
                            name="is_work"
                            value="{{($data->is_work==1)?1:0}}"
                            style="transform:scale(2)"
                            {{($data->is_work==1)?'checked':''}}
                        >
                        <label class="custom-control-label" for="is_work"></label>
                    </div>
                </div>
            </div>
        </div>

    </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
            <button class="btn btn-success py-2 px-4">Simpan</button>
        </div>
        <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='is_leaves' id="is_leaves" value='{{is_null($data->id)?0:$data->is_leave }}'>
    <input type='hidden' name='is_works' id="is_works" value='{{is_null($data->id)?0:$data->is_work }}'>
    <input type='hidden' name='is_national_holidays' id="is_national_holidays" value='{{is_null($data->id)?0:$data->is_national_holiday }}'>

    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.hr.holiday.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })
    $(document).ready(function(){
        $('.modal-dialog').addClass('modal-dialog-centered');
        $("#is_national_holiday").change(function (){
            if(this.checked) {
                $("#is_national_holidays").val(1)

            }else{
                $("#is_national_holidays").val(0)

            }
        })
        $("#is_leave").change(function (){
            if(this.checked) {
                $("#is_leaves").val(1)

            }else{
                $("#is_leaves").val(0)

            }
        })

        $("#is_work").change(function (){
            if(this.checked) {
                $("#is_works").val(1)

            }else{
                $("#is_works").val(0)

            }
        })
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah hari libur, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1" class="font-weight-bold">Jadwal Shift</label>
        <select class="form-control" name="shift_id" required id="parent">
            @foreach($shift as $item)
                <option value="{{$item->id}}"
                        @if($item->id==$data->shift_id) selected @endif
                >{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="modal-footer">
        @if(!is_null($data->id))
            <button type="button" class="btn btn-danger py-2 px-4" onclick="deleteData({{$data->id}})">
                <i class="fa fa-trash-alt mr-2"></i> Hapus
            </button>

        @else
            <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>

        @endif
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='staff_id' value='{{$staffId }}'>
    <input type='hidden' name='user_id' value='{{$userId }}'>
    <input type='hidden' name='md_merchant_id' value='{{$merchantId }}'>
    <input type='hidden' name='date' value='{{$date }}'>

    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $("#parent").select2()
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('merchant.hr.shift-roaster.save')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })

    function deleteData(id) {
        var data = new FormData();
        data.append('id', id);

        modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
            ajaxTransfer("{{route('merchant.hr.shift-roaster.delete')}}", data, function (response){
                var data = JSON.parse(response);
                toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    }
    $(document).ready(function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-dialog-centered');
    });

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah jadwal shift, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


</script>

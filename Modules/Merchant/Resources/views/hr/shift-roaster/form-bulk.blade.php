
<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info " role="alert">
               <i class="fa fa-info-circle mr-2"></i> Perhatian! Shift yang sudah ada akan digantikan.
            </div>
        </div>
        @include('merchant::component.branch-selection',
    ['withoutModal'=>false,
    'data'=>new \Modules\Merchant\Models\ShiftRoaster(),
    'subTitle'=>'Tentukan cabang mana yang akan melakukan '.strtolower($title).'',
    'selector'=>'branch-shift',
    'with_onchange'=>true,
    'function_onchange'=>'getEmployee()'

    ])

        <div class="col-md-3">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Jadwal Shift</label>
                <select class="form-control selectalldata" name="shift_id" required id="shiftid">
                    @foreach($shift as $item)
                        <option value="{{$item->id}}"
                        >{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">By Tanggal/Bulan</label>
                <div class="d-flex">
                    <div class="form-check-inline custom-control custom-radio mt-2 mr-3">
                        <input type="radio" value="date" class="custom-control-input" id="mark_attendance_by_dates" name="assign_shift_by" checked="" autocomplete="off">
                        <label class="custom-control-label pt-1 cursor-pointer" for="mark_attendance_by_dates">Tanggal</label>
                    </div>
                    <div class="form-check-inline custom-control custom-radio mt-2 mr-3">
                        <input type="radio" value="month" class="custom-control-input" id="mark_attendance_by_month" name="assign_shift_by" autocomplete="off">
                        <label class="custom-control-label pt-1 cursor-pointer" for="mark_attendance_by_month">Bulan</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Departemen</label>
                <select class="form-control selectalldata" name="departement_id" required id="depid" onchange="getEmployee()">
                    <option value="-1">Semua Departemen</option>
                    @foreach($departement as $item)
                        <option value="{{$item->id}}"
                        >{{$item->departemen}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6 bydate">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Tanggal</label>
                <input type="text" class="form-control" id="date-multiple" placeholder="Pilih banyak tanggal">
            </div>
        </div>

        <div class="col-md-3 byyear">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Tahun</label>
                <select class="form-control selectalldata" name="year_id">
                    <option value="{{date('Y')}}">{{date('Y')}}</option>
                    <option value="{{date('Y')+1}}">{{date('Y')+1}}</option>
                </select>
            </div>
        </div>
        <div class="col-md-3 byyear">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Bulan</label>
                <select class="form-control selectalldata" name="month_id">
                    <option value="01">Januari</option>
                    <option value="02">Febuari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="exampleInputPassword1" class="font-weight-bold">Pilih Karyawan</label>
                <select class="form-control selectalldata" required id="staffid" multiple>
                </select>
            </div>
        </div>






    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-light py-2 px-4" data-dismiss="modal">Tutup</button>
        <button class="btn btn-success py-2 px-4">Simpan</button>
    </div>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <input type='hidden' name='is_by_month' id="by_month_value" value='0'>

</form>

<script>
    $(function() {
        $('#date-multiple').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
        });
    });
    $(document).ready(function () {
        $(".selectalldata").select2()
        $(".byyear").hide()
        getEmployee()
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            var startDate = $('#date-multiple').data('daterangepicker').startDate._d;
            var endDate = $('#date-multiple').data('daterangepicker').endDate._d;
            data.append('start_date',moment(startDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
            data.append('end_date',moment(endDate,'YYYY-MM-DD').format('YYYY-MM-DD'));
            let selectedStaff = ($("#staffid").select2('data'))? $("#staffid").select2('data'):[];
            let staffIds = [];

            selectedStaff.forEach(item => {
                staffIds.push(item.other_id);
            });

            let staff_id=staffIds.join(',');

            data.append('staff_id',staff_id);

            ajaxTransfer("{{route('merchant.hr.shift-roaster.save-bulk')}}", data, function (response){
                var data = JSON.parse(response);
                 toastForSaveData(data.message,data.type,data.is_modal,data.redirect_url,data.is_with_datatable)
            });
        })
    })

    $(document).ready(function(){
        $('.modal-header').find('.modal-title').remove();
        $('.modal-header').addClass('py-5');
        $('.modal-header').prepend(`<div">
                                         <h4 class="modal-title">{{$title}}</h4>
                                        <span class="span-text">Untuk menambah jadwal shift, kamu harus mengisi data di bawah ini</span>
                                    </div>`);
    });


    $("#depid").change(function (){
        $('#staffid').val(null).trigger('change');
        getEmployee()
    })

    $("#branch-shift").change(function (){
        $('#staffid').val(null).trigger('change');
        getEmployee()
    })

    $("#mark_attendance_by_dates").click(function (){
        $(".bydate").show()
        $(".byyear").hide()
        $("#by_month_value").val(0)
    })
    $("#mark_attendance_by_month").click(function (){
        $(".bydate").hide()
        $(".byyear").show()
        $("#by_month_value").val(1)
    })



    function getEmployee()
    {
        let branch = $("#branch-shift").val()
        let dep = $("#depid").val()
        $("#staffid").select2({
            ajax: {
                type: "GET",
                url: "{{route('merchant.hr.shift-roaster.employee-list')}}?md_merchant_id="+branch+"&departement_id="+dep,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        key: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
        });
    }





</script>

@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <style>
        .table.table-other thead th {
            border-top: none!important;
            border-left: none!important;
            border-bottom: none!important;
            color: #515151!important;
            padding: 18px;
            text-transform: capitalize;
        }

        .table.table-other td {
            border-top: none!important;
            border-left: none!important;
            border-bottom: 1px solid rgba(186, 186, 186, 0.32)!important;
            color: #7F7F7F;
            font-size: 14px;
            padding: 18px;
        }

        .table.table-other thead {
            color: #858796!important;
            background: #F6F6F6!important;
        }
        .text-grey-custom{
            color: #616e80;
            font-size: 10px;
        }

    </style>
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid">
            <div class="p-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item d-flex align-items-center">
                            <a>Human Resources</a>
                        </li>
                        <li class="breadcrumb-item active d-flex align-items-center">
                            <a href="{{route('merchant.hr.job.index')}}">{{$title}}</a>
                        </li>
                    </ol>
                </nav>
                <span>Kelola jadwal dan pergantian shift karyawanmu disini</span>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        @include('merchant::alert')
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a onclick="activeMenu('human-resources','shift-roaster')" class="nav-link active" href="{{route('merchant.hr.shift-roaster.index')}}">{{$title}}</a>
            </li>
            <li class="nav-item">
                <a onclick="activeMenu('human-resources','shift-roaster-change')" class="nav-link" href="{{route('merchant.hr.shift-roaster-change.index')}}">Pergantian Shift</a>
            </li>
        </ul>
        <div class="card shadow mb-4">
            <div class="card-body">
                <table class="table table-borderless">
                    <tr>
                        <td class="text-left" style="padding: 0.1rem">
                            <button class="btn btn-success btn-rounded btn-sm  py-2 px-4" onclick="loadModalFullScreen(this)" target="{{route('merchant.hr.shift-roaster.add-bulk')}}?md_merchant_id={{$merchantId}}"><i class="fa fa-plus mr-2"></i> Tambah</button>
                            <a class="btn btn-info btn-rounded btn-sm py-2 px-4" onclick="exportData()" style="color: white"><i class="fa fa-download mr-2"></i> Download</a>


                        </td>
                        <td  class="text-right" style="padding: 0.1rem">
                            <h6><b>PERIODE {{strtoupper(\Carbon\Carbon::parse($startDate)->isoFormat('D MMMM Y'))}} - {{strtoupper(\Carbon\Carbon::parse($endDate)->isoFormat('D MMMM Y'))}}</b></h6>
                        </td>
                    </tr>
                </table>
                <div class="table-responsive">
                    <div class="table-basic">
                        <table class="table table-other">
                            <thead>
                            <tr style="background: #ebebeb">
                                <th class="font-weight-bold text-center" style="border-right: 1px solid rgba(123, 122, 122, 0.1);font-size: 14px">Karyawan</th>
                                @foreach($series as $s)
                                    <th class="text-center"><b style="font-size: 14px">{{$s->day}}</b> {{\Carbon\Carbon::parse($s->time)->isoFormat('dd')}}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($data)>0)
                                @foreach($data as $key =>$k)
                                    <tr>
                                        <td width="30%" style="border-right: 1px solid rgba(123, 122, 122, 0.1)">{{$k->fullname}} <span class="text-grey-custom">{{(is_null($k->job_name))?'':'['.$k->job_name.']'}}</span></td>
                                        @foreach(json_decode($k->roaster_list) as $r)
                                            <td class="text-center">
                                                @if($r->is_holiday==0)
                                                    @if($r->type_of_attendance=='S')
                                                        <a href="javascript:;" data-toggle="tooltip" data-original-title="Sakit">

                                                            <span class="iconify" data-icon="pepicons:leave-print" style="color: #1d82f5;" data-width="25" data-height="25"></span>
                                                        </a>

                                                    @elseif($r->type_of_attendance=='I')
                                                        <a href="javascript:;" data-toggle="tooltip" data-original-title="Izin/Cuti">
                                                            <span class="iconify" data-icon="pepicons:leave-print" data-toggle="tooltip" data-original-title="Izin/Cuti"  style="color: #1d82f5;" data-width="25" data-height="25"></span>
                                                        </a>

                                                    @else
                                                        @if(is_null($r->id))
                                                            <button type="button" class="btn btn-all-xs btn-xs" data-toggle="tooltip" data-original-title="Tambah Shift" onclick="loadModal(this)" target="{{route('merchant.hr.shift-roaster.add')}}?staff_id={{$k->staff_id}}&user_id={{$k->staff_user_id}}&md_merchant_id={{$k->md_merchant_id}}&date={{$r->time}}" style="background: #f8f9fa"><i class="fa fa-plus"></i></button>
                                                        @else
                                                            <button type="button" class="btn btn-all-xs btn-xs" data-toggle="tooltip" data-original-title="{{$r->name}}" style="background: {{$r->color}};color: white;font-size: 6px" onclick="loadModal(this)" target="{{route('merchant.hr.shift-roaster.add')}}?staff_id={{$k->staff_id}}&user_id={{$k->staff_user_id}}&md_merchant_id={{$k->md_merchant_id}}&date={{$r->time}}&id={{$r->id}}" >{{getShortSentence($r->name)}}</button>
                                                        @endif
                                                    @endif
                                                @else
                                                    <a href="javascript:;" data-toggle="tooltip" data-original-title="{{$r->occasion}}">
                                                        <span class="text-danger">Libur</span>
                                                    </a>
                                                @endif
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="{{count($series)+1}}">Data tidak tersedia</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                {{$data->withQueryString()->links('vendor.pagination.with-showing-entry')}}

            </div>
        </div>
    </div>

    <a class="btn-show-filter3" id="btn-show-filter3" rel="noopener noreferrer" onclick="showFilter('btn-show-filter3', 'form-filter3')">
        <div class="d-flex align-items-center">
            <i class="fas fa-filter mr-2 text-white"></i>
            <span class="text-white form-filter3-text-header">Filter</span>
        </div>
    </a>
    <div class="form-filter3" id="form-filter3">
        <div class="row">
            <div class="col-12">
                <div class="form-filter3-header py-3 px-30 d-flex align-items-center justify-content-between">
                    <div class="d-flex align-items-center">
                        <i class="fas fa-filter mr-2"></i>
                        <span class="text-white form-filter3-text-header">Filter</span>
                    </div>
                    <button type="button" class="close" aria-label="Close" onclick="showFilter('btn-show-filter3', 'form-filter3')">
                        <i style="font-size:14px;" class="fas fa-times text-white"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="row mb-3 px-30">
            <div class="col-12">
                <h4 class="font-weight-bold mt-3 text-dark">Filter Data</h4>
                <span class="text-span-desc">Tampilkan data sesuai dengan kebutuhanmu</span>
            </div>
        </div>
        <hr>
        <form id="form-filter-sale-order" class="px-30" method="get">
            <div class="row">

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Awal</label>
                        <input type="date" class="form-control form-control trans_time_custom" id="start_date" value="{{$startDate}}" style="margin-bottom: 3px;background: white" name="start_date">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Tanggal Akhir</label>
                        <input type="date" class="form-control form-control trans_time_custom" id="end_date" value="{{$endDate}}" style="background: white" name="end_date" required>

                    </div>
                </div>
                <div class="col-md-12 mb-3">
                    <label for="exampleInputPassword1">Pilih Cabang</label>
                    <select name="md_merchant_id" id="branch" class="form-control form-control"required>
                        @foreach(get_cabang() as $key  =>$item)
                            <option value="{{$item->id}}" @if($item->id==$merchantId) selected @endif>{{$item->nama_cabang}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12 mb-3">
                    <label for="exampleInputPassword1">Pilih Departemen</label>
                    <select name="departement_id" class="form-control form-control selectalldata" required>
                        <option value="-1">Semua Departemen</option>
                        @foreach($departement as $key  =>$item)
                            <option value="{{$item->id}}" @if($item->id==$departement_id) selected @endif>{{$item->departemen}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12 mb-3">
                    <label for="exampleInputPassword1">Pilih Jabatan</label>
                    <select name="job_id" class="form-control form-control selectalldata" required>
                        <option value="-1">Semua Jabatan</option>
                        @foreach($job as $key  =>$item)
                            <option value="{{$item->id}}" @if($item->id==$job_id) selected @endif>{{$item->jabatan}}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="row">
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
                <div class="col-md-12 mb-3 mt-3">
                    <button type="submit" class="btn btn-success py-2 px-4 btn-block">Terapkan</button>
                    <a href="{{route('merchant.hr.shift-roaster.index')}}" class="btn btn-light btn-block" data-original-title="" title="" rel="noopener noreferrer">Reset Data</a>

                </div>
            </div>
        </form>

    </div>
@endsection

@section('js')
    <script src="https://yidas.github.io/jquery-freeze-table/dist/js/freeze-table.js"></script>
    <script>
        $(document).ready(function() {
            $("#branch").select2()
            $(".selectalldata").select2()
            $(".table-basic").freezeTable();
        })

        const exportData = () => {
            modalConfirm('Export Data', 'Lanjutkan proses export data ?', function () {
                const data = new FormData();
                data.append('start_date','{{$startDate}}');
                data.append('end_date','{{$endDate}}');
                data.append('md_merchant_id',{{$merchantId}});
                data.append('job_id',{{$job_id}})
                data.append('departement_id',{{$departement_id}})
                ajaxTransfer("{{route('merchant.hr.shift-roaster.export-data')}}", data, '#modal-output');
            });

        }
    </script>
@endsection

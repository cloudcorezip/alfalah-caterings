@extends('backend-v2.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{$title}}</h1>
        <div class="dropdown">
            <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Filter Tanggal
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" onclick="getFilterDate('today')">Hari Ini</a>
                <a class="dropdown-item" onclick="getFilterDate('yesterday')">Kemarin</a>
                <a class="dropdown-item" onclick="getFilterDate('thisweek')">Minggu Ini</a>
                <a class="dropdown-item" onclick="getFilterDate('lastweek')">Minggu Lalu</a>
                <a class="dropdown-item" onclick="getFilterDate('thismonth')">Bulan Ini</a>
                <a class="dropdown-item" onclick="getFilterDate('lastmonth')">Bulan Lalu</a>
                <a class="dropdown-item" onclick="getFilterDate('thisyear')">Tahun Ini</a>
                <a class="dropdown-item" onclick="getFilterDate('lastyear')">Tahun Lalu</a>
            </div>
        </div>
    </div>
    <!-- Content Row -->
    <div class="row">

        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Transaksi</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="transaction">0</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-shopping-cart fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Pendapatan</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="income">Rp 0</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Komisi Untuk Senna</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="profit">Rp 0</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Pending Requests Card Example -->

    </div>

    <!-- Content Row -->

    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Pendapatan</h6>
                    {{--                    <div class="dropdown no-arrow">--}}
                    {{--                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>--}}
                    {{--                        </a>--}}
                    {{--                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">--}}
                    {{--                            <div class="dropdown-header">Dropdown Header:</div>--}}
                    {{--                            <a class="dropdown-item" href="#">Action</a>--}}
                    {{--                            <a class="dropdown-item" href="#">Another action</a>--}}
                    {{--                            <div class="dropdown-divider"></div>--}}
                    {{--                            <a class="dropdown-item" href="#">Something else here</a>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div id="container-1"></div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Transaksi Tiap Waktu</h6>
                    {{--                    <div class="dropdown no-arrow">--}}
                    {{--                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>--}}
                    {{--                        </a>--}}
                    {{--                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">--}}
                    {{--                            <div class="dropdown-header">Dropdown Header:</div>--}}
                    {{--                            <a class="dropdown-item" href="#">Action</a>--}}
                    {{--                            <a class="dropdown-item" href="#">Another action</a>--}}
                    {{--                            <div class="dropdown-divider"></div>--}}
                    {{--                            <a class="dropdown-item" href="#">Something else here</a>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div id="container-2"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Row -->

@endsection
@section('js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="{{asset('public/backend/core/')}}/accounting.js"></script>

    <script>
        $( document ).ready(function() {
            getFilterDate();
        });
        function getFilterDate($filterName=null,$customDate=null)
        {
            $.ajax({
                url: "{{route('jasa.report.general',['merchantId'=>merchant_id()])}}",
                type: 'post',
                data:{
                    "period_filter":$filterName,
                    "date_custom":$customDate
                },
                headers: {
                    'senna-auth':"{{get_user_token()}}"
                },
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    if(data.code==200)
                    {
                        $("#transaction").html(data.data.dashboard[0].transaction_amount);
                        $("#income").html(convertToRupiah(parseFloat(data.data.dashboard[0].income)));
                        $("#profit").html(convertToRupiah(parseFloat(data.data.dashboard[0].commission_for_senna)));
                        getChart(data.data.chart);
                    }
                }
            });

        }

        function getChart(data)
        {
            var income=[];
            var transaction=[];
            var categories=[];
            $.each(data,function (i,v) {
                income.push(parseFloat(v.total_price));
                categories.push(v.time);
                transaction.push(parseFloat(v.transaction_amount));

            })
            console.log(income);
            Highcharts.chart('container-1', {
                title: {
                    text: ''
                },

                subtitle: {
                    text: ''
                },

                yAxis: {
                    title: {
                        text: ''
                    }
                },

                xAxis: {
                    categories: categories
                },

                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },

                series: [{
                    name: 'Pendapatan',
                    data: income
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
            Highcharts.chart('container-2', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    title: {
                        text: 'Transaksi'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Transaksi',
                    data: transaction
                },]
            });
        }

        function convertToRupiah(angka)
        {
            return accounting.formatMoney(angka, "Rp", 2, ".", ",")
        }

    </script>
@endsection

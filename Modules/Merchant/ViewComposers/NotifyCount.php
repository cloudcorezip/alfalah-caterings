<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\ViewComposers;


use App\Models\MasterData\SennaPayment\TransactionStatus;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\UserActivityNotification;
use App\Models\SennaPayment\Transaction;
use Illuminate\View\View;

class NotifyCount
{
    public function compose(View $view)
    {

        $excludedViews=[
            // 'frontend.page.home',
            // 'frontend.page.features',
            // 'frontend.page.package',
            // 'frontend.page.contact',
            // 'frontend.page.demo',
            // 'frontend.page.term',
            // 'frontend.page.kebijakan',
            // 'login.index',
            // 'register.index',
            // 'forgot-password.index',
            // 'frontend.layout.main',
            // 'frontend.layout.partial.head',
            // 'frontend.layout.partial.nav',
            // 'frontend.layout.partial.footer',
            // 'blog::user.index',
            // 'pagination::bootstrap-4',
            // 'blog.main',
            // 'backend-v3.layout.menu',
            // 'backend-v3.layout.menu-merchant-v3',
            'backend-v3.layout.header'
        ];
        if(in_array($view->getName() , $excludedViews)==true)
        {
            try{
                $count=UserActivityNotification::where('md_user_id',user_id())
                    ->where('is_read',0)->count();
            }catch (\Exception $e)
            {
                $count=0;
            }
            $view->with('countNotifyMerchant',$count);

        }


    }

}

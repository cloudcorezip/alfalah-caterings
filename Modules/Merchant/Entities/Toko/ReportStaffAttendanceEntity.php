<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\StaffAttendance;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReportStaffAttendanceEntity extends StaffAttendance
{
    public static function getDataForDataTable($merchant_id)
    {

        try {
            return StaffAttendance::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_staff_attendances.id)'),
                'md_merchant_staff_attendances.*',
                'md_merchant_staff_attendances.md_staff_user_id',
                't.fullname as nama_staff',
                'md_merchant_staff_attendances.start_work as waktu_masuk',
                'md_merchant_staff_attendances.end_work as waktu_selesai',
                'md_merchant_staff_attendances.type_of_attendance as status_masuk'
            ])
                ->join('md_users as t','t.id','md_merchant_staff_attendances.md_staff_user_id')
                ->where('md_merchant_staff_attendances.md_merchant_id',$merchant_id)
                ->where('md_merchant_staff_attendances.is_deleted',0)
                ->orderBy('md_merchant_staff_attendances.created_at','desc')
                ;
        } catch (\Exception $e) {
            return [];

        }

    }

    public static function dataTable($request, $isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable($request->merchant_id);
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("md_merchant_staff_attendances.created_at::date between '$startDate' and '$endDate'
        ");

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(t.fullname) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi', 'status_masuk'],$isEdit,$isDelete,$isHead)
            ->editColumn('status_masuk',function ($list){
                if($list->status_masuk=='S'){
                    return '<h6><span class="badge badge-info">Izin Sakit</span></h6>';
                }else if($list->status_masuk=='I'){
                    return '<h6><span class="badge badge-info">Izin</span></h6>';
                }else if($list->status_masuk=='A'){
                    return '<h6><span class="badge badge-danger">Tidak Hadir</span></h6>';
                }else{
                    return '<h6><span class="badge badge-success">Hadir</span></h6>';
                }
            })
            ->editColumn('waktu_masuk',function ($list){
            if(is_null($list->waktu_masuk) && $list->status_masuk=='H')
                    {
                        return Carbon::parse($list->created_at)->isoFormat('dddd, D MMMM Y, HH:mm');

                    }else if(!is_null($list->waktu_masuk) && $list->status_masuk=='H'){
                        return Carbon::parse($list->waktu_masuk)->isoFormat('dddd, D MMMM Y, HH:mm');

                    }else{
                        return '-';

                    }
                })
            ->editColumn('waktu_selesai',function ($list){
                return (is_null($list->waktu_selesai))?'-':\Carbon\Carbon::parse($list->waktu_selesai)->isoFormat('dddd, D MMMM Y, HH:mm');
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'nama_staff',
            'waktu_masuk',
            'waktu_selesai',
            'status_masuk',
            'opsi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'md_staff_user_id'=>'md_merchant_staff_attendances.md_staff_user_id',
            'type_of_attendance' => 'type_of_attendance'
        ];
        return $field;
    }

    public static function getButton($list){

         return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.report.staff-attendance.detail-attendance',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                    <span class='fa fa-eye' style='color: white'></span>
                </a>
                <a href='".route('merchant.toko.report.staff-attendance.edit-attendance',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Edit'>
                    <span class='fa fa-edit' style='color: white'></span>
                </a>";
    }
}

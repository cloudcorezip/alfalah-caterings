<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantGradeStaff;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class GradeEntity extends MerchantGradeStaff
{

    /**
     * Senna Apps
     * Copyright (c) 2020.
     */
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('employee', merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return MerchantGradeStaff::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY merchant_grade_staffs.id)'),
                'merchant_grade_staffs.id',
                'merchant_grade_staffs.grade_name as nama_grade',
                'merchant_grade_staffs.basic_salary as gaji_pokok',
                'merchant_grade_staffs.desc as deskripsi',
                'merchant_grade_staffs.md_merchant_id',
                'merchant_grade_staffs.md_merchant_id as berlaku_untuk_cabang',
                'merchant_grade_staffs.md_user_id',
                'm.id as merchant_id',
                'merchant_grade_staffs.parent_id as sub_grade_dari'
            ])
                ->join('md_users as u', 'u.id', 'merchant_grade_staffs.md_user_id')
                ->join('md_merchants as m', 'm.md_user_id', 'u.id')
                ->orderBy('merchant_grade_staffs.id', 'desc');

        } catch (\Exception $e) {

            return [];

        }

    }


    public static function listOption()
    {
        $getBranch = MerchantUtil::getBranch(merchant_id(),1);
        if(merchant_detail()->is_branch==0)
        {
            return  self::getDataForDataTable()->whereIn('m.id', $getBranch)->where('merchant_grade_staffs.is_deleted', 0)->get();

        }else{
            return  self::getDataForDataTable()->whereIn('m.id', $getBranch)
                ->where(function($q){
                    $q->where("merchant_grade_staffs.md_merchant_id", '@>', '[' . json_encode(['id' =>"-1"]) . ']')
                    ->orWhere("merchant_grade_staffs.md_merchant_id", '@>', '[' . json_encode(['id' =>"".merchant_id().""]) . ']');
                })
                ->where('merchant_grade_staffs.is_deleted', 0)
                ->get();

        }
    }

    public static function dataTable($request, $isEdit = true, $isDelete = true, $isHead = true)
    {
        $data = self::getDataForDataTable();
        if ($request->input('search')['value']) {
            $search = $request->input('search')['value'];
            $data->where(function ($query) use ($search) {
                $query->whereRaw("lower(merchant_grade_staffs.grade_name) like '%" . strtolower(str_replace("'", "''", $search)) . "%' ")
                    ->orWhereRaw("lower(merchant_grade_staffs.desc) like '%" . strtolower(str_replace("'", "''", $search)) . "%'");
            });
        }
        // dd($request->input('search')['value']);
        $getBranch = MerchantUtil::getBranch(merchant_id(),1);
        $data->whereIn('m.id', $getBranch)
            ->where(function($q){
                $q->where("merchant_grade_staffs.md_merchant_id", '@>', '[' . json_encode(['id' =>"-1"]) . ']')
                ->orWhere("merchant_grade_staffs.md_merchant_id", '@>', '[' . json_encode(['id' =>"".merchant_id().""]) . ']');
            })
            ->where('merchant_grade_staffs.is_deleted', 0);

        return (DataTable::getInstance())->dataTableForMerchantV2(self::class, $data, ['berlaku_untuk_cabang','opsi'], $isEdit, $isDelete, $isHead)
            ->editColumn('berlaku_untuk_cabang',function ($list){
                $merchants=json_decode($list->berlaku_untuk_cabang);
                $items=[];


                foreach ($merchants as $item)
                {
                    if($item->id==-1 || $item->id=='-1'){
                        $items[]=(int)$item->id;
                        break;
                    }else{
                        $items[]=(int)$item->id;
                    }
                }
                $branch=Merchant::select('name')->whereIn('id',$items)->get();

                if(in_array(-1,$items)){
                    return "<label class='badge badge-info'>Untuk Semua Cabang</label>";
                }else{
                    $html="";
                    foreach ($branch as $b){
                        $html.="<li>".$b->name."</li>";
                    }
                    return $html;
                }

            })
            ->editColumn('gaji_pokok',function ($list){
                return rupiah($list->gaji_pokok);
            })
            ->editColumn('sub_grade_dari',function ($list){
                return is_null($list->sub_grade_dari)?'-':MerchantGradeStaff::find($list->sub_grade_dari)->grade_name;
            })
            ->make(true);

    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1)
        {
            return [
                'row_number',
                'nama_grade',
                'gaji_pokok',
                'sub_grade_dari',
                'deskripsi',
                'berlaku_untuk_cabang',
                'opsi'
            ];
        }else{
            return [
                'row_number',
                'nama_grade',
                'sub_grade_dari',
                'deskripsi',
                'opsi'
            ];
        }

    }


    public static function getButton($list, $edit, $delete, $isHead)
    {

        if ($list->merchant_id == merchant_id()) {
            if ($isHead == true) {
                return "<a  onclick='loadModalFullScreen(this)' target='" . route('merchant.toko.grade.add', ['id' => $list->id]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            } else {
                if ($edit == true && $delete == false) {
                    return "<a  onclick='loadModalFullScreen(this)' target='" . route('merchant.toko.grade.add', ['id' => $list->id]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                } elseif ($edit == false && $delete == true) {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                } elseif ($edit == false && $delete == false) {
                    return "";
                } else {
                    return "<a  onclick='loadModalFullScreen(this)' target='" . route('merchant.toko.grade.add', ['id' => $list->id]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if ($permission == true) {
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="' . route('merchant.toko.grade.add') . '"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }else if (merchant_detail()->is_branch == 0) {
            if ($permission == true) {
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="' . route('merchant.toko.grade.add') . '"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        } else {
            if (self::getAccess() == true) {
                if ($permission == true) {
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="' . route('merchant.toko.grade.add') . '"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            } else {
                return "";
            }
        }

    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\PurchaseOrder;
use Illuminate\Support\Facades\DB;

class StockFlowEntity extends Product
{

    public static function getDataForDataTable($startDate,$endDate)
    {

        try {
            return Product::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_products.id)'),
                'sc_products.id',
                'sc_products.code',
                'sc_products.name as nama_produk',
                DB::raw("
                coalesce(
                (
                  select
                    sum(ssi.total)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sc_products.id
                    and type = 'in'
                    and ssi.created_at :: date between '$startDate' and '$endDate'
                    and ssi.transaction_action not like '%Retur%'
                ),
                0
              ) as stok_masuk,
              coalesce(
                (
                  select
                   round(CAST(sum(ssi.total) as numeric),2)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sc_products.id
                    and type = 'out'
                    and ssi.created_at :: date  between '$startDate' and '$endDate'
                ),
                0
              )-  coalesce(
                (
                  select
                    sum(ssi.total)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sc_products.id
                    and type = 'in'
                    and ssi.created_at :: date between '$startDate' and '$endDate'
                    and ssi.transaction_action  like '%Retur%'
                ),
                0
              ) as stok_keluar,
              coalesce(
                (
                  select
                    sum(ssi.total)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sc_products.id
                    and type = 'in'
                    and ssi.transaction_action not like '%Retur%'
                ),
                0
              )- (coalesce(
                (
                  select
                    sum(ssi.total)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sc_products.id
                    and type = 'out'
                    and ssi.is_deleted = 0

                ),
                0
              )-coalesce(
                (
                  select
                    sum(ssi.total)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sc_products.id
                    and type = 'in'
                    and ssi.transaction_action like '%Retur%'
                ),
                0
              )) as sisa_stok

                ")
            ])
                ->leftJoin('md_users as u','u.id','sc_products.md_user_id')
                ->leftJoin('md_merchants as m','m.md_user_id','u.id')
                ->where('m.id',merchant_id())
                ->where('sc_products.is_deleted',0)
                ->where('sc_products.is_with_stock',1)
                ;
        } catch (\Exception $e) {

            return [];

        }

    }

    public static function dataTable($request)
    {
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

        $data=self::getDataForDataTable($startDate,$endDate);
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        return(DataTable::getInstance())->dataTable(self::class,$data,$request,[],
            ['sc_products.code','sc_products.name'])
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'code',
            'nama_produk',
            'stok_masuk',
            'stok_keluar',
            'sisa_stok',
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'sc_product_id'=>'sc_products.id',
        ];
        return $field;
    }

    public static function getButton($list){

    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko\Clinic;



use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Merk;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductSellingLevel;
use App\Models\SennaToko\StockInventory;
use App\Utils\BranchConfig\BranchConfigUtil;
use Illuminate\Support\Facades\DB;

class ClinicServiceEntity extends Product
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('master_data',merchant_id());
    }

    public static function getDataForDataTable()
    {
        try {
            return Product::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_products.id)'),
                'sc_products.id',
                'sc_products.code as kode',
                'sc_products.name as nama_layanan',
                'sc_products.foto',
                DB::raw('rupiah(sc_products.selling_price) as harga_jual'),
                DB::raw('rupiah(sc_products.purchase_price) as harga_beli'),
                DB::raw('rupiah(sc_products.selling_price) as harga_jual_di_senna_app'),
                'sc_products.stock',
                'c.name as kategori_layanan',
                'sc_products.created_at as tanggal_input',
                'sc_products.is_with_stock',
                'uu.name as nama_satuan',
                'm.id as merchant_id',
                DB::raw("case when sc_products.is_with_stock=1 then 'Barang' else 'Jasa/Layanan' end as jenis_produk"),
            ])
                ->join('md_units as uu','uu.id','sc_products.md_unit_id')
                ->join('md_users as u','u.id','sc_products.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->join('sc_product_categories as c','c.id','sc_products.sc_product_category_id')
                ->orderBy('sc_products.id','desc')
                ->where('m.id',merchant_id())
                ->where('sc_products.is_with_stock', 0)
                ->where('sc_products.md_sc_product_type_id','!=', 4)
                ->where('sc_products.is_deleted',0)

                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_products.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->orWhereRaw("lower(sc_products.name) like '%".strtolower(str_replace("'","''",$search))."%'")
                ->orWhereRaw("lower(c.name) like '%".strtolower(str_replace("'","''",$search))."%'")
                ->orWhereRaw("lower(uu.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','foto'],$isEdit,$isDelete,$isHead)
            ->editColumn('foto',function ($list){
                if(is_null($list->foto)){
                    return '<img src="'.env('S3_URL').'public/backend-v2/img/no-product-image.png" height="100px" width="100px">';

                }else{
                    return '<img src="'.env('S3_URL').$list->foto.'" height="100px" width="100px">';
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'kode',
            'nama_layanan',
            'kategori_layanan',
            'opsi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'sc_product_category_id'=>'c.id',
            'md_sc_product_type_id' => 'md_sc_product_type_id',
            'is_with_stock' => 'is_with_stock'
        ];
        return $field;
    }


    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id=merchant_id())
        {
            if($isHead==true)
            {
                return "<a   href='".route('merchant.toko.clinic.services.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded' title='Hapus'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  href='".route('merchant.toko.clinic.services.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a   href='".route('merchant.toko.clinic.services.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }

    }

    public static function add($permission)
    {

        if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" href="'.route('merchant.toko.product.add',['is_goods'=>0]).'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" href="'.route('merchant.toko.product.add',['is_goods'=>0]).'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
                }
            }else{
                return "";
            }
        }
    }
}

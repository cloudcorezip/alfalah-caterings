<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantStaff;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class StaffEntity extends MerchantStaff
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('employee', merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return MerchantStaff::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_staff.id)'),
                'md_merchant_staff.id',
                'u.fullname as nama_lengkap',
                'u.email',
                'r.name as jabatan',
                'm.name as outlet',
                'm.name as cabang',
                'g.grade_name as grade',
                'm.id as merchant_id'
            ])
                ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->leftJoin('merchant_grade_staffs as g','g.id','md_merchant_staff.merchant_grade_id')
                ->where('u.md_role_id', '!=', 23)
                ->where('md_merchant_staff.is_non_employee',0)
                ->orderBy('md_merchant_staff.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(u.fullname) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(u.email) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(r.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        $getBranch=MerchantUtil::getBranch(merchant_id());

        if(merchant_detail()->is_branch==0)
        {
            $data->whereIn('m.id',$getBranch)->where('md_merchant_staff.is_deleted',0);

        }else{
            $data->whereIn('m.id',[merchant_id()])->where('md_merchant_staff.is_deleted',0);

        }


        //$data->where('m.id',merchant_id())->where('md_merchant_staff.is_deleted',0);

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'], $isEdit,$isDelete,$isHead)
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1)
        {
            return [
                'row_number',
                'nama_lengkap',
                'email',
                'grade',
                'jabatan',
                'cabang',
                'opsi'
            ];
        }else{
            return [
                'row_number',
                'nama_lengkap',
                'email',
                'grade',
                'jabatan',
                'opsi'
            ];
        }

    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id=merchant_id())
        {
            if($isHead==true)
            {
                return "<a href='".route('merchant.toko.staff.addStaff',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a href='".route('merchant.toko.staff.addStaff',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a href='".route('merchant.toko.staff.addStaff',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }

    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if ($permission == true) {
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" href="'.route('merchant.toko.staff.addStaff').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else
        if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" href="'.route('merchant.toko.staff.addStaff').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" href="'.route('merchant.toko.staff.addStaff').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
                }
            }else{

                return "";

            }
        }

    }


}

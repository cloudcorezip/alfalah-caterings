<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko\Receipt;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Material;
use Illuminate\Support\Facades\DB;

class ReceiptItemEntity
{
    public static function getDataForDataTable($id)
    {
        try {
            return Material::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_product_raw_materials.id)'),
                'sc_product_raw_materials.id',
                'p.name as nama_item',
                'sc_product_raw_materials.quantity as jumlah',
                'sc_product_raw_materials.parent_sc_product_id',
                'sc_product_raw_materials.unit_name as nama_satuan',
                DB::raw('rupiah(sc_product_raw_materials.total) as total'),
            ])
                ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                ->leftJoin('md_units as u','u.id','p.md_unit_id')
                ->where('sc_product_raw_materials.parent_sc_product_id',$id)
                ->where('sc_product_raw_materials.is_deleted',0)
                ->orderBy('sc_product_raw_materials.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }
    }

    public static function dataTable($request,$id)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForDataTable($id),$request,['opsi'],
            ['p.name'])
            ->make(true);
    }

    public static function dataTableColumns()
    {
        return [
            'row_number',
            'nama_item',
            'jumlah',
            'nama_satuan',
            'total',
            'opsi'
        ];
    }

    public static function add($productId)
    {
        return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button type="button" class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.master-data.receipt.add-item').'?sc_product_id='.$productId.'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
    }

    public static function getButton($list)
    {
        return "<a  onclick='loadModal(this)' target='".route('merchant.toko.master-data.receipt.add-item',['id'=>$list->id,'sc_product_id'=>$list->parent_sc_product_id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteDataItem($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
    }
}

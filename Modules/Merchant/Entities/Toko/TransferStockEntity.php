<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\TransferStock;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class TransferStockEntity extends TransferStock
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('mutation',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return TransferStock::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_transfer_stocks.id)'),
                'sc_transfer_stocks.id',
                DB::raw('coalesce(sc_transfer_stocks.second_code,sc_transfer_stocks.code) as kode'),
                'f.name as outlet_asal',
                't.name as outlet_tujuan',
                'i.name as gudang_asal',
                'w.name as gudang_tujuan',
                'f.name as cabang_asal',
                't.name as cabang_tujuan',
                DB::raw("rupiah(total) as total"),
                'sc_transfer_stocks.created_at as waktu_mutasi_stok',
                'sc_transfer_stocks.timezone as zona_waktu',
                'sc_transfer_stocks.is_accepted as status',
                'f.id as merchant_id'
            ])
                ->join('md_merchants as f','f.id','sc_transfer_stocks.from_merchant_id')
                ->join('md_merchants as t','t.id','sc_transfer_stocks.to_merchant_id')
                ->join('md_merchant_inv_warehouses as w','w.id','sc_transfer_stocks.to_inv_warehouse_id')
                ->join('md_merchant_inv_warehouses as i','i.id','sc_transfer_stocks.inv_warehouse_id')
                ->where('sc_transfer_stocks.is_deleted',0)
                ;
        } catch (\Exception $e) {
            return [];

        }

    }

    public static function dataTable($request)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("sc_transfer_stocks.created_at::date between '$startDate' and '$endDate'
        ");

        $getBranch=MerchantUtil::getBranch(merchant_id());
        $data->whereIn('sc_transfer_stocks.from_merchant_id',$getBranch);


        return(DataTable::getInstance())->dataTable(self::class,$data,$request,['opsi','status'])
            ->editColumn('zona_waktu',function ($list){

                return getTimeZoneName($list->zona_waktu);
            })
            ->editColumn('opsi',function ($list){

                if(merchant_id()==$list->merchant_id)
                {
                    if($list->status==1){
                        return "<a href='".route('merchant.toko.transfer-stock.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                    <i class='fa fa-eye'></i>
                 </a>

                   <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                 ";
                    }else{
                        return "<a href='".route('merchant.toko.transfer-stock.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                    <i class='fa fa-eye'></i>
                 </a>

                   <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                ";
                    }
                }else{
                    return "";
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode',
            'cabang_asal',
            'cabang_tujuan',
            'gudang_asal',
            'gudang_tujuan',
            'total',
            'waktu_mutasi_stok',
            'zona_waktu'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'md_merchant_id'=>'f.id',
            'inv_warehouse_id'=>'i.id'
        ];
        return $field;
    }

    public static function getButton($list){

        if(merchant_id()==$list->merchant_id)
        {
            return "
                                            <a href='".route('merchant.toko.transfer-stock.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-edit'></i>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-edit-xs text-primary btn-rounded'>
                                                <span class='fa fa-trash' style='color: white'></span>
                                            </a>";
        }else{
            return "";
        }

    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return '<a href="'.route('merchant.toko.transfer-stock.add').'" class="btn btn-success btn-rounded btn-sm btn-block text-white py-2 px-4" style="color: white"><i class="fa fa-plus mr-1"></i> Buat Mutasi Stok</a>';
            }
        }
        else if(merchant_detail()->is_branch==0){
            if($permission==true){
                return '<a href="'.route('merchant.toko.transfer-stock.add').'" class="btn btn-success btn-rounded btn-sm btn-block text-white py-2 px-4" style="color: white"><i class="fa fa-plus mr-1"></i> Buat Mutasi Stok</a>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return '<a href="'.route('merchant.toko.transfer-stock.add').'" class="btn btn-success btn-rounded btn-sm btn-block text-white py-2 px-4" style="color: white"><i class="fa fa-plus mr-1"></i> Buat Mutasi Stok</a>';
                }
            }else{
                return "";
            }
        }

    }
}

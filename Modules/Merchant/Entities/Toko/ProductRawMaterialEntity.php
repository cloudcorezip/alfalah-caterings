<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\ProductSellingLevel;
use Illuminate\Support\Facades\DB;

class ProductRawMaterialEntity
{
    public static function getDataForRawMaterial($id)
    {
        try {
            return Material::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_product_raw_materials.id)'),
                'sc_product_raw_materials.id',
                'sc_product_raw_materials.quantity as jumlah',
                'p.name as nama_produk',
                'u.name as nama_satuan',
                'sc_product_raw_materials.parent_sc_product_id'
            ])
                ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                ->leftJoin('md_units as u','u.id','p.md_unit_id')
                ->where('sc_product_raw_materials.parent_sc_product_id',$id)
                ->where('sc_product_raw_materials.is_deleted',0)
                ->orderBy('sc_product_raw_materials.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }
    }

    public static function dataTableRawMaterial($request,$id)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForRawMaterial($id),$request,['opsi'],
            ['p.name'])
            ->make(true);
    }

    public static function dataTableColumns()
    {
        return [
            'row_number',
            'jumlah',
            'nama_produk',
            'nama_satuan',
            'opsi'
        ];
    }

    public static function add($productId)
    {
        return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.product.raw-material.add').'?sc_product_id='.$productId.'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
    }

    public static function getButton($list)
    {
        return "<a  onclick='loadModal(this)' target='".route('merchant.toko.product.raw-material.add',['id'=>$list->id,'sc_product_id'=>$list->parent_sc_product_id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteDataRawMaterial($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\DiscountCoupon;
use Illuminate\Support\Facades\DB;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\ProductSpecialPrice;

class SpecialPriceEntity extends ProductSpecialPrice
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('master_data',merchant_id());
    }

    private static function getBranch()
    {
        return MerchantUtil::getBranch(merchant_id(),1);
    }

    public static function getDataForDataTable()
    {

        try {
            return ProductSpecialPrice::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_product_special_prices.id)'),
                'sc_product_special_prices.id',
                'sc_product_special_prices.name as judul',
                'sc_product_special_prices.assign_to_branch as cabang_terkait',
                'sc_product_special_prices.start_date as deskripsi',
                'm.name as dibuat_oleh_outlet',
                'm.name as dibuat_oleh_cabang',
                'm.id as md_merchant_id',
                'sc_product_special_prices.is_active as status',
                'm.id as merchant_id'
            ])
            ->join('md_merchants as m', 'm.id', 'sc_product_special_prices.md_merchant_id')
            ->where('sc_product_special_prices.is_deleted', 0);


        }catch (\Exception $e)
        {
            return [];

        }

    }

    public static function dataTable($request,$isEdit=false,$isDelete=true,$isHead=false)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_product_special_prices.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(m.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("sc_product_special_prices.start_date::date between '$startDate' and '$endDate'
        ");
        $data->where('sc_product_special_prices.md_merchant_id',merchant_id());


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi', 'deskripsi', 'status'], $isEdit,$isDelete,$isHead)
            ->editColumn('deskripsi', function($list){
                $detail = ProductSpecialPrice::select([
                    "sppd.id",
                    "sppd.sc_product_id as detail_product_id",
                    "sppd.special_price",
                    "sp.name as product_name",
                    "sp.selling_price"
                ])
                ->join("sc_product_special_price_details as sppd", "sppd.sc_product_special_price_id", "sc_product_special_prices.id")
                ->join("sc_products as sp", "sp.id", "sppd.sc_product_id")
                ->where("sppd.is_deleted", 0)
                ->where("sppd.sc_product_special_price_id", $list->id)
                ->where("sppd.md_merchant_id", merchant_id())
                ->get();

                $text = '<ul>';
                foreach($detail as $key => $item){
                    $diff = $item->special_price - $item->selling_price;

                    if(($diff + $item->special_price) > $item->selling_price){
                        $diffText = "<span style='color:green;'> (+".rupiah($diff).")</span>";
                    } else {
                        $diffText = "<span style='color:#FF4444;'> (".rupiah($diff).")</span>";
                    }

                    $text .= "<li class='d-flex'>
                                <div style='width:48%;'>$item->product_name</div>
                                <div style='width:4%'>:</div>
                                <div style='width:48%;'>".rupiah($item->special_price)."$diffText</div>
                            </li>";
                }

                $text .= '</ul>';
                return $text;
            })
            ->editColumn('cabang_terkait', function($list){
                $list_branch="";
                if(!is_null($list->cabang_terkait)){
                    $merchant=Merchant::whereIn('id',json_decode($list->cabang_terkait))->get();

                    foreach($merchant as $key => $a){
                        $list_branch.= ($key == 0) ? $a->name : "," . $a->name;
                    }
                }

                return $list_branch;
            })
            ->editColumn('status',function ($list){
                if($list->status==1){
                    return '<h6><span class="badge badge-success">aktif</span></h6>';
                }else{
                    return '<h6><span class="badge badge-danger" >non-aktif</span></h6>';
                }
            })
            ->make(true);
    }

    public static function getFilterMap()
    {
        $field = [
            'status' => 'is_active'
        ];
        return $field;
    }
    public static function dataTableColumns()
    {

        return [
            'row_number',
            'judul',
            'deskripsi',
            'cabang_terkait',
            'status',
            'opsi'
        ];
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id())
        {

            if($isHead==true)
            {
                return "<a href='".route('merchant.toko.special-price.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a href='".route('merchant.toko.special-price.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a href='".route('merchant.toko.special-price.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return '<div class="col-md-12 mb-3">
                        <a href="'.route('merchant.toko.special-price.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else
        if(merchant_detail()->is_branch==0){
            if($permission==true){
                return '<div class="col-md-12 mb-3">
                        <a href="'.route('merchant.toko.special-price.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                return '<div class="col-md-12 mb-3">
                        <a href="'.route('merchant.toko.special-price.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }else{
                return "";
            }
        }
    }




}

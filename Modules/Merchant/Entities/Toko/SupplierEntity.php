<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\Supplier;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class SupplierEntity extends Supplier
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('supplier', merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return Supplier::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_suppliers.id)'),
                'sc_suppliers.id',
                'sc_suppliers.code as kode',
                'sc_suppliers.name as nama_supplier',
                'sc_suppliers.email',
                'sc_suppliers.phone_number as no_telp',
                'sc_suppliers.address as alamat',
                'c.name as kategori_supplier',
                'sc_suppliers.is_google_contact',
                'sc_suppliers.etag',
                'sc_suppliers.resource_name',
                'm.name as dibuat_oleh_cabang',
                'm.id as merchant_id'
            ])
                ->join('md_users as u','u.id','sc_suppliers.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->leftJoin('sc_supplier_categories as c','c.id','sc_suppliers.sc_supplier_categories_id')
                ->orderBy('sc_suppliers.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }
    }

    public static function listOption()
    {
        return self::getDataForDataTable()->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('sc_suppliers.is_deleted',0)->get();
    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_suppliers.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_suppliers.name) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(sc_suppliers.email) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(sc_suppliers.phone_number) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(sc_suppliers.address) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(c.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }


        $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('sc_suppliers.is_deleted',0);

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],$isEdit,$isDelete,$isHead)
            ->editColumn('dibuat_oleh_cabang',function ($list){
                return  $list->dibuat_oleh_cabang;

            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1)
        {
            return [
                'row_number',
                'kode',
                'nama_supplier',
                'email',
                'no_telp',
                'alamat',
                'kategori_supplier',
                'dibuat_oleh_cabang',
                'opsi'
            ];
        }else{
            return [
                'row_number',
                'kode',
                'nama_supplier',
                'email',
                'no_telp',
                'alamat',
                'kategori_supplier',
                'opsi'
            ];
        }
    }

    public static function getFilterMap()
    {
        $field = [
            'sc_supplier_categories_id' => 'c.id',

        ];
        return $field;
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id())
        {
            if($isHead==true)
            {
                if($list->is_google_contact == 0){
                    return "<a href='".route('merchant.toko.supplier.add-supplier',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                } else {
                    return "<a target='".route('merchant.toko.supplier.contact-sync-update-form',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteSyncData($list)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }

            }else{
                if($edit==true && $delete==false)
                {
                    return "<a href='".route('merchant.toko.supplier.add-supplier',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a href='".route('merchant.toko.supplier.add-supplier',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }

        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-md-12 mb-3">
                            <a class="btn btn-success btn-rounded btn-sm py-2 px-4" href="'.route('merchant.toko.supplier.add-supplier').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                        </div>';
            }
        }else if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return ' <div class="col-md-12 mb-3">
                            <a class="btn btn-success btn-rounded btn-sm py-2 px-4" href="'.route('merchant.toko.supplier.add-supplier').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                        </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm py-2 px-4" href="'.route('merchant.toko.supplier.add-supplier').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
                }
            }else{
                return "";
            }
        }

    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\SaleOrder;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApprovalSaleOrderEntity extends SaleOrder
{
    public static function getDataForDataTable()
    {

        try {
            return SaleOrder::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_sale_orders.id)'),
                'sc_sale_orders.id',
                DB::raw('coalesce(sc_sale_orders.second_code,sc_sale_orders.code) as kode_transaksi'),
                DB::raw('rupiah(sc_sale_orders.total_order) as total'),
                DB::raw('rupiah(sc_sale_orders.tax_order) as pajak'),
                DB::raw('rupiah(sc_sale_orders.promo_order) as diskon'),
                'cust.name as nama_pelanggan',
                'sc_sale_orders.time_to_order as waktu_transaksi',
                'sc_sale_orders.timezone',
                'sc_sale_orders.is_editable',
                'm.name as outlet',
                'm.name as cabang',
                'sc_sale_orders.code',
                'sc_sale_orders.second_code',
                'u.fullname as nama_sales',
                'sc_sale_orders.is_approve as status',
                'sc_sale_orders.approve_by'

            ])->leftJoin('md_users as u','u.id','sc_sale_orders.created_by')
                ->leftJoin('md_transaction_types as t','t.id','sc_sale_orders.md_transaction_type_id')
                ->leftJoin('md_sc_shipping_categories as c','c.id','sc_sale_orders.md_sc_shipping_category_id')
                ->leftJoin('sc_customers as cust','cust.id','sc_sale_orders.sc_customer_id')
                ->join('md_merchants as m','m.id','sc_sale_orders.md_merchant_id')
                ->where('sc_sale_orders.is_approved_shop',1)
                ->where('sc_sale_orders.is_cancel_user',0)
                ->where('sc_sale_orders.is_from_app_sales',1)
                ->whereIn('sc_sale_orders.is_approve',[0,1,2])
                ->where('sc_sale_orders.step_type',2)
                ->orderBy('sc_sale_orders.id','desc');
        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=false,$isDelete=false,$isHead=false)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_sale_orders.code) like '%".strtolower(str_replace('SO-','S-',str_replace("'","''",$search)))."%' ")
                    ->orWhereRaw("lower(sc_sale_orders.second_code) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(cust.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        $searchKey = (array)json_decode(base64_decode($request->input('searchParams')));
        $searchFilter = is_array($searchKey) ? $searchKey : [];

        if(empty($searchFilter)){
            $data->where('sc_sale_orders.is_deleted',0);
        }
        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

        if($request->md_merchant_id=='-1'){
            $getBranch=MerchantUtil::getBranch(merchant_id(),0);

            $data->whereIn('sc_sale_orders.md_merchant_id',$getBranch);
        }else{
            $data->where('sc_sale_orders.md_merchant_id',$request->md_merchant_id);
        }


        $data->whereRaw("
        sc_sale_orders.created_at::date between '$startDate' and '$endDate'
        ");
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','kredit','status'],$isEdit,$isDelete,$isHead)
            ->editColumn('kode_transaksi',function ($list){
                return str_replace('S','SO',$list->kode_transaksi);
            })
            ->editColumn('waktu_transaksi',function ($list){
                return Carbon::parse($list->waktu_transaksi)->isoFormat('dddd, D MMMM Y, HH:mm')." ".getTimeZoneName($list->timezone);
            })
            ->editColumn('status',function ($list){
                if($list->status==0){
                    return '<span class="badge badge-info">Menunggu Persetujuan</span>';

                }elseif ($list->status==1)
                {
                    return '<span class="badge badge-success">Disetujui</span>';
                }else{
                    return '<span class="badge badge-danger">Ditolak</span>';
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'cabang',
            'kode_transaksi',
            'nama_pelanggan',
            'pajak',
            'diskon',
            'total',
            'nama_sales',
            'status',
            'waktu_transaksi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'status' => 'sc_sale_orders.is_approve',
            'is_deleted'=>'sc_sale_orders.is_deleted',
            'md_merchant_id'=>'sc_sale_orders.md_merchant_id',
            'sc_customer_id' => 'sc_sale_orders.sc_customer_id',
            'created_by' => 'sc_sale_orders.created_by',

        ];
        return $field;
    }

    public static function getButton($list){
        if($list->is_editable==1)
        {
            return "
                                            <a href='".route('merchant.toko.sale-order.detail-approval',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye' ></i>
                                            </a>
                                          ";
        }else{
            return "";
        }
    }

    public static function countData()
    {
        $getBranch=MerchantUtil::getBranch(merchant_id(),0);

        return SaleOrder::
        select([
          'sc_sale_orders.id'

        ])->where('sc_sale_orders.is_approved_shop',1)
            ->where('sc_sale_orders.is_cancel_user',0)
            ->where('sc_sale_orders.is_from_app_sales',1)
            ->where('sc_sale_orders.is_approve',0)
            ->where('sc_sale_orders.step_type',2)
            ->where('sc_sale_orders.is_deleted',0)
            ->whereIn('sc_sale_orders.md_merchant_id',$getBranch)
            ->orderBy('sc_sale_orders.id','desc')->count();
    }
}

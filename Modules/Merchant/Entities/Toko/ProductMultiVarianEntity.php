<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\MultiVarian;
use Illuminate\Support\Facades\DB;

class ProductMultiVarianEntity extends Product
{
    public static function getDataForMultiVarian($id)
    {
        try {
            return MultiVarian::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_product_multi_varians.id)'),
                'sc_product_multi_varians.id',
                'sc_product_multi_varians.purchase_price as harga_beli',
                'sc_product_multi_varians.selling_price as harga_jual',
                'sc_product_multi_varians.label as label',
                'sc_product_multi_varians.sc_product_id',
                'product.name as nama_produk',
                'sc_product_multi_varians.multi_varian'
            ])
                ->join('sc_products as product','product.id', 'sc_product_multi_varians.sc_product_id')
                ->where('sc_product_multi_varians.sc_product_id',$id)
                ->where('sc_product_multi_varians.is_deleted',0)
                ->orderBy('sc_product_multi_varians.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }
    }

    public static function dataTableMultiVarian($request,$id)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForMultiVarian($id),$request,['opsi', 'harga_beli', 'harga_jual'])
            ->editColumn('harga_beli', function($list){
                return rupiah($list->harga_beli);
            })
            ->editColumn('harga_jual', function($list){
                return rupiah($list->harga_jual);
            })
            ->make(true);
    }

    public static function dataTableColumns()
    {
        return [
            'row_number',
            'nama_produk',
            'label',
            'harga_beli',
            'harga_jual',
            'opsi'
        ];
    }

    public static function add($productId)
    {
        return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.product.multi-varian.add').'?sc_product_id='.$productId.'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
    }

    public static function getButton($list)
    {
        return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.product.multi-varian.add',['id'=>$list->id,'sc_product_id'=>$list->sc_product_id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                <a onclick='deleteDataMultiVarian($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                    <span class='fa fa-trash-alt' style='color: white'></span>
                </a>
                ";
    }

}

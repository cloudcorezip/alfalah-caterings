<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Merk;
use App\Models\SennaToko\Product;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\ProductSellingLevel;
use App\Models\SennaToko\StockInventory;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class ProductEntity extends Product
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('master_data',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return Product::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_products.id)'),
                'sc_products.id',
                'sc_products.code as kode',
                'sc_products.name as produk',
                'sc_products.foto',
                DB::raw('rupiah(sc_products.selling_price) as di_jual'),
                DB::raw('rupiah(sc_products.purchase_price) as harga_beli'),
                DB::raw('rupiah(sc_products.selling_price) as harga_jual_di_senna_app'),
                'sc_products.stock',
                'c.name as kategori',
                'sc_products.created_at as tanggal_input',
                'sc_products.is_with_stock',
                'uu.name as satuan_default',
                'sc_products.assign_to_branch as cabang_terkait',
                'm.id as merchant_id',
                'sc_products.created_by_merchant',
                'sc_products.duplicated_by_id',
                DB::raw("case when sc_products.is_with_stock=1 then 'Barang' else 'Jasa/Layanan' end as jenis"),
            ])
                ->join('md_units as uu','uu.id','sc_products.md_unit_id')
                ->join('md_users as u','u.id','sc_products.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->join('sc_product_categories as c','c.id','sc_products.sc_product_category_id')
                ->orderBy('sc_products.id','desc')
                ->where('m.id',merchant_id())
                ->where('sc_products.md_sc_product_type_id','!=',4)
                ->where('sc_products.is_deleted',0)

                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }



    public static function getDataForDataTableStock($id,$warehouseId='-1')
    {

        try {
            if($warehouseId=='-1')
            {
                return StockInventory::select([
                    DB::raw('ROW_NUMBER () OVER (ORDER BY sc_stock_inventories.id)'),
                    'sc_stock_inventories.id',
                    DB::raw("(CASE WHEN sc_stock_inventories.type='initial' THEN 0
                 WHEN sc_stock_inventories.type='in' THEN sc_stock_inventories.total
                 ElSE 0 END
                 ) stok_masuk"),
                    DB::raw("(CASE WHEN sc_stock_inventories.type='initial' THEN 0
                 WHEN sc_stock_inventories.type='out' THEN sc_stock_inventories.total
                 ElSE 0 END
                 ) stok_keluar"),
                    DB::raw("(CASE WHEN sc_stock_inventories.type='initial' THEN 'stok awal'
                 WHEN sc_stock_inventories.type='out' THEN 'stok keluar'
                 ElSE 'stok masuk' END
                 ) tipe"),
                    DB::raw("rupiah(case when sc_stock_inventories.type='in' then sc_stock_inventories.purchase_price
                else 0 end) as harga_beli
                "),
                    'sc_stock_inventories.type',
                    'sc_stock_inventories.stockable_type',
                    DB::raw("'' as riwayat"),
                    'sc_stock_inventories.stockable_id',
                    'sc_stock_inventories.created_at as dibuat',
                    'sc_stock_inventories.residual_stock',
                    'w.name as gudang'
                ])->join('sc_products as p','p.id','sc_stock_inventories.sc_product_id')
                    ->leftJoin('md_merchant_inv_warehouses as w','w.id','sc_stock_inventories.inv_warehouse_id')
                    ->where('p.id',$id)
                    ->where('sc_stock_inventories.is_deleted',0)
                    ->orderBy('sc_stock_inventories.id','desc');
            }else{
                return StockInventory::select([
                    DB::raw('ROW_NUMBER () OVER (ORDER BY sc_stock_inventories.id)'),
                    'sc_stock_inventories.id',
                    DB::raw("(CASE WHEN sc_stock_inventories.type='initial' THEN 0
                 WHEN sc_stock_inventories.type='in' THEN sc_stock_inventories.total
                 ElSE 0 END
                 ) stok_masuk"),
                    DB::raw("(CASE WHEN sc_stock_inventories.type='initial' THEN 0
                 WHEN sc_stock_inventories.type='out' THEN sc_stock_inventories.total
                 ElSE 0 END
                 ) stok_keluar"),
                    DB::raw("(CASE WHEN sc_stock_inventories.type='initial' THEN 'stok awal'
                 WHEN sc_stock_inventories.type='out' THEN 'stok keluar'
                 ElSE 'stok masuk' END
                 ) tipe"),
                    DB::raw("rupiah(case when sc_stock_inventories.type='in' then sc_stock_inventories.purchase_price
                else 0 end) as harga_beli
                "),
                    'sc_stock_inventories.type',
                    'sc_stock_inventories.stockable_type',
                    DB::raw("'' as riwayat"),
                    'sc_stock_inventories.stockable_id',
                    'sc_stock_inventories.created_at as dibuat',
                    'sc_stock_inventories.residual_stock',
                    'w.name as gudang'
                ])->join('sc_products as p','p.id','sc_stock_inventories.sc_product_id')
                    ->leftJoin('md_merchant_inv_warehouses as w','w.id','sc_stock_inventories.inv_warehouse_id')
                    ->where('p.id',$id)
                    ->where('w.id',$warehouseId)
                    ->where('sc_stock_inventories.is_deleted',0)
                    ->orderBy('sc_stock_inventories.id','desc');
            }


        }catch (\Exception $e)
        {
            return [];

        }

    }
    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_products.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->orWhereRaw("lower(sc_products.name) like '%".strtolower(str_replace("'","''",$search))."%'")
                ->orWhereRaw("lower(c.name) like '%".strtolower(str_replace("'","''",$search))."%'")
                ->orWhereRaw("lower(uu.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','foto'],$isEdit,$isDelete,$isHead)
            ->editColumn('foto',function ($list){
                if(is_null($list->foto)){
                    return '<img src="'.env('S3_URL').'public/backend-v2/img/no-product-image.png" height="100px" width="100px">';

                }else{
                    return '<img src="'.env('S3_URL').$list->foto.'" height="100px" width="100px">';
                }
            })
            ->editColumn('cabang_terkait',function ($list){
                $list_branch="";
                if(!is_null($list->cabang_terkait)){
                    $merchant=Merchant::whereIn('id',json_decode($list->cabang_terkait))->get();

                    foreach($merchant as $key => $a){
                        $list_branch.= ($key == 0) ? $a->name : "," . $a->name;
                    }
                }

                return $list_branch;
            })
            ->make(true);
    }

    public static function dataTableStock($request,$id, $isEdit=true,$isDelete=true,$isHead=true)
    {
        $data = self::getDataForDataTableStock($id,$request->warehouse_id);
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_stock_inventories.transaction_action) like '%".strtolower($search)."%' ");
            });
        }
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,[],$isEdit,$isDelete,$isHead)
            ->editColumn('riwayat',function ($list){

                return 'Kode Transaksi : '.((is_null($list->stockable->second_code) || $list->stockable->second_code=='')?$list->stockable->code:$list->stockable->second_code);
            })
            ->make(true);
    }

    public static function dataTableColumnsStock()
    {
        return [
            'row_number',
            'stok_masuk',
            'stok_keluar',
            'harga_beli',
            'tipe',
            'riwayat',
            'gudang',
            'dibuat'
        ];
    }





    public static function dataTableColumns()
    {
        return [
            'row_number',
            'kode',
            'kategori',
            'jenis',
            'produk',
            'di_jual',
            'satuan_default',
            'cabang_terkait',
            'opsi',
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'sc_product_category_id'=>'c.id',
            'md_sc_product_type_id' => 'md_sc_product_type_id',
            'is_with_stock' => 'is_with_stock'
        ];
        return $field;
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if(is_null($list->duplicated_by_id)){
            if($list->merchant_id==merchant_id())
            {
                if($isHead==true)
                {
                    return "<a   href='".route('merchant.toko.product.detail',['id'=>$list->id,'is_goods'=>($list->is_with_stock==0)?0:1])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded' title='Hapus'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }else{
                    if($edit==true && $delete==false)
                    {
                        return "<a  href='".route('merchant.toko.product.detail',['id'=>$list->id,'is_goods'=>($list->is_with_stock==0)?0:1])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                    }elseif ($edit==false && $delete==true)
                    {
                        return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                    }elseif($edit==false && $delete==false){
                        return "";
                    }else{
                        return "<a   href='".route('merchant.toko.product.detail',['id'=>$list->id,'is_goods'=>($list->is_with_stock==0)?0:1])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                    }
                }
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return '
               <div class="col-lg-auto col-md-12 mb-3">
                            <button type="button" class="btn btn-sm btn-rounded py-2 px-4 btn-success dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tambah
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="'.route('merchant.toko.product.add',['is_goods'=>1]).'">Produk Tipe Barang</a>
                                <a class="dropdown-item" href="'.route('merchant.toko.product.add',['is_goods'=>0]).'">Produk Tipe Jasa/Layanan</a>
                                <a class="dropdown-item" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.product.form-import').'">Import Data</a>

                            </div>
                        </div>';
            }
        }
        else if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return '
               <div class="col-lg-auto col-md-12 mb-3">
                            <button type="button" class="btn btn-sm btn-rounded py-2 px-4 btn-success dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tambah
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="'.route('merchant.toko.product.add',['is_goods'=>1]).'">Produk Tipe Barang</a>
                                <a class="dropdown-item" href="'.route('merchant.toko.product.add',['is_goods'=>0]).'">Produk Tipe Jasa/Layanan</a>
                                <a class="dropdown-item" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.product.form-import').'">Import Data</a>

                            </div>
                        </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return '
               <div class="col-lg-auto col-md-12 mb-3">
                            <button type="button" class="btn btn-sm btn-rounded py-2 px-4 btn-success dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tambah
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="'.route('merchant.toko.product.add',['is_goods'=>1]).'">Produk Tipe Barang</a>
                                <a class="dropdown-item" href="'.route('merchant.toko.product.add',['is_goods'=>0]).'">Produk Tipe Jasa/Layanan</a>
                                <a class="dropdown-item" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.product.form-import').'">Import Data</a>

                            </div>
                        </div>';
                }

            }else{
                return "";
            }
        }

    }
}

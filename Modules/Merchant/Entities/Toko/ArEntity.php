<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\SaleOrder;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ArEntity extends SaleOrder
{
    public static function getDataForDataTable()
    {

        try {
            return SaleOrder::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_sale_orders.id)'),
                'sc_sale_orders.id',
                DB::raw('coalesce(sc_sale_orders.second_code,sc_sale_orders.code) as kode_transaksi'),
                'sc_sale_orders.second_code',
                'sc_sale_orders.code',
                DB::raw('sc_sale_orders.total as total'),
                DB::raw("coalesce(NULL,cust.name,'Pelanggan Umum') as nama_pelanggan"),
                DB::raw("
                COALESCE(
                              (
                                select
                                  sum(ar.residual_amount)
                                from
                                  sc_sale_orders as so_sub
                                  join acc_merchant_ar as ar on ar.arable_id = so_sub.id
                                  and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                                where
                                  so_sub.id = sc_sale_orders.id
                                  and so_sub.is_debet = 1
                              )
                          ,
                          0
                        ) as sisa_piutang
                "),
                DB::raw("
                COALESCE(
                              (
                                select
                                  sum(ar.paid_nominal)
                                from
                                  sc_sale_orders as so_sub
                                  join acc_merchant_ar as ar on ar.arable_id = so_sub.id
                                  and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                                where
                                  so_sub.id = sc_sale_orders.id
                                  and so_sub.is_debet = 1
                              )
                          ,
                          0
                        ) as terbayarkan
                "),
                DB::raw("
                COALESCE(
                              (
                                select
                                  ar.due_date
                                from
                                  sc_sale_orders as so_sub
                                  join acc_merchant_ar as ar on ar.arable_id = so_sub.id
                                  and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                                where
                                  so_sub.id = sc_sale_orders.id
                                  and so_sub.is_debet = 1
                              )
                        ) as jatuh_tempo
                "),
                DB::raw("coalesce(u.fullname,'-') as nama_pencatat"),
                "sc_sale_orders.created_at as waktu_transaksi",
                "sc_sale_orders.timezone",
                'sc_sale_orders.is_editable',
                'm.name as outlet'

            ])->leftJoin('md_users as u','u.id','sc_sale_orders.created_by')
                ->leftJoin('sc_customers as cust','cust.id','sc_sale_orders.sc_customer_id')
                ->join('md_merchants as m','m.id','sc_sale_orders.md_merchant_id')
                ->where('sc_sale_orders.is_deleted',0)
                ->where('sc_sale_orders.is_approved_shop',1)
                ->where('sc_sale_orders.is_cancel_user',0)
                ->where('sc_sale_orders.is_debet',1)
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request)
    {
        $data=self::getDataForDataTable();

        $searchKey = (array)json_decode(base64_decode($request->input('searchParams')));
        $searchFilter = is_array($searchKey) ? $searchKey : [];

        if(empty($searchFilter)){
            $data->where('sc_sale_orders.is_deleted',0);
        }


        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

        if($request->md_merchant_id=='-1'){
            $getBranch=MerchantUtil::getBranch(merchant_id(),0);

            $data->whereIn('sc_sale_orders.md_merchant_id',$getBranch);
        }else{
            $data->where('sc_sale_orders.md_merchant_id',$request->md_merchant_id);
        }

        $data->whereRaw("
        sc_sale_orders.created_at::date between '$startDate' and '$endDate'
        ");
        return(DataTable::getInstance())->dataTable(self::class,$data,$request,['opsi'],
            ['sc_sale_orders.code'])
            ->editColumn('total', function($list){
              return rupiah($list->total);
              })
              ->editColumn('sisa_piutang', function($list){
                return rupiah($list->sisa_piutang);
              })
            ->editColumn('terbayarkan', function($list){
              return rupiah($list->terbayarkan);
              })
            ->editColumn('waktu_transaksi',function ($list){
                return Carbon::parse($list->waktu_transaksi)->isoFormat('dddd, D MMMM Y, HH:mm')." ".getTimeZoneName($list->timezone);
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode_transaksi',
            'nama_pelanggan',
            'total',
            'terbayarkan',
            'sisa_piutang',
            'jatuh_tempo',
            'nama_pencatat',
            'waktu_transaksi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'sc_customer_id'=>'sc_sale_orders.sc_customer_id',
            'md_merchant_id'=>'sc_sale_orders.md_merchant_id',

        ];
        return $field;
    }

    public static function getButton($list){
        if($list->is_editable==1)
        {
            return "
                                            <a href='".route('merchant.toko.sale-order.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                          ";
        }else{

            return "";
        }
    }

}

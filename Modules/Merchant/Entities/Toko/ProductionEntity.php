<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Merk;
use App\Models\SennaToko\ProductionOfGood;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class ProductionEntity extends ProductionOfGood
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('mutation',merchant_id());
    }
    public static function getDataForDataTable()
    {

        try {
            return ProductionOfGood::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_inv_production_of_goods.id)'),
                'sc_inv_production_of_goods.id',
                'sc_inv_production_of_goods.code as kode_produksi',
                'p.name as nama_produk',
                'sc_inv_production_of_goods.time_of_production as waktu_produksi',
                'sc_inv_production_of_goods.amount as jumlah_produksi',
                'sc_inv_production_of_goods.selling_price as harga_jual',
                'w.name as gudang_produksi',
                'm.name as dibuat_oleh_cabang',
                'sc_inv_production_of_goods.timezone as zona_waktu'

            ])
            ->join('sc_products as p','p.id','sc_inv_production_of_goods.sc_product_id')
                ->join('md_merchant_inv_warehouses as w','w.id','sc_inv_production_of_goods.inv_warehouse_id')
                ->join('md_merchants as m','m.id','sc_inv_production_of_goods.md_merchant_id')
                ->where('sc_inv_production_of_goods.is_deleted',0)
                ->orderBy('sc_inv_production_of_goods.id','asc');
        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        if(isset($searchFilter["md_merchant_id"])){
            $getBranch = $searchFilter["md_merchant_id"];
        } else {
            $getBranch=MerchantUtil::getBranch(merchant_id());
        }

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_inv_production_of_goods.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(p.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }


        $data->whereIn('sc_inv_production_of_goods.md_merchant_id',$getBranch);

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],$isEdit,$isDelete,$isHead)
            ->editColumn('zona_waktu',function ($list){

                return getTimeZoneName($list->zona_waktu);
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1)
        {
            return [
                'row_number',
                'kode_produksi',
                'nama_produk',
                'gudang_produksi',
                'jumlah_produksi',
                'dibuat_oleh_cabang',
                'waktu_produksi',
                'zona_waktu',
                'opsi'
            ];
        }else{
            return [
                'row_number',
                'kode_produksi',
                'nama_produk',
                'gudang_produksi',
                'jumlah_produksi',
                'waktu_produksi',
                'zona_waktu',
                'opsi'
            ];
        }

    }

    public static function getFilterMap()
    {
        $field = [
            'status' => 'is_active'
        ];
        return $field;
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($isHead==true)
        {
            return "<a  href='".route('merchant.toko.stock.production.detail',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
        }else{
            if($edit==true && $delete==false)
            {
                return "<a  href='".route('merchant.toko.stock.production.detail',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
            }elseif ($edit==false && $delete==true)
            {

            }elseif($edit==false && $delete==false){
                return "";
            }else{
                return "<a  href='".route('merchant.toko.stock.production.detail',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-md-12">
                        <a class="btn btn-success btn-rounded btn-sm text-white py-2 px-4" href="'.route('merchant.toko.stock.production.add',["type"=>"select"]).'"><i class="fa fa-plus mr-2"></i> Produksi Produk</a>
                    </div>';
            }
        }else if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return ' <div class="col-md-12">
                        <a class="btn btn-success btn-rounded btn-sm text-white py-2 px-4" href="'.route('merchant.toko.stock.production.add',["type"=>"select"]).'"><i class="fa fa-plus mr-2"></i> Produksi Produk</a>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-md-12">
                        <a class="btn btn-success btn-rounded btn-sm text-white py-2 px-4" href="'.route('merchant.toko.stock.production.add',["type"=>"select"]).'"><i class="fa fa-plus mr-2"></i> Produksi Produk</a>
                    </div>';
                }
            }else{

                return "";
            }
        }

    }
}

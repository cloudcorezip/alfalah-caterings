<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantBranch;
use App\Models\SennaToko\SaleOrder;
use App\Models\Paypoin\Transaction;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SaleOrderEntity extends SaleOrder
{

    public static function getDataForDataTable()
    {

        try {

            return SaleOrder::selectRaw("
            ROW_NUMBER () OVER (ORDER BY sc_sale_orders.id),
            sc_sale_orders.id,
            coalesce(sc_sale_orders.second_code,sc_sale_orders.code) as kode_transaksi,
            rupiah(sc_sale_orders.total+sc_sale_orders.admin_fee) as jumlah,
            rupiah(sc_sale_orders.shipping_cost) as biaya_pengiriman,
            rupiah(sc_sale_orders.admin_fee) as biaya_administrasi,
            rupiah(sc_sale_orders.total) as jumlah_diterima,
            rupiah(sc_sale_orders.promo) as diskon,
            rupiah(sc_sale_orders.tax) as pajak,
            t.name as tipe_transaksi,
            cc.name as trans_name,
            sc_sale_orders.is_editable,
            t.id as transaction_id,
           sc_sale_orders.code,
           sc_sale_orders.second_code,
            rupiah(COALESCE(
                (
                  select
                    sum(ar.residual_amount)
                  from
                    sc_sale_orders as so_sub
                    join acc_merchant_ar as ar on ar.arable_id = so_sub.id
                    and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                  where
                    so_sub.id = sc_sale_orders.id
                    and so_sub.is_debet = 1
                )
            ,
            0
          )) as piutang,
          sc_sale_orders.created_at as waktu_transaksi,
          sc_sale_orders.timezone,
          cust.name as nama_pelanggan,
          sc_sale_orders.is_from_form_order,
          m.name as outlet,
          m.name as cabang,
          c.name as shipping_name,
          sc_sale_orders.is_debet,
          u.fullname as nama_pencatat,
          (sc_sale_orders.total) as total

            ")->leftJoin('md_users as u','u.id','sc_sale_orders.created_by')
            ->leftJoin('md_transaction_types as t','t.id','sc_sale_orders.md_transaction_type_id')
            ->leftJoin('md_sc_shipping_categories as c','c.id','sc_sale_orders.md_sc_shipping_category_id')
            ->leftJoin('sc_customers as cust','cust.id','sc_sale_orders.sc_customer_id')
            ->leftJoin('acc_coa_details as cc','cc.id','sc_sale_orders.coa_trans_id')
                ->join('md_merchants as m','m.id','sc_sale_orders.md_merchant_id')
                ->where('sc_sale_orders.is_approved_shop',1)
                ->where('sc_sale_orders.is_keep_transaction',0)
                ->where('sc_sale_orders.is_cancel_user',0)
            ->where('sc_sale_orders.step_type',0)
            ->orderBy('sc_sale_orders.created_at','desc')
            ;


        }catch (\Exception $e)
        {
            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_sale_orders.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_sale_orders.second_code) like '%".strtolower(str_replace("'","''",$search))."%'")
                ->orWhereRaw("lower(cust.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        $searchKey = (array)json_decode(base64_decode($request->input('searchParams')));
        $searchFilter = is_array($searchKey) ? $searchKey : [];

        if(empty($searchFilter)){
            $data->where('sc_sale_orders.is_deleted',0);
        }


        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                    $data->where([$field => $searchFilter[$key]]);
            }
        }

        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

        if($request->md_merchant_id=='-1'){
            $getBranch=MerchantUtil::getBranch(merchant_id(),0);

            $data->whereIn('sc_sale_orders.md_merchant_id',$getBranch);
        }else{
            $data->where('sc_sale_orders.md_merchant_id',$request->md_merchant_id);
        }

        $data->whereRaw("
        sc_sale_orders.created_at::date between '$startDate' and '$endDate'
        ");


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','kredit','tipe_transaksi'],$isEdit,$isDelete,$isHead)
            ->editColumn('tipe_transaksi',function ($list){
                $trans=($list->trans_name=='Kas' || $list->trans_name=='Kas Kasir Outlet')?'':'-'.$list->trans_name;

                $name=is_null($list->trans_name)?$list->tipe_transaksi:$list->tipe_transaksi.$trans;


                if($list->is_from_form_order == 1){
                    return (is_null($list->tipe_transaksi))?
                                                        '<span>-</span>':
                                                        '<span style="color:green">'.$name.'</span>';
                } else {
                    return (is_null($list->tipe_transaksi))?
                                                        '<span style="color:red">Piutang</span>':
                                                        '<span style="color:green">'.$name.'</span>';
                }
            })
            ->editColumn('waktu_transaksi',function ($list){
                return Carbon::parse($list->waktu_transaksi)->isoFormat('dddd, D MMMM Y, HH:mm')." ".getTimeZoneName($list->timezone);
            })
            ->make(true);
    }

    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'cabang',
            'kode_transaksi',
            'jumlah',
            'biaya_pengiriman',
            'biaya_administrasi',
            'jumlah_diterima',
            'piutang',
            'tipe_transaksi',
            'waktu_transaksi'
        ];
    }


    public static function getFilterMap()
    {
        $field = [
            'md_sc_transaction_status_id' => 'sc_sale_orders.md_sc_transaction_status_id',
            'md_sc_shipping_category_id' => 'sc_sale_orders.md_sc_shipping_category_id',
            'is_debet' => 'sc_sale_orders.is_debet',
            'is_deleted'=>'sc_sale_orders.is_deleted',
            'md_merchant_id'=>'sc_sale_orders.md_merchant_id',
            'sc_customer_id' => 'sc_sale_orders.sc_customer_id',


        ];
        return $field;
    }

    public static function getButton($list){
        if($list->is_editable==1)
        {
            return "
                   <a href='".route('merchant.toko.sale-order.detail',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                        <i class='fa fa-eye' ></i>
                   </a>
                                          ";
        }else if($list->is_editable==2){
            return "
            <a href='".route('merchant.toko.sale-order.detail-ppob',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
               <i class='fa fa-eye' ></i>
            </a>
          ";
        }else{
            return "";
        }
    }

    public static function export()
    {
        try {
            return SaleOrder::selectRaw("
                sc_sale_orders.second_code,
                sc_sale_orders.code,
                sc_sale_orders.created_at as order_date,
                sc_sale_orders.created_at as order_time,
                'POINT OF SALE' as order_source,
                u.fullname as serve_by,
                sc_sale_orders.assign_to_user_helper,
                m.name as branch_name,
                m.email_merchant as branch_email,
                cl.name as customer_type,
                cust.name as customer_name,
                cust.code as customer_code,
                cust.email as customer_email,
                cust.phone_number as customer_phone,
                cust.address as customer_address,
                sm.name as brand,
                pc.name as item_group,
                p.name as item_name,
                p.code as item_sku,
                null as item_uom,
                null as serial_no,
                ssod.quantity as qty,
                coalesce(ssod.unit_name,mu.name) as unit,
                'IDR' as currency,
                p.selling_price as basic_price,
                ssod.price,
                0 as add_on_price,
                sc_sale_orders.promo_percentage as discount_percent,
                sc_sale_orders.promo as discount_amount,
                sc_sale_orders.tax_percentage as tax_percent,
                0 as service_charge,
                sc_sale_orders.tax as tax_amount,
                ssod.sub_total,
                ssod.cost_per_unit,
                ssod.total_cost,
                ssod.sub_total - ssod.total_cost as profit,
                com.commission_value,
                null as shipping_to,
                null as shipping_address,
                sc_sale_orders.note_order as notes,
                sc_sale_orders.md_sc_transaction_status_id as paid,
                t.name as tipe_transaksi,
                acd.name as trans_name,
                null as bank,
                null as bank_account_name,
                sc.name as shipping_courier,
                null as shipping_service_type,
                null as shipping_tracking_no,
                null as shipping_date,
                sc_sale_orders.shipping_cost as shipping_cost,
                sc_sale_orders.md_sc_transaction_status_id as order_status,
                sc_sale_orders.created_at as posting_date,
                sc_sale_orders.admin_fee as admin_fee_digital_payment,
                u.fullname as cashier_name,
                sc_sale_orders.timezone,
                sc_sale_orders.is_debet as is_debt,
                ssod.is_bonus as is_bonus_product
            ")->leftJoin('md_users as u','u.id','sc_sale_orders.created_by')
                ->leftJoin('md_transaction_types as t','t.id','sc_sale_orders.md_transaction_type_id')
                ->leftJoin('md_sc_shipping_categories as sc','sc.id','sc_sale_orders.md_sc_shipping_category_id')
                ->leftJoin('sc_customers as cust','cust.id','sc_sale_orders.sc_customer_id')
                ->leftJoin('acc_coa_details as acd','acd.id','sc_sale_orders.coa_trans_id')
                ->join('md_merchants as m','m.id','sc_sale_orders.md_merchant_id')
                ->join('sc_sale_order_details as ssod','ssod.sc_sale_order_id','sc_sale_orders.id')
                ->leftJoin('sc_products as p','p.id','ssod.sc_product_id')
                ->leftJoin('sc_merks as sm','sm.id','p.sc_merk_id')
                ->leftJoin('sc_product_categories as pc','pc.id','p.sc_product_category_id')
                ->leftJoin('md_units as mu','mu.id','p.md_unit_id')
                ->leftJoin('sc_customer_level as cl','cl.id','cust.sc_customer_level_id')
                ->leftJoin('merchant_commission_lists as com','com.sc_sale_order_id','sc_sale_orders.id')
                ->where('sc_sale_orders.is_approved_shop',1)
                ->where('sc_sale_orders.is_keep_transaction',0)
                ->where('sc_sale_orders.is_cancel_user',0)
                ->where('sc_sale_orders.step_type',0)
                ->orderBy('sc_sale_orders.created_at','desc');

        }catch (\Exception $e)
        {
            return collect([]);
        }
    }

}

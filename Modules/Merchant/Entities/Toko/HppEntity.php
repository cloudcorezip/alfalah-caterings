<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\CollectionPagination;
use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Product;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HppEntity extends Product
{


    public static function getDatasSingle($start_date,$end_date,$md_merchant_id,$search_product,$page='',$isConsignment,$isShowAll,$warehouse,$isPage=true)
    {

        if(is_null($start_date)){
            $startDate=Carbon::now()->startOfYear()->format('Y-m-d');
            $endDate=Carbon::now()->endOfYear()->format('Y-m-d');

        }else{
            $startDate=$start_date;
            $endDate=$end_date;
        }
        $searchProduct="";

        if(!is_null($search_product)){
            $searchProduct="and lower(p.name || ' ' || p.code) like '%".strtolower($search_product)."%'";
        }
        if($isShowAll==1 && $isConsignment==1)
        {
            $consigment="and p.is_consignment=$isConsignment";
        }elseif ($isShowAll==0 && $isConsignment==1)
        {
            $consigment="and p.is_consignment=$isConsignment";

        }else{
            $consigment="";

        }

        $data=collect(DB::select("
             select
              p.id,
              p.name || ' (' || p.code || ')' as nama_produk,
              p.md_sc_product_type_id,
              m.id as merchant_id,
              w.id as warehouse_id,
              m.name as nama_cabang,
              w.name as nama_gudang,
              t.id as type_id,
              t.name as tipe_gudang,
              u.id as satuan_id,
              u.name as satuan,
              sso.total,
              sso.residual_stock,
              sso.purchase_price,
              to_char(sso.created_at,'YYYY-MM-DD') as trans_time,
              sso.type,
              sso.transaction_action,
              sso.is_deleted,
               (
                case when p.md_sc_product_type_id = 4
                and p.is_with_stock = 0 then 1 when p.md_sc_product_type_id = 1
                and p.is_with_stock = 0 then 0 else 1 end
              ) as is_goods
            from
                  sc_products p
                  join md_merchants m on m.md_user_id = p.md_user_id
                  left join md_units u on u.id =p.md_unit_id
                  left join sc_stock_inventories sso on p.id = sso.sc_product_id
                  left join md_merchant_inv_warehouses w on w.id = sso.inv_warehouse_id
                  left join merchant_inv_warehouse_types t on t.id = w.type_id
             where
               p.is_deleted = 0
              and p.md_sc_product_type_id in(1, 2, 4, 3,8)
              $consigment
              and m.id in $md_merchant_id
              and w.id in $warehouse
              $searchProduct
            order by
              m.id asc
            "))->where('is_goods',1);

        $result=[];

        foreach ($data->groupBy('nama_produk') as  $key => $item)
        {
            $branchList=[];
            $totalAvailable=0;
            $totalIn=0;
            $totalOut=0;
            $totalInv=0;
            $totalHpp=0;
            $totalStock=0;
            foreach ($item->groupBy('merchant_id') as $key2=>$item2)
            {
                if(count($item2)>0)
                {
                    $warehouseList=[];
                    $stockBranch=0;
                    $stockBranchIn=0;
                    $stockBranchOut=0;
                    $invBranch=0;
                    foreach ($item2->groupBy('warehouse_id') as $key3 => $item3)
                    {
                        if($key3!='' || !is_null($key3))
                        {
                            if(count($item3)>0)
                            {
                                $warehouse=$item3->first();
                                $stockWarehouse=0;
                                $stockWarehouseIn=0;
                                $stockWarehouseOutIn=0;
                                $stockWarehouseInOut=0;
                                $invValueWarehouse=0;
                                foreach ($item3 as $gg)
                                {
                                    $ggTime=Carbon::parse($gg->trans_time)->toDateString();

                                    if($gg->type=='in' && $gg->is_deleted==0){
                                        if(strpos($gg->transaction_action,'Retur')===false)
                                        {
                                            $stockWarehouse+=$gg->residual_stock;
                                            $invValueWarehouse+=$gg->residual_stock*$gg->purchase_price;
                                            $totalStock+=$gg->total;
                                            $totalHpp+=$gg->total*$gg->purchase_price;

                                            if(($ggTime>=$startDate)  && ($ggTime<=$endDate)){
                                                $stockWarehouseIn+=$gg->total;

                                            }
                                        }
                                    }
                                    if($gg->type=='out' && $gg->is_deleted==0){

                                        if(($ggTime>=$startDate)  && ($ggTime<=$endDate)){
                                            $stockWarehouseInOut+=$gg->total;

                                        }
                                    }
                                    if($gg->type=='in' && $gg->is_deleted==0){
                                        if(strpos($gg->transaction_action,'Retur')!==false)
                                        {
                                            if(($ggTime>=$startDate)  && ($ggTime<=$endDate)){
                                                $stockWarehouseOutIn+=$gg->total;

                                            }
                                        }
                                    }
                                }

                                if(!is_null($warehouse->warehouse_id))
                                {
                                    $warehouseList[]=(object)[
                                        'warehouse_id'=>$warehouse->warehouse_id,
                                        'warehouse_name'=>$warehouse->nama_gudang,
                                        'warehouse_type'=>(is_null($warehouse->tipe_gudang))?'-':$warehouse->tipe_gudang,
                                        'stok_tersedia'=>round($stockWarehouse,2),
                                        'nilai_persediaan'=>rupiah($invValueWarehouse),
                                        'stok_masuk'=>round($stockWarehouseIn,2),
                                        'stok_keluar'=>round($stockWarehouseInOut-$stockWarehouseOutIn,2),
                                    ];
                                    $stockBranch+=$stockWarehouse;
                                    $stockBranchIn+=$stockWarehouseIn;
                                    $stockBranchOut+=$stockWarehouseInOut-$stockWarehouseOutIn;
                                    $invBranch+=$invValueWarehouse;
                                }

                            }
                        }

                    }
                    if($item2->first()->md_sc_product_type_id==4)
                    {
                        $id="'".$item2->first()->id."_".$item2->first()->merchant_id."'";
                        $opsi=' <a onclick="loadDetail('.$id.')" class="btn btn-xs btn-edit-xs btn-rounded"" title="Lihat Detail">
                                                <i class="fa fa-eye" style="color: #236FC3"></i>
                                            </a>';
                    }else{
                        $id="?id=".$item2->first()->id."&merchant_id=".$item2->first()->merchant_id."";
                        $opsi='<a target="_blank" href="'.route('merchant.toko.stock.hpp.detail').$id.'" class="btn btn-xs btn-edit-xs btn-rounded" title="Lihat Arus Stok">
                                                <i class="fa fa-sliders-h" style="color: #236FC3"></i>
                                            </a>';
                    }
                    $branchList[]=(object)[
                        'id'=>$item2->first()->merchant_id,
                        'merchant_name'=>$item2->first()->nama_cabang,
                        'stok_tersedia'=>round($stockBranch,2),
                        'nilai_persediaan'=>rupiah($invBranch),
                        'stok_masuk'=>round($stockBranchIn,2),
                        'stok_keluar'=>round($stockBranchOut,2),
                        'warehouse'=>$warehouseList,
                        'opsi'=>$opsi,
                    ];
                    $totalIn+=$stockBranchIn;
                    $totalAvailable+=$stockBranch;
                    $totalOut+=$stockBranchOut;
                    $totalInv+=$invBranch;
                }
            }
            $product=$item->first();

            $result[]=(object)[
                'id'=>$product->id,
                'nama_produk'=>$product->nama_produk,
                'satuan'=>$product->satuan,
                'stok_tersedia'=>round($totalAvailable,2),
                'hpp'=>($totalHpp==0)?0:rupiah($totalHpp/$totalStock,2),
                'nilai_persediaan'=>rupiah($totalInv),
                'stok_masuk'=>round($totalIn,2),
                'stok_keluar'=>round($totalOut,2),
                'md_sc_product_type_id'=>$product->md_sc_product_type_id,
                'merchant'=>$branchList,
                'merchant_id'=>$product->merchant_id,
                'warehouse_id'=>$product->warehouse_id,
                'merchant_name'=>$product->nama_cabang,
                'warehouse_name'=>$product->nama_gudang
            ];
        }

        if($isPage==false)
        {
            return collect($result);

        }else{
            return CollectionPagination::paginate(collect($result),5);

        }

    }


    public static function getDataHpp($startDate,$endDate,$merchantId,$search,$page='',$isConsignment,$isShowAll,$isPage=true)
    {
        try{

            $searchProduct="";
            if(!is_null($search)){
                $searchProduct="and lower(p.name || ' ' || p.code) like '%".strtolower($search)."%'";
            }
            if($isShowAll==1 && $isConsignment==1)
            {
                $consigment="and p.is_consignment=$isConsignment";
            }elseif ($isShowAll==0 && $isConsignment==1)
            {
                $consigment="and p.is_consignment=$isConsignment";

            }else{
                $consigment="";

            }
            $startDate=Carbon::parse($startDate)->toDateString();
            $endDate=Carbon::parse($endDate)->toDateString();
            $data=collect(DB::select("
             select
              p.id,
              p.name || ' (' || p.code || ')' as nama_produk,
              p.md_sc_product_type_id,
              m.id as merchant_id,
              w.id as warehouse_id,
              m.name as nama_cabang,
              w.name as nama_gudang,
              t.id as type_id,
              t.name as tipe_gudang,
              u.id as satuan_id,
              u.name as satuan,
              sso.total,
              sso.residual_stock,
              sso.purchase_price,
              to_char(sso.created_at,'YYYY-MM-DD') as trans_time,
              sso.type,
              sso.transaction_action,
              sso.is_deleted,
               (
                case when p.md_sc_product_type_id = 4
                and p.is_with_stock = 0 then 1 when p.md_sc_product_type_id = 1
                and p.is_with_stock = 0 then 0 else 1 end
              ) as is_goods
            from
                  sc_products p
                  join md_merchants m on m.md_user_id = p.md_user_id
                  left join md_units u on u.id =p.md_unit_id
                  left join sc_stock_inventories sso on p.id = sso.sc_product_id
                  left join md_merchant_inv_warehouses w on w.id = sso.inv_warehouse_id
                  left join merchant_inv_warehouse_types t on t.id = w.type_id
             where
               p.is_deleted = 0
              and p.md_sc_product_type_id in(1, 2, 4,3, 8)
              $consigment
              and m.id in $merchantId
              $searchProduct
            order by
              m.id asc
            "))->where('is_goods',1);

            $result=[];
            foreach ($data->groupBy('nama_produk') as  $key => $item)
            {
                $branchList=[];
                $totalAvailable=0;
                $totalIn=0;
                $totalOut=0;
                $totalInv=0;
                $totalHpp=0;
                $totalStock=0;
                foreach ($item->groupBy('merchant_id') as $key2=>$item2)
                {
                    if(count($item2)>0)
                    {
                        $warehouseList=[];
                        $stockBranch=0;
                        $stockBranchIn=0;
                        $stockBranchOut=0;
                        $invBranch=0;
                        foreach ($item2->groupBy('warehouse_id') as $key3 => $item3)
                        {
                            if($key3!='' || !is_null($key3))
                            {
                                if(count($item3)>0)
                                {
                                    $warehouse=$item3->first();
                                    $stockWarehouse=0;
                                    $stockWarehouseIn=0;
                                    $stockWarehouseOutIn=0;
                                    $stockWarehouseInOut=0;
                                    $invValueWarehouse=0;
                                    foreach ($item3 as $gg)
                                    {
                                        $ggTime=Carbon::parse($gg->trans_time)->toDateString();

                                        if($gg->type=='in' && $gg->is_deleted==0){
                                            if(strpos($gg->transaction_action,'Retur')===false)
                                            {
                                                $stockWarehouse+=$gg->residual_stock;
                                                $invValueWarehouse+=$gg->residual_stock*$gg->purchase_price;
                                                $totalStock+=$gg->total;
                                                $totalHpp+=$gg->total*$gg->purchase_price;

                                                if(($ggTime>=$startDate)  && ($ggTime<=$endDate)){
                                                    $stockWarehouseIn+=$gg->total;

                                                }
                                            }
                                        }
                                        if($gg->type=='out' && $gg->is_deleted==0){

                                            if(($ggTime>=$startDate)  && ($ggTime<=$endDate)){
                                                $stockWarehouseInOut+=$gg->total;

                                            }
                                        }
                                        if($gg->type=='in' && $gg->is_deleted==0){
                                            if(strpos($gg->transaction_action,'Retur')!==false)
                                            {
                                                if(($ggTime>=$startDate)  && ($ggTime<=$endDate)){
                                                    $stockWarehouseOutIn+=$gg->total;

                                                }
                                            }
                                        }
                                    }

                                    if(!is_null($warehouse->warehouse_id))
                                    {
                                        $warehouseList[]=(object)[
                                            'warehouse_id'=>$warehouse->warehouse_id,
                                            'warehouse_name'=>$warehouse->nama_gudang,
                                            'warehouse_type'=>(is_null($warehouse->tipe_gudang))?'-':$warehouse->tipe_gudang,
                                            'stok_tersedia'=>round($stockWarehouse,2),
                                            'nilai_persediaan'=>rupiah($invValueWarehouse),
                                            'stok_masuk'=>round($stockWarehouseIn,2),
                                            'stok_keluar'=>round($stockWarehouseInOut-$stockWarehouseOutIn,2),
                                        ];
                                        $stockBranch+=$stockWarehouse;
                                        $stockBranchIn+=$stockWarehouseIn;
                                        $stockBranchOut+=$stockWarehouseInOut-$stockWarehouseOutIn;
                                        $invBranch+=$invValueWarehouse;
                                    }

                                }
                            }

                        }
                        if($item2->first()->md_sc_product_type_id==4)
                        {
                            $id="'".$item2->first()->id."_".$item2->first()->merchant_id."'";
                            $opsi=' <a onclick="loadDetail('.$id.')" class="btn btn-xs btn-edit-xs btn-rounded"" title="Lihat Detail">
                                                <i class="fa fa-eye" style="color: #236FC3"></i>
                                            </a>';
                        }else{
                            $id="?id=".$item2->first()->id."&merchant_id=".$item2->first()->merchant_id."";
                            $opsi='<a target="_blank" href="'.route('merchant.toko.stock.hpp.detail').$id.'" class="btn btn-xs btn-edit-xs btn-rounded" title="Lihat Arus Stok">
                                                <i class="fa fa-sliders-h" style="color: #236FC3"></i>
                                            </a>';
                        }
                        $branchList[]=(object)[
                            'id'=>$item2->first()->merchant_id,
                            'merchant_name'=>$item2->first()->nama_cabang,
                            'stok_tersedia'=>round($stockBranch,2),
                            'nilai_persediaan'=>rupiah($invBranch),
                            'stok_masuk'=>round($stockBranchIn,2),
                            'stok_keluar'=>round($stockBranchOut,2),
                            'warehouse'=>$warehouseList,
                            'opsi'=>$opsi,
                        ];
                        $totalIn+=$stockBranchIn;
                        $totalAvailable+=$stockBranch;
                        $totalOut+=$stockBranchOut;
                        $totalInv+=$invBranch;
                    }
                }
                $product=$item->first();

                $result[]=(object)[
                    'id'=>$product->id,
                    'nama_produk'=>$product->nama_produk,
                    'satuan'=>$product->satuan,
                    'stok_tersedia'=>round($totalAvailable,2),
                    'hpp'=>($totalHpp==0)?0:rupiah($totalHpp/$totalStock,2),
                    'nilai_persediaan'=>rupiah($totalInv),
                    'stok_masuk'=>round($totalIn,2),
                    'stok_keluar'=>round($totalOut,2),
                    'merchant'=>$branchList,
                ];
            }

            if($isPage==false)
            {
                return collect($result);

            }else{
                return CollectionPagination::paginate(collect($result),5);

            }
        }catch (\Exception $e)
        {

            return collect([]);
        }
    }

}

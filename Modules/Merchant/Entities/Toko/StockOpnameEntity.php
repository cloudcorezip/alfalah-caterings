<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\StockOpname;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class StockOpnameEntity extends StockOpname
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('stock_adjustment',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return StockOpname::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_stock_opnames.id)'),
                'sc_stock_opnames.id',
                DB::raw('coalesce(sc_stock_opnames.second_code,sc_stock_opnames.code) as kode'),
                'sc_stock_opnames.desc as deskripsi',
                'sc_stock_opnames.created_at_by as waktu_pencatatan',
                'u.fullname as nama_pencatat',
                'm.name as outlet',
                'm.name as cabang',
                'w.name as gudang',
                'sc_stock_opnames.timezone as zona_waktu',
                'sc_stock_opnames.is_draf as publish',
                'm.id as merchant_id'

            ])->join('md_merchants as m','m.id','sc_stock_opnames.md_merchant_id')
                ->join('md_users as u','u.id','sc_stock_opnames.md_user_id_created')
                ->leftJoin('md_merchant_inv_warehouses as w','w.id','sc_stock_opnames.inv_warehouse_id')
                ->where('sc_stock_opnames.is_adjustment_stock',0)
                ->where('sc_stock_opnames.is_deleted',0);

        } catch (\Exception $e) {
            return [];

        }

    }

    public static function dataTable($request)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("sc_stock_opnames.created_at_by::date between '$startDate' and '$endDate'
        ");

        $getBranch=MerchantUtil::getBranch(merchant_id());
        $data->whereIn('m.id',$getBranch)->where('sc_stock_opnames.is_deleted',0);


        return(DataTable::getInstance())->dataTable(self::class,$data,$request,['opsi','publish'],
            ['u.fullname'])
            ->editColumn('zona_waktu',function ($list){

                return getTimeZoneName($list->zona_waktu);
            })->editColumn('publish',function ($list){
                return ($list->publish==0)?"<badge class='badge badge-success'><small style='color: white'>Ya</small></badge>":"<badge class='badge badge-warning'><small style='color: white'>Belum</small></badge>";
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1)
        {
            return [
                'row_number',
                'opsi',
                'kode',
                'waktu_pencatatan',
                'zona_waktu',
                'nama_pencatat',
                'gudang',
                'cabang',
                'publish'
            ];
        }else{
            return [
                'row_number',
                'opsi',
                'kode',
                'waktu_pencatatan',
                'zona_waktu',
                'nama_pencatat',
                'gudang',
                'publish'
            ];
        }
    }

    public static function getFilterMap()
    {
        $field = [
            'md_merchant_id'=>'m.id',
            'inv_warehouse_id'=>'w.id'
        ];
        return $field;
    }

    public static function getButton($list){

        if(merchant_id()==$list->merchant_id){

            if($list->publish==0){
                return "
                                            <a href='".route('merchant.toko.stock-opname.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                          ";
            }else{
                return "
                                            <a href='".route('merchant.toko.stock-opname.add',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-edit'></i>
                                            </a>
                                            <a href='".route('merchant.toko.stock-opname.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>

                                          ";
            }
        }else{
            if(self::getAccess()==true)
            {
                return "
                                            <a href='".route('merchant.toko.stock-opname.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                          ";
            }else{
                return "";
            }
        }




    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){
            if($permission==true){
                return '  <a href="'.route('merchant.toko.stock-opname.add').'"
                           class="btn btn-success text-white py-2 px-4 btn-rounded">
                            <i class="fa fa-plus mr-2"></i>
                            <span>Tambah</span>
                        </a>';
            }
        }elseif (merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return '  <a href="'.route('merchant.toko.stock-opname.add').'"
                           class="btn btn-success text-white py-2 px-4 btn-rounded">
                            <i class="fa fa-plus mr-2"></i>
                            <span>Tambah</span>
                        </a>';
            }
        }
        else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return '  <a href="'.route('merchant.toko.stock-opname.add').'"
                           class="btn btn-success text-white py-2 px-4 btn-rounded">
                            <i class="fa fa-plus mr-2"></i>
                            <span>Tambah</span>
                        </a>';
                }
            }else{
                return "";
            }
        }

    }

}

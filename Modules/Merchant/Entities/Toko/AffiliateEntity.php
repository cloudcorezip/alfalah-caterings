<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantStaff;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class AffiliateEntity extends MerchantStaff
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('crm', merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return MerchantStaff::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_staff.id)'),
                'md_merchant_staff.id',
                'u.fullname as nama_lengkap',
                'md_merchant_staff.email',
                'md_merchant_staff.address as alamat',
                'm.name as outlet',
                'm.name as cabang',
                'm.id as merchant_id'
            ])
                ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                 ->where('md_merchant_staff.is_non_employee',1)
                ->orderBy('md_merchant_staff.id','desc');

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(u.fullname) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }

        $getBranch=MerchantUtil::getBranch(merchant_id(),0);

        if(merchant_detail()->is_branch==0)
        {
            $data->whereIn('m.id',$getBranch)->where('md_merchant_staff.is_deleted',0);

        }else{
            $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('md_merchant_staff.is_deleted',0);

        }


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'], $isEdit,$isDelete,$isHead)
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1)
        {
            return [
                'row_number',
                'nama_lengkap',
                'email',
                'alamat',
                'opsi'
            ];
        }else{
            return [
                'row_number',
                'nama_lengkap',
                'email',
                'alamat',
                'opsi'
            ];
        }

    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id())
        {

            if($isHead==true)
            {
                return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.affiliate.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.affiliate.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.affiliate.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }

    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.affiliate.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }
        else if(merchant_detail()->is_branch==0){
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.affiliate.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.affilate.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            }else{
                return "";
            }
        }

    }


}

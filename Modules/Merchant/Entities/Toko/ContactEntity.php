<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\Supplier;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class ContactEntity 
{

    public static function getDataForDataTableCustomer()
    {

        try {
            return $customer=Customer::
            select([
                'sc_customers.id',
                'sc_customers.code as kode',
                'sc_customers.name as nama',
                'sc_customers.is_deleted',
                'm.id as merchant_id',
                DB::raw('1 as type')
            ])
                ->join('md_users as u','u.id','sc_customers.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->leftJoin('sc_customer_level as l','l.id','sc_customers.sc_customer_level_id')
                ->orderBy('sc_customers.id','desc');

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function getDataForDataTableSupplier()
    {

        try {
            return $supplier= Supplier::
            select([
                'sc_suppliers.id',
                'sc_suppliers.code as kode',
                'sc_suppliers.name as nama',
                'sc_suppliers.is_deleted',
                'm.id as merchant_id',
                DB::raw('0 as type')
            ])
            ->join('md_users as u','u.id','sc_suppliers.md_user_id')
            ->join('md_merchants as m','m.md_user_id','u.id')
            ->leftJoin('sc_supplier_categories as c','c.id','sc_suppliers.sc_supplier_categories_id')
            ->orderBy('sc_suppliers.id','desc')
            ;
        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function listOption($search='')
    {
        $supplier=self::getDataForDataTableSupplier();
        $customer=self::getDataForDataTableCustomer();

        if($search!=''){
             $supplier->whereRaw("lower(sc_suppliers.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->orWhereRaw("lower(sc_suppliers.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('sc_suppliers.is_deleted',0);

            $customer->whereRaw("lower(sc_customers.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->orWhereRaw("lower(sc_customers.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('sc_customers.is_deleted',0);
            
            return $supplier->union($customer)->get();

        }else{
            $supplier->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('sc_suppliers.is_deleted',0);
            $customer->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('sc_customers.is_deleted',0);
            return $supplier->union($customer)->get();
        }

    }


}

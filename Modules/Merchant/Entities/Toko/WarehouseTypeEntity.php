<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\MasterData\MerchantWarehouseType;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class WarehouseTypeEntity extends MerchantWarehouseType
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('inventory', merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return MerchantWarehouseType::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY merchant_inv_warehouse_types.id)'),
                'merchant_inv_warehouse_types.id',
                'merchant_inv_warehouse_types.name as tipe_gudang',
                'merchant_inv_warehouse_types.desc as deskripsi',
                'm.id as merchant_id'
            ])
                ->join('md_merchants as m','m.id','merchant_inv_warehouse_types.md_merchant_id')
                ->orderBy('merchant_inv_warehouse_types.id','asc');
        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function listOption()
    {
        $getBranch=MerchantUtil::getBranch(merchant_id(),1);

        return self::getDataForDataTable()->whereIn('m.id',$getBranch)->where('merchant_inv_warehouse_types.is_deleted',0)
            ->get();
    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->whereRaw("lower(merchant_inv_warehouse_types.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
            ;
        }
        $data->where('m.id',merchant_id())->where('merchant_inv_warehouse_types.is_deleted',0);

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],$isEdit,$isDelete,$isHead)
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'tipe_gudang',
            'deskripsi',
            'opsi'
        ];
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id()){
            if($isHead==true)
            {
                return "<a  onclick='loadModal(this)' target='".route('merchant.toko.warehouse-type.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";

            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModal(this)' target='".route('merchant.toko.warehouse-type.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";

                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";

                }elseif($edit==false && $delete==false){
                    return "";
                }else{

                    return "<a  onclick='loadModal(this)' target='".route('merchant.toko.warehouse-type.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";

                }
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.warehouse-type.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }
        else if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.warehouse-type.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.warehouse-type.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            }else{

                return "";

            }
        }

    }
}

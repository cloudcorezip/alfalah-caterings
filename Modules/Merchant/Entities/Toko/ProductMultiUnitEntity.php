<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\MultiUnit;
use App\Models\SennaToko\ProductSellingLevel;
use Illuminate\Support\Facades\DB;

class ProductMultiUnitEntity extends MultiUnit
{
    public static function getDataForMultiUnit($id)
    {
        try {
            return MultiUnit::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_product_multi_units.id)'),
                'sc_product_multi_units.id',
                'sc_product_multi_units.quantity as nilai_konversi',
                'p.name as produk',
                'u2.name as konversi_ke',
                'u.name as konversi_dari',
                'sc_product_multi_units.price as harga_jual',
                'sc_product_multi_units.sc_product_id',
                'sc_product_multi_units.md_unit_id as unit_id',
                'p.md_unit_id as product_unit_id'
            ])
                ->join('sc_products as p','p.id','sc_product_multi_units.sc_product_id')
                ->leftJoin('md_units as u','u.id','sc_product_multi_units.md_unit_id')
                ->leftJoin('md_units as u2','u2.id','p.md_unit_id')
                ->where('sc_product_multi_units.sc_product_id',$id)
                ->where('sc_product_multi_units.is_deleted',0)
                ->orderBy('sc_product_multi_units.id','asc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }
    }

    public static function dataTableMultiUnit($request,$id)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForMultiUnit($id),$request,['opsi'],
            ['p.name'])
            ->editColumn('harga_jual',function ($list){
                return rupiah($list->harga_jual).' per '.$list->konversi_dari;
            })
            ->editColumn('konversi_dari', function($list){
                if($list->unit_id == $list->product_unit_id){
                    return $list->konversi_dari.' (satuan default)';
                } else {
                    return $list->konversi_dari;
                }
            })
            ->make(true);
    }

    public static function dataTableColumns()
    {
        return [
            'row_number',
            'produk',
            'konversi_dari',
            'konversi_ke',
            'nilai_konversi',
            'harga_jual',
            'opsi'
        ];
    }

    public static function add($productId)
    {
        return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.product.multi-unit.add').'?sc_product_id='.$productId.'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
    }

    public static function getButton($list)
    {

        return "<a  onclick='loadModal(this)' target='".route('merchant.toko.product.multi-unit.add',['id'=>$list->id,'sc_product_id'=>$list->sc_product_id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteDataMultiUnit($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\ReturSaleOrder;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReturSaleOrderEntity
{
    public static function getDataForDataTable()
    {

        try {
            return ReturSaleOrder::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_retur_sale_orders.id)'),
                'sc_retur_sale_orders.id',
                'sc_retur_sale_orders.code as kode',
                'sc_retur_sale_orders.total as total_retur',
                'sc_retur_sale_orders.created_at as tanggal_retur',
                's.name as nama_pelanggan',
                'p.id as sale_id',
                'p.step_type',
                'p.code as asal_faktur',
                'm.name as outlet',
                'm.name as cabang',
                'm.id as merchant_id',
                'sc_retur_sale_orders.timezone',
                'sc_retur_sale_orders.reason_id',
                'sc_retur_sale_orders.second_code',
                'sc_retur_sale_orders.code',
                'p.second_code as second_code_',
                'p.code as code_',

            ])
                ->join('sc_sale_orders as p','p.id','sc_retur_sale_orders.sc_sale_order_id')
                ->leftJoin('md_users as u','u.id','sc_retur_sale_orders.created_by')
                ->leftJoin('sc_customers as s','s.id','p.sc_customer_id')
                ->join('md_merchants as m','m.id','p.md_merchant_id')
                ->where('p.is_deleted',0)
                ->where('sc_retur_sale_orders.is_deleted',0)
                ->orderBy('sc_retur_sale_orders.id','desc')
                ;
        } catch (\Exception $e) {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_retur_sale_orders.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_retur_sale_orders.second_code) like '%".strtolower(str_replace("'","''",$search))."%'")
                ->orWhereRaw("lower(s.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $getBranch=MerchantUtil::getBranch(merchant_id(),0);
        $data->whereIn('p.md_merchant_id',$getBranch);

        $data->whereRaw("
        sc_retur_sale_orders.created_at::date between '$startDate' and '$endDate'
        ");
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],$isEdit,$isDelete,$isHead)
        ->editColumn('tanggal_retur',function ($list){
            return Carbon::parse($list->tanggal_retur)->isoFormat('dddd, D MMMM Y, HH:mm').' '.getTimeZoneName($list->timezone);
        })->editColumn('total_retur',function ($list){
                return rupiah($list->total_retur);
            })
            ->editColumn('asal_faktur',function ($list){
                if($list->step_type==3){
                    return is_null($list->second_code_)?str_replace('S','SD',$list->code_):$list->second_code_;
                }else{
                    return is_null($list->second_code_)?$list->code_:$list->second_code_;
                }
            })
        ->editColumn('nama_pelanggan',function ($list){
            if(is_null($list->nama_pelanggan)){
                return "Tanpa Pelanggan";
            }else{
                return $list->nama_pelanggan;
            }
        })
        ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode',
            'asal_faktur',
            'total_retur',
            'tanggal_retur',
            'nama_pelanggan',
            'cabang'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'sc_customer_id'=>'p.sc_customer_id',
            'md_merchant_id'=>'p.md_merchant_id',

        ];
        return $field;
    }

    public static function getButton($list){
        if($list->merchant_id==merchant_id())
        {
            return "
                                            <a onclick='loadModalFullScreen(this)' target='".route('merchant.toko.transaction.sale-order.detail-retur')."' data='id=".$list->id."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                            <a title='Hapus Retur' onclick='hapusRetur(".$list->id.",".$list->step_type.")' class='btn btn-xs btn-delete-xs btn-rounded' data-original-title='Hapus Retur'><i class='fa fa-trash-alt' style='color: red'></i></a>
                                            <a title='Cetak Retur' href='".route('merchant.toko.sale-order.export-data-retur',['id' => $list->sale_id, 'returId' => $list->id])."' class='btn btn-xs btn-edit-xs text-success btn-rounded'><i class='fa fa-download'></i></a>

                                          ";
        }else{
            return "
                                            <a onclick='loadModalFullScreen(this)' target='".route('merchant.toko.transaction.sale-order.detail-retur')."' data='id=".$list->id."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                            <a title='Cetak Retur' href='".route('merchant.toko.sale-order.export-data-retur',['id' => $list->sale_id, 'returId' => $list->id])."' class='btn btn-xs btn-edit-xs text-success btn-rounded'><i class='fa fa-download'></i></a>

                                          ";
        }

    }
}

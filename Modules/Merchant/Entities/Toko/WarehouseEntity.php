<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\SennaCashier\CashDrawer;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class WarehouseEntity extends MerchantWarehouse
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('inventory',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return MerchantWarehouse::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_inv_warehouses.id)'),
                'md_merchant_inv_warehouses.id',
                'md_merchant_inv_warehouses.name as nama_gudang',
                'md_merchant_inv_warehouses.desc as deskripsi',
                'md_merchant_inv_warehouses.is_default',
                'm.id as merchant_id',
                'm.name as nama_cabang',
                'md_merchant_inv_warehouses.is_to_all as berlaku_untuk_cabang',
                't.name as tipe_gudang',
                'm.id as merchant_id'

            ])
                ->join('md_merchants as m','m.id','md_merchant_inv_warehouses.md_merchant_id')
            ->leftJoin('merchant_inv_warehouse_types as t','t.id','md_merchant_inv_warehouses.type_id')
                ->orderBy('md_merchant_inv_warehouses.id','asc');
        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $getBranch=MerchantUtil::getBranch(merchant_id(),0);


        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->whereRaw("lower(md_merchant_inv_warehouses.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
            ;
        }


        $data->whereIn('m.id',$getBranch)->where('md_merchant_inv_warehouses.is_deleted',0);


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['berlaku_untuk_cabang','opsi'],$isEdit,$isDelete,$isHead)
            ->editColumn('berlaku_untuk_cabang',function ($list){
                if($list->berlaku_untuk_cabang==1)
                {
                    return "<label class='badge badge-dark'>Untuk semua cabang</label>";
                }else{
                    return "<label class='badge badge-info'>$list->nama_cabang</label>";

                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1)
        {
            return [
                'row_number',
                'nama_gudang',
                'tipe_gudang',
                'berlaku_untuk_cabang',
                'deskripsi',
                'opsi'
            ];

        }else{
            return [
                'row_number',
                'nama_gudang',
                'tipe_gudang',
                'deskripsi',
                'opsi'
            ];
        }

    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id())
        {
            if($isHead==true)
            {
                if($list->is_default==1){
                    return "<a  onclick='loadModal(this)' target='".route('merchant.toko.warehouse.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Edit Data'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }else{
                    return "<a  onclick='loadModal(this)' target='".route('merchant.toko.warehouse.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Edit Data'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }


            }else{
                if($edit==true && $delete==false)
                {
                    if($list->is_default==0){
                        return "<a  onclick='loadModal(this)' target='".route('merchant.toko.warehouse.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Edit Data'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                    }else{
                        return "";
                    }


                }elseif ($edit==false && $delete==true)
                {
                    if($list->is_default==0){
                        return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                    }else{
                        return  "";
                    }

                }elseif($edit==false && $delete==false){
                    return "";
                }else{

                    if($list->is_default==0)
                    {
                        return "<a  onclick='loadModal(this)' target='".route('merchant.toko.warehouse.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Edit Data'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                    }else{
                        return "<a  onclick='loadModal(this)' target='".route('merchant.toko.warehouse.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Edit Data'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                          ";
                    }

                }
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.warehouse.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }
        else if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.warehouse.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }else{

            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.warehouse.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            }else{

                return "";
            }
        }

    }
}

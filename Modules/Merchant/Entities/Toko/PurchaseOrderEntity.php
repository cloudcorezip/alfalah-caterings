<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\SaleOrder;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PurchaseOrderEntity extends PurchaseOrder
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('purchase',merchant_id());
    }
    public static function getDataForDataTable()
    {

        try {
            return PurchaseOrder::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_purchase_orders.id)'),
                'sc_purchase_orders.id',
                DB::raw('coalesce(sc_purchase_orders.second_code,sc_purchase_orders.code) as kode_pembelian'),
                'sc_purchase_orders.code',
                'sc_purchase_orders.second_code',
                't.name as tipe_transaksi',
                'c.name as trans_name',
                DB::raw('rupiah(sc_purchase_orders.total) as total_tagihan'),
                DB::raw('rupiah(sc_purchase_orders.tax) as pajak'),
                DB::raw('rupiah(sc_purchase_orders.discount) as diskon'),
                DB::raw("
                case when sc_purchase_orders.is_debet = 1 then
                      coalesce(
                        (
                          select
                            sum(amp.residual_amount)
                          from
                            acc_merchant_ap amp
                          where
                            amp.apable_id = sc_purchase_orders.id
                            and sc_purchase_orders.is_debet = 1
                            and amp.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                        ),
                        0
                      ) else 0 end as hutang

                "),
                'sc_purchase_orders.created_at as waktu_transaksi',
                'sc_purchase_orders.timezone',
                'sc_purchase_orders.is_editable',
                's.name as nama_supplier',
                'm.name as gudang',
                'm.name as outlet',
                'm.name as cabang',


            ])
                ->leftJoin('md_transaction_types as t','t.id','sc_purchase_orders.md_transaction_type_id')
                ->leftJoin('md_users as u','u.id','sc_purchase_orders.created_by')
                ->leftJoin('sc_suppliers as s','s.id','sc_purchase_orders.sc_supplier_id')
                ->leftJoin('acc_coa_details as c','c.id','sc_purchase_orders.coa_trans_id')
                ->join('md_merchants as m','m.id','sc_purchase_orders.md_merchant_id')
                ->join('md_merchant_inv_warehouses as inv','inv.id','sc_purchase_orders.inv_warehouse_id')
                ->where('sc_purchase_orders.step_type',0)
                ->orderBy('sc_purchase_orders.id','desc')
                ;
        } catch (\Exception $e) {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_purchase_orders.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_purchase_orders.second_code) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(s.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        if(empty($searchFilter)){
            $data->where('sc_purchase_orders.is_deleted', 0);
        }

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

        $getBranch=MerchantUtil::getBranch(merchant_id(),0);

        $data->whereIn('sc_purchase_orders.md_merchant_id',$getBranch);

        $data->whereRaw("
        sc_purchase_orders.created_at::date between '$startDate' and '$endDate'
        ");
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','kredit','tipe_transaksi'],$isEdit,$isDelete,$isHead)
        ->editColumn('tipe_transaksi',function ($list){
            $trans=($list->trans_name=='Kas' || $list->trans_name=='Kas Kasir Outlet')?'':'-'.$list->trans_name;
            $name=is_null($list->trans_name)?$list->tipe_transaksi:$list->tipe_transaksi.$trans;

            return (is_null($list->tipe_transaksi))?
                                                    '<span style="color:red">Hutang</span>':
                                                    '<span style="color:green">'.$name.'</span>';
        })


        ->editColumn('waktu_transaksi',function ($list){
            return Carbon::parse($list->waktu_transaksi)->isoFormat('dddd, D MMMM Y, HH:mm')." ".getTimeZoneName($list->timezone);
        })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode_pembelian',
            'nama_supplier',
            'total_tagihan',
            'hutang',
            'tipe_transaksi',
            // 'gudang',
            // 'cabang',
            'waktu_transaksi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'is_debet'=>'sc_purchase_orders.is_debet',
            'is_deleted' => 'sc_purchase_orders.is_deleted',
            'sc_supplier_id' => 'sc_purchase_orders.sc_supplier_id',
            'md_merchant_id'=>'sc_purchase_orders.md_merchant_id',
            'inv_warehouse_id'=>'sc_purchase_orders.inv_warehouse_id'
        ];
        return $field;
    }

    public static function getButton($list){

        if($list->is_editable==1)
        {
            if(count(MerchantUtil::getBranch(merchant_id()))>1){
                return "


                                            <a href='".route('merchant.toko.purchase-order.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                             <button type='button' class='btn btn-xs btn-success-xs text-white btn-rounded' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                    <i class='fa fa-download'></i>
                                                </button>
                                                <div class='dropdown-menu'>
                                                    <a class='dropdown-item' href='".route('merchant.toko.purchase-order.export-data-faktur-pdf',['id'=>$list->id])."'>PDF</a>
                                                    <a class='dropdown-item' onclick='exportDataSingle($list->id)'>Excel</a>
                                                </div>
                                          ";
            }else if(merchant_detail()->is_branch==0)
            {
                return "


                                            <a href='".route('merchant.toko.purchase-order.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                             <button type='button' class='btn btn-xs btn-success-xs text-white btn-rounded' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                    <i class='fa fa-download'></i>
                                                </button>
                                                <div class='dropdown-menu'>
                                                    <a class='dropdown-item' href='".route('merchant.toko.purchase-order.export-data-faktur-pdf',['id'=>$list->id])."'>PDF</a>
                                                    <a class='dropdown-item' onclick='exportDataSingle($list->id)'>Excel</a>
                                                </div>
                                          ";
            }else{
                if(self::getAccess()==true)
                {
                    return "


                                            <a href='".route('merchant.toko.purchase-order.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                             <button type='button' class='btn btn-xs btn-success-xs text-white btn-rounded' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                    <i class='fa fa-download'></i>
                                                </button>
                                                <div class='dropdown-menu'>
                                                    <a class='dropdown-item' href='".route('merchant.toko.purchase-order.export-data-faktur-pdf',['id'=>$list->id])."'>PDF</a>
                                                    <a class='dropdown-item' onclick='exportDataSingle($list->id)'>Excel</a>
                                                </div>
                                          ";
                }
            }

        }else{
            return "";
        }
    }
}
<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\PermitStaff;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PermitStaffEntity extends PermitStaff
{
    public static function getDataForDataTable()
    {

        try {
            return PermitStaff::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_staff_permit_submissions.id)'),
                'md_merchant_staff_permit_submissions.id',
                'md_merchant_staff_permit_submissions.md_staff_user_id',
                't.fullname as karyawan',
                'p.name as kategori_izin',
                'hh.name as hr_kategori_izin',
                'md_merchant_staff_permit_submissions.is_approved as status',
                'start_date as mulai_izin',
                'end_date as selesai_izin'
            ])
                ->leftJoin('md_permit_categories as p','p.id','md_merchant_staff_permit_submissions.md_permit_category_id')
                ->leftJoin('hr_merchant_leave_settings as hh','hh.id','md_merchant_staff_permit_submissions.hr_leave_id')
                ->join('md_users as t','t.id','md_merchant_staff_permit_submissions.md_staff_user_id')
                ->where('md_merchant_staff_permit_submissions.md_merchant_id',merchant_id())
                ;
        } catch (\Exception $e) {
            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(t.fullname) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }

        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("md_merchant_staff_permit_submissions.created_at::date between '$startDate' and '$endDate'
        ");

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','status'],$isEdit,$isDelete,$isHead)
            ->editColumn('status',function ($list){
                if($list->status==0){
                    return '<h6><span class="badge badge-secondary">Menunggu Persetujuan</span></h6>';
                }else if($list->status==1){
                    return '<h6><span class="badge badge-success">Telah disetujui</span></h6>';
                }else{
                    return '<h6><span class="badge badge-danger">Ditolak</span></h6>';
                }
            })
            ->editColumn('kategori_izin',function ($list){
                if(is_null($list->kategori_izin)){
                    return $list->hr_kategori_izin;
                } else {
                    return $list->kategori_izin;
                }
            })
            ->editColumn('mulai_izin', function($list){
                return Carbon::parse($list->mulai_izin)->isoFormat('DD MMMM YYYY');
            })
            ->editColumn('selesai_izin', function($list){
                return Carbon::parse($list->selesai_izin)->isoFormat('DD MMMM YYYY');
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'karyawan',
            'kategori_izin',
            'mulai_izin',
            'selesai_izin',
            'status',
            'opsi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'is_approved' => 'md_merchant_staff_permit_submissions.is_approved',
        ];
        return $field;
    }

    public static function getButton($list){
        return "
                <a onclick='loadModal(this)' target='".route('merchant.toko.permit-staff.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                    <i class='fa fa-eye'></i>
                </a>
            ";
    }

    public static function add($permission)
    {
        return '<div class="col-md-12 mb-3">
                    <a class="btn btn-success btn-rounded btn-sm py-2 px-4" href="'.route('merchant.toko.permit-staff.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                </div>';

    }
}

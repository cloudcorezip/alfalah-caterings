<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\StaffAttendance;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AttendancePerEmployeeEntity extends StaffAttendance
{
    public static function getDataForDataTable($merchant_id, $staff_user_id)
    {

        try {
            return StaffAttendance::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_staff_attendances.id)'),
                'md_merchant_staff_attendances.*',
                'md_merchant_staff_attendances.md_staff_user_id',
                't.fullname as nama_staff',
                'md_merchant_staff_attendances.start_work as jam_masuk',
                'md_merchant_staff_attendances.end_work as jam_selesai',
                'md_merchant_staff_attendances.type_of_attendance as status',
                'md_merchant_staff_attendances.created_at as tanggal'
            ])
                ->join('md_users as t','t.id','md_merchant_staff_attendances.md_staff_user_id')
                ->where('md_merchant_staff_attendances.md_merchant_id',$merchant_id)
                ->where('md_merchant_staff_attendances.md_staff_user_id',$staff_user_id)
                ->where('md_merchant_staff_attendances.is_deleted',0)
                ->orderBy('md_merchant_staff_attendances.created_at','desc')
                ;
        } catch (\Exception $e) {
            return [];

        }

    }

    public static function dataTable($request, $isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable($request->merchant_id, $request->staff_user_id);
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];
        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("md_merchant_staff_attendances.created_at::date between '$startDate' and '$endDate'
        ");

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(t.fullname) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi', 'status'],$isEdit,$isDelete,$isHead)
            ->editColumn('status',function ($list){
                if($list->status=='S'){
                    return '<h6><span class="badge badge-info">Izin Sakit</span></h6>';
                }else if($list->status=='I'){
                    return '<h6><span class="badge badge-info">Izin</span></h6>';
                }else if($list->status=='A'){
                    return '<h6><span class="badge badge-danger">Tidak Hadir</span></h6>';
                }else{
                    return '<h6><span class="badge badge-success">Hadir</span></h6>';
                }
            })
            ->editColumn('tanggal', function($list){
                return Carbon::parse($list->created_at)->isoFormat('dddd, D MMMM Y');
            })
            ->editColumn('jam_masuk',function ($list){
                return Carbon::parse($list->jam_masuk)->isoFormat('HH:mm');
            })
            ->editColumn('jam_selesai',function ($list){
                return (is_null($list->jam_selesai))?'-':Carbon::parse($list->jam_selesai)->isoFormat('HH:mm');
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'tanggal',
            'status',
            'jam_masuk',
            'jam_selesai',
            'opsi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [];
        return $field;
    }

    public static function getButton($list){

         return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.report.staff-attendance.detail-attendance',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                    <span class='fa fa-eye' style='color: white'></span>
                </a>";
    }
}

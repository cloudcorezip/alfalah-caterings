<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\ClinicReservation;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReservationEntity extends ClinicReservation
{
    public static function getDataForDataTable()
    {

        try {
            return ClinicReservation::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY clinic_reservations.id)'),
                'clinic_reservations.id',
                'clinic_reservations.reservation_number as no_reservasi',
                'clinic_reservations.reservation_date as tanggal',
                'sc.code as no_member',
                'sc.name as nama_lengkap',
                'sp.name as nama_layanan',
                'u.fullname as dokter',
                'clinic_reservations.is_type',
                'clinic_reservations.queue_number as no_antrian'
            ])
                ->join('sc_customers as sc', 'sc.id', 'clinic_reservations.sc_customer_id')
                ->join('sc_products as sp', 'sp.id', 'clinic_reservations.sc_product_id')
                ->join('md_merchant_staff as mms', 'mms.id', 'clinic_reservations.md_merchant_staff_id')
                ->join('md_users as u', 'u.id', 'mms.md_user_id')
                ->where('clinic_reservations.is_deleted', 0)
                ->where('clinic_reservations.md_merchant_id', merchant_id())
                ->orderBy('clinic_reservations.reservation_date','desc')
                ;

        }catch (\Exception $e)
        {
            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(clinic_reservations.reservation_number) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc.name) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(sc.code) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(u.fullname) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1' && $searchFilter[$key] != '-3') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        if(array_key_exists('start_date', $searchFilter) && array_key_exists('end_date', $searchFilter)){
            if(!is_null($searchFilter['start_date']) && !is_null($searchFilter['end_date'])){
                $startDate = $searchFilter['start_date'];
                $endDate = $searchFilter['end_date'];
                $data->whereRaw("
                    clinic_reservations.reservation_date::date between '$startDate' and '$endDate'
                ");
            }
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi', 'status'], $isEdit,$isDelete,$isHead)
            ->editColumn('tanggal', function($list){
                return Carbon::parse($list->tanggal)->isoFormat('dddd, D MMMM Y, HH:mm');
            })
            ->editColumn('no_antrian', function($list){
                if(is_null($list->no_antrian)){
                    return '-';
                } else {
                    return $list->no_antrian;
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'no_reservasi',
            'no_member',
            'nama_lengkap',
            'tanggal',
            'no_antrian',
            'dokter',
            'opsi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'reservation_date' => 'reservation_date',

        ];
        return $field;
    }

    public static function getButton($list,$edit,$delete,$isHead)
    {
        if($list->is_type == 1){
            return "<a href='".route('merchant.toko.reservation.detail', ['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                    <span class='fa fa-eye' style='color: white'></span>
                </a>
                <a href='".route('merchant.toko.reservation.add', ['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Edit'>
                    <span class='fa fa-edit' style='color: white'></span>
                </a>
                <a onclick='createQueue($list->id)' class='btn btn-xs btn-edit-xs btn-rounded' title='Buat no antrian'>
                    <span style='color:blue;' class='iconify' data-icon='fluent:arrow-rotate-clockwise-20-filled'></span>
                </a>
                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded' title='Batalkan reservasi'>
                    <span class='fa fa-trash-alt' style='color: white'></span>
                </a>
                ";
        } else {
            return "<a href='".route('merchant.toko.reservation.detail', ['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                    <span class='fa fa-eye' style='color: white'></span>
                </a>
                <a href='".route('merchant.toko.reservation.add', ['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Edit'>
                    <span class='fa fa-edit' style='color: white'></span>
                </a>
                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded' title='Batalkan reservasi'>
                    <span class='fa fa-trash-alt' style='color: white'></span>
                </a>
                ";
        }
    }

    public static function add($permission)
    {
        if($permission==true){
            return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" href="'.route('merchant.toko.reservation.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
        }
    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\Supplier;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class CustomerEntity extends Customer
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('customer',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return Customer::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_customers.id)'),
                'sc_customers.id',
                'sc_customers.code as kode',
                'sc_customers.name as nama_pelanggan',
                'sc_customers.email',
                'sc_customers.phone_number as no_telp',
                'sc_customers.address as alamat',
                'l.name as kategori_pelanggan',
                'sc_customers.is_google_contact',
                'sc_customers.google_contact_id',
                'sc_customers.etag',
                'sc_customers.resource_name',
                'm.name as terdaftar_di_cabang',
                'sc_customers.md_user_id',
                'm.id as merchant_id'
                //'point as poin',
            ])
                ->join('md_users as u','u.id','sc_customers.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->leftJoin('sc_customer_level as l','l.id','sc_customers.sc_customer_level_id')
                ->orderBy('sc_customers.id','desc');

        }catch (\Exception $e)
        {

            return [];

        }

    }


    public static function listOption($search='')
    {
        $data=self::getDataForDataTable();
        if($search!=''){
           return $data->whereRaw("lower(sc_customers.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->orWhereRaw("lower(sc_customers.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
               ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('sc_customers.is_deleted',0)
                ->get();

        }else{
            return $data
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('sc_customers.is_deleted',0)
                ->get();
        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_customers.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_customers.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_customers.email) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_customers.address) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(l.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_customers.phone_number) like '%".strtolower(str_replace("'","''",$search))."%' ");

            });
        }

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        if(isset($searchFilter['md_merchant_id'])){
            $data->whereIn('m.id',$searchFilter['md_merchant_id'])
                    ->where('sc_customers.is_deleted',0);
        } else {
            $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id()))
                    ->where('sc_customers.is_deleted',0);
        }

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],$isEdit,$isDelete,$isHead)
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){
            return [
                'row_number',
                'kode',
                'nama_pelanggan',
                'email',
                'no_telp',
                'alamat',
                'kategori_pelanggan',
                'terdaftar_di_cabang',
                'opsi'
            ];
        }else{
            return [
                'row_number',
                'kode',
                'nama_pelanggan',
                'email',
                'no_telp',
                'alamat',
                'kategori_pelanggan',
                'opsi'
            ];
        }
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id())
        {
            if($isHead==true)
            {
                return "<a href='".route('merchant.toko.customer.add-customer',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                        <span class='fa fa-edit' style='color: white'></span>
                    </a>
                    <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                        <span class='fa fa-trash-alt' style='color: white'></span>
                    </a>
                    ";

            }else{
                if($edit==true && $delete==false)
                {
                    return "<a href='".route('merchant.toko.customer.add-customer',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                            <span class='fa fa-edit' style='color: white'></span>
                        </a>
                        ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                        <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                            <span class='fa fa-trash-alt' style='color: white'></span>
                        </a>
                        ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a href='".route('merchant.toko.customer.add-customer',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                            <span class='fa fa-edit' style='color: white'></span>
                        </a>

                        <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                            <span class='fa fa-trash-alt' style='color: white'></span>
                        </a>
                        ";
                }
            }
        }
    }

    public static function getFilterMap()
    {
        $field = [
            'sc_customer_level_id' => 'l.id',

        ];
        return $field;
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return '<div class="col-lg-auto col-md-12 mb-3">
                        <a href="'.route('merchant.toko.customer.add-customer').'" class="btn btn-success btn-sm py-2 px-4 btn-rounded"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return '<div class="col-lg-auto col-md-12 mb-3">
                        <a href="'.route('merchant.toko.customer.add-customer').'" class="btn btn-success btn-sm py-2 px-4 btn-rounded"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }

        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return '<div class="col-lg-auto col-md-12 mb-3">
                        <a href="'.route('merchant.toko.customer.add-customer').'" class="btn btn-success btn-sm py-2 px-4 btn-rounded"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
                }
            }else{
                return "";
            }
        }

    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantBranch;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class BranchEntity extends MerchantBranch
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('master_data',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return MerchantBranch::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_branchs.id)'),
                'md_merchant_branchs.id',
                'b.name as nama_cabang',
                'u.fullname as nama_owner',
                'c.name as kategori_usaha',
                'md_merchant_branchs.email_branch as email_cabang',
                'b.branch_code as kode_cabang'
            ])
                ->join('md_merchants as m','m.id','md_merchant_branchs.head_merchant_id')
                ->join('md_merchants as b','b.id','md_merchant_branchs.branch_merchant_id')
                ->join('md_users as u','u.id','b.md_user_id')
                ->leftjoin('md_business_categories as c','c.id','b.md_business_category_id')
                ->orderBy('md_merchant_branchs.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->whereRaw("lower(b.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
            ;
        }

        $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id()))->where('md_merchant_branchs.is_active',1);

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],
            $isEdit,$isDelete,$isHead)
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode_cabang',
            'nama_cabang',
            'nama_owner',
            'kategori_usaha',
            'email_cabang',
        ];
    }



    public static function getButton($list){

        return "<a  onclick='loadModal(this)' target='".route('merchant.toko.branch.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a  onclick='loadModal(this)' target='".route('merchant.toko.branch.add',['id'=>$list->id,'page'=>'updatePwd'])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Update Password'>
                                                <span class='fa fa-key'></span>
                                            </a>
                                            <a onclick='disconnect($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.branch.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }
        else
            if(merchant_detail()->is_branch==0)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.branch.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            }else{
                if(self::getAccess()==true)
                {
                    if($permission==true){
                        return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.branch.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                    }
                }else{
                    return "";
                }
            }
    }
}

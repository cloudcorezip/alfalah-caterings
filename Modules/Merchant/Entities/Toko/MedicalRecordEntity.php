<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\ClinicMedicalRecord;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MedicalRecordEntity extends ClinicMedicalRecord
{
    public static function getDataForDataTable()
    {

        try {
            return ClinicMedicalRecord::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY clinic_medical_records.id)'),
                'clinic_medical_records.id',
                'clinic_medical_records.status',
                'cr.reservation_number as nomor_reservasi',
                'cr.reservation_date as tanggal',
                'sc.code as nomor_member',
                'sc.name as nama_lengkap',
                'sp.name as nama_layanan',
                'u.fullname as dokter'
            ])
                ->join('clinic_reservations as cr', 'cr.id', 'clinic_medical_records.clinic_reservation_id')
                ->join('sc_customers as sc', 'sc.id', 'cr.sc_customer_id')
                ->join('sc_products as sp', 'sp.id', 'cr.sc_product_id')
                ->join('md_merchant_staff as mms', 'mms.id', 'cr.md_merchant_staff_id')
                ->join('md_users as u', 'u.id', 'mms.md_user_id')
                ->where('cr.is_deleted', 0)
                ->where('clinic_medical_records.md_merchant_id', merchant_id())
                ->orderBy('cr.reservation_date','desc')
                ;

        }catch (\Exception $e)
        {
            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(cr.reservation_number) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc.name) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(sc.code) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(u.fullname) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1' && $searchFilter[$key] != '-3') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        if(array_key_exists('start_date', $searchFilter) && array_key_exists('end_date', $searchFilter)){
            if(!is_null($searchFilter['start_date']) && !is_null($searchFilter['end_date'])){
                $startDate = $searchFilter['start_date'];
                $endDate = $searchFilter['end_date'];
                $data->whereRaw("
                    cr.reservation_date::date between '$startDate' and '$endDate'
                ");
            }
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi', 'status'], $isEdit,$isDelete,$isHead)
            ->editColumn('tanggal', function($list){
                return Carbon::parse($list->tanggal)->isoFormat('dddd, D MMMM Y, HH:mm');
            })
            ->editColumn('status', function($list){
                if($list->status == 0){
                    return '<span class="px-3 py-2" style="background:#FFF5EC;color:#FF8100;border-radius:10px;">
                                Belum Tindakan
                            </span>';
                } else {
                    return '<span class="px-3 py-2" style="background:#EAFFF1;color:#62AC29;border-radius:10px;">
                                Sudah Tindakan
                            </span>';
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'nomor_reservasi',
            'nomor_member',
            'nama_lengkap',
            'tanggal',
            'dokter',
            'status',
            'opsi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'reservation_date' => 'reservation_date',
            'status' => 'clinic_medical_records.status'

        ];
        return $field;
    }

    public static function getButton($list,$edit,$delete,$isHead)
    {
        if($list->status == 0){
            return "<a href='".route('merchant.toko.medical-record.add', ['id'=>$list->id, 'page'=>'diagnosis'])."' class='btn btn-success' title='Ambil Tindakan' style='min-width:107px;'>
                    <small class='d-block mx-auto'>Ambil Tindakan</small>
                </a>
                ";
        } else {
            return "<a href='".route('merchant.toko.medical-record.detail', ['id'=>$list->id, 'page'=>'diagnosis'])."' class='btn btn-outline-success' title='Ambil Tindakan' style='min-width:107px;'>
                    <small class='d-block mx-auto'>Lihat Detail</small>
                </a>
                ";
        }
    }

    public static function add($permission)
    {
        if($permission==true){
            return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" href="'.route('merchant.toko.medical-record.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
        }
    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\PurchaseOrder;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ApEntity extends PurchaseOrder
{
    public static function getDataForDataTable()
    {

        try {
            return PurchaseOrder::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_purchase_orders.id)'),
                'sc_purchase_orders.id',
                DB::raw('coalesce(sc_purchase_orders.second_code,sc_purchase_orders.code) as kode_transaksi'),
                'sc_purchase_orders.code',
                'sc_purchase_orders.second_code',
                DB::raw('sc_purchase_orders.total as total_tagihan'),
                's.name as nama_supplier',
                DB::raw("
                      coalesce(
                        (
                          select
                            sum(amp.paid_nominal)
                          from
                            acc_merchant_ap amp
                          where
                            amp.apable_id = sc_purchase_orders.id
                            and sc_purchase_orders.is_debet = 1
                            and amp.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                        ),
                        0
                      ) as terbayarkan

                "),
                DB::raw("
                coalesce(
                  (
                    select
                      sum(amp.residual_amount)
                    from
                      acc_merchant_ap amp
                    where
                      amp.apable_id = sc_purchase_orders.id
                      and sc_purchase_orders.is_debet = 1
                      and amp.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                  ),
                  0
                ) as sisa_hutang

          "),
                DB::raw("
                      coalesce(
                        (
                          select
                            amp.due_date
                          from
                            acc_merchant_ap amp
                          where
                            amp.apable_id = sc_purchase_orders.id
                            and sc_purchase_orders.is_debet = 1
                            and amp.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                        )
                      ) as jatuh_tempo

                "),
                'sc_purchase_orders.created_at as waktu_transaksi',
                'sc_purchase_orders.timezone',
                'u.fullname as nama_pencatat',
                'sc_purchase_orders.is_editable'

            ])
                ->leftJoin('md_users as u','u.id','sc_purchase_orders.created_by')
                ->leftJoin('sc_suppliers as s','s.id','sc_purchase_orders.sc_supplier_id')
                ->where('sc_purchase_orders.is_deleted',0)
                ->where('sc_purchase_orders.is_debet',1)
                ;
        } catch (\Exception $e) {

            return [];

        }

    }

    public static function dataTable($request)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;


        $getBranch=MerchantUtil::getBranch(merchant_id(),0);

        $data->whereIn('sc_purchase_orders.md_merchant_id',$getBranch);

        $data->whereRaw("
        sc_purchase_orders.created_at::date between '$startDate' and '$endDate'
        ");
        return(DataTable::getInstance())->dataTable(self::class,$data,$request,['opsi'],
            ['sc_purchase_orders.code'])
            ->editColumn('total_tagihan', function($list){
              return rupiah($list->total_tagihan);
          })
          ->editColumn('sisa_hutang', function($list){
            return rupiah($list->sisa_hutang);
        })
        ->editColumn('terbayarkan', function($list){
          return rupiah($list->terbayarkan);
        })
        ->editColumn('waktu_transaksi',function ($list){
            return Carbon::parse($list->waktu_transaksi)->isoFormat('dddd, D MMMM Y, HH:mm')." ".getTimeZoneName($list->timezone);
        })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode_transaksi',
            'nama_supplier',
            'total_tagihan',
            'jatuh_tempo',
            'terbayarkan',
            'sisa_hutang',
            'nama_pencatat',
            'waktu_transaksi'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'sc_supplier_id'=>'sc_purchase_orders.sc_supplier_id',
            'md_merchant_id'=>'sc_purchase_orders.md_merchant_id',

        ];
        return $field;
    }

    public static function getButton($list){
        if($list->is_editable==1)
        {
            return "
                                            <a href='".route('merchant.toko.purchase-order.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                          ";
        }else{
            return "";
        }
    }

}

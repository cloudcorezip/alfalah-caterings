<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\SaleOrder;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PurchaseOrderOrderEntity extends PurchaseOrder
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('purchase',merchant_id());
    }
    public static function getDataForDataTable()
    {

        try {
            return PurchaseOrder::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_purchase_orders.id)'),
                'sc_purchase_orders.id',
                DB::raw('coalesce(sc_purchase_orders.second_code,sc_purchase_orders.code) as kode_pemesanan'),
                DB::raw('rupiah(sc_purchase_orders.total_order) as total_tagihan'),
                DB::raw('rupiah(sc_purchase_orders.tax_order) as pajak'),
                DB::raw('rupiah(sc_purchase_orders.promo_order) as diskon'),
                's.name as nama_supplier',
                'sc_purchase_orders.time_to_order as waktu_transaksi',
                'sc_purchase_orders.timezone',
                'sc_purchase_orders.is_editable',
                'sc_purchase_orders.is_step_close as status',
                'sc_purchase_orders.sc_supplier_id',
                'm.name as outlet',
                'm.name as cabang',
                'sc_purchase_orders.code',
                'sc_purchase_orders.second_code',

            ])
                ->leftJoin('md_users as u','u.id','sc_purchase_orders.created_by')
                ->leftJoin('sc_suppliers as s','s.id','sc_purchase_orders.sc_supplier_id')
                ->join('md_merchants as m','m.id','sc_purchase_orders.md_merchant_id')
                ->where('sc_purchase_orders.step_type',2)
                ->orderBy('sc_purchase_orders.id','desc')
                ;
        } catch (\Exception $e) {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_purchase_orders.code) like '%".strtolower(str_replace('PO-','P-',str_replace("'","''",$search)))."%' ")
                    ->orWhereRaw("lower(sc_purchase_orders.second_code) like '%".strtolower(str_replace("'","''",$search))."%'")
                    ->orWhereRaw("lower(s.name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        if(empty($searchFilter)){
            $data->where('sc_purchase_orders.is_deleted', 0);
        }

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

        $getBranch=MerchantUtil::getBranch(merchant_id(),0);


        $data->whereIn('sc_purchase_orders.md_merchant_id',$getBranch);

        $data->whereRaw("
        sc_purchase_orders.created_at::date between '$startDate' and '$endDate'
        ");
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','kredit','status'],$isEdit,$isDelete,$isHead)
            ->editColumn('kode_pembelian',function ($list){
                return str_replace('P','PO',$list->kode_pembelian);
            })
            ->editColumn('waktu_transaksi',function ($list){
                return Carbon::parse($list->waktu_transaksi)->isoFormat('dddd, D MMMM Y, HH:mm')." ".getTimeZoneName($list->timezone);
            })
            ->editColumn('status',function ($list){
                return ($list->status==0)?'<span style="color:green">Open</span>':'<span style="color:red">Close</span>';
            })
            ->make(true);
    }

    public static function getFilterMap()
    {
        $field = [
            'is_step_close'=>'sc_purchase_orders.is_step_close',
            'is_deleted' => 'sc_purchase_orders.is_deleted',
            'sc_supplier_id' => 'sc_purchase_orders.sc_supplier_id',
            'md_merchant_id'=>'sc_purchase_orders.md_merchant_id',

        ];
        return $field;
    }

    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode_pemesanan',
            'nama_supplier',
            'cabang',
            'pajak',
            'diskon',
            'total_tagihan',
            'status',
            'waktu_transaksi'
        ];
    }

    public static function getButton($list){
        if($list->is_editable==1)
        {
            if(count(MerchantUtil::getBranch(merchant_id()))>1){
                return "


                                            <a href='".route('merchant.toko.purchase-order.detail-order',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                             <button type='button' class='btn btn-xs btn-success-xs text-white btn-rounded' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                    <i class='fa fa-download'></i>
                                                </button>
                                                <div class='dropdown-menu'>
                                                    <a class='dropdown-item' href='".route('merchant.toko.purchase-order.export-data-faktur-pdf',['id'=>$list->id])."'>PDF</a>
                                                    <a class='dropdown-item' onclick='exportDataSingle($list->id)'>Excel</a>
                                                </div>
                                          ";
            }else if(merchant_detail()->is_branch==0)
            {
                return "


                                            <a href='".route('merchant.toko.purchase-order.detail-order',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                             <button type='button' class='btn btn-xs btn-success-xs text-white btn-rounded' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                    <i class='fa fa-download'></i>
                                                </button>
                                                <div class='dropdown-menu'>
                                                    <a class='dropdown-item' href='".route('merchant.toko.purchase-order.export-data-faktur-pdf',['id'=>$list->id])."'>PDF</a>
                                                    <a class='dropdown-item' onclick='exportDataSingle($list->id)'>Excel</a>
                                                </div>
                                          ";
            }else{
                if(self::getAccess()==true)
                {
                    return "


                                            <a href='".route('merchant.toko.purchase-order.detail-order',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                             <button type='button' class='btn btn-xs btn-success-xs text-white btn-rounded' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                    <i class='fa fa-download'></i>
                                                </button>
                                                <div class='dropdown-menu'>
                                                    <a class='dropdown-item' href='".route('merchant.toko.purchase-order.export-data-faktur-pdf',['id'=>$list->id])."'>PDF</a>
                                                    <a class='dropdown-item' onclick='exportDataSingle($list->id)'>Excel</a>
                                                </div>
                                          ";
                }
            }


        }else{
            return "";
        }
    }
}

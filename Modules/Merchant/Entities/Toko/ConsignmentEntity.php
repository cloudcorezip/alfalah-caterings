<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\ConsignmentGood;
use App\Models\SennaToko\StockOpname;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class ConsignmentEntity extends ConsignmentGood
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('consignment',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return ConsignmentGood::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_consignment_goods.id)'),
                'sc_consignment_goods.id',
                DB::raw('coalesce(sc_consignment_goods.second_code,sc_consignment_goods.code) as kode'),
                'sc_consignment_goods.note as deskripsi',
                'sc_consignment_goods.time_received as waktu_penerimaan',
                'u.fullname as nama_pencatat',
                'm.name as outlet',
                'm.name as cabang',
                'w.name as gudang',
                's.name as nama_supplier',
                'sc_consignment_goods.timezone as zona_waktu',
                DB::raw('rupiah(sc_consignment_goods.total) as total'),
                'm.id as merchant_id'

            ])->join('md_merchants as m','m.id','sc_consignment_goods.md_merchant_id')
                ->join('md_merchant_inv_warehouses as w','w.id','sc_consignment_goods.inv_warehouse_id')
                ->join('md_users as u','u.id','sc_consignment_goods.created_by')
                ->join('sc_suppliers as s','s.id','sc_consignment_goods.sc_supplier_id');

        } catch (\Exception $e) {
            return [];

        }

    }

    public static function dataTable($request)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("sc_consignment_goods.time_received::date between '$startDate' and '$endDate'
        ");

        $getBranch=MerchantUtil::getBranch(merchant_id());

        $data->whereIn('m.id',$getBranch)->where('sc_consignment_goods.is_deleted',0);


        return(DataTable::getInstance())->dataTable(self::class,$data,$request,['opsi'],
            ['u.fullname'])
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1)
        {
            return [
                'row_number',
                'opsi',
                'kode',
                'cabang',
                'gudang',
                'total',
                'nama_pencatat',
                'waktu_penerimaan',
                'zona_waktu'

            ];
        }else{
            return [
                'row_number',
                'opsi',
                'kode',
                'gudang',
                'cabang',
                'total',
                'nama_pencatat',
                'waktu_penerimaan',
                'zona_waktu'
            ];
        }
    }

    public static function getFilterMap()
    {
        $field = [
            'md_merchant_id'=>'m.id',
            'inv_warehouse_id'=>'w.id'
        ];
        return $field;
    }

    public static function getButton($list){

        if(merchant_id()==$list->merchant_id){
            return "
                                            <a href='".route('merchant.toko.consignment.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>
                                             <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
        }else{
            if(self::getAccess()==true)
            {
                return "
                                            <a href='".route('merchant.toko.consignment.detail',['id'=>$list->id])."'   class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                               <i class='fa fa-eye'></i>
                                            </a>

                                             <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                return "";
            }
        }




    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){
            if($permission==true){
                return '  <a href="'.route('merchant.toko.consignment.add').'"
                           class="btn btn-success text-white py-2 px-4 btn-rounded">
                            <i class="fa fa-plus mr-2"></i>
                            <span>Tambah</span>
                        </a>';
            }
        }else if(merchant_detail()->is_branch==0){
            if($permission==true){
                return '  <a href="'.route('merchant.toko.consignment.add').'"
                           class="btn btn-success text-white py-2 px-4 btn-rounded">
                            <i class="fa fa-plus mr-2"></i>
                            <span>Tambah</span>
                        </a>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return '  <a href="'.route('merchant.toko.consignment.add').'"
                           class="btn btn-success text-white py-2 px-4 btn-rounded">
                            <i class="fa fa-plus mr-2"></i>
                            <span>Tambah</span>
                        </a>';
                }
            }else{
                return "";
            }
        }

    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\ProductCategory;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class CategoryEntity extends ProductCategory
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('master_data',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return ProductCategory::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_product_categories.id)'),
                'sc_product_categories.id',
                'sc_product_categories.name as nama_kategori',
                'sc_product_categories.description as deskripsi',
                'sc_product_categories.name',
                'sc_product_categories.description',
                'sc_product_categories.md_user_id',
                'sc_product_categories.is_service',
                'm.name as dibuat_oleh_outlet',
                'm.name as dibuat_oleh_cabang',
                'm.id as merchant_id'

            ])
                ->join('md_users as u','u.id','sc_product_categories.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->orderBy('sc_product_categories.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }


    private  static  function getBranch()
    {
        return MerchantUtil::getBranch(merchant_id(),1);
    }


    public static function listOption()
    {
        $getBranch=self::getBranch();

        return self::getDataForDataTable()->whereIn('m.id',$getBranch)->where('sc_product_categories.is_deleted',0)
            ->get();
    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(sc_product_categories.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(sc_product_categories.description) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }
        $data->whereIn('m.id',self::getBranch())->where('sc_product_categories.is_deleted',0);
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'], $isEdit,$isDelete,$isHead)
            ->make(true);
    }


    public static function dataTableColumns()
    {
        if(count(self::getBranch())>1)
        {
            return [
                'row_number',
                'nama_kategori',
                'deskripsi',
                'dibuat_oleh_cabang',
                'opsi'
            ];
        }else{
            return [
                'row_number',
                'nama_kategori',
                'deskripsi',
                'opsi'
            ];
        }

    }

    public static function getFilterMap()
    {
        $field = [
            'is_service' => 'is_service',

        ];
        return $field;
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id())
        {

            if($isHead==true)
            {
                return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.product-category.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.product-category.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.product-category.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }

    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.product-category.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }
        else if(merchant_detail()->is_branch==0){
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.product-category.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.product-category.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            }else{
                return "";
            }
        }

    }

}

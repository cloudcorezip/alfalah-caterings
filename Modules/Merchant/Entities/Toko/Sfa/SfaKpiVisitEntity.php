<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko\Sfa;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\Sfa\SfaVisit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SfaKpiVisitEntity extends SfaVisit
{
    public static function getDataForDataTable()
    {

        try {
            return SfaVisit::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sfa_kpi_visits.id)'),
                'sfa_kpi_visits.id',
                'sfa_kpi_visits.name as nama_target_kunjungan',
                'sfa_kpi_visits.visit_count as target_kunjungan',
                'sfa_kpi_visits.min_sale as target_penjualan',
                'sfa_kpi_visits.min_new_customer as target_pelanggan_baru',
                'sfa_kpi_visits.start_date as periode_target',
                'sfa_kpi_visits.start_date',
                'sfa_kpi_visits.end_date',
                'u.fullname as nama_sales'
            ])
                ->join('md_users as u', 'u.id', 'sfa_kpi_visits.staff_user_id')
                ->orderBy('sfa_kpi_visits.id', 'desc');

        } catch (\Exception $e) {

            return [];

        }

    }

    public static function dataTable($request, $isEdit = true, $isDelete = true, $isHead = true)
    {
        $data = self::getDataForDataTable();
        if ($request->input('search')['value']) {
            $search = $request->input('search')['value'];
            $data->where(function ($query) use ($search) {
                $query->whereRaw("lower(u.fullname) like '%" . strtolower(str_replace("'", "''", $search)) . "%' ");
            });
        }
        $data->where('sfa_kpi_visits.md_merchant_id', merchant_id())->where('sfa_kpi_visits.is_deleted', 0);

        return (DataTable::getInstance())->dataTableForMerchantV2(self::class, $data, ['opsi'], $isEdit, $isDelete, $isHead)
            ->editColumn('periode_target',function ($list){
                return Carbon::parse($list->start_date)->isoFormat('D MMMM Y').' s/d '.Carbon::parse($list->end_date)->isoFormat('D MMMM Y');
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'nama_target_kunjungan',
            'nama_sales',
            'target_kunjungan',
            'target_penjualan',
            'target_pelanggan_baru',
            'periode_target'
        ];
    }


    public static function getButton($list, $edit, $delete, $isHead)
    {

        if ($isHead == true) {
            return "<a  onclick='loadModal(this)' target='" . route('merchant.toko.sfa.kpi.add', ['id' => $list->id]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
        } else {
            if ($edit == true && $delete == false) {
                return "<a  onclick='loadModal(this)' target='" . route('merchant.toko.sfa.kpi.add', ['id' => $list->id]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
            } elseif ($edit == false && $delete == true) {
                return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            } elseif ($edit == false && $delete == false) {
                return "";
            } else {
                return "<a  onclick='loadModal(this)' target='" . route('merchant.toko.sfa.kpi.add', ['id' => $list->id]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }
        }
    }

    public static function add($permission)
    {
        if ($permission == true) {
            return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="' . route('merchant.toko.sfa.kpi.add') . '"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
        }
    }
}

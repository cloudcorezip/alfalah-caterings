<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko\Sfa;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Sfa\SfaVisit;
use App\Models\SennaToko\Sfa\SfaVisitSchedule;
use App\Utils\SFA\ConfigUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SfaVisitScheduleEntity extends SfaVisitSchedule
{

    public static function getDataForDataTable()
    {

        try {
            return SfaVisitSchedule::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sfa_visit_schedules.visit_schedule)'),
                'sfa_visit_schedules.visit_schedule as jadwal_kunjungan',
                'u.fullname as nama_sales',
                'u.id as staff_user_id'
            ])
                ->join('md_users as u', 'u.id', 'sfa_visit_schedules.staff_user_id')
                ->groupBy(['sfa_visit_schedules.visit_schedule','u.fullname','u.id'])
                ->orderBy('sfa_visit_schedules.visit_schedule', 'desc');

        } catch (\Exception $e) {

            return [];

        }

    }

    public static function dataTable($request, $isEdit = true, $isDelete = true, $isHead = true)
    {
        $data = self::getDataForDataTable();
        if ($request->input('search')['value']) {
            $search = $request->input('search')['value'];
            $data->where(function ($query) use ($search) {
                $query->whereRaw("lower(u.fullname) like '%" . strtolower(str_replace("'", "''", $search)) . "%' ");
            });
        }
        $data->where('sfa_visit_schedules.md_merchant_id', merchant_id())->where('sfa_visit_schedules.is_deleted', 0);

        return (DataTable::getInstance())->dataTableForMerchantV2(self::class, $data, ['opsi'], $isEdit, $isDelete, $isHead)
            ->editColumn('jadwal_kunjungan',function ($list){
                return Carbon::parse($list->jadwal_kunjungan)->isoFormat('D MMMM Y');
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'jadwal_kunjungan',
            'nama_sales',
            'opsi',
        ];
    }


    public static function getButton($list, $edit, $delete, $isHead)
    {
        $date=Carbon::parse($list->jadwal_kunjungan)->format('Ymd');
        $deletedId=$list->staff_user_id.$date;
        if ($isHead == true) {
            return "<a  href='" . route('merchant.toko.sfa.schedule.add',['staff_user_id'=>$list->staff_user_id,'date'=>$list->jadwal_kunjungan]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($deletedId)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
        } else {
            if ($edit == true && $delete == false) {
                return "<a  href='" . route('merchant.toko.sfa.schedule.add',['staff_user_id'=>$list->staff_user_id,'date'=>$list->jadwal_kunjungan]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
            } elseif ($edit == false && $delete == true) {
                return "
                                            <a onclick='deleteData($deletedId)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            } elseif ($edit == false && $delete == false) {
                return "";
            } else {
                return "<a  href='" . route('merchant.toko.sfa.schedule.add',['staff_user_id'=>$list->staff_user_id,'date'=>$list->jadwal_kunjungan]) . "' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($deletedId)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }
        }
    }

    public static function add($permission)
    {
        if ($permission == true) {
            if(ConfigUtil::checkPermission(merchant_id(),'visit')==false){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="lockPermission()"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }else{
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <a class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" href="' . route('merchant.toko.sfa.schedule.add') . '"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }
    }

}

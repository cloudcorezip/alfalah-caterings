<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\Merk;
use App\Models\SennaToko\ProductSellingLevel;
use Illuminate\Support\Facades\DB;

class ProductSellingLevelEntity
{

    public static function getDataForPriceType($id)
    {
        try {
            return ProductSellingLevel::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY id)'),
                'id',
                'level_name as tipe_harga',
                'selling_price as harga_jual',
                'sc_product_id'
            ])
                ->where('sc_product_id',$id)
                ->where('is_deleted',0)
                ->orderBy('id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }
    }

    public static function dataTablePriceType($request,$id)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForPriceType($id),$request,['opsi'],
            ['level_name'])
            ->editColumn('harga_jual',function ($list){
                return rupiah($list->harga_jual);
            })
            ->make(true);
    }

    public static function dataTableColumns()
    {
        return [
            'row_number',
            'tipe_harga',
            'harga_jual',
            'opsi'
        ];
    }

    public static function add($productId)
    {
            return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.product.selling-price.add').'?sc_product_id='.$productId.'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
    }

    public static function getButton($list)
    {
        return "<a  onclick='loadModal(this)' target='".route('merchant.toko.product.selling-price.add',['id'=>$list->id,'sc_product_id'=>$list->sc_product_id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
    }



}

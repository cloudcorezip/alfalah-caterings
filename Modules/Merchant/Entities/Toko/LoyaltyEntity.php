<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\DiscountProduct;
use App\Models\SennaToko\Loyalty;
use App\Models\SennaToko\Product;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Models\MasterData\Merchant;

class LoyaltyEntity extends Loyalty
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('crm',merchant_id());
    }

    private static function getBranch()
    {
        return MerchantUtil::getBranch(merchant_id(),1);
    }

    public static function getDataForDataTable()
    {

        try {
            return Loyalty::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY crm_loyalty_points.id)'),
                'crm_loyalty_points.id',
                'crm_loyalty_points.md_merchant_id',
                'crm_loyalty_points.get_point_method as get_point_method',
                'crm_loyalty_points.sc_product_id',
                'crm_loyalty_points.min_transaction as transaksi',
                'crm_loyalty_points.point_earned as total_point',
                'crm_loyalty_points.is_active as status',
                'crm_loyalty_points.assign_to_branch as cabang_terkait',
                'm.name as dibuat_oleh_outlet',
                'sp.name as produk',
                'm.id as merchant_id'
            ])
            ->join('md_merchants as m', 'm.id', 'crm_loyalty_points.md_merchant_id')
            ->leftJoin('sc_products as sp', 'sp.id', 'crm_loyalty_points.sc_product_id')
            ->where('crm_loyalty_points.is_deleted',0);

        }catch (\Exception $e)
        {
            return [];
        }

    }

    public static function dataTable($request,$isEdit=false,$isDelete=true,$isHead=false)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        if(count(MerchantUtil::getBranch(merchant_id()))>1 || merchant_detail()->is_branch==0){
            $getBranch = MerchantUtil::getBranch(merchant_id(),1);

            $data->whereIn('crm_loyalty_points.md_merchant_id', $getBranch);
        }else{
            $data->where(function($q){
                $q->where('crm_loyalty_points.md_merchant_id', merchant_id())
                    ->orWhere("crm_loyalty_points.assign_to_branch", '@>', '['.merchant_id().']');
            });
        }


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','status'], $isEdit,$isDelete,$isHead)
            ->editColumn('cara_mendapatkan_poin', function($list){
                if($list->get_point_method == 1)
                {
                    return "Minimal total transaksi";
                } else {
                    return "Pembelian Produk";
                }
            })
            ->editColumn('produk', function($list){
                if($list->get_point_method == 1)
                {
                    return "-";
                } else {
                    return $list->produk;
                }
            })
            ->editColumn('transaksi', function($list){
                if($list->get_point_method == 1)
                {
                    return rupiah($list->transaksi);
                } else {
                    return "-";
                }
            })
            ->editColumn('status', function($list){
                if($list->status==1){
                    return '<h6><span class="badge badge-success">aktif</span></h6>';
                }else{
                    return '<h6><span class="badge badge-danger" >non-aktif</span></h6>';
                }
            })
            ->editColumn('cabang_terkait', function($list){
                $list_branch="";
                if(!is_null($list->cabang_terkait)){
                    $merchant=Merchant::whereIn('id',json_decode($list->cabang_terkait))->get();
                    foreach($merchant as $key => $a){
                        $list_branch.= ($key == 0) ? $a->name : "," . $a->name;
                    }
                }

                return $list_branch;
            })
            ->make(true);
    }

    public static function getFilterMap()
    {
        $field = [
            'status' => 'is_active'
        ];
        return $field;
    }
    public static function dataTableColumns()
    {
        return [
            'row_number',
            'cara_mendapatkan_poin',
            'produk',
            'transaksi',
            'total_point',
            'cabang_terkait',
            'status',
            'opsi',

        ];
    }

    public static function getButton($list,$isEdit,$isDelete,$isHead){

        if($list->merchant_id == merchant_id())
        {
            if($isHead==true)
            {
                return "<a onclick='loadModalFullScreen(this)' target='".route('merchant.toko.loyalty.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                            <span class='fa fa-edit' style='color: white'></span>
                        </a>

                        <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                            <span class='fa fa-trash-alt' style='color: white'></span>
                        </a>
                        ";
            }else{
                if($isEdit==true && $isDelete==false)
                {
                    return "<a onclick='loadModalFullScreen(this)' target='".route('merchant.toko.loyalty.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                <span class='fa fa-edit' style='color: white'></span>
                            </a>";
                }elseif ($isEdit==false && $isDelete==true)
                {
                    return "
                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                <span class='fa fa-trash-alt' style='color: white'></span>
                            </a>
                        ";
                }elseif($isEdit==false && $isDelete==false){
                    return "";
                }else{
                    return "<a onclick='loadModalFullScreen(this)' target='".route('merchant.toko.loyalty.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                <span class='fa fa-edit' style='color: white'></span>
                            </a>

                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                <span class='fa fa-trash-alt' style='color: white'></span>
                            </a>
                        ";
                }
            }
        }

    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-md-12 mb-3 d-flex justify-content-between align-items-center">
                            <a onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.loyalty.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4">
                                <i class="fa fa-plus mr-2"></i> Tambah
                            </a>
                            <a onclick="loadModal(this)" target="'.route('merchant.toko.loyalty.add-point-rate').'" class="btn btn-outline-green btn-sm py-2 px-4">
                                <span class="iconify mr-2" data-icon="bx:money"></span> Pengaturan Kurs Point
                            </a>
                        </div>
                        ';
            }
        }else if(merchant_detail()->is_branch==0){
            if($permission==true){
                return ' <div class="col-md-12 mb-3 d-flex justify-content-between align-items-center">
                            <a onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.loyalty.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4">
                                <i class="fa fa-plus mr-2"></i> Tambah
                            </a>
                            <a onclick="loadModal(this)" target="'.route('merchant.toko.loyalty.add-point-rate').'" class="btn btn-outline-green btn-sm py-2 px-4">
                                <span class="iconify mr-2" data-icon="bx:money"></span> Pengaturan Kurs Point
                            </a>
                        </div>
                        ';
            }
        }else{
            if(self::getAccess()==true)
            {
                return ' <div class="col-md-12 mb-3 d-flex justify-content-between align-items-center">
                    <a onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.loyalty.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4">
                        <i class="fa fa-plus mr-2"></i> Tambah
                    </a>
                    <a onclick="loadModal(this)" target="'.route('merchant.toko.loyalty.add-point-rate').'" class="btn btn-outline-green btn-sm py-2 px-4">
                        <span class="iconify mr-2" data-icon="bx:money"></span> Pengaturan Kurs Point
                    </a>
                </div>
                ';
            }else{
                return "";
            }
        }
    }




}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\MerchantCommission;
use Illuminate\Support\Facades\DB;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;

class CommissionEntity extends MerchantCommission
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('commission',merchant_id());
    }

    private static function getBranch()
    {
        return MerchantUtil::getBranch(merchant_id(),1);
    }

    public static function getDataForDataTable()
    {

        try {
            return MerchantCommission::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY merchant_commissions.id)'),
                'merchant_commissions.id',
                'merchant_commissions.name as nama',
                'merchant_commissions.staff_list as diberikan_kepada',
                'merchant_commissions.product_list',
                'merchant_commissions.value_commissions as nilai_komisi',
                'merchant_commissions.type_of_value_commissions',
                'merchant_commissions.type as ketentuan_komisi',
                'merchant_commissions.value_type',
                'merchant_commissions.is_for_employee',
                'merchant_commissions.status',
                'merchant_commissions.min_transaction',
                'm.id as merchant_id',
                'merchant_commissions.assign_to_branch'
            ])
                ->join('md_merchants as m','m.id','merchant_commissions.md_merchant_id')
                ->orderBy('merchant_commissions.id','desc');

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(merchant_commissions.name) like '%".strtolower(str_replace("'","''",$search))."%' ");

            });
        }
        if(merchant_detail_multi_branch(merchant_id())->is_branch==0 || count(MerchantUtil::getBranch(merchant_id()))>0){
            $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('merchant_commissions.is_deleted',0);
        }else{
            $data->where(function($q){
                $q->where('m.id',merchant_id())
                    ->orWhere("merchant_commissions.assign_to_branch", '@>', '[' .merchant_id(). ']')->where('merchant_commissions.is_deleted',0);
                });
        }


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','ketentuan_komisi','diberikan_kepada','status'],$isEdit,$isDelete,$isHead)
            ->editColumn('ketentuan_komisi',function ($list){
                $html="";
                $html.='<div style="background-color:#FBFBFB;border:1px dashed #C4C4C4;border-radius:10px;" class="p-4 mb-4">';
                $html.='<span>Komisi Berdasarkan : <b>'.(($list->ketentuan_komisi=='transaction')?'Transaksi':'Produk').'</b></span><br>';
                $html.='<span>Komisi Berupa : <b>'.(($list->value_type=='nominal')?'Nominal':'Persentase').'</b></span><br>';
                $html.='<span>Komisi Diberikan Kepada : <b>'.(($list->is_for_employee==1)?'Karyawan':'Affiliator').'</b></span><br>';
                $html.='<span>Min Transaksi : <b>'.rupiah($list->min_transaction).'</b></span><br>';
                $html.='<span>Jenis Komisi : ';
                if($list->type_of_value_commissions=='flat'){
                    $html.='<b>Flat</b></span></br>';
                }elseif($list->type_of_value_commissions=='multiple'){
                    $html.='<b>Kelipatan</b></span></br>';
                }else{
                    $html.='<b>Berjenjang</b></span></br>';
                }
                if($list->ketentuan_komisi=='product'){
                    if(!is_null($list->product_list)){
                        $html.='<span>Cara mendapatkan komisi dengan menjual produk : </span><br>';
                        $productList=collect(json_decode($list->product_list))->groupBy('code');
                        $no=0;
                        foreach ($productList as $key => $item){
                            $html.=($no==0)?'<b>'.$item->first()->name.'</b>':',<b>'.$item->first()->name.'</b>';
                            $no++;
                        }
                        $html.="<br>";
                    }
                }
                if(!is_null($list->nilai_komisi)){
                    $jsonValue=json_decode($list->nilai_komisi);
                    if($list->type_of_value_commissions=='flat'){
                        foreach ($jsonValue as $v){
                            $html.='<span>Nilai Komisi : </span><b>'.(($list->value_type=='nominal')?rupiah($v->value):($v->value.'%')).'</b>';
                        }
                    }elseif ($list->type_of_value_commissions=='multiple'){
                        foreach ($jsonValue as $v){
                            if($list->ketentuan_komisi=='product'){
                                $html.='<span>Nilai Komisi : </span><b>'.(($list->value_type=='nominal')?rupiah($v->value):($v->value.'%')).'</b><br>';
                                $html.='<span>Kelipatan  : </span><b>'.$v->rules.'</b>';
                            }else{
                                $html.='<span>Nilai Komisi : </span><b>'.(($list->value_type=='nominal')?rupiah($v->value):($v->value.'%')).'</b><br>';
                                $html.='<span>Kelipatan  : </span><b>'.rupiah($v->rules).'</b>';

                            }
                           }
                    }else{
                        $html.="<span>Komisi Didapat : </span><br>";
                        if($list->ketentuan_komisi=='product'){
                            $html.="<table class='table table-bordered'><tr style='background-color: antiquewhite'>
                                  <th>Min Qty</th>
                                  <th>".(($list->value_type=='nominal')?'Komisi Didapat':'Komisi Didapat (%)')."</th>
                                </tr>";
                        }else{
                            $html.="<table class='table table-bordered'><tr style='background-color: antiquewhite'>
                                  <th>Min Transaksi</th>
                                  <th>".(($list->value_type=='nominal')?'Komisi Didapat':'Komisi Didapat (%)')."</th>
                                </tr>";
                        }

                        foreach ($jsonValue as $vv){
                            $rValue=($list->value_type=='nominal')?rupiah($vv->value):($vv->value.'%').'';
                            $rRules=($list->ketentuan_komisi=='product')?$vv->rules:rupiah($vv->rules);
                            $html.="<tr>
<th style='font-weight: normal'>".$rRules."</th>
<th style='font-weight: normal'>".$rValue."</th></tr>";
                        }
                        $html.="</table>";
                    }
                }


                $html.="</div>";
                return $html;

            })->editColumn('diberikan_kepada',function ($list){
                if(!is_null($list->diberikan_kepada)){
                    $html="";
                    $html.='<ul class="list-group">';
                    $staff=json_decode($list->diberikan_kepada);
                    foreach ($staff as $item){
                        $html.='<li class="list-group-item">'.$item->name.'</li>';
                    }
                    $html.='</ul>';
                    return $html;
                }else{
                    return "";
                }

            })
            ->editColumn('status',function ($list){
                return ($list->status==1)?'<label class="badge badge-success">Aktif</label>':'<label class="badge badge-danger">Tidak Aktif</label>';
            })
            ->make(true);
    }

    public static function getFilterMap()
    {
        $field = [
            'status' => 'status'
        ];
        return $field;
    }


    public static function dataTableColumns()
    {
        return [
            'nama',
            'ketentuan_komisi',
            'diberikan_kepada',
            'status',
            'opsi',
        ];
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id== merchant_id())
        {
            if($isHead==true)
            {
                return "<a href='".route('merchant.toko.commission.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                    <span class='fa fa-edit' style='color: white'></span>
                                                </a>

                                                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                    <span class='fa fa-trash-alt' style='color: white'></span>
                                                </a>
                                            ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModal(this)' target='".route('merchant.toko.commission.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                    <span class='fa fa-edit' style='color: white'></span>
                                                </a>


                                            ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                    <span class='fa fa-trash-alt' style='color: white'></span>
                                                </a>
                                            ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  onclick='loadModal(this)' target='".route('merchant.commission.commission.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                    <span class='fa fa-edit' style='color: white'></span>
                                                </a>

                                                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                    <span class='fa fa-trash-alt' style='color: white'></span>
                                                </a>
                                            ";
                }
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-md-12 mb-3">

                            <a class="btn btn-success btn-rounded btn-sm py-2 px-4" href="'.route('merchant.toko.commission.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                        </div>';
            }
        }
        else if(merchant_detail()->is_branch == 0)
        {
            if($permission==true){
                return ' <div class="col-md-12 mb-3">

                            <a class="btn btn-success btn-rounded btn-sm py-2 px-4" href="'.route('merchant.toko.commission.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                        </div>';
            }
        } else {
            if(self::getAccess()==true)
            {
                return ' <div class="col-md-12 mb-3">

                            <a class="btn btn-success btn-rounded btn-sm py-2 px-4" href="'.route('merchant.toko.commission.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                        </div>';
            } else {
                return "";
            }
        }
    }


}

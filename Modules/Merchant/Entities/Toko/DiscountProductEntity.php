<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\DiscountProduct;
use App\Models\SennaToko\Product;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Models\MasterData\Merchant;

class DiscountProductEntity extends DiscountProduct
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('crm',merchant_id());
    }

    private static function getBranch()
    {
        return MerchantUtil::getBranch(merchant_id(),1);
    }

    public static function getDataForDataTable()
    {

        try {
            return DiscountProduct::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY crm_discount_by_products.id)'),
                'crm_discount_by_products.id',
                'crm_discount_by_products.name',
                'crm_discount_by_products.is_active as status',
                'crm_discount_by_products.activation_type as tipe_transaksi',
                'crm_discount_by_products.promo_type',
                'crm_discount_by_products.bonus_type',
                'crm_discount_by_products.bonus_value',
                'crm_discount_by_products.promo_value',
                'crm_discount_by_products.start_date',
                'crm_discount_by_products.end_date',
                'crm_discount_by_products.md_merchant_id',
                'crm_discount_by_products.assign_to_branch as cabang_terkait',
                'm.name as dibuat_outlet',
                'm.name as dibuat_cabang'

            ])->with('getDetail.getProduct')
                ->join('md_merchants as m', 'm.id', 'crm_discount_by_products.md_merchant_id')
                ->where('crm_discount_by_products.is_coupon',0)
                ->where('crm_discount_by_products.is_referral_affiliate',0)
                ->where('crm_discount_by_products.is_deleted',0)
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=false,$isDelete=true,$isHead=false)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("crm_discount_by_products.start_date::date between '$startDate' and '$endDate'
        ");

        if(merchant_detail_multi_branch(merchant_id())->is_branch==0 || count(MerchantUtil::getBranch(merchant_id()))>0){
            $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id()))->where('crm_discount_by_products.is_deleted',0);
        }else{
            $data->where(function($q){
                $q->where('m.id',merchant_id())
                    ->orWhere("crm_discount_by_products.assign_to_branch", '@>', '[' .merchant_id(). ']')->where('crm_discount_by_products.is_deleted',0);
            });
        }

        $data->orderBy('crm_discount_by_products.start_date','desc');

        return(DataTable::getInstance())->dataTableForMerchant(self::class,$data,$request,['opsi','tipe_transaksi','status', 'produk', 'potongan/bonus'],
        ['crm_discount_by_products.name'],$isEdit,$isDelete,$isHead)
            ->editColumn('tipe_transaksi',function ($list){
                if($list->tipe_transaksi==1){
                    return '<span>Otomatis</span>';
                }else{
                    return '<span>Manual</span>';
                }
            })
            ->editColumn('status',function ($list){
                if($list->status==1){
                    return '<h6><span class="badge badge-success">aktif</span></h6>';
                }else{
                    return '<h6><span class="badge badge-danger" >non-aktif</span></h6>';
                }
            })
            ->editColumn('produk',function ($list){
                $count = 0;
                $text = '<ul>';
                foreach($list->getDetail as $key => $item){
                    if(!is_null($item->md_merchant_id)){
                        if($item->md_merchant_id == merchant_id()){
                            if($count < 3){
                                $text.="<li>".$item->getProduct->name."</li>";
                                $count++;
                            }
                        }
                    } else {
                        if($count < 3){
                            $text.="<li>".$item->getProduct->name."</li>";
                            $count++;
                        }
                    }
                }

                if($count >= 3){
                    $n = $count - 3;
                    if($n > 0){
                        $text .= "<li> dan ".$n." produk lainnya</li>";
                    }
                }

                $text .="</ul>";
                return $text;
            })
            ->editColumn('kriteria',function ($list){
                if($list->promo_type == 1 && $list->bonus_type == 1){
                    return "Diskon harga (%), jika memenuhi total transaksi";
                } elseif($list->promo_type == 1 && $list->bonus_type == 2){
                    return "Diskon harga (Rp), jika memenuhi total transaksi";
                } elseif($list->promo_type == 1 && $list->bonus_type == 3){
                    return "Bonus Produk, jika memenuhi total transaksi";
                } elseif($list->promo_type == 2 && $list->bonus_type == 1){
                    return "Beli produk tertentu, dapat diskon harga (%)";
                } elseif($list->promo_type == 2 && $list->bonus_type == 2){
                    return "Beli produk tertentu, dapat diskon harga (Rp)";
                } else {
                    return "Beli produk tertentu, bonus produk tertentu";
                }
            })
            ->editColumn('potongan/bonus',function ($list){
                if($list->bonus_type==2){
                    foreach(json_decode($list->bonus_value) as $key => $b){
                        return rupiah($b->value);
                    }
                }else if($list->bonus_type==1){
                    foreach(json_decode($list->bonus_value) as $key => $b){
                        return $b->value."%";
                    }
                }else if($list->bonus_type==3){
                    $p=json_decode($list->bonus_value);
                    $text = "<ul>";
                    $count = 0;
                    foreach($p as $key =>$b){
                        $pn=Product::find($b->value);
                        if(isset($b->md_merchant_id)){
                            if(merchant_id() == $b->md_merchant_id){
                                if($count < 3){
                                    $text.="<li>".$pn->name."</li>";
                                    $count++;
                                }
                            }
                        } else {
                            if($count < 3){
                                $text .="<li>".$pn->name."</li>";
                                $count++;
                            }
                        }
                    }

                    if($count >= 3){
                        $n = $count - 3;
                        if($n > 0){
                            $text .= "<li> dan ".$n." produk lainnya</li>";
                        }
                    }

                    $text .= "</ul>";
                    return $text;
                }
            })
            ->editColumn('durasi',function ($list){
              return  Carbon::parse($list->start_date)->isoFormat('D MMMM Y')."-".Carbon::parse($list->end_date)->isoFormat('D MMMM Y');
            })
            ->editColumn('cabang_terkait', function($list){
                $list_branch="";
                if(!is_null($list->cabang_terkait)){
                    $merchant=Merchant::whereIn('id',json_decode($list->cabang_terkait))->get();

                    foreach($merchant as $key => $a){
                        $list_branch.= ($key == 0) ? $a->name : "," . $a->name;
                    }
                }

                return $list_branch;
            })
            ->make(true);
    }

    public static function getFilterMap()
    {
        $field = [
            'status' => 'is_active'
        ];
        return $field;
    }
    public static function dataTableColumns()
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){
            return [
                'row_number',
                'name',
                'status',
                'produk',
                'potongan/bonus',
                'kriteria',
                'durasi',
                'dibuat_cabang',
                'cabang_terkait',
                'opsi',

            ];
        } else {
            return [
                'row_number',
                'name',
                'status',
                'produk',
                'potongan/bonus',
                'kriteria',
                'durasi',
                'opsi',

            ];
        }

    }

    public static function getButton($list,$isEdit,$isDelete,$isHead){

        if($list->md_merchant_id == merchant_id())
        {
            if($isHead==true)
            {
                return "<a href='".route('merchant.toko.discount-product.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                    <span class='fa fa-edit' style='color: white'></span>
                                                </a>

                                                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                    <span class='fa fa-trash-alt' style='color: white'></span>
                                                </a>
                                            ";
            }else{
                if($isEdit==true && $isDelete==false)
                {
                    return "<a href='".route('merchant.toko.discount-product.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                    <span class='fa fa-edit' style='color: white'></span>
                                                </a>


                                            ";
                }elseif ($isEdit==false && $isDelete==true)
                {
                    return "
                                                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                    <span class='fa fa-trash-alt' style='color: white'></span>
                                                </a>
                                            ";
                }elseif($isEdit==false && $isDelete==false){
                    return "";
                }else{
                    return "<a href='".route('merchant.toko.discount-product.edit',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                    <span class='fa fa-edit' style='color: white'></span>
                                                </a>

                                                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                    <span class='fa fa-trash-alt' style='color: white'></span>
                                                </a>
                                            ";
                }
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-md-12 mb-3">
                        <a href="'.route('merchant.toko.discount-product.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else if(merchant_detail()->is_branch==0){
            if($permission==true){
                return ' <div class="col-md-12 mb-3">
                        <a href="'.route('merchant.toko.discount-product.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                return ' <div class="col-md-12 mb-3">
                        <a href="'.route('merchant.toko.discount-product.add').'" class="btn btn-success btn-rounded btn-sm py-2 px-4"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }else{
                return "";
            }
        }
    }




}

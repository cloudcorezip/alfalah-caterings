<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Toko;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantStaffCategory;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;

class CategorySpecialistEntity extends MerchantStaffCategory
{

    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('master_data',merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return MerchantStaffCategory::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_staff_categories.id)'),
                'md_merchant_staff_categories.id',
                'md_merchant_staff_categories.code as kode',
                'md_merchant_staff_categories.name as nama_kategori',
                'md_merchant_staff_categories.description as deskripsi',
                'm.id as merchant_id',
                'm.name as cabang'
            ])
                ->join('md_users as u','u.id','md_merchant_staff_categories.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->orderBy('md_merchant_staff_categories.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(md_merchant_staff_categories.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->orWhereRaw("lower(md_merchant_staff_categories.code) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }
        $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))->where('md_merchant_staff_categories.is_deleted',0);

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],
            $isEdit,$isDelete,$isHead)
            ->editColumn('kode',function($list){
                return !is_null($list->kode) ? $list->kode:'-';
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'kode',
            'nama_kategori',
            'deskripsi',
            'opsi'
        ];
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id())
        {
            if($isHead==true)
            {
                return "<a  onclick='loadModal(this)' target='".route('merchant.toko.category-specialist.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModal(this)' target='".route('merchant.toko.category-specialist.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  onclick='loadModal(this)' target='".route('merchant.toko.category-specialist.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }
    }


    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.category-specialist.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }
        else if(merchant_detail()->is_branch==0){
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.category-specialist.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.category-specialist.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            }else{
                return "";
            }
        }

    }





}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Report\SaleOrder;


use App\Classes\Singleton\DataTable;
use App\Models\SennaToko\SaleOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class VoidEntity
{
    public static function getDataForDataTable($merchant_id)
    {

        try {
            return SaleOrder::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_sale_orders.id)'),
                'sc_sale_orders.id',
                'sc_sale_orders.code as kode_transaksi',
                DB::raw('rupiah(sc_sale_orders.total) as void'),
                "sc_sale_orders.created_at as waktu_transaksi",
                "u.fullname as nama_pencatat",
                "mm.name as nama_cabang",
                DB::raw("coalesce(cust.name,'Pelanggan Umum') as nama_pelanggan"),
                "cust.code as kode_pelanggan",
                "sc_sale_orders.timezone",
                DB::raw("
                COALESCE(
                              (
                                select
                                  sum(ssod.quantity*ssod.value_conversation)
                                from
                                 sc_sale_order_details ssod
                                where
                                  ssod.sc_sale_order_id = sc_sale_orders.id
                                  and ssod.is_deleted=0
                                  and ssod.is_bonus=0
                              )
                          ,
                          0
                        ) as jumlah_produk
                "),

            ])
                ->leftJoin('md_users as u','u.id','sc_sale_orders.created_by')
                ->join('md_merchants as mm', 'mm.id', 'sc_sale_orders.md_merchant_id')
                ->leftJoin('sc_customers as cust','cust.id','sc_sale_orders.sc_customer_id')
                ->whereIn('sc_sale_orders.md_merchant_id',$merchant_id)
                ->where('sc_sale_orders.is_editable',1)
                ->where('sc_sale_orders.is_approved_shop',1)
                ->where('sc_sale_orders.is_deleted',1)
                ->where('sc_sale_orders.is_cancel_user',0)
                ->orderBy('sc_sale_orders.created_at','asc');

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true,$isDatatable=0)
    {
        $merchantId = [];
        $requestMerchantId = explode("_", $request->merchant_id);

        // check jika pilih semua
        if(in_array("-1", $requestMerchantId)){
            array_push($merchantId, merchant_id());
            foreach(get_cabang() as $key => $branch){
                array_push($merchantId, $branch->id);
            }
        } else {
            $merchantId = $requestMerchantId;
        }


        $data=self::getDataForDataTable($merchantId);
        if($isDatatable==0){
            if($request->input('search')['value']) {
                $search=$request->input('search')['value'];
                $data->where(function($query) use($search){
                    $query->whereRaw("lower(sc_sale_orders.code) like '%".strtolower($search)."%' ")
                        ->orWhereRaw("lower(u.fullname) like '%".strtolower($search)."%'");
                });
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("
        sc_sale_orders.created_at::date between '$startDate' and '$endDate'
        ");

        return($isDatatable==0)?(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['waktu_transaksi'],$isEdit,$isDelete,$isHead)
        ->editColumn('waktu_transaksi',function($list){
            return Carbon::parse($list->waktu_transaksi)->format('d M Y').' '.getTimeZoneName($list->timezone);
        })->editColumn('nama_pelanggan',function ($list){
            return is_null($list->kode_pelanggan)?$list->nama_pelanggan:$list->kode_pelanggan.' '.$list->nama_pelanggan;
            })
        ->make(true):
            $data->get();
    }

    public static function getButton($list,$edit,$delete,$isHead){

        return [];
    }


    public static function dataTableColumns()
    {
        return [
            'kode_transaksi',
            'nama_pelanggan',
            'void',
            'jumlah_produk',
            'waktu_transaksi',
            'nama_pencatat',
            'nama_cabang'
        ];
    }

    public static function sumAllVoid($request)
    {
        try {
            $merchantId = [];
            $requestMerchantId = explode("_", $request->merchant_id);

            // check jika pilih semua
            if(in_array("-1", $requestMerchantId)){
                array_push($merchantId, merchant_id());
                foreach(get_cabang() as $key => $branch){
                    array_push($merchantId, $branch->id);
                }
            } else {
                $merchantId = $requestMerchantId;
            }

            $data=SaleOrder::
            select([
                DB::raw("
                count(*) as total_void,
                  coalesce(
                    sum(total),
                    0
                  ) as amount_void,
                  coalesce(sum(COALESCE(
                    (
                      select
                        sum(ssod.quantity)
                      from
                        sc_sale_order_details ssod
                      where
                        ssod.sc_sale_order_id = sc_sale_orders.id
                        and ssod.is_deleted = 0
                        and ssod.is_bonus = 0
                    ),
                    0
                  )),0) as total_void_product

                ")
            ])
                ->whereIn('md_merchant_id',$merchantId)
                ->where('is_editable',1)
                ->where('is_approved_shop',1)
                ->where('is_deleted',1)
                ->where('is_cancel_user',0);
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

            return response()->json($data->whereRaw("created_at::date between '$startDate' and '$endDate'
        ")->get());


        }catch (\Exception $e)
        {
            return response()->json((object)[
                'total_void'=>0,
                'amount_void'=>0,
                'total_void_product'=>0
            ]);
        }

    }
}

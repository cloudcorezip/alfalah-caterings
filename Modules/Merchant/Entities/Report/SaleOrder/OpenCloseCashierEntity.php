<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Report\SaleOrder;

use App\Classes\Singleton\DataTable;
use App\Models\Accounting\MerchantShift;
use App\Models\SennaToko\SaleOrder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OpenCloseCashierEntity
{
    public static function getDataForDataTable($merchant_id)
    {

        try {
            return MerchantShift::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY acc_merchant_shifts.id)'),
                "acc_merchant_shifts.id",
                "u.fullname as nama_kasir",
                "r.name as jabatan",
                "mm.name as nama_cabang",
                "acc_merchant_shifts.created_at as waktu_buka_kasir",
                "acc_merchant_shifts.amount as modal_awal",
                "acc_merchant_shifts.end_date as waktu_tutup_kasir",
                "acc_merchant_shifts.actual_amount as total_tunai_aktual",
                "acc_merchant_shifts.system_amount as total_tunai_sistem",
                "acc_merchant_shifts.is_active",
                "acc_merchant_shifts.json_response"
            ])
                ->leftJoin('md_users as u','u.id','acc_merchant_shifts.staff_user_id')
                ->leftJoin('md_roles as r','r.id','u.md_role_id')
                ->join('md_merchants as mm', 'mm.id', 'acc_merchant_shifts.md_merchant_id')
                ->whereIn('acc_merchant_shifts.md_merchant_id',$merchant_id)
                ->where('acc_merchant_shifts.is_deleted',0)
                ->orderBy('acc_merchant_shifts.created_at','desc');

        }catch (\Exception $e)
        {
            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $merchantId = [];
        $requestMerchantId = explode("_", $request->merchant_id);

        // check jika pilih semua
        if(in_array("-1", $requestMerchantId)){
            array_push($merchantId, merchant_id());
            foreach(get_cabang() as $key => $branch){
                array_push($merchantId, $branch->id);
            }
        } else {
            $merchantId = $requestMerchantId;
        }

        $data=self::getDataForDataTable($merchantId);
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(u.fullname) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("
            acc_merchant_shifts.created_at::date between '$startDate' and '$endDate'
        ");
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','nama_kasir','waktu_buka_kasir', 'waktu_tutup_kasir', 'modal_awal','total_tunai_aktual', 'total_tunai_sistem'],$isEdit,$isDelete,$isHead)
            ->editColumn('nama_kasir', function($list){
                return ucwords($list->nama_kasir);
            })
            ->editColumn('waktu_buka_kasir', function($list){
                return Carbon::parse($list->waktu_buka_kasir)->format('d M Y , H:i');
            })
            ->editColumn('waktu_tutup_kasir', function($list){
                if(!is_null($list->waktu_tutup_kasir)){
                    return Carbon::parse($list->waktu_tutup_kasir)->format('d M Y , H:i');
                } else {
                    return "-";
                }
            })
            ->editColumn('modal_awal', function($list){
                return rupiah($list->modal_awal);
            })
            ->editColumn('total_tunai_aktual', function($list){
                return rupiah($list->total_tunai_aktual);
            })
            ->editColumn('total_tunai_sistem', function($list){
                return rupiah($list->total_tunai_sistem);
            })
            ->make(true);
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->is_active == 0){
            if(!is_null($list->json_response)){
                return "<a onclick='loadDetail($list)' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                        <i class='fa fa-edit' style='color: white'></i>
                    </a>
                ";
            } else {
                return "-";
            }
        }else {
            return "-";
        }
        
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'nama_cabang',
            'waktu_buka_kasir',
            'waktu_tutup_kasir',
            'nama_kasir',
            'modal_awal',
            'total_tunai_aktual',
            'total_tunai_sistem',
            'opsi'
        ];
    }
}

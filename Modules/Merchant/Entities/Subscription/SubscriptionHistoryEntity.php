<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Subscription;


use App\Classes\Singleton\DataTable;
use App\Models\SennaPayment\Transaction;
use Illuminate\Support\Facades\DB;

class SubscriptionHistoryEntity extends Transaction
{

    public static function getDataForDataTable()
    {

        try {
            return Transaction::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sp_transactions.id)'),
                'sp_transactions.id',
                'sp_transactions.code as kode_invoice',
                'sp_transactions.transactionable_type as type',
                'ts.name as status',
                'sp_transactions.amount as harga',
                'sps.id as sps_id',
                'spp.id as spp_id',
                DB::raw("
                case
                    when
                        sp_transactions.transactionable_type = 'App\Models\SennaPayment\PaymentSubscription'
                    then
                        sub.name
                    else
                        plug.name
                end as nama_layanan
                ")
            ])
                ->leftJoin('sp_payment_subscriptions as sps','sps.id','sp_transactions.transactionable_id')
                ->leftJoin('sp_payment_plugins as spp','spp.id','sp_transactions.transactionable_id')
                ->join('md_sp_transaction_status as ts','ts.id','sp_transactions.md_sp_transaction_status_id')
                ->join('md_sp_transaction_types as tt','tt.id','sp_transactions.md_sp_transaction_type_id')
                ->leftJoin('md_subscriptions as sub','sub.id','sps.md_subscription_id')
                ->leftJoin('md_plugins as plug','plug.id','spp.md_plugin_id')
                ->where(function($query){
                    $query->where('sp_transactions.transactionable_type','App\Models\SennaPayment\PaymentSubscription')
                        ->orWhere('sp_transactions.transactionable_type','App\Models\SennaPayment\PaymentPlugin');
                })
                ->where('sp_transactions.md_user_id',user_id())
                ->orderBy('sp_transactions.created_at','desc');

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data = self::getDataForDataTable();

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(sub.name) like '%".strtolower($search)."%' ")
                    ->orWhereRaw("lower(plug.name) like '%".strtolower($search)."%' ")
                    ->orWhereRaw("lower(sp_transactions.code) like '%".strtolower($search)."%' ")
                ;
            });
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi', 'harga', 'status', 'nama_layanan'],$isEdit,$isDelete,$isHead)
            ->editColumn('harga', function($list){
                return rupiah(ceil($list->harga));
            })
            ->editColumn('status', function($list){
                if(strtolower($list->status) == 'success'){
                    return '<i style="color:#7EB056;">'.$list->status.'</i>';
                } else {
                    return '<i style="color:#FF9E0D;">'.$list->status.'</i>';
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'kode_invoice',
            'nama_layanan',
            'harga',
            'status',
            'opsi',
        ];
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->type == 'App\Models\SennaPayment\PaymentSubscription'){
            return "<a href='".route('subscription.invoice', ['key' => encrypt($list->sps_id)])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-eye' style='color: white'></span>
                                            </a>
                                          ";
        } else {
            return "<a href='".route('plugin.invoice', ['key' => encrypt($list->spp_id),'status'=>(strtolower($list->status) == 'success')?1:0])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-eye' style='color: white'></span>
                                            </a>
                                          ";
        }

    }

    public static function add($permission)
    {
        if($permission==true){
            return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.merk.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
        }
    }




}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Asset;


use App\Classes\Singleton\DataTable;
use App\Models\Accounting\Asset\AssetCategory;
use App\Models\Accounting\Asset\AssetDepreciation;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AssetEntity extends AssetDepreciation
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('asset',merchant_id());
    }
    public static function getDataForDataTable()
    {

        try {
            return AssetDepreciation::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY acc_asset_depreciations.id)'),
                'acc_asset_depreciations.id',
                DB::raw('coalesce(acc_asset_depreciations.second_code,acc_asset_depreciations.code) as kode'),
                'acc_asset_depreciations.asset_name as nama_aset',
                'acc_asset_depreciations.asset_value as nilai_aset_original',
                DB::raw("rupiah(acc_asset_depreciations.asset_value) as nilai_aset"),
                'c.name as kategori_aset',
                DB::raw("(case when acc_asset_depreciations.is_paid=0 then 'Belum Lunas' else 'Lunas' end) as status_pembelian"),
                'acc_asset_depreciations.record_time as waktu_pembelian',
                'acc_asset_depreciations._timezone',
                'm.id as merchant_id',
                'm.name as outlet',
                'm.name as cabang',
                'acc_asset_depreciations.current_asset_value',
                'acc_asset_depreciations.is_depreciation'
            ])
                ->join('acc_asset_categories as c','c.id','acc_asset_depreciations.acc_asset_category_id')
                ->join('md_merchants as m','m.id','acc_asset_depreciations.md_merchant_id')
                ->orderBy('acc_asset_depreciations.id','desc')
                ->where('acc_asset_depreciations.is_deleted',0)
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
                $data->where(function($query) use($search){
                    $query->whereRaw("lower(acc_asset_depreciations.asset_name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(acc_asset_depreciations.code) like '%".strtolower(str_replace("'","''",$search))."%'")
                        ->orWhereRaw("lower(acc_asset_depreciations.second_code) like '%".strtolower(str_replace("'","''",$search))."%'");


                });
        }
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=$request->start_date;
        $endDate=$request->end_date;

        $encode=json_decode(base64_decode($request->encode));
        if(is_array($encode))
        {
            $filterMerchant=$encode;
        }else{
            $filterMerchant=json_decode($encode);
        }

        if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
            $getBranch=MerchantUtil::getBranch(merchant_id(),0);

            $data->whereIn('m.id',$getBranch);
        }else{
            $data->whereIn('m.id',$filterMerchant);
        }
        if($startDate!='-')
        {
            $data->whereRaw("
            acc_asset_depreciations.record_time::date between '$startDate' and '$endDate'
        ");
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],$isEdit,$isDelete,$isHead)
            ->editColumn('waktu_pembelian',function ($list){
                return Carbon::parse($list->waktu_pembelian)->isoFormat('dddd, D MMMM Y').' '.getTimeZoneName($list->_timezone);
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode',
            'nama_aset',
            'kategori_aset',
            'nilai_aset',
            'status_pembelian',
            'waktu_pembelian',
            'cabang',
        ];
    }

    public static function getFilterMap()
    {
        $field = [
        ];
        return $field;
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id())
        {
            if($isHead==true)
            {
                return "<a  href='".route('merchant.asset.asset.detail',['id'=>$list->id])."' class='btn btn-edit-xs btn-xs' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  href='".route('merchant.asset.asset.detail',['id'=>$list->id])."' class='btn btn-edit-xs btn-xs' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  href='".route('merchant.asset.asset.detail',['id'=>$list->id])."' class='btn btn-edit-xs btn-xs' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }
            }
        }

    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-lg-auto col-md-12">
                        <a class="btn btn-success btn-rounded btn-sm py-2 px-4 btn-block" href="'.route('merchant.asset.asset.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        } else if(merchant_detail()->is_branch==0)
        {
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12">
                        <a class="btn btn-success btn-rounded btn-sm py-2 px-4 btn-block" href="'.route('merchant.asset.asset.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
            }
        }else{
            if(self::getAccess()==true)
            {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12">
                        <a class="btn btn-success btn-rounded btn-sm py-2 px-4 btn-block" href="'.route('merchant.asset.asset.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
                }
            }else{
                return "";
            }
        }

    }

}

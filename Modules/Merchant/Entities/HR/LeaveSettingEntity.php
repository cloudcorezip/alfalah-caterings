<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\HR;


use App\Classes\Singleton\DataTable;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Models\Job;
use Modules\Merchant\Models\LeaveSetting;

class LeaveSettingEntity extends LeaveSetting
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('attendance_master', merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return LeaveSetting::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY hr_merchant_leave_settings.id)'),
                'hr_merchant_leave_settings.id',
                'hr_merchant_leave_settings.name as keterangan',
                'hr_merchant_leave_settings.max_day as maksimal_hari',
                'hr_merchant_leave_settings.is_with_paid as memperoleh_gaji',
                'hr_merchant_leave_settings.is_with_salary_cut',
                'hr_merchant_leave_settings.type_of_salary_cut',
                'hr_merchant_leave_settings.type_of_attendance',
                'hr_merchant_leave_settings.color',
                'hr_merchant_leave_settings.is_per_month',
                'hr_merchant_leave_settings.salary_cut_value',
                'm.id as merchant_id',
                'm.name as dibuat_oleh_cabang',
                'hr_merchant_leave_settings.type_of_attendance',
                'hr_merchant_leave_settings.type_of_attendance as keterangan_lainnya'
            ])
                ->join('md_merchants as m','m.id','hr_merchant_leave_settings.md_merchant_id')
                ->orderBy('hr_merchant_leave_settings.id','desc')
                ->where('hr_merchant_leave_settings.is_deleted',0)
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function listOption()
    {
        return self::getDataForDataTable()->whereIn('hr_merchant_leave_settings.md_merchant_id',MerchantUtil::getBranch(merchant_id(),1))->get();
    }


    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();

        $getBranch=MerchantUtil::getBranch(merchant_id(),1);
        $data->whereIn('hr_merchant_leave_settings.md_merchant_id',$getBranch);

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(hr_merchant_leave_settings.name) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','memperoleh_gaji','keterangan_lainnya'],$isEdit,$isDelete,$isHead)
            ->editColumn('memperoleh_gaji',function ($list){
                return ($list->memperoleh_gaji==1)?'<span class="badge badge-success">Ya</span>':'<span class="badge badge-danger">Tidak</span>';
            })
            ->editColumn('maksimal_hari',function ($list){
                return $list->maksimal_hari.' '. (($list->is_per_month==1)?'Per Bulan' :'Per Tahun');
            })
            ->editColumn('keterangan_lainnya',function ($list){
                return 'Potong Gaji : '.(($list->is_with_salary_cut==1)?'Ya':'Tidak').'<br>'.(($list->is_with_salary_cut==0)?'':'Nilai Potongan Gaji : '.(($list->type_of_salary_cut==1)?'Flat':$list->salary_cut_value.' %'));
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'keterangan',
            'maksimal_hari',
            'memperoleh_gaji',
            'keterangan_lainnya',
//            'dibuat_oleh_cabang',
            'opsi'
        ];
    }


    public static function getButton($list,$edit,$delete,$isHead){

        if($list->merchant_id==merchant_id()){
            if($isHead==true)
            {
                return "<a  onclick='loadModal(this)' target='".route('merchant.hr.leave-setting.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModal(this)' target='".route('merchant.hr.leave-setting.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  onclick='loadModal(this)' target='".route('merchant.hr.leave-setting.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }

    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.hr.leave-setting.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }else if (merchant_detail()->is_branch == 0) {
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.hr.leave-setting.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        } else {
            if (self::getAccess() == true) {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.hr.leave-setting.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            } else {
                return "";
            }
        }

    }
}

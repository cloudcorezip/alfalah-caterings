<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\HR;


use App\Classes\Singleton\DataTable;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Models\ShiftRoasterChange;

class ShiftRoasterChangeEntity extends ShiftRoasterChange
{
    public static function getDataForDataTable()
    {

        try {
            return ShiftRoasterChange::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY hr_merchant_shift_roaster_changes.id)'),
                'hr_merchant_shift_roaster_changes.id',
                'hr_merchant_shift_roaster_changes.shift_change_date as tanggal',
                'hr_merchant_shift_roaster_changes.is_accepted as status_approval',
                'hr_merchant_shift_roaster_changes.is_accepted',
                'hr_merchant_shift_roaster_changes.staff_id',
                'hr_merchant_shift_roaster_changes.staff_user_id',
                'hr_merchant_shift_roaster_changes.to_staff_id',
                'hr_merchant_shift_roaster_changes.to_staff_user_id',
                'hr_merchant_shift_roaster_changes.reason as alasan',
                'hr_merchant_shift_roaster_changes.is_from_mobile',
                'hr_merchant_shift_roaster_changes.shift_id',
                'm.id as merchant_id',
                'm.name as cabang',
                'hr_merchant_shift_roaster_changes.approval_by',
                'hr_merchant_shift_roaster_changes.approval_date',
                'sh.name as shift',
                'sh.color as shift_color',
                'u.fullname as karyawan',
                'u2.fullname as pengganti',
                'u3.fullname as approved_by',
                'hr_merchant_shift_roaster_changes.approval_date as waktu_approval',
                'hr_merchant_shift_roaster_changes.is_from_mobile'


            ])->join('md_merchants as m','m.id','hr_merchant_shift_roaster_changes.md_merchant_id')
                ->join('md_users as u','u.id','hr_merchant_shift_roaster_changes.staff_user_id')
                ->join('hr_merchant_shift_settings as sh','sh.id','hr_merchant_shift_roaster_changes.shift_id')
               ->leftJoin('md_users as u2','u2.id','hr_merchant_shift_roaster_changes.to_staff_user_id')
                ->leftJoin('md_users as u3','u3.id','hr_merchant_shift_roaster_changes.approval_by')
                ->orderBy('hr_merchant_shift_roaster_changes.id','desc')
                ->where('hr_merchant_shift_roaster_changes.is_deleted',0)
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function listOption()
    {
        return self::getDataForDataTable()->whereIn('hr_merchant_shift_roaster_changes.md_merchant_id',MerchantUtil::getBranch(merchant_id(),1))->get();
    }


    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();

        $getBranch=MerchantUtil::getBranch(merchant_id(),0);
        $data->whereIn('hr_merchant_shift_roaster_changes.md_merchant_id',$getBranch);

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(u.fullname) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }


        $startDate=$request->start_date;
        $endDate=$request->end_date;

        $encode=json_decode(base64_decode($request->encode));
        if(is_array($encode))
        {
            $filterMerchant=$encode;
        }else{
            $filterMerchant=json_decode($encode);
        }

        if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
            $getBranch=MerchantUtil::getBranch(merchant_id(),0);

            $data->whereIn('m.id',$getBranch);
        }else{
            $data->whereIn('m.id',$filterMerchant);
        }
        if($startDate!='-')
        {
            $data->whereRaw("
            hr_merchant_shift_roaster_changes.shift_change_date between '$startDate' and '$endDate'
        ");
        }


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','shift','status_approval'],$isEdit,$isDelete,$isHead)
            ->editColumn('tanggal',function ($list){
                return Carbon::parse($list->tanggal)->isoFormat('D MMMM YYYY');
            })
            ->editColumn('shift',function ($list){
                return '<span class="badge badge-info f-12 p-1" style="background-color: '.$list->shift_color.'">'.$list->shift.'</span>';
            })
            ->editColumn('status_approval',function ($list){
                if($list->status_approval==0)
                {
                    if(is_null($list->approved_by)){
                        return "<span class='badge badge-info'>Menunggu Approval</span>";
                    }
                }elseif($list->status_approval==1){
                    return "<span class='badge badge-success'>Disetujui</span>";
                } else{
                        return "<span class='badge badge-danger'>Ditolak</span>";
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'tanggal',
            'karyawan',
            'shift',
            'alasan',
            'pengganti',
            'status_approval',
            'approved_by',
            'cabang',
            'opsi'
        ];
    }


    public static function getButton($list,$edit,$delete,$isHead){

        if($isHead==true)
        {

            if($list->is_from_mobile==0)
            {

                if($list->is_accepted==1){
                    $idRejectApproval="'".$list->id.'_'.'1'.'_approval'."'";

                    return '<div class="dropdown">
  <button class="btn btn-all-other-xs btn-xs" style="border: 1px solid #99A5B5" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-ellipsis-v mr-1 ml-1"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item text-success" onclick="acceptOrRejectData('.$idRejectApproval.')"><i class="fa fa-check mr-1"></i>Batalkan Persetujuan</a>
    <a class="dropdown-item text-danger" onclick="deleteData('.$list->id.')"><i class="fa fa-trash-alt mr-1"></i>Hapus</a>
  </div>
</div>
            ';
                }elseif ($list->is_accepted==2){
                    $idRejectRejected="'".$list->id.'_'.'2'.'_reject'."'";

                    return '<div class="dropdown">
  <button class="btn btn-all-other-xs btn-xs" style="border: 1px solid #99A5B5" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-ellipsis-v mr-1 ml-1"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item text-orange" onclick="acceptOrRejectData('.$idRejectRejected.')"><i class="fa fa-remove mr-1"></i>Batalkan Penolakan</a>
    <a class="dropdown-item text-danger" onclick="deleteData('.$list->id.')"><i class="fa fa-trash-alt mr-1"></i>Hapus</a>
  </div>
</div>';
                }else{
                    $idRejectApproval="'".$list->id.'_'.'0'.'_approval'."'";
                    $idRejectRejected="'".$list->id.'_'.'0'.'_reject'."'";
                    return '<div class="dropdown">
  <button class="btn btn-all-other-xs btn-xs" style="border: 1px solid #99A5B5" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-ellipsis-v mr-1 ml-1"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item text-success" onclick="acceptOrRejectData('.$idRejectApproval.')"><i class="fa fa-check mr-1"></i>Setujui</a>
    <a class="dropdown-item text-orange" onclick="acceptOrRejectData('.$idRejectRejected.')"><i class="fa fa-remove mr-1"></i>Tolak</a>
    <a class="dropdown-item text-danger" onclick="deleteData('.$list->id.')"><i class="fa fa-trash-alt mr-1"></i>Hapus</a>
  </div>
</div>';
                }


            }else{

                if($list->is_accepted==2)
                {

                    $idRejectRejected="'".$list->id.'_'.'2'.'_reject'."'";

                    return '<div class="dropdown">
  <button class="btn btn-all-other-xs btn-xs" style="border: 1px solid #99A5B5" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-ellipsis-v mr-1 ml-1"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item text-orange" onclick="acceptOrRejectData('.$idRejectRejected.')"><i class="fa fa-remove mr-1"></i>Batalkan Penolakan</a>
  </div>
</div>';

                }elseif ($list->is_accepted==1)
                {
                    $idRejectApproval="'".$list->id.'_'.'1'.'_approval'."'";

                    return '<div class="dropdown">
  <button class="btn btn-all-other-xs btn-xs" style="border: 1px solid #99A5B5" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-ellipsis-v mr-1 ml-1"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item text-success" onclick="acceptOrRejectData('.$idRejectApproval.')"><i class="fa fa-check mr-1"></i>Batalkan Persetujuan</a>
    <a class="dropdown-item text-danger" onclick="deleteData('.$list->id.')"><i class="fa fa-trash-alt mr-1"></i>Hapus</a>
  </div>
</div>
            ';

                }else{
                    $idRejectApproval="'".$list->id.'_'.'0'.'_approval'."'";
                    $idRejectRejected="'".$list->id.'_'.'0'.'_reject'."'";
                    return '<div class="dropdown">
  <button class="btn btn-all-other-xs btn-xs" style="border: 1px solid #99A5B5" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
     <i class="fa fa-ellipsis-v mr-1 ml-1"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
   <a class="dropdown-item text-success" onclick="acceptOrRejectData('.$idRejectApproval.')"><i class="fa fa-check mr-1"></i>Setujui</a>
    <a class="dropdown-item text-orange" onclick="acceptOrRejectData('.$idRejectRejected.')"><i class="fa fa-remove mr-1"></i>Tolak</a>
   <div>
</div>
            ';
                }
            }
        }

    }

    public static function add($permission)
    {
        if($permission==true){
            return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.hr.shift-roaster-change.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
        }
    }
}

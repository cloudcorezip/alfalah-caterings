<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\HR;


use App\Classes\Singleton\DataTable;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Models\Departement;

class DepartementEntity  extends Departement
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('employee', merchant_id());
    }

    public static function getDataForDataTable()
    {

        try {
            return Departement::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY hr_merchant_departements.id)'),
                'hr_merchant_departements.id',
                'hr_merchant_departements.name as departemen',
                'm.id as merchant_id',
                'm.name as dibuat_oleh_cabang',
                'hr_merchant_departements.parent_id as sub_departemen_dari',
            ])
                ->join('md_merchants as m','m.id','hr_merchant_departements.md_merchant_id')
                ->orderBy('hr_merchant_departements.id','desc')
                ->where('hr_merchant_departements.is_deleted',0)
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function listOption()
    {
        return self::getDataForDataTable()->whereIn('hr_merchant_departements.md_merchant_id',MerchantUtil::getBranch(merchant_id(),1))->get();
    }


    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();

        $getBranch=MerchantUtil::getBranch(merchant_id(),1);
        $data->whereIn('hr_merchant_departements.md_merchant_id',$getBranch);

        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(hr_merchant_departements.name) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }


        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi'],$isEdit,$isDelete,$isHead)
            ->editColumn('sub_departemen_dari',function ($list){
                return is_null($list->sub_departemen_dari)?'-':Departement::getParent($list->sub_departemen_dari)->name;
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'departemen',
            'sub_departemen_dari',
//            'dibuat_oleh_cabang',
            'opsi'
        ];
    }


    public static function getButton($list,$edit,$delete,$isHead){

        if ($list->merchant_id == merchant_id()) {
            if($isHead==true)
            {
                return "<a  onclick='loadModal(this)' target='".route('merchant.hr.departement.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModal(this)' target='".route('merchant.hr.departement.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  onclick='loadModal(this)' target='".route('merchant.hr.departement.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.hr.departement.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        }else if (merchant_detail()->is_branch == 0) {
            if($permission==true){
                return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.hr.departement.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
            }
        } else {
            if (self::getAccess() == true) {
                if($permission==true){
                    return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.hr.departement.add').'"><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
                }
            } else {
                return "";
            }
        }

    }
}

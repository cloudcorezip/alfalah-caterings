<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\Merchant;
use Illuminate\Support\Facades\DB;

class MerchantEntity extends Merchant
{
    public static function getDataForDataTable()
    {

        try {
            return Merchant::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchants.id)'),
                'md_merchants.*',
                'u.fullname',
                'u.email',
                'b.name as type_business_name',
                'n.name as subscription_name',
                'u.is_verified_merchant',
                's.name as service_name'
            ])
                ->join('md_app_services as s','s.id','md_merchants.md_app_service_id')
                ->join('md_type_of_business as b','b.id','md_merchants.md_type_of_business_id')
                ->join('md_users as u','md_merchants.md_user_id','u.id')
                ->join('md_subscriptions as n','md_merchants.md_subscription_id','n.id')
                ->where('u.is_verified_merchant',1)
                ->where('md_merchants.id',merchant_id())
                ;

        }catch (\Exception $e)
        {
            
            return [];

        }

    }

    public static function dataTable($request)
    {
        $data=self::getDataForDataTable();
        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        return(DataTable::getInstance())->dataTable(self::class,$data,$request,['opsi','is_verified_merchant'],
            ['name','fullname'])
            ->editColumn('is_verified_merchant',function ($list){
                if($list->is_verified_merchant==0){
                    return '<span class="badge badge-warning"> Not Approved</span>';

                }elseif($list->is_verified_merchant==1){
                    return '<span class="badge badge-success">Approved</span>';
                }else{
                    return '<span class="badge badge-danger">Rejected</span>';

                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'name',
            'service_name',
            'type_business_name',
            'address',
            'fullname',
            'email',
            'is_verified_merchant',
            'created_at',
            'subscription_name'
        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'md_subscription_id'=>'n.id'
        ];
        return $field;
    }

    public static function getButton($list){
        return "<a href='".route('merchant.list.detail',['id'=>$list->id])."' class='btn btn-sm btn-success btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-eye' style='color: white'></span>
                                            </a>
                                          ";
    }

    public static function getDetail($id){

        try{
            return Merchant::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchants.id)'),
                'md_merchants.*',
                'u.fullname',
                'u.email',
                'b.name as type_business_name',
                'u.is_verified_merchant',
                's.name as service_name'
            ])
                ->join('md_app_services as s','s.id','md_merchants.md_app_service_id')
                ->join('md_type_of_business as b','b.id','md_merchants.md_type_of_business_id')
                ->join('md_users as u','md_merchants.md_user_id','u.id')
                ->where('md_merchants.id',$id)
                ->first();

        }catch (\Exception $e)
        {
            return null;
        }
    }


}

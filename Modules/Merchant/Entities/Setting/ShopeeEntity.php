<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Setting;


use App\Classes\Singleton\DataTable;
use App\Models\Plugin\EnableMerchantPluginDetail;
use App\Models\MasterData\Plugin;
use Illuminate\Support\Facades\DB;

class ShopeeEntity extends EnableMerchantpluginDetail
{

    public static function getDataForDataTable($subType)
    {

        try {
            return EnableMerchantPluginDetail::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY enable_merchant_plugin_details.id)'),
                'enable_merchant_plugin_details.id',
                'shop_name as nama_toko',
                'is_active as status',
                'shop_id',
                'p.id as plugins_id',
                'p.is_multiple',
                'p.name as nama'
            ])
                ->join('enable_merchant_plugins as emp', 'emp.id', 'enable_merchant_plugin_details.enable_merchant_plugin_id')
                ->join('md_plugins as p', 'p.id', 'emp.md_plugin_id')
                ->where('emp.is_deleted', 0)
                ->where('p.type', Plugin::E_COMMERCE)
                ->where('p.sub_type', $subType)
                ->where('p.is_deleted', 0)
                ->where('emp.md_merchant_id', merchant_id())
                ->where('enable_merchant_plugin_details.is_deleted', 0)
                ->orderBy('enable_merchant_plugin_details.id', 'desc');

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable($request->subType);
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(enable_merchant_plugin_details.shop_id) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(enable_merchant_plugin_details.code) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi', 'status'],$isEdit,$isDelete,$isHead)
            ->editColumn('status', function($list){
                if($list->status == 0){
                    return '<span class="badge badge-danger">Tidak Aktif</span>';
                } else {
                    return '<span class="badge badge-success">Aktif</span>';
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'nama_toko',
            'shop_id',
            'status',
            'opsi',
        ];
    }

    public static function getButton($list,$edit,$delete,$isHead){

        return "
                <a onclick='loadModal(this)' target='".route('setting.shopee.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Ubah Status'>
                    <span class='fa fa-edit' style='color: white'></span>
                </a>
                <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded' title='Hapus Data'>
                    <span class='fa fa-trash-alt' style='color: white'></span>
                </a>
            ";
    }

    public static function add($permission)
    {
        if($permission==true){
            return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('setting.connect-shop.add').'"><i class="fa fa-plus mr-2"></i> Hubungkan Toko Online</button>
                    </div>';
        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\UserActivityNotification;
use App\Models\SennaToko\ProductCategory;
use Illuminate\Support\Facades\DB;

class NotifyEntity extends UserActivityNotification
{

    public static function getDataForDataTable()
    {

        try {
            return UserActivityNotification::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY id)'),
                'id',
                'title as judul',
                'body as pemberitahuan',
                'is_read',
                'created_at as waktu_transaksi'
            ])
                ->where('md_user_id',user_id())
                ->orderBy('id','desc')
                ;

        }catch (\Exception $e)
        {
            
            return [];

        }

    }

    public static function dataTable($request)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForDataTable(),$request,['opsi'],
            ['title'])
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'judul',
            'pemberitahuan',
            'waktu_transaksi',
            'opsi'
        ];
    }



    public static function getButton($list)
    {
        if($list->is_read==0){
            return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-danger btn-rounded'>
                                                <span class='fa fa-eye' style='color: white'></span>
                                            </a>
                                          ";
        }
    }

}

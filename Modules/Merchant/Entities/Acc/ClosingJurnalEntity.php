<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Entities\Acc;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\Merchant;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Accounting\ClosingJournalDistribution;

class ClosingJurnalEntity
{
    public static function getDataForDataTable()
    {

        try {
            return ClosingJournalDistribution::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY acc_closing_journal_distributions.id)'),
                'acc_closing_journal_distributions.id',
                'acc_closing_journal_distributions.start_date as tanggal',
                'acc_closing_journal_distributions.end_date',
                'acc_closing_journal_distributions.end_amount as laba_atau_rugi',
                'acc_closing_journal_distributions._timezone',
                'acc_closing_journal_distributions.md_merchant_id',
                'm.name as outlet',
                'm.name as cabang',
                DB::raw("
                coalesce((
                SELECT
                  json_agg(jd.*) AS jsonb_agg
                FROM
                  (
                    select
                      acd.id,
                      acd.code,
                      acc.parent_code,
                      j.trans_amount,
                      acd.name
                    from
                      acc_jurnal_details ajd
                      join acc_coa_details acd on ajd.acc_coa_detail_id = acd.id
                      join acc_coa_categories acc on acc.id = acd.acc_coa_category_id
                      join acc_jurnals j on j.id = ajd.acc_jurnal_id
                    where
                      acc.code in('3.1.00', '3.1.00', '3.2.00')
                      and acc.md_merchant_id = acc_closing_journal_distributions.md_merchant_id
                      and j.is_deleted = 0
                      and j.md_merchant_id = acc_closing_journal_distributions.md_merchant_id
                      and j.ref_code = acc_closing_journal_distributions.code
                      order by ajd.id asc
                  ) jd
                )
                ,'[]') as distribusi_akhir
                "),
            ])->join('md_merchants as m','m.id','acc_closing_journal_distributions.md_merchant_id')
                ->where('acc_closing_journal_distributions.is_deleted',0);

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(acc_closing_journal_distributions.code) like '%".strtolower(str_replace("'","''",$search))."%' ");
            });
        }

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=$request->start_date;
        $endDate=$request->end_date;
        $encode=json_decode(base64_decode($request->encode));
        if(is_array($encode))
        {
            $filterMerchant=$encode;
        }else{
            $filterMerchant=json_decode($encode);
        }

        if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
            $getBranch=MerchantUtil::getBranch(merchant_id(),0);

            $data->whereIn('acc_closing_journal_distributions.md_merchant_id',$getBranch);
        }else{
            $data->whereIn('acc_closing_journal_distributions.md_merchant_id',$filterMerchant);
        }

        if($startDate!='-')
        {
            $data->whereRaw("
        acc_closing_journal_distributions.end_date::date between '$startDate' and '$endDate'
        ");
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','distribusi_akhir'],$isEdit,$isDelete,$isHead)
            ->editColumn('tanggal',function ($list){
                return Carbon::parse($list->tanggal)->isoFormat('dddd, D MMMM Y')
                    .' sd '.Carbon::parse($list->end_date)->isoFormat('dddd, D MMMM Y')
                    .' '.getTimeZoneName($list->_timezone);
            })->editColumn('laba_atau_rugi',function ($list){
                return ($list->laba_atau_rugi<0)?'('.rupiah(abs($list->laba_atau_rugi)).' )':rupiah($list->laba_atau_rugi);
            })->editColumn('distribusi_akhir',function ($list){
                if($list->distribusi_akhir!='[]' || !empty($list->distribusi_akhir)){
                    $item=json_decode($list->distribusi_akhir);
                    $html="";
                    foreach ($item as $b){
                        $bItem=$b->parent_code.' - '.str_replace('.','',$b->code).' '.$b->name.' : ';
                        $html.="<li>$bItem  ".rupiah($b->trans_amount)."</li>";
                    }
                    return $html;
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'cabang',
            'tanggal',
            'laba_atau_rugi',
            'distribusi_akhir'
        ];
    }



    public static function getButton($list,$edit,$delete,$isHead){

        if($list->md_merchant_id==merchant_id())
        {

            if($isHead==true)
            {
                return "    <a   href='".route('merchant.toko.acc.journal.closing.add',['id'=>$list->id,'step'=>1])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-eye' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return ' ';
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }


    }

    public static function getFilterMap()
    {
        $field = [
        ];
        return $field;
    }

    public static function add($permission)
    {
        if($permission==true){
            return ' <div class="col-lg-auto col-md-12">
                        <a class="btn btn-success btn-rounded btn-sm py-2 px-4 btn-block" href="'.route('merchant.toko.acc.journal.closing.add').'"><i class="fa fa-plus mr-2"></i> Tambah</a>
                    </div>';
        }
    }
}

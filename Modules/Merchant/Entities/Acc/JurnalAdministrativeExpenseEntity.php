<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Acc;


use App\Classes\Singleton\DataTable;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalAdministrativeExpense;
use App\Models\Accounting\JurnalDetail;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class JurnalAdministrativeExpenseEntity extends JurnalDetail
{
    public static function getDataForDataTable()
    {

        try {
            return Jurnal::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY acc_jurnals.id)'),
                'acc_jurnals.id',
                DB::raw('coalesce(acc_jurnals.second_code,acc_jurnals.ref_code) as kode'),
                DB::raw("replace(acc_jurnals.trans_name,coalesce(acc_jurnals.second_code,acc_jurnals.ref_code),'') as  nama_transaksi"),
                'acc_jurnals.trans_time as waktu_transaksi',
                'acc_jurnals.trans_purpose as tujuan',
                'acc_jurnals.trans_proof as bukti_transaksi',
                DB::raw("rupiah(acc_jurnals.trans_amount+acc_jurnals.admin_fee) as jumlah"),
                DB::raw("acc_jurnals.trans_amount+acc_jurnals.admin_fee as jumlah_original"),
                'm.id as merchant_id',
                'm.name as outlet',
                'm.name as cabang',
                'acc_jurnals.timezone',
                'acc_jurnals.trans_type',
                'acc_jurnals.admin_fee',
                'acc_jurnals.trans_amount'
            ])
                ->join('md_merchants as m','m.id','acc_jurnals.md_merchant_id')
                ->groupBy('acc_jurnals.id','m.id')
                ->whereIn('acc_jurnals.trans_type',[4,3])
                ->orderBy('acc_jurnals.id','asc');

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(acc_jurnals.ref_code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                    ->orWhereRaw("lower(acc_jurnals.second_code) like '%".strtolower(str_replace("'","''",$search))."%'")

                    ->orWhereRaw("lower(acc_jurnals.trans_name) like '%".strtolower(str_replace("'","''",$search))."%'");
            });
        }

        $data->where('acc_jurnals.is_deleted',0)->whereIn('acc_jurnals.trans_type',[4,3]);

        $encode=json_decode(base64_decode($request->encode));
        if(is_array($encode))
        {
            $filterMerchant=$encode;
        }else{
            $filterMerchant=json_decode($encode);
        }

        if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
            $getBranch=MerchantUtil::getBranch(merchant_id(),0);

            $data->whereIn('acc_jurnals.md_merchant_id',$getBranch);
        }else{
            $data->whereIn('acc_jurnals.md_merchant_id',$filterMerchant);
        }

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        $startDate=$request->start_date;
        $endDate=$request->end_date;
        if($startDate!='-')
        {
            $data->whereRaw("
        acc_jurnals.trans_time::date between '$startDate' and '$endDate'
        ");
        }
        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','bukti_transaksi'],$isEdit,$isDelete,$isHead)
            ->editColumn('bukti_transaksi',function ($list){
                if(!is_null($list->bukti_transaksi) && $list->bukti_transaksi!='-')
                {
                    return "<a href='".env('S3_URL').$list->bukti_transaksi."' target='_blank' class='btn btn-xs btn-rounded btn-info text-white'><i class='fa fa-download mr-2'></i>Download</a>";
                }else{
                    return '<span class="badge badge-pill badge-warning text-white">Tidak ada bukti transaksi</span>';
                }
            })
            ->editColumn('waktu_transaksi',function ($list){
                return Carbon::parse($list->waktu_transaksi)->isoFormat('dddd, D MMMM Y').' '.getTimeZoneName($list->timezone);
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode',
            'cabang',
            'nama_transaksi',
            'waktu_transaksi',
            'jumlah',
            'bukti_transaksi',

        ];
    }

    public static function getFilterMap()
    {
        $field = [
            'trans_type'=>'acc_jurnals.trans_type'
        ];
        return $field;
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->trans_type==4)
        {
            if($isHead==true)
            {
                return "<a  href='".route('merchant.toko.acc.jurnal.administrative-expense.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  href='".route('merchant.toko.acc.jurnal.administrative-expense.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  href='".route('merchant.toko.acc.jurnal.administrative-expense.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }else{
            if($isHead==true)
            {
                return "<a   href='".route('merchant.toko.acc.jurnal.other-income.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  href='".route('merchant.toko.acc.jurnal.other-income.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  href='".route('merchant.toko.acc.jurnal.other-income.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }


    }

    public static function add($permission)
    {
        if($permission==true){

            return ' <div class="col-lg-auto col-md-12 mb-3">
                            <button type="button" class="btn btn-sm btn-rounded py-2 px-4 btn-success dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-plus mr-2"></i>
                            Tambah
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="'.route('merchant.toko.acc.jurnal.administrative-expense.add').'">Biaya</a>
                                <a class="dropdown-item" href="'.route('merchant.toko.acc.jurnal.other-income.add').'">Pendapatan</a>
                            </div>
                        </div>';
        }
    }
}

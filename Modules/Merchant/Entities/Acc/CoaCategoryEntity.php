<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Acc;


use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;

class CoaCategoryEntity extends CoaCategory
{

    public static function getData()
    {
        return CoaDetailEntity::getData();
    }

    public static function getDatas()
    {
        try{
            $data=CoaCategory::
            where('md_merchant_id',merchant_id())
                ->whereIn('is_deleted',[0,2])
                ->orderBy('id','asc')
                ->get();
            return $data;
        }catch (\Exception $e)
        {
            return [];
        }
    }

    public static function add($permission)
    {
        if($permission==true){

            return ' <div class="col-lg-auto col-md-12">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.acc.setting.coa-category.add').'"><i class="fa fa-plus"></i> Tambah</button>
                    </div>';
        }
    }




}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Acc;

use App\Classes\Singleton\DataTable;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\MerchantAp;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MerchantApEntity extends MerchantAp
{

    public static function getDataForDataTable($startDate,$endDate,$search,$merchantList,$searchUser,$typePerson)
    {

        try {

            return collect(DB::select("
            with ap_or_ar as (
                      select
                        ap.id,
                        coalesce(ap.second_code,ap.ap_code) as kode,
                        ap.ap_name as nama_transaksi,
                        ap.ap_time as waktu_transaksi,
                        rupiah(ap.ap_amount) as jumlah,
                        ap.ap_amount as jumlah_original,
                        ap.is_paid_off as status,
                        ap.due_date as jatuh_tempo,
                        ap.timezone,
                        m.id as merchant_id,
                        m.name as outlet,
                        m.name as cabang,
                        'Utang' as type_ap_or_ar,
                        case when ap.is_from_customer=0 then 'supplier' else 'customer' end as tipe_person,
                        ap.ref_user_id,
                         (case when ap.is_from_customer=0 then (select name from sc_suppliers where id=ap.ref_user_id)
                       else (select name from sc_customers where id=ap.ref_user_id)
                       end) as nama_pemberi_atau_penerima,
                       ap.residual_amount,
                       ap.paid_nominal
                      from
                        acc_merchant_ap ap
                        join md_merchants as m on ap.md_merchant_id = m.id
                      where
                        ap.apable_id = 0
                        and ap.is_deleted = 0
                      union
                      select
                        ar.id,
                        coalesce(ar.second_code,ar.ar_code) as kode,
                        ar.ar_name as nama_transaksi,
                        ar.ar_time as waktu_transaksi,
                        rupiah(ar.ar_amount) as jumlah,
                        ar.ar_amount as jumlah_original,
                        ar.is_paid_off as status,
                        ar.due_date as jatuh_tempo,
                        ar.timezone,
                        m.id as merchant_id,
                        m.name as outlet,
                        m.name as cabang,
                        'Piutang' as type_ap_or_ar,
                       case when ar.is_from_supplier=1 then 'supplier' else 'customer' end as tipe_person,
                       ar.ref_user_id,
                       (case when ar.is_from_supplier=1 then (select name from sc_suppliers where id=ar.ref_user_id)
                       else (select name from sc_customers where id=ar.ref_user_id)
                       end) as nama_pemberi_atau_penerima,
                        ar.residual_amount,
                       ar.paid_nominal

                      from
                        acc_merchant_ar ar
                        join md_merchants m on m.id = ar.md_merchant_id
                      where
                        ar.is_deleted = 0
                        and ar.arable_id = 0
                    )
                    select
                      ROW_NUMBER () OVER (ORDER BY id),
                       id,
                       kode,
                       nama_transaksi,
                       waktu_transaksi,
                       jumlah,
                       jumlah_original,
                       status,
                       jatuh_tempo,
                       timezone,
                       merchant_id,
                       outlet,
                       cabang,
                       type_ap_or_ar as tipe,
                       nama_pemberi_atau_penerima,
                       tipe_person,
                       residual_amount,
                       paid_nominal
                    from
                      ap_or_ar where
                      merchant_id in $merchantList
                      $searchUser
                      $typePerson
                      and waktu_transaksi::date between '$startDate' and '$endDate' $search
            "));

        }catch (\Exception $e)
        {
            return collect([]);

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $startDate=$request->start_date;
        $endDate=$request->end_date;
        $refUserId=$request->ref_user_id;
        $typePerson=$request->type_person;

        if($refUserId!='-1' || $refUserId!=-1)
        {
            $searchUser="and ref_user_id=$refUserId";
            $searchType=($typePerson==0)?"and tipe_person='supplier'":"and tipe_person='customer'";
        }else{
            $searchUser='';
            $searchType='';
        }

        if($request->input('search')['value']) {
            $search="and lower(kode) like '%".strtolower($request->input('search')['value'])."%' or lower(nama_transaks) like '%".strtolower($request->input('search')['value'])."%'";
        }else{
            $search='';
        }

        $encode=json_decode(base64_decode($request->encode));
        if(is_array($encode))
        {
            $filterMerchant=$encode;
        }else{
            $filterMerchant=json_decode($encode);
        }
        if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
            $getBranch=MerchantUtil::getBranch(merchant_id(),0);
            $merchantList="(";
            foreach ($getBranch as $key => $item)
            {
                $merchantList.=($key==0)?$item:",".$item;
            }
            $merchantList.=")";

        }else{
            $merchantList="(";
            foreach ($filterMerchant as $key => $item)
            {
                $merchantList.=($key==0)?$item:",".$item;
            }
            $merchantList.=")";
        }

        $data=self::getDataForDataTable($startDate,$endDate,$search,$merchantList,$searchUser,$searchType);



        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi','status'],$isEdit,$isDelete,$isHead)
        ->editColumn('status',function ($list){
            if($list->status==0 && $list->jumlah_original==0){
                return '<span class="badge badge-warning text-white">Belum Ada Utang/Piutang</span>';
            }elseif($list->status == 0 && $list->jumlah_original > 0){
                return '<span class="badge badge-danger">Belum Lunas</span>';
            }else{
                return '<span class="badge badge-success">Lunas</span>';
            }
        })
            ->editColumn('waktu_transaksi',function ($list){
                return Carbon::parse($list->waktu_transaksi)->isoFormat('dddd, D MMMM Y').' '.getTimeZoneName($list->timezone);
            })
        ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'kode',
            'cabang',
            'tipe',
            'nama_transaksi',
            'waktu_transaksi',
            'jumlah',
            'status',
            'jatuh_tempo',

        ];
    }

    public static function getButton($list,$edit,$delete,$isHead){

        if($list->tipe=='Utang')
        {
            if($isHead==true)
            {
                return "<a  href='".route('merchant.toko.acc.journal.ap.detail',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.acc.journal.ap.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.acc.journal.ap.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }
        }else{
            if($isHead==true)
            {
                return "<a  href='".route('merchant.toko.acc.journal.ar.detail',['id'=>$list->id])."' class='btn btn-sm btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteDataAr($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
            }else{
                if($edit==true && $delete==false)
                {
                    return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.acc.journal.ar.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                          ";
                }elseif ($edit==false && $delete==true)
                {
                    return "
                                            <a onclick='deleteDataAr($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }elseif($edit==false && $delete==false){
                    return "";
                }else{
                    return "<a  onclick='loadModalFullScreen(this)' target='".route('merchant.toko.acc.journal.ar.add',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteDataAr($list->id)' class='btn btn-xs btn-delete-xs btn-rounded'>
                                                <span class='fa fa-trash-alt' style='color: white'></span>
                                            </a>
                                          ";
                }
            }

        }




    }

    public static function add($permission)
    {
        if($permission==true){

            return ' <div class="col-lg-auto col-md-12 mb-3">
                            <button type="button" class="btn btn-sm btn-rounded py-2 px-4 btn-success dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-plus mr-2"></i>
                            Tambah
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.acc.journal.ap.add').'">Utang</a>
                                <a class="dropdown-item" onclick="loadModalFullScreen(this)" target="'.route('merchant.toko.acc.journal.ar.add').'">Piutang</a>
                            </div>
                        </div>';
        }
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Entities\Acc;


use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;

class CoaDetailEntity extends CoaDetail
{
    private static function getAccess()
    {
        return BranchConfigUtil::getAccess('finance',merchant_id());
    }

    public static function getData()
    {
        try{
            $data=CoaCategory::
                where('md_merchant_id',merchant_id())
                ->where('is_deleted',0)
                ->whereNull('parent_id')
                ->get();
            return $data;
        }catch (\Exception $e)
        {
            return [];
        }
    }

    public static function getDatas()
    {
        try{
            $data=CoaDetail::
            select(['acc_coa_details.*'])
            ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',merchant_id())
                ->whereIn('acc_coa_details.is_deleted',[0,2])
                ->orderBy('acc_coa_details.id','asc')
                ->where('c.is_deleted',0)->get();
            return $data;
        }catch (\Exception $e)
        {
            return [];
        }
    }

    public static function add($permission)
    {
        if(count(MerchantUtil::getBranch(merchant_id()))>1){

            if($permission==true){
                return ' <div class="col-lg-2 col-md-6 mb-2">
                        <button class="btn btn-success btn-rounded btn-sm py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.acc.setting.coa-list.add').'"><i class="fa fa-plus"></i> Tambah</button>
                    </div>';
            }
        } else if(merchant_detail()->is_branch==0)
            {
                if($permission==true){
                    return ' <div class="col-lg-2 col-md-6 mb-2">
                        <button class="btn btn-success btn-rounded btn-sm py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.acc.setting.coa-list.add').'"><i class="fa fa-plus"></i> Tambah</button>
                    </div>';
                }
            }else{
                if(self::getAccess()==true)
                {
                    if($permission==true){
                        return ' <div class="col-lg-2 col-md-6 mb-2">
                        <button class="btn btn-success btn-rounded btn-sm py-2 px-4" onclick="loadModal(this)" target="'.route('merchant.toko.acc.setting.coa-list.add').'"><i class="fa fa-plus"></i> Tambah</button>
                    </div>';
                    }
                }else{
                    return "";
                }
            }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\QRIS;


use App\Classes\Singleton\DataTable;
use App\Models\QRIS\QRISWithdrawActivity;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class QRISWithdrawEntity extends QRISWithdrawActivity
{

    public static function getDataForDataTable()
    {

        try {
            DB::statement("refresh materialized view mv_wd_table");
            $sale =DB::table("mv_wd_table")
                ->select([
                    'tanggal',
                    'kode_transaksi',
                    DB::raw('rupiah(jumlah+biaya_admin) as jumlah_total'),
                    DB::raw('rupiah(biaya_admin) as biaya_admin'),
                    DB::raw('rupiah(jumlah) as jumlah_diterima'),
                    'asal_transaksi',
                    'tipe_transaksi',
                    'status_transaksi',
                    'waktu_kadaluarsa'
                ])
                ->where("md_merchant_id",merchant_id())
                ->orderBy('tanggal','desc');
            ;
            return $sale;
        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(kode_transaksi) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->orWhereRaw("lower(asal_transaksi) like '%".strtolower(str_replace("'","''",$search))."%'")
                ->orWhereRaw("lower(tipe_transaksi) like '%".strtolower(str_replace("'","''",$search))."%'")
                ;
            });
        }

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }
        $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
        $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
        $data->whereRaw("tanggal::date between '$startDate' and '$endDate'
        ");

        return(DataTable::getInstance())->dataTable(self::class,$data,$request,['status_transaksi','tipe_transaksi'])
                ->editColumn('status_transaksi',function ($list){
                    if($list->status_transaksi=="Paid" || $list->status_transaksi=="success" || $list->status_transaksi=="SUCCESS"){
                        return '<span class="btn btn-outline-success btn-rounded disabled">Success</span>';
                    }else if($list->status_transaksi=="pending" || $list->status_transaksi=="PENDING" || $list->status_transaksi=="Unpaid"){
                        $start = Carbon::parse($list->tanggal);
                        $finish = Carbon::parse($list->waktu_kadaluarsa);
                        if($start->diffInHours($finish)>=24){
                            return '<span class="btn btn-outline-danger btn-rounded disabled">Failed</span>';

                        }else{
                            return '<span class="btn btn-outline-primary btn-rounded disabled">Pending(Belum Terbayarkan)</span>';

                        }
                    }else{
                        return '<span class="btn btn-outline-danger btn-rounded disabled">Failed</span>';
                    }
                })
                ->editColumn('tipe_transaksi',function ($list){
                    if($list->tipe_transaksi=="in"){
                        return '<span class="text-success">Masuk</span>';
                    }else{
                        return '<span class="text-danger">Keluar</span>';
                    }
                })
                ->editColumn('tanggal',function ($list){
                        return \Carbon\Carbon::parse($list->tanggal)->isoFormat(' D MMMM Y  HH:mm:ss');
                })
            ->make(true);
    }

    public static function getFilterMap()
    {
        $field = [
            'status_transaksi'=>'status_transaksi'
        ];
        return $field;
    }

    public static function dataTableColumns()
    {
        return [
            'tanggal',
            'kode_transaksi',
            'jumlah_total',
            'biaya_admin',
            'jumlah_diterima',
            'asal_transaksi',
            'tipe_transaksi',
            'status_transaksi'
        ];
    }

    public static function getButton($list){
//        return  "<a  onclick='loadModal(this)' target='".route('qris-withdraw.detail',['code'=>$list->kode_transaksi,'tipe'=>$list->tipe_transaksi])."' class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
//                                                <span class='fa fa-eye' style='color: white'></span>
//                                            </a>
//
//                                          ";
        return "";
    }



}

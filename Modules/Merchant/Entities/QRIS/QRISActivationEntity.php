<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\QRIS;


use App\Classes\Singleton\DataTable;
use App\Models\QRIS\QRISActivation;
use Illuminate\Support\Facades\DB;

class QRISActivationEntity extends QRISActivation
{

    public static function getDataForDataTable()
    {

        try {
            return QRISActivation::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY qris_activations.id)'),
                'qris_activations.id',
                'md_user_id',
                'md_merchant_id',
                'identity_card',
                'account_book',
                'is_active as status',
                'bank_account_name as nama_akun_rekening',
                'bank_account_number as nomor_rekening',
                'qris_activations.code',
                'qris_activations.is_active',
                'n.name as bank'
            ])
            ->join('md_rajabiller_banks as n','n.id','qris_activations.md_rajabiller_bank_id')
            ->where('md_merchant_id',merchant_id())
            ;

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];
            $data->where(function($query) use($search){
                $query->whereRaw("lower(n.name) like '%".strtolower(str_replace("'","''",$search))."%' ")
                ->orWhereRaw("lower(bank_account_name) like '%".strtolower(str_replace("'","''",$search))."%'")
                ->orWhereRaw("lower(bank_account_number) like '%".strtolower(str_replace("'","''",$search))."%'")
                ;
            });
        }

        $searchKey = $request->input('sk');
        $searchFilter = $request->session()->get($searchKey, []);
        $searchFilter = is_array($searchFilter) ? $searchFilter : [];

        foreach (self::getFilterMap() as $key => $field) {
            if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                $data->where([$field => $searchFilter[$key]]);
            }
        }

        return(DataTable::getInstance())->dataTable(self::class,$data,$request,['opsi','code','status'])
            ->editColumn('status',function ($list){
                if($list->status==1){
                    return '<span class="text-success">Disetujui</span>';
                }else if($list->status==0){
                    return '<span class="text-warning">Menunggu Persetujuan</span>';
                }else{
                    return '<span class="text-danger">Ditolak</span>';
                }
            })
            ->make(true);
    }

    public static function getFilterMap()
    {
        $field = [
            'is_agree'=>'qris_activations.is_agree'
        ];
        return $field;
    }

    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'bank',
            'nama_akun_rekening',
            'nomor_rekening',
            'status'
        ];
    }

    public static function getButton($list){
        return  "
                    <a  href='".route('qris.details',['id'=>$list->id])."' class='btn btn-xs btn-edit-xs text-primary btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-eye' style='color: white'></span>
                                            </a>

                                          ";
    }


}

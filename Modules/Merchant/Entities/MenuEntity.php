<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantStaff;
use Illuminate\Support\Facades\DB;

class MenuEntity  extends MerchantStaff
{
    public static function getDataForDataTable()
    {

        try {
            return MerchantStaff::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_staff.id)'),
                'md_merchant_staff.id',
                'u.fullname as nama_lengkap',
                'u.email',
                'r.name as jabatan'
            ])
                ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->where('m.id',merchant_id())
                ->where('md_merchant_staff.is_deleted',0)
                ->where('md_merchant_staff.is_non_employee',0)
                ->orderBy('md_merchant_staff.id','desc')
                ;

        }catch (\Exception $e)
        {

            return [];

        }

    }
    public static function dataTable($request)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForDataTable(),$request,['opsi'],
            ['u.fullname'])
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'nama_lengkap',
            'email',
            'jabatan'
        ];
    }

    public static function getButton($list){
        return "<a  href='".route('merchant.menu.detail',['id'=>$list->id])."' class='btn btn-xs btn-primary btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>
                                          ";
    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Entities\Plugin;


use App\Classes\Singleton\DataTable;
use App\Models\SennaPayment\PaymentPlugin;
use Illuminate\Support\Facades\DB;

class PluginHistoryEntity extends PaymentPlugin
{

    public static function getDataForDataTable()
    {

        try {
            return PaymentPlugin::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sp_payment_plugins.id)'),
                'sp_payment_plugins.id',
                's.code as kode_invoice',
                'ts.name as status',
                'tt.name as transaction_type',
                's.amount as harga',
                's.transaction_to',
                's.transaction_from',
                'p.name as nama_plugin',
                's.discount',
                's.admin_fee',
                's.md_sp_transaction_status_id'
            ])
                ->join('sp_transactions as s','s.transactionable_id','sp_payment_plugins.id')
                ->join('md_sp_transaction_status as ts','ts.id','s.md_sp_transaction_status_id')
                ->join('md_sp_transaction_types as tt','tt.id','s.md_sp_transaction_type_id')
                ->join('md_plugins as p','p.id','sp_payment_plugins.md_plugin_id')
                ->where('s.transactionable_type','App\Models\SennaPayment\PaymentPlugin')
                ->where('s.md_user_id',user_id())
                ->orderBy('sp_payment_plugins.created_at','desc');

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request,$isEdit=true,$isDelete=true,$isHead=true)
    {
        $data=self::getDataForDataTable();
        if($request->input('search')['value']) {
            $search=$request->input('search')['value'];

            $data->where(function($query) use($search){
                $query->whereRaw("lower(p.name) like '%".strtolower($search)."%' ")
                    ->orWhereRaw("lower(s.code) like '%".strtolower($search)."%' ");
            });
        }

        return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,['opsi', 'harga', 'status'],$isEdit,$isDelete,$isHead)
            ->editColumn('harga', function($list){
                return rupiah(ceil($list->harga));
            })
            ->editColumn('status', function($list){
                if(strtolower($list->status) == 'success'){
                    return '<i style="color:#7EB056;">'.$list->status.'</i>';
                } else {
                    return '<i style="color:#FF9E0D;">'.$list->status.'</i>';
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'kode_invoice',
            'nama_plugin',
            'harga',
            'status',
            'opsi',
        ];
    }

    public static function getButton($list,$edit,$delete,$isHead){

        return "<a href='".route('plugin.invoice', ['key' => encrypt($list->id),'status'=>(strtolower($list->status) == 'success')?1:0])."' class='btn btn-xs btn-edit-xs btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-eye' style='color: white'></span>
                                            </a>
                                          ";
    }

    public static function add($permission)
    {
        if($permission==true){
            return ' <div class="col-lg-auto col-md-12 mb-3">
                        <button class="btn btn-success btn-rounded btn-sm btn-block py-2 px-4" onclick="loadModal(this)" target=""><i class="fa fa-plus mr-2"></i> Tambah</button>
                    </div>';
        }
    }




}

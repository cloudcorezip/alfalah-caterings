<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Plugin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\Plugin;
use App\Models\Plugin\EnableMerchantPlugin;
use App\Models\SennaPayment\Transaction;
use App\Models\SennaPayment\PaymentPlugin;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\SennaPayment\TransactionStatus;


class PluginSettingController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('/plugin-gcontact')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::post('/save','Plugin\PluginSettingController@save')
                ->name('plugin-gcontact.save');

        });
    }

    public function save(Request $request)
    {
        try {
            $id = $request->id;

            $data = EnableMerchantPlugin::find($id);

            $validator = Validator::make($request->all(), ['email' => 'required|email', 'client_id' =>'required','client_secret' =>'required', 'client_token' =>'required']);

            if($validator->fails()){
                $error = $validator->errors()->first();
                return response()->json([
                    'message'=>$error,
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $data->email = $request->email;
            $data->client_id = $request->client_id;
            $data->client_secret = $request->client_secret;
            $data->client_token = $request->client_token;
            $data->save();

            return response()->json([
                'message'=>'Data berhasil disimpan!',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Plugin;

use App\Builder\ClinicInstallation;
use App\Builder\SfaInstallation;
use App\Http\Controllers\Controller;
use App\Classes\Singleton\XenditCore;
use App\Models\Cfg\MerchantMenu;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\Plugin;
use App\Models\Plugin\EnableMerchantPlugin;
use App\Models\MasterData\Subscription;
use App\Models\SennaPayment\Transaction;
use App\Models\SennaPayment\PaymentPlugin;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\SennaPayment\TransactionStatus;
use App\Utils\Membership\Merchant\RedeemUtil;
use App\Utils\Subscription\SubscriptionUtil;


class PluginController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('/plugin')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::get('/','Plugin\PluginController@index')
                ->name('plugin.index');
            Route::post('/list/{id}','Plugin\PluginController@list')
                ->name('plugin.list');

            Route::post('/create-invoice', 'Plugin\PluginController@createInvoice')
                ->name('plugin.create-invoice');
            Route::get('/invoice/{key}/{status}','Plugin\PluginController@invoice')
                ->name('plugin.invoice');

            Route::post('/checkout', 'Plugin\PluginController@checkout')
                ->name('plugin.checkout');
            Route::post('/check-fee', 'Plugin\PluginController@checkFee')
                ->name('plugin.check-fee');
        });
    }

    public function index(Request $request)
    {
        $merchantId = merchant_id();

        $data = Plugin::whereNotNull('parent_id')
            ->where('is_deleted', 0)
            ->where('is_unlimited', 0)
            ->get();

        $params = [
            "title" => "Daftar Integrasi",
            "data" => $data,
            "user" => User::find(user_id())
        ];

        return view('merchant::plugin.index', $params);
    }


    public function list(Request $request)
    {
        try {
            $id = $request->id;

            $data = Plugin::with(['getChild' => function($query){
                        $query->where('is_unlimited', 0)
                            ->where('is_deleted', 0);
                    }])
                    ->where('id', $id)
                    ->first();
            $params = [
                "title" => "Daftar Paket Plugin - ".$data->name,
                "data" => $data
            ];

            return view('merchant::plugin.list', $params);

        } catch(\Exception $e)
        {
            abort(404);
        }
    }

    public function createInvoice(Request $request)
    {
        try {
            date_default_timezone_set((is_null($request->timezone))?'Asia/Jakarta':$request->timezone);
            DB::beginTransaction();

            $plugin=Plugin::find($request->md_plugin_id);
            $emp = EnableMerchantPlugin::
                    select([
                        'enable_merchant_plugins.id',
                        'enable_merchant_plugins.md_plugin_id',
                        'enable_merchant_plugins.status',
                        'mp.name'
                    ])
                        ->join('md_plugins as mp', 'mp.id', 'enable_merchant_plugins.md_plugin_id')
                        ->where('enable_merchant_plugins.md_merchant_id', merchant_id())
                        ->where('enable_merchant_plugins.status', 1)
                        ->where('enable_merchant_plugins.is_deleted',0)
                        ->where('mp.parent_id', $plugin->parent_id)
                        ->first();

            if(!is_null($emp) && $emp->md_plugin_id != $request->md_plugin_id){
                return response()->json([
                    'message'=>'Pembelian tidak dapat dilakukan dikarenakan masih ada plugin yang aktif',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            $data = new PaymentPlugin();

            $user = User::find(user_id());
            $code = CodeGenerator::generateTransactionCode($user->id,TransactionType::PLUGIN_FEE);
            $discount = ceil(($plugin->price * ($plugin->discount_percentage/100)));
            $amount =  ceil($plugin->price - $discount);

            $data->md_merchant_id = merchant_id();
            $data->md_plugin_id = $request->md_plugin_id;
            $data->is_from_web = 1;
            $data->save();

            $data->transaction()->create([
                'code'=>$code,
                'md_user_id'=>$user->id,
                'amount'=>$amount,
                'discount' => $discount,
                'transaction_from'=>$user->fullname,
                'md_sp_transaction_type_id'=>TransactionType::PLUGIN_FEE,
                'md_sp_transaction_status_id'=>TransactionStatus::PENDING,
            ]);

            DB::commit();

            return response()->json([
                'message'=>'Permintaan anda sedang diproses !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('plugin.invoice', ['key' => encrypt($data->id),'status'=>0])
            ]);

        }catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function invoice(Request $request)
    {
        try {
            $id = decrypt($request->key);
            $status=is_null($request->status)?0:$request->status;
            $user = User::find(user_id());

            $data = PaymentPlugin::with('transaction')
                        ->with('getMerchant')
                        ->with('getPlugin')
                        ->where('id', $id)->first();
            if(is_null($data)){
                return abort(404);
            }

            if($data->md_merchant_id != merchant_id()){
                return abort(404);
            }

            $plugin = Plugin::find($data->md_plugin_id);
            $price = ceil($plugin->price - ($plugin->price * ($plugin->discount_percentage/100)));
            $duration = $this->_getDuration($plugin->id);

            if($status==1){
                if($data->getPlugin->type=='sfa'){
                    $install=SfaInstallation::generate($data->md_merchant_id);
                    if($install==false)
                    {
                        return abort(404);

                    }
                }

                if($data->getPlugin->type=='clinic'){
                    $install=ClinicInstallation::generate($data->md_merchant_id);
                    if($install==false)
                    {
                        return abort(404);

                    }
                }
            }
            $params = [
                "title" => "Invoice ".$data->transaction->code,
                "data" => $data,
                "user" => $user,
                "price" => ceil($data->transaction->amount),
                "discount" => ceil($data->transaction->discount),
                "admin" => ceil($data->transaction->admin_fee),
                "duration" => ($duration)? $duration: '-',
                "created_at" => Carbon::parse($data->created_at)->isoFormat('D MMMM Y')
            ];


            return view('merchant::plugin.invoice', $params);

        }catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return abort(404);
        }
    }


    public function checkout(Request $request)
    {
        try {
            DB::beginTransaction();

            $id = $request->id;
            $user = User::find(user_id());
            $data = PaymentPlugin::with('transaction')
                        ->with('getPlugin')
                        ->where('id', $id)->first();

            if(is_null($id) || $data->md_merchant_id != merchant_id() || is_null($data)){
                return response()->json([
                    'message'=>'Terjadi kesalahan di server, silahkan coba lagi nanti !',
                    'type'=>'danger',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($request->md_transaction_type_id)){
                return response()->json([
                    'message'=>'Silahkan pilih metode pembayaran terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($data->getPlugin->discount_percentage>0){
                $price = ceil($data->getPlugin->price - ($data->getPlugin->price * ($data->getPlugin->discount_percentage/100)));
                $discount = ceil(($data->getPlugin->price * ($data->getPlugin->discount_percentage/100)));

            }else{
                $price = $data->getPlugin->price;
                $discount =0;

            }

            $amount = $price;

            $adminFee = ($request->md_transaction_type_id == 4)? ceil($amount * 0.016) : 5000;

            $params = [
                "external_id" => $data->transaction->code,
                "payer_email" => $user->email,
                "amount" => ceil($amount + $adminFee),
                "description" => "Pembayaran Plugin",
                'payment_methods'=>($request->md_transaction_type_id==4)?config('merchant.pay_sub_wallet'):config('merchant.pay_sub_va'),
                "success_redirect_url"=>route('plugin.invoice', ['key' => encrypt($data->id),'status'=>1]),
                "failure_redirect_url"=>route('plugin.invoice', ['key' => encrypt($data->id),'status'=>0])
            ];

            if($data->is_from_web == 0){
                return response()->json([
                    'message'=>'Silahkan buka aplikasi Senna untuk melakukan pembayaran',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=> ''
                ]);
            }

            if($data->is_expired == 0){
                if($data->md_transaction_type_id == $request->md_transaction_type_id){
                    $invoice = json_decode($data->content_resp_web, true);
                } else {
                    $invoice = XenditCore::createInvoiceWeb($params);
                }
            } else {
                return response()->json([
                    'message'=>'Mohon maaf pembayaran gagal dilakukan karena invoice sudah kadaluarsa',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=> ''
                ]);
            }

            if(!$invoice){
                return response()->json([
                    'message'=>'Terjadi kesalahan di server, silahkan coba lagi nanti !',
                    'type'=>'danger',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $data->content_resp_web = json_encode($invoice);
            $data->md_transaction_type_id = $request->md_transaction_type_id;
            $data->save();

            $data->transaction->amount = ceil($amount + $adminFee);
            $data->transaction->discount = ceil($discount);
            $data->transaction->admin_fee = $adminFee;
            $data->transaction->save();

            DB::commit();
            return response()->json([
                'message'=>'Permintaan anda sedang diproses !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=> $invoice['invoice_url']
            ]);
        }catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    protected function _getDuration($id)
    {
        try {
            $plugin = Plugin::where('is_deleted', 0)
                                ->where('id', $id)
                                ->first();

            switch($plugin->payment_type){
                case "monthly":
                    $monthDuration = "1 Bulan";
                    break;
                case "threemonth":
                    $monthDuration = "3 Bulan";
                    break;
                case "sixmonth":
                    $monthDuration = "6 Bulan";
                    break;
                case "yearly":
                    $monthDuration = "12 Bulan";
                    break;
                default:
                    if(!is_null($plugin->quota_range_max)){
                        $monthDuration = "Kuota Maksimal : ".$plugin->quota_range_max;
                    } else {
                        $monthDuration = "-";
                    }
                    break;
            }

            $duration = $monthDuration;
            return $duration;

        } catch(\Exception $e)
        {
            return false;
        }
    }

    public function checkFee(Request $request)
    {
        try {
            $id = $request->id;
            $data = PaymentPlugin::with('transaction')
                        ->with('getPlugin')
                        ->where('id', $id)->first();

            $price = ceil($data->getPlugin->price - ($data->getPlugin->price * ($data->getPlugin->discount_percentage/100)));
            $discount = ceil(($data->getPlugin->price * ($data->getPlugin->discount_percentage/100)));

            if(!is_null($request->md_transaction_type_id)){
                if($request->md_transaction_type_id == 4){
                    $adminFee = ceil($price * 0.016);
                    $amount = ceil($price + $adminFee);
                } else {
                    $adminFee = 5000;
                    $amount = ceil($price + $adminFee);
                }
            } else {
                $adminFee = 0;
            }

            return [
                "amountRupiah" => rupiah($amount),
                "adminFee" => rupiah($adminFee)
            ];

        } catch(\Exception $e)
        {
            return false;
        }
    }

}

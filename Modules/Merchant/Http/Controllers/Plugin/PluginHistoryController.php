<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Plugin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\Plugin;
use Modules\Merchant\Entities\Plugin\PluginHistoryEntity;
use App\Models\SennaPayment\Transaction; 
use App\Models\SennaPayment\PaymentPlugin;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\SennaPayment\TransactionStatus;
use App\Utils\Membership\Merchant\RedeemUtil;
use Illuminate\Support\Facades\DB;
use PDF;

class PluginHistoryController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('/plugin/history')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::get('/','Plugin\PluginHistoryController@index')
                ->name('plugin.history.index');
            Route::post('/data-table', 'Plugin\PluginHistoryController@dataTable')
                ->name('plugin.history.datatable');
            Route::get('/export/{key}', 'Plugin\PluginHistoryController@exportPdf')
                ->name('plugin.history.export-pdf');
        });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params = [
            "title" => "Riwayat Pembelian Plugin",
            "user" => User::find(user_id()),
            "tableColumns" => PluginHistoryEntity::dataTableColumns(),
            'key_val'=>$request->key
        ];

        return view('merchant::plugin.history.index', $params);
    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return PluginHistoryEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public function exportPdf(Request $request)
    {
        try {
            $id = decrypt($request->key);
            $user = User::find(user_id());
            $data = PaymentPlugin::with('transaction')
                        ->with('getMerchant')
                        ->with('getPlugin')
                        ->where('id', $id)->first();
        
            if(is_null($id) || $data->md_merchant_id != merchant_id() || is_null($data)){
                return abort(404);
            }

            $duration = $this->_getDuration($data->md_plugin_id);
            $params = [
                "data" => $data,
                "title" => 'Export Invoice - '.$data->transaction->code,
                "duration" => ($duration)? $duration: '-',
                "discount" => ceil($data->transaction->discount),
                "price" => ceil($data->transaction->amount),
                "admin"=> ceil($data->transaction->admin_fee),
                "created_at" => Carbon::parse($data->created_at)->isoFormat('D MMMM Y'),
                "user" => $user
            ];
            $pdf = PDF::loadview('merchant::plugin.history.invoice-pdf', $params)->setPaper('a4');
            return $pdf->stream('export-invoice-'.$data->transaction->code.'_'.date('Y-m-d').'.pdf');

        }catch(\Exception $e)
        {
            return abort(404);
        }
    }

    protected function _getDuration($id)
    {
        try {
            $plugin = Plugin::where('is_deleted', 0)
                                ->where('id', $id)
                                ->first();

            switch($plugin->payment_type){
                case "monthly":
                    $monthDuration = "1 Bulan";
                    break;
                case "threemonth":
                    $monthDuration = "3 Bulan";
                    break;
                case "sixmonth":
                    $monthDuration = "6 Bulan";
                    break;
                case "yearly":
                    $monthDuration = "12 Bulan";
                    break;
                default:
                    if(!is_null($plugin->quota_range_max)){
                        $monthDuration = "Kuota Maksimal : ".$plugin->quota_range_max;
                    } else {
                        $monthDuration = "-";
                    }
                    break;
            }

            $duration = $monthDuration;
            return $duration;

        } catch(\Exception $e)
        {
            return false;
        }
    }
}

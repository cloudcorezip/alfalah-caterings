<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Ajax;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\ContactEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Modules\Merchant\Entities\HR\DepartementEntity;

class AjaxShareDataController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('ajax/share-data')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/commission-list', [static::class, 'ajaxCommissionList'])
                    ->name('merchant.ajax.share-data.commission-list');
                Route::get('/promo-list', [static::class, 'getPromo'])
                    ->name('merchant.ajax.share-data.promo-list');
                Route::get('/employee-list', [static::class, 'getEmployee'])
                    ->name('merchant.ajax.share-data.employee-list');
                Route::get('/customer-list', [static::class, 'getCustomer'])
                    ->name('merchant.ajax.share-data.customer-list');
                Route::get('/contact-list', [static::class, 'getContact'])
                    ->name('merchant.ajax.share-data.contact-list');
                Route::get('/employee-by-merchant', [static::class, 'getEmployeeByMerchant'])
                    ->name('merchant.ajax.share-data.employee-by-merchant');
                Route::get('/supplier-list', [static::class, 'getSupplier'])
                    ->name('merchant.ajax.share-data.supplier-list');
                Route::get('/department-list', [static::class, 'getDepartment'])
                    ->name('merchant.ajax.share-data.department-list');
                Route::get('/parent-code-setting', [static::class, 'getParentCodeSetting'])
                    ->name('merchant.ajax.share-data.parent-code-setting');
                Route::get('/child-code-setting', [static::class, 'getChildCodeSetting'])
                    ->name('merchant.ajax.share-data.child-code-setting');
            });
    }


    public function ajaxCommissionList(Request  $request)
    {

        try {
            $merchantId=$request->md_merchant_id;
            $staffId=$request->staff_id;
            $isForEmployee=$request->is_for_employee;
            $key= $request->key;
            $commissionQuery=collect(DB::select("
            SELECT
              mc.*,
              mc.name as text,
              p ->> 'id' as product_id,
              s ->> 'id' as staff_id
            FROM
              merchant_commissions mc,
              jsonb_array_elements(mc.product_list) AS p,
              jsonb_array_elements(mc.staff_list) as s
            where
              mc.is_deleted = 0
              and mc.status = 1
              and mc.type='product'
              and mc.is_for_employee = $isForEmployee
              and mc.assign_to_branch @> '[$merchantId]'
              and cast(s ->> 'id' as text) in('$staffId')
            union
            SELECT
              mc.*,
              mc.name as text,
              p ->> 'id' as product_id,
              s ->> 'id' as staff_id
            FROM
              merchant_commissions mc,
              jsonb_array_elements(mc.product_list) AS p,
              jsonb_array_elements(mc.staff_list) as s
            where
              mc.is_deleted = 0
              and mc.status = 1
              and mc.is_for_employee = $isForEmployee
              and mc.type='product'
              and mc.md_merchant_id = $merchantId
              and cast(s ->> 'id' as text) in('$staffId')
            union
            SELECT
              mc.*,
              mc.name as text,
              '-1' product_id,
              s ->> 'id' as staff_id
            FROM
              merchant_commissions mc,
              jsonb_array_elements(mc.staff_list) as s
            where
              mc.is_deleted = 0
              and mc.status = 1
              and mc.type='transaction'
              and mc.is_for_employee = $isForEmployee
              and mc.md_merchant_id =$merchantId
              and cast(s ->> 'id' as text) in('$staffId')

            union
            SELECT
              mc.*,
              mc.name as text,
              '-1' product_id,
              s ->> 'id' as staff_id
            FROM
              merchant_commissions mc,
              jsonb_array_elements(mc.staff_list) as s
            where
              mc.is_deleted = 0
              and mc.status = 1
              and mc.is_for_employee = $isForEmployee
              and mc.type='transaction'
              and mc.assign_to_branch @> '[$merchantId]'
              and cast(s ->> 'id' as text) in('$staffId')
            "));

            return response()->json($commissionQuery);

        }catch (\Exception $e)
        {
            return response()->json([]);
        }
    }

    public function getPromo( Request  $request)
    {
        try {
            $merchantId=$request->md_merchant_id;
            $isCoupon=$request->is_coupon;
            $isAffiliate=$request->is_affiliate;
            $promoQuery=collect(DB::select("
            select
                cdbp.*,
                cdbp.name as text
                from crm_discount_by_products cdbp
                where
                cdbp.is_active=1
                and cdbp.is_deleted=0
                and cdbp.md_merchant_id=$merchantId
                and cdbp.is_coupon=$isCoupon
                and cdbp.is_referral_affiliate=$isAffiliate
                union
                select
                cdbp.*,
                cdbp.name as text
                from crm_discount_by_products cdbp
                where
                cdbp.is_active=1
                and cdbp.is_deleted=0
                and cdbp.assign_to_branch @> '[$merchantId]'
                and cdbp.is_coupon=$isCoupon
                and cdbp.is_referral_affiliate=$isAffiliate
            "));

            return response()->json($promoQuery);

        }catch (\Exception $e)
        {
            return response()->json([]);

        }
    }

    public function getEmployee(Request  $request)
    {
        try {
            $data=MerchantStaff::
            select(($request->is_non_employee==0)?[
                'u.id',
                DB::raw("  u.fullname || ' ' || coalesce( ' [' || p.name || '] ','') || '[' || m.name || '] ' as text"),
            ]:[
                'u.id',
                'u.fullname as text'
            ]
            )
                ->whereIn('md_merchant_staff.md_merchant_id', MerchantUtil::getBranch($request->md_merchant_id, 1))
                ->leftJoin('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->join('md_merchants as m', 'm.id', 'md_merchant_staff.md_merchant_id')
                ->leftJoin('hr_merchant_job_positions  as p','p.id','md_merchant_staff.job_position_id')
                ->where('md_merchant_staff.is_non_employee',$request->is_non_employee)
                ->where('md_merchant_staff.is_deleted',0)
                ->get();

            return response()->json($data);
        }catch (\Exception $e)
        {
            return response()->json([]);

        }

    }


    public function getEmployeeByMerchant(Request $request)
    {
        try {
            $search = $request->key;

            $data=MerchantStaff::
            select(($request->is_non_employee==0)?[
                'u.id',
                DB::raw("  u.fullname || ' ' || coalesce( ' [' || p.name || '] ','') || '[' || m.name || '] ' as text"),
            ]:[
                'u.id',
                'u.fullname as text'
            ]
            )
                ->leftJoin('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->join('md_merchants as m', 'm.id', 'md_merchant_staff.md_merchant_id')
                ->leftJoin('hr_merchant_job_positions  as p','p.id','md_merchant_staff.job_position_id')
                ->where('md_merchant_staff.md_merchant_id', $request->md_merchant_id)
                ->where('md_merchant_staff.is_non_employee',$request->is_non_employee)
                ->where('md_merchant_staff.is_deleted',0);

            if(!is_null($search)){
                $data->where(function($query) use($search){
                    $query->whereRaw("lower(u.fullname) like '%".strtolower(str_replace("'","''",$search))."%' ")
                        ->orWhereRaw("lower(p.name) like '%".strtolower(str_replace("'","''",$search))."%' ");
                });
            }

            return response()->json($data->get());

        } catch(\Exception $e)
        {
            return response()->json([]);
        }
    }


    function getCustomer(Request  $request)
    {
        $search=$request->key;
        $customer=CustomerEntity::listOption($search);

        foreach($customer as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->kode .' '.$item->nama_pelanggan,
            ];
        }

        return response()->json($response);

    }


    function getContact(Request  $request)
    {
        $search=$request->key;
        $contact=ContactEntity::listOption($search);
        $response=[];
        foreach($contact as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->kode .' '.$item->nama,
                "type"=>$item->type
            ];
        }

        return response()->json($response);

    }

    function getSupplier(Request  $request)
    {
        try {
            $search=$request->key;
            $data = SupplierEntity::
            select([
                'sc_suppliers.id',
                DB::raw(" sc_suppliers.code || ' ' || sc_suppliers.name as text")
            ])
            ->join('md_users as u','u.id','sc_suppliers.md_user_id')
            ->join('md_merchants as m','m.md_user_id','u.id')
            ->leftJoin('sc_supplier_categories as c','c.id','sc_suppliers.sc_supplier_categories_id')
            ->where('sc_suppliers.is_deleted', 0)
            ->whereIn('m.id', MerchantUtil::getBranch(merchant_id(),1));

            if(!is_null($search)){
                $data->where(function($query) use($search){
                    $query->whereRaw("lower(sc_suppliers.code) like '%".strtolower(str_replace("'","''",$search))."%' ")
                        ->orWhereRaw("lower(sc_suppliers.name) like '%".strtolower(str_replace("'","''",$search))."%' ");
                });
               
            }

            return response()->json($data->get());
        } catch(\Exception $e)
        {   
            return response()->json([]);
        }

    }

    public function getDepartment(Request $request)
    {
        try {
            $search = $request->key;

            $data = DepartementEntity::
            select([
                'hr_merchant_departements.id',
                'hr_merchant_departements.name as text'
            ])
                ->join('md_merchants as m','m.id','hr_merchant_departements.md_merchant_id')
                ->orderBy('hr_merchant_departements.id','desc')
                ->where('hr_merchant_departements.is_deleted',0)
                ;
                
            if(!is_null($search)){
                $data->where(function($query) use($search){
                    $query->whereRaw("lower(hr_merchant_departements.name) like '%".strtolower(str_replace("'","''",$search))."%' ");
                });
            }

            $data->whereIn('hr_merchant_departements.md_merchant_id',MerchantUtil::getBranch(merchant_id(),1));
            return response()->json($data->get());

        } catch(\Exception $e)
        {
            return response()->json([]);
        }
    }

    public function getParentCodeSetting(Request $request)
    {
        try {
            $keyword = $request->key;

            $search = is_null($keyword)? "":" and (mtc.name ilike '%$keyword%')";

            $data = DB::select("
            select
                mtc.id,
                mtc.name as text
            from
                md_transaction_codes mtc
            where
                mtc.is_active = 1
                and
                mtc.parent_id is null
                $search
            ");

            return response()->json($data);
        } catch(\Exception $e)
        {
            return response()->json([]);
        }
    }

    public function getChildCodeSetting(Request $request)
    {
        try {     
            $parentId = $request->parent_id;
            $data = DB::select("
            select
                mtc.*,
                case
                    when
                        mtc.parent_id = 3
                    then
                        mtc.default_function
                    else
                        mtc.value
                end as value
            from
                md_transaction_codes mtc
            where
                mtc.is_active = 1
                and
                mtc.parent_id = $parentId
            ");

            return response()->json($data);
            
        } catch(\Exception $e)
        {
            return response()->json([]);
        }
    }
}

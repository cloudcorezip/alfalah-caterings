<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Ajax;


use App\Classes\RangeCriteria;
use App\Models\MasterData\Merchant;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class AjaxDashboardController
{

    private $rangeCriteria;

    public function __construct()
    {
        $this->rangeCriteria = new RangeCriteria();
    }

    public function routeWeb()
    {
        Route::prefix('ajax/dashboard')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::post('/best-seller', [static::class, 'ajaxBestSeller'])
                    ->name('merchant.ajax.dashboard.best-seller');
                Route::post('/consignment', [static::class, 'ajaxConsignment'])
                    ->name('merchant.ajax.dashboard.consignment');
                Route::post('/cashflow', [static::class, 'ajaxCashflow'])
                    ->name('merchant.ajax.dashboard.cashflow');
                Route::post('/profit-lost', [static::class, 'profitLost'])
                    ->name('merchant.ajax.dashboard.profit-lost');
                Route::post('/omzet', [static::class, 'omzet'])
                    ->name('merchant.ajax.dashboard.omzet');
                Route::post('/ap', [static::class, 'ajaxAp'])
                    ->name('merchant.ajax.dashboard.ap');
                Route::post('/ar', [static::class, 'ajaxAr'])
                    ->name('merchant.ajax.dashboard.ar');
                Route::post('/cost', [static::class, 'ajaxCost'])
                    ->name('merchant.ajax.dashboard.cost');
                Route::post('/stock', [static::class, 'ajaxStock'])
                    ->name('merchant.ajax.dashboard.stock');
                Route::post('/commission', [static::class, 'ajaxCommission'])
                    ->name('merchant.ajax.dashboard.commission');
                Route::get('/range', [static::class, 'ajaxRange'])
                    ->name('merchant.ajax.dashboard.range');
            });

    }


    public function ajaxBestSeller(Request $request)
    {

        try {
            //$merchantId=5,3,4,5
            //date YYYY-MM-DD
            $merchantId=(is_null($request->md_merchant_id) || $request->md_merchant_id=='')?merchant_id():$request->md_merchant_id;
            $period=(is_null($request->period) || $request->period=='')?'5-month-thismonth':$request->period;
            $explodePeriod=explode('-',$period);
            $filter=$this->rangeCriteria->rangeFilterDashboard($explodePeriod[2]);

            $iService=(is_null($request->is_service) || $request->is_service=='')?0:$request->is_service;
            $iConsignment=(is_null($request->is_consignment) || $request->is_consignment=='')?0:$request->is_consignment;


            $queryIsService="";
            if($iService==1){
                $queryIsService="and spc.is_with_stock=0 and spc.md_sc_product_type_id!=4";
            }

            if($iConsignment==1){
                $queryIsConsignment="and spc.is_consignment=1";
            }else{
                $queryIsConsignment="and spc.is_consignment=0";
            }

            $data=collect(DB::select("
            with product_bestseller_multi_branch as (select
              spc.id,
              spc.name,
              spc.code,
              spc.foto,
              spc.product_photos,
              coalesce(mu.name, '') as unit_name,
              coalesce(sell.qty, 0) as qty,
              coalesce(
                round(sell.price),
                0
              ) as price
            from
              sc_products spc
              join md_merchants m on spc.md_user_id = m.md_user_id
              left join md_units mu on mu.id = spc.md_unit_id
              join(
                select
                  ssod.sc_product_id,
                  coalesce(
                    sum(
                      ssod.quantity * ssod.value_conversation
                    ),
                    0
                  ) qty,
                  coalesce(
                    sum(ssod.quantity * ssod.price),
                    0
                  ) price
                from
                  sc_sale_orders sso
                  join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                where
                  sso.is_deleted = 0
                  and sso.is_editable = 1
                  and sso.is_keep_transaction = 0
                  and sso.md_merchant_id in ($merchantId)
                  and sso.md_sc_transaction_status_id not in(3, 5)
                  and ssod.is_deleted = 0
                  and ssod.is_bonus = 0
                  and sso.step_type in (0)
                  and sso.created_at :: date >= '".$filter[0]."'
                  and sso.created_at :: date <= '".$filter[1]."'
                group by
                  ssod.sc_product_id
              ) as sell on sell.sc_product_id = spc.id
            where
              m.id in ($merchantId)
              and spc.is_deleted = 0
              $queryIsService
              $queryIsConsignment
            group by
              spc.id,
              sell.qty,
              sell.price,
              mu.name
            order by
              coalesce(sell.qty, 0)
             )
             select * from product_bestseller_multi_branch order by price desc limit 5
            "));

            return response()->json($data);


        }catch (\Exception $e)
        {
            return response()->json([]);
        }
    }

    public function ajaxConsignment(Request  $request)
    {
        try {
            $merchantId="";
            foreach (get_cabang() as $j => $b){
                $merchantId.=($j==0)?$b->id:','.$b->id;
            }

            if(is_null($request->keyword) || $request->keyword == "")
            {
              $keyword = "";
            }else {
              $k = $request->keyword;
              $keyword = "and (spc.name ILIKE '%$k%' or spc.code ILIKE '%$k%')";
            }

            $data=DB::select("
            with product_consignment_multi_branch as (
              select
                spc.id,
                spc.name,
                spc.code,
                spc.foto,
                spc.product_photos,
                coalesce(mu.name, '') as unit_name,
                coalesce(sell.qty, 0) as qty,
                coalesce(
                  round(sell.price),
                  0
                ) as price,
                m.name as merchant_name
              from
                sc_products spc
                join md_merchants m on spc.md_user_id = m.md_user_id
                left join md_units mu on mu.id = spc.md_unit_id
                join(
                  select
                    ssod.sc_product_id,
                    coalesce(
                      sum(
                        ssod.quantity * ssod.value_conversion
                      ),
                      0
                    ) qty,
                    coalesce(
                      sum(ssod.quantity * ssod.price),
                      0
                    ) price
                  from
                    sc_consignment_goods sso
                    join sc_consignment_good_details ssod on sso.id = ssod.sc_consignment_good_id
                  where
                    sso.is_deleted = 0
                  group by
                    ssod.sc_product_id
                ) as sell on sell.sc_product_id = spc.id
              where
                m.id in ($merchantId)
                and spc.is_deleted = 0
                and is_consignment = 1
                $keyword
              group by
                spc.id,
                sell.qty,
                sell.price,
                mu.name,
                m.name
              order by
                coalesce(sell.qty, 0)
            )
            select
              *
            from
              product_consignment_multi_branch
            order by
              price desc
            limit
              5
            ");

            return response()->json($data);

        }catch (\Exception $e)
        {
            return response()->json([]);
        }
    }


    public function ajaxCashflow(Request  $request)
    {
        try {
            //$merchantId=1,2,3,5
            //date YYYY-MM-DD
            $merchantId=(is_null($request->md_merchant_id) || $request->md_merchant_id=='')?merchant_id():$request->md_merchant_id;
            $period=(is_null($request->period) || $request->period=='')?'7-month-lastthreemonth':$request->period;
            $explodePeriod=explode('-',$period);
            $filter=$this->rangeCriteria->rangeFilterDashboard($explodePeriod[2]);


            $start    = (new \DateTime($filter[0]))->modify('first day of this month');
            $end      = (new \DateTime($filter[1]))->modify('first day of next month');
            $interval = \DateInterval::createFromDateString('1 month');
            $period   = new \DatePeriod($start, $interval, $end);

            $listOfMonth=[];
            $listMonthName=[];
            foreach ($period as $d => $dt) {
                if($d!=0){
                    $listOfMonth[]=$dt->format('Y-m-d');
                    $listMonthName[]=Carbon::parse($dt->format('Y-m-d'))->isoFormat('MMMM Y');
                }
               }

            $data=collect(DB::select("
            with acc_cashflow_report as (select
             acd.id AS det_id,
              acd.type_coa,
              acd.name AS det_name,
              acc.md_merchant_id,
              case
              when acc.name = 'Pendapatan Usaha' then 'Pendapatan'
              when acd.name = 'Utang Saldo Deposit' then 'Pendapatan'
              when acd.name = 'Utang PPN Keluaran' then 'Pendapatan'
              when acd.name = 'Utang Pembelian Barang' then 'Beban Operasional & Usaha'
              when acc.name = 'Aktiva Tetap' then 'Pembelian/Penjualan Aset'
              when acc.name = 'Aktiva Tidak Berwujud' then 'Pembelian/Penjualan Aset'
              when acc.name = 'Investasi Jangka Pendek' then 'Investasi Lainnya'
              when acc.name = 'Investasi Jangka Panjang' then 'Investasi Lainnya'
              when acc.name = 'Piutang' then 'Pembayaran/Penerimaan Pinjaman'
              when acc.name = 'Utang' then 'Pembayaran/Penerimaan Pinjaman'
              when acc.name = 'Saldo Laba' then 'Ekuitas/Modal'
              when acc.name = 'Dividen' then 'Ekuitas/Modal'
              when acc.name = 'Modal' then 'Ekuitas/Modal'
              else acc.name end as format_name,
              COALESCE(sub.amount, 0) AS amount,
              COALESCE(sub.original, '".$filter[0]."') AS _original,
              COALESCE(sub.coa_type, acd.type_coa) AS coa_type,
              0 as is_deleted
            from acc_coa_categories acc
            join acc_coa_details acd
            on acc.id=acd.acc_coa_category_id
            left join (
                select
                  d.acc_coa_detail_id,
                  d.amount,
                  j.trans_time,
                  to_char(j.trans_time, 'YYYY-MM-DD') AS original,
                  d.coa_type
                FROM
                  acc_jurnal_details d
                  JOIN acc_jurnals j ON j.id = d.acc_jurnal_id
                WHERE
                  j.is_deleted in(0,2)
                  and j.md_merchant_id  in ($merchantId)
              ) sub ON sub.acc_coa_detail_id = acd.id
            where
            acc.md_merchant_id in ($merchantId)
            and acc.name in (
                'Pendapatan','Pendapatan Usaha','Pendapatan Lainnya',
                'Beban Operasional & Usaha', 'Beban Lainnya',
                'Aktiva Tetap', 'Aktiva Tidak Berwujud',
                'Investasi Jangka Pendek', 'Investasi Jangka Panjang',
                'Utang', 'Utang Pajak', 'Piutang',
                'Modal', 'Saldo Laba','Dividen'
              )
            and acd.is_deleted in(0,2)
            and acd.name not in (
                'Diskon Pembelian Barang', 'Utang Konsinyasi','Utang PPN Keluaran'
            )
            and acd.name not like '%Akumulasi%'
            and acd.name not like '%%Utang PPh%'
            union all
            select
              d.acc_coa_detail_id as det_id,
              d.type_coa,
              d.name as det_name,
              aj.md_merchant_id,
              'Pembayaran ke Pemasok' as format_name,
              aj.trans_amount as amount,
              to_char(aj.trans_time, 'YYYY-MM-DD') AS _original,
              d.coa_type,
              aj.is_deleted
            from
              acc_jurnals aj
              join(
                select
                  ajd.acc_jurnal_id,
                  ajd.acc_coa_detail_id,
                  acd.type_coa,
                  acd.name,
                  ajd.coa_type
                from
                  acc_jurnal_details ajd
                  join acc_coa_details acd on ajd.acc_coa_detail_id = acd.id
                where
                  acd.code like '%1.1.01%'
                  or acd.code like '%1.1.02%'
              ) d on d.acc_jurnal_id = aj.id
            where
              aj.is_deleted in(0,2)
              and aj.md_merchant_id in ($merchantId)
              and substring(aj.ref_code from 0 for 2)='P'
              or substring(aj.ref_code from 0 for 3)='RP'
              or substring(aj.ref_code from 0 for 4)='PVT'
              or substring(aj.ref_code from 0 for 4)='PPO'
              )
            select acr.*,
            case when acr.format_name='Pendapatan' then 'operasional'
            when acr.format_name='Pendapatan Lainnya' then 'operasional'
            when acr.format_name='Beban Operasional & Usaha' then 'operasional'
            when acr.format_name='Beban Lainnya' then 'operasional'
            when acr.format_name='Pembelian/Penjualan Aset' then 'investasi'
            when acr.format_name='Investasi Lainnya' then 'investasi'
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 'pendanaan'
            when acr.format_name='Ekuitas/Modal'
            then 'pendanaan'
            else 'operasional' end as format_type_name,
            case when acr.format_name='Pendapatan' then 1
            when acr.format_name='Pendapatan Lainnya' then 2
            when acr.format_name='Beban Operasional & Usaha' then 3
            when acr.format_name='Beban Lainnya' then 5
            when acr.format_name='Pembelian/Penjualan Aset' then 6
            when acr.format_name='Investasi Lainnya' then 7
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 8
            when acr.format_name='Ekuitas/Modal'
            then 9
            else 4 end as order_number,
            case when acr.format_name='Pendapatan' then 1
            when acr.format_name='Pendapatan Lainnya' then 2
            when acr.format_name='Beban Operasional & Usaha' then 3
            when acr.format_name='Beban Lainnya' then 4
            when acr.format_name='Pembelian/Penjualan Aset' then 5
            when acr.format_name='Investasi Lainnya' then 6
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 7
            when acr.format_name='Ekuitas/Modal'
            then 8
            else 9 end as format_id
            from acc_cashflow_report acr  where acr.md_merchant_id in ($merchantId) order by md_merchant_id asc
            "));




            $result2=[];
            foreach ($listOfMonth as $ll =>$l){
                $arrayOfCashflow=[];
                $start=Carbon::parse($listOfMonth[$ll])->startOfMonth()->toDateString();
                $end= Carbon::parse($listOfMonth[$ll])->endOfMonth()->toDateString();

                $result=$data->sortBy('order_number')->where('_original','<=',$end);
                $groupMerchant=$result->groupBy('md_merchant_id');
                foreach ($groupMerchant as $m => $mm) {
                    $sumThisMonth = 0;
                    $sumOfCashIn = 0;
                    $sumOfCashOut = 0;

                    foreach ($mm->groupBy('format_type_name') as $key => $item) {
                        $cashIn = 0;
                        $cashOut = 0;
                        $sumTotal = 0;
                        foreach ($item->groupBy('format_id') as $i) {
                            $subTotal = 0;
                            $total = $i->whereBetween('_original', [$start, $end])
                                ->reduce(function ($a, $b) use ($subTotal) {
                                    if ($b->coa_type == 'Debit' && $b->type_coa == 'Debit') {
                                        $subTotal += $b->amount;
                                    } elseif ($b->coa_type == 'Kredit' && $b->type_coa == 'Debit') {
                                        $subTotal -= $b->amount;

                                    } elseif ($b->coa_type == 'Kredit' && $b->type_coa == 'Kredit') {
                                        $subTotal += $b->amount;
                                    } else {
                                        $subTotal -= $b->amount;
                                    }
                                    return $a + $subTotal;
                                }, 0);
                            if ($key == 'operasional') {
                                if (strpos($i->first()->format_name, 'Beban') !== false) {
                                    $sumTotal -= abs($total);
                                    $cashOut += abs($total);
                                } else {
                                    if (strpos($i->first()->format_name, 'Pemasok') !== false) {
                                        $sumTotal -= abs($total);
                                        $cashOut += abs($total);
                                    } else {
                                        $sumTotal += $total;
                                        $cashIn += $total;
                                    }
                                }
                            } else {
                                if ($key == 'investasi') {
                                    $sumTotal -= abs($total);
                                    $cashOut += abs($total);
                                } else {
                                    $sumTotal += $total;
                                    $cashIn += $total;
                                }

                            }

                        }
                        $sumThisMonth += $sumTotal;
                        $sumOfCashIn += $cashIn;
                        $sumOfCashOut += $cashOut;
                    }


                    $arrayOfCashflow[]=[
                        'merchant_id'=>$m,
                        'in'=>round($sumOfCashIn),
                        'out'=>-1 * round($sumOfCashOut,2),
                        'netto'=>round($sumThisMonth,2)
                    ];

                }
                $result2[]=[
                    'date'=>Carbon::parse($listOfMonth[$ll])->isoFormat('MMMM Y'),
                    'data'=>$arrayOfCashflow
                ];
            }

            $params=[
                'listMonth'=>$listMonthName,
                'group'=>$result2
            ];

            return response()->json($params);

        }catch (\Exception $e)
        {
            $params=[
                'listMonth'=>[],
                'group'=>[]
            ];
            return response()->json($params);

        }

    }

    public function profitLost(Request  $request)
    {
        try {
            //$merchantId=1,2,3,5
            //date YYYY-MM-DD
            $merchantId=(is_null($request->md_merchant_id) || $request->md_merchant_id=='')?merchant_id():$request->md_merchant_id;
            $period=(is_null($request->period) || $request->period=='')?'7-month-lastthreemonth':$request->period;
            $explodePeriod=explode('-',$period);
            $filter=$this->rangeCriteria->rangeFilterDashboard($explodePeriod[2]);


            $start    = (new \DateTime($filter[0]))->modify('first day of this month');
            $end      = (new \DateTime($filter[1]))->modify('first day of next month');
            $interval = \DateInterval::createFromDateString('1 month');
            $period   = new \DatePeriod($start, $interval, $end);

            $listOfMonth=[];
            $listMonthName=[];
            foreach ($period as $d => $dt) {
                if($d!=0){
                    $listOfMonth[]=$dt->format('Y-m-d');
                    $listMonthName[]=Carbon::parse($dt->format('Y-m-d'))->isoFormat('MMMM Y');
                }
            }

            $result2=[];
            foreach ($listOfMonth as $ll =>$l){
                $start=Carbon::parse($listOfMonth[$ll])->startOfMonth()->toDateString();
                $end= Carbon::parse($listOfMonth[$ll])->endOfMonth()->toDateString();
                $query="'".$start."' :: date, '".$end."' :: date";
                $arrayOfProfitLost=[];
                $profit=DB::select("
                    with acc_profit_lost as (
                     select
                      acd.id,
                      acd.code,
                      acd.name,
                      acd.type_coa,
                      CASE WHEN acc.code in ('4.1.00', '4.2.00')
                      AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_revenue,
                      CASE WHEN acc.code in('4.1.00', '4.2.00')
                      AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_revenue_min,
                      CASE WHEN acc.code = '5.0.00'
                      AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_hpp,
                      CASE WHEN acc.code = '5.0.00'
                      AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_hpp_min,
                      CASE WHEN acc.code in ('6.1.00', '6.2.00')
                      AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_cost,
                      CASE WHEN acc.code in('6.1.00', '6.2.00')
                      AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_cost_min,
                      coalesce(j.trans_time,'".$start." 00:00:00') as trans_time,
                      acc.md_merchant_id
                    from
                      acc_coa_details acd
                      join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                      left join (
                        select
                          ajd.acc_coa_detail_id,
                          ajd.amount,
                          ajd.coa_type,
                          aj.trans_time
                        from
                          acc_jurnal_details ajd
                          join acc_jurnals aj on ajd.acc_jurnal_id = aj.id
                        WHERE
                          aj.is_deleted in (0, 2)
                          and aj.trans_time :: date  between '".$start."' and '".$end."' and aj.md_merchant_id in ($merchantId)
                      ) j on j.acc_coa_detail_id = acd.id
                    where
                      acc.md_merchant_id in($merchantId)
                      and acc.code in (
                        '4.1.00', '4.2.00', '5.0.00',
                        '6.1.00', '6.2.00'
                      )
                      and acd.is_deleted != 1
                      and acd.code not in ('4.2.00.03')
                    group by
                      acd.id,
                      acc.code,
                      j.coa_type,
                      j.trans_time,
                      acc.md_merchant_id
                    order by
                      acc.code asc
                    )
                    select
                      TO_CHAR(mon.mon, 'YYYY-MM-DD') AS original,
                      coalesce(
                        s.amount_revenue - s.amount_revenue_min,
                        0
                      ) as amount_revenue,
                      coalesce(
                        s.amount_hpp - s.amount_hpp_min, 0
                      ) as amount_hpp,
                      coalesce(
                        s.amount_cost - s.amount_cost_min,
                        0
                      ) as amount_cost,
                      s.md_merchant_id
                    from
                      generate_series($query, interval '1 month') as mon(mon)
                      left join (
                        select
                          date_trunc('Month', trans_time) as mon,
                          sum(amount_revenue) amount_revenue,
                          sum(amount_revenue_min) as amount_revenue_min,
                          sum(amount_hpp) amount_hpp,
                          sum(amount_hpp_min) as amount_hpp_min,
                          sum(amount_cost) amount_cost,
                          sum(amount_cost_min) as amount_cost_min,
                          md_merchant_id
                        from
                          acc_profit_lost
                        group by
                          mon,md_merchant_id
                      ) s on mon.mon = s.mon
                    order by
                      mon.mon asc
                    ");

                foreach (collect($profit)->groupBy('md_merchant_id') as $m =>$mm){
                    foreach ($mm->groupBy('original') as $key => $item)
                    {
                        $laba=0;
                        $pendapatan=0;
                        $biaya=0;
                        foreach ($item as $v)
                        {
                            $laba+=$v->amount_revenue-($v->amount_hpp+$v->amount_cost);
                            $pendapatan+=$v->amount_revenue;
                            $biaya+=$v->amount_hpp+$v->amount_cost;
                        }
                        $arrayOfProfitLost[]=[
                            'merchant_id'=>$m,
                            'profitLost'=>(float)$laba,
                            'revenue'=>(float)$pendapatan,
                            'cost'=>(float)-1*$biaya
                        ];
                    }

                }

                $result2[]=[
                    'date'=>Carbon::parse($listOfMonth[$ll])->isoFormat('MMMM Y'),
                    'data'=>$arrayOfProfitLost
                ];
            }

            $params=[
                'listMonth'=>$listMonthName,
                'group'=>$result2
            ];

            return response()->json($params);

        }catch (\Exception $e)
        {
            $params=[
                'listMonth'=>[],
                'group'=>[]
            ];
            return response()->json($params);


        }
    }

    public function omzet(Request  $request)
    {
        try {
            $getBranch=MerchantUtil::getBranch(merchant_id());
            $period=(is_null($request->period) || $request->period=='')?'5-month-thismonth':$request->period;
            $explodePeriod=explode('-',$period);
            $filter=$this->rangeCriteria->rangeFilterDashboard($explodePeriod[2]);
            $filterPrevious=$this->rangeCriteria->rangeFilterPreviousDashboard($explodePeriod[2],$filter[0]);
            $result=Merchant::select([
                'md_merchants.id',
                'md_merchants.name',
                DB::raw("
                coalesce(
                (select sum(sso.total) from sc_sale_orders sso
                where sso.is_deleted=0
                 and sso.is_editable = 1
                 and sso.is_keep_transaction = 0
                 and sso.md_merchant_id = md_merchants.id
                 and sso.md_sc_transaction_status_id not in(3, 5)
                 and sso.step_type in (0)
                 and sso.created_at::date >= '".$filter[0]."'
                and sso.created_at::date <= '".$filter[1]."'
                )
                ,0) as omzet_this_month,
                coalesce(
                (select sum(sso.total) from sc_sale_orders sso
                where sso.is_deleted=0
                 and sso.is_editable = 1
                 and sso.is_keep_transaction = 0
                 and sso.md_merchant_id = md_merchants.id
                 and sso.md_sc_transaction_status_id not in(3, 5)
                 and sso.step_type in (0)
                 and sso.created_at::date >= '".$filterPrevious[0]."'
                and sso.created_at::date <= '".$filterPrevious[1]."'
                )
                ,0) as omzet_previous_month
                ")
            ])->whereIn('md_merchants.id',$getBranch)->get();


            return response()->json([
                'period'=>$this->rangeCriteria->findRangeName($period,0),
                'previousPeriod'=>$this->rangeCriteria->findRangeName($period,1),
                'filter'=>$filter,
                'previousFilter'=>$filterPrevious,
                'data'=>$result
            ]);

        }catch (\Exception $e)
        {
            return response()->json([
                'period'=>'',
                'previousPeriod'=>'',
                'filter'=>[],
                'previousFilter'=>[],
                'data'=>[]
            ]);

        }
    }

    public function ajaxAp(Request  $request)
    {
        try {
            $period=(is_null($request->period) || $request->period=='')?'5-month-thismonth':$request->period;
            $explodePeriod=explode('-',$period);
            $filter=$this->rangeCriteria->rangeFilterDashboard($explodePeriod[2]);


            $merchantList="(";
            foreach (get_cabang() as $key => $b){
                $merchantList.=($key==0)?$b->id:(','.$b->id);
            }
            $merchantList.=")";

            $result=DB::select("
        with ap_report_dashboard as (
            select
              ap.id,
              coalesce(ap.second_code, ap.ap_code) as kode,
              ap.ap_name as nama_transaksi,
              ap.ap_time as waktu_transaksi,
              ap.ap_amount as jumlah,
              ap.is_paid_off as status,
              ap.due_date as jatuh_tempo,
              ap.timezone,
              m.id as merchant_id,
              m.name as cabang,
              'Utang Lainnya' as type_ap_or_ar,
              (
                case when ap.is_from_customer = 0 then (
                  select
                    name
                  from
                    sc_suppliers
                  where
                    id = ap.ref_user_id
                ) else (
                  select
                    name
                  from
                    sc_customers
                  where
                    id = ap.ref_user_id
                ) end
              ) as nama_pemberi_atau_penerima,
              ap.residual_amount,
              ap.paid_nominal
            from
              acc_merchant_ap ap
              join md_merchants as m on ap.md_merchant_id = m.id
            where
              ap.apable_id = 0
              and ap.is_paid_off = 0
              and ap.is_deleted = 0
              and m.id in $merchantList
              and ap.due_date :: date between '" . $filter[0] . "'
              and '" . $filter[1] . "'
            union
            select
              ap.id,
              coalesce(spo.second_code, spo.code) as kode,
              'Pembelian Stok Produk' as nama_transaksi,
              spo.created_at as waktu_transaksi,
              spo.total - coalesce(
                (
                  select
                    sum(srpo.total)
                  from
                    sc_retur_purchase_orders srpo
                  where
                    srpo.sc_purchase_order_id = spo.id
                    and srpo.reason_id != 1
                    and srpo.is_deleted = 0
                ),
                0
              ) as jumlah,
              ap.is_paid_off as status,
              ap.due_date as jatuh_tempo,
              ap.timezone,
              m.id as merchant_id,
              m.name as cabang,
              'Utang Pembeliaan' as type_ap_or_ar,
              s.name as nama_pemberi_atau_penerima,
              ap.residual_amount,
              ap.paid_nominal
            from
              acc_merchant_ap ap
              join md_merchants as m on ap.md_merchant_id = m.id
              join sc_purchase_orders spo on spo.id = ap.apable_id
              join sc_suppliers s on s.id = spo.sc_supplier_id
            where
              ap.apable_type = 'App\Models\SennaToko\PurchaseOrder'
              and ap.is_deleted = 0
              and spo.is_deleted = 0
              and spo.step_type = 0
              and ap.is_paid_off = 0
              and m.id in $merchantList
              and ap.due_date :: date between '" . $filter[0] . "'
              and '" . $filter[1]. "'
             )
             select * from ap_report_dashboard order by jatuh_tempo asc
            ");

            $total=0;
            $sum=collect($result)->reduce(function ($a, $b)use ($total){
                $total+=$b->residual_amount;
                return $a+$total;
            },0);


            return response()->json([
                'total'=>$sum,
                'data'=>$result,
                'filter'=>$filter
            ]);
        }catch (\Exception $e)
        {
            return response()->json([
                'total'=>0,
                'data'=>[],
                'filter'=>[],
            ]);
        }
    }

    public function ajaxAr(Request  $request)
    {
        try {
            $period=(is_null($request->period) || $request->period=='')?'5-month-thismonth':$request->period;
            $explodePeriod=explode('-',$period);
            $filter=$this->rangeCriteria->rangeFilterDashboard($explodePeriod[2]);

            $merchantList="(";
            foreach (get_cabang() as $key => $b){
                $merchantList.=($key==0)?$b->id:(','.$b->id);
            }
            $merchantList.=")";

            $result=DB::select("
        with ar_report_dashboard as (
                    select
              ar.id,
              coalesce(ar.second_code, ar.ar_code) as kode,
              ar.ar_name as nama_transaksi,
              ar.ar_time as waktu_transaksi,
              ar.ar_amount as jumlah,
              ar.is_paid_off as status,
              ar.due_date as jatuh_tempo,
              ar.timezone,
              m.id as merchant_id,
              m.name as cabang,
              'Piutang Lainnya' as type_ap_or_ar,
              (
                case when ar.is_from_supplier = 1 then (
                  select
                    name
                  from
                    sc_suppliers
                  where
                    id = ar.ref_user_id
                ) else (
                  select
                    name
                  from
                    sc_customers
                  where
                    id = ar.ref_user_id
                ) end
              ) as nama_pemberi_atau_penerima,
              ar.residual_amount,
              ar.paid_nominal
            from
              acc_merchant_ar ar
              join md_merchants m on m.id = ar.md_merchant_id
            where
              ar.is_deleted = 0
              and ar.is_paid_off = 0
              and ar.arable_id = 0
              and m.id in $merchantList
              and ar.due_date :: date between '".$filter[0]."'
              and '".$filter[1]."'
            union
            select
              ar.id,
              coalesce(sso.second_code, sso.code) as kode,
              'Piutang Penjualan' as nama_transaksi,
              sso.created_at as waktu_transaksi,
              sso.total - coalesce(
                (
                  select
                    sum(srpo.total)
                  from
                    sc_retur_sale_orders srpo
                  where
                    srpo.sc_sale_order_id = sso.id
                    and srpo.reason_id != 1
                    and srpo.is_deleted = 0
                ),
                0
              ) as jumlah,
              ar.is_paid_off as status,
              ar.due_date as jatuh_tempo,
              ar.timezone,
              m.id as merchant_id,
              m.name as cabang,
              'Piutang Penjualan' as type_ap_or_ar,
              s.name as nama_pemberi_atau_penerima,
              ar.residual_amount,
              ar.paid_nominal
            from
              acc_merchant_ar ar
              join md_merchants m on m.id = ar.md_merchant_id
              join sc_sale_orders sso on sso.id = ar.arable_id
              join sc_customers s on s.id = sso.sc_customer_id
            where
              ar.is_deleted = 0
              and ar.arable_id = sso.id
              and sso.is_deleted = 0
              and sso.step_type = 0
              and ar.is_paid_off = 0
              and ar.arable_type='App\Models\SennaToko\SaleOrder'
              and m.id in $merchantList
              and ar.due_date :: date between '".$filter[0]."'
              and '".$filter[1]."'
             )
             select * from ar_report_dashboard order by jatuh_tempo asc
            ");

            $total=0;
            $sum=collect($result)->reduce(function ($a, $b)use ($total){
                $total+=$b->residual_amount;
                return $a+$total;
            },0);


            return response()->json([
                'total'=>$sum,
                'data'=>$result,
                'filter'=>$filter
            ]);

        }catch (\Exception $e)
        {
            return response()->json([
                'total'=>0,
                'data'=>[],
                'filter'=>[]
            ]);
        }
    }

    public function ajaxCost(Request  $request)
    {
        try {
            //merchantId Single not multiple
            //$merchantId=1
            //date YYYY-MM-DD
            $merchantId=(is_null($request->md_merchant_id) || $request->md_merchant_id=='')?merchant_id():$request->md_merchant_id;
            $period=(is_null($request->period) || $request->period=='')?'5-month-thismonth':$request->period;
            $explodePeriod=explode('-',$period);
            $filter=$this->rangeCriteria->rangeFilterDashboard($explodePeriod[2]);
            $filterPrevious=$this->rangeCriteria->rangeFilterPreviousDashboard($explodePeriod[2],$filter[0]);


            $data=DB::select("

           with query_for_cost as(select
            acd.id,
            acd.name,
            coalesce(d.amount,0) as total,
            coalesce(d1.amount,0) as total_previous_month,
            m.id as merchant_id,
            m.name as merchant_name
            from
              acc_coa_details acd
              join acc_coa_categories acc on acd.acc_coa_category_id=acc.id
              join md_merchants as m on m.id = acc.md_merchant_id
              left join (
                select
                  ajd.acc_coa_detail_id,
                  coalesce(
                    sum(ajd.amount),
                    0
                  )- coalesce(
                    sum(d2.amount),
                    0
                  ) as amount
                FROM
                  acc_jurnal_details ajd
                  JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                  left join (
                    select
                      ajd.acc_coa_detail_id,
                      ajd.amount
                    FROM
                      acc_jurnal_details ajd
                      JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                    WHERE
                      j.is_deleted in (0, 2)
                      and j.md_merchant_id=$merchantId
                      and ajd.coa_type = 'Kredit'
                      and j.trans_time :: date between '".$filter[0]."'
                  and '".$filter[1]."'
                  ) as d2 on d2.acc_coa_detail_id = ajd.acc_coa_detail_id
                WHERE
                  j.is_deleted in (0, 2)
                  and j.md_merchant_id=$merchantId
                  and ajd.coa_type = 'Debit'
                  and j.trans_time :: date between '".$filter[0]."'
                  and '".$filter[1]."'
                group by
                  ajd.acc_coa_detail_id
              ) as d on d.acc_coa_detail_id = acd.id
               left join (
                select
                  ajd.acc_coa_detail_id,
                  coalesce(
                    sum(ajd.amount),
                    0
                  )- coalesce(
                    sum(d2.amount),
                    0
                  ) as amount
                FROM
                  acc_jurnal_details ajd
                  JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                  left join (
                    select
                      ajd.acc_coa_detail_id,
                      ajd.amount
                    FROM
                      acc_jurnal_details ajd
                      JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                    WHERE
                      j.is_deleted in (0, 2)
                      and j.md_merchant_id=$merchantId
                      and ajd.coa_type = 'Kredit'
                      and j.trans_time :: date between '".$filterPrevious[0]."'
                  and '".$filterPrevious[1]."'
                  ) as d2 on d2.acc_coa_detail_id = ajd.acc_coa_detail_id
                WHERE
                  j.is_deleted in (0, 2)
                  and j.md_merchant_id=$merchantId
                  and ajd.coa_type = 'Debit'
                  and j.trans_time :: date between '".$filterPrevious[0]."'
                  and '".$filterPrevious[1]."'
                group by
                  ajd.acc_coa_detail_id
              ) as d1 on d1.acc_coa_detail_id = acd.id
            where
              acc.name in ('Beban Operasional & Usaha','Beban Lainnya')
              and acd.name not in ('Beban Penyesuaian','Penyesuiaan Persediaan','Penyesuaian')
              and acc.md_merchant_id=$merchantId
              and acd.is_deleted in (0,2)
              and d.amount > 0)
              select * from query_for_cost order by total desc

            ");

            $total=0;
            $sum=collect($data)->reduce(function ($a, $b)use ($total){
                $total+=$b->total;
                return $a+$total;
            },0);

            return response()->json([
                'total'=>$sum,
                'data' => $data,
                'previousPeriod'=>$this->rangeCriteria->findRangeName($period,1)
            ]);

        }catch (\Exception $e)
        {
            return response()->json([]);

        }

    }

    public function ajaxStock(Request  $request)
    {
        try {

            //$searchby=-1 untuk awal tampil semua 1 produk 2 cabang;
            //searchKey = kode produk kalau produk dan id cabang kalau cabang
            //example search key kalau produk valuenya dari kode 1568821144 kalau cabang dari id cabang
            $searchBy=(is_null($request->search_by) || $request->search_by=='')?-1:$request->search_by;
            $searchKey=$request->search_key;

            $productType = (is_null($request->product_type))? '1, 2, 4, 3, 8':$request->product_type;

            $searchProduct="";
            $merchantList="(";
            if($searchBy=='-1' || $searchBy==-1){
                foreach (get_cabang() as $key =>$item){
                    $merchantList.=($key==0)?$item->id:','.$item->id;
                }
            }else{
                if($searchBy==2 || $searchBy=='2'){
                    $merchantList.=$searchKey;
                }else{
                    foreach (get_cabang() as $key =>$item){
                        $merchantList.=($key==0)?$item->id:','.$item->id;
                    }

                    $searchProduct="and p.code in($searchKey)";

                }
            }
            $merchantList.=")";


            if(is_null($request->keyword) || $request->keyword == "")
            {
              $keyword = "";
            }else {
              $k = $request->keyword;
              $keyword = "and (p.name ILIKE '%$k%' or p.code ILIKE '%$k%')";
            }

            $startDate=collect(DB::select("
                select created_at
                from sc_sale_orders
                where md_merchant_id in $merchantList
                order by created_at asc limit 1"));
            if(count($startDate)>0){
                $endDate=Carbon::now()->toDateTimeString();
                $calculateDiffDate=collect(DB::select("
                SELECT
                  EXTRACT(
                    YEAR
                    FROM
                      age
                  ) * 12 + EXTRACT(
                    MONTH
                    FROM
                      age
                  ) AS months_between
                FROM
                  age(
                    TIMESTAMP '".$startDate[0]->created_at."',
                    TIMESTAMP '".$endDate."'
                  ) AS t(age);
                "));
                if($calculateDiffDate[0]->months_between==0){
                    $avg=1;
                }else{
                    $avg=$calculateDiffDate[0]->months_between;
                }
            }else{
                $avg=1;
            }

            $data=DB::select("
            with query_by_stock as (
             select
              p.id,
              p.code,
              p.name,
              p.name as text,
              p.name || ' (' || p.code || ')' as nama_produk,
              p.md_sc_product_type_id,
              m.id as merchant_id,
              m.name as nama_cabang,
              coalesce(sale.quantity,0)/".$avg." as quantity,
              u.id as satuan_id,
              coalesce(ss.residual_stock, 0) as residual_stock,
              u.name as satuan,
              p.foto
            from
              sc_products p
              join md_merchants m on m.md_user_id = p.md_user_id
              left join md_units u on u.id = p.md_unit_id
              left join (
                select
                  ssi.sc_product_id,
                  sum(ssi.residual_stock) as residual_stock
                from
                  sc_stock_inventories ssi
                where
                  ssi.is_deleted = 0
                  and ssi.type = 'in'
                  and ssi.transaction_action not like '%Retur%'
                group by ssi.sc_product_id
              ) ss on ss.sc_product_id=p.id
              left join (
                select
                ssod.sc_product_id,
                sum(ssod.quantity*ssod.value_conversation) as quantity
                from sc_sale_order_details ssod
                join sc_sale_orders sso
                on ssod.sc_sale_order_id=sso.id
                where
                 sso.is_deleted = 0
                 and sso.is_editable=1
                 and sso.is_keep_transaction=0
                 and ssod.is_bonus = 0
                 and sso.md_sc_transaction_status_id not in(3, 5)
                 and sso.step_type=0
                 and sso.md_merchant_id in $merchantList
                 group by ssod.sc_product_id
              ) sale on sale.sc_product_id=p.id
            where
              p.is_deleted = 0
              and p.md_sc_product_type_id in($productType)
              and m.id in $merchantList
              $searchProduct
              $keyword
            order by
              m.id asc
            )
             select * from query_by_stock order by quantity asc limit 3
            ");
            return response()->json($data);

        }catch (\Exception $e)
        {
            return response()->json([]);
        }
    }

    public function ajaxCommission(Request  $request)
    {
        try {
            //$merchantId=1,2,3,4

            $merchantId=(is_null($request->md_merchant_id) || $request->md_merchant_id=='')?merchant_id():$request->md_merchant_id;
            // $startDate=(is_null($request->start_date)|| $request->start_date=='')?Carbon::now()->subMonth()->startOfMonth()->toDateString():$request->start_date;
            // $endDate=(is_null($request->end_date)|| $request->end_date=='')?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            $staffId=(is_null($request->staff_id) || $request->staff_id=='')?"":"and mcl.staff_id=".$request->staff_id."";
            $period=(is_null($request->period) || $request->period=='')?'5-month-thismonth':$request->period;

            $explodePeriod=explode('-',$period);
            $filter=$this->rangeCriteria->rangeFilterDashboard($explodePeriod[2]);

            $data=collect(DB::select("
              select
                  mcl.id,
                  mcl.commission_value,
                  mcl.staff_id,
                  mcl.is_paid,
                  mu.fullname,
                  mu.foto,
                  m.name as merchant_name,
                  mcl.created_at
                from
                  merchant_commission_lists mcl
                  join md_merchant_staff mms on mcl.staff_id = mms.md_user_id
                  join md_merchants m on mms.md_merchant_id = m.id
                  join md_users mu on mu.id = mms.md_user_id
                  left join sc_sale_orders sso on sso.id = mcl.sc_sale_order_id
                  left join merchant_grade_staffs g on g.id = mms.merchant_grade_id
                  left join hr_merchant_job_positions as j on j.id = mms.job_position_id
                where
                  mcl.md_merchant_id in ($merchantId)
                  and mcl.created_at::date between '$filter[0]' and '$filter[1]'
                  $staffId
            "));
            $result=[];

            foreach ($data->groupBy('staff_id') as $key =>$item)
            {
                $commissionTotalAmount=0;
                $commissionTotalAmountUnpaid=0;
                $commissionTotalAmountPaid=0;
                foreach ($item as $j =>$v){
                    $itemCommissionAmount=0;
                    $itemCommissionAmountUnpaid=0;
                    $itemCommissionAmountPaid=0;
                    if(!is_null($v->commission_value)){
                        $jsonCommission=json_decode($v->commission_value);
                        foreach ($jsonCommission as $c){
                            $itemCommissionAmount+=$c->commission_amount;
                            if($v->is_paid==0){
                                $itemCommissionAmountUnpaid+=$c->commission_amount;
                            }else{
                                $itemCommissionAmountPaid+=$c->commission_amount;
                            }
                        }
                    }
                    $commissionTotalAmount+=$itemCommissionAmount;
                    $commissionTotalAmountPaid+=$itemCommissionAmountPaid;
                    $commissionTotalAmountUnpaid+=$itemCommissionAmountUnpaid;
                }
                $firstData=$item->first();
                $result[]=(object)[
                    'staff_id'=>$firstData->staff_id,
                    'fullname'=>$firstData->fullname,
                    'foto'=>$firstData->foto,
                    'commission_amount'=>$commissionTotalAmount,
                    'paid'=>$commissionTotalAmountPaid,
                    'unpaid'=>$commissionTotalAmountUnpaid,
                    'merchant_name'=>$firstData->merchant_name,
                ];
            }


            return response()->json($result);

        }catch (\Exception $e)
        {
            return response()->json([]);

        }
    }

    public function ajaxRange()
    {
        try {

            return response()->json($this->rangeCriteria->getListRange());
        }catch (\Exception $e)
        {
            return response()->json([]);


        }
    }




}

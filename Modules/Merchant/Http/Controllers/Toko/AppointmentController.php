<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\SennaToko\ClinicReservation;
use App\Models\MasterData\Merchant;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('appointment')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.appointment.index');
                Route::post('/detail', [static::class, 'detail'])
                    ->name('merchant.toko.appointment.detail');
                Route::post('/get-data', [static::class, 'getData'])
                    ->name('merchant.toko.appointment.get-data');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $merchant = Merchant::find(merchant_id());
        $params=[
            'title'=>'Jadwal Kalender',
            'data' => [],
            'merchant' => $merchant,
            'startDate' => Carbon::now()->startOfMonth()->toDateString(),
            'endDate' => Carbon::now()->endOfMonth()->toDateString()
        ];

        return view('merchant::toko.appointment.index',$params);

    }

    public function detail(Request $request)
    {
        try {
            $id = $request->id;

            $data = ClinicReservation::with(['getMerchant', 'getCustomer', 'getProduct', 'getStaff.getUser'])
                        ->where('is_deleted', 0)
                        ->where('id', $id)
                        ->first();
            $params = [
                "title" => "Detail",
                "data" => $data,
                'maritalStatus'=> generateMaritalStatus(),
                'religion' => generateReligion()
            ];
            
            return view('merchant::toko.appointment.detail', $params);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return abort(404);
        }
    }


    public function getData(Request $request)
    {
        try {

            $startDate = is_null($request->startDate)? Carbon::now()->startOfMonth()->toDateString():$request->startDate;
            $endDate = is_null($request->endDate)? Carbon::now()->endOfMonth()->toDateString():$request->endDate;

            $data = ClinicReservation::
            select([
                'clinic_reservations.id',
                'clinic_reservations.reservation_number',
                'clinic_reservations.reservation_date',
                'clinic_reservations.status',
                'sc.code as customer_code',
                'sc.name as customer_name',
                'sp.name as service_name',
                'u.fullname as doktor_name'
            ])
                ->join('sc_customers as sc', 'sc.id', 'clinic_reservations.sc_customer_id')
                ->join('sc_products as sp', 'sp.id', 'clinic_reservations.sc_product_id')
                ->join('md_merchant_staff as mms', 'mms.id', 'clinic_reservations.md_merchant_staff_id')
                ->join('md_users as u', 'u.id', 'mms.md_user_id')
                ->where('clinic_reservations.is_deleted', 0)
                ->where('clinic_reservations.md_merchant_id', merchant_id())
                ->orderBy('clinic_reservations.id','desc')
                // ->whereRaw("clinic_reservations.reservation_date::date between '$startDate' and '$endDate'")
                ->get();

            return $data;
        } catch(\Exception $e)
        {
            return [];
        }
    }
}

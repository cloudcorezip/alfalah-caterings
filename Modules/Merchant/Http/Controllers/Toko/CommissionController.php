<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use App\Models\SennaToko\Product;
use App\Models\MasterData\Merchant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\CashDrawerEntity;
use Modules\Merchant\Entities\Toko\CommissionEntity;
use Modules\Merchant\Entities\Toko\DiscountProductEntity;
use Ramsey\Uuid\Uuid;
use App\Utils\Merchant\MerchantUtil;

class CommissionController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('commission')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\CommissionController@index')
                    ->name('merchant.toko.commission.index');
                Route::post('/data-table', 'Toko\CommissionController@dataTable')
                    ->name('merchant.toko.commission.datatable');
                Route::get('/add', 'Toko\CommissionController@add')
                    ->name('merchant.toko.commission.add');
                Route::get('/edit', 'Toko\CommissionController@edit')
                    ->name('merchant.toko.commission.edit');
                Route::post('/save', 'Toko\CommissionController@save')
                    ->name('merchant.toko.commission.save');
                Route::post('/delete', 'Toko\CommissionController@delete')
                    ->name('merchant.toko.commission.delete');
                Route::post('/reload-data', 'Toko\CommissionController@reloadData')
                    ->name('merchant.toko.commission.reload-data');
                Route::get('/get-employee', 'Toko\CommissionController@getEmployee')
                    ->name('merchant.toko.commission.get-employee');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Komisi',
            'tableColumns'=>CommissionEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'add'=>(get_role()==3)?CommissionEntity::add(true):
                CommissionEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.commission.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return CommissionEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){
            $data=CommissionEntity::find($id);

        }else{
            $data=new CommissionEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'product'=>Product::where('md_user_id',user_id())
                ->where('is_deleted', 0)
                ->get(),
        ];


        return view('merchant::toko.commission.form2',$params);
    }

    public function getEmployee(Request  $request)
    {
        $data=MerchantStaff::
        select(($request->is_non_employee==0)?[
            'u.id',
            DB::raw("  u.fullname || ' ' || coalesce( ' [' || p.name || '] ','') || '[' || m.name || '] ' as text"),
        ]:[
            'u.id',
            'u.fullname as text'
        ]
        )
            ->whereIn('md_merchant_staff.md_merchant_id', MerchantUtil::getBranch($request->md_merchant_id, 1))
            ->leftJoin('md_users as u','u.id','md_merchant_staff.md_user_id')
            ->join('md_roles as r','r.id','u.md_role_id')
            ->join('md_merchants as m', 'm.id', 'md_merchant_staff.md_merchant_id')
            ->leftJoin('hr_merchant_job_positions  as p','p.id','md_merchant_staff.job_position_id')
            ->where('md_merchant_staff.is_non_employee',$request->is_non_employee)
            ->where('md_merchant_staff.is_deleted',0)
            ->get();

        return response()->json($data);

    }

    public  function edit(Request $request){

        $id=$request->id;
        if(!is_null($id)){
            $data=CommissionEntity::find($id);

        }else{
            $data=new CommissionEntity();
        }

        $productListIds = (!is_null($data->product_list))? array_column(json_decode($data->product_list), "id"):[];
        $productList = Product::select([
            "sc_products.id",
            "sc_products.code",
            "sc_products.name",
            "m.id as md_merchant_id",
            "m.name as merchant_name"
        ])
            ->join("md_users as u", "u.id", "sc_products.md_user_id")
            ->join("md_merchants as m", "m.md_user_id", "u.id")
            ->where("is_deleted", 0)
            ->whereIn("sc_products.id", $productListIds)
            ->get();

        if($data->is_for_employee==0){


            $otherStaffListIds = (!is_null($data->staff_list))? array_column(json_decode($data->staff_list), "id"):[];
            $staffListIds=[];
            $staffList=[];

            $otherStaffList = MerchantStaff::select([
                'u.id',
                'u.fullname'
            ])
                ->join("md_users as u", "u.id", "md_merchant_staff.md_user_id")
                ->join("md_roles as r", "r.id", "u.md_role_id")
                ->join('md_merchants as m', 'm.id', 'md_merchant_staff.md_merchant_id')
                ->whereIn("md_merchant_id", MerchantUtil::getBranch(merchant_id(), 1))
                ->where("md_merchant_staff.is_deleted", 0)
                ->whereIn("u.id", $otherStaffListIds)
                ->get();
        }else{

            $staffListIds = (!is_null($data->staff_list))? array_column(json_decode($data->staff_list), "id"):[];

            $staffList = MerchantStaff::select([
                'u.id',
                'u.fullname',
                'm.name as merchant_name'
            ])
                ->join("md_users as u", "u.id", "md_merchant_staff.md_user_id")
                ->join("md_roles as r", "r.id", "u.md_role_id")
                ->join('md_merchants as m', 'm.id', 'md_merchant_staff.md_merchant_id')
                ->whereIn("md_merchant_id", MerchantUtil::getBranch(merchant_id(), 1))
                ->where("md_merchant_staff.is_deleted", 0)
                ->whereIn("u.id", $staffListIds)
                ->get();
            $otherStaffListIds=[];
            $otherStaffList=[];
        }


        $branch = (is_null(json_decode($data->assign_to_branch)))? []:json_decode($data->assign_to_branch);

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'product'=>Product::where('md_user_id',user_id())
                ->where('is_deleted', 0)
                ->get(),
            'productListIds'=>$productListIds,
            'productList' => $productList,
            'staffListIds' => $staffListIds,
            'staffList' => $staffList,
            'otherStaffList'=>$otherStaffList,
            'otherStaffListId'=>$otherStaffListIds,
            'branch' => $branch
        ];


        return view('merchant::toko.commission.form2-edit',$params);
    }



    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $merchantId = merchant_id();

            if(!is_null($id)){

                $data=CommissionEntity::find($id);
            }else{
                $data=new CommissionEntity();
            }

            $isForEmployee=$request->is_for_employee;
            $requestProduct = json_decode($request->product_list);

            if($isForEmployee==0){
                $requestStaff=json_decode($request->other_staff_list);
            }else{
                $requestStaff=json_decode($request->staff_list);
            }

            if(is_null($data->id)){
                $validator = Validator::make($request->all(), $data->create);
                if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    return response()->json([
                        'message'=>$error,
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }else{
                $validator = Validator::make($request->all(), $data->update);
                if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    return response()->json([
                        'message'=>$error,
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }


            if($request->type == "product" && count($requestProduct) < 1)
            {
                return response()->json([
                    'message'=>'Silahkan pilih produk terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            if(count($requestStaff) < 1)
            {
                return response()->json([
                    'message'=>'Silahkan pilih karyawan atau penerima lainnya terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            if($request->is_all_branch == 1){
                $tmpBranch = get_cabang();
                $branch = [];
                foreach($tmpBranch as $k => $i){
                    if($i->id != $merchantId){
                        array_push($branch, $i->id);
                    }
                }
            } else {
                $branch = json_decode($request->branch);
            }

            $queryMerchantId = $branch;
            array_unshift($queryMerchantId, $merchantId);

            if($request->type == "product")
            {
                $productIds = [];
                $productCode = [];

                foreach($requestProduct as $key => $item)
                {
                    array_push($productIds, $item->id);
                }
                $product = Product::whereIn('id', $productIds)->get();
                foreach($product as $key => $item)
                {
                    array_push($productCode, $item->code);
                }
                $otherMerchant = Merchant::whereIn("id",$queryMerchantId)
                                    ->get()
                                    ->toArray();


                $checkProductOtherMerchant = Product::select([
                    "sc_products.id",
                    "sc_products.name",
                    "sc_products.code",
                    "m.id as md_merchant_id",
                    "m.name as merchant_name"
                ])
                ->join("md_users as u", "u.id", "sc_products.md_user_id")
                ->join("md_merchants as m", "m.md_user_id", "u.id")
                ->whereIn('sc_products.code', $productCode)
                ->whereIn("m.id", $queryMerchantId)
                ->where("sc_products.is_deleted", 0)
                ->get()
                ->toArray();

                foreach($queryMerchantId as $branchId)
                {
                    $productByMerchant = array_values(array_filter($checkProductOtherMerchant, function($val) use($branchId){
                        return $val["md_merchant_id"] == $branchId;
                    }));

                    $filterMerchant = array_values(array_filter($otherMerchant, function($val) use($branchId){
                        return $val["id"] == $branchId;
                    }));

                    $currentMerchant = array_shift($filterMerchant);

                    foreach($checkProductOtherMerchant as $key => $item)
                    {
                        if(!in_array($item["code"], array_column($productByMerchant, "code")))
                        {
                            return response()->json([
                                'message'=>"Produk ".$item["name"]." tidak tersedia di cabang ".$currentMerchant["name"],
                                'type'=>'warning',
                                'is_modal'=>false,
                                'redirect_url'=>''
                            ]);
                        }
                    }
                }

                $inMerchantId = (count($queryMerchantId) < 1)?"":"and m.id in(".implode(',', $queryMerchantId).")";
                $inProductCode = (count($queryMerchantId) < 1)?"":"and sp.code in('".implode( "', '", $productCode)."')";
                $whereCommisionId = is_null($id)? '':"and mc.id != $id";
                $inUserId = (count($requestStaff) < 1)?"":"and u.id in(".implode(',',array_column($requestStaff, 'id')).")";

                $checkStaff = DB::select("
                select
                    mc.id,
                    u.fullname,
                    sp.id as product_id,
                    sp.name as product_name,
                    m.name as merchant_name,
                    mcb.name as created_by_merchant
                from
                    merchant_commissions mc
                join lateral
                    JSONB_ARRAY_ELEMENTS(mc.product_list) as p(v) ON TRUE
                join lateral
                    JSONB_ARRAY_ELEMENTS(mc.staff_list) as s(v) ON TRUE
                join
                    md_merchants as mcb ON mcb.id = mc.md_merchant_id
                join
                    sc_products sp ON (p.v->>'id')::text::int = sp.id
                join
                    md_users as u ON (s.v->>'id')::text::int = u.id
                join
                    md_merchants as m ON m.md_user_id = sp.md_user_id
                where
                    mc.is_deleted = 0
                    and
                    mc.status = 1
                    $inMerchantId
                    $inProductCode
                    $inUserId
                    $whereCommisionId
                ");

                if(count($checkStaff) > 0){
                    return response()->json([
                        'message'=>'Komisi untuk '.$checkStaff[0]->fullname.' dengan produk '.$checkStaff[0]->product_name.' sudah tersedia di cabang '.$checkStaff[0]->merchant_name,
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }

                $data->product_list = json_encode($checkProductOtherMerchant);
            } else {
                $data->product_list = null;
            }

            $value=is_null($request->value)?[]:$request->value;
            $rules=is_null($request->value_other)?[]:$request->value_other;
            $valueExist=is_null($request->value_exist)?[]:$request->value_exist;
            $rulesExist=is_null($request->value_other_exist)?[]:$request->value_other_exist;
            $typeOfValueCommissions=$request->type_of_value_commissions;
            $valueType=$request->value_type;
            $type=$request->type;

            $valueCommission=[];

            if(!is_null($data->id)){
                $typeOfValueCommissionsExist=$data->type_of_value_commissions;
                $valueTypeExists=$data->value_type;
                $typeExists=$data->type;
                if($typeOfValueCommissions==$typeOfValueCommissionsExist && $valueType==$valueTypeExists && $type==$typeExists){

                    if($typeOfValueCommissions=='tiered'){
                        if(count($value)==0 && count($valueExist)==0){
                            return response()->json([
                                'message'=>'Nilai atau ketentuan komisi harus diisi!',
                                'type'=>'warning',
                                'is_modal'=>false,
                                'redirect_url'=>''
                            ]);
                        }
                    }elseif ($typeOfValueCommissions=='multiple' || $typeOfValueCommissions=='flat'){
                        if(count($valueExist)==0){
                            return response()->json([
                                'message'=>'Nilai atau ketentuan komisi harus diisi!',
                                'type'=>'warning',
                                'is_modal'=>false,
                                'redirect_url'=>''
                            ]);
                        }
                    }
                    if($typeOfValueCommissions=='tiered' || $typeOfValueCommissions=='multiple'){
                        if($request->value_type=='nominal'){
                            if(count($value)>0){
                                foreach ($value as $j =>$v){
                                    if($request->type=='product'){
                                        $amount=strtr($value[$j], array('.' => '', ',' => '.'));
                                        array_push($valueCommission,[
                                            'rules'=>$rules[$j],
                                            'value'=>$amount
                                        ]);
                                    }else{
                                        $amount=strtr($value[$j], array('.' => '', ',' => '.'));
                                        $rulesAmount=strtr($rules[$j], array('.' => '', ',' => '.'));
                                        array_push($valueCommission,[
                                            'rules'=>$rulesAmount,
                                            'value'=>$amount
                                        ]);
                                    }

                                }
                            }

                            if(count($valueExist)>0){
                                foreach ($valueExist as $j =>$v){
                                    if($request->type=='product'){
                                        $amount=strtr($valueExist[$j], array('.' => '', ',' => '.'));
                                        array_push($valueCommission,[
                                            'rules'=>$rulesExist[$j],
                                            'value'=>$amount
                                        ]);
                                    }else{
                                        $amount=strtr($valueExist[$j], array('.' => '', ',' => '.'));
                                        $rulesAmount=strtr($rulesExist[$j], array('.' => '', ',' => '.'));
                                        array_push($valueCommission,[
                                            'rules'=>$rulesAmount,
                                            'value'=>$amount
                                        ]);
                                    }

                                }
                            }

                        }else{
                            if(count($value)>0){
                                foreach ($value as $j =>$v){
                                    if($request->type=='product'){
                                        array_push($valueCommission,[
                                            'rules'=>$rules[$j],
                                            'value'=>$value[$j]
                                        ]);
                                    }else{
                                        $rulesAmount=strtr($rules[$j], array('.' => '', ',' => '.'));
                                        array_push($valueCommission,[
                                            'rules'=>$rulesAmount,
                                            'value'=>$value[$j]
                                        ]);
                                    }

                                }
                            }

                            if(count($valueExist)>0)
                            {
                                if($request->type=='product') {
                                    foreach ($valueExist as $j =>$v){
                                        array_push($valueCommission,[
                                            'rules'=>$rulesExist[$j],
                                            'value'=>$valueExist[$j]
                                        ]);
                                    }
                                }else{
                                    foreach ($valueExist as $j =>$v){
                                        $rulesAmount=strtr($rulesExist[$j], array('.' => '', ',' => '.'));
                                        array_push($valueCommission,[
                                            'rules'=>$rulesAmount,
                                            'value'=>$valueExist[$j]
                                        ]);
                                    }
                                }

                            }

                        }
                    }else{
                        foreach ($valueExist as $j =>$v){
                            if($request->value_type=='nominal'){
                                $amount=strtr($valueExist[$j], array('.' => '', ',' => '.'));
                                array_push($valueCommission,[
                                    'rules'=>0,
                                    'value'=>$amount
                                ]);
                            }else{
                                array_push($valueCommission,[
                                    'rules'=>0,
                                    'value'=>$valueExist[$j]
                                ]);
                            }

                        }
                    }

                }else{
                    if(count($value)==0){
                        return response()->json([
                            'message'=>'Nilai atau ketentuan komisi harus diisi!',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>''
                        ]);
                    }

                    if($request->type_of_value_commissions=='multiple' || $request->type_of_value_commissions=='tiered'){
                        if($request->value_type=='nominal'){
                            foreach ($value as $j =>$v){
                                if($request->type=='product'){
                                    $amount=strtr($value[$j], array('.' => '', ',' => '.'));
                                    array_push($valueCommission,[
                                        'rules'=>$rules[$j],
                                        'value'=>$amount
                                    ]);
                                }else{
                                    $amount=strtr($value[$j], array('.' => '', ',' => '.'));
                                    $rulesAmount=strtr($rules[$j], array('.' => '', ',' => '.'));
                                    array_push($valueCommission,[
                                        'rules'=>$rulesAmount,
                                        'value'=>$amount
                                    ]);
                                }

                            }
                        }else{
                            foreach ($value as $j =>$v){
                                if($request->value_type=='product'){
                                    array_push($valueCommission,[
                                        'rules'=>$rules[$j],
                                        'value'=>$value[$j]
                                    ]);
                                }else{
                                    $rulesAmount=strtr($rules[$j], array('.' => '', ',' => '.'));
                                    array_push($valueCommission,[
                                        'rules'=>$rules[$j],
                                        'value'=>$rulesAmount
                                    ]);
                                }

                            }
                        }
                    }else{
                        foreach ($value as $j =>$v){
                            if($request->value_type=='nominal'){
                                $amount=strtr($value[$j], array('.' => '', ',' => '.'));
                                array_push($valueCommission,[
                                    'rules'=>0,
                                    'value'=>$amount
                                ]);
                            }else{
                                array_push($valueCommission,[
                                    'rules'=>0,
                                    'value'=>$value[$j]
                                ]);
                            }

                        }
                    }
                }
            }else{
                if(count($value)<1){
                    return response()->json([
                        'message'=>'Nilai atau ketentuan komisi harus diisi!',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }
                if($request->type_of_value_commissions=='multiple' || $request->type_of_value_commissions=='tiered'){
                    if($request->value_type=='nominal'){
                        foreach ($value as $j =>$v){
                            if($request->type=='product'){
                                $amount=strtr($value[$j], array('.' => '', ',' => '.'));
                                array_push($valueCommission,[
                                    'rules'=>$rules[$j],
                                    'value'=>$amount
                                ]);
                            }else{
                                $amount=strtr($value[$j], array('.' => '', ',' => '.'));
                                $rulesAmount=strtr($rules[$j], array('.' => '', ',' => '.'));
                                array_push($valueCommission,[
                                    'rules'=>$rulesAmount,
                                    'value'=>$amount
                                ]);
                            }

                        }
                    }else{
                        foreach ($value as $j =>$v){
                            if($request->type=='product'){
                                array_push($valueCommission,[
                                    'rules'=>$rules[$j],
                                    'value'=>$value[$j]
                                ]);
                            }else{
                                $rulesAmount=strtr($rules[$j], array('.' => '', ',' => '.'));
                                array_push($valueCommission,[
                                    'rules'=>$rulesAmount,
                                    'value'=>$value[$j]
                                ]);
                            }

                        }
                    }
                }else{
                    foreach ($value as $j =>$v){
                        if($request->value_type=='nominal'){
                            $amount=strtr($value[$j], array('.' => '', ',' => '.'));
                            array_push($valueCommission,[
                                'rules'=>0,
                                'value'=>$amount
                            ]);
                        }else{
                            array_push($valueCommission,[
                                'rules'=>0,
                                'value'=>$value[$j]
                            ]);
                        }

                    }
                }

            }


            $data->name = $request->name;
            $data->type = $request->type;
            $data->value_type = $request->value_type;
            $data->type_of_value_commissions=$request->type_of_value_commissions;
            $data->value = 0;
            $data->staff_list = ($isForEmployee==0)?$request->other_staff_list:$request->staff_list;
            $data->md_merchant_id = $merchantId;
            $data->is_for_employee=$request->is_for_employee;
            $data->status = $request->status;
            $data->is_manual = $request->is_manual;
            $data->value_commissions=json_encode($valueCommission);
            $data->min_transaction = strtr($request->min_transaction, array('.' => '', ',' => '.'));

            $data->is_all_branch = $request->is_all_branch;

            if(count($branch) > 0){
                $data->assign_to_branch = json_encode($branch);
            } else {
                $data->assign_to_branch = null;
            }

            $data->save();
            DB::commit();
            return response()->json([
                'message'=>'Group komisi berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.commission.index')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=CommissionEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            DB::commit();
            return response()->json([
                'message'=>'Group komisi berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }

    public function reloadData(Request $request)
    {

        $searchParams = [];
        foreach (CommissionEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Komisi',
            'searchKey' => $searchKey,
            'tableColumns'=>CommissionEntity::dataTableColumns()
        ];
        return view('merchant::toko.commission.list',$params);

    }
}

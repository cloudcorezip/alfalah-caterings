<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\DigitalMenu\StoreMerchantConfig;
use App\Models\SennaToko\DigitalMenu\StoreMerchantDetail;
use App\Models\SennaToko\Product;
use App\Models\MasterData\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use QrCode;
use PDF;

class StoreController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('digital-menu')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/{id}', 'Toko\StoreController@index')
                    ->name('merchant.toko.digital-menu.index');
                Route::post('/generate/{id}', 'Toko\StoreController@generate')
                    ->name('merchant.toko.digital-menu.generate');
                Route::post('/save-order-type', 'Toko\StoreController@saveOrderAndPayment')
                    ->name('merchant.toko.digital-menu.save-order-type');
                Route::post('/save-to-catalog', 'Toko\StoreController@saveToCatalog')
                    ->name('merchant.toko.digital-menu.save-to-catalog');
                Route::post('/proses-catalog', 'Toko\StoreController@prosesCatalog')
                    ->name('merchant.toko.digital-menu.proses-catalog');
                Route::get('/download-qr/{id}', 'Toko\StoreController@downloadQr')
                    ->name('merchant.toko.digital-menu.download-qr');

            });
    }

    public function index($id,Request  $request)
    {

        $data=StoreMerchantConfig::where('md_merchant_id',$id)
            ->first();

        $params=[
            'title'=>'Pengaturan Katalog Digital',
            'data'=>$data,
            'merchant' => Merchant::find(merchant_id()),
            'dataProduct'=>(is_null($data))?[]:$this->dataProduct()->paginate(8),
            'dataCatalog'=>(is_null($data))?[]:$this->dataCatalog()->paginate(8,['*'], 'catalog')

        ];

        return view('merchant::toko.digital-menu.index',$params);


    }

    public function saveOrderAndPayment(Request  $request)
    {
        try {
            $id=$request->id;
            $data=StoreMerchantConfig::find($id);

            $data->order_type=$request->order_type;
            if($request->order_type==2){

                $data->payment_type=json_encode($request->payment_type);

            }
            $data->is_active=1;
            $data->tax_percentage=$request->tax_percentage;
            $data->save();
            return "<div class='alert alert-success' style='text-align: center'>Data berhasil disimpan!</div> <script>
           reload(1500)
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";

        }
    }

    public function generate($id,Request  $request)
    {
        try {
            $data=StoreMerchantConfig::where('md_merchant_id',$id)
                ->first();
            if(is_null($data))
            {
             $data=new StoreMerchantConfig();
            }

            $destinationPath = 'public/uploads/digital-menu/merchant/'.$id.'/file/';
            $code=CodeGenerator::codeGenerator(6);
            $check=StoreMerchantConfig::where('unique_code',$code)->first();
            if(is_null($check))
            {
                $code=CodeGenerator::codeGenerator(6);

            }
            if(env('APP_ENV')!='production')
            {
                $link='http://localhost:3100/'.$code;

            }else{
                $link='https://store.senna.co.id/'.$code;

            }
            $qrCodeFile=$destinationPath.date('YmdHis').'.png';
            $image=QrCode::format('png')->size(400)->generate($link);
            Storage::disk('s3')->put($qrCodeFile,$image);
            $data->unique_code=$code;
            $data->qr_code=$qrCodeFile;
            $data->url=$link;
            $data->order_type=1;
            $data->md_merchant_id=$id;
            $data->save();
            return "<div class='alert alert-success' style='text-align: center'>Data berhasil disimpan!</div> <script>
           reload(1500)
</script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }

    private  function dataProduct()
    {
        return Product::
        select([
            DB::raw('ROW_NUMBER () OVER (ORDER BY sc_products.id)'),
            'sc_products.id',
            'sc_products.code as kode_produk',
            'sc_products.name as nama_produk',
            'sc_products.foto as foto_produk',
            'c.name as nama_kategori_produk',
            'c.id as category_id',
            'sc_products.stock as stok',
            'sc_products.selling_price as harga',
            'uu.name as nama_satuan'
        ])
            ->join('md_units as uu','uu.id','sc_products.md_unit_id')
            ->join('md_users as u','u.id','sc_products.md_user_id')
            ->join('md_merchants as m','m.md_user_id','u.id')
            ->join('sc_product_categories as c','c.id','sc_products.sc_product_category_id')
            ->leftJoin('store_merchant_details as smd','smd.sc_product_id','sc_products.id')
            ->where('m.id',merchant_id())
            ->where('sc_products.is_deleted', 0)
            ->where('sc_products.is_with_stock', 1)
            ->whereNull('smd.sc_product_id')
            ->orderBy('sc_products.id','desc')
        ;
    }

    private  function dataCatalog()
    {
        return Product::
        select([
            DB::raw('ROW_NUMBER () OVER (ORDER BY sc_products.id)'),
            'sc_products.id',
            'sc_products.code as kode_produk',
            'sc_products.name as nama_produk',
            'sc_products.foto as foto_produk',
            'c.name as nama_kategori_produk',
            'c.id as category_id',
            'sc_products.stock as stok',
            'sc_products.selling_price as harga',
            'uu.name as nama_satuan',
            'smd.is_show',
            'smd.stock',
            'smd.id as catalog_id',
            'smd.discount_percentage as diskon'
        ])
            ->join('md_units as uu','uu.id','sc_products.md_unit_id')
            ->join('md_users as u','u.id','sc_products.md_user_id')
            ->join('md_merchants as m','m.md_user_id','u.id')
            ->join('sc_product_categories as c','c.id','sc_products.sc_product_category_id')
            ->join('store_merchant_details as smd','smd.sc_product_id','sc_products.id')
            ->where('m.id',merchant_id())
            ->orderBy('sc_products.id','asc')
            ;
    }

    public function saveToCatalog(Request  $request)
    {
        try {
            $ids=explode(',',$request->ids);
            $detail=[];
            foreach ($ids as $item)
            {
                $v=explode('_',$item);
                $detail[]=[
                    'sc_product_id'=>$v[0],
                    'sc_product_category_id'=>$v[1],
                    'store_merchant_config_id'=>$request->config_id,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                ];
            }
            StoreMerchantDetail::insert($detail);
            return "<div class='alert alert-success' style='text-align: center'>Data produk berhasil dimasukkan ke katalog!</div> <script>
           reload(1500)
</script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }

    public function prosesCatalog(Request  $request)
    {
        try {
            if($request->action==1)
            {
                $message='Data berhasil dihapus dari katalog';
                DB::select("delete from store_merchant_details where id in($request->ids)");
            }elseif($request->action==2){
                $message='Data berhasil disembunyikan dari katalog';
                DB::select("update store_merchant_details set is_show=0 where id in($request->ids)");

            }elseif($request->action==3){
                $message='Data berhasil ditampilkan dikatalog';
                DB::select("update store_merchant_details set is_show=1 where id in($request->ids)");

            }else{
                $message='Pengaturan diskon berhasil';
                DB::select("update store_merchant_details set discount_percentage=$request->discount where id in($request->ids)");

            }

            return "<div class='alert alert-success' style='text-align: center'>$message</div> <script>
           reload(1500)
</script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }

    public function downloadQr($id, Request $request){
        try{
            $data=StoreMerchantConfig::where('md_merchant_id',$id)->first();
            $merchant = Merchant::find(merchant_id());

            $pdf = PDF::loadview('merchant::toko.digital-menu.qr-pdf',["data"=>$data, "merchant" => $merchant])->setPaper('a5');
            return $pdf->stream('qr_code-'.$data->md_merchant_id.'-'.date('Y-m-d').'.pdf');

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return redirect()->back()->with(['error-download'=> "Terjadi Kesalahan di Server, Data Gagal di download"]);
        }
    }
}

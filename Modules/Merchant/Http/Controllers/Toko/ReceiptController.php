<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaMapping;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\Unit;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductionOfGoodDetail;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Merchant\ProductUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\Receipt\ReceiptEntity;
use Modules\Merchant\Entities\Toko\Receipt\ReceiptItemEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Ramsey\Uuid\Uuid;

class ReceiptController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/receipt')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/',  [static::class, 'index'])
                    ->name('merchant.toko.master-data.receipt.index');
                Route::get('/add',  [static::class, 'add'])
                    ->name('merchant.toko.master-data.receipt.add');
                Route::get('/detail/{id}', [static::class, 'detail'])
                    ->name('merchant.toko.master-data.receipt.detail');
                Route::post('/data-table',  [static::class, 'dataTable'])
                    ->name('merchant.toko.master-data.receipt.datatable');
                Route::post('/save',  [static::class, 'save'])
                    ->name('merchant.toko.master-data.receipt.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.master-data.receipt.delete');

                Route::post('/add-item',  [static::class, 'addItem'])
                    ->name('merchant.toko.master-data.receipt.add-item');
                Route::post('/data-table-item',  [static::class, 'dataTableItem'])
                    ->name('merchant.toko.master-data.receipt.datatable-item');
                Route::post('/save-item',  [static::class, 'saveItem'])
                    ->name('merchant.toko.master-data.receipt.save-item');
                Route::post('/delete-item', [static::class, 'deleteItem'])
                    ->name('merchant.toko.master-data.receipt.delete-item');

                Route::post('/add-cost',  [static::class, 'addCost'])
                    ->name('merchant.toko.master-data.receipt.add-cost');
                Route::post('/save-cost',  [static::class, 'saveCost'])
                    ->name('merchant.toko.master-data.receipt.save-cost');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Resep',
            'tableColumns'=>ReceiptEntity::dataTableColumns(),
            'add'=>ReceiptEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.receipt.index',$params);
    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ReceiptEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=2 || get_role()!=3)?false:true);
    }

    public function dataTableItem(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ReceiptItemEntity::dataTable($request,$request->id);
    }

    public function add(Request $request)
    {
        $id=$request->id;
        if(is_null($id))
        {
            $data=new ReceiptEntity();
        }else{
            $data=ReceiptEntity::find($id);
        }
        $product=Product::where('md_user_id',user_id())
            ->where('is_deleted',0)
            ->where('md_sc_product_type_id','!=',4)
            ->get();
        $warehouse=WarehouseEntity::where('md_merchant_id',merchant_id())
            ->where('is_deleted',0)
            ->get();
        $merchant=Merchant::find(merchant_id());
        $params=[
            'title'=>(is_null($id))?'Tambah Data ':'Edit Data',
            'product'=>$product,
            "code" => CodeGenerator::generalCode('PR',merchant_id()),
            'warehouse'=>$warehouse,
            'data'=>$data,
            'merchant'=>$merchant,
        ];

        return view("merchant::toko.master-data.receipt.form", $params);
    }

    public function detail(Request $request)
    {
        $id=$request->id;
        $data=Product::find($id);

        if(is_null($data)){
            abort(404);
        }

        $product=Product::where('md_user_id',user_id())
            ->where('is_deleted',0)
            ->get();
        $warehouse=WarehouseEntity::where('md_merchant_id',merchant_id())
            ->where('is_deleted',0)
            ->get();
        $merchant=Merchant::find(merchant_id());
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'product'=>$product,
            "code" => 'PR'.merchant_id().rand(0,100000).date('Ymd'),
            'warehouse'=>$warehouse,
            'data'=>$data,
            'merchant'=>$merchant,
            'tableColumnsItem'=>ReceiptItemEntity::dataTableColumns(),
            'addItem'=> ReceiptEntity::add($data->id),
            'cost_other'=> CoaDetail::select([
                'acc_coa_details.id',
                'acc_coa_details.name as text'
            ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchant->id)
                ->where('c.code','6.1.00')
                ->whereIn('acc_coa_details.is_deleted',[0,2])
                ->orderBy('acc_coa_details.id','asc')
                ->get()
        ];

        return view("merchant::toko.master-data.receipt.detail", $params);
    }

    public function addItem(Request $request)
    {
        $id=$request->id;
        if(!is_null($id)){

            $data=Material::find($id);
            $price=$data->total/$data->quantity;
        }else{
            $data=new Material();
            $price=0;
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'price'=>$price,
            'sc_product_id'=>$request->sc_product_id,
            'raw'=> Product::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_products.id)'),
                'sc_products.id',
                'sc_products.name',
                'u.name as unit_name'
            ])
                ->where('sc_products.md_user_id',user_id())
                ->join('sc_product_categories as c','c.id','sc_products.sc_product_category_id')
                ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->where('sc_products.is_deleted',0)
                ->where('md_sc_product_type_id','!=',4)
                ->where('sc_products.id', '!=', $request->sc_product_id)
                ->orderBy('id','desc')
                ->get(),
        ];
        return view('merchant::toko.master-data.receipt.form-item',$params);
    }

    public function saveItem(Request $request)
    {
        try {
            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();
            $id=$request->id;
            $exsistMaterial = Material::where('parent_sc_product_id', $request->sc_product_id)
                ->where('child_sc_product_id', $request->child_sc_product_id)
                ->where('is_deleted', 0)
                ->first();

            $checkMaterial = Material::where('parent_sc_product_id', $request->sc_product_id)
                ->where('is_deleted', 0)
                ->get();

            if($checkMaterial->count()>999){
                return response()->json([
                    'message' => "Jumlah produk resep melebihi batas!",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(!is_null($id)){
                $data=Material::find($id);
                if(!is_null($exsistMaterial) && $data->child_sc_product_id != $request->child_sc_product_id){
                    return response()->json([
                        'message' => "Item sudah digunakan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

            }else{
                $data=new Material();
                if(!is_null($exsistMaterial)){
                    return response()->json([
                        'message' => "Item sudah digunakan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }
            $total=strtr($request->total_price, array('.' => '', ',' => '.'));
            $data->total=$total;
            $data->quantity=$request->quantity;
            $data->unit_name=$request->unit_name;
            $data->is_multi_unit=$request->is_multi_unit;
            $data->is_deleted=0;
            $data->parent_sc_product_id=$request->sc_product_id;
            $data->child_sc_product_id=$request->child_sc_product_id;
            $data->multi_quantity=$request->quantity*$request->value_conversion;
            $data->value_conversion=$request->value_conversion;
            $data->multi_unit_name=$request->unit_name;
            $data->multi_unit_id=($request->multi_unit_id=='-1' || $request->multi_unit_id==-1)?null:$request->multi_unit_id;
            $data->unit_id=($request->unit_id==0|| $request->unit_id)?null:$request->unit_id;
            if(is_null($id)){
                $data->json_multi_unit=($request->json_multi_unit=='-1'|| $request->json_multi_unit==-1)?null:json_encode(json_decode($request->json_multi_unit));
            }
            $data->save();

            if(!is_null($id) && !is_null($data->assign_to)){
                foreach(json_decode($data->assign_to) as $key => $a){
                    $itemBranch=Material::find($a);
                    $checkChild=Product::whereRaw("lower(code) iLIKE '%".strtolower($itemBranch->getChild->code)."%' ")
                        ->where('md_user_id',$itemBranch->getParent->md_user_id)
                        ->where('is_deleted',0)
                        ->first();
                    if(!is_null($checkChild)){
                        $childId=$checkChild->id;
                    }else{
                        $child = Product::find($itemBranch->child_sc_product_id);
                        $newChild = $child->replicate();
                        $newChild->md_user_id = $itemBranch->getParent->md_user_id;
                        $newChild->stock=0;
                        $newChild->save();

                        $coaChild=CoaProductUtil::initialCoaProduct($itemBranch->getParent->getUser->getMerchant->id,$newChild,1);
                        if($coaChild==false)
                        {
                            return false;
                        }

                        $newChild->inv_id=$coaChild['inv_id'];
                        $newChild->hpp_id=$coaChild['hpp_id'];

                        $newChild->save();
                        $childId=$newChild->id;
                    }
                    $itemBranch->total=$total;
                    $itemBranch->unit_name=$request->unit_name;
                    $itemBranch->quantity=$request->quantity;
                    $itemBranch->is_multi_unit=$request->is_multi_unit;
                    $itemBranch->multi_quantity=$request->quantity*$request->value_conversion;
                    $itemBranch->multi_unit_name=$request->unit_name;
                    $itemBranch->multi_unit_id=($request->multi_unit_id=='-1' || $request->multi_unit_id==-1)?null:$request->multi_unit_id;
                    $itemBranch->unit_id=($request->unit_id==0|| $request->unit_id)?null:$request->unit_id;
                    $itemBranch->value_conversion=$request->value_conversion;
                    $itemBranch->child_sc_product_id=$childId;
                    $itemBranch->save();
                }
            }else{
                $assign_to_branch=json_decode($data->getParent->assign_to_branch);
                if(!is_null($assign_to_branch)){
                    foreach($assign_to_branch as $key => $a){
                        if($a!=""){
                            $merchant=Merchant::find($a);
                            foreach(json_decode($data->getParent->assign_to_product) as $i){
                                $newItem=new Material();
                                $newItem->total=$total;
                                $newItem->unit_name=$request->unit_name;
                                $newItem->quantity=$request->quantity;
                                $newItem->multi_quantity=$request->quantity*$request->value_conversion;
                                $newItem->multi_unit_name=$request->unit_name;
                                $newItem->is_multi_unit=$request->is_multi_unit;
                                $newItem->multi_unit_id=($request->multi_unit_id=='-1' || $request->multi_unit_id==-1)?null:$request->multi_unit_id;
                                $newItem->unit_id=($request->unit_id==0|| $request->unit_id)?null:$request->unit_id;
                                $newItem->value_conversion=$request->value_conversion;
                                $newItem->json_multi_unit=($request->json_multi_unit=='-1'|| $request->json_multi_unit==-1)?null:json_encode(json_decode($request->json_multi_unit));
                                $newItem->parent_sc_product_id=$i;
                                $child=Product::find($request->child_sc_product_id);

                                $checkChild=Product::whereRaw("lower(code) iLIKE '%".strtolower($child->code)."%' ")
                                    ->where('md_user_id',$merchant->md_user_id)
                                    ->where('is_deleted',0)
                                    ->first();
                                if(!is_null($checkChild)){
                                }else{
                                    $newChild = $child->replicate();
                                    $newChild->md_user_id = $merchant->md_user_id;
                                    $newChild->stock=0;
                                    $newChild->save();

                                    $coaChild=CoaProductUtil::initialCoaProduct($merchant->id,$newChild,1);
                                    if($coaChild==false)
                                    {
                                        return false;
                                    }

                                    $newChild->inv_id=$coaChild['inv_id'];
                                    $newChild->hpp_id=$coaChild['hpp_id'];

                                    $newChild->save();
                                }
                                $newItem->child_sc_product_id=$request->child_sc_product_id;
                                $newItem->save();
                            }
                        }

                    }
                }
            }
            DB::commit();
            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function save(Request $request)
    {
        try {
            $warehouse=parent::getGlobalWarehouse(merchant_id());
            if($warehouse==false)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan,inisiasi gudang persediaan gagal dibuat !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            date_default_timezone_set($request->_timezone);
            $unitName=$request->unit_name;
            $unitId=$request->unit;
            $productId=$request->sc_product_id;
            $multiUnitId=$request->multi_unit_id;
            $isMultiUnit=$request->is_multi_unit;
            $jsonMultiUnit=$request->json_multi_unit;
            $valueConversion=$request->value_conversion;
            $quantity=$request->quantity;
            $costId=$request->cost_id;
            $costName=$request->cost_name;
            $amount=$request->amount;
            $total=$request->total;

            DB::beginTransaction();
            $id = $request->id;
            if(!is_null($id)){

                $data=Product::find($id);
                $data->name=$request->name;
            }else{
                if(count($productId)<1){
                    return response()->json([
                        'message' => "Isi resep tidak boleh kosong",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                $data=new Product();
                $data->name=$request->name;
                $data->created_by_merchant=merchant_id();
            }

            $validator = Validator::make($request->all(), (is_null($id))?$data->ruleCreateWeb:$data->ruleUpdateWeb);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            $unit=Unit::whereRaw("lower(name) like '".strtolower($request->md_unit_id)."' ")->first();
            if(is_null($unit))
            {
                $unit=new Unit();
                $unit->name=$request->md_unit_id;
                $unit->save();
            }

            $selling_price=strtr($request->selling_price, array('.' => '', ',' => '.'));
            $data->code=$request->code;
            if(is_null($id))
            {
                $data->is_with_purchase_price=0;
            }

            $data->is_with_stock=0;
            $data->selling_price=$selling_price;
            $data->description=$request->description;
            $data->md_sc_product_type_id=4;
            $data->sc_product_category_id=$request->sc_product_category_id;
            $data->md_user_id=user_id();
            $data->md_unit_id=$unit->id;
            $data->md_sc_category_id=null;
            $data->profit_prosentase=0;
            $data->is_receipt=1;
            if(is_null($id))
            {
                $data->unique_code=Uuid::uuid4()->toString();
            }

            $data->is_deleted=0;

            $sumPrice=0;
            if(!empty($amount)){
                foreach($amount as $m){
                    $sumPrice+=(int)strtr($m, array('.' => '', ',' => '.'));
                }
            }
            if(!empty($total)){
                foreach($total as $m){
                    $sumPrice+=(int)strtr($m, array('.' => '', ',' => '.'));
                }
            }

            $dataCost=[];
            if(!empty($costId)){
                foreach(array_count_values($costId) as $val){
                    if($val>1){
                        return response()->json([
                            'message' => "Jenis biaya tidak boleh sama!",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }
                foreach($costId as $key => $c){
                    $a=strtr($amount[$key], array('.' => '', ',' => '.'));
                    // dd((int)$a);
                    $dataCost[]=[
                        "id"=>(int)$costId[$key],
                        "amount"=>(int)$a,
                        "name"=>$costName[$key]

                    ];
                }
                $data->cost_other=json_encode($dataCost);

            }

            $data->purchase_price=$sumPrice;
            $data->save();

            if(!is_null($id) && !is_null($data->assign_to_product)){
                $checkDuplicateProduct=ProductUtil::checkDuplicateProduct($data);
                if($checkDuplicateProduct==false){
                    return response()->json([
                        'message' => "Produk pada cabang telah digunakan tidak dapat dihapus!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $updateDuplicateProduct=ProductUtil::updateDuplicateProduct($data->assign_to_product,$data);
                if($updateDuplicateProduct==false){
                    return response()->json([
                        'message' => "Terjadi kesalahan pada update produk cabang!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                if(is_null($request->md_merchant_id)){
                    $data->assign_to_branch=null;
                    $data->assign_to_product=null;
                    $data->save();
                }
            }
            if(is_null($id)){
                $childProducts = [];
                foreach($productId as $key => $item){
                    if($quantity[$key] > 0){
                        $jsonMulti=($jsonMultiUnit[$key]=='-1' || $jsonMultiUnit[$key]==-1)?null:json_encode(json_decode($jsonMultiUnit[$key]));
                        $childProducts[] = [
                            "parent_sc_product_id" => $data->id,
                            "child_sc_product_id" => $productId[$key],
                            "quantity" => $quantity[$key],
                            "total"=>strtr($total[$key], array('.' => '', ',' => '.')),
                            "unit_name"=>$unitName[$key],
                            "created_at" => date('Y-m-d h:i:s'),
                            "updated_at" => date('Y-m-d h:i:s'),
                            "is_deleted" => 0,
                            'unit_id'=>$unitId[$key],
                            'is_multi_unit'=>$isMultiUnit[$key],
                            'multi_quantity'=>$quantity[$key]*$valueConversion[$key],
                            'json_multi_unit'=>$jsonMulti,
                            'value_conversion'=>$valueConversion[$key],
                            'multi_unit_name'=>$unitName[$key],
                            'multi_unit_id'=>($multiUnitId[$key]=='-1' ||$multiUnitId[$key]==-1)?null:$multiUnitId[$key]
                        ];
                    }
                }
                Material::insert($childProducts);
            }
            if(!is_null($request->md_merchant_id)){
                $dataBranch=explode(',',$request->md_merchant_id);
                $duplicatePackage=ProductUtil::duplicateProduct($dataBranch,$data,$request,$multi_unit=[],$selling_level=[]);

                if($duplicatePackage==false){
                    return response()->json([
                        'message' => "Terjadi kesalahan duplikasi resep!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $data->assign_to_branch=json_encode($dataBranch);

            }
            $data->save();
            DB::commit();

            return response()->json([
                'message' => "Data resep berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.master-data.receipt.index')
            ]);

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => '',
            ]);
        }
    }

    public function addCost(Request $request)
    {
        $params=[
            'title'=>'Tambah Biaya Baru'
        ];
        return view('merchant::toko.master-data.receipt.form-cost',$params);
    }

    public function saveCost(Request $request)
    {
        try {
            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();

            $data=new CoaDetail();
            $getCategory=CoaCategory::where('name','Beban Operasional & Usaha')->where('md_merchant_id',merchant_id())->first();
            $data->code=CoaUtil::generateCoaDetail($getCategory->id);
            $data->acc_coa_category_id=$getCategory->id;
            $data->name=$request->name;
            $data->md_sc_currency_id=1;
            $data->is_deleted=2;
            $data->type_coa="Debit";
            $data->save();

            $coa_map = new CoaMapping();
            $coa_map->name=$request->name;
            $coa_map->is_type=2;
            $coa_map->acc_coa_detail_id=$data->id;
            $coa_map->md_merchant_id=merchant_id();
            $coa_map->save();

            $checkCashflowFormatDetails=CoaCashflowFormatDetail::where([
                'acc_coa_detail_id'=>$data->id,
                'is_deleted'=>0
            ])->first();

            $getCashflowFormatId=CoaCashflowFormat::where('name','Beban Operasional & Usaha')->where('md_merchant_id',merchant_id())->first();
            if(!is_null($checkCashflowFormatDetails))
            {
                $checkCashflowFormatDetails->acc_coa_detail_id=$data->id;
                $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$getCashflowFormatId->id;
                $checkCashflowFormatDetails->save();
            }else{
                $checkCashflowFormatDetails=new CoaCashflowFormatDetail();
                $checkCashflowFormatDetails->acc_coa_detail_id=$data->id;
                $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$getCashflowFormatId->id;
                $checkCashflowFormatDetails->save();
            }

            DB::commit();
            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            if( PurchaseOrderDetail::where('sc_product_id',$id)
                    ->join('sc_purchase_orders as s','s.id','sc_purchase_order_details.sc_purchase_order_id')
                    ->where('s.is_deleted',0)
                    ->count()<1 &&
                SaleOrderDetail::where('sc_product_id',$id)
                    ->join('sc_sale_orders as s','s.id','sc_sale_order_details.sc_sale_order_id')
                    ->join('sc_products as p','p.id','sc_sale_order_details.sc_product_id')
                    ->where('s.is_deleted',0)
                    ->count()<1 &&
                StockInventory::where('sc_product_id',$id)
                    ->where('is_deleted',0)
                    ->where('type','in')
                    ->where('residual_stock','>',0)
                    ->count()<1 &&
                ProductionOfGoodDetail::where('sc_inv_production_of_good_details.sc_product_id',$id)
                    ->join('sc_inv_production_of_goods as s','s.id','sc_inv_production_of_good_details.sc_inv_production_of_good_id')->where('s.is_deleted',0)
                    ->count()<1
            ){
                $delete=true;

            }else{
                $delete=false;
            }
            if($delete==false)
            {
                return response()->json([
                    'message' => "Data resep tidak bisa dihapus,karena telah digunakan !",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }else{
                DB::beginTransaction();
                $data=Product::find($id);
                if($data->assign_to_product){
                    $checkDuplicateProduct=ProductUtil::checkDuplicateProduct($data);
                    if($checkDuplicateProduct==false){
                        return response()->json([
                            'message' => "Resep pada cabang telah digunakan tidak dapat dihapus!",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }

                }
                $data->is_deleted=1;
                $data->save();
                if($data->md_sc_product_type_id==3){
                    $raw=Material::where('child_sc_product_id',$id)->first();
                    if(!is_null($raw)){
                        $raw->is_deleted=1;
                        $raw->save();
                    }
                }

                if($data->md_sc_product_type_id == 4){
                    $raw=Material::where('child_sc_product_id',$id)->first();
                    if(!is_null($raw)){
                        $raw->is_deleted=1;
                        $raw->save();
                    }
                }

                $coa=CoaDetail::where('ref_external_id',$data->id)->where('ref_external_type',1)->get();
                foreach ($coa as $key =>$item)
                {
                    $item->is_deleted=1;
                    $item->save();
                    $coaCfD=CoaCashflowFormatDetail::where('acc_coa_detail_id',$item->id)->first();
                    if(!is_null($coaCfD))
                    {
                        $coaCfD->is_deleted=1;
                        $coaCfD->save();
                    }
                }
                DB::commit();

                return response()->json([
                    'message' => "Data berhasil dihapus !",
                    'type' => 'success',
                    'is_modal' => true,
                    'redirect_url' => ''
                ]);
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function deleteItem(Request $request)
    {
        try{
            $id=$request->id;
            $data=Material::find($id);
            if(!is_null($data->assign_to)){
                Material::whereIn('id',json_decode($data->assign_to))->update(['is_deleted'=>1]);
            }
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message' => "Data berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }
}

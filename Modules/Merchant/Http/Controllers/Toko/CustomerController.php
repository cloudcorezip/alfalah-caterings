<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\SennaCashier\CustomerLevel;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Utils\Plugin\PluginUtil;
use App\Models\MasterData\Plugin;
use App\Models\MasterData\Job;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantBranch;
use Modules\Merchant\Entities\Toko\CustLevelEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\DiscountEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use App\Models\Plugin\EnableMerchantPlugin;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/customer')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\CustomerController@index')
                    ->name('merchant.toko.customer.index');
                Route::post('/data-table', 'Toko\CustomerController@dataTable')
                    ->name('merchant.toko.customer.datatable');
                Route::post('/reload-data', 'Toko\CustomerController@reloadData')
                    ->name('merchant.toko.customer.reload-data');
                Route::post('/export-data', 'Toko\CustomerController@exportData')
                    ->name('merchant.toko.customer.export-data');
                Route::post('/add', 'Toko\CustomerController@add')
                    ->name('merchant.toko.customer.add');
                Route::post('/save', 'Toko\CustomerController@save')
                    ->name('merchant.toko.customer.save');
                Route::post('/delete', 'Toko\CustomerController@delete')
                    ->name('merchant.toko.customer.delete');

                Route::get('/add-customer', 'Toko\CustomerController@addCustomer')
                    ->name('merchant.toko.customer.add-customer');

                // sync google contact
                Route::post('/contact-sync-form', 'Toko\CustomerController@contactSyncForm')
                    ->name('merchant.toko.customer.contact-sync-form');
                Route::post('/contact-sync-compare', 'Toko\CustomerController@contactSyncCompare')
                    ->name('merchant.toko.customer.contact-sync-compare');
                Route::post('/contact-sync-save', 'Toko\CustomerController@contactSyncSave')
                    ->name('merchant.toko.customer.contact-sync-save');
                Route::post('/contact-sync-update-form', 'Toko\CustomerController@contactSyncUpdateForm')
                    ->name('merchant.toko.customer.contact-sync-update-form');
                Route::post('/contact-sync-update-save', 'Toko\CustomerController@contactSyncUpdateSave')
                    ->name('merchant.toko.customer.contact-sync-update-save');

                // get city
                Route::post('/get-city', 'Toko\CustomerController@getCity')
                    ->name('merchant.toko.customer.get-city');
                
                Route::post('/calculate-age', [static::class, 'calculateAge'])
                    ->name('merchant.toko.customer.calculate-age');

            });
    }


    private function _activePlugin()
    {
        return PluginUtil::checkActivePlugin(merchant_id(), Plugin::GOOGLE_CONTACT);
    }

    private function _apiUrl()
    {

        return env('NODE_URL');
    }

    // check customer code on merchant and branch
    private function _checkCustomerCode($id, $code)
    {
        $userIds = [];
        foreach(get_cabang() as $key => $item){
            array_push($userIds, $item->owner_user_id);
        }

        $checkCustomer = CustomerEntity::where('code', $code)
                                        ->where('id', '!=', $id)
                                        ->whereIn('md_user_id', $userIds)
                                        ->first();
        return $checkCustomer;
    }


    protected function _generateUniqueCode($prefix, $plus)
    {   
        $incremental = CustomerEntity::join('md_users as u', 'u.id', 'sc_customers.md_user_id')
                                    ->join('md_merchants as m', 'm.md_user_id', 'u.id')
                                    ->whereIn('m.id', MerchantUtil::getBranch(merchant_id(), 1))
                                    ->count() + $plus;
                                    
        $code = $prefix.sprintf("%07s", $incremental);
        $check = CustomerEntity::join('md_users as u', 'u.id', 'sc_customers.md_user_id')
                    ->join('md_merchants as m', 'm.md_user_id', 'u.id')
                    ->where('code', $code)
                    ->whereIn('m.id', MerchantUtil::getBranch(merchant_id(), 1))
                    ->first();

        if(is_null($check)){
            return $code;
        } else {
            return $this->_generateUniqueCode($prefix, $plus + 1);
        }
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Pelanggan',
            'tableColumns'=>CustomerEntity::dataTableColumns(),
            'add'=>(get_role()==3)?CustomerEntity::add(true):
                CustomerEntity::add(true),
            'key_val'=>$request->key,
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'level'=>CustomerLevel::
            select([
                'sc_customer_level.id',
                'sc_customer_level.code',
                'sc_customer_level.name',
                'sc_customer_level.description'
            ])
                ->join('md_users as u','u.id','sc_customer_level.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))
                ->orderBy('sc_customer_level.id','asc')->get(),
            'activePlugin' => $this->_activePlugin(),
            'apiUrl' => $this->_apiUrl()

        ];

        return view('merchant::toko.master-data.customer.index',$params);

    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (CustomerEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }

        if(isset($request->md_merchant_id)){
            $decodeMerchantIds = json_decode($request->md_merchant_id);
            if(in_array('-1', $decodeMerchantIds)){
                $merchantIds = MerchantUtil::getBranch(merchant_id(), 1);
            } else {
                $merchantIds = $decodeMerchantIds;
            }
            $searchParams['md_merchant_id'] = $merchantIds;
        }

        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Pelanggan',
            'searchKey' => $searchKey,
            'tableColumns'=>CustomerEntity::dataTableColumns(),
            'key_val'=>$request->key,
            'activePlugin' => $this->_activePlugin(),
            'apiUrl' => $this->_apiUrl()


        ];
        return view('merchant::toko.master-data.customer.list',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return CustomerEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public function addCustomer(Request $request){

        $id=$request->id;
        $isFromSale=is_null($request->is_sale)?0:1;

        if(!is_null($id)){
            $data=CustomerEntity::find($id);
            if(is_null($data)){
                return abort(404);
            }
            if($data->md_user_id != user_id()){
                return abort(404);
            }
            $code = $data->code;
        }else{
            $data=new CustomerEntity();
            $code = $this->_generateUniqueCode('PS-', 1);
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Pelanggan Baru':'Edit Pelanggan',
            'data'=>$data,
            'level'=>CustomerLevel::
            select([
                'sc_customer_level.id',
                'sc_customer_level.code',
                'sc_customer_level.name',
                'sc_customer_level.description'
            ])
                ->join('md_users as u','u.id','sc_customer_level.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))
                ->orderBy('sc_customer_level.id','asc')->get(),
            'activePlugin' => $this->_activePlugin(),
            'provinces' => Province::all(),
            'apiUrl' => $this->_apiUrl(),
            'isFromSale'=>$isFromSale,
            'jobs' => Job::all(),
            'religions' => generateReligion(),
            'mediaInfo' => generateMediaInfo(),
            'maritalStatus' => generateMaritalStatus(),
            'code' => $code
        ];


        return view('merchant::toko.master-data.customer.form-nopopup',$params);
    }

    public  function add(Request $request){

        $id=$request->id;
        $isFromSale=is_null($request->is_sale)?0:1;

        if(!is_null($id)){

            $data=CustomerEntity::find($id);
        }else{
            $data=new CustomerEntity();
        }
        $getBranch=MerchantUtil::getBranch(merchant_id(),1);

        $params=[
            'title'=>(is_null($id))?'Tambah Pelanggan Baru':'Edit Pelanggan',
            'data'=>$data,
            'level'=>CustLevelEntity::select(['sc_customer_level.*','m.name as merchant_name'])
                ->join('md_users as u','u.id','sc_customer_level.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->whereIn('m.id',$getBranch)
                ->where('is_deleted', 0)
            ->get(),
            'activePlugin' => $this->_activePlugin(),
            'provinces' => Province::all(),
            'apiUrl' => $this->_apiUrl(),
            'isFromSale'=>$isFromSale
        ];


        return view('merchant::toko.master-data.customer.form',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);

            $id=$request->id;

            if(!is_null($id)){
                $data=CustomerEntity::find($id);
            }else{
                $data=new CustomerEntity();
                $data->md_user_id=user_id();

            }

            if(isset($request->is_google_contact)){
                if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                    $emp = EnableMerchantplugin::find($this->_activePlugin()->id);
                    $quota = intval($emp->quota_used) + 1;
                    if($quota > $emp->quota_available){
                        return response()->json([
                            'message'=>'Kuota anda tidak cukup, data gagal disimpan !',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>''
                        ]);
                    }
                }
            }

            if(!is_null($request->code)){
                $checkCode = $this->_checkCustomerCode($data->id, $request->code);
                if(!is_null($checkCode))
                {
                    return response()->json([
                        'message'=>'Kode sudah digunakan !',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            $data->code=$request->code;
            $data->name=$request->name;
            $data->email=$request->email;
            $data->phone_number=$request->phone_number;
            $data->address=$request->address;
            $data->type_address = $request->type_address;
            $data->city_name = $request->city_name;
            $data->region_name = $request->region_name;
            $data->postal_code = $request->postal_code;
            $data->gender = $request->gender;
            $data->sc_customer_level_id=$request->sc_customer_level_id;
            $data->identity_card_number = $request->identity_card_number;
            $data->date_of_birth = $request->date_of_birth;
            $data->md_job_id = $request->md_job_id;
            $data->media_info = $request->media_info;
            $data->religion = $request->religion;
            $data->marital_status = $request->marital_status;
            $data->age = $request->age;

            if(isset($request->is_google_contact)){
                $data->etag = $request->etag;
                $data->resource_name = $request->resource_name;
                $data->is_google_contact = 1;
            }

            $data->save();

            if(isset($request->is_google_contact)){
                if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                    $emp = EnableMerchantplugin::find($this->_activePlugin()->id);
                    $quota = intval($emp->quota_used) + 1;
                    $emp->quota_used = $quota;
                    $emp->save();
                }
            }

            DB::commit();

            return response()->json([
                'message'=>'Pelanggan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=CustomerEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            if($data->is_google_contact == 1){
                if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                    $emp = EnableMerchantplugin::find($this->_activePlugin()->id);
                    if($emp->quota_used != 0){
                        $quota = intval($emp->quota_used) - 1;
                        $emp->quota_used = $quota;
                        $emp->save();
                    }
                }
            }

            DB::commit();

            return response()->json([
                'message'=>'Pelanggan berhasil dihapus',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $data = CustomerEntity::getDataForDataTable();
            if($request->search){
                $search = $request->search;
                $data->where(function($query) use($search){
                    $query->whereRaw("lower(sc_customers.code) like '%".strtolower($search)."%' ")
                        ->orWhereRaw("lower(sc_customers.name) like '%".strtolower($search)."%' ")
                        ->orWhereRaw("lower(sc_customers.email) like '%".strtolower($search)."%' ")
                        ->orWhereRaw("lower(sc_customers.address) like '%".strtolower($search)."%' ")
                        ->orWhereRaw("lower(l.name) like '%".strtolower($search)."%' ")
                        ->orWhereRaw("lower(sc_customers.phone_number) like '%".strtolower($search)."%' ");
                });
            }

            if($request->sc_customer_level_id != -1){
                $data->where('l.id', $request->sc_customer_level_id);
            }

            $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id()))->where('sc_customers.is_deleted',0);

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Data Pelanggan");
            if(merchant_detail()->is_branch==0)
            {
                $header = [
                    'No',
                    'Kode Pelanggan',
                    'Nama Pelanggan',
                    'Email',
                    'No Telp',
                    'Alamat',
                    'Kategori Pelanggan',
                    'Terdaftar di Cabang'
                ];
            }else{
                $header = [
                    'No',
                    'Kode Pelanggan',
                    'Nama Pelanggan',
                    'Email',
                    'No Telp',
                    'Alamat',
                    'Kategori Pelanggan'
                ];
            }

            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;

            foreach ($data->get() as $key => $item) {
                if(merchant_detail()->is_branch==0)
                {
                    $dataShow = [
                        $key+1,
                        $item->kode,
                        $item->nama_pelanggan,
                        $item->email,
                        $item->no_telp,
                        $item->alamat,
                        $item->kategori_pelanggan,
                        $item->terdaftar_di_cabang
                    ];
                }else{
                    $dataShow = [
                        $key+1,
                        $item->kode,
                        $item->nama_pelanggan,
                        $item->email,
                        $item->no_telp,
                        $item->alamat,
                        $item->kategori_pelanggan
                    ];
                }


                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-pelanggan_' . merchant_id()."_".date('Y-m-d-h-i-s') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL';</script>
            <script>
                toastForSaveData('Data pelanggan berhasil diexport!','success',true,'')
                let success = document.querySelector('#successExport');
                if(success){
                    setTimeout(() => {
                        closeModal();
                    }, 3000)
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<script>toastForSaveData('Terjadi kesalahan, Data pelanggan gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)

</script>";
        }

    }


    public function contactSyncForm(Request $request)
    {
        try {
            $userId = user_id();
            $data = CustomerEntity::where('md_user_id', $userId)
                            ->where('is_google_contact', 1)
                            ->where('is_deleted', 0)
                            ->get();

            $params = [
                "title" => "Sinkronisasi Google Contact",
                "data" => $data,
                "activePlugin" => $this->_activePlugin(),
                "apiUrl" => $this->_apiUrl()

            ];

            return view('merchant::toko.master-data.customer.sync.form', $params);

        } catch(\Exception $e)
        {
            abort(404);
        }
    }

    public function contactSyncCompare(Request $request)
    {
        try {
            $contactCustomer = SupplierEntity::where('is_deleted', 0)
                                        ->where('md_user_id', user_id())
                                        ->where('is_google_contact',1)
                                        ->get();

            return response()->json(['data'=> $contactCustomer]);

        } catch(\Exception $e)
        {
            return response()->json(['data'=> []]);
        }
    }

    public function contactSyncSave(Request $request)
    {
        try {
            DB::beginTransaction();

            $userId = user_id();
            $plugin = $this->_activePlugin();
            $emp = EnableMerchantplugin::find($plugin->id);

            if($plugin)
            {
                if($plugin->status == 0){
                    if(strtolower($plugin->payment_type) == "quota"){
                        return response()->json([
                            'message' => "Kuota tersedia tidak mencukupi, silahkan lakukan pembelian plugin untuk dapat menggunakan fitur !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    } else {
                        return response()->json([
                            'message' => "Masa berlangganan plugin anda sudah habis, silahkan lakukan pembelian plugin untuk dapat menggunakan fitur !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }
            }

            $dataFromDb = CustomerEntity::where('md_user_id', $userId)
                            ->where('is_google_contact', 1)
                            ->where('is_deleted', 0)
                            ->get()
                            ->toArray();
            $suppliers = SupplierEntity::where('md_user_id', $userId)
                            ->where('is_google_contact', 1)
                            ->where('is_deleted', 0)
                            ->get()
                            ->toArray();

            $requestContacts = json_decode($request->contacts, true);

            // filter request contact dengan data customer yang sudah ada di database
            $resultContact = array_filter($requestContacts, function($item) use($requestContacts, $dataFromDb){
                return array_search($item['resource_name'], array_column($dataFromDb, 'resource_name')) === false;
            });

            // filter request contact dengan data supplier yang sudah ada di database
            $result = array_filter($resultContact, function($item) use($resultContact, $suppliers){
                return array_search($item['resource_name'], array_column($suppliers, 'resource_name')) === false;
            });

            $data = [];

            foreach($result as $key => $item){
                $data[] = [
                    "name" => $item["name"],
                    "email" => $item["email"],
                    "phone_number" => $item["phone_number"],
                    "address" => $item["address"],
                    "md_user_id" => $userId,
                    "etag" => $item["etag"],
                    "is_google_contact" => 1,
                    "type_address" => $item['type_address'],
                    "city_name" => $item['city_name'],
                    "region_name" => $item['region_name'],
                    "postal_code" => $item['postal_code'],
                    "gender" => $item['genders'],
                    "resource_name" => $item["resource_name"],
                    "created_at" => date('Y-m-d h:i:s'),
                    "updated_at" => date('Y-m-d h:i:s')
                ];
            }

            // cek plugin adalah kuota
            // jika quota yang terpakai lebih dari quota yang tersedia, gagal menyimpan data
            if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                $quota = intval(count($result)) + intval($emp->quota_used);
                if($quota > intval($emp->quota_available)){
                    return response()->json([
                        'message' => "Kuota anda tidak cukup, data gagal disimpan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }


            CustomerEntity::insert($data);

            // menambah quota terpakai di enable_merchant_plugin
            if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                $quota = intval(count($result)) + intval($emp->quota_used);
                $emp->quota_used = $quota;
                $emp->save();
            }

            DB::commit();

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        } catch(\Exception $e)
        {
            DB::rollback();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function contactSyncUpdateForm(Request $request){

        $id=$request->id;

        $data = CustomerEntity::where('id', $id)
                    ->where('is_google_contact', 1)
                    ->first();

        $params=[
            'title'=>'Edit Pelanggan',
            'data'=>$data,
            'level'=>CustLevelEntity::where('md_user_id',user_id())
                ->where('is_deleted', 0)
                ->get(),
            'provinces' => Province::all(),
            'activePlugin' => $this->_activePlugin(),
            'apiUrl' => $this->_apiUrl()
        ];

        return view('merchant::toko.master-data.customer.sync.form-update',$params);
    }

    public function contactSyncUpdateSave(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);

            $id=$request->id;

            $data = CustomerEntity::where('id', $id)
                    ->where('is_google_contact', 1)
                    ->first();

            $data->code=$request->code;
            $data->name=$request->name;
            $data->md_user_id=user_id();
            $data->email=$request->email;
            $data->phone_number=$request->phone_number;
            $data->address=$request->address;
            $data->etag = $request->etag;
            $data->resource_name = $request->resource_name;
            $data->type_address = $request->type_address;
            $data->city_name = $request->city_name;
            $data->region_name = $request->region_name;
            $data->postal_code = $request->postal_code;
            $data->gender = $request->gender;
            if($request->sc_customer_level_id!='' || !is_null($request->sc_customer_level_id))
            {
                $data->sc_customer_level_id=$request->sc_customer_level_id;
            }
            $data->save();

            return response()->json([
                'message'=>'Pelanggan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch(\Exception $e)
        {
            DB::rollback();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function getCity(Request $request){
        try{
            $id = $request->id;
            $data = City::where('md_province_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }

    public function calculateAge(Request $request)
    {
        try {
            $dateOfBirth = $request->date;
            $today = date("Y-m-d");
            $diff = date_diff(date_create($dateOfBirth), date_create($today));

            return $diff->format('%y');
        
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return 0;
        }
    }

}

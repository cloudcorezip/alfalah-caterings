<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\MasterData\BusinessCategory;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\TypeOfBusiness;
use App\Models\MasterData\User;
use App\Models\SennaToko\Product;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\BranchEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\DiscountEntity;
use Modules\Merchant\Entities\Toko\StaffEntity;
use Ramsey\Uuid\Uuid;

class BranchController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/branch')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\BranchController@index')
                    ->name('merchant.toko.branch.index');
                Route::post('/data-table', 'Toko\BranchController@dataTable')
                    ->name('merchant.toko.branch.datatable');
                Route::post('/add', 'Toko\BranchController@add')
                    ->name('merchant.toko.branch.add');
                Route::post('/save', 'Toko\BranchController@save')
                    ->name('merchant.toko.branch.save');
                Route::post('/disconnect', 'Toko\BranchController@disconnect')
                    ->name('merchant.toko.branch.disconnect');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Cabang',
            'tableColumns'=>BranchEntity::dataTableColumns(),
            'add'=>(get_role()==2 || get_role()==3)?BranchEntity::add(true):
                BranchEntity::add(true),
            'key_val'=>$request->key
        ];
        return view('merchant::toko.master-data.branch.index',$params);

    }

    public function dataTable(Request $request)
    {
        return BranchEntity::dataTable($request);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=BranchEntity::find($id);
        }else{
            $data=new BranchEntity();
        }
        $categories =BusinessCategory::whereNull('parent_id')->with('getChild')->get();
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'categories'=>$categories
        ];

        if(is_null($request->page)){
            return view('merchant::toko.master-data.branch.form',$params);

        }else{
            return view('merchant::toko.master-data.branch.form-password',$params);

        }

    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);

            $id=$request->id;
            if(!is_null($id)){

                $data=BranchEntity::find($id);
            }else{
                $data=new BranchEntity();
                if(MerchantUtil::maxBranch(merchant_id())==false)
                {
                    return response()->json([
                        'message'=>'Mohon maaf anda telah melewati batas maksimal cabang pada paket berlangganan',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }




            if(is_null($request->page)){
                $merchant=Merchant::find(merchant_id());
                $branchUser=User::
                select(['m.id'])
                    ->join('md_merchants as m','m.md_user_id','md_users.id')
                    ->where('md_users.email',strtolower($request->email_branch))
                    ->where('md_role_id',$merchant->getUser->md_role_id)
                    ->where('md_users.email','!=',$merchant->getUser->email)
                    ->first();
                if(is_null($branchUser))
                {
                    $user=new User();
                    $user->fullname=$request->branch_owner;
                    $user->email=strtolower($request->email_branch);
                    $user->token=Hash::make(Uuid::uuid4()->toString());
                    $user->md_role_id=3;
                    $user->is_merchant=1;
                    $user->is_active=1;
                    $user->is_email_verified=1;
                    $user->balance=0;
                    $user->password=Hash::make($request->password);
                    $user->save();

                    $merchantNew=new Merchant();
                    $merchantNew->name = $request->branch_name;
                    $merchantNew->email_merchant = $user->email;
                    $merchantNew->md_app_service_id = 2;
                    $merchantNew->md_user_id = $user->id;
                    $merchantNew->md_inventory_method_id=2;
                    $merchantNew->md_business_category_id=$request->category_id;
                    $merchantNew->is_branch=1;
                    $merchantNew->branch_code = $request->branch_code;
                    $merchantNew->save();

                    $branchOther=BranchEntity::where('branch_merchant_id',$merchantNew->id)
                    ->where('head_merchant_id','!=',merchant_id())
                    ->where('is_active',1)
                    ->first();
                    if($branchOther){
                        return response()->json([
                        'message'=>'Akun sudah menjadi cabang pada merchant lain!',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                        ]);
                    }
                    $headBranchCheck=BranchEntity::where('head_merchant_id',$merchantNew->id)
                    ->where('branch_merchant_id',merchant_id())
                    ->where('is_active',1)
                    ->first();
                    if($headBranchCheck){
                        return response()->json([
                        'message'=>'Akun sudah menjadi pusat pada cabang ini!',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                        ]);
                    }

                    $data->email_branch=$user->email;
                    $data->head_merchant_id=merchant_id();
                    $data->branch_merchant_id=$merchantNew->id;
                    $data->is_accepted=1;
                    $data->is_active=1;
                    $data->save();
                    DB::commit();

                    Artisan::call('acc:build', ['--merchant' =>$merchantNew->id]);



                }else{

                    $branchCheck=BranchEntity::where('branch_merchant_id',$branchUser->id)
                                        ->where('head_merchant_id',merchant_id())
                                        ->where('is_active',1)
                                        ->first();
                    $branchOther=BranchEntity::where('branch_merchant_id',$branchUser->id)
                                        ->where('head_merchant_id','!=',merchant_id())
                                        ->where('is_active',1)
                                        ->first();
                    if($branchOther){
                        return response()->json([
                        'message'=>'Akun sudah menjadi cabang pada merchant lain!',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                        ]);
                    }
                    $headBranchCheck=BranchEntity::where('head_merchant_id',$branchUser->id)
                                        ->where('branch_merchant_id',merchant_id())
                                        ->where('is_active',1)
                                        ->first();
                    if($headBranchCheck){
                        return response()->json([
                        'message'=>'Akun sudah menjadi pusat pada cabang ini!',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                        ]);
                    }
                    if(is_null($branchCheck)){
                        $data=new BranchEntity();
                        $data->email_branch=strtolower($request->email_branch);
                        $data->head_merchant_id=merchant_id();
                        $data->branch_merchant_id=$branchUser->id;
                        $data->is_accepted=1;
                        $data->is_active=1;
                        $data->save();


                        $data->getMerchant->getUser->fullname=$request->branch_owner;
                        $data->getMerchant->getUser->email=strtolower($request->email_branch);
                        $data->getMerchant->getUser->save();

                        $data->getMerchant->name=$request->branch_name;
                        $data->getMerchant->email_merchant=strtolower($request->email_branch);
                        $data->getMerchant->md_business_category_id=$request->category_id;
                        $data->getMerchant->branch_code = $request->branch_code;
                        $data->getMerchant->save();
                        DB::commit();

                        Artisan::call('acc:build', ['--merchant' =>$branchUser->id]);

                    }else{
                        $branchCheck->is_active=1;
                        $branchCheck->save();

                        $branchCheck->getMerchant->getUser->fullname=$request->branch_owner;
                        $branchCheck->getMerchant->getUser->email=strtolower($request->email_branch);
                        $branchCheck->getMerchant->getUser->save();

                        $branchCheck->getMerchant->name=$request->branch_name;
                        $branchCheck->getMerchant->email_merchant=strtolower($request->email_branch);
                        $branchCheck->getMerchant->md_business_category_id=$request->category_id;
                        $branchCheck->getMerchant->branch_code = $request->branch_code;
                        $branchCheck->getMerchant->save();
                        DB::commit();

                        Artisan::call('acc:build', ['--merchant' =>$branchCheck->getMerchant->id]);

                    }



                }

            }

            if($request->page=='updatePwd')
            {

                $data->getMerchant->getUser->password=Hash::make($request->password);
                $data->getMerchant->getUser->save();

                DB::commit();

                Artisan::call('acc:build', ['--merchant' =>$data->getMerchant->id]);


            }
            return response()->json([
                'message'=>'Data berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }


    public function disconnect(Request $request)
    {
        try{
            $id=$request->id;
            $data=BranchEntity::find($id);
            $data->is_active=0;
            $data->save();

            $branchMerchant=Merchant::find($data->branch_merchant_id);
            $product=Product::where('md_user_id',user_id())->where('is_deleted',0)->whereNotNull('assign_to_branch')->get();
            foreach($product as $key => $item){
                if(!is_null($item->assign_to_branch) && json_decode($item->assign_to_branch)!=[]){
                    if(in_array($branchMerchant->id, json_decode($item->assign_to_branch))){
                        $newBranch=[];
                        foreach(json_decode($item->assign_to_branch) as $i){
                            if($i!=$branchMerchant->id){
                                $newBranch[]=$i;
                            }
                        }
                        $item->assign_to_branch=$newBranch;
                        $item->save();
                    }

                }
                if(!is_null($item->assign_to_product)){
                    foreach(json_decode($item->assign_to_product) as $p){
                        $checkProd=Product::where('id',$p)->where('md_user_id',$branchMerchant->md_user_id)->first();
                        if(!is_null($checkProd)){
                            if(in_array($checkProd->id, json_decode($item->assign_to_product))){
                                $newProduct=[];
                                foreach(json_decode($item->assign_to_product) as $p){
                                    if($p!=$checkProd->id){
                                        $newProduct[]=$p;
                                    }
                                }
                                $item->assign_to_product=$newProduct;
                                $item->save();
                            }
                        }
                    }
                }
            }
            DB::commit();
            return response()->json([
                'message'=>'Data cabang berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }

}

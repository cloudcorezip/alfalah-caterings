<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantShopCategory;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\MasterData\SennaCashier\ProductType;
use App\Models\SennaToko\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\HppEntity;
use Modules\Merchant\Entities\Toko\ProductEntity;
use Modules\Merchant\Entities\Toko\ProductRawMaterialEntity;
use Modules\Merchant\Entities\Toko\ProductSellingLevelEntity;
use Ramsey\Uuid\Uuid;

class HppController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('stock/hpp')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\HppController@data')
                    ->name('merchant.toko.stock.hpp.data');
                Route::get('/detail', 'Toko\HppController@detail')
                    ->name('merchant.toko.stock.hpp.detail');
                Route::post('/ajax-detail', 'Toko\HppController@ajaxDetail')
                    ->name('merchant.toko.stock.hpp.ajax-detail');
                Route::post('/export-data', 'Toko\HppController@exportData')
                    ->name('merchant.toko.stock.hpp.export-data');
                Route::post('/add-stock-adjustment', 'Toko\HppController@addStockAdjustment')
                    ->name('merchant.toko.stock.hpp.add-stock-adjustment');
            });
    }

    public function data()
    {
        if(is_null(request()->md_merchant_id))
        {
            $merchantId=[merchant_id()];
            $merchantString="(".merchant_id().")";
        }elseif (request()->md_merchant_id[0]=='-1')
        {
            $merchantString="(";
            foreach (get_cabang() as $key => $branch)
            {
                $merchantString.=($key==0)?"".$branch->id."":",".$branch->id."";;

            }
            $merchantString.=")";
            $merchantId=['-1'];
        }else{
            $merchantId=request()->md_merchant_id;
            $merchantString="(";
            $merchant=\request()->md_merchant_id;

            foreach ($merchant as $c =>$cc){
                $merchantString.=($c==0)?"".$merchant[$c]."":",".$merchant[$c]."";;

            }
            $merchantString.=")";

        }

        $start_date=request()->start_date;
        $end_date=\request()->end_date;
        $searchProduct= \request()->search_product;
        if(is_null($start_date)){
            $startDate=Carbon::now()->startOfYear()->format('Y-m-d');
            $endDate=Carbon::now()->endOfYear()->format('Y-m-d');

        }else{
            $startDate=$start_date;
            $endDate=$end_date;
        }
        $params=[
            'title'=>'Lihat Stok',
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'merchantId'=>$merchantId,
            'searchProduct'=>$searchProduct,
            'data'=>HppEntity::getDataHpp($startDate,$endDate,$merchantString,$searchProduct,\request()->page,request()->is_consignment,\request()->is_show_all),
            'is_consignment'=>is_null(\request()->is_consignment)?0:\request()->is_consignment,
            'is_show_all'=>is_null(\request()->is_show_all)?1:\request()->is_show_all,
            'type'=>1

        ];


        return view('merchant::toko.stock.hpp.data',$params);
    }


    public function ajaxDetail(Request  $request)
    {
        $productId=$request->id;
        $merchantId=$request->merchant_id;
        $originalProduct=Product::find($productId);
        $startDate=$request->start_date;
        $endDate=$request->end_date;
        $product=Product::
        select([
            'sc_products.*',
            "m.name as merchant_name"
        ])
        ->join('md_merchants as m','m.md_user_id','sc_products.md_user_id')
            ->where('sc_products.code',$originalProduct->code)
            ->where('m.id',$merchantId)->first();

        $stock=collect(DB::select("
        select
              p.id,
              p.name,
              p.code,
              w.name as warehouse_name,
              u.name as unit_name,
              coalesce(
                sum(
                  (
                    select
                      sum(ssi.residual_stock)
                    from
                      sc_stock_inventories ssi
                    where
                      ssi.type = 'in'
                      and ssi.is_deleted = 0
                      and ssi.sc_product_id = p.id
                      and ssi.inv_warehouse_id = w.id
                      and ssi.transaction_action not like '%Retur%'
                      and ssi.created_at :: date between '".$startDate."'
                      and '".$endDate."'
                  )
                ),
                0
              ) as residual_stock
            from
              md_merchant_inv_warehouses w
              join md_merchants m on m.id = w.md_merchant_id
              join sc_products p on p.md_user_id = m.md_user_id
              join sc_product_raw_materials sprm on sprm.child_sc_product_id = p.id
            left join md_units u on u.id=p.md_unit_id

            where
              w.is_deleted = 0
              and sprm.parent_sc_product_id = ".$product->id."
              and sprm.is_deleted=0
            group by
              p.id,
              w.name,
              w.id,
              u.name
            order by
              w.id asc
        "));

        return response()->json([
            'product'=>$product,
            'materialStock'=>$stock->groupBy('id')
        ]);

    }





    public  function detail(Request $request)
    {

        $id = $request->id;
        $data = ProductEntity::find($id);
        $merchantId=$request->merchant_id;
        $warehouseId=$request->warehouse_id;


        if(is_null($data)){
            abort(404);
        }

        $product=Product::
        select([
            'sc_products.*',
            "m.name as merchant_name"
        ])
            ->join('md_merchants as m','m.md_user_id','sc_products.md_user_id')
            ->where('sc_products.code',$data->code)
            ->where('sc_products.is_deleted',0)
            ->where('m.id',$merchantId)->first();


        if(is_null($warehouseId) || $warehouseId=='')
        {
            $params = [
                'title' => 'Arus Penerimaan - Pengeluaran Produk',
                'data' => $product,
                'warehouseId'=>'-1',
                'tableColumns' => ProductEntity::dataTableColumnsStock(),
                'inventory'=>MerchantWarehouse::select([
                    'md_merchant_inv_warehouses.*',
                    DB::raw("coalesce((select
               sum(ssi.residual_stock*ssi.purchase_price)
                from sc_stock_inventories ssi where inv_warehouse_id=md_merchant_inv_warehouses.id
                and ssi.sc_product_id = $product->id
                  and type = 'in' and ssi.transaction_action not like '%Retur%' and ssi.is_deleted = 0
                ),0) as value_inventory"),
                    DB::raw("coalesce((select
               sum(ssi.residual_stock)
                from sc_stock_inventories ssi where inv_warehouse_id=md_merchant_inv_warehouses.id
                and ssi.sc_product_id = $product->id
                  and type = 'in' and ssi.transaction_action not like '%Retur%' and ssi.is_deleted = 0

                ),0) as stock"),
                ])->where('md_merchant_inv_warehouses.is_deleted',0)
                    ->where('md_merchant_inv_warehouses.md_merchant_id',$merchantId)
                    ->first()
            ];
        }else{
            $params = [
                'title' => 'Arus Penerimaan - Pengeluaran Produk',
                'data' => $product,
                'warehouseId'=>$warehouseId,
                'tableColumns' => ProductEntity::dataTableColumnsStock(),
                'inventory'=>MerchantWarehouse::select([
                    'md_merchant_inv_warehouses.*',
                    DB::raw("coalesce((select
               sum(ssi.residual_stock*ssi.purchase_price)
                from sc_stock_inventories ssi where inv_warehouse_id=md_merchant_inv_warehouses.id
                and ssi.sc_product_id = $data->id
                  and type = 'in' and ssi.transaction_action not like '%Retur%' and ssi.is_deleted = 0
                  and ssi.inv_warehouse_id=$warehouseId
                ),0) as value_inventory"),
                    DB::raw("coalesce((select
               sum(ssi.residual_stock)
                from sc_stock_inventories ssi where inv_warehouse_id=md_merchant_inv_warehouses.id
                and ssi.sc_product_id = $data->id
                  and type = 'in' and ssi.transaction_action not like '%Retur%' and ssi.is_deleted = 0
                   and ssi.inv_warehouse_id=$warehouseId
                ),0) as stock"),
                ])->where('md_merchant_inv_warehouses.id',$warehouseId)
                    ->where('md_merchant_inv_warehouses.is_deleted',0)
                    ->first()
            ];

        }



        return view('merchant::toko.stock.hpp.detail', $params);
    }



    public function exportData(Request $request)
    {
        try{

            if(is_null(request()->md_merchant_id))
            {
                $merchantString="(".merchant_id().")";
            }elseif (request()->md_merchant_id=='-1')
            {
                $merchantString="(";
                foreach (get_cabang() as $key => $branch)
                {
                    $merchantString.=($key==0)?"".$branch->id."":",".$branch->id."";;

                }
                $merchantString.=")";
            }else{
                $merchantString="(".\request()->md_merchant_id.")";

            }


            $startDate=request()->start_date;
            $endDate=\request()->end_date;
            $searchProduct= \request()->search_product;
            $isConsignment=is_null(\request()->is_consignment)?0:\request()->is_consignment;
            $isShowAll=is_null(\request()->is_show_all)?0:\request()->is_show_all;


            $data=HppEntity::getDataHpp($startDate,$endDate,$merchantString,$searchProduct,request()->page,$isConsignment,$isShowAll,false);


            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('C2',"Laporan  Stok Produk Tanggal ".$startDate." s/d ".$endDate." ");
            $objPHPExcel->getActiveSheet()->mergeCells('C2:J2')->getStyle('C2')->getFont()->setSize(18)->setBold(true);
            $header = [
                'Produk',
                'Tipe Gudang',
                'Stok Tersedia',
                'HPP',
                'Nilai Persediaan',
                'Satuan',
                'Stok Masuk',
                'Stok Keluar'
            ];
            $col = 'C';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;
            foreach ($data as $key => $item) {
                $dataShow = [
                    '-- '.$item->nama_produk,
                    '',
                    $item->stok_tersedia,
                    $item->hpp,
                    $item->nilai_persediaan,
                    $item->satuan,
                    $item->stok_keluar,
                    $item->stok_masuk
                ];

                $col = 'C';
                $row = $num + 1;

                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;
                foreach ($item->merchant as $c =>$b){
                    $dataShow2 = [
                        '---- '.$b->merchant_name,
                        '',
                        $b->stok_tersedia,
                        '',
                        $b->nilai_persediaan,
                        '',
                        $b->stok_masuk,
                        $b->stok_keluar
                    ];
                    $col='C';
                    $row = $num+ 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                        $col++;
                    }
                    $num++;

                    foreach ($b->warehouse as $w =>$g)
                    {
                        $dataShow3 = [
                            $g->warehouse_name,
                            $g->warehouse_type,
                            $g->stok_tersedia,
                            '',
                            $g->nilai_persediaan,
                            '',
                            $g->stok_masuk,
                            $g->stok_keluar
                        ];
                        $col='C';
                        $row = $num + 1;
                        foreach ($dataShow3 as $ds3) {
                            $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds3);
                            $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                            $col++;
                        }
                        $num++;
                    }
                }
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-hpp_stock_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);

            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                let success = document.querySelector('#successExport');
                if(success){
                    toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Terjadi kesalahan di server, data gagal di export !','danger',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";
        }

    }

}

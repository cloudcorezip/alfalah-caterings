<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\MasterData\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\AffiliateEntity;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\StaffEntity;

class AffiliateController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('marketing/affiliator')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.affiliate.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.affiliate.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.toko.affiliate.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.affiliate.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.affiliate.delete');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Affiliator',
            'tableColumns'=>AffiliateEntity::dataTableColumns(),
            'add'=>AffiliateEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.affiliate.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return AffiliateEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public function add(Request $request)
    {
        $id=$request->id;
        if(!is_null($id)){

            $data=AffiliateEntity::find($id);
        }else{
            $data=new AffiliateEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::toko.master-data.affiliate.form',$params);
    }

    public function save(Request  $request)
    {
        try {
            DB::beginTransaction();
            $id = $request->id;
            if(!is_null($id)){
                $data = StaffEntity::find($id);
            } else {
                $data = new StaffEntity();
            }

            if(!is_null($id)){

                $user=User::find($data->md_user_id);
                $user->fullname=$request->name;
                $user->is_active=0;
                $user->is_email_verified=0;
                $user->is_staff=1;
                $user->md_role_id=1;
                $user->is_merchant=0;
                $user->is_active_merchant=0;
                $user->is_verified_merchant=0;
                $user->phone_number = $request->phone_number;
                $user->save();
                $data->address = $request->address;
                $data->email = $request->email;
                $data->md_user_id=$user->id;
                $data->md_merchant_id=merchant_id();
                $data->is_non_employee=1;
                $data->save();

            }else{
                $user=new User();
                $user->fullname=$request->name;
                $user->is_active=0;
                $user->is_email_verified=0;
                $user->is_staff=1;
                $user->md_role_id=1;
                $user->is_merchant=0;
                $user->is_active_merchant=0;
                $user->is_verified_merchant=0;
                $user->phone_number = $request->phone_number;
                $user->save();
                $data->address = $request->address;
                $data->email = $request->email;
                $data->md_user_id=$user->id;
                $data->md_merchant_id=merchant_id();
                $data->is_non_employee=1;
                $data->save();
            }

            DB::commit();

            return response()->json([
                'message'=>'Data affiliator berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }

    }



    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=AffiliateEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'affiliator berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }
}

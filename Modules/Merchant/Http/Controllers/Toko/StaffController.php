<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\Role;
use App\Models\MasterData\User;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\HR\DepartementEntity;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Entities\Toko\BranchEntity;
use Modules\Merchant\Entities\Toko\GradeEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\StaffEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Illuminate\Support\Facades\Storage;
use Image;

class StaffController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('master-data/staff')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\StaffController@index')
                    ->name('merchant.toko.staff.index');
                Route::post('/data-table', 'Toko\StaffController@dataTable')
                    ->name('merchant.toko.staff.datatable');
                Route::post('/add', 'Toko\StaffController@add')
                    ->name('merchant.toko.staff.add');
                Route::post('/save', 'Toko\StaffController@save')
                    ->name('merchant.toko.staff.save');
                Route::post('/add-non-employee', 'Toko\StaffController@addNonEmployee')
                    ->name('merchant.toko.staff.add-non-employee');
                Route::post('/save-non-employee', 'Toko\StaffController@saveNonEmployee')
                    ->name('merchant.toko.staff.save-non-employee');
                Route::post('/delete', 'Toko\StaffController@delete')
                    ->name('merchant.toko.staff.delete');

                Route::get('/add-staff', 'Toko\StaffController@addStaff')
                    ->name('merchant.toko.staff.addStaff');

                Route::get('/ajax-list', 'Toko\StaffController@ajaxList')
                    ->name('merchant.toko.staff.ajax-list');

            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Karyawan',
            'tableColumns'=>StaffEntity::dataTableColumns(),
            'add'=>(get_role()==2 || get_role()==3)?StaffEntity::add(true):
                StaffEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.staff.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return StaffEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=2 || get_role()!=3)?false:true);
    }

    public function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){
            $data=StaffEntity::find($id);
        }else{
            $data=new StaffEntity();
        }
        
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'role'=>Role::where('id','>=',11)
            ->orderBy('id','ASC')->get(),
            'gradeOption'=>GradeEntity::listOption(),
            'departementOption'=>DepartementEntity::listOption(),
            'jobOption'=>JobEntity::listOption(),
        ];

        return view('merchant::toko.master-data.staff.form',$params);
    }

    public function addStaff(Request $request)
    {
        $id = $request->id;
        if(!is_null($id)){
            $data = StaffEntity::find($id);

            $data = StaffEntity::select([
                "md_merchant_staff.*",
                "hmd.name as department_name"
            ])
            ->leftJoin('hr_merchant_departements as hmd', 'hmd.id', 'md_merchant_staff.departement_id')
            ->where('md_merchant_staff.id', $id)
            ->first();
            
            if(is_null($data)){
                return abort(404);
            }
        } else {
            $data = new StaffEntity();
        }

        $params = [
            "title" => (is_null($id))? 'Tambah Data':'Edit Data',
            "data" => $data,
            "role" => Role::where("id", ">=", 11)
                            ->where("id", "!=", 23)
                            ->orderBy("id", "ASC")
                            ->get(),
            "maritalStatus" => generateMaritalStatus(),
            "religion" => generateReligion(),
            "education" => generateEducation(),
            "bloodType" => generateBloodType(),
            'gradeOption'=>GradeEntity::listOption(),
            'jobOption'=>JobEntity::listOption(),
            'departementOption'=>DepartementEntity::listOption()

        ];

        return view("merchant::toko.master-data.staff.form-nopopup", $params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);

            DB::beginTransaction();
            $id=$request->id;
            if(!is_null($id)){

                $data=StaffEntity::find($id);
            }else{
                $data=new StaffEntity();

            }

            $merchant=Merchant::find(merchant_id());
            if(is_null($id))
            {
                if(MerchantUtil::maxStaff(merchant_id())==false)
                {
                    return response()->json([
                        'message'=>trans('custom.max_subs'),
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            if($request->is_create_email==1)
            {
                if($request->email==$merchant->getUser->email)
                {
                    return response()->json([
                        'message'=>trans('custom.email_already_use'),
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            $checkUser=User::where('email',strtolower($request->email))
                    ->where('md_users.id', '!=', $data->md_user_id)
                    ->first();

            if(!is_null($checkUser))
            {
                return response()->json([
                    'message'=>trans('custom.email_already_use'),
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(!is_null($request->code)){
                $checkCode = StaffEntity::where('code', $request->code)
                                        ->where('md_merchant_id', merchant_id())
                                        ->where('is_deleted', 0)
                                        ->where('id', '!=', $data->id)
                                        ->first();
                if(!is_null($checkCode))
                {
                    return response()->json([
                        'message'=>'Kode sudah digunakan !',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            $destinationPath = 'public/uploads/user/'.user_id().'/image/';

            if($request->hasFile('image'))
            {
                $file=$request->file('image');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('image'))->resize(300,300)->encode($file->getClientOriginalExtension());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, (string) $image);
            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->image;

                }
            }

            if(is_null($id))
            {
                $user=new User();
                $user->fullname=$request->name;

                if($request->is_create_email == 1){
                    $user->email=strtolower($request->email);
                    $user->password=Hash::make($request->password);
                }

                $user->is_active=1;
                $user->is_email_verified=1;
                $user->is_staff=1;
                $user->md_role_id=$request->md_role_id;
                $user->is_merchant=1;
                $user->is_active_merchant=1;
                $user->is_verified_merchant=1;
                $user->gender = $request->gender;
                $user->phone_number = $request->phone_number;
                $user->birth_place = $request->birth_place;
                $user->birth_date = $request->birth_date;
                $user->foto = $fileName;
                $user->save();
                $data->md_user_id=$user->id;
            }else{
                $user=User::find($data->md_user_id);
                $user->fullname=$request->name;
                $user->md_role_id=$request->md_role_id;
                $user->gender = $request->gender;
                $user->phone_number = $request->phone_number;
                $user->birth_place = $request->birth_place;
                $user->birth_date = $request->birth_date;
                $user->foto = $fileName;
                $user->save();

                if($request->is_create_email == 1){
                    $user->email=strtolower($request->email);
                    if(!is_null($request->password) || $request->password!='')
                    {
                        $user->password=Hash::make($request->password);
                    }
                    $user->save();
                }
            }


            if($request->is_create_email == 1){
                $data->email=$request->email;
            }
            if(merchant_detail()->is_branch==0)
            {
                $data->md_merchant_id=$request->md_merchant_id;

            }else{
                $data->md_merchant_id=merchant_id();

            }
            $data->merchant_grade_id=$request->merchant_grade_id;
            $data->code = $request->code;
            $data->marital_status = $request->marital_status;
            $data->blood_type = $request->blood_type;
            $data->last_education = $request->last_education;
            $data->is_active = $request->is_active;
            $data->religion = $request->religion;
            $data->address = $request->address;
            $data->is_create_email = $request->is_create_email;
            $data->merchant_grade_id=$request->merchant_grade_id;
            $data->departement_id=$request->departement_id;
            $data->job_position_id=$request->job_position_id;
            $data->join_date=$request->join_date;

            $data->save();
            DB::commit();
            return response()->json([
                'message'=>'Data karyawan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=> route('merchant.toko.staff.index')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=StaffEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            $user=User::find($data->md_user_id);
            $user->is_active=0;
            $user->save();
            return response()->json([
                'message'=>'Data karyawan berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    function addNonEmployee(Request  $request)
    {
        $id = $request->id;
        if(!is_null($id)){
            $data = StaffEntity::find($id);
        } else {
            $data = new StaffEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data
        ];

        return  view('merchant::toko.master-data.staff.form-other-employee',$params);
    }

    public function saveNonEmployee(Request  $request)
    {
        try {
            DB::beginTransaction();
            $id = $request->id;
            if(!is_null($id)){
                $data = StaffEntity::find($id);
            } else {
                $data = new StaffEntity();
            }
            $user=new User();
            $user->fullname=$request->name;
            $user->is_active=0;
            $user->is_email_verified=0;
            $user->is_staff=1;
            $user->md_role_id=1;
            $user->is_merchant=0;
            $user->is_active_merchant=0;
            $user->is_verified_merchant=0;
            $user->phone_number = $request->phone_number;
            $user->save();
            $data->address = $request->address;
            $data->email = $request->email;
            $data->md_user_id=$user->id;
            $data->md_merchant_id=merchant_id();
            $data->is_non_employee=1;
            $data->save();
            DB::commit();

            return response()->json([
                'message'=>'Data affiliator berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=> route('merchant.toko.staff.index')
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }

    }

    public function ajaxList(Request  $request)
    {
        try{
            $key=$request->key;
            if(is_null($key) || $key==''){
                $data=MerchantStaff::
                select([
                    'u.id',
                    DB::raw(" u.fullname || ' (' || r.name || ')'  as text")
                ])
                    ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                    ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                    ->join('md_roles as r','r.id','u.md_role_id')
                    ->leftJoin('merchant_grade_staffs as g','g.id','md_merchant_staff.merchant_grade_id')
                    ->orderBy('md_merchant_staff.id','desc')
                    ->where('md_merchant_staff.is_non_employee',0)
                    ->where('md_merchant_staff.is_deleted',0)
                    ->where('m.id',merchant_id())
                    ->get();
            }else{
                $data=MerchantStaff::
                select([
                    'u.id',
                    DB::raw(" u.fullname || ' (' || r.name || ')'  as text")
                ])
                    ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                    ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                    ->join('md_roles as r','r.id','u.md_role_id')
                    ->leftJoin('merchant_grade_staffs as g','g.id','md_merchant_staff.merchant_grade_id')
                    ->orderBy('md_merchant_staff.id','desc')
                    ->where('md_merchant_staff.is_non_employee',0)
                    ->where('md_merchant_staff.is_deleted',0)
                    ->where('m.id',merchant_id())
                    ->whereRaw("lower(u.fullname) like '%".strtolower(str_replace("'","''",$key))."%' ")
                    ->get();
            }
            return response()->json($data);
        }catch (\Exception $e)
        {
            return response()->json($e);

        }
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\SennaToko\DiscountProduct;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Merk;
use App\Models\MasterData\Merchant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Models\SennaToko\DiscountCoupon;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\CustLevelEntity;
use Modules\Merchant\Entities\Toko\DiscountCouponEntity;
use App\Utils\Merchant\MerchantUtil;
use Modules\Merchant\Entities\Toko\LoyaltyEntity;
use App\Models\SennaToko\LoyaltyPointRate;


class LoyaltyController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/loyalty')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.loyalty.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.loyalty.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.toko.loyalty.add');
                Route::post('/edit', [static::class, 'edit'])
                    ->name('merchant.toko.loyalty.edit');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.loyalty.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.loyalty.delete');
                Route::post('/reload-data', [static::class, 'reloadData'])
                    ->name('merchant.toko.loyalty.reload-data');
                Route::post('/add-point-rate', [static::class, 'addPointRate'])
                    ->name('merchant.toko.loyalty.add-point-rate');
                Route::post('/save-point-rate', [static::class, 'savePointRate'])
                    ->name('merchant.toko.loyalty.save-point-rate');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Loyalty Point',
            'tableColumns'=>LoyaltyEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'add'=>(get_role()==3)?LoyaltyEntity::add(true):
                LoyaltyEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.loyalty.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return LoyaltyEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public function add(Request $request){

        $id=$request->id;
        $userId = user_id();
        if(!is_null($id)){
            $data=LoyaltyEntity::find($id);
        }else{
            $data=new LoyaltyEntity();
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Loyalty Point':'Edit Data',
            'data'=>$data,
            'categoryCustomer' => CustLevelEntity::listOption()
        ];

        return view('merchant::toko.master-data.loyalty.form',$params);
    }

    public function edit(Request $request){

        $id=$request->id;
        $userId = user_id();
        if(!is_null($id)){
            $data=LoyaltyEntity::find($id);

        }else{
            $data=new LoyaltyEntity();
        }

        $branch = (is_null(json_decode($data->assign_to_branch)))? []:json_decode($data->assign_to_branch);

        $customerCriteria = json_decode($data->customer_criteria);
        $params=[
            'title'=>(is_null($id))?'Tambah Kupon Diskon':'Edit Data',
            'data'=>$data,
            'categoryCustomer' => CustLevelEntity::listOption(),
            'branch' => $branch,
            'customerCriteria' => $customerCriteria
        ];

        return view('merchant::toko.master-data.loyalty.form-edit',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);

            $id = $request->id;
            $merchantId = merchant_id();

            if(is_null($id))
            {
                $data = new LoyaltyEntity();
                $data->md_merchant_id = $merchantId;
            } else {
                $data = LoyaltyEntity::find($id);
            }

            if($request->get_point_method == 1){
                $validator = Validator::make($request->all(), $data->createMethodOne);
            } else {
                $validator = Validator::make($request->all(), $data->createMethodTwo);
            }


            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message'=>$error,
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->is_all_branch == 1){
                $tmpBranch = get_cabang();
                $branch = [];
                foreach($tmpBranch as $k => $i){
                    if($i->id != $merchantId){
                        array_push($branch, $i->id);
                    }
                }
            } else {
                $branch = json_decode($request->assign_to_branch);
            }

            if(count($branch) > 0){

                if($request->get_point_method!=1){
                    $otherMerchant = Merchant::whereIn("id",$branch)
                        ->get()
                        ->toArray();
                    $queryMerchantId = $branch;
                    array_unshift($queryMerchantId, $merchantId);

                    $findProduct = Product::find($request->sc_product_id);

                    if(!is_null($findProduct)){
                        $checkProductOtherMerchant = Product::select([
                            "sc_products.id",
                            "sc_products.name",
                            "sc_products.code",
                            "m.id as md_merchant_id",
                            "m.name as merchant_name"
                        ])
                            ->join("md_users as u", "u.id", "sc_products.md_user_id")
                            ->join("md_merchants as m", "m.md_user_id", "u.id")
                            ->where("sc_products.code", $findProduct->code)
                            ->whereIn("m.id", $queryMerchantId)
                            ->where("sc_products.is_deleted", 0)
                            ->get()
                            ->toArray();

                        foreach($branch as $key => $branchId)
                        {
                            $productByMerchant = array_values(array_filter($checkProductOtherMerchant, function($val) use($branchId){
                                return $val["md_merchant_id"] == $branchId;
                            }));

                            $filterMerchant = array_values(array_filter($otherMerchant, function($val) use($branchId){
                                return $val["id"] == $branchId;
                            }));

                            $currentMerchant = array_shift($filterMerchant);

                            foreach($checkProductOtherMerchant as $key => $item)
                            {
                                if(!in_array($item["code"], array_column($productByMerchant, "code")))
                                {
                                    return response()->json([
                                        'message'=>$item["name"]." tidak tersedia di cabang ".$currentMerchant["name"],
                                        'type'=>'warning',
                                        'is_modal'=>false,
                                        'redirect_url'=>''
                                    ]);
                                }
                            }
                        }
                    }
                }
            }

            if($request->get_point_method == 1)
            {
                $data->min_transaction = (float)strtr($request->min_transaction, array('.' => '', ',' => '.'));
                $data->sc_product_id = null;
            } else {

                $forCheckBranch = $branch;
                array_unshift($forCheckBranch, merchant_id());

                $p = Product::find($request->sc_product_id);

                $pCode = is_null($p)? null:$p->code;

                $checkProduct = LoyaltyEntity::select([
                    "crm_loyalty_points.id",
                    "sp.name"
                ])
                ->join("sc_products as sp", "sp.id", "crm_loyalty_points.sc_product_id")
                ->where("crm_loyalty_points.is_deleted", 0)
                ->where("crm_loyalty_points.id", "!=", $id)
                ->where("sp.code", $pCode)
                ->where(function($q) use($forCheckBranch){
                    $q->whereIn('crm_loyalty_points.md_merchant_id', $forCheckBranch)
                        ->orWhereJsonContains('crm_loyalty_points.assign_to_branch', $forCheckBranch);
                })
                ->first();

                if(!is_null($checkProduct)){
                    return response()->json([
                        'message'=>"Pengaturan poin untuk product $checkProduct->name sudah ada",
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }

                $data->sc_product_id = $request->sc_product_id;
                $data->min_transaction = 0;
            }

            $data->get_point_method = $request->get_point_method;
            $data->point_earned = $request->point_earned;
            $data->customer_criteria = $request->customer_criteria;
            $data->is_apply_multiple = $request->is_apply_multiple;
            $data->is_active = $request->is_active;
            $data->is_all_branch = $request->is_all_branch;

            if(count($branch) > 0){
                $data->assign_to_branch = json_encode($branch);
            } else {
                $data->assign_to_branch = null;
            }

            $data->save();

            DB::commit();
            return response()->json([
                'message'=>'Point loyalty berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=LoyaltyEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            DB::commit();
            return response()->json([
                'message'=>'Point loyalty berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal dihapus!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }



    public function reloadData(Request $request)
    {

        $searchParams = [];
        foreach (LoyaltyEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Loyalti',
            'searchKey' => $searchKey,
            'tableColumns'=>LoyaltyEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.master-data.discount-coupon.list',$params);

    }


    public function addPointRate(Request $request)
    {
        $merchantId = merchant_id();

        $data = LoyaltyPointRate::where('md_merchant_id', $merchantId)
                                    ->first();
        if(is_null($data))
        {
            $data = new LoyaltyPointRate();
        }

        $params=[
            'title'=>'Pengaturan Kurs Point',
            'data' => $data
        ];

        return view('merchant::toko.master-data.loyalty.form-point-rate',$params);
    }

    public function savePointRate(Request $request)
    {
        try {
            $id = $request->id;
            $merchantId = merchant_id();

            $data = LoyaltyPointRate::where('md_merchant_id', $merchantId)
                                        ->first();
            if(is_null($data))
            {
                $data = new LoyaltyPointRate();
            }

            $validator = Validator::make($request->all(), $data->create);

            if($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message'=>$error,
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $check = LoyaltyPointRate::where("md_merchant_id", $merchantId)
                                        ->where("id", "!=", $id)
                                        ->first();

            if(!is_null($check))
            {
                return response()->json([
                    'message'=>"Pengaturan kurs point sudah ada !",
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $data->md_merchant_id = $merchantId;
            $data->value_per_point = (float)strtr($request->value_per_point, array('.' => '', ',' => '.'));
            $data->save();

            return response()->json([
                'message'=>"Pengaturan kurs point berhasil disimpan !",
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }

    }
}

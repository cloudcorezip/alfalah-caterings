<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\PromoEntity;

class PromoController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/promo')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\PromoController@index')
                    ->name('merchant.toko.promo.index');
                Route::post('/data-table', 'Toko\PromoController@dataTable')
                    ->name('merchant.toko.promo.datatable');
            });
    }


    public function index()
    {
        $params=[
            'title'=>'Promo Produk',
            'tableColumns'=>PromoEntity::dataTableColumns()

        ];

        return view('merchant::toko.master-data.promo.index',$params);

    }

    public function dataTable(Request $request)
    {
        return PromoEntity::dataTable($request);
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\SennaToko\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\GradeEntity;

class GradeController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/grade')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\GradeController@index')
                    ->name('merchant.toko.grade.index');
                Route::post('/data-table', 'Toko\GradeController@dataTable')
                    ->name('merchant.toko.grade.datatable');
                Route::post('/add', 'Toko\GradeController@add')
                    ->name('merchant.toko.grade.add');
                Route::post('/save', 'Toko\GradeController@save')
                    ->name('merchant.toko.grade.save');
                Route::post('/delete', 'Toko\GradeController@delete')
                    ->name('merchant.toko.grade.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Grade',
            'tableColumns'=>GradeEntity::dataTableColumns(),
            'add'=>(get_role()==3)?GradeEntity::add(true):
                GradeEntity::add(true),
            'key_val'=>$request->key

        ];

        return view('merchant::toko.master-data.grade.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return GradeEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=GradeEntity::find($id);
            $merchantId=[];
            $branch=json_decode($data->md_merchant_id);

            foreach ($branch as $item)
            {
                $merchantId[]=$item->id;
            }

        }else{
            $data=new GradeEntity();
            $merchantId=[merchant_id()];
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'merchantId'=>$merchantId,
            'option'=>GradeEntity::listOption(),
        ];


        return view('merchant::toko.master-data.grade.form',$params);
    }

    public function save(Request $request)
    {

        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;


            if(!is_null($id)){

                $data=GradeEntity::find($id);
            }else{
                $data=new GradeEntity();
                $data->md_user_id=user_id();
            }
            // dd(json_decode($data->assign_to_branch));
            $data->grade_name=$request->grade_name;
            $merchant=explode(',',$request->md_merchant_id);
            $list=[];
            foreach ($merchant as $key =>$item)
            {
                $list[]=[
                    'id'=>$merchant[$key]
                ];
            }

            if($request->parent_id!='-1')
            {
                $data->parent_id=$request->parent_id;
            }
            $data->md_merchant_id=json_encode($list);

            $data->basic_salary=(float)strtr($request->basic_salary, array('.' => '', ',' => '.'));
            $data->desc=$request->desc;
            $data->save();

            return response()->json([
                'message'=>'Grade karyawan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=GradeEntity::find($id);
            $data->is_deleted=1;
            $data->save();



            return response()->json([
                'message'=>'Grade berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\Jurnal;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\StockOpname;
use App\Models\SennaToko\StockOpnameDetail;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Inventory\StockAdjustmentUtil;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\StockAdjustmentEntity;
use Modules\Merchant\Entities\Toko\StockOpnameEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Ramsey\Uuid\Uuid;

class StockAdjustmentController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('inv/stock-adjustment')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\StockAdjustmentController@data')
                    ->name('merchant.toko.stock-adjustment.data');
                Route::get('/detail/{id}', 'Toko\StockAdjustmentController@detail')
                    ->name('merchant.toko.stock-adjustment.detail');

                Route::get('/get-warehouse', 'Toko\StockAdjustmentController@getWarehouse')
                    ->name('merchant.toko.stock-adjustment.get-warehouse');
                Route::get('/add', 'Toko\StockAdjustmentController@add')
                    ->name('merchant.toko.stock-adjustment.add');
                Route::post('/save', 'Toko\StockAdjustmentController@save')
                    ->name('merchant.toko.stock-adjustment.save');
                Route::post('/data-table', 'Toko\StockAdjustmentController@dataTable')
                    ->name('merchant.toko.stock-adjustment.datatable');
                Route::post('/reload-data', 'Toko\StockAdjustmentController@reloadData')
                    ->name('merchant.toko.stock-adjustment.reload-data');
                Route::post('/export-data', 'Toko\StockAdjustmentController@exportData')
                    ->name('merchant.toko.stock-adjustment.export-data');
                Route::post('/delete', 'Toko\StockAdjustmentController@delete')
                    ->name('merchant.toko.stock-adjustment.delete');
            });
    }

    public function data()
    {

        $params=[
            'title'=>'Penyesuaian Stok',
            'tableColumns'=>StockAdjustmentEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'staff'=>MerchantStaff::select([
                'u.id',
                'u.fullname',
                'u.email',
                'r.name as role_name'
            ])
                ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->where('m.id',merchant_id())
                ->where('md_merchant_staff.is_deleted',0)
                ->where('md_merchant_staff.is_non_employee',0)
                ->get(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'add'=>(get_role()==3)?StockAdjustmentEntity::add(true):
                StockAdjustmentEntity::add(true),

        ];
        return view('merchant::toko.stock-adjustment.data',$params);
    }

    public function add(Request $request)
    {

        $id=$request->id;

        if(!is_null($id))
        {
            $data=StockAdjustmentEntity::find($id);
        }else{
            $data=new StockAdjustmentEntity();
        }
        $params=[
            'title'=>is_null($id)?'Tambah Data':'Edit Data',
            'data'=>$data,
            'warehouse'=>MerchantWarehouse::select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_merchant_inv_warehouses.id)'),
                'md_merchant_inv_warehouses.id',
                'md_merchant_inv_warehouses.name as nama_gudang',
                'md_merchant_inv_warehouses.desc as deskripsi',
                'md_merchant_inv_warehouses.is_default'
            ])
                ->join('md_merchants as m','m.id','md_merchant_inv_warehouses.md_merchant_id')
                ->where('m.id',$data->md_merchant_id)
                ->where('md_merchant_inv_warehouses.is_deleted',0)
                ->get(),
            'idWh'=>$request->warehouse,

        ];
        if(!is_null($data->is_draf) && $data->is_draf==1)
        {
            return view('merchant::toko.stock-adjustment.add-publish',$params);

        }else{
            return view('merchant::toko.stock-adjustment.add',$params);

        }
    }

    public function dataTable(Request $request)
    {
        return StockAdjustmentEntity::dataTable($request);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $merchantId=$request->md_merchant_id;
            $merchant=Merchant::find($merchantId);
            $userId=(get_is_staff()==0)?user_id():get_staff_id();

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'stock_opnames',1,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $date = new \DateTime($request->created_at_by);

            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            $id=$request->id;

            if(!is_null($id))
            {
                $data=StockOpname::find($id);
                $data->desc=$request->desc;
                $data->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
                $data->is_draf=$request->is_draf;
                $data->save();
                StockOpnameDetail::where('sc_stock_opname_id',$data->id)->delete();

            }else{
                $data= new StockOpname();
                $data->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
                $data->md_merchant_id=$merchantId;
                $data->md_user_id_created=(get_is_staff()==0)?$userId:get_staff_id();
                $data->desc=$request->desc;
                $data->code=CodeGenerator::generalCode('SA',$merchantId);
                $data->sync_id=Uuid::uuid4()->toString();
                $data->created_at_by=$request->created_at_by;
                $data->timezone=$request->_timezone;
                $data->is_deleted=0;
                $data->inv_warehouse_id=$request->inv_warehouse_id;
                $data->is_adjustment_stock=1;
                $data->is_draf=$request->is_draf;
                $data->save();
            }
            $stock=$request->stock;
            $stockReal=$request->stock_real;
            $stockDiff=$request->diff_stock;
            $sc_product_id=$request->sc_product_id;
            $stockInv=[];
            $avg=$request->average_price;
            $unitName=$request->unit_name;
            $isMultiUnit=$request->is_multi_unit;
            $multiUnitId=$request->multi_unit_id;
            $valueConversion=$request->nilai_konversi;
            $jsonMultiUnit=$request->json_multi_unit;
            $unitList=$request->unit_list;
            $unitOrigin=$request->unit_origin;


            $warehouse=MerchantWarehouse::find($request->inv_warehouse_id);

            if(empty($stock))
            {
                return response()->json([
                    'message' => "Data penyesuaian stock tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            $details=[];
            $productId="";

            foreach ($sc_product_id as $key => $v)
            {
                $productId.=($key==0)?"".$sc_product_id[$key]."":",".$sc_product_id[$key]."";
            }

            $products=collect(DB::select("
            SELECT
                p.*
            FROM
                sc_products p
            WHERE
                p.id in($productId)
                and p.is_deleted = 0
            "));

            $arrayProductJurnal=[];

            $arrayOfInvs=[];
            $count=0;
            $updateStockProduct="";

            foreach ($stock as $key =>$item)
            {
                if($unitList[$key]=='-1'|| $unitList[$key]==-1){
                    return response()->json([
                        'message' => "Ada produk yang belum memilih satuan!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                $avp=strtr($avg[$key], array('.' => '', ',' => '.'));

                if($stockDiff[$key]<0)
                {
                    if($avp==0)
                    {
                        return response()->json([
                            'message' => "Harga rata-rata tidak boleh kosong untuk selisih kurang dari 0 !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }

                if($stockDiff[$key]>0){
                    if($stock[$key]==0){
                        return response()->json([
                            'message' => "Stok produk ini masih 0, tidak bisa mencatat selisih kurang !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }

                $p=$products->firstWhere('id',$sc_product_id[$key]);

                if($isMultiUnit[$key]==0){
                    $details[]=[
                        'physical_count'=>$stockReal[$key],
                        'record_stock'=>$stock[$key],
                        'difference_amount'=>$stockDiff[$key],
                        'sc_product_id'=>$sc_product_id[$key],
                        'sc_stock_opname_id'=>$data->id,
                        'average_price'=>($stockDiff[$key]<0)?$avp:0,
                        'created_at'=>$request->created_at_by,
                        'updated_at'=>$request->created_at_by,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$p->name.' '.$p->code,
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'value_conversion'=>1,
                        'multi_unit_id'=>null,
                        'json_multi_unit'=>null,
                        'unit_name_origin_display'=>$unitOrigin[$key]
                    ];
                }else{
                    $details[]=[
                        'physical_count'=>$stockReal[$key],
                        'record_stock'=>$stock[$key],
                        'difference_amount'=>$stockDiff[$key],
                        'sc_product_id'=>$sc_product_id[$key],
                        'sc_stock_opname_id'=>$data->id,
                        'average_price'=>($stockDiff[$key]<0)?$avp:0,
                        'created_at'=>$request->created_at_by,
                        'updated_at'=>$request->created_at_by,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$p->name.' '.$p->code,
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$stockReal[$key]*$valueConversion[$key],
                        'value_conversion'=>$valueConversion[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                        'unit_name_origin_display'=>$unitOrigin[$key]
                    ];
                }


                if($data->is_draf==0)
                {
                    if($stockDiff[$key]<0)
                    {

                        array_push($stockInv,[
                            'sync_id'=>$data->id.Uuid::uuid4()->toString(),
                            'sc_product_id'=>$p->id,
                            'total'=>abs($stockDiff[$key]),
                            'record_stock'=>($warehouse->is_default==1)?($p->stock+abs($stockDiff[$key])):$p->stock,
                            'created_by'=>$userId,
                            'selling_price'=>$p->selling_price,
                            'residual_stock'=>abs($stockDiff[$key]),
                            'purchase_price'=>$avp/$valueConversion[$key],
                            'timezone'=>$request->_timezone,
                            'type'=>StockInventory::IN,
                            'inv_warehouse_id'=>$request->inv_warehouse_id,
                            'transaction_action'=>'Penambahan Stok '.$p->name. ' Dari Penyesuaian Stok '.(is_null($data->second_code)?$data->code:$data->second_code),
                            'stockable_type'=>'App\Models\SennaToko\StockOpname',
                            'stockable_id'=>$data->id,
                            'created_at'=>$request->created_at_by,
                            'updated_at'=>$request->created_at_by,
                            'unit_name'=>$unitName[$key],
                            'product_name'=>$p->name.' '.$p->code
                        ]);

                        array_push($arrayProductJurnal,[
                            'sc_product_id'=>$p->id,
                            'inv_id'=>$p->inv_id,
                            'name'=>$p->name,
                            'amount'=>$avp*(abs($stockDiff[$key])/$valueConversion[$key]),
                            'type'=>'plus'
                        ]);

                        if ($warehouse->is_default == 1) {
                            $stockP=$p->stock+(abs($stockDiff[$key]));
                            $updateStockProduct.=($count==0)?"(".$p->id.",".$stockP.")":",(".$p->id.",".$stockP.")";
                        }

                        $count++;

                    }else{

                        $subtraction=StockAdjustmentUtil::subtraction($p->id,$stockDiff[$key],$warehouse);

                        if($subtraction==false){
                            return response()->json([
                                'message' => "Terjadi kesalahan pengurangan stok gagal dilakukan !",
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);

                        }

                        if(!empty($subtraction['invs'])){

                            foreach ($subtraction['invs'] as $j =>$jj){
                                $arrayOfInvs[]=$jj;
                            }
                        }

                        array_push($stockInv, [
                            'sync_id'=>$data->id.Uuid::uuid4()->toString(),
                            'sc_product_id'=>$p->id,
                            'total'=>$stockDiff[$key],
                            'record_stock'=>($warehouse->is_default==1)?($p->stock-(abs($stockDiff[$key]))):$p->stock,
                            'created_by'=>$userId,
                            'selling_price'=>$p->selling_price,
                            'residual_stock'=>$stockDiff[$key],
                            'purchase_price'=>$p->purchase_price,
                            'type'=>StockInventory::OUT,
                            'timezone'=>$request->_timezone,
                            'inv_warehouse_id'=>$request->inv_warehouse_id,
                            'transaction_action'=>'Pengurangan Stok '.$p->name. ' Dari Penyesuaian Stok '.(is_null($data->second_code)?$data->code:$data->second_code),
                            'stockable_type'=>'App\Models\SennaToko\StockOpname',
                            'stockable_id'=>$data->id,
                            'created_at'=>$request->created_at_by,
                            'updated_at'=>$request->created_at_by,
                            'unit_name'=>$unitName[$key],
                            'product_name'=>$p->name.' '.$p->code
                        ]);

                        array_push($arrayProductJurnal,[
                            'sc_product_id'=>$p->id,
                            'inv_id'=>$p->inv_id,
                            'name'=>$p->name,
                            'amount'=>$subtraction['totalAmountDecrement'],
                            'type'=>'minus'
                        ]);
                    }
                }

            }



            $data->invs=json_encode($arrayOfInvs);
            $data->save();

            if($data->is_draf==0)
            {
                StockInventory::insert($stockInv);

            }
            StockOpnameDetail::insert($details);



            if($data->is_draf==0)
            {
                if($updateStockProduct!=""){
                    DB::statement("
                        update sc_products as t set
                                stock = c.column_a
                            from (values
                                ".$updateStockProduct."
                            ) as c(column_b, column_a)
                            where c.column_b = t.id;
                        ");
                }
            }


            DB::commit();

            if($data->is_draf==0)
            {
                if(CoaProductUtil::createStockAdjustmentBulk($userId,$data,$merchant,$arrayProductJurnal)==false)
                {
                    DB::rollBack();
                    return response()->json([
                        'message' => "Terjadi kesalahan, jurnal persediaan gagal dibuat !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }


            return response()->json([
                'message' => "Data penyesuaian stok berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.stock-adjustment.data')
            ]);

        }catch (\Exception $e)
        {

            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => '',
                'other'=>$e
            ]);
        }
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (StockAdjustmentEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Penyesuaian Stok',
            'searchKey' => $searchKey,
            'tableColumns'=>StockAdjustmentEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.stock-adjustment.list',$params);

    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $data = StockAdjustmentEntity::getDataForDataTable();
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            $searchKey = $request->input('search_key');
            $searchFilter = $request->session()->get($searchKey, []);
            $searchFilter = is_array($searchFilter) ? $searchFilter : [];

            $getBranch=MerchantUtil::getBranch(merchant_id());

            if($request->md_merchant_id=='-1'){

                $data->whereIn('m.id',$getBranch);

            }else{
                foreach (StockAdjustmentEntity::getFilterMap() as $key => $field) {
                    if (isset($searchFilter[$key]) && $searchFilter[$key] != '-') {
                        $data->where([$field => $searchFilter[$key]]);
                    }
                }
            }


            $data->whereRaw("
            sc_stock_opnames.created_at_by::date between '$startDate' and '$endDate'
            ");


            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Tanggal Penyesuaian Stok $startDate sd $endDate ");
            $header = [
                'No',
                'Kode',
                'Cabang',
                'Gudang',
                'Produk',
                'Stok',
                'Satuan',
                'Tipe Penyesuaian',
                'Waktu Pencatatan',
                'Zona Waktu',
                'Nama Pencatat'
            ];
            $col = 'A';
            foreach ($header as $v) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '2', strtoupper($v));
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->getFont()->setBold( true );
                $col++;
            }
            $num = 2;
            $no=1;
            foreach ($data->get() as $key =>$item)
            {
                foreach ($item->getDetail as $n =>$j) {
                    $unitName= (is_null($j->getProduct->getUnit))?'':$j->getProduct->getUnit->name;
                    $dataShow = [
                        $no,
                        $item->kode,
                        $item->outlet,
                        $item->gudang,
                        is_null($j->product_name)?$j->getProduct->name.' '.$j->getProduct->code : $j->product_name,
                        abs($j->difference_amount)/$j->value_conversion,
                        is_null($j->unit_name)?$unitName:$j->unit_name,
                        ($j->difference_amount<0)?'Penambahan Stok':'Pengurangan Stok',
                        $item->waktu_penyesuaian,
                        getTimeZoneName($item->zona_waktu),
                        $item->nama_pencatat
                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                    $no++;
                }
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-stock_adjustment_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
                    <div id='successExport' class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        let success = document.querySelector('#successExport');
                        if(success){
                            toastForSaveData('Data berhasil diexport!','success',true,'',false);
                        }
                    </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Terjadi kesalahan di server, data gagal di export !','danger',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";
        }

    }

    public function detail($id)
    {
        try{
            $data=StockOpname::with(['getDetail'=>function($query){
                $query->with('getProduct.getUnit');
            }])
                ->where('id',$id)
                ->first();

//            if($data->md_merchant_id != merchant_id()){
//                return redirect()->route('pages.not-allowed');
//            }

            $params=[
                'title'=>'Detail',
                'data'=>$data
            ];
            return view('merchant::toko.stock-adjustment.detail',$params);

        }catch (\Exception $e)
        {
            return view('pages.500');
        }

    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=StockOpname::find($id);
            $data->is_deleted=1;
            $data->save();
            DB::commit();

            return response()->json([
                'message' => "Data penyesuaian stok berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }


    public function getWarehouse(Request  $request)
    {

        $warehouse=WarehouseEntity::getDataForDataTable()->where('m.id',$request->merchant_id)
            ->where('md_merchant_inv_warehouses.is_deleted',0)->get();

        $response =[];
        foreach($warehouse as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>is_null($item->tipe_gudang)?$item->nama_gudang:$item->nama_gudang.'-( '.$item->tipe_gudang.' )' ,
            ];
        }

        return response()->json($response);
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;

class CategorySpecialistController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/category-specialist')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.category-specialist.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.category-specialist.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.toko.category-specialist.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.category-specialist.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.category-specialist.delete');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Kategori Spesialis',
            'tableColumns'=>CategorySpecialistEntity::dataTableColumns(),
            'add'=>(get_role()==3)?CategorySpecialistEntity::add(true):
                CategorySpecialistEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.category-specialist.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return CategorySpecialistEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public function add(Request $request)
    {
        $id=$request->id;
        if(!is_null($id)){

            $data=CategorySpecialistEntity::find($id);
        }else{
            $data=new CategorySpecialistEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::toko.master-data.category-specialist.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=CategorySpecialistEntity::find($id);
            }else{
                $data=new CategorySpecialistEntity();
                $data->md_user_id=user_id();

            }

            $check = CategorySpecialistEntity::where("code", $request->code)
                                    ->where('is_deleted', 0)
                                    ->where('md_user_id', user_id())
                                    ->where('id', '!=', $id)
                                    ->first();

            if(!is_null($check) && !is_null($request->code)){
                return response()->json([
                    'message'=>'Kode sudah digunakan !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            $data->code = $request->code;
            $data->name=$request->name;
            $data->description=$request->description;
            $data->is_specialist = 1;
            $data->save();

            return response()->json([
                'message'=>'Kategori spesialis berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=CategorySpecialistEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'Kategori spesialis berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }
}

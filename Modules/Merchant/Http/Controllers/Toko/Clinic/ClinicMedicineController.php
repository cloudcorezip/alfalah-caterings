<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Clinic;

use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\Role;
use App\Models\MasterData\User;
use App\Models\SennaToko\ProductCategory;
use App\Models\MasterData\Unit;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\ProductionOfGoodDetail;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\Clinic\ClinicMedicineEntity;
use Illuminate\Support\Facades\Storage;
use Image;
use Ramsey\Uuid\Uuid;


class ClinicMedicineController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('clinic/medicine')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/',  [static::class, 'index'])
                    ->name('merchant.toko.clinic.medicine.index');
                Route::get('/add',  [static::class, 'add'])
                    ->name('merchant.toko.clinic.medicine.add');
                Route::post('/data-table',  [static::class, 'dataTable'])
                    ->name('merchant.toko.clinic.medicine.datatable');
                Route::post('/save',  [static::class, 'save'])
                    ->name('merchant.toko.clinic.medicine.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.clinic.medicine.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Data Obat',
            'tableColumns'=>ClinicMedicineEntity::dataTableColumns(),
            'add'=>(get_role()==2 || get_role()==3)?ClinicMedicineEntity::add(true):
                ClinicMedicineEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.clinic.medicine.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ClinicMedicineEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=2 || get_role()!=3)?false:true);
    }

    public function add(Request $request)
    {
        $id = $request->id;
        if(!is_null($id)){
            $data = ClinicMedicineEntity::find($id);
            if(is_null($data)){
                return abort(404);
            }
        } else {
            $data = new ClinicMedicineEntity();
        }

        $params = [
            "title" => (is_null($id))? 'Tambah Data Obat':'Edit Data Obat',
            "data" => $data,
            "code" => 'PR'.merchant_id().rand(0,100000).date('Ymd'),
            'category'=>ProductCategory::where('md_user_id',user_id())
                                        ->where('is_deleted',0)->get(),
        ];

        return view("merchant::toko.clinic.medicine.form", $params);
    }

    public function save(Request $request)
    {
        try {
            $warehouse=parent::getGlobalWarehouse(merchant_id());
            if($warehouse==false)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan,inisiasi gudang persediaan gagal dibuat !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();

            $id=$request->id;

            if(!is_null($id)){

                $data=ClinicMedicineEntity::find($id);
                $OldIsWithStock=$data->is_with_stock;
                if($request->is_with_stock==0){
                    if( count(PurchaseOrderDetail::where('sc_product_id',$id)->get())<1 &&
                        count(SaleOrderDetail::where('sc_product_id',$id)->get())<1 &&
                        count(StockInventory::where('sc_product_id',$id)->get())<1){
                            CoaDetail::where('ref_external_id',$id)->where('ref_external_type',1)->delete();
                    }
                }
                CoaDetail::where('ref_external_id',$id)->where('name', 'like', '%Persediaan%')->update(['name' => 'Persediaan Produk '.$request->name]);
                CoaDetail::where('ref_external_id',$id)->where('name', 'like', '%Harga Pokok Penjualan%')->update(['name' => 'Harga Pokok Penjualan Produk '.$request->name]);

            }else{
                $data=new ClinicMedicineEntity();
                $data->name=$request->name;
            }

            $validator = Validator::make($request->all(), (is_null($id))?$data->ruleCreateWeb:$data->ruleUpdateWeb);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            $unit=Unit::whereRaw("lower(name) like '".strtolower($request->md_unit_id)."' ")->first();
            if(is_null($unit))
            {
                $unit=new Unit();
                $unit->name=$request->md_unit_id;
                $unit->save();
            }

            $selling_price=strtr($request->selling_price, array('.' => '', ',' => '.'));
            $purchase_price=strtr($request->purchase_price, array('.' => '', ',' => '.'));

            $data->code = $request->code;
            $data->name = $request->name;
            $data->is_with_stock = 1;
            $data->purchase_price = $purchase_price;
            $data->selling_price = $selling_price;
            $data->description = $request->description;
            $data->md_user_id = user_id();
            $data->md_sc_category_id = null;
            $data->profit_prosentase = 0;
            $data->md_sc_product_type_id = 1;
            $data->sc_product_category_id=$request->sc_product_category_id;

            $data->md_unit_id=$unit->id;

            if(is_null($id))
            {
                $data->unique_code=Uuid::uuid4()->toString();
            }

            $data->is_deleted=0;
            $data->save();

            if(is_null($id))
            {
                if($data->is_with_stock==1)
                {
                    $coaProduct=CoaProductUtil::initialCoaProduct(merchant_id(),$data,1);
                    if($coaProduct==false)
                    {
                        return response()->json([
                            'message' => "Terjadi kesalahan,inisiasi akun persediaan gagal dibuat !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                    $data->inv_id=$coaProduct['inv_id'];
                    $data->hpp_id=$coaProduct['hpp_id'];
                    $data->save();
                }
            }else{
                if($request->is_with_stock!=$OldIsWithStock && $request->is_with_stock==1){
                    $coaProduct=CoaProductUtil::initialCoaProduct(merchant_id(),$data,1);
                    if($coaProduct==false)
                    {
                        return response()->json([
                            'message' => "Terjadi kesalahan,inisiasi akun persediaan gagal dibuat !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }

                    $data->inv_id=$coaProduct['inv_id'];
                    $data->hpp_id=$coaProduct['hpp_id'];
                    $data->save();

                }
            }

            DB::commit();

            return response()->json([
                'message' => "Data obat berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.clinic.medicine.index')
            ]);
        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;

            if( PurchaseOrderDetail::where('sc_product_id',$id)
                                    ->join('sc_purchase_orders as s','s.id','sc_purchase_order_details.sc_purchase_order_id')
                                    ->where('s.is_deleted',0)
                                    ->count()<1 &&
                SaleOrderDetail::where('sc_product_id',$id)
                                ->join('sc_sale_orders as s','s.id','sc_sale_order_details.sc_sale_order_id')
                                ->join('sc_products as p','p.id','sc_sale_order_details.sc_product_id')
                                ->where('s.is_deleted',0)
                                ->count()<1 &&
                StockInventory::where('sc_product_id',$id)
                                ->where('is_deleted',0)
                                ->where('type','in')
                                ->where('residual_stock','>',0)
                                ->count()<1 &&
                ProductionOfGoodDetail::where('sc_inv_production_of_good_details.sc_product_id',$id)
                                ->join('sc_inv_production_of_goods as s','s.id','sc_inv_production_of_good_details.sc_inv_production_of_good_id')->where('s.is_deleted',0)
                                ->count()<1
                                ){
                $delete=true;

            }else{
                $delete=false;
            }
            
            if($delete==false)
            {
                return response()->json([
                    'message' => "Data layanan tidak bisa dihapus,karena telah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }else{
                DB::beginTransaction();
                $data=ClinicMedicineEntity::find($id);
                $data->is_deleted=1;
                $data->save();
                if($data->md_sc_product_type_id==3){
                    $raw=Material::where('child_sc_product_id',$id)->first();
                    $raw->is_deleted=1;
                    $raw->save();
                }
                $coa=CoaDetail::where('ref_external_id',$data->id)->where('ref_external_type',1)->get();
                foreach ($coa as $key =>$item)
                {
                    $item->is_deleted=1;
                    $item->save();
                    $coaCfD=CoaCashflowFormatDetail::where('acc_coa_detail_id',$item->id)->first();
                    if(!is_null($coaCfD))
                    {
                        $coaCfD->is_deleted=1;
                        $coaCfD->save();
                    }
                }
                DB::commit();

                return response()->json([
                    'message' => "Data berhasil dihapus !",
                    'type' => 'success',
                    'is_modal' => true,
                    'redirect_url' => ''
                ]);
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }
}

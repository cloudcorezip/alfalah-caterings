<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\Jurnal;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\SennaToko\ConsignmentGood;
use App\Models\SennaToko\ConsignmentGoodDetail;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\StockOpname;
use App\Models\SennaToko\StockOpnameDetail;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Inventory\ConsignmentUtil;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\Merchant\Entities\Toko\ConsignmentEntity;
use Modules\Merchant\Entities\Toko\StockAdjustmentEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Modules\Merchant\Entities\Toko\TransferStockEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Ramsey\Uuid\Uuid;

class ConsignmentController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('inv/consignment')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\ConsignmentController@data')
                    ->name('merchant.toko.consignment.data');
                Route::get('/detail/{id}', 'Toko\ConsignmentController@detail')
                    ->name('merchant.toko.consignment.detail');

                Route::get('/get-warehouse', 'Toko\ConsignmentController@getWarehouse')
                    ->name('merchant.toko.consignment.get-warehouse');
                Route::get('/add', 'Toko\ConsignmentController@add')
                    ->name('merchant.toko.consignment.add');
                Route::post('/save', 'Toko\ConsignmentController@save')
                    ->name('merchant.toko.consignment.save');
                Route::post('/data-table', 'Toko\ConsignmentController@dataTable')
                    ->name('merchant.toko.consignment.datatable');
                Route::post('/reload-data', 'Toko\ConsignmentController@reloadData')
                    ->name('merchant.toko.consignment.reload-data');
                Route::post('/export-data', 'Toko\ConsignmentController@exportData')
                    ->name('merchant.toko.consignment.export-data');
                Route::post('/delete', 'Toko\ConsignmentController@delete')
                    ->name('merchant.toko.consignment.delete');
            });
    }

    public function data()
    {

        $params=[
            'title'=>'Penerimaan Konsinyasi',
            'tableColumns'=>ConsignmentEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'add'=>(get_role()==3)?ConsignmentEntity::add(true):
                ConsignmentEntity::add(true),

        ];
        return view('merchant::toko.consignment.data',$params);
    }

    public function add(Request $request)
    {
        $params=[
            'title'=>'Tambah Data',
            'data'=>new ConsignmentEntity(),
            'supplier'=>SupplierEntity::listOption(),
            'idWh'=>$request->warehouse,

        ];
        return view('merchant::toko.consignment.add',$params);
    }

    public function dataTable(Request $request)
    {
        return ConsignmentEntity::dataTable($request);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $merchantId=$request->md_merchant_id;
            $merchant=Merchant::find($merchantId);
            $coaConsignment=ConsignmentUtil::generateCoaConsignment($merchantId);
            $date = new \DateTime($request->time_received);

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'consignment',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $checkPackageSubscription=MerchantUtil::checkUrl($merchantId);

            if($checkPackageSubscription['status']==true)
            {
                return response()->json([
                    'message'=>$checkPackageSubscription['message'],
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            if($coaConsignment==false){

                return response()->json([
                    'message' => "Pembuatan akun penerimaan konsinyasi terjadi kesalahan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if($request->sc_supplier_id=='-1')
            {
                return response()->json([
                    'message' => "Supplier atau pemasok  belum dipilih !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $userId=Merchant::find($merchantId)->md_user_id;
            $data=new ConsignmentGood();

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/consignment/attachment/';
            if($request->hasFile('attachment_file'))
            {
                $file=$request->file('attachment_file');
                $fileName= Uuid::uuid4()->toString().'.'.$file->getClientOriginalExtension();
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }
            $data->md_merchant_id=$merchantId;
            $data->sc_supplier_id=$request->sc_supplier_id;
            $data->created_by=(get_is_staff()==0)?$userId:get_staff_id();
            $data->note=$request->note;
            $data->code=CodeGenerator::generalCode('BK',$request->md_merchant_id);
            $data->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $data->time_received=$request->time_received;
            $data->create_time=date('Y-m-d H:i:s');
            $data->timezone=$request->_timezone;
            $data->is_deleted=0;
            $data->inv_warehouse_id=$request->inv_warehouse_id;
            $data->attachment_file=$fileName;
            $data->save();
            $quantity=$request->quantity;
            $sc_product_id=$request->sc_product_id;
            $prices=$request->price;
            $unitName=$request->unit_name;
            $warehouse=MerchantWarehouse::find($request->inv_warehouse_id);
            $isMultiUnit=$request->is_multi_unit;
            $multiUnitId=$request->multi_unit_id;
            $valueConversation=$request->nilai_konversi;
            $jsonMultiUnit=$request->json_multi_unit;
            $coaHppId=$request->coa_hpp_id;
            $coaInvId=$request->coa_inv_id;
            $unitList=$request->unit_list;

            if(empty($quantity))
            {
                return response()->json([
                    'message' => "Data penerimaan konsinyasi tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            $details=[];
            $total=0;

            $productId="";

            foreach ($sc_product_id as $key => $v)
            {
                $productId.=($key==0)?"".$sc_product_id[$key]."":",".$sc_product_id[$key]."";
            }

            $products=collect(DB::select("
            SELECT
                p.*
            FROM
                sc_products p
            WHERE
                p.id in($productId)
                and p.is_deleted = 0
            "));

            $updateStockProduct="";
            $stockIn=[];

            $invId=[];

            foreach ($quantity as $key =>$item)
            {

                if($unitList[$key]=='-1' || $unitList[$key]==-1){
                    return response()->json([
                        'message' => "Ada produk yang belum memilih satuan!!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                $price=strtr($prices[$key], array('.' => '', ',' => '.'));

                if($quantity[$key]<0)
                {
                    return response()->json([
                        'message' => "Jumlah diterima tidak boleh kurang dari 0 !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                if($price<0)
                {
                    return response()->json([
                        'message' => "Harga  tidak boleh kurang dari 1 !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                $product=$products->firstWhere('id',$sc_product_id[$key]);

                if($isMultiUnit[$key]==0){
                    $details[]=[

                        'sc_product_id'=>$sc_product_id[$key],
                        'sc_consignment_good_id'=>$data->id,
                        'price'=>$price,
                        'quantity'=>$quantity[$key],
                        'sub_total'=>$price*$quantity[$key],
                        'created_at'=>$request->time_received,
                        'updated_at'=>$request->time_received,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$product->name.' '.$product->code,
                        'is_multi_unit'=>0,
                        'multi_unit_id'=>null,
                        'multi_quantity'=>0,
                        'value_conversion'=>1,
                        'coa_hpp_id'=>$coaHppId[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'json_multi_unit'=>null
                    ];
                }else{
                    $details[]=[

                        'sc_product_id'=>$sc_product_id[$key],
                        'sc_consignment_good_id'=>$data->id,
                        'price'=>$price,
                        'quantity'=>$quantity[$key],
                        'sub_total'=>$price*$quantity[$key],
                        'created_at'=>$request->time_received,
                        'updated_at'=>$request->time_received,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$product->name.' '.$product->code,
                        'is_multi_unit'=>1,
                        'multi_unit_id'=>$multiUnitId[$key],
                        'multi_quantity'=>$quantity[$key]*$valueConversation[$key],
                        'value_conversion'=>$valueConversation[$key],
                        'coa_hpp_id'=>$coaHppId[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key]))
                    ];
                }



                $total+=$price*$quantity[$key];

                if ($warehouse->is_default == 1) {
                    $stockP=$product->stock+$quantity[$key]*$valueConversation[$key];
                    $updateStockProduct.=($key==0)?"(".$product->id.",".$stockP.")":",(".$product->id.",".$stockP.")";
                }


                $stockIn[]=[
                    'sync_id'=>$product->id.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$product->id,
                    'total'=>$quantity[$key]*$valueConversation[$key],
                    'inv_warehouse_id'=>$request->inv_warehouse_id,
                    'record_stock'=>$product->stock,
                    'created_by'=>$data->created_by,
                    'selling_price'=>$product->selling_price,
                    'purchase_price'=>$price/$valueConversation[$key],
                    'residual_stock'=>$quantity[$key]*$valueConversation[$key],
                    'type'=>StockInventory::IN,
                    'transaction_action'=> 'Penambahan Stok '.$product->name. ' Dari Penerimaan Konsinyasi Dengan Code '.(is_null($data->second_code)?$data->code:$data->code),
                    'stockable_type'=>'App\Models\SennaToko\ConsignmentGood',
                    'stockable_id'=>$data->id,
                    'created_at'=>$request->time_received,
                    'updated_at'=>$request->time_received,
                    'unit_name'=>$unitName[$key],
                    'product_name'=>$product->name.' '.$product->code,
                    'timezone'=>$data->timezone
                ];

                $invId[]=[
                    'inv_id'=>$product->inv_id,
                    'amount'=>$quantity[$key]*$price
                ];
            }

            $data->total=$total;
            $data->save();

            ConsignmentGoodDetail::insert($details);
            StockInventory::insert($stockIn);

            if($updateStockProduct!=""){
                DB::statement("
                        update sc_products as t set
                                stock = c.column_a
                            from (values
                                ".$updateStockProduct."
                            ) as c(column_b, column_a)
                            where c.column_b = t.id;
                        ");
            }

            if(ConsignmentUtil::insertConsignmentJurnal($merchant,$data,$coaConsignment,$invId,$total)==false){
                return response()->json([
                    'message' => "Terjadi kesalahan saat melakukan proses penulisan jurnal penerimaan konsinyasi !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }


            DB::commit();

            return response()->json([
                'message' => "Data penerimaan konsinyasi berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.consignment.data')
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=ConsignmentGood::find($id);

            if(is_null($data))
            {
                return response()->json([
                    'message' => "Data penerimaan konsinyasi tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $data->is_deleted=1;
            $data->save();

            $stock=StockInventory::where('stockable_type','App\Models\SennaToko\ConsignmentGood')
                ->where('stockable_id',$data->id);

            foreach ($stock->get() as $i =>$s)
            {
                if($s->residual_stock<$s->total)
                {
                    return response()->json([
                        'message' => "Data stok penerimaan konsinyasi sudah digunakan, kamu tidak dapat menghapus data  !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                if($s->getWarehouse->is_default==1)
                {
                    $s->getProduct->stock-=$s->residual_stock;
                    $s->getProduct->save();
                }
            }

            Jurnal::where(['ref_code'=>$data->code,
                'external_ref_id'=>$data->id,
                'md_merchant_id'=>$data->md_merchant_id
            ])
                ->update(['is_deleted'=>1]);

            $stock->delete();


            DB::commit();

            return response()->json([
                'message' => "Data penerimaan konsinyasi berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ConsignmentEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Penerimaan Konsinyasi',
            'searchKey' => $searchKey,
            'tableColumns'=>ConsignmentEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.consignment.list',$params);

    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $data = ConsignmentEntity::getDataForDataTable();
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            $searchKey = $request->input('search_key');
            $searchFilter = $request->session()->get($searchKey, []);
            $searchFilter = is_array($searchFilter) ? $searchFilter : [];

            $getBranch=MerchantUtil::getBranch(merchant_id());

            if($request->md_merchant_id=='-1'){

                $data->whereIn('m.id',$getBranch);

            }else{
                foreach (ConsignmentEntity::getFilterMap() as $key => $field) {
                    if (isset($searchFilter[$key]) && $searchFilter[$key] != '-') {
                        $data->where([$field => $searchFilter[$key]]);
                    }
                }
            }

            $data->whereRaw("
            sc_consignment_goods.time_received::date between '$startDate' and '$endDate'
            ")->where('sc_consignment_goods.is_deleted',0);


            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Tanggal Penerimaan Konsinyasi $startDate sd $endDate");


            $header = [
                'No',
                'Kode',
                'Cabang',
                'Gudang',
                'Produk',
                'Jumlah Diterima',
                'Satuan',
                'Harga',
                'Sub Total',
                'Waktu Penerimaan',
                'Zona Waktu',
                'Nama Pencatat'
            ];
            $col = 'A';

            foreach ($header as $v) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '2', strtoupper($v));
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->getFont()->setBold( true );
                $col++;
            }
            $num = 2;
            $no=1;
            foreach ($data->get() as $key =>$item)
            {
                foreach ($item->getDetail as $n =>$j) {
                    $unitName= (is_null($j->getProduct->getUnit))?'':$j->getProduct->getUnit->name;
                    $dataShow = [
                        $no,
                        $item->kode,
                        $item->outlet,
                        $item->gudang,
                        is_null($j->product_name)?$j->getProduct->name.' '.$j->getProduct->code : $j->product_name,
                        $j->quantity,
                        is_null($j->unit_name)?$unitName:$j->unit_name,
                        rupiah($j->price),
                        rupiah($j->sub_total),
                        $item->waktu_penerimaan,
                        getTimeZoneName($item->zona_waktu),
                        $item->nama_pencatat
                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                    $no++;
                }
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-consignment_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
                    <div id='successExport' class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        let success = document.querySelector('#successExport');
                        if(success){
                            toastForSaveData('Data berhasil diexport!','success',true,'',false);
                        }
                    </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Terjadi kesalahan di server, data gagal di export !','danger',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";
        }

    }

    public function detail($id)
    {
        try{
            $data=ConsignmentGood::with(['getDetail'=>function($query){
                $query->with('getProduct.getUnit');
            }])
                ->where('id',$id)
                ->first();
            $params=[
                'title'=>'Detail',
                'data'=>$data
            ];
            return view('merchant::toko.consignment.detail',$params);

        }catch (\Exception $e)
        {
            return view('pages.500');
        }

    }



}

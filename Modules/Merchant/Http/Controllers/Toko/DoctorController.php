<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\Role;
use App\Models\MasterData\User;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\HR\DepartementEntity;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Entities\Toko\DoctorEntity;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Illuminate\Support\Facades\Storage;
use Image;
use Modules\Merchant\Entities\Toko\GradeEntity;

class DoctorController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('master-data/doctor')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/',  [static::class, 'index'])
                    ->name('merchant.toko.doctor.index');
                Route::post('/data-table',  [static::class, 'dataTable'])
                    ->name('merchant.toko.doctor.datatable');
                Route::get('/add',  [static::class, 'add'])
                    ->name('merchant.toko.doctor.add');
                Route::post('/save',  [static::class, 'save'])
                    ->name('merchant.toko.doctor.save');
                Route::post('/delete',  [static::class, 'delete'])
                    ->name('merchant.toko.doctor.delete');
            });
    }

    protected function _generateUniqueCode($merchantId, $prefix)
    {
        $code = $prefix.'-'.CodeGenerator::codeGenerator(8);

        $check = DoctorEntity::where('code', $code)
                    ->where('md_merchant_id', $merchantId)
                    ->first();

        if(is_null($check)){
            return $code;
        } else {
            return $this->_generateUniqueCode($merchantId);
        }
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Dokter',
            'tableColumns'=>DoctorEntity::dataTableColumns(),
            'add'=>(get_role()==2 || get_role()==3)?DoctorEntity::add(true):
                DoctorEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.doctor.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return DoctorEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=2 || get_role()!=3)?false:true);
    }

    public function add(Request $request)
    {
        $id = $request->id;
        if(!is_null($id)){
            $data = DoctorEntity::find($id);
            if(is_null($data)){
                return abort(404);
            }

            if($data->getUser->md_role_id != 23){
                return abort(404);
            }

        } else {
            $data = new DoctorEntity();
        }

        $code = $this->_generateUniqueCode(merchant_id(), 'D');
        $params = [
            "title" => (is_null($id))? 'Tambah Data Dokter':'Edit Data Dokter',
            "data" => $data,
            "role" => Role::where("id", ">=", 11)
                        ->orderBy("id", "ASC")->get(),
            "maritalStatus" => generateMaritalStatus(),
            "religion" => generateReligion(),
            "education" => generateEducation(),
            "bloodType" => generateBloodType(),
            "code" => $code,
            "category" => CategorySpecialistEntity::where('is_deleted', 0)
                                            ->where('is_specialist', 1)
                                            ->where('md_user_id', user_id())
                                            ->orderBy('id')
                                            ->get(),
            'gradeOption'=>GradeEntity::listOption(),
            'jobOption'=>JobEntity::listOption(),
            'departementOption'=>DepartementEntity::listOption()
        ];

        return view("merchant::toko.master-data.doctor.form", $params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);

            DB::beginTransaction();
            $id=$request->id;
            if(!is_null($id)){

                $data=DoctorEntity::find($id);
            }else{
                $data=new DoctorEntity();

            }

            $merchant=Merchant::find(merchant_id());
            if(is_null($id))
            {
                if(MerchantUtil::maxStaff(merchant_id())==false)
                {
                    return response()->json([
                        'message'=>trans('custom.max_subs'),
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }
            if($request->email==$merchant->getUser->email)
            {
                return response()->json([
                    'message'=>trans('custom.email_already_use'),
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $checkUser=User::where('email',strtolower($request->email))
                    ->where('md_users.id', '!=', $data->md_user_id)
                    ->first();

            if(!is_null($checkUser))
            {
                return response()->json([
                    'message'=>trans('custom.email_already_use'),
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }


            $destinationPath = 'public/uploads/user/'.user_id().'/image/';

            if($request->hasFile('image'))
            {
                $file=$request->file('image');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('image'))->resize(300,300)->encode($file->getClientOriginalExtension());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, (string) $image);
            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->image;

                }
            }

            if(is_null($id))
            {
                $user=new User();
                $user->fullname=$request->name;
                if($request->is_create_email == 1){
                    $user->email=strtolower($request->email);
                    $user->password=Hash::make($request->password);
                }
                $user->is_active=1;
                $user->is_email_verified=1;
                $user->is_staff=1;
                $user->md_role_id=23;
                $user->is_merchant=1;
                $user->is_active_merchant=1;
                $user->is_verified_merchant=1;
                $user->gender = $request->gender;
                $user->phone_number = $request->phone_number;
                $user->birth_place = $request->birth_place;
                $user->birth_date = $request->birth_date;
                $user->foto = $fileName;
                $user->save();
                $data->md_user_id=$user->id;
            }else{
                $user=User::find($data->md_user_id);
                $user->fullname=$request->name;
                $user->md_role_id=23;
                $user->gender = $request->gender;
                $user->phone_number = $request->phone_number;
                $user->birth_place = $request->birth_place;
                $user->birth_date = $request->birth_date;
                $user->foto = $fileName;
                $user->save();

                if($request->is_create_email == 1){
                    $user->email=strtolower($request->email);
                    if(!is_null($request->password) || $request->password!='')
                    {
                        $user->password=Hash::make($request->password);
                    }
                    $user->save();
                }

            }


            if($request->is_create_email == 1){
                $data->email=$request->email;
            }

            $data->md_merchant_id=merchant_id();
            $data->code = $request->code;
            $data->marital_status = $request->marital_status;
            $data->blood_type = $request->blood_type;
            $data->last_education = $request->last_education;
            $data->is_active = $request->is_active;
            $data->religion = $request->religion;
            $data->address = $request->address;
            $data->license_number = $request->license_number;
            $data->md_merchant_staff_category_id = $request->md_merchant_staff_category_id;
            $data->is_create_email = $request->is_create_email;
            $data->merchant_grade_id=$request->merchant_grade_id;
            $data->departement_id=$request->departement_id;
            $data->job_position_id=$request->job_position_id;
            $data->join_date=$request->join_date;

            $data->save();
            DB::commit();
            return response()->json([
                'message'=>'Data Dokter berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.doctor.index')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=DoctorEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            $user=User::find($data->md_user_id);
            $user->is_active=0;
            $user->save();
            return response()->json([
                'message'=>'Data Dokter berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }
}

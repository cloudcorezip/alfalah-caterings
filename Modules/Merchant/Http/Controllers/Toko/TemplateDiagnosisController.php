<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\SennaToko\TemplateDiagnosis;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantBranch;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class TemplateDiagnosisController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('template-diagnosis')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.template-diagnosis.index');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.toko.template-diagnosis.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.template-diagnosis.save');

                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.template-diagnosis.delete');

                Route::post('/preview', [static::class, 'preview'])
                    ->name('merchant.toko.template-diagnosis.preview');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $firstDate=Carbon::now()->firstOfMonth();
        $data = TemplateDiagnosis::where('md_merchant_id', merchant_id())
                                ->where('is_deleted', 0)
                                ->orderBy('id', 'asc')
                                ->get();
        $merchant = Merchant::find(merchant_id());
        $params=[
            'title'=>'Template Diagnosa',
            'data' => $data,
            'merchant' => $merchant
        ];

        return view('merchant::toko.template-diagnosis.index',$params);

    }

    public function add(Request $request)
    {
        $id = $request->id;
        $userId = user_id();
        $merchantId = merchant_id();
        $merchant = Merchant::find($merchantId);
        $type = $request->type;
        if(!is_null($id)){
            $data=TemplateDiagnosis::find($id);
        }else{
            $data=new TemplateDiagnosis();
        }

        switch($type){
            case 'text':
                $title = 'Buat Pertanyaan';
                break;
            case 'radio':
                $title = 'Buat Pilihan Ganda';
                break;
            case 'file':
                $title = 'Buat Inputan File';
                break;
            default:
                $title = '';
                break;
        }

        $params=[
            'title'=> $title,
            'data'=>$data,
            'merchant' => $merchant,
            'type' => $type
        ];

        return view('merchant::toko.template-diagnosis.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){
                $data=TemplateDiagnosis::find($id);
            }else{
                $data=new TemplateDiagnosis();
            }

            if($request->type == 'radio'){
                $option = [];
                foreach($request->option as $item){
                    if(!is_null($item)){
                        array_push($option, $item);
                    }
                }

                if(count($option) == 0){
                    return response()->json([
                        'message'=>'Pilihan tidak boleh kosong !',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }

                $data->option = json_encode($option);
            }


            if($request->type == 'file')
            {
                $checkTypeFile = TemplateDiagnosis::where('md_merchant_id', merchant_id())
                                                ->where('is_deleted', 0)
                                                ->where('type', 'file')
                                                ->get();
                if(count($checkTypeFile) >= 10)
                {
                    return response()->json([
                        'message'=>'File telah mencapai batas !',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }
            }


            if(is_null($id)){
                $name = date_create(date('y-m-d h:i:s'));
                if($request->type == 'text'){
                    $data->name = "text-".date_timestamp_get($name);
                }

                if($request->type == 'radio'){
                    $data->name = "radio-".date_timestamp_get($name);
                }

                if($request->type == 'file'){
                    $data->name = "file-".date_timestamp_get($name);
                }

            }

            $data->md_merchant_id = merchant_id();
            $data->question = $request->question;
            $data->type = $request->type;
            $data->save();

            return response()->json([
                'message'=>'Pertanyaan berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=TemplateDiagnosis::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'Pertanyaan berhasil dihapus',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal dihapus!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }

    public function preview(Request $request)
    {
        $id = $request->id;
        $userId = user_id();
        $merchantId = merchant_id();
        $merchant = Merchant::find($merchantId);
        $data = TemplateDiagnosis::where('md_merchant_id', $merchantId)
                                ->where('is_deleted', 0)
                                ->orderBy('id', 'asc')
                                ->get();

        $params=[
            'title'=> '-',
            'data'=>$data,
            'merchant' => $merchant
        ];

        return view('merchant::toko.template-diagnosis.preview',$params);
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\Product;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\ProductionOfGood;
use App\Models\SennaToko\ProductionOfGoodBom;
use App\Models\SennaToko\ProductionOfGoodDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\MasterData\Unit;
use App\Models\Accounting\CoaMapping;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Inventory\InventoryUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Utils\Inventory\ProductionOfGoodUtil;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\PaymentUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\ProductionEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\ProductEntity;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ProductionOfGoodController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('stock/production-of-goods')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.stock.production.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.stock.production.datatable');
                Route::get('/add/{id?}', [static::class, 'add'])
                    ->name('merchant.toko.stock.production.add');
                Route::get('/detail/{id}', [static::class, 'detail'])
                    ->name('merchant.toko.stock.production.detail');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.stock.production.save');
                Route::post('/save-harga-jual', [static::class, 'saveHargaJual'])
                    ->name('merchant.toko.stock.production.save-harga-jual');
                Route::post('/save-biaya', [static::class, 'savePaymentBiaya'])
                    ->name('merchant.toko.stock.production.save-biaya');
                Route::post('/delete-biaya', [static::class, 'deleteBiaya'])
                    ->name('merchant.toko.stock.production.delete-biaya');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.stock.production.delete');
                Route::post('/get-component', [static::class, 'loadComponentOfProduction'])
                    ->name('merchant.toko.stock.production.component');

                Route::post('/load-all-material', [static::class, 'loadAllRawMaterial'])
                    ->name('merchant.toko.stock.production.load-all-material');
                Route::get('/add-cost', [static::class, 'addCost'])
                    ->name('merchant.toko.stock.production.add-cost');
                Route::post('/get-product', [static::class, 'getProduct'])
                    ->name('merchant.toko.stock.production.get-product');
                Route::post('/get-warehouse', [static::class, 'getWarehouse'])
                    ->name('merchant.toko.stock.production.get-warehouse');

                Route::post('/reload-data', [static::class, 'reloadData'])
                    ->name('merchant.toko.stock.production.reload-data');


            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Produksi Produk',
            'tableColumns'=>ProductionEntity::dataTableColumns(),
            'add'=>(get_role()==3)?ProductionEntity::add(true):
                MerkEntity::add(true),
            'key_val'=>$request->key,
            'searchKey'=>md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString()
        ];

        return view('merchant::toko.stock.production-of-good.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ProductionEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public function getProduct(Request $request)
    {
        try {
            $merchantId = $request->md_merchant_id;
            $merchant = Merchant::find($merchantId);
            $userId = $merchant->md_user_id;
            $search = $request->key;

            $data=Product::select([
                "id",
                "name as text"
            ])
                ->where('md_user_id',$userId)
                ->where('is_deleted',0)
                ->where('md_sc_product_type_id',2);

            if(!is_null($search)){
                $data->whereRaw("lower(sc_products.name) like '%".strtolower(str_replace("'","''",$search))."%' ");
            }

            return response()->json($data->get());
        } catch(\Exception $e)
        {
            return response()->json([]);
        }
    }

    public function getWarehouse(Request $request)
    {
        try {
            $merchantId = $request->md_merchant_id;

            $data=WarehouseEntity::select([
                "id",
                "name as text"
            ])
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->get();
            return response()->json($data);
        } catch(\Exception $e)
        {
            return response()->json([]);
        }
    }

    public  function add(Request $request){
        $id=$request->id;

        $type = $request->type;

        if(is_null($type)){
            return abort(404);
        }

        if(is_null($id))
        {
            $data=new ProductionOfGood();
            $merchantId = merchant_id();
            $userId = user_id();
        }else{
            $data=ProductionOfGood::find($id);
            $merchant = Merchant::find($data->md_merchant_id);
            $merchantId = $merchant->id;
            $userId = $merchant->md_user_id;
        }

        $warehouse=WarehouseEntity::where('md_merchant_id',$merchantId)
            ->where('is_deleted',0)
            ->get();

        $merchant=Merchant::find($merchantId);

        $params=[
            'title'=>(is_null($id))?'Tambah Data Produksi':'Edit Data Produksi '.$data->code,
            'warehouse'=>$warehouse,
            'data'=>$data,
            'merchant'=>$merchant,
            'category'=>CategoryEntity::listOption(),
            'code'=>CodeGenerator::generalCode('PR',merchant_id()),
            'merk'=>MerkEntity::listOption(),
            'type'=>$type
        ];


        if($type == "create"){
            return view('merchant::toko.stock.production-of-good.form-create-product',$params);
        } elseif($type == "select") {
            return view('merchant::toko.stock.production-of-good.form-select-product',$params);
        }
    }

    public function addCost(Request $request)
    {
        $id = $request->id;

        $data = ProductionOfGood::find($id);

        if(is_null($data)){
            return abort(404);
        }

        $merchant = Merchant::find($data->md_merchant_id);
        $merchantId = $merchant->id;

        $detail=ProductionOfGoodBom::
        select([
            'sc_inv_production_of_good_boms.*',
            'd.name as payment_name'
        ])
        ->where('sc_inv_production_of_good_id',$id)
            ->join('acc_coa_details as d','d.id','sc_inv_production_of_good_boms.trans_coa_detail_id')
            ->where('sc_inv_production_of_good_boms.is_deleted',0)
            ->get();

        $costList = CoaMapping::where('md_merchant_id',$merchantId)
                ->whereIn('is_deleted',[0])
                ->where('is_type',2)
                ->get();
        $paymentList = PaymentUtils::getPayment($merchantId,2);

        $params = [
            "title" => "Tambah Biaya Produksi",
            "data" => $data,
            "detail" => $detail,
            "costList" => $costList,
            "paymentList" => $paymentList
        ];
        return view('merchant::toko.stock.production-of-good.form-add-cost', $params);
    }

    public function detail($id)
    {

        $data=ProductionOfGood::find($id);

        if(is_null($data)){
            abort(404);
        }

        $params=[
            'title'=>'Detail Produksi '.$data->code,
            'data'=>$data,
            'material'=>Material::
            select([
                'p.id',
                'p.name',
                'p.code',
                'u.name as unit_name',
                'sc_product_raw_materials.quantity',
                'p.purchase_price'
            ])->where('parent_sc_product_id',$data->sc_product_id)
                ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                ->join('md_units as u','u.id','p.md_unit_id')
                ->where('sc_product_raw_materials.is_deleted',0)
            ->get(),
            'bom'=>ProductionOfGoodBom::
            select([
                'sc_inv_production_of_good_boms.*',
                'd.name as payment_name'
            ])
            ->where('sc_inv_production_of_good_id',$id)
            ->join('acc_coa_details as d','d.id','sc_inv_production_of_good_boms.trans_coa_detail_id')
                ->where('sc_inv_production_of_good_boms.is_deleted',0)
                ->get()
        ];

        return view('merchant::toko.stock.production-of-good.form-detail',$params);

    }

    public function save(Request $request)
    {
        try {
            DB::beginTransaction();

            $merchantId = $request->md_merchant_id;
            if(is_null($merchantId) || $merchantId == "-1"){
                return response()->json([
                    'message' => "Pilih cabang terlebih dahulu !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $merchant = Merchant::find($merchantId);
            $userId = $merchant->md_user_id;

            $checkPackageSubscription=MerchantUtil::checkUrl($merchantId);
        
            if(!is_null($checkPackageSubscription)){
                if($checkPackageSubscription['status']==true)
                {
                    return response()->json([
                        'message'=>$checkPackageSubscription['message'],
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            // jika tipe create, buat produk terlebih dahulu
            if($request->type == "create"){
                $product = new ProductEntity();
                $product->name=$request->name;
                $validator = Validator::make($request->all(), $product->ruleCreateWeb);
                if($validator->fails()) {
                    $error = $validator->errors()->first();
                    return response()->json([
                        'message' => $error,
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $unit=Unit::find($request->md_unit_id);
                $selling_price=strtr($request->selling_price, array('.' => '', ',' => '.'));
                $purchase_price=strtr($request->purchase_price, array('.' => '', ',' => '.'));
                $checkCode=Product::whereRaw("lower(code) like '".strtolower($request->code)."' ")
                                    ->where('md_user_id',$userId)
                                    ->first();
                if(!is_null($checkCode))
                {
                    return response()->json([
                        'message' => "Kode produk sudah digunakan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);

                }

                $product->code=$request->code;
                $product->purchase_price=$purchase_price;
                $product->selling_price=$selling_price;
                $product->is_with_stock=1;
                $product->is_with_purchase_price=($request->is_with_stock==0)?0:1;
                $product->is_consignment=0;
                $product->weight=0;
                if($request->sc_merk_id!=-1){
                    $product->sc_merk_id=$request->sc_merk_id;
                }
                $product->md_sc_product_type_id=2;
                $product->sc_product_category_id=$request->sc_product_category_id;
                $product->md_user_id=$userId;
                $product->is_show_senna_app=0;
                $product->md_unit_id=$unit->id;
                $product->md_sc_category_id=null;
                $product->profit_prosentase=0;
                $product->created_by_merchant=$merchantId;
                $product->is_deleted=0;
                $product->save();

                $bahanBaku = [];
                $decodeBahanBaku = is_null(json_decode($request->bahan_baku))?[]:json_decode($request->bahan_baku);
                if(count($decodeBahanBaku) < 1){
                    return response()->json([
                        'message' => "Masukkan bahan baku terlebih dahulu !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                if(count($decodeBahanBaku) > 0){
                    foreach($decodeBahanBaku as $key => $item){
                        if($item->quantity <= 0){
                            return response()->json([
                                'message' => "Masukkan quantity untuk bahan baku !",
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $bahanBaku[] = [
                            "parent_sc_product_id" => $product->id,
                            "child_sc_product_id" => $item->child_sc_product_id,
                            "quantity" => $item->quantity,
                            "created_at" => date('Y-m-d h:i:s'),
                            "updated_at" => date('Y-m-d h:i:s'),
                            "is_deleted" => 0
                        ];
                    }

                    Material::insert($bahanBaku);
                }

                if($product->is_with_stock==1)
                {
                    $coaProduct=CoaProductUtil::initialCoaProduct($merchantId,$product,1);
                    if($coaProduct==false)
                    {
                        return response()->json([
                            'message' => "Terjadi kesalahan,inisiasi akun persediaan gagal dibuat !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                    $product->inv_id=$coaProduct['inv_id'];
                    $product->hpp_id=$coaProduct['hpp_id'];
                    $product->save();
                }

                $productId = $product->id;
            } else {
                $productId = $request->sc_product_id;
            }

            // mulai buat barang produksi
            $code=CodeGenerator::generateJurnalCode(0,'PRD',$merchantId);
            $material=Material::where('parent_sc_product_id',$productId)
                ->where('is_deleted',0)
                ->get();
            $warehouse=WarehouseEntity::find($request->inv_warehouse_id);
            $productPod=Product::find($productId);
            $productIds = "";

            $date = new \DateTime($request->time_of_production);
            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            foreach ($material as $key =>$item)
            {
                $sumProduction=$item->quantity*$request->amount;
                $stockSum=StockInventory::where('sc_product_id',$item->child_sc_product_id)
                    ->where('type','in')
                    ->whereRaw("transaction_action not like '%Retur%'")
                    ->where('inv_warehouse_id',$request->inv_warehouse_id)
                    ->where('is_deleted',0)
                    ->orderBy('id','asc')
                    ->sum('residual_stock');
                if($sumProduction>$stockSum)
                {
                    return response()->json([
                        'message' => "Stok produk ".$item->getChild->name." pada gudang <b>$warehouse->name</b> tidak mencukupi !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                    break;
                }
                $productIds .= ($key == 0) ? $item->child_sc_product_id : "," . $item->child_sc_product_id;


            }

            $id = $request->id;

            if(is_null($id)){
                $data=new ProductionOfGood();
            } else {
                $data = ProductionOfGood::find($id);
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.stock.production.add-cost',['id'=>$data->id])
                ]);
            }

            if($request->type == "create"){
                $validator = Validator::make($request->all(), $data->ruleCreateProductFirst);
            } else {
                $validator = Validator::make($request->all(), $data->ruleProduction);
            }

            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' =>  $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $data->sc_product_id=$productId;
            $data->inv_warehouse_id=$request->inv_warehouse_id;
            $data->time_of_production=$request->time_of_production;
            $data->amount=$request->amount;
            $data->code=$code;
            $data->timezone=$request->_timezone;
            $data->note=$request->note;
            $data->md_merchant_id=$merchantId;
            $data->created_by=$userId;
            $data->save();
            $insert=[];
            $productCalculate = [];
            $total=0;
            $updateStockProduct="";
            $products = collect(DB::select("
                select
                sp.*
                from
                sc_products sp
                where
                sp.id in ($productIds)
            "));

            foreach ($material as $m =>$v)
            {
                $amount=$v->quantity*$data->amount;

                array_push($productCalculate,[
                    'id'=>$v->child_sc_product_id,
                    'amount'=>$amount,
                ]);
                $product=$products->firstWhere('id',$v->child_sc_product_id);

                if($warehouse->is_default==1)
                {
                    $stockP=$product->stock-$amount;
                    $updateStockProduct.=($m==0)?"(".$v->child_sc_product_id.",".$stockP.")":",(".$v->child_sc_product_id.",".$stockP.")";
                }

                $stockOut[]=[
                    'sync_id' => $v->child_sc_product_id . Uuid::uuid4()->toString(),
                    'sc_product_id' => $v->child_sc_product_id,
                    'total' => $amount,
                    'inv_warehouse_id' => $warehouse->id,
                    'record_stock' => 0,
                    'created_by' => $userId,
                    'selling_price' => 0,
                    'purchase_price' => 0,
                    'residual_stock' => 0,
                    'type' => StockInventory::OUT,
                    'transaction_action' => 'Pengurangan Stok Produk '.$product->name. ' Dari Produksi '.$data->code.'',
                    'stockable_type'=>'App\Models\SennaToko\Product',
                    'stockable_id'=>$v->child_sc_product_id,
                    'created_at'=>$data->time_of_production,
                    'updated_at'=>$data->time_of_production
                ];
            }

            $calculateInventory=InventoryUtilV2::inventoryCodev2($merchant->md_inventory_method_id,$warehouse->id,$productCalculate,'minus');
            if($calculateInventory==false)
            {
                return response()->json([
                    'message' => 'Terjadi kesalahan saat pengurangan stok dipersediaan bahan baku',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            foreach ($calculateInventory as $i)
            {
                $insert[]=[
                    'sc_product_id'=>$i['sc_product_id'],
                    'sc_stock_inventory_id'=>$i['id'],
                    'purchase_price'=>$i['purchase'],
                    'sub_total'=>$i['amount']*$i['purchase'],
                    'sc_inv_production_of_good_id'=>$data->id,
                    'amount'=>$i['amount'],
                    'created_at'=>$data->time_of_production,
                    'updated_at'=>$data->time_of_production
                ];
                $total+=$i['amount']*$i['purchase'];

            }
            $updateStockInv="";
            foreach ($calculateInventory as $key => $n)
            {
                $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
            }
            if($updateStockInv!=""){
                DB::statement("
                update sc_stock_inventories as t set
                        residual_stock = c.column_a
                    from (values
                        $updateStockInv
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
            }
            if($updateStockProduct!=""){
                DB::statement("
                update sc_products as t set
                        stock = c.column_a
                    from (values
                        $updateStockProduct
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
            }

            DB::table('sc_stock_inventories')->insert($stockOut);
            ProductionOfGoodDetail::insert($insert);

            $invRef=$productPod->stock()->insertGetId([
                'sync_id'=>Uuid::uuid4()->toString(),
                'sc_product_id'=>$productPod->id,
                'total'=>$data->amount,
                'record_stock'=>$data->amount,
                'created_by'=>$userId,
                'selling_price'=>0,
                'residual_stock'=>$data->amount,
                'purchase_price'=>$total/$data->amount,
                'timezone'=>$request->timezone,
                'type'=>StockInventory::IN,
                'inv_warehouse_id'=>$warehouse->id,
                'stockable_id'=>$productPod->id,
                'stockable_type'=>'App\Models\SennaToko\Product',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
                'transaction_action'=>'Penambahan Stok Produk '.$productPod->name. ' Dari Produksi '.$data->code.''
            ]);
            if($warehouse->is_default==1)
            {
                $productPod->stock+=$data->amount;
                $productPod->save();
            }

            $data->total=$total;
            $data->sc_stock_inventory_id=$invRef;
            $data->save();
            if(ProductionOfGoodUtil::createProductionOfGood($merchantId,$userId,$data,$productPod,$material)==false)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan dalam pembuatan jurnal pencatatan produksi produk !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            DB::commit();

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.stock.production.add-cost',['id'=>$data->id])
            ]);

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function loadAllRawMaterial(Request $request)
    {
        try {
            $merchantId = $request->md_merchant_id;
            $merchant = Merchant::find($merchantId);

            $userId = $merchant->md_user_id;

            $data = Product::
            select([
                'sc_products.id',
                'sc_products.name',
                'sc_products.code',
                'u.name as unit_name'
            ])
                ->where('sc_products.md_user_id',$userId)
                ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->where('sc_products.is_deleted',0)
                ->where('sc_products.is_with_stock',1)
                ->where('sc_products.md_sc_product_type_id',3)
                ->orderBy('id','desc')
                ->get();

            return $data;

        } catch(\Exception $e)
        {
            return [];
        }
    }

    public function loadComponentOfProduction(Request  $request)
    {
        $id=$request->id;
        $product=Product::find($id);
        // $material = $this->loadAllRawMaterial();
        $data=Material::
        select([
            'p.id',
            'p.name',
            'p.code',
            'u.name as unit_name',
            'sc_product_raw_materials.quantity'
        ])->where('parent_sc_product_id',$id)
            ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
            ->join('md_units as u','u.id','p.md_unit_id')
            ->where('sc_product_raw_materials.is_deleted',0)
            ->get();

        $html = '';

        foreach($data as $key => $item)
        {
            $row = $key + 1;
            $html .= '<div id="rm-'.$row.'" class="col-md-12 mb-4 row-raw-material" style="border:1px dashed #C4C4C4;background:#FBFBFB;border-radius:3px;">
                        <div class="form-group py-3">
                            <label class="font-weight-bold">Bahan Baku</label>
                            <select disabled name="raw-material" class="form-control raw-material" data-row="rm-'.$row.'">
                                <option></option>
                                <option selected data-code="'.$item->code.'" value="'.$item->id.'">'.$item->name.'</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Jumlah (Kuantitas)</label>
                            <input disabled type="number" class="form-control input-quantity" value="'.$item->quantity.'" data-row="rm-'.$row.'">
                        </div>
                    </div>';
        }

        $params = [
            "html" => $html,
            "satuan"=>$product->getUnit->name,
        ];
        return json_encode($params);
    }


    public function addPaymentBiaya(Request  $request)
    {
        $id=$request->id;
        if(is_null($id))
        {
            $data=new ProductionOfGood();
        }else{
            $data=ProductionOfGood::find($id);
        }
        $params=[
            'title'=>'Biaya Produksi',
            'paymentOption'=>CoaCategory::where('md_merchant_id',merchant_id())
                ->whereIn('code',['1.1.01','1.1.02'])
                ->get(),
            'data'=>$data
        ];

        return view('merchant::toko.stock.production-of-good.form-biaya',$params);

    }

    public function savePaymentBiaya(Request  $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();
            $data=ProductionOfGood::find($request->id);
            $merchant = Merchant::find($data->md_merchant_id);
            $merchantId = $merchant->id;
            $userId = $merchant->md_user_id;

            $requestCost = json_decode($request->data_cost);

            foreach($requestCost as $key => $item){

                $trans = explode('_',$item->trans_coa_detail_id);
                $amount_of_bom=strtr($item->amount_of_bom, array('.' => '', ',' => '.'));
                $admin_fee = strtr($item->admin_fee, array('.' => '', ',' => '.'));

                if(empty($item->trans_coa_detail_id)){
                    return response()->json([
                        'message' => "Metode pembayaran untuk $item->cost_coa_name belum dipilih !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                if($amount_of_bom <= 0){
                    return response()->json([
                        'message' => "Masukkan biaya untuk $item->cost_coa_name !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                if($admin_fee == ""){
                    return response()->json([
                        'message' => "Masukkan biaya admin untuk $item->cost_coa_name !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                $biaya=new ProductionOfGoodBom();
                $code=CodeGenerator::generateJurnalCode(0,'CSP',$merchantId);
                $biaya->name=$item->cost_coa_name;
                $biaya->code=$code;

                $biaya->amount_of_bom=$amount_of_bom;
                $biaya->md_transaction_type_id = $trans[0];
                $biaya->trans_coa_detail_id=$trans[1];
                $biaya->payment_to=$request->payment_to;
                $biaya->cost_coa_id = $item->cost_coa_id;
                $biaya->admin_fee = $admin_fee;

                $destinationPath = 'public/uploads/merchant/'.$merchantId.'/acc/pod/'.$data->id.'/';
                if($request->hasFile('payment_proof'))
                {
                    $file=$request->file('payment_proof');
                    $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                    $fileName=$destinationPath.$fileName;
                    Storage::disk('s3')->put($fileName, file_get_contents($file));

                }else{
                    if(is_null($data->id)){
                        $fileName=null;

                    }else{
                        $fileName=$data->payment_proof;
                    }
                }
                $biaya->payment_proof=$fileName;
                $biaya->created_at=date('Y-m-d H:i:s');
                $biaya->updated_at=date('Y-m-d H:i:s');
                $biaya->is_deleted=0;
                $biaya->sc_inv_production_of_good_id=$data->id;
                $biaya->save();

                $jurnal=new Jurnal();
                $jurnal->ref_code=$biaya->code;
                $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);
                $jurnal->trans_name='Biaya Produksi '.$data->code.'-'.$biaya->code;
                $jurnal->trans_time=$biaya->created_at;
                $jurnal->trans_note='Biaya Produksi '.$data->code.'-'.$biaya->code;
                $jurnal->trans_purpose='Biaya Produksi '.$data->code.'-'.$biaya->code;
                $jurnal->trans_amount=$biaya->amount_of_bom;
                $jurnal->trans_proof=$fileName;
                $jurnal->md_merchant_id=$merchantId;
                $jurnal->md_user_id_created=$userId;
                $jurnal->external_ref_id=$biaya->id;
                $jurnal->flag_name='Biaya Produksi '.$data->code.'-'.$biaya->code;
                $jurnal->save();
                // $fromCoa=merchant_detail()->coa_production_cost_id;

                $fromCoa = $biaya->cost_coa_id;

                $toCoa=CoaDetail::find($biaya->trans_coa_detail_id);
                $insert=[
                    [
                        'acc_coa_detail_id'=>$fromCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$biaya->amount_of_bom,
                        'created_at'=>$biaya->created_at,
                        'updated_at'=>$biaya->created_at,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa->id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>$toCoa->md_sc_currency_id,
                        'amount'=>$biaya->amount_of_bom,
                        'created_at'=>$biaya->created_at,
                        'updated_at'=>$biaya->created_at,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                ];
                JurnalDetail::insert($insert);
                $data->total=$data->total+$biaya->amount_of_bom+$biaya->admin_fee;
                $data->save();
                $productPod=StockInventory::find($data->sc_stock_inventory_id);
                $productPod->purchase_price=$data->total/$data->amount;
                $productPod->save();

                if($data->getWarehouse->is_default==1)
                {
                    $data->getProduct->purchase_price=$data->total/$data->amount;
                    $data->getProduct->save();
                }
            }


            DB::commit();
            return response()->json([
                'message' => "Data biaya disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.stock.production.detail', ["id" => $data->id])
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }


    public function deleteBiaya(Request  $request)
    {
        try {
            DB::beginTransaction();
            $data=ProductionOfGoodBom::find($request->id);

            $data->is_deleted=1;
            $data->save();
            Jurnal::where([
                'external_ref_id'=>$data->id,
                'ref_code'=>$data->code
            ])->update([
                'is_deleted'=>1
            ]);
            $pod=ProductionOfGood::find($data->sc_inv_production_of_good_id);
            $subtractor = $data->amount_of_bom + $data->admin_fee;

            $pod->total -= $subtractor;
            $pod->save();

            $productPod=StockInventory::find($pod->sc_stock_inventory_id);

            $productPod->purchase_price=$pod->total/$pod->amount;
            $productPod->save();

            if($pod->getWarehouse->is_default==1)
            {
                $pod->getProduct->purchase_price=$pod->total/$pod->amount;;
                $pod->getProduct->save();
            }
            DB::commit();
            return response()->json([
                'message' => "Data biaya berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }


    }

    public function delete(Request  $request)
    {
        try {
            DB::beginTransaction();
            $data=ProductionOfGood::find($request->id);
            $data->is_deleted=1;
            $data->save();
            Jurnal::where([
                'external_ref_id'=>$data->id,
                'ref_code'=>$data->code
            ])->update([
                'is_deleted'=>1
            ]);
            if($data->getWarehouse->is_default==1)
            {
                $data->getProduct->purchase_price=0;;
                $data->getProduct->save();
            }
            $productPod=StockInventory::find($data->sc_stock_inventory_id);
            $productPod->delete();
            $material=Material::where('parent_sc_product_id',$data->sc_product_id)
                ->where('is_deleted',0)
                ->get();

            foreach ($material as $m)
            {
                $stock=0;
                $product=Product::find($m->child_sc_product_id);
                foreach ($data->getDetail->where('sc_product_id',$product->id) as $item)
                {
                    $stock+=$item->amount;
                }
                if($data->getWarehouse->is_default==1)
                {
                    $product->stock+=$stock;
                    $product->save();
                }
                $inv=StockInventory::where('sc_product_id',$m->child_sc_product_id)
                    ->where('inv_warehouse_id',$data->inv_warehouse_id)
                    ->whereRaw("transaction_action like '%".$data->code."%'")
                    ->where('type','out')
                    ->first();
                if(is_null($inv))
                {
                    DB::rollBack();
                    return response()->json([
                        'message' => "Terjadi kesalahan,persediaan gagal dikembalikan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $inv->delete();
            }
            foreach ($data->getDetail as $d)
            {
                $stockInv=StockInventory::find($d->sc_stock_inventory_id);
                if(is_null($stockInv))
                {
                    DB::rollBack();
                    return response()->json([
                        'message' => "Terjadi kesalahan, komponen persediaan gagal dikembalikan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);

                }
                $stockInv->residual_stock+=$d->amount;
                $stockInv->save();
            }

            foreach ($data->getBom->where('is_deleted',0) as $b)
            {
                Jurnal::where([
                    'external_ref_id'=>$b->id,
                    'ref_code'=>$b->code
                ])->update([
                    'is_deleted'=>1
                ]);
            }
            DB::commit();
            return response()->json([
                'message' => "Data biaya berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.stock.production.index')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function saveHargaJual(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=ProductionOfGood::find($id);
            $selling_price=strtr($request->selling_price, array('.' => '', ',' => '.'));
            $data->selling_price=$selling_price;
            $data->save();
            StockInventory::find($data->sc_stock_inventory_id)
                ->update([
                    'selling_price'=>$data->selling_price
                ]);

            if($data->getWarehouse->is_default==1)
            {
                $data->getProduct->selling_price=$data->selling_price;
                $data->getProduct->save();
            }
            DB::commit();
            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.stock.production.index')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }

    public function getPaymentList(Request  $request)
    {
        try {
            $merchantId=$request->md_merchant_id;
            $type=$request->type;
            $data=collect(PaymentUtils::getPayment($merchantId,$type));

            $result=[];
            foreach ($data as $key =>$item)
            {
                $childs=[];
                foreach (collect($item['data']) as $child)
                {
                    $childs[]=[
                        'id'=>$child->trans_id.'_'.$child->acc_coa_id,
                        'text'=>$child->name
                    ];
                }

                $result[]=[
                    "text"=>$item['name'],
                    'children'=>$childs
                ];
            }

            return response()->json($result);


        }catch (\Exception $e)
        {
            return  response()->json([]);

        }
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ProductionEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }

        $reqMerchantId = json_decode($request->md_merchant_id);

        if(in_array('-1',$reqMerchantId)){
            $merchantIds = MerchantUtil::getBranch(merchant_id());
        } else {
            $merchantIds = json_decode($request->md_merchant_id);
        }

        $searchKey = $request->input('search_key');

        $searchParams["md_merchant_id"] = $merchantIds;
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Produksi Produk',
            'searchKey' => $searchKey,
            'tableColumns'=>ProductionEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.stock.production-of-good.list',$params);

    }
}

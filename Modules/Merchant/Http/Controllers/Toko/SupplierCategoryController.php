<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;

class SupplierCategoryController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/supplier-category')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\SupplierCategoryController@index')
                    ->name('merchant.toko.supplier-category.index');
                Route::post('/data-table', 'Toko\SupplierCategoryController@dataTable')
                    ->name('merchant.toko.supplier-category.datatable');
                Route::post('/add', 'Toko\SupplierCategoryController@add')
                    ->name('merchant.toko.supplier-category.add');
                Route::post('/save', 'Toko\SupplierCategoryController@save')
                    ->name('merchant.toko.supplier-category.save');
                Route::post('/delete', 'Toko\SupplierCategoryController@delete')
                    ->name('merchant.toko.supplier-category.delete');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Kategori Supplier',
            'tableColumns'=>SupplierCategoryEntity::dataTableColumns(),
            'add'=>(get_role()==3)?SupplierCategoryEntity::add(true):
                SupplierCategoryEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.supplier-category.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return SupplierCategoryEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=SupplierCategoryEntity::find($id);
        }else{
            $data=new SupplierCategoryEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::toko.master-data.supplier-category.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=SupplierCategoryEntity::find($id);
            }else{
                $data=new SupplierCategoryEntity();
                $data->md_user_id=user_id();

            }

            $data->name=$request->name;
            $data->description=$request->description;
            $data->save();

            return response()->json([
                'message'=>'Kategori supplier atau pemasok berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=SupplierCategoryEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'Kategori supplier atau pemasok berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }
}

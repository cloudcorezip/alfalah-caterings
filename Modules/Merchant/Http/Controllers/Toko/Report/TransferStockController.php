<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Report;

use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantStaff;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\TransferStock;
use App\Models\SennaToko\TransferStockDetail;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Utils\Inventory\MutationUtil;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Modules\Merchant\Entities\Toko\ApEntity;
use Modules\Merchant\Entities\Toko\StockAdjustmentEntity;
use Modules\Merchant\Entities\Toko\StockOpnameEntity;
use Modules\Merchant\Entities\Toko\TransferStockEntity;
use Modules\Merchant\Entities\Toko\BranchEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use App\Models\SennaToko\StockInventory;
use App\Utils\Inventory\InventoryUtilTransfer;
use App\Models\Accounting\CoaDetail;
use App\Utils\Accounting\CoaSaleUtil;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\Jurnal;
use Ramsey\Uuid\Uuid;

class TransferStockController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('report/transfer-stock')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\Report\TransferStockController@data')
                    ->name('merchant.toko.transfer-stock.data');
                Route::post('/data-table', 'Toko\Report\TransferStockController@dataTable')
                    ->name('merchant.toko.transfer-stock.datatable');
                Route::post('/reload-data', 'Toko\Report\TransferStockController@reloadData')
                    ->name('merchant.toko.transfer-stock.reload-data');
//                Route::post('/accepted', 'Toko\Report\TransferStockController@accepted')
//                    ->name('merchant.toko.transfer-stock.accepted');
//                Route::post('/refused', 'Toko\Report\TransferStockController@refused')
//                    ->name('merchant.toko.transfer-stock.refused');
                Route::post('/delete', 'Toko\Report\TransferStockController@delete')
                    ->name('merchant.toko.transfer-stock.delete');
                Route::get('/detail/{id}', 'Toko\Report\TransferStockController@detail')
                    ->name('merchant.toko.transfer-stock.detail');
                Route::get('/add', 'Toko\Report\TransferStockController@add')
                    ->name('merchant.toko.transfer-stock.add');
                Route::post('/save', 'Toko\Report\TransferStockController@save')
                    ->name('merchant.toko.transfer-stock.save');
                Route::post('/export-data', 'Toko\Report\TransferStockController@exportData')
                    ->name('merchant.toko.transfer-stock.export-data');
            });
    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=TransferStockEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            $stock=StockInventory::where('stockable_id',$data->id)
                ->where('stockable_type','App\Models\SennaToko\TransferStock')
                ->where('type','in')
                ->where('inv_warehouse_id',$data->to_inv_warehouse_id);

            foreach ($stock->get() as $i =>$s){

                if($s->residual_stock<$s->total){
                    return response()->json([
                        'message' => "Data stok produk ".$s->getProduct->name." sudah digunakan,kamu tidak dapat menghapus mutasi stok!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' =>''
                    ]);
                }

                if($s->getWarehouse->is_default==1)
                {
                    $s->getProduct->stock-=$s->residual_stock;
                    $s->getProduct->save();
                }
            }



            $invs=json_decode($data->invs);

            if(!empty($invs)){
                foreach ($invs as $s =>$ss)
                {
                    $ssi=StockInventory::find($ss->sc_stock_inventory_id);
                    if($ssi)
                    {
                        $ssi->residual_stock+=$ss->amount;
                        $ssi->save();

                        if($ssi->getWarehouse->is_default==1)
                        {
                            $ssi->getProduct->stock+=$ss->amount;
                            $ssi->getProduct->save();

                        }
                    }
                }
            }

            $stock->delete();

            StockInventory::where('stockable_id',$data->id)
                ->where('stockable_type','App\Models\SennaToko\TransferStock')
                ->where('type','out')
                ->where('inv_warehouse_id',$data->inv_warehouse_id)->delete();

            Jurnal::where(['ref_code'=>$data->code,
                'external_ref_id'=>$data->id,
                'md_merchant_id'=>$data->from_merchant_id
            ])
                ->update(['is_deleted'=>1]);

            Jurnal::where(['ref_code'=>$data->code,
                'external_ref_id'=>$data->id,
                'md_merchant_id'=>$data->to_merchant_id
            ])
                ->update(['is_deleted'=>1]);


            DB::commit();
            return response()->json([
                'message' => "Data mutasi stok berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.transfer-stock.data')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }


    public function detail($id)
    {
        try{
            $data=TransferStockEntity::with(['getDetail'])
                ->where('id',$id)
                ->first();

            $params=[
                'title'=>'Detail',
                'data'=>$data
            ];
            return view('merchant::toko.report.transfer-stock.detail',$params);

        }catch (\Exception $e)
        {
            return view('pages.500');
        }

    }
    public function data()
    {
        MutationUtil::createCoaTransfer(merchant_id());

        $params=[
            'title'=>'Mutasi Stok',
            'tableColumns'=>TransferStockEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'add'=>(get_role()==3)?TransferStockEntity::add(true):
                TransferStockEntity::add(true),

        ];


        return view('merchant::toko.report.transfer-stock.data',$params);

    }


    public function add(Request $request)
    {

        $params=[
            'title'=>'Tambah Data',
            'data'=>new TransferStockEntity(),
        ];
        return view('merchant::toko.report.transfer-stock.add',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);


            $date = new \DateTime($request->created_at);

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->from_merchant_id,'transfer_stock',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $checkPackageSubscription=MerchantUtil::checkUrl($request->from_merchant_id);

            if($checkPackageSubscription['status']==true)
            {
                return response()->json([
                    'message'=>$checkPackageSubscription['message'],
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            if(AccUtil::checkClosingJournal($request->from_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            if($request->inv_warehouse_id==$request->to_inv_warehouse_id)
            {

                return response()->json([
                    'message'=>"Asal dan Tujuan Gudang Tidak Boleh Sama ",
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }

            $initCoaTransfer1=MutationUtil::createCoaTransfer($request->from_merchant_id);
            if($initCoaTransfer1==false)
            {
                return response()->json([
                    'message'=>"Terjadi kesalahan saat membuat akun transfer stok di cabang asal",
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }
            $initCoaTransfer2=MutationUtil::createCoaTransfer($request->to_merchant_id);
            if($initCoaTransfer2==false)
            {
                return response()->json([
                    'message'=>"Terjadi kesalahan saat membuat akun transfer stok di cabang tujuan",
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',

                ]);
            }

            $sc_product_id=$request->sc_product_id;
            $quantity=$request->quantity;
            $unique_code=$request->unique_code;
            $residualStock=$request->original_stock;
            $unitName=$request->unit_name;
            $isMultiUnit=$request->is_multi_unit;
            $multiUnitId=$request->multi_unit_id;
            $valueConversion=$request->nilai_konversi;
            $jsonMultiUnit=$request->json_multi_unit;
            $unitList=$request->unit_list;
            $unitOrigin=$request->unit_origin;
            $productName=$request->product_name;
            if(empty($sc_product_id)){

                return response()->json([
                    'message'=>"Produk tidak boleh kosong",
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',

                ]);


            }
            $fromMerchant=Merchant::find($request->from_merchant_id);
            $toMerchant=Merchant::find($request->to_merchant_id);

            $data=new TransferStock();
            $code=CodeGenerator::generalCode('TFS',$request->from_merchant_id);
            $data->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $data->code=$code;
            $data->total=0;
            $data->from_merchant_id=$request->from_merchant_id;
            $data->to_merchant_id=$request->to_merchant_id;
            $data->inv_warehouse_id=$request->inv_warehouse_id;
            $data->to_inv_warehouse_id=$request->to_inv_warehouse_id;
            $data->is_accepted=1;
            $data->created_by=$fromMerchant->md_user_id;
            $data->timezone=$request->_timezone;
            $data->is_deleted=0;
            $data->created_at=$request->created_at;
            $data->updated_at=$request->created_at;
            $data->save();

            $from_warehouse=WarehouseEntity::find($request->inv_warehouse_id);
            $toWarehouse=WarehouseEntity::find($request->to_inv_warehouse_id);

            if($request->from_merchant_id==$request->to_merchant_id){
                $details=[];
                $stockMutation=[];
                $total=0;
                $productId="";
                foreach ($sc_product_id as $key => $v)
                {
                    $productId.=($key==0)?"".$sc_product_id[$key]."":",".$sc_product_id[$key]."";
                }
                $products=collect(DB::select("
                select
                  p.*
                 from sc_products p
                  join md_merchants m on m.md_user_id=p.md_user_id
                  where m.id=".$fromMerchant->id."
                  and p.id in($productId)
                "));

                $updateStockProduct="";
                $count=0;

                $productCalculateOut=[];

                $invs1=[];

                foreach ($sc_product_id as $key =>$item)
                {
                    $product=$products->firstWhere('id',$sc_product_id[$key]);




                    if($residualStock[$key]==0){
                        return response()->json([
                            'message'=>"Jumlah produk ".$product->name." kosong !",
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);
                    }

                    if($unitList[$key]=='-1' || $unitList[$key]==-1){
                        return response()->json([
                            'message'=>"Ada produk yang belum memilih satuan !",
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);
                    }
                    if($isMultiUnit[$key]==0){
                        $details[]=[
                            'sc_product_id'=>$sc_product_id[$key],
                            'sub_total'=>0,
                            'sc_transfer_stock_id'=>$data->id,
                            'quantity'=>$quantity[$key],
                            'unique_code'=>$unique_code[$key],
                            'unit_name'=>$unitName[$key],
                            'product_name'=>str_replace('-',' ',$productName[$key]),
                            'is_multi_unit'=>0,
                            'multi_quantity'=>0,
                            'value_conversion'=>1,
                            'multi_unit_id'=>null,
                            'json_multi_unit'=>null,
                            'unit_name_origin_display'=>$unitOrigin[$key]
                        ];
                    }else{
                        $details[]=[
                            'sc_product_id'=>$sc_product_id[$key],
                            'sub_total'=>0,
                            'sc_transfer_stock_id'=>$data->id,
                            'quantity'=>$quantity[$key],
                            'unique_code'=>$unique_code[$key],
                            'unit_name'=>$unitName[$key],
                            'product_name'=>str_replace('-',' ',$productName[$key]),
                            'is_multi_unit'=>1,
                            'multi_quantity'=>$quantity[$key]*$valueConversion[$key],
                            'value_conversion'=>$valueConversion[$key],
                            'multi_unit_id'=>$multiUnitId[$key],
                            'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                            'unit_name_origin_display'=>$unitOrigin[$key]
                        ];
                    }


                    if(($quantity[$key]*$valueConversion[$key])>$residualStock[$key]){
                        return response()->json([
                            'message'=>"Jumlah transfer produk ".$product->name." melebihi kapasitas stok yang tersedia !",
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);
                    }

                    if($from_warehouse->is_default==1 && $from_warehouse->md_merchant_id==$fromMerchant->id){
                        $min=$product->stock-($quantity[$key]*$valueConversion[$key]);
                        $updateStockProduct.=($count==0)?"(".$product->id.",".$min.")":",(".$product->id.",".$min.")";
                        $count++;
                    }else{
                        $min=$product->stock;
                    }

                    array_push($stockMutation,[
                        'sync_id'=>$product->id.Uuid::uuid4()->toString(),
                        'sc_product_id'=>$product->id,
                        'total'=>$quantity[$key]*$valueConversion[$key],
                        'inv_warehouse_id'=>$from_warehouse->id,
                        'record_stock'=>$min,
                        'created_by'=>(get_role()==3)?user_id():get_staff_id(),
                        'residual_stock'=>$quantity[$key]*$valueConversion[$key],
                        'type'=>StockInventory::OUT,
                        'selling_price'=>0,
                        'purchase_price'=>0,
                        'transaction_action'=> 'Pengurangan Stok '.$product->name. ' Dari Transfer Stok Dengan Code '.(is_null($data->second_code)?$data->code:$data->second_code),
                        'stockable_type'=>'App\Models\SennaToko\TransferStock',
                        'stockable_id'=>$data->id,
                        'created_at'=>$data->created_at,
                        'updated_at'=>$data->created_at,
                        'timezone'=>$data->timezone,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$product->name.' '.$product->code
                    ]);

                    if($toWarehouse->is_default==1 && $toWarehouse->md_merchant_id==$toMerchant->id){

                        $plus=$product->stock+($quantity[$key]*$valueConversion[$key]);
                        $updateStockProduct.=($count==0)?"(".$product->id.",".$plus.")":",(".$product->id.",".$plus.")";
                        $count++;
                    }

                    array_push($productCalculateOut,[
                        'id'=>$sc_product_id[$key],
                        'amount'=>($quantity[$key]*$valueConversion[$key])
                    ]);
                }


                if(!empty($productCalculateOut))
                {
                    $calculateInventory=InventoryUtilV2::inventoryCodev2($fromMerchant->md_inventory_method_id,$from_warehouse->id,$productCalculateOut,'minus');
                    if($calculateInventory==false)
                    {
                        return response()->json([
                            'message'=>'Terjadi kesalahan saat melakukan proses pemindahan data stok!',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);

                    }

                    $updateStockInv="";
                    $stockInvId=[];
                    foreach ($calculateInventory as $key => $n)
                    {
                        $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                        $stockInvId[]=$n['id'];
                        $total+=$n['amount']*$n['purchase'];

                        $invs1[]=[
                            'sc_stock_inventory_id'=>$n['id'],
                            'amount'=>$n['amount']
                        ];

                        $p2=$products->firstWhere('id',$n['sc_product_id']);

                        array_push($stockMutation, [
                            'sync_id' => $n['sc_product_id'] . Uuid::uuid4()->toString(),
                            'sc_product_id' => $n['sc_product_id'] ,
                            'total' => $n['amount'],
                            'inv_warehouse_id' => $toWarehouse->id,
                            'record_stock' => ($toWarehouse->is_default==1 && $toWarehouse->md_merchant_id==$toMerchant->id)?$p2->stock+$n['amount']: $n['amount'],
                            'created_by' => (get_role()==3)?user_id():get_staff_id(),
                            'residual_stock' => $n['amount'],
                            'type' => StockInventory::IN,
                            'selling_price' => $p2->selling_price,
                            'purchase_price' => $n['purchase'],
                            'transaction_action' => 'Penambahan Stok ' . $p2->name . ' Dari Transfer Stok Dengan Code ' . (is_null($data->second_code)?$data->code:$data->second_code),
                            'stockable_type' => 'App\Models\SennaToko\TransferStock',
                            'stockable_id' => $data->id,
                            'created_at' => $data->created_at,
                            'updated_at' =>$data->created_at,
                            'timezone'=>$data->timezone,
                            'unit_name'=>'',
                            'product_name'=>$p2->name.' '.$p2->code
                        ]);
                    }


                    if($updateStockInv!=""){
                        DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                    }
                    if($updateStockProduct!=""){
                        DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            ".$updateStockProduct."
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                    }
                    DB::table('sc_inv_production_of_goods')
                        ->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);

                }

                $data->total=$total;
                $data->invs=json_encode($invs1);
                $data->save();
                TransferStockDetail::insert($details);
                StockInventory::insert($stockMutation);

                DB::commit();

                return response()->json([
                    'message'=>'Data transfer stok berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>route('merchant.toko.transfer-stock.data'),

                ]);
            }else{
                $detailBetweenBranch=[];
                $stockMutationInOut=[];
                $totalIn=0;

                $uniqueCode="";
                foreach ($unique_code as $key => $v)
                {
                    $uniqueCode.=($key==0)?"'".$unique_code[$key]."'":",'".$unique_code[$key]."'";
                }
                $products=collect(DB::select("
                select
                  p.*
                from sc_products p
                  join md_merchants m on m.md_user_id=p.md_user_id
                  where m.id=".$fromMerchant->id."
                  and p.code in($uniqueCode)
                  and p.is_deleted=0
                  and p.is_with_stock=1
                "));

                $updateStockProductOut="";
                $updateStockProductIn="";
                $countOut=0;
                $countIn=0;

                $jurnalDetailOut=[];
                $jurnalDetailIn=[];
                $productCalculateIn=[];

                $amount=[];
                $invs2=[];

                $productIn=collect(DB::select("
                 select
                  p.*,
                  '1' as transfer_type
                 from sc_products p
                  join md_merchants m on m.md_user_id=p.md_user_id
                  where m.id=".$toMerchant->id."
                  and p.code in($uniqueCode)
                  and p.is_deleted=0
                  and p.is_with_stock=1
                  "));

                foreach ($products as $key =>$itemProduct)
                {
                    if(is_null($productIn->where('code',$itemProduct->code))){

                        return response()->json([
                            'message'=>"Produk ".$itemProduct->name." belum ada di cabang yang dituju!",
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);
                    }

                    if($residualStock[$key]==0){

                        return response()->json([
                            'message'=>"Jumlah produk ".$itemProduct->name." kosong !",
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);
                    }
                    if(($quantity[$key]*$valueConversion[$key])>$residualStock[$key]){

                        return response()->json([
                            'message'=>"Jumlah transfer produk ".$itemProduct->name." melebihi kapasitas stok yang tersedia !",
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);
                    }

                    if($unitList[$key]=='-1' || $unitList[$key]==-1){
                        return response()->json([
                            'message'=>"Ada produk yang belum memilih satuan !",
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);
                    }
                    if($isMultiUnit[$key]==0){

                        $detailBetweenBranch[]=[
                            'sc_product_id'=>$itemProduct->id,
                            'sub_total'=>0,
                            'sc_transfer_stock_id'=>$data->id,
                            'quantity'=>$quantity[$key],
                            'unique_code'=>$unique_code[$key],
                            'unit_name'=>$unitName[$key],
                            'product_name'=>str_replace('-',' ',$productName[$key]),
                            'is_multi_unit'=>0,
                            'multi_quantity'=>0,
                            'value_conversion'=>1,
                            'multi_unit_id'=>null,
                            'json_multi_unit'=>null,
                            'unit_name_origin_display'=>$unitOrigin[$key]
                        ];
                    }else{
                        $detailBetweenBranch[]=[
                            'sc_product_id'=>$sc_product_id[$key],
                            'sub_total'=>0,
                            'sc_transfer_stock_id'=>$data->id,
                            'quantity'=>$quantity[$key],
                            'unique_code'=>$unique_code[$key],
                            'unit_name'=>$unitName[$key],
                            'product_name'=>str_replace('-',' ',$productName[$key]),
                            'is_multi_unit'=>1,
                            'multi_quantity'=>$quantity[$key]*$valueConversion[$key],
                            'value_conversion'=>$valueConversion[$key],
                            'multi_unit_id'=>$multiUnitId[$key],
                            'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                            'unit_name_origin_display'=>$unitOrigin[$key]
                        ];
                    }


                    if($from_warehouse->is_default==1 && $from_warehouse->md_merchant_id==$fromMerchant->id){
                        $minOut=$itemProduct->stock-($quantity[$key]*$valueConversion[$key]);
                        $updateStockProductOut.=($countOut==0)?"(".$itemProduct->id.",".$minOut.")":",(".$itemProduct->id.",".$minOut.")";
                        $countOut++;
                    }else{
                        $minOut=$itemProduct->stock;
                    }

                    array_push($stockMutationInOut,[
                        'sync_id'=>$itemProduct->id.Uuid::uuid4()->toString(),
                        'sc_product_id'=>$itemProduct->id,
                        'total'=>$quantity[$key]*$valueConversion[$key],
                        'inv_warehouse_id'=>$from_warehouse->id,
                        'record_stock'=>$minOut,
                        'created_by'=>(get_role()==3)?user_id():get_staff_id(),
                        'residual_stock'=>$quantity[$key]*$valueConversion[$key],
                        'type'=>StockInventory::OUT,
                        'selling_price'=>0,
                        'purchase_price'=>0,
                        'transaction_action'=> 'Pengurangan Stok '.$itemProduct->name. ' Dari Transfer Stok Dengan Code '.$code,
                        'stockable_type'=>'App\Models\SennaToko\TransferStock',
                        'stockable_id'=>$data->id,
                        'created_at'=>$data->created_at,
                        'updated_at'=>$data->created_at,
                        'timezone'=>$data->timezone,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$itemProduct->name.' '.$itemProduct->code
                    ]);

                    array_push($jurnalDetailOut,[
                        'acc_coa_detail_id'=>$itemProduct->inv_id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'created_at'=>$data->created_at,
                        'updated_at'=>$data->updated_at,
                    ]);

                    array_push($productCalculateIn,[
                        'id'=>$sc_product_id[$key],
                        'amount'=>$quantity[$key]*$valueConversion[$key]
                    ]);
                }

                TransferStockDetail::insert($detailBetweenBranch);


                foreach ($productIn as $num =>$itemProductIn) {
                    if ($toWarehouse->is_default == 1 && $toWarehouse->md_merchant_id == $toMerchant->id) {
                        $plus = $itemProductIn->stock + ($quantity[$num]*$valueConversion[$num]);
                        $updateStockProductIn .= ($countIn == 0) ? "(" . $itemProductIn->id . "," . $plus . ")" : ",(" . $itemProductIn->id . "," . $plus . ")";
                        $countIn++;
                    }
                    array_push($jurnalDetailIn,[
                        'acc_coa_detail_id'=>$itemProductIn->inv_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'created_at'=>$data->created_at,
                        'updated_at'=>$data->updated_at,
                    ]);
                }


                if(!empty($productCalculateIn))
                {
                    $calculateInventory=InventoryUtilV2::inventoryCodev2($fromMerchant->md_inventory_method_id,$from_warehouse->id,$productCalculateIn,'minus');
                    if($calculateInventory==false)
                    {
                        return response()->json([
                            'message'=>'Terjadi kesalahan saat melakukan proses pemindahan data stok!',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',

                        ]);
                    }

                    $updateStockInv="";
                    $stockInvId=[];

                    foreach ($calculateInventory as $key => $n)
                    {
                        $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                        $stockInvId[]=$n['id'];
                        $totalIn+=$n['amount']*$n['purchase'];
                        $origin=$products->firstWhere('id',$n['sc_product_id']);
                        $toDuplicate=$productIn->firstWhere('code',$origin->code);

                        $invs2[]=[
                            'sc_stock_inventory_id'=>$n['id'],
                            'amount'=>$n['amount']
                        ];
                        if(is_null($toDuplicate)){

                            return response()->json([
                                'message'=>"Produk ".$origin->name." belum ada di cabang yang dituju!",
                                'type'=>'warning',
                                'is_modal'=>false,
                                'redirect_url'=>'',
                            ]);
                        }

                        array_push($stockMutationInOut, [
                            'sync_id' => $toDuplicate->id. Uuid::uuid4()->toString(),
                            'sc_product_id' =>$toDuplicate->id,
                            'total' => $n['amount'],
                            'inv_warehouse_id' => $toWarehouse->id,
                            'record_stock' => ($toWarehouse->is_default == 1 && $toWarehouse->md_merchant_id == $toMerchant->id)?$toDuplicate->stock+$n['amount']:$n['amount'],
                            'created_by' => (get_role()==3)?user_id():get_staff_id(),
                            'residual_stock' => $n['amount'],
                            'type' => StockInventory::IN,
                            'selling_price' => $toDuplicate->selling_price,
                            'purchase_price' => $n['purchase'],
                            'transaction_action' => 'Penambahan Stok ' . $toDuplicate->name . ' Dari Transfer Stok Dengan Code ' . (is_null($data->second_code)?$data->code:$data->second_code),
                            'stockable_type' => 'App\Models\SennaToko\TransferStock',
                            'stockable_id' => $data->id,
                            'created_at' => $data->created_at,
                            'updated_at' => $data->created_at,
                            'timezone'=>$data->timezone,
                            'unit_name'=>'',
                            'product_name'=>$toDuplicate->name.' '.$toDuplicate->code
                        ]);
                    }


                    $amount=[];
                    foreach ($products as $ip =>$in)
                    {
                        $subTotal=0;
                        foreach ($calculateInventory as $c =>$cc)
                        {
                            if($in->id==$cc['sc_product_id']){
                                $subTotal+=$cc['purchase']*$cc['amount'];
                            }
                        }
                        $amount[]=$subTotal;
                    }


                    if($updateStockInv!=""){
                        DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                    }
                    if($updateStockProductOut!=""){
                        DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            ".$updateStockProductOut."
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                    }

                    if($updateStockProductIn!=""){
                        DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            ".$updateStockProductIn."
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");


                    }
                    DB::table('sc_inv_production_of_goods')
                        ->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);
                }
                StockInventory::insert($stockMutationInOut);
                $data->total=$totalIn;
                $data->invs=json_encode($invs2);
                $data->save();

                if(MutationUtil::createJurnalMutation($data,$fromMerchant,$toMerchant,$initCoaTransfer1,$initCoaTransfer2,$jurnalDetailOut,$jurnalDetailIn,$totalIn,$amount)==false){
                    DB::rollBack();

                    return response()->json([
                        'message'=>'Terjadi kesalahan saat melakukan penulisan jurnal mutasi stok!',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>'',
                    ]);

                }

                DB::commit();

                return response()->json([
                    'message'=>'Data transfer stok berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>route('merchant.toko.transfer-stock.data'),

                ]);
            }
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',

            ]);

        }
    }

    public function dataTable(Request $request)
    {
        return TransferStockEntity::dataTable($request);
    }


    public function reloadData(Request $request)
    {

        $searchParams = [];
        foreach (TransferStockEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Mutasi Stok',
            'searchKey' => $searchKey,
            'tableColumns'=>TransferStockEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.report.transfer-stock.list',$params);

    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            $data = TransferStockEntity::getDataForDataTable();
            $searchKey = $request->input('search_key');
            $searchFilter = $request->session()->get($searchKey, []);
            $searchFilter = is_array($searchFilter) ? $searchFilter : [];
            $getBranch=MerchantUtil::getBranch(merchant_id());

            if($request->md_merchant_id=='-1'){

                $data->whereIn('sc_transfer_stocks.from_merchant_id',$getBranch);

            }else{
                foreach (TransferStockEntity::getFilterMap() as $key => $field) {
                    if (isset($searchFilter[$key]) && $searchFilter[$key] != '-') {
                        $data->where([$field => $searchFilter[$key]]);
                    }
                }
            }

            $data->whereRaw("
            sc_transfer_stocks.created_at::date between '$startDate' and '$endDate'
            ");

            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Tanggal Mutasi Stok $startDate sd $endDate");

            $header = [
                'No',
                'Kode',
                'Cabang Asal',
                'Gudang Asal',
                'Cabang Tujuan',
                'Gudang Tujuan',
                'Produk',
                'Satuan',
                'Jumlah Transfer',
                'Waktu Mutasi',
                'Zona Waktu'
            ];
            $col = 'A';
            foreach ($header as $v) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '2', strtoupper($v));
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->getFont()->setBold( true );
                $col++;
            }
            $num = 2;
            $no=1;
            foreach ($data->get() as $key =>$item)
            {
                foreach ($item->getDetail as $n =>$j) {
                    $unitName= (is_null($j->getProduct->getUnit))?'':$j->getProduct->getUnit->name;

                    $dataShow = [
                        $no,
                        $item->kode,
                        $item->outlet_asal,
                        $item->gudang_asal,
                        $item->outlet_tujuan,
                        $item->gudang_tujuan,
                        is_null($j->product_name)?$j->getProduct->name.' '.$j->getProduct->code : $j->product_name,
                        $j->quantity,
                        is_null($j->unit_name)?$unitName:$j->unit_name,
                        $item->waktu_mutasi_stok,
                        getTimeZoneName($item->zona_waktu)
                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                    $no++;
                }
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-stock_mutation_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
                    <div id='successExport' class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        let success = document.querySelector('#successExport');
                        if(success){
                            toastForSaveData('Data berhasil diexport!','success',true,'',false);
                        }
                    </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',

            ]);

        }

    }




}

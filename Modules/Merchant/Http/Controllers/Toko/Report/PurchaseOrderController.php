<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Report;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use App\Models\MasterData\TransactionType;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\Supplier;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\StockInventory;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\PurchaseOrderEntity;
use Modules\Merchant\Entities\Toko\PurchaseOrderOrderEntity;
use Modules\Merchant\Entities\Toko\PurchaseOrderOfferEntity;
use Modules\Merchant\Entities\Toko\PurchaseOrderDeliveryEntity;
use Modules\Merchant\Entities\Toko\SaleOrderEntity;
use Modules\Merchant\Entities\Toko\ReturPurchaseOrderEntity;
use Modules\Merchant\Entities\Toko\StockFlowEntity;
use Modules\Merchant\Entities\Toko\StockOpnameEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;
use PDF;
use Mail;
use Illuminate\Support\Facades\DB;

class PurchaseOrderController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('report/purchase-order')
            ->middleware('merchant-verification')
            ->group(function (){

                Route::get('/data', 'Toko\Report\PurchaseOrderController@data')
                    ->name('merchant.toko.purchase-order.data');
                Route::get('/data-order', 'Toko\Report\PurchaseOrderController@dataOrder')
                    ->name('merchant.toko.purchase-order.data-order');
                Route::get('/data-offer', 'Toko\Report\PurchaseOrderController@dataOffer')
                    ->name('merchant.toko.purchase-order.data-offer');
                    Route::get('/data-delivery', 'Toko\Report\PurchaseOrderController@dataDelivery')
                    ->name('merchant.toko.purchase-order.data-delivery');

                Route::get('/detail/{id}', 'Toko\Report\PurchaseOrderController@detail')
                    ->name('merchant.toko.purchase-order.detail');
                Route::get('/detail-order/{id}', 'Toko\Report\PurchaseOrderController@detailOrder')
                    ->name('merchant.toko.purchase-order.detail-order');
                Route::get('/detail-offer/{id}', 'Toko\Report\PurchaseOrderController@detailOffer')
                    ->name('merchant.toko.purchase-order.detail-offer');
                Route::get('/detail-delivery/{id}', 'Toko\Report\PurchaseOrderController@detailDelivery')
                    ->name('merchant.toko.purchase-order.detail-delivery');

                Route::post('/data-table', 'Toko\Report\PurchaseOrderController@dataTable')
                    ->name('merchant.toko.purchase-order.datatable');
                Route::post('/data-table-order', 'Toko\Report\PurchaseOrderController@dataTableOrder')
                    ->name('merchant.toko.purchase-order.datatable-order');
                Route::post('/data-table-offer', 'Toko\Report\PurchaseOrderController@dataTableOffer')
                    ->name('merchant.toko.purchase-order.datatable-offer');
                Route::post('/data-table-delivery', 'Toko\Report\PurchaseOrderController@dataTableDelivery')
                    ->name('merchant.toko.purchase-order.datatable-delivery');


                Route::post('/reload-data', 'Toko\Report\PurchaseOrderController@reloadData')
                    ->name('merchant.toko.purchase-order.reload-data');
                Route::post('/reload-data-offer', 'Toko\Report\PurchaseOrderController@reloadDataOffer')
                    ->name('merchant.toko.purchase-order.reload-data-offer');
                Route::post('/reload-data-order', 'Toko\Report\PurchaseOrderController@reloadDataOrder')
                    ->name('merchant.toko.purchase-order.reload-data-order');
                Route::post('/reload-data-delivery', 'Toko\Report\PurchaseOrderController@reloadDataDelivery')
                    ->name('merchant.toko.purchase-order.reload-data-delivery');

                Route::post('/export-data', 'Toko\Report\PurchaseOrderController@exportData')
                    ->name('merchant.toko.purchase-order.export-data');
                Route::post('/export-data-retur', 'Toko\Report\PurchaseOrderController@exportDataRetur')
                    ->name('merchant.toko.retur-purchase-order.export-data-excel');
                Route::post('/export-date-range', 'Toko\Report\PurchaseOrderController@exportByDateRange')
                    ->name('merchant.toko.purchase-order.export-date-range');

                Route::get('/cetakFakturPDF/{id}', 'Toko\Report\PurchaseOrderController@cetakFakturPDF')
                    ->name('merchant.toko.purchase-order.export-data-faktur-pdf');
                Route::get('/cetakReturPDF/{id}/{returId}', 'Toko\Report\PurchaseOrderController@cetakReturPDF')
                    ->name('merchant.toko.purchase-order.export-data-retur');
                Route::get('/cetakTagihanPDF/{id}/{apId}', 'Toko\Report\PurchaseOrderController@cetakTagihanPDF')
                    ->name('merchant.toko.purchase-order.export-data-bill');
                Route::get('/cetakLaporanPembayaranPDF/{id}', 'Toko\Report\PurchaseOrderController@cetakLaporanPembayaranPDF')
                    ->name('merchant.toko.purchase-order.export-payment-report');
                Route::post('/cetakFakturExcel', 'Toko\Report\PurchaseOrderController@cetakFakturExcel')
                    ->name('merchant.toko.purchase-order.export-faktur-excel');
            });
    }


    public function cetakFakturPDF($id){
        try{
            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            if($data->step_type==1)
            {
                $pdf = PDF::loadview('merchant::toko.report.purchase-order.purchase-offer-pdf',["data" => $data])->setPaper('a4', 'potrait');
                return $pdf->download('laporan-faktur-penawaran-'.$data->code.'.pdf');

            }elseif ($data->step_type==2)
            {
                $pdf = PDF::loadview('merchant::toko.report.purchase-order.purchase-order-pdf',["data" => $data])->setPaper('a4', 'potrait');
                return $pdf->download('laporan-faktur-pemesanan-'.$data->code.'.pdf');

            }elseif ($data->step_type==3)
            {
                $pdf = PDF::loadview('merchant::toko.report.purchase-order.purchase-delivery-pdf',["data" => $data])->setPaper('a4', 'potrait');
                return $pdf->download('laporan-faktur-delivery-'.$data->code.'.pdf');

            }else{
                $pdf = PDF::loadview('merchant::toko.report.purchase-order.purchase-invoice-pdf',["data" => $data])->setPaper('a4', 'potrait');
                return $pdf->download('laporan-faktur-pembelian-'.$data->code.'.pdf');

            }


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return redirect()->back()->with(['error-export'=> "Terjadi Kesalahan di Server, Data Gagal di Export"]);
        }

    }

    public function cetakReturPDF($id, $returId, Request $request){
        try{
            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            $retur = ReturPurchaseOrder::find($returId);

            $pdf = PDF::loadview('merchant::toko.report.purchase-order.retur-pdf',["data" => $data, "retur"=>$retur])->setPaper('a4', 'potrait');
            return $pdf->download('laporan-retur-pembelian-'.$data->code.'.pdf');

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return redirect()->back()->with(['error-export'=> "Terjadi Kesalahan di Server, Data Gagal di Export"]);
        }

    }

    public function cetakTagihanPDF($id, $apId,Request $request){
        try{
            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            $payment = $data->ap->getDetail->where('id', $apId)->first();

            $pdf = PDF::loadview('merchant::toko.report.purchase-order.bill-pdf',["data" => $data, "payment" => $payment])->setPaper('a4', 'potrait');
            return $pdf->download('laporan-tagihan-pembelian-'.$data->code.'.pdf');


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return redirect()->back()->with(['error-export'=> "Terjadi Kesalahan di Server, Data Gagal di Export"]);
        }

    }

    public function cetakLaporanPembayaranPDF($id, Request $request){
        try{
            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            $pdf = PDF::loadview('merchant::toko.report.purchase-order.payment-report-pdf',["data"=>$data])->setPaper('a4', 'landscape');
            return $pdf->download('laporan-pembayaran-pembelian-'.$data->code.'.pdf');

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return redirect()->back()->with(['error-export'=> "Terjadi Kesalahan di Server, Data Gagal di Export"]);
        }

    }



    public function data()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Faktur Pembelian',
            'merchantId'=>$merchantId,
            'tableColumns'=>PurchaseOrderEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'supplier'=>SupplierEntity::listOption(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
        ];
        return view('merchant::toko.report.purchase-order.data',$params);
    }

    public function dataOrder()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Pemesanan Pembelian',
            'merchantId'=>$merchantId,
            'tableColumns'=>PurchaseOrderOrderEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'supplier'=>SupplierEntity::listOption(),

        ];
        return view('merchant::toko.report.purchase-order.data-order',$params);
    }

    public function dataOffer()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Penawaran Pembelian',
            'merchantId'=>$merchantId,
            'tableColumns'=>PurchaseOrderOfferEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'supplier'=>SupplierEntity::listOption(),

        ];
        return view('merchant::toko.report.purchase-order.data-offer',$params);
    }

    public function dataDelivery()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Penerimaan Pembelian',
            'merchantId'=>$merchantId,
            'tableColumns'=>PurchaseOrderDeliveryEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'supplier'=>SupplierEntity::listOption(),

        ];
        return view('merchant::toko.report.purchase-order.data-delivery',$params);
    }

    public function dataTable(Request $request)
    {
        return PurchaseOrderEntity::dataTable($request);
    }

    public function dataTableOrder(Request $request)
    {
        return PurchaseOrderOrderEntity::dataTable($request);
    }

    public function dataTableOffer(Request $request)
    {
        return PurchaseOrderOfferEntity::dataTable($request);
    }

    public function dataTableDelivery(Request $request)
    {
        return PurchaseOrderDeliveryEntity::dataTable($request);
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (PurchaseOrderEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            $searchParams[$key] = $query;
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;
        $params = [
            'title' => 'Faktur Pembelian',
            'searchKey' => $searchKey,
            'tableColumns'=>PurchaseOrderEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->merchant_id
        ];
        return view('merchant::toko.report.purchase-order.list',$params);

    }

    public function reloadDataOrder(Request $request)
    {
        $searchParams = [];
        foreach (PurchaseOrderOrderEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            $searchParams[$key] = $query;
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Pemesanan Pembelian',
            'searchKey' => $searchKey,
            'tableColumns'=>PurchaseOrderOrderEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->merchant_id

        ];
        return view('merchant::toko.report.purchase-order.list-order',$params);

    }

    public function reloadDataOffer(Request $request)
    {
        $searchParams = [];
        foreach (PurchaseOrderOfferEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            $searchParams[$key] = $query;
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Penawaran Pembelian',
            'searchKey' => $searchKey,
            'tableColumns'=>PurchaseOrderOfferEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->merchant_id

        ];
        return view('merchant::toko.report.purchase-order.list-offer',$params);

    }

    public function reloadDataDelivery(Request $request)
    {
        $searchParams = [];
        foreach (PurchaseOrderDeliveryEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            $searchParams[$key] = $query;
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Penerimaan Pembelian',
            'searchKey' => $searchKey,
            'tableColumns'=>PurchaseOrderDeliveryEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->merchant_id

        ];
        return view('merchant::toko.report.purchase-order.list-delivery',$params);

    }

    public function exportData(Request $request,$fromGraphic=0,$periodFilter='',$time='')
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            if($request->step_type==0){
                $po = PurchaseOrderEntity::class;
                $data=$po::getDataForDataTable();
            }else if($request->step_type==1){
                $po = PurchaseOrderOfferEntity::class;
                $data=$po::getDataForDataTable();
            }else if($request->step_type==2){
                $po = PurchaseOrderOrderEntity::class;
                $data=$po::getDataForDataTable();
            }else{
                $po = PurchaseOrderDeliveryEntity::class;
                $data=$po::getDataForDataTable();
            }

            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            $searchKey = $request->input('search_key');
            $searchFilter = $request->session()->get($searchKey, []);
            $searchFilter = is_array($searchFilter) ? $searchFilter : [];

            $getBranch=MerchantUtil::getBranch(merchant_id());

            if($request->md_merchant_id=='-1'){

                $data->whereIn('sc_purchase_orders.md_merchant_id',$getBranch);

            }else{
                foreach ($po::getFilterMap() as $key => $field) {
                    if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                        $data->where([$field => $searchFilter[$key]]);
                    }
                }

            }
            $data->whereRaw("
                    sc_purchase_orders.created_at::date between '$startDate' and '$endDate'
                    ");

            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            if($request->step_type==0)
            {
                $title= "Faktur Pembelian";

            }elseif ($request->step_type==1)
            {
                $title= "Penawaran Pembelian";

            }elseif ($request->step_type==2)
            {

                $title= "Pemesanan Pembelian";


            }else{
                $title= "Pengiriman Pembelian";

            }

            $totalPurchase = 0;
            $totalDebit = 0;
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"$title Tanggal $startDate sd $endDate");
            $header = [
                'No',
                'Kode',
                'Supplier',
                'Hutang',
                'Waktu Transaksi',
                'Produk',
                'Jumlah',
                'Harga',
                'Satuan',
                'Sub Total'
            ];
            $col = 'A';
            foreach ($header as $v) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '2', strtoupper($v));
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->getFont()->setBold( true );
                $col++;
            }
            $num = 2;
            $no=0;
            foreach ($data->get() as $key =>$item)
            {
                if($request->step_type==0)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:$item->code;
                    $trans=($item->trans_name=='Kas' || $item->trans_name=='Kas Kasir Outlet')?'':'-'.$item->trans_name;
                    $transName=is_null($item->trans_name)?$item->tipe_transaksi:$item->tipe_transaksi.$trans;
                    $payment=(is_null($item->tipe_transaksi))?'Hutang':$transName;
                }elseif ($request->step_type==1)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PQ',$item->code);
                    $payment='Belum Ada Pembayaran';

                }elseif ($request->step_type==2)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PO',$item->code);
                    $payment='Belum Ada Pembayaran';

                }else{
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PD',$item->code);
                    $payment='Belum Ada Pembayaran';

                }


                foreach ($item->getDetail->where('is_deleted',0) as $n =>$j) {
                    $unitName= (is_null($j->getProduct->getUnit))?'':$j->getProduct->getUnit->name;
                    if($j->is_multi_unit==1)
                    {
                        if(is_null($j->unit_name)){
                            $q=$j->multi_quantity;
                        }else{
                            $q=$j->quantity;
                        }
                    }else{
                        $q=$j->quantity;
                    }
                    $dataShow = [
                        $no,
                        $kode,
                        (is_null($item->nama_supplier))?'':$item->nama_supplier,
                        (is_null($item->hutang))?rupiah(0):rupiah($item->hutang),
                        $item->waktu_transaksi,
                        is_null($j->product_name)?$j->getProduct->name.' '.$j->getProduct->code: $j->product_name,
                        $q,
                        rupiah($j->price),
                        is_null($j->unit_name)?$unitName:$j->unit_name,
                        rupiah($j->sub_total)
                    ];
                    $col = 'A';
                    $row = $num + 1;
                    $totalPurchase += $j->sub_total;
                    $totalDebit += $item->hutang;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                    $no++;
                }
            }

            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $rowDebit = $num+3;
            $rowTotal = $num+4;
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $rowDebit, "Total Hutang");
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $rowTotal, "Total Pembelian");
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $rowDebit, rupiah($totalDebit));
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $rowTotal, rupiah($totalPurchase));


            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }

            if($request->step_type==1){
                $filename = strtolower('export-penawaran-pembelian_'.$request->md_merchant_id."_".$startDate."_".$endDate.'.xlsx');
            }else if($request->step_type==2){
                $filename = strtolower('export-pemesanan-pembelian_'.$request->md_merchant_id."_".$startDate."_".$endDate.'.xlsx');
            }else if($request->step_type==3){
                $filename = strtolower('export-pengiriman-pembelian_'.$request->md_merchant_id."_".$startDate."_".$endDate.'.xlsx');
            }else{
                $filename = strtolower('export-pembelian_'.$request->md_merchant_id."_".$startDate."_".$endDate.'.xlsx');
            }

            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);

            return "<script> location.href='$fileURL'; </script>
                    <div id='successExport'></div>
                    <div class='text-center'>
                    <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                </div>
                <script>
                    let success = document.querySelector('#successExport');
                    if(success){
                        toastForSaveData('Data berhasil diexport!','success',true,'',false);
                    }
                </script>
            ";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }

    }

    public function detail($id,Request  $request)
    {
        try{
            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            $pluck=$data->stock->pluck('id');
            $check=StockInventory::whereIn('sc_stock_inventories.id',$pluck->all())
                ->where('sc_stock_inventories.type','in')
                ->whereRaw("sc_stock_inventories.transaction_action not like '%Retur%'")
                ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                ->where('sc_stock_inventories.is_deleted',0)
                ->orderBy('sc_stock_inventories.id','desc')
                ->count();
            if($check==0)
            {
                $isAddRetur=true;
            }else{
                $isAddRetur=false;
            }

            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'page'=>(is_null($request->page))?'transaksi':$request->page,
                'isAddRetur'=>$isAddRetur
            ];
            return view('merchant::toko.report.purchase-order.detail',$params);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }

    }

    public function detailOrder($id,Request  $request)
    {
        try{
            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            $pluck=$data->stock->pluck('id');
            $check=StockInventory::whereIn('sc_stock_inventories.id',$pluck->all())
                ->where('sc_stock_inventories.type','in')
                ->whereRaw("sc_stock_inventories.transaction_action not like '%Retur%'")
                ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                ->where('sc_stock_inventories.is_deleted',0)
                ->orderBy('sc_stock_inventories.id','desc')
                ->count();
            if($check==0)
            {
                $isAddRetur=true;
            }else{
                $isAddRetur=false;
            }

            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'page'=>(is_null($request->page))?'transaksi':$request->page,
                'isAddRetur'=>$isAddRetur
            ];
            return view('merchant::toko.report.purchase-order.detail-order',$params);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }

    }

    public function detailOffer($id,Request  $request)
    {
        try{
            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            $pluck=$data->stock->pluck('id');
            $check=StockInventory::whereIn('sc_stock_inventories.id',$pluck->all())
                ->where('sc_stock_inventories.type','in')
                ->whereRaw("sc_stock_inventories.transaction_action not like '%Retur%'")
                ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                ->where('sc_stock_inventories.is_deleted',0)
                ->orderBy('sc_stock_inventories.id','desc')
                ->count();
            if($check==0)
            {
                $isAddRetur=true;
            }else{
                $isAddRetur=false;
            }

            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'page'=>(is_null($request->page))?'transaksi':$request->page,
                'isAddRetur'=>$isAddRetur
            ];
            return view('merchant::toko.report.purchase-order.detail-offer',$params);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }

    }

    public function detailDelivery($id,Request  $request)
    {
        try{
            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            $pluck=$data->stock->pluck('id');
            $check=StockInventory::whereIn('sc_stock_inventories.id',$pluck->all())
                ->where('sc_stock_inventories.type','in')
                ->whereRaw("sc_stock_inventories.transaction_action not like '%Retur%'")
                ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                ->where('sc_stock_inventories.is_deleted',0)
                ->orderBy('sc_stock_inventories.id','desc')
                ->count();
            if($check==0)
            {
                $isAddRetur=true;
            }else{
                $isAddRetur=false;
            }

            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'page'=>(is_null($request->page))?'transaksi':$request->page,
                'isAddRetur'=>$isAddRetur
            ];
            return view('merchant::toko.report.purchase-order.detail-delivery',$params);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }

    }


    public function cetakFakturExcel(Request $request)
    {
        try{
            $id = $request->id;
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            $styleTextCenter = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ]
            ];

            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $data=PurchaseOrder::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with('getMerchant')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit');
                }])
                ->where('id',$id)
                ->first();

            foreach($objPHPExcel->getActiveSheet()->getMergeCells() as $cells){
              $objPHPExcel->getActiveSheet()->unmergeCells($cells);
            }

            $objPHPExcel->getActiveSheet()->setCellValue('B3',$data->getMerchant->name)->getStyle('B3')->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->mergeCells('B3:C3')->getStyle('B3')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->setCellValue('B4',"No.telp:");
            $objPHPExcel->getActiveSheet()->setCellValue('C4',$data->getMerchant->phone_merchant);

            $objPHPExcel->getActiveSheet()->setCellValue('B5',"Alamat:");
            $objPHPExcel->getActiveSheet()->setCellValue('C5',$data->getMerchant->address);

            if($data->step_type==0)
            {
                $title='Faktur Pembelian';
                $kode=(!is_null($data->second_code))?$data->second_code:$data->code;
                $tax=$data->tax;
                $discount=$data->discount;
                $total=$data->total;
            }elseif ($data->step_type==1)
            {
                $title='Penawaran Pembelian';
                $kode=(!is_null($data->second_code))?$data->second_code:str_replace('P','PQ',$data->code);
                $tax=$data->tax_offer;
                $discount=$data->promo_offer;
                $total=$data->total_offer;
            }elseif ($data->step_type==2)
            {
                $title='Pemesanan Pembelian';
                $kode=(!is_null($data->second_code))?$data->second_code:str_replace('P','PO',$data->code);
                $tax=$data->tax_order;
                $discount=$data->promo_order;
                $total=$data->total_order;
            }else{
                $title= 'Penerimaan Pembelian';
                $kode=(!is_null($data->second_code))?$data->second_code:str_replace('P','PD',$data->code);
                $tax=$data->tax_delivery;
                $discount=$data->promo_delivery;
                $total=$data->total_delivery;
            }

            $objPHPExcel->getActiveSheet()->setCellValue('E3',"$title")->getStyle('E3')->getFont()->setSize(25);
            $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($styleTextCenter)->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->mergeCells('E3:H5');

            $objPHPExcel->getActiveSheet()->setCellValue('B7',"Kode Transaksi:");
            $objPHPExcel->getActiveSheet()->setCellValue('C7',$kode);
            $objPHPExcel->getActiveSheet()->setCellValue('B8',"Supplier:");
            $objPHPExcel->getActiveSheet()->setCellValue('C8',$data->getSupplier->name);
            $objPHPExcel->getActiveSheet()->setCellValue('B9',"Gudang:");
            $objPHPExcel->getActiveSheet()->setCellValue('C9',(is_null($data->getWarehouse))?'':$data->getWarehouse->name);

            if($data->step_type==0){
                if($data->is_debet==1){
                    $objPHPExcel->getActiveSheet()->setCellValue('G7',"Hutang:");
                    $objPHPExcel->getActiveSheet()->setCellValue('H7','Ya');
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue('G7',"Pembayaran:");
                    $objPHPExcel->getActiveSheet()->setCellValue('H7',$data->getTransactionType->name);
                }

            }

            $objPHPExcel->getActiveSheet()->setCellValue('G8',"Waktu Transaksi:");
            $objPHPExcel->getActiveSheet()->setCellValue('H8',Carbon::parse($data->created_at)->isoFormat('dddd, D MMMM Y, HH:mm'));


            $header = [
                'No',
                'Kode',
                'Barang',
                'Jumlah',
                'Satuan',
                'Harga Satuan',
                'Sub Total'
            ];
            $col = 'B';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '11', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '11')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '11')->getFont()->setBold( true );
                $col++;
            }

            $firstRow = "B2:".$objPHPExcel->getActiveSheet()->getHighestColumn()."2";
            $objPHPExcel->getActiveSheet()->mergeCells($firstRow);

            $num = 11;

            foreach ($data->getDetail->where('is_deleted', 0) as $key => $item) {
                if($item->is_multi_unit==1)
                {
                    if(is_null($item->unit_name)){
                        $q=$item->multi_quantity;
                        $u=$item->getProduct->getUnit->name;
                    }else{
                        $q=$item->quantity;
                        $u=$item->unit_name;
                    }
                }else{
                    $q=$item->quantity;
                    $u=!is_null($item->unit_name)?$item->unit_name:$item->getProduct->getUnit->name;
                }
                $dataShow = [
                    $key + 1,
                    $item->getProduct->code,
                    $item->getProduct->name,
                    $q,
                    $u,
                    rupiah($item->price),
                    rupiah($item->price * $item->quantity)
                ];

                $col = 'B';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                    $col++;
                }
                $num++;
            }

            $lastrow = $objPHPExcel->getActiveSheet()->getHighestRow() + 3;
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$lastrow, 'Nama Pencatat');
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$lastrow, $data->getStaff->fullname);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($lastrow + 1), 'Status Transaksi');
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($lastrow + 1), ($data->is_deleted==1)?'Terhapus':'-');
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($lastrow + 2), 'Catatan');
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($lastrow + 2), is_null($data->note_order)?'-':$data->note_order);

            $objPHPExcel->getActiveSheet()->setCellValue('G'.$lastrow, 'Diskon');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 1), 'Total Harga');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 2), 'Dibayar');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 3), 'Sisa Tagihan');

            $objPHPExcel->getActiveSheet()->setCellValue('H'.$lastrow, rupiah($discount));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 1), rupiah($total));

            if($data->step_type==0){
                if($data->is_debet == 0){
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 2), rupiah($data->total));
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 3), rupiah(0));
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 2), rupiah($data->ap->paid_nominal));
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 3), rupiah($data->ap->residual_amount));
                }
            }

            $lastColRow = $objPHPExcel->getActiveSheet()->getHighestColumn().$objPHPExcel->getActiveSheet()->getHighestRow();
            $objPHPExcel->getActiveSheet()->getStyle('B2:'.$lastColRow)->applyFromArray($styleArray);

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/purchase-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('laporan-'.str_replace(' ','',$title).'-'.$data->code.'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
              <div id='successExport' class='text-center'>
                  <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
              </div>
              <script>
                if(document.querySelector('#successExport')){
                  toastForSaveData('Data berhasil diexport!','success',false,'',false);
                }
              </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }


}
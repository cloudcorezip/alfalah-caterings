<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */
namespace Modules\Merchant\Http\Controllers\Toko\Report;


use App\Classes\RangeCriteria;
use App\Http\Controllers\Controller;
use App\Models\Accounting\MerchantAr;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\MasterData\TransactionType;
use App\Models\Paypoin\Transaction;
use App\Models\MasterData\CashierTransactionStatus;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\ReturSaleOrder;
use App\Utils\Merchant\MerchantUtil;
use Modules\Merchant\Entities\Toko\ApprovalSaleOrderEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\PurchaseOrderDeliveryEntity;
use Modules\Merchant\Entities\Toko\PurchaseOrderEntity;
use Modules\Merchant\Entities\Toko\PurchaseOrderOfferEntity;
use Modules\Merchant\Entities\Toko\PurchaseOrderOrderEntity;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use App\Utils\ExpeditionUtil;
use App\Utils\PaymentUtils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\BranchEntity;
use Modules\Merchant\Entities\Toko\SaleOrderEntity;
use Modules\Merchant\Entities\Toko\SaleOrderOfferEntity;
use Modules\Merchant\Entities\Toko\SaleOrderOrderEntity;
use Modules\Merchant\Entities\Toko\ReturSaleOrderEntity;
use Modules\Merchant\Entities\Toko\SaleOrderDeliveryEntity;
use Modules\Merchant\Entities\Toko\StockFlowEntity;
use Ramsey\Uuid\Uuid;
use PDF;

class SaleOrderController extends Controller
{


    public function routeWeb()
    {
        Route::prefix('report/sale-order')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\Report\SaleOrderController@data')
                    ->name('merchant.toko.sale-order.data');
                Route::get('/data-order', 'Toko\Report\SaleOrderController@dataOrder')
                    ->name('merchant.toko.sale-order.data-order');
                Route::get('/data-offer', 'Toko\Report\SaleOrderController@dataOffer')
                    ->name('merchant.toko.sale-order.data-offer');
                Route::get('/data-delivery', 'Toko\Report\SaleOrderController@dataDelivery')
                    ->name('merchant.toko.sale-order.data-delivery');

                Route::get('/detail/{id}', 'Toko\Report\SaleOrderController@detail')
                    ->name('merchant.toko.sale-order.detail');
                Route::get('/detail-ppob/{id}', 'Toko\Report\SaleOrderController@detailPPOB')
                    ->name('merchant.toko.sale-order.detail-ppob');
                Route::get('/detail-order/{id}', 'Toko\Report\SaleOrderController@detailOrder')
                    ->name('merchant.toko.sale-order.detail-order');
                Route::get('/detail-offer/{id}', 'Toko\Report\SaleOrderController@detailOffer')
                    ->name('merchant.toko.sale-order.detail-offer');
                Route::get('/detail-delivery/{id}', 'Toko\Report\SaleOrderController@detailDelivery')
                    ->name('merchant.toko.sale-order.detail-delivery');

                Route::post('/data-table', 'Toko\Report\SaleOrderController@dataTable')
                    ->name('merchant.toko.sale-order.datatable');
                Route::post('/data-table-order', 'Toko\Report\SaleOrderController@dataTableOrder')
                    ->name('merchant.toko.sale-order.datatable-order');
                Route::post('/data-table-offer', 'Toko\Report\SaleOrderController@dataTableOffer')
                    ->name('merchant.toko.sale-order.datatable-offer');
                Route::post('/data-table-delivery', 'Toko\Report\SaleOrderController@dataTableDelivery')
                    ->name('merchant.toko.sale-order.datatable-delivery');

                Route::post('/reload-data', 'Toko\Report\SaleOrderController@reloadData')
                    ->name('merchant.toko.sale-order.reload-data');
                Route::post('/reload-data-offer', 'Toko\Report\SaleOrderController@reloadDataOffer')
                    ->name('merchant.toko.sale-order.reload-data-offer');
                Route::post('/reload-data-order', 'Toko\Report\SaleOrderController@reloadDataOrder')
                    ->name('merchant.toko.sale-order.reload-data-order');
                Route::post('/reload-data-delivery', 'Toko\Report\SaleOrderController@reloadDataDelivery')
                    ->name('merchant.toko.sale-order.reload-data-delivery');

                Route::post('/export-data', 'Toko\Report\SaleOrderController@exportData')
                    ->name('merchant.toko.sale-order.export-data');

                Route::get('/cetakPDF/{id}', 'Toko\Report\SaleOrderController@cetakPDF')
                    ->name('merchant.toko.sale-order.cetakPDF');
                Route::get('/cetakExcel/{id}', 'Toko\Report\SaleOrderController@cetakExcel')
                    ->name('merchant.toko.sale-order.cetakExcel');
                Route::get('/cetakFakturPDF/{id}', 'Toko\Report\SaleOrderController@cetakFakturPDF')
                    ->name('merchant.toko.sale-order.export-data-faktur');
                Route::get('/cetakLaporanPembayaranPDF/{id}', 'Toko\Report\SaleOrderController@cetakLaporanPembayaranPDF')
                    ->name('merchant.toko.sale-order.export-payment-report');
                Route::get('/cetakTagihanPDF/{id}/{arId}', 'Toko\Report\SaleOrderController@cetakTagihanPDF')
                    ->name('merchant.toko.sale-order.export-data-bill');
                Route::get('/cetakReturPDF/{id}/{returId}', 'Toko\Report\SaleOrderController@cetakReturPDF')
                    ->name('merchant.toko.sale-order.export-data-retur');

                Route::post('/cetakFakturExcel', 'Toko\Report\SaleOrderController@cetakFakturExcel')
                    ->name('merchant.toko.sale-order.export-faktur-excel');
            });
    }

    public function cetakFakturPDF($id, Request $request){
        try{
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')->where('id',$id)->first();

            if($data->step_type==1)
            {
                $pdf = PDF::loadview('merchant::toko.report.sale-order.sale-offer-pdf',["data" => $data])->setPaper('a4', 'potrait');
                return $pdf->download('laporan-faktur-penawaran-'.$data->code.'.pdf');

                //return view('merchant::toko.report.sale-order.sale-offer-pdf',["data" => $data]);



            }elseif ($data->step_type==2)
            {
                $pdf = PDF::loadview('merchant::toko.report.sale-order.sale-order-pdf',["data" => $data])->setPaper('a4', 'potrait');
                return $pdf->download('laporan-faktur-pemesanan-'.$data->code.'.pdf');

            }elseif ($data->step_type==3)
            {
                $pdf = PDF::loadview('merchant::toko.report.sale-order.sale-delivery-pdf',["data" => $data])->setPaper('a4', 'potrait');
                return $pdf->download('laporan-faktur-delivery-'.$data->code.'.pdf');

            }else{
                $pdf = PDF::loadview('merchant::toko.report.sale-order.sale-invoice-pdf',["data" => $data])->setPaper('a4', 'potrait');
                return $pdf->download('laporan-faktur-penjualan-'.$data->code.'.pdf');

            }

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return redirect()->back()->with(['error-export'=> "Terjadi Kesalahan di Server, Data Gagal di Export"]);
        }

    }



    public function cetakLaporanPembayaranPDF($id, Request $request){
        try {
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
            ->with('getTransactionStatus')
            ->with('getTransactionType')
            ->with('getStaff')
            ->with('ar')
            ->with('getMerchant')->where('id',$id)->first();

            $pdf = PDF::loadview('merchant::toko.report.sale-order.payment-report-pdf',["data"=>$data])->setPaper('a4', 'potrait');
            return $pdf->download('laporan-pembayaran-penjualan-'.$data->code.'.pdf');

        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return redirect()->back()->with(['error-export'=> 'Terjadi Kesalahan di Server, Data Gagal di Export']);
        }

    }

    public function cetakTagihanPDF($id,$arId, Request $request){
        try {
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
            ->with('getTransactionStatus')
            ->with('getTransactionType')
            ->with('getStaff')
            ->with('ar')
            ->with('getMerchant')->where('id',$id)->first();

            $payment = $data->ar->getDetail->where('id', $arId)->first();

            $pdf = PDF::loadview('merchant::toko.report.sale-order.bill-pdf',["data"=>$data, "payment" => $payment])->setPaper('a4', 'landscape');
            return $pdf->download('laporan-tagihan-penjualan-'.$data->code.'.pdf');

        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return redirect()->back()->with(['error-export'=> 'Terjadi Kesalahan di Server, Data Gagal di Export']);
        }

    }

    public function cetakReturPDF($id, $returId, Request $request){
        try {
            $data = SaleOrder::with('getDetail.getProduct.getUnit')
            ->with('getTransactionStatus')
            ->with('getTransactionType')
            ->with('getStaff')
            ->with('ar')
            ->with('getMerchant')->where('id', $id)->first();

            $retur = ReturSaleOrder::find($returId);

            $pdf = PDF::loadview('merchant::toko.report.sale-order.retur-pdf',["data"=>$data, "retur" => $retur])->setPaper('a4', 'potrait');
            return $pdf->download('laporan-retur-penjualan-'.$data->code.'.pdf');


        } catch(\Exception $e){
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return redirect()->back()->with(['error-export' => 'Terjadi Kesalahan di Server, Data Gagal di Export']);
        }
    }

    public function cetakExcel($id){
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);

            $data=SaleOrder::with('getDetail.getProduct.getUnit')
            ->with('getTransactionStatus')
            ->with('getTransactionType')
            ->with('getStaff')
            ->with('ar')
            ->with('getMerchant')->where('id',$id)->first();

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Invoice $data->code");
            $date=date("d-m-Y", strtotime($data->created_at));
            $objPHPExcel->getActiveSheet()->setCellValue('A2',"Tanggal: $date");
            $stats=($data->is_debet==0)?'Lunas':'Kredit';
            $objPHPExcel->getActiveSheet()->setCellValue('A4',"Status: $stats");
            $cust=is_null($data->getCustomer)?'-':$data->getCustomer->name;
            $objPHPExcel->getActiveSheet()->setCellValue('A3',"Customer: $cust");
            $diskon=rupiah($data->promo);
            $objPHPExcel->getActiveSheet()->setCellValue('E2',"Promo/Diskon: $diskon");
            $ship=rupiah($data->shipping_cost);
            $objPHPExcel->getActiveSheet()->setCellValue('E3',"Biaya Pengiriman: $ship");
            $total=rupiah($data->total+$data->shipping_cost);
            $objPHPExcel->getActiveSheet()->setCellValue('E4',"Total: $total");

            if($data->is_debet==1){
                $objPHPExcel->getActiveSheet()->setCellValue('A5',"Jatuh Tempo: ".date("d-m-Y", strtotime($data->ar->due_date)));
                $objPHPExcel->getActiveSheet()->setCellValue('E5',"Terbayarkan: ".rupiah($data->ar->paid_nominal));
                $objPHPExcel->getActiveSheet()->setCellValue('E6',"Sisa Piutang: ".rupiah($data->ar->residual_amount));
            }

            $header = [
                'No',
                'Nama Produk',
                'Qty',
                'Harga',
                'Satuan',
                'Sub Total'
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '9', strtoupper($item));
                $col++;
            }

            $num = 9;

            foreach ($data->getDetail->where('is_deleted',0) as $key => $item) {
                $dataShow = [
                    $key+1,
                    $item->getProduct->name,
                    $item->quantity,
                    number_format($item->price,3),
                    $item->getProduct->getUnit->name,
                    number_format($item->sub_total,3)
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $col++;
                }
                $num++;
            }

            $destinationPath = 'public/uploads/merchant/'.$data->code.'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-invoice_' . $data->code . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL';setTimeout('window.close()', 1000);;</script>";
        }catch (\Exception $e)
        {

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }


    public function data()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Faktur Penjualan',
            'merchantId'=>$merchantId,
            'tableColumns'=>SaleOrderEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'shippingMethod'=>ExpeditionUtil::getList(merchant_id()),
            'transactionStatus'=>CashierTransactionStatus::all(),
            'shippingCategory'=>ShippingCategory::whereNull('parent_id')->get(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchParams'=>base64_encode(json_encode([])),
            'countApproval'=>ApprovalSaleOrderEntity::countData()
        ];
        return view('merchant::toko.report.sale-order.data',$params);
    }


    public function dataOrder()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Pemesanan Penjualan',
            'merchantId'=>$merchantId,
            'tableColumns'=>SaleOrderOrderEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchParams'=>base64_encode(json_encode([])),
            'countApproval'=>ApprovalSaleOrderEntity::countData()

        ];
        return view('merchant::toko.report.sale-order.data-order',$params);
    }

    public function dataOffer()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Penawaran Penjualan',
            'merchantId'=>$merchantId,
            'tableColumns'=>SaleOrderOfferEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchParams'=>base64_encode(json_encode([])),
            'countApproval'=>ApprovalSaleOrderEntity::countData()

        ];
        return view('merchant::toko.report.sale-order.data-offer',$params);
    }
    public function dataDelivery()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Pengiriman Penjualan',
            'merchantId'=>$merchantId,
            'tableColumns'=>SaleOrderDeliveryEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchParams'=>base64_encode(json_encode([])),
            'countApproval'=>ApprovalSaleOrderEntity::countData()

        ];
        return view('merchant::toko.report.sale-order.data-delivery',$params);
    }
    public function dataTable(Request $request)
    {
        return SaleOrderEntity::dataTable($request);
    }

    public function dataTableOrder(Request $request)
    {
        return SaleOrderOrderEntity::dataTable($request);
    }

    public function dataTableDelivery(Request $request)
    {
        return SaleOrderDeliveryEntity::dataTable($request);
    }

    public function dataTableOffer(Request $request)
    {
        return SaleOrderOfferEntity::dataTable($request);
    }


    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (SaleOrderEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }

        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);

        $request['count_data'] = 1;

        $params = [
            'title' => 'Faktur Penjualan',
            'searchKey' => $searchKey,
            'tableColumns'=>SaleOrderEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->md_merchant_id,
            'searchParams'=>base64_encode(json_encode($request->session()->get($searchKey)))
        ];

        return view('merchant::toko.report.sale-order.list',$params);

    }

    public function reloadDataOffer(Request $request)
    {

        $searchParams = [];
        foreach (SaleOrderOfferEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Penawaran Penjualan',
            'searchKey' => $searchKey,
            'tableColumns'=>SaleOrderOfferEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->md_merchant_id,
            'searchParams'=>base64_encode(json_encode($request->session()->get($searchKey)))


        ];
        return view('merchant::toko.report.sale-order.list-offer',$params);

    }

    public function reloadDataOrder(Request $request)
    {

        $searchParams = [];
        foreach (SaleOrderOrderEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Pemesanan Penjualan',
            'searchKey' => $searchKey,
            'tableColumns'=>SaleOrderOrderEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->md_merchant_id,
            'searchParams'=>base64_encode(json_encode($request->session()->get($searchKey)))


        ];
        return view('merchant::toko.report.sale-order.list-order',$params);

    }

    public function reloadDataDelivery(Request $request)
    {

        $searchParams = [];
        foreach (SaleOrderDeliveryEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Pengiriman Penjualan',
            'searchKey' => $searchKey,
            'tableColumns'=>SaleOrderDeliveryEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->md_merchant_id,
            'searchParams'=>base64_encode(json_encode($request->session()->get($searchKey)))


        ];
        return view('merchant::toko.report.sale-order.list-delivery',$params);

    }

    public function exportData(Request $request,$fromGraphic=0,$periodFilter='',$time='')
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            if($request->step_type==0){
                $po=SaleOrderEntity::class;
                $data=SaleOrderEntity::export();
                $title='Faktur Penjualan';
            }else if($request->step_type==1){
                $po=SaleOrderOfferEntity::class;
                $data=SaleOrderOfferEntity::export();
                $title='Penawaran Penjualan';

            }else if($request->step_type==2){
                $po=SaleOrderOrderEntity::class;
                $data = SaleOrderOrderEntity::export();
                $title='Pemesanan Penjualan';

            }else{
                $po=SaleOrderDeliveryEntity::class;
                $data = SaleOrderDeliveryEntity::export();
                $title='Pengiriman Penjualan';

            }

            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

            $searchKey = (array)json_decode(base64_decode($request->input('searchParams')));
            $searchFilter = is_array($searchKey) ? $searchKey : [];

            if(empty($searchFilter)){
                $data->where('sc_sale_orders.is_deleted',0);
            }

            $getBranch=MerchantUtil::getBranch(merchant_id());

            if($request->md_merchant_id=='-1'){

                $data->whereIn('sc_sale_orders.md_merchant_id',$getBranch);

            }else{
                foreach ($po::getFilterMap() as $key => $field) {
                    if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                        $data->where([$field => $searchFilter[$key]]);
                    }
                }

            }
            $data->whereRaw("
                    sc_sale_orders.created_at::date between '$startDate' and '$endDate'
                    ");


            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan $title Tanggal $startDate sd $endDate");
            $header = [
                'order no',
                'order date',
                'order time',
                'order source',
                'serve by',
                'employee list',
                'branch name',
                'branch email',
                'customer type',
                'customer id',
                'customer name',
                'customer code',
                'customer email',
                'customer phone',
                'customer address',
                'brand',
                'item group',
                'item name',
                'item sku',
                'item uom',
                'serial no',
                'qty',
                'unit',
                'currency',
                'basic price',
                'price',
                'add on price',
                'discount percent',
                'discount amount',
                'service charge',
                'tax percent',
                'tax amount',
                'sub total',
                'cost per unit',
                'total cost',
                'profit',
                'commission',
                'shipping to',
                'shipping address',
                'notes',
                'paid',
                'payment type',
                'bank',
                'bank account name',
                'bank account no',
                'shipping courier',
                'shipping service type',
                'shipping tracking no',
                'shipping date',
                'shipping cost',
                'order status',
                'posting time',
                'admin fee digital payment',
                'cashier name',
                'timezone',
                'is debt',
                'is bonus product',
            ];
            $col = 'A';
            foreach ($header as $v) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '2', strtoupper($v));
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->getFont()->setBold( true );
                $col++;
            }
            $num = 2;
            $no=0;
            foreach ($data->get() as $key =>$item)
            {
                if($request->step_type==0)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:$item->code;
                    $payment=($item->trans_name=='Kas' || $item->trans_name=='Kas Kasir Outlet')?'Tunai':'Non Tunai';

                }elseif ($request->step_type==1)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PQ',$item->code);
                    $payment='-';

                }elseif ($request->step_type==2)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PO',$item->code);
                    $payment='-';

                }else{
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PD',$item->code);
                    $payment='-';
                }
                $assign="";
                if(!empty($item->assign_to_user_helper)){
                    foreach (json_decode($item->assign_to_user_helper) as $askey =>$as)
                    {
                        $assign.=($askey==0)?$as->fullname:",".$as->fullname;
                    }
                }
                $commissionValue=0;
                if(!is_null($item->commission_value)){
                    $jsonCommission=json_decode($item->commission_value);
                    foreach ($jsonCommission as $c){
                        $commissionValue+=$c->commission_amount;
                    }
                }

                $dataShow = [
                    $kode,
                    $item->order_date,
                    $item->order_time,
                    $item->order_source,
                    ($assign!="")?$assign:$item->serve_by,
                    $assign,
                    $item->branch_name,
                    $item->branch_email,
                    $item->customer_type,
                    $item->customer_id,
                    $item->customer_name,
                    $item->customer_code,
                    $item->customer_email,
                    $item->customer_phone,
                    $item->customer_address,
                    $item->brand,
                    $item->item_group,
                    $item->item_name,
                    $item->item_sku,
                    $item->item_uom,
                    $item->serial_no,
                    $item->qty,
                    $item->unit,
                    $item->currency,
                    $item->basic_price,
                    $item->price,
                    $item->add_on_price,
                    $item->discount_percent,
                    $item->discount_amount,
                    $item->tax_percent,
                    $item->service_charge,
                    $item->tax_amount,
                    $item->sub_total,
                    $item->cost_per_unit,
                    $item->total_cost,
                    $item->profit,
                    $commissionValue,
                    '',
                    '',
                    $item->notes,
                    ($item->paid==2)?1:0,
                    $payment,
                    '',
                    '',
                    '',
                    $item->shipping_courier,
                    '',
                    '',
                    '',
                    $item->shipping_cost,
                    ($item->order_status==2)?1:0,
                    $item->posting_time,
                    $item->admin_fee_digital_payment,
                    $item->cashier_name,
                    $item->timezone,
                    $item->is_debt,
                    $item->is_bonus_product
                ];
                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
                $no++;
            }
            foreach(range('A','Z') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            if($request->step_type==1){
                $filename = strtolower('export-penawaran-penjualan_'.merchant_id()."_".$startDate."_".$endDate.'.xlsx');
            }else if($request->step_type==2){
                $filename = strtolower('export-pemesanan-penjualan_'.merchant_id()."_".$startDate."_".$endDate.'.xlsx');
            }else if($request->step_type==3){
                $filename = strtolower('export-penerimaan-penjualan_'.merchant_id()."_".$startDate."_".$endDate.'.xlsx');
            }else{
                $filename = strtolower('export-penjualan_' . merchant_id()."_".$startDate."_".$endDate.'.xlsx');
            }
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
                    <div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <div id='successExport'></div>
                    <script>
                        let success = document.querySelector('#successExport');
                        if(success){
                            toastForSaveData('Data berhasil diexport!','success',true,'',false);
                        }
                    </script>";

        }catch (\Exception $e)
        {
            dd($e);
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";


        }

    }

    public function detail($id,Request  $request)
    {
        try{
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')->where('id',$id)->first();
            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'promo_product'=>0,
                'page'=>(is_null($request->page))?'transaksi':$request->page

            ];
            return view('merchant::toko.report.sale-order.detail',$params);
        }catch (\Exception $e)
        {

        }
    }

    public function detailPPOB($id,Request  $request)
    {
        try{
            $data=DB::table("tp_paypoin_transactions")->find($id);

            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'promo_product'=>0,
                'page'=>'transaksi'

            ];
            return view('merchant::toko.report.sale-order.detail-ppob',$params);
        }catch (\Exception $e)
        {

        }
    }

    public function detailOrder($id,Request  $request)
    {
        try{
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')->where('id',$id)->first();

            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'page'=>(is_null($request->page))?'transaksi':$request->page

            ];
            return view('merchant::toko.report.sale-order.detail-order',$params);
        }catch (\Exception $e)
        {

        }
    }

    public function detailOffer($id,Request  $request)
    {
        try{
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')->where('id',$id)->first();

            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'page'=>(is_null($request->page))?'transaksi':$request->page

            ];
            return view('merchant::toko.report.sale-order.detail-offer',$params);
        }catch (\Exception $e)
        {

        }
    }

    public function detailDelivery($id,Request  $request)
    {
        try{
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')->where('id',$id)->first();

            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'page'=>(is_null($request->page))?'transaksi':$request->page

            ];
            return view('merchant::toko.report.sale-order.detail-delivery',$params);
        }catch (\Exception $e)
        {

        }
    }

    // export faktur excel
    public function cetakFakturExcel(Request $request)
    {
        try{
            $id = $request->id;
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            $styleTextCenter = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                ]
            ];

            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getShippingCategory')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')->where('id',$id)->first();

                // $objDrawing = new Drawing();
                // $img=env('S3_URL').$data->getMerchant->image;
                // $objDrawing->setPath($img);
                // $objDrawing->setHeight(21);

            if($data->step_type==0)
            {
                $title='Faktur Penjualan';
                $kode=(!is_null($data->second_code))?$data->second_code:$data->code;
                $tax=$data->tax;
                $promo=$data->promo;
                $total=$data->total;

            }elseif ($data->step_type==1)
            {
                $title='Penawaran Penjualan';
                $kode=(!is_null($data->second_code))?$data->second_code:str_replace('S','SQ',$data->code);
                $tax=$data->tax_offer;
                $promo=$data->promo_offer;
                $total=$data->total_offer;

            }elseif ($data->step_type==2)
            {
                $title='Pemesanan Penjualan';
                $kode=(!is_null($data->second_code))?$data->second_code:str_replace('S','SO',$data->code);
                $tax=$data->tax_order;
                $promo=$data->promo_order;
                $total=$data->total_order;

            }else{
                $title= 'Pengiriman Penjualan';
                $kode=(!is_null($data->second_code))?$data->second_code:str_replace('S','SD',$data->code);
                $tax=$data->tax_delivery;
                $promo=$data->promo_delivery;
                $total=$data->total_delivery;

            }

            $objPHPExcel->getActiveSheet()->setCellValue('B3',$data->getMerchant->name)->getStyle('B3')->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->mergeCells('B3:C3')->getStyle('B3')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->setCellValue('B4',"No.telp:");
            $objPHPExcel->getActiveSheet()->setCellValue('C4',$data->getMerchant->phone_merchant);

            $objPHPExcel->getActiveSheet()->setCellValue('B5',"Alamat:");
            $objPHPExcel->getActiveSheet()->setCellValue('C5',$data->getMerchant->address);

            $objPHPExcel->getActiveSheet()->setCellValue('E3',$title)->getStyle('E3')->getFont()->setSize(25);
            $objPHPExcel->getActiveSheet()->getStyle("E3")->applyFromArray($styleTextCenter)->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->mergeCells('E3:H5');

            $objPHPExcel->getActiveSheet()->setCellValue('B7',"No.Faktur:");
            $objPHPExcel->getActiveSheet()->setCellValue('C7',$kode);
            $objPHPExcel->getActiveSheet()->setCellValue('B8',"Pelanggan:");
            $objPHPExcel->getActiveSheet()->setCellValue('C8',is_null($data->getCustomer)?'Pelanggan Umum':$data->getCustomer->name);
            $objPHPExcel->getActiveSheet()->setCellValue('B9',"Pengiriman:");
            $objPHPExcel->getActiveSheet()->setCellValue('C9',is_null($data->getShippingCategory)?'':$data->getShippingCategory->name);

            if($data->is_debet==1){
                $objPHPExcel->getActiveSheet()->setCellValue('G7',"Hutang:");
                $objPHPExcel->getActiveSheet()->setCellValue('H7','Ya');
            }else{
                $objPHPExcel->getActiveSheet()->setCellValue('G7',"Pembayaran:");
                $objPHPExcel->getActiveSheet()->setCellValue('H7',is_null($data->getTransactionType)?'':$data->getTransactionType->name);
            }

            $objPHPExcel->getActiveSheet()->setCellValue('G8',"Waktu Transaksi:");
            $objPHPExcel->getActiveSheet()->setCellValue('H8',Carbon::parse($data->created_at)->isoFormat('dddd, D MMMM Y, HH:mm'));

            $header = [
                'No',
                'Kode',
                'Barang',
                'Jumlah',
                'Satuan',
                'Harga Satuan',
                'Sub Total'
            ];
            $col = 'B';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '11', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '11')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '11')->getFont()->setBold( true );
                $col++;
            }

            $firstRow = "B2:".$objPHPExcel->getActiveSheet()->getHighestColumn()."2";
            $objPHPExcel->getActiveSheet()->mergeCells($firstRow);

            $num = 11;

            $promo_product = 0;
            foreach ($data->getDetail->where('is_deleted', 0) as $key => $item) {
                if($item->is_multi_unit==1)
                {
                    if(is_null($item->unit_name)){
                        $q=$item->multi_quantity;
                        $u=$item->getProduct->getUnit->name;
                    }else{
                        $q=$item->quantity;
                        $u=$item->unit_name;
                    }
                }else{
                    $q=$item->quantity;
                    $u=!is_null($item->unit_name)?$item->unit_name:$item->getProduct->getUnit->name;
                }
                $dataShow = [
                    $key + 1,
                    $item->getProduct->code,
                    $item->getProduct->name,
                    $q,
                    $u,
                    ($item->is_bonus==0)? rupiah($item->price):"Bonus Produk",
                    ($item->is_bonus==0)? rupiah($item->sub_total):0
                ];

                if($item->multi_quantity!=0){
                    if($item->value_conversation==0){
                        if($item->price*$item->quantity!=$item->sub_total){
                            $promo_product+=$item->price*$item->quantity-$item->sub_total;
                        }
                    }else{
                        if($item->price*($item->quantity)!=$item->sub_total){
                            $promo_product+=$item->price*$item->quantity-$item->sub_total;
                        }
                    }

                } else {
                    if($item->price*$item->quantity!=$item->sub_total){
                        $promo_product+=$item->price*$item->quantity-$item->sub_total;
                    }
                }

                $col = 'B';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                    $col++;
                }
                $num++;
            }

            $lastrow = $objPHPExcel->getActiveSheet()->getHighestRow() + 3;
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$lastrow, 'Nama Kasir');
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$lastrow, $data->getStaff->fullname);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($lastrow + 1), 'Status Transaksi');
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($lastrow + 1), ($data->is_deleted==1)?'Terhapus':'-');
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($lastrow + 2), 'Catatan');
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($lastrow + 2), is_null($data->note_order)?'-':$data->note_order);

            $objPHPExcel->getActiveSheet()->setCellValue('G'.$lastrow, 'Potongan Harga');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 1), 'Pajak');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 2), 'Biaya Pengiriman');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 3), 'Biaya Admin (Payment Digital)');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 4), 'Total Harga');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 5), 'Dibayar');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($lastrow + 6), 'Sisa Tagihan');

            $objPHPExcel->getActiveSheet()->setCellValue('H'.$lastrow, rupiah((is_null($promo))?0+$promo_product:$promo+$promo_product));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 1), rupiah((is_null($tax))?0:$tax));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 2), rupiah($data->shipping_cost));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 3), rupiah($data->admin_fee));

            if($data->md_transaction_type_id == 4){
                $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 4), rupiah($total+$data->admin_fee));
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 4), rupiah($total+$data->admin_fee));
            }

            if($data->is_debet == 0){
                if($data->step_type==0)
                {
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 5), rupiah($total));
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 6), rupiah(0));

                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 5), rupiah(0));
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 6), rupiah(0));
                }
            }else{
                $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 5), rupiah($data->ar->paid_nominal));
                $objPHPExcel->getActiveSheet()->setCellValue('H'.($lastrow + 6), rupiah($data->ar->residual_amount));
            }

            $lastColRow = $objPHPExcel->getActiveSheet()->getHighestColumn().$objPHPExcel->getActiveSheet()->getHighestRow();
            $objPHPExcel->getActiveSheet()->getStyle('B2:'.$lastColRow)->applyFromArray($styleArray);

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            foreach(range('B','H') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $filename = strtolower('laporan-faktur-penjualan-'.$data->code.'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
              <div id='successExport' class='text-center'>
                  <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
              </div>
              <script>
                if(document.querySelector('#successExport')){
                  toastForSaveData('Data berhasil diexport!','success',false,'',false);
                }
              </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }

}

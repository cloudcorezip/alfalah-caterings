<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Report;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use App\Models\MasterData\TransactionType;
use App\Models\SennaToko\Customer;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\ArEntity;
use Modules\Merchant\Entities\Toko\SaleOrderEntity;
use Ramsey\Uuid\Uuid;

class ArController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('report/ar')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\Report\ArController@data')
                    ->name('merchant.toko.ar.data');
                Route::post('/data-table', 'Toko\Report\ArController@dataTable')
                    ->name('merchant.toko.ar.datatable');
                Route::post('/reload-data', 'Toko\Report\ArController@reloadData')
                    ->name('merchant.toko.ar.reload-data');
                Route::post('/export-data', 'Toko\Report\ArController@exportData')
                    ->name('merchant.toko.ar.export-data');
            });
    }


    public function data()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Piutang',
            'tableColumns'=>ArEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'merchantId'=>$merchantId,
            'searchParams'=>base64_encode(json_encode([]))
        ];
        return view('merchant::toko.report.ar.data',$params);
    }

    public function dataTable(Request $request)
    {
        return ArEntity::dataTable($request);
    }


    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ArEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Piutang',
            'searchKey' => $searchKey,
            'tableColumns'=>ArEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->md_merchant_id,
            'searchParams'=>base64_encode(json_encode($request->session()->get($searchKey)))


        ];
        return view('merchant::toko.report.ar.list',$params);

    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $data = ArEntity::getDataForDataTable();

            $searchKey = (array)json_decode(base64_decode($request->input('searchParams')));
            $searchFilter = is_array($searchKey) ? $searchKey : [];

            if(empty($searchFilter)){
                $data->where('sc_sale_orders.is_deleted',0);
            }


            foreach (ArEntity::getFilterMap() as $key => $field) {
                if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                    $data->where([$field => $searchFilter[$key]]);
                }
            }
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

            if($request->md_merchant_id=='-1'){
                $getBranch=MerchantUtil::getBranch(merchant_id(),0);

                $data->whereIn('sc_sale_orders.md_merchant_id',$getBranch);
            }else{
                $data->where('sc_sale_orders.md_merchant_id',$request->md_merchant_id);
            }

            $data->whereRaw("
        sc_sale_orders.created_at::date between '$startDate' and '$endDate'
        ");

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Piutang Tanggal $startDate sd $endDate ");
            $header = [
                'No',
                'Kode Transaksi',
                'Nama Pelanggan',
                'Total',
                'Terbayarkan',
                'Sisa Piutang',
                'Jatuh Tempo',
                'Nama Pencatat',
                'Waktu Transaksi'
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;

            foreach ($data->get() as $key => $item) {
                $dataShow = [
                    $key+1,
                    is_null($item->second_code)?$item->code:$item->second_code,
                    $item->nama_pelanggan,
                    rupiah($item->total),
                    rupiah($item->terbayarkan),
                    rupiah($item->sisa_piutang),
                    $item->jatuh_tempo,
                    $item->nama_pencatat,
                    $item->waktu_transaksi
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-piutang_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
                    <div id='successExport'></div>
                    <script>
                        let success = document.querySelector('#successExport');
                        if(success){
                            toastForSaveData('Data piutang penjualan berhasil diexport!','success',true,'',false);
                        }
                    </script>
            ";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<script>toastForSaveData('Terjadi kesalahan, Data piutang penjualan gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)

</script>";

        }

    }

}

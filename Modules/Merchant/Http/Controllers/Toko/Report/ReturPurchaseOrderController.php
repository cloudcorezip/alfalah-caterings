<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Report;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use App\Models\MasterData\TransactionType;
use App\Models\SennaToko\Supplier;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\PurchaseOrderEntity;
use Modules\Merchant\Entities\Toko\ReturPurchaseOrderEntity;
use Modules\Merchant\Entities\Toko\ReturSaleOrderEntity;
use App\Models\Accounting\ArrayOfAccounting;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Ramsey\Uuid\Uuid;

class ReturPurchaseOrderController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('report/retur-purchase-order')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/add', 'Toko\Report\ReturPurchaseOrderController@add')
                    ->name('merchant.toko.retur-purchase-order.add');
                Route::get('/detail/{id}', 'Toko\Report\ReturPurchaseOrderController@detail')
                    ->name('merchant.toko.retur-purchase-order.detail');
                Route::get('/data', 'Toko\Report\ReturPurchaseOrderController@data')
                    ->name('merchant.toko.retur-purchase-order.data');
                Route::post('/data-table', 'Toko\Report\ReturPurchaseOrderController@dataTable')
                    ->name('merchant.toko.retur-purchase-order.datatable');
                Route::post('/reload-data', 'Toko\Report\ReturPurchaseOrderController@reloadData')
                    ->name('merchant.toko.retur-purchase-order.reload-data');
                Route::post('/export-data', 'Toko\Report\ReturPurchaseOrderController@exportData')
                    ->name('merchant.toko.retur-purchase-order.export-data');
            });
    }

    public function detail($id,Request  $request){


        $params=[
            'title'=>'Detail',
            'data'=>PurchaseOrderEntity::find($id)
        ];

        return view('merchant::toko.report.retur-purchase-order.detail',$params);
    }

    public function add(Request  $request){


        $params=[
            'title'=>'Tambah Data',
            'supplier'=>SupplierEntity::listOption()
        ];

        return view('merchant::toko.report.retur-purchase-order.add',$params);
    }

    public function data()
    {
        $params=[
            'title'=>'Retur',
            'tableColumns'=>ReturPurchaseOrderEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'supplier'=>SupplierEntity::listOption(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),

        ];
        return view('merchant::toko.report.retur-purchase-order.data',$params);
    }

    public function dataTable(Request $request)
    {
        return ReturPurchaseOrderEntity::dataTable($request);
    }


    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ReturPurchaseOrderEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Retur',
            'searchKey' => $searchKey,
            'tableColumns'=>ReturPurchaseOrderEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.report.retur-purchase-order.list',$params);

    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            $data = ReturPurchaseOrderEntity::getDataForDataTable();
            $searchKey = $request->input('search_key');
            $searchFilter = $request->session()->get($searchKey, []);
            $searchFilter = is_array($searchFilter) ? $searchFilter : [];

            $getBranch=MerchantUtil::getBranch(merchant_id());

            if($request->md_merchant_id=='-1'){

                $data->whereIn('p.md_merchant_id',$getBranch);

            }else{
                foreach (ReturPurchaseOrderEntity::getFilterMap() as $key => $field) {
                    if (isset($searchFilter[$key]) && $searchFilter[$key] != '-') {
                        $data->where([$field => $searchFilter[$key]]);
                    }
                }

            }

            $data->whereRaw("
        sc_retur_purchase_orders.created_at::date between '$startDate' and '$endDate'
        ");

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Retur Pembelian Tanggal $startDate sd $endDate");

            $header = [
                'No',
                'Kode Retur',
                'Asal Faktur',
                'Cabang',
                'Gudang',
                'Supplier',
                'Total Retur',
                'Tanggal Retur',
                'Zona Waktu',
                'Alasan Retur',
                'Produk',
                'Jumlah',
                'Satuan',
                'Sub Total'
            ];
            $col = 'A';
            foreach ($header as $v) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '2', strtoupper($v));
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->getFont()->setBold( true );
                $col++;
            }
            $num = 2;
            $no=1;
            foreach ($data->get() as $key =>$item)
            {
                if($item->step_type==3){
                    $asal=is_null($item->second_code_)?str_replace('P','PD',$item->code_):$item->second_code_;
                }else{
                    $asal=is_null($item->second_code_)?$item->code_:$item->second_code_;
                }
                if ($item->reason_id!=3){
                    $reason=\App\Models\Accounting\ArrayOfAccounting::searchReason($item->reason_id);

                }else{
                    $reason="Retur Dengan Mengurangi Hutang";
                }

                foreach ($item->getDetail as $n =>$j) {
                    $unitName= (is_null($j->getProduct->getUnit))?'':$j->getProduct->getUnit->name;
                    $dataShow = [
                        $no,
                        (is_null($item->second_code))?$item->code:$item->second_code,
                        $asal,
                        $item->cabang,
                        $item->gudang,
                        $item->nama_supplier,
                        $item->total_retur,
                        $item->tanggal_retur,
                        $item->timezone,
                        $reason,
                        is_null($j->product_name)?$j->getProduct->name.' '.$j->getProduct->code : $j->product_name,
                        $j->quantity,
                        is_null($j->unit_name)?$unitName:$j->unit_name,
                        rupiah($j->sub_total)
                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                    $no++;
                }
            }

            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-retur-pembelian_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; closeModal(); </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";


        }

    }

}

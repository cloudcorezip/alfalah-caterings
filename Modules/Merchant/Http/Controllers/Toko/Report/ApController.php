<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Report;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use App\Models\SennaToko\Supplier;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\ApEntity;
use Modules\Merchant\Entities\Toko\ArEntity;
use Modules\Merchant\Entities\Toko\PurchaseOrderEntity;
use Modules\Merchant\Entities\Toko\ReturPurchaseOrderEntity;
use Modules\Merchant\Entities\Toko\ReturSaleOrderEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;

class ApController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('report/ap')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\Report\ApController@data')
                    ->name('merchant.toko.ap.data');
                Route::post('/data-table', 'Toko\Report\ApController@dataTable')
                    ->name('merchant.toko.ap.datatable');
                Route::post('/reload-data', 'Toko\Report\ApController@reloadData')
                    ->name('merchant.toko.ap.reload-data');
                Route::post('/export-data', 'Toko\Report\ApController@exportData')
                    ->name('merchant.toko.ap.export-data');
            });
    }

    public function data()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Utang',
            'tableColumns'=>ApEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'supplier'=>SupplierEntity::listOption(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'merchantId'=>$merchantId

        ];
        return view('merchant::toko.report.ap.data',$params);
    }

    public function dataTable(Request $request)
    {
        return ApEntity::dataTable($request);
    }


    public function reloadData(Request $request)
    {
//        dd($request->all());
        $searchParams = [];
        foreach (ApEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Utang',
            'searchKey' => $searchKey,
            'tableColumns'=>ApEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->merchantId

        ];
        return view('merchant::toko.report.ap.list',$params);
    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            $data = ApEntity::getDataForDataTable();
            $searchKey = $request->input('search_key');
            $searchFilter = $request->session()->get($searchKey, []);
            $searchFilter = is_array($searchFilter) ? $searchFilter : [];

            foreach (ApEntity::getFilterMap() as $key => $field) {
                if (isset($searchFilter[$key]) && $searchFilter[$key] != '-') {
                    $data->where([$field => $searchFilter[$key]]);
                }
            }

            $getBranch=MerchantUtil::getBranch(merchant_id());

            if($request->md_merchant_id=='-1'){

                $data->whereIn('sc_purchase_orders.md_merchant_id',$getBranch);

            }else {
                $data->where('sc_purchase_orders.md_merchant_id',$request->md_merchant_id);
            }

            $data->whereRaw("
        sc_purchase_orders.created_at::date between '$startDate' and '$endDate'
        ");

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Hutang Tanggal $startDate sampai $endDate ");
            $header = [
                'No',
                'Kode Transaksi',
                'Nama Supplier',
                'Total Tagihan',
                'Terbayarkan',
                'Sisa Hutang',
                'Jatuh Tempo',
                'Nama Pencatat',
                'Waktu Transaksi'
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;

            foreach ($data->get() as $key => $item) {
                $dataShow = [
                    $key+1,
                    ($item->second_code)?$item->code:$item->second_code,
                    $item->nama_supplier,
                    rupiah($item->total_tagihan),
                    rupiah($item->terbayarkan),
                    rupiah($item->sisa_hutang),
                    $item->jatuh_tempo,
                    $item->nama_pencatat,
                    $item->waktu_transaksi
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-hutang_pembelian' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL';</script>
                    <div id='successExport'></div>
                    <script>
                        let success = document.querySelector('#successExport');
                        if(success){
                            toastForSaveData('Data utang pembelian berhasil diexport!','success',true,'',false);
                        }
                    </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<script>toastForSaveData('Terjadi kesalahan, Data utang pembelian gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)

</script>";


        }

    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Report;


use App\Classes\RangeCriteria;
use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\ShiftEntity;
use App\Models\MasterData\MerchantShiftCashflow;
use Ramsey\Uuid\Uuid;
use PDF;

class ShiftController extends Controller
{


    public function routeWeb()
    {
        Route::prefix('report/shift')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\Report\ShiftController@data')
                    ->name('merchant.toko.shift.data');
                Route::get('/detail/{id}', 'Toko\Report\ShiftController@detail')
                    ->name('merchant.toko.shift.detail');
                Route::post('/data-table', 'Toko\Report\ShiftController@dataTable')
                    ->name('merchant.toko.shift.datatable');
                Route::post('/reload-data', 'Toko\Report\ShiftController@reloadData')
                    ->name('merchant.toko.shift.reload-data');
                Route::get('/cetakExcel/{id}', 'Toko\Report\ShiftController@cetakExcel')
                    ->name('merchant.toko.shift.cetakExcel');
                Route::get('/cetakPDF/{id}', 'Toko\Report\ShiftController@cetakPDF')
                    ->name('merchant.toko.shift.cetakPDF');
            });
    }


    public function cetakPDF($id){
        $data=MerchantShiftCashflow::with('getCashType')
                ->with('getShift.getUser.getRole')
                ->with('getShift.getMerchant')
                ->where('md_merchant_shift_id',$id)
                ->where('is_deleted',0)
                ->get();

        $title='Detail Shift | ';
        $pdf = PDF::loadview('merchant::toko.report.shift.detail-pdf',['data'=>$data,'title'=>$title]);
    	return $pdf->download('Detail-Shift-'.$data[0]->id.'.pdf');
    }

    public function cetakExcel($id){
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);

            $data=MerchantShiftCashflow::with('getCashType')
                ->with('getShift.getUser.getRole')
                ->where('md_merchant_shift_id',$id)
                ->where('is_deleted',0)
                ->get();

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Detail Shift");
            $petugas=$data[0]->getShift->getUser->fullname;
            $objPHPExcel->getActiveSheet()->setCellValue('A2',"Petugas: ");
            $objPHPExcel->getActiveSheet()->setCellValue('B2',$petugas);
            $posisi=$data[0]->getShift->getUser->getRole->name;
            $objPHPExcel->getActiveSheet()->setCellValue('A4',"Posisi: ");
            $objPHPExcel->getActiveSheet()->setCellValue('B4',$posisi);
            $kas_awal=rupiah($data[0]->getShift->starting_cash);
            $objPHPExcel->getActiveSheet()->setCellValue('A3',"Saldo Awal: ");
            $objPHPExcel->getActiveSheet()->setCellValue('B3',$kas_awal);
            $start=date( 'd-m-Y H:i', strtotime($data[0]->getShift->start_shift));
            $objPHPExcel->getActiveSheet()->setCellValue('C2',"Mulai Shift: ");
            $objPHPExcel->getActiveSheet()->setCellValue('D2',$start);
            $end=date( 'd-m-Y H:i', strtotime($data[0]->getShift->end_shift));
            $objPHPExcel->getActiveSheet()->setCellValue('C3',"Akhir Shift: ");
            $objPHPExcel->getActiveSheet()->setCellValue('D3',$end);
            $objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->getStyle('C3')->getFont()->setBold( true );

            $header = [
                'No',
                'Tipe Transaksi',
                'Keterangan',
                'Jumlah',
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '6', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '6')->getFont()->setBold( true );
                $col++;
            }

            $num = 6;
            $penjualan=0;$pembelian=0;$pemasukan_lain=0;$pengeluaran_lain=0;
            foreach ($data as $key => $item) {
                $dataShow = [
                    $key+1,
                    $item->getCashType->name,
                    $item->note,
                    rupiah($item->amount)
                ];
                if ($item->md_sc_cash_type_id==1){
                    $penjualan+=$item->amount;
               }else if($item->md_sc_cash_type_id==2){
                    $pembelian+=$item->amount;
               }else if($item->md_sc_cash_type_id==3){
                    $pemasukan_lain+=$item->amount;
               }else if($item->md_sc_cash_type_id==4){
                    $pengeluaran_lain+=$item->amount;
               }
                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $col++;
                }
                $num++;
            }

            $lastRow= $row + 2;
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$lastRow,"Penjualan: ");
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$lastRow,rupiah($penjualan));
            $lastRow+= 1;
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$lastRow,"Pemasukan Lain: ");
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$lastRow,rupiah($pemasukan_lain));
            $lastRow+= 1;
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$lastRow,"Pembelian: ");
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$lastRow,rupiah($pembelian));
            $lastRow+= 1;
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$lastRow,"Pengeluaran Lain: ");
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$lastRow,rupiah($pengeluaran_lain));
            $lastRow+= 1;
            $penerimaan_aktual=$penjualan+$pemasukan_lain+$data[0]->getShift->starting_cash-$pembelian+$pengeluaran_lain;
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$lastRow,"Penerimaan Aktual: ");
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$lastRow,rupiah($penerimaan_aktual));
            $lastRow+= 1;
            $selisih=$penerimaan_aktual-($penjualan+$pemasukan_lain+$data[0]->starting_cash);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$lastRow,"Selisih: ");
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$lastRow,rupiah($selisih));
            foreach(range('A','G') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.$data[0]->getShift->id.'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-shift_' .$data[0]->getShift->id.'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>";
        }catch (\Exception $e)
        {

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function data()
    {
        $params=[
            'title'=>'Data Shift',
            'tableColumns'=>ShiftEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'staff'=>MerchantStaff::select([
                'u.id',
                'u.fullname',
                'u.email',
                'r.name as role_name'
            ])
                ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->where('md_merchant_id',merchant_id())
                ->where('md_merchant_staff.is_non_employee',0)
                ->where('md_merchant_staff.is_deleted',0)
                ->get(),
            'start_date'=>Carbon::parse(date('Y-m-d H:i:s'))->firstOfMonth()->toDateTimeString(),
            'end_date'=>Carbon::parse(date('Y-m-d H:i:s'))->endOfMonth()->toDateTimeString()
        ];
        return view('merchant::toko.report.shift.data',$params);
    }

    public function dataTable(Request $request)
    {
        return ShiftEntity::dataTable($request);
    }


    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ShiftEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Data Shift',
            'searchKey' => $searchKey,
            'tableColumns'=>ShiftEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.report.shift.list',$params);

    }

    public function detail($id)
    {
        try{
            $data=MerchantShiftCashflow::with('getCashType')
                ->with('getShift.getUser.getRole')
                ->where('md_merchant_shift_id',$id)
                ->where('is_deleted',0)
                ->get();

            $params=[
                'title'=>'Detail Shift ',
                'data'=>$data,
                'first'=>$data->first()
            ];


            return view('merchant::toko.report.shift.detail',$params);
        }catch (\Exception $e)
        {

        }
    }
}

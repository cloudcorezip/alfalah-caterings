<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Report;


use App\Http\Controllers\Controller;
use App\Models\SennaToko\SaleOrder;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\ApprovalSaleOrderEntity;
use Modules\Merchant\Entities\Toko\CustLevelEntity;
use Ramsey\Uuid\Uuid;

class ApprovalSaleOrderController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('report/sale-order/approval')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\Report\ApprovalSaleOrderController@data')
                    ->name('merchant.toko.sale-order.data-approval');
                Route::get('/detail/{id}', 'Toko\Report\ApprovalSaleOrderController@detail')
                    ->name('merchant.toko.sale-order.detail-approval');

                Route::post('/data-table', 'Toko\Report\ApprovalSaleOrderController@dataTable')
                    ->name('merchant.toko.sale-order.datatable-approval');

                Route::post('/reload-data', 'Toko\Report\ApprovalSaleOrderController@reloadData')
                    ->name('merchant.toko.sale-order.reload-data-approval');

                Route::post('/export-data', 'Toko\Report\ApprovalSaleOrderController@exportData')
                    ->name('merchant.toko.sale-order.export-data-approval');
                Route::post('/approve-data', 'Toko\Report\ApprovalSaleOrderController@approveData')
                    ->name('merchant.toko.sale-order.approve-data');

            });

    }

    public function data()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params=[
            'title'=>'Persetujuan Penjualan',
            'merchantId'=>$merchantId,
            'tableColumns'=>ApprovalSaleOrderEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchParams'=>base64_encode(json_encode([])),
            'countApproval'=>ApprovalSaleOrderEntity::countData()
        ];
        return view('merchant::toko.report.sale-order.data-approval',$params);
    }

    public function dataTable(Request $request)
    {
        return ApprovalSaleOrderEntity::dataTable($request);
    }
    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ApprovalSaleOrderEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }

        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);

        $request['count_data'] = 1;

        $params = [
            'title' => 'Persetujuan Penjualan',
            'searchKey' => $searchKey,
            'tableColumns'=>ApprovalSaleOrderEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->md_merchant_id,
            'searchParams'=>base64_encode(json_encode($request->session()->get($searchKey)))
        ];

        return view('merchant::toko.report.sale-order.list-approval',$params);

    }

    public function exportData(Request $request,$fromGraphic=0,$periodFilter='',$time='')
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            $po = ApprovalSaleOrderEntity::class;
            $data=$po::getDataForDataTable();
            $title='Persetujuan Penjualan';

            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

            $searchKey = (array)json_decode(base64_decode($request->input('searchParams')));
            $searchFilter = is_array($searchKey) ? $searchKey : [];

            if(empty($searchFilter)){
                $data->where('sc_sale_orders.is_deleted',0);
            }

            $getBranch=MerchantUtil::getBranch(merchant_id());

            if($request->md_merchant_id=='-1'){

                $data->whereIn('sc_sale_orders.md_merchant_id',$getBranch);

            }else{
                foreach ($po::getFilterMap() as $key => $field) {
                    if (isset($searchFilter[$key]) && $searchFilter[$key] != '-1') {
                        $data->where([$field => $searchFilter[$key]]);
                    }
                }

            }
            $data->whereRaw("
                    sc_sale_orders.created_at::date between '$startDate' and '$endDate'
                    ");

            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan $title Tanggal $startDate sd $endDate");
            $header = [
                'No',
                'Kode',
                'Pelanggan',
                'Metode Transaksi',
                'Nama Pencatat',
                'Diskon',
                'Pajak',
                'Biaya Pengiriman',
                'Metode Pengiriman',
                'Total',
                'Jumlah Hutang',
                'Waktu Transaksi',
                'Zona Waktu',
                'Gudang',
                'Cabang',
                'Produk',
                'Jumlah',
                'Harga',
                'Satuan',
                'Sub Total',

            ];
            $col = 'A';
            foreach ($header as $v) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '2', strtoupper($v));
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '2')->getFont()->setBold( true );
                $col++;
            }
            $num = 2;
            $no=0;
            foreach ($data->get() as $key =>$item)
            {
                if($request->step_type==0)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:$item->code;
                    $trans=($item->trans_name=='Kas' || $item->trans_name=='Kas Kasir Outlet')?'':'-'.$item->trans_name;

                    $name=is_null($item->trans_name)?$item->tipe_transaksi:$item->tipe_transaksi.$trans;
                    if($item->is_from_form_order==1)
                    {
                        $payment= (is_null($item->tipe_transaksi))? 'Belum Terbayar': $name;
                    }else{
                        $payment= (is_null($item->tipe_transaksi))? 'Piutang': $name;

                    }

                }elseif ($request->step_type==1)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PQ',$item->code);
                    $payment='Belum Ada Pembayaran';

                }elseif ($request->step_type==2)
                {
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PO',$item->code);
                    $payment='Belum Ada Pembayaran';

                }else{
                    $kode=(!is_null($item->second_code))?$item->second_code:str_replace('P','PD',$item->code);
                    $payment='Belum Ada Pembayaran';

                }
                $promo_product=0;

                foreach ($item->getDetail->where('is_deleted',0) as $n =>$j) {
                    if($j->is_multi_unit==1)
                    {
                        if(is_null($j->unit_name)){
                            $q=$j->multi_quantity;
                            $u=$j->getProduct->getUnit->name;
                        }else{
                            $q=$j->quantity;
                            $u=$j->unit_name;
                        }
                    }else{
                        $q=$j->quantity;
                        $u=!is_null($j->unit_name)?$j->unit_name:$j->getProduct->getUnit->name;
                    }
                    $dataShow = [
                        $no,
                        $kode,
                        is_null($item->nama_pelanggan)?'-':$item->nama_pelanggan,
                        $payment,
                        is_null($item->nama_pencatat)?'-':$item->nama_pencatat,
                        ($promo_product==0)?$item->diskon:$promo_product,
                        $item->pajak,
                        $item->shipping_cost,
                        is_null($item->shipping_name)?'-':$item->shipping_name,
                        $item->total,
                        (is_null($item->piutang))?0:$item->piutang,
                        $item->waktu_transaksi,
                        $item->timezone,
                        'Gudang Utama',
                        $item->outlet,
                        is_null($j->product_name)?$j->getProduct->name.' '.$j->getProduct->code: $j->product_name,
                        $q,
                        ($j->is_bonus==0)? rupiah($j->price):"Bonus Produk",
                        $u,
                        ($j->is_bonus==0)? rupiah($j->sub_total):0,

                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                    $no++;
                    if($j->multi_quantity!=0){
                        if($j->value_conversation==0){
                            if($j->price*$j->quantity!=$j->sub_total){
                                $promo_product+=$j->price*$j->quantity-$j->sub_total;
                            }
                        }else{
                            if(($j->price*$j->quantity)!=$j->sub_total){
                                $promo_product+=$j->price*$j->quantity-$j->sub_total;
                            }
                        }

                    } else {
                        if($j->price*$j->quantity!=$j->sub_total){
                            $promo_product+=$j->price*$j->quantity-$j->sub_total;
                        }
                    }
                }
            }
            foreach(range('A','Z') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-pemasanan-penjualan_' . merchant_id()."_".$startDate."_".$endDate.'.xlsx');

            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
                    <div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <div id='successExport'></div>
                    <script>
                        let success = document.querySelector('#successExport');
                        if(success){
                            toastForSaveData('Data berhasil diexport!','success',true,'',false);
                        }
                    </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";


        }

    }

    public function detail($id,Request  $request)
    {
        try{
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('getMerchant')->where('id',$id)->first();
            $params=[
                'title'=>'Detail',
                'data'=>$data,
                'promo_product'=>0,
                'page'=>(is_null($request->page))?'transaksi':$request->page

            ];
            return view('merchant::toko.report.sale-order.detail-approval',$params);
        }catch (\Exception $e)
        {
            abort(500);
        }
    }

    public function approveData(Request $request)
    {
        try{
            $id=$request->id;
            $data=SaleOrder::find($id);
            $data->is_approve=$request->is_approve;
            $data->approve_by=user_id();
            $data->save();
            if($data->is_approve==1){
                $message='Pemesanan berhasil disetujui';
            }elseif ($data->is_approve==2)
            {
                $message='Pemesanan berhasil ditolak';

            }else{
                $message='Pembetalan persetujuan pemesanan berhasil disimpan';

            }
            return response()->json([
                'message'=>$message,
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }
}

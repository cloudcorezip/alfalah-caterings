<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Report;

use App\Classes\CollectionPagination;
use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Attendance\MerchantStaffAttendance;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\PermitCategory;
use App\Models\MasterData\User;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Modules\Merchant\Entities\Toko\PermitStaffEntity;
use Modules\Merchant\Entities\Toko\ReportStaffAttendanceEntity;
use Modules\Merchant\Entities\Toko\AttendancePerEmployeeEntity;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Entities\HR\DepartementEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use PDF;
class StaffAttendanceController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('report/staff-attendance')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\Report\StaffAttendanceController@data')
                    ->name('merchant.toko.report.staff-attendance.data');
                Route::post('/data-table', 'Toko\Report\StaffAttendanceController@dataTable')
                    ->name('merchant.toko.report.staff-attendance.datatable');
                Route::post('/reload-data', 'Toko\Report\StaffAttendanceController@reloadData')
                    ->name('merchant.toko.report.staff-attendance.reload-data');
                Route::post('/cetak-pdf', 'Toko\Report\StaffAttendanceController@cetakPDF')
                    ->name('merchant.toko.report.staff-attendance.cetakPDF');
                Route::post('/detail', 'Toko\Report\StaffAttendanceController@detailAttendance')
                    ->name('merchant.toko.report.staff-attendance.detail-attendance');
                Route::post('/save', 'Toko\Report\StaffAttendanceController@save')
                    ->name('merchant.toko.report.staff-attendance.save');
                Route::get('/add-attendance', 'Toko\Report\StaffAttendanceController@addAttendance')
                    ->name('merchant.toko.report.staff-attendance.add-attendance');
                Route::get('/add-attendance-from-list', 'Toko\Report\StaffAttendanceController@addAttendanceFromList')
                    ->name('merchant.toko.report.staff-attendance.add-attendance-from-list');
                Route::get('/edit-attendance', 'Toko\Report\StaffAttendanceController@editAttendance')
                    ->name('merchant.toko.report.staff-attendance.edit-attendance');
                Route::post('/get-staff', 'Toko\Report\StaffAttendanceController@getStaff')
                    ->name('merchant.toko.report.staff-attendance.get-staff');

                Route::post('/check-holiday' , 'Toko\Report\StaffAttendanceController@checkHoliday')
                    ->name('merchant.toko.report.staff-attendance.check-holiday');

                Route::post('/check-shift' , 'Toko\Report\StaffAttendanceController@checkShift')
                    ->name('merchant.toko.report.staff-attendance.check-shift');

                Route::get('/data-employee', 'Toko\Report\StaffAttendanceController@dataEmployee')
                    ->name('merchant.toko.report.staff-attendance.dataEmployee');

                Route::post('/data-table-employee', 'Toko\Report\StaffAttendanceController@dataTableEmployee')
                    ->name('merchant.toko.report.staff-attendance.datatable-employee');

                Route::post('/reload-data-employee', 'Toko\Report\StaffAttendanceController@reloadDataEmployee')
                    ->name('merchant.toko.report.staff-attendance.reload-data-employee');

                Route::post('/load-header-employee', 'Toko\Report\StaffAttendanceController@loadHeaderEmployee')
                    ->name('merchant.toko.report.staff-attendance.load-header-employee');
            });
    }


    public function getStaff(Request $request)
    {
        try {
            $merchantId = $request->md_merchant_id;
            $data=MerchantStaff::
            select([
                'u.id',
                DB::raw("u.fullname || '-' || coalesce(jp.name, '') as text")
            ])
            ->where(['md_merchant_staff.md_merchant_id'=>$merchantId])
            ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
            ->leftJoin('hr_merchant_job_positions as jp','jp.id','md_merchant_staff.job_position_id')
            ->where('md_merchant_staff.is_non_employee',0)
            ->where('md_merchant_staff.is_deleted',0)
            ->get();
            return response()->json($data);

        } catch(\Exception $e)
        {
            return [];
        }
    }

    private function _loadHeaderDay($startDate,$endDate)
    {
        return DB::select("
        select
          to_char(mon.mon, 'YYYY-MM-DD') AS time,
          to_char(mon.mon, 'DD') as day,
          to_char(mon.mon, 'MM') as month
        from
          generate_series(
            '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
            interval '1 day'
          ) as mon(mon)
        ");
    }

    private function _loadData($startDate,$endDate,$merchantId,$isPagination=true,$page='')
    {
        try {
            $now = date('Y-m-d');

            $data=collect(DB::select("
            select
                mms.id as staff_id,
                u.id as staff_user_id,
                u.fullname,
                mms.md_merchant_id,
                (select
                    count(*)
                from
                    md_merchant_staff_attendances
                where
                    md_merchant_staff_attendances.md_merchant_id = mms.md_merchant_id
                    and
                    md_merchant_staff_attendances.md_staff_user_id = mms.md_user_id
                    and
                    md_merchant_staff_attendances.is_deleted = 0
                    and
                    md_merchant_staff_attendances.type_of_attendance = 'H'
                    and
                    md_merchant_staff_attendances.created_at::date between '$startDate' and '$endDate'
                ) as total_day,
                (select
                    extract(epoch from sum(hsa.end_work - hsa.start_work)) as total_hour
                from
                    md_merchant_staff_attendances hsa
                where
                    hsa.md_merchant_id = mms.md_merchant_id
                    and
                    hsa.md_staff_user_id = mms.md_user_id
                    and
                    hsa.is_deleted = 0
                    and
                    hsa.type_of_attendance = 'H'
                    and
                    hsa.created_at::date between '$startDate' and '$endDate'
                    and
                    hsa.end_work is not null
                ) as total_hour,
                coalesce(
                    (
                      select
                        jsonb_agg(jd.*) AS jsonb_agg
                      from
                        (
                          select
                            u.fullname,
                            mmsa.id,
                            mmsa.start_work,
                            mmsa.end_work,
                            mmsa.is_end,
                            to_char(mon.mon, 'YYYY-MM-DD') AS time,
                            to_char(mon.mon, 'DD') as day,
                            to_char(mon.mon, 'MM') as month,
                            (CASE
                                WHEN
                                    mmsa.type_of_attendance is null AND mon.mon < '$now'
                                THEN
                                    'A'
                                ELSE (CASE
                                        WHEN
                                            mmsa.type_of_attendance is null
                                        THEN
                                            '-'
                                        ELSE
                                            mmsa.type_of_attendance
                                        END)
                            END) as type_of_attendance,
                            (CASE
                                WHEN
                                    hmh.is_work = 1
                                THEN
                                    0
                                ELSE (CASE
                                        WHEN
                                            hmh.occasion isnull
                                        THEN
                                            0
                                        ELSE
                                            1
                                    END)
                            END) as is_holiday,
                            hmh.occasion
                          from
                            generate_series(
                              '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                              interval '1 day'
                            ) as mon(mon)
                            left join (
                                select
                                    hm.is_work,
                                    hm.holiday_date,
                                    hm.occasion
                                from
                                    hr_merchant_holidays hm
                                where
                                    hm.is_deleted = 0
                                    and
                                    hm.md_merchant_id = mms.md_merchant_id
                            ) hmh on hmh.holiday_date = mon.mon
                            left join (
                              select
                                s.id,
                                date_trunc('day', s.created_at) as mon,
                                s.type_of_attendance,
                                s.start_work,
                                s.end_work,
                                s.is_end
                              from
                                md_merchant_staff_attendances s
                              where
                                s.md_staff_user_id =u.id
                              group by
                                mon,
                                s.id
                            ) mmsa on mon.mon = mmsa.mon
                          order by
                            mon.mon asc
                        ) jd
                    ),
                    '[]'
                  ) as details
            from
                md_merchant_staff mms
            join
                md_users u on mms.md_user_id = u.id
            join
                md_roles r on r.id = u.md_role_id
            where
                mms.md_merchant_id = $merchantId
                and mms.is_deleted = 0
                and mms.is_non_employee=0
            "));
            if($isPagination==true)
            {
                return CollectionPagination::paginate($data,10);
            }else{
                return  $data;
            }
        }catch (\Exception $e)
        {
            return collect([]);
        }
    }

    public function checkHoliday(Request $request)
    {
        try {
            $date = $request->date;
            $merchantId = $request->md_merchant_id;

            $data = collect(DB::select(
            "select
                hmh.id,
                hmh.md_merchant_id,
                hmh.occasion,
                hmh.holiday_date,
                hmh.is_work
            from
                hr_merchant_holidays hmh
            where
                hmh.holiday_date::date = '$date'
                and
                hmh.md_merchant_id = ".MerchantUtil::getHeadBranch($merchantId)."
                and
                hmh.is_deleted = 0
            "
            ))->first();

            if(is_null($data)){
                return response()->json(["is_work" => 1]);
            }

            return response()->json($data);

        } catch(\Exception $e)
        {
            return response()->json(["is_work" => 1]);
        }
    }

    public function checkShift(Request $request)
    {
        try {
            $staffUserId = $request->staff_user_id;
            $date = $request->date;

            $data = collect(DB::select(
            "select
                hmsr.id,
                hmsr.shift_date,
                hmss.name as text,
                hmss.start_shift,
                hmss.end_shift,
                hmss.half_shift,
                hmss.max_late,
                hmss.max_allowed_attendance_per_day,
                hmss.work_day,
                coalesce(hmas.is_allowed_shift_other_time, 1) as is_allowed_shift_other_time
            from
                hr_merchant_shift_roasters hmsr
            join
                hr_merchant_shift_settings hmss on hmss.id = hmsr.shift_id
            left join
                hr_merchant_attendance_settings hmas on hmas.md_merchant_id = hmsr.md_merchant_id
            where
                hmsr.staff_user_id = $staffUserId
                and
                hmsr.shift_date::date = '$date'
            "
            ));

            return response()->json($data);

        } catch(\Exception $e)
        {
            return response()->json();
        }
    }


    private function _checkHoliday($merchantId, $date)
    {
        try {

            $data = collect(DB::select(
            "select
                hmh.id,
                hmh.md_merchant_id,
                hmh.occasion,
                hmh.holiday_date,
                hmh.is_work
            from
                hr_merchant_holidays hmh
            where
                hmh.holiday_date::date = '$date'
                and
                hmh.md_merchant_id = ".MerchantUtil::getHeadBranch($merchantId)."
                and
                hmh.is_deleted = 0
            "
            ))->first();

            if(is_null($data)){
                return (object) ["is_work"=> 1, "occasion"=>""];
            }

            return $data;

        } catch(\Exception $e)
        {
            return (object) ["is_work" => 1, "occasion"=>""];
        }
    }

    private function _checkShift($staffUserId, $date)
    {
        try {
            $data = collect(DB::select(
            "select
                hmsr.id,
                hmsr.shift_date,
                hmss.name as text,
                hmss.start_shift,
                hmss.end_shift,
                hmss.half_shift,
                hmss.max_late,
                hmss.max_allowed_attendance_per_day,
                hmss.work_day,
                hmas.is_allowed_shift_change,
                coalesce(hmas.is_allowed_shift_other_time, 1) as is_allowed_shift_other_time
            from
                hr_merchant_shift_roasters hmsr
            join
                hr_merchant_shift_settings hmss on hmss.id = hmsr.shift_id
            left join
                hr_merchant_attendance_settings hmas on hmas.md_merchant_id = hmsr.md_merchant_id
            where
                hmsr.staff_user_id = $staffUserId
                and
                hmsr.shift_date::date = '$date'
            "
            ))->first();

            if(is_null($data)){
                return false;
            }

            return $data;

        } catch(\Exception $e)
        {
            return false;
        }
    }

    public function data(Request $request)
    {
        parent::setMenuKey($request->key);
        $merchantId=(is_null($request->md_merchant_id))?merchant_id():$request->md_merchant_id;
        $jobId=(is_null($request->job_id))?'-1':$request->job_id;
        $departement=is_null($request->departement_id)?'-1':$request->departement_id;

        $startDate=$request->start_date;
        $endDate=$request->end_date;
        if(is_null($startDate) || $startDate==''){
            $startDate=Carbon::now()->startOfMonth()->toDateString();
            $endDate=Carbon::now()->endOfMonth()->toDateString();
        }

        $params=[
            'title'=>'Presensi',
            'data'=>$this->_loadData($startDate,$endDate,$merchantId,true),
            'series'=>$this->_loadHeaderDay($startDate,$endDate),
            'merchantId'=>$merchantId,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'job' => JobEntity::listOption(),
            'job_id' => $jobId,
            'departement'=>DepartementEntity::listOption(),
            'departement_id'=>$departement
        ];

        return view('merchant::toko.report.staff-attendance.data',$params);
    }

    public function dataEmployee(Request $request)
    {
        parent::setMenuKey($request->key);
        $merchantId  = (is_null($request->md_merchant_id))?merchant_id():$request->md_merchant_id;
        $startDate = Carbon::now()->startOfMonth()->toDateString();
        $endDate = Carbon::now()->endOfMonth()->toDateString();

        $staff = MerchantStaff::
        select([
            "u.id",
            "u.fullname as staff_name",
            "p.name as position_name",
            DB::raw("  u.fullname || ' ' || coalesce( ' [' || p.name || '] ','') || '[' || m.name || '] ' as text")
        ])
        ->join('md_users as u', 'u.id', 'md_merchant_staff.md_user_id')
        ->join('md_merchants as m', 'm.id', 'md_merchant_staff.md_merchant_id')
        ->leftJoin('hr_merchant_job_positions as p', 'p.id', 'md_merchant_staff.job_position_id')
        ->where('md_merchant_staff.md_merchant_id', $merchantId)
        ->first();

        $params = [
            'title' => 'Presensi Per Karyawan',
            'merchantId'=>$merchantId,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'tableColumns'=>AttendancePerEmployeeEntity::dataTableColumns(),
            'key_val'=>$request->key,
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'staff' => $staff,
            'staff_user_id' => is_null($staff)?null:$staff->id
        ];

        return view('merchant::toko.report.staff-attendance.data-employee', $params);
    }

    public function dataTable(Request $request)
    {
        return ReportStaffAttendanceEntity::dataTable($request);
    }

    public function dataTableEmployee(Request $request)
    {
        return AttendancePerEmployeeEntity::dataTable($request);
    }

    public function loadHeaderEmployee(Request $request)
    {
        try {
            $staff_user_id = $request->md_staff_user_id;
            $merchantId = $request->md_merchant_id;

            if(is_null($staff_user_id)){
                return [];
            }

            $data = collect(DB::select("
            select
                mu.id,
                mms.id as staff_id,
                mu.fullname,
                mu.foto,
                coalesce(jp.name, '') as job_name,
                (select
                    count(*)
                from
                    md_merchant_staff_attendances
                where
                    md_merchant_staff_attendances.md_merchant_id = mm.id
                    and
                    md_merchant_staff_attendances.md_staff_user_id = mu.id
                    and
                    md_merchant_staff_attendances.is_deleted = 0
                    and
                    md_merchant_staff_attendances.type_of_attendance = 'H'
                ) as kehadiran,
                (select
                    count(*)
                from
                    md_merchant_staff_attendances
                where
                    md_merchant_staff_attendances.md_merchant_id = mm.id
                    and
                    md_merchant_staff_attendances.md_staff_user_id = mu.id
                    and
                    md_merchant_staff_attendances.is_deleted = 0
                    and
                    md_merchant_staff_attendances.is_late = 1
                ) as terlambat,
                (select
                    count(*)
                from
                    md_merchant_staff_attendances
                where
                    md_merchant_staff_attendances.md_merchant_id = mm.id
                    and
                    md_merchant_staff_attendances.md_staff_user_id = mu.id
                    and
                    md_merchant_staff_attendances.is_deleted = 0
                    and
                    md_merchant_staff_attendances.type_of_attendance = 'I'
                ) as izin,
                (select
                    count(*)
                from
                    md_merchant_staff_attendances
                where
                    md_merchant_staff_attendances.md_merchant_id = mm.id
                    and
                    md_merchant_staff_attendances.md_staff_user_id = mu.id
                    and
                    md_merchant_staff_attendances.is_deleted = 0
                    and
                    md_merchant_staff_attendances.type_of_attendance = 'S'
                ) as sakit
            from
                md_merchant_staff mms
            join
                md_users mu on mu.id = mms.md_user_id
            join
                md_merchants mm on mm.id = mms.md_merchant_id
            left join
                hr_merchant_job_positions jp on jp.id = mms.job_position_id
            where
                mms.md_user_id = $staff_user_id
                and
                mms.md_merchant_id = $merchantId

            "));

            return $data;

        } catch(\Exception $e)
        {
            return [];
        }
    }

    public function reloadDataEmployee(Request $request)
    {
        $searchParams = [];
        foreach (AttendancePerEmployeeEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }

        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Presensi Per Karyawan',
            'searchKey' => $searchKey,
            'tableColumns'=>AttendancePerEmployeeEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->md_merchant_id,
            'staff_user_id' => $request->md_staff_user_id
        ];
        return view('merchant::toko.report.staff-attendance.list-employee',$params);

    }

    public function detailAttendance(Request $request)
    {
        $id = $request->id;

        $data = collect(DB::select("
        select
            mmsa.id,
            mm.name as merchant_name,
            mu.fullname as staff_name,
            hmss.name as shift_name,
            mmsa.note,
            mmsa.start_work,
            mmsa.end_work,
            mmsa.start_work_file,
            mmsa.end_work_file,
            mmsa.created_at,
            mmsa.shift_roaster_id
        from
            md_merchant_staff_attendances mmsa
        join
            md_merchants mm on mm.id = mmsa.md_merchant_id
        join
            md_users mu on mu.id = mmsa.md_staff_user_id
        left join
            hr_merchant_shift_roasters hmsr on hmsr.id = mmsa.shift_roaster_id
        left join
            hr_merchant_shift_settings hmss on hmss.id = hmsr.shift_id
        where
            mmsa.id = $id
        "))->first();

        $startWork = $data->start_work;
        $endWork = $data->end_work;
        $diff = date_diff(date_create($startWork), date_create($endWork));
        $diffHour = $diff->format('%h jam %i menit');
        $params=[
            'title'=>'Detail Presensi',
            'data'=>$data,
            'diffHour' => $diffHour
        ];

        return view('merchant::toko.report.staff-attendance.detail',$params);
    }

    public function addAttendance(Request $request)
    {
        $id = $request->id;
        $now = Carbon::now();

        if(is_null($id)){
            $data = new MerchantStaffAttendance();
        } else {
            $data = MerchantStaffAttendance::find($id);
        }

        $params=[
            'title'=>'Tambah Presensi',
            'data' => $data,
            'now'=>$now
        ];

        return view('merchant::toko.report.staff-attendance.form-add-attendance',$params);
    }

    public function addAttendanceFromList(Request $request)
    {
        $merchantId = $request->md_merchant_id;
        $date = $request->date;
        $staff_user_id = $request->staff_user_id;

        $data = new MerchantStaffAttendance();

        $staff = MerchantStaff::
        select([
            "u.id",
            "u.fullname as staff_name",
            "p.name as position_name",
            DB::raw("  u.fullname || ' ' || coalesce( ' [' || p.name || '] ','') || '[' || m.name || '] ' as text")
        ])
        ->join('md_users as u', 'u.id', 'md_merchant_staff.md_user_id')
        ->join('md_merchants as m', 'm.id', 'md_merchant_staff.md_merchant_id')
        ->leftJoin('hr_merchant_job_positions as p', 'p.id', 'md_merchant_staff.job_position_id')
        ->where('md_merchant_staff.md_user_id', $staff_user_id)
        ->first();

        $params=[
            'title'=>'Tambah Presensi',
            'data' => $data,
            'staff'=>$staff,
            'date'=>$date,
            'merchantId' => $merchantId,
            'staff_user_id' => $staff_user_id
        ];

        return view('merchant::toko.report.staff-attendance.form-add-attendance-from-list',$params);

    }

    public function editAttendance(Request $request)
    {
        $id = $request->id;
        $now = Carbon::now();
        $data = MerchantStaffAttendance::select([
            "md_merchant_staff_attendances.*",
            "u.fullname as staff_name",
            "jp.name as position_name",
            "hmss.name as shift_roaster_name",
            DB::raw("  u.fullname || ' ' || coalesce( ' [' || jp.name || '] ','') || '[' || m.name || '] ' as text")
        ])
        ->join('md_users as u', 'u.id', 'md_merchant_staff_attendances.md_staff_user_id')
        ->join('md_merchant_staff as mms', 'mms.md_user_id', 'u.id')
        ->join('md_merchants as m', 'm.id', 'md_merchant_staff_attendances.md_merchant_id')
        ->leftJoin('hr_merchant_job_positions as jp', 'jp.id', 'mms.job_position_id')
        ->leftJoin('hr_merchant_shift_roasters as hmsr', 'hmsr.id', 'md_merchant_staff_attendances.shift_roaster_id')
        ->leftJoin('hr_merchant_shift_settings as hmss', 'hmss.id', 'hmsr.shift_id')
        ->where('md_merchant_staff_attendances.id', $id)
        ->first();

        if(is_null($data)){
            return abort(404);
        }

        $params=[
            'title'=>'Ubah Presensi',
            'data' => $data,
            'now'=>$now
        ];

        return view('merchant::toko.report.staff-attendance.form-edit-attendance',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $id = $request->id;
            $merchantId = $request->md_merchant_id;
            if(is_null($merchantId) || $merchantId == '-1'){
                return response()->json([
                    'message'=>'Cabang belum dipilih !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $createdAt=Carbon::parse($request->created_at);
            $startWork=Carbon::parse($request->start_work);
            $endWork=Carbon::parse($request->end_work);

            if($createdAt->toDateString()!=$startWork->toDateString())
            {
                return response()->json([
                    'message'=>'Perhatian! waktu absensi dan tanggal pencatatan absensi tidak sama',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if(!is_null($request->end_work)){
                if($createdAt->toDateString() != $endWork->toDateString()){
                    return response()->json([
                        'message'=>'Perhatian! tanggal waktu akhir kerja tidak sesuai dengan hari yang dipilih',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false
                    ]);
                }
            }

            $check=PermitStaffEntity::whereDate('created_at',$createdAt->toDateString())
                ->where('md_staff_user_id',$request->md_staff_user_id)
                ->first();

            if($check)
            {
                return response()->json([
                    'message'=>"Mohon maaf,karyawan ini telah melakukan permintaan izin pada tanggal".$createdAt->isoFormat('D MMMM Y')."!",
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            if(!is_null($request->end_work)){
                if($request->start_work>$request->end_work){
                    return response()->json([
                        'message'=>'Jam masuk kerja tidak boleh melebihi jam pulang',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false
                    ]);
                }
            }

            if(is_null($id)){
                $data=new MerchantStaffAttendance();
            } else {
                $data = MerchantStaffAttendance::find($id);
            }

            $checkAttendance = MerchantStaffAttendance::
            select([
                'md_merchant_staff_attendances.*',
                'u.fullname',
                'r.name as role_name'
            ])
            ->join('md_users as u','u.id','md_merchant_staff_attendances.md_staff_user_id')
            ->join('md_roles as r','r.id','u.md_role_id')
            ->where('md_merchant_staff_attendances.md_staff_user_id',$request->md_staff_user_id)
            ->where('md_merchant_staff_attendances.id','!=' ,$id)
            ->whereDate('md_merchant_staff_attendances.created_at',$createdAt->toDateString())
            ->first();

            if(!is_null($checkAttendance)){
                return response()->json([
                    'message'=>"Mohon maaf, $checkAttendance->fullname sudah melakukan absensi pada tanggal ".$createdAt->isoFormat('D MMMM Y'),
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if(is_null($data->id)){
                $validator = Validator::make($request->all(),$data->fileStartValidate);
                if ($validator->fails()) {
                    $error = $validator->errors()->first();

                    return response()->json([
                        'message'=>$error,
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false
                    ]);
                }
            }

            $checkIsWork = $this->_checkHoliday($merchantId, $request->created_at);

            if($checkIsWork->is_work == 0){
                return response()->json([
                    'message'=>"Tidak dapat melakukan absensi pada hari libur $checkIsWork->occasion",
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $checkShift = $this->_checkShift($request->md_staff_user_id, $request->created_at);

            if($checkShift != false){
                if($checkShift->is_allowed_shift_other_time == 0){
                    if(Carbon::parse($request->start_work)->isoFormat('H:mm:ss') > $checkShift->end_shift){
                        return response()->json([
                            'message'=>" Tidak dapat melakukan presensi di luar jam shift kerja",
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);
                    }

                }
            }

            $destinationPath = 'public/uploads/merchant/'.$merchantId.'/attendance/'.date('YmdHis').'/';

            if($request->hasFile('end_work_file'))
            {
                if(is_null($request->end_work)){
                    return response()->json([
                        'message'=>'Jam pulang kerja belum diisi !',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false
                    ]);
                }
                $file2=$request->file('end_work_file');
                $fileName2= uniqid(rand(1,1000000)).'_end_'.date('YmdHis').'_'.str_replace(' ','_',$file2->getClientOriginalName());
                $fileName2=$destinationPath.$fileName2;
                Storage::disk('s3')->put($fileName2, file_get_contents($file2));
            }else{
                if(!is_null($data->id))
                {
                    $fileName2=$data->end_work_file;

                }else{
                    $fileName2=null;

                }
            }

            if(!is_null($request->end_work)){
                if(is_null($fileName2)){
                    return response()->json([
                        'message'=>'Foto akhir kerja belum diisi !',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false
                    ]);
                }
            }

            if($request->hasFile('start_work_file'))
            {
                $file=$request->file('start_work_file');
                $fileName= uniqid(rand(1,1000000)).'_start_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                if(!is_null($data->id))
                {
                    $fileName=$data->start_work_file;

                }else{
                    $fileName=null;

                }
            }

            if(!is_null($request->end_work)){
                $data->is_end = 1;
            }

            $data->start_work=$request->start_work;
            $data->md_merchant_id=$merchantId;
            $data->md_staff_user_id=$request->md_staff_user_id;
            $data->start_work_file=$fileName;
            $data->end_work=$request->end_work;
            $data->end_work_file=$fileName2;
            $data->start_longitude="null";
            $data->start_latitude="null";
            $data->type_of_attendance='H';
            $data->shift_roaster_id = $request->shift;
            $data->is_late = $request->is_late;
            $data->note = $request->note;
            $data->created_at=$request->created_at;
            $data->updated_at=$request->created_at;
            $data->timezone = $request->_timezone;
            $data->save();

            DB::commit();
            return response()->json([
                'message'=>'Data presensi berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.report.staff-attendance.data'),
                'is_with_datatable'=>true
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false
            ]);


        }
    }

    public function cetakPDF(Request $request,$fromGraphic=0,$periodFilter='',$time=''){
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);

            $merchantId = $request->md_merchant_id;
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

            $userId=$request->md_staff_user_id;
            if($userId=='-1'){
                $stafId=MerchantStaff::where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->get();

                $data=[];

                foreach($stafId as $item){
                    $data[]=['name'=>$item->getUser->fullname];
                    $uid=$item->getUser->id;
                    $data[]= DB::select("
                    select
                    to_char(mon.mon, 'YYYY-MM-DD') AS time,
                    case when s.type_of_attendance is null then 'A' else s.type_of_attendance end,
                    s.start_work,
                    s.end_work,
                    s.start_latitude,
                    s.start_longitude,
                    s.end_latitude,
                    s.end_longitude,
                    s.start_work_file,
                    s.end_work_file
                    from
                    generate_series(
                        '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                        interval '1 day'
                    ) as mon(mon)
                    left join (
                        select
                        date_trunc('day', mmsa.created_at) as mon,
                        mmsa.*
                        from
                        md_merchant_staff_attendances mmsa
                        where mmsa.md_staff_user_id=$uid
                        group by
                        mon,
                        mmsa.id
                    ) s on mon.mon = s.mon
                    order by
                    mon.mon asc;
                    ");
                }

            }else{
                $u=User::find($userId);

                $data[]=['name'=>$u->fullname];
                $data[]= DB::select("
                    select
                    to_char(mon.mon, 'YYYY-MM-DD') AS time,
                    case when s.type_of_attendance is null then 'A' else s.type_of_attendance end,
                    s.start_work,
                    s.end_work,
                    s.start_latitude,
                    s.start_longitude,
                    s.end_latitude,
                    s.end_longitude,
                    s.start_work_file,
                    s.end_work_file
                    from
                    generate_series(
                        '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                        interval '1 day'
                    ) as mon(mon)
                    left join (
                        select
                        date_trunc('day', mmsa.created_at) as mon,
                        mmsa.*
                        from
                        md_merchant_staff_attendances mmsa
                        where mmsa.md_staff_user_id=$userId
                        group by
                        mon,
                        mmsa.id
                    ) s on mon.mon = s.mon
                    order by
                    mon.mon asc;
                    ");
            }


                //dd($stafId);
                $styleArray = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];
                $style = array(
                    'alignment' => array(
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER
                    )
                );

                // set data excel
                $fileName = public_path('example/example.xlsx');
                $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

                if($fromGraphic==0)
                {
                    $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Absensi Karyawan Tanggal $startDate sampai $endDate ");

                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Absensi Karyawan Tanggal $time ");

                }

                $alpha=0;$hadir=0;$izin=0;
                $num = 4;

                foreach ($data as $key => $item) {

                    if(!array_key_exists("name",$item)){
                        $sumRow = $num+count($item);
                        $objPHPExcel->getActiveSheet()->mergeCells('A'.$num.':A'.$sumRow);
                        foreach($item as $key => $i){

                            $dataShow = [
                                $key+1,
                                $i->time,
                                (!is_null($i->start_work))?date('H:i', strtotime($i->start_work)):'-',
                                (!is_null($i->end_work))?date('H:i', strtotime($i->end_work)):'-',
                                $i->type_of_attendance
                            ];
                            if($i->type_of_attendance=='A')
                            {
                                $alpha++;
                            }elseif ($i->type_of_attendance=='H')
                            {
                                $hadir++;
                            }else{
                                $izin++;
                            }
                            $col = 'A';
                            $row = $num + 1;
                            foreach ($dataShow as $ds) {
                                $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                                $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                                $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($style);
                                $col++;
                            }

                            $num++;
                        }
                        $num= $row + 1;
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$num,"Total Alpha:");
                        $objPHPExcel->getActiveSheet()->getStyle('D'.$num)->getFont()->setBold( true );
                        $objPHPExcel->getActiveSheet()->getStyle('D'.$num)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$num,$alpha);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->applyFromArray($style);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->getFont()->setBold( true );
                        $num+= 1;
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$num,"Total Hadir:");
                        $objPHPExcel->getActiveSheet()->getStyle('D'.$num)->getFont()->setBold( true );
                        $objPHPExcel->getActiveSheet()->getStyle('D'.$num)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$num,$hadir);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->applyFromArray($style);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->getFont()->setBold( true );
                        $num+= 1;
                        $objPHPExcel->getActiveSheet()->setCellValue('D'.$num,"Total Izin:");
                        $objPHPExcel->getActiveSheet()->getStyle('D'.$num)->getFont()->setBold( true );
                        $objPHPExcel->getActiveSheet()->getStyle('D'.$num)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->setCellValue('E'.$num,$izin);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->applyFromArray($style);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->getStyle('E'.$num)->getFont()->setBold( true );
                        $alpha=0;$hadir=0;$izin=0;
                        $num+=3;
                    }else{
                        $header = [
                            ' ',
                            'Tanggal',
                            'Masuk Kerja',
                            'Pulang Kerja',
                            'Keterangan'
                        ];
                        $col = 'A';
                        foreach ($header as $i) {
                            $objPHPExcel->getActiveSheet()->setCellValue($col . $num, strtoupper($i));
                            $objPHPExcel->getActiveSheet()->getStyle($col . $num)->applyFromArray($style);
                            $objPHPExcel->getActiveSheet()->getStyle($col . $num)->applyFromArray($styleArray);
                            $objPHPExcel->getActiveSheet()->getStyle($col . $num)->getFont()->setBold( true );
                            $col++;
                        }
                        $name=$item['name'];
                        $objPHPExcel->getActiveSheet()->setCellValue('A'.$num,"Nama: $name");

                    }

                }


            foreach(range('A','E') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.$merchantId.'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-absensi_'.$merchantId. '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL';</script>
           <script>
                toastForSaveData('Data absensi berhasil diexport!','success',true,'')
                 setTimeout(() => {
                        closeModal();
                    }, 3000)
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<script>toastForSaveData('Terjadi kesalahan, Data absensi gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)</script>";
        }
    }

    public function detail($id)
    {
        try{
            $staff=ReportStaffAttendanceEntity::with(['getStaff.getRole'])
                ->where('md_staff_user_id',$id)
                ->first();
            $data=ReportStaffAttendanceEntity::where('md_staff_user_id',$id)
                ->get();

            $params=[
                'title'=>'Detail Absensi ',
                'data'=>$data,
                'staff'=>$staff
            ];
            return view('merchant::toko.report.staff-attendance.detail',$params);

        }catch (\Exception $e)
        {
            return view('pages.500');
        }

    }


    public function reloadData(Request $request)
    {

        $searchParams = [];
        foreach (ReportStaffAttendanceEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Detail Absensi',
            'searchKey' => $searchKey,
            'tableColumns'=>ReportStaffAttendanceEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'merchantId'=>$request->merchant_id
        ];
        return view('merchant::toko.report.staff-attendance.list',$params);

    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Http\Controllers\Controller;
use App\Models\SennaCashier\SupplierCategories;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\MasterData\Plugin;
use App\Utils\Plugin\PluginUtil;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use App\Models\Plugin\EnableMerchantPlugin;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;

class SupplierController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('master-data/supplier')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\SupplierController@index')
                    ->name('merchant.toko.supplier.index');
                Route::post('/data-table', 'Toko\SupplierController@dataTable')
                    ->name('merchant.toko.supplier.datatable');
                Route::post('/add', 'Toko\SupplierController@add')
                    ->name('merchant.toko.supplier.add');
                Route::post('/save', 'Toko\SupplierController@save')
                    ->name('merchant.toko.supplier.save');
                Route::post('/delete', 'Toko\SupplierController@delete')
                    ->name('merchant.toko.supplier.delete');
                Route::post('/export-data', 'Toko\SupplierController@exportData')
                    ->name('merchant.toko.supplier.export-data');
                Route::post('/reload-data', 'Toko\SupplierController@reloadData')
                    ->name('merchant.toko.supplier.reload-data');

                Route::get('/add-supplier', 'Toko\SupplierController@addSupplier')
                    ->name('merchant.toko.supplier.add-supplier');

                // sync google contact
                Route::post('/contact-sync-form', 'Toko\SupplierController@contactSyncForm')
                    ->name('merchant.toko.supplier.contact-sync-form');
                Route::post('/contact-sync-compare', 'Toko\SupplierController@contactSyncCompare')
                    ->name('merchant.toko.supplier.contact-sync-compare');
                Route::post('/contact-sync-save', 'Toko\SupplierController@contactSyncSave')
                    ->name('merchant.toko.supplier.contact-sync-save');
                Route::post('/contact-sync-update-form', 'Toko\SupplierController@contactSyncUpdateForm')
                    ->name('merchant.toko.supplier.contact-sync-update-form');
                Route::post('/contact-sync-update-save', 'Toko\SupplierController@contactSyncUpdateSave')
                    ->name('merchant.toko.supplier.contact-sync-update-save');

                // get city
                Route::post('/get-city', 'Toko\CustomerController@getCity')
                    ->name('merchant.toko.supplier.get-city');
            });
    }

    private function _activePlugin()
    {
        return PluginUtil::checkActivePlugin(merchant_id(), Plugin::GOOGLE_CONTACT);
    }

    private function _apiUrl()
    {
        return env('NODE_URL');
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Supplier',
            'tableColumns'=>SupplierEntity::dataTableColumns(),
            'add'=>(get_role()==3)?SupplierEntity::add(true):
                MerkEntity::add(true),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'level'=>SupplierCategories::
            select([
                'sc_supplier_categories.id',
                'sc_supplier_categories.name',
                'sc_supplier_categories.description'
            ])
                ->join('md_users as u','u.id','sc_supplier_categories.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))
                ->where('sc_supplier_categories.is_deleted',0)
                ->orderBy('sc_supplier_categories.id','asc')->get(),
            'key_val'=>$request->key,
            'activePlugin' => $this->_activePlugin(),
            'apiUrl' => $this->_apiUrl()


        ];

        return view('merchant::toko.master-data.supplier.index',$params);

    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (SupplierEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Supplier',
            'searchKey' => $searchKey,
            'tableColumns'=>SupplierEntity::dataTableColumns(),
            'key_val'=>$request->key,
            'activePlugin' => $this->_activePlugin(),
            'apiUrl' => $this->_apiUrl()
        ];
        return view('merchant::toko.master-data.supplier.list',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return SupplierEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=SupplierEntity::find($id);
        }else{
            $data=new SupplierEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'category'=>SupplierCategories::
            select([
                'sc_supplier_categories.id',
                'sc_supplier_categories.name',
                'sc_supplier_categories.description'
            ])
                ->join('md_users as u','u.id','sc_supplier_categories.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))
                ->where('sc_supplier_categories.is_deleted',0)
                ->orderBy('sc_supplier_categories.id','asc')->get(),
            'activePlugin' => $this->_activePlugin(),
            'provinces' => Province::all(),
            'apiUrl' => $this->_apiUrl()
        ];

        return view('merchant::toko.master-data.supplier.form',$params);
    }

    public  function addSupplier(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=SupplierEntity::find($id);
        }else{
            $data=new SupplierEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'category'=>SupplierCategories::
            select([
                'sc_supplier_categories.id',
                'sc_supplier_categories.name',
                'sc_supplier_categories.description'
            ])
                ->join('md_users as u','u.id','sc_supplier_categories.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id(),1))
                ->where('sc_supplier_categories.is_deleted',0)
                ->orderBy('sc_supplier_categories.id','asc')->get(),
            'activePlugin' => $this->_activePlugin(),
            'provinces' => Province::all(),
            'apiUrl' => $this->_apiUrl()
        ];

        return view('merchant::toko.master-data.supplier.form-nopopup',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);

            $id=$request->id;
            if(!is_null($id)){

                $data=SupplierEntity::find($id);
            }else{
                $data=new SupplierEntity();
                $data->md_user_id=user_id();
            }

            if(isset($request->is_google_contact)){
                if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                    $emp = EnableMerchantplugin::find($this->_activePlugin()->id);
                    $quota = intval($emp->quota_used) + 1;
                    if($quota > $emp->quota_available){
                        return response()->json([
                            'message'=>'Kuota anda tidak cukup, data gagal disimpan !',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>''
                        ]);
                    }
                }
            }

            $data->code=$request->code;
            $data->name=$request->name;
            $data->email=$request->email;
            $data->phone_number=$request->phone_number;
            $data->address=$request->address;
            $data->type_address = $request->type_address;
            $data->city_name = $request->city_name;
            $data->region_name = $request->region_name;
            $data->postal_code = $request->postal_code;
            $data->gender = $request->gender;
            $data->sc_supplier_categories_id=$request->sc_supplier_categories_id;

            if(isset($request->is_google_contact)){
                $data->etag = $request->etag;
                $data->resource_name = $request->resource_name;
                $data->is_google_contact = 1;
            }

            $data->save();

            if(isset($request->is_google_contact)){
                if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                    $emp = EnableMerchantplugin::find($this->_activePlugin()->id);
                    $quota = intval($emp->quota_used) + 1;
                    $emp->quota_used = $quota;
                    $emp->save();
                }
            }

            DB::commit();

            return response()->json([
                'message'=>'Supplier atau pemasuk berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }


    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=SupplierEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            if($data->is_google_contact == 1){
                if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                    $emp = EnableMerchantplugin::find($this->_activePlugin()->id);
                    if($emp->quota_used != 0){
                        $quota = intval($emp->quota_used) - 1;
                        $emp->quota_used = $quota;
                        $emp->save();
                    }
                }
            }

            DB::commit();

            return response()->json([
                'message'=>'Supplier atau pemasuk berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $data = SupplierEntity::getDataForDataTable();
            if($request->search){
                $search = $request->search;
                $data->where(function($query) use($search){
                    $query->whereRaw("lower(sc_suppliers.code) like '%".strtolower($search)."%' ")
                        ->orWhereRaw("lower(sc_suppliers.name) like '%".strtolower($search)."%'")
                        ->orWhereRaw("lower(sc_suppliers.email) like '%".strtolower($search)."%'")
                        ->orWhereRaw("lower(sc_suppliers.phone_number) like '%".strtolower($search)."%'")
                        ->orWhereRaw("lower(sc_suppliers.address) like '%".strtolower($search)."%'")
                        ->orWhereRaw("lower(c.name) like '%".strtolower($search)."%'");
                });
            }

            if($request->sc_supplier_categories_id != -1){
                $data->where('c.id', $request->sc_supplier_categories_id);
            }

            $data->whereIn('m.id',MerchantUtil::getBranch(merchant_id()))->where('sc_suppliers.is_deleted',0);

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Data Supplier");
            if(merchant_detail()->is_branch==0)
            {
                $header = [
                    'No',
                    'Kode Supplier',
                    'Nama Supplier',
                    'Email',
                    'No Telp',
                    'Alamat',
                    'Kategori Supplier',
                    'Dibuat oleh Cabang'
                ];
            }else{
                $header = [
                    'No',
                    'Kode Supplier',
                    'Nama Supplier',
                    'Email',
                    'No Telp',
                    'Alamat',
                    'Kategori Supplier'
                ];
            }

            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;

            foreach ($data->get() as $key => $item) {
                if(merchant_detail()->is_branch==0)
                {
                    $dataShow = [
                        $key+1,
                        $item->kode,
                        $item->nama_supplier,
                        $item->email,
                        $item->no_telp,
                        $item->alamat,
                        $item->kategori_supplier,
                        $item->dibuat_oleh_cabang
                    ];
                }else{
                    $dataShow = [
                        $key+1,
                        $item->kode,
                        $item->nama_supplier,
                        $item->email,
                        $item->no_telp,
                        $item->alamat,
                        $item->kategori_supplier
                    ];
                }


                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-supplier_' . merchant_id()."_".date('Y-m-d-h-i-s') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL';</script>
            <script>
                            toastForSaveData('Data supplier berhasil diexport!','success',true,'')
                let success = document.querySelector('#successExport');
                if(success){
                    setTimeout(() => {
                        closeModal();
                    }, 3000)
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<script>toastForSaveData('Terjadi kesalahan, Data supplier gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)

                    </script>";
        }

    }

    // begin syncronisaztion google contact
    public function contactSyncCompare(Request $request)
    {
        try {
            $contactCustomer = CustomerEntity::where('is_deleted', 0)
                                        ->where('md_user_id', user_id())
                                        ->where('is_google_contact',1)
                                        ->get();

            return response()->json(['data'=> $contactCustomer]);

        } catch(\Exception $e)
        {
            return response()->json(['data'=> []]);
        }
    }

    public function contactSyncForm(Request $request)
    {
        try {
            $userId = user_id();
            $data = SupplierEntity::where('md_user_id', $userId)
                            ->where('is_google_contact', 1)
                            ->where('is_deleted', 0)
                            ->get();


            $params = [
                "title" => "Sinkronisasi Google Contact",
                "data" => $data,
                "activePlugin" => $this->_activePlugin(),
                "apiUrl" => $this->_apiUrl()
            ];

            return view('merchant::toko.master-data.supplier.sync.form', $params);

        } catch(\Exception $e)
        {
            abort(404);
        }
    }

    public function contactSyncSave(Request $request)
    {
        try {
            DB::beginTransaction();
            $userId = user_id();
            $plugin = $this->_activePlugin();
            $emp = EnableMerchantplugin::find($this->_activePlugin()->id);

            if($plugin)
            {
                if($plugin->status == 0){
                    if(strtolower($plugin->payment_type) == "quota"){
                        return response()->json([
                            'message' => "Kuota tersedia tidak mencukupi, silahkan lakukan pembelian plugin untuk dapat menggunakan fitur !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    } else {
                        return response()->json([
                            'message' => "Masa berlangganan plugin anda sudah habis, silahkan lakukan pembelian plugin untuk dapat menggunakan fitur !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }
            }


            $dataFromDb = SupplierEntity::where('md_user_id', $userId)
                            ->where('is_google_contact', 1)
                            ->where('is_deleted', 0)
                            ->get()
                            ->toArray();
            $customers = CustomerEntity::where('md_user_id', $userId)
                            ->where('is_google_contact', 1)
                            ->where('is_deleted', 0)
                            ->get()
                            ->toArray();

            $requestContacts = json_decode($request->contacts, true);
            // filter request contact dengan data supplier yang sudah ada di database
            $resultContact = array_filter($requestContacts, function($item) use($requestContacts, $dataFromDb){
                return array_search($item['resource_name'], array_column($dataFromDb, 'resource_name')) === false;
            });

            // filter request contact dengan data customer yang sudah ada di database
            $result = array_filter($resultContact, function($item) use($resultContact, $customers){
                return array_search($item['resource_name'], array_column($customers, 'resource_name')) === false;
            });

            $data = [];

            foreach($result as $key => $item){
                $data[] = [
                    "name" => $item["name"],
                    "email" => $item["email"],
                    "phone_number" => $item["phone_number"],
                    "address" => $item["address"],
                    "md_user_id" => $userId,
                    "etag" => $item["etag"],
                    "is_google_contact" => 1,
                    "type_address" => $item['type_address'],
                    "city_name" => $item['city_name'],
                    "region_name" => $item['region_name'],
                    "postal_code" => $item['postal_code'],
                    "gender" => $item['genders'],
                    "resource_name" => $item["resource_name"],
                    "created_at" => date('Y-m-d h:i:s'),
                    "updated_at" => date('Y-m-d h:i:s')
                ];
            }

            // cek plugin adalah kuota
            // jika quota yang terpakai lebih dari quota yang tersedia, gagal menyimpan data
            if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                $quota = intval(count($result)) + intval($emp->quota_used);
                if($quota > intval($emp->quota_available)){
                    return response()->json([
                        'message' => "Kuota anda tidak cukup, data gagal disimpan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }

            SupplierEntity::insert($data);

            // menambah quota terpakai di enable_merchant_plugin
            if(strtolower($this->_activePlugin()->payment_type) == 'quota'){
                $quota = intval(count($result)) + intval($emp->quota_used);
                $emp->quota_used = $quota;
                $emp->save();
            }

            DB::commit();

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        } catch(\Exception $e)
        {
            DB::rollback();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function contactSyncUpdateForm(Request $request){

        $id=$request->id;

        $data = SupplierEntity::where('id', $id)
                    ->where('is_google_contact', 1)
                    ->first();

        $params=[
            'title'=>'Edit Pelanggan',
            'data'=>$data,
            'category'=>SupplierCategories::
            select([
                'sc_supplier_categories.id',
                'sc_supplier_categories.name',
                'sc_supplier_categories.description'
            ])
                ->join('md_users as u','u.id','sc_supplier_categories.md_user_id')
                ->join('md_merchants as m','m.md_user_id','u.id')
                ->whereIn('m.id',MerchantUtil::getBranch(merchant_id()))
                ->orderBy('sc_supplier_categories.id','asc')->get(),
            'provinces' => Province::all(),
            'activePlugin' => $this->_activePlugin(),
            'apiUrl' => $this->_apiUrl()
        ];

        return view('merchant::toko.master-data.supplier.sync.form-update',$params);
    }

    public function contactSyncUpdateSave(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $data = SupplierEntity::where('id', $id)
                    ->where('is_google_contact', 1)
                    ->where('is_deleted', 0)
                    ->first();

            $data->code=$request->code;
            $data->name=$request->name;
            $data->md_user_id=user_id();
            $data->email=$request->email;
            $data->phone_number=$request->phone_number;
            $data->address=$request->address;
            $data->etag = $request->etag;
            $data->resource_name = $request->resource_name;
            $data->type_address = $request->type_address;
            $data->city_name = $request->city_name;
            $data->region_name = $request->region_name;
            $data->postal_code = $request->postal_code;
            $data->gender = $request->gender;
            $data->sc_supplier_categories_id=$request->sc_supplier_categories_id;

            $data->save();

            return response()->json([
                'message'=>'Data berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch(\Exception $e)
        {
            DB::rollback();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function getCity(Request $request){
        try{
            $id = $request->id;
            $data = City::where('md_province_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\Customer;
use App\Models\MasterData\Merchant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\CustLevelEntity;
use Modules\Merchant\Entities\Toko\SpecialPriceEntity;
use App\Utils\Merchant\MerchantUtil;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;


class SpecialPriceController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/special-price')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.special-price.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.special-price.datatable');
                Route::get('/add', [static::class, 'add'])
                    ->name('merchant.toko.special-price.add');
                Route::get('/edit', [static::class, 'edit'])
                    ->name('merchant.toko.special-price.edit');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.special-price.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.special-price.delete');
                Route::post('/reload-data', [static::class, 'reloadData'])
                    ->name('merchant.toko.special-price.reload-data');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Harga Khusus',
            'tableColumns'=>SpecialPriceEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'add'=>(get_role()==3)?SpecialPriceEntity::add(true):
                SpecialPriceEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.special-price.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return SpecialPriceEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public function add(Request $request){
        $id=$request->id;
        $userId = user_id();
        if(!is_null($id)){
            $data=SpecialPriceEntity::find($id);
        }else{
            $data=new SpecialPriceEntity();
        }


        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'categoryCustomer' => CustLevelEntity::listOption()
        ];

        return view('merchant::toko.master-data.special-price.form',$params);
    }

    public  function edit(Request $request){
        $id=$request->id;
        $userId = user_id();
        $data=SpecialPriceEntity::find($id);

        $detailProductSpecialPrice = collect(DB::select("
        select
            sppd.id,
            sppd.sc_product_id,
            sp.name,
            sp.code,
            sppd.special_price,
            m.id as md_merchant_id,
            sppd.is_deleted
        from
            sc_product_special_price_details sppd
        join
            sc_product_special_prices spp on sppd.sc_product_special_price_id = spp.id
        join
            sc_products sp on sp.id = sppd.sc_product_id
        join
            md_users u on u.id = sp.md_user_id
        join
            md_merchants m on m.md_user_id = u.id
        where
            spp.id = $id
            and
            sppd.is_deleted = 0
        "));

        $assignToBranch = (!json_decode($data->assign_to_branch))? []:json_decode($data->assign_to_branch);

        $detailProductIdDb = $detailProductSpecialPrice->filter(function($item){
            return $item->md_merchant_id == merchant_id();
        })->values();

        $params=[
            'title'=>(is_null($id))?'Tambah Harga Khusus':'Edit Data',
            'data'=>$data,
            'categoryCustomer' => CustLevelEntity::listOption(),
            'assign_to_branch' => $assignToBranch,
            'details' => $detailProductSpecialPrice,
            'detailProductIdDb' => $detailProductIdDb
        ];

        return view('merchant::toko.master-data.special-price.form-edit',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $id = $request->id;
            $merchantId = merchant_id();
            $userId = user_id();
            $startDate = $request->start_date;
            $endDate = $request->end_date;

            if(is_null($request->start_date)){
                return response()->json([
                    'message'=>'Tanggal mulai belum di isi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->end_date<$request->start_date){
                return response()->json([
                    'message'=>'Tanggal berakhir kurang dari tanggal mulai',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->is_daily == 0){
                if($request->end_hour<$request->start_hour){
                    return response()->json([
                        'message'=>'Jam berakhir kurang dari jam mulai',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            if(is_null($id)){
                $data = new SpecialPriceEntity();
                $data->md_merchant_id = $merchantId;
            } else {
                $data = SpecialPriceEntity::find($id);
            }

            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message'=>$error,
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->is_all_branch == 1){
                $tmpBranch = get_cabang();
                $branch = [];
                foreach($tmpBranch as $k => $i){
                    if($i->id != $merchantId){
                        array_push($branch, $i->id);
                    }
                }
            } else {
                $branch = json_decode($request->assign_to_branch);
            }

            if(count(json_decode($request->day)) == 0){
                return response()->json([
                    'message'=>'Silahkan pilih minimal 1 hari untuk harga khusus',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $reqDetailProduct = json_decode($request->detail_product);
            if(count($reqDetailProduct) < 1){
                return response()->json([
                    'message'=>'Silahkan pilih produk terlebih dahulu',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $queryMerchantId = $branch;
            array_unshift($queryMerchantId, merchant_id());

            $allMerchant = Merchant::whereIn("id",$queryMerchantId)
                                ->get();

            $productCode = Product::whereIn("id", array_column($reqDetailProduct, "sc_product_id"))
                                    ->where("is_deleted", 0)
                                    ->get();

            $allProduct = Product::select([
                "sc_products.id",
                "sc_products.name as name",
                "sc_products.code as code",
                "m.id as md_merchant_id"
            ])
            ->join("md_users as u", "u.id", "sc_products.md_user_id")
            ->join("md_merchants as m", "m.md_user_id", "u.id")
            ->whereIn("code", $productCode->pluck("code")->toArray())
            ->whereIn("m.id", $allMerchant->pluck("id")->toArray())
            ->where("sc_products.is_deleted", 0)
            ->get();

            foreach($queryMerchantId as $branchId){
                $currentMerchant = $allMerchant->filter(function($item) use($branchId){
                    return $item->id == $branchId;
                })->first();
                $productByMerchant = $allProduct->filter(function($item) use($branchId){
                    return $item->md_merchant_id == $branchId;
                });
                foreach($allProduct as $key => $item){
                    if(!$productByMerchant->pluck('code')->contains($item->code)){
                        return response()->json([
                            'message'=>"Produk $item->name tidak tersedia di cabang $currentMerchant->name",
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                }
            }

            $dayName=[];
            foreach(json_decode($request->day) as $item){
                $dayName[]=["hari"=>$item->day_name];
            }

            $checkDataActive = SpecialPriceEntity::select([
                "sppd.sc_product_special_price_id",
                "sppd.sc_product_id",
                "sc_product_special_prices.days",
                "m.id as md_merchant_id",
                "m.name as merchant_name",
                "sp.name as product_name"
            ])
            ->join("sc_product_special_price_details as sppd", "sppd.sc_product_special_price_id", "sc_product_special_prices.id")
            ->join("sc_products as sp", "sp.id", "sppd.sc_product_id")
            ->join("md_merchants as m", "m.id", "sppd.md_merchant_id")
            ->where("sc_product_special_prices.id", "!=", $id)
            ->where("sppd.sc_product_special_price_id", "!=", $id)
            ->whereIn("sppd.md_merchant_id", $allMerchant->pluck('id'))
            ->whereIn("sp.code", $allProduct->pluck('code'))
            ->where(function($q) use($startDate,$endDate){
                $q->where([['sc_product_special_prices.start_date','<=',$startDate],
                            ['sc_product_special_prices.end_date','>=',$startDate]])
                    ->orWhere([['sc_product_special_prices.start_date','>=',$startDate],
                                ['sc_product_special_prices.start_date','<=',$endDate]]);
            })
            ->where("sc_product_special_prices.is_deleted", 0)
            ->where("sppd.is_deleted", 0)
            ->where("sc_product_special_prices.is_active", 1)
            ->get();

            if(count($checkDataActive) > 0 && $request->is_active == 1){
                foreach($checkDataActive as $key => $item){
                    $dayFromDb =  array_column(json_decode($item->days), 'hari');
                    foreach($dayName as $k => $i){
                        if(in_array($i["hari"], $dayFromDb)){
                            return response()->json([
                                'message'=>"Harga khusus untuk produk $item->product_name sudah ada untuk jangka waktu yang dipilih pada hari ".$i['hari'],
                                'type'=>'warning',
                                'is_modal'=>true,
                                'redirect_url'=>''
                            ]);
                        }
                    }
                }


            }

            $checkName = SpecialPriceEntity::whereRaw("lower(name)='".strtolower(str_replace("'","",$request->name))."'")
                                            ->where("id", "!=", $id)
                                            ->where("md_merchant_id", merchant_id())
                                            ->where("is_deleted", 0)
                                            ->first();
            if(!is_null($checkName)){
                return response()->json([
                    'message'=>"Judul yang anda masukkan sudah digunakan",
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $data->name = $request->name;
            $data->start_date = $request->start_date;
            $data->end_date = $request->end_date;
            $data->is_daily = $request->is_daily;
            $data->days = json_encode($dayName);
            $data->is_all_branch = $request->is_all_branch;
            $data->is_active = $request->is_active;

            if($request->is_daily == 1)
            {
                $data->start_hour = '00:00';
                $data->end_hour = '23:59';
            } else {
                $data->start_hour = $request->start_hour;
                $data->end_hour = $request->end_hour;
            }

            if(count($branch) > 0){
                $data->assign_to_branch = json_encode($branch);
            } else {
                $data->assign_to_branch = null;
            }

            $data->save();

            if(is_null($id)){
                $details = [];
                foreach($allProduct as $key => $item){
                    foreach($reqDetailProduct as $k => $i){
                        if($item->code == $i->product_code){
                            $specialPrice = strtr($i->special_price, array('.' => '', ',' => '.'));
                            if($specialPrice <= 0){
                                return response()->json([
                                    'message'=>"Masukkan harga khusus untuk $item->name",
                                    'type'=>'warning',
                                    'is_modal'=>true,
                                    'redirect_url'=>''
                                ]);
                            }

                            $details[] = [
                                "sc_product_special_price_id" => $data->id,
                                "sc_product_id" => $item->id,
                                "special_price" => $specialPrice,
                                "md_merchant_id" => $item->md_merchant_id,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];
                        }
                    }
                }

                DB::table('sc_product_special_price_details')
                        ->insert($details);
            } else {

                $detailProductFromDB = SpecialPriceEntity::select([
                    "sppd.id",
                    "sppd.sc_product_id",
                    "sppd.md_merchant_id",
                    "sppd.special_price",
                    "sppd.is_deleted"
                ])
                ->join("sc_product_special_price_details as sppd", "sppd.sc_product_special_price_id", "sc_product_special_prices.id")
                ->where("sppd.sc_product_special_price_id", $id)
                ->get();

                DB::table("sc_product_special_price_details")
                    ->whereIn("id", $detailProductFromDB->pluck("id"))
                    ->update(["is_deleted" => 1]);

                $details = [];
                foreach($allProduct as $key => $item){
                    foreach($reqDetailProduct as $k => $i){
                        if($item->code == $i->product_code){
                            $specialPrice = strtr($i->special_price, array('.' => '', ',' => '.'));
                            if($specialPrice <= 0){
                                return response()->json([
                                    'message'=>"Masukkan harga khusus untuk $item->name",
                                    'type'=>'warning',
                                    'is_modal'=>true,
                                    'redirect_url'=>''
                                ]);
                            }

                            $details[] = [
                                "sc_product_special_price_id" => $data->id,
                                "sc_product_id" => $item->id,
                                "special_price" => $specialPrice,
                                "md_merchant_id" => $item->md_merchant_id,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];
                        }
                    }
                }

                DB::table('sc_product_special_price_details')
                        ->insert($details);
            }


            DB::commit();
            return response()->json([
                'message'=>'Harga khusus berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.special-price.index')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=SpecialPriceEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            DB::table("sc_product_special_price_details")
                ->where("sc_product_special_price_id", $data->id)
                ->update(["is_deleted" => 1]);

            DB::commit();
            return response()->json([
                'message'=>'Data berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }



    public function reloadData(Request $request)
    {

        $searchParams = [];
        foreach (SpecialPriceEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }

        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Kupon Diskon',
            'searchKey' => $searchKey,
            'tableColumns'=>SpecialPriceEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.master-data.special-price.list',$params);

    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Attendance\MerchantStaffAttendance;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\PermitCategory;
use App\Models\MasterData\Merchant;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Modules\Merchant\Entities\Toko\PermitStaffEntity;
use Modules\Merchant\Entities\HR\LeaveSettingEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use PDF;

class PermitStaffController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('permit-staff')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/data', 'Toko\PermitStaffController@data')
                    ->name('merchant.toko.permit-staff.data');
                Route::get('/add', 'Toko\PermitStaffController@add')
                    ->name('merchant.toko.permit-staff.add');
                Route::post('/save', 'Toko\PermitStaffController@save')
                    ->name('merchant.toko.permit-staff.save');
                Route::post('/data-table', 'Toko\PermitStaffController@dataTable')
                    ->name('merchant.toko.permit-staff.datatable');
                Route::post('/reload-data', 'Toko\PermitStaffController@reloadData')
                    ->name('merchant.toko.permit-staff.reload-data');
                Route::get('/cetak-pdf', 'Toko\PermitStaffController@cetakPDF')
                    ->name('merchant.toko.permit-staff.cetakPDF');
                Route::post('/detail/{id}', 'Toko\PermitStaffController@detail')
                    ->name('merchant.toko.permit-staff.detail');
                Route::get('/approve/{id}', 'Toko\PermitStaffController@approve')
                    ->name('merchant.toko.permit-staff.approve');
                Route::get('/canceled/{id}', 'Toko\PermitStaffController@canceled')
                    ->name('merchant.toko.permit-staff.canceled');
                Route::post('/get-staff', 'Toko\PermitStaffController@getStaff')
                    ->name('merchant.toko.permit-staff.get-staff');
            });
    }

    public function getStaff(Request $request)
    {
        try {
            $merchantId = $request->md_merchant_id;
            $data=MerchantStaff::
            select([
                'u.id',
                DB::raw("u.fullname || '-' || coalesce(jp.name, '') as text")
            ])
                ->where(['md_merchant_staff.md_merchant_id'=>$merchantId])
                ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->leftJoin('hr_merchant_job_positions as jp','jp.id','md_merchant_staff.job_position_id')
                ->where('md_merchant_staff.is_non_employee',0)
                ->where('md_merchant_staff.is_deleted',0)
                ->get();
            return response()->json($data);

        } catch(\Exception $e)
        {
            return [];
        }
    }

    public function data()
    {
        $params=[
            'title'=>'Izin/Cuti',
            'tableColumns'=>PermitStaffEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'add'=>PermitStaffEntity::add(true)
        ];

        return view('merchant::toko.permit-staff.data',$params);

    }

    public function add(Request $request)
    {
        $now = Carbon::now();
        $data = new PermitStaffEntity();
        $params = [
            'title' => 'Tambah Izin/Cuti',
            'now' => $now,
            'permit' => PermitCategory::all(),
            'leaveOption' => LeaveSettingEntity::listOption(),
            'data' => $data
        ];

        return view('merchant::toko.permit-staff.form', $params);
    }

    public function detail($id)
    {
        try{
            if(is_null($id)){
                return abort(404);
            }
            $data = collect(DB::select("
            select
                mmsps.id,
                mu.fullname,
                mmsps.start_date,
                mmsps.end_date,
                mmsps.type_of_attendance,
                mmsps.supporting_file,
                mmsps.desc,
                mmsps.is_approved,
                mmsps.is_canceled,
                mpc.name as permit_category_name,
                hmls.name as hr_category_name
            from
                md_merchant_staff_permit_submissions mmsps
            join
                md_users mu on mu.id = mmsps.md_staff_user_id
            left join
                md_permit_categories mpc on mpc.id = mmsps.md_permit_category_id
            left join
                hr_merchant_leave_settings hmls on hmls.id = mmsps.hr_leave_id
            where
                mmsps.id = $id
            "))->first();

            if(is_null($data)){
                return abort(404);
            }

            $params=[
                'title'=>'Detail Izin/Cuti',
                'data'=>$data
            ];
            return view('merchant::toko.permit-staff.detail',$params);

        }catch (\Exception $e)
        {
            return view('pages.500');
        }

    }


    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $merchantId = $request->md_merchant_id;
            $merchant = Merchant::find($merchantId);
            if(is_null($merchant)){
                return response()->json([
                    'message'=>'Cabang belum dipilih !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $userId = $merchant->md_user_id;
            $startDate = $request->start_date;
            $endDate = $request->end_date;
            $merchantStaff = MerchantStaff::where('md_user_id', $request->md_staff_user_id)->first();

            $hrLeaveId = $request->hr_leave_id;

            $date=Carbon::parse($request->start_date);

            if($endDate < $startDate){
                return response()->json([
                    'message'=>'Tanggal mulai izin lebih dari tanggal selesai izin !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $id = $request->id;
            if(is_null($id)){
                $data=new PermitStaffEntity();
            } else {
                $data =  PermitStaffEntity::find($id);
            }

            $checkAttendance=MerchantStaffAttendance::
            select([
                'md_merchant_staff_attendances.*',
                'u.fullname',
                'r.name as role_name'
            ])
                ->where([
                    'md_merchant_staff_attendances.md_staff_user_id'=>$request->md_staff_user_id
                ])
                ->whereRaw("md_merchant_staff_attendances.created_at::date between '$startDate' and '$endDate'")
                ->where('md_merchant_staff_attendances.is_deleted',0)
                ->where('md_merchant_staff_attendances.permit_id', '!=', $id)
                ->join('md_users as u','u.id','md_merchant_staff_attendances.md_staff_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->first();


            if($checkAttendance)
            {
                if($checkAttendance->is_permit == 1){
                    return response()->json([
                        'message'=>"Mohon maaf,karyawan ini telah melakukan permintaan izin pada tanggal ".$date->isoFormat('D MMMM Y')."!",
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false
                    ]);
                } else {
                    return response()->json([
                        'message'=>"Mohon maaf,karyawan ini terdaftar masuk pada tanggal ".$checkAttendance->created_at->isoFormat('D MMMM Y')."!",
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false
                    ]);
                }
            }
            $check=PermitStaffEntity::where('md_staff_user_id',$request->md_staff_user_id)
                ->where(function($q) use($startDate,$endDate, $date){
                    $q->where([['start_date','<=',$startDate],
                        ['end_date','>=',$startDate]])
                        ->orWhere([['start_date','>=',$startDate],
                            ['start_date','<=',$endDate]])
                        ->orWhereDate('end_date', '=', $date);
                })
                ->where('id', '!=', $id)
                ->where('is_canceled', 0)
                ->where('is_approved', 0)
                ->first();

            if($check)
            {
                return response()->json([
                    'message'=>"Mohon maaf,karyawan ini telah melakukan permintaan izin pada tanggal ".$date->isoFormat('D MMMM Y')."!",
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }

            $presensiSetting=DB::table('hr_merchant_attendance_settings')
                ->select([
                    'is_count_leave_from_date_joining',
                    'is_use_rule_leave'
                ])
                ->where('md_merchant_id',MerchantUtil::getHeadBranch($merchantId))->first();


            $isActiveLeave = (is_null($presensiSetting))? []:$presensiSetting;
            $leaveSetting = LeaveSettingEntity::find($hrLeaveId);

            if(!empty($isActiveLeave)){
                if($isActiveLeave->is_use_rule_leave==1 && $isActiveLeave->is_count_leave_from_date_joining==1){
                    if($leaveSetting->is_for_employee_over_one_year == 1){
                        $joinDate = $merchantStaff->join_date;
                        if(!is_null($joinDate)){
                            $oneYear = Carbon::now()->subYears(1);
                            if($joinDate > $oneYear){
                                return response()->json([
                                    'message'=>"Tidak dapat menambah izin / cuti untuk karyawan yang bergabung kurang dari 1 tahun !",
                                    'type'=>'warning',
                                    'is_modal'=>true,
                                    'redirect_url'=>'',
                                    'is_with_datatable'=>false
                                ]);
                            }
                        }

                    }

                }
            }

            $dateRange = CarbonPeriod::create($startDate, $endDate);

            if($leaveSetting->is_per_month == 0){
                $startDateRange = Carbon::now()->startOfYear()->toDateString();
                $endDateRange = Carbon::now()->lastOfYear()->toDateString();
            } else {
                $startDateRange = Carbon::now()->startOfMonth()->toDateString();
                $endDateRange = Carbon::now()->lastOfMonth()->toDateString();
            }



            if(!empty($isActiveLeave)){
                if($isActiveLeave->is_use_rule_leave==1){
                    $checkLeave = collect(DB::select("
            select
                count(total) as total
            from
                hr_merchant_staff_amount_leave hmsal
            where
                hmsal.leave_id = $hrLeaveId
                and
                hmsal.staff_user_id = ".$request->md_staff_user_id."
                and
                hmsal.is_approve = 1
                and
                hmsal.md_merchant_id = $merchantId
                and
                hmsal.is_deleted = 0
                and
                hmsal.date between '$startDateRange' and '$endDateRange'
            "))->first();

                    if(($checkLeave->total + count($dateRange)) > $leaveSetting->max_day){
                        return response()->json([
                            'message'=>"Izin / cuti melebihi batas yang ditentukan !",
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);
                    }
                }
            }


            $destinationPath = 'public/uploads/merchant/'.$merchantId.'/attendance/'.date('YmdHis').'/';

            if($request->hasFile('support_file'))
            {
                $file=$request->file('support_file');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                if(is_null($id)){
                    $fileName=null;
                } else {
                    $fileName=$data->supporting_file;
                }
            }

            if(is_null($fileName)){
                return response()->json([
                    'message'=>"Dokumen pendukung belum diisi !",
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $data->md_staff_user_id=$request->md_staff_user_id;
            $data->md_merchant_id=$merchantId;
            $data->supporting_file=$fileName;
            $data->is_approved=1;
            $data->start_date=$request->start_date;
            $data->end_date=$request->end_date;
            $data->created_at=$request->start_date;
            $data->updated_at=$request->start_date;
            $data->md_user_id_approved=$userId;
            $data->type_of_attendance=$leaveSetting->type_of_attendance;
            $data->hr_leave_id = $hrLeaveId;
            $data->desc = $request->note;
            $data->timezone = $request->_timezone;
            $data->save();

            $insertAttendance = [];
            $insertAmountLeave = [];

            $type = ($leaveSetting->is_per_month == 0)? 'YEAR':'MONTH';

            foreach($dateRange as $dr)
            {
                array_push($insertAttendance, [
                    "md_merchant_id" => $merchantId,
                    "md_staff_user_id" => $request->md_staff_user_id,
                    "start_longitude" => "null",
                    "start_latitude" => "null",
                    "type_of_attendance" => $leaveSetting->type_of_attendance,
                    "is_permit" => 1,
                    "created_at" => $dr->format('Y-m-d H:i:s'),
                    "updated_at" => $dr->format('Y-m-d H:i:s'),
                    "permit_id" => $data->id
                ]);

                array_push($insertAmountLeave, [
                    "leave_id" => $leaveSetting->id,
                    "staff_id" => $merchantStaff->id,
                    "staff_user_id" => $request->md_staff_user_id,
                    "md_merchant_id" => $merchantId,
                    "type_of_attendance" => $leaveSetting->type_of_attendance,
                    "permit_id" => $data->id,
                    "date" => $dr->format('Y-m-d'),
                    "type" => $type,
                    "is_approve" => 1,
                    "timezone" => $request->_timezone,
                    "created_at" => $dr->format('Y-m-d H:i:s'),
                    "updated_at" => $dr->format('Y-m-d H:i:s')

                ]);
            }

            if(is_null($id)){
                DB::table('md_merchant_staff_attendances')
                    ->insert($insertAttendance);

                DB::table('hr_merchant_staff_amount_leave')
                    ->insert($insertAmountLeave);
            }

            DB::commit();

            return response()->json([
                'message'=>'Izin berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.permit-staff.data'),
                'is_with_datatable'=>false
            ]);


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false
            ]);

        }

    }

    public function approve($id)
    {
        try{
            DB::beginTransaction();
            $data=PermitStaffEntity::find($id);

            if(is_null($data)){
                return view('pages.500');
            }

            if($data->is_approved == 1){
                return redirect()->route('merchant.toko.permit-staff.data');
            }

            $data->is_approved=1;
            $data->md_user_id_approved=user_id();
            $data->save();

            $dateRange = CarbonPeriod::create($data->start_date, $data->end_date);

            $insertAttendance = [];
            $insertAmountLeave = [];

            $merchantStaff = MerchantStaff::where('md_user_id',$data->md_staff_user_id)->first();

            foreach($dateRange as $dr){
                array_push($insertAttendance, [
                    "md_merchant_id" => $data->md_merchant_id,
                    "md_staff_user_id" => $data->md_staff_user_id,
                    "start_longitude" => "null",
                    "start_latitude" => "null",
                    "type_of_attendance" => $data->type_of_attendance,
                    "is_permit" => 1,
                    "created_at" => $dr->format('Y-m-d H:i:s'),
                    "updated_at" => $dr->format('Y-m-d H:i:s'),
                    "permit_id" => $data->id
                ]);
            }

            DB::table('md_merchant_staff_attendances')
                ->insert($insertAttendance);

            if(is_null($data->md_permit_category_id)){
                $leaveSetting = LeaveSettingEntity::find($data->hr_leave_id);
                $type = ($leaveSetting->is_per_month == 0)? 'YEAR':'MONTH';
                foreach($dateRange as $dr){
                    array_push($insertAmountLeave, [
                        "leave_id" => $data->hr_leave_id,
                        "staff_id" => $merchantStaff->id,
                        "staff_user_id" => $data->md_staff_user_id,
                        "md_merchant_id" => $data->md_merchant_id,
                        "type_of_attendance" => $data->type_of_attendance,
                        "permit_id" => $data->id,
                        "date" => $dr->format('Y-m-d'),
                        "type" => $type,
                        "is_approve"=>1,
                        "timezone" => $data->timezone,
                        "created_at" => $dr->format('Y-m-d H:i:s'),
                        "updated_at" => $dr->format('Y-m-d H:i:s')

                    ]);
                }

                DB::table('hr_merchant_staff_amount_leave')
                    ->insert($insertAmountLeave);
            }

            DB::commit();
            return redirect()->route('merchant.toko.permit-staff.data');

        }catch (\Exception $e)
        {
            DB::rollBack();
            return view('pages.500');
        }

    }

    // ini reject ya
    public function canceled($id)
    {
        try{
            DB::beginTransaction();
            $data=PermitStaffEntity::find($id);
            $data->is_approved=2;
            $data->md_user_id_approved=user_id();
            $data->save();

            DB::commit();
            return redirect()->route('merchant.toko.permit-staff.data');

        }catch (\Exception $e)
        {
            DB::rollBack();
            return view('pages.500');
        }

    }




    public function dataTable(Request $request)
    {
        return PermitStaffEntity::dataTable($request);
    }


    public function reloadData(Request $request)
    {

        $searchParams = [];
        foreach (PermitStaffEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Izin/Cuti',
            'searchKey' => $searchKey,
            'tableColumns'=>PermitStaffEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.permit-staff.list',$params);

    }


}

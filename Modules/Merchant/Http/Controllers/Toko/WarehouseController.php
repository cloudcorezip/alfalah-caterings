<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\SennaToko\StockInventory;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CashDrawerEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantWarehouse;
use Modules\Merchant\Entities\Toko\WarehouseTypeEntity;

class WarehouseController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/warehouse')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\WarehouseController@index')
                    ->name('merchant.toko.warehouse.index');
                Route::post('/data-table', 'Toko\WarehouseController@dataTable')
                    ->name('merchant.toko.warehouse.datatable');
                Route::post('/add', 'Toko\WarehouseController@add')
                    ->name('merchant.toko.warehouse.add');
                Route::post('/save', 'Toko\WarehouseController@save')
                    ->name('merchant.toko.warehouse.save');
                Route::post('/delete', 'Toko\WarehouseController@delete')
                    ->name('merchant.toko.warehouse.delete');
            });
    }

    public function index(Request $request)
    {
        //parent::generateWarehouse(merchant_id());
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Gudang',
            'tableColumns'=>WarehouseEntity::dataTableColumns(),
            'add'=>(get_role()==3)?WarehouseEntity::add(true):
                WarehouseEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.warehouse.index',$params);

    }


    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return WarehouseEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=WarehouseEntity::find($id);
        }else{
            $data=new WarehouseEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'typeOption'=>WarehouseTypeEntity::listOption()
        ];



        return view('merchant::toko.master-data.warehouse.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);

            if($request->md_merchant_id==-1 || $request->md_merchant_id=='-1')
            {
                $isToAll=1;
                $merchantId=merchant_id();
            }else{
                $isToAll=0;
                $merchantId=$request->md_merchant_id;
            }

            $checkPackageSubscription=MerchantUtil::checkUrl($merchantId);

            if($checkPackageSubscription['status']==true)
            {
                return response()->json([
                    'message'=>$checkPackageSubscription['message'],
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($request->id)){
                $data=new WarehouseEntity();
                $data->name=$request->name;
                $data->md_merchant_id=$merchantId;
                $data->is_default=0;
                $data->is_to_all=$isToAll;
                $data->type_id=$request->type_id;
                $data->desc=$request->desc;
                $data->save();
                return response()->json([
                    'message'=>'Data gudang berhasil disimpan',
                    'type'=>'success',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }else{
                $data=WarehouseEntity::find($request->id);
                $check=StockInventory::where('inv_warehouse_id',$request->id)->first();
                if(!is_null($check))
                {
                    if($merchantId!=$data->md_merchant_id){
                        return response()->json([
                            'message'=>'Gudang telah digunakan untuk penyimpanan,tidak dapat diubah ',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }else{
                        $data->name=$request->name;
                        if($data->is_default==0)
                        {
                            $data->md_merchant_id=$merchantId;

                        }
                        $data->is_to_all=$isToAll;
                        $data->type_id=$request->type_id;
                        $data->desc=$request->desc;
                        $data->save();
                        return response()->json([
                            'message'=>'Data gudang berhasil disimpan',
                            'type'=>'success',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                }else{
                    $data->name=$request->name;
                    if($data->is_default==0)
                    {
                        $data->md_merchant_id=$merchantId;

                    }
                    $data->is_to_all=$isToAll;
                    $data->type_id=$request->type_id;
                    $data->desc=$request->desc;
                    $data->save();
                    return response()->json([
                        'message'=>'Data gudang berhasil disimpan',
                        'type'=>'success',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=WarehouseEntity::find($id);

            $check=StockInventory::where('inv_warehouse_id',$request->id)->first();
            if(!is_null($check)) {
                if ($request->md_merchant_id != $data->md_merchant_id) {
                    return response()->json([
                        'message' => 'Gudang telah digunakan untuk penyimpanan,tidak dapat diubah ',
                        'type' => 'warning',
                        'is_modal' => true,
                        'redirect_url' => ''
                    ]);
                }
            }else{

                $data->is_deleted=1;
                $data->save();
                return response()->json([
                    'message'=>'Data gudang berhasil dihapus',
                    'type'=>'success',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Classes\Singleton\XenditCore;
use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;
use Modules\Merchant\Entities\Toko\MedicalRecordEntity;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\TemplateDiagnosis;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantBranch;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaPurchaseUtil;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\PaymentUtils;
use App\Utils\ThirdParty\VirtualAccountUtil;
use Ramsey\Uuid\Uuid;
use PDF;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;


class MedicalRecordController extends Controller
{
    public function __construct()
    {
        $this->sale=SaleOrder::class;
        $this->message=Message::getInstance();
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->xendit=XenditCore::getInstance();
        $this->virtualAccount=new VirtualAccountUtil();
    }

    public function routeWeb()
    {
        Route::prefix('medical-record')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.medical-record.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.medical-record.datatable');
                
                Route::get('/add', [static::class, 'add'])
                    ->name('merchant.toko.medical-record.add');
                
                Route::post('/reload-data', [static::class, 'reloadData'])
                    ->name('merchant.toko.medical-record.reload-data');
                
                Route::post('/save-diagnosis', [static::class, 'saveDiagnosis'])
                    ->name('merchant.toko.medical-record.save-diagnosis');
                Route::post('/save-action', [static::class, 'saveAction'])
                    ->name('merchant.toko.medical-record.save-action');
                Route::post('/save-recipe', [static::class, 'saveRecipe'])
                    ->name('merchant.toko.medical-record.save-recipe');
                Route::post('/save-payment', [static::class, 'savePayment'])
                    ->name('merchant.toko.medical-record.save-payment');

                Route::get('/detail', [static::class, 'detail'])
                    ->name('merchant.toko.medical-record.detail');

            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $firstDate=Carbon::now()->firstOfMonth();
        $params=[
            'title'=>'Rekam Medis',
            'tableColumns'=>MedicalRecordEntity::dataTableColumns(),
            'add'=>(get_role()==3)?MedicalRecordEntity::add(true):
                    MedicalRecordEntity::add(true),
            'key_val'=>$request->key,
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'date' => $firstDate->toDateString()
        ];

        return view('merchant::toko.medical-record.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return MedicalRecordEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public function add(Request $request)
    {
        try {
            parent::setMenuKey($request->key);
            $id = $request->id;
            $reservationId = $request->resv;
            $userId = user_id();
            $merchantId = merchant_id();
            $merchant = Merchant::find($merchantId);
            $page = $request->page;

            $acceptedPage = ['diagnosis', 'action', 'recipe', 'payment'];

            if(!in_array($page, $acceptedPage)){
                return abort(404);
            }

            $data = MedicalRecordEntity::find($id);
                            
            if(is_null($data)){
                return abort(404);
            }

            if($data->status == 1){
                $page = 'payment';
            }

            $reservation=ClinicReservation::with(['getMerchant', 'getCustomer', 'getProduct', 'getStaff.getUser'])
                                        ->where('is_deleted', 0)
                                        ->where('id', $data->clinic_reservation_id)
                                        ->first();
            
            // template diagnosis untuk page diagnosis (page=diagnosis)
            $templateDiagnosis = !is_null($data->diagnosis) ? json_decode($data->diagnosis) : TemplateDiagnosis::where("md_merchant_id", $merchantId)
                                                                                        ->where('is_deleted', 0)
                                                                                        ->get();

            //data product untuk tindakan (page=action)                                                                            
            $services = Product::where('is_with_stock', 0)
                                    ->where('is_deleted', 0)
                                    ->where('md_sc_product_type_id', '!=', 3)
                                    ->where('md_user_id', user_id())
                                    ->get();
            $serviceFromDb = json_decode($data->service);
            $serviceIds = [];
            if(!is_null($serviceFromDb)){
                foreach($serviceFromDb as $key => $item){
                    array_push($serviceIds, $item->sc_product_id);
                }
            }

            $dataService = Product::with(['getUnit', 'getCategory'])
                                    ->whereIn('id', $serviceIds)
                                    ->get();

            //data product untuk resep (page=recipe)
            $recipe = Product::with(['getUnit', 'getCategory'])
                                ->where('is_with_stock', 1)
                                ->where('md_sc_product_type_id', '!=', 3)
                                ->where('is_deleted', 0)
                                ->where('md_user_id', user_id())
                                ->get();

            $recipeFromDb = json_decode($data->recipe);
            $recipeIds = [];
            if(!is_null($recipeFromDb)){
                foreach($recipeFromDb as $key => $item){
                    array_push($recipeIds, $item->sc_product_id);
                }
            }
            $dataRecipe = Product::with(['getUnit', 'getCategory'])
                                    ->whereIn('id', $recipeIds)
                                    ->get();

            
            
            // data yang dibutuhkan untuk pembayaran (page=payment)
            $staff = MerchantStaff::select([
                        'u.id',
                        'u.fullname',
                        'u.email',
                        'r.name as role_name'
                    ])
                        ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                        ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                        ->join('md_roles as r','r.id','u.md_role_id')
                        ->where('m.md_user_id',user_id())
                        ->where('md_merchant_staff.is_non_employee',0)
                        ->where('md_merchant_staff.is_deleted',0)
                        ->get();
            $coaInv=CoaPurchaseUtil::coaInvPurchaseOrder($merchantId);
            $coaJson=CoaSaleUtil::getCoaSaleOrder($merchantId);
            $warehouse=parent::getGlobalWarehouse($merchantId);
            $saleOrder = SaleOrder::find($data->sc_sale_order_id);

            $params=[
                'title'=>'Data Rekam Medis',
                'data'=>$data,
                'reservation' => $reservation,
                'page' => $page,
                'key_val'=>$request->key,
                'searchKey' => md5(Uuid::uuid4()->toString()),
                'merchant' => $merchant,
                'templateDiagnosis' => $templateDiagnosis,
                'services' => $services,
                'recipe' => $recipe,
                'dataService' => $dataService,
                'dataRecipe' => $dataRecipe,
                'staff' => $staff,
                'inventory_id'=>(is_null(merchant_detail()->md_inventory_method_id))?null:merchant_detail()->md_inventory_method_id,
                'coa_json'=>json_encode($coaJson),
                'warehouse'=>json_encode($warehouse),
                'coa_inv'=>collect($coaInv),
                'transType'=>PaymentUtils::getPayment($merchantId),
                'saleOrder' => $saleOrder
            ];

            return view('merchant::toko.medical-record.form',$params);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return abort(404);
        }
    }

    public function saveDiagnosis(Request $request)
    {
        try {
            $id = $request->id;
            $merchantId = merchant_id();
            $templateDiagnosis = TemplateDiagnosis::where("md_merchant_id", $merchantId)
                                                    ->get();
            if(is_null($id)){
                $data = new MedicalRecordEntity();
            } else {
                $data = MedicalRecordEntity::find($id);
            }

            $qa = [];
            $patternText = "/text-/i";
            $patternRadio = "/radio-/i";
            $patternFile = "/file-/i";
            foreach($request->all() as $key => $item){
                if(preg_match($patternText, $key)){
                    $template = $templateDiagnosis->filter(function($c) use($key){
                        return $c->name == $key;
                    })->first();

                    $val = [
                        "md_merchant_template_diagnosis_id" => $template->id,
                        "type" => $template->type,
                        "name" => $key,
                        "question" => $template->question,
                        "option" => $template->option,
                        "value" => $request->$key,
                    ];
                    array_push($qa, $val);
                }

                if(preg_match($patternRadio, $key)){
                    $template = $templateDiagnosis->filter(function($c) use($key){
                        return $c->name == $key;
                    })->first();

                    $val = [
                        "md_merchant_template_diagnosis_id" => $template->id,
                        "type" => $template->type,
                        "name" => $key,
                        "question" => $template->question,
                        "option" => $template->option,
                        "value" => $request->$key,
                    ];
                    array_push($qa, $val);
                }
            }
            

            // upload file
            $destinationPath = 'public/uploads/diagnosis/'.user_id().'/image/';
            $requestFile = [];
            

            if(is_null($data->diagnosis)){
                $requestFile = $templateDiagnosis->filter(function($c) use($key, $patternFile){
                    return $c->type == 'file' && $c->is_deleted == 0;
                })->values();
            } else {
                foreach(json_decode($data->diagnosis) as $key => $item){
                    if($item->type == 'file'){
                        array_push($requestFile, $item);
                    }               
                }
            }
            
            foreach($requestFile as $k => $i){
                if($request->hasFile($i->name))
                {
                    $file=$request->file($i->name);
                    $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                    $image = Image::make(request()->file($i->name))->encode($file->getClientOriginalExtension());
                    $fileName=$destinationPath.$fileName;
                    Storage::disk('s3')->put($fileName, (string) $image);

                    $valFile = [
                        "md_merchant_template_diagnosis_id" => (is_null($data->diagnosis))? $i->id : $i->md_merchant_template_diagnosis_id,
                        "type" => $i->type,
                        "name" => $i->name,
                        "question" => $i->question,
                        "option" => $i->option,
                        "value" => $fileName,
                    ];

                }else{
                    if(is_null($id)){
                        $fileName=null;
                    }else{
                        $diagnosa = json_decode($data->diagnosis);

                        if(!is_null($diagnosa)){
                            $fileObj = array_values(
                                array_filter($diagnosa,function($q)use($i){
                                    return $q->name == $i->name;
                                })
                            )[0];

                            $fileName = $fileObj->value;
                        } else {
                            $fileName = null;
                        }
                        
                    }

                    $valFile = [
                        "md_merchant_template_diagnosis_id" => (is_null($data->diagnosis))? $i->id : $i->md_merchant_template_diagnosis_id,
                        "type" => $i->type,
                        "name" => $i->name,
                        "question" => $i->question,
                        "option" => $i->option,
                        "value" => $fileName,
                    ];
                }

                
                array_push($qa, $valFile);
            }
            // end upload
            
            $data->md_merchant_id = $merchantId;
            $data->clinic_reservation_id = $request->clinic_reservation_id;
            $data->diagnosis = json_encode($qa);

            $data->save();

            return response()->json([
                'message'=>'Data diagnosa berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.medical-record.add', ['id'=>$data->id, 'page' => 'action'])
            ]);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal dihapus!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function saveAction(Request $request)
    {
        try {
            $id = $request->id;

            if(count(json_decode($request->services)) == 0){
                return response()->json([
                    'message'=>'Silahkan tambah tindakan terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            $arrService = json_decode($request->services);

            foreach($arrService as $key => $item)
            {
                $findProduct = Product::find($item->sc_product_id);
                
                if($findProduct->md_sc_product_type_id == 4)
                {
                    $material=Material::
                        select([
                            'sc_product_raw_materials.id',
                            'sc_product_raw_materials.quantity',
                            'p.id as product_id',
                            'p.name as product_name',
                            'p.stock',
                            'p.is_with_stock'
                        ])
                            ->where('sc_product_raw_materials.parent_sc_product_id',$findProduct->id)
                            ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                            ->where('sc_product_raw_materials.is_deleted',0)
                            ->where('p.is_deleted',0)
                            ->get();
                    
                    foreach($material as $k => $v){
                        if($v->is_with_stock == 1){
                            if($v->stock < ($item->quantity * $v->quantity)){
                                return response()->json([
                                    'message'=>"Stock product $v->product_name di dalam paket $findProduct->name tidak mencukupi !",
                                    'type'=>'warning',
                                    'is_modal'=>false,
                                    'redirect_url'=>''
                                ]);
                            }
                        }
                    }
                }
                
            }

            $data = MedicalRecordEntity::find($id);
            $data->service = $request->services;
            $data->save();

            return response()->json([
                'message'=>'Data berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.medical-record.add', ['id'=>$data->id, 'page' => 'recipe'])
            ]);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal dihapus!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function saveRecipe(Request $request)
    {
        try {
            $id = $request->id;
            $data = MedicalRecordEntity::find($id);
            $arrayRecipe = json_decode($request->recipes);
            $productIds = [];

            foreach($arrayRecipe as $key => $item){
                array_push($productIds, $item->sc_product_id);
            }

            $products = Product::whereIn('id', $productIds)->get();

            foreach ($products as $key => $product){
                $filteredRecipe = array_values(
                    array_filter($arrayRecipe, function($var) use($product){
                        return $product->id == $var->sc_product_id;
                    })
                )[0];
                
                if($filteredRecipe->quantity <= 0)
                {
                    return response()->json([
                        'message' => "Masukkan quantity untuk $product->name",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                
                if($product->stock < $filteredRecipe->quantity)
                {
                    return response()->json([
                        'message' => "Stok produk $product->name tidak mencukupi",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }

            $data->recipe = $request->recipes;

            $data->save();

            return response()->json([
                'message'=>'Data resep berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.medical-record.add', ['id'=>$data->id, 'page' => 'payment'])
            ]);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function savePayment(Request $request)
    {
        try {
            $merchantId=merchant_id();
            $userId=user_id();
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            $timestamp=$request->created_at;
            $date = new \DateTime($request->created_at);
            $code=CodeGenerator::generateSaleOrder(merchant_id());

            $medicalRecord = MedicalRecordEntity::find($request->medical_record_id);
            $reservation = ClinicReservation::find($medicalRecord->clinic_reservation_id);

            if($reservation->status == 2){
                return response()->json([
                    'message' => 'Reservasi sudah selesai. Data tidak dapat disimpan!',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $userHelpers = $request->user_helper;
            $doctor = MerchantStaff::with('getUser')
                                        ->where('id',$reservation->md_merchant_staff_id)
                                        ->first();
            $doctorId = $doctor->getUser->id;

            if(is_null($doctor) || is_null($doctorId)){
                return response()->json([
                    'message' => 'Data dokter tidak ditemukan !',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            if($request->is_debet==0)
            {
                if($request->md_transaction_type_id == '-1') {
                    return response()->json([
                        'message' => 'Metode Pembayaran belum dipilih !',
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }else{
                if($request->due_date<$request->created_at){
                    return response()->json([
                        'message' => "Tanggal jatuh tempo tidak boleh kurang dari tanggal transaksi !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }

            $warehouse=json_decode($request->warehouse);
            $coaJson=collect(json_decode($request->coa_json));
            $inventoryId=$request->inventory_id;
            $is_with_load_shipping=0;

            if($request->is_debet==0)
            {
                $trans = explode('_', $request->md_transaction_type_id);
            }else{
                $trans=null;
            }

            if(!is_null($trans)){
                if($trans[2]==1){
                    $status= TransactionStatus::UNPAID;
                }else{
                    $status=($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;
                }
            }else{
                $status=($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;
            }

            if(!is_null($trans)){
                if($trans[2]==1){
                    if($trans[0]==4){
                        $adminFee = ceil(0.008 * $request->total);
                        $total = ceil($adminFee + $request->total);
                    }elseif ($trans[0]==5){
                        $adminFee = 5000;
                        $total = ceil($adminFee + $request->total);
                    }else {
                        $adminFee = ceil(0.016 * $request->total);
                        $total = ceil($adminFee + $request->total);
                    }
                }else{
                    $adminFee=0;
                    $total=$request->total;
                }
            }else{
                $adminFee=0;
                $total=$request->total;
            }

            $purchaseId=SaleOrder::insertGetId([
                'sc_customer_id'=>$reservation->sc_customer_id,
                'code'=>$code,
                'step_type'=>$request->step_type,
                'is_with_load_shipping'=>$is_with_load_shipping,
                'coa_trans_id'=>(!is_null($trans))?$trans[1]:null,
                'md_transaction_type_id'=>(!is_null($trans))?$trans[0]:null,
                'qr_code'=>'-',
                'is_debet'=>$request->is_debet,
                'total'=>$total-$adminFee,
                'md_merchant_id'=>$merchantId,
                'md_sc_transaction_status_id'=>$status,
                'md_sc_shipping_category_id'=>27,
                'tax'=>$request->tax,
                'promo'=>$request->discount,
                'promo_percentage'=>$request->discount_percentage,
                'tax_percentage'=>$request->tax_percentage,
                'shipping_cost'=>0,
                'timezone'=>$request->timezone,
                'note_order'=>$request->note,
                'created_by'=>$userId,
                'is_approved_shop'=>1,
                'created_at'=>$timestamp,
                'updated_at'=>$timestamp,
                'admin_fee'=>$adminFee,
                'is_from_clinic'=>1,
                'assign_to_user_id' => $doctorId,
                'assign_to_user_helper' => $userHelpers
            ]);

            $sc_product_id = $request->sc_product_id;
            $prices = $request->selling_price;
            $quantity = $request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $details = [];
            $stockInOut=[];
            if (empty($sc_product_id)) {
                return response()->json([
                    'message' => 'Data item penjualan tidak boleh kosong !',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            $products=$this->product::select([
                "sc_products.*",
                "mu.name as unit_name"
            ])
            ->join('md_units as mu', 'mu.id', 'sc_products.md_unit_id')
            ->whereIn('sc_products.id',$sc_product_id)
            ->get();

            $productCalculate=[];
            $arrayProductOut=[];
            $updateStockProduct="";

            foreach ($sc_product_id as $key =>$p) {
                $product=$products->firstWhere('id',$sc_product_id[$key]);
                $originalPrice=$prices[$key];

                $stockOutPackage=[];

                if($product->is_with_stock==1)
                {
                    array_push($arrayProductOut,[
                        'sc_product_id'=>$product->id,
                        'product_name'=>$product->name,
                        'product_code'=>$product->code,
                        'unit_name'=>$product->unit_name,
                        'product_name_2'=>$product->name."-".$product->code,
                        'selling_price'=>$originalPrice,
                        'purchase_price'=>$product->purchase_price,
                        'quantity'=>$quantity[$key],
                        'product_stock'=>$product->stock

                    ]);
                }

                if($product->md_sc_product_type_id==4)
                {
                    $rawMaterial=Material::
                    select([
                        'sc_product_raw_materials.id',
                        'sc_product_raw_materials.child_sc_product_id',
                        'sc_product_raw_materials.unit_name',
                        'sc_product_raw_materials.quantity',
                        'sc_product_raw_materials.total',
                        'p.id as product_id',
                        'p.name as product_name',
                        'p.code as product_code',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'p.is_with_stock',
                    ])
                        ->where('sc_product_raw_materials.parent_sc_product_id',$product->id)
                        ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                        ->where('sc_product_raw_materials.is_deleted',0)
                        ->where('p.is_deleted',0)
                        ->get();
                    if($rawMaterial->count()>0)
                    {
                        foreach ($rawMaterial as $rr =>$r)
                        {

                            if($r->is_with_stock==1)
                            {

                                array_push($arrayProductOut,[
                                    'sc_product_id'=>$r->child_sc_product_id,
                                    'product_name'=>$r->product_name,
                                    'product_code'=>$r->product_code,
                                    'unit_name'=>$r->unit_name,
                                    'product_name_2'=>$r->product_name.' '.$r->product_code,
                                    'selling_price'=>$r->selling_price,
                                    'purchase_price'=>$r->purchase_price,
                                    'quantity'=>$quantity[$key]*$r->quantity,
                                    'product_stock'=>$r->stock
                                ]);
                            }
                            $stockOutPackage[]=[
                                'id'=>$r->child_sc_product_id,
                                'head'=>$product->id,
                                'stock_out'=>$quantity[$key]*$r->quantity,
                                'amount'=>$quantity[$key]*$r->total,
                                'unit_name'=>$r->unit_name,
                                'product_name'=>$r->product_name.' '.$r->product_code,
                                'is_with_stock'=>$r->is_with_stock
                            ];

                        }
                    }
                }



                $details[]=[
                    'sc_product_id'=>$sc_product_id[$key],
                    'quantity'=>$quantity[$key],
                    'is_bonus'=>0,
                    'sub_total'=>$quantity[$key]*($originalPrice),
                    'sc_sale_order_id'=>$purchaseId,
                    'profit'=>0,
                    'price'=>$originalPrice,
                    'coa_inv_id'=>$coaInvId[$key],
                    'coa_hpp_id'=>$coHppId[$key],
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp,
                    'unit_name'=>$product->unit_name,
                    'product_name'=>$product->name."-".$product->code,
                    'package_stock_out'=>json_encode($stockOutPackage)
                ];
            }

            if(!empty($arrayProductOut))
            {
                $arrays=collect($arrayProductOut)->groupBy('sc_product_id');
                $countOutOut=0;
                foreach ($arrays as $j =>$i)
                {

                    $quantity=0;
                    $firstData=$i->first();
                    foreach ($i as $ii =>$iii)
                    {
                        $quantity+=$iii['quantity'];
                    }


                    if($firstData['product_stock']<$quantity)
                    {
                        return response()->json([
                            'message' => "Stok produk  ".$firstData['product_name']." tidak mencukupi !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }

                    if($warehouse->is_default==1)
                    {
                        $stockP=$firstData['product_stock'] - $quantity;
                        $updateStockProduct.=($countOutOut==0)?"(".$j.",".$stockP.")":",(".$j.",".$stockP.")";
                        $countOutOut++;
                    }else{
                        $stockP=$firstData['product_stock'];
                    }

                    array_push($productCalculate,[
                        'id'=>$j,
                        'amount'=>$quantity
                    ]);

                    $stockInOut[]=[
                        'sync_id' => $j . Uuid::uuid4()->toString(),
                        'sc_product_id' => $j,
                        'total' => $quantity,
                        'inv_warehouse_id' => $warehouse->id,
                        'record_stock' => $stockP,
                        'created_by' => $userId,
                        'selling_price' => $firstData['selling_price'],
                        'purchase_price' => $firstData['purchase_price'],
                        'residual_stock' => $quantity,
                        'type' => StockInventory::OUT,
                        'transaction_action' => 'Pengurangan Stok ' . $firstData['product_name'] . ' Dari Faktur Penjualan Dengan Code ' . $code,
                        'stockable_type'=>'App\Models\SennaToko\SaleOrder',
                        'stockable_id'=>$purchaseId,
                        'created_at'=>$request->created_at,
                        'updated_at'=>$request->created_at,
                        'timezone'=>$request->timezone,
                        'unit_name'=>$firstData['unit_name'],
                        'product_name'=>$firstData['product_name_2']
                    ];

                }
            }
            
            $arrayOfSession=[
                'productCalculate'=>$productCalculate,
                'updateStockProduct'=>ltrim($updateStockProduct, ','),
                'inv_id'=>$inventoryId,
                'warehouse_id'=>$warehouse->id,
                'code' => $code,
                'user_id'=>$userId,
                'merchant_id'=>$merchantId,
                'coaJson'=>$coaJson,
                'timestamp'=>$timestamp,
                'is_debet'=>$request->is_debet,
                'stock_in_out'=>$stockInOut,
                'is_with_load_shipping'=>$is_with_load_shipping,
                'total'=>$total-$adminFee,
                'paid_nominal'=>$request->paid_nominal,
                'shipping_cost'=>0,
                'due_date'=>$request->due_date,
                'timezone'=>$request->timezone,
                'admin_fee'=>$adminFee,
                'purchaseId'=>$purchaseId
            ];

            DB::table('sc_sale_order_details')->insert($details);

            if(!is_null($trans)){
                if($trans[2]==1){
                    $updateSale=SaleOrder::find($purchaseId);
                    $updateSale->json_order_online=json_encode($arrayOfSession);
                    $updateSale->is_payment_gateway=1;
                    if($trans[0]==4){
                        $paramsQRIS = [
                            'external_id' => (string)Uuid::uuid4()->toString(),
                            'type' => 'DYNAMIC',
                            'callback_url' =>\route('pay.callback.all'),
                            'amount' =>(float)$total,
                        ];

                        if(env('APP_ENV')=='production'){
                            $qris=$this->xendit::createQRIS($paramsQRIS);

                            if (array_key_exists('message', $qris)) {
                                return response()->json([
                                    'message' => $qris['message'],
                                    'type' => 'warning',
                                    'is_modal' => false,
                                    'redirect_url' => ''
                                ]);
                            }
                        }else{
                            $qris=[
                                'id' => (string)Uuid::uuid4()->toString(),
                                'external_id' => 'testing_id_123',
                                'nominal' => 1500,
                                'qr_string' => '0002010102##########CO.XENDIT.WWW011893600#######14220002152#####414220010303TTT####015CO.XENDIT.WWW02180000000000000000000TTT52045######ID5911XenditQRIS6007Jakarta6105121606##########3k1mOnF73h11111111#3k1mOnF73h6v53033605401163040BDB',
                                'callback_url' => 'https://yourwebsite.com/callback',
                                'type' => 'DYNAMIC',
                                'status' => 'ACTIVE',
                                'created' => '2020-01-08T18:18:18.661Z',
                                'updated' => '2020-01-08T18:18:18.661Z',
                            ];
                        }

                        $destinationPath = 'public/uploads/transaction/'.$userId.'/qr/';
                        $qrCodeFile=$destinationPath.$code.'.png';
                        $imageQR=QrCode::size(600)
                            ->format('png')
                            ->generate($qris['qr_string']);
                        Storage::disk('s3')->put($qrCodeFile, $imageQR);
                        $updateSale->qr_code=$qrCodeFile;
                        $updateSale->md_merchant_payment_method_id=$trans[4];
                        $updateSale->expired_time=Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
                        $updateSale->qris_response=json_encode($qris);
                        $updateSale->checkout_response=json_encode($qris);
                        $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $updateSale->save();
                        
                        $medicalRecord->sc_sale_order_id = $updateSale->id;
                        $medicalRecord->status = 1;
                        $medicalRecord->save();

                        $reservation->status = 2;
                        $reservation->save();

                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($purchaseId),

                            ])
                        ]);
                    }elseif ($trans[0]==5){
                        $response=$this->virtualAccount->create($userId,strtoupper($trans[3]),$total,16,(string)Uuid::uuid4()->toString());
                        if($response == false){
                            return response()->json([
                                'message' => 'Terjadi Kesalahan saat pembuatan VA !',
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }

                        $updateSale->md_merchant_payment_method_id=$trans[4];
                        $updateSale->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                        $updateSale->checkout_response=json_encode($response);
                        $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $updateSale->save();

                        $medicalRecord->sc_sale_order_id = $updateSale->id;
                        $medicalRecord->status = 1;
                        $medicalRecord->save();

                        $reservation->status = 2;
                        $reservation->save();

                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($purchaseId),

                                ])
                        ]);

                    }else{
                        $idEWallet=$trans[3].'_'.$trans[4];
                        if($idEWallet=='ID_OVO'){
                            if(is_null($request->mobile_number)){
                                return response()->json([
                                    'message' => 'No telpon untuk metode pembayaran OVO wajib diisi',
                                    'type' => 'warning',
                                    'is_modal' => false,
                                    'redirect_url' => ''
                                ]);
                            }
                            $ewallet = [
                                'reference_id' => (string)Uuid::uuid4()->toString(),
                                'currency' => 'IDR',
                                'amount' => $total,
                                'checkout_method' => 'ONE_TIME_PAYMENT',
                                'channel_code' => $idEWallet,
                                'channel_properties' => [
                                    'mobile_number'=>$request->mobile_number,
                                    'success_redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                        'key' => encrypt($purchaseId),
                                        'status'=>2
                                    ]),
                                    'failure_redirect_url'=>route('merchant.toko.transaction.sale-order.checkout', [
                                        'key' => encrypt($purchaseId),
                                        'status'=>5

                                    ])
                                ],
                            ];
                        }else{
                            $ewallet = [
                                'reference_id' => (string)Uuid::uuid4()->toString(),
                                'currency' => 'IDR',
                                'amount' => $total,
                                'checkout_method' => 'ONE_TIME_PAYMENT',
                                'channel_code' => $idEWallet,
                                'channel_properties' => [
                                    'success_redirect_url' => route('checkout-notification', [
                                        'key' => encrypt($purchaseId),
                                        'status'=>2
                                    ]),
                                    'failure_redirect_url'=>route('checkout-notification', [
                                        'key' => encrypt($purchaseId),
                                        'status'=>5

                                    ])
                                ],
                            ];

                        }
                        $ewalletRes=$this->xendit::createEWalletCharge($ewallet);

                        if (array_key_exists('message', $ewalletRes)) {
                            return response()->json([
                                'message' => $ewalletRes['message'],
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $updateSale->md_merchant_payment_method_id=$trans[5];
                        $updateSale->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                        $updateSale->checkout_response=json_encode($ewalletRes);
                        $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $updateSale->save();

                        $medicalRecord->sc_sale_order_id = $updateSale->id;
                        $medicalRecord->status = 1;
                        $medicalRecord->save();

                        $reservation->status = 2;
                        $reservation->save();

                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($purchaseId),

                            ])
                        ]);
                    }

                }else{
                    Session::put('sale_faktur_'.$purchaseId,$arrayOfSession);

                    $medicalRecord->sc_sale_order_id = $purchaseId;
                    $medicalRecord->status = 1;
                    $medicalRecord->save();

                    $reservation->status = 2;
                    $reservation->save();

                    DB::commit();

                    Artisan::call('sale:faktur', ['--sale_id' =>$purchaseId]);
                    return response()->json([
                        'message' => 'Data berhasil disimpan!',
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('merchant.toko.sale-order.detail', ['id' => $purchaseId])

                    ]);
                }
            }else{
                Session::put('sale_faktur_'.$purchaseId,$arrayOfSession);

                $medicalRecord->sc_sale_order_id = $purchaseId;
                $medicalRecord->status = 1;
                $medicalRecord->save();
                
                $reservation->status = 2;
                $reservation->save();

                DB::commit();

                Artisan::call('sale:faktur', ['--sale_id' =>$purchaseId]);
                return response()->json([
                    'message' => 'Data berhasil disimpan!',
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.sale-order.detail', ['id' => $purchaseId])
                ]);

            }


        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (MedicalRecordEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $searchParams['start_date'] = $request->input('start_date');
        $searchParams['end_date'] = $request->input('end_date');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Data Rekam Medis',
            'searchKey' => $searchKey,
            'tableColumns'=>MedicalRecordEntity::dataTableColumns(),
            'key_val'=>$request->key,


        ];
        return view('merchant::toko.medical-record.list',$params);

    }

    public function detail(Request $request)
    {
        try {
            $id = $request->id;
            $userId = user_id();
            $merchantId = merchant_id();
            $merchant = Merchant::find($merchantId);
            $page = $request->page;

            $acceptedPage = ['diagnosis', 'action', 'recipe', 'payment'];

            if(!in_array($page, $acceptedPage)){
                return abort(404);
            }

            $data = MedicalRecordEntity::find($id);
                            
            if(is_null($data)){
                return abort(404);
            }

            $reservation=ClinicReservation::with(['getMerchant', 'getCustomer', 'getProduct', 'getStaff.getUser'])
                                        ->where('is_deleted', 0)
                                        ->where('id', $data->clinic_reservation_id)
                                        ->first();
            
            // template diagnosis untuk page diagnosis (page=diagnosis)
            $templateDiagnosis = !is_null($data->diagnosis) ? json_decode($data->diagnosis) : TemplateDiagnosis::where("md_merchant_id", $merchantId)
                                                                                        ->where('is_deleted', 0)
                                                                                        ->get();

            //data product untuk tindakan (page=action)                                                                            
            $services = Product::where('is_with_stock', 0)
                                    ->where('is_deleted', 0)
                                    ->where('md_user_id', user_id())
                                    ->get();
            $serviceFromDb = json_decode($data->service);
            $serviceIds = [];
            if(!is_null($serviceFromDb)){
                foreach($serviceFromDb as $key => $item){
                    array_push($serviceIds, $item->sc_product_id);
                }
            }

            $dataService = Product::with(['getUnit', 'getCategory'])
                                    ->whereIn('id', $serviceIds)
                                    ->get();

            //data product untuk resep (page=recipe)
            $recipe = Product::with(['getUnit', 'getCategory'])
                                ->where('is_with_stock', 1)
                                ->where('is_deleted', 0)
                                ->where('md_user_id', user_id())
                                ->get();

            $recipeFromDb = json_decode($data->recipe);
            $recipeIds = [];
            if(!is_null($recipeFromDb)){
                foreach($recipeFromDb as $key => $item){
                    array_push($recipeIds, $item->sc_product_id);
                }
            }
            $dataRecipe = Product::with(['getUnit', 'getCategory'])
                                    ->whereIn('id', $recipeIds)
                                    ->get();
                                    
            // data yang dibutuhkan untuk pembayaran (page=payment)
            $staff = MerchantStaff::select([
                        'u.id',
                        'u.fullname',
                        'u.email',
                        'r.name as role_name'
                    ])
                        ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                        ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                        ->join('md_roles as r','r.id','u.md_role_id')
                        ->where('m.md_user_id',user_id())
                        ->where('md_merchant_staff.is_non_employee',0)
                        ->where('md_merchant_staff.is_deleted',0)
                        ->get();
            $saleOrder = SaleOrder::find($data->sc_sale_order_id);

            if(is_null($saleOrder)){
                return abort(404);
            }

            $params=[
                'title'=>'Detail Rekam Medis',
                'data'=>$data,
                'reservation' => $reservation,
                'page' => $page,
                'merchant' => $merchant,
                'templateDiagnosis' => $templateDiagnosis,
                'services' => $services,
                'recipe' => $recipe,
                'dataService' => $dataService,
                'dataRecipe' => $dataRecipe,
                'staff' => $staff,
                'saleOrder' => $saleOrder
            ];

            return view('merchant::toko.medical-record.detail.detail',$params);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return abort(404);
        }
    }
    
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantMenu;
use App\Models\MasterData\Unit;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class UnitController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/unit')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::post('/add', 'Toko\UnitController@add')
                    ->name('merchant.toko.unit.add');
                Route::post('/save', 'Toko\UnitController@save')
                    ->name('merchant.toko.unit.save');
            });
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=Unit::find($id);
        }else{
            $data=new Unit();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];
        
        return view('merchant::toko.master-data.unit.form',$params);

    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);

            $checkUnit=Unit::whereRaw("lower(name) like '".strtolower($request->name)."' ")->first();            
            if($checkUnit){
                return response()->json([
                    'message'=>'Satuan sudah ada!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            $data=new Unit();
            $data->name=$request->name;
            $data->save();

            return response()->json([
                'message'=>'Satuan produk berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            // dd($e);
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }


}

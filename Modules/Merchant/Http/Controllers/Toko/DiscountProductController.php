<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\DiscountProductEntity;
use App\Models\SennaToko\DiscountProduct;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Merk;
use App\Models\SennaToko\DiscountProductDetail;
use App\Models\MasterData\Merchant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use App\Utils\Merchant\MerchantUtil;

class DiscountProductController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('marketing/discount-product')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\DiscountProductController@index')
                    ->name('merchant.toko.discount-product.index');
                Route::post('/data-table', 'Toko\DiscountProductController@dataTable')
                    ->name('merchant.toko.discount-product.datatable');
                Route::get('/add', 'Toko\DiscountProductController@add')
                    ->name('merchant.toko.discount-product.add');
                Route::get('/edit', 'Toko\DiscountProductController@edit')
                    ->name('merchant.toko.discount-product.edit');
                Route::post('/save', 'Toko\DiscountProductController@save')
                    ->name('merchant.toko.discount-product.save');
                Route::post('/delete', 'Toko\DiscountProductController@delete')
                    ->name('merchant.toko.discount-product.delete');
                Route::post('/reload-data', 'Toko\DiscountProductController@reloadData')
                    ->name('merchant.toko.discount-product.reload-data');
            });
    }

    protected function _getPromoType()
    {
        $data = [
            [
                "promo_type" => 1,
                "bonus_type" => 1,
                "description" => "Diskon harga (%), jika memenuhi total transaksi",
            ],
            [
                "promo_type" => 1,
                "bonus_type" => 2,
                "description" => "Diskon harga (Rp), jika memenuhi total transaksi",
            ],
            [
                "promo_type" => 1,
                "bonus_type" => 3,
                "description" => "Bonus Produk, jika memenuhi total transaksi",
            ],
            [
                "promo_type" => 2,
                "bonus_type" => 1,
                "description" => "Beli produk tertentu, dapat diskon harga (%)",
            ],
            [
                "promo_type" => 2,
                "bonus_type" => 2,
                "description" => "Beli produk tertentu, dapat diskon harga (Rp)",
            ],
            [
                "promo_type" => 2,
                "bonus_type" => 3,
                "description" => "Beli produk tertentu, bonus produk tertentu",
            ]
        ];

        return $data;
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Promo',
            'tableColumns'=>DiscountProductEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'add'=>(get_role()==3)?DiscountProductEntity::add(true):
                DiscountProductEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.discount-product.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return DiscountProductEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public function add(Request $request){

        $id=$request->id;
        $merchantId = merchant_id();
        $userId = user_id();
        $bonusProduct=null;
        if(!is_null($id)){
            $data=DiscountProductEntity::find($id);
            if($data->bonus_type==3){
                $bonusProduct = json_decode($data->bonus_value);
            }

        }else{
            $data=new DiscountProductEntity();
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'bonusProduct'=>(is_null($bonusProduct)?null:$bonusProduct),
            'category'=>CategoryEntity::listOption(),
            'merk' => MerkEntity::listOption(),
        ];

        return view('merchant::toko.master-data.discount-product.form2',$params);
    }

    public function edit(Request $request){

        $id=$request->id;
        $merchantId = merchant_id();
        if(!is_null($id)){
            $data=DiscountProductEntity::find($id);
        }else{
            $data=new DiscountProductEntity();
        }

        $days = ["Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"];
        $promoType = $this->_getPromoType();

        $detailProductPromo = DB::select("
        select
            dpd.id,
            dpd.sc_product_id,
            sp.name,
            dpd.quantity,
            m.id as md_merchant_id
        from
            crm_discount_by_product_details dpd
        join
            crm_discount_by_products dp on dpd.discount_by_product_id = dp.id
        join
            sc_products sp on sp.id = dpd.sc_product_id
        join
            md_users u on u.id = sp.md_user_id
        join
            md_merchants m on m.md_user_id = u.id
        where
            dp.id = $id
        ");

        $selectProductByCategory = DB::select("
        select
            spc.id,
            spc.name,
            s.v->>'quantity' as quantity
        from
            crm_discount_by_products dp
        join lateral
            JSONB_ARRAY_ELEMENTS(dp.select_product_by_value) as s(v) ON TRUE
        join
            sc_product_categories spc ON (s.v->>'id')::text::int = spc.id
        where
            dp.id = $id
        ");

        $selectProductByMerk = DB::select("
        select
            sm.id,
            sm.name,
            s.v->>'quantity' as quantity
        from
            crm_discount_by_products dp
        join lateral
            JSONB_ARRAY_ELEMENTS(dp.select_product_by_value) as s(v) ON TRUE
        join
            sc_merks sm ON (s.v->>'id')::text::int = sm.id
        where
            dp.id = $id
        ");

        if($data->bonus_type == 3)
        {
            $bonusProduct = DB::select("
            select
                sp.code as code,
                sp.foto as foto,
                sp.name as name,
                sp.id as value,
                (s.v->>'quantity')::int as quantity,
                sp.description as description,
                sp.is_with_stock,
                sp.selling_price,
                m.id as md_merchant_id
            from
                crm_discount_by_products dp
            join lateral
                JSONB_ARRAY_ELEMENTS(dp.bonus_value) as s(v) on TRUE
            join
                sc_products sp ON (s.v->>'value')::text::int = sp.id
            join
                md_users as u on u.id = sp.md_user_id
            join
                md_merchants as m on m.md_user_id = u.id
            where
                dp.id = $id
            ");
        } else {
            $bonusProduct = [];
        }

        $customerTerms = json_decode($data->customer_terms);

        if(is_null($customerTerms)){
            $customerTerms = (object)[
                "birth_date" => "-1",
                "birth_year" => "-1",
                "customer_name" => [],
                "specific_customer_id" => []
            ];
        }

        $assignToBranch = json_decode($data->assign_to_branch);

        $customerName = $customerTerms->customer_name;

        if(gettype($customerName) == 'array'){
            $custNameVal = implode(',', array_column($customerName, 'name'));
        } else {
            $custNameVal = $customerName;
        }

        $specificCustomerId = isset($customerTerms->specific_customer_id)? $customerTerms->specific_customer_id:[];
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'bonusProduct'=>is_null($bonusProduct)? []:$bonusProduct,
            'days' => $days,
            'promoType' => $promoType,
            'detailProductPromo' => collect($detailProductPromo),
            'selectProductByCategory' => collect($selectProductByCategory),
            'selectProductByMerk' => collect($selectProductByMerk),
            'customerTerms' => $customerTerms,
            'assignToBranch' => is_null($assignToBranch)? []:$assignToBranch,
            'custNameVal' => $custNameVal,
            'specificCustomerId' => $specificCustomerId,
            'customers'=>Customer::select(['id','name as nama_pelanggan'])->whereIn('id',array_column($specificCustomerId,'sc_customer_id'))->get()

        ];

        return view('merchant::toko.master-data.discount-product.form2-edit',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);

            if(is_null($request->start_date)){
                return response()->json([
                    'message'=>'Tanggal mulai promo belum di isi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->end_date<$request->start_date){
                return response()->json([
                    'message'=>'Tanggal berakhir promo kurang dari tanggal mulai promo',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->is_daily == 0){
                if($request->end_clock<$request->start_clock){
                    return response()->json([
                        'message'=>'Jam berakhir promo kurang dari jam mulai promo',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            if(is_null($request->code)){
                return response()->json([
                    'message'=>'Kode promo belum diisi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($request->desc)){
                return response()->json([
                    'message'=>'Deskripsi belum diisi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->all_branch == 1){
                $tmp = get_cabang();
                $forCheckBranch = [];
                foreach($tmp as $k => $i){
                    if($i->id != merchant_id()){
                        array_push($forCheckBranch, intval($i->id));
                    }
                }
            } else {
                $tmpCheckBranch = json_decode($request->assign_to_branch);
                $forCheckBranch = [];

                foreach($tmpCheckBranch as $key => $item){
                    array_push($forCheckBranch, intval($item));
                }
            }

            array_unshift($forCheckBranch, merchant_id());

            if($request->select_product_by == 1){
                $product = json_decode($request->details);
            } else if($request->select_product_by == 2){
                $reqCategoryIds = [];
                $reqCategory = json_decode($request->details);

                foreach($reqCategory as $key => $item){
                    array_push($reqCategoryIds, $item->id);
                }

                $productCategory = CategoryEntity::whereIn("id", $reqCategoryIds)
                            ->where("is_deleted", 0)
                            ->get();

                $product = Product::select([
                    "sc_products.id",
                    "sc_products.sc_product_category_id"
                ])
                ->join("md_users as u", "u.id", "sc_products.md_user_id")
                ->join("md_merchants as m", "m.md_user_id", "u.id")
                ->where('is_deleted', 0)
                ->whereIn('sc_product_category_id', $reqCategoryIds)
                ->whereIn('m.id', $forCheckBranch)
                ->get();

                foreach($productCategory as $key => $item){
                    if(!$product->contains('sc_product_category_id', $item->id)){
                        return response()->json([
                            'message'=>"Tidak ada produk untuk kategori $item->name !",
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                }

            } else if($request->select_product_by == 3){
                $reqMerkIds = [];
                $reqMerk = json_decode($request->details);
                foreach($reqMerk as $key => $item){
                    array_push($reqMerkIds, $item->id);
                }
                $merk = Merk::whereIn("id", $reqMerkIds)
                            ->where("is_deleted", 0)
                            ->get();

                $product = Product::select([
                    "sc_products.id",
                    "sc_products.sc_merk_id"
                ])
                ->join("md_users as u", "u.id", "sc_products.md_user_id")
                ->join("md_merchants as m", "m.md_user_id", "u.id")
                ->where('is_deleted', 0)
                ->whereIn('sc_merk_id', $reqMerkIds)
                ->whereIn('m.id', $forCheckBranch)
                ->get();

                foreach($merk as $key => $item){
                    if(!$product->contains('sc_merk_id', $item->id)){
                        return response()->json([
                            'message'=>"Tidak ada produk untuk merk $item->name !",
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                }
            }

            $id=$request->id;
            $code=DiscountProductEntity::where('code',$request->code)
                                            ->where('md_merchant_id',merchant_id())
                                            ->where('id', '!=', $id)
                                            ->where('is_deleted',0)
                ->where('is_referral_affiliate',0)
                ->where('is_coupon',0)
                ->get();
            if(count($code)>0){
                return response()->json([
                    'message'=>'Kode promo telah digunakan',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(!is_null($id))
            {
                $data = DiscountProductEntity::find($id);
            } else {
                $data = new DiscountProductEntity();
            }

            if($request->is_type == 1){
                foreach($product as $item){
                    $start=$request->start_date;
                    $end=$request->end_date;
                    $promoActive=DiscountProductEntity::select([
                        "crm_discount_by_products.id",
                        "crm_discount_by_products.md_merchant_id",
                        "crm_discount_by_products.name",
                        "crm_discount_by_products.assign_to_branch",
                        "d.sc_product_id"
                    ])
                    ->join('crm_discount_by_product_details as d','d.discount_by_product_id','crm_discount_by_products.id')
                    ->where('is_active',1)
                        ->where('is_referral_affiliate',0)
                        ->where('is_coupon',0)
                        ->where('is_deleted',0)
                    ->where(function($q) use($forCheckBranch){
                        $q->whereIn('crm_discount_by_products.md_merchant_id', $forCheckBranch)
                            ->orWhereJsonContains('crm_discount_by_products.assign_to_branch', $forCheckBranch);
                    })
                    ->where('d.discount_by_product_id','!=',$id)
                    ->where('d.sc_product_id',$item->id)
                    ->where(function($q) use($start,$end){
                        $q->where([['start_date','<=',$start],
                                    ['end_date','>=',$start]])
                            ->orWhere([['start_date','>=',$start],
                                        ['start_date','<=',$end]]);
                    })
                    ->get();

                    if(count($promoActive)>0 && $request->is_active == 1){
                        return response()->json([
                            'message'=>'Ada promo pada jangka waktu ini',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                }

                // cek apakah ada promo tipe transaksi
                $promoTrans=DiscountProductEntity::where('is_active',1)
                            ->where('is_deleted',0)
                    ->where('is_referral_affiliate',0)
                    ->where('is_coupon',0)
                    ->where('id','!=',$id)
                            ->where('is_type', 2)
                            ->where(function($q) use($forCheckBranch){
                                $q->whereIn('md_merchant_id', $forCheckBranch)
                                    ->orWhereJsonContains('assign_to_branch', $forCheckBranch);
                            })
                            ->where(function($q) use($start,$end){
                                $q->where([['start_date','<=',$start],
                                            ['end_date','>=',$start]])
                                    ->orWhere([['start_date','>=',$start],
                                                ['start_date','<=',$end]]);
                            })
                            ->get();

                if(count($promoTrans)>0 && $request->is_active == 1){
                    return response()->json([
                        'message'=>'Ada promo pada jangka waktu ini',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }


            } else {
                $start=$request->start_date;
                $end=$request->end_date;

                $promoActive=DiscountProductEntity::where('is_active',1)
                    ->where('is_referral_affiliate',0)
                    ->where('is_coupon',0)
                    ->where('is_deleted',0)
                                            ->where('id','!=',$id)
                                            ->where(function($q) use($forCheckBranch){
                                                $q->whereIn('md_merchant_id', $forCheckBranch)
                                                    ->orWhereJsonContains('assign_to_branch', $forCheckBranch);
                                            })
                                            ->where(function($q) use($start,$end){
                                                $q->where([['start_date','<=',$start],
                                                            ['end_date','>=',$start]])
                                                    ->orWhere([['start_date','>=',$start],
                                                                ['start_date','<=',$end]]);
                                            })
                                            ->get();

                if(count($promoActive)>0 && $request->is_active == 1){
                    return response()->json([
                        'message'=>'Ada promo pada jangka waktu ini',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            if(is_null($id)){
                if($request->promo_type == 2){
                    if(count(json_decode($request->details)) < 1){
                        return response()->json([
                            'message'=>'Silahkan pilih produk untuk syarat promo !',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                }
            }

            if($request->all_branch == 1){
                $tmp = get_cabang();
                $branch = [];
                foreach($tmp as $k => $i){
                    if($i->id != merchant_id()){
                        array_push($branch, intval($i->id));
                    }
                }
            } else {
                $branch = [];
                $tmp = json_decode($request->assign_to_branch);
                foreach($tmp as $k => $i){
                    array_push($branch, intval($i));
                }
            }

            // cek produk untuk syarat promo apakah tersedia di merchant cabang
            if($request->promo_type == 2){
                $queryMerchantId = $branch;
                array_unshift($queryMerchantId, merchant_id());

                $otherMerchant = Merchant::whereIn("id",$queryMerchantId)
                                            ->get()
                                            ->toArray();
                $productIds = [];
                $productCode = [];

                foreach($product as $key => $item)
                {
                    array_push($productIds, $item->id);
                }

                $termProduct = Product::select([
                    "sc_products.id",
                    "sc_products.name",
                    "sc_products.code",
                    "m.id as md_merchant_id",
                    "m.name as merchant_name"
                ])
                ->join("md_users as u", "u.id", "sc_products.md_user_id")
                ->join("md_merchants as m", "m.md_user_id", "u.id")
                ->whereIn('sc_products.id', $productIds)
                ->whereIn("m.id", $queryMerchantId)
                ->where("sc_products.is_deleted", 0)
                ->get();

                foreach($termProduct as $key => $item)
                {
                    array_push($productCode, $item->code);
                }

                $checkProductOtherMerchant = Product::select([
                    "sc_products.id",
                    "sc_products.name",
                    "sc_products.code",
                    "m.id as md_merchant_id",
                    "m.name as merchant_name"
                ])
                ->join("md_users as u", "u.id", "sc_products.md_user_id")
                ->join("md_merchants as m", "m.md_user_id", "u.id")
                ->whereIn("sc_products.code", $productCode)
                ->whereIn("m.id", $queryMerchantId)
                ->where("sc_products.is_deleted", 0)
                ->get()
                ->toArray();

                foreach($queryMerchantId as $branchId)
                {
                    $productByMerchant = array_values(array_filter($checkProductOtherMerchant, function($val) use($branchId){
                        return $val["md_merchant_id"] == $branchId;
                    }));

                    $filterMerchant = array_values(array_filter($otherMerchant, function($val) use($branchId){
                        return $val["id"] == $branchId;
                    }));

                    $currentMerchant = array_shift($filterMerchant);

                    foreach($checkProductOtherMerchant as $key => $item)
                    {
                        if(!in_array($item["code"], array_column($productByMerchant, "code")))
                        {
                            return response()->json([
                                'message'=>$item["name"]." tidak tersedia di cabang ".$currentMerchant["name"],
                                'type'=>'warning',
                                'is_modal'=>false,
                                'redirect_url'=>''
                            ]);
                        }
                    }
                }
            }

            // cek produk untuk bonus apakah tersedia di merchant cabang
            if($request->bonus_type == 3){
                $queryMerchantId = $branch;
                array_unshift($queryMerchantId, merchant_id());

                $otherMerchant = Merchant::whereIn("id",$queryMerchantId)
                                            ->get()
                                            ->toArray();

                $requestBonusProduct = json_decode($request->bonus_value);
                $bonusProductIds = [];
                $bonusProductCode = [];

                $queryMerchantId = $branch;
                array_unshift($queryMerchantId, merchant_id());

                foreach($requestBonusProduct as $key => $item)
                {
                    array_push($bonusProductIds, $item->value);
                }

                $termBonusProduct = Product::select([
                    "sc_products.id",
                    "sc_products.name",
                    "sc_products.code",
                    "m.id as md_merchant_id",
                    "m.name as merchant_name"
                ])
                ->join("md_users as u", "u.id", "sc_products.md_user_id")
                ->join("md_merchants as m", "m.md_user_id", "u.id")
                ->whereIn('sc_products.id', $bonusProductIds)
                ->whereIn("m.id", $queryMerchantId)
                ->where("sc_products.is_deleted", 0)
                ->get();

                foreach($termBonusProduct as $key => $item)
                {
                    array_push($bonusProductCode, $item->code);
                }

                $checkBonusProductOtherMerchant = Product::select([
                    "sc_products.id",
                    "sc_products.name",
                    "sc_products.code",
                    "m.id as md_merchant_id",
                    "m.name as merchant_name"
                ])
                ->join("md_users as u", "u.id", "sc_products.md_user_id")
                ->join("md_merchants as m", "m.md_user_id", "u.id")
                ->whereIn("sc_products.code", $bonusProductCode)
                ->whereIn("m.id", $queryMerchantId)
                ->where("sc_products.is_deleted", 0)
                ->get()
                ->toArray();

                foreach($branch as $branchId)
                {
                    $productByMerchant = array_values(array_filter($checkBonusProductOtherMerchant, function($val) use($branchId){
                        return $val["md_merchant_id"] == $branchId;
                    }));

                    $filterMerchant = array_values(array_filter($otherMerchant, function($val) use($branchId){
                        return $val["id"] == $branchId;
                    }));

                    $currentMerchant = array_shift($filterMerchant);

                    foreach($checkBonusProductOtherMerchant as $key => $item)
                    {
                        if(!in_array($item["code"], array_column($productByMerchant, "code")))
                        {
                            return response()->json([
                                'message'=>"Bonus produk ".$item["name"]." tidak tersedia di cabang ".$currentMerchant["name"],
                                'type'=>'warning',
                                'is_modal'=>false,
                                'redirect_url'=>''
                            ]);
                        }
                    }
                }
            }

            if($request->bonus_type == 3)
            {
                if(count(json_decode($request->bonus_value)) < 1)
                {
                    return response()->json([
                        'message'=>"Silahkan pilih bonus produk terlebih dahulu !",
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message'=>$error,
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            if(count(json_decode($request->day)) == 0){
                return response()->json([
                    'message'=>'Silahkan pilih minimal 1 hari untuk promo',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            $customerTerms = json_decode($request->req_customer_terms);

            if($request->is_all_customer == 0)
            {
                if($request->is_specific_customer == 0){
                    $custName = ($customerTerms->customer_name)? $customerTerms->customer_name:[];

                    if(count($custName) < 1 && $customerTerms->birth_date == "-1")
                    {
                        return response()->json([
                            'message'=>'Isi nama pelanggan atau tanggal lahir pelanggan terlebih dahulu !',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                } else {
                    $specificCustomerId = ($customerTerms->specific_customer_id)? $customerTerms->specific_customer_id:[];

                    if(count($specificCustomerId) < 1){
                        return response()->json([
                            'message'=>'Silahkan pilih pelanggan terlebih dahulu !',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                }
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/promo/';

            if($request->hasFile('foto'))
            {
                $file=$request->file('foto');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }

            $data->name=$request->name;
            $data->desc=$request->desc;
            $data->code=$request->code;
            $dayName=[];
            foreach(json_decode($request->day) as $item){
                $dayName[]=["hari"=>$item->day_name];
            }

            $data->day=json_encode($dayName);
            $data->activation_type=$request->activation_type;
            $bonusValue=[];
            foreach(json_decode($request->bonus_value) as $item){
                if($request->bonus_type==3){
                    $p=Product::find($item->value);
                    if($p->count()<1){
                        return response()->json([
                            'message'=>'Bonus produk tidak tersedia',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                    if($item->quantity <= 0){
                        return response()->json([
                            'message'=>'Masukkan kuantitas bonus untuk produk '.$p->name,
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                    $bonusValue[]=[
                        "value"=>$item->value,
                        "name"=>$p->name,
                        "foto"=>$p->foto,
                        "description"=>$p->description,
                        "is_with_stock"=>$p->is_with_stock,
                        "code"=>$p->code,
                        "selling_price"=>$p->selling_price,
                        "quantity"=>$item->quantity,
                        "md_merchant_id" => merchant_id()
                        ];
                }else{
                    if($request->bonus_type == 1){
                        $bonusValue[]=[
                            "value"=>$item->value
                            ];
                    }

                    if($request->bonus_type == 2){
                        $bonusValue[]=[
                            "value"=>(float)strtr($item->value, array('.' => '', ',' => '.'))
                            ];
                    }
                }

            }

            if(is_null($id)){

                $data->md_merchant_id=merchant_id();
                $data->bonus_type=$request->bonus_type;
                $data->bonus_value=json_encode($bonusValue);
                $data->promo_type=$request->promo_type;
                $data->promo_value=strtr($request->promo_value, array('.' => '', ',' => '.'));
                $data->is_applies_multiply=$request->is_applies_multiply;
                $data->is_all_branch = $request->all_branch;
                $data->is_type = $request->is_type;

                if($request->is_type == 1){
                    $data->select_product_by = $request->select_product_by;
                }

                if($request->bonus_type == 1){
                    $data->max_promo_value = strtr($request->max_promo_value, array('.' => '', ',' => '.'));
                }
            }

            $data->start_date=$request->start_date;
            $data->end_date=$request->end_date;
            $data->foto=$fileName;
            $data->is_active=$request->is_active;
            $data->is_all_customer = $request->is_all_customer;
            $data->is_specific_customer = $request->is_specific_customer;
            $data->customer_terms = $request->req_customer_terms;
            $data->is_daily = $request->is_daily;

            if($request->is_daily == 1)
            {
                $data->start_clock = '00:00';
                $data->end_clock = '23:59';
            } else {
                $data->start_clock = $request->start_clock;
                $data->end_clock = $request->end_clock;
            }

            $data->save();

            $details=[];

            if($request->is_type == 1){
                if(is_null($id)){
                    if($request->select_product_by == 1){
                        foreach($product as $key => $item){
                            if($item->quantity <= 0){
                                return response()->json([
                                    'message'=>'Minimal pembelian produk tidak boleh kurang dari 0',
                                    'type'=>'warning',
                                    'is_modal'=>true,
                                    'redirect_url'=>''
                                ]);
                            }

                            $details[]=[
                                'discount_by_product_id'=>$data->id,
                                'sc_product_id'=>$item->id,
                                'quantity'=>$item->quantity,
                                'md_merchant_id' => merchant_id(),
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                ];
                        }
                        DB::table('crm_discount_by_product_details')
                        ->insert($details);
                    }

                    if($request->select_product_by == 2){
                        $categoryIds = [];
                        $reqDetails = json_decode($request->details);

                        foreach($reqDetails as $key => $item){
                            $categoryIds[] = $item->id;
                        }

                        $prod = Product::whereIn("sc_product_category_id", $categoryIds)
                                        ->where('md_user_id', user_id())
                                        ->where('is_deleted', 0)
                                        ->get();

                        foreach($reqDetails as $key => $item){
                            foreach($prod as $k => $i){
                                if($i->sc_product_category_id == $item->id){
                                    if($item->quantity <= 0){
                                        return response()->json([
                                            'message'=>'Minimal pembelian produk tidak boleh kurang dari 0',
                                            'type'=>'warning',
                                            'is_modal'=>true,
                                            'redirect_url'=>''
                                        ]);
                                    }

                                    $details[] = [
                                        'discount_by_product_id'=>$data->id,
                                        'sc_product_id'=>$i->id,
                                        'quantity'=>$item->quantity,
                                        'md_merchant_id' => merchant_id(),
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'),
                                    ];
                                }
                            }
                        }

                        $data->select_product_by_value = $request->details;
                        $data->save();

                        DB::table('crm_discount_by_product_details')
                        ->insert($details);

                    }

                    if($request->select_product_by == 3){
                        $merkIds = [];
                        $reqDetails = json_decode($request->details);

                        foreach($reqDetails as $key => $item){
                            $merkIds[] = $item->id;
                        }

                        $prod = Product::whereIn("sc_merk_id", $merkIds)
                                        ->where('md_user_id', user_id())
                                        ->where('is_deleted', 0)
                                        ->get();

                        foreach($reqDetails as $key => $item){
                            foreach($prod as $k => $i){
                                if($i->sc_merk_id == $item->id){
                                    if($item->quantity <= 0){
                                        return response()->json([
                                            'message'=>'Minimal pembelian produk tidak boleh kurang dari 0',
                                            'type'=>'warning',
                                            'is_modal'=>true,
                                            'redirect_url'=>''
                                        ]);
                                    }
                                    $details[] = [
                                        'discount_by_product_id'=>$data->id,
                                        'sc_product_id'=>$i->id,
                                        'quantity'=>$item->quantity,
                                        'md_merchant_id' => merchant_id(),
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'),
                                    ];
                                }
                            }
                        }

                        $data->select_product_by_value = $request->details;
                        $data->save();

                        DB::table('crm_discount_by_product_details')
                        ->insert($details);
                    }
                }
            }

            if($data->is_all_branch == 1){
                $tmpBranch = get_cabang();
                $assign_to_branch = [];
                foreach($tmpBranch as $k => $i){
                    if($i->id != $data->md_merchant_id){
                        array_push($assign_to_branch, $i->id);
                    }
                }

            } else {
                $assign_to_branch = json_decode($request->assign_to_branch);
            }



            if(is_null($id)){
                if(count($assign_to_branch) > 0){
                    $data->assign_to_branch = json_encode($assign_to_branch);
                    $data->save();
                }
            }

            if(count($assign_to_branch) > 0 && is_null($id))
            {
                $branchInsert = $this->_branchInsert($data, $assign_to_branch);
                if($branchInsert["status"] ==  false){
                    return response()->json([
                        'message'=>$branchInsert["message"],
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }
            }

            // ambil data dari discount product detail lalu masukkan ke json_product_result
            if(is_null($id) && $request->is_type == 1){
                $jsonProduct = DiscountProductDetail::select([
                    "crm_discount_by_product_details.discount_by_product_id",
                    "crm_discount_by_product_details.sc_product_id",
                    "crm_discount_by_product_details.quantity",
                    "crm_discount_by_product_details.md_merchant_id"
                ])
                ->where('discount_by_product_id', $data->id)
                ->get();

                $jsonProductResult = [];
                
                foreach($jsonProduct as $key => $item){
                    $jsonProductResult[$item->sc_product_id] = [
                                                            "quantity"=>(int)$item->quantity,
                                                            "sc_product_id" => $item->sc_product_id,
                                                            "md_merchant_id" => $item->md_merchant_id,
                                                            "discount_by_product_id" => $item->discount_by_product_id
                                                        ];
                }
                
                $data->json_product_result = json_encode($jsonProductResult);
                $data->save();
            }

            DB::commit();
            return response()->json([
                'message'=>'Promo berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.discount-product.index')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    protected function _branchInsert($data, $branch)
    {
        try {

            $start= $data->start_date;
            $end = $data->end_date;

            $productCode = [];
            $promoDetails = DiscountProductDetail::select([
                "crm_discount_by_product_details.*",
                "sp.id as product_id",
                "sp.name as product_name",
                "sp.code"
            ])
            ->join('sc_products as sp', 'sp.id', 'crm_discount_by_product_details.sc_product_id')
            ->where('discount_by_product_id', $data->id)
            ->get();

            foreach($promoDetails as $key => $item)
            {
                array_push($productCode, $item->code);
            }

            foreach($branch as $branchId)
            {
                $merchant = Merchant::find($branchId);
                $product = Product::whereIn("code", $productCode)
                                    ->where("md_user_id", $merchant->md_user_id)
                                    ->get();

                $code = DiscountProductEntity::where('code', $data->code)
                                            ->where('md_merchant_id',$merchant->id)
                    ->where('is_referral_affiliate',0)
                    ->where('is_coupon',0)
                    ->where('is_deleted',0)
                                            ->get();

                if(count($code)>0){
                    return [
                        "status" => false,
                        "message" => 'Kode sudah digunakan !'
                    ];
                }


                if($data->is_type == 1){
                    foreach($product as $item){
                        $promoActive=DiscountProductEntity::join('crm_discount_by_product_details as d','d.discount_by_product_id','crm_discount_by_products.id')
                                            ->where('is_deleted',0)
                            ->where('is_referral_affiliate',0)
                            ->where('is_coupon',0)
                            ->where('is_active',1)
                                            ->where('crm_discount_by_products.md_merchant_id',$merchant->id)
                                            ->where(function($q) use($merchant){
                                                $q->where('crm_discount_by_products.md_merchant_id', $merchant->id)
                                                    ->orWhereJsonContains('crm_discount_by_products.assign_to_branch', [$merchant->id]);
                                            })
                                            ->where('d.sc_product_id',$item->id)
                                            ->where(function($q) use($start,$end){
                                                $q->where([['start_date','<=',$start],
                                                            ['end_date','>=',$start]])
                                                    ->orWhere([['start_date','>=',$start],
                                                                ['start_date','<=',$end]]);
                                            })
                                            ->get();

                        if(count($promoActive)>0){
                            return [
                                "status" => false,
                                "message" => 'Ada promo untuk produk '.$item->name.' pada jangka waktu ini di cabang '.$merchant->name
                            ];
                        }
                    }

                    // cek apakah ada promo tipe transaksi
                    $promoTrans=DiscountProductEntity::where('is_active',1)
                        ->where('is_deleted',0)
                        ->where('is_referral_affiliate',0)
                        ->where('is_coupon',0)
                        ->where('is_type', 2)
                        ->where('md_merchant_id',$merchant->id)
                        ->where('md_merchant_id',$merchant->id)
                        ->where(function($q) use($merchant){
                            $q->where('md_merchant_id', $merchant->id)
                                ->orWhereJsonContains('assign_to_branch', [$merchant->id]);
                        })
                        ->where(function($q) use($start,$end){
                            $q->where([['start_date','<=',$start],
                                        ['end_date','>=',$start]])
                                ->orWhere([['start_date','>=',$start],
                                            ['start_date','<=',$end]]);
                        })
                        ->get();

                    if(count($promoTrans)>0){
                        return [
                            "status" => false,
                            "message" => 'Ada promo pada jangka waktu ini di cabang '.$merchant->name
                        ];
                    }
                } else {
                    $promoActive=DiscountProductEntity::where('is_active',1)
                                                ->where('is_deleted',0)
                        ->where('is_referral_affiliate',0)
                        ->where('is_coupon',0)
                        ->where('md_merchant_id',$merchant->id)
                                                ->where('md_merchant_id',$merchant->id)
                                                ->where(function($q) use($merchant){
                                                    $q->where('md_merchant_id', $merchant->id)
                                                        ->orWhereJsonContains('assign_to_branch', [$merchant->id]);
                                                })
                                                ->where(function($q) use($start,$end){
                                                    $q->where([['start_date','<=',$start],
                                                                ['end_date','>=',$start]])
                                                        ->orWhere([['start_date','>=',$start],
                                                                    ['start_date','<=',$end]]);
                                                })
                                                ->get();
                    if(count($promoActive)>0){
                        return [
                            "status" => false,
                            "message" => 'Ada promo pada jangka waktu ini untuk cabang '.$merchant->name
                        ];
                    }
                }


                if($data->bonus_type == 3)
                {
                    $bonusProductDecode = json_decode($data->bonus_value);
                    $bonusValue = json_decode($data->bonus_value);

                    foreach($bonusProductDecode as $key => $item)
                    {
                        $bonusProduct = Product::where("code", $item->code)
                                            ->where("md_user_id", $merchant->md_user_id)
                                            ->first();

                        array_push($bonusValue, [
                            "md_merchant_id" => $merchant->id,
                            "code"=> $bonusProduct->code,
                            "foto" => $bonusProduct->image,
                            "name" => $bonusProduct->name,
                            "value" => $bonusProduct->id,
                            "quantity" => $item->quantity,
                            "description" => $item->description,
                            "is_with_stock" => $item->is_with_stock,
                            "selling_price" => $bonusProduct->selling_price
                        ]);
                    }

                    $data->bonus_value = json_encode($bonusValue);
                    $data->save();
                }

                if($data->promo_type == 2)
                {
                    $details = [];
                    foreach($promoDetails as $key => $item){
                        $branchProduct = Product::where('code', $item->code)
                                                ->where('md_user_id', $merchant->md_user_id)
                                                ->first();

                        $details[]=[
                            'discount_by_product_id'=>$data->id,
                            'sc_product_id'=>$branchProduct->id,
                            'quantity'=>$item->quantity,
                            'md_merchant_id' => $merchant->id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'),
                            ];
                    }
                    DB::table('crm_discount_by_product_details')
                    ->insert($details);
                }
            }

            return [
                "status" => true,
                "message" => "Data berhasil disimpan !"
            ];

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return [
                "status" => false,
                "message" => "Terjadi kesalahan saat menyimpan promo di cabang !"
            ];
        }
    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=DiscountProductEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            DB::commit();
            return response()->json([
                'message'=>'Promo berdasarkan produk berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }



    public function reloadData(Request $request)
    {

        $searchParams = [];
        foreach (DiscountProductEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Promo Per Produk',
            'searchKey' => $searchKey,
            'tableColumns'=>DiscountProductEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.master-data.discount-product.list',$params);

    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantMenu;
use App\Models\MasterData\TransactionType;
use App\Models\MasterData\VaBank;
use App\Models\QRIS\RajabillerBank;
use App\Models\QRIS\QRISActivation;
use App\Models\SennaToko\PaymentMethod;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Utils\Accounting\CoaUtil;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\QRIS\QRISActivationEntity;

class PaymentMethodController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/payment-method')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::post('/save', 'Toko\PaymentMethodController@save')
                    ->name('merchant.toko.payment-method.save');
                Route::post('/payment', 'Toko\PaymentMethodController@getPayment')
                    ->name('merchant.toko.payment-method.get-payment');
                Route::post('/save-bank', 'Toko\PaymentMethodController@saveBankTransfer')
                    ->name('merchant.toko.payment-method.save-bank');
                Route::post('/update-bank-status', 'Toko\PaymentMethodController@updateStatusBank')
                    ->name('merchant.toko.payment-method.update-bank-status');
                Route::post('/update-bank-detail', 'Toko\PaymentMethodController@updateDetailBank')
                    ->name('merchant.toko.payment-method.update-bank-detail');
                Route::post('/add', 'Toko\PaymentMethodController@addBankTransfer')
                    ->name('merchant.toko.bank-transfer.add');
            });
    }
    public function saveBankTransfer(Request $request)
    {
        try{
            DB::beginTransaction();
            $bank_id = explode('_', $request->bank_id);

            $data=PaymentMethod::where('md_va_bank_id',$bank_id[0])
                ->where('transaction_type_id',$request->type)
                ->where('md_merchant_id',merchant_id())
                ->where('bank_account_number',$request->bank_account_number)
                ->first();
            if(!is_null($data)){
                return response()->json([
                    'message'=>'Metode pembayaran bank tersebut sudah ada!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            $bank=RajabillerBank::find($bank_id[0]);
            $checkQris=QRISActivation::where('bank_account_number',$request->bank_account_number)->where('md_merchant_id',merchant_id())->where('is_active',1)->where('is_agree',1)->first();

            if(is_null($request->add_new_coa) && is_null($request->coa_name)){
                $coa_name=$bank->name.'-'.$request->bank_account_number;
            }else{
                $coa_name=$request->coa_name;
            }
            if(!is_null($checkQris)){
                $coa_detail=CoaDetail::find($checkQris->qrisbank_qris_id);
                $coa_detail->name=$coa_name;
                $coa_detail->save();
            }else{
                if($request->acc_coa_id!=-1 && !is_null($request->add_new_coa)){
                    $acc_coa_id = explode('_', $request->acc_coa_id);
                    $coa_detail=CoaDetail::find($acc_coa_id[0]);
                    $coa_detail->name=$coa_name;
                    $coa_detail->save();
                }else{
                    $coa_detail=CoaDetail::where('acc_coa_details.name',$coa_name)
                    ->whereIn('acc_coa_details.is_deleted',[0,2])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',merchant_id())
                    ->first();
                    if(is_null($coa_detail)){
                        $coaCategory=CoaCategory::where('code','1.1.02')->where('md_merchant_id',merchant_id())->first();
                        $coaDetailCode=CoaUtil::generateCoaDetail($coaCategory->id);

                        $coa_detail=new CoaDetail();
                        $coa_detail->code=$coaDetailCode;
                        $coa_detail->name=$coa_name;
                        $coa_detail->acc_coa_category_id=$coaCategory->id;
                        $coa_detail->is_deleted=2;
                        $coa_detail->md_sc_currency_id=1;
                        $coa_detail->type_coa='Debit';
                        $coa_detail->save();
                    }
                }
            }

            $data=new PaymentMethod();

            $data->md_va_bank_id=$bank_id[0];
            $data->md_merchant_id=merchant_id();
            $data->bank_account_number=$request->bank_account_number;
            $data->bank_account_name=$request->bank_account_name;
            $data->payment_name=$coa_detail->name;
            $data->transaction_type_id=$request->type;
            $data->status=1;
            $data->acc_coa_id=$coa_detail->id;
            $data->acc_coa_code=$coa_detail->code;
            $data->save();

            DB::commit();
            return response()->json([
                'message'=>'Metode pembayaran berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);


        }
    }
    public  function addBankTransfer(Request $request){

        $data=PaymentMethod::select(['md_merchant_payment_methods.*','coa.name'])
                            ->where('md_merchant_payment_methods.id',$request->id)
                            ->join('acc_coa_details as coa','coa.id','md_merchant_payment_methods.acc_coa_id')
                            ->first();
        $acc_coa=CoaDetail::select(['acc_coa_details.name','acc_coa_details.code','acc_coa_details.id'])
                        ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                        ->where('acc_coa_details.code','like','%1.1.02%')
                        ->whereIn('acc_coa_details.is_deleted',([0,2]))
                        ->where('c.md_merchant_id',merchant_id())
                        ->whereNotIn('acc_coa_details.name',['Gopay','Ovo','Dana','ShopeePay','Cashlez','LinkAja','Payment Gateway'])
                        ->get();
        $params=[
            'bank'=>RajabillerBank::get(),
            'data'=>$data,
            'acc_coa'=>$acc_coa
        ];

        if(is_null($request->id)){
            return view('merchant::toko.profile.add-bank',$params);
        }else{
            return view('merchant::toko.profile.detail-bank',$params);
        }
    }

    public function updateDetailBank(Request $request)
    {
        try{

            $data=PaymentMethod::find($request->id);
            $bank_id = explode('_', $request->bank_id);
            // dd($request->id);
            $bank=RajabillerBank::find($bank_id[0]);

            $coa = explode('_', $request->acc_coa_id);

            $coa_detail=CoaDetail::find($coa[0]);
            $coa_detail->name=$request->coa_name;
            $coa_detail->save();

            $data->md_va_bank_id=$bank->id;
            $data->bank_account_number=$request->bank_account_number;
            $data->bank_account_name=$request->bank_account_name;
            $data->payment_name=$coa_detail->name;
            $data->acc_coa_id=$coa_detail->id;
            $data->acc_coa_code=$coa_detail->code;
            $data->save();


            return response()->json([
                'message'=>'Metode pembayaran berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);


        }
    }

    public function updateStatusBank(Request $request)
    {
        try{
            // dd($request->all());
            $data=PaymentMethod::find($request->id);

            $data->status=$request->is_active;
            $data->save();


            return response()->json([
                'message'=>'Metode pembayaran berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }

    public function save(Request $request)
    {
        try{
            $data=PaymentMethod::where('md_va_bank_id',$request->md_va_bank_id)->where('transaction_type_id',$request->type)->where('md_merchant_id',merchant_id())->first();
            if(is_null($data)){
                $data=new PaymentMethod();
            }

            $coaCategory=CoaCategory::where('code','1.1.02')->where('md_merchant_id',merchant_id())->first();
            $coaDetailCode=CoaUtil::generateCoaDetail($coaCategory->id);

            $coa_qris=CoaDetail::where('name','QRIS')->where('acc_coa_category_id',$coaCategory->id)->whereIn('is_deleted',[0,2])->first();
            if(!is_null($coa_qris)){
                $coa_qris->name='Payment Gateway';
                $coa_qris->save();
                $data->acc_coa_id=$coa_qris->id;
                $data->acc_coa_code=$coa_qris->code;
            }else{
                $coaDetail=CoaDetail::where('name','Payment Gateway')->where('acc_coa_category_id',$coaCategory->id)->whereIn('is_deleted',[0,2])->first();
                if(is_null($coaDetail)){
                    $coa_detail=new CoaDetail();
                    $coa_detail->code=$coaDetailCode;
                    $coa_detail->name='Payment Gateway';
                    $coa_detail->acc_coa_category_id=$coaCategory->id;
                    $coa_detail->is_deleted=0;
                    $coa_detail->md_sc_currency_id=1;
                    $coa_detail->type_coa='Debit';
                    $coa_detail->save();
                    $data->acc_coa_id=$coa_detail->id;
                    $data->acc_coa_code=$coa_detail->code;
                }else{
                    $data->acc_coa_id=$coaDetail->id;
                    $data->acc_coa_code=$coaDetail->code;
                }

            }

            // dd($coaDetail);

            if($request->type==2){

                $coaTunai=CoaDetail::
                select([
                    'acc_coa_details.id',
                    'acc_coa_details.code'
                ])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',merchant_id())
                    ->where('acc_coa_details.code','1.1.01.01')
                    ->first();

                $data->acc_coa_id=$coaTunai->id;
                $data->acc_coa_code=$coaTunai->code;
            }


            $data->md_va_bank_id=$request->md_va_bank_id;
            $data->md_merchant_id=merchant_id();
            $data->transaction_type_id=$request->type;
            $data->status=$request->is_active;
            $data->save();

            return response()->json([
                'message'=>'Metode pembayaran berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }

    public function getPayment(Request  $request)
    {

        try{

            $paymentMethod=PaymentMethod::where('md_merchant_id',merchant_id())
                ->get();


            $params=[
                'paymentMethod'=>$paymentMethod,
                'row'=>QRISActivationEntity::where('md_merchant_id',merchant_id())
                    ->first(),
            ];

            return response()->json([
                'code'=>200,
                'message'=>'Pengambilan data berhasil',
                'data'=>$params
            ]);

        }catch (\Exception $e){
            return response()->json([
                'code'=>500,
                'message'=>'Terjadi kesalahan saat pengambilan data',
                'data'=>[]
            ]);
        }



    }


}

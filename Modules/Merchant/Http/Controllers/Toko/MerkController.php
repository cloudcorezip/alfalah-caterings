<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantMenu;
use App\Models\SennaToko\Merk;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;

class MerkController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/merk')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\MerkController@index')
                    ->name('merchant.toko.merk.index');
                Route::post('/data-table', 'Toko\MerkController@dataTable')
                    ->name('merchant.toko.merk.datatable');
                Route::post('/add', 'Toko\MerkController@add')
                    ->name('merchant.toko.merk.add');
                Route::post('/save', 'Toko\MerkController@save')
                    ->name('merchant.toko.merk.save');
                Route::post('/delete', 'Toko\MerkController@delete')
                    ->name('merchant.toko.merk.delete');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Merek Produk',
            'tableColumns'=>MerkEntity::dataTableColumns(),
            'add'=>(get_role()==3)?MerkEntity::add(true):
                MerkEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.merk.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return MerkEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=MerkEntity::find($id);
        }else{
            $data=new MerkEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];
        if(isset($request->no_reload)){
            return view('merchant::toko.master-data.merk.form-no-reload',$params);

        }else{
            return view('merchant::toko.master-data.merk.form',$params);

        }
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=MerkEntity::find($id);
            }else{
                $data=new MerkEntity();
                $data->md_user_id=user_id();

            }

            $data->name=$request->name;
            $data->description=$request->description;
            $data->save();


            $noReload=$request->no_reload;
            if(is_null($noReload)){
                $isReload=true;
            }else{
                if($noReload==1){
                    $isReload=false;
                }else{
                    $isReload=true;
                }
            }



            return response()->json([
                'message'=>'Merk produk berhasil disimpan',
                'type'=>'success',
                'is_modal'=>$isReload,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=MerkEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'Merk produk berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }
}

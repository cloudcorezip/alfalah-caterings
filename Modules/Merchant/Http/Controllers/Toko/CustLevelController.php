<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CustLevelEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;

class CustLevelController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/cust-level')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\CustLevelController@index')
                    ->name('merchant.toko.cust-level.index');
                Route::post('/data-table', 'Toko\CustLevelController@dataTable')
                    ->name('merchant.toko.cust-level.datatable');
                Route::post('/add', 'Toko\CustLevelController@add')
                    ->name('merchant.toko.cust-level.add');
                Route::post('/save', 'Toko\CustLevelController@save')
                    ->name('merchant.toko.cust-level.save');
                Route::post('/delete', 'Toko\CustLevelController@delete')
                    ->name('merchant.toko.cust-level.delete');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Kategori Pelanggan',
            'tableColumns'=>CustLevelEntity::dataTableColumns(),
            'add'=>(get_role()==3)?CustLevelEntity::add(true):
                CustLevelEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.cust-level.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return CustLevelEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=CustLevelEntity::find($id);
        }else{
            $data=new CustLevelEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::toko.master-data.cust-level.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=CustLevelEntity::find($id);
            }else{
                $data=new CustLevelEntity();
                $data->md_user_id=user_id();
            }

            $check = CustLevelEntity::where("code", $request->code)
                                    ->where('is_deleted', 0)
                                    ->where('md_user_id', user_id())
                                    ->where('id', '!=', $id)
                                    ->first();

            if(!is_null($check) && !is_null($request->code)){
                return response()->json([
                    'message'=>'Kode sudah digunakan !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            $data->code = $request->code;
            $data->name=$request->name;
            $data->description=$request->description;
            $data->save();

            return response()->json([
                'message'=>'Kategori pelanggan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=CustLevelEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'Kategori pelanggan berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\WarehouseTypeEntity;

class WarehouseTypeController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('master-data/warehouse-type')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.warehouse-type.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.warehouse-type.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.toko.warehouse-type.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.warehouse-type.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.warehouse-type.delete');
            });
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Tipe Gudang',
            'tableColumns'=>WarehouseTypeEntity::dataTableColumns(),
            'add'=>(get_role()==3)?WarehouseTypeEntity::add(true):
                WarehouseTypeEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.warehouse-type.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return WarehouseTypeEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public function add(Request $request)
    {
        $id=$request->id;
        if(!is_null($id)){

            $data=WarehouseTypeEntity::find($id);
        }else{
            $data=new WarehouseTypeEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::toko.master-data.warehouse-type.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=WarehouseTypeEntity::find($id);
            }else{
                $data=new WarehouseTypeEntity();
                $data->md_merchant_id=merchant_id();

            }

            $checkPackageSubscription=MerchantUtil::checkUrl(merchant_id());

            if($checkPackageSubscription['status']==true)
            {
                return response()->json([
                    'message'=>$checkPackageSubscription['message'],
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $data->name=$request->name;
            $data->desc=$request->desc;
            $data->save();

            return response()->json([
                'message'=>'Tipe gudang berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=WarehouseTypeEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'Tipe gudang berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }
}

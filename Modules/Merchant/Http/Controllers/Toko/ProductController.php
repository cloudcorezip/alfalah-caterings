<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Models\MasterData\MerchantShopCategory;
use App\Models\MasterData\SennaCashier\ProductType;
use App\Models\MasterData\Varian;
use App\Models\MasterData\Unit;
use App\Models\Accounting\CoaMapping;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\ProductSellingLevel;
use App\Models\SennaToko\MultiUnit;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\ProductionOfGoodDetail;
use App\Models\Accounting\CoaDetail;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\Merchant\ProductUtil;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\MultiVarian;
use App\Utils\App\AppUtil;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\ProductEntity;
use Modules\Merchant\Entities\Toko\BranchEntity;
use Image;
use Modules\Merchant\Entities\Toko\ProductRawMaterialEntity;
use Modules\Merchant\Entities\Toko\ProductMultiUnitEntity;
use Modules\Merchant\Entities\Toko\ProductSellingLevelEntity;
use Modules\Merchant\Entities\Toko\ProductMultiVarianEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Psy\Util\Str;
use Ramsey\Uuid\Uuid;

class ProductController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('master-data/product')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\ProductController@index')
                    ->name('merchant.toko.product.index');
                Route::post('/data-table', 'Toko\ProductController@dataTable')
                    ->name('merchant.toko.product.datatable');
                Route::post('/data-table-stock', 'Toko\ProductController@dataTableStock')
                    ->name('merchant.toko.product.datatable-stock');
                Route::get('/add', 'Toko\ProductController@add')
                    ->name('merchant.toko.product.add');
                Route::get('/detail/{id}', 'Toko\ProductController@detail')
                    ->name('merchant.toko.product.detail');
                Route::post('/save', 'Toko\ProductController@save')
                    ->name('merchant.toko.product.save');
                Route::post('/save-stock', 'Toko\ProductController@saveStock')
                    ->name('merchant.toko.product.save-stock');
                Route::post('/delete', 'Toko\ProductController@delete')
                    ->name('merchant.toko.product.delete');
                Route::post('/form-import', 'Toko\ProductController@formImport')
                    ->name('merchant.toko.product.form-import');
                Route::post('/save-import', 'Toko\ProductController@saveImport')
                    ->name('merchant.toko.product.save-import');
                Route::post('/export-data', 'Toko\ProductController@exportData')
                    ->name('merchant.toko.product.export-data');
                Route::post('/export-global-category', 'Toko\ProductController@exportDataGlobalCategory')
                    ->name('merchant.toko.product.export-global-category');
                Route::post('/reload-data', 'Toko\ProductController@reloadData')
                    ->name('merchant.toko.product.reload-data');

                Route::post('/add-price-type', 'Toko\ProductController@addPriceType')
                    ->name('merchant.toko.product.selling-price.add');
                Route::post('/save-price-type', 'Toko\ProductController@savePriceType')
                    ->name('merchant.toko.product.selling-price.save');
                Route::post('/data-table-price-type', 'Toko\ProductController@dataTablePriceType')
                    ->name('merchant.toko.product.selling-price.datatable');
                Route::post('/delete-price-type', 'Toko\ProductController@deletePriceType')
                    ->name('merchant.toko.product.selling-price.delete');

                Route::post('/delete-product-photos', 'Toko\ProductController@deleteProductPhotos')
                    ->name('merchant.toko.product.delete-product-photo');

                Route::post('/add-multi-unit', 'Toko\ProductController@addMultiUnit')
                    ->name('merchant.toko.product.multi-unit.add');
                Route::post('/save-multi-unit', 'Toko\ProductController@saveMultiUnit')
                    ->name('merchant.toko.product.multi-unit.save');
                Route::post('/data-table-multi-unit', 'Toko\ProductController@dataTableMultiUnit')
                    ->name('merchant.toko.product.multi-unit.datatable');
                Route::post('/delete-multi-unit', 'Toko\ProductController@deleteMultiUnit')
                    ->name('merchant.toko.product.multi-unit.delete');

                //bahan baku
                Route::post('/add-raw-material', 'Toko\ProductController@addRawMaterial')
                    ->name('merchant.toko.product.raw-material.add');
                Route::post('/save-raw-material', 'Toko\ProductController@saveRawMaterial')
                    ->name('merchant.toko.product.raw-material.save');
                Route::post('/data-table-raw-material', 'Toko\ProductController@dataTableRawMaterial')
                    ->name('merchant.toko.product.raw-material.datatable');
                Route::post('/delete-raw-material', 'Toko\ProductController@deleteRawMaterial')
                    ->name('merchant.toko.product.raw-material.delete');

                // multi varian
                Route::post('/add-multi-varian', 'Toko\ProductController@addMultiVarian')
                    ->name('merchant.toko.product.multi-varian.add');
                Route::post('/save-multi-varian', 'Toko\ProductController@saveMultiVarian')
                    ->name('merchant.toko.product.multi-varian.save');
                Route::post('/data-table-multi-varian', 'Toko\ProductController@dataTableMultiVarian')
                    ->name('merchant.toko.product.multi-varian.datatable');
                Route::post('/delete-multi-varian', 'Toko\ProductController@deleteMultiVarian')
                    ->name('merchant.toko.product.multi-varian.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Produk',
            'tableColumns'=>ProductEntity::dataTableColumns(),
            'add'=>(get_role()==3)?ProductEntity::add(true):
                ProductEntity::add(true),
            'key_val'=>$request->key,
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'sc_product_category_id'=>CategoryEntity::listOption(),
            'productType'=>ProductType::whereNotIn('id',[2,4])->get()
        ];

        return view('merchant::toko.master-data.product.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ProductEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=ProductEntity::find($id);
        }else{
            $data=new ProductEntity();
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'category'=>CategoryEntity::listOption(),
            'productType'=>ProductType::whereNotIn('id',[2,4])->get(),
            'code'=>CodeGenerator::generalCode('PR',merchant_id()),
            'merk'=>MerkEntity::listOption(),
            'unit'=>Unit::all(),
            //'varian' => Varian::where('is_deleted', 0)->get(),
            'raw'=> Product::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_products.id)'),
                'sc_products.id',
                'sc_products.name',
                'u.name as unit_name'
            ])
                ->where('sc_products.md_user_id',user_id())
                ->join('sc_product_categories as c','c.id','sc_products.sc_product_category_id')
                ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->where('sc_products.is_deleted',0)
                ->where('sc_products.is_with_stock',1)
                ->where('sc_products.md_sc_product_type_id',8)
                ->orderBy('id','desc')
                ->get(),
            'is_goods'=>$request->is_goods
        ];


        return view('merchant::toko.master-data.product.form2',$params);
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ProductEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Produk',
            'searchKey' => $searchKey,
            'tableColumns'=>ProductEntity::dataTableColumns(),
            'key_val'=>$request->key

        ];
        return view('merchant::toko.master-data.product.list',$params);

    }

    public  function addPriceType(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=ProductSellingLevel::find($id);
        }else{
            $data=new ProductSellingLevel();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'sc_product_id'=>$request->sc_product_id
        ];
        return view('merchant::toko.master-data.product.form-custom.price-type',$params);
    }

    public function savePriceType(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=ProductSellingLevel::find($id);
            }else{
                $data=new ProductSellingLevel();
            }
            $product = Product::where('id', $request->sc_product_id)->first();

            $selling_price=strtr($request->selling_price, array('.' => '', ',' => '.'));
            $data->level_name=$request->level_name;
            $data->selling_price=$selling_price;
            $data->is_deleted=0;
            $data->sc_product_id=$request->sc_product_id;
            $data->save();

            if(!is_null($data->assign_to)){
                foreach(json_decode($data->assign_to) as $key => $a){
                    $branch_sl=ProductSellingLevel::find($a);
                    $branch_sl->level_name=$request->level_name;
                    $branch_sl->selling_price=$selling_price;
                    $branch_sl->is_deleted=0;
                    $branch_sl->save();
                }
            }

            if(is_null($id) && !is_null($product->assign_to_product) ){
                $jp=json_decode($product->assign_to_product);
                $jml=json_decode($data->assign_to);

                foreach($jp as $key => $m){
                    $branch_sl = new ProductSellingLevel();
                    $branch_sl->level_name=$request->level_name;
                    $branch_sl->selling_price=$selling_price;
                    $branch_sl->sc_product_id=$m;
                    $branch_sl->is_deleted=0;
                    $branch_sl->save();

                    $jml[]=$branch_sl->id;
                    $data->assign_to=$jml;

                }
                $data->save();

            }

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public  function addMultiUnit(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=MultiUnit::find($id);
        }else{
            $data=new MultiUnit();
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'unit'=>Unit::all(),
            'product'=>Product::with('getUnit')->where('id',$request->sc_product_id)->first(),
        ];
        return view('merchant::toko.master-data.product.form-custom.multi-unit',$params);
    }

    public function saveMultiUnit(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $exsistUnit = MultiUnit::where('md_unit_id', $request->md_unit_id)
                            ->where('sc_product_id', $request->sc_product_id)
                            ->where('is_deleted', 0)
                            ->first();
            $product = Product::where('id', $request->sc_product_id)->first();

            $isUnit = MultiUnit::where('sc_product_id', $request->sc_product_id)
                            ->where('is_deleted', 0)
                            ->get();

            if(!is_null($id)){
                $data=MultiUnit::find($id);
                if(!is_null($exsistUnit) && $data->md_unit_id != $request->md_unit_id){
                    return response()->json([
                        'message' => "Satuan sudah digunakan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }else{
                $data=new MultiUnit();
                if(!is_null($exsistUnit)){
                    return response()->json([
                        'message' => "Satuan sudah digunakan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }

            if(is_null($id) && count($isUnit) < 1){
                $defaultUnit = new MultiUnit();
                $defaultUnit->md_unit_id = $product->md_unit_id;
                $defaultUnit->price = $product->selling_price;
                $defaultUnit->quantity = 1;
                $defaultUnit->sc_product_id = $product->id;
                $defaultUnit->save();
            }

            if(isset($request->is_default_unit)){
                $product->md_unit_id = $request->md_unit_id;
                $product->save();
            }


            $price=strtr($request->price, array('.' => '', ',' => '.'));
            $data->md_unit_id=$request->md_unit_id;
            $data->price=$price;
            $data->quantity=$request->quantity;
            $data->sc_product_id=$request->sc_product_id;
            $data->save();
            // dd($data->id);
            if(is_null($id) && !is_null($product->assign_to_product) ){
                $jp=json_decode($product->assign_to_product);
                $jmu=json_decode($data->assign_to);

                foreach($jp as $key => $m){
                    $branch_mu = new MultiUnit();
                    $branch_mu->md_unit_id=$request->md_unit_id;
                    $branch_mu->price=$price;
                    $branch_mu->quantity=$request->quantity;
                    $branch_mu->sc_product_id = $m;
                    $branch_mu->save();

                    $jmu[]=$branch_mu->id;
                    $data->assign_to=$jmu;

                }
                $data->save();

                // dd($data->assign_to);
            }

            if(!is_null($data->assign_to)){
                foreach($data->assign_to as $key => $a ){

                        $branch_mu=MultiUnit::find($a);
                        $branch_mu->md_unit_id=$request->md_unit_id;
                        $branch_mu->price=$price;
                        $branch_mu->quantity=$request->quantity;
                        $branch_mu->save();

                    // array_push(json_decode($product->assign_to_product),$a);

                }


            }

            DB::commit();
            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public  function addRawMaterial(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=Material::find($id);
        }else{
            $data=new Material();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'sc_product_id'=>$request->sc_product_id,
            'raw'=> Product::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY sc_products.id)'),
                'sc_products.id',
                'sc_products.name',
                'u.name as unit_name'
            ])
                ->where('sc_products.md_user_id',user_id())
                ->join('sc_product_categories as c','c.id','sc_products.sc_product_category_id')
                ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->where('sc_products.is_deleted',0)
                ->where('sc_products.is_with_stock',1)
                ->where('sc_products.md_sc_product_type_id',3)
                ->orderBy('id','desc')
                ->get(),
        ];
        return view('merchant::toko.master-data.product.form-custom.raw-material',$params);
    }

    public function saveRawMaterial(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $exsistMaterial = Material::where('parent_sc_product_id', $request->sc_product_id)
                            ->where('child_sc_product_id', $request->child_sc_product_id)
                            ->where('is_deleted', 0)
                            ->first();

            if(!is_null($id)){
                $data=Material::find($id);
                if(!is_null($exsistMaterial) && $data->child_sc_product_id != $request->child_sc_product_id){
                    return response()->json([
                        'message' => "Bahan baku sudah digunakan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

            }else{
                $data=new Material();
                if(!is_null($exsistMaterial)){
                    return response()->json([
                        'message' => "Bahan baku sudah digunakan !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }

            $data->quantity=$request->quantity;
            $data->is_deleted=0;
            $data->parent_sc_product_id=$request->sc_product_id;
            $data->child_sc_product_id=$request->child_sc_product_id;
            $data->save();
            $product = Product::where('id', $request->sc_product_id)->first();

            if(is_null($id) && !is_null($product->assign_to_product) ){
                $jp=json_decode($product->assign_to_product);
                $jml=json_decode($data->assign_to);

                foreach($jp as $key => $m){
                    $child = Product::find($request->child_sc_product_id);
                    $checkChild = Product::whereRaw("lower(code) iLIKE '%".strtolower($child->code)."%' ")
                                        ->where('md_user_id',$child->md_user_id)
                                        ->where('is_deleted',0)
                                        ->first();
                    if(!is_null($checkChild)){
                        $childId=$checkChild->id;
                    }else{
                        $newChild = $child->replicate();
                        $newChild->md_user_id = $child->md_user_id;
                        $newChild->save();

                        $coaChild=CoaProductUtil::initialCoaProduct($child->getUser->getMerchant->id,$newChild,1);
                        if($coaChild==false)
                        {
                            return false;
                        }

                        $newChild->inv_id=$coaChild['inv_id'];
                        $newChild->hpp_id=$coaChild['hpp_id'];

                        $newChild->save();
                        $childId=$newChild->id;
                    }

                    $branch_raw = new Material();
                    $branch_raw->quantity=$request->quantity;
                    $branch_raw->is_deleted=0;
                    $branch_raw->parent_sc_product_id=$m;
                    $branch_raw->child_sc_product_id=$request->child_sc_product_id;
                    $branch_raw->save();

                    $jml[]=$branch_raw->id;
                    $data->assign_to=$jml;

                }
                $data->save();
            }

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server, cobalah beberapa saat lagi !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public  function addMultiVarian(Request $request){

        $id=$request->id;

        if(!is_null($id)){

            $data=MultiVarian::find($id);
        }else{
            $data=new MultiVarian();
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'varian' => Varian::where('is_deleted', 0)->get(),
            'product'=>Product::find($request->sc_product_id),
        ];

        return view('merchant::toko.master-data.product.form-custom.multi-varian',$params);
    }

    public function saveMultiVarian(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=MultiVarian::find($id);
            }else{
                $data=new MultiVarian();
            }

            $decodeMultiVarian = json_decode($request->multi_varian);

            if(count($decodeMultiVarian) == 0){
                return response()->json([
                    'message' => "Kamu belum memilih varian !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $arrVarianId = [];
            foreach($decodeMultiVarian as $key => $item){
                $arrVarianId[] = $item->id;
            }

            if(count($decodeMultiVarian) != count(array_unique($arrVarianId))){
                return response()->json([
                    'message' => "Varian produk tidak boleh ada yang sama !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $data->sc_product_id = $request->sc_product_id;
            $data->selling_price = strtr($request->selling_price, array('.' => '', ',' => '.'));
            $data->purchase_price = strtr($request->purchase_price, array('.' => '', ',' => '.'));
            $data->is_deleted = 0;
            $data->label = $request->label;
            $data->multi_varian = json_encode($decodeMultiVarian);
            $data->save();

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public  function detail(Request $request){

        $id=$request->id;
        $data=ProductEntity::find($id);

        if(is_null($data)){
            abort(404);
        }

        if($data->md_user_id != user_id()){
            return redirect()->route('pages.not-allowed');
        }

        $total=count(PurchaseOrderDetail::where('sc_product_id',$id)->get())+count(SaleOrderDetail::where('sc_product_id',$id)->get())+count(StockInventory::where('sc_product_id',$id)->get());
        $isUnit = MultiUnit::where('sc_product_id', $id)
            ->where('is_deleted', 0)
            ->get();
        $isPriceType = DB::table('sc_product_selling_levels')
                        ->selectRaw('COUNT(*) as total')
                        ->where('is_deleted', 0)
                        ->where('sc_product_id', $id)
                        ->first();
        $getBranch=MerchantUtil::getBranch(merchant_id(),1);

        $cost=CoaMapping::where('md_merchant_id',merchant_id())
            ->whereIn('is_deleted',[0])
            ->where('is_type',2)->get();
        $params=[
            'title'=>'Edit Data : '.$data->name,
            'page'=>is_null($request->page)?null:$request->page,
            'data'=>$data,
            'tableColumns'=>ProductEntity::dataTableColumnsStock(),
            'category'=>ProductCategory::select(['sc_product_categories.*','m.name as merchant_name'])
                ->join('md_merchants as m','m.md_user_id','sc_product_categories.md_user_id')
                ->whereIn('m.id',$getBranch)
                ->where('is_deleted',0)->get(),
            'merk'=>MerkEntity::select(['sc_merks.*','m.name as merchant_name'])
                ->join('md_merchants as m','m.md_user_id','sc_merks.md_user_id')
                ->whereIn('m.id', $getBranch)
                ->where('is_deleted', 0)->get(),
            'productType'=>ProductType::whereNotIn('id',[2,4])->get(),
            'tableColumnsPriceType'=>ProductSellingLevelEntity::dataTableColumns(),
            'add'=>ProductSellingLevelEntity::add($data->id),
            'tableColumnsRawMaterial'=>ProductRawMaterialEntity::dataTableColumns(),
            'addRaw'=>ProductRawMaterialEntity::add($data->id),
            'tableColumnsMultiUnit'=>ProductMultiUnitEntity::dataTableColumns(),
            'addUnit'=>ProductMultiUnitEntity::add($data->id),
            'tableColumnsMultiVarian' => ProductMultiVarianEntity::dataTableColumns(),
            'addVarian' => ProductMultiVarianEntity::add($data->id),
            'totalTransaction'=>$total,
            'varian' => Varian::where('is_deleted', 0)->get(),
            'is_unit' => count($isUnit) > 0 ? 1:0,
            'is_price_type' => $isPriceType->total > 0 ? 1:0,
            'is_goods'=>$request->is_goods,
            'cost'=>$cost

        ];



        return view('merchant::toko.master-data.product.detail2',$params);
    }

    public function dataTableStock(Request $request)
    {
        return ProductEntity::dataTableStock($request,$request->id);
    }

    public function dataTablePriceType(Request $request)
    {
        return ProductSellingLevelEntity::dataTablePriceType($request,$request->id);
    }

    public function dataTableMultiUnit(Request $request)
    {
        return ProductMultiUnitEntity::dataTableMultiUnit($request,$request->id);
    }

    public function dataTableMultiVarian(Request $request)
    {
        return ProductMultiVarianEntity::dataTableMultiVarian($request, $request->id);
    }

    public function deletePriceType(Request $request)
    {
        try{
            $id=$request->id;
            $data=ProductSellingLevel::find($id);
            $data->is_deleted=1;
            $data->save();

            if(!is_null($data->assign_to)){
                foreach(json_decode($data->assign_to) as $key => $a){
                    $data=ProductSellingLevel::find($a);
                    $data->is_deleted=1;
                    $data->save();
                }
            }

            return response()->json([
                'message' => "Data berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function dataTableRawMaterial(Request $request)
    {
        return ProductRawMaterialEntity::dataTableRawMaterial($request,$request->id);
    }

    public function deleteRawMaterial(Request $request)
    {
        try{
            $id=$request->id;
            $data=Material::find($id);
            $data->is_deleted=1;
            $data->save();

            if(!is_null($data->assign_to)){
                foreach(json_decode($data->assign_to) as $key => $a){
                    $data=Material::find($a);
                    $data->is_deleted=1;
                    $data->save();
                }
            }

            return response()->json([
                'message' => "Data berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }

    public function deleteMultiUnit(Request $request)
    {
        try{
            $id=$request->id;
            $data=MultiUnit::find($id);
            $product = Product::where('id', $data->sc_product_id)->where('is_deleted', 0)->first();
            $isUnit = MultiUnit::where('sc_product_id', $product->id)
                ->where('is_deleted', 0)
                ->get();

            if($data->md_unit_id == $product->md_unit_id && count($isUnit) > 1){
                return response()->json([
                    'message' => "Satuan default tidak bisa dihapus !. Untuk menghapus satuan default, hapus terlebih dahulu satuan-satuan lainnya",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(!is_null($data->assign_to)){
                foreach(json_decode($data->assign_to) as $key => $a){
                    $data=MultiUnit::find($a);
                    $data->is_deleted=1;
                    $data->save();
                }
            }

            $data->is_deleted=1;
            $data->save();

            return response()->json([
                'message' => "Data berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function deleteMultiVarian(Request $request)
    {
        try{
            $id=$request->id;
            $data=MultiVarian::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message' => "Data berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function deleteProductPhotos(Request  $request)
    {
        try{
            $data=Product::find($request->id);
            if(!is_null($data->product_photos)){
                $new=[];
                foreach (json_decode($data->product_photos) as $item){
                    if($item->fileName!=$request->fileName){
                        $new[]=[
                            'fileName'=>$item->fileName
                        ];
                    }
                }
                $data->product_photos=json_encode($new);
                $data->save();
            }else{
                if(!is_null($data->foto)){
                    $data->foto=null;
                    $data->save();
                }
            }

            return response()->json([
                'message' => "Foto produk berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }

    public function save(Request $request)
    {
        try{
            $warehouse=parent::getGlobalWarehouse(merchant_id());
            if($warehouse==false)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan,inisiasi gudang persediaan gagal dibuat !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();
            $id=$request->id;

            if(!is_null($id)){

                $data=ProductEntity::find($id);
                $OldIsWithStock=$data->is_with_stock;
                if($request->is_with_stock==0){
                    if( count(PurchaseOrderDetail::where('sc_product_id',$id)->get())<1 &&
                        count(SaleOrderDetail::where('sc_product_id',$id)->get())<1 &&
                        count(StockInventory::where('sc_product_id',$id)->get())<1){
                            CoaDetail::where('ref_external_id',$id)->where('ref_external_type',1)->delete();
                    }
                }
                CoaDetail::where('ref_external_id',$id)->where('name', 'like', '%Persediaan%')->update(['name' => 'Persediaan Produk '.$request->name]);
                CoaDetail::where('ref_external_id',$id)->where('name', 'like', '%Harga Pokok Penjualan%')->update(['name' => 'Harga Pokok Penjualan Produk '.$request->name]);

                $data->name=$request->name;
            }else{
                $data=new ProductEntity();
                $data->name=$request->name;
            }

            $validator = Validator::make($request->all(), (is_null($id))?$data->ruleCreateWeb:$data->ruleUpdateWeb);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            if(!is_null($id)){
                if($request->is_unit == 1 && $request->md_sc_product_type_id == 2){
                    return response()->json([
                        'message' => "Untuk merubah tipe produk ke barang produksi, silahkan hapus data multi satuan terlebih dahulu !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                if($request->is_price_type == 1 && $request->md_sc_product_type_id == 2){
                    return response()->json([
                        'message' => "Untuk merubah tipe produk ke barang produksi, silahkan hapus data tipe harga terlebih dahulu !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

            }

            $destinationPath = 'public/uploads/merchant/'.user_id().'/product/';
            $jsonFiles=[];
            if($id){
                $oldFiles=$request->old_files;
                if(!empty($oldFiles)){
                    foreach ($oldFiles as $v => $item){
                        array_push($jsonFiles,[
                            'fileName'=>$oldFiles[$v]
                        ]);
                    }
                }
            }

            if($request->hasFile('files')){
                foreach ($request->file('files') as $itemImage){
                    $fileName= Uuid::uuid4()->toString().'.'.$itemImage->getClientOriginalExtension();
                    $image = Image::make($itemImage)->resize(600,600)->encode($itemImage->getClientOriginalExtension());
                    $fileName=$destinationPath.$fileName;
                    Storage::disk('s3')->put($fileName, (string) $image);
                    array_push($jsonFiles,[
                        'fileName'=>$fileName
                    ]);

                }
            }


            $unit=Unit::find($request->md_unit_id);
            // if(is_null($unit))
            // {
            //     $unit=new Unit();
            //     $unit->name=$request->md_unit_id;
            //     $unit->save();
            // }
            $selling_price=strtr($request->selling_price, array('.' => '', ',' => '.'));
            $purchase_price=strtr($request->purchase_price, array('.' => '', ',' => '.'));

            $checkCode=Product::whereRaw("lower(code) like '".strtolower($request->code)."' ")
                                ->where('md_user_id',user_id())
                                ->where('id', '!=', $id)
                                ->first();
            if(!is_null($checkCode))
            {
                return response()->json([
                'message' => "Kode produk sudah digunakan !",
                'type' => 'warning',
                'is_modal' => false,
                'redirect_url' => ''
                ]);

            }


            $data->code=$request->code;

            if(is_null($id))
            {
                $data->purchase_price=$purchase_price;
                $data->is_with_stock=$request->is_with_stock;
                $data->is_with_purchase_price=($request->is_with_stock==0)?0:1;

            }

            if(!empty($jsonFiles)){
                $data->foto=$jsonFiles[0]['fileName'];
                $data->product_photos=json_encode($jsonFiles);
            }

            $data->is_with_stock=$request->is_with_stock;
            $data->is_consignment=(is_null($request->is_consignment))?0:$request->is_consignment;

            $data->purchase_price=$purchase_price;
            $data->selling_price=$selling_price;
            $data->weight=(is_null($request->weight))?0:$request->weight;
            $data->description=$request->content_1;
            if($request->is_with_stock==1){
                if($request->sc_merk_id!=-1){
                    $data->sc_merk_id=$request->sc_merk_id;
                }
            }
            if($request->is_with_stock==1){
                $data->md_sc_product_type_id=$request->md_sc_product_type_id;
                $data->long=$request->long;
                $data->wide=$request->wide;
                $data->hight=$request->hight;
                $data->package_contents=$request->package_contents;

            }else{
                $data->md_sc_product_type_id=1;
            }
            $data->sc_product_category_id=$request->sc_product_category_id;
            $data->md_user_id=user_id();
            $data->is_show_senna_app=0;
            $data->md_unit_id=$unit->id;
            $data->md_sc_category_id=null;
            $data->profit_prosentase=0;
            if(is_null($id))
            {
                $data->unique_code=Uuid::uuid4()->toString();
            }
            $data->created_by_merchant=merchant_id();
            $data->is_deleted=0;
            $data->save();
            $cost_other=json_decode($request->cost_other);

             //biaya tambahan
             if(is_null($id) && ($data->is_with_stock==0 || $data->md_sc_product_type_id==4)){
                if(!is_null($cost_other) && !empty($cost_other)){
                    $c=[];
                    $sum=0;

                    foreach($cost_other as $item){
                        $amount=strtr($item->amount, array('.' => '', ',' => '.'));

                        $c[]=[
                            "id"=>(int)$item->cost_id,
                            "amount"=>(int)$amount,
                            "name"=>$item->cost_name
                            ];
                        $sum+=$amount;
                    }
                    $data->cost_other=json_encode($c);
                    $data->purchase_price=$sum;
                    $data->save();
                }
            }


            if(!is_null($id) && ($data->is_with_stock==0 || $data->md_sc_product_type_id==4)){
                if(!is_null($cost_other) && !empty($cost_other)){
                    $c=[];
                    $sum=0;
                    foreach($cost_other as $item){
                        $amount=strtr($item->amount, array('.' => '', ',' => '.'));

                        $c[]=[
                            "id"=>(int)$item->cost_id,
                            "amount"=>(int)$amount,
                            "name"=>$item->cost_name
                            ];
                        $sum+=$amount;
                    }
                    $data->cost_other=json_encode($c);
                    $data->purchase_price=$sum;
                }else{
                    $data->cost_other=null;
                    $data->purchase_price=0;
                }
                $data->save();
            }

            $multi_unit=[];
            $selling_level=[];
            
            if(is_null($id) && isset($request->multi_satuan)){
                $multiSatuan = [];
                $decodeMultiSatuan = json_decode($request->multi_satuan);

                if(count($decodeMultiSatuan) > 0){
                    array_push($multiSatuan,[
                        "price" => $data->purchase_price,
                        "md_unit_id" => $data->md_unit_id,
                        "quantity" => 1,
                        "sc_product_id" => $data->id,
                        "created_at" => date('Y-m-d h:i:s'),
                        "updated_at" => date('Y-m-d h:i:s'),
                    ]);
                    foreach($decodeMultiSatuan as $key => $item){
                        array_push($multiSatuan,[
                            "price" => strtr($item->price, array('.' => '', ',' => '.')),
                            "md_unit_id" => $item->md_unit_id,
                            "quantity" => $item->quantity,
                            "sc_product_id" => $data->id,
                            "created_at" => date('Y-m-d h:i:s'),
                            "updated_at" => date('Y-m-d h:i:s')
                        ]);
                    }

                    MultiUnit::insert($multiSatuan);
                    $unit_id = DB::getPdo()->lastInsertId();
                    $multi_unit[]=$unit_id;
                }
            }

            if(is_null($id) && isset($request->tipe_harga)){
                $tipeHarga = [];
                $decodeTipeHarga = json_decode($request->tipe_harga);

                if(count($decodeTipeHarga) > 0){
                    foreach($decodeTipeHarga as $key => $item){
                        $tipeHarga[] = [
                            "selling_price" => strtr($item->selling_price, array('.' => '', ',' => '.')),
                            "level_name" => $item->level_name,
                            "is_deleted" => 0,
                            "sc_product_id" => $data->id,
                            "created_at" => date('Y-m-d h:i:s'),
                            "updated_at" => date('Y-m-d h:i:s')
                        ];
                    }

                    ProductSellingLevel::insert($tipeHarga);
                    $level_id = DB::getPdo()->lastInsertId();
                    $selling_level[]=$level_id;
                }
            }

            if(is_null($id) && isset($request->multi_varian)){
                $multiVarian = [];
                $decodeMultiVarian = json_decode($request->multi_varian);

                if(count($decodeMultiVarian) > 0){
                    foreach($decodeMultiVarian as $key => $item){
                        $multi_varian=new MultiVarian();
                        $multi_varian->sc_product_id=$data->id;
                        $multi_varian->purchase_price=strtr($item->purchase_price, array('.' => '', ',' => '.'));
                        $multi_varian->selling_price=strtr($item->selling_price, array('.' => '', ',' => '.'));
                        $multi_varian->created_at=date('Y-m-d h:i:s');
                        $multi_varian->updated_at=date('Y-m-d h:i:s');
                        $multi_varian->multi_varian=json_encode($item->multi_varian);
                        $multi_varian->label=$item->label;
                        $multi_varian->save();

                        $coaProduct=CoaProductUtil::initialCoaVarian(merchant_id(),$multi_varian->id,2,$item->label,$data->name);
                        if($coaProduct==false)
                        {
                            return response()->json([
                                'message' => "Terjadi kesalahan,inisiasi akun persediaan gagal dibuat !",
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $multi_varian->inv_id=$coaProduct['inv_id'];
                        $multi_varian->hpp_id=$coaProduct['hpp_id'];
                        $multi_varian->save();

                    }

                    $data->is_multi_varian = 1;
                    $data->save();

                }
            }

            if(is_null($id) && isset($request->bahan_baku)){
                $bahanBaku = [];
                $decodeBahanBaku = json_decode($request->bahan_baku);

                if(count($decodeBahanBaku) > 0){
                    foreach($decodeBahanBaku as $key => $item){
                        $bahanBaku[] = [
                            "parent_sc_product_id" => $data->id,
                            "child_sc_product_id" => $item->child_sc_product_id,
                            "quantity" => $item->quantity,
                            "created_at" => date('Y-m-d h:i:s'),
                            "updated_at" => date('Y-m-d h:i:s'),
                            "is_deleted" => 0
                        ];
                    }

                    Material::insert($bahanBaku);
                }
            }

            if(!is_null($id) && !is_null($data->assign_to_product)){

                $checkDuplicateProduct=ProductUtil::checkDuplicateProduct($data);
                if($checkDuplicateProduct==false){
                    return response()->json([
                        'message' => "Produk pada cabang telah digunakan tidak dapat dihapus!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $updateDuplicateProduct=ProductUtil::updateDuplicateProduct($data->assign_to_product,$data);
                if($updateDuplicateProduct==false){
                    return response()->json([
                        'message' => "Terjadi kesalahan pada update produk cabang!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                if(is_null($request->md_merchant_id)){
                    $data->assign_to_branch=null;
                    $data->assign_to_product=null;
                    $data->save();
                }
            }

            if(!is_null($request->md_merchant_id)){
                $dataBranch=explode(',',$request->md_merchant_id);

                $duplicateProduct=ProductUtil::duplicateProduct($dataBranch,$data,$request,$multi_unit,$selling_level);
                //dd($duplicateProduct);
                if($duplicateProduct==false){
                    return response()->json([
                        'message' => "Terjadi kesalahan duplikasi produk!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $data->assign_to_branch=$dataBranch;
                $data->save();
            }

            if(is_null($id))
            {
                    if($data->is_with_stock==1)
                    {
                        $coaProduct=CoaProductUtil::initialCoaProduct(merchant_id(),$data,1);
                        if($coaProduct==false)
                        {
                            return response()->json([
                                'message' => "Terjadi kesalahan,inisiasi akun persediaan gagal dibuat !",
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $data->inv_id=$coaProduct['inv_id'];
                        $data->hpp_id=$coaProduct['hpp_id'];
                        $data->save();
                    }

            }else{
                if($request->is_with_stock!=$OldIsWithStock && $request->is_with_stock==1){
                    $coaProduct=CoaProductUtil::initialCoaProduct(merchant_id(),$data,1);
                    if($coaProduct==false)
                    {
                        return response()->json([
                            'message' => "Terjadi kesalahan,inisiasi akun persediaan gagal dibuat !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }

                    $data->inv_id=$coaProduct['inv_id'];
                    $data->hpp_id=$coaProduct['hpp_id'];
                    $data->save();

                }
            }



            DB::commit();
            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.product.index')
            ]);


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;

            if( PurchaseOrderDetail::where('sc_product_id',$id)
                                    ->join('sc_purchase_orders as s','s.id','sc_purchase_order_details.sc_purchase_order_id')
                                    ->where('s.is_deleted',0)
                                    ->count()<1 &&
                SaleOrderDetail::where('sc_product_id',$id)
                                ->join('sc_sale_orders as s','s.id','sc_sale_order_details.sc_sale_order_id')
                                ->join('sc_products as p','p.id','sc_sale_order_details.sc_product_id')
                                ->where('p.is_with_stock',1)
                                ->where('s.is_deleted',0)
                                ->count()<1 &&
                StockInventory::where('sc_product_id',$id)
                                ->where('is_deleted',0)
                                ->where('type','in')
                                ->where('residual_stock','>',0)
                                ->count()<1 &&
                ProductionOfGoodDetail::where('sc_inv_production_of_good_details.sc_product_id',$id)
                                ->join('sc_inv_production_of_goods as s','s.id','sc_inv_production_of_good_details.sc_inv_production_of_good_id')->where('s.is_deleted',0)
                                ->count()<1
                                ){
                $delete=true;

            }else{
                $delete=false;
            }
            if($delete==false)
            {
                return response()->json([
                    'message' => "Data produk tidak bisa dihapus,karena telah digunakan !",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }else{
                DB::beginTransaction();
                $data=ProductEntity::find($id);
                if($data->assign_to_product){
                    $checkDuplicateProduct=ProductUtil::checkDuplicateProduct($data);
                    if($checkDuplicateProduct==false){
                        return response()->json([
                            'message' => "Produk pada cabang telah digunakan tidak dapat dihapus!",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }
                $data->is_deleted=1;
                $data->save();
                if($data->md_sc_product_type_id==3){
                    $raw=Material::where('child_sc_product_id',$id)->first();
                    if(!is_null($raw)){
                        $raw->is_deleted=1;
                        $raw->save();
                    }
                }
                $coa=CoaDetail::where('ref_external_id',$data->id)->where('ref_external_type',1)->get();
                foreach ($coa as $key =>$item)
                {
                    $item->is_deleted=1;
                    $item->save();
                    $coaCfD=CoaCashflowFormatDetail::where('acc_coa_detail_id',$item->id)->first();
                    if(!is_null($coaCfD))
                    {
                        $coaCfD->is_deleted=1;
                        $coaCfD->save();
                    }
                }
                DB::commit();

                return response()->json([
                    'message' => "Data berhasil dihapus !",
                    'type' => 'success',
                    'is_modal' => true,
                    'redirect_url' => ''
                ]);
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function formImport(Request $request)
    {
        $params=[
            'title'=>'Form Import',
        ];

        return view('merchant::toko.master-data.product.form-import',$params);

    }

    public function saveImport(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $warehouse=parent::getGlobalWarehouse(merchant_id());
            if($warehouse==false)
            {
                return response()->json([
                    'message' => 'Terjadi kesalahan saat inisialisasi gudang persediaan',
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            DB::beginTransaction();
            $rule=[
                'file'  => 'required|file|mimes:xls,xlsx'
            ];
            $validator = Validator::make($request->all(), $rule);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $file = $request->file('file');
            $fileName = date('YmdHis').$file->getClientOriginalName();
            $destinationPath = 'public/import/'.user_id().'';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $file->move($destinationPath,$fileName);
            $reader = ReaderEntityFactory::createXLSXReader();
            $reader->open($destinationPath.'/'.$fileName);
            $data=[];
            $product=new Product();
            $totalRows=0;
            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator() as $row) {
                    $totalRows++;
                }
            }
            if($totalRows>26)
            {
                return response()->json([
                    'message' => "Maksimal upload produk sejumlah 25 data/upload",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            foreach ($reader->getSheetIterator() as $sheet) {
                foreach ($sheet->getRowIterator()  as $key => $row) {
                    $cells = $row->getCells();
                    if(count($cells)!=8){
                        return response()->json([
                            'message' => "Jumlah kolom di excel tidak sesuai ketentuan",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                    if($key>1)
                    {
                        $category=CategoryEntity::whereRaw("lower(name) iLIKE '%".strtolower($cells[4]->getValue())."%' ")
                            ->where('md_user_id',user_id())
                            ->where('is_deleted',0)
                            ->first();
                        if(is_null($category))
                        {
                            $category=new CategoryEntity();
                            $category->name=$cells[4]->getValue();
                            $category->md_user_id=user_id();
                            $category->save();
                        }
                        $unit=Unit::whereRaw("lower(name) iLIKE '%".strtolower($cells[5]->getValue())."%' ")
                                    ->first();
                        $checkCode=Product::whereRaw("lower(code) iLIKE '%".strtolower($cells[1]->getValue())."%' ")
                                    ->where('md_user_id',user_id())
                                    ->where('is_deleted',0)
                                    ->first();
                        if(is_null($checkCode)){
                        return response()->json([
                            'message' => "Kode produk ".$cells[1]->getValue()." telah digunakan!",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                        }
                        if(is_null($unit))
                        {
                            $unit=new Unit();
                            $unit->name=$cells[5]->getValue();
                            $unit->save();
                        }
                        if($cells[6]->getValue()==0)
                        {
                            $stock=0;
                            $isPurchase=0;
                        }else{
                            $stock=0;
                            $isPurchase=1;
                        }
                        if($cells[7]->getValue()!=1 && $cells[7]->getValue()!=2 || is_null($cells[7]->getValue())){
                            $type=1;
                        }else{
                            $type=$cells[7]->getValue();
                        }
                        $data[]=[
                            'name'=>$cells[0]->getValue(),
                            'code'=>$cells[1]->getValue(),
                            'purchase_price'=>($isPurchase==1)?$cells[2]->getValue():0,
                            'selling_price'=>$cells[3]->getValue(),
                            'sc_product_category_id'=>$category->id,
                            'md_unit_id'=>$unit->id,
                            'weight' =>0,
                            'is_with_stock'=>$cells[6]->getValue(),
                            'md_sc_product_type_id' =>$type,
                            'stock'=>0,
                            'is_show_senna_app'=>0,
                            'is_with_purchase_price'=>$isPurchase,
                            'md_user_id'=>user_id(),
                            'is_deleted'=>0,
                            'unique_code'=>Uuid::uuid4()->toString()
                        ];
                    }

                }
            }
            $chunkData =$data;
            if (isset($chunkData) && !empty($chunkData)) {
                foreach ($chunkData as $chunkDataVal) {
                    $validator = Validator::make($chunkDataVal, $product->ruleCreate);
                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        return response()->json([
                            'message' => $error,
                            'type' => 'danger',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                    if($chunkDataVal['is_with_stock']==1)
                    {
                        $getId=Product::insertGetId($chunkDataVal);
                        $productTemp=Product::find($getId);
                        $coaProduct=CoaProductUtil::initialCoaProduct(merchant_id(),$productTemp,1);
                        if($coaProduct==false)
                        {
                            return response()->json([
                                'message' => "Terjadi kesalahan saat proses inisialisasi data persediaan",
                                'type' => 'danger',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $productTemp->inv_id=$coaProduct['inv_id'];
                        $productTemp->hpp_id=$coaProduct['hpp_id'];
                        $productTemp->save();

                    }else{
                        Product::insert($chunkDataVal);
                    }

                }
            }
            $reader->close();
            DB::commit();

            return response()->json([
                'message' => "Data berhasil diimport",
                'type' => 'success',
                'is_modal' => true,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal diupload !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function exportData(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $data=ProductEntity::getDataForDataTable();

            if($request->search){
                $search = $request->search;
                $data->where(function($query) use($search){
                    $query->whereRaw("lower(sc_products.code) like '%".strtolower($search)."%' ")
                    ->orWhereRaw("lower(sc_products.name) like '%".strtolower($search)."%'")
                    ->orWhereRaw("lower(c.name) like '%".strtolower($search)."%'")
                    ->orWhereRaw("lower(uu.name) like '%".strtolower($search)."%'");
                });
            }

            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Rekap Barang/Jasa Tanggal ".date('Y-m-d')."");
            $header = [
                'No',
                'Kode Produk',
                'Nama Produk',
                'Harga Beli',
                'Harga Jual',
                'Stok',
                'Kategori',
                'Tanggal Input'
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;

            foreach ($data->get() as $key => $item) {
                $dataShow = [
                    $key+1,
                    $item->kode_produk,
                    $item->nama_produk,
                    $item->harga_beli,
                    $item->harga_jual,
                    $item->stock,
                    $item->kategori_produk,
                    $item->tanggal_input
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-product_'.merchant_id()."_".date('Y-m-d').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                let success = document.querySelector('#successExport');
                if(success){
                    toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Terjadi kesalahan di server, data gagal di export !','danger',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";

        }
    }

    public function saveStock(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $data=Product::find($request->id);
            if($data->md_sc_type_product_id==2){
                return response()->json([
                    'message' => "Stok barang produksi hanya dapat ditambahkan melalui menu produksi produk !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $warehouse=WarehouseEntity::find($request->inv_warehouse_id);

            $validator = Validator::make($request->all(), $data->ruleStockWeb);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            DB::beginTransaction();
            $purchase_price=strtr($request->purchase_price, array('.' => '', ',' => '.'));
            if($request->type=='in' && $purchase_price==0)
            {
                return response()->json([
                    'message' => "Harga beli produk harus diisi ketika melakukan penambahan penyesuaian stok !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            if($request->amount>0)
            {

                if($request->type!='in')
                {
                    if($request->amount>$request->stock)
                    {
                        return response()->json([
                            'message' => "Stok Produk Tidak Mencukupi !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                }

                if($request->type=='in')
                {
                    if($warehouse->is_default==1)
                    {
                        $data->stock+=$request->amount;
                    }
                }
                $data->purchase_price=$purchase_price;
                $data->save();

                if($request->type=='in'){
                    $data->stock()->create([
                        'sync_id'=>$data->id.Uuid::uuid4()->toString(),
                        'sc_product_id'=>$data->id,
                        'total'=>$request->amount,
                        'record_stock'=>$data->stock,
                        'created_by'=>user_id(),
                        'selling_price'=>$data->selling_price,
                        'residual_stock'=>$request->amount,
                        'purchase_price'=>$purchase_price,
                        'timezone'=>$request->_timezone,
                        'type'=>StockInventory::IN,
                        'inv_warehouse_id'=>$request->inv_warehouse_id,
                        'transaction_action'=>'Penambahan Stok '.$data->name. ' Dari Aplikasi',
                    ]);

                    if(CoaProductUtil::inventoryAdjustment(user_id(),merchant_id(),$data,date('Y-m-d H:i:s'),'plus',$purchase_price,$request->amount)==false)
                    {
                        return response()->json([
                            'message' => "Terjadi kesalahan,inisiasi jurnal persediaan gagal dibuat !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }

                }else{
                    if($this->subtraction($data->id,$request->amount,$warehouse)==false){

                        return response()->json([
                            'message' => "Terjadi kesalahan,inisiasi jurnal persediaan gagal dibuat !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                    $data->stock()->create([
                        'sync_id'=>$data->id.Uuid::uuid4()->toString(),
                        'sc_product_id'=>$data->id,
                        'total'=>$request->amount,
                        'record_stock'=>$data->stock,
                        'created_by'=>user_id(),
                        'selling_price'=>$data->selling_price,
                        'residual_stock'=>$request->amount,
                        'purchase_price'=>$purchase_price,
                        'type'=>StockInventory::OUT,
                        'timezone'=>$request->_timezone,
                        'inv_warehouse_id'=>$request->inv_warehouse_id,
                        'transaction_action'=>'Pengurangan Stok '.$data->name. ' Dari Aplikasi'
                    ]);
                }
            }
            DB::commit();

            return response()->json([
                'message' => "Stok berhasil diperbaharui !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal diperbarui !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }

    private function subtraction($productId,$quantity,$warehouse)
    {
        try{
            $stock=StockInventory::where('sc_product_id',$productId)
                ->where('type','in')
                ->whereRaw("transaction_action not like '%Retur%'")
                ->where('is_deleted',0)
                ->where('inv_warehouse_id',$warehouse->id)
                ->orderBy('created_at','asc')
                ->get();

            $initial=[$quantity];
            $subtraction=[];
            foreach ($stock as $item)
            {
                $subtraction[]=$item->residual_stock;
            }
            DB::beginTransaction();
            $decrementData=$this->decrementStock(array_merge($initial,$subtraction));
            if(!empty($decrementData))
            {
                foreach ($stock as $key => $v)
                {
                    $inv=StockInventory::find($v->id);
                    if($v->residual_stock!=$decrementData[$key]['residual_amount'])
                    {
                        $inv->residual_stock=$decrementData[$key]['residual_amount'];
                        if($warehouse->is_default==1)
                        {
                            $v->getProduct->stock-=$decrementData[$key]['decrement_amount'];
                            $v->getProduct->save();

                        }

                        if(CoaProductUtil::inventoryAdjustment(user_id(),merchant_id(),$v->getProduct,date('Y-m-d H:i:s'),'minus',$v->purchase_price,$decrementData[$key]['decrement_amount'])==false)
                        {
                            DB::rollBack();
                            return  false;
                        }
                        if($decrementData[$key]['residual_amount']==0)
                        {
                            $inv->is_deleted=1;
                        }
                        $inv->save();
                    }

                }
            }

            DB::commit();
            return true;
        }catch (\Exception $e)
        {

            DB::rollBack();
            return false;

        }

    }

    private function decrementStock($array)
    {
        $total=$array[0];
        $data=[];
        for($i=1;$i<count($array);$i++)
        {
            if($total>=$array[$i]){
                $data[]=[
                    'decrement_amount'=>$array[$i],
                    'residual_amount'=>0,
                ];

            }else{
                if($total<0){

                    $data[]=[
                        'decrement_amount'=>0,
                        'residual_amount'=>$array[$i],
                    ];
                }else{

                    $data[]=[
                        'decrement_amount'=>$total,
                        'residual_amount'=>$array[$i]-$total
                    ];
                }

            }

            $total -= $array[$i];
        }

        return $data;

    }




}

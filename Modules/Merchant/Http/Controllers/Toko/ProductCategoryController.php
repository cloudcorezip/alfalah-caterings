<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use App\Models\SennaToko\Product;
use App\Models\MasterData\Merchant;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;


class ProductCategoryController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('master-data/product-category')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Toko\ProductCategoryController@index')
                    ->name('merchant.toko.product-category.index');
                Route::post('/data-table', 'Toko\ProductCategoryController@dataTable')
                    ->name('merchant.toko.product-category.datatable');
                Route::post('/add', 'Toko\ProductCategoryController@add')
                    ->name('merchant.toko.product-category.add');
                Route::post('/save', 'Toko\ProductCategoryController@save')
                    ->name('merchant.toko.product-category.save');
                Route::post('/delete', 'Toko\ProductCategoryController@delete')
                    ->name('merchant.toko.product-category.delete');
                Route::post('/reload-data', 'Toko\ProductCategoryController@reloadData')
                    ->name('merchant.toko.category.reload-data');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Kategori Produk',
            'tableColumns'=>CategoryEntity::dataTableColumns(),
            'add'=>(get_role()==3)?CategoryEntity::add(true):
                CategoryEntity::add(true),
            'key_val'=>$request->key,
            'searchKey' => md5(Uuid::uuid4()->toString()),


        ];


        return view('merchant::toko.master-data.category.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return CategoryEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){
            $data=CategoryEntity::find($id);
        }else{
            $data=new CategoryEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data
        ];

        if(isset($request->no_reload)){
            return view('merchant::toko.master-data.category.form-no-reload',$params);
        }else{
            return view('merchant::toko.master-data.category.form',$params);
        }
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (CategoryEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Kategori Produk',
            'searchKey' => $searchKey,
            'tableColumns'=>CategoryEntity::dataTableColumns(),
            'key_val'=>$request->key
        ];
        return view('merchant::toko.master-data.category.list',$params);

    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;


            if(!is_null($id)){

                $data=CategoryEntity::find($id);
            }else{
                $data=new CategoryEntity();
                $data->md_user_id=user_id();

            }
            // dd(json_decode($data->assign_to_branch));
            $data->name=$request->name;
            $data->description=$request->description;
            $data->is_service=$request->is_service;
            $data->save();


            $noReload=$request->no_reload;
            if(is_null($noReload)){
                $isReload=true;
            }else{
                if($noReload==1){
                    $isReload=false;
                }else{
                    $isReload=true;
                }
            }




            return response()->json([
                'message'=>'Kategori produk berhasil disimpan',
                'type'=>'success',
                'is_modal'=>$isReload,
                'redirect_url'=>''
            ]);


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            if(Product::where('sc_product_category_id',$id)->where('is_deleted',0)->count()>=1){
                return response()->json([
                    'message'=>'Data kategori produk tidak dapat dihapus karena telah digunakan!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            $data=CategoryEntity::find($id);
            $data->is_deleted=1;
            $data->save();



            return response()->json([
                'message'=>'Kategori produk berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }

}

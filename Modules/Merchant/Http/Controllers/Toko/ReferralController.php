<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Http\Controllers\Controller;
use App\Models\Marketing\MerchantCommission;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\DiscountProductDetail;
use App\Models\SennaToko\Merk;
use App\Models\SennaToko\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\DiscountCouponEntity;
use Modules\Merchant\Entities\Toko\DiscountProductEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Toko\ReferralEntity;
use Ramsey\Uuid\Uuid;

class ReferralController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('marketing/referral')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.referral.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.referral.datatable');
                Route::get('/add', [static::class, 'add'])
                    ->name('merchant.toko.referral.add');
                Route::get('/edit', [static::class, 'edit'])
                    ->name('merchant.toko.referral.edit');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.referral.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.referral.delete');
                Route::post('/reload-data', [static::class, 'reloadData'])
                    ->name('merchant.toko.referral.reload-data');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Referral',
            'tableColumns'=>ReferralEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'add'=>ReferralEntity::add(true),
            'key_val'=>$request->key
        ];

        return view('merchant::toko.master-data.referral.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ReferralEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){
            $data=ReferralEntity::find($id);

        }else{
            $data=new ReferralEntity();
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::toko.master-data.referral.form',$params);
    }

    public function edit(Request $request){

        $id=$request->id;
        if(!is_null($id)){
            $data=ReferralEntity::find($id);
        }else{
            $data=new ReferralEntity();
        }

        $customerTerms = json_decode($data->customer_terms);

        if(is_null($customerTerms)){
            $customerTerms = (object)[
                "birth_date" => "-1",
                "birth_year" => "-1",
                "customer_name" => [],
                "specific_customer_id" => []
            ];
        }

        $assignToBranch = json_decode($data->assign_to_branch);

        $customerName = $customerTerms->customer_name;

        if(gettype($customerName) == 'array'){
            $custNameVal = implode(',', array_column($customerName, 'name'));
        } else {
            $custNameVal = $customerName;
        }

        $specificCustomerId = isset($customerTerms->specific_customer_id)? $customerTerms->specific_customer_id:[];
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'customerTerms' => $customerTerms,
            'assignToBranch' => is_null($assignToBranch)? []:$assignToBranch,
            'custNameVal' => $custNameVal,
            'specificCustomerId' => $specificCustomerId,
            'customers'=>Customer::select(['id','name as nama_pelanggan'])->whereIn('id',array_column($specificCustomerId,'sc_customer_id'))->get()
        ];
        return view('merchant::toko.master-data.referral.edit',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);

            if(is_null($request->start_date)){
                return response()->json([
                    'message'=>'Tanggal mulai kupon belum di isi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->end_date<$request->start_date){
                return response()->json([
                    'message'=>'Tanggal berakhir kupon kurang dari tanggal mulai kupon',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($request->code)){
                return response()->json([
                    'message'=>'Kode referal belum diisi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($request->commission_id) || $request->commission_id=='-1'){
                return response()->json([
                    'message'=>'Tipe komisi belum diisi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($request->promo_id) || $request->promo_id=='-1'){
                return response()->json([
                    'message'=>'Benefit Pelanggan belum diisi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($request->staff_user_id) || $request->staff_user_id=='-1'){
                return response()->json([
                    'message'=>'Affiliator belum diisi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if($request->all_branch == 1){
                $tmp = get_cabang();
                $forCheckBranch = [];
                foreach($tmp as $k => $i){
                    if($i->id != merchant_id()){
                        array_push($forCheckBranch, intval($i->id));
                    }
                }
            } else {
                $tmpCheckBranch = json_decode($request->assign_to_branch);
                $forCheckBranch = [];

                foreach($tmpCheckBranch as $key => $item){
                    array_push($forCheckBranch, intval($item));
                }
            }

            array_unshift($forCheckBranch, merchant_id());

            $id=$request->id;

            if(!is_null($id))
            {
                $data = ReferralEntity::find($id);
            } else {
                $data = new ReferralEntity();
            }
            if(is_null($data->id)){
                $code=DiscountProductEntity::where('code',$request->code)
                    ->where('md_merchant_id',merchant_id())
                    ->where('id', '!=', $id)
                    ->where('is_deleted',0)
                    ->where('is_coupon',0)
                    ->where('is_referral_affiliate',1)
                    ->get();
                if(count($code)>0){
                    return response()->json([
                        'message'=>'Kode Referral telah digunakan',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);
                }
            }

            if($request->all_branch == 1){
                $tmp = get_cabang();
                $branch = [];
                foreach($tmp as $k => $i){
                    if($i->id != merchant_id()){
                        array_push($branch, intval($i->id));
                    }
                }
            } else {
                $branch = [];
                $tmp = json_decode($request->assign_to_branch);
                foreach($tmp as $k => $i){
                    array_push($branch, intval($i));
                }
            }


            $validator = Validator::make($request->all(), $data->createReferral);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message'=>$error,
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            $customerTerms = json_decode($request->req_customer_terms);

            if($request->is_all_customer == 0)
            {
                if($request->is_specific_customer == 0){
                    $custName = ($customerTerms->customer_name)? $customerTerms->customer_name:[];

                    if(count($custName) < 1 && $customerTerms->birth_date == "-1")
                    {
                        return response()->json([
                            'message'=>'Isi nama pelanggan atau tanggal lahir pelanggan terlebih dahulu !',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                } else {
                    $specificCustomerId = ($customerTerms->specific_customer_id)? $customerTerms->specific_customer_id:[];

                    if(count($specificCustomerId) < 1){
                        return response()->json([
                            'message'=>'Silahkan pilih pelanggan terlebih dahulu !',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>''
                        ]);
                    }
                }
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/referral/';

            if($request->hasFile('foto'))
            {
                $file=$request->file('foto');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }

            $data->name='-';
            $data->is_referral_affiliate=1;
            $data->bonus_value=json_encode([]);
            $data->day=json_encode([]);
            $data->is_limited = (is_null($request->is_limited) || $request->is_limited=='null')?0:$request->is_limited;
            $data->max_coupon_use = $request->max_coupon_use;
            $data->max_coupon_use_per_customer = $request->max_coupon_use_per_customer;
            $data->activation_type=$request->activation_type;
            if(is_null($id)){
                $data->md_merchant_id=merchant_id();
                $data->is_all_branch = $request->all_branch;
                $data->commission_id=$request->commission_id;
                $data->staff_user_id=$request->staff_user_id;
                $data->promo_id=$request->promo_id;
                $data->code=$request->code;
            }
            $data->start_date=$request->start_date;
            $data->end_date=$request->end_date;
            $data->foto=$fileName;
            $data->is_active=$request->is_active;
            $data->is_all_customer = $request->is_all_customer;
            $data->is_specific_customer = $request->is_specific_customer;
            $data->customer_terms = $request->req_customer_terms;
            $data->save();

            if($data->is_all_branch == 1){
                $tmpBranch = get_cabang();
                $assign_to_branch = [];
                foreach($tmpBranch as $k => $i){
                    if($i->id != $data->md_merchant_id){
                        array_push($assign_to_branch, $i->id);
                    }
                }

            } else {
                $assign_to_branch = json_decode($request->assign_to_branch);
            }

            if(is_null($id)){
                if(count($assign_to_branch) > 0){
                    $data->assign_to_branch = json_encode($assign_to_branch);
                    $data->save();
                }
            }

            DB::commit();
            return response()->json([
                'message'=>'Referral berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('merchant.toko.referral.index')
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>$e->getMessage()
            ]);
        }
    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=ReferralEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            DB::commit();
            return response()->json([
                'message'=>'Referral berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }



    public function reloadData(Request $request)
    {

        $searchParams = [];
        foreach (ReferralEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Referral',
            'searchKey' => $searchKey,
            'tableColumns'=>ReferralEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::toko.master-data.discount-coupon.list',$params);

    }

}

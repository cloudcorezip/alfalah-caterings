<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Sfa;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use App\Models\SennaToko\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\Sfa\SfaKpiVisitEntity;
use Modules\Merchant\Entities\Toko\Sfa\SfaVisitScheduleEntity;

class SfaScheduleController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('sales/sfa-visit-schedule')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.sfa.schedule.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.sfa.schedule.datatable');
                Route::get('/add', [static::class, 'add'])
                    ->name('merchant.toko.sfa.schedule.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.sfa.schedule.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.sfa.schedule.delete');
                Route::post('/delete-detail', [static::class, 'deleteDetail'])
                    ->name('merchant.toko.sfa.schedule.delete-detail');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Jadwal Kunjungan',
            'tableColumns'=>SfaVisitScheduleEntity::dataTableColumns(),
            'add'=>(get_role()==3)?SfaVisitScheduleEntity::add(true):
                SfaVisitScheduleEntity::add(true),
            'key_val'=>$request->key

        ];

        return view('merchant::toko.sfa.visit-schedule.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return SfaVisitScheduleEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public  function add(Request $request){

        $staffId=$request->staff_user_id;
        if(!is_null($staffId)){
            $date=Carbon::parse($request->date)->format('Y-m-d');
            $data=SfaVisitScheduleEntity::where('staff_user_id',$staffId)
                ->where('is_deleted',0)
                ->whereDate('visit_schedule',$date)->get();
        }else{
            $data=new SfaVisitScheduleEntity();
            $date=null;
        }
        $params=[
            'title'=>(is_null($staffId))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'staffId'=>$staffId,
            'date'=>$date

        ];
        return view('merchant::toko.sfa.visit-schedule.form',$params);
    }

    public function save(Request $request)
    {
        try{
            $staffId=$request->staff_user_id;
            $date=Carbon::parse($request->visit_schedule)->format('Y-m-d');
            $dateYmd=Carbon::parse($request->visit_schedule)->format('Ymd');
            $customer=$request->sc_customer_id;

            $data=SfaVisitScheduleEntity::where('staff_user_id',$staffId)
                ->where('is_deleted',0)
                ->whereDate('visit_schedule',$date)->first();

            if(is_null($data)){
                if(empty($customer)){
                    return response()->json([
                        'message'=>'Pelanggan untuk jadwal kunjungan tidak boleh kosong',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }
                $insert=[];
                foreach ($customer as $key => $c){
                    $insert[]=[
                        'sc_customer_id'=>$customer[$key],
                        'visit_schedule'=>$request->visit_schedule,
                        'md_merchant_id'=>merchant_id(),
                        'staff_user_id'=>$staffId,
                        'created_by'=>user()->id,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'unique_code'=>$staffId.$dateYmd
                    ];
                }
                SfaVisitScheduleEntity::insert($insert);
            }else{
                if($date!=$request->original_date){
                    if($data){
                        return response()->json([
                            'message'=>'Tanggal kunjungan tidak boleh sama dengan jadwal kunjungan yang lain',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>''
                        ]);
                    }
                }
                $check=SfaVisitScheduleEntity::where('staff_user_id',$staffId)
                    ->whereDate('visit_schedule',$date)->where('is_visit',1)
                    ->where('is_deleted',0)
                    ->first();
                if($check){
                    return response()->json([
                        'message'=>'Jadwal kunjungan tidak dapat diedit karena sudah dilakukan kunjungan oleh sales',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }
                if(empty($customer)){
                    return response()->json([
                        'message'=>'Pelanggan untuk jadwal kunjungan tidak boleh kosong',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }

                $insert=[];
                foreach ($customer as $key => $c){
                    $checkingData=SfaVisitScheduleEntity::where('staff_user_id',$staffId)
                        ->whereDate('visit_schedule',$date)->where('sc_customer_id',$customer[$key])
                        ->where('is_deleted',0)
                        ->first();
                    if(is_null($checkingData)){
                        $insert[]=[
                            'sc_customer_id'=>$customer[$key],
                            'visit_schedule'=>$request->visit_schedule,
                            'md_merchant_id'=>merchant_id(),
                            'staff_user_id'=>$staffId,
                            'created_by'=>user()->id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'),
                            'unique_code'=>$staffId.$dateYmd
                        ];
                    }
                }
                SfaVisitScheduleEntity::insert($insert);
            }

            return response()->json([
                'message'=>'Jadwal kunjungan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.sfa.schedule.index')
            ]);


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=SfaVisitScheduleEntity::where('unique_code',$id)
                ->where('is_deleted',0)
                ->get();

            $visit=0;
            foreach($data as $k =>$item){
                if($item->visit==1){
                    $visit++;
                }
            }
            if($visit>0){
                return response()->json([
                    'message'=>'Jadwal kunjungan tidak dapat diedit karena sudah dilakukan kunjungan oleh sales',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }else{
                SfaVisitScheduleEntity::where('unique_code',$id)
                    ->update([
                        'is_deleted'=>1
                    ]);

                return response()->json([
                    'message'=>'Jadwal kunjungan berhasil dihapus',
                    'type'=>'success',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }

    public function deleteDetail(Request $request)
    {
        try{
            $id=$request->id;
            $staffId=$request->staff_id;
            $date=Carbon::parse($request->date)->format('Y-m-d');
            $data=SfaVisitScheduleEntity::where('sc_customer_id',$id)
            ->where('staff_user_id',$staffId)
                ->whereDate('visit_schedule',$date)
                ->where('is_deleted',0)
                ->first();
            if($data->is_visit==1){
                return response()->json([
                    'message'=>'Jadwal kunjungan tidak dapat diedit karena sudah dilakukan kunjungan oleh sales',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);

            }else{
                $data->is_deleted=1;
                $data->save();
                $checkCount=SfaVisitScheduleEntity::where('staff_user_id',$staffId)->where('is_deleted',0)
                    ->where('visit_schedule',$date)
                    ->count();
                if($checkCount<1){
                    return response()->json([
                        'message'=>'Jadwal kunjungan untuk '.$data->getCustomer->name.' berhasil dihapus',
                        'type'=>'success',
                        'is_modal'=>false,
                        'redirect_url'=>\route('merchant.toko.sfa.schedule.index')
                    ]);
                }else{
                    return response()->json([
                        'message'=>'Jadwal kunjungan untuk '.$data->getCustomer->name.' berhasil dihapus',
                        'type'=>'success',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }

            }




        }catch (\Exception $e)
        {

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }
    }

}

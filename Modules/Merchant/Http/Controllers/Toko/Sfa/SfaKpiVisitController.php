<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Toko\Sfa;


use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaff;
use App\Models\SennaToko\Product;
use App\Utils\SFA\ConfigUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\Sfa\SfaKpiVisitEntity;

class SfaKpiVisitController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('sale/sfa-kpi-visit')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.sfa.kpi.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.sfa.kpi.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.toko.sfa.kpi.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.sfa.kpi.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.sfa.kpi.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Target Penjualan',
            'tableColumns'=>SfaKpiVisitEntity::dataTableColumns(),
            'add'=>(get_role()==3)?SfaKpiVisitEntity::add(true):
                SfaKpiVisitEntity::add(true),
            'key_val'=>$request->key

        ];

        return view('merchant::toko.sfa.kpi.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return SfaKpiVisitEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public  function add(Request $request){
        if(ConfigUtil::checkPermission(merchant_id(),'target')==false){
           return  "<script>otherMessage('warning','Pengaturan target sales belum diaktifkan,Silahkan atur terlebih dahulu')
closeModalAfterSuccess()
</script>";
        }else{
            $id=$request->id;
            if(!is_null($id)){

                $data=SfaKpiVisitEntity::find($id);
            }else{
                $data=new SfaKpiVisitEntity();
            }
            $params=[
                'title'=>(is_null($id))?'Tambah Data':'Edit Data',
                'data'=>$data
            ];
            return view('merchant::toko.sfa.kpi.form',$params);

        }

    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=SfaKpiVisitEntity::find($id);
            }else{
                $data=new SfaKpiVisitEntity();
            }

            $data->name=$request->name;
            $data->created_by=user()->id;
            $data->md_merchant_id=merchant_id();
            $data->staff_user_id=$request->staff_user_id;
            $data->visit_count=$request->visit_count;
            $data->min_sale=$request->min_sale;
            $data->min_new_customer=$request->min_new_customer;
            $data->start_date=$request->start_date;
            $data->end_date=$request->end_date;
            $data->save();

            return response()->json([
                'message'=>'Target penjualan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=SfaKpiVisitEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'Target penjualan berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Classes\Singleton\XenditCore;
use App\Http\Controllers\Controller;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\MasterData\TransactionType;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\PaymentMethod;
use App\Models\SennaToko\Product;
// use App\Models\SennaToko\DiscountProduct;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrderDetail;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\ReturSaleOrderDetail;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\StockSaleMapping;
use App\Models\SennaToko\Supplier;
use App\Models\SennaToko\TempPurchaseOrderDetail;
use App\Models\SennaToko\TempSaleOrderDetail;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaPurchaseUtil;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\CRM\CommissionUtil;
use App\Utils\ExpeditionUtil;
use App\Utils\Inventory\InventoryUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\Merchant\ProductUtil;
use App\Utils\PaymentUtils;
use App\Utils\ThirdParty\VirtualAccountUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\ContactEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Image;
use Xendit\Xendit;
use QrCode;


class SaleOrderController extends Controller
{

    protected $sale;
    protected $message;
    protected $product;
    protected $ar;
    protected $xendit;
    protected  $virtualAccount;


    public function __construct()
    {
        $this->sale=SaleOrder::class;
        $this->message=Message::getInstance();
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->xendit=XenditCore::getInstance();
        $this->virtualAccount=new VirtualAccountUtil();


    }

    public function routeWeb()
    {
        Route::prefix('transaction/sale-order')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/add/{page}/{id}', 'Toko\SaleOrderController@add')
                    ->name('merchant.toko.transaction.sale-order.add');
                Route::get('/get-customer', 'Toko\SaleOrderController@getCustomer')
                    ->name('merchant.toko.transaction.sale-order.customer-list');
                Route::get('/get-contact', 'Toko\SaleOrderController@getContact')
                    ->name('merchant.toko.transaction.sale-order.contact-list');
                Route::post('/save', 'Toko\SaleOrderController@save')
                    ->name('merchant.toko.transaction.sale-order.save');
                Route::get('/checkout', 'Toko\SaleOrderController@checkout')
                    ->name('merchant.toko.transaction.sale-order.checkout');
                Route::get('/checkout-ar', 'Toko\SaleOrderController@checkoutAr')
                    ->name('merchant.toko.transaction.sale-order-ar.checkout');
                Route::get('/checkout-expired', 'Toko\SaleOrderController@checkoutExpired')
                    ->name('merchant.toko.transaction.sale-order-ar.checkout-expired');
                //offer
                Route::post('/save-offer', 'Toko\SaleOrderController@saveOffer')
                    ->name('merchant.toko.transaction.sale-order.save-offer');
                //order
                Route::post('/save-order', 'Toko\SaleOrderController@saveOrder')
                    ->name('merchant.toko.transaction.sale-order.save-order');
                //delivery
                Route::post('/save-delivery', 'Toko\SaleOrderController@saveDelivery')
                    ->name('merchant.toko.transaction.sale-order.save-delivery');

                Route::post('/save-invoice', 'Toko\SaleOrderController@saveInvoice')
                    ->name('merchant.toko.transaction.sale-order.save-invoice');

                Route::post('/void', 'Toko\SaleOrderController@VoidTransaction')
                    ->name('merchant.toko.transaction.sale-order.void');
                Route::post('/delete-sale', 'Toko\SaleOrderController@deleteSale')
                    ->name('merchant.toko.transaction.sale-order.delete');
                Route::post('/delete-delivery', 'Toko\SaleOrderController@deleteDelivery')
                    ->name('merchant.toko.transaction.sale-order.delete-delivery');

                //payment
                Route::post('/add-payment', 'Toko\SaleOrderController@addPayment')
                    ->name('merchant.toko.transaction.sale-order.add-payment');
                Route::post('/save-payment', 'Toko\SaleOrderController@savePayment')
                    ->name('merchant.toko.transaction.sale-order.save-payment');
                Route::post('/delete-payment', 'Toko\SaleOrderController@deletePayment')
                    ->name('merchant.toko.transaction.sale-order.delete-payment');
                Route::post('/delete-detail', 'Toko\SaleOrderController@deleteDetail')
                    ->name('merchant.toko.transaction.sale-order.delete-detail');

                Route::get('/add-retur', 'Toko\SaleOrderController@addRetur')
                    ->name('merchant.toko.transaction.sale-order.add-retur');
                Route::post('/detail-retur', 'Toko\SaleOrderController@detailRetur')
                    ->name('merchant.toko.transaction.sale-order.detail-retur');
                Route::post('/delete-retur', 'Toko\SaleOrderController@deleteRetur')
                    ->name('merchant.toko.transaction.sale-order.delete-retur');
                Route::post('/delete-retur-delivery', 'Toko\SaleOrderController@deleteReturDeliv')
                    ->name('merchant.toko.transaction.sale-order.delete-retur-delivery');
                Route::post('/save-retur', 'Toko\SaleOrderController@saveRetur')
                    ->name('merchant.toko.transaction.sale-order.save-retur');
                Route::post('/save-retur-deliv', 'Toko\SaleOrderController@saveReturDeliv')
                    ->name('merchant.toko.transaction.sale-order.save-retur-deliv');



                Route::get('/payment-list', 'Toko\SaleOrderController@getPaymentList')
                    ->name('merchant.toko.transaction.sale-order.payment-list');

            });
    }

    public  function add($page,$id){
        if($id==-1){
            $data=new $this->sale;
            $payment=null;
        }else{
            $data=$this->sale::find($id);
            $payment=PaymentUtils::getPayment($data->md_merchant_id);
        }



        $params=[
            'title'=>'Tambah Data',
            'data'=>$data,
            'transType'=>$payment,
            'shippingMethod'=>ExpeditionUtil::getList(merchant_id()),
        ];

        if($page=="penerimaan"){
            return view('merchant::toko.transaction.sale-order.form-delivery',$params);
        }else if($page=="pemesanan"){
            return view('merchant::toko.transaction.sale-order.form-order',$params);
        }else if($page=="penawaran"){
            return view('merchant::toko.transaction.sale-order.form-offer',$params);
        }else if($page=="pembayaran"){
            return view('merchant::toko.transaction.sale-order.form-invoice',$params);
        }else{
            return view('merchant::toko.transaction.sale-order.form',$params);
        }

    }


    public function getPaymentList(Request  $request)
    {
        try {
            $merchantId=$request->md_merchant_id;
            $data=collect(PaymentUtils::getPayment($merchantId));

            $result=[];
            foreach ($data as $key =>$item)
            {
                $childs=[];
                if(count($item['data'])>0)
                {
                    if($item['name']=='Pembayaran Digital'){


                        foreach (collect($item['data']) as $child)
                        {
                            $subChilds=[];
                            $child=collect($child);
                            foreach ($child['data'] as $subChild)
                            {
                                $subChild=collect($subChild);
                                $subChilds[]=[
                                    'id'=>$subChild['trans_id'].'_'.$subChild['acc_coa_id'].'_1_'.$subChild['alias'].'_'.$subChild['id'],
                                    'text'=>$subChild['name']
                                ];
                            }
                            $childs[]=[
                                'id'=>Uuid::uuid4()->toString(),
                                'text'=>$child['name'],
                                'children'=>$subChilds
                            ];

                        }
                    }else{

                        foreach (collect($item['data']) as $child2)
                        {
                            $childs[]=[
                                'id'=>$child2->trans_id.'_'.$child2->acc_coa_id.'_0',
                                'text'=>$child2->name
                            ];
                        }
                    }

                }

                $result[]=[
                    "text"=>$item['name'],
                    'children'=>$childs
                ];
            }
            // dd($result);
            return response()->json($result);


        }catch (\Exception $e)
        {
            return  response()->json([]);

        }
    }


    public function saveOffer(Request $request)
    {
        try {

            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            if (is_null(MerchantUtil::checkMerchant($request->md_merchant_id))) {
                return response()->json([
                    'message' => "Akun tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $timestamp=$request->created_at;
            $date = new \DateTime($request->created_at);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'sale',1,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($request->sc_customer_id==-1)
            {
                return response()->json([
                    'message' => "Pelanggan tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $purchase=new $this->sale;

            $purchaseOffer=$this->sale::find($request->id);
            if(!is_null($purchaseOffer))
            {
                if($purchaseOffer->step_type!=1){
                    $purchaseOffer->is_step_close=1;
                    $purchaseOffer->save();
                    $purchase=new $this->sale;
                }else{
                    $purchase=$this->sale::find($request->id);
                    SaleOrderDetail::where('sc_sale_order_id',$request->id)->delete();
                }
                $purchase->code=$purchaseOffer->code;
                $purchase->sc_customer_id=$purchaseOffer->sc_customer_id;

            }else{
                $purchase->sc_customer_id=$request->sc_customer_id;
                $purchase->code=CodeGenerator::generateSaleOrder($request->md_merchant_id);
            }
            $purchase->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $purchase->step_type=1;
            $purchase->note_order=$request->note;
            $purchase->qr_code = '-';
            $purchase->is_debet = 0;
            $purchase->is_with_retur = 0;
            $purchase->change_money = 0;
            $purchase->md_merchant_id = $request->md_merchant_id;
            $purchase->paid_nominal = 0;
            $purchase->md_sc_transaction_status_id = ($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;
            $purchase->md_sc_shipping_category_id=$request->md_sc_shipping_category_id;

            $purchase->shipping_cost=$request->shipping_cost;
            $purchase->timezone = $request->timezone;
            $purchase->created_by = user_id();
            $purchase->sync_id = $request->md_merchant_id . Uuid::uuid4()->toString();

            $purchase->time_to_offer=$request->created_at;
            $purchase->total_offer=$request->total;
            $purchase->promo_offer = $request->discount;
            $purchase->promo_percentage_offer = $request->discount_percentage;
            $purchase->tax_offer = $request->tax;
            $purchase->tax_percentage_offer = $request->tax_percentage;

            $purchase->is_approved_shop=1;
            $purchase->created_at=$timestamp;
            $purchase->updated_at=$timestamp;
            $purchase->save();

            $sc_product_id = $request->sc_product_id;
            $prices = $request->selling_price;
            $quantity = $request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coaHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $unitList=$request->unit_list;
            $jsonMultiUnit=$request->json_multi_unit;
            $details = [];
            if (empty($sc_product_id)) {

                return response()->json([
                    'message' => "Data item penjualan tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $products=$this->product::whereIn('id',$sc_product_id)
                ->get();

            foreach ($sc_product_id as $key =>$item)
            {
                if($isMultiUnit[$key]==1){
                    if($unitList[$key]=='-1' || $unitList==-1)
                    {
                        return response()->json([
                            'message' => "Ada produk yang belum memilih satuan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                }

                $p=$products->firstWhere('id',$sc_product_id[$key]);
                if(is_null($p))
                {
                    return response()->json([
                        'message' => "Data produk tidak boleh kosong !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                $originalPrice=(float)strtr($prices[$key], array('.' => '', ',' => '.'));

                $stockOutPackage=[];

                if($p->md_sc_product_type_id==4)
                {
                    $rawMaterial=Material::where('parent_sc_product_id',$p->id)
                        ->where('is_deleted',0)
                        ->get();
                    if($rawMaterial->count()>0)
                    {
                        foreach ($rawMaterial as $rr =>$r)
                        {
                            $stockOutPackage[]=[
                                'id'=>$r->child_sc_product_id,
                                'head'=>$p->id,
                                'stock_out'=>$quantity[$key]*$r->quantity,
                                'amount'=>$quantity[$key]*$r->total,
                                'unit_name'=>$r->unit_name
                            ];
                        }
                    }

                }

                if($isMultiUnit[$key]==0){
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'quantity'=>$quantity[$key],
                        'is_bonus'=>0,
                        'sub_total'=>$quantity[$key]*($originalPrice),
                        'sc_sale_order_id'=>$purchase->id,
                        'profit'=>0,
                        'price'=>$originalPrice,
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coaHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'package_stock_out'=>json_encode($stockOutPackage),
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                    ];
                }else{
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'quantity'=>$quantity[$key],
                        'is_bonus'=>0,
                        'sub_total'=>$quantity[$key]*($originalPrice),
                        'sc_sale_order_id'=>$purchase->id,
                        'profit'=>0,
                        'price'=>$originalPrice,
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coaHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'package_stock_out'=>json_encode($stockOutPackage),
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                    ];
                }


            }


            SaleOrderDetail::insert($details);
            DB::commit();

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.sale-order.detail-offer', ['id' => $purchase->id])
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    //Order
    public function saveOrder(Request $request)
    {
        try {

            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            if (is_null(MerchantUtil::checkMerchant($request->md_merchant_id))) {
                return response()->json([
                    'message' => "Akun tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($request->sc_customer_id==-1){
                return response()->json([
                    'message' => "Silahkan pilih pelanggan terlebih dahulu !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $timestamp=$request->created_at;
            $date = new \DateTime($request->created_at);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'sell',2,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $purchase=new $this->sale;

            $purchaseOffer=$this->sale::find($request->id);
            if(!is_null($purchaseOffer))
            {
                if($purchaseOffer->step_type!=2){
                    $purchaseOffer->is_step_close=1;
                    $purchaseOffer->save();
                    $purchase=new $this->sale;
                }else{
                    $purchase=$this->sale::find($request->id);
                    SaleOrderDetail::where('sc_sale_order_id',$request->id)->delete();
                }
                $purchase->code=$purchaseOffer->code;
                $purchase->sc_customer_id=$purchaseOffer->sc_customer_id;

            }else{
                $purchase->sc_customer_id=$request->sc_customer_id;
                $purchase->code=CodeGenerator::generateSaleOrder($request->md_merchant_id);
            }

            $purchase->ref_code=str_replace(' ','',$request->ref_code);
            $purchase->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);

            $purchase->step_type=2;
            $purchase->qr_code = '-';
            $purchase->is_debet = 0;
            $purchase->is_with_retur = 0;
            $purchase->change_money = 0;
            $purchase->note_order=$request->note;

            $purchase->total = $request->total;
            $purchase->md_merchant_id = $request->md_merchant_id;
            $purchase->paid_nominal = 0;
            $purchase->md_sc_transaction_status_id = ($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;
            $purchase->md_sc_shipping_category_id=$request->md_sc_shipping_category_id;

            $purchase->shipping_cost=$request->shipping_cost;
            $purchase->timezone = $request->timezone;
            $purchase->created_by = user_id();
            $purchase->sync_id = $request->md_merchant_id. Uuid::uuid4()->toString();

            $purchase->time_to_order=$request->created_at;
            $purchase->total_order=$request->total;
            $purchase->promo_order = $request->discount;
            $purchase->promo_percentage_order = $request->discount_percentage;
            $purchase->tax_order = $request->tax;
            $purchase->tax_percentage_order = $request->tax_percentage;

            $purchase->is_approved_shop=1;
            $purchase->created_at=$timestamp;
            $purchase->updated_at=$timestamp;
            $purchase->save();

            $sc_product_id = $request->sc_product_id;
            $prices = $request->selling_price;
            $quantity = $request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coaHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $unitList=$request->unit_list;
            $jsonMultiUnit=$request->json_multi_unit;
            $details = [];

            if (empty($sc_product_id)) {
                return response()->json([
                    'message' => "Data item penjualan tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $products=$this->product::whereIn('id',$sc_product_id)
                ->get();

            foreach ($sc_product_id as $key =>$item)
            {
                if($isMultiUnit[$key]==1){
                    if($unitList[$key]=='-1' || $unitList==-1)
                    {
                        return response()->json([
                            'message' => "Ada produk yang belum memilih satuan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                }

                $p=$products->firstWhere('id',$sc_product_id[$key]);
                if(is_null($p))
                {
                    return response()->json([
                        'message' => "Data produk tidak boleh kosong !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }

                $originalPrice=(float)strtr($prices[$key], array('.' => '', ',' => '.'));

                $stockOutPackage=[];

                if($p->md_sc_product_type_id==4)
                {
                    $rawMaterial=Material::where('parent_sc_product_id',$p->id)
                        ->where('is_deleted',0)
                        ->get();
                    if($rawMaterial->count()>0)
                    {
                        foreach ($rawMaterial as $rr =>$r)
                        {
                            $stockOutPackage[]=[
                                'id'=>$r->child_sc_product_id,
                                'head'=>$p->id,
                                'stock_out'=>$quantity[$key]*$r->quantity,
                                'amount'=>$quantity[$key]*$r->total,
                                'unit_name'=>$r->unit_name
                            ];
                        }
                    }

                }

                if($isMultiUnit[$key]==0)
                {
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'quantity'=>$quantity[$key],
                        'is_bonus'=>0,
                        'sub_total'=>$quantity[$key]*($originalPrice),
                        'sc_sale_order_id'=>$purchase->id,
                        'profit'=>($quantity[$key]*$originalPrice)-($p->purchase_price*$quantity[$key]),
                        'price'=>$originalPrice,
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coaHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'package_stock_out'=>json_encode($stockOutPackage),
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                    ];
                }else{
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'quantity'=>$quantity[$key],
                        'is_bonus'=>0,
                        'sub_total'=>$quantity[$key]*($originalPrice),
                        'sc_sale_order_id'=>$purchase->id,
                        'profit'=>($quantity[$key]*$originalPrice*$nilaiKonversi[$key])-($p->purchase_price*$quantity[$key]*$nilaiKonversi[$key]),
                        'price'=>$originalPrice,
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coaHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'package_stock_out'=>json_encode($stockOutPackage),
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                    ];
                }

            }


            SaleOrderDetail::insert($details);

            DB::commit();

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.sale-order.detail-order', ['id' => $purchase->id])
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    //Delivery
    public function saveDelivery(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            if(is_null(MerchantUtil::checkMerchant($request->md_merchant_id)))
            {
                return response()->json([
                    'message' => "Akun merchant tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $userId=user_id();
            $merchantId=$request->md_merchant_id;
            $sc_product_id=$request->sc_product_id;
            $price=$request->selling_price;
            $quantity=$request->quantity;
            $unitName=$request->unit_name;
            $max_quantity=$request->max_quantity;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $unitList=$request->unit_list;
            $jsonMultiUnit=$request->json_multi_unit;
            $multiQuantity=$request->multi_quantity;
            $stockInOut=[];

            $details=[];
            if(empty($sc_product_id))
            {
                return response()->json([
                    'message' => "Data iten penjualan tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            $purchase=new $this->sale;

            $purchaseOffer=$this->sale::find($request->id);
            if(!is_null($purchaseOffer))
            {
                if($purchaseOffer->step_type!=3){
                    $purchaseOffer->is_step_close=1;
                    $purchaseOffer->save();
                    $purchase=new $this->sale;
                }else{
                    $purchase=$this->sale::find($request->id);
                    SaleOrderDetail::where('sc_sale_order_id',$request->id)->delete();
                }
                $purchase->code=$purchaseOffer->code;
                $purchase->sc_customer_id=$purchaseOffer->sc_customer_id;

            }else{
                $purchase->sc_customer_id=$request->sc_customer_id;
                $purchase->code=CodeGenerator::generateSaleOrder($request->md_merchant_id);

            }
            if ($request->md_sc_shipping_category_id == '-1') {
                return response()->json([
                    'message' => "Metode Pengiriman belum dipilih !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $totalDev=0;
            foreach ($sc_product_id as $key =>$item)
            {
                $originalPrice=strtr($price[$key], array('.' => '', ',' => '.'));

                if($quantity[$key]<1){
                    return response()->json([
                        'message' => "Jumlah tidak boleh kosong !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $totalDev+=$originalPrice*$quantity[$key];
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'sell',3,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(!in_array($request->md_sc_shipping_category_id,[3,28,27]))
            {
                if($request->shipping_cost==0)
                {
                    return response()->json([
                        'message' => "Silahkan mengisi biaya pengiriman terlebih dahulu !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $is_with_load_shipping=1;
            }else{
                $is_with_load_shipping=0;
            }

            $purchase->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $purchase->ref_code=$request->ref_code;

            $purchase->step_type=3;
            $purchase->qr_code='-';
            $purchase->is_debet=0;
            $purchase->is_with_retur=0;
            $purchase->change_money=0;
            $purchase->total=$request->total;
            $purchase->total_delivery=$request->total;
            $purchase->md_merchant_id=$merchantId;
            $purchase->shipping_cost=$request->shipping_cost;
            $purchase->md_sc_shipping_category_id=$request->md_sc_shipping_category_id;
            $purchase->paid_nominal=0;
            $purchase->note_order=$request->note;
            $purchase->md_sc_transaction_status_id=TransactionStatus::UNPAID;
            $purchase->promo_delivery = $request->discount;
            $purchase->promo_percentage_delivery=$request->discount_percentage;
            $purchase->tax_delivery=$request->tax;
            $purchase->tax_percentage_delivery=$request->tax_percentage;
            $purchase->created_at=$request->created_at;
            $purchase->timezone=$request->timezone;
            $purchase->created_by=$userId;
            $purchase->is_with_load_shipping=$is_with_load_shipping;
            $purchase->sync_id=$merchantId.Uuid::uuid4()->toString();
            $purchase->created_at=$request->created_at;
            $purchase->updated_at=$request->created_at;
            $purchase->time_to_delivery=$request->created_at;
            $purchase->is_approved_shop=1;
            $purchase->save();

            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $inventoryId=merchant_detail_multi_branch($merchantId)->md_inventory_method_id;
            $products=$this->product::whereIn('id',$sc_product_id)->get();

            $productCalculate=[];
            $updateStockProduct="";
            $warehouse=Controller::getGlobalWarehouse($merchantId);
            $arrayProductOut=[];
            foreach ($sc_product_id as $key =>$p)
            {
                $product=$products->firstWhere('id',$sc_product_id[$key]);

                $originalPrice=strtr($price[$key], array('.' => '', ',' => '.'));
                $stockOutPackage=[];

                if($isMultiUnit[$key]==0){
                    if($max_quantity[$key] < $quantity[$key]){
                        return response()->json([
                            'message' => "Jumlah penerimaan melebihi jumlah pemesanan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }else{
                    if($multiQuantity[$key] < ($quantity[$key]*$nilaiKonversi[$key])){
                        return response()->json([
                            'message' => "Jumlah penerimaan melebihi jumlah pemesanan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }


                if($product->is_with_stock==1)
                {
                    if($isMultiUnit[$key]==0){
                        array_push($arrayProductOut,[
                            'sc_product_id'=>$product->id,
                            'product_name'=>$product->name,
                            'product_code'=>$product->code,
                            'unit_name'=>$unitName[$key],
                            'product_name_2'=>$productName[$key],
                            'selling_price'=>$originalPrice,
                            'purchase_price'=>$product->purchase_price,
                            'quantity'=>$quantity[$key],
                            'product_stock'=>$product->stock,
                        ]);
                    }else{
                        array_push($arrayProductOut,[
                            'sc_product_id'=>$product->id,
                            'product_name'=>$product->name,
                            'product_code'=>$product->code,
                            'unit_name'=>$unitName[$key],
                            'product_name_2'=>$productName[$key],
                            'selling_price'=>($originalPrice*$quantity[$key])/($quantity[$key]*$nilaiKonversi[$key]),
                            'purchase_price'=>$product->purchase_price,
                            'quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                            'product_stock'=>$product->stock,

                        ]);
                    }

                }


                if($product->md_sc_product_type_id==4)
                {
                    $rawMaterial=Material::
                    select([
                        'sc_product_raw_materials.id',
                        'sc_product_raw_materials.child_sc_product_id',
                        'sc_product_raw_materials.unit_name',
                        'sc_product_raw_materials.quantity',
                        'sc_product_raw_materials.total',
                        'p.id as product_id',
                        'p.name as product_name',
                        'p.code as product_code',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'p.is_with_stock',
                        'sc_product_raw_materials.value_conversion',
                    ])
                        ->where('sc_product_raw_materials.parent_sc_product_id',$product->id)
                        ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                        ->where('sc_product_raw_materials.is_deleted',0)
                        ->where('p.is_deleted',0)
                        ->get();
                    if($rawMaterial->count()>0)
                    {
                        foreach ($rawMaterial as $rr =>$r)
                        {

                            if($r->is_with_stock==1)
                            {

                                array_push($arrayProductOut,[
                                    'sc_product_id'=>$r->child_sc_product_id,
                                    'product_name'=>$r->product_name,
                                    'product_code'=>$r->product_code,
                                    'unit_name'=>$r->unit_name,
                                    'product_name_2'=>$r->product_name.' '.$r->product_code,
                                    'selling_price'=>$r->selling_price,
                                    'purchase_price'=>$r->purchase_price,
                                    'quantity'=>$quantity[$key]*($r->quantity*$r->value_conversion),
                                    'product_stock'=>$r->stock,
                                ]);
                            }
                            $stockOutPackage[]=[
                                'id'=>$r->child_sc_product_id,
                                'head'=>$product->id,
                                'stock_out'=>$quantity[$key]*($r->quantity*$r->value_conversion),
                                'amount'=>$quantity[$key]*$r->total,
                                'unit_name'=>$r->unit_name,
                                'product_name'=>$r->product_name.' '.$r->product_code,
                                'is_with_stock'=>$r->is_with_stock
                            ];

                        }
                    }
                }
                if($isMultiUnit[$key]==0){
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_sale_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'package_stock_out'=>json_encode($stockOutPackage),
                        'created_at'=>$request->created_at,
                        'updated_at'=>$request->created_at,
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,

                    ];
                }else{
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_sale_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'package_stock_out'=>json_encode($stockOutPackage),
                        'created_at'=>$request->created_at,
                        'updated_at'=>$request->created_at,
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                    ];
                }


            }


            DB::table('sc_sale_order_details')->insert($details);

            if(!empty($arrayProductOut))
            {
                $arrays=collect($arrayProductOut)->groupBy('sc_product_id');
                $countOutOut=0;

                foreach ($arrays as $j =>$i)
                {

                    $quantity=0;
                    $firstData=$i->first();
                    foreach ($i as $ii =>$iii)
                    {
                        $quantity+=$iii['quantity'];
                    }


                    if($firstData['product_stock']<$quantity)
                    {
                        return response()->json([
                            'message' => "Stok produk  ".$firstData['product_name']." pada gudang utama atau gudang penjualan tidak mencukupi !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }

                    if($warehouse->is_default==1)
                    {
                        $stockP=$firstData['product_stock'] - $quantity;
                        $updateStockProduct.=($countOutOut==0)?"(".$j.",".$stockP.")":",(".$j.",".$stockP.")";
                        $countOutOut++;
                    }else{
                        $stockP=$firstData['product_stock'];
                    }

                    array_push($productCalculate,[
                        'id'=>$j,
                        'amount'=>$quantity
                    ]);

                    $stockInOut[]=[
                        'sync_id' => $j . Uuid::uuid4()->toString(),
                        'sc_product_id' => $j,
                        'total' => $quantity,
                        'inv_warehouse_id' => $warehouse->id,
                        'record_stock' => $stockP,
                        'created_by' => $userId,
                        'selling_price' => $firstData['selling_price'],
                        'purchase_price' => $firstData['purchase_price'],
                        'residual_stock' => $quantity,
                        'type' => StockInventory::OUT,
                        'transaction_action' => 'Pengurangan Stok ' . $firstData['product_name'] . ' Dari Pengiriman Penjualan Dengan Code ' . $purchase->code,
                        'stockable_type'=>'App\Models\SennaToko\SaleOrder',
                        'stockable_id'=>$purchase->id,
                        'created_at'=>$request->created_at,
                        'updated_at'=>$request->created_at,
                        'timezone'=>$purchase->timezone,
                        'unit_name'=>$firstData['unit_name'],
                        'product_name'=>$firstData['product_name_2']
                    ];

                }
            }

            DB::commit();


            if(!empty($productCalculate)){
                $calculate=[
                    'productCalculate'=>$productCalculate,
                    'updateStockProduct'=>ltrim($updateStockProduct, ','),
                    'inv_id'=>$inventoryId,
                    'warehouse_id'=>$warehouse->id,
                    'stock_in_out'=>$stockInOut
                ];
                Session::put('sale_delivery_'.$purchase->id,$calculate);
                Artisan::call('sale:delivery', ['--sale_id' =>$purchase->id]);

            }


            if($purchase->step_type==1){
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.sale-order.detail-offer',['id'=>$purchase->id])
                ]);
            }else{
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.sale-order.detail-delivery',['id'=>$purchase->id])
                ]);

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }

    }

    public function saveInvoice(Request $request){
        try {

            $merchantId=$request->md_merchant_id;
            $userId=user_id();
            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'sell',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            $timestamp=$request->created_at;
            $date = new \DateTime($request->created_at);
            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($request->is_debet==0)
            {
                if ($request->md_transaction_type_id == '-1') {
                    return response()->json([
                        'message' => "Metode Pembayaran belum dipilih !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }else{
                if($request->due_date<$request->created_at){
                    return response()->json([
                        'message' => "Tanggal jatuh tempo tidak boleh kurang dari tanggal transaksi !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }


            $inventoryId=merchant_detail_multi_branch($merchantId)->md_inventory_method_id;

            if($request->is_debet==0)
            {
                $trans = explode('_', $request->md_transaction_type_id);

            }else{
                $trans=null;
            }

            if(!is_null($trans)){

                if($trans[2]==1){

                    $status= TransactionStatus::UNPAID;

                }else{

                    $status=($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;

                }

            }else{

                $status=($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;


            }
            $purchase=SaleOrder::find($request->id);
            $newPurchase= $purchase->replicate();
            $purchase->is_step_close=1;
            $purchase->save();
            $newPurchaseDetail=[];
            $price=$request->selling_price;
            $quantity=$request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;

            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $jsonMultiUnit=$request->json_multi_unit;

            if(!is_null($trans)){
                if($trans[2]==1){
                    if($trans[0]==4){
                        $adminFee = ceil(0.008 * $request->total);
                        $total = ceil($adminFee + $request->total);
                    }elseif ($trans[0]==5){
                        $adminFee = 5000;
                        $total = ceil($adminFee + $request->total);
                    }else {
                        $adminFee = ceil(0.016 * $request->total);
                        $total = ceil($adminFee + $request->total);
                    }
                }else{
                    $adminFee=0;
                    $total=$request->total;
                }
            }else{
                $adminFee=0;
                $total=$request->total;
            }
            $newPurchase->step_type=0;
            $newPurchase->note_order=$request->note;
            //$newPurchase->code=$request->code;

            $newPurchase->md_sc_transaction_status_id=$status;

            if($request->shipping_cost>0)
            {
                $newPurchase->is_with_load_shipping=1;
                $newPurchase->shipping_cost=$request->shipping_cost;
            }else{
                $newPurchase->shipping_cost=0;
                $newPurchase->is_with_load_shipping=0;
            }

            $newPurchase->second_code=(is_null($request->second_code) || $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $newPurchase->ref_code=$request->ref_code;
            $newPurchase->coa_trans_id=(!is_null($trans))?$trans[1]:null;
            $newPurchase->md_transaction_type_id=(!is_null($trans))?$trans[0]:null;
            $newPurchase->is_debet=$request->is_debet;
            $newPurchase->tax=$request->tax;
            $newPurchase->tax_percentage=$request->tax_percentage;
            $newPurchase->promo=$request->discount;
            $newPurchase->promo_percentage=$request->discount_percentage;
            $newPurchase->total=$total-$adminFee;
            $newPurchase->admin_fee=$adminFee;
            $newPurchase->is_from_step_delivery=1;
            $newPurchase->save();

            $sc_product_id = $request->sc_product_id;
            if (empty($sc_product_id)) {
                return response()->json([
                    'message' => "Data item penjualan tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $userHelpers=json_decode($request->user_helpers);
            $productList="(";
            $productWithQuantitys=[];
            $totalOriginal=0;

            foreach ($sc_product_id as $key => $item)
            {
                $productList.=($key==0)?$sc_product_id[$key]:",".$sc_product_id[$key];

                $originalPrice=strtr($price[$key], array('.' => '', ',' => '.'));

                if($isMultiUnit[$key]==0){

                    $newPurchaseDetail[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_sale_order_id'=>$newPurchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'created_at'=>$request->created_at,
                        'updated_at'=>$request->created_at,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                    ];
                }else{

                    $newPurchaseDetail[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_sale_order_id'=>$newPurchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'created_at'=>$request->created_at,
                        'updated_at'=>$request->created_at,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                    ];
                }

                $productWithQuantitys[]=(object)[
                    'id'=>$sc_product_id[$key],
                    'origin_quantity'=>$quantity[$key],
                    'quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                    'price'=>$quantity[$key]*$originalPrice,
                    'product_name'=>$productName[$key],
                    'unit_name'=>$unitName[$key],
                    'origin_unit_name'=>'',
                ];

                $totalOriginal+=$quantity[$key]*($originalPrice);



            }

            //Perhitungan Komisi Cuy,master komisi belum fix

            $productList.=")";
            if(!empty($userHelpers)){
                $userHelperIds="(".$request->user_helper_ids.")";
                $checkCommission=CommissionUtil::resultCommission($productList,$productWithQuantitys,$userHelperIds,$merchantId,$totalOriginal,$newPurchase->id,$timestamp,$request->_timezone);
                // cek sekalian tipe datanya, jika hasil [] agar lolos
                if($checkCommission===false){
                    return response()->json([
                        'message' => "Terjadi kesalahan saat melakukan perhitungan komisi!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }else{
                    DB::table('merchant_commission_lists')->insert($checkCommission);

                }

            }


            SaleOrderDetail::insert($newPurchaseDetail);

            if($request->is_debet==1)
            {
                MerchantAr::insert([
                    'sync_id'=>$merchantId.Uuid::uuid4()->toString(),
                    'md_merchant_id'=>$merchantId,
                    'residual_amount'=>($request->is_with_load_shipping==0)?$request->total-$request->paid_nominal:($request->total-$request->shipping_cost)-$request->paid_nominal,
                    'paid_nominal'=>$request->paid_nominal,
                    'is_paid_off'=>0,
                    'arable_id'=>$newPurchase->id,
                    'arable_type'=>'App\Models\SennaToko\SaleOrder',
                    'due_date'=>$request->due_date,
                    'timezone'=>$request->timezone,
                    'created_at'=>$request->created_at,
                    'updated_at'=>$request->created_at
                ]);

            }

            $warehouse=Controller::getGlobalWarehouse($merchantId);

            if(!is_null($trans)){
                if($trans[2]==1){
                    $arrayOfSession=[
                        'inv_id'=>$inventoryId,
                        'warehouse_id'=>$warehouse->id,
                        'user_id'=>$userId,
                        'merchant_id'=>$merchantId,
                        'coaJson'=>'',
                    ];
                    $newPurchase->json_order_online=json_encode($arrayOfSession);
                    $newPurchase->is_payment_gateway=1;
                    if($trans[0]==4){
                        $paramsQRIS = [
                            'external_id' => (string)Uuid::uuid4()->toString(),
                            'type' => 'DYNAMIC',
                            'callback_url' =>\route('pay.callback.all'),
                            'amount' =>(float)$total,
                        ];

                        if(env('APP_ENV')=='production'){
                            $qris=$this->xendit::createQRIS($paramsQRIS);
                            if (array_key_exists('message', $qris)) {
                                return response()->json([
                                    'message' => $qris['message'],
                                    'type' => 'warning',
                                    'is_modal' => false,
                                    'redirect_url' => ''
                                ]);
                            }
                        }else{
                            $qris=array (
                                'id' => (string)Uuid::uuid4()->toString(),
                                'external_id' => 'testing_id_123',
                                'nominal' => 1500,
                                'qr_string' => '0002010102##########CO.XENDIT.WWW011893600#######14220002152#####414220010303TTT####015CO.XENDIT.WWW02180000000000000000000TTT52045######ID5911XenditQRIS6007Jakarta6105121606##########3k1mOnF73h11111111#3k1mOnF73h6v53033605401163040BDB',
                                'callback_url' => 'https://yourwebsite.com/callback',
                                'type' => 'DYNAMIC',
                                'status' => 'ACTIVE',
                                'created' => '2020-01-08T18:18:18.661Z',
                                'updated' => '2020-01-08T18:18:18.661Z',
                            );
                        }

                        $destinationPath = 'public/uploads/transaction/'.$userId.'/qr/';
                        $qrCodeFile=$destinationPath.$request->code.'.png';
                        $imageQR=QrCode::size(600)
                            ->format('png')
                            ->generate($qris['qr_string']);
                        Storage::disk('s3')->put($qrCodeFile, $imageQR);
                        $newPurchase->qr_code=$qrCodeFile;
                        $newPurchase->md_merchant_payment_method_id=$trans[4];
                        $newPurchase->expired_time=Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
                        $newPurchase->qris_response=json_encode($qris);
                        $newPurchase->checkout_response=json_encode($qris);
                        $newPurchase->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $newPurchase->save();
                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($newPurchase->id),

                            ])
                        ]);
                    }elseif ($trans[0]==5){
                        $response=$this->virtualAccount->create($userId,strtoupper($trans[3]),$total,16,(string)Uuid::uuid4()->toString());
                        if (array_key_exists('message', $response)) {
                            return response()->json([
                                'message' => $response['message'],
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $newPurchase->md_merchant_payment_method_id=$trans[4];
                        $newPurchase->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                        $newPurchase->checkout_response=json_encode($response);
                        $newPurchase->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $newPurchase->save();
                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($newPurchase->id),

                            ])
                        ]);

                    }else{
                        $idEWallet=$trans[3].'_'.$trans[4];
                        if($idEWallet=='ID_OVO'){
                            if(is_null($request->mobile_number)){
                                return response()->json([
                                    'message' => 'No telpon untuk metode pembayaran OVO wajib diisi',
                                    'type' => 'warning',
                                    'is_modal' => false,
                                    'redirect_url' => ''
                                ]);
                            }

                            $ewallet = [
                                'reference_id' => (string)Uuid::uuid4()->toString(),
                                'currency' => 'IDR',
                                'amount' => $total,
                                'checkout_method' => 'ONE_TIME_PAYMENT',
                                'channel_code' => $idEWallet,
                                'channel_properties' => [
                                    'mobile_number'=>$request->mobile_number,
                                    'success_redirect_url' => route('checkout-notification', [
                                        'key' => encrypt($newPurchase->id),
                                        'status'=>2
                                    ]),
                                    'failure_redirect_url'=>route('checkout-notification', [
                                        'key' => encrypt($newPurchase->id),
                                        'status'=>5

                                    ])
                                ],
                            ];
                        }else{
                            $ewallet = [
                                'reference_id' => (string)Uuid::uuid4()->toString(),
                                'currency' => 'IDR',
                                'amount' => $total,
                                'checkout_method' => 'ONE_TIME_PAYMENT',
                                'channel_code' => $idEWallet,
                                'channel_properties' => [
                                    'success_redirect_url' => route('checkout-notification', [
                                        'key' => encrypt($newPurchase->id),
                                        'status'=>2
                                    ]),
                                    'failure_redirect_url'=>route('checkout-notification', [
                                        'key' => encrypt($newPurchase->id),
                                        'status'=>5

                                    ])
                                ],
                            ];

                        }
                        $ewalletRes=$this->xendit::createEWalletCharge($ewallet);
                        if (array_key_exists('message', $ewalletRes)) {
                            return response()->json([
                                'message' => $ewalletRes['message'],
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $newPurchase->md_merchant_payment_method_id=$trans[5];
                        $newPurchase->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                        $newPurchase->checkout_response=json_encode($ewalletRes);
                        $newPurchase->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $newPurchase->save();
                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($newPurchase->id),

                            ])
                        ]);
                    }

                }else{
                    DB::commit();

                    if(CoaSaleUtil::coaStockAdjustmentFromSaleV2($userId, $merchantId,'',$warehouse->id,$inventoryId, $newPurchase->id) == false) {
                        DB::rollBack();
                        return response()->json([
                            'message' => "Terjadi kesalahan dalam pencatatan jurnal penjualan dengan kode penjualan $newPurchase->code !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }

                    return response()->json([
                        'message' => "Data berhasil disimpan !",
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('merchant.toko.sale-order.detail', ['id' => $newPurchase->id])
                    ]);

                }
            }else{
                DB::commit();

                if(CoaSaleUtil::coaStockAdjustmentFromSaleV2($userId, $merchantId,'',$warehouse->id,$inventoryId, $newPurchase->id) == false) {
                    DB::rollBack();
                    return response()->json([
                        'message' => "Terjadi kesalahan dalam pencatatan jurnal penjualan dengan kode penjualan $newPurchase->code !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }


                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.sale-order.detail', ['id' => $newPurchase->id])
                ]);

            }


        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function save(Request $request)
    {
        try {
            $merchantId=$request->md_merchant_id;
            $userId=user_id();
            DB::beginTransaction();


            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'sell',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            date_default_timezone_set($request->timezone);
            $timestamp=$request->created_at;
            $date = new \DateTime($request->created_at);

            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            if($request->is_debet==0)
            {
                if ($request->md_transaction_type_id == '-1') {
                    return response()->json([
                        'message' => 'Metode Pembayaran belum dipilih !',
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }else{
                if($request->due_date<$request->created_at){
                    return response()->json([
                        'message' => "Tanggal jatuh tempo tidak boleh kurang dari tanggal transaksi !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }

            if ($request->md_sc_shipping_category_id == '-1') {
                return response()->json([
                    'message' => 'Metode Pengiriman belum dipilih !',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $inventoryId=merchant_detail_multi_branch($merchantId)->md_inventory_method_id;
            $warehouse=Controller::getGlobalWarehouse($merchantId);

            if($request->is_debet==0)
            {
                $trans = explode('_', $request->md_transaction_type_id);

            }else{
                $trans=null;
            }

            if($request->shipping_cost<=0)
            {
                $is_with_load_shipping=0;
            }else{

                $is_with_load_shipping=1;

            }

            if (!is_null($request->id)) {
                $sale_delivery=SaleOrder::find($request->id);
                $sale_delivery->is_step_close=1;
                $sale_delivery->save();
            }

            if(!is_null($trans)){

                if($trans[2]==1){

                    $status= TransactionStatus::UNPAID;

                }else{

                    $status=($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;

                }

            }else{

                $status=($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;


            }

            if(!is_null($trans)){
                if($trans[2]==1){
                    if($trans[0]==4){
                        $adminFee = ceil(0.008 * $request->total);
                        $total = ceil($adminFee + $request->total);
                    }elseif ($trans[0]==5){
                        $adminFee = 5000;
                        $total = ceil($adminFee + $request->total);
                    }else {
                        $adminFee = ceil(0.016 * $request->total);
                        $total = ceil($adminFee + $request->total);
                    }
                }else{
                    $adminFee=0;
                    $total=$request->total;
                }
            }else{
                $adminFee=0;
                $total=$request->total;
            }

            $userHelpers=json_decode($request->user_helpers);

            $second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $ref_code=$request->ref_code;
            $code=CodeGenerator::generateSaleOrder($merchantId);

            $purchaseId=SaleOrder::insertGetId([
                'sc_customer_id'=>($request->sc_customer_id!='-1')?$request->sc_customer_id:null,
                'code'=>$code,
                'second_code'=>$second_code,
                'ref_code'=>$ref_code,
                'step_type'=>$request->step_type,
                'is_with_load_shipping'=>$is_with_load_shipping,
                'coa_trans_id'=>(!is_null($trans))?$trans[1]:null,
                'md_transaction_type_id'=>(!is_null($trans))?$trans[0]:null,
                'qr_code'=>'-',
                'is_debet'=>$request->is_debet,
                'total'=>$total-$adminFee,
                'md_merchant_id'=>$merchantId,
                'md_sc_transaction_status_id'=>$status,
                'md_sc_shipping_category_id'=>$request->md_sc_shipping_category_id,
                'tax'=>$request->tax,
                'promo'=>$request->discount,
                'promo_percentage'=>$request->discount_percentage,
                'tax_percentage'=>$request->tax_percentage,
                'shipping_cost'=>$request->shipping_cost,
                'timezone'=>$request->timezone,
                'note_order'=>$request->note,
                'created_by'=>$userId,
                'is_approved_shop'=>1,
                'created_at'=>$timestamp,
                'updated_at'=>$timestamp,
                'admin_fee'=>$adminFee,
                'assign_to_user_helper'=>json_encode($userHelpers)
            ]);

            $sc_product_id = $request->sc_product_id;
            $prices = $request->selling_price;
            $quantity = $request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $jsonMultiUnit=$request->json_multi_unit;
            $unitList=$request->unit_list;


            $details = [];
            $stockInOut=[];
            if (empty($sc_product_id)) {
                return response()->json([
                    'message' => 'Data item penjualan tidak boleh kosong !',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            $products=$this->product::select([
                'sc_products.*',
                'u.name as unit_name'
            ])->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->whereIn('sc_products.id',$sc_product_id)
                ->get();
            $productCalculate=[];
            $arrayProductOut=[];
            $updateStockProduct="";
            $productList="(";
            $productWithQuantitys=[];
            $totalOriginal=0;
            foreach ($sc_product_id as $key =>$p) {

                if($isMultiUnit[$key]==1){
                    if($unitList[$key]=='-1' || $unitList==-1)
                    {
                        return response()->json([
                            'message' => "Ada produk yang belum memilih satuan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                }

                $productList.=($key==0)?$sc_product_id[$key]:",".$sc_product_id[$key];

                $product=$products->firstWhere('id',$sc_product_id[$key]);

                $originalPrice=strtr($prices[$key], array('.' => '', ',' => '.'));
                $stockOutPackage=[];

                if($product->is_with_stock==1)
                {
                    if($isMultiUnit[$key]==0){
                        array_push($arrayProductOut,[
                            'sc_product_id'=>$product->id,
                            'product_name'=>$product->name,
                            'product_code'=>$product->code,
                            'unit_name'=>$unitName[$key],
                            'product_name_2'=>$productName[$key],
                            'selling_price'=>$originalPrice,
                            'purchase_price'=>$product->purchase_price,
                            'quantity'=>$quantity[$key],
                            'product_stock'=>$product->stock

                        ]);
                    }else{
                        array_push($arrayProductOut,[
                            'sc_product_id'=>$product->id,
                            'product_name'=>$product->name,
                            'product_code'=>$product->code,
                            'unit_name'=>$unitName[$key],
                            'product_name_2'=>$productName[$key],
                            'selling_price'=>($originalPrice*$quantity[$key])/($nilaiKonversi[$key]*$quantity[$key]),
                            'purchase_price'=>$product->purchase_price,
                            'quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                            'product_stock'=>$product->stock

                        ]);
                    }

                }

                if($product->md_sc_product_type_id==4)
                {
                    $rawMaterial=Material::
                    select([
                        'sc_product_raw_materials.id',
                        'sc_product_raw_materials.child_sc_product_id',
                        'sc_product_raw_materials.unit_name',
                        'sc_product_raw_materials.quantity',
                        'sc_product_raw_materials.total',
                        'p.id as product_id',
                        'p.name as product_name',
                        'p.code as product_code',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'p.is_with_stock',
                        'sc_product_raw_materials.value_conversion',
                    ])
                        ->where('sc_product_raw_materials.parent_sc_product_id',$product->id)
                        ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                        ->where('sc_product_raw_materials.is_deleted',0)
                        ->where('p.is_deleted',0)
                        ->get();
                    if($rawMaterial->count()>0)
                    {
                        foreach ($rawMaterial as $rr =>$r)
                        {

                            if($r->is_with_stock==1)
                            {

                                array_push($arrayProductOut,[
                                    'sc_product_id'=>$r->child_sc_product_id,
                                    'product_name'=>$r->product_name,
                                    'product_code'=>$r->product_code,
                                    'unit_name'=>$r->unit_name,
                                    'product_name_2'=>$r->product_name.' '.$r->product_code,
                                    'selling_price'=>$r->selling_price,
                                    'purchase_price'=>$r->purchase_price,
                                    'quantity'=>$quantity[$key]*($r->quantity*$r->value_conversion),
                                    'product_stock'=>$r->stock
                                ]);
                            }
                            $stockOutPackage[]=[
                                'id'=>$r->child_sc_product_id,
                                'head'=>$product->id,
                                'stock_out'=>$quantity[$key]*($r->quantity*$r->value_conversion),
                                'amount'=>$quantity[$key]*$r->total,
                                'unit_name'=>$r->unit_name,
                                'product_name'=>$r->product_name.' '.$r->product_code,
                                'is_with_stock'=>$r->is_with_stock
                            ];

                        }
                    }
                }

                if($isMultiUnit[$key]==0){
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'quantity'=>$quantity[$key],
                        'is_bonus'=>0,
                        'sub_total'=>$quantity[$key]*($originalPrice),
                        'sc_sale_order_id'=>$purchaseId,
                        'profit'=>0,
                        'price'=>$originalPrice,
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'package_stock_out'=>json_encode($stockOutPackage),
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                    ];
                }else{
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'quantity'=>$quantity[$key],
                        'is_bonus'=>0,
                        'sub_total'=>$quantity[$key]*($originalPrice),
                        'sc_sale_order_id'=>$purchaseId,
                        'profit'=>0,
                        'price'=>$originalPrice,
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'package_stock_out'=>json_encode($stockOutPackage),
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                    ];
                }

                $productWithQuantitys[]=(object)[
                    'id'=>$sc_product_id[$key],
                    'origin_quantity'=>$quantity[$key],
                    'quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                    'price'=>$quantity[$key]*$originalPrice,
                    'product_name'=>$productName[$key],
                    'unit_name'=>$unitName[$key],
                    'origin_unit_name'=>$product->unit_name,
                ];

                $totalOriginal+=$quantity[$key]*($originalPrice);

            }


            if(!empty($arrayProductOut))
            {
                $arrays=collect($arrayProductOut)->groupBy('sc_product_id');
                $countOutOut=0;
                foreach ($arrays as $j =>$i)
                {

                    $quantity=0;
                    $firstData=$i->first();
                    foreach ($i as $ii =>$iii)
                    {
                        $quantity+=$iii['quantity'];
                    }


                    if($firstData['product_stock']<$quantity)
                    {
                        return response()->json([
                            'message' => "Stok produk  ".$firstData['product_name']." pada gudang penjualan tidak mencukupi !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }

                    if($warehouse->is_default==1)
                    {
                        $stockP=$firstData['product_stock'] - $quantity;
                        $updateStockProduct.=($countOutOut==0)?"(".$j.",".$stockP.")":",(".$j.",".$stockP.")";
                        $countOutOut++;
                    }else{
                        $stockP=$firstData['product_stock'];
                    }


                    array_push($productCalculate,[
                        'id'=>$j,
                        'amount'=>$quantity
                    ]);

                    $stockInOut[]=[
                        'sync_id' => $j . Uuid::uuid4()->toString(),
                        'sc_product_id' => $j,
                        'total' => $quantity,
                        'inv_warehouse_id' => $warehouse->id,
                        'record_stock' => $stockP,
                        'created_by' => $userId,
                        'selling_price' => $firstData['selling_price'],
                        'purchase_price' => $firstData['purchase_price'],
                        'residual_stock' => $quantity,
                        'type' => StockInventory::OUT,
                        'transaction_action' => 'Pengurangan Stok ' . $firstData['product_name'] . ' Dari Faktur Penjualan Dengan Code ' . (is_null($second_code)?$code:$second_code),
                        'stockable_type'=>'App\Models\SennaToko\SaleOrder',
                        'stockable_id'=>$purchaseId,
                        'created_at'=>$request->created_at,
                        'updated_at'=>$request->created_at,
                        'timezone'=>$request->timezone,
                        'unit_name'=>$firstData['unit_name'],
                        'product_name'=>$firstData['product_name_2']
                    ];

                }
            }

            $productList.=")";
           if(!empty($userHelpers)){
                $userHelperIds="(".$request->user_helper_ids.")";
                $checkCommission=CommissionUtil::resultCommission($productList,$productWithQuantitys,$userHelperIds,$merchantId,$totalOriginal,$purchaseId,$timestamp,$request->_timezone);

                // cek sekalian tipe datanya, jika hasil [] agar lolos
                if($checkCommission===false){
                    return response()->json([
                        'message' => "Terjadi kesalahan saat melakukan perhitungan komisi!",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }else{
                    DB::table('merchant_commission_lists')->insert($checkCommission);

                }

            }


            $arrayOfSession=[
                'productCalculate'=>$productCalculate,
                'updateStockProduct'=>ltrim($updateStockProduct, ','),
                'inv_id'=>$inventoryId,
                'warehouse_id'=>$warehouse->id,
                'code'=>$code,
                'user_id'=>$userId,
                'merchant_id'=>$merchantId,
                'coaJson'=>'',
                'timestamp'=>$timestamp,
                'is_debet'=>$request->is_debet,
                'stock_in_out'=>$stockInOut,
                'is_with_load_shipping'=>$is_with_load_shipping,
                'total'=>$total-$adminFee,
                'paid_nominal'=>$request->paid_nominal,
                'shipping_cost'=>$request->shipping_cost,
                'due_date'=>$request->due_date,
                'timezone'=>$request->timezone,
                'admin_fee'=>$adminFee,
                'purchaseId'=>$purchaseId,
            ];


            DB::table('sc_sale_order_details')->insert($details);
            DB::commit();


            if(!is_null($trans)){
                if($trans[2]==1){
                    $updateSale=SaleOrder::find($purchaseId);
                    $updateSale->json_order_online=json_encode($arrayOfSession);
                    $updateSale->is_payment_gateway=1;
                    if($trans[0]==4){
                        $paramsQRIS = [
                            'external_id' => (string)Uuid::uuid4()->toString(),
                            'type' => 'DYNAMIC',
                            'callback_url' =>\route('pay.callback.all'),
                            'amount' =>(float)$total,
                        ];

                        if(env('APP_ENV')=='production'){
                            $qris=$this->xendit::createQRIS($paramsQRIS);

                            if (array_key_exists('message', $qris)) {
                                return response()->json([
                                    'message' => $qris['message'],
                                    'type' => 'warning',
                                    'is_modal' => false,
                                    'redirect_url' => ''
                                ]);
                            }
                        }else{
                            $qris=[
                                'id' => (string)Uuid::uuid4()->toString(),
                                'external_id' => 'testing_id_123',
                                'nominal' => 1500,
                                'qr_string' => '0002010102##########CO.XENDIT.WWW011893600#######14220002152#####414220010303TTT####015CO.XENDIT.WWW02180000000000000000000TTT52045######ID5911XenditQRIS6007Jakarta6105121606##########3k1mOnF73h11111111#3k1mOnF73h6v53033605401163040BDB',
                                'callback_url' => 'https://yourwebsite.com/callback',
                                'type' => 'DYNAMIC',
                                'status' => 'ACTIVE',
                                'created' => '2020-01-08T18:18:18.661Z',
                                'updated' => '2020-01-08T18:18:18.661Z',
                            ];
                        }

                        $destinationPath = 'public/uploads/transaction/'.$userId.'/qr/';
                        $qrCodeFile=$destinationPath.$request->code.'.png';
                        $imageQR=QrCode::size(600)
                            ->format('png')
                            ->generate($qris['qr_string']);
                        Storage::disk('s3')->put($qrCodeFile, $imageQR);
                        $updateSale->qr_code=$qrCodeFile;
                        $updateSale->md_merchant_payment_method_id=$trans[4];
                        $updateSale->expired_time=Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
                        $updateSale->qris_response=json_encode($qris);
                        $updateSale->checkout_response=json_encode($qris);
                        $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $updateSale->save();
                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($purchaseId),

                            ])
                        ]);
                    }elseif ($trans[0]==5){
                        $response=$this->virtualAccount->create($userId,strtoupper($trans[3]),$total,16,(string)Uuid::uuid4()->toString());
                        if (array_key_exists('message', $response)) {
                            return response()->json([
                                'message' => $response['message'],
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $updateSale->md_merchant_payment_method_id=$trans[4];
                        $updateSale->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                        $updateSale->checkout_response=json_encode($response);
                        $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $updateSale->save();
                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($purchaseId),

                            ])
                        ]);

                    }else{
                        $idEWallet=$trans[3].'_'.$trans[4];
                        if($idEWallet=='ID_OVO'){
                            if(is_null($request->mobile_number)){
                                return response()->json([
                                    'message' => 'No telpon untuk metode pembayaran OVO wajib diisi',
                                    'type' => 'warning',
                                    'is_modal' => false,
                                    'redirect_url' => ''
                                ]);
                            }
                            $ewallet = [
                                'reference_id' => (string)Uuid::uuid4()->toString(),
                                'currency' => 'IDR',
                                'amount' => $total,
                                'checkout_method' => 'ONE_TIME_PAYMENT',
                                'channel_code' => $idEWallet,
                                'channel_properties' => [
                                    'mobile_number'=>$request->mobile_number,
                                    'success_redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                        'key' => encrypt($purchaseId),
                                        'status'=>2
                                    ]),
                                    'failure_redirect_url'=>route('merchant.toko.transaction.sale-order.checkout', [
                                        'key' => encrypt($purchaseId),
                                        'status'=>5

                                    ])
                                ],
                            ];
                        }else{
                            $ewallet = [
                                'reference_id' => (string)Uuid::uuid4()->toString(),
                                'currency' => 'IDR',
                                'amount' => $total,
                                'checkout_method' => 'ONE_TIME_PAYMENT',
                                'channel_code' => $idEWallet,
                                'channel_properties' => [
                                    'success_redirect_url' => route('checkout-notification', [
                                        'key' => encrypt($purchaseId),
                                        'status'=>2
                                    ]),
                                    'failure_redirect_url'=>route('checkout-notification', [
                                        'key' => encrypt($purchaseId),
                                        'status'=>5

                                    ])
                                ],
                            ];

                        }
                        $ewalletRes=$this->xendit::createEWalletCharge($ewallet);

                        if (array_key_exists('message', $ewalletRes)) {
                            return response()->json([
                                'message' => $ewalletRes['message'],
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $updateSale->md_merchant_payment_method_id=$trans[5];
                        $updateSale->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                        $updateSale->checkout_response=json_encode($ewalletRes);
                        $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                        $updateSale->save();
                        DB::commit();

                        return response()->json([
                            'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                            'type' => 'success',
                            'is_modal' => false,
                            'redirect_url' => route('merchant.toko.transaction.sale-order.checkout', [
                                'key' => encrypt($purchaseId),

                            ])
                        ]);
                    }

                }else{
                    Session::put('sale_faktur_'.$purchaseId,$arrayOfSession);
                    DB::commit();

                    Artisan::call('sale:faktur', ['--sale_id' =>$purchaseId]);
                    return response()->json([
                        'message' => 'Data berhasil disimpan!',
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('merchant.toko.sale-order.detail', ['id' => $purchaseId])

                    ]);
                }
            }else{
                Session::put('sale_faktur_'.$purchaseId,$arrayOfSession);
                DB::commit();

                Artisan::call('sale:faktur', ['--sale_id' =>$purchaseId]);
                return response()->json([
                    'message' => 'Data berhasil disimpan!',
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.sale-order.detail', ['id' => $purchaseId])
                ]);

            }


        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message' => 'Terjadi kesalahan di server,data gagal disimpan !',
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => '',
                'error_detail'=>$e->getMessage()
            ]);
        }
    }

    public function checkout(Request  $request)
    {
        $key=decrypt($request->key);
        $saleOrder=SaleOrder::find($key);

        $data = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.md_merchant_id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'b.name',
            'b.icon',
            'vb.code as bank_code',
            't.icon as icon_wallet',
            't.name as wallet_name'
        ])
            ->leftJoin('md_va_banks as vb', 'vb.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->leftJoin('md_banks as b', 'b.id', 'vb.md_bank_id')
            ->leftJoin('md_transaction_types as t','t.id','md_merchant_payment_methods.transaction_type_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.id', $saleOrder->md_merchant_payment_method_id)
            ->where('md_merchant_payment_methods.md_merchant_id',$saleOrder->md_merchant_id)
            ->first();

        date_default_timezone_set($saleOrder->timezone);

        if(date('Y-m-d H:i:s')>$saleOrder->expired_time){
            return Redirect::route('merchant.toko.transaction.sale-order-ar.checkout-expired',[
                'type'=>'order',
                'id'=>encrypt($saleOrder->id)
            ]);
        }

        $params = [
            "title" => "Checkout Pembayaran - ".(is_null($saleOrder->second_code)?$saleOrder->code:$saleOrder->second_code),
            "data" => $data,
            "merchant" => $saleOrder->getMerchant,
            "saleOrder" => $saleOrder,
            "expireDate" => $saleOrder->expired_time,
            'va'=>json_decode($saleOrder->checkout_response)
        ];

        return view('merchant::toko.transaction.sale-order.checkout-detail', $params);


    }


    public function checkoutAr(Request  $request)
    {
        $key=decrypt($request->key);
        $saleOrder=MerchantArDetail::find($key);

        $data = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.md_merchant_id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'b.name',
            'b.icon',
            'vb.code as bank_code',
            't.icon as icon_wallet',
            't.name as wallet_name'
        ])
            ->leftJoin('md_va_banks as vb', 'vb.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->leftJoin('md_banks as b', 'b.id', 'vb.md_bank_id')
            ->leftJoin('md_transaction_types as t','t.id','md_merchant_payment_methods.transaction_type_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.id', $saleOrder->md_merchant_payment_method_id)
            ->where('md_merchant_payment_methods.md_merchant_id',$saleOrder->getAr->md_merchant_id)
            ->first();

        if(date('Y-m-d H:i:s')>$saleOrder->expired_time){
            Redirect::route('merchant.toko.transaction.sale-order-ar.checkout-expired',[
                'type'=>'ar',
                'id'=>encrypt($saleOrder->id)
            ]);
        }

        $params = [
            "title" => "Checkout Pembayaran Piutang - ".(is_null($saleOrder->second_code)?$saleOrder->ar_detail_code:$saleOrder->second_code),
            "data" => $data,
            "merchant" => Merchant::find($saleOrder->getAr->md_merchant_id),
            "saleOrder" => $saleOrder,
            "expireDate" => $saleOrder->expired_time,
            'va'=>json_decode($saleOrder->checkout_response)
        ];


        return view('merchant::toko.transaction.sale-order.checkout-ar', $params);


    }



    public function checkoutExpired(Request  $request){

        $type=$request->type;
        $id=decrypt($request->id);

        if($type=='order'){
            $data=SaleOrder::find($id);
            $code=is_null($data->second_cde)?$data->code:$data->second_code;
            $merchantId=$data->md_merchant_id;
        }else{
            $data=MerchantArDetail::find($id);
            $code=is_null($data->second_code)?$data->ar_detail_code:$data->second_code;
            $merchantId=$data->getAr->md_merchant_id;
        }

        $params=[
            'title'=>'Pilih Metode Pembayaran Transaksi '.$code,
            'data'=>$data,
            'paymentOption'=>PaymentUtils::getPayment($merchantId),
            'type'=>$type
        ];
        return view('merchant::toko.transaction.sale-order.checkout-expired', $params);

    }

    public function deleteDetail(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=SaleOrderDetail::find($id);
            $merchantId=$data->getOrder->md_merchant_id;

            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $data->is_deleted=1;
            $data->save();
            if($data->step_type==0 || $data->step_type==3){
                $stock=$data->getSale->stock->where('sc_product_id',$data->sc_product_id)->first();
                //dd($stock);
                if($data->getPurchase->getWarehouse->is_default==1)
                {
                    $stock->getProduct->stock+=$data->quantity;
                    $stock->getProduct->save();
                }
                $stock->delete();
            }
            DB::commit();
            return response()->json([
                'message' => 'Data item penjualan berhasil dihapus !',
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => 'Terjadi kesalahan di server,data gagal dihapus !',
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function addPayment(Request  $request)
    {
        $arId=$request->ar_id;
        $ar=MerchantAr::find($arId);
        $merchantId=$ar->md_merchant_id;
        $data=new MerchantArDetail();

        $params=[
            'title'=>"Tambah Pembayaran",
            'data'=>$data,
            'ar_id'=>$arId,
            'ap'=>$ar,
            'paymentOption'=>PaymentUtils::getPayment($merchantId),
            'md_merchant_id'=>$merchantId
        ];

        return view('merchant::toko.transaction.sale-order.form-payment',$params);

    }

    public function savePayment(Request $request)
    {
        try{
            $merchantId=$request->md_merchant_id;
            $userId=user_id();
            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();
            $data=new MerchantArDetail();
            $ar=MerchantAr::find($request->ar_id);
            $paid_nominal=strtr($request->paid_nominal, array('.' => '', ',' => '.'));

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'ap',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }


            if($paid_nominal>$ar->residual_amount)
            {
                return response()->json([
                    'message' => 'Nominal pembayaran melebihi kekurangan yang harus dibayar !',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $date = new \DateTime($request->paid_date);

            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $trans=explode('_',$request->trans_coa_detail_id);

            if(!in_array($trans[0],[4,5,15])){
                if($paid_nominal>=$ar->residual_amount)
                {
                    $ar->is_paid_off=1;
                    $ar->arable->md_sc_transaction_status_id=TransactionStatus::PAID;
                    $ar->arable->save();
                }else{
                    $ar->arable->md_sc_transaction_status_id=6;
                    $ar->arable->save();
                }
                $ar->residual_amount-=$paid_nominal;
                $ar->paid_nominal+=$paid_nominal;
                $ar->save();

            }

            $destinationPath = 'public/uploads/merchant/'.$merchantId.'/acc/jurnal/ar/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }

            if(!is_null($trans)){
                if($trans[2]==1){
                    if($trans[0]==4){
                        $adminFee = ceil(0.008 * $paid_nominal);
                        $total = ceil($adminFee + $paid_nominal);
                    }elseif ($trans[0]==5){
                        $adminFee = 5000;
                        $total = ceil($adminFee + $paid_nominal);
                    }else {
                        $adminFee = ceil(0.016 * $paid_nominal);
                        $total = ceil($adminFee + $paid_nominal);
                    }
                }else{
                    $adminFee=0;
                    $total=$paid_nominal;
                }
            }else{
                $adminFee=0;
                $total=$paid_nominal;
            }

            $data->ar_detail_code=CodeGenerator::generalCode('PTR',$merchantId);
            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);

            $data->paid_nominal=$total-$adminFee;
            if(!in_array($trans[0],[4,5,15])){
                $data->paid_date=$request->paid_date;
            }

            $data->note=$request->note;
            $data->acc_merchant_ar_id=$ar->id;
            $data->created_by=$userId;
            $data->timezone=$request->_timezone;
            $data->note=$request->note;
            $data->admin_fee=$adminFee;
            $data->trans_coa_detail_id=$trans[1];
            $data->trans_proof=$fileName;
            $data->md_transaction_type_id=$trans[0];

            if(!in_array($trans[0],[4,5,15])){
                $data->md_transaction_status_id=\App\Models\MasterData\TransactionStatus::PAID;
            }else{
                $data->md_transaction_status_id=\App\Models\MasterData\TransactionStatus::UNPAID;
            }
            $data->save();

            $fromCoa=merchant_detail_multi_branch($merchantId)->coa_ar_sale_id;

            $toCoa=$data->trans_coa_detail_id;

            $codeSO=is_null($ar->arable->second_code)?$ar->arable->code:$ar->arable->second_code;
            $codeAr=is_null($data->second_code)?$data->ar_detail_code:$data->second_code;

            if(!in_array($trans[0],[4,5,15])){
                $jurnal=new Jurnal();
                $jurnal->ref_code=$data->ar_detail_code;
                $jurnal->trans_code=CodeGenerator::generalCode('JRN',$merchantId);
                $jurnal->trans_name='Pembayaran Penjualan '.$codeSO.'-'.$codeAr;
                $jurnal->trans_time=$data->paid_date;
                $jurnal->trans_note='Pembayaran Penjualan  '.$codeSO.'-'.$codeAr;
                $jurnal->trans_purpose='Pembayaran Penjualan  '.$codeSO.'-'.$codeAr;
                $jurnal->trans_amount=$data->paid_nominal;
                $jurnal->trans_proof=$fileName;
                $jurnal->md_merchant_id=$merchantId;
                $jurnal->md_user_id_created=$userId;
                $jurnal->external_ref_id=$data->id;
                $jurnal->flag_name='Pembayaran Penjualan  '.$codeSO.'-'.$codeAr;
                $jurnal->save();

                $insert=[
                    [
                        'acc_coa_detail_id'=>$fromCoa,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                ];
                JurnalDetail::insert($insert);

                DB::commit();

                return response()->json([
                    'message' => 'Data pembayaran berhasil disimpan !',
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }else{
                $arrayOfSession=[
                    'arId'=>$ar->id,
                    'paid_nominal'=>$paid_nominal,
                    'from_coa'=>$fromCoa,
                    'to_coa'=>$toCoa,
                    'userId'=>$userId,
                    'merchantId'=>$merchantId,
                    'total'=>$paid_nominal
                ];
                $data->array_insert=json_encode($arrayOfSession);

                $data->is_payment_gateway=1;
                $data->save();

                if($trans[0]==4){
                    $paramsQRIS = [
                        'external_id' => (string)Uuid::uuid4()->toString(),
                        'type' => 'DYNAMIC',
                        'callback_url' =>\route('pay.callback.all'),
                        'amount' =>(float)$total,
                    ];

                    if(env('APP_ENV')=='production'){

                        $qris=$this->xendit::createQRIS($paramsQRIS);

                        if (array_key_exists('message', $qris)) {
                            return response()->json([
                                'message' => $qris['message'],
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                    }else{
                        $qris=array (
                            'id' => (string)Uuid::uuid4()->toString(),
                            'external_id' => 'testing_id_123',
                            'nominal' => 1500,
                            'qr_string' => '0002010102##########CO.XENDIT.WWW011893600#######14220002152#####414220010303TTT####015CO.XENDIT.WWW02180000000000000000000TTT52045######ID5911XenditQRIS6007Jakarta6105121606##########3k1mOnF73h11111111#3k1mOnF73h6v53033605401163040BDB',
                            'callback_url' => 'https://yourwebsite.com/callback',
                            'type' => 'DYNAMIC',
                            'status' => 'ACTIVE',
                            'created' => '2020-01-08T18:18:18.661Z',
                            'updated' => '2020-01-08T18:18:18.661Z',
                        );
                    }

                    $destinationPath = 'public/uploads/transaction-ar/'.$userId.'/qr/';
                    $qrCodeFile=$destinationPath.$request->code.'.png';
                    $imageQR=QrCode::size(600)
                        ->format('png')
                        ->generate($qris['qr_string']);
                    Storage::disk('s3')->put($qrCodeFile, $imageQR);
                    $data->qr_code=$qrCodeFile;
                    $data->is_type_payment_gateway=2;
                    $data->md_merchant_payment_method_id=$trans[4];
                    $data->expired_time=Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
                    $data->checkout_response=json_encode($qris);
                    $data->save();
                    DB::commit();

                    return response()->json([
                        'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('merchant.toko.transaction.sale-order-ar.checkout', [
                            'key' => encrypt($data->id),

                        ])
                    ]);
                }elseif ($trans[0]==5){
                    $response=$this->virtualAccount->create($userId,strtoupper($trans[3]),$total,16,(string)Uuid::uuid4()->toString());
                    if (array_key_exists('message', $response)) {
                        return response()->json([
                            'message' => $response['message'],
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                    $data->md_merchant_payment_method_id=$trans[4];
                    $data->is_type_payment_gateway=3;
                    $data->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                    $data->checkout_response=json_encode($response);
                    $data->save();
                    DB::commit();

                    return response()->json([
                        'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('merchant.toko.transaction.sale-order-ar.checkout', [
                            'key' => encrypt($data->id),

                        ])
                    ]);

                }else {
                    $idEWallet = $trans[3] . '_' . $trans[4];
                    if ($idEWallet == 'ID_OVO') {
                        if (is_null($request->mobile_number)) {
                            return response()->json([
                                'message' => 'No telpon untuk metode pembayaran OVO wajib diisi',
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $ewallet = [
                            'reference_id' => (string)Uuid::uuid4()->toString(),
                            'currency' => 'IDR',
                            'amount' => $total,
                            'checkout_method' => 'ONE_TIME_PAYMENT',
                            'channel_code' => $idEWallet,
                            'channel_properties' => [
                                'mobile_number' => $request->mobile_number,
                                'success_redirect_url' => route('merchant.toko.transaction.sale-order-ar.checkout', [
                                    'key' => encrypt($data->id),
                                    'status' => 2
                                ]),
                                'failure_redirect_url' => route('merchant.toko.transaction.sale-order-ar.checkout', [
                                    'key' => encrypt($data->id),
                                    'status' => 5

                                ])
                            ],
                        ];
                    } else {
                        $ewallet = [
                            'reference_id' => (string)Uuid::uuid4()->toString(),
                            'currency' => 'IDR',
                            'amount' => $total,
                            'checkout_method' => 'ONE_TIME_PAYMENT',
                            'channel_code' => $idEWallet,
                            'channel_properties' => [
                                'success_redirect_url' => route('checkout-notification', [
                                    'key' => encrypt($data->id),
                                    'status' => 2
                                ]),
                                'failure_redirect_url' => route('checkout-notification', [
                                    'key' => encrypt($data->id),
                                    'status' => 5

                                ])
                            ],
                        ];

                    }
                    $ewalletRes = $this->xendit::createEWalletCharge($ewallet);
                    if (array_key_exists('message', $ewalletRes)) {
                        return response()->json([
                            'message' => $ewalletRes['message'],
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                    $data->md_merchant_payment_method_id = $trans[5];
                    $data->is_type_payment_gateway=1;
                    $data->expired_time = Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                    $data->checkout_response = json_encode($ewalletRes);
                    $data->save();
                    DB::commit();

                    return response()->json([
                        'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('merchant.toko.transaction.sale-order-ar.checkout', [
                            'key' => encrypt($data->id),

                        ])
                    ]);
                }

            }
        }catch (\Exception $e){
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => 'Terjadi kesalahan di server,data gagal disimpan !',
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function deletePayment(Request $request)
    {
        try{

            DB::beginTransaction();
            $id=$request->id;
            $data=MerchantArDetail::find($id);
            $date = new \DateTime($data->paid_date);
            $ar=MerchantAr::find($data->acc_merchant_ar_id);
            $merchantId=$ar->md_merchant_id;
            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $ar->residual_amount+=$data->paid_nominal;
            $ar->paid_nominal-=$data->paid_nominal;
            $ar->is_paid_off=0;
            $ar->save();
            if($ar->arable->paid_nominal>0)
            {
                if($ar->arable->paid_nominal==$data->paid_nominal)
                {
                    $ar->arable->paid_nominal-=$data->paid_nominal;
                }
            }
            $ar->arable->save();

            Jurnal::where([
                'external_ref_id'=>$data->id,
                'ref_code'=>$data->ar_detail_code,
                'md_merchant_id'=>$merchantId
            ])->update([
                'is_deleted'=>1
            ]);
            $data->delete();
            DB::commit();

            return response()->json([
                'message' => "Data pembayaran berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }



    public function saveRetur(Request  $request)
    {
        try{
            $userId=user_id();
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $date = new \DateTime($request->created_at);
            if($request->created_at>Carbon::now())
            {
                return response()->json([
                    'message'=>'Tanggal retur tidak boleh melebihi hari ini!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }
            DB::beginTransaction();
            $data=SaleOrder::find($id);
            $merchantId=$data->md_merchant_id;

            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$data->md_merchant_id,'so_retur',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }


            $timestamp = $request->created_at;

            TempSaleOrderDetail::where('sc_sale_order_id',$data->id)
                ->update(['is_current'=>0]);

            $retur=new ReturSaleOrder();
            $retur->sc_sale_order_id=$id;
            $retur->code=CodeGenerator::returSaleOrder($merchantId);
            $retur->md_sc_transaction_status_id=TransactionStatus::PAID;
            $retur->is_retur=1;
            $retur->step_type=$request->retur_from;
            $retur->created_at=$request->created_at;
            $retur->note=$request->note;
            $retur->reason_id=$request->reason_id;
            $retur->timezone=$request->_timezone;
            $retur->second_code=(is_null($request->second_code) || $request->second_code=='')?null:$request->second_code;
            $retur->save();
            $data->is_with_retur=1;
            $data->save();


            $arrayOfProduct=$request->sc_product_id;
            $arrayOfQuantity=$request->quantity_retur;
            $arrayQuantity=$request->quantity;
            $arrayOfPurchaseDetail=$request->sale_order_detail_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $multiUnitId=$request->multi_unit_id;
            $multiQuantity=$request->multi_quantity;
            $multiQuantityOrigin=$request->origin_multi_quantity;
            $valueConversation=$request->value_conversation;
            $jsonMultiUnit=$request->json_multi_unit;
            $total=0;
            $amountOfHpp=[];
            $temp=[];
            $details=[];
            $stockIn=[];
            $sumQuantity=0;
            foreach ($arrayOfQuantity as $key =>$item)
            {
                if($isMultiUnit[$key]==0){
                    $sumQuantity+=$arrayOfQuantity[$key];
                }else{
                    $sumQuantity+=$arrayOfQuantity[$key]*$valueConversation[$key];
                }
            }
            if($sumQuantity==0){


                return response()->json([
                    'message'=>'Jumlah pengembalian produk tidak boleh kosong',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }
            $purchaseDetail=SaleOrderDetail::whereIn('id',$arrayOfPurchaseDetail)->get();
            $updateStockProduct="";
            $productCalculate=[];
            $products=Product::whereIn('id',$arrayOfProduct)->get();
            $temporaryCalculate=[];
            $warehouse=Controller::getGlobalWarehouse($merchantId);

            foreach ($arrayOfProduct as $key =>$item)
            {
                if($isMultiUnit[$key]==0){
                    if($arrayOfQuantity[$key]>$arrayQuantity[$key])
                    {

                        return response()->json([
                            'message'=>'Jumlah pengembalian produk melebihi maksimal produk dijual',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);


                    }
                }else{
                    if(($arrayOfQuantity[$key]*$valueConversation[$key])>$multiQuantity[$key])
                    {

                        return response()->json([
                            'message'=>'Jumlah pengembalian produk melebihi maksimal produk dijual',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);


                    }
                }
                $product=$products->firstWhere('id',$arrayOfProduct[$key]);
                $saleOrderLine=$purchaseDetail->firstWhere('id',$arrayOfPurchaseDetail[$key]);


                if($saleOrderLine->is_bonus==0)
                {
                    if($retur->reason_id!=1)
                    {
                        if($product->is_with_stock==1)
                        {
                            if($isMultiUnit[$key]==0){
                                array_push($temporaryCalculate,[
                                    'sc_product_id'=>$product->id,
                                    'product_name'=>$product->name,
                                    'product_code'=>$product->code,
                                    'unit_name'=>$unitName[$key],
                                    'product_name_2'=>$productName[$key],
                                    'selling_price'=>$saleOrderLine->price,
                                    'purchase_price'=>$product->purchase_price,
                                    'quantity'=>$arrayOfQuantity[$key],
                                    'product_stock'=>$product->stock

                                ]);
                            }else{
                                array_push($temporaryCalculate,[
                                    'sc_product_id'=>$product->id,
                                    'product_name'=>$product->name,
                                    'product_code'=>$product->code,
                                    'unit_name'=>$unitName[$key],
                                    'product_name_2'=>$productName[$key],
                                    'selling_price'=>($saleOrderLine->sub_total/$multiQuantity[$key]),
                                    'purchase_price'=>$product->purchase_price,
                                    'quantity'=>$arrayOfQuantity[$key]*$valueConversation[$key],
                                    'product_stock'=>$product->stock

                                ]);
                            }

                        }

                        if($product->md_sc_product_type_id==4)
                        {
                            $rawMaterial=Material::
                            select([
                                'sc_product_raw_materials.id',
                                'sc_product_raw_materials.child_sc_product_id',
                                'sc_product_raw_materials.unit_name',
                                'sc_product_raw_materials.quantity',
                                'sc_product_raw_materials.total',
                                'p.id as product_id',
                                'p.name as product_name',
                                'p.code as product_code',
                                'p.stock',
                                'p.selling_price',
                                'p.purchase_price',
                                'p.is_with_stock',
                            ])
                                ->where('sc_product_raw_materials.parent_sc_product_id',$product->id)
                                ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                                ->where('sc_product_raw_materials.is_deleted',0)
                                ->where('p.is_deleted',0)
                                ->get();
                            if($rawMaterial->count()>0)
                            {
                                foreach ($rawMaterial as $rr =>$r)
                                {

                                    if($r->is_with_stock==1)
                                    {

                                        array_push($temporaryCalculate,[
                                            'sc_product_id'=>$r->child_sc_product_id,
                                            'product_name'=>$r->product_name,
                                            'product_code'=>$r->product_code,
                                            'unit_name'=>$r->unit_name,
                                            'product_name_2'=>$r->product_name.' '.$r->product_code,
                                            'selling_price'=>$r->selling_price,
                                            'purchase_price'=>$r->purchase_price,
                                            'quantity'=>$arrayOfQuantity[$key]*$r->quantity,
                                            'product_stock'=>$r->stock
                                        ]);
                                    }

                                }
                            }
                        }
                    }

                    if($isMultiUnit[$key]==0){
                        $total+=$arrayOfQuantity[$key]*$saleOrderLine->price;

                    }else{
                        if($multiQuantity[$key]==0){
                            $total+=(($arrayOfQuantity[$key]*$valueConversation[$key])/1)*$saleOrderLine->sub_total;

                        }else{
                            $total+=(($arrayOfQuantity[$key]*$valueConversation[$key])/$multiQuantityOrigin[$key])*$saleOrderLine->sub_total;
                        }

                    }


                    $temp[]=[
                        'sync_id'=>$arrayOfProduct[$key].Uuid::uuid4()->toString(),
                        'sc_product_id'=>$saleOrderLine->sc_product_id,
                        'quantity'=>$saleOrderLine->quantity,
                        'sub_total'=>$saleOrderLine->sub_total,
                        'price'=>$saleOrderLine->price,
                        'sc_sale_order_id'=>$data->id,
                        'is_current'=>1,
                        'created_at'=>$saleOrderLine->created_at,
                        'updated_at'=>$saleOrderLine->updated_at
                    ];


                    if($arrayOfQuantity[$key]!=0)
                    {
                        if($isMultiUnit[$key]==0){

                            $details[]=[
                                'sc_product_id'=>$arrayOfProduct[$key],
                                'quantity'=>$arrayOfQuantity[$key],
                                'sub_total'=>$arrayOfQuantity[$key]*$saleOrderLine->price,
                                'sc_sale_order_detail_id'=>$saleOrderLine->id,
                                'sc_retur_sale_order_id'=>$retur->id,
                                'type'=>'minus',
                                'coa_inv_id'=>$saleOrderLine->coa_inv_id,
                                'coa_hpp_id'=>$saleOrderLine->coa_hpp_id,
                                'created_at'=>$timestamp,
                                'updated_at'=>$timestamp,
                                'unit_name'=>$unitName[$key],
                                'product_name'=>$productName[$key],
                                'is_multi_unit'=>0,
                                'multi_quantity'=>0,
                                'value_conversation'=>1,
                                'multi_unit_id'=>null,
                                'json_multi_unit'=>null,
                            ];
                        }else{
                            if($jsonMultiUnit[$key]==null || $jsonMultiUnit[$key]=='null'){
                                $jsonMulti=null;
                            }else{
                                $jsonMulti=json_encode(json_decode($jsonMultiUnit[$key]));
                            }
                            if($multiQuantity[$key]==0){
                                $subPriceRetur=(($arrayOfQuantity[$key]*$valueConversation[$key])/1)*$saleOrderLine->sub_total;
                            }else{
                                $subPriceRetur=(($arrayOfQuantity[$key]*$valueConversation[$key])/$multiQuantityOrigin[$key])*$saleOrderLine->sub_total;

                            }

                            $details[]=[
                                'sc_product_id'=>$arrayOfProduct[$key],
                                'quantity'=>$arrayOfQuantity[$key],
                                'sub_total'=>$subPriceRetur,
                                'sc_sale_order_detail_id'=>$saleOrderLine->id,
                                'sc_retur_sale_order_id'=>$retur->id,
                                'type'=>'minus',
                                'coa_inv_id'=>$saleOrderLine->coa_inv_id,
                                'coa_hpp_id'=>$saleOrderLine->coa_hpp_id,
                                'created_at'=>$timestamp,
                                'updated_at'=>$timestamp,
                                'unit_name'=>$unitName[$key],
                                'product_name'=>$productName[$key],
                                'is_multi_unit'=>1,
                                'multi_quantity'=>$arrayOfQuantity[$key]*$valueConversation[$key],
                                'value_conversation'=>$valueConversation[$key],
                                'multi_unit_id'=>$multiUnitId[$key],
                                'json_multi_unit'=>$jsonMulti,
                            ];

                        }

                    }
                }

            }



            if(!empty($temporaryCalculate))
            {
                $arrays=collect($temporaryCalculate)->groupBy('sc_product_id');
                $countOutOut=0;

                foreach ($arrays as $j =>$i)
                {
                    $quantity=0;
                    $firstData=$i->first();
                    foreach ($i as $ii =>$iii)
                    {
                        $quantity+=$iii['quantity'];
                    }

                    if($warehouse->is_default==1)
                    {
                        $stockP=$firstData['product_stock'] + $quantity;
                        $updateStockProduct.=($countOutOut==0)?"(".$j.",".$stockP.")":",(".$j.",".$stockP.")";
                        $countOutOut++;
                    }else{
                        $stockP=$firstData['product_stock'];
                    }

                    array_push($productCalculate,[
                        'id'=>$j,
                        'amount'=>$quantity
                    ]);

                    $stockIn[]=[
                        'sync_id' => $j . Uuid::uuid4()->toString(),
                        'sc_product_id' => $j,
                        'total' => $quantity,
                        'inv_warehouse_id' => $warehouse->id,
                        'record_stock' => $stockP,
                        'created_by' => $userId,
                        'selling_price' => $firstData['selling_price'],
                        'purchase_price' => $firstData['purchase_price'],
                        'residual_stock' => $quantity,
                        'transaction_action' => 'Penambahan Stok ' . $firstData['product_name'] . ' Dari Retur Transaksi Dengan Code ' . (is_null($retur->second_code)?$retur->code:$retur->second_code),
                        'type' => StockInventory::IN,
                        'stockable_type'=>'App\Models\SennaToko\ReturSaleOrder',
                        'stockable_id'=>$retur->id,
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'timezone'=>$request->timezone,
                        'unit_name'=>$firstData['unit_name'],
                        'product_name'=>$firstData['product_name_2']
                    ];

                }
            }


            if(!empty($productCalculate))
            {
                $poDelivery=SaleOrder::where('code',$data->code)
                    ->where('step_type',3)
                    ->where('md_merchant_id',$data->md_merchant_id)
                    ->first();

                if(is_null($poDelivery))
                {
                    $purchaseId=$data->id;
                }else{
                    $purchaseId=$poDelivery->id;
                }
                $inventoryId=merchant_detail_multi_branch($merchantId)->md_inventory_method_id;

                $calculateInventory=InventoryUtilV2::inventoryCodev2($inventoryId,$warehouse->id,$productCalculate,'plus',$purchaseId);

                if($calculateInventory==false)
                {

                    return response()->json([
                        'message' => "Terjadi kesalahan saat penambahan stok dari retur penjualan dipersediaan",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => '',
                        'is_with_datatable'=>false,
                    ]);


                }
                $updateStockInv="";
                foreach ($calculateInventory as $key => $n)
                {
                    $amountOfHpp[]=[
                        'id'=>$n['id'],
                        'total'=>$n['total']
                    ];

                    $updateStockInv.=($key==0)?"(".$n['sc_stock_inventory_id'].",".$n['total_increment'].")":",(".$n['sc_stock_inventory_id'].",".$n['total_increment'].")";
                }

                if($updateStockInv!=""){
                    DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                }

            }
            if($updateStockProduct!="") {
                DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            $updateStockProduct
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
            }


            $data->save();
            if(!empty($stockIn)){
                StockInventory::insert($stockIn);
            }

            $retur->total=$total;
            $retur->created_by=$userId;
            $retur->timezone=$request->_timezone;
            $retur->sync_id=$id.Uuid::uuid4()->toString();
            $retur->save();

            if($retur->reason_id==3)
            {
                if($data->ar->is_paid_off==0){
                    $data->ar->residual_amount-=$total;
                    $data->ar->save();
                }
            }


            TempSaleOrderDetail::insert($temp);
            ReturSaleOrderDetail::insert($details);
            DB::commit();

            if($retur->reason_id!=1)
            {
                if($request->retur_from==0){
                    if(CoaSaleUtil::coaReturSaleOrderv2($userId,$merchantId,'',$retur,$timestamp,$amountOfHpp)==false)
                    {
                        DB::rollBack();

                        return response()->json([
                            'message' => "Pencatatan jurnal untuk retur penjualan terjadi kesalahan",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => '',
                            'is_with_datatable'=>false,
                        ]);


                    }
                }
            }

            return response()->json([
                'message' => "Data retur penjualan berhasil disimpan",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => \route('merchant.toko.retur-sale-order.data'),
                'is_with_datatable'=>false,
            ]);


        }catch (\Exception $e)
        {
            DB::rollBack();

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }
    }

    public function saveReturDeliv(Request  $request)
    {
        try{
            $userId=user_id();
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $date = new \DateTime($request->created_at);

            DB::beginTransaction();
            $data=SaleOrder::find($id);
            $merchantId=$data->md_merchant_id;
            $warehouse=Controller::getGlobalWarehouse($merchantId);

            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$data->md_merchant_id,'so_retur',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $timestamp = $request->created_at;


            TempSaleOrderDetail::where('sc_sale_order_id',$data->id)
                ->update(['is_current'=>0]);
            $retur=new ReturSaleOrder();
            $retur->sc_sale_order_id=$id;
            $retur->second_code=(is_null($request->second_code) || $request->second_code=='')?null:$request->second_code;
            $retur->code=CodeGenerator::returSaleOrder($merchantId);
            $retur->md_sc_transaction_status_id=TransactionStatus::PAID;
            $retur->is_retur=1;
            $retur->step_type=$request->retur_from;
            $retur->created_at=$request->created_at;
            $retur->note=$request->note;
            $retur->reason_id=$request->reason_id;
            $retur->timezone=$request->_timezone;
            $retur->save();
            $data->is_with_retur=1;
            $data->save();


            $arrayOfProduct=$request->sc_product_id;
            $arrayOfQuantity=$request->quantity_retur;
            $arrayQuantity=$request->quantity;
            $arrayOfPurchaseDetail=$request->sale_order_detail_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $multiUnitId=$request->multi_unit_id;
            $multiQuantity=$request->multi_quantity;
            $valueConversation=$request->value_conversation;
            $jsonMultiUnit=$request->json_multi_unit;
            $multiQuantityOrigin=$request->origin_multi_quantity;

            $total=0;
            $amountOfHpp=[];
            $temp=[];
            $details=[];
            $stockIn=[];
            $sumQuantity=0;
            foreach ($arrayOfQuantity as $key =>$item)
            {
                $sumQuantity+=$arrayOfQuantity[$key];
            }

            if($sumQuantity==0){

                return response()->json([
                    'message'=>'Jumlah pengembalian produk tidak boleh kosong',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $purchaseDetail=SaleOrderDetail::whereIn('id',$arrayOfPurchaseDetail)->get();
            $productAll=Product::whereIn('id',$arrayOfProduct)->get();
            $updateStockProduct="";
            $productCalculate=[];
            $temporaryCalculate=[];

            foreach ($arrayOfProduct as $key =>$item)
            {
                $saleOrderLine=$purchaseDetail->firstWhere('id',$arrayOfPurchaseDetail[$key]);
                $product=$productAll->firstWhere('id',$arrayOfProduct[$key]);

                if($isMultiUnit[$key]==0){
                    if($arrayOfQuantity[$key]>$arrayQuantity[$key])
                    {

                        return response()->json([
                            'message'=>'Jumlah pengembalian produk melebihi maksimal produk dijual',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);


                    }
                }else{
                    if(($arrayOfQuantity[$key]*$valueConversation[$key])>$multiQuantity[$key])
                    {

                        return response()->json([
                            'message'=>'Jumlah pengembalian produk melebihi maksimal produk dijual',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);


                    }
                }

                if($saleOrderLine->is_bonus==0)
                {
                    if($retur->reason_id!=1)
                    {
                        if($product->is_with_stock==1)
                        {
                            if($isMultiUnit[$key]==0){
                                array_push($temporaryCalculate,[
                                    'sc_product_id'=>$product->id,
                                    'product_name'=>$product->name,
                                    'product_code'=>$product->code,
                                    'unit_name'=>$unitName[$key],
                                    'product_name_2'=>$productName[$key],
                                    'selling_price'=>$saleOrderLine->price,
                                    'purchase_price'=>$product->purchase_price,
                                    'quantity'=>$arrayOfQuantity[$key],
                                    'product_stock'=>$product->stock

                                ]);
                            }else{
                                array_push($temporaryCalculate,[
                                    'sc_product_id'=>$product->id,
                                    'product_name'=>$product->name,
                                    'product_code'=>$product->code,
                                    'unit_name'=>$unitName[$key],
                                    'product_name_2'=>$productName[$key],
                                    'selling_price'=>($saleOrderLine->sub_total/$multiQuantity[$key]),
                                    'purchase_price'=>$product->purchase_price,
                                    'quantity'=>$arrayOfQuantity[$key]*$valueConversation[$key],
                                    'product_stock'=>$product->stock

                                ]);
                            }

                        }

                        if($product->md_sc_product_type_id==4)
                        {
                            $rawMaterial=Material::
                            select([
                                'sc_product_raw_materials.id',
                                'sc_product_raw_materials.child_sc_product_id',
                                'sc_product_raw_materials.unit_name',
                                'sc_product_raw_materials.quantity',
                                'sc_product_raw_materials.total',
                                'p.id as product_id',
                                'p.name as product_name',
                                'p.code as product_code',
                                'p.stock',
                                'p.selling_price',
                                'p.purchase_price',
                                'p.is_with_stock',
                            ])
                                ->where('sc_product_raw_materials.parent_sc_product_id',$product->id)
                                ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                                ->where('sc_product_raw_materials.is_deleted',0)
                                ->where('p.is_deleted',0)
                                ->get();

                            if($rawMaterial->count()>0)
                            {
                                foreach ($rawMaterial as $rr =>$r)
                                {

                                    if($r->is_with_stock==1)
                                    {

                                        array_push($temporaryCalculate,[
                                            'sc_product_id'=>$r->child_sc_product_id,
                                            'product_name'=>$r->product_name,
                                            'product_code'=>$r->product_code,
                                            'unit_name'=>$r->unit_name,
                                            'product_name_2'=>$r->product_name.' '.$r->product_code,
                                            'selling_price'=>$r->selling_price,
                                            'purchase_price'=>$r->purchase_price,
                                            'quantity'=>$arrayOfQuantity[$key]*$r->quantity,
                                            'product_stock'=>$r->stock
                                        ]);
                                    }

                                }
                            }
                        }

                    }

                    if($isMultiUnit[$key]==0){
                        $total+=$arrayOfQuantity[$key]*$saleOrderLine->price;

                    }else{
                        if($multiQuantity[$key]==0){
                            $total+=(($arrayOfQuantity[$key]*$valueConversation[$key])/1)*$saleOrderLine->sub_total;

                        }else{
                            $total+=(($arrayOfQuantity[$key]*$valueConversation[$key])/$multiQuantityOrigin[$key])*$saleOrderLine->sub_total;
                        }

                    }


                    $temp[]=[
                        'sync_id'=>$arrayOfProduct[$key].Uuid::uuid4()->toString(),
                        'sc_product_id'=>$saleOrderLine->sc_product_id,
                        'quantity'=>$saleOrderLine->quantity,
                        'sub_total'=>$saleOrderLine->sub_total,
                        'price'=>$saleOrderLine->price,
                        'sc_sale_order_id'=>$data->id,
                        'is_current'=>1,
                        'created_at'=>$saleOrderLine->created_at,
                        'updated_at'=>$saleOrderLine->updated_at
                    ];

                    if($arrayOfQuantity[$key]!=0)
                    {
                        if($isMultiUnit[$key]==0){

                            $details[]=[
                                'sc_product_id'=>$arrayOfProduct[$key],
                                'quantity'=>$arrayOfQuantity[$key],
                                'sub_total'=>$arrayOfQuantity[$key]*$saleOrderLine->price,
                                'sc_sale_order_detail_id'=>$saleOrderLine->id,
                                'sc_retur_sale_order_id'=>$retur->id,
                                'type'=>'minus',
                                'coa_inv_id'=>$saleOrderLine->coa_inv_id,
                                'coa_hpp_id'=>$saleOrderLine->coa_hpp_id,
                                'created_at'=>$timestamp,
                                'updated_at'=>$timestamp,
                                'unit_name'=>$unitName[$key],
                                'product_name'=>$productName[$key],
                                'is_multi_unit'=>0,
                                'multi_quantity'=>0,
                                'value_conversation'=>1,
                                'multi_unit_id'=>null,
                                'json_multi_unit'=>null,
                            ];
                        }else{
                            if($jsonMultiUnit[$key]==null || $jsonMultiUnit[$key]=='null'){
                                $jsonMulti=null;
                            }else{
                                $jsonMulti=json_encode(json_decode($jsonMultiUnit[$key]));
                            }
                            if($multiQuantity[$key]==0){
                                $subPriceRetur=(($arrayOfQuantity[$key]*$valueConversation[$key])/1)*$saleOrderLine->sub_total;
                            }else{
                                $subPriceRetur=(($arrayOfQuantity[$key]*$valueConversation[$key])/$multiQuantityOrigin[$key])*$saleOrderLine->sub_total;

                            }

                            $details[]=[
                                'sc_product_id'=>$arrayOfProduct[$key],
                                'quantity'=>$arrayOfQuantity[$key],
                                'sub_total'=>$subPriceRetur,
                                'sc_sale_order_detail_id'=>$saleOrderLine->id,
                                'sc_retur_sale_order_id'=>$retur->id,
                                'type'=>'minus',
                                'coa_inv_id'=>$saleOrderLine->coa_inv_id,
                                'coa_hpp_id'=>$saleOrderLine->coa_hpp_id,
                                'created_at'=>$timestamp,
                                'updated_at'=>$timestamp,
                                'unit_name'=>$unitName[$key],
                                'product_name'=>$productName[$key],
                                'is_multi_unit'=>1,
                                'multi_quantity'=>$arrayOfQuantity[$key]*$valueConversation[$key],
                                'value_conversation'=>$valueConversation[$key],
                                'multi_unit_id'=>$multiUnitId[$key],
                                'json_multi_unit'=>$jsonMulti,
                            ];

                        }

                    }
                }

            }



            if(!empty($temporaryCalculate))
            {
                $arrays=collect($temporaryCalculate)->groupBy('sc_product_id');
                $countOutOut=0;

                foreach ($arrays as $j =>$i)
                {

                    $quantity=0;
                    $firstData=$i->first();
                    foreach ($i as $ii =>$iii)
                    {
                        $quantity+=$iii['quantity'];
                    }

                    if($warehouse->is_default==1)
                    {
                        $stockP=$firstData['product_stock'] + $quantity;
                        $updateStockProduct.=($countOutOut==0)?"(".$j.",".$stockP.")":",(".$j.",".$stockP.")";
                        $countOutOut++;
                    }else{
                        $stockP=$firstData['product_stock'];
                    }

                    array_push($productCalculate,[
                        'id'=>$j,
                        'amount'=>$quantity
                    ]);

                    $stockIn[]=[
                        'sync_id' => $j . Uuid::uuid4()->toString(),
                        'sc_product_id' => $j,
                        'total' => $quantity,
                        'inv_warehouse_id' => $warehouse->id,
                        'record_stock' => $stockP,
                        'created_by' => $userId,
                        'selling_price' => $firstData['selling_price'],
                        'purchase_price' => $firstData['purchase_price'],
                        'residual_stock' => $quantity,
                        'type' => StockInventory::IN,
                        'transaction_action' => 'Penambahan Stok ' . $firstData['product_name'] . ' Dari Retur Pengiriman Penjualan Dengan Code ' . str_replace('P','PD',$data->code),
                        'stockable_type'=>'App\Models\SennaToko\ReturSaleOrder',
                        'stockable_id'=>$retur->id,
                        'created_at'=>$request->created_at,
                        'updated_at'=>$request->created_at,
                        'timezone'=>$data->timezone,
                        'unit_name'=>$firstData['unit_name'],
                        'product_name'=>$firstData['product_name_2']
                    ];

                }
            }

            $inventoryId=merchant_detail_multi_branch($merchantId)->md_inventory_method_id;


            if(!empty($productCalculate))
            {
                $calculateInventory=InventoryUtilV2::inventoryCodev2($inventoryId,$warehouse->id,$productCalculate,'plus',$data->id);
                if($calculateInventory==false)
                {

                    return response()->json([
                        'message' => "Terjadi kesalahan saat penambahan stok dari retur pengiriman penjualan",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => '',
                        'is_with_datatable'=>false,
                    ]);
                }
                $updateStockInv="";
                foreach ($calculateInventory as $key => $n)
                {
                    $amountOfHpp[]=[
                        'id'=>$n['id'],
                        'total'=>$n['total']
                    ];

                    $updateStockInv.=($key==0)?"(".$n['sc_stock_inventory_id'].",".$n['total_increment'].")":",(".$n['sc_stock_inventory_id'].",".$n['total_increment'].")";
                }

                if($updateStockInv!=""){
                    DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                }

            }
            if($updateStockProduct!="") {
                DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            $updateStockProduct
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
            }

            StockInventory::insert($stockIn);

            $retur->total=$total;
            $retur->created_by=$userId;
            $retur->timezone=$request->_timezone;
            $retur->sync_id=$id.Uuid::uuid4()->toString();
            $retur->save();

            if($retur->reason_id==3)
            {
                $data->ar->residual_amount-=$total;
                $data->ar->save();
            }

            TempSaleOrderDetail::insert($temp);
            ReturSaleOrderDetail::insert($details);

            if($retur->reason_id!=1)
            {
                if($request->retur_from==0){
                    if(CoaSaleUtil::coaReturSaleOrderv2($userId,$merchantId,'',$retur,$timestamp,$amountOfHpp)==false)
                    {
                        return response()->json([
                            'message' => "Pencatatan jurnal untuk retur penjualan terjadi kesalahan",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => '',
                            'is_with_datatable'=>false,
                        ]);
                    }
                }
            }
            DB::commit();

            return response()->json([
                'message' => "Data retur penjualan berhasil disimpan",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => \route('merchant.toko.retur-sale-order.data'),
                'is_with_datatable'=>false,
            ]);



        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }
    }

    public function deleteRetur(Request  $request)
    {
        try{

            DB::beginTransaction();
            $id=$request->id;
            $data=ReturSaleOrder::find($id);
            $totalBack=0;

            $purchase=SaleOrder::find($data->sc_sale_order_id);
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($purchase->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $data->is_deleted=1;
            $data->save();

            $purchase->is_with_retur=0;
            $purchase->save();

            $warehouse=parent::getGlobalWarehouse($purchase->md_merchant_id);
            $inventoryId=(is_null(merchant_detail_multi_branch($purchase->md_merchant_id)->md_inventory_method_id))?null:
                merchant_detail_multi_branch($purchase->md_merchant_id)->md_inventory_method_id;

            if($data->reason_id!=1)
            {
                $productCalculateOut=[];
                $updateStockProduct="";
                $updateStockInv="";
                $pId=[];
                $temporaryCalculate=[];

                foreach ($data->getDetail as $d)
                {
                    $totalBack+=$d->sub_total;
                    $product=Product::find($d->sc_product_id);

                    if($product->is_with_stock==1)
                    {
                        array_push($temporaryCalculate,[
                            'sc_product_id'=>$product->id,
                            'product_name'=>$product->name,
                            'product_code'=>$product->code,
                            'unit_name'=>is_null($d->unit_name)?'':$d->unit_name,
                            'product_name_2'=>is_null($d->product_name)?$product->name.' '.$product->code : $d->product_name,
                            'selling_price'=>$d->sub_total/($d->quantity*$d->value_conversation),
                            'purchase_price'=>$product->purchase_price,
                            'quantity'=>$d->quantity*$d->value_conversation,
                            'product_stock'=>$product->stock

                        ]);
                    }
                    if($product->md_sc_product_type_id==4)
                    {
                        $rawMaterial=Material::
                        select([
                            'sc_product_raw_materials.id',
                            'sc_product_raw_materials.child_sc_product_id',
                            'sc_product_raw_materials.unit_name',
                            'sc_product_raw_materials.quantity',
                            'sc_product_raw_materials.total',
                            'p.id as product_id',
                            'p.name as product_name',
                            'p.code as product_code',
                            'p.stock',
                            'p.selling_price',
                            'p.purchase_price',
                            'p.is_with_stock',
                        ])
                            ->where('sc_product_raw_materials.parent_sc_product_id',$product->id)
                            ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                            ->where('sc_product_raw_materials.is_deleted',0)
                            ->where('p.is_deleted',0)
                            ->get();

                        if($rawMaterial->count()>0)
                        {
                            foreach ($rawMaterial as $rr =>$r)
                            {

                                if($r->is_with_stock==1)
                                {

                                    array_push($temporaryCalculate,[
                                        'sc_product_id'=>$r->child_sc_product_id,
                                        'product_name'=>$r->product_name,
                                        'product_code'=>$r->product_code,
                                        'unit_name'=>$r->unit_name,
                                        'product_name_2'=>$r->product_name.' '.$r->product_code,
                                        'selling_price'=>$r->selling_price,
                                        'purchase_price'=>$r->purchase_price,
                                        'quantity'=>$d->quantity*$r->quantity,
                                        'product_stock'=>$r->stock
                                    ]);
                                }

                            }
                        }
                    }

                }
                if(!empty($temporaryCalculate))
                {
                    $arrays=collect($temporaryCalculate)->groupBy('sc_product_id');
                    $countOutOut=0;

                    foreach ($arrays as $j =>$i)
                    {
                        $quantity=0;
                        $firstData=$i->first();
                        $pId[]=$j;
                        foreach ($i as $ii =>$iii)
                        {
                            $quantity+=$iii['quantity'];
                        }

                        if($warehouse->is_default==1)
                        {
                            $stockP=$firstData['product_stock'] - $quantity;
                            $updateStockProduct.=($countOutOut==0)?"(".$j.",".$stockP.")":",(".$j.",".$stockP.")";
                            $countOutOut++;
                        }

                        array_push($productCalculateOut,[
                            'id'=>$j,
                            'amount'=>$quantity
                        ]);

                    }
                }

                if(!empty($productCalculateOut))
                {
                    $calculateInventory=InventoryUtilV2::inventoryCodev2($inventoryId,$warehouse->id,$productCalculateOut,'minus');

                    if($calculateInventory==false)
                    {

                        return response()->json([
                            'message' => "Terjadi kesalahan saat pengurangan stok dari retur penjualan dipersediaan",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => '',
                            'is_with_datatable'=>false,
                        ]);
                    }

                    foreach ($calculateInventory as $key => $n)
                    {
                        $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                    }


                    if($updateStockInv!=""){
                        DB::statement("
                update sc_stock_inventories as t set
                        residual_stock = c.column_a
                    from (values
                        $updateStockInv
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                    }

                    if($updateStockProduct!=""){
                        DB::statement("
                update sc_products as t set
                        stock = c.column_a
                    from (values
                        $updateStockProduct
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                    }
                    StockInventory::where('stockable_type','App\Models\SennaToko\ReturSaleOrder')
                        ->whereIn('sc_product_id',$pId)
                        ->where('stockable_id',$data->id)->update([
                            'is_deleted'=>1
                        ]);
                }


                if(!is_null($purchase->ar))
                {
                    $purchase->ar->residual_amount+=$totalBack;
                    $purchase->ar->save();

                    if($purchase->ar->residual_amount==0)
                    {
                        $purchase->ar->is_paid_off=0;
                        $purchase->ar->save();
                        $purchase->md_sc_transaction_status_id=6;
                        $purchase->save();
                    }
                }

                $jurnal=Jurnal::where([
                    'external_ref_id'=>$data->id,
                    'ref_code'=>$data->code,
                    'md_merchant_id'=>$purchase->md_merchant_id
                ])->first();

                $jurnal->is_deleted=1;
                $jurnal->save();
            }


            DB::commit();

            return response()->json([
                'message'=>'Data retur berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }
    }

    public function deleteReturDeliv(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=ReturSaleOrder::find($id);
            $totalBack=0;

            $purchase=SaleOrder::find($data->sc_sale_order_id);

            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($purchase->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $data->is_deleted=1;
            $data->save();
            $warehouse=parent::getGlobalWarehouse($purchase->md_merchant_id);
            $inventoryId=(is_null(merchant_detail_multi_branch($purchase->md_merchant_id)->md_inventory_method_id))?null:
                merchant_detail_multi_branch($purchase->md_merchant_id)->md_inventory_method_id;
            if($purchase->is_step_close==1)
            {
                return response()->json([
                    'message'=>'Transaksi tidak bisa dihapus sebelum menghapus faktur pembelian dengan kode transaksi '.$purchase->code.'',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $purchase->is_with_retur=0;
            $purchase->save();


            if($data->reason_id!=1)
            {
                $productCalculateOut=[];
                $updateStockProduct="";
                $updateStockInv="";
                $temporaryCalculate=[];
                $pId=[];
                foreach ($data->getDetail as $d)
                {

                    $totalBack+=$d->sub_total;
                    $product=Product::find($d->sc_product_id);

                    if($product->is_with_stock==1)
                    {
                        array_push($temporaryCalculate,[
                            'sc_product_id'=>$product->id,
                            'product_name'=>$product->name,
                            'product_code'=>$product->code,
                            'unit_name'=>is_null($d->unit_name)?'':$d->unit_name,
                            'product_name_2'=>is_null($d->product_name)?$product->name.' '.$product->code : $d->product_name,
                            'selling_price'=>$d->sub_total/($d->quantity*$d->value_conversation),
                            'purchase_price'=>$product->purchase_price,
                            'quantity'=>$d->quantity*$d->value_conversation,
                            'product_stock'=>$product->stock

                        ]);
                    }
                    if($product->md_sc_product_type_id==4)
                    {
                        $rawMaterial=Material::
                        select([
                            'sc_product_raw_materials.id',
                            'sc_product_raw_materials.child_sc_product_id',
                            'sc_product_raw_materials.unit_name',
                            'sc_product_raw_materials.quantity',
                            'sc_product_raw_materials.total',
                            'p.id as product_id',
                            'p.name as product_name',
                            'p.code as product_code',
                            'p.stock',
                            'p.selling_price',
                            'p.purchase_price',
                            'p.is_with_stock',
                        ])
                            ->where('sc_product_raw_materials.parent_sc_product_id',$product->id)
                            ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                            ->where('sc_product_raw_materials.is_deleted',0)
                            ->where('p.is_deleted',0)
                            ->get();

                        if($rawMaterial->count()>0)
                        {
                            foreach ($rawMaterial as $rr =>$r)
                            {

                                if($r->is_with_stock==1)
                                {

                                    array_push($temporaryCalculate,[
                                        'sc_product_id'=>$r->child_sc_product_id,
                                        'product_name'=>$r->product_name,
                                        'product_code'=>$r->product_code,
                                        'unit_name'=>$r->unit_name,
                                        'product_name_2'=>$r->product_name.' '.$r->product_code,
                                        'selling_price'=>$r->selling_price,
                                        'purchase_price'=>$r->purchase_price,
                                        'quantity'=>$d->quantity*$r->quantity,
                                        'product_stock'=>$r->stock
                                    ]);
                                }

                            }
                        }
                    }

                }
                if(!empty($temporaryCalculate))
                {
                    $arrays=collect($temporaryCalculate)->groupBy('sc_product_id');
                    $countOutOut=0;

                    foreach ($arrays as $j =>$i)
                    {
                        $quantity=0;
                        $firstData=$i->first();
                        $pId[]=$j;
                        foreach ($i as $ii =>$iii)
                        {
                            $quantity+=$iii['quantity'];
                        }

                        if($warehouse->is_default==1)
                        {
                            $stockP=$firstData['product_stock'] - $quantity;
                            $updateStockProduct.=($countOutOut==0)?"(".$j.",".$stockP.")":",(".$j.",".$stockP.")";
                            $countOutOut++;
                        }

                        array_push($productCalculateOut,[
                            'id'=>$j,
                            'amount'=>$quantity
                        ]);

                    }
                }


                if(!empty($productCalculateOut))
                {
                    $calculateInventory=InventoryUtilV2::inventoryCodev2($inventoryId,$warehouse->id,$productCalculateOut,'minus');

                    if($calculateInventory==false)
                    {

                        return response()->json([
                            'message' => "Terjadi kesalahan saat pengurangan stok dari retur penjualan dipersediaan",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => '',
                            'is_with_datatable'=>false,
                        ]);
                    }

                    foreach ($calculateInventory as $key => $n)
                    {
                        $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                    }

                    if($updateStockInv!=""){
                        DB::statement("
                update sc_stock_inventories as t set
                        residual_stock = c.column_a
                    from (values
                        $updateStockInv
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                    }

                    if($updateStockProduct!=""){
                        DB::statement("
                update sc_products as t set
                        stock = c.column_a
                    from (values
                        $updateStockProduct
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                    }
                }


                StockInventory::where('stockable_type','App\Models\SennaToko\ReturSaleOrder')
                    ->whereIn('sc_product_id',$pId)
                    ->where('stockable_id',$data->id)->delete();


                if(!is_null($purchase->ar))
                {
                    $purchase->ar->residual_amount+=$totalBack;
                    $purchase->ar->save();

                    if($purchase->ar->residual_amount==0)
                    {
                        $purchase->ar->is_paid_off=0;
                        $purchase->ar->save();
                        $purchase->md_sc_transaction_status_id=6;
                        $purchase->save();
                    }
                }

            }



            DB::commit();

            return response()->json([
                'message'=>'Data retur berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }
    }

    public function detailRetur(Request  $request)
    {
        $data=ReturSaleOrder::find($request->id);
        $params=[
            'title'=>'Detail',
            'data'=>$data
        ];

        return view('merchant::toko.transaction.sale-order.detail-retur',$params);


    }

    public function voidTransaction(Request  $request)
    {
        try{
            DB::beginTransaction();
            $orderId=$request->id;
            $data=SaleOrder::find($orderId);
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if(is_null($data))
            {
                return response()->json([
                    'message' => "Data penjualan tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            if($data->getRetur->where('is_deleted',0)->count()>0)
            {
                return response()->json([
                    'message' => "Silahkan menghapus data retur penjualan pada transaksi ini terlebih dahulu !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }


            $soDelivery=SaleOrder::where('code',$data->code)
                ->where('step_type',3)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->first();

            SaleOrder::where('code',$data->code)
                ->where('step_type',2)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->update([
                    'is_step_close'=>0
                ]);

            if(!is_null($soDelivery))
            {
                $soDelivery->is_step_close=0;
                $soDelivery->save();
            }

            foreach (StockSaleMapping::where('sc_sale_order_id',$data->id)->get() as $stockMap)
            {
                $totalRetur=0;
                foreach ($data->getRetur->where('reason_id','!=',1)->where('is_deleted',0) as $r)
                {
                    foreach ($r->getDetail->where('sc_product_id',$stockMap->sc_product_id) as $d)
                    {
                        if($d->is_multi_unit==1)
                        {
                            $totalRetur+=$d->multi_quantity;

                        }else{
                            $totalRetur+=$d->quantity;

                        }
                    }
                }
                $stockInv=StockInventory::find($stockMap->sc_stock_inventory_id);
                $stockInv->residual_stock+=$stockMap->amount-$totalRetur;
                $stockInv->save();
                $product=Product::find($stockMap->sc_product_id);
                $product->stock+=$stockMap->amount-$totalRetur;
                $product->save();
            }
            $data->is_deleted=1;
            $data->save();
            Jurnal::where(['ref_code'=>$data->code,
                'external_ref_id'=>$data->id,
                'md_merchant_id'=>$data->md_merchant_id

            ])
                ->update(['is_deleted'=>1]);

            StockInventory::where('stockable_type','App\Models\SennaToko\SaleOrder')
                ->where('stockable_id',$data->id)
                ->delete();

            foreach ($data->getRetur as $retur)
            {
                $retur->is_deleted=1;
                $retur->save();

                StockInventory::where('stockable_type','App\Models\SennaToko\ReturSaleOrder')
                    ->where('stockable_id',$retur->id)
                    ->delete();

                Jurnal::where(['ref_code'=>$retur->code,
                    'external_ref_id'=>$retur->id,
                    'md_merchant_id'=>$data->md_merchant_id
                ])
                    ->update(['is_deleted'=>1]);
            }
            if($data->is_debet==1)
            {
                $data->ar->is_deleted=1;
                $data->ar->save();
                foreach ($data->ar->getDetail as $ar)
                {
                    $ar->is_deleted=1;
                    $ar->save();
                    Jurnal::where(['ref_code'=>$ar->ar_detail_code,
                        'external_ref_id'=>$ar->id,
                        'md_merchant_id'=>$data->md_merchant_id

                    ])
                        ->update(['is_deleted'=>1]);
                }
            }

            DB::commit();

            return response()->json([
                'message' => "Data transaksi penjualan berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }

    public function deleteSale(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=SaleOrder::find($id);
            $date = new \DateTime($data->created_at);

            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if($data->step_type==2)
            {
                SaleOrder::where('code',$data->code)
                    ->where('md_merchant_id',$data->md_merchant_id)
                    ->where('step_type',1)
                    ->update([
                        'is_step_close'=>0
                    ]);
            }

            if(is_null($data))
            {
                return response()->json([
                    'message' => "Data penjualan tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            $data->is_deleted=1;
            $data->save();

            DB::commit();

            return response()->json([
                'message' => "Data transaksi penjualan berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }

    }

    public function deleteDelivery(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=SaleOrder::find($id);
            $date = new \DateTime($data->created_at);

            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if(is_null($data))
            {
                return response()->json([
                    'message' => "Data penjualan tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if($data->getRetur->where('is_deleted',0)->count()>0)
            {
                return response()->json([
                    'message' => "Silahkan menghapus data retur penjualan pada transaksi ini terlebih dahulu !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            foreach (StockSaleMapping::where('sc_sale_order_id',$data->id)->get() as $stockMap)
            {
                $totalRetur=0;
                foreach ($data->getRetur->where('reason_id','!=',1)->where('is_deleted',0) as $r)
                {
                    foreach ($r->getDetail->where('sc_product_id',$stockMap->sc_product_id) as $d)
                    {
                        if($d->is_multi_unit==1){

                            $totalRetur+=$d->multi_quantity;
                        }else{
                            $totalRetur+=$d->quantity;

                        }
                    }
                };
                $stockInv=StockInventory::find($stockMap->sc_stock_inventory_id);
                $stockInv->residual_stock+=$stockMap->amount-$totalRetur;
                $stockInv->save();
                $product=Product::find($stockMap->sc_product_id);
                $product->stock+=$stockMap->amount-$totalRetur;
                $product->save();
            }
            StockInventory::where('stockable_type','App\Models\SennaToko\SaleOrder')
                ->where('stockable_id',$data->id)
                ->delete();

            $data->is_step_close=1;
            $data->is_deleted=1;
            $data->save();

            SaleOrder::where('code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->where('step_type',2)
                ->update([
                    'is_step_close'=>0
                ]);

            DB::commit();

            return response()->json([
                'message' => "Data transaksi penjualan berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }

    }

    function getCustomer(Request  $request)
    {
        $search=$request->key;
        $customer=CustomerEntity::listOption($search);
        $response = [];
        foreach($customer as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->kode .' '.$item->nama_pelanggan,
            ];
        }

        return response()->json($response);

    }


    function getContact(Request  $request)
    {
        $search=$request->key;
        $contact=ContactEntity::listOption($search);;
        $response=[];
        foreach($contact as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->kode .' '.$item->nama,
                "type"=>$item->type
            ];
        }

        return response()->json($response);

    }

}

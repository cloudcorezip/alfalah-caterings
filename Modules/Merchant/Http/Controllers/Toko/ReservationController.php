<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;

use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;
use Modules\Merchant\Entities\Toko\ReservationEntity;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ClinicMedicalRecord;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantBranch;
use Ramsey\Uuid\Uuid;
use PDF;
use Carbon\Carbon;
use App\Utils\Merchant\MerchantUtil;

class ReservationController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('reservation')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.toko.reservation.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.toko.reservation.datatable');
                Route::get('/add', [static::class, 'add'])
                    ->name('merchant.toko.reservation.add');
                Route::get('/create', [static::class, 'create'])
                    ->name('merchant.toko.reservation.create');

                Route::post('/save-member', [static::class, 'saveMember'])
                    ->name('merchant.toko.reservation.save-member');

                Route::get('/detail', [static::class, 'detail'])
                    ->name('merchant.toko.reservation.detail');

                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.toko.reservation.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.toko.reservation.delete');

                Route::post('/create-queue', [static::class, 'createQueue'])
                    ->name('merchant.toko.reservation.create-queue');

                Route::get('/print/{id}', [static::class, 'printPdf'])
                    ->name('merchant.toko.reservation.print-pdf');

                Route::post('/reload-data', [static::class, 'reloadData'])
                    ->name('merchant.toko.reservation.reload-data');

                Route::post('/export-data', [static::class, 'exportData'])
                    ->name('merchant.toko.reservation.export-data');

                Route::post('/calculate-age', [static::class, 'calculateAge'])
                    ->name('merchant.toko.reservation.calculate-age');

            });
    }

    protected function _getHeadAndBranch()
    {
        $merchant = Merchant::find(merchant_id());
        if($merchant->is_branch == 0){
            $head = $merchant;
        } else {
            $merchantBranch = MerchantBranch::where('branch_merchant_id', $merchant->id)
                                            ->where('is_active', 1)
                                            ->first();
            $head = Merchant::find($merchantBranch->head_merchant_id);
        }

        $data = MerchantBranch::
                select([
                    'md_merchant_branchs.id',
                    'b.name as nama_cabang',
                    'md_merchant_branchs.email_branch as email_cabang',
                    'b.id as branch_merchant_id',
                    'u.id as branch_user_id'
                ])
                    ->join('md_merchants as m','m.id','md_merchant_branchs.head_merchant_id')
                    ->join('md_merchants as b','b.id','md_merchant_branchs.branch_merchant_id')
                    ->join('md_users as u', 'u.id', 'b.md_user_id')
                    ->orderBy('md_merchant_branchs.id','desc')
                    ->where('md_merchant_branchs.is_active', 1)
                    ->where('m.id', $head->id)
                    ->get();

        return collect([
            "head" => $head,
            "data" => $data
        ]);

    }

    protected function _generateUniqueCode($userId, $merchantId ,$prefix, $plus)
    {
        $merchant = Merchant::find($merchantId);
        $branch = $this->_getHeadAndBranch();

        $userIds = [$branch['head']->md_user_id];
        foreach($branch['data'] as $key => $item){
            array_push($userIds, $item->branch_user_id);
        }

        $incremental = CustomerEntity::whereIn('md_user_id', $userIds)->count() + $plus;
        $code = $prefix.sprintf("%07s", $incremental);
        $check = CustomerEntity::where('code', $code)
                    ->whereIn('md_user_id', $userIds)
                    ->first();
        if(is_null($check)){
            return $code;
        } else {
            return $this->_generateUniqueCode($userId, $merchantId, $prefix, $plus + 1);
        }
    }



    protected function _generateQueueCode($merchantId, $rDate, $plus)
    {
        $cDate = date('ymd', strtotime($rDate));
        $total = ClinicReservation::where('md_merchant_id', $merchantId)
                                            ->whereRaw("reservation_date::date between '$rDate' and '$rDate'")
                                            ->whereNotNull('queue_number')
                                            ->get();
        $totalData = ClinicReservation::where('md_merchant_id', $merchantId)
                                    ->get();
        $row = count($total) + $plus;
        $rowReservation = count($totalData) + $plus;

        $queue = "A-".sprintf("%02s", $row);
        $reservationNumber = $merchantId.'-'.$cDate.'-'.sprintf("%02s", $rowReservation);

        $check = ClinicReservation::where('md_merchant_id', merchant_id())
                                    ->whereRaw("reservation_date::date between '$rDate' and '$rDate'")
                                    ->where('queue_number', $queue)
                                    ->first();
        if(is_null($check)){
            return [
                "queue" =>$queue,
                "reservation_number" => $reservationNumber
            ];
        } else {
            return $this->_generateQueueCode($merchantId, $rDate, $plus + 1);
        }
    }

    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $firstDate=Carbon::now()->firstOfMonth();
        $params=[
            'title'=>'Reservasi',
            'tableColumns'=>ReservationEntity::dataTableColumns(),
            'add'=>(get_role()==3)?ReservationEntity::add(true):
                    ReservationEntity::add(true),
            'key_val'=>$request->key,
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'date' => $firstDate->toDateString()
        ];

        return view('merchant::toko.reservation.index',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ReservationEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public function create(Request $request)
    {
        try {
            $id = $request->id;
            $userId = user_id();
            $merchantId = merchant_id();
            $page = $request->page;
            $customerId = $request->member;
            $code = $this->_generateUniqueCode($userId, $merchantId,'PS-', 1);

            $acceptedPage = ['cust', 'service', 'print'];

            if(!in_array($page, $acceptedPage)){
                return abort(404);
            }

            if(!is_null($id)){
                $data=ReservationEntity::with(['getMerchant', 'getCustomer', 'getProduct', 'getStaff.getUser'])
                                        ->where('is_deleted', 0)
                                        ->where('id', $id)
                                        ->first();
                if(is_null($data)){
                    return abort(404);
                }
                $customer = CustomerEntity::find($data->getCustomer->id);
            }else{
                $data=new ReservationEntity();
                $customer = is_null($customerId)? new CustomerEntity(): CustomerEntity::where('md_user_id', $userId)
                                                                                            ->where('id', $customerId)
                                                                                            ->where('is_deleted', 0)
                                                                                            ->first();
            }

            if(is_null($customer)){
                return abort(404);
            }

            $service = Product::where('md_user_id', $userId)
                                ->where('is_with_stock', 0)
                                ->where('is_deleted', 0)
                                ->get();
            $staff = MerchantStaff::with(['getUser' => function($query){
                                        $query->where('md_role_id', 23);
                                    }])
                                    ->where('md_merchant_id', $merchantId)
                                    ->where('is_deleted', 0)
                                    ->where('is_active', 1)
                                    ->get();

            $doctor = $staff->filter(function($item){
                return (!is_null($item->getUser));
            });

            switch($page)
            {
                case "cust":
                    $title = (is_null($id))? 'Tambah Pasien Baru': 'Edit Pasien';
                    break;
                case "service":
                    $title = (is_null($id))? 'Tambah Layanan': 'Edit Layanan';
                    break;
                case "print":
                    $title = "Cetak Kartu";
                    break;
                default:
                    $title = "";
                    break;
            }

            $params=[
                'title'=>$title,
                'data'=>$data,
                'jobs' => Job::all(),
                'religions' => generateReligion(),
                'mediaInfo' => CustomerEntity::generateMediaInfo(),
                'maritalStatus' => CustomerEntity::generateMaritalStatus(),
                'page' => $page,
                'customerId' => $customerId,
                'service' => $service,
                'doctor' => $doctor,
                'customer' => $customer,
                'code' => $code
            ];

            return view('merchant::toko.reservation.form-create',$params);
        } catch(\Exception $e)
        {
            return abort(404);
        }
    }

    public function add(Request $request)
    {
        $id = $request->id;
        $userId = user_id();
        $merchantId = merchant_id();
        $merchant = Merchant::find($merchantId);

        if(!is_null($id)){

            $data=ReservationEntity::find($id);
        }else{
            $data=new ReservationEntity();
        }

        $customer = CustomerEntity::select([
            "sc_customers.id",
            "sc_customers.name as name",
            "sc_customers.code as code",
            "sc_customers.gender as gender"
        ])
        ->join('md_users as u', 'u.id', 'sc_customers.md_user_id')
        ->join('md_merchants as m', 'm.md_user_id', 'u.id')
        ->whereIn('m.id',MerchantUtil::getBranch($merchant->id,1))
        ->where('sc_customers.is_deleted', 0)
        ->get();

        $service = Product::where('md_user_id', $userId)
                                    ->where('is_with_stock', 0)
                                    ->where('is_deleted', 0)
                                    ->get();
        $staff = MerchantStaff::with(['getUser' => function($query){
                                    $query->where('md_role_id', 23);
                                }])
                                ->where('md_merchant_id', $merchantId)
                                ->where('is_deleted', 0)
                                ->get();

        $doctor = $staff->filter(function($item){
            return (!is_null($item->getUser));
        });

        $params=[
            'title'=>(is_null($id))?'Tambah Reservasi':'Edit Reservasi',
            'data'=>$data,
            'customer' => $customer,
            'service' => $service,
            'doctor' => $doctor,
            'merchant' => $merchant
        ];

        return view('merchant::toko.reservation.form',$params);
    }

    public function saveMember(Request $request)
    {
        try {
            date_default_timezone_set($request->_timezone);
            $customerId=$request->sc_customer_id;
            $id = $request->id; // reservation id

            if(!is_null($customerId)){
                $data=CustomerEntity::find($customerId);
            }else{
                $data=new CustomerEntity();
            }

            $data->code=$request->code;
            $data->name=$request->name;
            $data->md_user_id=user_id();
            $data->email=$request->email;
            $data->phone_number=$request->phone_number;
            $data->address=$request->address;
            $data->type_address = $request->type_address;
            $data->city_name = $request->city_name;
            $data->region_name = $request->region_name;
            $data->postal_code = $request->postal_code;
            $data->gender = $request->gender;
            $data->sc_customer_level_id=$request->sc_customer_level_id;
            $data->identity_card_number = $request->identity_card_number;
            $data->date_of_birth = $request->date_of_birth;
            $data->md_job_id = $request->md_job_id;
            $data->media_info = $request->media_info;
            $data->religion = $request->religion;
            $data->marital_status = $request->marital_status;
            $data->age = $request->age;

            $data->save();

            if(is_null($id)){
                return response()->json([
                    'message'=>'Member berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=> route('merchant.toko.reservation.create', ['page'=>'service','member'=>$data->id])
                ]);
            } else {
                return response()->json([
                    'message'=>'Member berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=> route('merchant.toko.reservation.create', ['id'=>$id,'page'=>'service','member'=>$data->id])
                ]);
            }

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $userId = user_id();
            $merchantId = merchant_id();
            $code = $this->_generateUniqueCode($userId, $merchantId,'PS-', 1);

            $merchantStaff = MerchantStaff::find($request->md_merchant_staff_id);
            $permit = DB::select("
            select
                id
            from
                md_merchant_staff_permit_submissions mmsps
            where
                md_staff_user_id = $merchantStaff->md_user_id
                and
                mmsps.start_date < '$request->reservation_date'
                and
                mmsps.end_date > '$request->reservation_date'
            ");

            if(count($permit) > 0){
                return response()->json([
                    'message'=>'Dokter tidak bertugas pada saat tanggal reservasi yang anda pilih !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            $id=$request->id;
            $rDate = $request->reservation_date;

            if(!is_null($id)){
                $data=ClinicReservation::find($id);
                if($data->status != 0){
                    return response()->json([
                        'message'=>'Data reservasi tidak dapat diubah dkarenakan reservasi sedang berjalan / sudah selesai !',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]); 
                }
            }else{
                $data=new ClinicReservation();
            }

            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            // if($request->reservation_date < date("Y-m-d h:i:s"))
            // {
            //     return response()->json([
            //         'message'=>'Tanggal reservasi tidak boleh kurang dari sekarang !',
            //         'type'=>'warning',
            //         'is_modal'=>false,
            //         'redirect_url'=>''
            //     ]);
            // }

            $checkReservationDate = DB::select("
            select
                id,
                to_char(reservation_date, 'HH24:MI') as hour
            from 
                clinic_reservations
            where
                md_merchant_id = $merchantId
                and
                reservation_date::date = '$request->reservation_date'
                and
                is_deleted = 0
            ");

            $hour = Carbon::parse($request->reservation_date)->format('H:i');
            $oldReservationDate = $data->reservation_date;
            
            // if(in_array($hour, array_column($checkReservationDate, 'hour')) && !in_array($data->id, array_column($checkReservationDate, 'id'))){
            //     return response()->json([
            //         'message'=>'Tanggal dan waktu reservasi tidak tersedia !',
            //         'type'=>'warning',
            //         'is_modal'=>false,
            //         'redirect_url'=>''
            //     ]);
            // }

            $checkByDate = ClinicReservation::where("reservation_date", $request->reservation_date)
                                            ->where("id","!=", $id)
                                            ->first();

            if(!is_null($checkByDate)){
                return response()->json([
                    'message'=>'Tanggal dan waktu reservasi tidak tersedia !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }
            
            if(date('Y-m-d', strtotime($oldReservationDate)) != date('Y-m-d', strtotime($request->reservation_date))){
                $queue = $this->_generateQueueCode(merchant_id(), $rDate, 1);
                $data->queue_number = $queue["queue"];
                $data->counter_code = "A";
                $data->reservation_number = $queue["reservation_number"];
            }

            $data->md_merchant_id = merchant_id();
            $data->sc_customer_id = $request->sc_customer_id;
            $data->md_merchant_staff_id = $request->md_merchant_staff_id;
            $data->sc_product_id = $request->sc_product_id;
            $data->reservation_date = $request->reservation_date;
            $data->save();

            if(is_null($id)){
                $medicalRecord = new ClinicMedicalRecord();
                $medicalRecord->md_merchant_id = merchant_id();
                $medicalRecord->clinic_reservation_id = $data->id;
                $medicalRecord->save();
            } else {
                $medicalRecord = ClinicMedicalRecord::where('clinic_reservation_id', $data->id)
                                                    ->first();
            }

            $product = Product::find($data->sc_product_id);

            $prodJson = [
                [
                    "name" => $product->name,
                    "price" => $product->selling_price,
                    "quantity" => 1,
                    "sub_total" => $product->selling_price,
                    "sc_product_id" => $product->id
                ]
            ];

            $medicalRecord->service = json_encode($prodJson);
            $medicalRecord->save();

            $customer = CustomerEntity::find($data->sc_customer_id);

            if(is_null($customer->code) || $customer->code == ""){
                $customer->code = $code;
                $customer->save();
            }

            DB::commit();

            if($request->is_from_create == 1){
                return response()->json([
                    'message'=>'Data reservasi berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>route('merchant.toko.reservation.create', ['id'=>$data->id,'page'=>'print'])
                ]);
            } else {
                return response()->json([
                    'message'=>'Data reservasi berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>route('merchant.toko.reservation.add', ['id'=> $data->id])
                ]);
            }

        }catch (\Exception $e)
        {
            DB::rollback();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function detail(Request $request)
    {
        try {
            $id = $request->id;
            $userId = user_id();
            $merchantId = merchant_id();
            $data=ReservationEntity::with(['getMerchant', 'getCustomer', 'getProduct', 'getStaff.getUser'])
                                    ->where('is_deleted', 0)
                                    ->where('id', $id)
                                    ->first();

            $params=[
                'title'=>'Detail Reservasi',
                'data'=>$data,
                'maritalStatus'=> generateMaritalStatus(),
                'religion' => generateReligion()
            ];

            return view('merchant::toko.reservation.detail',$params);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server, gagal memuat data!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=> route('merchant.toko.reservation.index')
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=ReservationEntity::find($id);
            $data->is_deleted=1;
            $data->save();
            return response()->json([
                'message'=>'Data reservasi berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal dihapus!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }

    public function createQueue(Request $request)
    {
        try {
            $id = $request->id;
            $data = ReservationEntity::find($id);

            $rDate = date('Y-m-d H:i:s');
            
            if(is_null($data->queue_number)){
                $queue = $this->_generateQueueCode(merchant_id(), $rDate, 1);

                $data->queue_number = $queue["queue"];
                $data->counter_code = "A";
                $data->reservation_date = $rDate;
                $data->save();

                return response()->json([
                    'message'=>'Nomor antrian untuk reservasi no '.$data->reservation_number.' berhasil dibuat!',
                    'type'=>'success',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            } else {
                return response()->json([
                    'message'=>'Nomor antrian untuk reservasi no '.$data->reservation_number.' sudah dibuat!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal dihapus!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function printPdf(Request $request)
    {
        try {
            $id = $request->id;
            $data = ReservationEntity::with(['getMerchant','getCustomer', 'getProduct', 'getStaff.getUser'])
                                        ->where('is_deleted', 0)
                                        ->where('id', $id)
                                        ->first();
            $params = [
                "title" => "Reservasi ".$data->reservation_number,
                "data" => $data
            ];

            $pdf = PDF::loadview('merchant::toko.reservation.print-pdf', $params)->setPaper('a6');
            return $pdf->stream('Reservasi_'.$data->reservation_number.'_'.date('Y-m-d h:i:s').'.pdf');
        } catch(\Exception $e)
        {
            return abort(404);
        }
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ReservationEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $searchParams['start_date'] = $request->input('start_date');
        $searchParams['end_date'] = $request->input('end_date');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Reservasi',
            'searchKey' => $searchKey,
            'tableColumns'=>ReservationEntity::dataTableColumns(),
            'key_val'=>$request->key,


        ];
        return view('merchant::toko.reservation.list',$params);

    }

    public function exportData(Request $request)
    {
        try{
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $data = ReservationEntity::getDataForDataTable();
            $searchKey = $request->input('search_key');
            $searchFilter = $request->session()->get($searchKey, []);
            $searchFilter = is_array($searchFilter) ? $searchFilter : [];

            foreach (ReservationEntity::getFilterMap() as $key => $field) {
                if (isset($searchFilter[$key]) && $searchFilter[$key] != '-') {
                    $data->where([$field => $searchFilter[$key]]);
                }
            }

            $startDate=$request->start_date;
            $endDate=$request->end_date;
            if(!is_null($startDate) && !is_null($endDate)){
                $data->whereRaw("
                        clinic_reservations.reservation_date::date between '$startDate' and '$endDate'
                    ");
            }

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Reservasi ");
            $header = ['No', 'Nomor Reservasi','Nomor Member', 'Nama Lengkap','Tanggal', 'Dokter'];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;

            foreach ($data->get() as $key => $item) {
                $dataShow = [
                    $key+1,
                    $item->nomor_reservasi,
                    $item->nomor_member,
                    $item->nama_lengkap,
                    $item->nama_layanan,
                    $item->dokter,
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-reservasi_' . merchant_id()."_".date('Y-m-d-h-i-s') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);

            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                let success = document.querySelector('#successExport');
                if(success){
                    toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Terjadi kesalahan di server, data gagal di export !','danger',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";
        }

    }

    public function calculateAge(Request $request)
    {
        try {
            $dateOfBirth = $request->date;
            $today = date("Y-m-d");
            $diff = date_diff(date_create($dateOfBirth), date_create($today));

            return $diff->format('%y');
        
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return 0;
        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Toko;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\SaleOrderDetail;
use App\Utils\Accounting\AccUtil;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\MasterData\TransactionType;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrderDetail;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\TempPurchaseOrderDetail;
use App\Models\SennaToko\MultiUnit;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaPurchaseUtil;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\PaymentUtils;
use Carbon\Carbon;
use Modules\Merchant\Entities\Acc\JournalEntity;
use Modules\Merchant\Entities\Toko\CustLevelEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use App\Models\SennaToko\Supplier;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\Merchant\ProductUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;
use QrCode;
use Image;
use function GuzzleHttp\Psr7\str;


class PurchaseOrderController extends Controller
{

    protected $purchase;
    protected $message;
    protected $product;
    protected $ap;
    protected $multiunit;

    public function __construct()
    {
        $this->purchase=PurchaseOrder::class;
        $this->message=Message::getInstance();
        $this->product=Product::class;
        $this->ap=MerchantAp::class;
        $this->multiunit=MultiUnit::class;
    }

    public function routeWeb()
    {
        Route::prefix('transaction/purchase-order')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/add/{page}/{id}', 'Toko\PurchaseOrderController@add')
                    ->name('merchant.toko.transaction.purchase-order.add');
                Route::post('/save', 'Toko\PurchaseOrderController@save')
                    ->name('merchant.toko.transaction.purchase-order.save');

                Route::post('/void-transaction', 'Toko\PurchaseOrderController@voidTransaction')
                    ->name('merchant.toko.transaction.purchase-order.void');
                Route::post('/delete-purchase', 'Toko\PurchaseOrderController@deletePurchase')
                    ->name('merchant.toko.transaction.purchase-order.delete');
                Route::post('/delete-delivery', 'Toko\PurchaseOrderController@deleteDelivery')
                    ->name('merchant.toko.transaction.purchase-order.delete-delivery');

                //offer
                Route::post('/save-offer-order', 'Toko\PurchaseOrderController@saveOffer')
                    ->name('merchant.toko.transaction.purchase-order.save-offer-order');
                //order
                Route::post('/save-order', 'Toko\PurchaseOrderController@saveOrder')
                    ->name('merchant.toko.transaction.purchase-order.save-order');
                //delivery
                Route::post('/save-delivery', 'Toko\PurchaseOrderController@saveDelivery')
                    ->name('merchant.toko.transaction.purchase-order.save-delivery');
                //invoice
                Route::post('/save-invoice', 'Toko\PurchaseOrderController@saveInvoice')
                    ->name('merchant.toko.transaction.purchase-order.save-invoice');

                //payment
                Route::post('/add-payment', 'Toko\PurchaseOrderController@addPayment')
                    ->name('merchant.toko.transaction.purchase-order.add-payment');
                Route::post('/save-payment', 'Toko\PurchaseOrderController@savePayment')
                    ->name('merchant.toko.transaction.purchase-order.save-payment');
                Route::post('/delete-payment', 'Toko\PurchaseOrderController@deletePayment')
                    ->name('merchant.toko.transaction.purchase-order.delete-payment');
                Route::post('/delete-detail', 'Toko\PurchaseOrderController@deleteDetail')
                    ->name('merchant.toko.transaction.purchase-order.delete-detail');

                //retur
                Route::get('/add-retur', 'Toko\PurchaseOrderController@addRetur')
                    ->name('merchant.toko.transaction.purchase-order.add-retur');
                Route::post('/save-retur', 'Toko\PurchaseOrderController@saveRetur')
                    ->name('merchant.toko.transaction.purchase-order.save-retur');
                Route::post('/detail-retur', 'Toko\PurchaseOrderController@detailRetur')
                    ->name('merchant.toko.transaction.purchase-order.detail-retur');
                Route::post('/delete-retur', 'Toko\PurchaseOrderController@deleteRetur')
                    ->name('merchant.toko.transaction.purchase-order.delete-retur');

                //retur deliv
                Route::get('/add-retur-deliv', 'Toko\PurchaseOrderController@addReturDeliv')
                    ->name('merchant.toko.transaction.purchase-order.add-retur-deliv');
                Route::post('/save-retur-deliv', 'Toko\PurchaseOrderController@saveReturDeliv')
                    ->name('merchant.toko.transaction.purchase-order.save-retur-deliv');
                Route::post('/delete-retur-deliv', 'Toko\PurchaseOrderController@deleteReturDeliv')
                    ->name('merchant.toko.transaction.purchase-order.delete-retur-deliv');
                //payment
                Route::get('/payment-list', 'Toko\PurchaseOrderController@getPaymentList')
                    ->name('merchant.toko.transaction.purchase-order.payment-list');



            });
    }


    public  function add($page,$id){

        if($id==-1){
            $data=new $this->purchase;
            $payment=null;
        }else{
            $data=$this->purchase::find($id);
            $payment=PaymentUtils::getPayment($data->md_merchant_id,2);
        }



        $params=[
            'title'=>($id==-1)?'Tambah Data':'Tambah Data',
            'data'=>$data,
            'supplier'=>SupplierEntity::listOption(),
            'warehouse'=> WarehouseEntity::where('md_merchant_id',$data->md_merchant_id)->where('is_deleted',0)->get(),
            'transType'=>$payment,
        ];

        if($page=="penerimaan"){
            return view('merchant::toko.transaction.purchase-order.form-delivery',$params);
        }else if($page=="pemesanan"){
            return view('merchant::toko.transaction.purchase-order.form-order',$params);
        }else if($page=="penawaran"){
            return view('merchant::toko.transaction.purchase-order.form-offer',$params);
        }else if($page=="pembayaran"){
            return view('merchant::toko.transaction.purchase-order.form-invoice',$params);
        }else{
            return view('merchant::toko.transaction.purchase-order.form',$params);
        }
    }

    public function getPaymentList(Request  $request)
    {
        try {
            $merchantId=$request->md_merchant_id;
            $type=$request->type;
            $data=collect(PaymentUtils::getPayment($merchantId,$type));

            $result=[];
            foreach ($data as $key =>$item)
            {
                $childs=[];
                foreach (collect($item['data']) as $child)
                {
                    $childs[]=[
                        'id'=>$child->trans_id.'_'.$child->acc_coa_id,
                        'text'=>$child->name
                    ];
                }

                $result[]=[
                    "text"=>$item['name'],
                    'children'=>$childs
                ];
            }

            return response()->json($result);


        }catch (\Exception $e)
        {
            return  response()->json([]);

        }
    }



    public function saveOffer(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            if(is_null(MerchantUtil::checkMerchant($request->md_merchant_id)))
            {
                return response()->json([
                    'message' => "Akun Merchant tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'purchase',$request->step_type,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if($request->created_at>=Carbon::now()){
                return response()->json([
                    'message' => "Waktu transaksi tidak boleh melebihi hari ini !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $date = new \DateTime($request->created_at);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $purchaseOffer=$this->purchase::find($request->id);
            if(!is_null($purchaseOffer))
            {

                if($purchaseOffer->step_type!=1){
                    $purchaseOffer->is_step_close=1;
                    $purchaseOffer->save();
                    $purchase=new $this->purchase;
                }else{
                    $purchase=$this->purchase::find($request->id);
                    PurchaseOrderDetail::where('sc_purchase_order_id',$request->id)->delete();
                }

                $purchase->code=$purchaseOffer->code;
                $purchase->sc_supplier_id=$purchaseOffer->sc_supplier_id;

            }else{
                $purchase=new $this->purchase;
                $purchase->sc_supplier_id=$request->sc_supplier_id;
                $purchase->code=CodeGenerator::generatePurchaseOrder($request->md_merchant_id);

            }
            $purchase->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $purchase->step_type=$request->step_type;
            $purchase->qr_code='-';
            $purchase->is_debet=0;
            $purchase->is_with_retur=0;
            $purchase->change_nominal=0;
            $purchase->change_money=0;
            $purchase->sc_supplier_id=$request->sc_supplier_id;
            $purchase->total_offer=$request->total_offer;
            $purchase->md_merchant_id=$request->md_merchant_id;
            $purchase->paid_nominal=0;
            $purchase->note = $request->note;
            $purchase->md_sc_transaction_status_id=TransactionStatus::UNPAID;
            $purchase->promo_offer=$request->promo_offer;
            $purchase->promo_percentage_offer=$request->promo_percentage_offer;
            $purchase->tax_offer=$request->tax_offer;
            $purchase->tax_percentage_offer=$request->tax_percentage_offer;
            $purchase->timezone=$request->timezone;
            $purchase->created_by=(get_role()==3)?user_id():get_staff_id();
            $purchase->sync_id=Uuid::uuid4()->toString();
            $purchase->created_at=$request->created_at;
            $purchase->updated_at=$request->created_at;
            $purchase->time_to_offer=$request->created_at;
            $purchase->save();
            $sc_product_id=$request->sc_product_id;
            $price=$request->selling_price;
            $quantity=$request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $unitList=$request->unit_list;
            $jsonMultiUnit=$request->json_multi_unit;
            $details=[];

            if(empty($sc_product_id))
            {
                return response()->json([
                    'message' => "Data item pembelian tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            foreach ($sc_product_id as $key =>$item)
            {
                if($isMultiUnit[$key]==1){
                    if($unitList[$key]=='-1' || $unitList==-1)
                    {
                        return response()->json([
                            'message' => "Ada produk yang belum memilih satuan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                }

                $originalPrice=strtr($price[$key], array('.' => '', ',' => '.'));

                if(is_null($sc_product_id[$key])){
                    return response()->json([
                        'message' => "Data produk tidak boleh kosong !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                if($isMultiUnit[$key]==0)
                {
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at
                    ];
                }else{
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$nilaiKonversi[$key]*$quantity[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at
                    ];
                }


            }
            PurchaseOrderDetail::insert($details);

            DB::commit();
            if($purchase->step_type==1){
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.purchase-order.detail-offer',['id'=>$purchase->id])
                ]);
            }else{
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.purchase-order.detail-order',['id'=>$purchase->id])
                ]);
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }

    }

    //Penawaran
    public function saveOrder(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            if(is_null(MerchantUtil::checkMerchant($request->md_merchant_id)))
            {
                return response()->json([
                    'message' => "Akun Merchant tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($request->created_at>=Carbon::now()){
                return response()->json([
                    'message' => "Waktu transaksi tidak boleh melebihi hari ini !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $date = new \DateTime($request->created_at);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'purchase',2,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $purchaseOffer=$this->purchase::find($request->id);
            if(!is_null($purchaseOffer))
            {
                if($purchaseOffer->step_type!=2){
                    $purchaseOffer->is_step_close=1;
                    $purchaseOffer->save();
                    $purchase=new $this->purchase;
                }else{
                    $purchase=$this->purchase::find($request->id);
                    PurchaseOrderDetail::where('sc_purchase_order_id',$request->id)->delete();
                }
                $purchase->code=$purchaseOffer->code;
                $purchase->sc_supplier_id=$purchaseOffer->sc_supplier_id;
            }else{
                $purchase=new $this->purchase;
                $purchase->sc_supplier_id=$request->sc_supplier_id;
                $purchase->code=CodeGenerator::generatePurchaseOrder($request->md_merchant_id);

            }

            $purchase->ref_code=str_replace(' ','',$request->ref_code);
            $purchase->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $purchase->step_type=2;
            $purchase->qr_code='-';
            $purchase->is_debet=0;
            $purchase->is_with_retur=0;
            $purchase->change_nominal=0;
            $purchase->change_money=0;
            $purchase->total_order=$request->total_order;
            $purchase->md_merchant_id=$request->md_merchant_id;
            $purchase->paid_nominal=0;
            $purchase->note = $request->note;
            $purchase->md_sc_transaction_status_id=TransactionStatus::UNPAID;
            $purchase->promo_order=(is_null($request->promo_order)) ? 0 : $request->promo_order;
            $purchase->created_at=$request->created_at;
            $purchase->promo_percentage_order=(is_null($request->promo_percentage_order)) ? 0 : $request->promo_percentage_order;
            $purchase->tax_order=$request->tax_order;
            $purchase->tax_percentage_order=$request->tax_percentage_order;
            $purchase->timezone=$request->timezone;
            $purchase->created_by=(get_role()==3)?user_id():get_staff_id();
            $purchase->sync_id=Uuid::uuid4()->toString();
            $purchase->created_at=$request->created_at;
            $purchase->updated_at=$request->created_at;
            $purchase->time_to_order=$request->created_at;
            $purchase->inv_warehouse_id=$request->inv_warehouse_id;
            $purchase->save();
            $sc_product_id=$request->sc_product_id;
            $price=$request->selling_price;
            $quantity=$request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $unitList=$request->unit_list;
            $jsonMultiUnit=$request->json_multi_unit;

            $details=[];
            if(empty($sc_product_id))
            {
                return response()->json([
                    'message' => "Data item pembelian tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            foreach ($sc_product_id as $key =>$item)
            {
                if($isMultiUnit[$key]==1){
                    if($unitList[$key]=='-1' || $unitList==-1)
                    {
                        return response()->json([
                            'message' => "Ada produk yang belum memilih satuan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                }

                $originalPrice=strtr($price[$key], array('.' => '', ',' => '.'));

                if(is_null($sc_product_id[$key])){
                    return response()->json([
                        'message' => "Data produk tidak boleh kosong !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                if($isMultiUnit[$key]==0)
                {
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at
                    ];
                }else{
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$nilaiKonversi[$key]*$quantity[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at
                    ];
                }

            }
            PurchaseOrderDetail::insert($details);
            DB::commit();

            if($purchase->step_type==1){
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.purchase-order.detail-offer',['id'=>$purchase->id])
                ]);
            }else{
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.purchase-order.detail-order',['id'=>$purchase->id])
                ]);
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }

    }

    public function saveDelivery(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            if(is_null(MerchantUtil::checkMerchant($request->md_merchant_id)))
            {
                return response()->json([
                    'message' => "Akun Merchant tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($request->created_at>=Carbon::now()){
                return response()->json([
                    'message' => "Waktu transaksi tidak boleh melebihi hari ini !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $date = new \DateTime($request->created_at);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            //dd($request->coa_inv_id);
            $sc_product_id=$request->sc_product_id;
            $price=$request->selling_price;
            $quantity=$request->quantity;
            $max_quantity=$request->max_quantity;
            $productName=$request->product_name;



            if(empty($sc_product_id))
            {
                return response()->json([
                    'message' => "Data item pembelian tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'purchase',3,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }


            $purchaseOffer=$this->purchase::find($request->id);
            if(!is_null($purchaseOffer))
            {
                if($purchaseOffer->step_type!=3){
                    $purchaseOffer->is_step_close=1;
                    $purchaseOffer->save();
                    $purchase=new $this->purchase;
                }else{
                    $purchase=$this->purchase::find($request->id);
                    PurchaseOrderDetail::where('sc_purchase_order_id',$request->id)->delete();
                }
                $purchase->code=$purchaseOffer->code;
                $purchase->sc_supplier_id=$purchaseOffer->sc_supplier_id;

            }else{
                $purchase=new $this->purchase;
                $purchase->sc_supplier_id=$request->sc_supplier_id;
                $purchase->code=CodeGenerator::generatePurchaseOrder($request->md_merchant_id);
            }

            $totalDev=0;
            foreach ($sc_product_id as $key =>$item)
            {
                $originalPrice=strtr($price[$key], array('.' => '', ',' => '.'));

                if($quantity[$key]<1){
                    return response()->json([
                        'message' => "Jumlah tidak boleh kosong !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $totalDev+=$originalPrice*$quantity[$key];
            }
            $purchase->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $purchase->ref_code=$request->ref_code;
            $purchase->step_type=3;
            $purchase->qr_code='-';
            $purchase->is_debet=0;
            $purchase->is_with_retur=0;
            $purchase->change_nominal=0;
            $purchase->change_money=0;
            $purchase->total_delivery=$request->total;
            $purchase->total=$purchase->total_delivery;
            $purchase->md_merchant_id=$request->md_merchant_id;
            $purchase->paid_nominal=0;
            $purchase->note = $request->note;
            $purchase->md_sc_transaction_status_id=TransactionStatus::UNPAID;
            $purchase->created_at=$request->created_at;
            $purchase->promo_delivery=$request->promo;
            $purchase->promo_percentage_delivery=$request->promo_percentage;
            $purchase->tax_delivery=$request->tax;
            $purchase->tax_percentage_delivery=$request->tax_percentage;
            $purchase->timezone=$request->timezone;
            $purchase->created_by=(get_role()==3)?user_id():get_staff_id();
            $purchase->sync_id=Uuid::uuid4()->toString();
            $purchase->created_at=$request->created_at;
            $purchase->updated_at=$request->created_at;
            $purchase->time_to_delivery=$request->created_at;
            $purchase->inv_warehouse_id=$request->inv_warehouse_id;
            $purchase->save();
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $jsonMultiUnit=$request->json_multi_unit;
            $multiQuantity=$request->multi_quantity;

            $details=[];
            $stockIn=[];
            $warehouse=WarehouseEntity::find($request->inv_warehouse_id);
            if(empty($sc_product_id))
            {
                return response()->json([
                    'message' => "Data item pembelian tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $products=$this->product::whereIn('id',$sc_product_id)->get();
            $updateStockProduct="";

            foreach ($sc_product_id as $key =>$p)
            {
                $originalPrice=strtr($price[$key], array('.' => '', ',' => '.'));

                $product=$products->firstWhere('id',$sc_product_id[$key]);

                if($isMultiUnit[$key]==0){
                    if($max_quantity[$key] < $quantity[$key]){
                        return response()->json([
                            'message' => "Jumlah penerimaan melebihi jumlah pemesanan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }else{
                    if($multiQuantity[$key] < ($quantity[$key]*$nilaiKonversi[$key])){
                        return response()->json([
                            'message' => "Jumlah penerimaan melebihi jumlah pemesanan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                }




                if($isMultiUnit[$key]==0)
                {
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at
                    ];
                }else{
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$nilaiKonversi[$key]*$quantity[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at
                    ];
                }

                $purchasePrice=(float)$originalPrice/(float)$nilaiKonversi[$key];
                if($purchasePrice<$product->purchase_price)
                {
                    $poPrice=$purchasePrice;
                }else{
                    $poPrice=$purchasePrice;
                }

                if ($warehouse->is_default == 1) {
                    $stockP=$product->stock+($quantity[$key]*$nilaiKonversi[$key]);
                    $updateStockProduct.=($key==0)?"(".$product->id.",".$stockP.",".$poPrice.")":",(".$product->id.",".$stockP.",".$poPrice.")";
                }


                $stockIn[]=[
                    'sync_id'=>$product->id.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$product->id,
                    'total'=>$quantity[$key]*$nilaiKonversi[$key],
                    'inv_warehouse_id'=>$request->inv_warehouse_id,
                    'record_stock'=>($warehouse->is_default == 1)?$product->stock+($quantity[$key]*$nilaiKonversi[$key]):($quantity[$key]*$nilaiKonversi[$key]),
                    'created_by'=>$request->created_by,
                    'selling_price'=>$product->selling_price,
                    'purchase_price'=>$poPrice,
                    'residual_stock'=>$quantity[$key]*$nilaiKonversi[$key],
                    'type'=>StockInventory::IN,
                    'transaction_action'=> 'Penambahan Stok '.$product->name. ' Dari Pembelian Dengan Code '.(is_null($purchase->second_code)?$purchase->code:$purchase->second_code),
                    'stockable_type'=>'App\Models\SennaToko\PurchaseOrder',
                    'stockable_id'=>$purchase->id,
                    'created_at'=>$request->created_at,
                    'updated_at'=>$request->created_at,
                    'timezone'=>$request->_timezone,
                    'unit_name'=>$unitName[$key],
                    'product_name'=>$productName[$key]
                ];

            }

            PurchaseOrderDetail::insert($details);

            DB::commit();

            $calculate=[
                'updateStockProduct'=>$updateStockProduct,
                'inv_id'=>$request->inv_warehouse_id,
                'warehouse_id'=>$warehouse->id,
                'stock_in'=>$stockIn
            ];
            Session::put('purchase_delivery_'.$purchase->id,$calculate);
            Artisan::call('purchase:delivery', ['--purchase_id' =>$purchase->id]);

            if($purchase->step_type==1){
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.purchase-order.detail-offer',['id'=>$purchase->id])
                ]);
            }else{
                return response()->json([
                    'message' => "Data berhasil disimpan !",
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('merchant.toko.purchase-order.detail-delivery',['id'=>$purchase->id])
                ]);
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }

    }

    public function saveInvoice(Request $request)
    {
        try{
            //dd($request->all());

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'purchase',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }


            $date = new \DateTime($request->created_at);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($request->created_at>=Carbon::now()){
                return response()->json([
                    'message' => "Waktu transaksi tidak boleh melebihi hari ini !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            DB::beginTransaction();

            date_default_timezone_set($request->timezone);
            $purchase=$this->purchase::find($request->id);
            $newPurchase = $purchase->replicate();
            $purchase->is_step_close=1;
            $purchase->save();
            $detailNew=[];



            $sc_product_id=$request->sc_product_id;
            $price=$request->selling_price;
            $quantity=$request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $jsonMultiUnit=$request->json_multi_unit;

            if($request->created_at<$newPurchase->created_at){
                return response()->json([
                    'message' => "Tanggal tidak boleh kurang dari tanggal pengiriman $newPurchase->created_at",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $validator = Validator::make($request->all(),$purchase->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if($request->is_debet==0)
            {
                $trans = explode('_', $request->md_transaction_type_id);
                if($request->md_transaction_type_id=='-1')
                {
                    return response()->json([
                        'message' => "Metode Pembayaran belum dipilih !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }else{
                if($request->due_date<$request->created_at){
                    return response()->json([
                        'message' => "Tanggal jatuh tempo tidak boleh kurang dari tanggal transaksi !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $trans=null;
            }
            $newPurchase->second_code=(is_null($request->second_code) || $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $newPurchase->ref_code=$request->ref_code;
            $newPurchase->step_type=$request->step_type;
            $newPurchase->qr_code='-';
            $newPurchase->is_debet=$request->is_debet;
            $newPurchase->is_with_retur=0;
            $newPurchase->change_nominal=0;
            $newPurchase->change_money=0;
            $newPurchase->sc_supplier_id=$request->sc_supplier_id;
            $newPurchase->paid_nominal=0;
            $newPurchase->discount=$request->promo;
            $newPurchase->discount_percentage=$request->promo_percentage;
            $newPurchase->tax=$request->tax;
            $newPurchase->tax_percentage=$request->tax_percentage;
            $newPurchase->total=$request->total;
            $newPurchase->note = $request->note;
            if($request->is_debet==1)
            {
                $newPurchase->md_sc_transaction_status_id=TransactionStatus::UNPAID;
            }else{
                $newPurchase->md_sc_transaction_status_id=TransactionStatus::PAID;
            }
            if(!is_null($trans))
            {
                $newPurchase->md_transaction_type_id=$trans[0];
                $newPurchase->coa_trans_id=$trans[1];
            }
            $newPurchase->timezone=$request->timezone;
            $newPurchase->created_by=(get_role()==3)?user_id():get_staff_id();
            $newPurchase->sync_id=Uuid::uuid4()->toString();
            $newPurchase->save();

            foreach ($sc_product_id as $key => $item){
                $originalPrice=strtr($price[$key], array('.' => '', ',' => '.'));

                if($isMultiUnit[$key]==0){
                    $detailNew[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$newPurchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                        'created_at'=>$newPurchase->created_at,
                        'updated_at'=>$newPurchase->updated_at
                    ];
                }else{
                    $detailNew[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$newPurchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                        'created_at'=>$newPurchase->created_at,
                        'updated_at'=>$newPurchase->updated_at
                    ];
                }


            }


            PurchaseOrderDetail::insert($detailNew);

            if($newPurchase->is_debet==1)
            {
                $newPurchase->ap()->create([
                    'sync_id'=>Uuid::uuid4()->toString(),
                    'md_merchant_id'=>$request->md_merchant_id,
                    'residual_amount'=>$request->total-$request->paid_nominal,
                    'paid_nominal'=>$request->paid_nominal,
                    'is_paid_off'=>0,
                    'due_date'=>$request->due_date,
                    'timezone'=>$request->timezone
                ]);
            }
            DB::commit();

            if(CoaPurchaseUtil::coaStockAdjustmentFromPurchasev2($newPurchase->created_by,$request->md_merchant_id,'',$newPurchase)==false)
            {
                DB::rollBack();
                return response()->json([
                    'message' => "Terjadi kesalahan dalam pencatatan jurnal pembelian dengan kode pembelian ".(is_null($newPurchase->second_code)?$newPurchase->code:$newPurchase->second_code)." !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.purchase-order.detail',['id'=>$newPurchase->id])
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }

    }



    public function save(Request $request)
    {
        try{

            $date = new \DateTime($request->created_at);
            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'purchase',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($request->created_at>=Carbon::now()){
                return response()->json([
                    'message' => "Waktu transaksi tidak boleh melebihi hari ini !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            DB::beginTransaction();
            //dd($request->timezone);
            $timestamp=$request->created_at;
            date_default_timezone_set($request->timezone);
            if(is_null($request->id)){

                $purchase=new $this->purchase;
                $purchase->code=CodeGenerator::generatePurchaseOrder($request->md_merchant_id);

            }else{
                $purchaseOld=$this->purchase::find($request->id);
                $purchaseOld->is_step_close=1;
                $purchaseOld->save();
                $purchase=$purchaseOld->replicate();
                if($request->created_at<$purchaseOld->created_at){
                    return response()->json([
                        'message' => "Tanggal tidak boleh kurang dari tanggal pengiriman $purchaseOld->created_at",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }


            $validator = Validator::make($request->all(),$purchase->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if($request->is_debet==0)
            {
                $trans = explode('_', $request->md_transaction_type_id);
                if($request->md_transaction_type_id=='-1')
                {
                    return response()->json([
                        'message' => "Metode Pembayaran belum dipilih !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }else{
                if($request->due_date<$request->created_at){
                    return response()->json([
                        'message' => "Tanggal jatuh tempo tidak boleh kurang dari tanggal transaksi !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
                $trans=null;
            }
            $warehouse=WarehouseEntity::find($request->inv_warehouse_id);
            $purchase->second_code=(is_null($request->second_code)|| $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $purchase->ref_code=$request->ref_code;
            $purchase->step_type=$request->step_type;
            $purchase->qr_code='-';
            $purchase->is_debet=$request->is_debet;
            $purchase->is_with_retur=0;
            $purchase->change_nominal=0;
            $purchase->change_money=0;
            $purchase->sc_supplier_id=$request->sc_supplier_id;
            $purchase->total=$request->total;
            $purchase->md_merchant_id=$request->md_merchant_id;
            $purchase->paid_nominal=0;
            $purchase->step_type=0;
            $purchase->note = $request->note;
            if($request->is_debet==1)
            {
                $purchase->md_sc_transaction_status_id=TransactionStatus::UNPAID;
            }else{
                $purchase->md_sc_transaction_status_id=TransactionStatus::PAID;
            }
            if(!is_null($trans))
            {
                $purchase->md_transaction_type_id=$trans[0];
                $purchase->coa_trans_id=$trans[1];
            }

            $purchase->discount=$request->discount;
            $purchase->discount_percentage=$request->discount_percentage;
            $purchase->tax=$request->tax;
            $purchase->tax_percentage=$request->tax_percentage;
            $purchase->created_at=$timestamp;
            $purchase->updated_at = $timestamp;
            $purchase->inv_warehouse_id=$request->inv_warehouse_id;
            $purchase->timezone=$request->timezone;
            $purchase->created_by=user_id();
            $purchase->sync_id=Uuid::uuid4()->toString();
            $purchase->save();

            $sc_product_id=$request->sc_product_id;
            $price=$request->selling_price;
            $quantity=$request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $nilaiKonversi=$request->nilai_konversi;
            $multiUnitId=$request->multi_unit_id;
            $jsonMultiUnit=$request->json_multi_unit;
            $unitList=$request->unit_list;


            $details=[];
            $stockIn=[];
            if(empty($sc_product_id))
            {
                return response()->json([
                    'message' => "Data item pembelian tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $products=$this->product::whereIn('id',$sc_product_id)->get();
            $updateStockProduct="";

            foreach ($sc_product_id as $key =>$p)
            {
                if($isMultiUnit[$key]==1){
                    if($unitList[$key]=='-1' || $unitList==-1)
                    {
                        return response()->json([
                            'message' => "Ada produk yang belum memilih satuan !",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);

                    }
                }

                $product=$products->firstWhere('id',$sc_product_id[$key]);
                $originalPrice=(float)strtr($price[$key], array('.' => '', ',' => '.'));

                if($isMultiUnit[$key]==0){
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>0,
                        'multi_quantity'=>0,
                        'multi_unit_id'=>null,
                        'value_conversation'=>1,
                        'json_multi_unit'=>null,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at
                    ];
                }else{
                    $details[]=[
                        'sc_product_id'=>$sc_product_id[$key],
                        'price'=>$originalPrice,
                        'sub_total'=>$originalPrice*$quantity[$key],
                        'sc_purchase_order_id'=>$purchase->id,
                        'quantity'=>$quantity[$key],
                        'coa_inv_id'=>$coaInvId[$key],
                        'coa_hpp_id'=>$coHppId[$key],
                        'unit_name'=>$unitName[$key],
                        'product_name'=>$productName[$key],
                        'is_multi_unit'=>1,
                        'multi_quantity'=>$quantity[$key]*$nilaiKonversi[$key],
                        'multi_unit_id'=>$multiUnitId[$key],
                        'value_conversation'=>$nilaiKonversi[$key],
                        'json_multi_unit'=>json_encode(json_decode($jsonMultiUnit[$key])),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at

                    ];
                }

                $purchasePrice=(float)$originalPrice/(float)$nilaiKonversi[$key];
                if($purchasePrice<$product->purchase_price)
                {
                    $poPrice=$purchasePrice;
                }else{
                    $poPrice=$purchasePrice;
                }

                if ($warehouse->is_default == 1) {
                    $stockP=$product->stock+($quantity[$key]*$nilaiKonversi[$key]);
                    $updateStockProduct.=($key==0)?"(".$product->id.",".$stockP.",".$poPrice.")":",(".$product->id.",".$stockP.",".$poPrice.")";
                }

                $stockIn[]=[
                    'sync_id'=>$product->id.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$product->id,
                    'total'=>$quantity[$key]*$nilaiKonversi[$key],
                    'inv_warehouse_id'=>$request->inv_warehouse_id,
                    'record_stock'=>($warehouse->is_default == 1)?$product->stock+($quantity[$key]*$nilaiKonversi[$key]):$quantity[$key]*$nilaiKonversi[$key],
                    'created_by'=>$request->created_by,
                    'selling_price'=>$product->selling_price,
                    'purchase_price'=>$poPrice,
                    'residual_stock'=>$quantity[$key]*$nilaiKonversi[$key],
                    'type'=>StockInventory::IN,
                    'transaction_action'=> 'Penambahan Stok '.$product->name. ' Dari Pembelian Dengan Code '.(is_null($purchase->second_code)?$purchase->code:$purchase->second_code),
                    'stockable_type'=>'App\Models\SennaToko\PurchaseOrder',
                    'stockable_id'=>$purchase->id,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp,
                    'timezone'=>$request->_timezone,
                    'unit_name'=>$unitName[$key],
                    'product_name'=>$productName[$key]
                ];
            }

            $arrayOfSession=[
                'purchase'=>$purchase,
                'updateStockProduct'=>$updateStockProduct,
                'inv_id'=>$request->inv_warehouse_id,
                'warehouse_id'=>$warehouse->id,
                'user_id'=>user_id(),
                'merchant_id'=>$request->md_merchant_id,
                'coaJson'=>'',
                'is_debet'=>$request->is_debet,
                'stock_in'=>$stockIn,
                'is_with_load_shipping'=>$request->is_with_load_shipping,
                'total'=>$request->total,
                'paid_nominal'=>$request->paid_nominal,
                'shipping_cost'=>$request->shipping_cost,
                'due_date'=>$request->due_date,
                'timezone'=>$request->timezone,
                'timestamp'=>$timestamp
            ];

            Session::put('purchase_faktur_'.$purchase->id,$arrayOfSession);
            PurchaseOrderDetail::insert($details);
            DB::commit();
            Artisan::call('purchase:faktur', ['--purchase_id' =>$purchase->id]);

            return response()->json([
                'message' => "Data berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('merchant.toko.purchase-order.detail',['id'=>$purchase->id])
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }

    }

    public function deleteDetail(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            PurchaseOrderDetail::find($id)->delete();
            DB::commit();

            return response()->json([
                'message' => "Data item penawaran pembelian berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }

    public function addPayment(Request  $request)
    {
        $apId=$request->ap_id;
        $data=new MerchantApDetail();
        $ap=MerchantAp::find($apId);


        $params=[
            'title'=>"Tambah Pembayaran",
            'data'=>$data,
            'ap_id'=>$apId,
            'ap'=>$ap,
            'paymentOption'=>PaymentUtils::getPayment($ap->md_merchant_id,2),
            'purchase'=>$ap->apable
        ];

        return view('merchant::toko.transaction.purchase-order.form-payment',$params);

    }

    public function savePayment(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $date = new \DateTime($request->paid_date);


            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'ap',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            DB::beginTransaction();
            $data=new MerchantApDetail();
            $ar=MerchantAp::find($request->ap_id);
            $paid_nominal=strtr($request->paid_nominal, array('.' => '', ',' => '.'));
            $adminFee=($request->admin_fee=='' || is_null($request->admin_fee))?0:strtr($request->admin_fee, array('.' => '', ',' => '.'));

            if($paid_nominal>$ar->residual_amount)
            {
                return response()->json([
                    'message' => "Nominal pembayaran melebihi kekurangan yang harus dibayar !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if($paid_nominal>=$ar->residual_amount)
            {
                $ar->is_paid_off=1;
                $ar->apable->md_sc_transaction_status_id=TransactionStatus::PAID;
                $ar->apable->save();
            }else{
                $ar->apable->md_sc_transaction_status_id=6;
                $ar->apable->save();
            }

            $ar->residual_amount-=$paid_nominal;
            $ar->paid_nominal+=$paid_nominal;
            $ar->save();

            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/ap/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }


            $data->ap_code=CodeGenerator::generalCode('PPO',$request->md_merchant_id);
            $data->paid_nominal=$paid_nominal;
            $data->paid_date=$request->paid_date;
            $data->admin_fee=$adminFee;
            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
            $data->note=$request->note;
            $data->acc_merchant_ap_id=$ar->id;
            $data->created_by=user_id();
            $data->timezone=$request->_timezone;
            $data->note=$request->note;
            $data->trans_coa_detail_id=$request->trans_coa_detail_id;
            $data->trans_proof=$fileName;
            $data->save();

            $codePO=is_null($ar->apable->second_code)?$ar->apable->code:$ar->apable->second_code;
            $codeAp=is_null($data->second_code)?$data->ap_code:$data->second_code;

            $jurnal=new Jurnal();
            $jurnal->ref_code=$data->ap_code;
            $jurnal->trans_code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
            $jurnal->trans_name='Pembayaran Pembelian  '.$codePO.'-'.$codeAp;
            $jurnal->trans_time=$data->paid_date;
            $jurnal->trans_note='Pembayaran Pembelian  '.$codePO.'-'.$codeAp;
            $jurnal->trans_purpose='Pembayaran Pembelian  '.$codePO.'-'.$codeAp;
            $jurnal->trans_amount=$data->paid_nominal;
            $jurnal->trans_proof=$fileName;
            $jurnal->md_merchant_id=$request->md_merchant_id;
            $jurnal->md_user_id_created=user_id();
            $jurnal->external_ref_id=$data->id;
            $jurnal->timezone=$request->_timezone;
            $jurnal->flag_name='Pembayaran Pembelian  '.$codePO;
            $jurnal->save();

            $fromCoa=merchant_detail_multi_branch($request->md_merchant_id)->coa_ap_purchase_id;
            $toCoa=$data->trans_coa_detail_id;
            if($data->admin_fee==0)
            {
                $insert=[
                    [
                        'acc_coa_detail_id'=>$fromCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                ];
            }else{
                $insert=[
                    [
                        'acc_coa_detail_id'=>$fromCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal+$data->admin_fee,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],

                    [
                        'acc_coa_detail_id'=>merchant_detail_multi_branch($request->md_merchant_id)->coa_administration_bank_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->admin_fee,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                ];
            }

            JurnalDetail::insert($insert);

            DB::commit();

            return response()->json([
                'message' => "Data pembayaran berhasil disimpan !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {

            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message' => "Terjadi kesalahan di server, data gagal disimpan !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }

    public function deletePayment(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=MerchantApDetail::find($id);

            $date = new \DateTime($data->paid_date);
            $ar=MerchantAp::find($data->acc_merchant_ap_id);

            if(AccUtil::checkClosingJournal($ar->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $ar->residual_amount+=$data->paid_nominal;
            $ar->paid_nominal-=$data->paid_nominal;
            $ar->is_paid_off=0;
            $ar->save();
            $ar->apable->md_sc_transaction_status_id=6;
            if($ar->apable->paid_nominal>0)
            {
                if($ar->apable->paid_nominal==$data->paid_nominal)
                {
                    $ar->apable->paid_nominal-=$data->paid_nominal;
                }
            }
            $ar->apable->save();
            Jurnal::where([
                'external_ref_id'=>$data->id,
                'ref_code'=>$data->ap_code,
                'md_merchant_id'=>$ar->md_merchant_id
            ])->update([
                'is_deleted'=>1
            ]);
            $data->delete();
            DB::commit();

            return response()->json([
                'message' => "Data pembayaran berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }
    }




    //retur

    public function saveRetur(Request  $request)
    {
        try{

            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $date = new \DateTime($request->created_at);
            if($request->created_at>Carbon::now())
            {
                return response()->json([
                    'message'=>'Tanggal retur tidak boleh melebihi hari ini!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'po_retur',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            DB::beginTransaction();
            $data=PurchaseOrder::find($id);
            $poDelivery=PurchaseOrder::where('code',$data->code)
                ->where('step_type',3)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->first();
            if(is_null($poDelivery))
            {
                $purchaseId=$data->id;
            }else{
                $purchaseId=$poDelivery->id;
            }


            TempPurchaseOrderDetail::where('sc_purchase_order_id',$data->id)
                ->update(['is_current'=>0]);

            $retur=new ReturPurchaseOrder();
            $retur->sc_purchase_order_id=$id;
            $retur->code=CodeGenerator::returPurchaseOrder($request->md_merchant_id);
            $retur->md_sc_transaction_status_id=TransactionStatus::PAID;
            $retur->is_retur=1;
            $retur->step_type=$request->retur_from;
            $retur->created_at=$request->created_at;
            $retur->note=$request->note;
            $retur->reason_id=$request->reason_id;
            $retur->second_code=(is_null($request->second_code) || $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $retur->save();
            $data->is_with_retur=1;
            $data->save();


            $arrayOfProduct=$request->sc_product_id;
            $timestamp = $request->created_at;
            $arrayOfQuantity=$request->quantity_retur;
            $arrayQuantity=$request->quantity;
            $arrayOfPurchaseDetail=$request->purchase_order_detail_id;
            $productName=$request->product_name;
            $unitName=$request->unit_name;
            $isMultiUnit=$request->is_multi_unit;
            $multiUnitId=$request->multi_unit_id;
            $multiQuantity=$request->multi_quantity;
            $multiQuantityOrigin=$request->origin_multi_quantity;
            $valueConversation=$request->value_conversation;
            $jsonMultiUnit=$request->json_multi_unit;
            $total=0;
            $temp=[];
            $sumQuantity=0;

            $pluck=$data->stock->pluck('id');

            $check=StockInventory::whereIn('sc_stock_inventories.id',$pluck->all())
                ->where('sc_stock_inventories.type','in')
                ->whereRaw("sc_stock_inventories.transaction_action not like '%Retur%'")
                ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                ->where('sc_stock_inventories.is_deleted',0)
                ->count();

            if($check==0)
            {
                $isAddRetur=true;
            }else{
                $isAddRetur=false;
            }

            if($isAddRetur==false)
            {
                return response()->json([
                    'message'=>'Persediaan dalam pembelian ini sudah digunakan,kamu tidak bisa membuat retur',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            foreach ($arrayOfQuantity as $key =>$item)
            {
                if($isMultiUnit[$key]==0){
                    $sumQuantity+=$arrayOfQuantity[$key];
                }else{
                    $sumQuantity+=$arrayOfQuantity[$key]*$valueConversation[$key];

                }
            }
            if($sumQuantity==0){


                return response()->json([
                    'message'=>'Jumlah pengembalian produk tidak boleh kosong',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }
            $purchaseDetail=PurchaseOrderDetail::whereIn('id',$arrayOfPurchaseDetail)->get();
            $stockAbles=StockInventory::
            select([
                'sc_stock_inventories.*',
                'sc_stock_inventories.id as inv_id',
                'p.id',
                'p.name as product_name_2',
                'p.stock',
                'p.code as product_code_2',
                'm.name as unit_name_2'
            ])
                ->whereIn('sc_stock_inventories.sc_product_id',$arrayOfProduct)
                ->join('sc_products as p','p.id','sc_stock_inventories.sc_product_id')
                ->leftJoin('md_units as m','m.id','p.md_unit_id')
                ->where('sc_stock_inventories.stockable_type','App\Models\SennaToko\PurchaseOrder')
                ->where('sc_stock_inventories.stockable_id',$purchaseId)->get();


            $warehouse=$data->getWarehouse;

            foreach ($arrayOfProduct as $key =>$item)
            {

                if($isMultiUnit[$key]==0){
                    if($arrayOfQuantity[$key]>$arrayQuantity[$key])
                    {

                        return response()->json([
                            'message'=>'Jumlah pengembalian produk melebihi maksimal produk dibeli',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);


                    }
                }else{
                    if(($arrayOfQuantity[$key]*$valueConversation[$key])>$multiQuantity[$key])
                    {

                        return response()->json([
                            'message'=>'Jumlah pengembalian produk melebihi maksimal produk dibeli',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);


                    }
                }

                $stockAble=$stockAbles->firstWhere('sc_product_id',$arrayOfProduct[$key]);
                $saleOrderLine=$purchaseDetail->firstWhere('id',$arrayOfPurchaseDetail[$key]);

                if($isMultiUnit[$key]==0){
                    $total+=$arrayOfQuantity[$key]*$saleOrderLine->price;

                }else{
                    if($multiQuantity[$key]==0){
                        $total+=(($arrayOfQuantity[$key]*$valueConversation[$key])/1)*$saleOrderLine->sub_total;

                    }else{
                        $total+=(($arrayOfQuantity[$key]*$valueConversation[$key])/$multiQuantityOrigin[$key])*$saleOrderLine->sub_total;
                    }

                }

                $temp[]=[
                    'sync_id'=>$stockAble->id.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$saleOrderLine->sc_product_id,
                    'quantity'=>$saleOrderLine->quantity,
                    'sub_total'=>$saleOrderLine->sub_total,
                    'price'=>$saleOrderLine->price,
                    'sc_purchase_order_id'=>$data->id,
                    'is_current'=>1,
                    'created_at'=>$saleOrderLine->created_at,
                    'updated_at'=>$saleOrderLine->updated_at
                ];

                if($isMultiUnit[$key]==0)
                {
                    if($arrayOfQuantity[$key]!=0)
                    {
                        $returDetails[]=[
                            'sc_product_id'=>$stockAble->sc_product_id,
                            'quantity'=>$arrayOfQuantity[$key],
                            'sub_total'=>$arrayOfQuantity[$key]*$saleOrderLine->price,
                            'sc_purchase_order_detail_id'=>$saleOrderLine->id,
                            'sc_retur_purchase_order_id'=>$retur->id,
                            'type'=>'minus',
                            'created_at'=>$timestamp,
                            'updated_at'=>$timestamp,
                            'product_name'=>$productName[$key],
                            'unit_name'=>$unitName[$key],
                            'is_multi_unit'=>0,
                            'multi_quantity'=>0,
                            'value_conversation'=>1,
                            'multi_unit_id'=>null,
                            'json_multi_unit'=>null,
                        ];
                    }
                }else{
                    if(($arrayOfQuantity[$key]*$valueConversation[$key])!=0)
                    {
                        if($jsonMultiUnit[$key]=='-1'|| $jsonMultiUnit[$key]==-1){
                            $jsonMulti=null;
                        }else{
                            $jsonMulti=json_encode(json_decode($jsonMultiUnit[$key]));
                        }
                        if($multiQuantity[$key]==0){
                            $subPriceRetur=(($arrayOfQuantity[$key]*$valueConversation[$key])/1)*$saleOrderLine->sub_total;
                        }else{
                            $subPriceRetur=(($arrayOfQuantity[$key]*$valueConversation[$key])/$multiQuantityOrigin[$key])*$saleOrderLine->sub_total;

                        }
                        $returDetails[]=[
                            'sc_product_id'=>$stockAble->sc_product_id,
                            'quantity'=>$arrayOfQuantity[$key],
                            'sub_total'=>$subPriceRetur,
                            'sc_purchase_order_detail_id'=>$saleOrderLine->id,
                            'sc_retur_purchase_order_id'=>$retur->id,
                            'type'=>'minus',
                            'created_at'=>$timestamp,
                            'updated_at'=>$timestamp,
                            'product_name'=>$productName[$key],
                            'unit_name'=>$unitName[$key],
                            'is_multi_unit'=>1,
                            'multi_quantity'=>$arrayOfQuantity[$key]*$valueConversation[$key],
                            'value_conversation'=>$valueConversation[$key],
                            'multi_unit_id'=>$multiUnitId[$key],
                            'json_multi_unit'=>$jsonMulti,
                        ];
                    }
                }

            }

            $data->save();
            $retur->total=$total;
            $retur->created_by=user_id();
            $retur->timezone=$request->_timezone;
            $retur->sync_id=$id.Uuid::uuid4()->toString();
            $retur->save();

            if($retur->reason_id==3)
            {
                if($data->ap->is_paid_off==0){
                    $data->ap->residual_amount-=$total;
                    $data->ap->save();
                }
            }

            ReturPurchaseOrderDetail::insert($returDetails);
            TempPurchaseOrderDetail::insert($temp);
            DB::commit();

            $arrayOfSession=[
                'retur'=>$retur,
                'stockAbles'=>$stockAbles,
                'arrayOfProduct'=>$arrayOfProduct,
                'arrayOfQuantity'=>$arrayOfQuantity,
                'isMultiUnit'=>$isMultiUnit,
                'multiQuantity'=>$multiQuantity,
                'valueConversation'=>$valueConversation,
                'arrayOfPurchaseDetail'=>$arrayOfPurchaseDetail,
                'purchase_id'=>$id,
                'retur_id'=>$retur->id,
                'retur_code'=>is_null($retur->second_code)?$retur->code:$retur->second_code,
                'retur_reason_id'=>$retur->reason_id,
                'retur_from'=>$request->retur_from,
                'warehouse_id'=>$warehouse->id,
                'warehouse_is_default'=>$warehouse->is_default,
                'timestamp'=>$timestamp,
                'user_id'=>user_id(),
                'merchant_id'=>$request->md_merchant_id,
            ];



            Session::put('purchase_retur_'.$retur->id,$arrayOfSession);
            Artisan::call('purchase:retur', ['--purchase_id' =>$retur->id]);

            return response()->json([
                'message'=>'Data retur pembelian disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.retur-purchase-order.data'),
                'is_with_datatable'=>false
            ]);


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }
    }

    public function saveReturDeliv(Request  $request)
    {
        try{

            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $date = new \DateTime($request->created_at);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->second_code),$request->md_merchant_id,'po_retur',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            DB::beginTransaction();
            $data=PurchaseOrder::find($id);

            TempPurchaseOrderDetail::where('sc_purchase_order_id',$data->id)
                ->update(['is_current'=>0]);

            $retur=new ReturPurchaseOrder();
            $retur->sc_purchase_order_id=$id;
            $retur->code=CodeGenerator::returPurchaseOrder($request->md_merchant_id);
            $retur->md_sc_transaction_status_id=TransactionStatus::PAID;
            $retur->is_retur=1;
            $retur->step_type=$request->retur_from;
            $retur->created_at=$request->created_at;
            $retur->note=$request->note;
            $retur->reason_id=$request->reason_id;
            $retur->second_code=(is_null($request->second_code) || $request->second_code=='')?null:str_replace(' ','',$request->second_code);
            $retur->save();
            $data->is_with_retur=1;
            $data->save();


            $arrayOfProduct=$request->sc_product_id;
            $timestamp = $request->created_at;
            $arrayOfQuantity=$request->quantity_retur;
            $arrayQuantity=$request->quantity;
            $arrayOfPurchaseDetail=$request->purchase_order_detail_id;
            $unitName=$request->unit_name;
            $productName=$request->product_name;
            $isMultiUnit=$request->is_multi_unit;
            $multiUnitId=$request->multi_unit_id;
            $multiQuantity=$request->multi_quantity;
            $multiQuantityOrigin=$request->origin_multi_quantity;
            $valueConversation=$request->value_conversation;
            $jsonMultiUnit=$request->json_multi_unit;
            $total=0;
            $returDetails=[];
            $sumQuantity=0;


            $pluck=$data->stock->pluck('id');

            $check=StockInventory::whereIn('sc_stock_inventories.id',$pluck->all())
                ->where('sc_stock_inventories.type','in')
                ->whereRaw("sc_stock_inventories.transaction_action not like '%Retur%'")
                ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                ->where('sc_stock_inventories.is_deleted',0)
                ->count();

            if($check==0)
            {
                $isAddRetur=true;
            }else{
                $isAddRetur=false;
            }

            if($isAddRetur==false)
            {
                return response()->json([
                    'message'=>'Persediaan dalam penerimaan pembelian ini sudah digunakan,kamu tidak bisa membuat retur',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            foreach ($arrayOfQuantity as $key =>$item)
            {
                if($isMultiUnit[$key]==0){
                    $sumQuantity+=$arrayOfQuantity[$key];
                }else{
                    $sumQuantity+=$arrayOfQuantity[$key]*$valueConversation[$key];

                }
            }

            if($sumQuantity==0){

                return response()->json([
                    'message'=>'Jumlah pengembalian produk tidak boleh kosong',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $purchaseDetail=PurchaseOrderDetail::whereIn('id',$arrayOfPurchaseDetail)->get();
            $stockAbles=StockInventory::
            select([
                'sc_stock_inventories.*',
                'sc_stock_inventories.id as inv_id',
                'p.id',
                'p.name as product_name_2',
                'p.stock',
                'p.code as product_code_2',
                'm.name as unit_name_2'
            ])
                ->whereIn('sc_stock_inventories.sc_product_id',$arrayOfProduct)
                ->join('sc_products as p','p.id','sc_stock_inventories.sc_product_id')
                ->leftJoin('md_units as m','m.id','p.md_unit_id')
                ->where('sc_stock_inventories.stockable_type','App\Models\SennaToko\PurchaseOrder')
                ->where('sc_stock_inventories.stockable_id',$data->id)->get();

            $warehouse=$data->getWarehouse;

            foreach ($arrayOfProduct as $key =>$item)
            {
                if($isMultiUnit[$key]==0){
                    if($arrayOfQuantity[$key]>$arrayQuantity[$key])
                    {

                        return response()->json([
                            'message'=>'Jumlah pengembalian produk melebihi maksimal produk dibeli',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);


                    }
                }else{
                    if(($arrayOfQuantity[$key]*$valueConversation[$key])>$multiQuantity[$key])
                    {

                        return response()->json([
                            'message'=>'Jumlah pengembalian produk melebihi maksimal produk dibeli',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);


                    }
                }
                $saleOrderLine=$purchaseDetail->firstWhere('id',$arrayOfPurchaseDetail[$key]);
                $stockAble=$stockAbles->firstWhere('sc_product_id',$arrayOfProduct[$key]);

                if($isMultiUnit[$key]==0){
                    $total+=$arrayOfQuantity[$key]*$saleOrderLine->price;

                }else{
                    if($multiQuantity[$key]==0){
                        $total+=(($arrayOfQuantity[$key]*$valueConversation[$key])/1)*$saleOrderLine->sub_total;

                    }else{
                        $total+=(($arrayOfQuantity[$key]*$valueConversation[$key])/$multiQuantityOrigin[$key])*$saleOrderLine->sub_total;
                    }

                }

                if($isMultiUnit[$key]==0)
                {
                    if($arrayOfQuantity[$key]!=0)
                    {
                        $returDetails[]=[
                            'sc_product_id'=>$stockAble->sc_product_id,
                            'quantity'=>$arrayOfQuantity[$key],
                            'sub_total'=>$arrayOfQuantity[$key]*$saleOrderLine->price,
                            'sc_purchase_order_detail_id'=>$saleOrderLine->id,
                            'sc_retur_purchase_order_id'=>$retur->id,
                            'type'=>'minus',
                            'created_at'=>$timestamp,
                            'updated_at'=>$timestamp,
                            'product_name'=>$productName[$key],
                            'unit_name'=>$unitName[$key],
                            'is_multi_unit'=>0,
                            'multi_quantity'=>0,
                            'value_conversation'=>1,
                            'multi_unit_id'=>null,
                            'json_multi_unit'=>null,
                        ];
                    }
                }else{
                    if(($arrayOfQuantity[$key]*$valueConversation[$key])!=0)
                    {
                        if($jsonMultiUnit[$key]==-1 || $jsonMultiUnit[$key]=='-1'){
                            $jsonMulti=null;
                        }else{
                            $jsonMulti=json_encode(json_decode($jsonMultiUnit[$key]));
                        }
                        if($multiQuantity[$key]==0){
                            $subPriceRetur=(($arrayOfQuantity[$key]*$valueConversation[$key])/1)*$saleOrderLine->sub_total;
                        }else{
                            $subPriceRetur=(($arrayOfQuantity[$key]*$valueConversation[$key])/$multiQuantityOrigin[$key])*$saleOrderLine->sub_total;

                        }
                        $returDetails[]=[
                            'sc_product_id'=>$stockAble->sc_product_id,
                            'quantity'=>$arrayOfQuantity[$key],
                            'sub_total'=>$subPriceRetur,
                            'sc_purchase_order_detail_id'=>$saleOrderLine->id,
                            'sc_retur_purchase_order_id'=>$retur->id,
                            'type'=>'minus',
                            'created_at'=>$timestamp,
                            'updated_at'=>$timestamp,
                            'product_name'=>$productName[$key],
                            'unit_name'=>$unitName[$key],
                            'is_multi_unit'=>1,
                            'multi_quantity'=>$arrayOfQuantity[$key]*$valueConversation[$key],
                            'value_conversation'=>$valueConversation[$key],
                            'multi_unit_id'=>$multiUnitId[$key],
                            'json_multi_unit'=>$jsonMulti,
                        ];
                    }
                }

            }


            $data->save();
            $retur->total=$total;
            $retur->created_by=user_id();
            $retur->timezone=$request->_timezone;
            $retur->sync_id=$id.Uuid::uuid4()->toString();
            $retur->save();

            if($retur->reason_id==3)
            {
                $data->ap->residual_amount-=$total;
                $data->ap->save();
            }
            ReturPurchaseOrderDetail::insert($returDetails);

            DB::commit();
            $arrayOfSession=[
                'retur'=>$retur,
                'stockAbles'=>$stockAbles,
                'purchaseDetail'=>$purchaseDetail,
                'arrayOfProduct'=>$arrayOfProduct,
                'arrayOfQuantity'=>$arrayOfQuantity,
                'arrayOfPurchaseDetail'=>$arrayOfPurchaseDetail,
                'isMultiUnit'=>$isMultiUnit,
                'multiQuantity'=>$multiQuantity,
                'valueConversation'=>$valueConversation,
                'purchase_id'=>$id,
                'retur_id'=>$retur->id,
                'retur_code'=>is_null($retur->second_code)?$retur->code:$retur->second_code,
                'retur_reason_id'=>$retur->reason_id,
                'retur_from'=>$request->retur_from,
                'warehouse_id'=>$warehouse->id,
                'warehouse_is_default'=>$warehouse->is_default,
                'timestamp'=>$timestamp,
                'user_id'=>user_id(),
                'merchant_id'=>$request->md_merchant_id,
            ];


            Session::put('purchase_retur_deliv_'.$retur->id,$arrayOfSession);
            Artisan::call('purchase:retur_deliv', ['--purchase_id' =>$retur->id]);

            return response()->json([
                'message'=>'Data retur pembelian disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.retur-purchase-order.data'),
                'is_with_datatable'=>false
            ]);

        }catch (\Exception $e)
        {

            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }
    }

    public function deleteRetur(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=ReturPurchaseOrder::find($id);
            $date = new \DateTime($data->created_at);
            $purchase=PurchaseOrder::find($data->sc_purchase_order_id);

            if(AccUtil::checkClosingJournal($purchase->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $data->is_deleted=1;
            $data->save();
            $purchase->is_with_retur=0;
            $purchase->save();
            $totalBack=0;
            $poDelivery=PurchaseOrder::where('code',$purchase->code)
                ->where('step_type',3)
                ->where('md_merchant_id',$purchase->md_merchant_id)
                ->first();

            if(is_null($poDelivery))
            {
                $purchaseId=$purchase->id;
            }else{
                $purchaseId=$poDelivery->id;
            }

            if($data->reason_id!=1)
            {
                foreach ($data->getDetail as $d)
                {
                    $stockAble=StockInventory::where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                        ->where('stockable_id',$purchaseId)
                        ->first();
                    if($d->is_multi_unit==1){
                        $stockAble->residual_stock+=$d->multi_quantity;
                        $stockAble->save();
                    }else{
                        $stockAble->residual_stock+=$d->quantity;
                        $stockAble->save();
                    }

                    $totalBack+=$d->sub_total;

                    if($purchase->getWarehouse->is_default==1)
                    {
                        if($d->is_multi_unit==1){
                            $stockAble->getProduct->stock +=$d->multi_quantity;
                            $stockAble->getProduct->save();
                        }else{
                            $stockAble->getProduct->stock +=$d->quantity;
                            $stockAble->getProduct->save();
                        }

                    }
                    StockInventory::where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\ReturPurchaseOrder')
                        ->where('stockable_id',$data->id)
                        ->first()->delete();
                }

                if(!is_null($purchase->ap))
                {
                    $purchase->ap->residual_amount+=$totalBack;
                    $purchase->ap->save();

                    if($purchase->ap->residual_amount==0)
                    {
                        $purchase->ap->is_paid_off=0;
                        $purchase->ap->save();
                        $purchase->md_sc_transaction_status_id=6;
                        $purchase->save();
                    }
                }
                if($purchase->step_type==0){
                    Jurnal::where([
                        'external_ref_id'=>$data->id,
                        'ref_code'=>$data->code
                    ])->update([
                        'is_deleted'=>1
                    ]);
                }

            }
            DB::commit();

            return response()->json([
                'message'=>'Data retur pembelian berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }
    }

    public function deleteReturDeliv(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=ReturPurchaseOrder::find($id);
            $date = new \DateTime($data->created_at);
            $purchase=PurchaseOrder::find($data->sc_purchase_order_id);

            if(AccUtil::checkClosingJournal($purchase->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $data->is_deleted=1;
            $data->save();
            if($purchase->is_step_close==1)
            {
                return response()->json([
                    'message'=>'Transaksi tidak bisa dihapus sebelum menghapus faktur pembelian dengan kode transaksi '.$purchase->code.'',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $purchase->is_with_retur=0;
            $purchase->save();

            if($data->reason_id!=1)
            {
                foreach ($data->getDetail as $d)
                {

                    $stockAble=$purchase->stock->where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                        ->where('stockable_id',$purchase->id)
                        ->first();
                    if($d->is_multi_unit==1){
                        $stockAble->residual_stock+=$d->multi_quantity;
                        $stockAble->save();
                    }else{
                        $stockAble->residual_stock+=$d->quantity;
                        $stockAble->save();
                    }

                    if($purchase->getWarehouse->is_default==1)
                    {
                        if($d->is_multi_unit==1){
                            $stockAble->getProduct->stock +=$d->multi_quantity;
                            $stockAble->getProduct->save();
                        }else{
                            $stockAble->getProduct->stock +=$d->quantity;
                            $stockAble->getProduct->save();
                        }

                    }
                    $data->stock->where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\ReturPurchaseOrder')
                        ->where('stockable_id',$data->id)
                        ->first()->delete();
                }

            }
            ReturPurchaseOrderDetail::where('sc_retur_purchase_order_id',$data->id)->delete();

            DB::commit();

            return response()->json([
                'message'=>'Data retur pembelian berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }
    }


    public function detailRetur(Request  $request)
    {
        //dd($request->all());
        $data=ReturPurchaseOrder::find($request->id);
        $params=[
            'title'=>'Detail',
            'data'=>$data
        ];

        return view('merchant::toko.transaction.purchase-order.detail-retur',$params);
    }

    public function voidTransaction(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=PurchaseOrder::find($id);
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $warehouse=parent::getGlobalWarehouse($data->md_merchant_id);

            if(is_null($data))
            {
                return response()->json([
                    'message' => "Data pembelian tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($data->getRetur->where('is_deleted',0)->count()>0)
            {
                return response()->json([
                    'message' => "Silahkan menghapus data retur pembelian pada transaksi ini terlebih dahulu !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $isUse=0;
            $productId=$data->getDetail->pluck('sc_product_id');
            $stockAble=StockInventory::whereIn('sc_product_id',$productId->all())
                ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                ->where('type','in')
                ->whereRaw("transaction_action not like '%Retur%'")
                ->where('stockable_id',$data->id)
                ->get();

            if($stockAble->count()>0){
                foreach ($stockAble as $map)
                {
                    $totalRetur=0;
                    foreach ($data->getRetur->where('reason_id','!=',1)->where('is_deleted',0) as $r)
                    {
                        foreach ($r->getDetail->where('sc_product_id',$map->sc_product_id) as $d)
                        {
                            if($d->is_multi_unit==1){
                                $totalRetur+=$d->multi_quantity;

                            }else{
                                $totalRetur+=$d->quantity;
                            }

                        }
                    }
                    if(($map->residual_stock+$totalRetur)<$map->total)
                    {
                        $isUse++;
                    }
                }
            }

            if($isUse>0)
            {
                return response()->json([
                    'message' => "Mohon maaf data persediaan barang sudah digunakan,anda tidak dapat melakukan penghapusan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }else{
                foreach ($data->getDetail as $d)
                {
                    $stockAble=$data->stock->where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                        ->where('stockable_id',$data->id)
                        ->first();
                    if(!is_null($stockAble)){
                        if($stockAble->inv_warehouse_id==$warehouse->id)
                        {
                            $product=Product::find($d->sc_product_id);
                            $product->stock-=$stockAble->residual_stock;
                            $product->save();
                        }
                        $stockAble->is_deleted=1;
                        $stockAble->save();
                    }
                }
                $data->is_deleted=1;
                $data->save();

                Jurnal::where(['ref_code'=>$data->code,
                    'external_ref_id'=>$data->id
                ])
                    ->update(['is_deleted'=>1]);

                StockInventory::where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                    ->where('stockable_id',$data->id)
                    ->delete();

                if($data->getRetur->count()>0)
                {
                    foreach ($data->getRetur as $retur)
                    {
                        $retur->is_deleted=1;
                        $retur->save();

                        StockInventory::where('stockable_type','App\Models\SennaToko\ReturPurchaseOrder')
                            ->where('stockable_id',$retur->id)
                            ->delete();

                        Jurnal::where(['ref_code'=>$retur->code,
                            'external_ref_id'=>$retur->id
                        ])
                            ->update(['is_deleted'=>1]);
                    }
                }

                if($data->is_debet==1)
                {
                    $data->ap->is_deleted=1;
                    $data->ap->save();
                    foreach ($data->ap->getDetail as $ap)
                    {
                        $ap->is_deleted=1;
                        $ap->save();
                        Jurnal::where(['ref_code'=>$ap->ap_code,
                            'external_ref_id'=>$ap->id
                        ])
                            ->update(['is_deleted'=>1]);
                    }
                }
            }

            PurchaseOrder::where('step_type',3)->where('code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->update([
                    'is_step_close'=>0
                ]);

            PurchaseOrder::where('step_type',2)->where('code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->update([
                    'is_step_close'=>0
                ]);

            DB::commit();

            return response()->json([
                'message' => "Data transaksi pembelian berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }

    }

    public function deletePurchase(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=PurchaseOrder::find($id);
            $date = new \DateTime($data->created_at);

            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if($data->step_type==2)
            {
                PurchaseOrder::where('code',$data->code)
                    ->where('md_merchant_id',$data->md_merchant_id)
                    ->where('step_type',1)
                    ->update([
                        'is_step_close'=>0
                    ]);
            }

            if(is_null($data))
            {
                return response()->json([
                    'message' => "Data pembelian tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            $data->is_deleted=1;
            $data->save();

            DB::commit();

            return response()->json([
                'message' => "Data transaksi pembelian berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }

    }

    public function deleteDelivery(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=PurchaseOrder::find($id);


            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'danger',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $warehouse=parent::getGlobalWarehouse($data->md_merchant_id);
            if(is_null($data))
            {
                return response()->json([
                    'message' => "Data pembelian tidak ditemukan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }

            if($data->getRetur->where('is_deleted',0)->count()>0)
            {
                return response()->json([
                    'message' => "Silahkan menghapus data retur pembelian pada transaksi ini terlebih dahulu !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $isUse=0;
            $productId=$data->getDetail->pluck('sc_product_id');
            $stockAble=StockInventory::whereIn('sc_product_id',$productId->all())
                ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                ->where('type','in')
                ->whereRaw("transaction_action not like '%Retur%'")
                ->where('stockable_id',$data->id)
                ->get();
            foreach ($stockAble as $map)
            {
                $totalRetur=0;
                foreach ($data->getRetur->where('reason_id','!=',1)->where('is_deleted',0) as $r)
                {
                    foreach ($r->getDetail->where('sc_product_id',$map->sc_product_id) as $d)
                    {
                        if($d->is_multi_unit==1)
                        {
                            $totalRetur+=$d->multi_quantity;

                        }else{
                            $totalRetur+=$d->quantity;

                        }
                    }
                }
                if(($map->residual_stock+$totalRetur)<$map->total)
                {
                    $isUse++;
                }
            }

            if($isUse>0)
            {
                return response()->json([
                    'message' => "Mohon maaf data persediaan barang sudah digunakan,anda tidak dapat melakukan penghapusan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }else{
                foreach ($data->getDetail as $d)
                {
                    $stockAble=$data->stock->where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                        ->where('stockable_id',$data->id)
                        ->first();
                    if($stockAble->inv_warehouse_id==$warehouse->id)
                    {
                        $product=Product::find($d->sc_product_id);
                        $product->stock-=$stockAble->residual_stock;
                        $product->save();
                    }
                    $stockAble->is_deleted=1;
                    $stockAble->save();
                }
                $data->is_deleted=1;
                $data->save();

                StockInventory::where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                    ->where('stockable_id',$data->id)
                    ->delete();

                foreach ($data->getRetur as $retur)
                {
                    $retur->is_deleted=1;
                    $retur->save();

                    StockInventory::where('stockable_type','App\Models\SennaToko\ReturPurchaseOrder')
                        ->where('stockable_id',$retur->id)
                        ->delete();

                }
                if($data->is_debet==1)
                {
                    $data->ap->is_deleted=1;
                    $data->ap->save();
                    foreach ($data->ap->getDetail as $ap)
                    {
                        $ap->is_deleted=1;
                        $ap->save();
                    }
                }
            }

            PurchaseOrder::where('code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->where('step_type',2)
                ->update([
                    'is_step_close'=>0
                ]);

            DB::commit();

            return response()->json([
                'message' => "Data transaksi penerimaan pembelian berhasil dihapus !",
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message' => "Terjadi kesalahan di server,data gagal dihapus !",
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);

        }

    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Grab;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Models\MasterData\User;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\TypeOfBusiness;
use App\Models\Grab\GrabActivation;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;
use App\Models\MasterData\Region\District;
use App\Models\MasterData\SennaCashier\Category;
use App\Models\MasterData\Bank;

use Image;

class GrabActivationController extends Controller
{
    protected $maxFileSize = 1 * 1000 * 1000;

    public function routeWeb()
    {
        Route::prefix('grab/activation')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::get('/','Grab\GrabActivationController@index')
                ->name('grab.activation');
            Route::post('/save','Grab\GrabActivationController@save')
                ->name('grab.activation.save');

            Route::get('/create','Grab\GrabActivationController@create')
                ->name('grab.activation.create');
            Route::post('/stepOne','Grab\GrabActivationController@stepOne')
                ->name('grab.activation.step-one');
            Route::post('/stepTwo','Grab\GrabActivationController@stepTwo')
                ->name('grab.activation.step-two');
            Route::post('/stepThree','Grab\GrabActivationController@stepThree')
                ->name('grab.activation.step-three');
            Route::post('/stepFour','Grab\GrabActivationController@stepFour')
                ->name('grab.activation.step-four');
            Route::post('/stepFive','Grab\GrabActivationController@stepFive')
                ->name('grab.activation.step-five');
            Route::post('/stepSix','Grab\GrabActivationController@stepSix')
                ->name('grab.activation.step-six');
            Route::post('/stepSeven','Grab\GrabActivationController@stepSeven')
                ->name('grab.activation.step-seven');

            Route::post('/delete','Grab\GrabActivationController@delete')
                ->name('grab.activation.delete');

            Route::get('/get-city','Grab\GrabActivationController@getCity')
                ->name('grab.activation.get-city');
            Route::get('/get-district','Grab\GrabActivationController@getDistrict')
                ->name('grab.activation.get-district');
        });
    }

    public function index(Request $request)
    {
        $data = GrabActivation::where('md_merchant_id', merchant_id())->first();
        $foodType = $this->getFoodType();
        $timePickup = $this->getTimePickup();
        $merchant = Merchant::with('getDistrict.getCity.getProvince')
                    ->with('getTypeOfBusiness')
                    ->find(merchant_id());

        if(is_null($data)){
            $step = null;
        } else {
            $step = $request->step;

            if($data->registration_status == 1 && $step != 'success'){
                return redirect()->route('merchant.toko.profile.detail',['id'=>merchant_id(),'page'=>'orderOnline']);
            }
        }

        $params = [
            "title" => "Pengajuan Akun Digital",
            "data" => $data,
            "user" => User::find(user_id()),
            "merchant" => $merchant,
            "step" => $step
        ];


        return view('merchant::grab.index', $params);
    }

    public function save(Request $request)
    {
        try {
            $grabMerchant = GrabActivation::where('md_merchant_id', merchant_id())->first();

            if(!is_null($grabMerchant)){
                return "<div class='alert alert-warning' style='text-align: center'>Outlet anda sudah melakukan pengajuan !</div><script>reload(2000)</script>";
            }

            if(!$request->hasFile('screenshot_file'))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Screenshot aplikasi Grab Merchant belum dipilih !</div>";
            }

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if($request->hasFile('screenshot_file')){
                $file=$request->file('screenshot_file');
                if($file->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Screenshot aplikasi Grab Merchant lebih dari 1MB !</div>";
                }
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('screenshot_file'))->encode($file->getClientOriginalExtension());
                $fileIdentity=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileIdentity, (string) $image);

                $screenshot_file = $fileIdentity;
            }

            $data = new GrabActivation();

            $data->md_merchant_id = merchant_id();
            $data->registrant_name = $request->registrant_name;
            $data->registrant_email = $request->registrant_email;
            $data->registrant_phone_number = $request->registrant_phone_number;
            $data->business_type_name = $request->business_type_name;
            $data->owner_name = $request->registrant_name;
            $data->owner_phone_number = $request->registrant_phone_number;
            $data->owner_email = $request->registrant_email;
            $data->business_email = $request->registrant_email;
            $data->business_phone_number = $request->registrant_phone_number;
            $data->grab_food_name = $request->grab_food_name;
            $data->countries_name = $request->countries_name;
            $data->province_name = $request->province_name;
            $data->city_name = $request->city_name;
            $data->district_name = $request->district_name;
            $data->is_have_account = 1;
            $data->screenshot_file = $screenshot_file;
            $data->registration_status = 1;
            $data->save();

            return "<script>window.location.href='".route('grab.activation', ['step'=>'success'])."'</script>";

        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function create(Request $request)
    {
        $data = GrabActivation::where('md_merchant_id', merchant_id())->first();
        $foodType = $this->getFoodType();
        $timePickup = $this->getTimePickup();

        if(is_null($data)){
            $step = null;
        } else {
            $step = $request->step;

            if($data->registration_status == 1 && $step != 8){
                return redirect()->route('merchant.toko.profile.detail',['id'=>merchant_id(),'page'=>'orderOnline']);
            }
        }

        $params = [
            "title" => "Pengajuan Akun Digital",
            "data" => $data,
            "user" => User::find(user_id()),
            "merchant" => Merchant::find(merchant_id()),
            "step" => $step,
            "type" => TypeOfBusiness::All(),
            "province" => Province::All(),
            "category" => Category::All(),
            "bank"=> Bank::All(),
            "foodType" => $foodType,
            "timePickup" => $timePickup
        ];


        return view('merchant::grab.form', $params);
    }

    public function stepOne(Request $request)
    {
        try {

            $id = $request->id;

            if(is_null($id)){
                $data = new GrabActivation();
            } else {
                $data = GrabActivation::find($id);
            }

            $email=GrabActivation::whereRaw("lower(registrant_email)='".strtolower($request->registrant_email)."'")
                                    ->where('md_merchant_id','!=', merchant_id())
                                    ->first();
            if(!is_null($email)){
                return "<div class='alert alert-warning' style='text-align: center'>Email sudah digunakan !</div><script>reload(2000)</script>";
            }

            $data->md_merchant_id = merchant_id();
            $data->registrant_name = $request->registrant_name;
            $data->registrant_email = $request->registrant_email;
            $data->registrant_phone_number = $request->registrant_phone_number;
            $data->is_same_bank_name = 1;

            $data->save();

            return "<script>window.location.href='".route('grab.activation.create', ['step'=>2])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function stepTwo(Request $request)
    {
        try {
            $id = $request->id;
            $data = GrabActivation::find($id);

            $imageIdentity = json_decode($data->owner_file);

            if(!$request->hasFile('owner_identity_file') && !isset($imageIdentity->identity_card))
            {
                return "<div class='alert alert-warning' style='text-align: center'>KTP/Paspor belum dipilih !</div>";
            }

            if(!$request->hasFile('owner_identity_face') && !isset($imageIdentity->identity_card_with_face))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto verifikasi wajah dengan KTP belum dipilih!</div>";
            }

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if($request->hasFile('owner_identity_file')){
                $file1=$request->file('owner_identity_file');
                if($file1->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Foto KTP tidak boleh lebih dari 1MB !</div>";
                }
            }

            if($request->hasFile('owner_identity_face')){
                $file2=$request->file('owner_identity_face');
                if($file2->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Foto verifikasi wajah dengan KTP tidak boleh lebih dari 1MB !</div>";
                }
            }

            if($request->hasFile('owner_identity_file')){
                $file1=$request->file('owner_identity_file');
                $fileNameIdentity= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file1->getClientOriginalName());
                $image1 = Image::make(request()->file('owner_identity_file'))->encode($file1->getClientOriginalExtension());
                $fileIdentity=$destinationPath.$fileNameIdentity;
                Storage::disk('s3')->put($fileIdentity, (string) $image1);

                $owner_file = ["identity_card" => $fileIdentity];
            } else {
                $owner_file = ["identity_card" => $imageIdentity->identity_card];
            }

            if($request->hasFile('owner_identity_face')){
                $file2=$request->file('owner_identity_face');
                $fileNameIdentityFace= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file2->getClientOriginalName());
                $image2 = Image::make(request()->file('owner_identity_face'))->encode($file1->getClientOriginalExtension());
                $fileIdentityFace=$destinationPath.$fileNameIdentityFace;
                Storage::disk('s3')->put($fileIdentityFace, (string) $image2);

                $owner_file["identity_card_with_face"] = $fileIdentityFace;
            } else {
                $owner_file["identity_card_with_face"] = $imageIdentity->identity_card_with_face;
            }

            $data->business_type_name = $request->business_type_name;
            $data->owner_name = $request->owner_name;
            $data->owner_address =  $request->owner_address;
            $data->owner_citizenship = $request->owner_citizenship;
            $data->owner_identity_card_number = $request->owner_identity_card_number;
            $data->owner_phone_number =  $request->owner_phone_number;
            $data->owner_email =  $request->owner_email;
            $data->owner_birthdate = $request->owner_birthdate;
            $data->job_level =  strtolower($request->job_level);
            $data->owner_file = json_encode($owner_file);

            $data->save();

            return "<script>window.location.href='".route('grab.activation.create', ['step'=>3])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function stepThree(Request $request)
    {
        try {
            ini_set('max_execution_time', 0);
            $id = $request->id;
            $data = GrabActivation::find($id);

            $outlet_foto = json_decode($data->outlet_foto);

            if(!$request->hasFile('front_view_outlet') && !isset($outlet_foto->front_view_outlet))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto tampak depan outlet belum dipilih !</div>";
            }

            if(!$request->hasFile('inside_view_outlet') && !isset($outlet_foto->inside_view_outlet))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto tampak dalam outlet belum dipilih !</div>";
            }

            if(!$request->hasFile('side_view_outlet') && !isset($outlet_foto->side_view_outlet))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto tampak samping outlet belum dipilih !</div>";
            }

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if($request->hasFile('front_view_outlet')){
                $file1 = $request->file('front_view_outlet');
                if($file1->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Foto tampak depan outlet lebih dari 1MB !</div>";
                }
            }

            if($request->hasFile('inside_view_outlet')){
                $file2 = $request->file('inside_view_outlet');
                if($file2->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Foto tampak dalam outlet lebih dari 1MB !</div>";
                }
            }

            if($request->hasFile('side_view_outlet')){
                $file3 = $request->file('side_view_outlet');
                if($file3->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Foto tampak samping outlet lebih dari 1MB !</div>";
                }
            }

            if($request->hasFile('other_view_outlet')){
                $file4 = $request->file('other_view_outlet');
                if($file4->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Foto outlet lainnya lebih dari 1MB !</div>";
                }
            }


            if($request->hasFile('front_view_outlet')){
                $file1 = $request->file('front_view_outlet');
                $frontView = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file1->getClientOriginalName());
                $image1 = Image::make(request()->file('front_view_outlet'))->encode($file1->getClientOriginalExtension());
                $frontViewOutlet = $destinationPath.$frontView;
                Storage::disk('s3')->put($frontViewOutlet, (string) $image1);

                $outlet_file = ["front_view_outlet" => $frontViewOutlet];
            } else {
                $outlet_file = ["front_view_outlet" => $outlet_foto->front_view_outlet];
            }

            if($request->hasFile('inside_view_outlet')){
                $file2 = $request->file('inside_view_outlet');
                $insideView = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file2->getClientOriginalName());
                $image2 = Image::make(request()->file('inside_view_outlet'))->encode($file2->getClientOriginalExtension());
                $insideViewOutlet = $destinationPath.$insideView;
                Storage::disk('s3')->put($insideViewOutlet, (string) $image2);

                $outlet_file["inside_view_outlet"] = $insideViewOutlet;
            } else {
                $outlet_file["inside_view_outlet"] = $outlet_foto->inside_view_outlet;
            }

            if($request->hasFile('side_view_outlet')){
                $file3 = $request->file('side_view_outlet');
                $sideView = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file3->getClientOriginalName());
                $image3 = Image::make(request()->file('side_view_outlet'))->encode($file3->getClientOriginalExtension());
                $sideViewOutlet = $destinationPath.$sideView;
                Storage::disk('s3')->put($sideViewOutlet, (string) $image3);

                $outlet_file["side_view_outlet"] = $sideViewOutlet;
            } else {
                $outlet_file["side_view_outlet"] = $outlet_foto->side_view_outlet;
            }

            if($request->hasFile('other_view_outlet')){
                $file4 = $request->file('other_view_outlet');
                $otherView = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file4->getClientOriginalName());
                $image4 = Image::make(request()->file('other_view_outlet'))->encode($file4->getClientOriginalExtension());
                $otherViewOutlet = $destinationPath.$otherView;
                Storage::disk('s3')->put($otherViewOutlet, (string) $image4);

                $outlet_file["other_view_outlet"] = $otherViewOutlet;
            } else {
                if(isset($outlet_foto->other_view_outlet)){
                    $outlet_file["other_view_outlet"] = $outlet_foto->other_view_outlet;
                }
            }

            $data->business_email = $request->business_email;
            $data->business_phone_number = $request->business_phone_number;
            $data->grab_food_name = $request->grab_food_name;
            $data->business_scalable =  $request->business_scalable;
            $data->business_revenue_average = $request->business_revenue_average;
            $data->business_category =  $request->business_category;
            $data->business_address =  $request->business_address;
            $data->countries_name = $request->countries_name;
            $data->province_name = $request->province_name;
            $data->city_name = $request->city_name;
            $data->district_name = $request->district_name;
            $data->location_type =  $request->location_type;
            $data->postal_code =  $request->postal_code;
            $data->google_map_link = $request->google_map_link;
            $data->location_benchmark = $request->location_benchmark;
            $data->outlet_foto = json_encode($outlet_file);

            $data->save();

            return "<script>window.location.href='".route('grab.activation.create', ['step'=>4])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }

    }

    public function stepFour(Request $request)
    {
        try {
            $id = $request->id;
            $data = GrabActivation::find($id);

            $bank_file = $data->bank_file;
            $attorney_letter_file = $data->letter_of_attorney_file;
            $is_same_bank_name = ($request->is_same_bank_name == 'null') ? 1 : 0;

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if(!$request->hasFile('bank_file') && is_null($bank_file))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Cover depan buku tabungan belum dipilih !</div>";
            }

            if($is_same_bank_name === 0 && !$request->hasFile('letter_of_attorney_file') && is_null($attorney_letter_file)){
                return "<div class='alert alert-warning' style='text-align: center'>Foto surat kuasa belum dipilih !</div>";
            }

            if($request->hasFile('bank_file')){
                $file1 = $request->file('bank_file');
                if($file1->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Cover depan buku tabungan lebih dari 1MB !</div>";
                }
            }

            if($request->hasFile('letter_of_attorney_file')){
                $file2 = $request->file('letter_of_attorney_file');
                if($file2->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Foto surat kuasa lebih dari 1MB !</div>";
                }
            }

            if($request->hasFile('bank_file')){
                $file1 = $request->file('bank_file');
                $fileBank = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file1->getClientOriginalName());
                $image1 = Image::make(request()->file('bank_file'))->encode($file1->getClientOriginalExtension());
                $bankFileName = $destinationPath.$fileBank;
                Storage::disk('s3')->put($bankFileName, (string) $image1);

                $bankFile = $bankFileName;
            } else {
                $bankFile = $bank_file;
            }

            if($request->hasFile('letter_of_attorney_file')){
                $file2 = $request->file('letter_of_attorney_file');
                $fileAttorney = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file2->getClientOriginalName());
                $image2 = Image::make(request()->file('letter_of_attorney_file'))->encode($file2->getClientOriginalExtension());
                $attorneyFileName = $destinationPath.$fileAttorney;
                Storage::disk('s3')->put($attorneyFileName, (string) $image2);

                $attorney_file = $attorneyFileName;
            } else {
                $attorney_file = $attorney_letter_file;
            }

            $data->bank_name = $request->bank_name;
            $data->bank_number = $request->bank_number;
            $data->bank_account_name = $request->bank_account_name;
            $data->bank_branch = $request->bank_branch;
            $data->is_same_bank_name = $is_same_bank_name;
            $data->bank_file = $bankFile;
            $data->letter_of_attorney_file = $attorney_file;

            $data->save();

            return "<script>window.location.href='".route('grab.activation.create', ['step'=>5])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function stepFive(Request $request)
    {
        try {
            ini_set('max_execution_time', 0);
            $id = $request->id;
            $data = GrabActivation::find($id);

            $logo_file = $data->outlet_logo;
            $menu_file = json_decode($data->menu_list);
            $food_file = json_decode($data->food_list);

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if(!$request->hasFile('outlet_logo') && is_null($logo_file))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Logo restoran belum dipilih !</div>";
            }

            if($request->hasFile('outlet_logo')){
                $file1 = $request->file('outlet_logo');
                if($file1->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Logo restoran lebih dari 1MB !</div>";
                }
            }

            if(!$request->hasFile('menu') && is_null($menu_file))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Pilih setidaknya 1 foto daftar menu !</div>";
            }

            if($request->hasFile('menu')){
                foreach($request->file('menu') as $key => $item){
                    $file = $item;
                    $field = str_replace("menu", '', $key);
                    $fieldName = str_replace("'",'',$field);
                    if($file->getSize() > $this->maxFileSize){
                        return "<div class='alert alert-warning' style='text-align: center'>Foto Daftar Menu ".$fieldName." lebih dari 1MB !</div>";
                    }
                }

            }

            if(!$request->hasFile('food') && is_null($food_file))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Pilih setidaknya 1 foto makanan dan minuman !</div>";
            }

            if($request->hasFile('food')){
                foreach($request->file('food') as $key => $item){
                    $file = $item;
                    $field = str_replace("food", '', $key);
                    $fieldName = str_replace("'",'',$field);
                    if($file->getSize() > $this->maxFileSize){
                        return "<div class='alert alert-warning' style='text-align: center'>Foto Makanan dan Minuman " .$fieldName. " lebih dari 1MB !</div>";
                    }
                }
            }


            if($request->hasFile('outlet_logo')){
                $file1 = $request->file('outlet_logo');
                $fileLogo = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file1->getClientOriginalName());
                $image1 = Image::make(request()->file('outlet_logo'))->encode($file1->getClientOriginalExtension());
                $logoFileName = $destinationPath.$fileLogo;
                Storage::disk('s3')->put($logoFileName, (string) $image1);

                $logoFile = $logoFileName;
            } else {
                $logoFile = $logo_file;
            }

            $menu = [];
            if($request->hasFile('menu')){
                foreach($request->file('menu') as $key => $item){
                    $file = $item;
                    $fileMenu = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                    $image = Image::make($file)->encode($file->getClientOriginalExtension());
                    $fileMenuName = $destinationPath.$fileMenu;
                    Storage::disk('s3')->put($fileMenuName, (string) $image);
                    if(is_null($menu_file)){
                        $menu += [str_replace("'","",$key) => $fileMenuName];
                    } else {
                        $menu = $menu_file;
                        $k = str_replace("'","",$key);
                        $menu->$k = $fileMenuName;
                    }

                }

            } else {
                $menu = $menu_file;
            }

            $food = [];

            if($request->hasFile('food')){
                foreach($request->file('food') as $key => $item){
                    $file = $item;
                    $fileFood = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                    $image = Image::make($file)->encode($file->getClientOriginalExtension());
                    $fileFoodName = $destinationPath.$fileFood;
                    Storage::disk('s3')->put($fileFoodName, (string) $image);
                    if(is_null($food_file)){
                        $food += [str_replace("'","",$key) => $fileFoodName];
                    } else {
                        $food = $food_file;
                        $k = str_replace("'","",$key);
                        $food->$k = $fileFoodName;
                    }

                }

            } else {
                $food = $food_file;
            }

            $outlet_type_food = explode(',', $request->outlet_type_food);
            $outlet_tag_food = explode(',', $request->outlet_tag_food);

            $data->outlet_type_food = json_encode($outlet_type_food);
            $data->outlet_tag_food = json_encode($outlet_tag_food);
            $data->food_classification = $request->food_classification;
            $data->outlet_detail = $request->outlet_detail;
            $data->time_pickup = $request->time_pickup;
            $data->outlet_logo = $logoFile;
            $data->menu_list = json_encode($menu);
            $data->food_list = json_encode($food);

            $data->save();

            return "<script>window.location.href='".route('grab.activation.create', ['step'=>6])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function stepSix(Request $request)
    {
        try {
            $id = $request->id;
            $data = GrabActivation::find($id);

            $data->time_operational = $request->time_operational;

            $data->save();

            return "<script>window.location.href='".route('grab.activation.create', ['step'=>7])."'</script>";
        }catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function stepSeven(Request $request)
    {
        try {
            $id = $request->id;
            $data = GrabActivation::find($id);
            $npwp_file = $data->npwp_file;

            if(!$request->hasFile('npwp_file') && is_null($npwp_file))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto NPWP belum dipilih !</div>";
            }

            if($request->hasFile('npwp_file')){
                $file = $request->file('npwp_file');
                if($file->getSize() > $this->maxFileSize){
                    return "<div class='alert alert-warning' style='text-align: center'>Foto NPWP lebih dari 1MB !</div>";
                }
            }

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if($request->hasFile('npwp_file')){
                $file = $request->file('npwp_file');
                $fileNpwp = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('npwp_file'))->encode($file->getClientOriginalExtension());
                $npwpFileName = $destinationPath.$fileNpwp;
                Storage::disk('s3')->put($npwpFileName, (string) $image);

                $npwpFile = $npwpFileName;
            } else {
                $npwpFile = $npwp_file;
            }


            $data->npwp_number = $request->npwp_number;
            $data->npwp_file = $npwpFile;
            $data->registration_status = 1;
            $data->save();

            return "<script>window.location.href='".route('grab.activation.create', ['step'=>8])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function delete(Request $request)
    {
        try{
            $id = $request->id;
            $data = GrabActivation::find($id);
            $data->delete();

            return "<div class='alert alert-success' style='text-align: center'>Riwayat pendaftaran berhasil dihapus !</div>
                    <script>reload(2000);</script>";


        }catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function getCity(Request $request)
    {
        try{
            $id = $request->id;
            $data = City::where('md_province_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }

    public function getDistrict(Request $request)
    {
        try{
            $id = $request->id;
            $data = District::where('md_city_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }

    public function getFoodType()
    {
        try {
            $data = [
                "bakery","bakso","bali","beverage","buah","bubble tea","burger",
                "chicken","chinese","desert","fast food","frozen food","indian",
                "indonesian","japanese","juice","korean","martabak","mie","padang",
                "pizza","salad","sate","seafood","snacks","soup","tea","thai",
                "vegetables","vietnamese","western"
            ];

           return $data;

        }catch(\Exception $e){
            return [];
        }
    }

    public function getTimePickup()
    {
        try {
            $data = [
                "5 Menit",
                "10 Menit",
                "15 Menit",
                "20 Menit",
                "25 Menit",
                "30 Menit",
                "Lebih dari 30 Menit"
            ];

            return $data;
        } catch(\Exception $e){
            return [];
        }
    }

}

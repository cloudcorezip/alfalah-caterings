<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Grab;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Models\MasterData\User;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\TypeOfBusiness;
use App\Models\Grab\GrabActivation;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;
use App\Models\MasterData\Region\District;
use App\Models\MasterData\SennaCashier\Category;
use App\Models\MasterData\Bank;

use Image;

class GrabIntegrationController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('grab/activation')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::get('/','Grab\GrabIntegrationController@index')
                ->name('grab.activation');
            Route::post('/stepOne','Grab\GrabIntegrationController@stepOne')
                ->name('grab.activation.step-one');
            Route::post('/stepTwo','Grab\GrabIntegrationController@stepTwo')
                ->name('grab.activation.step-two');
            Route::post('/stepThree','Grab\GrabIntegrationController@stepThree')
                ->name('grab.activation.step-three');
            Route::post('/stepFour','Grab\GrabIntegrationController@stepFour')
                ->name('grab.activation.step-four');
            Route::post('/stepFive','Grab\GrabIntegrationController@stepFive')
                ->name('grab.activation.step-five');
            Route::post('/stepSix','Grab\GrabIntegrationController@stepSix')
                ->name('grab.activation.step-six');
            Route::get('/get-city','Grab\GrabIntegrationController@getCity')
                ->name('grab.activation.get-city');
            Route::get('/get-district','Grab\GrabIntegrationController@getDistrict')
                ->name('grab.activation.get-district');
        });
    }

    public function index(Request $request)
    {
        $data = GrabActivation::where('md_merchant_id', merchant_id())->first();
        $foodType = $this->getFoodType();

        if(is_null($data)){
            $step = null;
        } else {
            $step = $request->step;
        }

        $params = [
            "title" => "Pengajuan Akun Digital",
            "data" => $data,
            "user" => User::find(user_id()),
            "merchant" => Merchant::find(merchant_id()),
            "step" => $step,
            "type" => TypeOfBusiness::All(),
            "province" => Province::All(),
            "category" => Category::All(),
            "bank"=> Bank::All(),
            "foodType" => $foodType
        ];


        return view('merchant::grab.index', $params);
    }

    public function stepOne(Request $request)
    {
        try {
            $initial = ["initial" => "null"];

            $id = $request->id;

            if(is_null($id)){
                $data = new GrabActivation();
            } else {
                $data = GrabActivation::find($id);
            }


            $data->md_merchant_id = merchant_id();
            $data->registrant_name = $request->registrant_name;
            $data->registrant_email = $request->registrant_email;
            $data->registrant_phone_number = $request->registrant_phone_number;

            $data->save();

            return "<script>window.location.href='".route('grab.activation', ['step'=>2])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function stepTwo(Request $request)
    {
        try {
            $id = $request->id;
            $data = GrabActivation::find($id);

            $imageIdentity = json_decode($data->owner_file);

            if(!$request->hasFile('owner_identity_file') && !isset($imageIdentity->identity_card))
            {
                return "<div class='alert alert-warning' style='text-align: center'>KTP/Paspor belum dipilih !</div>";
            }

            if(!$request->hasFile('owner_identity_face') && !isset($imageIdentity->identity_card_with_face))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto verifikasi wajah dengan KTP belum dipilih!</div>";
            }

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if($request->hasFile('owner_identity_file')){
                $file1=$request->file('owner_identity_file');
                $fileNameIdentity= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file1->getClientOriginalName());
                $image1 = Image::make(request()->file('owner_identity_file'))->encode($file1->getClientOriginalExtension());
                $fileIdentity=$destinationPath.$fileNameIdentity;
                Storage::disk('s3')->put($fileIdentity, (string) $image1);

                $owner_file = ["identity_card" => $fileIdentity];
            } else {
                $owner_file = ["identity_card" => $imageIdentity->identity_card];
            }

            if($request->hasFile('owner_identity_face')){
                $file2=$request->file('owner_identity_face');
                $fileNameIdentityFace= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file2->getClientOriginalName());
                $image2 = Image::make(request()->file('owner_identity_face'))->encode($file1->getClientOriginalExtension());
                $fileIdentityFace=$destinationPath.$fileNameIdentityFace;
                Storage::disk('s3')->put($fileIdentityFace, (string) $image2);

                $owner_file["identity_card_with_face"] = $fileIdentityFace;
            } else {
                $owner_file["identity_card_with_face"] = $imageIdentity->identity_card_with_face;
            }

            $data->business_type_name = $request->business_type_name;
            $data->owner_name = $request->owner_name;
            $data->owner_address =  $request->owner_address;
            $data->owner_citizenship = $request->owner_citizenship;
            $data->owner_identity_card_number = $request->owner_identity_card_number;
            $data->owner_phone_number =  $request->owner_phone_number;
            $data->owner_email =  $request->owner_email;
            $data->owner_birthdate = $request->owner_birthdate;
            $data->job_level =  strtolower($request->job_level);
            $data->owner_file = json_encode($owner_file);

            $data->save();

            return "<script>window.location.href='".route('grab.activation', ['step'=>3])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function stepThree(Request $request)
    {
        try {
            $id = $request->id;
            $data = GrabActivation::find($id);

            $outlet_foto = json_decode($data->outlet_foto);

            if(!$request->hasFile('front_view_outlet') && !isset($outlet_foto->front_view_outlet))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto tampak depan outlet belum dipilih !</div>";
            }

            if(!$request->hasFile('inside_view_outlet') && !isset($outlet_foto->inside_view_outlet))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto tampak dalam outlet belum dipilih !</div>";
            }

            if(!$request->hasFile('side_view_outlet') && !isset($outlet_foto->side_view_outlet))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Foto tampak samping outlet belum dipilih !</div>";
            }

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if($request->hasFile('front_view_outlet')){
                $file1 = $request->file('front_view_outlet');
                $frontView = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file1->getClientOriginalName());
                $image1 = Image::make(request()->file('front_view_outlet'))->encode($file1->getClientOriginalExtension());
                $frontViewOutlet = $destinationPath.$frontView;
                Storage::disk('s3')->put($frontViewOutlet, (string) $image1);

                $outlet_file = ["front_view_outlet" => $frontViewOutlet];
            } else {
                $outlet_file = ["front_view_outlet" => $outlet_foto->front_view_outlet];
            }

            if($request->hasFile('inside_view_outlet')){
                $file2 = $request->file('inside_view_outlet');
                $insideView = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file2->getClientOriginalName());
                $image2 = Image::make(request()->file('inside_view_outlet'))->encode($file2->getClientOriginalExtension());
                $insideViewOutlet = $destinationPath.$insideView;
                Storage::disk('s3')->put($insideViewOutlet, (string) $image2);

                $outlet_file["inside_view_outlet"] = $insideViewOutlet;
            } else {
                $outlet_file["inside_view_outlet"] = $outlet_foto->inside_view_outlet;
            }

            if($request->hasFile('side_view_outlet')){
                $file3 = $request->file('side_view_outlet');
                $sideView = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file3->getClientOriginalName());
                $image3 = Image::make(request()->file('side_view_outlet'))->encode($file3->getClientOriginalExtension());
                $sideViewOutlet = $destinationPath.$sideView;
                Storage::disk('s3')->put($sideViewOutlet, (string) $image3);

                $outlet_file["side_view_outlet"] = $sideViewOutlet;
            } else {
                $outlet_file["side_view_outlet"] = $outlet_foto->side_view_outlet;
            }

            if($request->hasFile('other_view_outlet')){
                $file4 = $request->file('other_view_outlet');
                $otherView = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file4->getClientOriginalName());
                $image4 = Image::make(request()->file('other_view_outlet'))->encode($file4->getClientOriginalExtension());
                $otherViewOutlet = $destinationPath.$otherView;
                Storage::disk('s3')->put($otherViewOutlet, (string) $image4);

                $outlet_file["other_view_outlet"] = $otherViewOutlet;
            } else {
                if(isset($outlet_foto->other_view_outlet)){
                    $outlet_file["other_view_outlet"] = $outlet_foto->other_view_outlet;
                }
            }

            $data->business_email = $request->business_email;
            $data->business_phone_number = $request->business_phone_number;
            $data->grab_food_name = $request->grab_food_name;
            $data->business_scalable =  $request->business_scalable;
            $data->business_revenue_average = $request->business_revenue_average;
            $data->business_category =  $request->business_category;
            $data->business_address =  $request->business_address;
            $data->countries_name = $request->countries_name;
            $data->province_name = $request->province_name;
            $data->city_name = $request->city_name;
            $data->district_name = $request->district_name;
            $data->location_type =  $request->location_type;
            $data->postal_code =  $request->postal_code;
            $data->google_map_link = $request->google_map_link;
            $data->location_benchmark = $request->location_benchmark;
            $data->outlet_foto = json_encode($outlet_file);

            $data->save();

            return "<script>window.location.href='".route('grab.activation', ['step'=>4])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }

    }

    public function stepFour(Request $request)
    {
        try {
            $id = $request->id;
            $data = GrabActivation::find($id);

            $bank_file = $data->bank_file;
            $attorney_letter_file = $data->letter_of_attorney_file;
            $is_same_bank_name = ($request->is_same_bank_name == 'null') ? 1 : 0;

            $destinationPath = 'public/uploads/merchant/'.user_id().'/grab/';

            if(!$request->hasFile('bank_file') && is_null($bank_file))
            {
                return "<div class='alert alert-warning' style='text-align: center'>Cover depan buku tabungan belum dipilih !</div>";
            }

            if($is_same_bank_name === 0 && !$request->hasFile('letter_of_attorney_file') && is_null($attorney_letter_file)){
                return "<div class='alert alert-warning' style='text-align: center'>Foto surat kuasa belum dipilih !</div>";
            }

            if($request->hasFile('bank_file')){
                $file1 = $request->file('bank_file');
                $fileBank = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file1->getClientOriginalName());
                $image1 = Image::make(request()->file('bank_file'))->encode($file1->getClientOriginalExtension());
                $bankFileName = $destinationPath.$fileBank;
                Storage::disk('s3')->put($bankFileName, (string) $image1);

                $bankFile = $bankFileName;
            } else {
                $bankFile = $bank_file;
            }

            if($request->hasFile('letter_of_attorney_file')){
                $file2 = $request->file('letter_of_attorney_file');
                $fileAttorney = uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file2->getClientOriginalName());
                $image2 = Image::make(request()->file('letter_of_attorney_file'))->encode($file2->getClientOriginalExtension());
                $attorneyFileName = $destinationPath.$fileAttorney;
                Storage::disk('s3')->put($attorneyFileName, (string) $image2);

                $attorney_file = $attorneyFileName;
            } else {
                $attorney_file = $attorney_letter_file;
            }


            $data->bank_name = $request->bank_name;
            $data->bank_number = $request->bank_number;
            $data->bank_account_name = $request->bank_account_name;
            $data->bank_branch = $request->bank_branch;
            $data->is_same_bank_name = $is_same_bank_name;
            $data->bank_file = $bankFile;
            $data->letter_of_attorney_file = $attorney_file;

            $data->save();

            return "<script>window.location.href='".route('grab.activation', ['step'=>5])."'</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }

    public function stepFive(Request $request)
    {
        return "<script>window.location.href='".route('grab.activation', ['step'=>6])."'</script>";
    }

    public function stepSix(Request $request)
    {
        return "<script>window.location.href='".route('grab.activation', ['step'=>7])."'</script>";
    }

    public function getCity(Request $request)
    {
        try{
            $id = $request->id;
            $data = City::where('md_province_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }

    public function getDistrict(Request $request)
    {
        try{
            $id = $request->id;
            $data = District::where('md_city_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }

    public function getFoodType()
    {
        try {
            $data = [
                "bakery","bakso","bali","beverage","buah","bubble tea","burger",
                "chicken","chinese","desert","fast food","frozen food","indian",
                "indonesian","japanese","juice","korean","martabak","mie","padang",
                "pizza","salad","sate","seafood","snacks","soup","tea","thai",
                "vegetables","vietnamese","western"
            ];

           return $data;

        }catch(\Exception $e){
            return [];
        }
    }

}

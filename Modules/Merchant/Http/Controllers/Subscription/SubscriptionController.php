<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Subscription;

use App\Http\Middleware\SubscriptionWebVerification;
use App\Http\Controllers\Controller;
use App\Classes\Singleton\XenditCore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\Subscription;
use App\Models\SennaPayment\Transaction;
use App\Models\SennaPayment\PaymentSubscription;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\SennaPayment\TransactionStatus;
use App\Utils\Membership\Merchant\RedeemUtil;
use App\Utils\Subscription\SubscriptionUtil;


class SubscriptionController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('/subscription')
        //->middleware(SubscriptionWebVerification::class)
        ->group(function (){
            Route::get('/','Subscription\SubscriptionController@index')
                ->name('subscription.index');
            Route::get('/invoice/{key}','Subscription\SubscriptionController@invoice')
                ->name('subscription.invoice');
            Route::post('/create-invoice', 'Subscription\SubscriptionController@createInvoice')
                ->name('subscription.create-invoice');
            Route::post('/checkout', 'Subscription\SubscriptionController@checkout')
                ->name('subscription.checkout');
            Route::post('/check-referral', 'Subscription\SubscriptionController@checkReferral')
                ->name('subscription.check-referral');
        });
    }

    public function checkReferral(Request $request)
    {
        try {
            $id = $request->id;
            $data = PaymentSubscription::with('transaction')
                        ->with('getSubscription')
                        ->where('id', $id)->first();

            $price = ceil($data->getSubscription->price - ($data->getSubscription->price * ($data->getSubscription->discount_percentage/100)));
            $discount = ceil(($data->getSubscription->price * ($data->getSubscription->discount_percentage/100)));
            $referralCode = is_null($data->refferal_code)? $request->referral_code:$data->refferal_code;

            $promo=DB::select("select * from mrkt_promos mp where mp.content @>'[{".'"id"'.":".'"'."".$data->getSubscription->id."".'"'."}]'
            and '".date('Y-m-d')."' between mp.start_period::date and mp.end_period::date
            and mp.code='".$referralCode."' limit 1");

            $promoPercentage=false;

            if(count($promo)>0){
                if($promo[0]->value==0){
                    $valuePromo=$promo[0]->value_percentage;
                    $promoPercentage=true;

                }else{
                    $valuePromo=$promo[0]->value;
                    $promoPercentage=false;
                }
            }else{
                $valuePromo=0;
            }

            if($promoPercentage==true){
                $amount=ceil($price - ($price * ($valuePromo/100)));
                $totalPromoRef = ceil($price * ($valuePromo/100));
            }else{
                $amount=ceil($price - $valuePromo);
                $totalPromoRef = ceil($valuePromo);
            }

            if(!is_null($request->md_transaction_type_id)){
                if($request->md_transaction_type_id == 4){
                    $adminFee = ceil($amount * 0.007);
                    $amount = ceil($amount + $adminFee);
                } else {
                    $adminFee = 4500;
                    $amount = ceil($amount + $adminFee);
                }
            } else {
                $adminFee = 0;
            }

            return [
                "amountRupiah" => rupiah($amount),
                "totalPromoRupiah" => rupiah(ceil($discount + $totalPromoRef)),
                "adminFee" => rupiah($adminFee),
                "discount" => ceil($discount + $totalPromoRef),
                "admin" => $adminFee,
                "is_get_promo" => (count($promo) > 0)? true:false
            ];

        } catch(\Exception $e)
        {
            return false;
        }
    }

    public function index(Request $request)
    {
        $data = Subscription::where('is_deleted', 0)
            ->where('type','yearly')
                ->orderBy('id', 'ASC')
                ->get();

        $paymentSubscription = PaymentSubscription::with('transaction')
                                ->with('getMerchant')
                                ->with('getSubscription')
                                ->where('md_merchant_id', merchant_id())->get();

        $params = [
            "title" => "Layanan Berlangganan",
            "data" => $data,
            "user" => User::find(user_id()),
            "paymentSubscription" => $paymentSubscription
        ];


        return view('merchant::subscription.index', $params);
    }

    public function invoice(Request $request)
    {
        try {
            $id = decrypt($request->key);
            $user = User::find(user_id());

            $data = PaymentSubscription::with('transaction')
                        ->with('getMerchant')
                        ->with('getSubscription')
                        ->where('id', $id)->first();
            if(is_null($data)){
                return abort(404);
            }

            if($data->md_merchant_id != merchant_id()){
                return abort(404);
            }

            $subscription = Subscription::find($data->md_subscription_id);
            $price = ceil($subscription->price - ($subscription->price * ($subscription->discount_percentage/100)));
            $endDate = SubscriptionUtil::calculateSubsVAOrQRIS($user,$subscription);

            $duration = $this->_getDuration($subscription->id);

            $reffData = $this->checkReferral(new Request(['id' => $id, 'md_transaction_type_id'=>$data->md_transaction_type_id]));

            $params = [
                "title" => "Invoice ".$data->transaction->code,
                "data" => $data,
                "user" => $user,
                "price" => ceil($data->transaction->amount),
                "discount" => $reffData['discount'],
                "admin" => $reffData['admin'],
                "duration" => ($duration)? $duration: '-',
                "created_at" => Carbon::parse($data->created_at)->isoFormat('D MMMM Y')
            ];


            return view('merchant::subscription.invoice', $params);

        }catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return abort(404);
        }
    }

    public function createInvoice(Request $request)
    {
        try {
            date_default_timezone_set((is_null($request->timezone))?'Asia/Jakarta':$request->timezone);
            DB::beginTransaction();

            $data = new PaymentSubscription();

            $user = User::find(user_id());
            $code = CodeGenerator::generateTransactionCode($user->id,TransactionType::SUBSCRIPTION_FEE);
            $subscription=Subscription::find($request->md_subscription_id);
            $discount = ceil(($subscription->price * ($subscription->discount_percentage/100)));
            $amount =  ceil($subscription->price - $discount);

            $data->md_merchant_id = merchant_id();
            $data->md_subscription_id = $request->md_subscription_id;
            $data->is_from_web = 1;
            $data->save();

            $data->transaction()->create([
                'code'=>$code,
                'md_user_id'=>$user->id,
                'amount'=>$amount,
                'discount' => $discount,
                'transaction_from'=>$user->fullname,
                'md_sp_transaction_type_id'=>TransactionType::SUBSCRIPTION_FEE,
                'md_sp_transaction_status_id'=>TransactionStatus::PENDING,
            ]);

            DB::commit();

            return response()->json([
                'message'=>'Permintaan anda sedang diproses !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('subscription.invoice', ['key' => encrypt($data->id)])
            ]);

        }catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function checkout(Request $request)
    {
        try {
            DB::beginTransaction();

            $id = $request->id;
            $user = User::find(user_id());
            $data = PaymentSubscription::with('transaction')
                        ->with('getSubscription')
                        ->where('id', $id)->first();

            if(is_null($id) || $data->md_merchant_id != merchant_id() || is_null($data)){
                return response()->json([
                    'message'=>'Terjadi kesalahan di server, silahkan coba lagi nanti !',
                    'type'=>'danger',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($request->md_transaction_type_id)){
                return response()->json([
                    'message'=>'Silahkan pilih metode pembayaran terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $price = ceil($data->getSubscription->price - ($data->getSubscription->price * ($data->getSubscription->discount_percentage/100)));
            $discount = ceil(($data->getSubscription->price * ($data->getSubscription->discount_percentage/100)));
            $referralCode = is_null($data->refferal_code)? $request->referral_code:$data->refferal_code;

            $promo=DB::select("select * from mrkt_promos mp where mp.content @>'[{".'"id"'.":".'"'."".$data->getSubscription->id."".'"'."}]'
            and '".date('Y-m-d')."' between mp.start_period::date and mp.end_period::date
            and mp.code='".$referralCode."' limit 1");

            $promoPercentage=false;

            if(count($promo)>0){
                if($promo[0]->value==0){
                    $valuePromo=$promo[0]->value_percentage;
                    $promoPercentage=true;
                    $valDiscount = ceil($price * ($valuePromo/100));

                }else{
                    $valuePromo=$promo[0]->value;
                    $promoPercentage=false;
                    $valDiscount = $valuePromo;
                }
            }else{
                $valuePromo=0;
                $valDiscount=0;
            }

            $amount = ($promoPercentage)? ceil($price - ($price * ($valuePromo/100))) : ceil($price - $valuePromo);

            // md_transaction_type_id
            // 4 => QRIS
            // 5 => VA

            $adminFee = ($request->md_transaction_type_id == 4)? ceil($amount * 0.007) : 4500;

            $params = [
                "external_id" => $data->transaction->code,
                "payer_email" => $user->email,
                "amount" => ceil($amount + $adminFee),
                "description" => "Pembayaran Paket Berlangganan",
                'payment_methods'=>($request->md_transaction_type_id==4)?config('merchant.pay_sub_wallet'):config('merchant.pay_sub_va'),
                "success_redirect_url"=>route('subscription.invoice', ['key' => encrypt($data->id)]),
                "failure_redirect_url"=>route('subscription.invoice', ['key' => encrypt($data->id)])
            ];

            if($data->is_from_web == 0){
                return response()->json([
                    'message'=>'Silahkan buka aplikasi Senna untuk melakukan pembayaran',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=> ''
                ]);
            }

            if($data->is_expired == 0){
                if($data->md_transaction_type_id == $request->md_transaction_type_id){
                    $invoice = json_decode($data->content_resp_web, true);
                } else {
                    $invoice = XenditCore::createInvoiceWeb($params);
                }
            } else {
                return response()->json([
                    'message'=>'Mohon maaf pembayaran gagal dilakukan karena invoice sudah kadaluarsa',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=> ''
                ]);
            }

            if(!$invoice){
                return response()->json([
                    'message'=>'Terjadi kesalahan di server, silahkan coba lagi nanti !',
                    'type'=>'danger',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $data->content_resp_web = json_encode($invoice);
            if(count($promo) > 0){
                $data->refferal_code = $referralCode;
            }
            $data->md_transaction_type_id = $request->md_transaction_type_id;
            $data->save();

            $data->transaction->amount = ceil($amount + $adminFee);
            $data->transaction->discount = ceil($discount + $valDiscount);
            $data->transaction->admin_fee = $adminFee;
            $data->transaction->save();

            DB::commit();
            return response()->json([
                'message'=>'Permintaan anda sedang diproses !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=> $invoice['invoice_url']
            ]);
        }catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    protected function _getDuration($id)
    {
        try {
            $subscription = Subscription::where('is_deleted', 0)
                                ->where('id', $id)
                                ->first();

            switch($subscription->type){
                case "monthly":
                    $monthDuration = 1;
                    break;
                case "threemonth":
                    $monthDuration = 3;
                    break;
                case "sixmonth":
                    $monthDuration = 6;
                    break;
                case "yearly":
                    $monthDuration = 12;
                    break;
                default:
                    $monthDuration = 0;
                    break;
            }

            $duration = $monthDuration + $subscription->plus_month." Bulan";

            return $duration;

        } catch(\Exception $e)
        {
            return false;
        }
    }

}

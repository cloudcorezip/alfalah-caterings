<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Subscription;

use App\Http\Middleware\SubscriptionWebVerification;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\Subscription;
use Modules\Merchant\Entities\Subscription\SubscriptionHistoryEntity;
use App\Models\SennaPayment\Transaction;
use App\Models\SennaPayment\PaymentSubscription;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\SennaPayment\TransactionStatus;
use App\Utils\Membership\Merchant\RedeemUtil;
use Illuminate\Support\Facades\DB;
use PDF;

class SubscriptionHistoryController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('/subscription/history')
        //->middleware(SubscriptionWebVerification::class)
        ->group(function (){
            Route::get('/','Subscription\SubscriptionHistoryController@index')
                ->name('subscription.history.index');
            Route::post('/data-table', 'Subscription\SubscriptionHistoryController@dataTable')
                ->name('subscription.history.datatable');
            Route::get('/export/{key}', 'Subscription\SubscriptionHistoryController@exportPdf')
                ->name('subscription.history.export-pdf');
        });
    }

    private function checkReferral(Request $request)
    {
        try {
            $id = $request->id;
            $data = PaymentSubscription::with('transaction')
                        ->with('getSubscription')
                        ->where('id', $id)->first();

            $price = ceil($data->getSubscription->price - ($data->getSubscription->price * ($data->getSubscription->discount_percentage/100)));
            $discount = ceil(($data->getSubscription->price * ($data->getSubscription->discount_percentage/100)));
            $referralCode = is_null($data->refferal_code)? $request->referral_code:$data->refferal_code;

            $promo=DB::select("select * from mrkt_promos mp where mp.content @>'[{".'"id"'.":".'"'."".$data->getSubscription->id."".'"'."}]'
            and '".date('Y-m-d')."' between mp.start_period::date and mp.end_period::date
            and mp.code='".$referralCode."' limit 1");

            $promoPercentage=false;

            if(count($promo)>0){
                if($promo[0]->value==0){
                    $valuePromo=$promo[0]->value_percentage;
                    $promoPercentage=true;

                }else{
                    $valuePromo=$promo[0]->value;
                    $promoPercentage=false;
                }
            }else{
                $valuePromo=0;
            }

            if($promoPercentage==true){
                $amount=ceil($price - ($price * ($valuePromo/100)));
                $totalPromoRef = ceil($price * ($valuePromo/100));
            }else{
                $amount=ceil($price - $valuePromo);
                $totalPromoRef = ceil($valuePromo);
            }

            if(!is_null($request->md_transaction_type_id)){
                if($request->md_transaction_type_id == 4){
                    $adminFee = ceil($amount * 0.007);
                    $amount = ceil($amount + $adminFee);
                } else {
                    $adminFee = 4500;
                    $amount = ceil($amount + $adminFee);
                }
            } else {
                $adminFee = 0;
            }

            if(count($promo) > 0){
                return [
                    "amountRupiah" => rupiah($amount),
                    "totalPromoRupiah" => rupiah(ceil($discount + $totalPromoRef)),
                    "adminFee" => rupiah($adminFee),
                    "discount" => ceil($discount + $totalPromoRef),
                    "admin" => $adminFee,
                    "is_get_promo" => true
                ];
            } else {
                return [
                    "amountRupiah" => rupiah($amount),
                    "totalPromoRupiah" => rupiah(ceil($discount + $totalPromoRef)),
                    "adminFee" => rupiah($adminFee),
                    "discount" => ceil($discount + $totalPromoRef),
                    "admin" => $adminFee,
                    "is_get_promo" => false
                ];
            }

        } catch(\Exception $e)
        {
            return false;
        }
    }

    public function index(Request $request)
    {
        $data = Subscription::where('is_deleted', 0)
                ->orderBy('id', 'ASC')
                ->get()
        ;
        parent::setMenuKey($request->key);

        $params = [
            "title" => "Riwayat Pembelian",
            "data" => $data,
            "user" => User::find(user_id()),
            "tableColumns" => SubscriptionHistoryEntity::dataTableColumns(),
            'key_val'=>$request->key
        ];


        return view('merchant::subscription.history.index', $params);
    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return SubscriptionHistoryEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public function exportPdf(Request $request)
    {
        try {
            $id = decrypt($request->key);
            $user = User::find(user_id());
            $data = PaymentSubscription::with('transaction')
                        ->with('getMerchant')
                        ->with('getSubscription')
                        ->where('id', $id)->first();

            if(is_null($id) || $data->md_merchant_id != merchant_id() || is_null($data)){
                return abort(404);
            }

            $duration = $this->_getDuration($data->md_subscription_id);
            $reffData = $this->checkReferral(new Request(['id' => $id, 'md_transaction_type_id'=>$data->md_transaction_type_id]));
            $params = [
                "data" => $data,
                "title" => 'Export Invoice - '.$data->transaction->code,
                "duration" => ($duration)? $duration: '-',
                "discount" => $reffData['discount'],
                "price" => ceil($data->transaction->amount),
                "admin"=>$reffData['admin'],
                "created_at" => Carbon::parse($data->created_at)->isoFormat('D MMMM Y'),
                "user" => $user
            ];
            $pdf = PDF::loadview('merchant::subscription.invoice-pdf', $params)->setPaper('a4');
            return $pdf->stream('export-invoice-'.$data->transaction->code.'_'.date('Y-m-d').'.pdf');

        }catch(\Exception $e)
        {
            return abort(404);
        }
    }

    protected function _getDuration($id)
    {
        try {
            $subscription = Subscription::where('is_deleted', 0)
                                ->where('id', $id)
                                ->first();

            switch($subscription->type){
                case "monthly":
                    $monthDuration = 1;
                    break;
                case "threemonth":
                    $monthDuration = 3;
                    break;
                case "sixmonth":
                    $monthDuration = 6;
                    break;
                case "yearly":
                    $monthDuration = 12;
                    break;
                default:
                    $monthDuration = 0;
                    break;
            }

            $duration = $monthDuration + $subscription->plus_month." Bulan";

            return $duration;

        } catch(\Exception $e)
        {
            return false;
        }
    }
}

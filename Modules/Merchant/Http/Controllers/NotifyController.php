<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\NotifyEntity;
use Modules\Merchant\Entities\Toko\CategoryEntity;

class NotifyController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('notify')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'NotifyController@index')
                    ->name('merchant.notify.index');
                Route::post('/data-table', 'NotifyController@dataTable')
                    ->name('merchant.notify.datatable');
                Route::post('/delete', 'NotifyController@read')
                    ->name('merchant.notify.read');
            });
    }


    public function index()
    {
        $params=[
            'title'=>'Pesan Masuk',
            'tableColumns'=>NotifyEntity::dataTableColumns()

        ];

        return view('merchant::notify.index',$params);

    }

    public function dataTable(Request $request)
    {
        return NotifyEntity::dataTable($request);
    }



    public function read(Request $request)
    {
        try{
            $id=$request->id;
            $data=NotifyEntity::find($id);
            $data->is_read=1;
            $data->save();
            return "<div class='alert alert-success' style='text-align: center'>Data berhasil dibaca!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal dihapus!</div>";


        }
    }
}

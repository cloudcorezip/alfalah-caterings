<?php

namespace Modules\Merchant\Http\Controllers;

use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantTempAmount;
use App\Models\MasterData\MerchantMenu;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\Plugin\EnableMerchantPlugin;
use App\Models\Plugin\EnableMerchantPluginDetail;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\SaleOrder;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Modules\Merchant\Entities\MenuEntity;
use Modules\Merchant\Entities\MerchantEntity;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\TypeOfBusiness;
use App\Models\MasterData\User;
use App\Models\MasterData\MerchantServiceAvailable;
use App\Models\MasterData\Region\District;
use App\Models\MasterData\InventoryMethod;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;
use App\Models\MasterData\Plugin;
use App\Models\MasterData\VaBank;
use App\Models\SennaToko\PaymentMethod;
use App\Models\MasterData\TransactionType;
use App\Models\QRIS\QRISActivation;
use App\Models\Grab\GrabActivation;
use Modules\Merchant\Entities\Toko\FreeDeliveryEntity;
use Modules\Merchant\Entities\Toko\TimeOperationalEntity;
use Modules\Merchant\Entities\QRIS\QRISActivationEntity;
use Modules\Merchant\Entities\Setting\ShopeeEntity;
use App\Utils\Plugin\PluginUtil;
use Ramsey\Uuid\Uuid;
use Image;


class DashboardClinicController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeWeb()
    {
        Route::get('/dashboard-clinic', [static::class, 'dashboardClinic'])
            ->name('merchant.dashboard-clinic')
            ->middleware('merchant-verification');
        
        Route::post('/load-reservation', [static::class, 'loadReservation'])
            ->name('merchant.load-reservation')
            ->middleware('merchant-verification');
    }

    public function dashboardClinic(Request $request)
    {
        $merchantId=merchant_id();
        $userId = user_id();
        if(is_null($merchantId))
        {
            return view('merchant::not-found');
        }

        $startDate=Carbon::now()->startOfMonth()->toDateString();
        $endDate=Carbon::now()->endOfMonth()->toDateString();

        $query=DB::select("
                select
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                           sso.id,
                           sso.code,
                           coalesce(c.name,'Pelanggan Umum') as customer_name,
                           ar.due_date as ar_due_date
                        FROM
                          sc_sale_orders sso
                          left join acc_merchant_ar ar  on ar.arable_id = sso.id
                          left join sc_customers as c on c.id=sso.sc_customer_id
                        WHERE
                          sso.md_merchant_id=$merchantId
                          and sso.is_deleted=0
                          and sso.is_editable=1
                          and sso.is_debet=1
                          and ar.is_paid_off=0
                          and ar.is_deleted=0
                          and ar.is_editable=1
                          and ar.arable_type='App\Models\SennaToko\SaleOrder'
                          order by ar.due_date asc
                          limit 5
                      ) jd
                  ) AS piutang,
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          sp.id,
                          sp.code,
                          sp.name,
                          sp.stock,
                          mu.name as unit_name
                        FROM
                          sc_products sp
                          join md_users u  on u.id = sp.md_user_id
                          join md_merchants  m on m.md_user_id=u.id
                          join md_units  mu on mu.id=sp.md_unit_id
                        WHERE
                          m.id=$merchantId
                          and sp.is_deleted=0
                          and sp.is_with_stock=1
                          and sp.stock<=10
                          limit 5
                      ) jd
                  ) AS stockalert

                from
                  md_merchants m
                where
                  m.id = $merchantId
        ");

        $totalReservation = collect(DB::select("
        select
            count(*) as total
        from    
            clinic_reservations cr
        where
            cr.md_merchant_id = $merchantId
            and
            cr.is_deleted = 0
            and
            cr.reservation_date::date between '$startDate' and '$endDate'
        "))->first();
        
        $totalCustomer = collect(DB::select("
        select
            count(*) as total
        from    
            sc_customers sc
        where
            sc.md_user_id = $userId
            and
            sc.is_deleted = 0
            and
            sc.created_at::date between '$startDate' and '$endDate'
        "))->first();
        

        $params=[
            'title'=>'Ringkasan Bisnis',
            'stockAlert'=>collect(json_decode($query[0]->stockalert)),
            'piutang'=>collect(json_decode($query[0]->piutang)),
            'reservation' => $totalReservation,
            'customer' => $totalCustomer
        ];

        return view('merchant::toko.dashboard.clinic',$params);

    }

    public function loadReservation(Request $request)
    {
        try {
            $merchantId=merchant_id();
            $interval=(is_null($request->interval))?'month':$request->interval;
            
            if($interval == "month"){
                $startDate=Carbon::now()->subMonth(2)->startOfMonth()->toDateString();
                $endDate=Carbon::now()->addMonth(2)->endOfMonth()->toDateString();
                $time = "to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $series = "generate_series(
                    '$startDate' :: timestamp, '$endDate' :: timestamp,
                    interval '1 month'
                ) as mon(mon)";
                $group = "to_char(clinic_reservations.reservation_date, 'YYYY-MM')";
                $created = "to_char(clinic_reservations.reservation_date, 'YYYY-MM') as reservation_date";
                $on = "to_char(mon.mon, 'YYYY-MM')";
            } else {
                $startDate=Carbon::now()->subYear(2)->startOfYear()->toDateString();
                $endDate=Carbon::now()->addYear(2)->endOfYear()->toDateString();
                $time = "to_char(mon.mon, 'YYYY') AS time,";
                $series = "generate_series(
                    '$startDate' :: timestamp, '$endDate' :: timestamp,
                    interval '1 year'
                ) as mon(mon)";
                $group = "to_char(clinic_reservations.reservation_date, 'YYYY')";
                $created = "to_char(clinic_reservations.reservation_date, 'YYYY') as reservation_date";
                $on = "to_char(mon.mon, 'YYYY')";
            }
            
            $data = DB::select("
            select
                $time
                coalesce(
                    sum(cr.total),
                    0
                ) as total_reservation
            from
                $series
            left join (
                select
                    count(*) as total,
                    clinic_reservations.md_merchant_id,
                    clinic_reservations.is_deleted,
                    $created
                from 
                    clinic_reservations
                where
                    clinic_reservations.md_merchant_id = $merchantId
                    and
                    clinic_reservations.is_deleted = 0
                group by 
                    $group,
                    clinic_reservations.md_merchant_id,
                    clinic_reservations.is_deleted
            ) as cr on cr.reservation_date = $on
            group by
                mon.mon, cr.reservation_date
            order by
                mon.mon asc
            ");

            if(!empty($data)){
                return $this->message::getJsonResponse(200,'Data report tersedia',$data);
            }else{
                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }
        } catch(\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

}

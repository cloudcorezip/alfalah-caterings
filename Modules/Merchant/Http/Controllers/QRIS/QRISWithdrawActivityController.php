<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\QRIS;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\JurnalDetail;
use App\Models\QRIS\QRISActivation;
use App\Models\QRIS\QRISWithdrawActivity;
use App\Utils\Winpay\RajaBillerUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\Merchant\Entities\Acc\JournalEntity;
use Modules\Merchant\Entities\QRIS\QRISWithdrawEntity;
use Illuminate\Support\Facades\DB;
use App\Models\SennaToko\SaleOrder;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Models\QRIS\XenditAvailableBank;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

use Image;

class QRISWithdrawActivityController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('digital-payment/withdraw')
        ->middleware('merchant-verification')
        ->group(function (){
                Route::get('/', 'QRIS\QRISWithdrawActivityController@index')
                    ->name('qris-withdraw.index');
                Route::post('/data-table', 'QRIS\QRISWithdrawActivityController@dataTable')
                    ->name('qris-withdraw.datatable');
                Route::post('/add', 'QRIS\QRISWithdrawActivityController@add')
                    ->name('qris-withdraw.add');
            Route::post('/detail', 'QRIS\QRISWithdrawActivityController@getDetailWd')
                ->name('qris-withdraw.detail');
            Route::post('/inquiry-dana', 'QRIS\QRISWithdrawActivityController@inquiryDana')
                ->name('qris-withdraw.inquiry');
                Route::post('/reload-data', 'QRIS\QRISWithdrawActivityController@reloadData')
                    ->name('qris-withdraw.reload.data');
                Route::post('/create', 'QRIS\QRISWithdrawActivityController@save')
                    ->name('qris-withdraw.create');
                Route::post('/details', 'QRIS\QRISWithdrawActivityController@details')
                    ->name('qris-withdraw.details');
                Route::post('/delete', 'QRIS\QRISWithdrawActivityController@delete')
                    ->name('qris-withdraw.delete');
                Route::post('/load-qris', 'QRIS\QRISWithdrawActivityController@loadQrisWithdraw')
                    ->name('qris-withdraw.load');
            });
    }

    public function loadQrisWithdraw()
    {
        $merchantId=merchant_id();
        DB::statement("refresh materialized view mv_wd_table");
        $saldoIn=DB::select("select coalesce(sum(jumlah),0) as saldo_masuk
from mv_wd_table where md_merchant_id=$merchantId and status_transaksi in ('SUCCESS','success')  and tipe_transaksi='in'")[0]->saldo_masuk;
        $saldoPending=DB::select("select coalesce(sum(jumlah),0) as saldo_masuk
from mv_wd_table where md_merchant_id=$merchantId and status_transaksi in ('PENDING','pending') and tipe_transaksi='in'")[0]->saldo_masuk;
        $wd=DB::select("select coalesce(sum(jumlah),0)+coalesce(sum(biaya_admin)) as saldo_masuk
from mv_wd_table where md_merchant_id=$merchantId  and tipe_transaksi='out' and status_transaksi='SUCCESS' ")[0]->saldo_masuk;


        $params=[
            'saldo_pending'=>rupiah($saldoPending),
            'saldo'=>rupiah($saldoIn-$wd),
            'jumlah_transaksi'=> DB::select("select  count(*) as jumlah from mv_wd_table where md_merchant_id=$merchantId")[0]->jumlah,
            'total_penarikan'=>rupiah($wd)

        ];

        return json_encode((object)$params);
    }

    public function index()
    {
        $activation=QRISActivation::where('md_merchant_id',merchant_id())
            ->where('is_active',1)
            ->first();
        $params=[
            'title'=>'Ringkasan Pembayaran Digital',
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'tableColumns'=>QRISWithdrawEntity::dataTableColumns(),
            'activation'=>$activation

        ];

        return view('merchant::qris-withdraw.index',$params);

    }

    public function getDetailWd(Request  $request)
    {
        $activation=QRISActivation::where('md_merchant_id',merchant_id())->first();
        if($request->tipe=='in')
        {
            $wd=SaleOrder::where('code',$request->code)->first();


        }else{
            $wd=QRISWithdrawActivity::where('unique_code',$request->code)->first();

        }
        $params=[
            'title'=>'Detail Penarikan',
            'activation'=>$activation,
            'wd'=>$wd,
            'tipe'=>$request->tipe

        ];


        return view('merchant::qris-withdraw.detail-wd',$params);


    }

    public function dataTable(Request $request)
    {
        return QRISWithdrawEntity::dataTable($request);
    }
    public  function details(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=QRISWithdrawEntity::find($id);
        }else{
            $data=new QRISWithdrawEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::qris-withdraw.form',$params);
    }
    public  function add(Request $request){
        $merchantId=merchant_id();
        $saldoIn=DB::select("select coalesce(sum(jumlah),0) as saldo_masuk
from mv_wd_table where md_merchant_id=$merchantId and status_transaksi in('SUCCESS','success')  and tipe_transaksi='in'")[0]->saldo_masuk;

        $wd=DB::select("select coalesce(sum(jumlah),0)+coalesce(sum(biaya_admin)) as saldo_masuk
from mv_wd_table where md_merchant_id=$merchantId  and tipe_transaksi='out' and status_transaksi='SUCCESS' ")[0]->saldo_masuk;

        if(merchant_id()==4090){
            $subSaldo=$saldoIn-$wd;
            $adminFee=($subSaldo*0.01)+config('qris_rule.administration_wd');
            $saldo=$subSaldo-$adminFee;
        }else{
            $saldo=$saldoIn-($wd+config('qris_rule.administration_wd'));
            $adminFee=config('qris_rule.administration_wd');

        }

        if($saldoIn==0){
            $params=[
                'title'=>'Buat Penarikan',
                'data'=>QRISActivation::where('md_merchant_id',merchant_id())
                    ->first(),
                'saldo'=>0,
                'admin_fee'=>0
            ];

        }else{
            $params=[
                'title'=>'Buat Penarikan',
                'data'=>QRISActivation::where('md_merchant_id',merchant_id())
                    ->first(),
                'saldo'=>$saldo,
                'admin_fee'=>(merchant_id()==4090)?$adminFee+config('qris_rule.adminstration_wd'):config('qris_rule.adminstration_wd')
            ];

        }


        return view('merchant::qris-withdraw.form-wd',$params);
    }

    public function inquiryDana(Request  $request)
    {
        try {
            DB::beginTransaction();
            $data=new QRISWithdrawActivity();

            date_default_timezone_set($request->_timezone);
            $activation=QRISActivation::where('id',$request->id)
                ->first();
            if($request->amount<$request->admin_fee)
            {

                return response()->json([
                    'message'=>'Mohon maaf saldo kamu tidak cukup untuk membayar biaya penarikan',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);


            }

            if($request->amount>$request->saldo)
            {

                return response()->json([
                    'message'=>'Mohon maaf jumlah yang ditarik lebih besar dari saldo yang tersedia',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);

            }
            if(is_null($activation))
            {

                return response()->json([
                    'message'=>'Data aktivasi metode pembayaran tidak ditemukan',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);

            }
            $checkSaldo=RajaBillerUtil::balanceCheck();
            if($checkSaldo==false)
            {

                return response()->json([
                    'message'=>'Terjadi kesalahan saat melakukan proses pengecekan saldo,cobalah beberapa saat lagi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);


            }
            $isFail=false;
            $data->unique_code=CodeGenerator::generalCode('WD',$request->md_merchant_id);
            $data->md_user_id=user_id();
            $data->md_merchant_id=merchant_id();
            $data->qris_activation_id=$activation->id;
            $data->amount=$request->amount;
            $data->admin_fee=$request->admin_fee;
            if($checkSaldo->SALDO<($request->amount+$request->admin_fee))
            {
                $data->transaction_status='PENDING';
                $data->save();

            }else{
                $inquiry=RajaBillerUtil::inquiryTransferDana($activation->bank_account_number,$request->amount,$activation->getBank->bank_code,$activation->phone_number);
                if($inquiry==false)
                {
                    return response()->json([
                        'message'=>'Terjadi kesalahan saat melakukan proses inquiry saldo,cobalah beberapa saat lagi',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false,

                    ]);

                }
                $pay=RajaBillerUtil::pay($activation->bank_account_number,$request->amount,$activation->getBank->bank_code,$activation->phone_number,$inquiry->REF2);
                if($pay==false)
                {
                    return response()->json([
                        'message'=>'Terjadi kesalahan saat melakukan proses penarikan dana,cobalah beberapa saat lagi',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false,

                    ]);

                }
                if($pay->STATUS=='00')
                {
                    if($pay->KET=='Success')
                    {
                        $data->response_status=json_encode($inquiry);
                        $data->transaction_status='SUCCESS';
                        $data->response_create=json_encode($pay);
                        $data->save();

                        if($this->insertJurnal($data,$activation)==false)
                        {

                            return response()->json([
                                'message'=>'Terjadi kesalahan saat melakukan pencatatan jurnal  settlement dan rekonsiliasi',
                                'type'=>'warning',
                                'is_modal'=>true,
                                'redirect_url'=>'',
                                'is_with_datatable'=>false,

                            ]);


                        }

                    }else{
                        $data->response_status=json_encode($inquiry);
                        $data->transaction_status='PENDING';
                        $data->response_create=json_encode($pay);
                        $data->save();
                    }

                }else{
                    $isFail=true;
                    $data->response_status=json_encode($inquiry);
                    $data->transaction_status='FAILED';
                    $data->response_create=json_encode($pay);
                    $data->save();
                }
            }
            DB::commit();
            if($isFail==true)
            {

                return response()->json([
                    'message'=>'Terjadi kesalahan,penarikan gagal dilakukan!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>true,

                ]);

            }else{

                return response()->json([
                    'message'=>'Penarikan saldo berhasil,silahkan cek akun bank kamu',
                    'type'=>'success',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>true,

                ]);
            }


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);
        }
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (QRISWithdrawEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;



        $params = [
            'title' => 'Ringkasan Pembayaran Digital',
            'searchKey' => $searchKey,
            'tableColumns'=>QRISWithdrawEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
        ];
        return view('merchant::qris-withdraw.list',$params);

    }

    public function save(Request $request)
    {
        try{
            $data=new QRISWithdrawEntity();

            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message'=>$error,
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/QRISActivation/';

            if($request->hasFile('identity_card'))
            {
                $file=$request->file('identity_card');
                $fileIdentity= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileIdentity=$destinationPath.$fileIdentity;
                Storage::disk('s3')->put($fileIdentity, file_get_contents($file));

            }else{
                $fileIdentity=null;
            }

            if($request->hasFile('account_book'))
            {
                $file=$request->file('account_book');
                $fileAccount= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileAccount=$destinationPath.$fileAccount;
                Storage::disk('s3')->put($fileAccount, file_get_contents($file));

            }else{
                $fileAccount=null;
            }


            $data->code=merchant_id() . Uuid::uuid4()->toString();
            $data->md_merchant_id=merchant_id();
            $data->md_user_id=$request->md_user_id;
            $data->identity_card=$fileIdentity;
            $data->account_book=$fileAccount;
            $data->bank_account_name=$request->bank_account_name;
            $data->bank_account_number=$request->bank_account_number;
            $data->is_active=0;
            $data->is_agree=0;
            $data->balance_amount=0;
            $data->md_xendit_available_bank_id=$request->md_xendit_available_bank_id;
            $data->save();


            return response()->json([
                'message'=>'Data pendaftaran Pembayaran Digital berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);
        }
    }

    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            QRISWithdrawEntity::where('id',$id)->delete();


            return response()->json([
                'message'=>'Data aktivitas penarikan berhasil diahpus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }

    private function insertJurnal($data,$activation)
    {
        try{
            DB::beginTransaction();
            $jurnal=new JournalEntity();
            $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$activation->md_merchant_id);
            $jurnal->ref_code=$data->unique_code;
            $jurnal->trans_name='Penarikan Saldo '.$data->unique_code;
            $jurnal->trans_time=date('Y-m-d H:i:s');
            $jurnal->trans_note='penarikan';
            $jurnal->trans_purpose='Penarikan Saldo '.$data->unique_code;
            $jurnal->trans_amount=$data->amount+config('qris_rule.administration_wd');
            $jurnal->admin_fee=config('qris_rule.administration_wd');
            $jurnal->md_merchant_id=$activation->md_merchant_id;
            $jurnal->md_user_id_created=$activation->md_user_id;
            $jurnal->save();
            $adminFee=merchant_detail()->coa_administration_bank_id;

            $insert=[
                [
                    'acc_coa_detail_id'=>$activation->qrisdetail_qris_id,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$data->amount+config('qris_rule.administration_wd'),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'acc_jurnal_id'=>$jurnal->id
                ],
                [
                    'acc_coa_detail_id'=>$activation->qrisbank_qris_id,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$data->amount,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'acc_jurnal_id'=>$jurnal->id
                ],
                [
                    'acc_coa_detail_id'=>$adminFee,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>config('qris_rule.administration_wd'),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'acc_jurnal_id'=>$jurnal->id
                ],

            ];

            JurnalDetail::insert($insert);
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }


}

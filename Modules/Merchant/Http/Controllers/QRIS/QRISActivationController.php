<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\QRIS;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\Merchant\Entities\QRIS\QRISActivationEntity;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Models\QRIS\RajabillerBank;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

use Image;

class QRISActivationController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('digital-payment/activation')
        ->middleware('merchant-verification')
        ->group(function (){
                Route::post('/data-table', 'QRIS\QRISActivationController@dataTable')
                    ->name('qris.datatable');
                Route::get('/add', 'QRIS\QRISActivationController@add')
                    ->name('qris.add');
                Route::post('/policy', 'QRIS\QRISActivationController@policy')
                    ->name('qris.policy');
                Route::post('/reload-data', 'QRIS\QRISActivationController@reloadData')
                    ->name('qris.reload.data');
                Route::post('/create', 'QRIS\QRISActivationController@save')
                    ->name('qris.create');
                Route::get('/details/{id}', 'QRIS\QRISActivationController@details')
                    ->name('qris.details');
                Route::post('/delete', 'QRIS\QRISActivationController@delete')
                    ->name('qris.delete');
            });
    }

    public function dataTable(Request $request)
    {
        return QRISActivationEntity::dataTable($request);
    }

    public  function details($id){

        $data=QRISActivationEntity::find($id);

        if(is_null($data)){
            abort(404);
        }

        if($data->md_user_id != user_id()){
            return redirect()->route('pages.not-allowed');
        }

        $params=[
            'title'=>'Detail Pembayaran Digital',
            'data'=>$data
        ];

        return view('merchant::qris.detail',$params);
    }

    public  function add(Request $request){
        $id=$request->id;
        if(!is_null($id)){

            $data=QRISActivationEntity::find($id);
        }else{
            $data=new QRISActivationEntity();
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Update Data',
            'bank'=>RajabillerBank::all(),
            'data'=>$data
        ];

        return view('merchant::qris.add',$params);
    }

    public function policy(Request $request){
        $id=$request->id;
        if(!is_null($id)){

            $data=QRISActivationEntity::find($id);
        }else{
            $data=new QRISActivationEntity();
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Update Data',
            'bank'=>RajabillerBank::all(),
            'data'=>$data
        ];

        return view('merchant::qris.privacy-policy',$params);
    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (QRISActivationEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Pembayaran Digital',
            'searchKey' => $searchKey,
            'tableColumns'=>QRISActivationEntity::dataTableColumns(),
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date
        ];
        return view('merchant::qris.list',$params);

    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            if(!is_null($id)){

                $data=QRISActivationEntity::find($id);
            }else{
                $data=new QRISActivationEntity();
            }
            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message'=>$error,
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>true,
                ]);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/QRISActivation/';

            if($request->hasFile('identity_card'))
            {
                $file=$request->file('identity_card');
                $fileIdentity= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileIdentity=$destinationPath.$fileIdentity;
                Storage::disk('s3')->put($fileIdentity, file_get_contents($file));

            }else{
                $fileIdentity=null;
            }

            if($request->hasFile('account_book'))
            {
                $file=$request->file('account_book');
                $fileAccount= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileAccount=$destinationPath.$fileAccount;
                Storage::disk('s3')->put($fileAccount, file_get_contents($file));

            }else{
                $fileAccount=null;
            }


            $data->code=merchant_id() . Uuid::uuid4()->toString();
            $data->md_merchant_id=merchant_id();
            $data->md_user_id=$request->md_user_id;
            $data->phone_number=$request->phone_number;
            $data->identity_card=$fileIdentity;
            $data->account_book=$fileAccount;
            $data->bank_account_name=$request->bank_account_name;
            $data->bank_account_number=$request->bank_account_number;
            $data->is_active=0;
            $data->is_agree=1;
            $data->balance_amount=0;
            $data->md_rajabiller_bank_id=$request->md_rajabiller_bank_id;
            $data->save();
            DB::commit();

            return response()->json([
                'message'=>'Data pendaftaran akun Pembayaran Digital berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.profile.detail',['id'=>merchant_id()])."?page=digital-payment",
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            QRISActivationEntity::where('id',$id)->delete();

            return response()->json([
                'message'=>'Data aktivasi pendaftaran Pembayaran Digital berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantMenu;
use App\Models\MasterData\MerchantStaff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Modules\Merchant\Entities\MenuEntity;
use Modules\Merchant\Entities\Toko\StaffEntity;

class MenuController extends Controller
{
    public function routeWeb()
    {
        Route::post('/menu/save', 'MenuController@save')
            ->name('merchant.menu.save')
            ->middleware('merchant-verification');
        Route::post('/menu-staff', 'MenuController@getMenuStaff')
            ->name('merchant.menu.staff')
            ->middleware('merchant-verification');
    }

    public function save(Request $request)
    {
        try {
            DB::beginTransaction();
            $menu=removeStringNull($request->is_active);
            $staffId=$request->staff_user_id;

            if(empty($menu)){
                return response()->json([
                    'message'=>'Data menu tidak boleh kosong!',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            $parent=MerchantMenu::whereIn('id',$menu)
                ->get();

            $parentId=[];

            foreach ($parent as $item)
            {
                $parentId[]=(string)$item->getParent->id;
            }


            $check=DB::table('md_merchant_menu_staffs')
                ->where('md_merchant_id',merchant_id())
                ->where('staff_user_id',$staffId)
                ->count();
            if($check>0){
                DB::table('md_merchant_menu_staffs')
                    ->where('md_merchant_id',merchant_id())
                    ->where('staff_user_id',$staffId)
                    ->delete();
            }

            $merge=array_merge($menu,$parentId);
            $insertParent=[];
            foreach (array_unique($merge) as $k =>$item)
            {
                $insertParent[]=[
                    'md_merchant_id'=>merchant_id(),
                    'menu_merchant_id'=>$merge[$k],
                    'is_active'=>1,
                    'staff_user_id'=>$staffId
                ];
            }


            DB::table('md_merchant_menu_staffs')->insert($insertParent);
            DB::commit();
            return response()->json([
                'message'=>'Data pengaturan hak akses berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }

    }

    public function getMenuStaff(Request $request)
    {
        $staffId=$request->staff_id;

        $menus=DB::table('md_merchant_menu_staffs')
            ->select('menu_merchant_id')
            ->where('md_merchant_id',merchant_id())
            ->where('staff_user_id',$staffId)
            ->get();

        return response()->json($menus);
    }




}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Report;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Report\SaleOrder\RefundEntity;
use Modules\Merchant\Entities\Report\SaleOrder\VoidEntity;
use Modules\Merchant\Entities\Report\SaleOrder\OpenCloseCashierEntity;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class ReportSaleOrderController extends Controller
{

    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeWeb()
    {
        Route::prefix('report/sale-order')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::get('/', 'Report\ReportSaleOrderController@index')
                ->name('merchant.report.sale-order.index');
            Route::get('/category', 'Report\ReportSaleOrderController@category')
                ->name('merchant.report.sale-order.category');
            Route::get('/product', 'Report\ReportSaleOrderController@product')
                ->name('merchant.report.sale-order.product');
            Route::get('/ajax-get-product', 'Report\ReportSaleOrderController@ajaxGetProduct')
                ->name('merchant.report.sale-order.ajax-get-product');
            Route::get('/customer-sell', 'Report\ReportSaleOrderController@customerSell')
                ->name('merchant.report.sale-order.customer-sell');
            Route::get('/busiest-selling-time', 'Report\ReportSaleOrderController@busiestSellingTime')
                ->name('merchant.report.sale-order.busiest-selling-time');
            Route::get('/busiest-product-time', 'Report\ReportSaleOrderController@busiestProductTime')
                ->name('merchant.report.sale-order.busiest-product-time');
            Route::get('/pay-type', 'Report\ReportSaleOrderController@payType')
                ->name('merchant.report.sale-order.pay-type');
            Route::get('/tax', 'Report\ReportSaleOrderController@tax')
                ->name('merchant.report.sale-order.tax');
            Route::get('/promo', 'Report\ReportSaleOrderController@promo')
                ->name('merchant.report.sale-order.promo');
            Route::get('/retur', 'Report\ReportSaleOrderController@retur')
                ->name('merchant.report.sale-order.retur');

            Route::post('/retur-report-detail', 'Report\ReportSaleOrderController@returReportDetail')
                ->name('merchant.report.sale-order.retur-report-detail');


            Route::get('/refund', 'Report\ReportSaleOrderController@refund')
                ->name('merchant.report.sale-order.refund');
            Route::post('/data-table-refund', 'Report\ReportSaleOrderController@dataTableRefund')
                ->name('merchant.report.sale-order.refund.datatable');
            Route::get('/void', 'Report\ReportSaleOrderController@void')
                ->name('merchant.report.sale-order.void');
            Route::post('/data-table-void', 'Report\ReportSaleOrderController@dataTableVoid')
                ->name('merchant.report.sale-order.void.datatable');


            Route::post('/sum-all-void', 'Report\ReportSaleOrderController@sumAllVoid')
                ->name('merchant.report.sale-order.void.sum');


            Route::post('/sum-all-refund', 'Report\ReportSaleOrderController@sumAllRefund')
                ->name('merchant.report.sale-order.refund.sum');


            Route::get('/close-cashier', 'Report\ReportSaleOrderController@closeCashier')
                ->name('merchant.report.sale-order.close-cashier');
            Route::post('/data-table-close-cashier', 'Report\ReportSaleOrderController@dataTableCloseCashier')
                ->name('merchant.report.sale-order.close-cashier.datatable');
            Route::post('/daily-sell', 'Report\ReportSaleOrderController@penjualanHarianV2')
                ->name('merchant.report.sale-order.daily-sell');
            Route::post('/category-sell', 'Report\ReportSaleOrderController@penjualanKategoriV2')
                ->name('merchant.report.sale-order.category-sell');
            Route::post('/product-sell', 'Report\ReportSaleOrderController@penjualanProdukV2')
                ->name('merchant.report.sale-order.product-sell');
            Route::post('/cust-sell', 'Report\ReportSaleOrderController@penjualanPelanggan')
                ->name('merchant.report.sale-order.cust-sell');





            Route::post('/refund-or-void', 'Report\ReportSaleOrderController@refundOrVoidTrx')
                ->name('merchant.report.sale-order.refund-or-void');
            Route::post('/time-for-sale-or-product', 'Report\ReportSaleOrderController@waktuTeramaiPenjualanOrProductV2')
                ->name('merchant.report.sale-order.time-for-sale-or-product');
            Route::post('/type-buyer', 'Report\ReportSaleOrderController@jenisBayarV2')
                ->name('merchant.report.sale-order.type-buyer');
            Route::post('/piutang-report', 'Report\ReportSaleOrderController@piutangReport')
                ->name('merchant.report.sale-order.piutang-report');
            Route::post('/pajak-report', 'Report\ReportSaleOrderController@pajakReport')
                ->name('merchant.report.sale-order.pajak-report');
            Route::post('/promo-report', 'Report\ReportSaleOrderController@promoReportV2')
                ->name('merchant.report.sale-order.promoReport');
            Route::post('/retur-report', 'Report\ReportSaleOrderController@returReportV2')
                ->name('merchant.report.sale-order.returReport');

            //export
            Route::post('/export-daily-sell', 'Report\ReportSaleOrderController@exportDailySell')
                ->name('merchant.report.sale-order.export-daily-sell');
            Route::post('/export-product', 'Report\ReportSaleOrderController@exportProduct')
                ->name('merchant.report.sale-order.export-product');
            Route::post('/export-category', 'Report\ReportSaleOrderController@exportCategory')
                ->name('merchant.report.sale-order.export-category');
            Route::post('/export-customer-sell', 'Report\ReportSaleOrderController@exportCustomerSell')
                ->name('merchant.report.sale-order.export-customer-sell');
            Route::post('/export-busiest-selling-or-product', 'Report\ReportSaleOrderController@exportBusiestSellingOrProduct')
                ->name('merchant.report.sale-order.export-busiest-selling-or-product');
            Route::post('/export-tax', 'Report\ReportSaleOrderController@exportTax')
                ->name('merchant.report.sale-order.export-tax');
            Route::post('/export-void', 'Report\ReportSaleOrderController@exportVoid')
                ->name('merchant.report.sale-order.export-void');
            Route::post('/export-refund', 'Report\ReportSaleOrderController@exportRefund')
                ->name('merchant.report.sale-order.export-refund');
            Route::post('/export-type-buyer', 'Report\ReportSaleOrderController@exportTypeBuyer')
                ->name('merchant.report.sale-order.export-type-buyer');
            Route::post('/export-retur', 'Report\ReportSaleOrderController@exportRetur')
                ->name('merchant.report.sale-order.export-retur');
            Route::post('/export-close-cashier', 'Report\ReportSaleOrderController@exportCloseCashier')
                ->name('merchant.report.sale-order.export-close-cashier');
            Route::post('/export-promo', 'Report\ReportSaleOrderController@exportPromo')
                ->name('merchant.report.sale-order.export-promo');

        });
    }

    public function index(){

        $merchantId=merchant_id();
        $startDate=Carbon::now()->startOfMonth()->toDateString();
        $endDate=Carbon::now()->endOfMonth()->toDateString();
        $branch = get_cabang();

        $params = [
            "title" => "Laporan Penjualan Harian",
            'merchantId'=>$merchantId,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "branch" => $branch
        ];
        return view('merchant::report.sale-order.daily-sale.index', $params);
    }

    public function penjualanHarianV2(Request $request)
    {
        try{
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $interval=(is_null($request->interval))?'month':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            if($endDate>date('Y-m-d'))
            {
                $endDate=date('Y-m-d');
            }

            if($interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM-DD') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM-DD')";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $series="generate_series(
              '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                interval '1 day'
              ) as mon(mon)";


            }elseif($interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM')";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $series="generate_series(
              '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                interval '1 month'
              ) as mon(mon)";


            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY') as created_at";
                $group="to_char(sso.created_at, 'YYYY')";
                $on="to_char(mon.mon, 'YYYY')";
                $series="generate_series(
              '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                interval '1 year'
              ) as mon(mon)";
            }


            $data=DB::select("
            select
              m.id,
              m.name,
            coalesce(
            (
              select
                jsonb_agg(jd.*) AS jsonb_agg
              from
                (
                select
                 $time
                  coalesce(
                    sum(sso1.trans_count),
                    0
                  ) as trans_count,
                  coalesce(
                    sum(round(sso1.trans_amount)),
                    0
                  ) as trans_amount,
                  coalesce(
                    sum(sso1.qty),
                    0
                  ) as product_qty
                from
                  $series
                  left join (
                    select
                      count(*) as trans_count,
                      sum(sso.total) as trans_amount,
                      coalesce(
                        sum(d.quantity),
                        0
                      ) as qty,
                  $created
                    from
                      sc_sale_orders sso
                      join(
                        select
                          coalesce(
                            sum(ssod.quantity*ssod.value_conversation),
                            0
                          ) as quantity,
                          ssod.sc_sale_order_id
                        from
                          sc_sale_order_details ssod
                        where
                          ssod.is_bonus = 0
                          and ssod.is_deleted = 0
                        group by
                          ssod.sc_sale_order_id
                      ) as d on d.sc_sale_order_id = sso.id
                    where
                      sso.md_merchant_id = m.id
                      and sso.is_deleted = 0
                      and sso.is_editable = 1
                      and sso.is_keep_transaction = 0
                      and sso.step_type in (0)
                      and sso.md_sc_transaction_status_id not in(3, 5)
                    group by
                      $group
                  ) as sso1 on sso1.created_at = $on
                group by
                  mon.mon
                order by
                  mon.mon asc
                ) jd
                    ),
                    '[]'
              ) trans_details
            from
              md_merchants m
            where
              m.id in($merchantId)
            ");

            if(!empty($data)){
                $time=[];
                foreach ($data as $j => $item)
                {
                    if($j==0)
                    {
                        $transDetails=json_decode($item->trans_details);
                        foreach ($transDetails as $d)
                        {
                            $time[]=$d->time;
                        }
                    }
                }
                $result=[];
                foreach ($time as $t =>$tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if($time[$t]==$d->time)
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'name'=>$i->name,
                                    'product_qty'=>$d->product_qty,
                                    'trans_count'=>$d->trans_count,
                                    'trans_amount'=>$d->trans_amount,
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'time'=>$time[$t],
                        'branch'=>$branch
                    ];
                }
                $params=[
                    'table'=>$result,
                    'chart'=>$data
                ];
                return $this->message::getJsonResponse(200,'Data report tersedia',$params);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'),[]);

        }
    }

    public function exportDailySell(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $responseJson = $this->penjualanHarianV2($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;
            $view="";
            switch($request->interval){
                case "day":
                    $view = "Hari";
                    break;
                case "month":
                    $view = "Bulan";
                    break;
                case "year":
                    $view = "Tahun";
                    break;
            };

            $totalSale=0;
            $totalTrx=0;
            $totalProduct=0;
            foreach ($data->table as $t =>$tt)
            {
                $branch=collect($tt->branch);
                $totalSale+=$branch->sum('trans_amount');
                $totalTrx+=$branch->sum('trans_count');
                $totalProduct+=$branch->sum('product_qty');

            }


            foreach($objPHPExcel->getActiveSheet()->getMergeCells() as $cells){
                $objPHPExcel->getActiveSheet()->unmergeCells($cells);
            }

            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Penjualan Harian")->getStyle('A1')->getFont()->setSize(18);

            $objPHPExcel->getActiveSheet()->setCellValue('A3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('B3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('A4',"Tampilan Periode");
            $objPHPExcel->getActiveSheet()->setCellValue('B4', $view);

            $objPHPExcel->getActiveSheet()->setCellValue('G3',"Total Penjualan");
            $objPHPExcel->getActiveSheet()->setCellValue('H3', rupiah($totalSale));

            $objPHPExcel->getActiveSheet()->setCellValue('G4',"Total Transaksi");
            $objPHPExcel->getActiveSheet()->setCellValue('H4', $totalTrx);

            $objPHPExcel->getActiveSheet()->setCellValue('G5',"Total Produk Terjual");
            $objPHPExcel->getActiveSheet()->setCellValue('H5', $totalProduct);


            $header = [
                'Waktu',
                'Penjualan',
                'Jumlah Transaksi',
                'Jumlah Produk Terjual',

            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '8', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->getFont()->setBold( true );
                $col++;
            }

            $firstRow = "A1:".$objPHPExcel->getActiveSheet()->getHighestColumn()."1";
            $objPHPExcel->getActiveSheet()->mergeCells($firstRow);

            $num = 8;

            foreach ($data->table as $key => $item) {
                $dataShow = [
                    $item->time,
                    '',
                    '',
                    '',
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                    $col++;
                }
                $num++;
                foreach ($item->branch as $c =>$cc)
                {
                    $dataShow2 = [
                        $cc->name,
                        rupiah($cc->trans_amount),
                        $cc->trans_count,
                        $cc->product_qty,
                    ];

                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                        $col++;
                    }
                    $num++;
                }
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-penjualan-harian_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                if(document.querySelector('#successExport')){
                    toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }

    public function category()
    {

        $merchantId=merchant_id();
        $startDate=Carbon::now()->startOfMonth()->toDateString();
        $endDate=Carbon::now()->endOfMonth()->toDateString();

        $category = CategoryEntity::listOption();

        $params = [
          "title" => "Laporan Penjualan Kategori",
          'merchantId'=>$merchantId,
          "category" => $category,
          "startDate"=> $startDate,
          "endDate" => $endDate,
          "branch" => get_cabang()

        ];
        return view('merchant::report.sale-order.category.index', $params);
    }

    public function penjualanKategoriV2(Request  $request)
    {
        try{
            //Parameter
            //interval day,month
            //start_date=YYYY-MM-DD
            //end_date=YYYY-MM-DD

            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $interval=(is_null($request->interval))?'day':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            $categories=(is_null($request->categories))?"":$request->categories;

            if($endDate>date('Y-m-d'))
            {
                $endDate=date('Y-m-d');
            }

            if($interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM-DD') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM-DD')";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $interval_='1 day';
            }elseif($interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM')";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $interval_='1 month';
            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY') as created_at";
                $group="to_char(sso.created_at, 'YYYY')";
                $on="to_char(mon.mon, 'YYYY')";
                $interval_='1 year';

            }
            $data=DB::select("
            select
              mm.id,
              mm.name,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) AS jsonb_agg
                  from
                    (
                    select
                      spc.id,
                      spc.name,
                      coalesce(sell.qty, 0) as qty,
                      coalesce(round(sell.price), 0) as price,
                      coalesce(
                        (
                          SELECT
                            jsonb_agg(jd.*) AS jsonb_agg
                          FROM
                            (
                              select
                                $time
                                coalesce(
                                  sum(p.qty),
                                  0
                                ) as quantity
                              from
                                generate_series(
                                  '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                                  interval '".$interval_."'
                                ) as mon(mon)
                                left join(
                                  select
                                  $created,
                                    coalesce(
                                      sum(ssod.quantity*ssod.value_conversation),
                                      0
                                    ) qty
                                  from
                                    sc_sale_orders sso
                                    join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                                    join sc_products sp on sp.id = ssod.sc_product_id
                                  where
                                    sso.is_deleted = 0
                                    and sp.is_deleted = 0
                                    and sso.is_editable = 1
                                    and sso.md_merchant_id = mm.id
                                    and sso.is_keep_transaction = 0
                                    and sso.md_sc_transaction_status_id not in(3, 5)
                                    and ssod.is_deleted = 0
                                    and ssod.is_bonus = 0
                                    and sso.step_type in (0)
                                    and sp.sc_product_category_id = spc.id
                                  group by
                                  $group
                                ) as p on p.created_at = $on
                              group by
                                mon.mon
                              order by
                                mon.mon asc
                            ) jd
                        ),
                        '[]'
                      ) AS get_detail
                    from
                      sc_product_categories spc
                      join md_merchants m on spc.md_user_id = m.md_user_id
                      left join(
                        select
                          sp.sc_product_category_id,
                          coalesce(
                            sum(ssod.quantity*ssod.value_conversation),
                            0
                          ) qty,
                          coalesce(
                            sum(ssod.quantity * ssod.price),
                            0
                          ) price
                        from
                          sc_sale_orders sso
                          join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                          join sc_products sp on sp.id = ssod.sc_product_id
                        where
                          sso.is_deleted = 0
                          and sp.is_deleted = 0
                          and sso.is_editable = 1
                          and sso.is_keep_transaction = 0
                          and sso.md_merchant_id = mm.id
                          and sso.md_sc_transaction_status_id not in(3, 5)
                          and ssod.is_deleted = 0
                          and ssod.is_bonus = 0
                          and sso.step_type in (0)
                          and sso.created_at :: date between '".$startDate."'
                          and '".$endDate."'
                        group by
                          sp.sc_product_category_id
                      ) as sell on sell.sc_product_category_id = spc.id
                    where
                      m.id = mm.id
                      and spc.is_deleted = 0
                      $categories
                    group by
                      spc.id,
                      sell.qty,
                      sell.price
                    order by
                      coalesce(sell.qty, 0) desc
                       ) jd
                    ),
                    '[]'
                  ) trans_details
                from
                  md_merchants mm
                where
                  mm.id in($merchantId)
              ");


            if(!empty($data)){

                $categories=[];
                foreach ($data as $j => $item)
                {
                    if($j==0)
                    {
                        $transDetails=json_decode($item->trans_details);
                        foreach ($transDetails as $d)
                        {
                            $categories[]=$d->name;
                        }
                    }
                }

                $result=[];
                foreach ($categories as $t =>$tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if(strtolower($categories[$t])==strtolower($d->name))
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'branch_name'=>$i->name,
                                    'product_category_id'=>$d->id,
                                    'product_category_name'=>$d->name,
                                    'qty'=>$d->qty,
                                    'price'=>$d->price,
                                    'get_detail'=>$d->get_detail
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'category'=>$categories[$t],
                        'branch'=>$branch
                    ];
                }

                $params=[
                    'table'=>$result
                ];



                return $this->message::getJsonResponse(200,'Data report tersedia',$params);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }


    public function exportCategory(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJson = $this->penjualanKategoriV2($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;

            $totalProduct=0;
            $totalSale=0;

            foreach ($data->table as $t =>$tt)
            {
                $branch=collect($tt->branch);
                $totalProduct+=$branch->sum('qty');
                $totalSale+=$branch->sum('price');
            }

            $objPHPExcel->getActiveSheet()->setCellValue('I2',"Laporan Penjualan Kategori")->getStyle('I2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('I2:K2');
            $objPHPExcel->getActiveSheet()->getStyle('B2:K2')->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold( true );

            $objPHPExcel->getActiveSheet()->setCellValue('I3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('J3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('I4',"Total Produk");
            $objPHPExcel->getActiveSheet()->setCellValue('J4', $totalProduct." item");

            $objPHPExcel->getActiveSheet()->setCellValue('I5',"Total Penjualan");
            $objPHPExcel->getActiveSheet()->setCellValue('J5', rupiah($totalSale));


            $header = [
                'Nama Kategori',
                'Produk Terjual',
                'Total Penjualan'
            ];
            $col = 'I';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '8', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->getFont()->setBold( true );
                $col++;
            }

            $num = 8;

            foreach ($data->table as $key => $item) {
                $dataShow = [
                    $item->category,
                    '',
                    ''
                ];

                $col = 'I';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;
                foreach ($item->branch as $b =>$bb){
                    $dataShow2 = [
                        $bb->branch_name,
                        $bb->qty,
                        rupiah($bb->price)
                    ];

                    $col = 'I';
                    $row = $num + 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                }
            }
            foreach(range('F','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-penjualan-kategori_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                if(document.querySelector('#successExport')){
                  toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }



    public function product()
    {
      $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
      $startDate=Carbon::now()->startOfMonth()->toDateString();
      $endDate=Carbon::now()->endOfMonth()->toDateString();

      $branch = get_cabang();
      $params = [
        "title" => "Laporan Penjualan Produk",
        'merchantId'=>$merchantId,
        "startDate" => $startDate,
        "endDate" => $endDate,
        "branch" => $branch
      ];

      return view('merchant::report.sale-order.product.index', $params);
    }

    public function ajaxGetProduct(Request  $request)
    {
        try{
            $merchantId=$request->md_merchant_id;
            $isWithStock=(is_null($request->is_with_stock))?[0,1]:[$request->is_with_stock];
            $key=$request->key;
            $categoryCode=$request->category;

            $data=Product::
            select([
                'sc_products.code',
                'sc_products.name'
            ])
                ->join('md_merchants as m','m.md_user_id','sc_products.md_user_id')
                ->where('sc_products.is_deleted',0)
                ->whereIn('sc_products.is_with_stock',$isWithStock)
                ->where('m.id',$merchantId);

            if(!is_null($categoryCode)){

                $data->whereRaw("sc_products.sc_product_category_id in $categoryCode");
            }

            if($key=='')
            {

            }else{
                $data->whereRaw("lower(sc_products.code || ' ' || sc_products.name) like '%".strtolower($key)."%'");
            }

            $result=[];
            foreach ($data->get() as $key =>$item)
            {
                if($key==0)
                {
                    array_push($result,[
                        'id'=>'-1',
                        'text'=>'Semua'
                    ]);
                }
                array_push($result,[
                    'id'=>$item->code,
                    'text'=>$item->code.' '.$item->name
                ]);
            }
            return response()->json($result);
        }catch (\Exception $e)
        {
            return response()->json([]);
        }

    }

    public function penjualanProdukV2(Request  $request)
    {
        try{
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $interval=(is_null($request->interval))?'day':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            $productCodes=(is_null($request->product_code))?"":$request->product_code;
            $isDataTable=is_null($request->is_datatable)?0:$request->is_datatable;


            if(is_null($productCodes))
            {
                $productCodes="";
            }

            if($endDate>date('Y-m-d'))
            {
                $endDate=date('Y-m-d');
            }


            if($interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM-DD') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM-DD')";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $interval_='1 day';
            }elseif($interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM')";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $interval_='1 month';
            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY') as created_at";
                $group="to_char(sso.created_at, 'YYYY')";
                $on="to_char(mon.mon, 'YYYY')";
                $interval_='1 year';
            }


            $data=collect(DB::select("
            select
              mm.id,
              mm.name,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) AS jsonb_agg
                  from
                    (
                       select
                          spc.id,
                          spc.name,
                          spc.code,
                          coalesce(mu.name,'') as unit_name,
                          coalesce(sell.qty, 0) as qty,
                          coalesce(round(sell.price), 0) as price,
                          coalesce(
                            (
                              SELECT
                                jsonb_agg(jd.*) AS jsonb_agg
                              FROM
                                (
                                  select
                                    $time
                                    coalesce(
                                      sum(p.qty),
                                      0
                                    ) as quantity
                                  from
                                    generate_series(
                                      '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                                      interval '".$interval_."'
                                    ) as mon(mon)
                                    left join(
                                      select
                                        $created,
                                        coalesce(
                                          sum(ssod.quantity*ssod.value_conversation),
                                          0
                                        ) qty
                                      from
                                        sc_sale_orders sso
                                        join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                                      where
                                        sso.is_deleted = 0
                                        and sso.is_editable = 1
                                        and sso.is_keep_transaction = 0
                                        and sso.md_merchant_id = mm.id
                                        and sso.md_sc_transaction_status_id not in(3, 5)
                                        and ssod.is_deleted = 0
                                        and ssod.is_bonus = 0
                                        and sso.step_type in (0)
                                        and ssod.sc_product_id = spc.id
                                      group by
                                        $group
                                    ) as p on p.created_at = $on
                                  group by
                                    mon.mon
                                  order by
                                    mon.mon asc
                                ) jd
                            ),
                            '[]'
                          ) AS get_detail
                        from
                          sc_products spc
                          join md_merchants m on spc.md_user_id = m.md_user_id
                          left join md_units mu on mu.id = spc.md_unit_id
                          left join(
                            select
                              ssod.sc_product_id,
                              coalesce(
                                sum(ssod.quantity*ssod.value_conversation),
                                0
                              ) qty,
                              coalesce(
                                sum(ssod.quantity * ssod.price),
                                0
                              ) price
                            from
                              sc_sale_orders sso
                              join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                            where
                              sso.is_deleted = 0
                              and sso.is_editable = 1
                              and sso.is_keep_transaction = 0
                              and sso.md_merchant_id = mm.id
                              and sso.md_sc_transaction_status_id not in(3, 5)
                              and ssod.is_deleted = 0
                              and ssod.is_bonus = 0
                              and sso.step_type in (0)
                              and sso.created_at::date >= '".$startDate."'
                              and sso.created_at::date <= '".$endDate."'
                            group by
                              ssod.sc_product_id
                          ) as sell on sell.sc_product_id = spc.id
                        where
                          m.id = mm.id
                          and spc.is_deleted = 0
                          $productCodes
                        group by
                          spc.id,
                          sell.qty,
                          sell.price,
                          mu.name
                        order by
                          coalesce(sell.qty, 0) desc
                        ) jd
                    ),
                    '[]'
                  ) trans_details
                from
                  md_merchants mm
                where
                  mm.id in($merchantId)

            "));



            if(!empty($data)){
                $products=[];
                foreach ($data as $j => $item)
                {
                    if($j==0)
                    {
                        $transDetails=json_decode($item->trans_details);
                        foreach ($transDetails as $d)
                        {
                            $products[]=[
                                'code'=>$d->code,
                                'name'=>$d->name,
                                'unit_name'=>$d->unit_name,
                            ];

                        }
                    }
                }

                $result=[];
                foreach ($products as $tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if(strtolower($tm['code'])==strtolower($d->code))
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'branch_name'=>$i->name,
                                    'product_code'=>$d->code,
                                    'product_name'=>$d->name,
                                    'unit_name'=>$d->unit_name,
                                    'qty'=>$d->qty,
                                    'price'=>$d->price,
                                    'get_detail'=>$d->get_detail
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'product'=>$tm['name'].' SKU('.$tm['code'].')',
                        'unit_name'=>$tm['unit_name'],
                        'branch'=>$branch
                    ];
                }

                if($isDataTable==0)
                {
                    $params=[
                        'table'=>$result
                    ];
                    return $this->message::getJsonResponse(200,'Data report tersedia',$params);

                }else{

                }

              }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function exportProduct(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJson = $this->penjualanProdukV2($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;


            $totalProduct=0;
            $totalSale=0;

            foreach ($data->table as $t =>$tt)
            {
                $branch=collect($tt->branch);
                $totalProduct+=$branch->sum('qty');
                $totalSale+=$branch->sum('price');
            }


            $objPHPExcel->getActiveSheet()->setCellValue('I2',"Laporan Penjualan Produk")->getStyle('I2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('I2:K2');
            $objPHPExcel->getActiveSheet()->getStyle('B2:K2')->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold( true );

            $objPHPExcel->getActiveSheet()->setCellValue('I3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('K3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('I4',"Total Produk");
            $objPHPExcel->getActiveSheet()->setCellValue('K4', $totalProduct." produk");

            $objPHPExcel->getActiveSheet()->setCellValue('I5',"Total Penjualan");
            $objPHPExcel->getActiveSheet()->setCellValue('K5', rupiah($totalSale));


            $header = [
                'Nama Produk',
                'Jumlah Produk',
                'Satuan',
                'Total Penjualan'
            ];
            $col = 'I';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '8', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->getFont()->setBold( true );
                $col++;
            }

            $num = 8;

            foreach ($data->table as $key => $item) {
                $dataShow = [
                    $item->product,
                    '',
                    $item->unit_name,
                    '',
                ];

                $col = 'I';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;

                foreach ($item->branch as $b =>$bb)
                {
                    $dataShow2 = [
                        $bb->branch_name,
                        $bb->qty,
                        '',
                        rupiah($bb->price)
                    ];

                    $col = 'I';
                    $row = $num + 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                }
            }
            foreach(range('F','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-penjualan-produk_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }


    public function customerSell()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
        $branch = get_cabang();
        $startDate=Carbon::now()->startOfMonth()->toDateString();
        $endDate=Carbon::now()->endOfMonth()->toDateString();


        $params = [
          "title" => "Laporan Penjualan Pelanggan",
          'merchantId'=>$merchantId,
            'branch'=>$branch,
            "startDate" => $startDate,
            "endDate" => $endDate,
        ];

        return view('merchant::report.sale-order.customer.index', $params);
    }

    public function penjualanPelanggan(Request  $request)
    {
        try {
            //day,month,year
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $interval=(is_null($request->interval))?'year':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            $productCodes=(is_null($request->customer_id))?"":$request->customer_id;

            if(is_null($productCodes))
            {
                $productCodes="";
            }

            if($endDate>date('Y-m-d'))
            {
                $endDate=date('Y-m-d');
            }
            if($interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM-DD') as created_at,";
                $group="to_char(sso.created_at, 'YYYY-MM-DD')";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $interval_='1 day';
            }elseif($interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM') as created_at,";
                $group="to_char(sso.created_at, 'YYYY-MM')";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $interval_='1 month';
            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY') as created_at,";
                $group="to_char(sso.created_at, 'YYYY')";
                $on="to_char(mon.mon, 'YYYY')";
                $interval_='1 year';
            }

            $data=DB::select("
             select
              mm.id,
              mm.name,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) AS jsonb_agg
                  from
                    (
                    select
                      spc.id,
                      spc.name,
                      spc.code,
                      spc.phone_number,
                      spc.email,
                      coalesce(
                        (
                          SELECT
                            jsonb_agg(jd.*) AS jsonb_agg
                          FROM
                            (
                              select
                                $time
                                coalesce(
                                  sum(p.trans_count),
                                  0
                                ) as trans_count,
                                coalesce(
                                  sum(round(p.trans_amount)),
                                  0
                                ) as trans_amount
                              from
                                generate_series(
                                  '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                                  interval '".$interval_."'
                                ) as mon(mon)
                                left join(
                                  select
                                    $created
                                    count(*) as trans_count,
                                    coalesce(
                                      sum(sso.total),
                                      0
                                    ) trans_amount
                                  from
                                    sc_sale_orders sso
                                  where
                                    sso.is_deleted = 0
                                    and sso.is_editable = 1
                                    and sso.is_keep_transaction = 0
                                    and sso.md_merchant_id = mm.id
                                    and sso.md_sc_transaction_status_id not in(3, 5)
                                    and sso.step_type in (0)
                                    and sso.sc_customer_id = spc.id
                                  group by
                                   $group
                                ) as p on p.created_at = $on
                              group by
                                mon.mon
                              order by
                                mon.mon asc
                            ) jd
                        ),
                        '[]'
                      ) AS get_detail,
                      coalesce(sell.trans_count,0) as trans_count,
                        coalesce(sell.trans_amount,0) as trans_amount
                      from
                      sc_customers spc
                      join md_merchants m on spc.md_user_id = m.md_user_id
                      join(
                        select
                          sso.sc_customer_id,
                          count(*) as trans_count,
                          coalesce(
                            sum(sso.total),
                            0
                          ) trans_amount
                        from
                          sc_sale_orders sso
                        where
                          sso.is_deleted = 0
                          and sso.is_editable = 1
                          and sso.is_keep_transaction = 0
                          and sso.md_merchant_id = mm.id
                          and sso.md_sc_transaction_status_id not in(3, 5)
                          and sso.step_type in (0)
                          and sso.created_at :: date between '".$startDate."'
                          and '".$endDate."'
                        group by
                          sso.sc_customer_id
                      ) as sell on sell.sc_customer_id = spc.id
                    where
                      m.id = mm.id
                      and spc.is_deleted = 0
                      $productCodes
                    group by
                      spc.id,
                      sell.trans_count,
                      sell.trans_amount
                    order by
                      coalesce(
                        sum(sell.trans_amount),
                        0
                      ) desc
                   ) jd
                ),
                '[]'
              ) trans_details
            from
              md_merchants mm
            where
              mm.id in($merchantId)
            ");
            if(!empty($data)){
                $products=[];
                foreach ($data as $j => $item)
                {
                    if($j==0)
                    {
                        $transDetails=json_decode($item->trans_details);
                        foreach ($transDetails as $d)
                        {
                            $products[]=[
                                'id'=>$d->id,
                                'code'=>$d->code,
                                'name'=>$d->name
                            ];

                        }
                    }
                }
                $result=[];
                foreach ($products as $tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if(strtolower($tm['id'])==strtolower($d->id))
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'branch_name'=>$i->name,
                                    'customer_code'=>$d->code,
                                    'customer_name'=>$d->name,
                                    'trans_count'=>$d->trans_count,
                                    'trans_amount'=>$d->trans_amount,
                                    'get_detail'=>$d->get_detail
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'customer'=>$tm['code'].' '. $tm['name'],
                        'branch'=>$branch
                    ];
                }

                $params=[
                    'table'=>$result
                ];
                return $this->message::getJsonResponse(200,'Data report tersedia',$params);


            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function exportCustomerSell(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJson = $this->penjualanPelanggan($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;

            $totalProduct=0;
            $totalSale=0;

            foreach ($data->table as $t =>$tt)
            {
                $branch=collect($tt->branch);
                $totalProduct+=$branch->sum('trans_count');
                $totalSale+=$branch->sum('trans_amount');
            }


            $objPHPExcel->getActiveSheet()->setCellValue('I2',"Laporan Penjualan Per Pelanggan")->getStyle('I2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('I2:K2');
            $objPHPExcel->getActiveSheet()->getStyle('B2:K2')->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold( true );

            $objPHPExcel->getActiveSheet()->setCellValue('I3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('K3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('I4',"Total Transaksi");
            $objPHPExcel->getActiveSheet()->setCellValue('K4', $totalProduct." transaksi");

            $objPHPExcel->getActiveSheet()->setCellValue('I5',"Total Pembelian");
            $objPHPExcel->getActiveSheet()->setCellValue('K5', rupiah($totalSale));


            $header = [
                'Nama Pelanggan',
                'Total Transaksi',
                'Total Pembelian'
            ];
            $col = 'I';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '8', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->getFont()->setBold( true );
                $col++;
            }

            $num = 8;

            foreach ($data->table as $key => $item) {;
                $dataShow = [
                    $item->customer,
                    '',
                    ''
                ];

                $col = 'I';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;

                foreach ($item->branch as $b =>$bb)
                {
                    $dataShow2 = [
                        $bb->branch_name,
                        $bb->trans_count,
                        rupiah($bb->trans_amount)
                    ];

                    $col = 'I';
                    $row = $num + 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                }
            }
            foreach(range('F','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-penjualan-produk_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }


    public function void(Request $request)
    {
        parent::setMenuKey($request->key);
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params = [
            "title" => "Laporan Void",
            "tableColumns" => VoidEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'startDate'=>Carbon::now()->startOfMonth()->toDateString(),
            'endDate'=>Carbon::now()->lastOfMonth()->toDateString(),
            'merchantId'=>$merchantId
        ];

        return view('merchant::report.sale-order.void.index', $params);
    }

    public function dataTableVoid(Request $request)
    {
        return VoidEntity::dataTable($request);
    }
    public function sumAllVoid(Request  $request)
    {
        return VoidEntity::sumAllVoid($request);

    }

    public function refund(Request $request)
    {
        parent::setMenuKey($request->key);
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params = [
            "title" => "Laporan Refund",
            "tableColumns" => RefundEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'startDate'=>Carbon::now()->startOfMonth()->toDateString(),
            'endDate'=>Carbon::now()->lastOfMonth()->toDateString(),
            'merchantId'=>$merchantId
        ];

        return view('merchant::report.sale-order.refund.index', $params);
    }

    public function dataTableRefund(Request $request)
    {
        return RefundEntity::dataTable($request);
    }


    public function sumAllRefund(Request  $request)
    {
        return RefundEntity::sumAllRefund($request);

    }


    public function exportVoid(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJsonTransHeader = $this->sumAllVoid($request);
            $transHeader = json_decode($responseJsonTransHeader->getContent());
            $data = VoidEntity::dataTable($request,true,true,true,1);
            $objPHPExcel->getActiveSheet()->setCellValue('F2',"Laporan Void")->getStyle('F2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('F2:J2');
            $objPHPExcel->getActiveSheet()->getStyle("F2:J2")->applyFromArray($style);

            $objPHPExcel->getActiveSheet()->setCellValue('F3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('G3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));
            $objPHPExcel->getActiveSheet()->mergeCells('G3:I3');

            $objPHPExcel->getActiveSheet()->setCellValue('F4',"Total Void");
            $objPHPExcel->getActiveSheet()->setCellValue('G4', rupiah($transHeader[0]->amount_void));

            $objPHPExcel->getActiveSheet()->setCellValue('F5',"Total Transaksi");
            $objPHPExcel->getActiveSheet()->setCellValue('G5', $transHeader[0]->total_void." Transaksi");

            $objPHPExcel->getActiveSheet()->setCellValue('F6',"Jumlah Produk");
            $objPHPExcel->getActiveSheet()->setCellValue('G6', $transHeader[0]->total_void_product." item");

            $header = [
                'Kode Transaksi',
                'Nama Pelanggan',
                'Void',
                'Jumlah Produk',
                'Waktu Transaksi',
                'Nama Pencatat',
                'Cabang'
            ];
            $col = 'F';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '7', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '7')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '7')->getFont()->setBold( true );
                $col++;
            }

            $num = 7;

            foreach ($data as $key => $item) {
                $dataShow = [
                    $item->kode_transaksi,
                    is_null($item->kode_pelanggan)?$item->nama_pelanggan:$item->kode_pelanggan.' '.$item->nama_pelanggan,
                    $item->void,
                    $item->jumlah_produk,
                    Carbon::parse($item->waktu_transaksi)->format('d F Y, H:m').' '.getTimeZoneName($item->timezone),
                    $item->nama_pencatat,
                    $item->nama_cabang
                ];

                $col = 'F';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-laporan-void_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }

    public function exportRefund(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJsonTransHeader = $this->sumAllRefund($request);
            $transHeader = json_decode($responseJsonTransHeader->getContent());
            $data = RefundEntity::dataTable($request,true,true,true,1);

            $objPHPExcel->getActiveSheet()->setCellValue('F2',"Laporan Refund")->getStyle('F2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('F2:J2');
            $objPHPExcel->getActiveSheet()->getStyle('F2:H2')->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold( true );

            $objPHPExcel->getActiveSheet()->setCellValue('F3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('G3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('F4',"Total Refund");
            $objPHPExcel->getActiveSheet()->setCellValue('G4', rupiah($transHeader[0]->amount_refund));

            $objPHPExcel->getActiveSheet()->setCellValue('F5',"Total Transaksi");
            $objPHPExcel->getActiveSheet()->setCellValue('G5', $transHeader[0]->total_refund." Transaksi");

            $objPHPExcel->getActiveSheet()->setCellValue('F6',"Jumlah Produk");
            $objPHPExcel->getActiveSheet()->setCellValue('G6', $transHeader[0]->total_refund_product." item");

            $header = [
                'Kode Transaksi',
                'Nama Pelanggan',
                'Refund',
                'Jumlah Produk',
                'Waktu Transaksi',
                'Nama Pencatat',
                'Cabang'
            ];
            $col = 'F';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '7', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '7')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '7')->getFont()->setBold( true );
                $col++;
            }

            $num = 7;

            foreach ($data as $key => $item) {
                $dataShow = [
                    $item->kode_transaksi,
                    is_null($item->kode_pelanggan)?$item->nama_pelanggan:$item->kode_pelanggan.' '.$item->nama_pelanggan,
                    $item->refund,
                    $item->jumlah_produk,
                    Carbon::parse($item->waktu_transaksi)->format('d F Y, H:m').' '.getTimeZoneName($item->timezone),
                    $item->nama_pencatat,
                    $item->nama_cabang
                ];

                $col = 'F';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('F','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-laporan-refund_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }


    public function closeCashier(Request $request)
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        parent::setMenuKey($request->key);
        $params = [
            "title" => "Laporan Tutup Kasir",
            "tableColumns" => OpenCloseCashierEntity::dataTableColumns(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'startDate'=>Carbon::now()->startOfMonth()->toDateString(),
            'endDate'=>Carbon::now()->lastOfMonth()->toDateString(),
            'merchantId'=>$merchantId
        ];

        return view('merchant::report.sale-order.close-cashier.index', $params);
    }

    public function dataTableCloseCashier(Request $request)
    {
        return OpenCloseCashierEntity::dataTable($request);
    }


    public function busiestSellingTime()
    {
      $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
      $startDate=Carbon::now()->startOfMonth()->toDateString();
      $endDate=Carbon::now()->endOfMonth()->toDateString();

      $params = [
        "title" => "Waktu Teramai Penjualan",
        'merchantId'=>$merchantId,
        "startDate" => $startDate,
        "endDate" => $endDate,
        "branch" => get_cabang()
      ];

      return view('merchant::report.sale-order.busiest-selling-time.index', $params);
    }

    public function busiestProductTime()
    {
      $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
      $startDate=Carbon::now()->startOfMonth()->toDateString();
      $endDate=Carbon::now()->endOfMonth()->toDateString();
      $params = [
        "title" => "Waktu Teramai Produk",
        'merchantId'=>$merchantId,
        "startDate" => $startDate,
        "endDate" => $endDate,
        "branch" => get_cabang()
      ];

      return view('merchant::report.sale-order.busiest-product-time.index', $params);
    }



    public function waktuTeramaiPenjualanOrProductV2(Request  $request)
    {
        try {
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $type=(is_null($request->type))?"sell":$request->type;
            $interval=(is_null($request->interval))?'hour':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            if($endDate>date('Y-m-d')){
                $endDate=date('Y-m-d');
            }
            if($interval=='hour')
            {
                $series="with hours as (
                   select generate_series(
                  date_trunc('hour', '2021-09-01 00:00:00'::timestamp),
                    date_trunc('hour', '2021-09-01 23:00:00'::timestamp),
                  '1 hour'::interval
                ) as hour
                )";
                $time="to_char(hours.hour,'HH24:MI') as time,";
                $created="to_char(sso.created_at,'HH24:00') as created_at";
                $group="to_char(sso.created_at,'HH24:00')";
                $on="to_char(hours.hour,'HH24:MI')";
                $from="hours";

            }elseif ($interval=='day_name')
            {
                $series="with days_name as (
                  select
                    generate_series(
                      '2021-08-02' :: timestamp, '2021-08-08' :: timestamp,
                      interval '1 day'
                    ) as day_name
                ) ";
                $time="to_char(days_name.day_name,'Day') as time,";
                $created="to_char(sso.created_at,'Day') as created_at";
                $group="to_char(sso.created_at,'Day')";
                $on="to_char(days_name.day_name,'Day')";
                $from="days_name";

            }elseif ($interval=='day')
            {
                $series="
                 with dates_name as (
                  select
                    generate_series(
                      '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                      interval '1 day'
                    ) as date_name
                )
                ";
                $time="to_char(dates_name.date_name, 'DD') || ' ' || to_char(dates_name.date_name,'Mon') as time,";
                $created="to_char(sso.created_at, 'YYYY-MM-DD') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM-DD')";
                $on="to_char(dates_name.date_name, 'YYYY-MM-DD')";
                $from="dates_name";
            }
            else{
                $series=" with months_name as (
                  select
                    generate_series(
                      '2021-01-01' :: timestamp, '2021-12-31' :: timestamp,
                      interval '1 month'
                    ) as month_name
                ) ";
                $time="to_char(months_name.month_name,'Month') as time,";
                $created="to_char(sso.created_at, 'Month') as created_at";
                $group="to_char(sso.created_at, 'Month')";
                $on="to_char(months_name.month_name, 'Month')";
                $from="months_name";
            }

            if($type=="sell")
            {
                $sell="sum(sso.total) as trans_amount";
            }else{
                $sell="coalesce(sum(d.price),0) as trans_amount";

            }

            $data=DB::select("
            select
              m.id,
              m.name,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) as jsonb_agg
                  from
                    (
                      $series
                      select
                        $time
                        coalesce(s.trans_count, 0) as trans_count,
                        coalesce(s.trans_amount, 0) as trans_amount,
                        coalesce(s.qty, 0) as product_qty
                      from
                      $from
                        left join(
                        select
                          count(*) as trans_count,
                          $sell,
                          coalesce(
                          sum(d.quantity),
                          0
                          ) as qty,
                        $created
                        from
                          sc_sale_orders sso
                          join(
                          select
                            coalesce(
                            sum(ssod.quantity*ssod.value_conversation),
                            0
                            ) as quantity,
                            coalesce(
                            sum(ssod.quantity*ssod.price),
                            0
                            ) as price,
                            ssod.sc_sale_order_id
                          from
                            sc_sale_order_details ssod
                          where
                            ssod.is_bonus = 0
                            and ssod.is_deleted = 0
                          group by
                            ssod.sc_sale_order_id
                          ) as d on d.sc_sale_order_id = sso.id
                        where
                          sso.is_approved_shop = 1
                          and sso.is_editable = 1
                          and sso.md_sc_transaction_status_id not in(3, 5)
                          and sso.is_deleted = 0
                          and sso.is_keep_transaction = 0
                          and sso.step_type in (0)
                          and sso.is_cancel_user = 0
                          and sso.md_merchant_id = m.id
                          and sso.created_at :: date between '".$startDate."' and '".$endDate."'
                        group by
                          $group
                        ) as s on s.created_at = $on
                    ) jd
                ), '[]'
              ) as trans_details
            from
              md_merchants m
            where
              m.id in($merchantId)
            ");

            $resultData=[];

            if(!empty($data)){
                $time=[];
                foreach ($data as $j => $item)
                {
                    $trans=[];
                    $transDetails=json_decode($item->trans_details);

                    if($j==0)
                    {
                        foreach ($transDetails as $d)
                        {
                            $time[]=$d->time;
                        }

                    }

                    foreach ($transDetails as $dd)
                    {
                        $trans[]=[
                            'time'=>($interval=='day')?$dd->time:dayOrMonthLocale(str_replace(" ",'',$dd->time)),
                            'product_qty'=>$dd->product_qty,
                            'trans_count'=>$dd->trans_count,
                            'trans_amount'=>$dd->trans_amount
                        ];
                    }

                    $resultData[]=[
                        'id'=>$item->id,
                        'name'=>$item->name,
                        'trans_details'=>json_encode($trans)
                    ];
                }
                $result=[];
                foreach ($time as $t =>$tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if($time[$t]==$d->time)
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'name'=>$i->name,
                                    'product_qty'=>$d->product_qty,
                                    'trans_count'=>$d->trans_count,
                                    'trans_amount'=>$d->trans_amount
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'time'=>($interval=='day')?$time[$t]:dayOrMonthLocale(str_replace(" ",'',$time[$t])),
                        'branch'=>$branch
                    ];
                }

                $params=[
                    'table'=>$result,
                    'chart'=>$resultData
                ];
                return $this->message::getJsonResponse(200,'Data report tersedia',$params);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }

    }


    public function exportBusiestSellingOrProduct(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $responseJson = $this->waktuTeramaiPenjualanOrProductV2($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;

            $view;
            $title;

            switch($request->interval){
                case "hour":
                    $view = "Jam";
                    break;
                case "day_name":
                    $view = "Hari";
                    break;
                case "day":
                    $view = "Tanggal";
                    break;
                case "month":
                    $view = "Bulan";
                    break;
            };

            switch($request->type){
                case "sell":
                    $title = "Laporan Waktu Teramai Penjualan";
                    break;
                case "product":
                    $title = "Laporan Waktu Teramai Produk";
                    break;
            }

            $objPHPExcel->getActiveSheet()->setCellValue('F2',$title)->getStyle('F2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('F2:I2');
            $objPHPExcel->getActiveSheet()->getStyle('F2:H2')->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold( true );


            $objPHPExcel->getActiveSheet()->setCellValue('F3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('G3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('F4',"Tampilan Periode");
            $objPHPExcel->getActiveSheet()->setCellValue('G4', $view);

            $objPHPExcel->getActiveSheet()->setCellValue('F5',"Total Transaksi");
            $objPHPExcel->getActiveSheet()->setCellValue('G5', $request->total_trans." Transaksi");

            $objPHPExcel->getActiveSheet()->setCellValue('F6',"Total Penjualan");
            $objPHPExcel->getActiveSheet()->setCellValue('G6',$request->total_sell);

            $objPHPExcel->getActiveSheet()->setCellValue('F7',"Total Produk");
            $objPHPExcel->getActiveSheet()->setCellValue('G7', $request->total_product." produk");


            $header = [
                'Waktu',
                'Jumlah Produk',
                'Transaksi',
                'Penjualan',
            ];
            $col = 'F';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '9', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '9')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '9')->getFont()->setBold( true );
                $col++;
            }

            $num = 9;

            foreach ($data->table as $key => $item) {
                $dataShow = [
                    ($request->interval=='day')?$item->time:dayOrMonthLocale($item->time),
                   '',
                   '',
                   ''
                ];

                $col = 'F';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;

                foreach ($item->branch as $b =>$bb)
                {
                    $dataShow2 = [
                        $bb->name,
                        $bb->product_qty,
                        $bb->trans_count,
                        $bb->trans_amount
                    ];

                    $col = 'F';
                    $row = $num + 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                }
            }
            foreach(range('F','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            if($request->type == "sell"){
                $filename = strtolower('export-waktu-teramai-penjualan_'.$view.'_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            } else {
                $filename = strtolower('export-waktu-teramai-product_'.$view.'_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            }

            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }

    public function tax()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
        $startDate=Carbon::now()->startOfMonth()->toDateString();
        $endDate=Carbon::now()->endOfMonth()->toDateString();
        $params = [
            "title" => "Laporan PPN ( Pajak Pertambahan Nilai )",
            'merchantId'=>$merchantId,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "branch" => get_cabang()
        ];
        return view('merchant::report.sale-order.tax.index', $params);
    }

    public function pajakReport(Request  $request)
    {
        try {
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $interval=(is_null($request->interval))?'day':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;

            if($endDate>date('Y-m-d')){
                $endDate=date('Y-m-d');
            }

            if($interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM-DD') as created_at,";
                $group="to_char(sso.created_at, 'YYYY-MM-DD')";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $interval_='1 day';
            }elseif($interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM') as created_at,";
                $group="to_char(sso.created_at, 'YYYY-MM')";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $interval_='1 month';
            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY') as created_at,";
                $group="to_char(sso.created_at, 'YYYY')";
                $on="to_char(mon.mon, 'YYYY')";
                $interval_='1 year';
            }

            $data=DB::select("
            select
              m.id,
              m.name,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) as jsonb_agg
                  from
                    (
                        select
                      $time
                       coalesce (sum(p.trans_count),
                        0
                      ) as trans_count,
                      coalesce(
                        sum(p.trans_amount),
                        0
                      ) as trans_amount,
                      coalesce(
                        sum(p.tax_amount),
                        0
                      ) as tax_amount
                    from
                      generate_series(
                        '" . $startDate . "' :: timestamp, '" . $endDate . "' :: timestamp,
                        interval '" . $interval_ . "'
                      ) as mon(mon)
                      left join(
                        select
                        $created
                          count(*) as trans_count,
                          coalesce(
                            sum(sso.total),
                            0
                          ) trans_amount,
                          coalesce(
                            sum(sso.tax),
                            0
                          ) tax_amount
                        from
                          sc_sale_orders sso
                        where
                          sso.is_deleted = 0
                          and sso.is_editable = 1
                          and sso.step_type in (0)
                          and sso.md_merchant_id = m.id
                          and sso.md_sc_transaction_status_id not in(3, 5)
                        group by
                          $group
                      ) as p on p.created_at = $on
                    group by
                      mon.mon
                    order by
                      mon.mon asc
                 ) jd
                ),'[]'
                ) trans_details
                from
                  md_merchants m
                where
                  m.id in($merchantId)
        ");
            $resultData=[];

            if(!empty($data)){
                $time=[];
                foreach ($data as $j => $item)
                {
                    $trans=[];
                    $transDetails=json_decode($item->trans_details);

                    if($j==0)
                    {
                        foreach ($transDetails as $d)
                        {
                            $time[]=$d->time;
                        }

                    }

                    foreach ($transDetails as $dd)
                    {
                        $trans[]=[
                            'time'=>($interval=='day')?$dd->time:dayOrMonthLocale(str_replace(" ",'',$dd->time)),
                            'tax_amount'=>$dd->tax_amount,
                            'trans_count'=>$dd->trans_count,
                            'trans_amount'=>$dd->trans_amount
                        ];
                    }

                    $resultData[]=[
                        'id'=>$item->id,
                        'name'=>$item->name,
                        'trans_details'=>json_encode($trans)
                    ];
                }
                $result=[];
                foreach ($time as $t =>$tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if($time[$t]==$d->time)
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'name'=>$i->name,
                                    'tax_amount'=>$d->tax_amount,
                                    'trans_count'=>$d->trans_count,
                                    'trans_amount'=>$d->trans_amount
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'time'=>($interval=='day')?$time[$t]:dayOrMonthLocale(str_replace(" ",'',$time[$t])),
                        'branch'=>$branch
                    ];
                }

                $params=[
                    'table'=>$result,
                    'chart'=>$resultData
                ];
                return $this->message::getJsonResponse(200,'Data report tersedia',$params);
            }else{
                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function exportTax(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJson = $this->pajakReport($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;

            $view;

            switch($request->interval){
                case "day":
                    $view = "Hari";
                    break;
                case "month":
                    $view = "Bulan";
                    break;
                case "year":
                    $view = "Tahun";
                    break;
            };

            $objPHPExcel->getActiveSheet()->setCellValue('H1',"Laporan Pajak Penjualan")->getStyle('H1')->getFont()->setSize(18)->setBold(true);
            $objPHPExcel->getActiveSheet()->mergeCells('H1:K1');
            $objPHPExcel->getActiveSheet()->getStyle("H1:K1")->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold( true );

            $objPHPExcel->getActiveSheet()->setCellValue('H3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('I3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('H4',"Tampilan Periode");
            $objPHPExcel->getActiveSheet()->setCellValue('I4', $view);

            $objPHPExcel->getActiveSheet()->setCellValue('H5',"Total Transaksi");
            $objPHPExcel->getActiveSheet()->setCellValue('I5', $request->total_trans." Transaksi");

            $objPHPExcel->getActiveSheet()->setCellValue('H6',"Total Penjualan");
            $objPHPExcel->getActiveSheet()->setCellValue('I6', $request->total_sell);

            $objPHPExcel->getActiveSheet()->setCellValue('H7',"Total Pajak");
            $objPHPExcel->getActiveSheet()->setCellValue('I7', $request->total_tax);


            $header = [
                'Waktu',
                'Total Transaksi',
                'Total Penjualan',
                'Pajak Penjualan'
            ];
            $col = 'H';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '9', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '9')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '9')->getFont()->setBold( true );
                $col++;
            }

            $num = 9;

            foreach ($data->table as $key => $item) {


                $dataShow = [
                    $item->time,
                    '',
                    '',
                    ''
                ];

                $col = 'H';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;

                foreach ($item->branch as $b =>$bb)
                {
                    $dataShow = [
                        $bb->name,
                        $bb->trans_count,
                        rupiah($bb->trans_amount),
                        rupiah($bb->tax_amount)
                    ];

                    $col = 'H';
                    $row = $num + 1;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                }
            }
            foreach(range('H','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-laporan-pajax_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }




    public function payType()
    {
        $totalData = DB::table('sc_customers')
          ->selectRaw('COUNT(*) AS result')
          ->where('md_user_id', user_id())
          ->where('is_deleted', 0)
          ->first();
          $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $params = [
          "title" => "Laporan Jenis Bayar",
          "totalData" => $totalData,
          'merchantId'=>$merchantId
        ];

        return view('merchant::report.sale-order.pay-type.index', $params);
    }

    public function retur()
    {
      $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
      $branch = get_cabang();
      $startDate=Carbon::now()->startOfMonth()->toDateString();
      $endDate=Carbon::now()->endOfMonth()->toDateString();

      $params = [
        "title" => "Laporan Retur Penjualan",
        'merchantId'=>$merchantId,
        "branch" => $branch,
        "startDate" => $startDate,
        "endDate" => $endDate
      ];

      return view('merchant::report.sale-order.retur.index', $params);
    }

    public function returReportV2(Request $request)
    {
        try {
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $interval=(is_null($request->interval))?'month':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            if($endDate>date('Y-m-d')){

                $endDate=date('Y-m-d');
            }

            $reasonId=(is_null($request->reason_id))?'-1':$request->reason_id;
            if($reasonId=='-1'){

                $queryReason='and srso.reason_id in(1,2,3)';
            }elseif($reasonId=='1' || $reasonId==1){
                $queryReason='and srso.reason_id=1';
            }else{
                $queryReason='and srso.reason_id!=1';
            }
            if($interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $series="generate_series(
          '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
            interval '1 day'
          ) as mon(mon)";
                $createdRetur="to_char(srso.created_at, 'YYYY-MM-DD') as created_at";
                $groupRetur="to_char(srso.created_at, 'YYYY-MM-DD')";


            }elseif($interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $series="generate_series(
          '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
            interval '1 month'
          ) as mon(mon)";
                $createdRetur="to_char(srso.created_at, 'YYYY-MM') as created_at";
                $groupRetur="to_char(srso.created_at, 'YYYY-MM')";


            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $on="to_char(mon.mon, 'YYYY')";
                $series="generate_series(
          '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
            interval '1 year'
          ) as mon(mon)";
                $createdRetur="to_char(srso.created_at, 'YYYY') as created_at";
                $groupRetur="to_char(srso.created_at, 'YYYY')";

            }


            $data=DB::select("
        select
          m.id,
          m.name,
        coalesce(
        (
          select
            jsonb_agg(jd.*) AS jsonb_agg
          from
            (
            select
             $time
              coalesce(
                sum(sso2.retur_count),
                0
              ) as retur_count,
              coalesce(
                sum(round(sso2.retur_amount)),
                0
              ) as retur_amount,
              coalesce(
                sum(sso2.qty),
                0
              ) as retur_product_qty
            from
              $series
              left join (
                select
                  count(*) as retur_count,
                  sum(srso.total) as retur_amount,
                  coalesce(
                    sum(d.quantity),
                    0
                  ) as qty,
                  $createdRetur
                from
                  sc_retur_sale_orders srso
                  join sc_sale_orders sso
                  on srso.sc_sale_order_id=sso.id
                  join(
                    select
                      coalesce(
                        sum(srsod.quantity*srsod.value_conversation),
                        0
                      ) as quantity,
                      srsod.sc_retur_sale_order_id
                    from
                      sc_retur_sale_order_details srsod
                    where srsod.type='minus'
                    group by
                      srsod.sc_retur_sale_order_id
                  ) as d on d.sc_retur_sale_order_id = srso.id
                where
                srso.is_deleted=0
                and srso.is_retur=1
                and srso.is_editable=1
                $queryReason
                and sso.md_merchant_id = m.id
                and sso.is_deleted = 0
                and sso.is_editable = 1
                and sso.is_keep_transaction = 0
                and sso.step_type in (0, 3)
                and sso.md_sc_transaction_status_id not in(3, 5)
                group by
                  $groupRetur
              ) as sso2 on sso2.created_at = $on
            group by
              mon.mon
            order by
              mon.mon asc
            ) jd
                ),
                '[]'
          ) trans_details
        from
          md_merchants m
        where
          m.id in($merchantId)
        ");

            if(!empty($data)){
                $time=[];
                foreach ($data as $j => $item)
                {
                    if($j==0)
                    {
                        $transDetails=json_decode($item->trans_details);
                        foreach ($transDetails as $d)
                        {
                            $time[]=$d->time;
                        }
                    }
                }
                $result=[];
                foreach ($time as $t =>$tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if($time[$t]==$d->time)
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'name'=>$i->name,
                                    'retur_count'=>$d->retur_count,
                                    'retur_amount'=>$d->retur_amount,
                                    'retur_product_qty'=>$d->retur_product_qty,
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'time'=>$time[$t],
                        'branch'=>$branch
                    ];
                }
                $params=[
                    'table'=>$result,
                    'chart'=>$data
                ];
                return $this->message::getJsonResponse(200,'Data report tersedia',$params);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }
        } catch(\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function returReportDetail(Request  $request)
    {
        try {
            $interval=(is_null($request->interval))?'month':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            if($endDate>date('Y-m-d')){

                $endDate=date('Y-m-d');
            }
            $customParams=explode('_',$request->custom_params);

            if($interval=='day'){

                $newDate=Carbon::parse($customParams[1].' '.$customParams[2])->format('Y-m-d');
                $groupRetur="to_char(r.created_at, 'YYYY-MM-DD')='$newDate'";

            }elseif($interval=='month'){

                $newDate=Carbon::parse($customParams[1].' '.$customParams[2])->format('Y-m');
                $groupRetur="to_char(r.created_at, 'YYYY')='$newDate'";

            }else{
                $newDate=$customParams[1];
                $groupRetur="to_char(r.created_at, 'YYYY')='$newDate'";

            }

            $data=collect(DB::select("
            select
              r.id,
              r.reason_id,
              r.code,
              r.total,
              r.timezone,
              r.created_at,
              m.name as branch_name,
              coalesce(c.name, 'Pelanggan Umum') as customer_name,
              s.code as selling_code,
              (case when r.reason_id=1 then 'Barang Rusak' else 'Pembatalan Item' end)as reason_note,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) AS jsonb_agg
                  from
                    (
                      select
                        srsod.id,
                        coalesce(srsod.product_name, p.name) as product_name,
                        coalesce(srsod.unit_name, p.name) as unit_name,
                        srsod.is_multi_unit,
                        srsod.quantity,
                        srsod.multi_quantity
                      from
                        sc_retur_sale_order_details srsod
                        join sc_products p on p.id = srsod.sc_product_id
                        left join md_units as u on u.id = p.md_unit_id
                      where
                        srsod.sc_retur_sale_order_id = r.id
                    )jd
                ),'[]') as details
                from
                  sc_retur_sale_orders r
                  join sc_sale_orders s on r.sc_sale_order_id = s.id
                  join md_merchants m on s.md_merchant_id = m.id
                  left join sc_customers c on c.id = s.sc_customer_id
                where
                  r.is_deleted = 0
                  and r.is_retur = 1
                  and r.is_editable = 1
                  and s.md_merchant_id = ".$customParams[0]."
                  and s.is_deleted = 0
                  and s.is_editable = 1
                  and s.is_keep_transaction = 0
                  and s.step_type in (0, 3)
                  and s.md_sc_transaction_status_id not in(3, 5)
                  and $groupRetur
            "));

            return response()->json($data);


        }catch (\Exception $e)
        {
            return  response()->json([]);
        }
    }

    public function exportRetur(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $responseJson = $this->returReportV2($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;
            $view;

            switch($request->interval){
                case "day":
                    $view = "Hari";
                    break;
                case "month":
                    $view = "Bulan";
                    break;
                case "year":
                    $view = "Tahun";
                    break;
            };

            $objPHPExcel->getActiveSheet()->setCellValue('G2',"Laporan Retur Penjualan")->getStyle('G2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('G2:I2');
            $objPHPExcel->getActiveSheet()->getStyle('G2:I2')->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('G2')->getFont()->setBold( true );

            $objPHPExcel->getActiveSheet()->setCellValue('G3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('H3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('G4',"Tampilan Periode");
            $objPHPExcel->getActiveSheet()->setCellValue('H4', $view);

            $objPHPExcel->getActiveSheet()->setCellValue('G5',"Total Trx Retur");
            $objPHPExcel->getActiveSheet()->setCellValue('H5', $request->total_trans." Transaksi");

            $objPHPExcel->getActiveSheet()->setCellValue('G6',"Total Retur Penjualan");
            $objPHPExcel->getActiveSheet()->setCellValue('H6', $request->total_retur);

            $objPHPExcel->getActiveSheet()->setCellValue('G7',"Total Retur Produk");
            $objPHPExcel->getActiveSheet()->setCellValue('H7', $request->total_product);

            $header = [
                'Waktu',
                'Total Produk',
                'Total Transaksi',
                'Retur Penjualan'
            ];
            $col = 'G';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '8', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->getFont()->setBold( true );
                $col++;
            }

            $num = 9;

            foreach ($data->table as $key => $item) {
                $dataShow = [
                    $item->time,
                    '',
                    '',
                    ''
                ];

                $col = 'G';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;

                foreach ($item->branch as $b =>$bb){

                    $dataShow2 = [
                        $bb->name,
                        $bb->retur_product_qty,
                        $bb->retur_count,
                        rupiah($bb->retur_amount)
                    ];

                    $col = 'G';
                    $row = $num + 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                }

            }


            foreach(range('G','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-retur-penjualan_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }




    public function jenisBayarV2(Request $request)
    {
      try {
        $type = "sell";

        $data = DB::select("
        select
          m.id,
          m.name,
          coalesce(
            (
              select
                jsonb_agg(jd.*) as jd
              from
                (
                  with tname as(
                        select
                        t.id,
                        d.name as coa_name,
                        t.name,
                        d.id as coa_id,
                        d.md_merchant_id
                      from
                        md_transaction_types t
                        left join (
                          select
                            acd.id,
                            acd.name,
                            case when acd.name = 'Kas' then 'Tunai' when acd.name like '%Bank%' then 'Transfer' else acd.name end as alias_name,
                            c.md_merchant_id
                          from
                            acc_coa_details acd
                            join acc_coa_categories c on acd.acc_coa_category_id = c.id
                            and acd.name not like '%Kas Kasir Outlet%'
                            and acd.code SIMILAR TO '%1.1.01.%|%1.1.02.%'
                            and c.md_merchant_id = 46
                            and acd.is_deleted in(0, 2)
                        ) as d on lower(d.alias_name)= lower(
                          case when t.id in(4, 5, 15) then 'Payment Gateway' else t.name end
                        )
                      where
                        t.id not in (1, 6, 14)
                      order by
                        t.id asc
                        )
                        select
                          tname.id,
                        case when tname.id in(4, 5, 15) then concat(
                            'Payment Gateway - ', tname.name
                          ) when tname.id = 2 then 'Tunai' when tname.id = 13 then 'Transfer Bank' else concat(tname.name, ' (Hanya Label)') end as name,
                          coalesce(s.trans_count, 0) + coalesce(ap.paid_count, 0) as trans_count,
                          coalesce(
                            sum(s.total),
                            0
                          )+ coalesce(
                            sum(ap.paid),
                            0
                          ) trans_amount,
                          coalesce(
                            (
                              SELECT
                                jsonb_agg(jd.*) AS jsonb_agg
                              FROM
                                (
                                  select
                                  to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,
                                    coalesce(
                                      sum(p.trans_count),
                                      0
                                    ) as trans_count,
                                    coalesce(
                                      sum(p.trans_amount),
                                      0
                                    ) as trans_amount
                                  from
                                    generate_series(
                                      '2022-05-01' :: timestamp, '2022-05-31' :: timestamp,
                                      interval '1 day'
                                    ) as mon(mon)
                                    left join(
                                      select
                                        to_char(sso.created_at, 'YYYY-MM-DD') as created_at,
                                        count(*) as trans_count,
                                        coalesce(
                                          sum(sso.total),
                                          0
                                        ) trans_amount
                                      from
                                        sc_sale_orders sso
                                      where
                                        sso.is_deleted = 0
                                        and sso.is_editable = 1
                                        and sso.step_type in (0,3)
                                        and sso.md_merchant_id = 46
                                        and sso.md_sc_transaction_status_id not in(3, 5)
                                        and sso.md_transaction_type_id = tname.id
                                      group by
                                        to_char(sso.created_at, 'YYYY-MM-DD')
                                    ) as p on p.created_at = to_char(mon.mon, 'YYYY-MM-DD')
                                  group by
                                    mon.mon
                                  order by
                                    mon.mon asc
                                ) jd
                            ),
                            '[]'
                          ) AS get_detail_selling,
                          coalesce(
                            (
                              SELECT
                                jsonb_agg(jd.*) AS jsonb_agg
                              FROM
                                (
                                  select
                                    to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,
                                    coalesce(
                                      sum(p.trans_count),
                                      0
                                    ) as trans_count,
                                    coalesce(
                                      sum(p.trans_amount),
                                      0
                                    ) as trans_amount
                                  from
                                    generate_series(
                                      '2022-05-01' :: timestamp, '2022-05-31' :: timestamp,
                                      interval '1 day'
                                    ) as mon(mon)
                                    left join(
                                      select
                                        to_char(amad.created_at, 'YYYY-MM-DD') as created_at,
                                        count(*) as trans_count,
                                        sum(amad.paid_nominal) as trans_amount
                                      from
                                        acc_merchant_ar_details amad
                                        join acc_merchant_ar ar on amad.acc_merchant_ar_id = ar.id
                                      where
                                        amad.is_deleted = 0
                                        and ar.md_merchant_id = 46
                                        and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                                        and ar.is_deleted = 0
                                        and ar.is_editable = 1
                                        and amad.trans_coa_detail_id = tname.coa_id
                                        and amad.md_transaction_type_id = case when amad.md_transaction_type_id isnull then null else tname.id end

                                      group by
                                        to_char(amad.created_at, 'YYYY-MM-DD')
                                    ) as p on p.created_at = to_char(mon.mon, 'YYYY-MM-DD')
                                  group by
                                  mon.mon
                                  order by
                                    mon.mon asc
                                ) jd
                            ),
                            '[]'
                          ) AS get_detail_ap
                        from
                          tname
                          left join(
                            select
                              count(*) as trans_count,
                              sum(sso.total) as total,
                              sso.md_transaction_type_id
                            from
                              sc_sale_orders sso
                            where
                              sso.is_deleted = 0
                              and sso.is_editable = 1
                              and sso.step_type in (0,3)
                              and sso.md_sc_transaction_status_id not in(3, 5)
                              and sso.md_merchant_id = 46
                              and sso.created_at::date between '2022-05-01' and '2022-05-31'
                            group by
                              sso.md_transaction_type_id
                          ) as s on s.md_transaction_type_id = tname.id
                          left join(
                            select
                              count(*) as paid_count,
                              sum(amad.paid_nominal) as paid,
                              amad.trans_coa_detail_id,
                              case when d.name like '%Kas%' then 'Tunai'
                              when amad.is_type_payment_gateway=1 then 'E-Wallet'
                              when amad.is_type_payment_gateway=2 then 'QRIS'
                              when amad.is_type_payment_gateway=3 then 'Virtual Account'
                              when d.name like '%Bank%' then 'Transfer'
                              else d.name end as payment_name
                            from
                              acc_merchant_ar_details amad
                              join acc_merchant_ar ar on amad.acc_merchant_ar_id = ar.id
                              join acc_coa_details d on d.id=amad.trans_coa_detail_id
                            where
                              amad.is_deleted = 0
                              and ar.md_merchant_id = 46
                              and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                              and ar.is_deleted = 0
                              and ar.is_editable = 1
                              and amad.created_at::date between '2022-05-01' and '2022-05-31'
                            group by
                              amad.trans_coa_detail_id,d.name,amad.is_type_payment_gateway
                          ) as ap on ap.payment_name = tname.name
                        where
                          tname.md_merchant_id = 46
                        group by
                          tname.id,
                          tname.name,
                          s.trans_count,
                          s.total,
                          ap.paid_count,
                          tname.coa_id
                        order by trans_amount desc
                ) as jd
            ),
          '[]') as trans_details
        from
          md_merchants m
        where
          m.id in (46);
        ");

        if(!empty($data)){

          $paytype=[];
          foreach ($data as $j => $item)
          {
              if($j==0)
              {
                  $transDetails=json_decode($item->trans_details);
                  foreach ($transDetails as $d)
                  {
                      $paytype[]=$d->name;
                  }
              }
          }

          $result=[];
          foreach ($paytype as $t =>$tm)
          {
              $branch=[];
              foreach ($data as $k =>$i)
              {
                  $transDetails=json_decode($i->trans_details);
                  // dd($transDetails);
                  foreach ($transDetails as $d)
                  {
                      if(strtolower($paytype[$t])==strtolower($d->name))
                      {

                          $branch[]=[
                              'id'=>$i->id,
                              'branch_name'=>$i->name,
                              'trans_amount'=>$d->trans_amount,
                              'trans_count' => $d->trans_count,
                              'get_detail_selling'=>$d->get_detail_selling,
                              'get_detail_ap' => $d->get_detail_ap
                          ];
                      }

                  }
              }

              $result[]=[
                  'paytype'=>$paytype[$t],
                  'branch'=>$branch
              ];
          }

          $params=[
              'table'=>$result
          ];

          return $this->message::getJsonResponse(200,'Data report tersedia',$params);
      }else{

          return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
      }

      } catch(\Exception $e)
      {
          log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
          return $this->message::getJsonResponse(500, trans('custom.errors'), []);
      }
    }

    public function piutangReport(Request  $request)
    {
        try{
          $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

            $data=DB::select("select
          coalesce(s.total_transaksi, 0) as total_count_ar,
          coalesce(ar.residual_amount, 0) as total_amount_ar
        from
          md_merchants m
          join(
            select
              count(*) as total_transaksi,
              ($merchantId) as md_merchant_id
            from
              sc_sale_orders sso
            where
              sso.is_editable = 1
              and sso.step_type in (0,3)
              and sso.is_deleted = 0
              and sso.is_debet = 1
              and sso.md_merchant_id = $merchantId
          ) as s on s.md_merchant_id = m.id
          join(
            select
              sum(paid_nominal) as paid_nominal,
              sum(residual_amount) as residual_amount,
              ($merchantId) as md_merchant_id
            from
              acc_merchant_ar
            where
              md_merchant_id = $merchantId
              and arable_type = 'App\Models\SennaToko\SaleOrder'
              and is_deleted = 0
              and is_editable = 1
          ) as ar on ar.md_merchant_id = m.id
        where
          m.id = $merchantId
       ");

            if(!empty($data)){

                return $this->message::getJsonResponse(200,'Data report tersedia',$data);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);


        }
    }



    public function promo()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;



        $params = [
            "title" => "Laporan Promo",
            'merchantId'=>$merchantId
        ];
        return view('merchant::report.sale-order.promo.index', $params);
    }

    public function promoReportV2(Request $request)
    {
      try {
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;

            $data=DB::select("
            select
              mm.id,
              mm.name,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) as jsonb_agg
                  from
                    (
                      with discount_report as(select
                        smd.name as discount_name,
                        coalesce(d.promo, 0) as total_promo
                      from
                        sc_merchant_discounts smd
                        left join(
                          select
                            sso.discount_primary_id,
                            sum(sso.promo) as promo
                          from
                            sc_sale_orders sso
                            join sc_merchant_discounts s on sso.discount_primary_id = s.id
                          where
                            sso.promo > 0
                            and sso.md_merchant_id = $merchantId
                            and sso.is_editable = 1
                            and sso.is_deleted = 0
                            and sso.md_sc_transaction_status_id not in(3, 5)
                            and sso.step_type in (0, 3)
                            and sso.created_at :: date between '".$startDate."'
                            and '".$endDate."'
                          group by
                            sso.discount_primary_id
                        ) as d on d.discount_primary_id = smd.id
                      where
                        smd.md_merchant_id = $merchantId
                        and smd.is_deleted = 0
                      union
                      select
                        ('Promo Lain-Lain') as discount_name,
                        coalesce(
                          sum(sso.promo),
                          0
                        ) as total_promo
                      from
                        sc_sale_orders sso
                      where
                        sso.promo > 0
                        and sso.md_merchant_id = $merchantId
                        and sso.discount_primary_id isnull
                        and sso.is_editable = 1
                        and sso.is_deleted = 0
                        and sso.md_sc_transaction_status_id not in(3, 5)
                        and sso.step_type in (0)
                        and sso.created_at :: date between '".$startDate."'
                        and '".$endDate."'
                      union
                      select
                        cdbp.name as discount_name,
                        coalesce(d.promo,0) as total_promo
                      from
                        crm_discount_by_products cdbp
                      left join (
                          select
                            ssod.ref_discount_id,
                            case when ssod.is_bonus=0 then sum(ssod.price*ssod.quantity)-sum(ssod.sub_total)
                            else  sum(ssod.price*quantity)
                            end as promo
                          from
                            sc_sale_order_details ssod
                            join sc_sale_orders sso on ssod.sc_sale_order_id = sso.id
                          where
                            ssod.is_deleted = 0
                            and sso.md_merchant_id = $merchantId
                            and sso.is_editable = 1
                            and sso.is_deleted = 0
                            and ssod.is_bonus=0
                            and sso.md_sc_transaction_status_id not in(3, 5)
                            and sso.step_type in (0)
                            and sso.created_at :: date between '".$startDate."'
                            and '".$endDate."'
                            group by ssod.ref_discount_id,ssod.is_bonus
                        ) d on d.ref_discount_id=cdbp.id
                      where
                        cdbp.md_merchant_id = $merchantId
                        and cdbp.is_deleted = 0
                        and cdbp.bonus_type in(1,2)
                      union
                      select
                      cdbp.name as discount_name,
                      coalesce(d.promo,0) as total_promo
                    from
                      crm_discount_by_products cdbp
                    left join (
                        select
                          ssod.ref_discount_id,
                case when ssod.is_bonus=0 then sum(ssod.price*ssod.quantity)-sum(ssod.sub_total)
                          else sum(ssod.price*quantity)
                          end as promo
                        from
                          sc_sale_order_details ssod
                          join sc_sale_orders sso on ssod.sc_sale_order_id = sso.id
                        where
                          ssod.is_deleted = 0
                          and sso.md_merchant_id = $merchantId
                          and sso.is_editable = 1
                          and sso.is_deleted = 0
                          and ssod.is_bonus=1
                          and sso.md_sc_transaction_status_id not in(3, 5)
                          and sso.step_type in (0, 3)
                          and sso.created_at :: date between '".$startDate."'
                          and '".$endDate."'
                          group by ssod.ref_discount_id,ssod.is_bonus
                      ) d on d.ref_discount_id=cdbp.id
                    where
                      cdbp.md_merchant_id = $merchantId
                      and cdbp.is_deleted = 0
                      and cdbp.bonus_type =3
                        )
                      select
                        *
                      from
                        discount_report
                      order by total_promo desc
                    ) as jd
                ), '[]'
              ) as details
        from
          md_merchants mm
        where
          mm.id = $merchantId
         ");

          if(!empty($data)){

              return $this->message::getJsonResponse(200,'Data report tersedia',$data);
          }else{

              return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
          }
      } catch(\Exception $e)
      {
          log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
          return $this->message::getJsonResponse(500, trans('custom.errors'), []);
      }
    }

    public function exportPromo(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJson = $this->promoReport($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;

            $objPHPExcel->getActiveSheet()->setCellValue('I2',"Laporan Promo")->getStyle('I2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('I2:J2');
            $objPHPExcel->getActiveSheet()->getStyle("I2:J2")->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('I2')->getFont()->setBold( true );

            $objPHPExcel->getActiveSheet()->setCellValue('I3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('J3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $header = [
                'Nama Promo',
                'Total Promo',
            ];
            $col = 'I';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '5', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '5')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '5')->getFont()->setBold( true );
                $col++;
            }

            $num = 5;

            foreach ($data as $key => $item) {
                $dataShow = [
                    $item->discount_name,
                    rupiah($item->total_promo)
                ];

                $col = 'I';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('I','L') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-laporan-promo_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }


    public function exportTypeBuyer(Request $request)
    {
        try{
          $styleArray = [
              'borders' => [
                  'outline' => [
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                  ],
              ],
          ];
          $style = array(
            'alignment' => array(
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            )
        );
          $fileName = public_path('example/example.xlsx');
          $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

          $responseJson = $this->jenisBayar($request);
          $response = json_decode($responseJson->getContent());
          $data = $response->data;

          $objPHPExcel->getActiveSheet()->setCellValue('F2',"Laporan Jenis Bayar")->getStyle('F2')->getFont()->setSize(18);
          $objPHPExcel->getActiveSheet()->mergeCells('F2:H2');
          $objPHPExcel->getActiveSheet()->getStyle("F2:H2")->applyFromArray($style);

          $objPHPExcel->getActiveSheet()->setCellValue('F3',"Periode Tanggal");
          $objPHPExcel->getActiveSheet()->setCellValue('G3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

          $header = [
              'Jenis Bayar',
              'Jumlah Transaksi',
              'Penjualan'
          ];
          $col = 'F';
          foreach ($header as $item) {
              $objPHPExcel->getActiveSheet()->setCellValue($col . '6', strtoupper($item));
              $objPHPExcel->getActiveSheet()->getStyle($col . '6')->applyFromArray($styleArray);
              $objPHPExcel->getActiveSheet()->getStyle($col . '6')->getFont()->setBold( true );
              $col++;
          }

          $num = 6;

          foreach ($data as $key => $item) {
              $dataShow = [
                  $item->name,
                  $item->trans_count,
                  rupiah(round($item->trans_amount))
              ];

              $col = 'F';
              $row = $num + 1;
              foreach ($dataShow as $ds) {
                  $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                  $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                  $col++;
              }
              $num++;
          }
          foreach(range('A','K') as $columnID) {
              $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
          }
          $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
          if(!file_exists($destinationPath)){
              mkdir($destinationPath,0777,true);
          }
          $filename = strtolower('export-laporan-jenis-bayar_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
          $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
          $writer->save($destinationPath.'/'.$filename);

          $fileURL = asset($destinationPath.'/'.$filename);
          return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

      }catch (\Exception $e)
      {
          log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
          return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

      }
    }

    public function exportCloseCashier(Request $request)
    {
        try{
          $styleArray = [
              'borders' => [
                  'outline' => [
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                  ],
              ],
          ];
          $style = array(
            'alignment' => array(
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            )
        );
          $fileName = public_path('example/example.xlsx');
          $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

          $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
          $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
          $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

          $data = OpenCloseCashierEntity::getDataForDataTable($merchantId);
          $data->whereRaw("
            acc_merchant_shifts.created_at::date between '$startDate' and '$endDate'
          ");

          foreach($objPHPExcel->getActiveSheet()->getMergeCells() as $cells){
            $objPHPExcel->getActiveSheet()->unmergeCells($cells);
          }

          $objPHPExcel->getActiveSheet()->setCellValue('D2',"Laporan Tutup Kasir")->getStyle('D2')->getFont()->setSize(18);
          $objPHPExcel->getActiveSheet()->mergeCells('D2:J2');
          $objPHPExcel->getActiveSheet()->getStyle('D2:F2')->applyFromArray($style);
          $objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold( true );

          $objPHPExcel->getActiveSheet()->setCellValue('F3',"Periode Tanggal ".Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));
          $objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($style);
          $objPHPExcel->getActiveSheet()->mergeCells('F3:I3');

          $header = [
              'No',
              'Waktu Buka Kasir',
              'Waktu Tutup Kasir',
              'Nama Kasir',
              'Modal Awal',
              'Total Tunai Aktual',
              'Total Tunai Sistem'
          ];
          $col = 'D';
          foreach ($header as $item) {
              $objPHPExcel->getActiveSheet()->setCellValue($col . '5', strtoupper($item));
              $objPHPExcel->getActiveSheet()->getStyle($col . '5')->applyFromArray($styleArray);
              $objPHPExcel->getActiveSheet()->getStyle($col . '5')->getFont()->setBold( true );
              $col++;
          }

          $firstRow = "A1:".$objPHPExcel->getActiveSheet()->getHighestColumn()."1";
          $objPHPExcel->getActiveSheet()->mergeCells($firstRow);

          $num = 5;

          foreach ($data->get() as $key => $item) {
              $dataShow = [
                  $item->row_number,
                  Carbon::parse($item->waktu_buka_kasir)->format('d M Y , H:i'),
                  !is_null($item->waktu_tutup_kasir)? Carbon::parse($item->waktu_tutup_kasir)->format('d M Y , H:i'):"-",
                  ucwords($item->nama_kasir),
                  rupiah($item->modal_awal),
                  rupiah($item->total_tunai_aktual),
                  rupiah($item->total_tunai_sistem)
              ];

              $col = 'D';
              $row = $num + 1;
              foreach ($dataShow as $ds) {
                  $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                  $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                  $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                  $col++;
              }
              $num++;
          }

          foreach(range('D','M') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

          $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/sale-order';
          if(!file_exists($destinationPath)){
              mkdir($destinationPath,0777,true);
          }
          $filename = strtolower('export-laporan-tutup-kasir_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
          $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
          $writer->save($destinationPath.'/'.$filename);

          $fileURL = asset($destinationPath.'/'.$filename);
          return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

      }catch (\Exception $e)
      {
          log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
          return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

      }
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Report;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use PhpOffice\PhpSpreadsheet\Style\Alignment;


class ReportPurchaseOrderController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeWeb()
    {
        Route::prefix('report/purchase-order')
            ->middleware('merchant-verification')
            ->group(function () {
                //get index
                Route::get('/retur', 'Report\ReportPurchaseOrderController@retur')
                    ->name('merchant.report.purchase-order.retur');
                   Route::get('/product', 'Report\ReportPurchaseOrderController@product')
                    ->name('merchant.report.purchase-order.product');

                   //aajx
                Route::post('/retur-report', 'Report\ReportPurchaseOrderController@returReport')
                    ->name('merchant.report.purchase-order.retur-report');
                   Route::post('/product-report', 'Report\ReportPurchaseOrderController@pembelianProduk')
                    ->name('merchant.report.purchase-order.product-report');
                //export
                Route::post('/export-product', 'Report\ReportPurchaseOrderController@exportProduct')
                    ->name('merchant.report.purchase-order.export-product');
                   Route::post('/export-retur', 'Report\ReportPurchaseOrderController@exportRetur')
                    ->name('merchant.report.purchase-order.export-retur');

                Route::post('/retur-report-detail', 'Report\ReportPurchaseOrderController@returReportDetail')
                    ->name('merchant.report.purchase-order.retur-report-detail');


            });
    }


    public function retur()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
        $branch = get_cabang();
        $startDate=Carbon::now()->startOfMonth()->toDateString();
        $endDate=Carbon::now()->endOfMonth()->toDateString();

        $params = [
        "title" => "Laporan Retur Pembelian",
            'merchantId'=>$merchantId,
            "branch" => $branch,
            "startDate" => $startDate,
            "endDate" => $endDate
      ];

      return view('merchant::report.purchase-order.retur', $params);
    }


    public function returReportDetail(Request  $request)
    {
        try {
            $interval=(is_null($request->interval))?'month':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            if($endDate>date('Y-m-d')){

                $endDate=date('Y-m-d');
            }
            $customParams=explode('_',$request->custom_params);

            if($interval=='day'){

                $newDate=Carbon::parse($customParams[1].' '.$customParams[2])->format('Y-m-d');
                $groupRetur="to_char(r.created_at, 'YYYY-MM-DD')='$newDate'";

            }elseif($interval=='month'){

                $newDate=Carbon::parse($customParams[1].' '.$customParams[2])->format('Y-m');
                $groupRetur="to_char(r.created_at, 'YYYY')='$newDate'";

            }else{
                $newDate=$customParams[1];
                $groupRetur="to_char(r.created_at, 'YYYY')='$newDate'";

            }

            $data=collect(DB::select("
            select
              r.id,
              r.reason_id,
              r.code,
              r.total,
              r.timezone,
              r.created_at,
              m.name as branch_name,
              coalesce(c.name, 'Pelanggan Umum') as customer_name,
              s.code as selling_code,
              (case when r.reason_id=1 then 'Barang Rusak' else 'Pembatalan Item' end)as reason_note,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) AS jsonb_agg
                  from
                    (
                      select
                        srsod.id,
                        coalesce(srsod.product_name, p.name) as product_name,
                        coalesce(srsod.unit_name, p.name) as unit_name,
                        srsod.is_multi_unit,
                        srsod.quantity,
                        srsod.multi_quantity
                      from
                        sc_retur_purchase_order_details srsod
                        join sc_products p on p.id = srsod.sc_product_id
                        left join md_units as u on u.id = p.md_unit_id
                      where
                        srsod.sc_retur_purchase_order_id = r.id
                    )jd
                ),'[]') as details
                from
                  sc_retur_purchase_orders r
                  join sc_purchase_orders s on r.sc_purchase_order_id = s.id
                  join md_merchants m on s.md_merchant_id = m.id
                  left join sc_suppliers c on c.id = s.sc_supplier_id
                where
                  r.is_deleted = 0
                  and r.is_retur = 1
                  and r.is_editable = 1
                  and s.md_merchant_id = ".$customParams[0]."
                  and s.is_deleted = 0
                  and s.is_editable = 1
                  and s.step_type in (0, 3)
                  and s.md_sc_transaction_status_id not in(3, 5)
                  and $groupRetur
            "));

            return response()->json($data);


        }catch (\Exception $e)
        {
            return  response()->json([]);
        }
    }


    public function product()
    {
      $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $startDate=Carbon::now()->startOfMonth()->toDateString();
        $endDate=Carbon::now()->endOfMonth()->toDateString();
        $branch = get_cabang();
        $category = CategoryEntity::listOption();

        $params = [
        "title" => "Laporan Pembelian Komprehensif",
        'merchantId'=>$merchantId,
          "startDate" => $startDate,
          "endDate" => $endDate,
          "branch" => $branch,
            "category" => $category,

        ];

      return view('merchant::report.purchase-order.product', $params);
    }



    public function returReport(Request  $request)
    {
        try{

            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $interval=(is_null($request->interval))?'month':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;
            if($endDate>date('Y-m-d')){

                $endDate=date('Y-m-d');
            }

            $reasonId=(is_null($request->reason_id))?'-1':$request->reason_id;
            if($reasonId=='-1'){

                $queryReason='and srpo.reason_id in(1,2,3)';
            }elseif($reasonId=='1' || $reasonId==1){
                $queryReason='and srpo.reason_id=1';
            }else{
                $queryReason='and srpo.reason_id!=1';
            }

            if($interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $created="to_char(srpo.created_at, 'YYYY-MM-DD') as created_at,";
                $group="to_char(srpo.created_at, 'YYYY-MM-DD')";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $interval_='1 day';
            }elseif($interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(srpo.created_at, 'YYYY-MM') as created_at,";
                $group="to_char(srpo.created_at, 'YYYY-MM')";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $interval_='1 month';
            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(srpo.created_at, 'YYYY') as created_at,";
                $group="to_char(srpo.created_at, 'YYYY')";
                $on="to_char(mon.mon, 'YYYY')";
                $interval_='1 year';
            }

            $data=DB::select("

    select
          m.id,
          m.name,
        coalesce(
        (
          select
            jsonb_agg(jd.*) AS jsonb_agg
          from
            (
            select
             $time
              coalesce(
                sum(p.retur_count),
                0
              ) as retur_count,
              coalesce(
                sum(round(p.retur_amount)),
                0
              ) as retur_amount,
              coalesce(
                sum(p.qty),
                0
              ) as retur_product_qty

            from
              generate_series(
                '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                interval '".$interval_."'
              ) as mon(mon)
              left join(
                select
                $created
                  count(*) as retur_count,
                  coalesce(
                    sum(srpo.total),
                    0
                  ) retur_amount,
                   coalesce(
                    sum(d.quantity),
                    0
                  ) as qty
                from
                  sc_retur_purchase_orders srpo
                  join sc_purchase_orders spo on srpo.sc_purchase_order_id = spo.id
                  join(
                    select
                      coalesce(
                        sum(srsod.quantity*(case when srsod.value_conversation=0 then 1 else srsod.value_conversation end)),
                        0
                      ) as quantity,
                      srsod.sc_retur_purchase_order_id
                    from
                      sc_retur_purchase_order_details srsod
                    where srsod.type='minus'
                    group by
                      srsod.sc_retur_purchase_order_id
                  ) as d on d.sc_retur_purchase_order_id = srpo.id
                where
                  spo.is_deleted = 0
                  and srpo.is_deleted = 0
                  and srpo.is_editable = 1
                  and srpo.is_retur = 1
                  $queryReason
                  and spo.is_editable = 1
                  and spo.step_type in (0, 3)
                  and spo.md_merchant_id =m.id
                  and spo.md_sc_transaction_status_id not in(3, 5)
                group by
                  $group
              ) as p on p.created_at = $on
            group by
              mon.mon
            order by
              mon.mon asc
           ) jd
                ),
                '[]'
          ) trans_details
        from
          md_merchants m
        where
          m.id in($merchantId)
            ");

            if(!empty($data)){

                $time=[];
                foreach ($data as $j => $item)
                {
                    if($j==0)
                    {
                        $transDetails=json_decode($item->trans_details);
                        foreach ($transDetails as $d)
                        {
                            $time[]=$d->time;
                        }
                    }
                }
                $result=[];
                foreach ($time as $t =>$tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if($time[$t]==$d->time)
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'name'=>$i->name,
                                    'retur_count'=>$d->retur_count,
                                    'retur_amount'=>$d->retur_amount,
                                    'retur_product_qty'=>$d->retur_product_qty,
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'time'=>$time[$t],
                        'branch'=>$branch
                    ];
                }
                $params=[
                    'table'=>$result,
                    'chart'=>$data
                ];
                return $this->message::getJsonResponse(200,'Data report tersedia',$params);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }


    public function pembelianProduk(Request  $request)
    {
        try{

            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $interval=(is_null($request->interval))?'day':$request->interval;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            $productCodes=(is_null($request->product_code))?"":$request->product_code;
            $categoryId=(is_null($request->category_id))?"":$request->category_id;



            if(is_null($productCodes))
            {
                $productCodes="";
            }

            if(is_null($categoryId))
            {
                $categoryId="";
            }

            if($endDate>date('Y-m-d'))
            {
                $endDate=date('Y-m-d');
            }

            if($interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM-DD') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM-DD')";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $interval_='1 day';
            }elseif($interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY-MM') as created_at";
                $group="to_char(sso.created_at, 'YYYY-MM')";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $interval_='1 month';
            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(sso.created_at, 'YYYY') as created_at";
                $group="to_char(sso.created_at, 'YYYY')";
                $on="to_char(mon.mon, 'YYYY')";
                $interval_='1 year';
            }

            $data=DB::select("
            select
              mm.id,
              mm.name,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) AS jsonb_agg
                  from
                    (
                    select
                      spc.id,
                      spc.name,
                      coalesce (mu.name,'-1') as unit_name,
                      coalesce(sell.qty, 0) as qty,
                      coalesce(round(sell.price), 0) as price,
                      coalesce(
                        (
                          SELECT
                            jsonb_agg(jd.*) AS jsonb_agg
                          FROM
                            (
                              select
                                $time
                                coalesce(
                                  sum(p.qty),
                                  0
                                ) as quantity
                              from
                                generate_series(
                                  '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                                  interval '".$interval_."'
                                ) as mon(mon)
                                left join(
                                  select
                                    $created,
                                    coalesce(
                                      sum(ssod.quantity*(case when ssod.value_conversation=0 then 1 else ssod.value_conversation end)),
                                      0
                                    ) qty
                                  from
                                    sc_purchase_orders sso
                                    join sc_purchase_order_details ssod on sso.id = ssod.sc_purchase_order_id
                                  where
                                    sso.is_deleted = 0
                                    and sso.is_editable = 1
                                    and sso.md_merchant_id = mm.id
                                    and sso.md_sc_transaction_status_id not in(3, 5)
                                    and ssod.is_deleted = 0
                                    and sso.step_type in (0)
                                    and ssod.sc_product_id = spc.id
                                  group by
                                   $group
                                ) as p on p.created_at = $on
                              group by
                                mon.mon
                              order by
                                mon.mon asc
                            ) jd
                        ),
                        '[]'
                      ) AS get_detail
                    from
                      sc_products spc
                      join md_merchants m on spc.md_user_id = m.md_user_id
                      left join md_units as mu on mu.id = spc.md_unit_id
                      left join(
                        select
                          ssod.sc_product_id,
                          coalesce(
                            sum(ssod.quantity*(case when ssod.value_conversation=0 then 1 else ssod.value_conversation end)),
                            0
                          ) qty,
                          coalesce(
                            sum(ssod.quantity * ssod.price),
                            0
                          ) price
                        from
                          sc_purchase_orders sso
                          join sc_purchase_order_details ssod on sso.id = ssod.sc_purchase_order_id
                        where
                          sso.is_deleted = 0
                          and sso.is_editable = 1
                          and sso.md_merchant_id = mm.id
                          and sso.md_sc_transaction_status_id not in(3, 5)
                          and ssod.is_deleted = 0
                          and sso.step_type in (0)
                          and sso.created_at :: date between '".$startDate."'
                          and '".$endDate."'
                        group by
                          ssod.sc_product_id
                      ) as sell on sell.sc_product_id = spc.id
                    where
                      m.id = mm.id
                      and spc.is_deleted = 0
                      and spc.is_with_stock=1
                      $categoryId
                      $productCodes
                    group by
                      spc.id,
                      sell.qty,
                      sell.price,
                      mu.name
                    order by
                      coalesce(sell.qty, 0) desc
                    ) jd
                ),
                    '[]'
                  ) trans_details
                from
                  md_merchants mm
                where
                  mm.id in($merchantId)
            ");



            if(!empty($data)){


                $categories=[];
                $unitName=[];
                foreach ($data as $j => $item)
                {
                    if($j==0)
                    {
                        $transDetails=json_decode($item->trans_details);
                        foreach ($transDetails as $d)
                        {
                            $categories[]=$d->name;
                            $unitName[]=$d->unit_name;
                        }
                    }
                }

                $result=[];
                foreach ($categories as $t =>$tm)
                {
                    $branch=[];
                    foreach ($data as $k =>$i)
                    {
                        $transDetails=json_decode($i->trans_details);
                        foreach ($transDetails as $d)
                        {
                            if(strtolower($categories[$t])==strtolower($d->name))
                            {
                                $branch[]=[
                                    'id'=>$i->id,
                                    'branch_name'=>$i->name,
                                    'product_id'=>$d->id,
                                    'product_name'=>$d->name,
                                    'unit_name'=>$d->unit_name,
                                    'qty'=>$d->qty,
                                    'price'=>$d->price,
                                    'get_detail'=>$d->get_detail
                                ];
                            }

                        }
                    }

                    $result[]=[
                        'product'=>$categories[$t],
                        'unit_name'=>$unitName[$t],
                        'branch'=>$branch
                    ];
                }

                $params=[
                    'table'=>$result
                ];

                return $this->message::getJsonResponse(200,'Data report tersedia',$params);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function exportByDateQuery(Request $request)
    {
        try{

          $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
          $interval=(is_null($request->interval))?'day':  $request->interval;
          $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
          $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
          $productCodes=(is_null($request->product_code))?"":$request->product_code;
          $categoryId=(is_null($request->category_id))?"":$request->category_id;


          if(is_null($productCodes))
          {
              $productCodes="";
          }

          if(is_null($categoryId))
          {
              $categoryId="";
          }

          if($endDate>date('Y-m-d'))
          {
              $endDate=date('Y-m-d');
          }

          if($interval=='hari')
          {
              $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
              $created="to_char(sso.created_at, 'YYYY-MM-DD') as created_at";
              $group="to_char(sso.created_at, 'YYYY-MM-DD')";
              $on="to_char(mon.mon, 'YYYY-MM-DD')";
              $interval_='1 day';
          } else if ($interval=='month')
          {
              $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
              $created="to_char(sso.created_at, 'YYYY-MM') as created_at";
              $group="to_char(sso.created_at, 'YYYY-MM')";
              $on="to_char(mon.mon, 'YYYY-MM')";
              $interval_='1 month';
          }else{
              $time="to_char(mon.mon, 'YYYY') AS time,";
              $created="to_char(sso.created_at, 'YYYY') as created_at";
              $group="to_char(sso.created_at, 'YYYY')";
              $on="to_char(mon.mon, 'YYYY')";
              $interval_='1 year';
          }

          $data=DB::select("
          select
            mm.id,
            mm.name,
            coalesce(
              (
                select
                  jsonb_agg(jd.*) AS jsonb_agg
                from
                  (
                  select
                    spc.id,
                    spc.name,
                    ram.id as cat_id,
                    coalesce (mu.name,'-1') as unit_name,
                    coalesce(sell.qty, 0) as qty,
                    coalesce(round(sell.price), 0) as price,
                    coalesce(
                      (
                        SELECT
                          jsonb_agg(jd.*) AS jsonb_agg
                        FROM
                          (
                            select
                              $time
                              coalesce(
                                sum(p.qty),
                                0
                              ) as quantity,
                              coalesce(
                                sum(p.price),
                                0
                              ) as price
                            from
                              generate_series(
                                '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                                interval '".$interval_."'
                              ) as mon(mon)
                              left join(
                                select
                                  $created,
                                  coalesce(
                                    sum(ssod.quantity*(case when ssod.value_conversation=0 then 1 else ssod.value_conversation end)),
                                    0
                                  ) qty,
                                  coalesce(
                                    sum(ssod.sub_total),
                                    0
                                  ) price
                                from
                                  sc_purchase_orders sso
                                  join sc_purchase_order_details ssod on sso.id = ssod.sc_purchase_order_id
                                where
                                  sso.is_deleted = 0
                                  and sso.is_editable = 1
                                  and sso.md_merchant_id = mm.id
                                  and sso.md_sc_transaction_status_id not in(3, 5)
                                  and ssod.is_deleted = 0
                                  and sso.step_type in (0)
                                  and ssod.sc_product_id = spc.id
                                group by
                                $group
                              ) as p on p.created_at = $on
                            group by
                              mon.mon
                            order by
                              mon.mon asc
                          ) jd
                      ),
                      '[]'
                    ) AS get_detail
                  from
                    sc_products spc
                    join md_merchants m on spc.md_user_id = m.md_user_id
                    join sc_product_categories ram on spc.sc_product_category_id = ram.id
                    left join md_units as mu on mu.id = spc.md_unit_id
                    left join(
                      select
                        ssod.sc_product_id,
                        coalesce(
                          sum(ssod.quantity*(case when ssod.value_conversation=0 then 1 else ssod.value_conversation end)),
                          0
                        ) qty,
                        coalesce(
                          sum(ssod.quantity * ssod.price),
                          0
                        ) price
                      from
                        sc_purchase_orders sso
                        join sc_purchase_order_details ssod on sso.id = ssod.sc_purchase_order_id
                      where
                        sso.is_deleted = 0
                        and sso.is_editable = 1
                        and sso.md_merchant_id = mm.id
                        and sso.md_sc_transaction_status_id not in(3, 5)
                        and ssod.is_deleted = 0
                        and sso.step_type in (0)
                        and sso.created_at :: date between '".$startDate."'
                        and '".$endDate."'
                      group by
                        ssod.sc_product_id
                    ) as sell on sell.sc_product_id = spc.id
                  where
                    m.id = mm.id
                    and spc.is_deleted = 0
                    and spc.is_with_stock=1
                    $categoryId
                    $productCodes
                  group by
                    spc.id,
                    sell.qty,
                    sell.price,
                    mu.name,
                    ram.id
                  order by
                    ram.id, spc.id asc
                  ) jd
              ),
                  '[]'
                ) trans_details
              from
                md_merchants mm
              where
                mm.id in($merchantId)
          ");


          if(!empty($data)){


              $categories=[];
              $unitName=[];
              $sum=[];
              $allSum=0;
              foreach ($data as $j => $item)
              {
                  if($j==0)
                  {
                      $transDetails=json_decode($item->trans_details);
                      foreach ($transDetails as $d)
                      {
                        for($i=0;$i < count($d->get_detail);$i++) {
                            if(empty($sum[$i])) {
                                $sum[$i] = $d->get_detail[$i]->price;
                              } else {
                                $sum[$i] += $d->get_detail[$i]->price;
                            }
                            $allSum += $d->get_detail[$i]->price;
                          }

                          $categories[]=$d->name;
                          $unitName[]=$d->unit_name;
                      }
                  }
              }

              $result=[];
              foreach ($categories as $t =>$tm)
              {
                  $branch=[];
                  foreach ($data as $k =>$i)
                  {
                      $transDetails=json_decode($i->trans_details);
                      foreach ($transDetails as $d)
                      {
                          if(strtolower($categories[$t])==strtolower($d->name))
                          {
                              $branch[]=[
                                  'id'=>$i->id,
                                  'branch_name'=>$i->name,
                                  'product_id'=>$d->id,
                                  'product_name'=>$d->name,
                                  'category_id' => $d->cat_id,
                                  'unit_name'=>$d->unit_name,
                                  'qty'=>$d->qty,
                                  'price'=>$d->price,
                                  'get_detail'=>$d->get_detail
                              ];
                          }

                      }
                  }

                  $result[]=[
                      'product'=>$categories[$t],
                      'unit_name'=>$unitName[$t],
                      'branch'=>$branch
                  ];
              }

              array_push($sum, $allSum);

              $params=[
                  'table'=>$result,
                  'sum'=>$sum
              ];

              return $this->message::getJsonResponse(200,'Data report tersedia',$params);
          }else{

              return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
          }

      }catch (\Exception $e)
      {
          log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
          return $this->message::getJsonResponse(500, trans('custom.errors'), []);

      }

    }

    public function exportByDateFormat(Request $request)
    {

      try{
        setlocale(LC_TIME, 'id');
        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $style = array(
          'alignment' => array(
              'horizontal' => Alignment::HORIZONTAL_CENTER,
              'vertical' => Alignment::VERTICAL_CENTER
          )
        );
        $styleProductNameHeader = array(
          'alignment' => array(
              'horizontal' => Alignment::HORIZONTAL_CENTER,
              'vertical' => Alignment::VERTICAL_CENTER
          ),
          'font' => [
            'bold' => true,
          ],
          'borders' => array(
            'outline' => array(
              'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            )
          )
        );

        $colHeaderStyle = array(
          'borders' => [
            'outline' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              ],
          ],
            'alignment' => array(
              'horizontal' => Alignment::HORIZONTAL_CENTER,
              'vertical' => Alignment::VERTICAL_CENTER
          )
        );

        $numberStyle = array(
            'alignment' => array(
                'horizontal' => Alignment::HORIZONTAL_RIGHT
            ),
            'borders' => array(
              'outline' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
              )
            )
        );

        $fileName = public_path('example/example.xlsx');
        $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

        $responseJson = $this->exportByDateQuery($request);
        $response = json_decode($responseJson->getContent());
        $data = $response->data;

        $objPHPExcel->getActiveSheet()->setCellValue('C4', 'YAYASAN MASJID DARUSSALAM TROPODO');
        $objPHPExcel->getActiveSheet()->setCellValue('C5', 'DAPUR UMUM');

        $header = [
          'Koding Barang',
          'Nama Produk'
        ];

        $subheader = [
          'kelompok',
          'rincian'
        ];


        foreach($data->table[0]->branch[0]->get_detail as $timeline) {
          if($request->interval == 'month') {
            array_push($header, Carbon::parse($timeline->time)->translatedFormat('F Y'));
          } else if ($request->interval == 'hari') {
            array_push($header, 'TANGGAL '.Carbon::parse($timeline->time)->translatedFormat('jS F'));
          }else {
            array_push($header, $timeline->time);
          }
          array_push($subheader, "volume");
          array_push($subheader, "harga");
          
        }
        
        array_push($header, 'JUMLAH');
        array_push($subheader, "volume");
        array_push($subheader, "harga");
        

        $col = 'A';
        $col2 = 'D';
        foreach ($header as $item) {
          if($col == 'A') {
            $objPHPExcel->getActiveSheet()->setCellValue($col . '9', strtoupper($item));
            $objPHPExcel->getActiveSheet()->mergeCells('A9:B9');
            $objPHPExcel->getActiveSheet()->getStyle('A9:B9')->applyFromArray($styleProductNameHeader);
            $col++;
          } else if ($col == 'C') {
            $objPHPExcel->getActiveSheet()->setCellValue($col . '9', strtoupper($item));
            $objPHPExcel->getActiveSheet()->mergeCells('C9:C10');
            $objPHPExcel->getActiveSheet()->getStyle('C9:C10')->applyFromArray($styleProductNameHeader);
          } else {
            $temp = $col.'9';
            $temp2 = $col2.'9';
            $objPHPExcel->getActiveSheet()->setCellValue($col . '9', strtoupper($item));
            $objPHPExcel->getActiveSheet()->mergeCells("$temp:$temp2");
            $objPHPExcel->getActiveSheet()->getStyle("$temp:$temp2")->applyFromArray($styleProductNameHeader);
            $col++;
            $col++;
            $col2++;
            $col2++;
          }
          $objPHPExcel->getActiveSheet()->getStyle($col . '9')->getFont()->setBold( true );
          $objPHPExcel->getActiveSheet()->getColumnDimension($col)
              ->setAutoSize(true);
          $col++;
          $col2++;
        }

        $col = 'A';
        $col2='B';
        foreach($subheader as $item) {
          $temp = $col.'10';
          $temp2 = $col2.'10';
          $objPHPExcel->getActiveSheet()->setCellValue($col . '10' , strtoupper($item));
          if($item == 'volume') {
            $objPHPExcel->getActiveSheet()->mergeCells("$temp:$temp2");
            $col++;
            $col2++;
          }
          if($item == 'volume') {
            $objPHPExcel->getActiveSheet()->getStyle("$temp:$temp2")->applyFromArray($colHeaderStyle);
            $objPHPExcel->getActiveSheet()->getStyle("$temp:$temp2")->getFont()->setBold( true );
          } else {
            $objPHPExcel->getActiveSheet()->getStyle($col.'10')->applyFromArray($colHeaderStyle);
            $objPHPExcel->getActiveSheet()->getStyle($col . '10')->getFont()->setBold( true );
          }
            if($col == 'B') {
              $col++;
              $col2++;
            }
            $col++;
            $col2++;
        }

        $num = 11;
        $index = 0;
        $tempRow = 0;

        foreach ($data->table as $key => $item) {
            if($item->branch[0]->qty == 0) {
                continue;
            }

            $category_id = $item->branch[0]->category_id;
            if($index >= 1) {
              if($data->table[$index-1]->branch[0]->category_id == $item->branch[0]->category_id) {
                $category_id = '';
              }
            }


            $dataShow = [
                $category_id,
                $item->branch[0]->product_id,
                $item->product,
            ];

            for($i=0; $i < count($item->branch[0]->get_detail);$i++) {
                if($item->branch[0]->get_detail[$i]->quantity == 0) {
                  array_push($dataShow, '');
                  array_push($dataShow, '');
                  array_push($dataShow, '');
                } else {
                  array_push(
                    $dataShow, $item->branch[0]->get_detail[$i]->quantity
                  );
                  array_push(
                    $dataShow, $item->branch[0]->unit_name,
                  );
                  array_push(
                    $dataShow, rupiah($item->branch[0]->get_detail[$i]->price),
                  );
                }
            }


            if($item->branch[0]->price != 0) {
                array_push(
                    $dataShow, $item->branch[0]->qty
                );
                array_push(
                    $dataShow, $item->branch[0]->unit_name
                );
                array_push(
                    $dataShow, rupiah($item->branch[0]->price)
                );
            } else {
                array_push( 
                  $dataShow, ''
                );
                array_push( 
                  $dataShow, ''
                );
                array_push( 
                  $dataShow, ''
                );
            }

            // set data table
            $col = 'A';
            $row = $num + 1;
            $tempRow = $row;
            $k = 0;
            foreach ($dataShow as $ds) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . $num, $ds);
                if($k > 2) {
                    $objPHPExcel->getActiveSheet()->getStyle($col . $num)->applyFromArray($styleArray);
                }
                $col++;
                $k++;
            }

            $num++;
            $index++;
        }

        $tempSum = [];

        foreach($data->sum as $sum) {
            array_push($tempSum, $sum);
            array_push($tempSum, '');
            array_push($tempSum, '');
        }

        $colSum = 'F';
        foreach($tempSum as $sum) {
          $row = $tempRow;
          if($sum == '') {
              $objPHPExcel->getActiveSheet()->setCellValue($colSum . $row, '');
          } else {
              $objPHPExcel->getActiveSheet()->setCellValue($colSum . $row, rupiah($sum));
              $objPHPExcel->getActiveSheet()->getStyle($colSum . $row)->applyFromArray($numberStyle);
              $objPHPExcel->getActiveSheet()->getStyle($colSum . $row)->getFont()->setBold( true );
          }
          $colSum++;
        }

        $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/purchase-order';
        if(!file_exists($destinationPath)){
            mkdir($destinationPath,0777,true);
        }
        $filename = strtolower('export-pembelian-produk_'.merchant_id()."_".date('Y-m-d-h-i-s').'_'.$request->interval.'.xlsx');
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
        $writer->save($destinationPath.'/'.$filename);

        $fileURL = asset($destinationPath.'/'.$filename);
        return "<script> location.href='$fileURL'; </script>
          <div id='successExport' class='text-center'>
              <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
          </div>
          <script>
            if(document.querySelector('#successExport')){
              toastForSaveData('Data berhasil diexport!','success',true,'',true);
            }
          </script>";

    }catch (\Exception $e)
    {
        log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
        return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

    }

    }

    public function exportProduct(Request $request)
    {
        if($request->interval != "day") {
            return $this->exportByDateFormat($request);
        }

        try{
          $styleArray = [
              'borders' => [
                  'outline' => [
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                  ],
              ],
          ];
          $style = array(
            'alignment' => array(
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER
            )
        );
          $fileName = public_path('example/example.xlsx');
          $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

          $responseJson = $this->pembelianProduk($request);
          $response = json_decode($responseJson->getContent());
          $data = $response->data;

          $objPHPExcel->getActiveSheet()->setCellValue('F2',"Laporan Pembelian")->getStyle('F2')->getFont()->setSize(18)->setBold(true);
          $objPHPExcel->getActiveSheet()->mergeCells('F2:H2');
          $objPHPExcel->getActiveSheet()->getStyle('F2:H2')->applyFromArray($style);
          $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold( true );

          $objPHPExcel->getActiveSheet()->setCellValue('F3',"Periode Tanggal");
          $objPHPExcel->getActiveSheet()->setCellValue('G3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

          $objPHPExcel->getActiveSheet()->setCellValue('F4',"Total Produk");
          $objPHPExcel->getActiveSheet()->setCellValue('G4', $request->total_product." produk");

          $objPHPExcel->getActiveSheet()->setCellValue('F5',"Total Pembelian");
          $objPHPExcel->getActiveSheet()->setCellValue('G5', $request->total_price);

          $header = [
              'Nama Produk',
              'Jumlah Produk',
              'Satuan (Default)',
              'Pembelian',

          ];
          $col = 'F';
          foreach ($header as $item) {
              $objPHPExcel->getActiveSheet()->setCellValue($col . '8', strtoupper($item));
              $objPHPExcel->getActiveSheet()->getStyle($col . '8')->applyFromArray($styleArray);
              $objPHPExcel->getActiveSheet()->getStyle($col . '8')->getFont()->setBold( true );
              $col++;
          }

          $num = 8;

          foreach ($data->table as $key => $item) {
              $dataShow = [
                  $item->product,
                  $item->unit_name,
                  '',
                  ''
              ];

              $col = 'F';
              $row = $num + 1;
              foreach ($dataShow as $ds) {
                  $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                  $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                  $col++;
              }
              $num++;

              foreach ($item->branch as $b =>$bb)
              {
                  $dataShow2 = [
                      $bb->branch_name,
                      $bb->qty,
                      $bb->unit_name,
                      rupiah($bb->price)
                  ];

                  $col = 'F';
                  $row = $num + 1;
                  foreach ($dataShow2 as $ds2) {
                      $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                      $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                      $col++;
                  }
                  $num++;
              }
          }

          foreach(range('F','K') as $columnID) {
              $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                  ->setAutoSize(true);
          }
          $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/purchase-order';
          if(!file_exists($destinationPath)){
              mkdir($destinationPath,0777,true);
          }
          $filename = strtolower('export-pembelian-produk_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
          $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
          $writer->save($destinationPath.'/'.$filename);

          $fileURL = asset($destinationPath.'/'.$filename);
          return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

      }catch (\Exception $e)
      {
          log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
          return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

      }
    }


    public function exportRetur(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
              'alignment' => array(
                  'horizontal' => Alignment::HORIZONTAL_CENTER,
                  'vertical' => Alignment::VERTICAL_CENTER
              )
          );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $responseJson = $this->returReport($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;
            $view;

            switch($request->interval){
              case "day":
                $view = "Hari";
                break;
              case "month":
                $view = "Bulan";
                break;
              case "year":
                $view = "Tahun";
                break;
            };

            $objPHPExcel->getActiveSheet()->setCellValue('F2',"Laporan Retur Pembelian")->getStyle('F2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('F2:H2');
            $objPHPExcel->getActiveSheet()->getStyle('F2:H2')->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setBold( true );

            $objPHPExcel->getActiveSheet()->setCellValue('F3',"Periode Tanggal");
            $objPHPExcel->getActiveSheet()->setCellValue('G3', Carbon::parse($request->start_date)->format('d F Y')." - ".Carbon::parse($request->end_date)->format('d F Y'));

            $objPHPExcel->getActiveSheet()->setCellValue('F4',"Tampilan Periode");
            $objPHPExcel->getActiveSheet()->setCellValue('G4', $view);

            $objPHPExcel->getActiveSheet()->setCellValue('F5',"Total Trx Retur");
            $objPHPExcel->getActiveSheet()->setCellValue('G5', $request->total_trans." Transaksi");

            $objPHPExcel->getActiveSheet()->setCellValue('F6',"Total Retur Pembelian");
            $objPHPExcel->getActiveSheet()->setCellValue('G6', $request->total_retur);

            $objPHPExcel->getActiveSheet()->setCellValue('F7',"Total Retur Produk");
            $objPHPExcel->getActiveSheet()->setCellValue('G7', $request->total_product);


            $header = [
                'Waktu',
                'Total Produk',
                'Jumlah Trx Retur',
                'Retur Pembelian',

            ];
            $col = 'F';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '8', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '8')->getFont()->setBold( true );
                $col++;
            }

            $num = 9;

            foreach ($data->table as $key => $item) {
                $dataShow = [
                    $item->time,
                    '',
                    '',
                    '',
                ];

                $col = 'F';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;

                foreach ($item->branch as $b =>$bb)
                {
                    $dataShow2 = [
                        $bb->name,
                        $bb->retur_product_qty,
                        $bb->retur_count,
                        rupiah($bb->retur_amount),
                    ];

                    $col = 'F';
                    $row = $num + 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                }
            }
            foreach(range('F','I') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/purchase-order';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-retur-pembelian_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }


}

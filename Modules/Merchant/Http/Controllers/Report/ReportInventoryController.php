<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Report;


use App\Classes\CollectionPagination;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Toko\HppEntity;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Ramsey\Uuid\Uuid;

class ReportInventoryController extends Controller
{


    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeWeb()
    {
        Route::prefix('report/inventory')
            ->middleware('merchant-verification')
            ->group(function () {
                Route::get('/turnover-stock', 'Report\ReportInventoryController@turnoverStock')
                    ->name('merchant.report.inventory.turnover-stock');

                Route::get('/demage-stock', 'Report\ReportInventoryController@demageStock')
                    ->name('merchant.report.inventory.demage-stock');


                //export
                Route::post('/export-turnover', 'Report\ReportInventoryController@exportTurnover')
                    ->name('merchant.report.inventory.export-turnover');
                Route::post('/export-demage-stock', 'Report\ReportInventoryController@exportDemageStock')
                    ->name('merchant.report.inventory.export-demage-stock');
            });
    }

    public function turnoverStock()
    {

        if(is_null(request()->md_merchant_id))
        {
            $merchantId=merchant_id();
            $merchantString="(".merchant_id().")";
            $warehouse="(".(Controller::getGlobalWarehouse(merchant_id()))->id.")";

        }else{
            $merchantId=request()->md_merchant_id;
            $merchantString="(".\request()->md_merchant_id.")";
            $warehouse="(".\request()->inv_warehouse_id.")";


        }

        $start_date=request()->start_date;
        $end_date=\request()->end_date;
        $searchProduct= \request()->search_product;
        if(is_null($start_date)){
            $startDate=Carbon::now()->startOfYear()->format('Y-m-d');
            $endDate=Carbon::now()->endOfYear()->format('Y-m-d');

        }else{
            $startDate=$start_date;
            $endDate=$end_date;
        }
        $warehouseId=is_null(\request()->inv_warehouse_id)?(Controller::getGlobalWarehouse(merchant_id()))->id:\request()->inv_warehouse_id;

        $params=[
            'title'=>'Stok Gudang',
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'merchantId'=>$merchantId,
            'searchProduct'=>$searchProduct,
            'warehouseId'=>$warehouseId,
            'data'=>HppEntity::getDatasSingle($startDate,$endDate,$merchantString,$searchProduct,request()->page,request()->is_consignment,\request()->is_show_all,$warehouse),
            'is_consignment'=>is_null(\request()->is_consignment)?0:\request()->is_consignment,
            'is_show_all'=>is_null(\request()->is_show_all)?1:\request()->is_show_all,
        ];

      return view('merchant::report.inventory.turnover-stock', $params);

    }



    public function exportTurnover(Request $request)
    {
        try{

            if(is_null(request()->md_merchant_id))
            {
                $merchantString="(".merchant_id().")";
                $warehouse="(".(Controller::getGlobalWarehouse(merchant_id()))->id.")";

            }else{
                $merchantString="(".\request()->md_merchant_id.")";
                $warehouse="(".\request()->inv_warehouse_id.")";


            }

            $startDate=request()->start_date;
            $endDate=\request()->end_date;
            $searchProduct= \request()->search_product;
            $isConsignment=is_null(\request()->is_consignment)?0:\request()->is_consignment;
            $isShowAll=is_null(\request()->is_show_all)?0:\request()->is_show_all;


            $data=HppEntity::getDatasSingle($startDate,$endDate,$merchantString,$searchProduct,request()->page,$isConsignment,$isShowAll,$warehouse,false);
            if($data->count()<1)
            {

                return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Data tidak tersedia, data gagal di export !','warning',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";
            }




            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('C2',"Laporan  Stok Produk Tanggal ".$startDate." s/d ".$endDate." ");
            $objPHPExcel->getActiveSheet()->mergeCells('C2:J2')->getStyle('C2')->getFont()->setSize(18)->setBold(true);
            $header = [
                'Produk',
                'Nama Cabang',
                'Nama Gudang',
                'Stok Tersedia',
                'HPP',
                'Nilai Persediaan',
                'Satuan',
                'Stok Masuk',
                'Stok Keluar',
            ];
            $col = 'C';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;
            foreach ($data as $key => $item) {
                $dataShow = [
                    $item->nama_produk,
                    $item->merchant_name,
                    $item->warehouse_name,
                    $item->stok_tersedia,
                    $item->hpp,
                    $item->nilai_persediaan,
                    $item->satuan,
                    $item->stok_masuk,
                    $item->stok_keluar
                 ];

                $col = 'C';
                $row = $num + 1;

                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-hpp_stock_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);

            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                let success = document.querySelector('#successExport');
                if(success){
                    toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Terjadi kesalahan di server, data gagal di export !','danger',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";
        }
    }



    public function demageStock()
    {
        if(is_null(request()->md_merchant_id))
        {
            $merchantId=[merchant_id()];
            $merchantString="(".merchant_id().")";
        }elseif (request()->md_merchant_id[0]=='-1')
        {
            $merchantString="(";
            foreach (get_cabang() as $key => $branch)
            {
                $merchantString.=($key==0)?"".$branch->id."":",".$branch->id."";;

            }
            $merchantString.=")";
            $merchantId=['-1'];
        }else{
            $merchantId=request()->md_merchant_id;
            $merchantString="(";
            $merchant=\request()->md_merchant_id;

            foreach ($merchant as $c =>$cc){
                $merchantString.=($c==0)?"".$merchant[$c]."":",".$merchant[$c]."";;

            }
            $merchantString.=")";

        }

        $startDate=is_null(\request()->start_date)?Carbon::now()->subMonths(1)->startOfMonth()->toDateString(): request()->start_date;
        $endDate=is_null(\request()->end_date)?Carbon::now()->toDateString():\request()->end_date;
        $type=is_null(\request()->type)?'sale':\request()->type;

        $params=[
            'title'=>'Stok Produk Rusak',
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'merchantId'=>$merchantId,
            'data'=>$this->_loadDemageStock($startDate,$endDate,$merchantString,true,$type),
            'type'=>$type

        ];

        return view('merchant::report.inventory.demage-stock', $params);
    }




    private function _loadDemageStock($startDate,$endDate,$merchantId,$isPage,$type='sale')
    {
        try {

            if($type=='sale'){
                $data=collect(DB::select("
                select
              sp.id,
              sp.code,
              sp.name,
              sp.name || ' (' || sp.code || ')' as nama_produk,
              coalesce(d.qty, 0) as qty,
              coalesce(d.multi_qty, 0) as multy_qty,
              coalesce(o.qty_damage, 0) as qty_damage,
              coalesce(o.qty_expired, 0) as qty_expired,
              coalesce(u.name, '') as unit_name,
              'sale' as retur_from,
              w.id as warehouse_id,
              w.name as warehouse_name,
              m.name as outlet_name,
              m.id as merchant_id
            from
              sc_products sp
              join md_merchants m on m.md_user_id = sp.md_user_id
              join md_merchant_inv_warehouses w on w.md_merchant_id = m.id
              left join md_units u on u.id = sp.md_unit_id
              left join(
                select
                  srsod.sc_product_id,
                  coalesce(
                    sum(srsod.quantity),
                    0
                  ) as qty,
                  coalesce(
                    sum(srsod.multi_quantity),
                    0
                  ) as multi_qty
                from
                  sc_retur_sale_order_details srsod
                  join sc_retur_sale_orders r on srsod.sc_retur_sale_order_id = r.id
                where
                  r.is_deleted = 0
                  and r.is_retur = 1
                  and r.created_at :: date between '".$startDate."'
                  and '".$endDate."'
                  and r.reason_id = 1
                group by
                  srsod.sc_product_id
              ) as d on d.sc_product_id = sp.id
              left join(
                select
                  ssod.sc_product_id,
                  case when ssod.is_damage = 1 then coalesce(
                    sum(ssod.qty_damage),
                    0
                  ) else 0 end as qty_damage,
                  case when ssod.is_expired = 1 then coalesce(
                    sum(ssod.qty_expired),
                    0
                  ) else 0 end as qty_expired
                from
                  sc_stock_opname_details ssod
                  join sc_stock_opnames sso on ssod.sc_stock_opname_id = sso.id
                where
                  sso.created_at_by :: date between '".$startDate."'
                  and '".$endDate."'
                  and sso.is_deleted = 0
                  and sso.is_draf = 0
                group by
                  ssod.sc_product_id,
                  ssod.is_damage,
                  ssod.is_expired
              ) as o on o.sc_product_id = sp.id
            where
              m.id in $merchantId
              and sp.is_deleted = 0
              and w.is_default = 1

            "));

                if($isPage==false){
                    return $data->groupBy('code');
                }else{
                   return CollectionPagination::paginate($data->groupBy('code'),10);
                }

            }


        }catch (\Exception $e)
        {
            if($isPage){
                return CollectionPagination::paginate(collect([]),10);

            }else{

                return (object)[];

            }

        }

    }

    public function exportDemageStock()
    {
        try{

            if(is_null(request()->md_merchant_id))
            {
                $merchantString="(".merchant_id().")";
            }elseif (request()->md_merchant_id=='-1')
            {
                $merchantString="(";
                foreach (get_cabang() as $key => $branch)
                {
                    $merchantString.=($key==0)?"".$branch->id."":",".$branch->id."";;

                }
                $merchantString.=")";
            }else{
                $merchantString="(".\request()->md_merchant_id.")";

            }


            $startDate=request()->start_date;
            $endDate=\request()->end_date;
            $type=is_null(\request()->type)?'sale':\request()->type;


            $data=$this->_loadDemageStock($startDate,$endDate,$merchantString,false,$type);

            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('C2',"Laporan  Stok Produk Rusak Tanggal ".$startDate." s/d ".$endDate." ");
            $objPHPExcel->getActiveSheet()->mergeCells('C2:J2')->getStyle('C2')->getFont()->setSize(18)->setBold(true);
            $header = [
                'Produk',
                'Rusak',
                'Cacat',
                'Kadaluarsa',
                'Satuan',
            ];
            $col = 'C';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;
            foreach ($data as $key => $item) {

                $name=is_null($item->first())?'':$item->first()->nama_produk;

                $dataShow = [
                    '-- '.$name,
                    (count($item)<1)?rupiah(0):round($item->sum('qty'),2),
                    (count($item)<1)?rupiah(0):round($item->sum('qty_damage'),2),
                    (count($item)<1)?rupiah(0):round($item->sum('qty_expired'),2),
                    (count($item)<1)?'':$item->first()->unit_name,
                ];

                $col = 'C';
                $row = $num + 1;

                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;
                foreach ($item->sortBy('merchant_id') as $c){
                    $dataShow2 = [
                        '---- '.$c->outlet_name.' - '.$c->warehouse_name,
                        round($c->qty,2),
                        round($c->qty_damage,2),
                        round($c->qty_expired,2),

                        ''
                    ];
                    $col='C';
                    $row = $num+ 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                }
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-demage_stock_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);

            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                let success = document.querySelector('#successExport');
                if(success){
                    toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Terjadi kesalahan di server, data gagal di export !','danger',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";
        }

    }
}

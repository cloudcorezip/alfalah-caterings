<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Report;


use App\Classes\Singleton\DataTable;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class ReportAccController extends Controller
{


    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }
    public function routeWeb()
    {
        Route::prefix('report/acc')
            ->middleware('merchant-verification')
            ->group(function () {
                Route::get('/ar', 'Report\ReportAccController@piutangUsaha')
                    ->name('merchant.report.acc.ar');
                Route::get('/ap', 'Report\ReportAccController@hutangUsaha')
                    ->name('merchant.report.acc.ap');
                Route::get('/expense', 'Report\ReportAccController@pengeluaranUmum')
                    ->name('merchant.report.acc.expense');

                Route::post('/ar-report', 'Report\ReportAccController@arReport')
                    ->name('merchant.report.acc.ar-report');
                Route::post('/ap-report', 'Report\ReportAccController@apReport')
                    ->name('merchant.report.acc.ap-report');
                Route::post('/exp-report', 'Report\ReportAccController@expReport')
                    ->name('merchant.report.acc.exp-report');

                // export
                Route::post('/export-ar', 'Report\ReportAccController@exportAr')
                    ->name('merchant.report.acc.export-ar');
                Route::post('/export-ap', 'Report\ReportAccController@exportAp')
                    ->name('merchant.report.acc.export-ap');
                Route::post('/export-exp', 'Report\ReportAccController@exportExpense')
                    ->name('merchant.report.acc.export-exp');

            });
    }

    public function piutangUsaha()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $totalData = DB::table('acc_merchant_ar')
            ->selectRaw('ar_purpose')
            ->where('md_merchant_id', merchant_id())
            ->where('arable_type', '#')
            ->where('is_deleted', 0)
            ->where('is_editable', 1)
            ->groupBy('ar_purpose')
            ->get();

        $params = [
            "title" => "Laporan Piutang Usaha",
            "totalData" => count($totalData),
            'merchantId'=>$merchantId
        ];

        return view('merchant::report.acc.ar', $params);
    }

    public function hutangUsaha()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        $totalData = DB::table('acc_merchant_ap')
            ->selectRaw('ap_purpose')
            ->where('md_merchant_id', merchant_id())
            ->where('apable_type', '#')
            ->where('is_deleted', 0)
            ->where('is_editable', 1)
            ->groupBy('ap_purpose')
            ->get();

        $params = [
            "title" => "Laporan Utang Usaha",
            "totalData" => count($totalData),
            'merchantId'=>$merchantId
        ];

        return view('merchant::report.acc.ap', $params);
    }

    public function pengeluaranUmum()
    {
        $startDate = request()->start_date;
        $endDate = request()->end_date;

        if(is_null($startDate))
        {
            $firstDate=Carbon::now()->firstOfYear();
            $lastDate=Carbon::now()->lastOfYear();
        }else{
            $firstDate=Carbon::parse($startDate);
            $lastDate=Carbon::parse($endDate);
        }
        $params = [
            "title" => "Laporan Pengeluaran Umum",
            'startDate'=>$firstDate,
            'endDate'=>$lastDate
        ];

        return view('merchant::report.acc.general-expenses', $params);
    }


    public function arReport(Request  $request)
    {
        try{
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;

            $data=DB::select("
                                select
                                    ama.ar_purpose,
                                    sum(ama.ar_amount) ar_amount,
                                    sum(
                                        coalesce(r.paid_nominal, 0)
                                    ) paid_nominal
                                    from
                                    acc_merchant_ar ama
                                    left join(
                                        select
                                        amad.acc_merchant_ar_id,
                                        sum(paid_nominal) as paid_nominal
                                        from
                                        acc_merchant_ar_details amad
                                        where
                                        amad.is_deleted = 0
                                        and amad.paid_date::date between '".$startDate."'
                                        and '".$endDate."'
                                        and amad.type_action = 2
                                        group by
                                        amad.acc_merchant_ar_id
                                    ) as r on r.acc_merchant_ar_id = ama.id
                                    where
                                    ama.arable_type = '#'
                                    and ama.is_deleted = 0
                                    and ama.is_editable = 1
                                    and ama.md_merchant_id = $merchantId
                                    and ama.created_at::date between '".$startDate."'
                                    and '".$endDate."'
                                    group by
                                    ama.ar_purpose
                                    order by
                                    sum(ama.ar_amount) desc offset $offset
                                    limit
                                    $limit
                            ");


            if(!empty($data)){

                return $this->message::getJsonResponse(200,'Data report tersedia',$data);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function apReport(Request  $request)
    {
        try{

            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;

            $data=DB::select("
            select
                  ama.ap_purpose,
                  sum(ama.ap_amount) ap_amount,
                  sum(
                    coalesce(r.paid_nominal, 0)
                  ) paid_nominal
                from
                  acc_merchant_ap ama
                  left join(
                    select
                      amad.acc_merchant_ap_id,
                      sum(paid_nominal) as paid_nominal
                    from
                      acc_merchant_ap_details amad
                    where
                      amad.is_deleted = 0
                      and amad.paid_date::date between '".$startDate."'
                      and '".$endDate."'
                      and amad.type_action = 2
                    group by
                      amad.acc_merchant_ap_id
                  ) as r on r.acc_merchant_ap_id = ama.id
                where
                  ama.apable_type = '#'
                  and ama.is_deleted = 0
                  and ama.is_editable = 1
                  and ama.md_merchant_id = $merchantId
                  and ama.created_at::date between '".$startDate."'
                  and '".$endDate."'
                group by
                  ama.ap_purpose
                order by
                  sum(ama.ap_amount) desc offset $offset
                limit
                  $limit
            ");

            if(!empty($data)){

                return $this->message::getJsonResponse(200,'Data report tersedia',$data);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public  static function getButton()
    {

    }

    public function expReport(Request  $request)
    {
        try{

            if(is_null($request->md_merchant_id))
            {
                $merchantId="(".merchant_id().")";
            }elseif ($request->md_merchant_id=='-1' || $request->md_merchant_id==-1)
            {
                $merchantId="(";
                foreach (get_cabang() as $b =>$bb)
                {
                    $merchantId.=($b==0)?$bb->id:",".$bb->id;
                }
                $merchantId.=")";
            }else{
                $merchantId="(".$request->md_merchant_id.")";

            }
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;

            $data=collect(DB::select("
            with query_for_cost as(select
             acd.id,
             acd.name as nama_pengeluaran,
             rupiah(d.amount) as total,
              d.amount as total_original,
              m.name as outlet
            from
              acc_coa_details acd
              join acc_coa_categories acc on acd.acc_coa_category_id=acc.id
              join md_merchants as m on m.id = acc.md_merchant_id
              left join (
                select
                  ajd.acc_coa_detail_id,
                  coalesce(
                    sum(ajd.amount),
                    0
                  )- coalesce(
                    sum(d2.amount),
                    0
                  ) as amount
                FROM
                  acc_jurnal_details ajd
                  JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                  left join (
                    select
                      ajd.acc_coa_detail_id,
                      ajd.amount
                    FROM
                      acc_jurnal_details ajd
                      JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                    WHERE
                      j.is_deleted in (0, 2)
                      and j.md_merchant_id in $merchantId
                      and ajd.coa_type = 'Kredit'
                      and j.trans_time :: date between '".$startDate."'
                  and '".$endDate."'
                  ) as d2 on d2.acc_coa_detail_id = ajd.acc_coa_detail_id
                WHERE
                  j.is_deleted in (0, 2)
                  and j.md_merchant_id in $merchantId
                  and ajd.coa_type = 'Debit'
                  and j.trans_time :: date between '".$startDate."'
                  and '".$endDate."'
                group by
                  ajd.acc_coa_detail_id
              ) as d on d.acc_coa_detail_id = acd.id
            where
              acc.name in ('Beban Operasional & Usaha','Beban Lainnya')
              and acd.name not in ('Beban Penyesuaian','Penyesuiaan Persediaan','Penyesuaian')
              and acc.md_merchant_id in $merchantId
              and acd.is_deleted in (0,2)
              and d.amount > 0)
              select * from query_for_cost order by total desc
            "));

            if($request->is_datatable==1)
            {
                return(DataTable::getInstance())->dataTableForMerchantV2(self::class,$data,[],true,true,true)
                    ->make(true);
            }else{
                $result=[];
                foreach ($data->groupBy('nama_pengeluaran') as $p =>$pp)
                {
                    $outlet=[];
                    foreach ($pp as $ppp)
                    {
                        $outlet[]=[
                            'outlet'=>$ppp->outlet,
                            'total'=>$ppp->total_original
                        ];
                    }
                    $result[]=[
                        'nama_pengeluaran'=>$p,
                        'total'=>$pp->sum('total_original'),
                        'outlet'=>$outlet
                    ];
                }

                return  response()->json($result);

            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }


    public function exportAr(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJson = $this->arReport($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;

            foreach($objPHPExcel->getActiveSheet()->getMergeCells() as $cells){
                $objPHPExcel->getActiveSheet()->unmergeCells($cells);
            }

            $objPHPExcel->getActiveSheet()->setCellValue('I2',"Laporan Piutang Usaha")->getStyle('I2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('I2:L2');
            $objPHPExcel->getActiveSheet()->getStyle("I2:M2")->applyFromArray($style);

            $header = [
                'No',
                'Penerima Piutang',
                'Total Piutang',
                'Piutang Terbayar'
            ];

            $col = 'I';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '3', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->getFont()->setBold( true );
                $col++;
            }

            //   $firstRow = "A1:D1";
            //   $objPHPExcel->getActiveSheet()->mergeCells($firstRow);

            $num = 3;

            foreach ($data as $key => $item) {
                $dataShow = [
                    $key + 1,
                    $item->ar_purpose,
                    rupiah($item->ar_amount),
                    rupiah($item->paid_nominal)
                ];

                $col = 'I';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                    $col++;
                }
                $num++;
            }
            foreach(range('I','M') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/acc';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-piutang-usaha_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                if(document.querySelector('#successExport')){
                    toastForSaveData('Data berhasil diexport!','success',true,'',true);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }


    public function exportAp(Request $request)
    {
        try{
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $responseJson = $this->apReport($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;

            foreach($objPHPExcel->getActiveSheet()->getMergeCells() as $cells){
                $objPHPExcel->getActiveSheet()->unmergeCells($cells);
            }

            $objPHPExcel->getActiveSheet()->setCellValue('I2',"Laporan Utang Usaha")->getStyle('I2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('I2:L2');
            $objPHPExcel->getActiveSheet()->getStyle("I2:M2")->applyFromArray($style);

            $header = [
                'No',
                'Pemberi Hutang',
                'Total Hutang',
                'Hutang Terbayar'
            ];

            $col = 'I';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '3', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->getFont()->setBold( true );
                $col++;
            }

            //   $firstRow = "A1:D1";
            //   $objPHPExcel->getActiveSheet()->mergeCells($firstRow);

            $num = 3;

            foreach ($data as $key => $item) {
                $dataShow = [
                    $key + 1,
                    $item->ap_purpose,
                    rupiah($item->ap_amount),
                    rupiah($item->paid_nominal)
                ];

                $col = 'I';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                    $col++;
                }
                $num++;
            }
            foreach(range('I','M') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/acc';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-hutang-usaha_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                if(document.querySelector('#successExport')){
                    toastForSaveData('Data berhasil diexport!','success',true,'',true);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }

    public function exportExpenseEachDateQuery(Request $request)
    {

        try {

            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;

            if(is_null($request->md_merchant_id))
            {
                $merchantId="(".merchant_id().")";
            }elseif ($request->md_merchant_id=='-1' || $request->md_merchant_id==-1)
            {
                $merchantId="(";
                foreach (get_cabang() as $b =>$bb)
                {
                    $merchantId.=($b==0)?$bb->id:",".$bb->id;
                }
                $merchantId.=")"; 
            }else{
                $merchantId="(".$request->md_merchant_id.")";
            }

            if($request->interval=='day')
            {
                $time="to_char(mon.mon, 'DD') || ' ' || to_char(mon.mon, 'Mon') AS time,";
                $created="to_char(j.trans_time, 'YYYY-MM-DD') as time_created";
                $group="to_char(j.trans_time, 'YYYY-MM-DD')";
                $on="to_char(mon.mon, 'YYYY-MM-DD')";
                $interval_='1 day';
            } else if ($request->interval=='month')
            {
                $time="to_char(mon.mon, 'Mon') || ' ' || to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(j.trans_time, 'YYYY-MM') as time_created";
                $group="to_char(j.trans_time, 'YYYY-MM')";
                $on="to_char(mon.mon, 'YYYY-MM')";
                $interval_='1 month';
            }else{
                $time="to_char(mon.mon, 'YYYY') AS time,";
                $created="to_char(j.trans_time, 'YYYY') as time_created";
                $group="to_char(j.trans_time, 'YYYY')";
                $on="to_char(mon.mon, 'YYYY')";
                $interval_='1 year';
            }

            
            $data=DB::select("
                    select
                    mm.id,
                    mm.name,
                    coalesce(
                    (
                        select
                        jsonb_agg(acm_mapping_result.*) as jsonagg
                        from
                        (
                            select
                            acm.id,
                            acm.name as nama_pengeluaran,
                            coalesce(
                                (
                                select
                                    jsonb_agg(count_debet_kredit.*) as jsonb
                                from
                                    (
                                    select
                                        $time
                                        coalesce(p.amount, 0) as total_amount
                                    from
                                        generate_series(
                                        '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                                        interval '".$interval_."'
                                        ) as mon(mon)
                                        left join (
                                        select
                                            ajd.acc_coa_detail_id,
                                            $created,
                                            coalesce(
                                            sum(ajd.amount),
                                            0
                                            )- coalesce(
                                            sum(d2.amount),
                                            0
                                            ) as amount
                                        from
                                            acc_jurnals j
                                            join acc_jurnal_details ajd on j.id = ajd.acc_jurnal_id
                                            left join (
                                            select
                                                ajd.acc_coa_detail_id,
                                                ajd.amount
                                            from
                                                acc_jurnal_details ajd
                                                join acc_jurnals aj on ajd.acc_jurnal_id = aj.id
                                            WHERE
                                                aj.is_deleted in (0, 2)
                                                and aj.md_merchant_id in (196)
                                                and ajd.coa_type = 'Kredit'
                                                and aj.trans_time :: date between '".$startDate."'
                                                and '".$endDate."'
                                            ) as d2 on d2.acc_coa_detail_id = ajd.acc_coa_detail_id
                                        WHERE
                                            j.is_deleted in (0, 2)
                                            and j.md_merchant_id in (196)
                                            and ajd.coa_type = 'Debit'
                                            and j.trans_time :: date between '".$startDate."'
                                            and '".$endDate."'
                                        group by
                                            ajd.acc_coa_detail_id,
                                            $group
                                        ) as p on p.acc_coa_detail_id = acm.acc_coa_detail_id
                                        and p.time_created = $on
                                    ) count_debet_kredit
                                ),
                                '[]'
                            ) AS coa_amount
                            from
                            acc_coa_mappings acm
                            where
                            acm.is_deleted = 0
                            and acm.is_type = 2
                            and acm.md_merchant_id = mm.id
                        ) acm_mapping_result
                    ),
                    '[]'
                    ) result
                from
                    md_merchants mm
                where
                    mm.id in ($merchantId)
            ");

            if(!empty($data)) {
                $id=[];
                $coa_names=[];
                $sum=[];
                $total=0;
                foreach($data as $j => $item)
                {
                    if($j==0)
                    {
                        $transDetails=json_decode($item->result);
                        foreach($transDetails as $d)
                        {
                            for($i=0;$i < count($d->coa_amount);$i++)
                            {
                                if(empty($sum[$i])){
                                    $sum[$i] = $d->coa_amount[$i]->total_amount;
                                } else {
                                    $sum[$i] += $d->coa_amount[$i]->total_amount;
                                }
                                $total += $d->coa_amount[$i]->total_amount;   
                            }
                            $coa_names[]=$d->nama_pengeluaran;
                            $id[]=$d->id;
                        }
                    }
                }

                $arrData=[];
                foreach($coa_names as $t => $tm)
                {
                    $value=[];
                    foreach($data as $k => $i)
                    {
                        $transDetails=json_decode($i->result);
                        foreach($transDetails as $d)
                        {
                            if(strtolower($coa_names[$t])==strtolower($d->nama_pengeluaran))
                            {                                
                                $trans_details=$d->coa_amount;
                                $sumAmount = 0;
                                foreach($d->coa_amount as $l) 
                                {
                                    $sumAmount += $l->total_amount;
                                }
                            }
                        }
                    }

                    $arrData[]=[
                        'id'=>$id[$t],
                        'coa_name'=>$coa_names[$t],
                        'trans_details'=>$trans_details,
                        'sum_amount'=> $sumAmount
                    ];
                }

                
                $arrData = array_filter($arrData, function($e){
                    return $e["sum_amount"] > 0;
                });

                $response=[
                    'result'=>$arrData,
                    'sum'=>$sum
                ];


                return $this->message::getJsonResponse(200, 'Data Report tersedia', $response);
            } else {
                return $this->message::getJsonResponse(404, 'Data laporan tidak ditemukan', []);
            }

        } catch(\Exception $e) {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
              return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function exportExpenseEachDate(Request $request) 
    {
        try {

            $headerStyle = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER
                ],
                'font' => [
                    'bold' => true
                ]
            ];

            $coaCodeStyle = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER
                ],
            ];

            $coaNameStyle = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_LEFT
                ],
            ];
            
            $coaAmountStyle = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT
                ],
            ];

            $sumMonthlyStyle = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_RIGHT
                ],
                'font' => [
                    'bold' => true
                ]
            ];

            $responseJson=$this->exportExpenseEachDateQuery($request);
            $response = json_decode($responseJson->getContent());
            $data = $response->data;

            $filename = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);

            $objPHPExcel->getActiveSheet()->setCellValue('A4', 'YAYASAN MASJID DARUSSALAM TROPODO');
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'DAPUR UMUM');
            $objPHPExcel->getActiveSheet()->setCellValue('E5', 'LAPORAN PENGELUARAN LAINNYA');

            $header = [
                'Kode',
                'Nama Pengeluaran'
            ];

            foreach(reset($data->result)->trans_details as $item)
            {
                if($request->interval == "month") {
                    array_push($header, Carbon::parse($item->time)->translatedFormat('F Y'));
                } else if($reqest->interval == "year") {
                    array_push($header, Carbon::parse($item->time)->translatedFormat('Y'));
                } else {
                    array_push($header, Carbon::parse($item->time)->translatedFormat('jS F'));
                }
            }

            array_push($header, "Jumlah");

            $col = 'A'; 
            foreach($header as $item) 
            {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '9', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '9')->applyFromArray($headerStyle);
                $col++;
            }
            
            $row = 10;
            foreach($data->result as $item) 
            {
                $expenses_data = [
                    $item->id,
                    $item->coa_name
                ];
                $col = 'A';
                //iteration to get data position for cell styling
                $i = 0;

                foreach($item->trans_details as $value)   
                {
                    array_push($expenses_data, rupiah($value->total_amount));
                }
                array_push($expenses_data, rupiah($item->sum_amount));

                foreach($expenses_data as $cell_data) 
                {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, strtoupper($cell_data));                    
                    if($i == 0) {
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($coaCodeStyle);
                    } else if ($i == 1) {
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($coaNameStyle);
                    } else {
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($coaAmountStyle);
                    }
                    $col++;
                    $i++;
                }
                $row++;
            }

            $sum_monthly=[];
            $sum_monthly_all = 0;

            foreach($data->sum as $sum) 
            {
                array_push($sum_monthly, $sum);
                $sum_monthly_all += $sum;
            }

            array_push($sum_monthly, $sum_monthly_all);

            
            $col = 'C';
            foreach($sum_monthly as $sum)
            {
                $objPHPExcel->getActiveSheet()->setCellValue($col . $row, rupiah($sum));
                $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($sumMonthlyStyle);
                $col++;
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/jurnal-expense';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-pengeluaran-umum_'.merchant_id()."_".date('Y-m-d-h-i-s').'_'.$request->interval.'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);
    
            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
              <div id='successExport' class='text-center'>
                  <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
              </div>
              <script>
                if(document.querySelector('#successExport')){
                  toastForSaveData('Data berhasil diexport!','success',true,'',true);
                }
              </script>";




        }catch(\Exception $e) {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";
        }
    }

    public function exportExpense(Request $request)
    {
        try{

            if(isset($request->interval)) {
                return $this->exportExpenseEachDate($request);
            }

            if(is_null($request->md_merchant_id))
            {
                $merchantId="(".merchant_id().")";
            }elseif ($request->md_merchant_id=='-1' || $request->md_merchant_id==-1)
            {
                $merchantId="(";
                foreach (get_cabang() as $b =>$bb)
                {
                    $merchantId.=($b==0)?$bb->id:",".$bb->id;
                }
                $merchantId.=")";
            }else{
                $merchantId="(".$request->md_merchant_id.")";

            }
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;

            $data=DB::select("
                        select
              acm.name as nama_pengeluaran,
              rupiah(d.amount) as total,
              d.amount as total_original,
              m.name as outlet
            from
              acc_coa_mappings acm
              join md_merchants as m on m.id=acm.md_merchant_id
              left join (
                select
                  ajd.acc_coa_detail_id,
                  coalesce(sum(ajd.amount),0)-coalesce(sum(d2.amount),0) as amount
                FROM
                  acc_jurnal_details ajd
                  JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                  left join (
                    select
                      ajd.acc_coa_detail_id,
                      ajd.amount
                    FROM
                      acc_jurnal_details ajd
                      JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                    WHERE
                      j.is_deleted in (0, 2)
                      and j.md_merchant_id in $merchantId
                      and ajd.coa_type='Kredit'
                      and j.trans_time::date between '".$startDate."' and '".$endDate."'

                  )as d2 on d2.acc_coa_detail_id=ajd.acc_coa_detail_id
                WHERE
                  j.is_deleted in (0, 2)
                  and j.md_merchant_id  in $merchantId
                  and ajd.coa_type='Debit'
                  and j.trans_time::date between '".$startDate."' and '".$endDate."'
                group by
                  ajd.acc_coa_detail_id
              ) as d on d.acc_coa_detail_id = acm.acc_coa_detail_id
            where
              acm.is_deleted = 0
              and acm.is_type = 2
              and acm.md_merchant_id in $merchantId
              and d.amount>0
            ");
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            foreach($objPHPExcel->getActiveSheet()->getMergeCells() as $cells){
                $objPHPExcel->getActiveSheet()->unmergeCells($cells);
            }

            $objPHPExcel->getActiveSheet()->setCellValue('F2',"Laporan Pengeluaran Umum $startDate sd $endDate")->getStyle('F2')->getFont()->setSize(18);
            $objPHPExcel->getActiveSheet()->mergeCells('F2:H2');
            $objPHPExcel->getActiveSheet()->getStyle("F2:H2")->applyFromArray($style);

            $header = [
                'No',
                'Nama Pengeluaran',
                'Jumlah',
            ];

            $col = 'F';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '3', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->getFont()->setBold( true );
                $col++;
            }

            $firstRow = "A1:C1";
            $objPHPExcel->getActiveSheet()->mergeCells($firstRow);

            $num = 3;

            foreach ($data as $key => $item) {
                $dataShow = [
                    $key + 1,
                    $item->nama_pengeluaran,
                    rupiah($item->total_original),
                ];

                $col = 'F';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                    $col++;
                }
                $num++;
            }
            foreach(range('F','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export/acc';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-pengeluaran-umum_'.merchant_id()."_".date('Y-m-d-h-i-s').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                if(document.querySelector('#successExport')){
                    toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }

}

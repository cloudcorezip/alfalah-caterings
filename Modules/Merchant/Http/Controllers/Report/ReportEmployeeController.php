<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Report;


use App\Classes\CollectionPagination;
use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\JurnalDetail;
use App\Models\SennaToko\MerchantCommissionList;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\PaymentUtils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\Merchant\Entities\Acc\JournalEntity;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Ramsey\Uuid\Uuid;

class ReportEmployeeController extends Controller
{


    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }
    public function routeWeb()
    {
        Route::prefix('report/employee')
            ->middleware('merchant-verification')
            ->group(function () {
                Route::get('/commission', [static::class, 'commission'])
                    ->name('merchant.report.employee.commission');
                Route::post('/export-commission', [static::class, 'exportCommission'])
                    ->name('merchant.report.employee.export-commission');
                Route::post('/add-pay-commission', [static::class, 'formPayCommission'])
                    ->name('merchant.report.employee.add-pay-commission');
                Route::post('/save-pay-commission', [static::class, 'savePayCommission'])
                    ->name('merchant.report.employee.save-pay-commission');
                Route::post('/reject-commission', [static::class, 'rejectPayCommission'])
                    ->name('merchant.report.employee.reject-commission');
            });
    }

    public function commission()
    {
        if(is_null(request()->md_merchant_id))
        {
            $merchantId=[merchant_id()];
            $merchantString="(".merchant_id().")";
        }elseif (request()->md_merchant_id[0]=='-1')
        {
            $merchantString="(";
            foreach (get_cabang() as $key => $branch)
            {
                $merchantString.=($key==0)?"".$branch->id."":",".$branch->id."";;

            }
            $merchantString.=")";
            $merchantId=['-1'];
        }else{
            $merchantId=request()->md_merchant_id;
            $merchantString="(";
            $merchant=\request()->md_merchant_id;

            foreach ($merchant as $c =>$cc){
                $merchantString.=($c==0)?"".$merchant[$c]."":",".$merchant[$c]."";;

            }
            $merchantString.=")";

        }

        $startDate=is_null(\request()->start_date)?Carbon::now()->subMonths(1)->startOfYear()->toDateString(): request()->start_date;
        $endDate=is_null(\request()->end_date)?Carbon::now()->toDateString():\request()->end_date;
        $staffId=is_null(\request()->staff_user_id)?'-1':\request()->staff_user_id;

        $params=[
            'title'=>'Laporan Komisi Karyawan',
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'merchantId'=>$merchantId,
            'data'=>$this->_loadCommission($startDate,$endDate,$staffId,$merchantString,true),

        ];
        return view('merchant::report.employee.sales.index', $params);
    }


    private function _loadCommission($startDate,$endDate,$staffId,$merchantId,$isPage=true)
    {
        try {
            $data=collect(DB::select("
              select
                  mcl.id,
                  mcl.commission_id,
                  mcl.commission_name,
                  mcl.commission_value,
                  mcl.is_paid,
                  mcl.payment_paid_date,
                  mcl.staff_id,
                  mcl.payment_paid_id,
                  mu.fullname,
                  coalesce(g.grade_name, '-') as grade_name,
                  coalesce(g.basic_salary, 0) as basic_salary,
                  m.name as merchant_name,
                  coalesce(sso.second_code, sso.code) as sale_code,
                  mcl.sc_sale_order_id,
                  mcl.created_at,
                  coalesce(j.name,'-') as job_name
                from
                  merchant_commission_lists mcl
                  join md_merchant_staff mms on mcl.staff_id = mms.md_user_id
                  join md_merchants m on mms.md_merchant_id = m.id
                  join md_users mu on mu.id = mms.md_user_id
                  left join sc_sale_orders sso on sso.id = mcl.sc_sale_order_id
                  left join merchant_grade_staffs g on g.id = mms.merchant_grade_id
                  left join hr_merchant_job_positions as j on j.id = mms.job_position_id
                where
                  mcl.md_merchant_id in $merchantId
                  and mcl.created_at::date between '$startDate' and '$endDate'
            "));

            $result=[];
            $totalAmount=0;
            $totalPaid=0;
            $totalUnpaid=0;
            $countData=0;
            $countDataPaid=0;
            foreach ($data->groupBy('staff_id') as $key =>$item)
            {
                $commissionList=[];
                $commissionTotalAmount=0;
                $commissionTotalAmountUnpaid=0;
                $commissionTotalAmountPaid=0;
                foreach ($item as $j =>$v){
                    $itemCommissionAmount=0;
                    $itemCommissionAmountUnpaid=0;
                    $itemCommissionAmountPaid=0;
                    if(!is_null($v->commission_value)){
                        $jsonCommission=json_decode($v->commission_value);
                        foreach ($jsonCommission as $c){
                            $itemCommissionAmount+=$c->commission_amount;
                            if($v->is_paid==0){
                                $itemCommissionAmountUnpaid+=$c->commission_amount;
                                $countData++;
                            }else{
                                $itemCommissionAmountPaid+=$c->commission_amount;
                                $countDataPaid++;
                            }
                        }
                    }
                    $commissionList[]=(object)[
                        'id'=>$v->id,
                        'commission_id'=>$v->commission_id,
                        'commission_name'=>$v->commission_name,
                        'sale_code'=>$v->sale_code,
                        'sale_id'=>$v->sc_sale_order_id,
                        'is_paid'=>$v->is_paid,
                        'total'=>rupiah($itemCommissionAmount),
                        'total_original'=>$itemCommissionAmount,
                        'paid'=>rupiah($itemCommissionAmountPaid),
                        'unpaid'=>rupiah($itemCommissionAmountUnpaid)
                    ];
                    $commissionTotalAmount+=$itemCommissionAmount;
                    $commissionTotalAmountPaid+=$itemCommissionAmountPaid;
                    $commissionTotalAmountUnpaid+=$itemCommissionAmountUnpaid;
                }
                $firstData=$item->first();
                $result[]=(object)[
                    'staff_id'=>$firstData->staff_id,
                    'fullname'=>$firstData->fullname,
                    'job_name'=>$firstData->job_name,
                    'grade_name'=>$firstData->grade_name,
                    'basic_salary'=>rupiah($firstData->basic_salary),
                    'commission_amount'=>rupiah($commissionTotalAmount),
                    'merchant_name'=>$firstData->merchant_name,
                    'details'=>$commissionList
                ];
                $totalAmount+=$commissionTotalAmount;
                $totalUnpaid+=$commissionTotalAmountUnpaid;
                $totalPaid+=$commissionTotalAmountPaid;
            }

            if($isPage)
            {
                return (object)[
                    'total_amount'=>rupiah($totalAmount),
                    'total_paid'=>rupiah($totalPaid),
                    'total_unpaid'=>rupiah($totalUnpaid),
                    'total_unpaid_original'=>$totalUnpaid,
                    'count_data'=>$countData,
                    'count_data_paid'=>$countDataPaid,
                    'data'=>CollectionPagination::paginate(collect($result),10)
                ];
            }else{
                return (object)[
                    'total_amount'=>rupiah($totalAmount),
                    'total_paid'=>rupiah($totalPaid),
                    'total_unpaid'=>rupiah($totalUnpaid),
                    'total_unpaid_original'=>$totalUnpaid,
                    'count_data'=>$countData,
                    'count_data_paid'=>$countDataPaid,
                    'data'=>collect($result)
                ];
            }
        }catch (\Exception $e)
        {
            if($isPage){
                return (object)[
                    'total_amount'=>0,
                    'total_paid'=>0,
                    'total_unpaid'=>0,
                    'total_unpaid_original'=>0,
                    'count_data'=>0,
                    'count_data_paid'=>0,
                    'data'=>CollectionPagination::paginate(collect([]),10)
                ];

            }else{
                return (object)[
                    'total_amount'=>0,
                    'total_paid'=>0,
                    'total_unpaid'=>0,
                    'total_unpaid_original'=>0,
                    'count_data'=>0,
                    'count_data_paid'=>0,
                    'data'=>CollectionPagination::paginate(collect([]),10)
                ];
            }

        }
    }

    public function exportCommission()
    {
        try{

            if(is_null(request()->md_merchant_id))
            {
                $merchantString="(".merchant_id().")";
            }elseif (request()->md_merchant_id=='-1')
            {
                $merchantString="(";
                foreach (get_cabang() as $key => $branch)
                {
                    $merchantString.=($key==0)?"".$branch->id."":",".$branch->id."";;

                }
                $merchantString.=")";
            }else{
                $merchantString="(".\request()->md_merchant_id.")";

            }

            $staffId='-1';


            $startDate=request()->start_date;
            $endDate=\request()->end_date;


            $data=$this->_loadCommission($startDate,$endDate,$staffId,$merchantString,false);

            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            $objPHPExcel->getActiveSheet()->setCellValue('C2',"Laporan Komisi Tanggal ".$startDate." s/d ".$endDate." ");
            $objPHPExcel->getActiveSheet()->mergeCells('C2:J2')->getStyle('C2')->getFont()->setSize(18)->setBold(true);
            $header = [
                'Nama Karyawan',
                'Jabatan',
                'Grade',
                'Cabang',
                'Gaji Pokok',
                'Total Komisi',
                'Status'

            ];
            $col = 'C';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle($col . '4')->getFont()->setBold( true );
                $col++;
            }

            $num = 4;
            foreach ($data->data as $key => $item) {

                $dataShow = [
                    $item->fullname,
                    $item->job_name,
                    $item->grade_name,
                    $item->merchant_name,
                    $item->basic_salary,
                    $item->commission_amount,
                    ''

                ];

                $col = 'C';
                $row = $num + 1;

                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $col++;
                }
                $num++;
                if(count($item->details)>0){
                    $dataShow2 = [
                        '',
                        '',
                        'Nama Komisi',
                        '',
                        'Ref Transaksi',
                        'Nilai Komisi',
                        '',
                    ];
                    $col='C';
                    $row = $num+ 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray)->getFont()->setBold(true);
                        $col++;
                    }
                    $num++;

                    foreach ($item->details as $c){
                        $dataShow3 = [
                            '',
                            '',
                            $c->commission_name,
                            '',
                            $c->sale_code,
                            $c->total,
                            ($c->is_paid==0)?'Belum Terbayarkan':'Terbayarkan',
                        ];
                        $col='C';
                        $row = $num+ 1;
                        foreach ($dataShow3 as $ds3) {
                            $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds3);
                            $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                            $col++;
                        }
                        $num++;

                    }

                }
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-commission_' . merchant_id()."_".date('Y-m-d') . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);

            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
                let success = document.querySelector('#successExport');
                if(success){
                    toastForSaveData('Data berhasil diexport!','success',true,'',false);
                }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='text-center'>
                        <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
                    </div>
                    <script>
                        toastForSaveData('Terjadi kesalahan di server, data gagal di export !','danger',true,'',false);
                        setTimeout(() => {
                            closeModal()
                        }, 3000);
                    </script>";
        }
    }

    public function formPayCommission(Request  $request)
    {
        if ($request->id == '-1') {
            return "<script>otherMessage('warning','Terjadi kesalahan! Setidaknya pilih satu data');closeModalAfterSuccess()</script>";
        } else {
            $ids=json_decode($request->id);
            if(empty($ids->ids)){
                return "<script>otherMessage('warning','Terjadi kesalahan! Setidaknya pilih satu data');closeModalAfterSuccess()</script>";

            }else{
                $merchantId = $request->merchant_id;
                $totalAmount=0;
                if(count($ids->ids)<$ids->countData){
                    foreach ($ids->commissionValue as $c)
                    {
                        $totalAmount+=$c;
                    }
                }else{
                    $totalAmount=$ids->totalUnpaid;
                }

                $coaCommissionId=$this->generateCoaCommission($merchantId);
                $params = [
                    'title' => 'Pembayaran Komisi Periode '.$ids->startDate.' sd '.$ids->endDate,
                    'payment' => PaymentUtils::getPayment($merchantId, 2),
                    'merchantId' => $merchantId,
                    'opt'=>$ids->opt,
                    'ids'=>json_encode($ids->ids),
                    'totalAmount'=>$totalAmount,
                    'coaCommissionId'=>$coaCommissionId,
                    'startDate'=>$ids->startDate,
                    'endDate'=>$ids->endDate,
                    'countData'=>$ids->countData,
                    'merchantIds'=>json_encode($ids->merchantIds)
                ];

                return view('merchant::report.employee.sales.form-commission', $params);

            }

        }
    }

    public function savePayCommission(Request  $request)
    {
        try {
            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();
            $date = new \DateTime($request->payment_paid_date);
            if(AccUtil::checkClosingJournal($request->merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->merchant_id,'commission_code',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/pay-commission/';

            if($request->hasFile('payment_trans_proof'))
            {
                $file=$request->file('payment_trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                $fileName=null;

            }

            $ids=json_decode($request->ids);
            $opt=$request->opt;
            if($opt==1){

                if(count($ids)<$request->count_data){
                    $data=MerchantCommissionList::where('is_paid',0)
                        ->whereRaw("created_at::date between '$request->start_date' and '$request->end_date'")
                        ->whereIn('id',$ids)
                        ->get();
                }else{
                    $data=MerchantCommissionList::where('is_paid',0)
                        ->whereRaw("created_at::date between '$request->start_date' and '$request->end_date'")
                        ->whereIn('md_merchant_id',json_decode($request->merchant_ids))
                        ->get();
                }
            }else{
                $data=MerchantCommissionList::where('is_paid',0)
                    ->whereRaw("created_at::date between '$request->start_date' and '$request->end_date'")
                    ->whereIn('id',$ids)
                    ->get();

            }
            $secondCode=(is_null($request->code)|| $request->code=='')?CodeGenerator::generalCode('PAYC',$request->merchant_id):$request->code;
            $admin_fee=strtr($request->admin_fee, array('.' => '', ',' => '.'));

            if($data->count()<1){
                return response()->json([
                    'message' => "Pembayaran komisi sudah dilakukan!",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if($admin_fee==0){
                $adminFee=0;
            }else{
                $adminFee=$admin_fee/$data->count();
            }
            if($data->count()>0){
                $details=[];

                foreach ($data as $key => $item) {
                    $journal = new JournalEntity();
                    $saleCode = (is_null($item->getSaleOrder->second_code)) ? $item->getSaleOrder->code : $item->getSaleOrder->second_code;
                    $code = CodeGenerator::generalCode('JRN', $request->merchant_id);
                    $commissionValueData = json_decode($item->commission_value);
                    $totalAmount = 0;
                    foreach ($commissionValueData as $c) {
                        $totalAmount += $c->commission_amount;
                    }
                    $journal->trans_code = $code;
                    $journal->ref_code = $secondCode;
                    $journal->trans_name = 'Pembayaran Komisi Penjualan ' . $saleCode . ' ' . $secondCode;
                    $journal->trans_time = $request->payment_paid_date;
                    $journal->trans_note = 'Pembayaran Komisi Penjualan ' . $saleCode . ' ' . $secondCode;
                    $journal->trans_purpose = 'Pembayaran Komisi Penjualan ' . $saleCode . ' ' . $secondCode;
                    $journal->trans_proof = 'Pembayaran Komisi Penjualan ' . $saleCode . ' ' . $secondCode;
                    $journal->trans_type = 0;
                    $journal->trans_amount = $totalAmount + $adminFee;
                    $journal->md_merchant_id = $request->merchant_id;
                    $journal->md_user_id_created = user_id();
                    $journal->timezone = $request->_timezone;
                    $journal->save();


                    array_push($details, [
                        'acc_coa_detail_id' => $request->payment_paid_id,
                        'coa_type' => 'Kredit',
                        'md_sc_currency_id' => 1,
                        'amount' => $totalAmount+$adminFee,
                        'created_at' => $journal->trans_time,
                        'updated_at' => $journal->trans_time,
                        'acc_jurnal_id' => $journal->id,
                        'ref_code' => $journal->trans_code,
                    ]);

                    array_push($details, [
                        'acc_coa_detail_id' => $request->coa_commission_id,
                        'coa_type' => 'Debit',
                        'md_sc_currency_id' => 1,
                        'amount' => $totalAmount,
                        'created_at' => $journal->trans_time,
                        'updated_at' => $journal->trans_time,
                        'acc_jurnal_id' => $journal->id,
                        'ref_code' => $journal->trans_code,
                    ]);

                    if ($adminFee > 0) {
                        array_push($details, [
                            'acc_coa_detail_id'=>merchant_detail_multi_branch($request->merchant_id)->coa_administration_bank_id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$adminFee,
                            'created_at'=>$journal->trans_time,
                            'updated_at'=>$journal->trans_time,
                            'acc_jurnal_id'=>$journal->id,
                            'ref_code'=>$journal->trans_code,
                        ]);

                    }

                    $item->is_paid=1;
                    $item->payment_paid_id=$request->payment_paid_id;
                    $item->second_code=$secondCode;
                    $item->payment_paid_date=$request->payment_paid_date;
                    $item->payment_user_created=user_id();
                    $item->payment_timezone=$request->_timezone;
                    $item->payment_trans_proof=$fileName;
                    $item->ref_jurnal_id=$journal->id;
                    $item->admin_fee=$adminFee;
                    $item->coa_commission_id=$request->coa_commission_id;
                    $item->save();
                }

                JurnalDetail::insert($details);
            }
            DB::commit();
            return response()->json([
                'message'=>'Data pembayaran komisi berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>'',
                'is_with_datatable'=>false
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'error'=>$e->getMessage()
            ]);

        }
    }

    public function rejectPayCommission(Request  $request)
    {
        try {
            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();
            $ids=$request->ids;
            $countData=$request->count_data_paid;
            $isCheckAll=$request->is_checkAll;
            $date = new \DateTime(date('Y-m-d H:i:s'));
            if(AccUtil::checkClosingJournal(merchant_id(),$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);
            }
            if($isCheckAll==1){

                if(count($ids)<$countData){
                    $data=MerchantCommissionList::where('is_paid',1)
                        ->whereRaw("created_at::date between '$request->start_date' and '$request->end_date'")
                        ->whereRaw("id in (".$request->ids.")")
                        ->get();
                }else{
                    $data=MerchantCommissionList::where('is_paid',1)
                        ->whereRaw("created_at::date between '$request->start_date' and '$request->end_date'")
                        ->whereRaw("md_merchant_id in (".$request->merchant_ids.")")
                        ->get();
                }
            }else{
                $data=MerchantCommissionList::where('is_paid',1)
                    ->whereRaw("created_at::date between '$request->start_date' and '$request->end_date'")
                    ->whereRaw("id in (".$request->ids.")")
                    ->get();
            }

            foreach ($data as $item){
                JournalEntity::where('id',$item->ref_jurnal_id)->update([
                    'is_deleted'=>1
                ]);
                $item->is_paid=0;
                $item->payment_paid_id=null;
                $item->second_code=null;
                $item->payment_paid_date=null;
                $item->payment_user_created=null;
                $item->payment_timezone=null;
                $item->payment_trans_proof=null;
                $item->ref_jurnal_id=null;
                $item->admin_fee=null;
                $item->coa_commission_id=null;
                $item->save();
            }

            DB::commit();

            return response()->json([
                'message'=>'Data pembayaran komisi berhasil dibatalkan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>'',
                'is_with_datatable'=>false
            ]);

        }catch (\Exception $e){
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }
    }

    private function generateCoaCommission($merchantId)
    {
        try{
            $category=CoaCategory::where('md_merchant_id',$merchantId)
                ->where('name','Beban Operasional & Usaha')->first();
            if($category){
                $checkDetail=CoaDetail::where('name','Komisi')
                    ->where('acc_coa_category_id',$category->id)
                    ->whereIn('is_deleted',[0,2])
                    ->first();
                if(is_null($checkDetail)){
                    $checkDetail=new CoaDetail();
                    $checkDetail->code=CoaUtil::generateCoaDetail($category->id);
                    $checkDetail->name='Komisi';
                    $checkDetail->acc_coa_category_id=$category->id;
                    $checkDetail->is_deleted=0;
                    $checkDetail->md_sc_currency_id=1;
                    $checkDetail->type_coa='Debit';
                    $checkDetail->save();
                }else{
                    $checkDetail->is_deleted=0;
                    $checkDetail->save();
                }
                return $checkDetail->id;
            }else{
                return  false;
            }

        }catch (\Exception $e)
        {
            return false;
        }
    }

}

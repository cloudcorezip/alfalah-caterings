<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\ClosingJournalDetail;
use App\Models\Accounting\ClosingJournalDistribution;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\Period;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Acc\ClosingJurnalEntity;
use Modules\Merchant\Entities\Acc\JournalEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;

class ClosingJournalController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/journal/closing')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\ClosingJournalController@index')
                    ->name('merchant.toko.acc.journal.closing.index');

                Route::get('/add', 'Acc\ClosingJournalController@add')
                    ->name('merchant.toko.acc.journal.closing.add');
                //new

                Route::post('/reload-data', 'Acc\ClosingJournalController@reloadData')
                    ->name('merchant.toko.acc.journal.closing.reload');
                Route::post('/data-table', 'Acc\ClosingJournalController@dataTable')
                    ->name('merchant.toko.acc.journal.closing.datatable');
                Route::post('/save', 'Acc\ClosingJournalController@save')
                    ->name('merchant.toko.acc.journal.closing.save');

                Route::post('/delete', 'Acc\ClosingJournalController@delete')
                    ->name('merchant.toko.acc.journal.closing.delete');

                Route::post('/export-data', 'Acc\ClosingJournalController@exportData')
                    ->name('merchant.toko.acc.journal.closing.export-data');

            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Jurnal Penutup',
            'tableColumns'=>ClosingJurnalEntity::dataTableColumns(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'add'=>(get_role()==3)?ClosingJurnalEntity::add(true):
                ClosingJurnalEntity::add(true),
            'key_val'=>$request->key,
            'merchantId'=>[-1],
            'encodeMerchant'=>base64_encode(json_encode(MerchantUtil::getBranch(merchant_id())))
        ];

        return view('merchant::acc.journal.closing.index',$params);
    }


    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (ClosingJurnalEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;


        $params = [
            'title' => 'Jurnal Penutup',
            'searchKey' => $searchKey,
            'tableColumns'=>ClosingJurnalEntity::dataTableColumns(),
            'start_date'=>(is_null($request->startDate))?Carbon::now()->startOfMonth()->toDateString():$request->startDate,
            'end_date'=>(is_null($request->endDate))?Carbon::now()->endOfMonth()->toDateString():$request->endDate,
            'merchantId'=>$request->md_merchant_id,
            'encodeMerchant'=>base64_encode(json_encode($request->md_merchant_id))


        ];
        return view('merchant::acc.journal.closing.list',$params);

    }


    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ClosingJurnalEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public  function add(Request $request){

        $id=$request->id;
        $step=(is_null($request->step))?0:$request->step;

        if(!is_null($id)){

            $data=ClosingJournalDistribution::find($id);
            if(is_null($data)){
                abort(404);
            }
        }else{
            $data=new ClosingJournalDistribution();
        }

        if(!is_null($id) && $step==1){
            $startDate=Carbon::parse($data->start_date)->format('Y-m-d');
            $endDate=Carbon::parse($data->end_date)->format('Y-m-d');

            $account=collect(DB::select("
            select
              acd.id,
              acc.parent_code,
              acd.code,
              acd.name,
              acd.type_coa,
              coalesce(
                sum(s.amount_debit),
                0
              ) as amount_debit,
              coalesce(
                sum(s.amount_kredit),
                0
              ) amount_kredit,
              (case when acc.code in('4.1.00','4.2.00') then 'pendapatan' else 'pengeluaran' end) as type_of_category
            from
              acc_coa_details acd
              join acc_coa_categories acc on acc.id = acd.acc_coa_category_id
              left join(
                select
                  ajd.acc_coa_detail_id,
                  (
                    case when j.trans_time between '".$startDate." 00:00:00'
                    and '".$endDate." 23:59:58'
                    and ajd.coa_type = 'Debit' then ajd.amount else 0 end
                  ) as amount_debit,
                  (
                    case when j.trans_time between '".$startDate." 00:00:00'
                    and '".$endDate." 23:59:58'
                    and ajd.coa_type = 'Kredit' then ajd.amount else 0 end
                  ) as amount_kredit
                from
                  acc_jurnal_details ajd
                  join acc_jurnals j on j.id = ajd.acc_jurnal_id
                where
                  j.md_merchant_id = ".$data->md_merchant_id."
                  and j.is_deleted in(0, 2)
              ) s on s.acc_coa_detail_id = acd.id
            where
              acc.md_merchant_id = ".$data->md_merchant_id."
              and acc.is_deleted in (0, 2)
              and acd.is_deleted in (0, 2)
              and acc.code in(
                '4.1.00', '4.2.00','5.0.00','6.1.00','6.2.00'
              )
            group by
              acd.id,
              acc.code,
              acc.parent_code
            "));
            $group=$account->groupBy('type_of_category');
            $accountClosing=[];

            foreach ($group as $key =>$item)
            {
                $subAccount=[];
                $count=0;
                foreach ($item as $child){
                    if($child->amount_debit>0 || $child->amount_kredit>0){
                        $subAccount[]=$child;
                        $count++;
                    }
                }

                $accountClosing[]=(object)[
                    'key'=>$key,
                    'child'=>$subAccount,
                    'count'=>$count
                ];

            }
            if(count($accountClosing)>0){

                $accountDistribution=CoaDetail::
                select([
                    'acc_coa_details.*',
                    'c.parent_code'
                ])
                    ->whereIn('acc_coa_details.code',['3.1.00.01','3.1.00.02','3.2.00.01'])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$data->md_merchant_id)
                    ->get();
            }else{
                $accountDistribution=[];
            }

        }else{
            $accountClosing=[];
            $accountDistribution=[];
        }
        $params=[
            'title'=>(is_null($id))?'Tambah':'Edit',
            'data'=>$data,
            'step'=>$step,
            'accountClosing'=>$accountClosing,
            'accountDistribution'=>$accountDistribution

        ];


        return view('merchant::acc.journal.closing.form',$params);
    }


    public function save(Request  $request)
    {
        try {
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $step=$request->step;

            if(!is_null($id)){

                $data=ClosingJournalDistribution::find($id);
            }else{
                $data=new ClosingJournalDistribution();
                $code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $data->code=$code;
            }

            if($step==0){

                if(date('Y-m-d')==$request->end_date)
                {
                    return response()->json([
                        'message'=>'Tanggal akhir tidak boleh sama dengan tanggal hari ini',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>'',
                    ]);

                }
                if(date('Y-m-d')<$request->end_date)
                {
                    return response()->json([
                        'message'=>'Tanggal akhir tidak boleh lebih dari tanggal hari ini',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>'',
                    ]);
                }
                $data->start_date=$request->start_date;
                $data->end_date=$request->end_date;
                $data->md_merchant_id=$request->md_merchant_id;
                $data->created_by=user_id();
                $data->_timezone=$request->_timezone;
                $data->save();
                DB::commit();

                return response()->json([
                    'message'=>'Data berhasil disimpan',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>\route('merchant.toko.acc.journal.closing.add').'?id='.$data->id.'&step=1',
                ]);
            }else{
                $data->debit_amount=$request->amount_debit_original;
                $data->kredit_amount=$request->amount_kredit_original;
                $data->end_amount=$request->end_amount_original;
                $data->save();

                $coaIkhtisar=merchant_detail_multi_branch($request->md_merchant_id)->coa_resume_profit_lost_id;

                $coaIdDetail=$request->coa_id_detail;
                $typeCoaDetail=$request->type_coa_detail;
                $amountDetail=$request->amount_detail;
                $coaDistributionId=$request->coa_distribution_id;
                $amountDistribution=$request->amount;

                $totalAmountDistribution=0;

                $checkDetail=$data->getDetail->count();
                if($checkDetail==0)
                {
                    $closing=[];
                    $detJournal=[];
                    $detJournal2=[];
                    foreach ($coaIdDetail as $dkey =>$d)
                    {
                        $journal=new JournalEntity();
                        $code=$data->code;
                        $journal->trans_code=$code;
                        $journal->ref_code=$code;
                        $journal->trans_name='Jurnal Penutup';
                        $journal->trans_time=''.Carbon::parse($data->end_date)->format('Y-m-d').' 23:59:59';
                        $journal->trans_note='Jurnal Penutup';
                        $journal->trans_purpose='Jurnal Penutup';
                        $journal->trans_proof='Jurnal Penutup';
                        $journal->trans_type=0;
                        $journal->trans_amount=$amountDetail[$dkey];
                        $journal->md_merchant_id=$request->md_merchant_id;
                        $journal->md_user_id_created=user_id();
                        $journal->timezone=$request->_timezone;
                        $journal->save();

                        $closing[]=[
                            'code'=>$journal->trans_code,
                            'from_acc_coa_detail_id'=>$coaIdDetail[$dkey],
                            'to_acc_coa_detail_id'=>$coaIkhtisar,
                            'from_coa_type'=>($typeCoaDetail[$dkey]=='Debit')?"Kredit":'Debit',
                            'to_coa_type'=>($typeCoaDetail[$dkey]=='Debit')?"Debit":"Kredit",
                            'time'=>$journal->trans_time,
                            'amount'=>$amountDetail[$dkey],
                            'is_reject'=>2,
                            'md_merchant_id'=>$request->md_merchant_id,
                            'created_at'=>$journal->trans_time,
                            'updated_at'=>$journal->trans_time,
                            '_timezone'=>$request->_timezone,
                            'acc_distribution_id'=>$data->id,
                        ];

                        $detJournal[]=[
                            'acc_coa_detail_id'=>$coaIdDetail[$dkey],
                            'coa_type'=>($typeCoaDetail[$dkey]=='Debit')?"Kredit":'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$amountDetail[$dkey],
                            'created_at'=>$journal->trans_time,
                            'updated_at'=>$journal->trans_time,
                            'acc_jurnal_id'=>$journal->id,
                            'ref_code'=>$journal->trans_code,
                        ];
                        $detJournal2[]=[
                            'acc_coa_detail_id'=>$coaIkhtisar,
                            'coa_type'=>($typeCoaDetail[$dkey]=='Debit')?"Debit":"Kredit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$amountDetail[$dkey],
                            'created_at'=>$journal->trans_time,
                            'updated_at'=>$journal->trans_time,
                            'acc_jurnal_id'=>$journal->id,
                            'ref_code'=>$journal->trans_code,
                        ];
                    }

                    JurnalDetail::insert($detJournal);
                    JurnalDetail::insert($detJournal2);
                    ClosingJournalDetail::insert($closing);
                    DB::commit();

                }

                foreach ($amountDistribution as $key =>$item){
                    $totalAmountDistribution+=strtr($amountDistribution[$key], array('.' => '', ',' => '.'));
                }

                $data->amount_distribution=$totalAmountDistribution;
                $data->is_distribution=1;
                $data->save();

                if($totalAmountDistribution>abs($data->end_amount)){
                    return response()->json([
                        'message'=>'Terjadi kesalahan, Saldo distribusi tidak boleh lebih besar dari saldo akhir',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>'',
                        'is_with_datatable'=>'',
                    ]);
                }

                if($totalAmountDistribution>0)
                {
                    if(date('Y-m-d')>Carbon::parse($data->end_date)->format('Y-m-d')){
                        $distributionDate=date('Y-m-d');
                    }else{
                        $distributionDate=Carbon::parse($data->end_date)->format('Y-m-d');
                    }
                    $detailDistribution=[];
                    foreach ($coaDistributionId as $distkey =>$dist)
                    {
                        $journalDistribution=JournalEntity::where('ref_code',$data->code)
                            ->where('ref_id',$coaDistributionId[$distkey])
                            ->where('is_deleted',0)
                            ->first();
                        $a=strtr($amountDistribution[$distkey], array('.' => '', ',' => '.'));

                        if($a>0){
                            if(is_null($journalDistribution)){
                                $journalDistribution=new JournalEntity();
                                $codeDistribution=CodeGenerator::generateJurnalCode(0,'JRN',merchant_id(),1);
                                $journalDistribution->trans_code=$codeDistribution;
                                $journalDistribution->ref_code=$data->code;
                                $journalDistribution->ref_id=$coaDistributionId[$distkey];
                                $journalDistribution->trans_name='Distribusi Ikhtisar Laba Rugi';
                                $journalDistribution->trans_time=''.$distributionDate.' 23:59:59';
                                $journalDistribution->trans_note='Distribusi Ikhtisar Laba Rugi';
                                $journalDistribution->trans_purpose='Distribusi Ikhtisar Laba Rugi';
                                $journalDistribution->trans_proof='Distribusi Ikhtisar Laba Rugi';
                                $journalDistribution->trans_type=0;
                                $journalDistribution->trans_amount=$a;
                                $journalDistribution->md_merchant_id=$request->md_merchant_id;
                                $journalDistribution->md_user_id_created=user_id();
                                $journalDistribution->timezone=$request->_timezone;
                                $journalDistribution->save();
                            }else{
                                $journalDistribution->trans_type=0;
                                $journalDistribution->trans_amount=$a;
                                $journalDistribution->md_merchant_id=$request->md_merchant_id;
                                $journalDistribution->md_user_id_created=user_id();
                                $journalDistribution->timezone=$request->_timezone;
                                $journalDistribution->save();

                                JurnalDetail::where('acc_jurnal_id',$journalDistribution->id)->delete();
                            }

                            array_push($detailDistribution,[
                                'acc_coa_detail_id'=>$coaDistributionId[$distkey],
                                'coa_type'=>($data->end_amount<0)?'Debit':'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$a,
                                'created_at'=>$journalDistribution->trans_time,
                                'updated_at'=>$journalDistribution->trans_time,
                                'acc_jurnal_id'=>$journalDistribution->id,
                                'ref_code'=>$journalDistribution->trans_code,
                            ]);


                            array_push($detailDistribution,[
                                'acc_coa_detail_id'=>$coaIkhtisar,
                                'coa_type'=>($data->end_amount<0)?'Kredit':'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$a,
                                'created_at'=>$journalDistribution->trans_time,
                                'updated_at'=>$journalDistribution->trans_time,
                                'acc_jurnal_id'=>$journalDistribution->id,
                                'ref_code'=>$journalDistribution->trans_code,
                            ]);

                        }

                    }

                    JurnalDetail::insert($detailDistribution);

                }

                DB::commit();

                return response()->json([
                    'message'=>'Distribusi Ikhtisar Laba Rugi berhasil disimpan!',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>\route('merchant.toko.acc.journal.closing.add').'?id='.$data->id.'&step=1',
                    'is_with_datatable'=>false,
                ]);




            }


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);

        }
    }


    public function delete( Request  $request)
    {
        try {
            DB::beginTransaction();

            $id=$request->id;
            $data=ClosingJournalDistribution::find($id);
            $data->is_deleted=1;
            $data->save();

            JournalEntity::where('ref_code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->update([
                    'is_deleted'=>1
                ]);

            DB::commit();

            return response()->json([
                'message'=>'Data berhasil dihapus!',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>'',
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);


        }
    }

    public function exportData(Request $request){
        try{
            $data=ClosingJurnalEntity::getDataForDataTable();

            $encode=json_decode($request->md_merchant_id);

            if(is_array($encode))
            {
                $filterMerchant=$encode;
            }else{
                $filterMerchant=json_decode($encode);
            }

            if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
                $getBranch=MerchantUtil::getBranch(merchant_id(),0);

                $data->whereIn('acc_closing_journal_distributions.md_merchant_id',$getBranch);

            }else{
                $data->whereIn('acc_closing_journal_distributions.md_merchant_id',$filterMerchant);

            }

            if($request->startDate!='-')
            {
                $data->whereRaw("
                acc_closing_journal_distributions.end_date::date between '$request->startDate' and '$request->endDate'
                ");
            }
            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $sd=date("Y-m-d", strtotime($request->startDate));
            $ed=date("Y-m-d", strtotime($request->endDate));
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Pencatatan Jurnal Penutup Tanggal $sd sampai $ed ");

            $header = [
                'No',
                'Tanggal',
                'Cabang',
                'Laba/Rugi',
                'Distribusi Akhir',
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $col++;
            }

            $num = 4;
            foreach ($data->get() as $key => $item) {
                $html="";
                if($item->distribusi_akhir!='[]' || !empty($item->distribusi_akhir)){
                    $itemB=json_decode($item->distribusi_akhir);
                    foreach ($itemB as $b){
                        $bItem=$b->parent_code.' - '.str_replace('.','',$b->code).' '.$b->name.' : ';
                        $html.="$bItem  ".rupiah($b->trans_amount)." , ";
                    }
                }
                $dataShow = [
                    $key+1,
                    Carbon::parse($item->tanggal)->isoFormat('dddd, D MMMM Y')
                    .' sd '.Carbon::parse($item->end_date)->isoFormat('dddd, D MMMM Y')
                    .' '.getTimeZoneName($item->_timezone),
                    $item->outlet,
                    ($item->laba_atau_rugi<0)?'('.rupiah(abs($item->laba_atau_rugi)).' )':rupiah($item->laba_atau_rugi),
                    $html

                ];
                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $col++;

                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-pencatatan-jurnal'.merchant_id()."_".date('Y-m-d').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <script>
                toastForSaveData('Jurnal Umum berhasil diexport!','success',true,'')
                 setTimeout(() => {
                        closeModal();
                    }, 3000)
            </script>
            ";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<script>toastForSaveData('Terjadi kesalahan, Data jurnal umum gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)

</script>";

        }
    }



}

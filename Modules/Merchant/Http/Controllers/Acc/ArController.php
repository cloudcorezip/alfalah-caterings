<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc;


use App\Classes\CollectionPagination;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\MasterData\User;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\PaymentUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use Modules\Merchant\Entities\Acc\MerchantArEntity;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\CoaMapping;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Supplier;
use App\Classes\Singleton\CodeGenerator;
use Illuminate\Support\Facades\Storage;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Image;
use Str;
use Illuminate\Support\Facades\DB;

class ArController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/journal/ar')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/detail/{id}', 'Acc\ArController@detail')
                    ->name('merchant.toko.acc.journal.ar.detail');
                 Route::post('/add', 'Acc\ArController@add')
                    ->name('merchant.toko.acc.journal.ar.add');
                Route::post('/add-detail/{arId}/{type}', 'Acc\ArController@addDetail')
                    ->name('merchant.toko.acc.journal.ar.add-detail');
                Route::post('/save', 'Acc\ArController@save')
                    ->name('merchant.toko.acc.journal.ar.save');
                Route::post('/save-detail/{type}', 'Acc\ArController@saveDetail')
                    ->name('merchant.toko.acc.journal.ar.save-detail');
                Route::post('/delete', 'Acc\ArController@delete')
                    ->name('merchant.toko.acc.journal.ar.delete');
                Route::post('/delete-giving', 'Acc\ArController@deleteGiving')
                    ->name('merchant.toko.acc.journal.ar.delete-giving');
                Route::post('/delete-payment', 'Acc\ArController@deletePayment')
                    ->name('merchant.toko.acc.journal.ar.delete-payment');
                Route::get('/ar-list', 'Acc\ArController@getArList')
                    ->name('merchant.toko.acc.journal.ar.ar-list');
            });
    }

    public function detail($id,Request  $request)
    {
        $data=MerchantAr::find($id);

        if(is_null($data)){
            abort(404);
        }
        $ar=MerchantArDetail::whereIn('type_action',[1,3,2])->where('acc_merchant_ar_id',$id)->with(["getAr" => function($q) use ($data){
            $q->where('md_merchant_id', '=', $data->md_merchant_id);
        }])->get();
        $params=[
            'title'=>'Detail',
            'data'=>$data,
            'ar'=>CollectionPagination::paginate($ar),
        ];

        return view('merchant::acc.journal.ar.detail',$params);

    }


    public function  getArList(Request  $request,$merchantId=null,$type=0)
    {
        try {
            $merchantId=(!is_null($merchantId))?$merchantId:$request->md_merchant_id;
            $data=CoaDetail::
            select([
                'acc_coa_details.id',
                'acc_coa_details.name as text',
                'c.md_merchant_id'
            ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)
                ->whereIn('acc_coa_details.is_deleted',[0,2])
                ->whereIn('c.name',['Piutang'])
                ->whereRaw("acc_coa_details.name not like '%Piutang Pembelian%'  and acc_coa_details.name not like '%Piutang Penjualan%'")
                ->orderBy('acc_coa_details.id','asc')
                ->get();
            if($type==0)
            {
                return response()->json($data);

            }else{
                return $data;
            }
        }catch (\Exception $e)
        {
            if($type==0)
            {
                return  response()->json([]);
            }else{
                return [];
            }

        }

    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=MerchantAr::find($id);
            $detJurnal=JurnalDetail::where('acc_jurnal_id',$id)->get();
            $arList=$this->getArList($request,$data->md_merchant_id,1);

        }else{
            $data=new MerchantArEntity();
            $detJurnal=null;
            $arList=null;
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'detJurnal' =>$detJurnal,
            'categoryOption'=>$arList
        ];

        return view('merchant::acc.journal.ar.form',$params);
    }

    public  function addDetail(Request $request,$arId,$type){

        $id=$request->id;
        if(!is_null($id)){

            $data=MerchantArDetail::find($id);
            $ar=MerchantAr::find($arId);
        }else{
            $data=new MerchantArDetail();
            $ar=MerchantAr::find($arId);
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'ar' =>$ar,
            'payment'=>PaymentUtils::getPayment($ar->md_merchant_id,2)

        ];
        if($type==1){
            return view('merchant::acc.journal.ar.form-ar',$params);
        }else if($type==2){
            return view('merchant::acc.journal.ar.form-ar-payment',$params);
        }

    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            date_default_timezone_set($request->_timezone);
            $date = new \DateTime($request->trans_time);

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'ar',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }
            if(!is_null($id)){

                $data=MerchantAr::find($id);
            }else{
                $data=new MerchantAr();
            }

            if($request->due_date<$request->trans_time){
                return response()->json([
                    'message'=>'Waktu jatuh tempo tidak boleh kurang dari tanggal transaksi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);

            }

            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->trans_proof;

                }
            }
            if(is_null($id))
            {
                $code=CodeGenerator::generalCode('ARC',$request->md_merchant_id);
            }else{
                $code=$data->ar_code;
            }
            $isFromSupplier=0;
            if($request->type==0){
                $isFromSupplier=1;
                $user=Supplier::find($request->trans_purpose);
            }else{
                $user=Customer::find($request->trans_purpose);
            }
            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
            $data->is_from_supplier=$isFromSupplier;
            $data->ref_user_id=$user->id;
            $data->arable_id=0;
            $data->arable_type="#";
            $data->ar_code=$code;
            $data->ar_name=$request->trans_name;
            $data->ar_note=$request->trans_note;
            $data->ar_purpose=$user->name;
            $data->ar_time=$request->trans_time;
            $data->ar_proof=$fileName;
            $data->ar_acc_coa=$request->acc_coa_category_id;
            $data->sync_id=$data->id.Uuid::uuid4()->toString();
            $data->md_merchant_id=$request->md_merchant_id;
            $data->timezone=$request->_timezone;
            $data->ref_user_id=$request->trans_purpose;
            $data->sc_customer_id=$request->trans_purpose;
            $data->due_date=$request->due_date;
            $data->save();

            DB::commit();
            return response()->json([
                'message'=>'Piutang usaha berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.acc.journal.ar.detail',['id'=>$data->id]),
            ]);
        }catch (\Exception $e)
        {

           DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);
        }
    }

    public function saveDetail(Request $request,$type)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $timestamp = date('Y-m-d H:i:s');
            if(!is_null($request->id)){
                Jurnal::where('external_ref_id',$request->id)->update(['is_deleted'=>1]);
            }
            $date = new \DateTime($request->paid_date);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }
            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'ar_detail',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/ar/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }
            $amount=strtr($request->amount, array('.' => '', ',' => '.'));
            $pay=CoaDetail::find($request->payment_acc_coa_detail_id);
            if($type==1){
                $jurnal=new Jurnal();
                $ar=MerchantAr::where('id',$request->ar_id)->first();

                if(is_null($request->id)){
                    $arDetail=new MerchantArDetail();
                    $ar->residual_amount+=$amount;
                    $ar->ar_amount+=$amount;
                    $arDetail->acc_merchant_ar_id=$ar->id;
                    $arDetail->paid_nominal=$amount;
                    $arDetail->paid_date=$request->paid_date;
                    $arDetail->note=$request->note;
                    $arDetail->timezone=$request->_timezone;
                    $arDetail->trans_proof=$fileName;
                    $arDetail->created_by=user_id();
                    $arDetail->supplier_from_or_to_desc=$pay->name;
                    $arDetail->ar_detail_code=CodeGenerator::generalCode('SAR',$request->md_merchant_id);;
                    $arDetail->ar_type="Pemberian Piutang Usaha melalui ".$pay->name;
                    $arDetail->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
                    $arDetail->type_action=1;
                    $arDetail->save();
                }else{
                    $arDetail=MerchantArDetail::find($request->id);
                    $ar->residual_amount-=$arDetail->paid_nominal;
                    $ar->ar_amount-=$arDetail->paid_nominal;
                    $ar->residual_amount+=$amount;
                    $ar->ar_amount+=$amount;
                    $arDetail->acc_merchant_ar_id=$ar->id;
                    $arDetail->paid_nominal=$amount;
                    $arDetail->paid_date=$request->paid_date;
                    $arDetail->note=$request->note;
                    $arDetail->timezone=$request->_timezone;
                    $arDetail->trans_proof=$fileName;
                    $arDetail->created_by=user_id();
                    $arDetail->supplier_from_or_to_desc=$pay->name;
                    $arDetail->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
                    $arDetail->type_action=1;
                    $arDetail->save();
                }
                if($ar->residual_amount>0){
                    $ar->is_paid_off=0;
                }
                $codeAp=is_null($ar->second_code)?$ar->ar_code:$ar->second_code;
                $codeReceived=is_null($arDetail->second_code)?$arDetail->ar_detail_code:$arDetail->second_code;

                $jurnal->ref_code=$arDetail->ar_detail_code;
                $jurnal->external_ref_id=$arDetail->id;
                $jurnal->trans_amount=$ar->ar_amount;
                $jurnal->trans_code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $jurnal->md_user_id_created=user_id();
                $jurnal->md_merchant_id=$request->md_merchant_id;
                $jurnal->trans_name='Pemberian Piutang Usaha Terhadap ' .$ar->ar_purpose.'-'.$codeAp.'-'.$codeReceived;
                $jurnal->trans_purpose='Pemberian Piutang Usaha Terhadap ' .$ar->ar_purpose.'-'.$codeAp.'-'.$codeReceived;
                $jurnal->trans_time=$request->paid_date;
                $jurnal->timezone=$request->_timezone;
                $jurnal->trans_amount=0;
                $jurnal->trans_type=6;
                $jurnal->save();
                $ar->save();
                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$request->ar_acc_coa;
                $detjurnal->coa_type="Debit";
                $detjurnal->amount=$amount;
                $detjurnal->ref_id=$arDetail->id;
                $detjurnal->ref_code=$ar->ar_code;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$timestamp;
                $detjurnal->updated_at=$timestamp;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$pay->id;
                $detjurnal->coa_type="Kredit";
                $detjurnal->amount=$amount;
                $detjurnal->ref_id=$arDetail->id;
                $detjurnal->ref_code=$ar->ar_code;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$timestamp;
                $detjurnal->updated_at=$timestamp;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();

            }else if($type==2){
                $ar=MerchantAr::where('id',$request->ar_id)->first();

                $residual_amount=$ar->residual_amount;
                if($amount>$residual_amount){

                    return response()->json([
                        'message'=>'Pembayaran tidak boleh melebihi jumlah piutang yang tersisa!',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false,
                    ]);
                }
                $jurnal=new Jurnal();

                if(is_null($request->id)){
                    $arDetail=new MerchantArDetail();
                    $ar->residual_amount-=$amount;
                    $arDetail->acc_merchant_ar_id=$ar->id;
                    $arDetail->paid_nominal=$amount;
                    $arDetail->trans_proof=$fileName;
                    $arDetail->ar_type="Pembayaran Piutang Usaha melalui ".$pay->name;
                    $arDetail->paid_date=$request->paid_date;
                    $arDetail->timezone=$request->_timezone;
                    $arDetail->note=$request->note;
                    $arDetail->created_by=user_id();
                    $arDetail->ar_detail_code=CodeGenerator::generalCode('XAR',$request->md_merchant_id);
                    $arDetail->type_action=2;
                    $arDetail->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
                    $arDetail->save();
                }else{
                    $arDetail=MerchantArDetail::find($request->id);
                    $ar->residual_amount+=$arDetail->paid_nominal;
                    $ar->residual_amount-=$amount;
                    $arDetail->trans_proof=$fileName;
                    $arDetail->acc_merchant_ar_id=$ar->id;
                    $arDetail->paid_nominal=$amount;
                    $arDetail->ar_type="Pembayaran Piutang Usaha melalui ".$pay->name;
                    $arDetail->paid_date=$request->paid_date;
                    $arDetail->note=$request->note;
                    $arDetail->type_action=2;
                    $arDetail->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
                    $arDetail->save();
                }

                $ar->paid_nominal=MerchantArDetail::where('acc_merchant_ar_id',$ar->id)->where('type_action',2)->sum('paid_nominal');
                if($ar->residual_amount==0){
                    $ar->is_paid_off=1;
                }

                $codeAp=is_null($ar->second_code)?$ar->ar_code:$ar->second_code;
                $codeReceived=is_null($arDetail->second_code)?$arDetail->ar_detail_code:$arDetail->second_code;

                $jurnal->ref_code=$arDetail->ar_detail_code;
                $jurnal->external_ref_id=$arDetail->id;
                $jurnal->trans_amount=$ar->residual_amount;
                $jurnal->trans_code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $jurnal->md_user_id_created=user_id();
                $jurnal->md_merchant_id=$request->md_merchant_id;
                $jurnal->trans_name='Pembayaran Piutang Usaha Oleh ' .$ar->ar_purpose.'-'.$codeAp.'-'.$codeReceived;
                $jurnal->trans_purpose='Pembayaran Piutang Usaha Oleh ' .$ar->ar_purpose.'-'.$codeAp.'-'.$codeReceived;
                $jurnal->trans_time=$request->paid_date;
                $jurnal->trans_amount=0;
                $jurnal->trans_type=6;
                $jurnal->timezone=$request->_timezone;
                $jurnal->save();
                $ar->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$pay->id;
                $detjurnal->coa_type="Debit";
                $detjurnal->amount=$amount;
                $detjurnal->ref_id=$arDetail->id;
                $detjurnal->ref_code=$ar->ar_code;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$timestamp;
                $detjurnal->updated_at=$timestamp;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$request->ar_acc_coa;
                $detjurnal->coa_type="Kredit";
                $detjurnal->amount=$amount;
                $detjurnal->ref_id=$arDetail->id;
                $detjurnal->ref_code=$ar->ar_code;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$timestamp;
                $detjurnal->updated_at=$timestamp;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();


            }

            DB::commit();
            return response()->json([
                'message'=>'Detail piutang usaha berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e){

           DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=MerchantAr::find($id);

            $date = new \DateTime($data->ar_time);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                'type'=>'warning',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);
            }
            $data->is_deleted=1;
            $data->save();
            MerchantArDetail::where('acc_merchant_ar_id',$id)->update(['is_deleted'=>1]);
            $arDetail=MerchantArDetail::where('acc_merchant_ar_id',$id)->get();
            foreach($arDetail as $key => $item){
                Jurnal::where([
                    'external_ref_id'=>$item->id,
                    'ref_code'=>$item->ar_detail_code,
                    'md_merchant_id'=>$data->md_merchant_id
                ])->update(['is_deleted'=>1]);
            }
            MerchantArDetail::where('acc_merchant_ar_id',$id)->delete();
            DB::commit();
            return response()->json([
                'message'=>'Piutang usaha berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);

        }
    }

    public function deleteGiving(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=MerchantArDetail::find($id);
            $arDetPayment=MerchantArDetail::where('acc_merchant_ar_id',$data->acc_merchant_ar_id)->where('type_action',2)->where('is_deleted',0)->get();
            if(count($arDetPayment)>0){
                return response()->json([
                    'message'=>'Sudah terjadi pembayaran piutang, data tidak dapat dihapus!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,
                ]);

            }
            $date = new \DateTime($data->paid_date);
            if(AccUtil::checkClosingJournal($data->getAr->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $ar=MerchantAr::find($data->acc_merchant_ar_id);

            if($ar->getDetail->where('type_action',2)->where('is_deleted',0)->sum('paid_nominal')
            >=$ar->getDetail->where('type_action',1)->where('is_deleted',0)->sum('paid_nominal')
            ){

                return response()->json([
                    'message'=>'Silahkan menghapus pelunasan pembayaran terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,
                ]);
            }
            if($data->is_deleted==0)
            {
                $data->is_deleted=1;
                $data->save();
                $data->getAr->residual_amount-=$data->paid_nominal;
                $data->getAr->ar_amount-=$data->paid_nominal;
                $data->getAr->save();
                Jurnal::where([
                    'external_ref_id'=>$data->id,
                    'ref_code'=>$data->ar_detail_code,
                    'md_merchant_id'=>$data->getAr->md_merchant_id
                ])->update(['is_deleted'=>1]);
            }
            DB::commit();
            return response()->json([
                'message'=>'Detail piutang usaha berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }

    }

    public function deletePayment(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=MerchantArDetail::find($id);
            if($data->is_deleted==0)
            {
                $data->is_deleted=1;
                $data->save();
                $data->getAr->residual_amount+=$data->paid_nominal;
                $data->getAr->paid_nominal-=$data->paid_nominal;
                $data->getAr->is_paid_off=0;
                $data->getAr->save();
                Jurnal::where([
                    'external_ref_id'=>$data->id,
                    'ref_code'=>$data->ar_detail_code,
                    'md_merchant_id'=>$data->getAr->md_merchant_id
                ])->update(['is_deleted'=>1]);
            }
            DB::commit();
            return response()->json([
                'message'=>'Detail pembayaran piutang usaha berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }

}

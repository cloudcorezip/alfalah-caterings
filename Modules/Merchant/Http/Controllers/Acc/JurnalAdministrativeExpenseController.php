<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaMapping;
use App\Models\Accounting\JurnalAdministrativeExpense;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantCashflow;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\SennaToko\Supplier;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\PaymentUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;
use Modules\Merchant\Entities\Acc\JournalEntity;
use Modules\Merchant\Entities\Acc\JurnalAdministrativeExpenseEntity;
use Modules\Merchant\Entities\Acc\JurnalOtherIncomeEntity;
use Image;
use Carbon\Carbon;

class JurnalAdministrativeExpenseController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/jurnal/administrative-expense')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\JurnalAdministrativeExpenseController@index')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.index');
                Route::post('/exportData', 'Acc\JurnalAdministrativeExpenseController@exportData')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.exportData');
                Route::post('/data-table', 'Acc\JurnalAdministrativeExpenseController@dataTable')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.datatable');
                Route::get('/add', 'Acc\JurnalAdministrativeExpenseController@add')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.add');
                Route::post('/save', 'Acc\JurnalAdministrativeExpenseController@save')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.save');
                Route::post('/delete', 'Acc\JurnalAdministrativeExpenseController@delete')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.delete');
                Route::post('/reload-data', 'Acc\JurnalAdministrativeExpenseController@reloadData')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.reload');
                Route::get('/cost-list', 'Acc\JurnalAdministrativeExpenseController@getCostList')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.cost-list');
                Route::get('/payment-list', 'Acc\JurnalAdministrativeExpenseController@getPaymentList')
                    ->name('merchant.toko.acc.jurnal.administrative-expense.payment-list');


            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);

        $params=[
            'title'=>'Pendapatan & Biaya',
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->endOfMonth()->toDateString(),
            'tableColumns'=>JurnalAdministrativeExpenseEntity::dataTableColumns(),
            'add'=>(get_role()==3)?JurnalAdministrativeExpenseEntity::add(true):
                JurnalAdministrativeExpenseEntity::add(true),
            'key_val'=>$request->key,
            'merchantId'=>[-1],
            'encodeMerchant'=>base64_encode(json_encode(MerchantUtil::getBranch(merchant_id(),1)))

        ];


        return view('merchant::acc.journal.administrative-expense.index',$params);

    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (JurnalAdministrativeExpenseEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }

        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;
        $params = [
            'title' => 'Pendapatan & Biaya',
            'searchKey' => $searchKey,
            'tableColumns'=>JurnalAdministrativeExpenseEntity::dataTableColumns(),
            'key_val'=>$request->key,
            'start_date'=>(is_null($request->startDate))?Carbon::now()->startOfMonth()->toDateString():$request->startDate,
            'end_date'=>(is_null($request->endDate))?Carbon::now()->endOfMonth()->toDateString():$request->endDate,
            'merchantId'=>$request->md_merchant_id,
            'encodeMerchant'=>base64_encode(json_encode($request->md_merchant_id))
        ];
        return view('merchant::acc.journal.administrative-expense.list',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return JurnalAdministrativeExpenseEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public  function add(Request $request){

        $id=$request->id;

        if(!is_null($id)){
            $data=JournalEntity::find($id);
            $costList=$this->getCostList($request,$data->md_merchant_id,1);
            $payment=PaymentUtils::getPayment($data->md_merchant_id,2);
        }else{
            $data=new JournalEntity();
            $costList=null;
            $payment=null;
        }

        $firstOption=JurnalDetail::where('acc_jurnal_id',$id)
            ->where('coa_type','Debit')
            ->orderBy('id','asc')
            ->first();
        if(!is_null($id))
        {
            $lastOption=JurnalDetail::where('acc_jurnal_id',$id)
                ->whereNotIn('acc_coa_detail_id',[merchant_detail_multi_branch($data->md_merchant_id)->coa_administration_bank_id,$firstOption->acc_coa_detail_id])
                ->orderBy('id','desc')
                ->first();
        }else{
            $lastOption=null;
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'supplier'=>SupplierEntity::listOption(),
            'costList'=>$costList,
            'payment'=>$payment,
            'firstOption'=>$firstOption,
            'lastOption'=>$lastOption
        ];


        return view('merchant::acc.journal.administrative-expense.form2',$params);
    }

    public function  getCostList(Request  $request,$merchantId=null,$type=0)
    {
        try {
            $merchantId=(!is_null($merchantId))?$merchantId:$request->md_merchant_id;
            $data=CoaDetail::
            select([
                'acc_coa_details.id',
                'acc_coa_details.name as text',
                'c.md_merchant_id'
            ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)
                ->whereIn('acc_coa_details.is_deleted',[0,2])
                ->whereIn('c.name',['Beban Operasional & Usaha','Beban Lainnya'])
                ->whereRaw("acc_coa_details.name not like '%Penyesuaian%'  and acc_coa_details.name not like '%Penyusutan%' and acc_coa_details.name not like '%Produksi%' and acc_coa_details.name not like '%Aset%' and acc_coa_details.name not like '%Diskon Penjualan%' ")
                ->orderBy('acc_coa_details.id','asc')
                ->get();
            if($type==0)
            {
                return response()->json($data);

            }else{
                return $data;
            }
        }catch (\Exception $e)
        {
            if($type==0)
            {
                return  response()->json([]);
            }else{
                return [];
            }

        }

    }

    public function getPaymentList(Request  $request)
    {
        try {
            $merchantId=$request->md_merchant_id;
            $data=collect(PaymentUtils::getPayment($merchantId,2));

            $result=[];
            foreach ($data as $key =>$item)
            {
                $childs=[];
                foreach (collect($item['data']) as $child)
                {
                    $childs[]=[
                        'id'=>$child->acc_coa_id,
                        'text'=>$child->name
                    ];
                }

                $result[]=[
                    "text"=>$item['name'],
                    'children'=>$childs
                ];
            }

            return response()->json($result);


        }catch (\Exception $e)
        {
            return  response()->json([]);

        }
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $date = new \DateTime($request->trans_time);
            $id=$request->id;

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'jurnal',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);

            }
            if(!is_null($id)){

                $data=JournalEntity::find($id);
                $cashflow=MerchantCashflow::find($data->external_ref_id);

                JurnalDetail::where('acc_jurnal_id',$data->id)->delete();

            }else{
                $data=new JournalEntity();
                $cashflow=new MerchantCashflow();

            }
            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/administrative-expense/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->trans_proof;

                }
            }

            if(is_null($id))
            {
                $data->trans_code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $data->ref_code=CodeGenerator::otherCode('EXP',$request->md_merchant_id,date('y'),date('m'),date('d'));
                $cashflow->code=$data->ref_code;
            }
            $mapping=CoaMapping::where('acc_coa_detail_id',$request->acc_coa_detail_id)->
            where('md_merchant_id',merchant_id())
                ->where('is_deleted',0)
                ->first();
            $trans_amount=strtr($request->trans_amount, array('.' => '', ',' => '.'));
            $admin_fee=strtr($request->admin_fee, array('.' => '', ',' => '.'));
            $cashflow->md_sc_cash_type_id=2;
            $cashflow->name=$request->trans_name;
            $cashflow->amount=$trans_amount;
            $cashflow->admin_fee=$admin_fee;
            $cashflow->md_merchant_id=$request->md_merchant_id;
            $cashflow->cashflow_coa_detail_id=$request->acc_coa_detail_id;
            $cashflow->to_cashflow_coa_detail_id=$request->payment_acc_coa_detail_id;
            $cashflow->note=$request->trans_note;
            $cashflow->is_deleted=0;
            $cashflow->created_by=user_id();
            $cashflow->save();


            $data->second_code=(is_null($request->code) || $request->code=='')?null:str_replace(' ','',$request->code);
            $code=(is_null($data->second_code))?$data->ref_code:$data->second_code;

            $data->trans_name=$request->trans_name.' '.$code;
            $data->trans_time=$date->format('Y-m-d H:i:s');
            $data->trans_note=$request->trans_note;
            $data->timezone=$request->_timezone;
            $data->trans_proof=$fileName;
            $data->trans_type=4;
            $data->trans_amount=$trans_amount;
            $data->admin_fee=$admin_fee;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->md_user_id_created=user_id();
            $data->ref_code=$cashflow->code;
            $data->coa_name=$mapping->name;
            $data->external_ref_id=$cashflow->id;
            if($request->sc_supplier_id!='-1'){
                $supplier=Supplier::find($request->sc_supplier_id);
                $data->sc_supplier_id=$request->sc_supplier_id;
                $data->trans_purpose=$supplier->name;
            }else{
                $data->trans_purpose='Internal';
            }
            $data->save();
            $adminFee=merchant_detail_multi_branch($request->md_merchant_id)->coa_administration_bank_id;
            if($admin_fee>0){
                $insert=[
                    [
                        'acc_coa_detail_id'=>$request->acc_coa_detail_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$trans_amount,
                        'created_at'=>$date->format('Y-m-d H:i:s'),
                        'updated_at'=>$date->format('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$data->id
                    ],
                    [
                        'acc_coa_detail_id'=>$adminFee,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$admin_fee,
                        'created_at'=>$date->format('Y-m-d H:i:s'),
                        'updated_at'=>$date->format('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$data->id
                    ],
                    [
                        'acc_coa_detail_id'=>$request->payment_acc_coa_detail_id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$trans_amount+$admin_fee,
                        'created_at'=>$date->format('Y-m-d H:i:s'),
                        'updated_at'=>$date->format('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$data->id
                    ],
                ];
            }else{
                $insert=[
                    [
                        'acc_coa_detail_id'=>$request->acc_coa_detail_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$trans_amount,
                        'created_at'=>$date->format('Y-m-d H:i:s'),
                        'updated_at'=>$date->format('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$data->id
                    ],
                    [
                        'acc_coa_detail_id'=>$request->payment_acc_coa_detail_id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$trans_amount,
                        'created_at'=>$date->format('Y-m-d H:i:s'),
                        'updated_at'=>$date->format('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$data->id
                    ],

                ];
            }
            //dd($insert);
            JurnalDetail::insert(
                $insert
                );

            DB::commit();
            return response()->json([
                'message'=>'Pencatatan pengeluaran berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.acc.jurnal.administrative-expense.index'),
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>'',
            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=JournalEntity::find($id);
            $date = new \DateTime($data->trans_time);

            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }else{
                $data->is_deleted=1;
                $data->save();
                return response()->json([
                    'message'=>'Pencatatan pengeluaran berhasil disimpan',
                    'type'=>'success',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }
        }catch (\Exception $e)
        {

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);

        }
    }

    public function exportData(Request $request){
        try{

            $data=JurnalAdministrativeExpenseEntity::getDataForDataTable();

            $encode=json_decode($request->md_merchant_id);

            if(is_array($encode))
            {
                $filterMerchant=$encode;
            }else{
                $filterMerchant=json_decode($encode);
            }

            if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
                $getBranch=MerchantUtil::getBranch(merchant_id(),0);

                $data->whereIn('acc_jurnals.md_merchant_id',$getBranch);
            }else{
                $data->whereIn('acc_jurnals.md_merchant_id',$filterMerchant);
            }

            if($request->startDate!='-')
            {
                $data->whereRaw("
                acc_jurnals.trans_time::date between '$request->startDate' and '$request->endDate'
                ");
            }
            if($request->trans_type!='-1'){

                $data->whereRaw("acc_jurnals.trans_type=".$request->trans_type."");
            }

        //dd($data);

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $sd=date("Y-m-d", strtotime($request->startDate));
            $ed=date("Y-m-d", strtotime($request->endDate));

            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Pendapatan & Biaya Tanggal $sd sampai $ed ");

            $header = [
                'No',
                'Kode',
                'Nama Transaksi',
                'Waktu Transaksi',
                'Asal Pendapatan / Pengeluaran',
                'Metode Pembayaran',
                'Nominal',
                'Biaya Lain-Lain',
                'Total',
                'Tipe',
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $col++;
            }


            $sumExpenses = 0;
            $sumSurplus = 0;
            $num = 4;
            foreach ($data->get() as $key => $item) {
                $d=$item->getDetail;
                $coa_name = $item->trans_type == 4 ? $d->where('coa_type','Debit')->where('acc_coa_detail_id','!=',merchant_detail_multi_branch($item->merchant_id)->coa_administration_bank_id)->first()->getCoa->name : $d->where('coa_type','Kredit')->first()->getCoa->name;
                $trans_method = $item->trans_type == 4 ? $d->where('coa_type','Kredit')->first()->getCoa->name : $d->where('coa_type','Debit')->where('acc_coa_detail_id','!=',merchant_detail_multi_branch($item->merchant_id)->coa_administration_bank_id)->first()->getCoa->name;
                $dataShow = [
                    $key+1,
                    $item->kode,
                    $item->nama_transaksi,
                    Carbon::parse($item->waktu_transaksi)->isoFormat('dddd, D MMMM Y'),
                    $coa_name,
                    $trans_method,
                    rupiah($item->trans_amount),
                    rupiah($item->admin_fee),
                    rupiah($item->trans_amount+$item->admin_fee),
                    ($item->trans_type==4)?'Biaya':'Pendapatan'
                ];
                if($item->trans_type==4) {
                    $sumExpenses += $item->jumlah_original;
                } else {
                    $sumSurplus += $item->jumlah_original;
                }
                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $col++;
                }
                $num++;
            }

            $rowDebit = $num+2;
            $rowTotal = $num+3;
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $rowDebit, "Total Pendapatan");
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $rowTotal, "Total Pengeluaran");
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $rowDebit, rupiah($sumSurplus));
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $rowTotal, rupiah($sumExpenses));


            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-pendapatan-biaya'.merchant_id()."_".date('Y-m-d').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL';</script>
           <script>
                toastForSaveData('Pengeluaran umum berhasil diexport!','success',true,'')
                 setTimeout(() => {
                        closeModal();
                    }, 3000)
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<script>toastForSaveData('Terjadi kesalahan, Data pengeluaran umum gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)

</script>";


        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc;


use App\Http\Controllers\Controller;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\MasterData\User;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Models\SennaToko\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use App\Models\Accounting\CoaMapping;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use Modules\Merchant\Entities\Toko\BranchEntity;
use Modules\Merchant\Http\Controllers\Acc\Report\CashflowController;

class CoaDetailController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/setting/coa-list')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\CoaDetailController@index')
                    ->name('merchant.toko.acc.setting.coa-list.index');

//                Route::get('/ajax-type-cashflow-format', 'Acc\CoaDetailController@getCoaCashflowFormat')
//                    ->name('merchant.toko.acc.setting.coa-list.ajax-type-cashflow-format');
//
                Route::post('/add', 'Acc\CoaDetailController@add')
                    ->name('merchant.toko.acc.setting.coa-list.add');
                Route::post('/save', 'Acc\CoaDetailController@save')
                    ->name('merchant.toko.acc.setting.coa-list.save');
                Route::post('/delete', 'Acc\CoaDetailController@delete')
                    ->name('merchant.toko.acc.setting.coa-list.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Daftar Akun',
            'add'=>(get_role()==2 || get_role()==3)?CoaDetailEntity::add(true):
                CoaDetailEntity::add(true),
            'key_val'=>$request->key,
            'datas'=>CoaDetailEntity::getDatas(),

        ];
        //dd($params['data']);
        return view('merchant::acc.setting.coa-list.index',$params);

    }

    public function add( Request  $request)
    {
        $id=$request->id;
        if(!is_null($id)){

            $data=CoaDetailEntity::find($id);
            $checkCashflowFormatDetails=CoaCashflowFormatDetail::where([
                'acc_coa_detail_id'=>$id,
                'is_deleted'=>0
            ])->first();
        }else{
            $data=new CoaDetailEntity();
            $checkCashflowFormatDetails=[];
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'categoryOption'=>CoaCategoryEntity::getData(),
            'checkCashflowFormatDetails'=>$checkCashflowFormatDetails
        ];

        return view('merchant::acc.setting.coa-list.form',$params);
    }

    public function save(Request  $request)
    {
        try{

            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $branch=get_cabang();
            $id=$request->id;
            $coaCategory=CoaCategory::find($request->acc_coa_category_id);
            if(!is_null($id)){

                $data=CoaDetailEntity::find($id);
                if($data->is_deleted==2)
                {
                    foreach ($branch as $b)
                    {

                        $category=CoaCategory::where('code',$coaCategory->code)
                            ->where('md_merchant_id',$b->id)->first();

                        if(!is_null($category))
                        {
                            $coaDetail=CoaDetail::where('acc_coa_category_id',$category->id)
                                ->where('name',$data->name)->first();
                            if(!is_null($coaDetail))
                            {
                                if($data->acc_coa_category_id!=$coaCategory->id)
                                {
                                    $coaDetail->code=CoaUtil::generateCoaDetail($category->id);
                                }
                                $coaDetail->name=$request->name;
                                $coaDetail->acc_coa_category_id=$category->id;
                                $coaDetail->md_sc_currency_id=1;
                                $coaDetail->type_coa=$request->type_coa;
                                $coaDetail->save();

//                                if($category->name=="Beban Operasional & Usaha" || $category->name=='Beban Lainnya' || $category->name=='Pendapatan Usaha' || $category->name=='Pendapatan Lainnya'){
//                                    $coa_map = new CoaMapping();
//                                    $coa_map->name=$request->name;
//                                    $coa_map->is_type=2;
//                                    $coa_map->acc_coa_detail_id=$coaDetail->id;
//                                    $coa_map->md_merchant_id=$category->md_merchant_id;
//                                    $coa_map->save();
//                                }

//                                if($request->is_include_cashflow==1)
//                                {
//                                    $coaFormat=CoaCashflowFormat::where('name',$cashFlowFormat->name)
//                                        ->where('md_merchant_id',$b->id)->first();
//                                    if(!is_null($coaFormat))
//                                    {
//                                        $checkCashflowFormatDetails=CoaCashflowFormatDetail::where([
//                                            'acc_coa_detail_id'=>$coaDetail->id,
//                                            'is_deleted'=>0
//                                        ])->first();
//
//                                        if(is_null($request->type_cashflow_format))
//                                        {
//                                            return response()->json([
//                                                'message'=>'Tipe Format Arus Kas tidak boleh kosong!',
//                                                'type'=>'warning',
//                                                'is_modal'=>true,
//                                                'redirect_url'=>'',
//                                                'is_with_datatable'=>false,
//                                            ]);
//                                        }
//                                        if(!is_null($checkCashflowFormatDetails))
//                                        {
//                                            $checkCashflowFormatDetails->acc_coa_detail_id=$coaDetail->id;
//                                            $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$coaFormat->id;
//                                            $checkCashflowFormatDetails->save();
//                                        }else{
//                                            $checkCashflowFormatDetails=new CoaCashflowFormatDetail();
//                                            $checkCashflowFormatDetails->acc_coa_detail_id=$coaDetail->id;
//                                            $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$coaFormat->id;
//                                            $checkCashflowFormatDetails->save();
//                                        }
//                                    }
//
//
//                                }else{
//                                    $checkCashflowFormatDetails=CoaCashflowFormatDetail::where([
//                                        'acc_coa_detail_id'=>$coaDetail->id,
//                                    ])->first();
//
//                                    if(!is_null($checkCashflowFormatDetails))
//                                    {
//                                        $checkCashflowFormatDetails->delete();
//                                    }
//                                }
                            }else{
                                $dataNew=new CoaDetailEntity();
                                $dataNew->code=CoaUtil::generateCoaDetail($category->id);
                                $dataNew->name=$request->name;
                                $dataNew->acc_coa_category_id=$category->id;
                                $dataNew->is_deleted=($b->id==merchant_id())?2:0;
                                $dataNew->md_sc_currency_id=1;
                                $dataNew->type_coa=$request->type_coa;
                                $dataNew->is_parent=($b->head_id==merchant_id())?1:0;
                                $dataNew->save();
//                                if($category->name=="Beban Operasional & Usaha" || $category->name=='Beban Lainnya' || $category->name=='Pendapatan Usaha' || $category->name=='Pendapatan Lainnya'){
//                                    $coa_map = new CoaMapping();
//                                    $coa_map->name=$request->name;
//                                    $coa_map->is_type=2;
//                                    $coa_map->acc_coa_detail_id=$dataNew->id;
//                                    $coa_map->md_merchant_id=$category->md_merchant_id;
//                                    $coa_map->save();
//                                }
//
//                                if($request->is_include_cashflow==1)
//                                {
//                                    $coaFormat=CoaCashflowFormat::where('name',$cashFlowFormat->name)
//                                        ->where('md_merchant_id',$b->id)->first();
//
//                                    if(!is_null($coaFormat))
//                                    {
//                                        $checkCashflowFormatDetails=CoaCashflowFormatDetail::where([
//                                            'acc_coa_detail_id'=>$dataNew->id,
//                                            'is_deleted'=>0
//                                        ])->first();
//
//                                        if(is_null($request->type_cashflow_format))
//                                        {
//                                            return response()->json([
//                                                'message'=>'Tipe Format Arus Kas tidak boleh kosong!',
//                                                'type'=>'warning',
//                                                'is_modal'=>true,
//                                                'redirect_url'=>'',
//                                                'is_with_datatable'=>false,
//                                            ]);
//                                        }
//                                        if(!is_null($checkCashflowFormatDetails))
//                                        {
//                                            $checkCashflowFormatDetails->acc_coa_detail_id=$dataNew->id;
//                                            $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$coaFormat->id;
//                                            $checkCashflowFormatDetails->save();
//                                        }else{
//                                            $checkCashflowFormatDetails=new CoaCashflowFormatDetail();
//                                            $checkCashflowFormatDetails->acc_coa_detail_id=$dataNew->id;
//                                            $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$coaFormat->id;
//                                            $checkCashflowFormatDetails->save();
//                                        }
//                                    }
//
//                                }



                            }

                        }

                    }
                }
            }else{
                foreach ($branch as $b)
                {
                    $category=CoaCategory::where('code',$coaCategory->code)
                        ->where('md_merchant_id',$b->id)->first();
                    if(!is_null($category))
                    {
                        $data=new CoaDetailEntity();
                        $data->code=CoaUtil::generateCoaDetail($category->id);
                        $data->name=$request->name;
                        $data->acc_coa_category_id=$category->id;
                        $data->is_deleted=($b->id==merchant_id())?2:0;
                        $data->md_sc_currency_id=1;
                        $data->type_coa=$request->type_coa;
                        $data->is_parent=($b->head_id==merchant_id())?1:0;
                        $data->save();
//                        if($category->name=="Beban Operasional & Usaha" || $category->name=='Beban Lainnya' || $category->name=='Pendapatan Usaha' || $category->name=='Pendapatan Lainnya'){
//                            $coa_map = new CoaMapping();
//                            $coa_map->name=$request->name;
//                            $coa_map->is_type=2;
//                            $coa_map->acc_coa_detail_id=$data->id;
//                            $coa_map->md_merchant_id=$category->md_merchant_id;
//                            $coa_map->save();
//                        }
//
//                        if($request->is_include_cashflow==1)
//                        {
//                            $coaFormat=CoaCashflowFormat::where('name',$cashFlowFormat->name)
//                                ->where('md_merchant_id',$b->id)->first();
//                            if(!is_null($coaFormat))
//                            {
//                                $checkCashflowFormatDetails=CoaCashflowFormatDetail::where([
//                                    'acc_coa_detail_id'=>$data->id,
//                                    'is_deleted'=>0
//                                ])->first();
//
//                                if(is_null($request->type_cashflow_format))
//                                {
//                                    return response()->json([
//                                        'message'=>'Tipe Format Arus Kas tidak boleh kosong!',
//                                        'type'=>'warning',
//                                        'is_modal'=>true,
//                                        'redirect_url'=>'',
//                                        'is_with_datatable'=>false,
//                                    ]);
//                                }
//                                if(!is_null($checkCashflowFormatDetails))
//                                {
//                                    $checkCashflowFormatDetails->acc_coa_detail_id=$data->id;
//                                    $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$coaFormat->id;
//                                    $checkCashflowFormatDetails->save();
//                                }else{
//                                    $checkCashflowFormatDetails=new CoaCashflowFormatDetail();
//                                    $checkCashflowFormatDetails->acc_coa_detail_id=$data->id;
//                                    $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$coaFormat->id;
//                                    $checkCashflowFormatDetails->save();
//                                }
//                            }
//
//                        }else{
//                            $checkCashflowFormatDetails=CoaCashflowFormatDetail::where([
//                                'acc_coa_detail_id'=>$data->id,
//                            ])->first();
//                            if(!is_null($checkCashflowFormatDetails))
//                            {
//                                $checkCashflowFormatDetails->delete();
//                            }
//                        }

                    }

                }

            }
            DB::commit();

            return response()->json([
                'message'=>'Daftar Akun berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $checkPayment=PaymentMethod::where('acc_coa_id',$id)->first();
            if(!is_null($checkPayment)){
                return response()->json([
                    'message'=>'Akun telah terkait dengan metode pembayaran!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);
            }

            $data=CoaDetailEntity::find($id);
            $category=CoaCategory::find($data->acc_coa_category_id);
            $coaDelete=[];
            foreach (get_cabang() as $b)
            {
                $categoryDetail=CoaCategory::where('code',$category->code)
                    ->where('md_merchant_id',$b->id)->first();
                if(!is_null($categoryDetail))
                {
                    $d=CoaDetail::where('acc_coa_category_id',$categoryDetail->id)
                        ->where('name',$data->name)->first();
                    if($d)
                    {
                        $coaDelete[]=$d->id;
                    }
                }
            }

            CoaDetail::whereIn('id',$coaDelete)->delete();

            DB::commit();
            return response()->json([
                'message'=>'Daftar Akun berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }

//    public function getCoaCashflowFormat(Request  $request)
//    {
//        $data=CoaCashflowFormat::where('md_merchant_id',merchant_id())
//            ->where('type',$request->type)->get();
//        $result=[];
//
//        foreach ($data as $item)
//        {
//            $result[]=[
//                'id'=>$item->id,
//                'text'=>CashflowController::replaceNameOfReport($item->name)
//            ];
//        }
//
//        return response()->json($result);
//    }


}

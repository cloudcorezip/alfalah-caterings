<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc;


use App\Classes\CollectionPagination;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\MasterData\User;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\PaymentUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use Modules\Merchant\Entities\Acc\MerchantApEntity;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\CoaMapping;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\SennaToko\Supplier;
use App\Models\SennaToko\Customer;
use App\Classes\Singleton\CodeGenerator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\ApEntity;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use Image;
use Str;

use Illuminate\Support\Facades\DB;

class ApController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/journal/ap')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\ApController@index')
                    ->name('merchant.toko.acc.journal.ap.index');
                Route::get('/detail/{id}', 'Acc\ApController@detail')
                    ->name('merchant.toko.acc.journal.ap.detail');
                Route::post('/data-table', 'Acc\ApController@dataTable')
                    ->name('merchant.toko.acc.journal.ap.datatable');
                Route::post('/add', 'Acc\ApController@add')
                    ->name('merchant.toko.acc.journal.ap.add');
                Route::post('/add-detail/{apId}/{type}', 'Acc\ApController@addDetail')
                    ->name('merchant.toko.acc.journal.ap.add-detail');
                Route::post('/save', 'Acc\ApController@save')
                    ->name('merchant.toko.acc.journal.ap.save');
                Route::post('/save-detail/{type}', 'Acc\ApController@saveDetail')
                    ->name('merchant.toko.acc.journal.ap.save-detail');
                Route::post('/delete', 'Acc\ApController@delete')
                    ->name('merchant.toko.acc.journal.ap.delete');
                Route::post('/delete-giving', 'Acc\ApController@deleteGiving')
                    ->name('merchant.toko.acc.journal.ap.delete-giving');
                Route::post('/delete-payment', 'Acc\ApController@deletePayment')
                    ->name('merchant.toko.acc.journal.ap.delete-payment');
                Route::post('/reload-data', 'Acc\ApController@reloadData')
                    ->name('merchant.toko.acc.journal.ap.reload');
                Route::post('/exportData', 'Acc\ApController@exportData')
                    ->name('merchant.toko.acc.journal.ap.exportData');

                Route::get('/ap-list', 'Acc\ApController@getApList')
                    ->name('merchant.toko.acc.journal.ap.ap-list');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Utang & Piutang',
            'tableColumns'=>MerchantApEntity::dataTableColumns(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'supplier'=>SupplierEntity::listOption(),
            'add'=>(get_role()==3)?MerchantApEntity::add(true):
                MerchantApEntity::add(true),
            'key_val'=>$request->key,
            'merchantId'=>[-1],
            'encodeMerchant'=>base64_encode(json_encode(MerchantUtil::getBranch(merchant_id()))),
            'ref_user_id'=>'-1',
            'type_person'=>'-1'
        ];

        return view('merchant::acc.journal.ap.index',$params);

    }

    public function detail($id,Request  $request)
    {
        $data=MerchantAp::find($id);

        if(is_null($data)){
            abort(404);
        }

        $ap=MerchantApDetail::whereIn('type_action',[1,3,2])
            ->where('is_deleted',0)
            ->where('acc_merchant_ap_id',$id)->with(["getAp" => function($q)use($data){
            $q->where('md_merchant_id', '=', $data->md_merchant_id);
        }])->get();

        $params=[
            'title'=>'Detail',
            'data'=>$data,
            'ap'=>CollectionPagination::paginate($ap),
        ];


        return view('merchant::acc.journal.ap.detail',$params);

    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Utang & Piutang',
            'searchKey' => $searchKey,
            'tableColumns'=>MerchantApEntity::dataTableColumns(),
            'start_date'=>(is_null($request->startDate))?Carbon::now()->startOfMonth()->toDateString():$request->startDate,
            'end_date'=>(is_null($request->endDate))?Carbon::now()->endOfMonth()->toDateString():$request->endDate,
            'merchantId'=>$request->md_merchant_id,
            'encodeMerchant'=>base64_encode(json_encode($request->md_merchant_id)),
            'ref_user_id'=>$request->ref_user_id,
            'type_person'=>$request->type_person,
        ];
        return view('merchant::acc.journal.ap.list',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return MerchantApEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=MerchantAp::find($id);
            $detJurnal=JurnalDetail::where('acc_jurnal_id',$id)->get();
            $apList=$this->getApList($request,$data->md_merchant_id,1);
        }else{
            $data=new MerchantApEntity();
            $detJurnal=null;
            $apList=null;
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'supplier'=>SupplierEntity::listOption(),
            'detJurnal' =>$detJurnal,
            'categoryOption'=>$apList
        ];
        return view('merchant::acc.journal.ap.form',$params);
    }

    public function  getApList(Request  $request,$merchantId=null,$type=0)
    {
        try {
            $merchantId=(!is_null($merchantId))?$merchantId:$request->md_merchant_id;
            $data=CoaDetail::
            select([
                'acc_coa_details.id',
                'acc_coa_details.name as text',
                'c.md_merchant_id'
            ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)
                ->whereIn('acc_coa_details.is_deleted',[0,2])
                ->whereIn('c.name',['Utang'])
                ->whereRaw("acc_coa_details.name not like '%Utang Saldo Deposit%'  and acc_coa_details.name not like '%Utang Pembelian Barang%' and acc_coa_details.name not like '%Utang Pembelian Aset%' and acc_coa_details.name not like '%Utang Konsinyasi%'")
                ->orderBy('acc_coa_details.id','asc')
                ->get();
            if($type==0)
            {
                return response()->json($data);

            }else{
                return $data;
            }
        }catch (\Exception $e)
        {
            if($type==0)
            {
                return  response()->json([]);
            }else{
                return [];
            }

        }

    }


    public  function addDetail(Request $request,$apId,$type){

        $id=$request->id;
        if(!is_null($id)){

            $data=MerchantApDetail::find($id);
            $ap=MerchantAp::find($apId);
        }else{
            $data=new MerchantApDetail();
            $ap=MerchantAp::find($apId);
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'supplier'=>SupplierEntity::listOption(),
            'ap' =>$ap,
            'payment'=>PaymentUtils::getPayment($ap->md_merchant_id,2)
        ];

        if($type==1){
            return view('merchant::acc.journal.ap.form-ap',$params);
        }else if($type==2){
            return view('merchant::acc.journal.ap.form-ap-payment',$params);
        }

    }

    public function save(Request $request)
    {
        try{

            DB::beginTransaction();
            $id=$request->id;
            date_default_timezone_set($request->_timezone);
            $date = new \DateTime($request->trans_time);
            if(!is_null($id)){
                $data=MerchantAp::find($id);
            }else{
                $data=new MerchantAp();
            }

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'ap',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            //dd($request->all());
            if($request->due_date<$request->trans_time){
                return response()->json([
                    'message'=>'Waktu jatuh tempo tidak boleh kurang dari tanggal transaksi',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);

            }
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }

            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->trans_proof;
                }
            }
            if(is_null($id))
            {
                $code=CodeGenerator::generalCode('APC',$request->md_merchant_id);

            }else{
                $code=$data->ap_code;
            }
            $isFromCustomer=0;
            if($request->type==1){
                $isFromCustomer=1;
                $user=Customer::find($request->trans_purpose);
            }else{
                $user=Supplier::find($request->trans_purpose);
            }
            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
            $data->is_from_customer=$isFromCustomer;
            $data->ref_user_id=$user->id;
            $data->apable_id=0;
            $data->apable_type="#";
            $data->ap_code=$code;
            $data->ap_name=$request->trans_name;
            $data->ap_note=$request->trans_note;
            $data->ap_purpose=$user->name;
            $data->ap_time=$request->trans_time;
            $data->ap_proof=$fileName;
            $data->ap_acc_coa=$request->acc_coa_category_id;
            $data->sync_id=$data->id.Uuid::uuid4()->toString();
            $data->md_merchant_id=$request->md_merchant_id;
            $data->timezone=$request->_timezone;
            $data->ref_user_id=$request->trans_purpose;
            $data->sc_supplier_id=$request->trans_purpose;
            $data->due_date=$request->due_date;
            $data->save();

            DB::commit();
            return response()->json([
                'message'=>'Utang usaha berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.acc.journal.ap.detail',['id'=>$data->id]),
            ]);
        }catch (\Exception $e)
        {
           DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);

        }
    }

    public function saveDetail(Request $request,$type)
    {
        try{
            //dd($request->all());

            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $timestamp = date('Y-m-d H:i:s');
            if(!is_null($request->id)){
                Jurnal::where('external_ref_id',$request->id)->update(['is_deleted'=>1]);
            }

            $date = new \DateTime($request->paid_date);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }
            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'ap_detail',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $adminFee=merchant_detail_multi_branch($request->md_merchant_id)->coa_administration_bank_id;

            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/ar/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }
            $amount=strtr($request->amount, array('.' => '', ',' => '.'));
            if($type==1){
                $jurnal=new Jurnal();
                $ap=MerchantAp::where('id',$request->ap_id)->first();
                $pay=CoaDetail::find($request->payment_acc_coa_detail_id);

                if(is_null($request->id)){
                    $apDetail=new MerchantApDetail();
                    $ap->residual_amount+=$amount;
                    $ap->ap_amount+=$amount;
                    $apDetail->acc_merchant_ap_id=$ap->id;
                    $apDetail->paid_nominal=$amount;
                    $apDetail->paid_date=$request->paid_date;
                    $apDetail->note=$request->note;
                    $apDetail->timezone=$request->_timezone;
                    $apDetail->trans_proof=$fileName;
                    $apDetail->created_by=user_id();
                    $apDetail->supplier_from_or_to_desc=$pay->name;
                    $apDetail->ap_detail_code=CodeGenerator::generalCode('SAP',$request->md_merchant_id);
                    $apDetail->ap_type="Penerimaan Utang Usaha dari ".$pay->name;
                    $apDetail->type_action=1;
                    $apDetail->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
                    $apDetail->save();
                }else{
                    $apDetail=MerchantApDetail::find($request->id);
                    $ap->residual_amount-=$apDetail->paid_nominal;
                    $ap->ap_amount-=$apDetail->paid_nominal;
                    $ap->residual_amount+=$amount;
                    $ap->ap_amount+=$amount;
                    $apDetail->acc_merchant_ap_id=$ap->id;
                    $apDetail->paid_nominal=$amount;
                    $apDetail->paid_date=$request->paid_date;
                    $apDetail->note=$request->note;
                    $apDetail->timezone=$request->_timezone;
                    $apDetail->trans_proof=$fileName;
                    $apDetail->ap_type="Penerimaan Utang Usaha dari ".$pay->name;
                    $apDetail->type_action=1;
                    $apDetail->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
                    $apDetail->save();
                }
                if($ap->residual_amount>0){
                    $ap->is_paid_off=0;
                }
                $ap->save();
                $codeAp=is_null($ap->second_code)?$ap->ap_code:$ap->second_code;
                $codeReceived=is_null($apDetail->second_code)?$apDetail->ap_detail_code:$apDetail->second_code;
                $jurnal->external_ref_id=$apDetail->id;
                $jurnal->ref_code=$apDetail->ap_detail_code;
                $jurnal->trans_amount=$ap->ap_amount;
                $jurnal->trans_code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $jurnal->md_user_id_created=user_id();
                $jurnal->md_merchant_id=$request->md_merchant_id;
                $jurnal->trans_name='Pemberian Utang Usaha Oleh ' .$ap->ap_purpose.'-'.$codeAp.'-'.$codeReceived;
                $jurnal->trans_purpose='Pemberian Utang Usaha Oleh ' .$ap->ap_purpose.'-'.$codeAp.'-'.$codeReceived;
                $jurnal->trans_time=$request->paid_date;
                $jurnal->timezone=$request->_timezone;
                $jurnal->trans_type=5;
                $jurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$request->payment_acc_coa_detail_id;
                $detjurnal->coa_type="Debit";
                $detjurnal->amount=$amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$timestamp;
                $detjurnal->updated_at=$timestamp;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$request->ap_acc_coa;
                $detjurnal->coa_type="Kredit";
                $detjurnal->amount=$amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$timestamp;
                $detjurnal->updated_at=$timestamp;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();


            }else if($type==2){

                $jurnal=new Jurnal();
                $ap=MerchantAp::where('id',$request->ap_id)->first();
                $pay=CoaDetail::find($request->payment_acc_coa_detail_id);
                $residual_amount=$ap->residual_amount;

                if($amount>$residual_amount){
                    return response()->json([
                        'message'=>'Pembayaran tidak boleh melebihi jumlah hutang yang tersisa!',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false,
                    ]);
                }

                if(is_null($request->id)){
                    $apDetail=new MerchantApDetail();
                    $ap->residual_amount-=$amount;
                    $apDetail->acc_merchant_ap_id=$ap->id;
                    $apDetail->paid_nominal=$amount;
                    $apDetail->ap_type='Pembayaran Utang Usaha dengan '.$pay->name;
                    $apDetail->paid_date=$request->paid_date;
                    $apDetail->note=$request->note;
                    $apDetail->timezone=$request->_timezone;
                    $apDetail->created_by=user_id();
                    $apDetail->trans_proof=$fileName;
                    $apDetail->supplier_from_or_to_desc=$pay->name;
                    $apDetail->ap_detail_code=CodeGenerator::generalCode('XAP',$request->md_merchant_id);
                    $apDetail->type_action=2;
                    $apDetail->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
                    $apDetail->save();
                }else{
                    $apDetail=MerchantApDetail::find($request->id);
                    $ap->residual_amount+=$apDetail->paid_nominal;
                    $ap->residual_amount-=$amount;
                    $apDetail->acc_merchant_ap_id=$ap->id;
                    $apDetail->paid_nominal=$amount;
                    $apDetail->ap_type='Pembayaran Utang Usaha dengan '.$pay->name;
                    $apDetail->paid_date=$request->paid_date;
                    $apDetail->note=$request->note;
                    $apDetail->trans_proof=$fileName;
                    $apDetail->supplier_from_or_to_desc=$pay->name;
                    $apDetail->type_action=2;
                    $apDetail->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
                    $apDetail->save();
                }

                $ap->paid_nominal=MerchantApDetail::where('acc_merchant_ap_id',$ap->id)->where('type_action',2)->sum('paid_nominal');
                if($ap->residual_amount==0){
                    $ap->is_paid_off=1;
                }
                $ap->save();

                $codeAp=is_null($ap->second_code)?$ap->ap_code:$ap->second_code;
                $codeReceived=is_null($apDetail->second_code)?$apDetail->ap_detail_code:$apDetail->second_code;

                $admin_fee=(is_null($request->admin_fee) || $request->admin_fee=='')?0:strtr($request->admin_fee, array('.' => '', ',' => '.'));

                $jurnal->external_ref_id=$apDetail->id;
                $jurnal->ref_code=$apDetail->ap_detail_code;
                $jurnal->trans_amount=$amount+$admin_fee;
                $jurnal->trans_code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $jurnal->md_user_id_created=user_id();
                $jurnal->md_merchant_id=$request->md_merchant_id;
                $jurnal->trans_name='Pembayaran Utang Usaha Terhadap ' .$ap->ap_purpose.'-'.$codeAp.'-'.$codeReceived;
                $jurnal->trans_purpose='Pembayaran Utang Usaha Terhadap ' .$ap->ap_purpose.'-'.$codeAp.'-'.$codeReceived;
                $jurnal->trans_time=$request->paid_date;
                $jurnal->timezone=$request->_timezone;
                $jurnal->trans_type=5;
                $jurnal->save();
                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$request->ap_acc_coa;
                $detjurnal->coa_type="Debit";
                $detjurnal->amount=$amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$timestamp;
                $detjurnal->updated_at=$timestamp;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$request->payment_acc_coa_detail_id;
                $detjurnal->coa_type="Kredit";
                $detjurnal->amount=$amount+$admin_fee;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$timestamp;
                $detjurnal->updated_at=$timestamp;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();
                if($admin_fee>0){
                    $detjurnal=new JurnalDetail();
                    $detjurnal->acc_coa_detail_id=$adminFee;
                    $detjurnal->coa_type="Debit";
                    $detjurnal->amount=$admin_fee;
                    $detjurnal->acc_jurnal_id=$jurnal->id;
                    $detjurnal->created_at=$timestamp;
                    $detjurnal->updated_at=$timestamp;
                    $detjurnal->md_sc_currency_id=1;
                    $detjurnal->save();
                }
            }

            DB::commit();
            return response()->json([
                'message'=>'Detail utang usaha berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e){
           DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }


    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=MerchantAp::find($id);
            $date = new \DateTime($data->ap_time);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }
            $data->is_deleted=1;
            $data->save();
            MerchantApDetail::where('acc_merchant_ap_id',$id)->update(['is_deleted'=>1]);
            $apDetail=MerchantApDetail::where('acc_merchant_ap_id',$id)->get();
            foreach($apDetail as $key => $item){
                Jurnal::where([
                    'external_ref_id'=>$item->id,
                    'ref_code'=>$item->ap_detail_code,
                    'md_merchant_id'=>$data->md_merchant_id
                ])->update(['is_deleted'=>1]);
            }
            MerchantApDetail::where('acc_merchant_ap_id',$id)->delete();
            DB::commit();

            return response()->json([
                'message'=>'Utang usaha berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);

        }
    }

    public function deleteGiving(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=MerchantApDetail::find($id);
            $apDetPayment=MerchantApDetail::where('acc_merchant_ap_id',$data->acc_merchant_ap_id)->where('type_action',2)->where('is_deleted',0)->get();
            if(count($apDetPayment)>0){
                return response()->json([
                    'message'=>'Sudah terjadi pembayaran hutang, data tidak dapat dihapus!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,
                ]);

            }
            $date = new \DateTime($data->paid_date);
            if(AccUtil::checkClosingJournal($data->getAp->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $ap=MerchantAp::find($data->acc_merchant_ap_id);

            if($ap->getDetail->where('type_action',2)->where('is_deleted',0)->sum('paid_nominal')
            >=$ap->getDetail->where('type_action',1)->where('is_deleted',0)->sum('paid_nominal')
            ){

                return response()->json([
                    'message'=>'Silahkan menghapus pelunasan pembayaran terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,
                ]);

            }
            if($data->is_deleted==0)
            {
                $data->is_deleted=1;
                $data->save();
                $data->getAp->residual_amount-=$data->paid_nominal;
                $data->getAp->ap_amount-=$data->paid_nominal;
                $data->getAp->save();
                Jurnal::where([
                    'external_ref_id'=>$data->id,
                    'ref_code'=>$data->ap_detail_code,
                    'md_merchant_id'=>$data->getAp->md_merchant_id
                ])->update(['is_deleted'=>1]);
            }
            DB::commit();
            return response()->json([
                'message'=>'Detail utang usaha berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);


        }

    }

    public function deletePayment(Request  $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=MerchantApDetail::find($id);

            $date = new \DateTime($data->paid_date);
            if(AccUtil::checkClosingJournal($data->getAp->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            if($data->is_deleted==0)
            {
                $data->is_deleted=1;
                $data->save();
                $data->getAp->residual_amount+=$data->paid_nominal;
                $data->getAp->paid_nominal-=$data->paid_nominal;
                $data->getAp->is_paid_off=0;
                $data->getAp->save();
                Jurnal::where([
                    'external_ref_id'=>$data->id,
                    'ref_code'=>$data->ap_detail_code,
                    'md_merchant_id'=>$data->getAp->md_merchant_id
                ])->update(['is_deleted'=>1]);
            }
            DB::commit();
            return response()->json([
                'message'=>'Detail pembayaran utang usaha berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }

    public function exportData(Request $request){
        try{

            $startDate=$request->startDate;
            $endDate=$request->endDate;
            $refUserId=$request->ref_user_id;
            $typePerson=$request->type_person;

            if($refUserId!='-1' || $refUserId!=-1)
            {
                $searchUser="and ref_user_id=$refUserId";
                $searchType=($typePerson==0)?"and tipe_person='supplier'":"and tipe_person='customer'";
            }else{
                $searchUser='';
                $searchType='';
            }

            $encode=$request->md_merchant_id;
            if(is_array($encode))
            {
                $filterMerchant=$encode;
            }else{
                $filterMerchant=json_decode($encode);
            }
            if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
                $getBranch=MerchantUtil::getBranch(merchant_id(),0);
                $merchantList="(";
                foreach ($getBranch as $key => $item)
                {
                    $merchantList.=($key==0)?$item:",".$item;
                }
                $merchantList.=")";

            }else{
                $merchantList="(";
                foreach ($filterMerchant as $key => $item)
                {
                    $merchantList.=($key==0)?$item:",".$item;
                }
                $merchantList.=")";
            }

            $data=MerchantApEntity::getDataForDataTable($startDate,$endDate,'',$merchantList,$searchUser,$searchType);

            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $sd=date("Y-m-d", strtotime($startDate));
            $ed=date("Y-m-d", strtotime($endDate));
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Pencatatan Utang Tanggal $sd sampai $ed ");

            $header = [
                'No',
                'Kode',
                'Nama Transaksi',
                'Waktu Transaksi',
                'Status',
                'Jatuh Tempo',
                'Jumlah',
                'Cabang',
                'Tipe',
                'Nama Penerima/ Pemberi',
                'Dari',
                'Sisa Utang/Piutang',
                'Terbayarkan'
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $col++;
            }

            $num = 4;
            foreach ($data as $key => $item) {
                if($item->status == 0 && $item->jumlah_original == 0){
                    $status="Belum Ada Utang/Piutang";
                }else if($item->status == 0 && $item->jumlah_original >= 0){
                    $status="Belum Lunas";
                }else{
                    $status="Lunas";
                }
                $dataShow = [
                    $key+1,
                    $item->kode,
                    $item->nama_transaksi,
                    Carbon::parse($item->waktu_transaksi)->isoFormat('dddd, D MMMM Y').' '.getTimeZoneName($item->timezone),
                    $status,
                    Carbon::parse($item->jatuh_tempo)->isoFormat('dddd, D MMMM Y'),
                    rupiah($item->jumlah_original),
                    $item->outlet,
                    $item->tipe,
                    $item->nama_pemberi_atau_penerima,
                    $item->tipe_person,
                    rupiah($item->residual_amount),
                    rupiah($item->paid_nominal)
                ];
                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-utang-usaha'.merchant_id()."_".date('Y-m-d').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL';</script>
           <script>
                toastForSaveData('Utang/piutang berhasil diexport!','success',true,'')

            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<script>toastForSaveData('Terjadi kesalahan, Data utang/piutang gagal diexport!','danger',true,'')


</script>";

        }
    }


}

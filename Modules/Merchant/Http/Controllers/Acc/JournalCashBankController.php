<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc;


use App\Http\Controllers\Controller;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaMapping;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\MasterData\User;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use Modules\Merchant\Entities\Acc\JournalEntity;
use Modules\Merchant\Entities\Acc\JurnalCashBankEntity;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\SennaToko\Supplier;
use App\Classes\Singleton\CodeGenerator;
use Illuminate\Support\Facades\Storage;
use Image;
use Str;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class JournalCashBankController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/journal/cash-bank')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\JournalCashBankController@index')
                    ->name('merchant.toko.acc.journal.cash-bank.index');
                Route::get('/get-coa', 'Acc\JournalCashBankController@getCoa')
                    ->name('merchant.toko.acc.journal.cash-bank.get-coa');
                Route::post('/data-table', 'Acc\JournalCashBankController@dataTable')
                    ->name('merchant.toko.acc.journal.cash-bank.datatable');
                Route::get('/add', 'Acc\JournalCashBankController@add')
                    ->name('merchant.toko.acc.journal.cash-bank.add');
                Route::post('/save', 'Acc\JournalCashBankController@save')
                    ->name('merchant.toko.acc.journal.cash-bank.save');
                Route::post('/delete', 'Acc\JournalCashBankController@delete')
                    ->name('merchant.toko.acc.journal.cash-bank.delete');
                Route::post('/reload-data', 'Acc\JournalCashBankController@reloadData')
                    ->name('merchant.toko.acc.journal.cash-bank.reload');
                Route::post('/exportData', 'Acc\JournalCashBankController@exportData')
                    ->name('merchant.toko.acc.journal.cash-bank.exportData');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Kas & Bank',
            'tableColumns'=>JurnalCashBankEntity::dataTableColumns(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'add'=>(get_role()==3)?JurnalCashBankEntity::add(true):
                JurnalCashBankEntity::add(true),
            'key_val'=>$request->key,
            'merchantId'=>[-1],
            'encodeMerchant'=>base64_encode(json_encode(MerchantUtil::getBranch(merchant_id())))
        ];

        return view('merchant::acc.journal.cash-bank.index',$params);

    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (JurnalCashBankEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Kas & Bank',
            'searchKey' => $searchKey,
            'tableColumns'=>JurnalCashBankEntity::dataTableColumns(),
            'start_date'=>(is_null($request->startDate))?Carbon::now()->startOfMonth()->toDateString():$request->startDate,
            'end_date'=>(is_null($request->endDate))?Carbon::now()->endOfMonth()->toDateString():$request->endDate,
            'merchantId'=>$request->md_merchant_id,
            'encodeMerchant'=>base64_encode(json_encode($request->md_merchant_id))

        ];
        return view('merchant::acc.journal.cash-bank.list',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return JurnalCashBankEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }
    public  function add(Request $request){

        $id=$request->id;
        $type=$request->type;
        if(!is_null($id)){

            $data=JournalEntity::find($id);
            $data2=JurnalDetail::where('acc_jurnal_id',$id)
                ->first();
        }else{
            $data=new JournalEntity();
            $data2=new JournalEntity();

        }
        $bankOption=CoaDetail::
        select([
            'acc_coa_details.id',
            'acc_coa_details.name as text',
            'c.md_merchant_id'
        ])
            ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
            ->whereIn('c.md_merchant_id',MerchantUtil::getBranch(merchant_id()))
            ->whereIn('acc_coa_details.is_deleted',[0,2])
            ->where('acc_coa_details.name','!=','Payment Gateway')
            ->where('c.code','1.1.02')
            ->orderBy('acc_coa_details.id','asc')
            ->get();
        $cashOption=CoaDetail::
            select([
                'acc_coa_details.id',
                 'acc_coa_details.name as text',
                  'c.md_merchant_id'
            ])
            ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
            ->whereIn('c.md_merchant_id',MerchantUtil::getBranch(merchant_id()))
            ->whereIn('acc_coa_details.is_deleted',[0,2])
                ->where('c.code','1.1.01')
            ->orderBy('acc_coa_details.id','asc')
                ->get();

        if($type==1)
        {
            $title=(is_null($id))?'Tambah Transfer Antar Bank':'Edit Transfer Antar Bank';
            $view='merchant::acc.journal.cash-bank.form-transfer';
        }elseif ($type==2)
        {
            $title=(is_null($id))?'Tambah Transfer Antar Kas':'Edit Transfer Antar Kas';
            $view='merchant::acc.journal.cash-bank.form-transfer';

        }elseif ($type==3)
        {
            $title=(is_null($id))?'Tambah Penarikan Bank ke Kas':'Edit Penarikan Bank ke Kas';
            $view='merchant::acc.journal.cash-bank.form-bank-to-cash';

        }else{

            $title=(is_null($id))?'Tambah Setoran Kas ke Bank':'Edit Setoran Kas ke Bank';
            $view='merchant::acc.journal.cash-bank.form-cash-to-bank';

        }

        if($type==1 || $type==2)
        {
            $firstOption=JurnalDetail::where('acc_jurnal_id',$id)
                ->where('coa_type','Kredit')
                ->orderBy('id','asc')
                ->first();
            if(!is_null($id))
            {
                $lastOption=JurnalDetail::where('acc_jurnal_id',$id)
                    ->whereNotIn('acc_coa_detail_id',[merchant_detail_multi_branch($data->md_merchant_id)->coa_administration_bank_id,$firstOption->acc_coa_detail_id])
                    ->orderBy('id','desc')
                    ->first();
            }else{
                $lastOption=null;
            }

        }else{
            $firstOption=JurnalDetail::where('acc_jurnal_id',$id)
                ->where('coa_type','Kredit')
                ->orderBy('id','asc')
                ->first();
            if(!is_null($id))
            {
                $lastOption=JurnalDetail::where('acc_jurnal_id',$id)
                    ->whereNotIn('acc_coa_detail_id',[merchant_detail_multi_branch($data->md_merchant_id)->coa_administration_bank_id,$firstOption->acc_coa_detail_id])
                    ->orderBy('id','desc')
                    ->get();
            }else{
                $lastOption=null;

            }

        }

        $params=[
            'title'=>$title,
            'data'=>$data,
            'bankOption'=>json_encode($bankOption),
            'cashOption'=>json_encode($cashOption),
            'firstOption'=>$firstOption,
            'lastOption'=>$lastOption,
            'data2'=>$data2,
            'type'=>$type
        ];

        return view($view,$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $date = new \DateTime($request->trans_time);
            $id=$request->id;

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'jurnal',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);

            }
            if(!is_null($id)){

                $data=JournalEntity::find($id);
                JurnalDetail::where('acc_jurnal_id',$data->id)->delete();

            }else{

                $data=new JournalEntity();
            }
            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/cash-bank/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->trans_proof;

                }
            }

            if(is_null($id))
            {
                $data->trans_code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $refCode=CodeGenerator::generalCode('CSB',$request->md_merchant_id);
                $data->ref_code=$refCode;

            }
            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
            $code=(is_null($data->second_code))?$data->ref_code:$data->second_code;
            $data->trans_name=$request->trans_name.' '.$code;
            $data->trans_time=$date->format('Y-m-d H:i:s');
            $data->trans_note=$request->trans_note;
            $data->trans_purpose=ArrayOfAccounting::searchOfActivity($request->trans_purpose);
            $data->trans_proof=$fileName;
            $data->trans_type=2;
            $admin_fee=(is_null($request->admin_fee))?0:strtr($request->admin_fee, array('.' => '', ',' => '.'));
            $data->md_merchant_id=$request->md_merchant_id;
            $data->md_user_id_created=user_id();
            $data->flag_name=$request->flag_name;
            $data->timezone=$request->_timezone;
            $data->save();

            $adminFee=merchant_detail_multi_branch($request->md_merchant_id)->coa_administration_bank_id;
            $coa1=$request->from_acc_coa_detail_id;
            $coa2=$request->to_acc_coa_detail_id;
            $amount=$request->amount;
            if($request->trans_purpose==1 || $request->trans_purpose==2)
            {
                $trans_amount=strtr($request->trans_amount, array('.' => '', ',' => '.'));
                $data->trans_amount=$trans_amount;
                $data->admin_fee=$admin_fee;
                $data->save();

                if($admin_fee!=0){

                    JurnalDetail::insert([
                        [
                            'acc_coa_detail_id'=>$coa1,
                            'coa_type'=>"Kredit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$trans_amount+$admin_fee,
                            'created_at'=>$date->format('Y-m-d H:i:s'),
                            'updated_at'=>$date->format('Y-m-d H:i:s'),
                            'acc_jurnal_id'=>$data->id
                        ],
                        [
                            'acc_coa_detail_id'=>$coa2,
                            'coa_type'=>"Debit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$trans_amount,
                            'created_at'=>$date->format('Y-m-d H:i:s'),
                            'updated_at'=>$date->format('Y-m-d H:i:s'),
                            'acc_jurnal_id'=>$data->id
                        ],
                        [
                            'acc_coa_detail_id'=>$adminFee,
                            'coa_type'=>"Debit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$admin_fee,
                            'created_at'=>$date->format('Y-m-d H:i:s'),
                            'updated_at'=>$date->format('Y-m-d H:i:s'),
                            'acc_jurnal_id'=>$data->id
                        ],

                    ]);
                }else{
                    JurnalDetail::insert([
                        [
                            'acc_coa_detail_id'=>$coa1,
                            'coa_type'=>"Kredit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$trans_amount,
                            'created_at'=>$date->format('Y-m-d H:i:s'),
                            'updated_at'=>$date->format('Y-m-d H:i:s'),
                            'acc_jurnal_id'=>$data->id
                        ],
                        [
                            'acc_coa_detail_id'=>$coa2,
                            'coa_type'=>"Debit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$trans_amount,
                            'created_at'=>$date->format('Y-m-d H:i:s'),
                            'updated_at'=>$date->format('Y-m-d H:i:s'),
                            'acc_jurnal_id'=>$data->id
                        ],

                    ]);
                }
            }else{

                if(empty($coa2))
                {

                    return response()->json([
                        'message'=>'Data tujuan penarikan tidak boleh kosong',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>'',
                    ]);

                }
                $insert=[];
                $totalAmount=0;
                foreach ($coa2 as $key =>$item)
                {
                    if($coa2[$key]==-1 || $coa2[$key]=='-1')
                    {
                        return response()->json([
                            'message'=>'Data tujuan penarikan tidak boleh -1',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',
                        ]);
                    }

                    $a=(is_null($amount[$key]))?0:strtr($amount[$key], array('.' => '', ',' => '.'));
                    $insert[]= [
                        'acc_coa_detail_id'=>$coa2[$key],
                        'coa_type'=>"Debit",
                        'md_sc_currency_id'=>1,
                        'amount'=>$a,
                        'created_at'=>$date->format('Y-m-d H:i:s'),
                        'updated_at'=>$date->format('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$data->id
                    ];
                    $totalAmount+=$a;
                }

                if($admin_fee!=0)
                {
                    $data->trans_amount=$totalAmount;
                    $data->admin_fee=$admin_fee;
                    $data->save();

                    JurnalDetail::insert($insert);

                    JurnalDetail::insert([
                        [
                            'acc_coa_detail_id'=>$coa1,
                            'coa_type'=>"Kredit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$totalAmount+$admin_fee,
                            'created_at'=>$date->format('Y-m-d H:i:s'),
                            'updated_at'=>$date->format('Y-m-d H:i:s'),
                            'acc_jurnal_id'=>$data->id
                        ],
                        [
                            'acc_coa_detail_id'=>$adminFee,
                            'coa_type'=>"Debit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$admin_fee,
                            'created_at'=>$date->format('Y-m-d H:i:s'),
                            'updated_at'=>$date->format('Y-m-d H:i:s'),
                            'acc_jurnal_id'=>$data->id
                        ],

                    ]);
                }else{
                    $data->trans_amount=$totalAmount;
                    $data->admin_fee=$admin_fee;
                    $data->save();

                    JurnalDetail::insert($insert);
                    JurnalDetail::insert([
                        [
                            'acc_coa_detail_id'=>$coa1,
                            'coa_type'=>"Kredit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$totalAmount,
                            'created_at'=>$date->format('Y-m-d H:i:s'),
                            'updated_at'=>$date->format('Y-m-d H:i:s'),
                            'acc_jurnal_id'=>$data->id
                        ]
                    ]);
                }


            }
            DB::commit();
            return response()->json([
                'message'=>'Pencatatan kas dan bank berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.acc.journal.cash-bank.index'),
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>'',
            ]);


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=JournalEntity::find($id);
            $date = new \DateTime($data->trans_time);

            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }else{
                $data->is_deleted=1;
                $data->save();
                return response()->json([
                    'message'=>'Pencatatan kas dan bank berhasil dihapus',
                    'type'=>'success',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);


        }
    }

    public function getCoa(Request $request)
    {

        $categoryOption=CoaCategoryEntity::getData();
        $response =[];
        foreach($categoryOption as $key =>$item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->code."  ".$item->name,
                "disabled"=>true
            ];
            foreach ($item->getDetail as $dd)
            {
                $response[] = [
                    "id"=>$dd->id,
                    "text"=>'----'.$dd->code."  ".$dd->name,
                    "disabled"=>false
                ];
            }
            foreach ($item->getChild as $c)
            {
                $response[] = [
                    "id"=>$c->id,
                    "text"=>'--'.$c->code."  ".$c->name,
                    "disabled"=>true
                ];
                foreach ($c->getDetail as $ddd)
                {
                    $response[] = [
                        "id"=>$ddd->id,
                        "text"=>'----'.$ddd->code."  ".$ddd->name,
                        "disabled"=>false
                    ];
                }

                foreach ($c->getChild as $cc)
                {
                    $response[] = [
                        "id"=>$cc->id,
                        "text"=>'---'.$cc->code."  ".$cc->name,
                        "disabled"=>true
                    ];
                    foreach ($cc->getDetail as $dddd)
                    {
                        $response[] = [
                            "id"=>$dddd->id,
                            "text"=>'----'.$dddd->code."  ".$dddd->name,
                            "disabled"=>false
                        ];
                    }
                }
            }
        }
        return response()->json($response);
    }


    public function exportData(Request $request){
        try{

            $data=JurnalCashBankEntity::getDataForDataTable();

            $encode=json_decode($request->md_merchant_id);

            if(is_array($encode))
            {
                $filterMerchant=$encode;
            }else{
                $filterMerchant=json_decode($encode);
            }

            if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
                $getBranch=MerchantUtil::getBranch(merchant_id(),0);

                $data->whereIn('acc_jurnals.md_merchant_id',$getBranch);
            }else{
                $data->whereIn('acc_jurnals.md_merchant_id',$filterMerchant);
            }

            if($request->startDate!='-')
            {
                $data->whereRaw("
                acc_jurnals.trans_time::date between '$request->startDate' and '$request->endDate'
                ");
            }

            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $sd=date("Y-m-d", strtotime($request->startDate));
            $ed=date("Y-m-d", strtotime($request->endDate));
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Pencatatan Kas & Bank $sd sampai $ed ");

            $header = [
                'No',
                'Kode',
                'Nama Transaksi',
                'Waktu Transaksi',
                'Bukti Transaksi',
                'Keterangan',
                'Cabang',
                'Akun',
                'Debit',
                'Kredit',
                'Tipe'
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $col++;
            }

            $num = 4;
            $no=0;
            foreach ($data->get() as $key => $item) {

                foreach ($item->getDetail as $d)
                {
                    $dataShow = [
                        $no,
                        $item->kode,
                        $item->nama_transaksi,
                        Carbon::parse($item->waktu_transaksi)->isoFormat('dddd, D MMMM Y').' '.getTimeZoneName($item->timezone),
                        env('S3_URL').$item->bukti_transaksi,
                        $item->keterangan,
                        $item->outlet,
                        $d->getCoa->name,
                        ($d->coa_type=='Debit')?rupiah($d->amount):rupiah(0),
                        ($d->coa_type=='Kredit')?rupiah($d->amount):rupiah(0),
                        $item->trans_purpose


                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $col++;
                    }
                    $num++;
                    $no++;
                }
            }
             foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-jurnal-kas-bank'.merchant_id()."_".date('Y-m-d').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL';</script>
           <script>
                toastForSaveData('Jurnal kas dan bank berhasil diexport!','success',true,'')
                 setTimeout(() => {
                        closeModal();
                    }, 3000)
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<script>toastForSaveData('Terjadi kesalahan, Data jurnal kas dan bank gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)

</script>";


        }
    }

}

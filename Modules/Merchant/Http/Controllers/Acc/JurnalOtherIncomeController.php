<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaMapping;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantCashflow;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\SennaToko\Supplier;
use App\Utils\Accounting\AccUtil;
use App\Models\Accounting\Jurnal;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\PaymentUtils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use Modules\Merchant\Entities\Acc\JournalEntity;
use Modules\Merchant\Entities\Acc\JurnalOtherIncomeEntity;
use Image;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;

class JurnalOtherIncomeController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/jurnal/other-income')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::post('/data-table', 'Acc\JurnalOtherIncomeController@dataTable')
                    ->name('merchant.toko.acc.jurnal.other-income.datatable');
                Route::get('/add', 'Acc\JurnalOtherIncomeController@add')
                    ->name('merchant.toko.acc.jurnal.other-income.add');
                Route::post('/save', 'Acc\JurnalOtherIncomeController@save')
                    ->name('merchant.toko.acc.jurnal.other-income.save');
                Route::post('/delete', 'Acc\JurnalOtherIncomeController@delete')
                    ->name('merchant.toko.acc.jurnal.other-income.delete');
                Route::post('/reload-data', 'Acc\JurnalOtherIncomeController@reloadData')
                    ->name('merchant.toko.acc.jurnal.other-income.reload');
                Route::get('/revenue-list', 'Acc\JurnalOtherIncomeController@getRevenueList')
                    ->name('merchant.toko.acc.jurnal.other-income.revenue-list');
            });
    }


    public  function add(Request $request){

        $id=$request->id;

        if(!is_null($id)){
            $data=JournalEntity::find($id);
            $costList=$this->getRevenueList($request,$data->md_merchant_id,1);
            $payment=PaymentUtils::getPayment($data->md_merchant_id,2);
        }else{
            $data=new JournalEntity();
            $costList=null;
            $payment=null;
        }

        $firstOption=JurnalDetail::where('acc_jurnal_id',$id)
            ->where('coa_type','Debit')
            ->orderBy('id','asc')
            ->first();
        if(!is_null($id))
        {
            $lastOption=JurnalDetail::where('acc_jurnal_id',$id)
                ->whereNotIn('acc_coa_detail_id',[merchant_detail_multi_branch($data->md_merchant_id)->coa_administration_bank_id,$firstOption->acc_coa_detail_id])
                ->orderBy('id','desc')
                ->first();
        }else{
            $lastOption=null;
        }

        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'supplier'=>SupplierEntity::listOption(),
            'costList'=>$costList,
            'payment'=>$payment,
            'firstOption'=>$firstOption,
            'lastOption'=>$lastOption
        ];


        return view('merchant::acc.journal.other-income.form2',$params);
    }

    public function  getRevenueList(Request  $request,$merchantId=null,$type=0)
    {
        try {
            $merchantId=(!is_null($merchantId))?$merchantId:$request->md_merchant_id;
            $data=CoaDetail::
            select([
                'acc_coa_details.id',
                'acc_coa_details.name as text',
                'c.md_merchant_id'
            ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)
                ->whereIn('acc_coa_details.is_deleted',[0,2])
                ->whereIn('c.name',['Pendapatan','Pendapatan Lainnya'])
                ->whereRaw("acc_coa_details.name not like '%Penjualan%'  and acc_coa_details.name not like '%Laba Penjualan Aset%' and acc_coa_details.name not like '%Diskon Pembelian Barang%'")
                ->orderBy('acc_coa_details.id','asc')
                ->get();
            if($type==0)
            {
                return response()->json($data);

            }else{
                return $data;
            }
        }catch (\Exception $e)
        {
            if($type==0)
            {
                return  response()->json([]);
            }else{
                return [];
            }

        }

    }


    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $date = new \DateTime($request->trans_time);
            $id=$request->id;

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'jurnal',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }
            if(!is_null($id)){

                $data=JournalEntity::find($id);
                $cashflow=MerchantCashflow::find($data->external_ref_id);
                JurnalDetail::where('acc_jurnal_id',$data->id)->delete();
            }else{
                $data=new JournalEntity();
                $cashflow=new MerchantCashflow();
            }
            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/other-income/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->trans_proof;

                }
            }

            if(is_null($id))
            {
                $data->trans_code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $data->ref_code=CodeGenerator::otherCode('RVN',$request->md_merchant_id,date('y'),date('m'),date('d'));
                $cashflow->code=$data->ref_code;
            }

            $mapping=CoaMapping::where('acc_coa_detail_id',$request->acc_coa_detail_id)->
            where('md_merchant_id',merchant_id())
                ->where('is_deleted',0)
                ->first();


            $trans_amount=strtr($request->trans_amount, array('.' => '', ',' => '.'));

            $cashflow->md_sc_cash_type_id=3;
            $cashflow->name=$request->trans_name;
            $cashflow->amount=$trans_amount;
            $cashflow->md_merchant_id=$request->md_merchant_id;
            $cashflow->cashflow_coa_detail_id=$request->acc_coa_detail_id;
            $cashflow->to_cashflow_coa_detail_id=$request->payment_acc_coa_detail_id;
            $cashflow->note=$request->trans_note;
            $cashflow->is_deleted=0;
            $cashflow->created_by=user_id();
            $cashflow->save();


            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
            $code=(is_null($data->second_code))?$data->ref_code:$data->second_code;
            $data->trans_name=$request->trans_name.' '.$code;
            $data->trans_time=$date->format('Y-m-d H:i:s');
            $data->trans_note=$request->trans_note;
            $data->trans_proof=$fileName;
            $data->trans_type=3;
            $data->trans_amount=$trans_amount;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->md_user_id_created=user_id();
            $data->ref_code=$cashflow->code;
            $data->coa_name=$mapping->name;
            $data->external_ref_id=$cashflow->id;
            $data->timezone=$request->_timezone;
            if($request->sc_supplier_id!='-1'){
                $data->sc_supplier_id=$request->sc_supplier_id;
                $data->trans_purpose=Supplier::find($request->sc_supplier_id)->name;
            }else{
                $data->trans_purpose='Internal';
            }
            $data->save();
            $insert=[
                [
                    'acc_coa_detail_id'=>$request->acc_coa_detail_id,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$trans_amount,
                    'created_at'=>$date->format('Y-m-d H:i:s'),
                    'updated_at'=>$date->format('Y-m-d H:i:s'),
                    'acc_jurnal_id'=>$data->id
                ],
                [
                    'acc_coa_detail_id'=>$request->payment_acc_coa_detail_id,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$trans_amount,
                    'created_at'=>$date->format('Y-m-d H:i:s'),
                    'updated_at'=>$date->format('Y-m-d H:i:s'),
                    'acc_jurnal_id'=>$data->id
                ],
            ];

            JurnalDetail::insert(
                $insert
            );

            DB::commit();
            return response()->json([
                'message'=>'Pencatatan pendapatan lainnya berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.acc.jurnal.administrative-expense.index'),

            ]);
        }catch (\Exception $e)
        {

            DB::commit();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>'',
            ]);

        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc\Asset;


use App\Classes\CollectionPagination;
use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\Asset\AssetCategory;
use App\Models\Accounting\Asset\AssetDepreciation;
use App\Models\Accounting\Asset\AssetDepreciationDetail;
use App\Models\Accounting\Asset\AssetDepreciationPayment;
use App\Models\Accounting\Asset\DepreciatonMethod;
use App\Models\Accounting\Asset\PropertyGroup;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\JurnalDetail;
use App\Models\SennaToko\Supplier;
use App\Utils\Accounting\AssetDepreciationMethodUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\PaymentUtils;
use Carbon\Carbon;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use League\CommonMark\Inline\Element\Code;
use Modules\Merchant\Entities\Acc\JournalEntity;
use Modules\Merchant\Entities\Asset\AssetEntity;
use Modules\Merchant\Entities\Asset\CategoryEntity;

class AssetController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('asset/asset')
            ->group(function (){
                Route::get('/', 'Acc\Asset\AssetController@index')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.index');
                Route::post('/data-table', 'Acc\Asset\AssetController@dataTable')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.datatable');
                Route::get('/add', 'Acc\Asset\AssetController@add')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.add');
                Route::get('/detail', 'Acc\Asset\AssetController@detail')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.detail');
                Route::get('/select-benefit', 'Acc\Asset\AssetController@getSelectBenefit')
                    // ->middleware('api-verification')
                    ->name('merchant.asset.asset.benefit');
                Route::post('/save', 'Acc\Asset\AssetController@save')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.save');
                Route::post('/delete', 'Acc\Asset\AssetController@delete')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.delete');
                Route::post('/book', 'Acc\Asset\AssetController@book')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.book');
                Route::post('/delete', 'Acc\Asset\AssetController@delete')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.delete');

                //payment
                Route::post('/save-payment', 'Acc\Asset\AssetController@savePayment')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.save-payment');
                Route::post('/add-payment', 'Acc\Asset\AssetController@formPayment')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.add-payment');

                //sell
                Route::post('/save-sell', 'Acc\Asset\AssetController@saveSell')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.save-sell');

                Route::post('/cancel-sell', 'Acc\Asset\AssetController@cancelSell')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.cancel-sell');

                Route::post('/add-sell', 'Acc\Asset\AssetController@formSell')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.add-sell');
                Route::post('/delete-payment', 'Acc\Asset\AssetController@deletePayment')
                    ->middleware('merchant-verification')
                    ->name('merchant.asset.asset.delete-payment');
                Route::post('/reload-data', 'Acc\Asset\AssetController@reloadData')
                    ->name('merchant.asset.asset.reload');
                Route::post('/exportData', 'Acc\Asset\AssetController@exportData')
                    ->name('merchant.asset.asset.exportData');

                Route::get('/asset-list', 'Acc\Asset\AssetController@getAssetAccount')
                    ->name('merchant.asset.asset.asset-list');
                Route::get('/depreciation-list', 'Acc\Asset\AssetController@getDepreciationAccount')
                    ->name('merchant.asset.asset.depreciation-list');

                Route::post('/json-depreciation', 'Acc\Asset\AssetController@getDepreciation')
                    ->name('merchant.asset.asset.json-depreciation');
            });
    }


    public function index(Request $request)
    {
        $params=[
            'title'=>'Data Aset',
            'tableColumns'=>AssetEntity::dataTableColumns(),
            'start_date'=>Carbon::now()->subYear(1)->startOfYear()->toDateString(),
            'end_date'=>Carbon::now()->endOfYear()->toDateString(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'add'=>(get_role()==3)?AssetEntity::add(true):
                AssetEntity::add(true),
            'merchantId'=>[-1],
            'encodeMerchant'=>base64_encode(json_encode(MerchantUtil::getBranch(merchant_id())))

        ];

        return view('merchant::acc.asset.asset.index',$params);

    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (AssetEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;

        $params = [
            'title' => 'Data Aset',
            'searchKey' => $searchKey,
            'tableColumns'=>AssetEntity::dataTableColumns(),
            'start_date'=>(is_null($request->startDate))?Carbon::now()->startOfMonth()->toDateString():$request->startDate,
            'end_date'=>(is_null($request->endDate))?Carbon::now()->endOfMonth()->toDateString():$request->endDate,
            'merchantId'=>$request->md_merchant_id,
            'encodeMerchant'=>base64_encode(json_encode($request->md_merchant_id))


        ];
        return view('merchant::acc.asset.asset.list',$params);

    }

    public function dataTable(Request $request)
    {
        return AssetEntity::dataTable($request);
    }

    public function book(Request  $request)
    {
        $params=[
            'title'=>'Informasi Kelompok Harta',
            'request'=>$request->all()
        ];

        return view('merchant::acc.asset.asset.book',$params);

    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=AssetEntity::find($id);
            $asset=$this->getAssetAccount($request,$data->md_merchant_id,2);
            $depreciation=$this->getDepreciationAccount($request,$data->md_merchant_id,2);
            $payment=PaymentUtils::getPayment($data->md_merchant_id,2);

        }else{
            $data=new AssetEntity();
            $asset=null;
            $depreciation=null;
            $payment=null;
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'is_initial'=>$request->is_initial,
            'supplier'=>SupplierEntity::listOption(),
            'assetAccount'=>$asset,
            'depreciationAccount'=>$depreciation,
            'category'=>CategoryEntity::listOption(),
            'propertyGroup'=>PropertyGroup::whereNull('parent_id')->get(),
            'depreciationMethod'=>DepreciatonMethod::all(),
            'payment'=>$payment,

        ];

        return view('merchant::acc.asset.asset.form',$params);
    }

    public function getAssetAccount(Request $request,$merchantId='',$type=0)
    {
        try {
            if($merchantId=='')
            {
                $merchant=$request->md_merchant_id;
            }else{
                $merchant=$merchantId;
            }
            $this->initCoaIntangibleAsset($merchant);

            $data=CoaDetail::
            select([
                'acc_coa_details.id',
                'acc_coa_details.name as text'
            ])
                ->join('acc_coa_categories as  c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchant)
                ->whereIn('c.code',['1.2.01','1.2.03'])
                ->orderBy('acc_coa_details.id','asc')
                ->get();
            return ($type==0)?response()->json($data):$data;

        }catch (\Exception $e)
        {
            return response()->json([]);

        }
    }


    public function getDepreciationAccount(Request $request,$merchantId='',$type=0)
    {
        try {
            if($merchantId=='')
            {
                $merchant=$request->md_merchant_id;
            }else{
                $merchant=$merchantId;
            }
            $data=CoaDetail::
            select([
                'acc_coa_details.id',
                'acc_coa_details.name as text'
            ])
                ->join('acc_coa_categories as  c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchant)
                ->where('c.code','6.1.00')
                ->orderBy('acc_coa_details.id','asc')
                ->get();
            return ($type==0)?response()->json($data):$data;
        }catch (\Exception $e)
        {
            return response()->json([]);

        }
    }



    public function detail(Request  $request)
    {
        $data=AssetEntity::find($request->id);
        $detail=$data->getDetail;
        $payment=$data->getPayment;
        $year=collect(DB::select("
       SELECT to_char(dd,'YYYY') as id,
        'Tahun '||to_char(dd,'YYYY') as text
        FROM generate_series
                ( '".$data->record_time."'
                , '".date('Y-m-d H:i:s')."'::timestamp
                , '1 year'::interval) dd
                ;
        "));
        $ext=['png','jpeg','jpg'];

        if(!is_null($data->file_asset))
        {
            if(in_array(pathinfo($data->file_asset,PATHINFO_EXTENSION),$ext))
            {
                $image=true;
            }else{
                $image=false;
            }
        }else{
            $image=false;
        }

        $params=[
            'title'=>'Detail',
            'data'=>$data,
            'detCount'=>$detail->count(),
            'detail'=>$detail,
            'payCount'=>$payment->count(),
            'payDetail'=>$payment,
            "year"=>$year,
            'image'=>$image
        ];

        return view('merchant::acc.asset.asset.detail',$params);

    }


    public function save(Request $request)
    {
        try{

            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();
            $date = new \DateTime($request->record_time);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false,

                ]);
            }

            $checkPackageSubscription=MerchantUtil::checkUrl($request->md_merchant_id);

            if($checkPackageSubscription['status']==true)
            {
                return response()->json([
                    'message'=>$checkPackageSubscription['message'],
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }
            $id=$request->id;

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'asset',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            if(!is_null($id)){

                $data=AssetEntity::find($id);
            }else{
                $data=new AssetEntity();
            }

            $destinationPath = 'public/uploads/merchant/asset/'.$request->md_merchant_id.'/list/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->trans_proof;
                }
            }

            if($request->hasFile('file_asset'))
            {
                $fileAsset=$request->file('file_asset');
                $fileNameAsset= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$fileAsset->getClientOriginalName());
                $fileNameAsset=$destinationPath.$fileNameAsset;
                Storage::disk('s3')->put($fileNameAsset, file_get_contents($fileAsset));
            }else{
                if(is_null($id)){
                    $fileNameAsset=null;

                }else{
                    $fileNameAsset=$data->file_asset;
                }
            }
            if($request->sc_supplier_id=='-1')
            {
                $supplierId=NULL;
            }else{
                $supplierId=$request->sc_supplier_id;
            }
            if(is_null($id))
            {
                $data->code=CodeGenerator::generalCode('AST',$request->md_merchant_id);

            }
            if($request->acc_coa_asset_id=='-1')
            {
                return response()->json([
                    'message'=>'Silahkan pilih akun aset terlebih dahulu!',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }
            if($request->is_paid==1)
            {
                if($request->payment_coa_detail_id=='-1')
                {
                    return response()->json([
                        'message'=>'Silahkan pilih metode pembayaran terlebih dahulu!',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>'',
                        'is_with_datatable'=>false
                    ]);
                }
                $data->payment_coa_detail_id=$request->payment_coa_detail_id;
            }

            $data->asset_name=$request->asset_name;
            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
            $asset_value=strtr($request->asset_value, array('.' => '', ',' => '.'));
            $admin_fee=strtr($request->admin_fee, array('.' => '', ',' => '.'));
            $data->asset_value=$asset_value;
            $data->file_asset=$fileNameAsset;
            $data->admin_fee=$admin_fee;
            $data->acc_asset_category_id=$request->acc_asset_category_id;
            $data->record_time=$request->record_time;
            $data->sc_supplier_id=$supplierId;
            $data->desc=$request->desc;
            $data->acc_coa_asset_id=$request->acc_coa_asset_id;
            $data->trans_proof=$fileName;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->_timezone=$request->_timezone;
            $data->is_paid=$request->is_paid;
            $data->is_intangible_asset=$request->is_intangible_asset;
            $data->save();
            DB::commit();

            if($request->is_depreciation==0)
            {
                $data->is_depreciation=0;
                $data->save();
                DB::commit();

            }else{

                if($request->is_intangible_asset==0)
                {
                    if($request->account_depreciation_id=='-1')
                    {
                        return response()->json([
                            'message'=>'Silahkan pilih akun penyusutan terlebih dahulu!',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);

                    }
                    if($request->account_accumulation_id=='-1')
                    {
                        return response()->json([
                            'message'=>'Silahkan pilih akun akumulasi penyusutan terlebih dahulu!',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);

                    }
                    $data->acc_md_property_group_id=$request->acc_md_property_group_id;
                    $data->acc_md_depreciation_method_id=$request->acc_md_depreciation_method_id;
                    $data->account_depreciation_id=$request->account_depreciation_id;
                    $data->account_accumulation_id=$request->account_accumulation_id;
                    $data->value_depreciation=$request->value_depreciation;
                    $data->is_depreciation=1;
                    $data->benefit_period=$request->benefit_period;
                    $data->save();
                    DB::commit();

                }else{
                    if($request->account_depreciation_id_intangible=='-1')
                    {
                        return response()->json([
                            'message'=>'Silahkan pilih akun penyusutan terlebih dahulu!',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);

                    }
                    if($request->account_accumulation_id_intangible=='-1')
                    {
                        return response()->json([
                            'message'=>'Silahkan pilih akun akumulasi penyusutan terlebih dahulu!',
                            'type'=>'warning',
                            'is_modal'=>false,
                            'redirect_url'=>'',
                            'is_with_datatable'=>false
                        ]);

                    }
                    $data->acc_md_depreciation_method_id=$request->acc_md_depreciation_method_id_intangible;
                    $data->account_depreciation_id=$request->account_depreciation_id_intangible;
                    $data->account_accumulation_id=$request->account_accumulation_id_intangible;
                    $data->value_depreciation=$request->value_depreciation_intangible/100;
                    $data->is_depreciation=1;
                    $data->benefit_period=$request->benefit_period_intangible;
                    $data->save();
                    DB::commit();

                }

            }


            if($this->generateJurnalAsset($data)==false)
            {
                DB::rollBack();
                return response()->json([
                    'message'=>'Terjadi kesalahan saat pemrosesan inisialisasi aset ke jurnal!',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }else{
                return response()->json([
                    'message'=>'Pencatatan aset berhasil disimpan',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>\route('merchant.asset.asset.detail',['id'=>$data->id]),
                    'is_with_datatable'=>false
                ]);
            }
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }
    }


    public function saveDepreciation($data)
    {
        try {
            DB::beginTransaction();
            $now=Carbon::now()->toDateString();
            $endDate=Carbon::parse($data->record_time)->addMonths($data->benefit_period*12)->toDateTimeString();
            $start    = new \DateTime($data->record_time);
            $end      = new \DateTime($endDate);
            $interval = \DateInterval::createFromDateString('1 month');
            $period   = new \DatePeriod($start, $interval, $end);

            $calculate=AssetDepreciationMethodUtil::calculate($data->asset_value,$data->benefit_period,$data->value_depreciation,$data->acc_md_depreciation_method_id);
            $insertDepr=[];
            $jurnalDetail=[];
            $totalDepreciation=0;
            foreach ($period as $key => $date) {
                $endOfMonth=Carbon::parse($date->format("Y-m-d H:i:s"))->endOfMonth();
                if($now>=$endOfMonth->toDateString())
                {
                    $check=AssetDepreciationDetail::whereDate('time',$endOfMonth->toDateString())
                        ->where('acc_asset_depreciation_id',$data->id)->first();
                    if(is_null($check))
                    {
                        $totalDepreciation+=$calculate[$key];
                        $codeDepr='DEP'.$data->md_merchant_id.$data->id.($key+1);
                        $insertDepr[] = [
                            'code'=>$codeDepr,
                            'value_depreciation'=>$calculate[$key],
                            'time'=>$endOfMonth->toDateString(),
                            'record_time_to_journal'=>$endOfMonth->toDateString().' 23:59:59',
                            'is_record_to_journal'=>1,
                            'acc_asset_depreciation_id'=>$data->id,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'),
                            'timezone'=>$data->_timezone
                        ];

                        $journal=new JournalEntity();
                        $code=CodeGenerator::generalCode('JRN',$data->md_merchant_id);
                        $journal->trans_code=$code;
                        $journal->ref_code=$codeDepr;
                        $assetCode=is_null($data->second_code)?$data->code:$data->second_code;
                        $journal->trans_name='Penyusutan '.$data->asset_name.' '.$assetCode;
                        $journal->trans_time=$endOfMonth->toDateString().' 23:59:59';
                        $journal->trans_note='Penyusutan '.$data->asset_name.' '.$assetCode;
                        $journal->trans_purpose='Penyusutan '.$data->asset_name.' '.$assetCode;
                        $journal->trans_proof='Penyusutan '.$data->asset_name.' '.$assetCode;
                        $journal->trans_type=0;
                        $journal->trans_amount=$calculate[$key];
                        $journal->md_merchant_id=$data->md_merchant_id;
                        $journal->md_user_id_created=user_id();
                        $journal->timezone=$data->_timezone;
                        $journal->save();

                        array_push($jurnalDetail,[
                            'acc_coa_detail_id'=>$data->account_depreciation_id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$calculate[$key],
                            'created_at'=>$journal->trans_time,
                            'updated_at'=>$journal->trans_time,
                            'acc_jurnal_id'=>$journal->id,
                            'ref_code'=>$journal->trans_code,
                        ]);

                        array_push($jurnalDetail,[
                            'acc_coa_detail_id'=>$data->account_accumulation_id,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$calculate[$key],
                            'created_at'=>$journal->trans_time,
                            'updated_at'=>$journal->trans_time,
                            'acc_jurnal_id'=>$journal->id,
                            'ref_code'=>$journal->trans_code,
                        ]);
                    }
                }else{
                    break;
                }
            }

            AssetDepreciationDetail::insert($insertDepr);
            JurnalDetail::insert($jurnalDetail);
            $data->current_asset_value=$data->asset_value-$totalDepreciation;
            $data->save();

            DB::commit();
            return true;


        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

    private function generateJurnalAsset($data)
    {
        try{
            DB::beginTransaction();
            $check=JournalEntity::where('ref_code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->first();
            if(!is_null($check))
            {
                JournalEntity::where('ref_code',$data->code)
                    ->where('md_merchant_id',$data->md_merchant_id)->update([
                        'is_deleted'=>1
                    ]);
                foreach ($data->getDetail as $s)
                {
                    JournalEntity::where('ref_code',$s->code)
                        ->where('md_merchant_id',$data->md_merchant_id)
                        ->update([
                            'is_deleted'=>1
                        ]);
                }

                foreach ($data->getPayment as $item)
                {
                    JournalEntity::where('ref_code',$item->code)
                        ->where('md_merchant_id',$data->md_merchant_id)
                        ->update([
                            'is_deleted'=>1
                        ]);
                }

                AssetDepreciationDetail::where('acc_asset_depreciation_id', $data->id)->delete();
                AssetDepreciationPayment::where('acc_asset_depreciation_id',$data->id)->delete();


            }
            $journal=new JournalEntity();
            $code=CodeGenerator::generalCode('JRN',$data->md_merchant_id);
            $journal->trans_code=$code;
            $journal->ref_code=$data->code;
            $assetCode=is_null($data->second_code)?$data->code:$data->second_code;
            $journal->trans_name='Pembelian Aset '.$data->asset_name.' '.$assetCode;
            $journal->trans_time=$data->record_time;
            $journal->trans_note='Pembelian Aset '.$data->asset_name.' '.$assetCode;
            $journal->trans_purpose='Pembelian Aset '.$data->asset_name.' '.$assetCode;
            $journal->trans_proof='Pembelian Aset '.$data->asset_name.' '.$assetCode;
            $journal->trans_type=0;
            $journal->trans_amount=$data->asset_value;
            $journal->md_merchant_id=$data->md_merchant_id;
            $journal->md_user_id_created=user_id();
            $journal->timezone=$data->_timezone;
            $journal->save();

            if($data->is_paid==1)
            {
                $detail=[
                    [
                        'acc_coa_detail_id'=>$data->acc_coa_asset_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->asset_value,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ],
                    [
                        'acc_coa_detail_id'=>$data->payment_coa_detail_id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->asset_value+$data->admin_fee,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ]
                ];
                JurnalDetail::insert($detail);

                if($data->admin_fee>0)
                {
                    $detail2=[
                        [
                            'acc_coa_detail_id'=>merchant_detail_multi_branch($data->md_merchant_id)->coa_administration_bank_id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->admin_fee,
                            'created_at'=>$journal->trans_time,
                            'updated_at'=>$journal->trans_time,
                            'acc_jurnal_id'=>$journal->id,
                            'ref_code'=>$journal->trans_code,
                        ],
                    ];
                    JurnalDetail::insert($detail2);

                }
            }else{
                $coaUtangAsset=merchant_detail_multi_branch($data->md_merchant_id)->coa_debt_asset_id;
                $detail3=[
                    [
                        'acc_coa_detail_id'=>$data->acc_coa_asset_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->asset_value,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ],
                    [
                        'acc_coa_detail_id'=>$coaUtangAsset,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->asset_value,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ]
                ];
                JurnalDetail::insert($detail3);
            }
            DB::commit();
            if($data->is_depreciation==1)
            {
                if($this->saveDepreciation($data)==false)
                {
                    DB::rollBack();
                    return false;
                }else{
                    return  true;
                }
            }else{
                return true;

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }


    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=AssetEntity::find($id);
            $date = new \DateTime($data->paid_date);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            JournalEntity::where('ref_code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)->update([
                    'is_deleted'=>1
                ]);
            foreach ($data->getDetail as $s)
            {
                JournalEntity::where('ref_code',$s->code)
                    ->where('md_merchant_id',$data->md_merchant_id)
                    ->update([
                        'is_deleted'=>1
                    ]);
            }

            foreach ($data->getPayment as $item)
            {
                JournalEntity::where('ref_code',$item->code)
                    ->where('md_merchant_id',$data->md_merchant_id)
                    ->update([
                        'is_deleted'=>1
                    ]);
            }

            AssetDepreciationDetail::where('acc_asset_depreciation_id', $data->id)->delete();
            AssetDepreciationPayment::where('acc_asset_depreciation_id',$data->id)->delete();
            $data->is_deleted=1;
            $data->save();

            DB::commit();

            return response()->json([
                'message'=>'Data aset berhasil dihapus',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.asset.asset.index'),
                'is_with_datatable'=>false
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }
    }

    public function formPayment(Request  $request)
    {
        $asset=AssetEntity::find($request->acc_depreciation_id);

        $id=$request->id;

        if(is_null($id))
        {
            $data=new AssetDepreciationPayment();
        }else{
            $data=AssetDepreciationPayment::find($id);
        }


        $params=[
            'title'=>'Tambah Pembayaran',
            'data'=>$data,
            'asset'=>$asset,
            'payment'=>PaymentUtils::getPayment($asset->md_merchant_id,2)
        ];

        return view('merchant::acc.asset.asset.form-payment',$params);

    }


    public function formSell(Request  $request)
    {
        $asset=AssetEntity::find($request->acc_depreciation_id);


        $params=[
            'title'=>'Penjualan Aset',
            'asset'=>$asset,
            'payment'=>PaymentUtils::getPayment($asset->md_merchant_id,2)
        ];

        return view('merchant::acc.asset.asset.form-sell-asset',$params);

    }

    public function saveSell(Request  $request)
    {

        try{
            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();

            if(FindCodeUtil::findCode(str_replace(' ','',$request->sell_second_code),$request->md_merchant_id,'asset_sell',0,$request->acc_asset_depreciation_id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $asset=AssetEntity::find($request->acc_asset_depreciation_id);
            $date = new \DateTime($request->sell_date);
            $paid_nominal=(is_null($request->sell_price) || $request->sell_price=='')?0:strtr($request->sell_price, array('.' => '', ',' => '.'));
            $admin_fee=(is_null($request->sell_ppn_amount) || $request->sell_ppn_amount=='')?0:strtr($request->sell_ppn_amount, array('.' => '', ',' => '.'));

            if(AccUtil::checkClosingJournal($asset->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if($date->format('Y-m-d')<=Carbon::parse($asset->record_time)->toDateString())
            {
                return response()->json([
                    'message'=>'Waktu penjualan tidak boleh kurang dari waktu pencatatan aset',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $destinationPath = 'public/uploads/merchant/'.$asset->md_merchant_id.'/acc/jurnal/asset-sell/';

            if($request->hasFile('sell_trans_proof'))
            {
                $file=$request->file('sell_trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                $fileName=null;

            }

            if(is_null($asset->sell_code))
            {
                $asset->sell_code=CodeGenerator::generalCode('SAST',$asset->md_merchant_id);

            }

            if($request->sell_coa_detail_id=='-1')
            {
                return response()->json([
                    'message'=>'Silahkan pilih metode penjualan terlebih dahulu',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            $ppn=merchant_detail_multi_branch($asset->md_merchant_id)->coa_debt_ppn_out_id;
            $asset->sell_date=$request->sell_date;
            $asset->sell_second_code=(is_null($request->sell_second_code)|| $request->sell_second_code=='')?null:str_replace(' ','',$request->sell_second_code);
            $asset->sell_trans_proof=$fileName;
            $asset->sell_coa_detail_id=$request->sell_coa_detail_id;
            if($admin_fee>0)
            {
                $asset->sell_ppn_amount=$admin_fee;
                $asset->sell_coa_ppn_id=$ppn;
            }
            $asset->is_sell=1;
            $asset->sell_price=$paid_nominal;
            $asset->save();

            if($asset->is_depreciation==1)
            {
                $valueDepreciation=$asset->getDetail->sum('value_depreciation');
                $assetCurrentValue=$asset->asset_value-$valueDepreciation;

            }else{
                $assetCurrentValue=$asset->asset_value;
            }

            $coaCostOrRevenueAsset= CoaDetail::select([
                'acc_coa_details.id',
                'acc_coa_details.name'
            ])->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$asset->md_merchant_id)
                ->whereIn('acc_coa_details.name',['Laba Penjualan Aset','Beban Penjualan Aset'])
                ->where('acc_coa_details.is_deleted',0)
                ->get();


            $isProfit=round($paid_nominal-$assetCurrentValue,2);

            $journal=new JournalEntity();
            $code=CodeGenerator::generalCode('JRN',$asset->md_merchant_id);
            $journal->trans_code=$code;
            $journal->ref_code=$asset->sell_code;
            $assetCode=is_null($asset->second_code)?$asset->code:$asset->second_code;
            $paymentCode=is_null($asset->sell_second_code)?$asset->sell_code:$asset->sell_second_code;
            $journal->trans_name='Penjualan Aset '.$asset->asset_name.' '.$assetCode.'-'.$paymentCode;
            $journal->trans_time=$asset->sell_date;
            $journal->trans_note='Penjualan Aset '.$asset->asset_name.' '.$assetCode.'-'.$paymentCode;
            $journal->trans_purpose='Penjualan Aset '.$asset->asset_name.' '.$assetCode.'-'.$paymentCode;
            $journal->trans_proof=$asset->sell_trans_proof;
            $journal->trans_type=0;
            $journal->trans_amount=$asset->sell_price+$admin_fee;
            $journal->md_merchant_id=$asset->md_merchant_id;
            $journal->md_user_id_created=user_id();
            $journal->external_ref_id=$asset->id;
            $journal->timezone=$request->_timezone;
            $journal->save();
            if($isProfit<0)
            {
                if($asset->is_depreciation==1)
                {
                    if($admin_fee>0)
                    {
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal+$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->account_accumulation_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->getDetail->sum('value_depreciation'),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$ppn,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$coaCostOrRevenueAsset->firstWhere('name','Beban Penjualan Aset')->id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>abs($isProfit),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }else{
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->account_accumulation_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->getDetail->sum('value_depreciation'),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$coaCostOrRevenueAsset->firstWhere('name','Beban Penjualan Aset')->id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>abs($isProfit),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }
                    JurnalDetail::insert($insert);
                }else{
                    if($admin_fee>0)
                    {
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal+$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$ppn,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$coaCostOrRevenueAsset->firstWhere('name','Beban Penjualan Aset')->id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>abs($isProfit),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }else{
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$coaCostOrRevenueAsset->firstWhere('name','Beban Penjualan Aset')->id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>abs($isProfit),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }
                    JurnalDetail::insert($insert);
                }

            }elseif ($isProfit==0)
            {
                if($asset->is_depreciation==1)
                {
                    if($admin_fee>0)
                    {
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal+$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->account_accumulation_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->getDetail->sum('value_depreciation'),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$ppn,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }else{
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->account_accumulation_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->getDetail->sum('value_depreciation'),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }
                    JurnalDetail::insert($insert);
                }else{
                    if($admin_fee>0)
                    {
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal+$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$ppn,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                        ];
                    }else{
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }
                    JurnalDetail::insert($insert);
                }

            }else{

                if($asset->is_depreciation==1)
                {
                    if($admin_fee>0)
                    {
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal+$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->account_accumulation_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->getDetail->sum('value_depreciation'),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$ppn,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$coaCostOrRevenueAsset->firstWhere('name','Laba Penjualan Aset')->id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>abs($isProfit),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }else{
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->account_accumulation_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->getDetail->sum('value_depreciation'),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$coaCostOrRevenueAsset->firstWhere('name','Laba Penjualan Aset')->id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>abs($isProfit),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }
                    JurnalDetail::insert($insert);
                }else{
                    if($admin_fee>0)
                    {
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal+$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$ppn,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$admin_fee,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$coaCostOrRevenueAsset->firstWhere('name','Laba Penjualan Aset')->id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>abs($isProfit),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }else{
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$asset->sell_coa_detail_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$paid_nominal,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$coaCostOrRevenueAsset->firstWhere('name','Laba Penjualan Aset')->id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>abs($isProfit),
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],
                            [
                                'acc_coa_detail_id'=>$asset->acc_coa_asset_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$asset->asset_value,
                                'created_at'=>$journal->trans_time,
                                'updated_at'=>$journal->trans_time,
                                'acc_jurnal_id'=>$journal->id,
                                'ref_code'=>$journal->trans_code,
                            ],

                        ];
                    }
                    JurnalDetail::insert($insert);
                }

            }
            DB::commit();
            return response()->json([
                'message'=>'Data penjualan aset berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }

    }

    public function cancelSell(Request  $request)
    {
        try{
            DB::beginTransaction();
            $data=AssetDepreciation::find($request->id);
            $date = new \DateTime($data->sell_date);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }


            JournalEntity::where('ref_code',$data->sell_code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->update([
                    'is_deleted'=>1
                ]);

            $data->sell_code=null;
            $data->sell_date=null;
            $data->sell_second_code=null;
            $data->sell_trans_proof=null;
            $data->sell_coa_detail_id=null;
            $data->sell_ppn_amount=0;
            $data->sell_coa_ppn_id=null;
            $data->is_sell=0;
            $data->sell_price=0;
            $data->save();

            DB::commit();

            return response()->json([
                'message'=>'Data penjualan aset berhasil dibatalkan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }
    }
    public function savePayment(Request  $request)
    {

        try{
            date_default_timezone_set($request->_timezone);
            DB::beginTransaction();

            $id=$request->id;

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'asset_payment',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $asset=AssetEntity::find($request->acc_asset_depreciation_id);
            $date = new \DateTime($request->paid_date);
            $paid_nominal=strtr($request->paid_nominal, array('.' => '', ',' => '.'));
            $admin_fee=strtr($request->admin_fee, array('.' => '', ',' => '.'));
            if(AccUtil::checkClosingJournal($asset->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }

            if($date->format('Y-m-d')<=Carbon::parse($asset->record_time)->toDateString())
            {
                return response()->json([
                    'message'=>'Waktu pembayaran tidak boleh kurang dari waktu pencatatan aset',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $data=new AssetDepreciationPayment();
            if($paid_nominal>($asset->asset_value-$asset->lack_of_payment))
            {
                return response()->json([
                    'message'=>'Nominal pembayaran melebihi nominal yang ada',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }else{
                if($asset->asset_value-($asset->lack_of_payment+$paid_nominal)==0)
                {
                    $asset->lack_of_payment=$asset->asset_value;
                    $asset->is_paid=1;
                    $asset->save();
                }else{
                    $asset->lack_of_payment+=$paid_nominal;
                    $asset->save();
                }
            }

            $destinationPath = 'public/uploads/merchant/'.$asset->md_merchant_id.'/acc/jurnal/asset/';

            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->trans_proof;
                }
            }

            if(is_null($id))
            {
                $data->code=CodeGenerator::generalCode('PYA',$asset->md_merchant_id);

            }

            if($request->payment_coa_detail_id=='-1')
            {
                return response()->json([
                    'message'=>'Silahkan pilih metode pembayaran terlebih dahulu',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $data->paid_date=$request->paid_date;
            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
            $data->paid_nominal=$paid_nominal;
            $data->acc_asset_depreciation_id=$asset->id;
            $data->type_payment=0;
            $data->trans_proof=$fileName;
            $data->desc=$request->desc;
            $data->timezone=$request->_timezone;
            $data->acc_payment_id=$request->payment_coa_detail_id;
            $data->admin_fee=$admin_fee;
            $data->save();
            if($this->generateJurnalPayment($data,$asset)==false){

                return response()->json([
                    'message'=>'Terjadi kesalahan saat pencatatan jurnal pembayaran pembelian aset',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);

            }

            DB::commit();
            return response()->json([
                'message'=>'Data pembayaran aset berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }

    }

    public function deletePayment(Request  $request)
    {
        try{
            DB::beginTransaction();
            $data=AssetDepreciationPayment::find($request->id);
            $date = new \DateTime($data->getAssetDepreciation->paid_date);
            if(AccUtil::checkClosingJournal($data->getAssetDepreciation->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>false
                ]);
            }
            $data->getAssetDepreciation->is_paid=0;
            $data->getAssetDepreciation->lack_of_payment-=$data->paid_nominal;
            $data->getAssetDepreciation->save();

            JournalEntity::where('ref_code',$data->code)
                ->where('md_merchant_id',$data->getAssetDepreciation->md_merchant_id)
                ->update([
                    'is_deleted'=>1
                ]);
            $data->delete();
            DB::commit();

            return response()->json([
                'message'=>'Data pembayaran aset berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false
            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }

    }


    private function generateJurnalPayment($data,$asset)
    {
        try{
            DB::beginTransaction();
            $coaUtangAsset=merchant_detail_multi_branch($asset->md_merchant_id)->coa_debt_asset_id;

            $adminFee=merchant_detail_multi_branch($asset->md_merchant_id)->coa_administration_bank_id;

            $journal=new JournalEntity();
            $code=CodeGenerator::generalCode('JRN',$asset->md_merchant_id);
            $journal->trans_code=$code;
            $journal->ref_code=$data->code;
            $assetCode=is_null($asset->second_code)?$asset->code:$asset->second_code;
            $paymentCode=is_null($data->second_code)?$data->code:$data->second_code;
            $journal->trans_name='Pembayaran Aset '.$asset->asset_name.' '.$assetCode.'-'.$paymentCode;
            $journal->trans_time=$data->paid_date;
            $journal->trans_note='Pembayaran Aset '.$asset->asset_name.' '.$assetCode.'-'.$paymentCode;
            $journal->trans_purpose='Pembayaran Aset '.$asset->asset_name.' '.$assetCode.'-'.$paymentCode;
            $journal->trans_proof=$data->trans_proof;
            $journal->trans_type=0;
            $journal->trans_amount=$data->paid_nominal+$data->admin_fee;
            $journal->md_merchant_id=$asset->md_merchant_id;
            $journal->md_user_id_created=user_id();
            $journal->external_ref_id=$asset->id;
            $journal->timezone=$data->timezone;
            $journal->save();

            if($data->admin_fee==0)
            {
                $detail=[
                    [
                        'acc_coa_detail_id'=>$coaUtangAsset,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ],
                    [
                        'acc_coa_detail_id'=>$data->acc_payment_id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ]
                ];
            }else{
                $detail=[
                    [
                        'acc_coa_detail_id'=>$coaUtangAsset,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ],
                    [
                        'acc_coa_detail_id'=>$adminFee,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->admin_fee,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ],
                    [
                        'acc_coa_detail_id'=>$data->acc_payment_id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal+$data->admin_fee,
                        'created_at'=>$journal->trans_time,
                        'updated_at'=>$journal->trans_time,
                        'acc_jurnal_id'=>$journal->id,
                        'ref_code'=>$journal->trans_code,
                    ]
                ];
            }

            JurnalDetail::insert($detail);
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }


    public function getDepreciation(Request  $request)
    {
        try {
            $assetId=$request->asset_id;
            $data=AssetDepreciationDetail::
            select([
                'id',
                'value_depreciation',
                DB::raw("to_char(time,'Mon') as month_name"),
                DB::raw("to_char(time,'YYYY') as year_name")
            ])
                ->where('acc_asset_depreciation_id',$assetId)
                ->orderBy('time','asc')
                ->get();

            return response()->json($data);

        }catch (\Exception $e)
        {
            return response()->json([]);
        }
    }


    public function getSelectBenefit(Request  $request)
    {
        $id=$request->id;
        $methodId=$request->method_id;
        if(!is_null($methodId))
        {
            $query= "and ag.acc_md_depreciation_method_id=$methodId";
        }else{
            $query="";
        }
        $data=DB::select("select
              d.id,
              d.name,
              ag.benefit_period,
              ag.depreciation_value
            from
              acc_md_property_depreciation_groups ag
              join acc_md_depreciation_methods d on ag.acc_md_depreciation_method_id = d.id
              join acc_md_property_groups g on g.id = ag.acc_md_property_group_id
            where
              ag.acc_md_property_group_id = $id $query
            ");

        if(!is_null($methodId))
        {
            return response()->json($data);

        }else{
            $response =[];
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->name,
                    'benefit'=>$item->benefit_period,
                    'value'=>$item->depreciation_value,
                    "selected"=>($item->id==1)?true:false
                ];
            }
            return response()->json($response);

        }

    }

    public function exportData(Request $request){
        try{
            $startDate = $request->startDate;
            $endDate = $request->endDate;

            $data=AssetEntity::getDataForDataTable();

            $encode=json_decode($request->md_merchant_id);

            if(is_array($encode))
            {
                $filterMerchant=$encode;
            }else{
                $filterMerchant=json_decode($encode);
            }

            if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
                $getBranch=MerchantUtil::getBranch(merchant_id(),0);

                $data->whereIn('m.id',$getBranch);
            }else{
                $data->whereIn('m.id',$filterMerchant);
            }

            if($startDate!='-')
            {
                $data->whereRaw("
            acc_asset_depreciations.record_time::date between '$startDate' and '$endDate'
        ");
            }



            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Pencatatan Asset Tanggal $startDate sampai $endDate ");

            $header = [
                'No',
                'Kode',
                'Nama Aset',
                'Kategori Aset',
                'Nilai Aset',
                'Sumber Pencatatan',
                'Waktu Pembelian',
                'Cabang',
                'Nilai Aset Saat Ini'
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $col++;
            }

            $num = 4;
            foreach ($data->get() as $key => $item) {
                $dataShow = [
                    $key+1,
                    $item->kode,
                    $item->nama_aset,
                    $item->kategori_aset,
                    rupiah($item->nilai_aset_original),
                    $item->sumber_pencatatan,
                    Carbon::parse($item->waktu_pembelian)->isoFormat('dddd, D MMMM Y').' '.getTimeZoneName($item->_timezone),
                    $item->outlet,
                    ($item->is_depreciation==0)?rupiah($item->nilai_aset_original):rupiah($item->current_asset_value)
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $col++;
                }
                $num++;
            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-asset'.merchant_id()."_".date('Y-m-d').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);

            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";

        }
    }

    public static function monthlyGenerate($merchantId,$userId)
    {
        try {
            $dateTimestamp=Carbon::now()->endOfMonth()->toDateString().' 23:59:59';
            $nowTimestamp=date('Y-m-d').' 23:59:59';
            if($dateTimestamp==$nowTimestamp){

                $assetData=AssetDepreciation::where('is_depreciation',1)
                    ->whereIn('md_merchant_id',MerchantUtil::getBranch($merchantId))
                    ->where('is_sell',0)
                    ->where('is_deleted',0)
                    ->get();
                if($assetData->count()>0)
                {
                    DB::beginTransaction();
                    $insertDepr=[];
                    $jurnalDetail=[];
                    foreach ($assetData as $data)
                    {
                        $now=Carbon::now()->toDateString();
                        $endDate=Carbon::parse($data->record_time)->addMonths($data->benefit_period*12)->toDateTimeString();
                        $start    = new \DateTime($data->record_time);
                        $end      = new \DateTime($endDate);
                        $interval = \DateInterval::createFromDateString('1 month');
                        $period   = new \DatePeriod($start, $interval, $end);

                        $calculate=AssetDepreciationMethodUtil::calculate($data->asset_value,$data->benefit_period,$data->value_depreciation,$data->acc_md_depreciation_method_id);
                        $totalDepreciation=0;
                        foreach ($period as $key => $date) {
                            $endOfMonth=Carbon::parse($date->format("Y-m-d H:i:s"))->endOfMonth();
                            if($now==$endOfMonth->toDateString())
                            {
                                $check=AssetDepreciationDetail::whereDate('time',$endOfMonth->toDateString())
                                    ->where('acc_asset_depreciation_id',$data->id)->first();
                                if(is_null($check))
                                {
                                    $totalDepreciation+=$calculate[$key];
                                    $codeDepr='DEP'.$data->md_merchant_id.$data->id.($key+1);
                                    $insertDepr[] = [
                                        'code'=>$codeDepr,
                                        'value_depreciation'=>$calculate[$key],
                                        'time'=>$endOfMonth->toDateString(),
                                        'record_time_to_journal'=>$endOfMonth->toDateString().' 23:59:59',
                                        'is_record_to_journal'=>1,
                                        'acc_asset_depreciation_id'=>$data->id,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'),
                                        'timezone'=>$data->_timezone
                                    ];

                                    $journal=new JournalEntity();
                                    $code=CodeGenerator::generalCode('JRN',$data->md_merchant_id);
                                    $journal->trans_code=$code;
                                    $journal->ref_code=$codeDepr;
                                    $assetCode=is_null($data->second_code)?$data->code:$data->second_code;
                                    $journal->trans_name='Penyusutan '.$data->asset_name.' '.$assetCode;
                                    $journal->trans_time=$endOfMonth->toDateString().' 23:59:59';
                                    $journal->trans_note='Penyusutan '.$data->asset_name.' '.$assetCode;
                                    $journal->trans_purpose='Penyusutan '.$data->asset_name.' '.$assetCode;
                                    $journal->trans_proof='Penyusutan '.$data->asset_name.' '.$assetCode;
                                    $journal->trans_type=0;
                                    $journal->trans_amount=$calculate[$key];
                                    $journal->md_merchant_id=$data->md_merchant_id;
                                    $journal->md_user_id_created=$userId;
                                    $journal->timezone=$data->_timezone;
                                    $journal->save();

                                    array_push($jurnalDetail,[
                                        'acc_coa_detail_id'=>$data->account_depreciation_id,
                                        'coa_type'=>'Debit',
                                        'md_sc_currency_id'=>1,
                                        'amount'=>$calculate[$key],
                                        'created_at'=>$journal->trans_time,
                                        'updated_at'=>$journal->trans_time,
                                        'acc_jurnal_id'=>$journal->id,
                                        'ref_code'=>$journal->trans_code,
                                    ]);

                                    array_push($jurnalDetail,[
                                        'acc_coa_detail_id'=>$data->account_accumulation_id,
                                        'coa_type'=>'Kredit',
                                        'md_sc_currency_id'=>1,
                                        'amount'=>$calculate[$key],
                                        'created_at'=>$journal->trans_time,
                                        'updated_at'=>$journal->trans_time,
                                        'acc_jurnal_id'=>$journal->id,
                                        'ref_code'=>$journal->trans_code,
                                    ]);
                                }
                            }else{
                                break;
                            }
                        }
                        $data->current_asset_value=$data->asset_value-$totalDepreciation;
                        $data->save();
                        DB::commit();

                    }
                    AssetDepreciationDetail::insert($insertDepr);
                    JurnalDetail::insert($jurnalDetail);
                    DB::commit();
                    return true;
                }else{
                    DB::commit();
                    return true;
                }

            }else{
                DB::commit();
                return true;
            }
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

    private function initCoaIntangibleAsset($merchantId)
    {
        try {
            $check=CoaCategory::where('name','Aktiva Tidak Berwujud')
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->first();
            $coaBeban=CoaCategory::where('name','Beban Operasional & Usaha')
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->first();
            if(!is_null($check))
            {
                $checkDetail=CoaDetail::where('acc_coa_category_id',$check->id)
                    ->where('name','Akumulasi Amortisasi Aset Tidak Berwujud')
                    ->where('is_deleted',0)
                    ->first();
                if(is_null($checkDetail))
                {

                    CoaDetail::insert([
                        'code' => CoaUtil::generateCoaDetail($check->id),
                        'name' => 'Aset Tidak Berwujud',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $check->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    CoaDetail::insert([
                        'code' => CoaUtil::generateCoaDetail($check->id),
                        'name' => 'Akumulasi Amortisasi Aset Tidak Berwujud',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $check->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    $coaCostAmortation=CoaDetail::insertGetId([
                        'code' => CoaUtil::generateCoaDetail($coaBeban->id),
                        'name' => 'Amortisasi Aset Tidak Berwujud',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $coaBeban->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);

                    $cashflowFormat=CoaCashflowFormat::
                    where('md_merchant_id',$merchantId)
                        ->where('name','Beban Operasional & Usaha')
                        ->first();

                    if(!is_null($cashflowFormat))
                    {
                        CoaCashflowFormatDetail::insert([
                            'acc_coa_cashflow_format_id'=>$cashflowFormat->id,
                            'acc_coa_detail_id'=>$coaCostAmortation
                        ]);
                    }

                }
            }
            return true;
        }catch (\Exception $e)
        {
            return false;

        }
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc\Asset;


use App\Http\Controllers\Controller;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Modules\Merchant\Entities\Asset\CategoryEntity;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('asset/category')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\Asset\CategoryController@index')
                    ->name('merchant.asset.category.index');
                Route::post('/data-table', 'Acc\Asset\CategoryController@dataTable')
                    ->name('merchant.asset.category.datatable');
                Route::post('/add', 'Acc\Asset\CategoryController@add')
                    ->name('merchant.asset.category.add');
                Route::post('/save', 'Acc\Asset\CategoryController@save')
                    ->name('merchant.asset.category.save');
                Route::post('/delete', 'Acc\Asset\CategoryController@delete')
                    ->name('merchant.asset.category.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Kategori',
            'tableColumns'=>CategoryEntity::dataTableColumns(),
            'add'=>(get_role()==3)?CategoryEntity::add(true):
                CategoryEntity::add(true),
                'searchKey' => md5(Uuid::uuid4()->toString()),
                'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
                'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),

        ];

        return view('merchant::acc.asset.category.index',$params);


    }

    public function dataTable(Request $request)
    {
        return CategoryEntity::dataTable($request);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=CategoryEntity::find($id);
        }else{
            $data=new CategoryEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::acc.asset.category.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=CategoryEntity::find($id);
            }else{
                $data=new CategoryEntity();
                $data->md_merchant_id=merchant_id();

            }

            $checkPackageSubscription=MerchantUtil::checkUrl(merchant_id());

            if($checkPackageSubscription['status']==true)
            {
                return response()->json([
                    'message'=>$checkPackageSubscription['message'],
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            $data->name=$request->name;
            $data->desc=$request->description;
            $data->save();

            return response()->json([
                'message'=>'Kategori aset berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=CategoryEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            return response()->json([
                'message'=>'Kategori aset berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }
}

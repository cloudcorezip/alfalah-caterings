<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc;


use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\MasterData\User;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\FindCodeUtil;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use Modules\Merchant\Entities\Acc\JournalEntity;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\SennaToko\Supplier;
use App\Classes\Singleton\CodeGenerator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Modules\Merchant\Entities\Toko\SupplierEntity;
use Ramsey\Uuid\Uuid;
use Image;
use Str;
use Illuminate\Support\Facades\DB;

class JournalController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/journal')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\JournalController@index')
                    ->name('merchant.toko.acc.journal.index');
                Route::get('/get-coa', 'Acc\JournalController@getCoa')
                    ->name('merchant.toko.acc.journal.get-coa');
                Route::get('/get-cash-bank', 'Acc\JournalController@getCashBank')
                    ->name('merchant.toko.acc.journal.get-cash-bank');
                Route::get('/get-bank', 'Acc\JournalController@getBank')
                    ->name('merchant.toko.acc.journal.get-bank');
                Route::get('/get-cash', 'Acc\JournalController@getCash')
                    ->name('merchant.toko.acc.journal.get-cash');
                Route::get('/get-supplier', 'Acc\JournalController@getSupplier')
                    ->name('merchant.toko.acc.journal.get-supplier');
                Route::post('/data-table', 'Acc\JournalController@dataTable')
                    ->name('merchant.toko.acc.journal.datatable');
                Route::get('/add', 'Acc\JournalController@add')
                    ->name('merchant.toko.acc.journal.add');
                Route::post('/save', 'Acc\JournalController@save')
                    ->name('merchant.toko.acc.journal.save');
                Route::post('/delete', 'Acc\JournalController@delete')
                    ->name('merchant.toko.acc.journal.delete');
                Route::post('/reload-data', 'Acc\JournalController@reloadData')
                    ->name('merchant.toko.acc.journal.reload');
                Route::post('/exportData', 'Acc\JournalController@exportData')
                    ->name('merchant.toko.acc.journal.exportData');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Jurnal Umum',
            'tableColumns'=>JournalEntity::dataTableColumns(),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'add'=>(get_role()==3)?JournalEntity::add(true):
                JournalEntity::add(true),
            'key_val'=>$request->key,
            'merchantId'=>[-1],
            'encodeMerchant'=>base64_encode(json_encode(MerchantUtil::getBranch(merchant_id())))
        ];

        return view('merchant::acc.journal.index',$params);

    }

    public function reloadData(Request $request)
    {
        $searchParams = [];
        foreach (JournalEntity::getFilterMap() as $key => $value) {
            $query = $request->input($key);
            if ($query !== '-1') {
                $searchParams[$key] = $query;
            }
        }
        $searchKey = $request->input('search_key');
        $request->session()->put($searchKey, $searchParams);
        $request['count_data'] = 1;


        $params = [
            'title' => 'Jurnal Umum',
            'searchKey' => $searchKey,
            'tableColumns'=>JournalEntity::dataTableColumns(),
            'start_date'=>(is_null($request->startDate))?Carbon::now()->startOfMonth()->toDateString():$request->startDate,
            'end_date'=>(is_null($request->endDate))?Carbon::now()->endOfMonth()->toDateString():$request->endDate,
            'merchantId'=>$request->md_merchant_id,
            'encodeMerchant'=>base64_encode(json_encode($request->md_merchant_id))


        ];
        return view('merchant::acc.journal.list',$params);

    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return JournalEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=JournalEntity::find($id);
            if(is_null($data)){
                abort(404);
            }

            $detJurnal=JurnalDetail::where('acc_jurnal_id',$id)->get();
        }else{
            $data=new JournalEntity();
            $detJurnal=null;
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'n'=>0,
            'supplier'=>SupplierEntity::listOption(),
            'detJurnal' =>$detJurnal,
            'getCoa'=>json_encode($this->getCoaV2())
        ];


        return view('merchant::acc.journal.add',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;

            if(FindCodeUtil::findCode(str_replace(' ','',$request->code),$request->md_merchant_id,'jurnal',0,$request->id)==true)
            {
                return response()->json([
                    'message' => "Kode sudah digunakan !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            date_default_timezone_set($request->_timezone);
            $date = new \DateTime($request->trans_time);
            if(AccUtil::checkClosingJournal($request->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }
            if(!is_null($id)){

                $data=Jurnal::find($id);
                JurnalDetail::where('acc_jurnal_id',$data->id)->delete();
            }else{
                $data=new Jurnal();
            }

            $acc_coa_category_id=$request->acc_coa_category_id;
            $amountDebit=$request->amount_debit;
            $amountKredit=$request->amount_kredit;
            $sumDebit=0;
            $sumKredit=0;
            $desc=$request->desc;
            foreach ($acc_coa_category_id as $key => $item)
            {
                $a=strtr($amountDebit[$key], array('.' => '', ',' => '.'));
                $sumDebit+=$a;
                $b=strtr($amountKredit[$key], array('.' => '', ',' => '.'));
                $sumKredit+=$b;
            }

            if($sumDebit==0){
                return response()->json([
                    'message'=>'Terjadi kesalahan! jumlah kredit atau debit harus lebih besar dari 0',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }else if($sumKredit==0){
                return response()->json([
                    'message'=>'Terjadi kesalahan! jumlah kredit atau debit harus lebih besar dari 0',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }else if($sumDebit!=$sumKredit){
                return response()->json([
                    'message'=>'Terjadi kesalahan! Jumlah kredit dan debit harus sama',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }


            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->trans_proof;

                }
            }
            if(is_null($id))
            {
                $code=CodeGenerator::generalCode('JRN',$request->md_merchant_id);
                $data->trans_code=$code;
                $data->ref_code=$code;


            }
            $data->second_code=(is_null($request->code)|| $request->code=='')?null:str_replace(' ','',$request->code);
            $code=(is_null($data->second_code))?$data->trans_code:$data->second_code;
            $supplier=Supplier::find($request->trans_purpose);
            $data->md_user_id_created=user_id();
            $data->md_merchant_id=$request->md_merchant_id;
            $data->trans_name=$request->trans_name.' '.$code;
            $data->trans_purpose=($request->trans_purpose=='-1')?'Internal Perusahaan':$supplier->name;
            if(!is_null($supplier))
            {
                $data->sc_supplier_id=$supplier->id;
            }
            $data->trans_time=$date->format('Y-m-d H:i:s');
            $data->trans_proof=$fileName;
            $data->trans_note=$request->trans_note;
            $data->trans_amount=$sumDebit;
            $data->trans_type=1;
            $data->timezone=$request->_timezone;
            $data->save();
            $details=[];
            foreach ($acc_coa_category_id as $key => $item)
            {
                $a=strtr($amountDebit[$key], array('.' => '', ',' => '.'));
                $b=strtr($amountKredit[$key], array('.' => '', ',' => '.'));

                $details[]=[
                    'acc_coa_detail_id'=>$acc_coa_category_id[$key],
                    'coa_type'=>($a==0)?'Kredit':'Debit',
                    'amount'=>($a==0)?$b:$a,
                    'acc_jurnal_id'=>$data->id,
                    'created_at'=>$date->format('Y-m-d H:i:s'),
                    'updated_at'=>$date->format('Y-m-d H:i:s'),
                    'md_sc_currency_id'=>1,
                    'trans_desc'=>is_null($desc[$key])?$request->trans_name:$desc[$key]
                ];
            }
            JurnalDetail::insert($details);
            DB::commit();

            return response()->json([
                'message'=>'Pencatatan jurnal umum berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>\route('merchant.toko.acc.journal.index'),
            ]);
        }catch (\Exception $e)
        {
           DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>'',
            ]);
        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=JournalEntity::find($id);
            $date = new \DateTime($data->trans_time);

            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message'=>'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>'',
                ]);
            }else{
                $data->is_deleted=1;
                $data->save();
                return response()->json([
                    'message'=>'Pencatatan jurnal umum berhasil dihapus',
                    'type'=>'success',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                ]);
            }
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
            ]);

        }
    }

    private function getCoaV2()
    {
        $merchantString="(";
        foreach (get_cabang() as $key => $branch)
        {
            $merchantString.=($key==0)?"".$branch->id."":",".$branch->id."";;

        }
        $merchantString.=")";

        $data=collect(DB::select("
        select acc.parent_code,acd.code,acd.id,acd.name,acc.md_merchant_id from 
        acc_coa_details acd
        join acc_coa_categories acc
        on acd.acc_coa_category_id=acc.id
        where acd.is_deleted in (0,2) and acc.is_deleted in (0,2) and acd.name != 'Payment Gateway'
        and acc.md_merchant_id in $merchantString order by acc.code,acd.code asc
        "));
        $response =[];
        foreach($data as $key =>$item){

            $response[] = [
                "id"=>$item->id,
                "text"=>$item->parent_code.' - '.str_replace('.','',$item->code)."  ".$item->name,
                "disabled"=>false,
                'md_merchant_id'=>$item->md_merchant_id
            ];
        }
        return $response;
    }


    public function getSupplier(Request $request)
    {

        $categoryOption=SupplierEntity::listOption();
        $response =[];
        foreach($categoryOption as $key =>$item){

                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->nama_supplier
                ];

        }
        return response()->json($response);
    }

    public function exportData(Request $request){
        try{
            $data=JournalEntity::getDataForDataTable();

            $encode=json_decode($request->md_merchant_id);

            if(is_array($encode))
            {
                $filterMerchant=$encode;
            }else{
                $filterMerchant=json_decode($encode);
            }

            if($filterMerchant[0]=='-1' || $filterMerchant[0]==-1){
                $getBranch=MerchantUtil::getBranch(merchant_id(),0);

                $data->whereIn('acc_jurnals.md_merchant_id',$getBranch);
            }else{
                $data->whereIn('acc_jurnals.md_merchant_id',$filterMerchant);
            }

            if($request->startDate!='-')
            {
                $data->whereRaw("
                acc_jurnals.trans_time::date between '$request->startDate' and '$request->endDate'
                ");
            }
            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $sd=date("Y-m-d", strtotime($request->startDate));
            $ed=date("Y-m-d", strtotime($request->endDate));
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Pencatatan Jurnal Tanggal $sd sampai $ed ");

            $header = [
                'No',
                'Kode',
                'Nama Transaksi',
                'Waktu Transaksi',
                'Tujuan Transaksi',
                'Bukti Transaksi',
                'Keterangan',
                'Cabang',
                'Akun',
                'Debit',
                'Kredit'
            ];
            $col = 'A';
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '4', strtoupper($item));
                $col++;
            }

            $num = 4;
            $no=0;
            foreach ($data->get() as $key => $item) {
                foreach ($item->getDetail as $d)
                {
                    $dataShow = [
                        $key+1,
                        $item->kode,
                        $item->nama_transaksi,
                        Carbon::parse($item->waktu_transaksi)->isoFormat('dddd, D MMMM Y').' '.getTimeZoneName($item->timezone),
                        $item->tujuan_transaksi,
                        env('S3_URL').$item->bukti_transaksi,
                        $item->keterangan,
                        $item->outlet,
                        $d->getCoa->name,
                        ($d->coa_type=='Debit')?rupiah($d->amount):rupiah(0),
                        ($d->coa_type=='Kredit')?rupiah($d->amount):rupiah(0)
                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $col++;
                    }
                    $num++;
                    $no++;
                }

            }
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.merchant_id().'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-pencatatan-jurnal'.merchant_id()."_".date('Y-m-d').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <script>
                toastForSaveData('Jurnal Umum berhasil diexport!','success',true,'')
                 setTimeout(() => {
                        closeModal();
                    }, 3000)
            </script>
            ";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<script>toastForSaveData('Terjadi kesalahan, Data jurnal umum gagal diexport!','danger',true,'')
                    setTimeout(() => {
                        closeModal();
                    }, 1500)

</script>";

        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc\Report;


use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\MasterData\Merchant;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PDF;

class LedgerController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('report/ledger')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\Report\LedgerController@index')
                    ->name('merchant.toko.acc.report.ledger.index');
                Route::post('/export-excel', 'Acc\Report\LedgerController@exportExcel')
                    ->name('merchant.toko.acc.report.ledger.exportExcel');
            });
    }

    public function index()
    {
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
        $startDate = request()->start_date;
        $endDate = request()->end_date;
        $coa=request()->coa;
        if(is_null($startDate))
        {
            $firstDate=Carbon::now()->firstOfMonth();
            $lastDate=Carbon::now()->lastOfMonth();
        }else{
            $firstDate=Carbon::parse($startDate);
            $lastDate=Carbon::parse($endDate);
        }

        if(!is_null($coa) && ($coa=='-1')==false)
        {
            $data=collect(DB::select("
            SELECT acd.id,
            acc.parent_code,
            acd.code,
            acd.name,
            j.trans_code,
            j.trans_time,
            j.md_merchant_id,
            j.coa_name,
            ajd.coa_type,
            acd.type_coa,
            ajd.amount,
                    j.ref_code,
            j.trans_purpose,
            to_char(j.trans_time, 'YYYY-MM'::text) AS month_year,
            to_char(j.trans_time, 'MM'::text) AS month,
            to_char(j.trans_time, 'YYYY'::text) AS year,
            to_char(j.trans_time, 'YYYY-MM-DD'::text) AS original
        FROM acc_coa_details acd
            JOIN acc_jurnal_details ajd ON acd.id = ajd.acc_coa_detail_id
            JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
            JOIN acc_coa_categories acc on acc.id=acd.acc_coa_category_id
        WHERE j.is_deleted = ANY (ARRAY[0, 2]) and j.md_merchant_id=$merchantId and acd.id=$coa
        ORDER BY acd.code, j.trans_time asc
                    "));

        }else{
            $data=collect(DB::select("
                    SELECT acd.id,
                             acc.parent_code,
            acd.code,
            acd.name,
            j.trans_code,
            j.trans_time,
            j.md_merchant_id,
            j.coa_name,
            ajd.coa_type,
            acd.type_coa,
            ajd.amount,
            j.ref_code,
            j.trans_purpose,
            to_char(j.trans_time, 'YYYY-MM'::text) AS month_year,
            to_char(j.trans_time, 'MM'::text) AS month,
            to_char(j.trans_time, 'YYYY'::text) AS year,
            to_char(j.trans_time, 'YYYY-MM-DD'::text) AS original
        FROM acc_coa_details acd
            JOIN acc_jurnal_details ajd ON acd.id = ajd.acc_coa_detail_id
            JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                     JOIN acc_coa_categories acc on acc.id=acd.acc_coa_category_id
        WHERE j.is_deleted = ANY (ARRAY[0, 2]) and j.md_merchant_id=$merchantId
        ORDER BY acd.code, j.trans_time asc"));

        }


        $result=[];
        foreach ($data->groupBy('id') as $key =>$item)
        {
            $amountDebitPrevious=0;
            $amountKreditPrevious=0;
            $amountDebitThisMonth=0;
            $amountKreditThisMonth=0;

            foreach ($item->where('original','<',$firstDate->toDateString()) as $prev)
            {
                if($prev->original<=$firstDate->toDateString())
                {
                    ($prev->coa_type=='Debit')?$amountDebitPrevious+=$prev->amount:$amountKreditPrevious+=$prev->amount;
                }
            }
            $child=$item->where('original','>=',$firstDate->toDateString())->where('original','<=',$lastDate->toDateString());
            foreach ($child as $value)
            {
                if($value->original>=$firstDate->toDateString() && $value->original<=$lastDate->toDateString())
                {
                    ($value->coa_type=='Debit')?$amountDebitThisMonth+=$value->amount:$amountKreditThisMonth+=$value->amount;

                }
            }
            $result[]=(object)[
                'parent_code'=>$item->first()->parent_code,
                'ref_code'=>$item->first()->ref_code,
                'code'=>$item->first()->code,
                'name'=>$item->first()->name,
                'type_coa'=>$item->first()->type_coa,
                'child'=>(object)$child,
                'sum_of_before_debit'=>$amountDebitPrevious,
                'sum_of_before_kredit'=>$amountKreditPrevious,
                'sum_of_debit'=>$amountDebitThisMonth,
                'sum_of_kredit'=>$amountKreditThisMonth
            ];
        }



        $sumOfCoaAmount = [
            'sum_of_total_debit' => collect($result)->reduce(function($i, $k) {
                return $i + $k->sum_of_debit+ $k->sum_of_before_debit;
            }, 0),
            'sum_of_total_kredit' => collect($result)->reduce(function($i, $k) {
                return $i + $k->sum_of_kredit+$k->sum_of_before_kredit;
            }, 0)
        ];

        $merchant=Merchant::find($merchantId);

        $params=[
            'title'=>'Laporan Buku Besar '.$merchant->name.' ',
            'sub_title'=>$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y').'',
            'data'=>collect($result),
            'sumOfCoaAmount'=>$sumOfCoaAmount,
            'date'=>$firstDate->toDateString(),
            'period'=>(is_null(request()->period_name))?
                date('Y-m')
                :request()->period_name,
            'coa'=>(is_null(request()->coa)?'-1':request()->coa),
            'startDate'=>$firstDate,
            'endDate'=>$lastDate,
            'merchantId'=>$merchantId,
            'merchant'=>$merchant

        ];

        return view('merchant::acc.report.ledger.index',$params);
    }
    public function exportExcel(){
        try{
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            $startDate = request()->start_date;
            $endDate = request()->end_date;
            $coa=request()->coa;
            if(is_null($startDate))
            {
                $firstDate=Carbon::now()->firstOfMonth();
                $lastDate=Carbon::now()->lastOfMonth();
            }else{
                $firstDate=Carbon::parse($startDate);
                $lastDate=Carbon::parse($endDate);
            }
            if(!is_null($coa) && ($coa=='-1')==false)
            {
                $data=collect(DB::select("
            SELECT acd.id,
            acc.parent_code,
            acd.code,
            acd.name,
            j.trans_code,
            j.trans_time,
            j.md_merchant_id,
            ajd.coa_type,
            acd.type_coa,
            ajd.amount,
                    j.ref_code,
            j.trans_purpose,
            to_char(j.trans_time, 'YYYY-MM'::text) AS month_year,
            to_char(j.trans_time, 'MM'::text) AS month,
            to_char(j.trans_time, 'YYYY'::text) AS year,
            to_char(j.trans_time, 'YYYY-MM-DD'::text) AS original
        FROM acc_coa_details acd
            JOIN acc_jurnal_details ajd ON acd.id = ajd.acc_coa_detail_id
            JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
            JOIN acc_coa_categories acc on acc.id=acd.acc_coa_category_id
        WHERE j.is_deleted = ANY (ARRAY[0, 2]) and j.md_merchant_id=$merchantId and acd.id=$coa
        ORDER BY acd.code, j.trans_time asc
                    "));

            }else{
                $data=collect(DB::select("
                    SELECT acd.id,
                             acc.parent_code,
            acd.code,
            acd.name,
            j.trans_code,
            j.trans_time,
            j.md_merchant_id,
            ajd.coa_type,
            acd.type_coa,
            ajd.amount,
            j.ref_code,
            j.trans_purpose,
            j.trans_name,
            to_char(j.trans_time, 'YYYY-MM'::text) AS month_year,
            to_char(j.trans_time, 'MM'::text) AS month,
            to_char(j.trans_time, 'YYYY'::text) AS year,
            to_char(j.trans_time, 'YYYY-MM-DD'::text) AS original
        FROM acc_coa_details acd
            JOIN acc_jurnal_details ajd ON acd.id = ajd.acc_coa_detail_id
            JOIN acc_jurnals j ON j.id = ajd.acc_jurnal_id
                     JOIN acc_coa_categories acc on acc.id=acd.acc_coa_category_id
        WHERE j.is_deleted = ANY (ARRAY[0, 2]) and j.md_merchant_id=$merchantId
        ORDER BY acd.code, j.trans_time asc"));

            }


            $result=[];
            foreach ($data->groupBy('id') as $key =>$item)
            {
                $amountDebitPrevious=0;
                $amountKreditPrevious=0;
                $amountDebitThisMonth=0;
                $amountKreditThisMonth=0;

                foreach ($item->where('original','<',$firstDate->toDateString()) as $prev)
                {
                    if($prev->original<$firstDate->toDateString())
                    {
                        ($prev->coa_type=='Debit')?$amountDebitPrevious+=$prev->amount:$amountKreditPrevious+=$prev->amount;
                    }
                }




                $child=$item->whereBetween('original',[$firstDate->toDateString(),$lastDate->toDateString()]);
                foreach ($item->whereBetween('original',[$firstDate->toDateString(),$lastDate->toDateString()]) as $value)
                {
                    if($value->original>=$firstDate->toDateString() && $value->original<=$lastDate->toDateString())
                    {
                        ($value->coa_type=='Debit')?$amountDebitThisMonth+=$value->amount:$amountKreditThisMonth+=$value->amount;

                    }
                }
                $result[]=(object)[
                    'parent_code'=>$item->first()->parent_code,
                    'code'=>$item->first()->code,
                    'name'=>$item->first()->name,
                    'ref_code'=>$item->first()->ref_code,
                    'type_coa'=>$item->first()->type_coa,
                    'child'=>(object)$child,
                    'sum_of_before_debit'=>$amountDebitPrevious,
                    'sum_of_before_kredit'=>$amountKreditPrevious,
                    'sum_of_debit'=>$amountDebitThisMonth,
                    'sum_of_kredit'=>$amountKreditThisMonth
                ];
            }



            $sumOfCoaAmount = [
                'sum_of_total_debit' => collect($result)->reduce(function($i, $k) {
                    return $i + $k->sum_of_debit+ $k->sum_of_before_debit;
                }, 0),
                'sum_of_total_kredit' => collect($result)->reduce(function($i, $k) {
                    return $i + $k->sum_of_kredit+$k->sum_of_before_kredit;
                }, 0)
            ];

            $merchant=Merchant::find($merchantId);


            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1','Laporan Buku Besar '.$merchant->name.' Periode '.$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y').'');

            $header = [
                'Tanggal',
                'Transaksi',
                'Debit',
                'Kredit',
                'Saldo',
                'Keterangan'
            ];
            $col = 'A';
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '3', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->getFont()->setBold( true );
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($styleArray);
                $col++;
            }

            $num = 3;
            $data=collect($result);
            foreach ($data as $key => $item) {
                $dataShow1 = [
                    $item->parent_code .' - '.str_replace('.','',$item->code)." ".$item->name,
                    " ",
                    " ",
                    " ",
                    " ",
                    " "
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow1 as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold( true );
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;
                if($item->type_coa=='Debit'){
                    if($item->sum_of_before_debit<$item->sum_of_before_kredit){
                        $sum="(".rupiah(abs($item->sum_of_before_kredit-$item->sum_of_before_debit)).")";
                    }else{
                        $sum=rupiah($item->sum_of_before_debit-$item->sum_of_before_kredit);
                    }
                }else{
                    if($item->sum_of_before_kredit<$item->sum_of_before_debit){
                        $sum="(".rupiah(abs($item->sum_of_before_debit-$item->sum_of_before_kredit)).")";
                    }else{
                        $sum=rupiah($item->sum_of_before_kredit-$item->sum_of_before_debit);
                    }
                }
                $dataShow2 = [
                    "Saldo Awal",
                    " ",
                    rupiah($item->sum_of_before_debit),
                    rupiah($item->sum_of_before_kredit),
                    $sum,
                    " "
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow2 as $ds2) {
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row,$ds2)->getFont()->setBold( true );
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $col++;

                }
                $num++;
                $initialDebit=$item->sum_of_before_debit-$item->sum_of_before_kredit;
                $temp=$initialDebit;
                $initialKredit=$item->sum_of_before_kredit-$item->sum_of_before_debit;
                $tempKredit=$initialKredit;

                foreach($item->child as $k =>$j){

                    if($item->type_coa=='Debit'){
                        if($j->coa_type=='Debit'){
                            $temp+=$j->amount;
                        }else{
                            $temp-=$j->amount;
                        }
                        $resultTemp=($temp<0)?'('.rupiah(abs($temp)).')':rupiah($temp);
                    }else{
                        if($j->coa_type=='Kredit'){
                            $tempKredit+=$j->amount;
                        }else{
                            $tempKredit-=$j->amount;
                        }
                        $resultTemp=($tempKredit<0)?'('.rupiah(abs($tempKredit)).')':rupiah($tempKredit);
                    }
                    $dataShow3 = [
                        \Carbon\Carbon::parse($j->trans_time)->isoFormat('D MMMM Y'),
                        $j->trans_purpose,
                        ($j->coa_type=='Debit')?rupiah($j->amount):'-',
                        ($j->coa_type=='Kredit')?rupiah($j->amount):'-',
                        $resultTemp,
                        ($j->trans_purpose=='Internal')?($j->trans_name):' '
                    ];
                    $col = 'A';
                    $row = $num+ 1;
                    foreach ($dataShow3 as $ds3) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds3);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;

                }
                if($item->type_coa=='Debit'){
                    if(($item->sum_of_before_debit+$item->sum_of_debit)-($item->sum_of_before_kredit+$item->sum_of_kredit)<0){
                        $end="(".rupiah(abs(($item->sum_of_before_debit+$item->sum_of_debit)-($item->sum_of_before_kredit+$item->sum_of_kredit))).")";
                    }else{
                        $end=rupiah(($item->sum_of_before_debit+$item->sum_of_debit)-($item->sum_of_before_kredit+$item->sum_of_kredit));
                    }
                }else{
                    if(($item->sum_of_before_kredit+$item->sum_of_kredit)-($item->sum_of_before_debit+$item->sum_of_debit)<0){
                        $end="(".rupiah(abs(($item->sum_of_before_kredit+$item->sum_of_kredit)-($item->sum_of_before_debit+$item->sum_of_debit))).")";
                    }else{
                        $end=rupiah(($item->sum_of_before_kredit+$item->sum_of_kredit)-($item->sum_of_before_debit+$item->sum_of_debit));
                    }
                }
                $dataShow4 = [
                    "Saldo Akhir ".$item->parent_code .' - '.str_replace('.','',$item->code)." ".$item->name,
                    " ",
                    rupiah($item->sum_of_before_debit+$item->sum_of_debit),
                    rupiah($item->sum_of_before_kredit+$item->sum_of_kredit),
                    $end,
                    ' '
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow4 as $ds4) {
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds4);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row,$ds4)->getFont()->setBold( true );
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
                    $col++;

                }
                $num+=2;

            }
            $row++;
            $totalDeb=rupiah($sumOfCoaAmount['sum_of_total_debit']);
            $totalKred=rupiah($sumOfCoaAmount['sum_of_total_debit']);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,"Total");
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $totalDeb);
            $objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row,$totalKred);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold( true );

            //dd($data);
            foreach(range('A','F') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.uniqid(date('YmdHis').merchant_id()).'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('Laporan-Buku-Besar-Periode-' .$firstDate->isoFormat('D-MMMM-Y').'-sampai-'.$lastDate->isoFormat('D-MMMM-Y').''. '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";
        }catch (\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }

    }

}

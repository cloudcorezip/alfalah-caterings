<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc\Report;


use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\MasterData\Merchant;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PDF;

class ProfitAndLostController extends Controller
{


    public function routeWeb()
    {
        Route::prefix('acc/report/profit-lost')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\Report\ProfitAndLostController@index')
                    ->name('merchant.toko.acc.report.profit-lost.index');
                Route::get('/exportPDF', 'Acc\Report\ProfitAndLostController@exportPDF')
                    ->name('merchant.toko.acc.report.profit-lost.exportPDF');
                Route::post('/exportExcel', 'Acc\Report\ProfitAndLostController@exportExcel')
                    ->name('merchant.toko.acc.report.profit-lost.exportExcel');
            });
    }



    public function index()
    {
        $startDate = request()->start_date;
        $endDate = request()->end_date;
        $merchantIds=request()->md_merchant_id;
        if(is_null($merchantIds))
        {
            $merchantId=[merchant_id()];
            $merchantIds=[merchant_id()];

        }elseif ($merchantIds[0]=='-1')
        {
            $merchantId=[];
            foreach (get_cabang() as $key => $branch)
            {

                $merchantId[]=$branch->id;
            }

        }else{
            $merchantId=$merchantIds;

        }


        if(is_null($startDate))
        {
            $firstDate=Carbon::now()->firstOfYear();
            $lastDate=Carbon::now()->lastOfYear();
            $isMonth=false;
        }else{
            $firstDate=Carbon::parse($startDate);
            $lastDate=Carbon::parse($endDate);
            $isMonth=true;
        }
        $titlePeriod=$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y');
        $profitLost=[];
        $merchant=Merchant::whereIn('id',$merchantId)->paginate(2);

        foreach ($merchant as $m => $mm)
        {
            $data=collect(DB::select("
            select
              acd.id,
              acd.code,
              acd.name,
              acd.type_coa,
              case WHEN acc.code in('4.1.00', '4.2.00') THEN 'pendapatan' ELSE 'pengeluaran' END AS profit_type,
              case WHEN acc.code = '4.1.00' THEN 'pendapatan_atas_penjualan'
              WHEN acc.code = '4.2.00' THEN 'pendapatan_lainnya'
              WHEN acc.code = '5.0.00' THEN 'harga_pokok_penjualan'
              WHEN acc.code = '6.1.00' THEN 'pengeluaran_operasional_&_usaha'
              ELSE 'pengeluaran_lainnya' END AS flag,
              CASE WHEN acc.code in ('4.1.00', '4.2.00')
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_revenue,
              CASE WHEN acc.code in('4.1.00', '4.2.00')
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_revenue_min,
              CASE WHEN acc.code = '5.0.00'
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_hpp,
              CASE WHEN acc.code = '5.0.00'
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_hpp_min,
              CASE WHEN acc.code in ('6.1.00', '6.2.00')
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_cost,
              CASE WHEN acc.code in('6.1.00', '6.2.00')
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_cost_min
            from
              acc_coa_details acd
              join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
              left join (
                select
                  ajd.acc_coa_detail_id,
                  ajd.amount,
                  ajd.coa_type
                from
                  acc_jurnal_details ajd
                  join acc_jurnals aj on ajd.acc_jurnal_id = aj.id
                WHERE
                  aj.is_deleted in (0, 2)
                  and aj.trans_time :: date  between '".$firstDate->toDateString()."' and '".$lastDate->toDateString()."' and aj.md_merchant_id=".$merchantId[$m]."
              ) j on j.acc_coa_detail_id = acd.id
            where
              acc.md_merchant_id = ".$mm->id."
              and acc.code in (
                '4.1.00', '4.2.00', '5.0.00',
                '6.1.00', '6.2.00'
              )
              and acd.is_deleted != 1
              and acd.code not in ('4.2.00.03')
            group by
              acd.id,
              acc.code,
              j.coa_type
            order by
              acc.code asc
            "));

            $result=[];

            foreach ($data->groupBy('profit_type') as $key =>$item)
            {
                $child=[];

                foreach ($item->groupBy('flag') as $i =>$j)
                {

                    if($i=='harga_pokok_penjualan')
                    {
                        $amount=$j->sum('amount_hpp')-$j->sum('amount_hpp_min');
                    }elseif ($i=='pendapatan_atas_penjualan' || $i=='pendapatan_lainnya')
                    {
                        $amount=$j->sum('amount_revenue')-$j->sum('amount_revenue_min');
                    }else{
                        $amount=$j->sum('amount_cost')-$j->sum('amount_cost_min');

                    }
                    $child[]=(object)[
                        'name'=>ucwords(str_replace('_',' ',$i)),
                        'amount'=>$amount
                    ];
                }



                $result[]=(object)[
                    'merchantId'=>$mm->id,
                    'key'=>$key,
                    'name'=>($key=='hpp')?'Harga Pokok Penjualan':ucwords($key),
                    'child'=>collect($child),
                    'total'=>collect($child)->sum('amount')
                ];
            }

            $profitLost[]=(object)$result;
        }


        $headers=[];
        foreach ($profitLost as $c =>$cc)
        {
            if($c==0)
            {
                foreach ($cc as $header)
                {
                    $childHeaders=[];
                    foreach ($header->child as $childHeader)
                    {
                        $childHeaders[]=$childHeader->name;
                    }
                    $headers[]=[
                        'key'=>$header->key,
                        'name'=>$header->name,
                        'child'=>$childHeaders
                    ];
                }
            }
        }
        $allData=[];

        foreach ($headers as $p =>$pp)
        {
            $parentTotals=[];
            foreach ($profitLost as $pl =>$pll)
            {
                foreach ($pll as $l)
                {
                    if($l->key==$pp['key'])
                    {
                        $parentTotals[]=(object)[
                            'total'=>$l->total,
                        ];
                    }

                }
            }


            $childs=[];
            foreach ($pp['child'] as $child){
                $value=[];
                foreach ($profitLost as $all)
                {
                    foreach ($all as $prof)
                    {
                        if($pp['key']==$prof->key){
                            foreach ($prof->child as $childData)
                            {
                                if($childData->name==$child){
                                    $value[]=(object)[
                                        'amount'=>$childData->amount
                                    ];
                                }
                            }
                        }
                    }
                }
                $childs[]=(object)[
                    'name'=>$child,
                    'value'=>$value,
                ];
            }

            $allData[]=(object)[
                'key'=>$pp['key'],
                'name'=>$pp['name'],
                'child'=>$childs,
                'totals'=>$parentTotals,
            ];

        }
        $summary=[];
        foreach ($profitLost as $all3)
        {
            $sumAllIncrement=0;
            $sumAllDecrement=0;
            foreach ($all3 as $pl3){
                if($pl3->key=='pendapatan'){

                    $sumAllIncrement+=$pl3->total;
                }else{
                    $sumAllDecrement+=$pl3->total;

                }
            }
            $summary[]=(object)[
                'sumAllIncrement'=>$sumAllIncrement,
                'sumAllDecrement'=>$sumAllDecrement
            ];
        }


        $params=[
            'title'=>"Laporan Laba Rugi",
            'sub_title'=>$titlePeriod,
            'data'=>collect($allData),
            'summary'=>$summary,
            'date'=>date('Y-m-d'),
            'time'=>$firstDate->toDateString(),
            'isMonth'=>$isMonth,
            'startDate'=>$firstDate->toDateString(),
            'endDate'=>$lastDate->toDateString(),
            'merchantId'=>$merchantIds,
            'merchantList'=>$merchant
        ];



        return view('merchant::acc.report.profit-lost.index',$params);

    }


    public function exportExcel()
    {
        try {
            $startDate = request()->start_date;
            $endDate = request()->end_date;

            $merchantIds=json_decode(request()->md_merchant_id);
            if(is_null($merchantIds))
            {
                $merchantId=[merchant_id()];
            }elseif ($merchantIds[0]=='-1')
            {
                $merchantId=[];
                foreach (get_cabang() as $key => $branch)
                {
                    $merchantId[]=$branch->id;
                }
            }else{
                $merchantId=$merchantIds;
            }
            $merchant=Merchant::whereIn('id',$merchantId)->get();

            if(is_null($startDate))
            {
                $firstDate=Carbon::now()->firstOfYear();
                $lastDate=Carbon::now()->lastOfYear();

            }else{
                $firstDate=Carbon::parse($startDate);
                $lastDate=Carbon::parse($endDate);
            }

            $profitLost=[];

            foreach ($merchant as $m => $mm)
            {
                $data=collect(DB::select("
            select
              acd.id,
              acd.code,
              acd.name,
              acd.type_coa,
              case WHEN acc.code in('4.1.00', '4.2.00') THEN 'pendapatan' ELSE 'pengeluaran' END AS profit_type,
              case WHEN acc.code = '4.1.00' THEN 'pendapatan_atas_penjualan'
              WHEN acc.code = '4.2.00' THEN 'pendapatan_lainnya'
              WHEN acc.code = '5.0.00' THEN 'harga_pokok_penjualan'
              WHEN acc.code = '6.1.00' THEN 'pengeluaran_operasional_&_usaha'
              ELSE 'pengeluaran_lainnya' END AS flag,
              CASE WHEN acc.code in ('4.1.00', '4.2.00')
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_revenue,
              CASE WHEN acc.code in('4.1.00', '4.2.00')
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_revenue_min,
              CASE WHEN acc.code = '5.0.00'
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_hpp,
              CASE WHEN acc.code = '5.0.00'
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_hpp_min,
              CASE WHEN acc.code in ('6.1.00', '6.2.00')
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_cost,
              CASE WHEN acc.code in('6.1.00', '6.2.00')
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_cost_min
            from
              acc_coa_details acd
              join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
              left join (
                select
                  ajd.acc_coa_detail_id,
                  ajd.amount,
                  ajd.coa_type
                from
                  acc_jurnal_details ajd
                  join acc_jurnals aj on ajd.acc_jurnal_id = aj.id
                WHERE
                  aj.is_deleted in (0, 2)
                  and aj.trans_time :: date  between '".$firstDate->toDateString()."' and '".$lastDate->toDateString()."' and aj.md_merchant_id=".$merchantId[$m]."
              ) j on j.acc_coa_detail_id = acd.id
            where
              acc.md_merchant_id = ".$mm->id."
              and acc.code in (
                '4.1.00', '4.2.00', '5.0.00',
                '6.1.00', '6.2.00'
              )
              and acd.is_deleted != 1
              and acd.code not in ('4.2.00.03')
            group by
              acd.id,
              acc.code,
              j.coa_type
            order by
              acc.code asc
            "));

                $result=[];

                foreach ($data->groupBy('profit_type') as $key =>$item)
                {
                    $child=[];

                    foreach ($item->groupBy('flag') as $i =>$j)
                    {
                        if($i=='harga_pokok_penjualan')
                        {
                            $amount=$j->sum('amount_hpp')-$j->sum('amount_hpp_min');
                        }elseif ($i=='pendapatan_atas_penjualan' || $i=='pendapatan_lainnya')
                        {
                            $amount=$j->sum('amount_revenue')-$j->sum('amount_revenue_min');
                        }else{
                            $amount=$j->sum('amount_cost')-$j->sum('amount_cost_min');

                        }
                        $child[]=(object)[
                            'name'=>ucwords(str_replace('_',' ',$i)),
                            'amount'=>$amount
                        ];
                    }



                    $result[]=(object)[
                        'merchantId'=>$mm->id,
                        'key'=>$key,
                        'name'=>($key=='hpp')?'Harga Pokok Penjualan':ucwords($key),
                        'child'=>collect($child),
                        'total'=>collect($child)->sum('amount')
                    ];
                }

                $profitLost[]=(object)[
                    'merchant_id'=>$mm->id,
                    'merchant_name'=>$mm->name,
                    'data'=>$result
                ];
            }


            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

            foreach ($profitLost as $k =>$i) {
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($k);
                $objPHPExcel->getActiveSheet()->setTitle($i->merchant_name);
                $objPHPExcel->getActiveSheet()->setCellValue('H2','LAPORAN LABA RUGI '.strtoupper($i->merchant_name).'');
                $objPHPExcel->getActiveSheet()->setCellValue('H3','Periode '.$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y').'');
                $objPHPExcel->getActiveSheet()->mergeCells('H2:J2');
                $objPHPExcel->getActiveSheet()->mergeCells('H3:J3');
                $objPHPExcel->getActiveSheet()->getStyle('H2:J2')->getFont()->setBold( true );
                $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );
                $objPHPExcel->getActiveSheet()->getStyle("H2:J2")->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getStyle("H3:J3")->applyFromArray($style);
                $col = 'H';

                $sumAllIncrement=0;
                $sumAllDecrement=0;
                $num = 4;
                foreach ($i->data as $key =>$item) {
                    $total=0;
                    $dataShow1 = [
                        strtoupper($item->name),
                        " ",
                        " "
                    ];

                    $col = 'H';
                    $row = $num + 1;
                    foreach ($dataShow1 as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':J'.$row);
                        $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                    foreach($item->child as $c =>$child){
                        if($child->amount<0){
                            $sumT='('.rupiah(abs($child->amount)).')';
                        }else{
                            $sumT=rupiah($child->amount);
                        }

                        $dataShow2 = [
                            " ",
                            $child->name,
                            $sumT
                        ];

                        $col = 'H';
                        $row = $num + 1;
                        foreach ($dataShow2 as $ds2) {
                            $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                            $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                            $col++;
                        }
                        $num++;
                    }

                    $row++;
                    if($item->total<0){
                        $sumT='('.rupiah(abs($item->total)).')';
                    }else{
                        $sumT=rupiah($item->total);
                    }

                    $dataShow3 = [
                        "TOTAL ".strtoupper($item->name),
                        " ",
                        $sumT
                    ];

                    $col = 'H';
                    $row = $num + 1;
                    foreach ($dataShow3 as $ds3) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds3);
                        $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':I'.$row);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                        $col++;
                    }
                    $row++;
                    $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':J'.$row);
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->applyFromArray($styleArray);
                    $num+=2;
                    $total+=$item->total;
                    if($item->key=='pendapatan'){
                        $sumAllIncrement+=$total;
                    }else{
                        $sumAllDecrement+=$total;

                    }
                }
                $row++;
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,"TOTAL LABA/RUGI PERUSAHAAN");
                $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':I'.$row);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                if($sumAllIncrement-$sumAllDecrement<0){
                    $sumT='('.rupiah(abs($sumAllIncrement-$sumAllDecrement)).')';
                }else{
                    $sumT=rupiah($sumAllIncrement-$sumAllDecrement);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$row,$sumT);
            }

            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.uniqid(date('YmdHis').merchant_id()).'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('Laporan-Laba-rugi-Periode-' .$firstDate->isoFormat('D-MMMM-Y').'-sampai-'.$lastDate->isoFormat('D-MMMM-Y').''. '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
        <div id='successExport' class='text-center'>
            <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
        </div>
        <script>
          if(document.querySelector('#successExport')){
            toastForSaveData('Data berhasil diexport!','success',true,'',true);
          }
        </script>";

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }

    }

}

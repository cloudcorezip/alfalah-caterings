<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc\Report;


use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\MasterData\Merchant;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
class TrialBalanceController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('acc/report/trial-balance')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\Report\TrialBalanceController@index')
                    ->name('merchant.toko.acc.report.trial-balance.index');
                Route::post('/exportExcel', 'Acc\Report\TrialBalanceController@exportExcel')
                    ->name('merchant.toko.acc.report.trial-balance.exportExcel');
            });
    }


    public function index()
    {

      $startDate = request()->start_date;
      $endDate = request()->end_date;
      $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

      $periodName = (is_null(request()->period_name))?date('Y'):request()->period_name;
      if(is_null($startDate))
      {
          $firstDate=Carbon::now()->firstOfYear();
          $lastDate=Carbon::now()->lastOfYear();

      }else{
          $firstDate=Carbon::parse($startDate);
          $lastDate=Carbon::parse($endDate);

      }
      $titlePeriod=$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y');


        $data2=DB::select("WITH RECURSIVE trial_balance AS (
                  SELECT
                    c.*,
                    json '[]',
                    (
                      SELECT
                        json_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          select
                            acd.id,
                            acd.code,
                            acd.name,
                            acd.type_coa,
                            coalesce(sum(s.sum_of_before_debit),0) sum_of_before_debit,
                            coalesce(sum(s.sum_of_before_kredit),0) sum_of_before_kredit,
                            coalesce(sum(s.sum_of_debit),0) sum_of_debit,
                            coalesce(sum(s.sum_of_kredit),0) sum_of_kredit
                          from
                            acc_coa_details acd
                            left join (select ajd.acc_coa_detail_id,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_before_debit,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_before_kredit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_debit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_kredit
                            from acc_jurnal_details ajd
                            join acc_jurnals j
                            on j.id=ajd.acc_jurnal_id
                            where j.md_merchant_id = ".$merchantId."
                            and j.is_deleted in(0, 2)
                            ) s
                            on s.acc_coa_detail_id=acd.id
                          where
                            acd.acc_coa_category_id = c.id
                            and acd.is_deleted in (0,2)
                            group by acd.id
                        ) jd
                    ) as detail
                  FROM
                    acc_coa_categories c
                  WHERE
                    NOT EXISTS(
                      SELECT
                        c.id
                      FROM
                        acc_coa_categories AS hypothetic_child
                      WHERE
                        hypothetic_child.parent_id = c.id
                    )
                    and c.md_merchant_id = ".$merchantId."
                    and c.is_deleted=0
                  UNION ALL
                  SELECT
                    (parent).*,
                    json_agg(child) AS children,
                    (case when (parent).code='3.0.00' then
                    (
                      SELECT
                        json_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          select
                            acd.id,
                            acd.code,
                            acd.name,
                            acd.type_coa,
                            coalesce(sum(s.sum_of_before_debit),0) sum_of_before_debit,
                            coalesce(sum(s.sum_of_before_kredit),0) sum_of_before_kredit,
                            coalesce(sum(s.sum_of_debit),0) sum_of_debit,
                            coalesce(sum(s.sum_of_kredit),0) sum_of_kredit
                          from
                            acc_coa_details acd
                            left join (select ajd.acc_coa_detail_id,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_before_debit,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_before_kredit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_debit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_kredit
                            from acc_jurnal_details ajd
                            join acc_jurnals j
                            on j.id=ajd.acc_jurnal_id
                            where j.md_merchant_id = ".$merchantId."
                            and j.is_deleted in(0, 2)
                            ) s
                            on s.acc_coa_detail_id=acd.id
                          where
                            acd.acc_coa_category_id = (parent).id
                            and acd.is_deleted in (0,2)
                            group by acd.id
                        ) jd
                    ) else
                    json '[]'
                    end)
                  FROM
                    (
                      SELECT
                        parent,
                        child
                      FROM
                        trial_balance AS child
                        JOIN acc_coa_categories parent ON parent.id = child.parent_id
                        and parent.md_merchant_id = ".$merchantId."
                    ) branch
                  GROUP BY
                    branch.parent
                )
                SELECT
                  json_agg(t) as data
                FROM
                  trial_balance t
                  LEFT JOIN acc_coa_categories AS hypothetic_parent ON(
                    hypothetic_parent.id = t.parent_id and hypothetic_parent.md_merchant_id=".$merchantId."
                  )
                WHERE
                  hypothetic_parent.id IS null;");

        $result=collect(json_decode($data2[0]->data));
        $merchant=Merchant::find($merchantId);

        $params=[
            'title'=>'Laporan Neraca Saldo '.$merchant->name,
            'sub_title'=>$titlePeriod,
            'data'=>$result->sortBy('id'),
            'period'=>$periodName,
            'startDate'=>$firstDate,
            'endDate'=>$lastDate,
            'merchantId'=>$merchantId,
            'merchant'=>$merchant
        ];

        return view('merchant::acc.report.trial-balance.index',$params);
    }

    public function exportExcel(){
        try{

          $startDate = request()->start_date;
          $endDate = request()->end_date;
          $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

          if(is_null($startDate))
          {
              $firstDate=Carbon::now()->firstOfYear();
              $lastDate=Carbon::now()->lastOfYear();

          }else{
              $firstDate=Carbon::parse($startDate);
              $lastDate=Carbon::parse($endDate);

          }
            $merchant=Merchant::find($merchantId);

            $titlePeriod='Laporan Neraca Saldo '.$merchant->name.' Periode '.$firstDate->isoFormat('D MMMM Y').'-'.$lastDate->isoFormat('D MMMM Y');



            $data2=DB::select("WITH RECURSIVE trial_balance AS (
                  SELECT
                    c.*,
                    json '[]',
                    (
                      SELECT
                        json_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          select
                            acd.id,
                            acd.code,
                            acd.name,
                            acd.type_coa,
                            coalesce(sum(s.sum_of_before_debit),0) sum_of_before_debit,
                            coalesce(sum(s.sum_of_before_kredit),0) sum_of_before_kredit,
                            coalesce(sum(s.sum_of_debit),0) sum_of_debit,
                            coalesce(sum(s.sum_of_kredit),0) sum_of_kredit
                          from
                            acc_coa_details acd
                            left join (select ajd.acc_coa_detail_id,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_before_debit,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_before_kredit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_debit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_kredit
                            from acc_jurnal_details ajd
                            join acc_jurnals j
                            on j.id=ajd.acc_jurnal_id
                            where j.md_merchant_id = ".$merchantId."
                            and j.is_deleted in(0, 2)
                            ) s
                            on s.acc_coa_detail_id=acd.id
                          where
                            acd.acc_coa_category_id = c.id
                            and acd.is_deleted in (0,2)
                            group by acd.id
                        ) jd
                    ) as detail
                  FROM
                    acc_coa_categories c
                  WHERE
                    NOT EXISTS(
                      SELECT
                        c.id
                      FROM
                        acc_coa_categories AS hypothetic_child
                      WHERE
                        hypothetic_child.parent_id = c.id
                    )
                    and c.md_merchant_id = ".$merchantId."
                    and c.is_deleted=0
                  UNION ALL
                  SELECT
                    (parent).*,
                    json_agg(child) AS children,
                    (case when (parent).code='3.0.00' then
                    (
                      SELECT
                        json_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          select
                            acd.id,
                            acd.code,
                            acd.name,
                            acd.type_coa,
                            coalesce(sum(s.sum_of_before_debit),0) sum_of_before_debit,
                            coalesce(sum(s.sum_of_before_kredit),0) sum_of_before_kredit,
                            coalesce(sum(s.sum_of_debit),0) sum_of_debit,
                            coalesce(sum(s.sum_of_kredit),0) sum_of_kredit
                          from
                            acc_coa_details acd
                            left join (select ajd.acc_coa_detail_id,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_before_debit,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_before_kredit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_debit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_kredit
                            from acc_jurnal_details ajd
                            join acc_jurnals j
                            on j.id=ajd.acc_jurnal_id
                            where j.md_merchant_id = ".$merchantId."
                            and j.is_deleted in(0, 2)
                            ) s
                            on s.acc_coa_detail_id=acd.id
                          where
                            acd.acc_coa_category_id = (parent).id
                            and acd.is_deleted in (0,2)
                            group by acd.id
                        ) jd
                    ) else
                    json '[]'
                    end)
                  FROM
                    (
                      SELECT
                        parent,
                        child
                      FROM
                        trial_balance AS child
                        JOIN acc_coa_categories parent ON parent.id = child.parent_id
                        and parent.md_merchant_id = ".$merchantId."
                    ) branch
                  GROUP BY
                    branch.parent
                )
                SELECT
                  json_agg(t) as data
                FROM
                  trial_balance t
                  LEFT JOIN acc_coa_categories AS hypothetic_parent ON(
                    hypothetic_parent.id = t.parent_id and hypothetic_parent.md_merchant_id=".$merchantId."
                  )
                WHERE
                  hypothetic_parent.id IS null;");

            $newResult=collect(json_decode($data2[0]->data))->sortBy('id');



            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',$titlePeriod);

            $header = [
                'Nama Akun',
                'Saldo Awal',
                'Mutasi Debit',
                'Mutasi Kredit',
                'Saldo Akhir'
            ];
            $col = 'A';
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '3', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->getFont()->setBold( true );
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($styleArray);
                $col++;
            }

            $num = 3;
            $totalDebit=0;
            $totalKredit=0;
            $endOfBalance=0;
            foreach ($newResult as $key => $item) {
                $dataShow1 = [
                    $item->parent_code.' - '.str_replace('.','',$item->code)." ".$item->name,
                    " ",
                    " ",
                    " ",
                    " "
                ];
                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow1 as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold( true );
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;

                $det1=collect($item->detail)->sortBy('id');
                if($det1->count()>0){
                    $subOfDebit=0;
                    $subOfKredit=0;
                    $subOfEndBalance=0;
                    foreach($det1 as $d =>$dd2){
                        if($dd2->type_coa=='Debit'){
                            $firstBalance=$dd2->sum_of_before_debit-$dd2->sum_of_before_kredit;
                        }else{
                            $firstBalance=$dd2->sum_of_before_kredit-$dd2->sum_of_before_debit;
                        }
                        $result=($firstBalance<0)?'('.rupiah(abs($firstBalance)).')':rupiah($firstBalance);
                        $sumDebit=($dd2->sum_of_debit<0)?'('.rupiah(abs($dd2->sum_of_debit)).')':rupiah($dd2->sum_of_debit);
                        $sumKredit=($dd2->sum_of_kredit<0)?'('.rupiah(abs($dd2->sum_of_kredit)).')':rupiah($dd2->sum_of_kredit);
                        if($dd2->type_coa=='Debit')
                        {
                            $endBalance=$firstBalance+$dd2->sum_of_debit-$dd2->sum_of_kredit;

                        }else{
                            $endBalance=$firstBalance+$dd2->sum_of_kredit-$dd2->sum_of_debit;

                        }
                        $result2=($endBalance<0)?'('.rupiah(abs($endBalance)).')':rupiah($endBalance);
                        $subOfDebit+=$dd2->sum_of_debit;
                        $subOfKredit+=$dd2->sum_of_kredit;
                        if(strpos($dd2->name,'Akumulasi')!==false)
                        {
                            $subOfEndBalance-=$endBalance;

                        }else{
                            $subOfEndBalance+=$endBalance;

                        }
                        $endOfBalance+=$subOfEndBalance;
                        $dataShow2 = [
                            $item->parent_code.' - '.str_replace('.','',$dd2->code)." ".$dd2->name,
                            $result,
                            $sumDebit,
                            $sumKredit,
                            $result2
                        ];
                        $col = 'A';
                        $row = $num + 1;
                        foreach ($dataShow2 as $ds2) {
                            $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                            $col++;
                        }
                        $num++;
                    }
                    $sb=($subOfDebit<0)?'('.rupiah(abs($subOfDebit)).')':rupiah($subOfDebit);
                    $sk=($subOfKredit<0)?'('.rupiah(abs($subOfKredit)).')':rupiah($subOfKredit);
                    $sEnd=($subOfEndBalance<0)?'('.rupiah(abs($subOfEndBalance)).')':rupiah($subOfEndBalance);


                    $dataShow3 = [
                        " ",
                        "Sub Total",
                        $sb,
                        $sk,
                        $sEnd
                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow3 as $ds3) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds3);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold( true );
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $totalDebit+=$subOfDebit;
                    $totalKredit+=$subOfKredit;
                    $num++;
                }
                $json1=collect($item->json)->sortBy('id');
                foreach($json1 as $n =>$c){
                    $dataShow4 = [
                        $item->parent_code.' - '.str_replace('.','',$c->code)." ".$c->name,
                        " ",
                        " ",
                        " ",
                        " "
                    ];
                    $col = 'A';
                    $row = $num + 1;
                    foreach ($dataShow4 as $ds4) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds4);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold( true );
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $col++;
                    }
                    $num++;
                    $json2=collect($c->json)->sortBy('id');
                    if($json2->count()>0){
                        foreach($json2 as $c2 =>$cc){
                            $subOfDebit=0;
                            $subOfKredit=0;
                            $subOfEndBalance=0;

                            $dataShow9 = [
                                $item->parent_code.' - '.str_replace('.','',$cc->code)." ".$cc->name,
                                " ",
                                " ",
                                " ",
                                " "
                            ];

                            $col = 'A';
                            $row = $num + 1;
                            foreach ($dataShow9 as $ds9) {
                                $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds9);
                                $objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold( true );
                                $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                                $col++;
                            }
                            $num++;
                            $det2=collect($cc->detail)->sortBy('id');

                            foreach($det2 as $d =>$dd){
                                if($dd->type_coa=='Debit'){
                                    $firstBalance=$dd->sum_of_before_debit-$dd->sum_of_before_kredit;
                                }else{
                                    $firstBalance=$dd->sum_of_before_kredit-$dd->sum_of_before_debit;
                                }
                                $fb=($firstBalance<0)?'('.rupiah(abs($firstBalance)).')':rupiah($firstBalance);

                                $sb=($dd->sum_of_debit<0)?'('.rupiah(abs($dd->sum_of_debit)).')':rupiah($dd->sum_of_debit);
                                $sk=($dd->sum_of_kredit<0)?'('.rupiah(abs($dd->sum_of_kredit)).')':rupiah($dd->sum_of_kredit);

                                if($dd->type_coa=='Debit')
                                {
                                    $endBalance=$firstBalance+$dd->sum_of_debit-$dd->sum_of_kredit;
                                }else{
                                    $endBalance=$firstBalance+$dd->sum_of_kredit-$dd->sum_of_debit;
                                }
                                $eb=($endBalance<0)?'('.rupiah(abs($endBalance)).')':rupiah($endBalance);
                                $subOfDebit+=$dd->sum_of_debit;
                                $subOfKredit+=$dd->sum_of_kredit;
                                if(strpos($dd->name,'Akumulasi')!==false)
                                {
                                    $subOfEndBalance-=$endBalance;

                                }else{
                                    $subOfEndBalance+=$endBalance;

                                }
                                $dataShow5 = [
                                    $item->parent_code.' - '.str_replace('.','',$dd->code)." ".$dd->name,
                                    $fb,
                                    $sb,
                                    $sk,
                                    $eb
                                ];

                                $col = 'A';
                                $row = $num + 1;
                                foreach ($dataShow5 as $ds5) {
                                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds5);
                                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                                    $col++;
                                }
                                $num++;
                            }
                            if($det2->count()>0){
                                $sb=($subOfDebit<0)?'('.rupiah(abs($subOfDebit)).')':rupiah($subOfDebit);
                                $sk=($subOfKredit<0)?'('.rupiah(abs($subOfKredit)).')':rupiah($subOfKredit);
                                $sEnd=($subOfEndBalance<0)?'('.rupiah(abs($subOfEndBalance)).')':rupiah($subOfEndBalance);
                                $dataShow6 = [
                                    " ",
                                    "Sub Total",
                                    $sb,
                                    $sk,
                                    $sEnd
                                ];
                                $col = 'A';
                                $row = $num + 1;
                                foreach ($dataShow6 as $ds6) {
                                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds6);
                                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold( true );
                                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                                    $col++;
                                }
                                $num++;
                                $totalDebit+=$subOfDebit;
                                $totalKredit+=$subOfKredit;
                                $endOfBalance+=$subOfEndBalance;
                            }
                        }
                    }else{
                        $det3=collect($c->detail)->sortBy('id');

                        if($det3->count()>0){
                            $subOfDebit=0;
                            $subOfKredit=0;
                            $subOfEndBalance=0;
                            foreach($det3 as $d =>$dd1){
                                if($dd1->type_coa=='Debit'){
                                    $firstBalance=$dd1->sum_of_before_debit-$dd1->sum_of_before_kredit;
                                }else{
                                    $firstBalance=$dd1->sum_of_before_kredit-$dd1->sum_of_before_debit;
                                }
                                $fb=($firstBalance<0)?'('.rupiah(abs($firstBalance)).')':rupiah($firstBalance);

                                $sd=($dd1->sum_of_debit<0)?'('.rupiah(abs($dd1->sum_of_debit)).')':rupiah($dd1->sum_of_debit);
                                $sk=($dd1->sum_of_kredit<0)?'('.rupiah(abs($dd1->sum_of_kredit)).')':rupiah($dd1->sum_of_kredit);

                                if($dd1->type_coa=='Debit')
                                {
                                    $endBalance=$firstBalance+$dd1->sum_of_debit-$dd1->sum_of_kredit;

                                }else{
                                    $endBalance=$firstBalance+$dd1->sum_of_kredit-$dd1->sum_of_debit;

                                }
                                $eb=($endBalance<0)?'('.rupiah(abs($endBalance)).')':rupiah($endBalance);
                                $subOfDebit+=$dd1->sum_of_debit;
                                $subOfKredit+=$dd1->sum_of_kredit;
                                if(strpos($dd1->name,'Akumulasi')!==false)
                                {
                                    $subOfEndBalance-=$endBalance;

                                }else{
                                    $subOfEndBalance+=$endBalance;

                                }
                                $dataShow7 = [
                                    $item->parent_code.' - '.str_replace('.','',$dd1->code)." ".$dd1->name,
                                    $fb,
                                    $sd,
                                    $sk,
                                    $eb
                                ];
                                $col = 'A';
                                $row = $num + 1;
                                foreach ($dataShow7 as $ds7) {
                                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds7);
                                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                                    $col++;
                                }
                                $num++;
                            }

                            $sd=($subOfDebit<0)?'('.rupiah(abs($subOfDebit)).')':rupiah($subOfDebit);
                            $sk=($subOfKredit<0)?'('.rupiah(abs($subOfKredit)).')':rupiah($subOfKredit);
                            $se=($subOfEndBalance<0)?'('.rupiah(abs($subOfEndBalance)).')':rupiah($subOfEndBalance);

                            $dataShow8 = [
                                " ",
                                "Sub Total",
                                $sd,
                                $sk,
                                $se
                            ];
                            $col = 'A';
                            $row = $num + 1;
                            foreach ($dataShow8 as $ds8) {
                                $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds8);
                                $objPHPExcel->getActiveSheet()->getStyle($col . $row)->getFont()->setBold( true );
                                $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                                $col++;
                            }
                            $num++;
                            $totalDebit+=$subOfDebit;
                            $totalKredit+=$subOfKredit;
                            $endOfBalance+=$subOfEndBalance;
                        }
                    }
                }
            }

            $row++;
            $totalDeb=($totalDebit<0)?'('.rupiah(abs($totalDebit)).')':rupiah($totalDebit);
            $totalKred=($totalKredit<0)?'('.rupiah(abs($totalKredit)).')':rupiah($totalKredit);
            $end=($endOfBalance<0)?'('.rupiah(abs($endOfBalance)).')':rupiah($endOfBalance);


            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,"Total");
            $objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':B'.$row);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':B'.$row)->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $totalDeb);
            $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row,$totalKred);
            $objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row,$end);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':E'.$row)->getFont()->setBold( true );

            //dd($data);
            foreach(range('A','E') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.$merchantId.'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('Laporan-Neraca-Saldo-'.$firstDate->isoFormat('Y').'.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";
        }catch (\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,cobalah beberapa saat lagi!</div>";
        }

    }

}

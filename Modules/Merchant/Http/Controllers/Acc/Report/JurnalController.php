<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc\Report;


use App\Classes\CollectionPagination;
use App\Http\Controllers\Controller;
use App\Models\Accounting\Jurnal;
use App\Models\MasterData\Merchant;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Http\Controllers\Acc\PeriodController;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class JurnalController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('acc/report/jurnal')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\Report\JurnalController@index')
                    ->name('merchant.toko.acc.report.jurnal.index');
                Route::post('/exportExcel', 'Acc\Report\JurnalController@exportExcel')
                    ->name('merchant.toko.acc.report.jurnal.exportExcel');
            });
    }


    public function index()
    {
        $startDate = request()->start_date;
        $endDate = request()->end_date;
        $transCode = request()->trans_code;
        $refCode = request()->ref_code;
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;

        if(is_null($startDate))
        {
            $firstDate=Carbon::now()->firstOfMonth();
            $lastDate=Carbon::now()->lastOfMonth();
        }else{
            $firstDate=Carbon::parse($startDate);
            $lastDate=Carbon::parse($endDate);
        }
        $query="";
        if ($firstDate->toDateString() != '') {
            $query="and aj.trans_time::date between '".$firstDate->toDateString()."' and '".$lastDate->toDateString()."'";
        }

        if ($transCode != '' || !is_null($transCode)) {
            $query="and trans_code='".str_replace(" ", "", $transCode)."'";
        }

        if ($refCode !='' || !is_null($refCode)) {
            $query="and aj.ref_code='".str_replace(" ", "", $refCode)."'";

        }
        $data=collect(DB::select("
        SELECT aj.id AS jurnal_id,
            aj.trans_name,
            aj.trans_code,
            aj.ref_code,
            aj.external_ref_id,
            aj.trans_time,
            d.code,
            d.name,
            ajd.coa_type,
            ajd.id as ajd_id,
            ajd.amount,
            aj.md_merchant_id,
            ajd.id AS jurnal_detail_id,
            acc.parent_code,
            to_char(aj.trans_time, 'YYYY'::text) AS year,
            to_char(aj.trans_time, 'YYYY-MM'::text) AS month_year,
            to_char(aj.trans_time, 'MM'::text) AS month,
            to_char(aj.trans_time, 'YYYY-MM-DD'::text) AS original
        FROM acc_jurnals aj
            JOIN acc_jurnal_details ajd ON aj.id = ajd.acc_jurnal_id
            JOIN acc_coa_details d ON ajd.acc_coa_detail_id = d.id
            JOIN acc_coa_categories acc on acc.id=d.acc_coa_category_id
        WHERE aj.is_deleted = ANY (ARRAY[0, 2]) and aj.md_merchant_id=$merchantId $query
        ORDER BY ajd.id,aj.trans_time asc
        "));


        if ($data->count() > 0) {
            $ceil = ceil($data->count() / 10);
            $tempOfData = (request()->page != $ceil) ? $data->groupBy('jurnal_id')->slice(0,10 * (is_null(request()->page) ? 1 : request()->page)) : $data->groupBy('jurnal_id');
            $sumOfAmount = collect([
                (object)[
                    'sum_of_debit' => $tempOfData->reduce(function ($i, $k) {
                        $debit=0;
                        foreach ($k as $j)
                        {
                            ($j->coa_type=='Debit')?$debit+=$j->amount:0;
                        }
                        return $i + $debit;
                    }, 0),
                    'sum_of_kredit' => $tempOfData->reduce(function ($i, $k) {
                        $kredit=0;
                        foreach ($k as $j)
                        {
                            ($j->coa_type=='Kredit')?$kredit+=$j->amount:0;
                        }
                        return $i + $kredit;
                    }, 0),
                ]
            ]);
        }else{
            $sumOfAmount = collect([
                (object)[
                    'sum_of_debit' => 0,
                    'sum_of_kredit' => 0
                ]
            ]);
        }

        $merchant=Merchant::find($merchantId);

        $params=[
            'title'=>'Laporan Jurnal Transaksi',
            'sub_title'=>$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y').'',
            'data'=>CollectionPagination::paginate($data->groupBy('jurnal_id')),
            'sumOfAmount'=>$sumOfAmount,
            'startDate'=>$firstDate->toDateString(),
            'endDate'=>$lastDate->toDateString(),
            'date'=>date('Y-m-d'),
            'merchantId'=>$merchantId,
            'merchant'=>$merchant
        ];

        return view('merchant::acc.report.jurnal.index',$params);
    }

    public function exportExcel(Request $request){
        try{

            $startDate = request()->start_date;
            $endDate = request()->end_date;
            $transCode = request()->trans_code;
            $refCode = request()->ref_code;
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            if(is_null($startDate))
            {
                $firstDate=Carbon::now()->firstOfMonth();
                $lastDate=Carbon::now()->lastOfMonth();
            }else{
                $firstDate=Carbon::parse($startDate);
                $lastDate=Carbon::parse($endDate);
            }
            $query="";
            if ($firstDate->toDateString() != '') {
                $query="and aj.trans_time::date between '".$firstDate->toDateString()."' and '".$lastDate->toDateString()."'";
            }

            if ($transCode != '' || !is_null($transCode)) {
                $query="and trans_code='".str_replace(" ", "", $transCode)."'";
            }

            if ($refCode !='' || !is_null($refCode)) {
                $query="and ref_code='".str_replace(" ", "", $refCode)."'";

            }
            $collect=collect(DB::select("
        SELECT aj.id AS jurnal_id,
            aj.trans_name,
            aj.trans_code,
            aj.ref_code,
            aj.external_ref_id,
            aj.trans_time,
            d.code,
            d.name,
            ajd.coa_type,
            ajd.id as ajd_id,
            ajd.amount,
            aj.md_merchant_id,
            ajd.id AS jurnal_detail_id,
            acc.parent_code,
            to_char(aj.trans_time, 'YYYY'::text) AS year,
            to_char(aj.trans_time, 'YYYY-MM'::text) AS month_year,
            to_char(aj.trans_time, 'MM'::text) AS month,
            to_char(aj.trans_time, 'YYYY-MM-DD'::text) AS original
        FROM acc_jurnals aj
            JOIN acc_jurnal_details ajd ON aj.id = ajd.acc_jurnal_id
            JOIN acc_coa_details d ON ajd.acc_coa_detail_id = d.id
            JOIN acc_coa_categories acc on acc.id=d.acc_coa_category_id
        WHERE aj.is_deleted = ANY (ARRAY[0, 2]) and aj.md_merchant_id=$merchantId $query
        ORDER BY aj.trans_time asc
        "));



            if ($collect->count() > 0) {
                $ceil = ceil($collect->count() / 10);
                $tempOfData = (request()->page != $ceil) ? $collect->groupBy('jurnal_id')->slice(0,10 * (is_null(request()->page) ? 1 : request()->page)) : $collect->groupBy('jurnal_id');
                $sumOfAmount = collect([
                    (object)[
                        'sum_of_debit' => $tempOfData->reduce(function ($i, $k) {
                            $debit=0;
                            foreach ($k as $j)
                            {
                                ($j->coa_type=='Debit')?$debit+=$j->amount:0;
                            }
                            return $i + $debit;
                        }, 0),
                        'sum_of_kredit' => $tempOfData->reduce(function ($i, $k) {
                            $kredit=0;
                            foreach ($k as $j)
                            {
                                ($j->coa_type=='Kredit')?$kredit+=$j->amount:0;
                            }
                            return $i + $kredit;
                        }, 0),
                    ]
                ]);
            }else{
                $sumOfAmount = collect([
                    (object)[
                        'sum_of_debit' => 0,
                        'sum_of_kredit' => 0
                    ]
                ]);
            }
            $data=CollectionPagination::paginate($collect->groupBy('jurnal_id'));
            if(count($data)<1){

                return "<div class='alert alert-warning'>Data kosong tidak dapat dieksport!</div>";
            }
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $merchant=Merchant::find($merchantId);
            $period=strtoupper(convertMonthToId((int) date('m')).' '.date('Y').'');
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"LAPORAN JURNAL TRANSAKSI ".strtoupper($merchant->name)." PERIODE $period");

            $header = [
                'Akun',
                'Debit',
                'Kredit',
            ];
            $col = 'A';
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            foreach ($header as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '3', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->getFont()->setBold( true );
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($styleArray);
                $col++;
            }

            $num = 3;
            $row1=null;
            $row=0;
            foreach ($data as $n => $item) {
                $first=$item->where('coa_type','Debit')
                    ->where('name','!=','Administrasi Bank')
                    ->sortBy('ajd_id')
                    ->first();
                $dataShow = [
                    \Carbon\Carbon::parse($first->trans_time)->isoFormat('D MMMM Y').' | '.$first->trans_name,
                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':C'.$row.'', $ds)->applyFromArray($styleArray)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':C'.$row.'');
                    $col++;
                }
                $num++;


                $dataShow2 = [
                    $first->parent_code.' - '.str_replace('.','',$first->code).' '.$first->name,
                    rupiah($first->amount),
                    ''

                ];

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow2 as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row, $ds)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;


                foreach($item->where('jurnal_detail_id','!=',$first->jurnal_detail_id)->sortBy('coa_type') as $d){
                    $dataShow3 = [
                        '  '.$d->parent_code.' - '.str_replace('.','',$d->code).' '.$d->name,
                        '',
                        rupiah($d->amount),
                    ];

                    $col = 'A';
                    $row = $num+ 1;

                    foreach ($dataShow3 as $ds2) {

                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row, $ds2)->applyFromArray($styleArray);
                        $col++;

                    }
                    $num++;
                }

            }

            $row++;
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,"TOTAL");
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,rupiah($sumOfAmount[0]->sum_of_debit));
            $objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row,rupiah($sumOfAmount[0]->sum_of_kredit));
            $objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($styleArray);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$row.':C'.$row)->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold( true );

            foreach(range('A','G') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.$transCode.'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('export-jurnal_' . $transCode . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);
            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',true,'',true);
              }
            </script>";
        }catch (\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
            //return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";
        }
    }


}

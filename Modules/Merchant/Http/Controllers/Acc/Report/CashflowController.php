<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc\Report;


use App\Classes\CollectionPagination;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCategory;
use App\Models\MasterData\Merchant;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PDF;

class CashflowController
{

    public function routeWeb()
    {
        Route::prefix('acc/report/cashflow')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\Report\CashflowController@index')
                    ->name('merchant.toko.acc.report.cashflow.index');
                Route::get('/exportPDF', 'Acc\Report\CashflowController@exportPDF')
                    ->name('merchant.toko.acc.report.cashflow.exportPDF');
                Route::post('/exportExcel', 'Acc\Report\CashflowController@exportExcel')
                    ->name('merchant.toko.acc.report.cashflow.exportExcel');
            });
    }

    public function index()
    {
        $startDate = request()->start_date;
        $endDate = request()->end_date;
        $merchantIds=request()->md_merchant_id;
        if(is_null($merchantIds))
        {
            $merchantId=[merchant_id()];
            $merchantIds=[merchant_id()];


        }elseif ($merchantIds[0]=='-1')
        {
            $merchantId=[];
            foreach (get_cabang() as $key => $branch)
            {

                $merchantId[]=$branch->id;
            }


        }else{
            $merchantId=$merchantIds;
        }



        if(is_null($startDate))
        {
            $firstDate=Carbon::now()->firstOfYear();
            $lastDate=Carbon::now()->lastOfYear();
            $isMonth=false;
        }else{
            $firstDate=Carbon::parse($startDate);
            $lastDate=Carbon::parse($endDate);
            $isMonth=true;
        }

        $allCashflow=[];
        $titlePeriod=$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y');

        $merchant=Merchant::whereIn('id',$merchantId)->paginate(2);

        foreach ($merchant as $m => $mm)
        {

            $data=collect(DB::select("
            with acc_cashflow_report as (select
             acd.id AS det_id,
              acd.type_coa,
              acd.name AS det_name,
              acc.md_merchant_id,
              case
              when acc.name = 'Pendapatan Usaha' then 'Pendapatan'
              when acd.name = 'Utang Saldo Deposit' then 'Pendapatan'
              when acd.name = 'Utang PPN Keluaran' then 'Pendapatan'
              when acd.name = 'Utang Pembelian Barang' then 'Beban Operasional & Usaha'
              when acc.name = 'Aktiva Tetap' then 'Pembelian/Penjualan Aset'
              when acc.name = 'Aktiva Tidak Berwujud' then 'Pembelian/Penjualan Aset'
              when acc.name = 'Investasi Jangka Pendek' then 'Investasi Lainnya'
              when acc.name = 'Investasi Jangka Panjang' then 'Investasi Lainnya'
              when acc.name = 'Piutang' then 'Pembayaran/Penerimaan Pinjaman'
              when acc.name = 'Utang' then 'Pembayaran/Penerimaan Pinjaman'
              when acc.name = 'Saldo Laba' then 'Ekuitas/Modal'
              when acc.name = 'Dividen' then 'Ekuitas/Modal'
              when acc.name = 'Modal' then 'Ekuitas/Modal'
              else acc.name end as format_name,
              COALESCE(sub.amount, 0) AS amount,
              COALESCE(sub.original, '".$firstDate->toDateString()."') AS _original,
              COALESCE(sub.coa_type, acd.type_coa) AS coa_type,
              0 as is_deleted
            from acc_coa_categories acc
            join acc_coa_details acd
            on acc.id=acd.acc_coa_category_id
            left join (
                select
                  d.acc_coa_detail_id,
                  d.amount,
                  j.trans_time,
                  to_char(j.trans_time, 'YYYY-MM-DD') AS original,
                  d.coa_type
                FROM
                  acc_jurnal_details d
                  JOIN acc_jurnals j ON j.id = d.acc_jurnal_id
                WHERE
                  j.is_deleted in(0,2)
                  and j.md_merchant_id = ".$mm->id."
              ) sub ON sub.acc_coa_detail_id = acd.id
            where
            acc.md_merchant_id=".$mm->id."
            and acc.name in (
                'Pendapatan','Pendapatan Usaha', 'Pendapatan Lainnya',
                'Beban Operasional & Usaha', 'Beban Lainnya',
                'Aktiva Tetap', 'Aktiva Tidak Berwujud',
                'Investasi Jangka Pendek', 'Investasi Jangka Panjang',
                'Utang', 'Utang Pajak', 'Piutang',
                'Modal', 'Saldo Laba','Dividen'
              )
            and acd.is_deleted in(0,2)
            and acd.name not in (
                'Diskon Pembelian Barang', 'Utang Konsinyasi','Utang PPN Keluaran'
            )
            and acd.name not like '%Akumulasi%'
            and acd.name not like '%%Utang PPh%'
            union all
            select
              d.acc_coa_detail_id as det_id,
              d.type_coa,
              d.name as det_name,
              aj.md_merchant_id,
              'Pembayaran ke Pemasok' as format_name,
              aj.trans_amount as amount,
              to_char(aj.trans_time, 'YYYY-MM-DD') AS _original,
              d.coa_type,
              aj.is_deleted
            from
              acc_jurnals aj
              join(
                select
                  ajd.acc_jurnal_id,
                  ajd.acc_coa_detail_id,
                  acd.type_coa,
                  acd.name,
                  ajd.coa_type
                from
                  acc_jurnal_details ajd
                  join acc_coa_details acd on ajd.acc_coa_detail_id = acd.id
                where
                  acd.code like '%1.1.01%'
                  or acd.code like '%1.1.02%'
              ) d on d.acc_jurnal_id = aj.id
            where
              aj.is_deleted in(0,2)
              and aj.md_merchant_id = ".$mm->id."
              and substring(aj.ref_code from 0 for 2)='P'
              or substring(aj.ref_code from 0 for 3)='RP'
              or substring(aj.ref_code from 0 for 4)='PVT'
              or substring(aj.ref_code from 0 for 4)='PPO'
              )
            select acr.*,
            case when acr.format_name='Pendapatan' then 'operasional'
            when acr.format_name='Pendapatan Lainnya' then 'operasional'
            when acr.format_name='Beban Operasional & Usaha' then 'operasional'
            when acr.format_name='Beban Lainnya' then 'operasional'
            when acr.format_name='Pembelian/Penjualan Aset' then 'investasi'
            when acr.format_name='Investasi Lainnya' then 'investasi'
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 'pendanaan'
            when acr.format_name='Ekuitas/Modal'
            then 'pendanaan'
            else 'operasional' end as format_type_name,
            case when acr.format_name='Pendapatan' then 1
            when acr.format_name='Pendapatan Lainnya' then 2
            when acr.format_name='Beban Operasional & Usaha' then 3
            when acr.format_name='Beban Lainnya' then 5
            when acr.format_name='Pembelian/Penjualan Aset' then 6
            when acr.format_name='Investasi Lainnya' then 7
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 8
            when acr.format_name='Ekuitas/Modal'
            then 9
            else 4 end as order_number,
            case when acr.format_name='Pendapatan' then 1
            when acr.format_name='Pendapatan Lainnya' then 2
            when acr.format_name='Beban Operasional & Usaha' then 3
            when acr.format_name='Beban Lainnya' then 4
            when acr.format_name='Pembelian/Penjualan Aset' then 5
            when acr.format_name='Investasi Lainnya' then 6
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 7
            when acr.format_name='Ekuitas/Modal'
            then 8
            else 9 end as format_id
            from acc_cashflow_report acr  where acr.md_merchant_id=".$mm->id."
            "));

            $result=$data->sortBy('order_number')->where('_original','<=',$lastDate->toDateString());
            $cashflow=[];
            foreach ($result->groupBy('format_type_name') as $key =>$item)
            {
                $child=[];
                $sumTotal=0;
                $sumBefore=0;
                foreach ($item->groupBy('format_id') as $i)
                {

                    $subTotal=0;
                    $before=$i->where('_original','<',$firstDate->toDateString())
                        ->reduce(function ($a,$b)use ($subTotal){
                            if($b->coa_type=='Debit' && $b->type_coa=='Debit')
                            {
                                $subTotal+=$b->amount;
                            }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Debit')
                            {
                                $subTotal-=$b->amount;

                            }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Kredit')
                            {
                                $subTotal+=$b->amount;
                            }else{
                                $subTotal-=$b->amount;
                            }
                            return $a+$subTotal;
                        },0);
                    $total=$i->whereBetween('_original',[$firstDate->toDateString(),$lastDate->toDateString()])
                        ->reduce(function ($a,$b)use ($subTotal){
                            if($b->coa_type=='Debit' && $b->type_coa=='Debit')
                            {
                                $subTotal+=$b->amount;
                            }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Debit')
                            {
                                $subTotal-=$b->amount;

                            }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Kredit')
                            {
                                $subTotal+=$b->amount;
                            }else{
                                $subTotal-=$b->amount;
                            }
                            return $a+$subTotal;
                        },0);


                    $child[]=(object)[
                        'name'=>$i->first()->format_name,
                        'amount'=>$total,
                        'order_number'=>$i->first()->order_number
                    ];


                    if($key=='operasional'){
                        if(strpos($i->first()->format_name,'Beban')!==false)
                        {
                            $sumBefore-=$before;
                            $sumTotal-=$total;
                        }else{
                            $sumBefore+=$before;
                            $sumTotal+=$total;
                        }
                    }else{
                        $sumBefore+=$before;
                        $sumTotal+=$total;
                    }
                }

                if($key=='operasional')
                {
                    if(count($child)==4)
                    {
                        array_push($child,(object)[
                            'name'=>'Pembayaran ke Pemasok',
                            'amount'=>0,
                            'order_number'=>4
                        ]);
                    }
                }
                $cashflow[]=(object)[
                    'key'=>ucwords($key),
                    'child'=>collect($child)->sortBy('order_number'),
                    'sum_all'=>$sumTotal,
                    'sum_before'=>$sumBefore
                ];
            }


            $allCashflow[]=[
                'cashflow'=>$cashflow
            ];

        }
        $headers=[];
        foreach ($allCashflow as $c =>$cc)
        {
            if($c==0)
            {
                foreach ($cc['cashflow'] as $header)
                {
                    $childHeaders=[];
                    foreach ($header->child as $childHeader)
                    {
                        $childHeaders[]=$childHeader->name;
                    }
                    $headers[]=[
                        'key'=>$header->key,
                        'child'=>$childHeaders
                    ];
                }
            }
        }

        $allData=[];

        foreach ($headers as $h)
        {
            $childs=[];
            foreach ($h['child'] as $child){

                $value=[];
                foreach ($allCashflow as $all)
                {
                    foreach ($all['cashflow'] as $cashflow)
                    {
                        if($h['key']==$cashflow->key){
                            foreach ($cashflow->child as $childData)
                            {
                                if($childData->name==$child){
                                    $value[]=(object)[
                                        'amount'=>$childData->amount
                                    ];
                                }
                            }
                        }
                    }
                }
                $childs[]=(object)[
                    'name'=>$child,
                    'value'=>$value,
                ];
            }
            $sums=[];
            foreach ($allCashflow as $all2)
            {
                foreach ($all2['cashflow'] as $cashflow2){
                    if($h['key']==$cashflow2->key){
                        $sums[]=(object)[
                            'sum_all'=>$cashflow2->sum_all,
                            'sum_before'=>$cashflow2->sum_before
                        ];
                    }
                }
            }


            $allData[]=(object)[
                'key'=>$h['key'],
                'child'=>$childs,
                'sums'=>$sums,
            ];

        }

        $summary=[];
        foreach ($allCashflow as $all3)
        {
            $previousMonth=0;
            $thisMonth=0;
            foreach ($all3['cashflow'] as $cashflow3){
                if($cashflow3->key=='Investasi'){
                    $previousMonth-=$cashflow3->sum_before;
                    $thisMonth-=$cashflow3->sum_all;

                }else{
                    $previousMonth+=$cashflow3->sum_before;
                    $thisMonth+=$cashflow3->sum_all;

                }
            }
            $summary[]=(object)[
                'previousMonth'=>$previousMonth,
                'thisMonth'=>$thisMonth
            ];
        }


        $params=[
            'title'=>'Laporan Arus Kas',
            'sub_title'=>$titlePeriod,
            'data'=>$allData,
            'summary'=>$summary,
            'date'=>date('Y-m-d'),
            'isMonth'=>$isMonth,
            'time'=>$firstDate->toDateString(),
            'startDate'=>$firstDate->toDateString(),
            'endDate'=>$lastDate->toDateString(),
            'merchantId'=>$merchantIds,
            'merchantList'=>$merchant
        ];

        return view('merchant::acc.report.cashflow.index',$params);

    }

    public function exportExcel()
    {
        try {
            $startDate = request()->start_date;
            $endDate = request()->end_date;
            $merchantIds=json_decode(request()->md_merchant_id);
            if(is_null($merchantIds))
            {
                $merchantId=[merchant_id()];
            }elseif ($merchantIds[0]=='-1')
            {
                $merchantId=[];
                foreach (get_cabang() as $key => $branch)
                {
                    $merchantId[]=$branch->id;
                }
            }else{
                $merchantId=$merchantIds;
            }

            $merchant=Merchant::whereIn('id',$merchantId)->get();


            if(is_null($startDate))
            {
                $firstDate=Carbon::now()->firstOfYear();
                $lastDate=Carbon::now()->lastOfYear();

            }else{
                $firstDate=Carbon::parse($startDate);
                $lastDate=Carbon::parse($endDate);
            }
            $result=[];
            foreach ($merchant as $m =>$mm)
            {
                $data=collect(DB::select("
            with acc_cashflow_report as (select
             acd.id AS det_id,
              acd.type_coa,
              acd.name AS det_name,
              acc.md_merchant_id,
              case
              when acc.name = 'Pendapatan Usaha' then 'Pendapatan'
              when acd.name = 'Utang Saldo Deposit' then 'Pendapatan'
              when acd.name = 'Utang PPN Keluaran' then 'Pendapatan'
              when acd.name = 'Utang Pembelian Barang' then 'Beban Operasional & Usaha'
              when acc.name = 'Aktiva Tetap' then 'Pembelian/Penjualan Aset'
              when acc.name = 'Aktiva Tidak Berwujud' then 'Pembelian/Penjualan Aset'
              when acc.name = 'Investasi Jangka Pendek' then 'Investasi Lainnya'
              when acc.name = 'Investasi Jangka Panjang' then 'Investasi Lainnya'
              when acc.name = 'Piutang' then 'Pembayaran/Penerimaan Pinjaman'
              when acc.name = 'Utang' then 'Pembayaran/Penerimaan Pinjaman'
              when acc.name = 'Saldo Laba' then 'Ekuitas/Modal'
              when acc.name = 'Dividen' then 'Ekuitas/Modal'
              else acc.name end as format_name,
              COALESCE(sub.amount, 0) AS amount,
              COALESCE(sub.original, '".$firstDate->toDateString()."') AS _original,
              COALESCE(sub.coa_type, acd.type_coa) AS coa_type,
              0 as is_deleted
            from acc_coa_categories acc
            join acc_coa_details acd
            on acc.id=acd.acc_coa_category_id
            left join (
                select
                  d.acc_coa_detail_id,
                  d.amount,
                  j.trans_time,
                  to_char(j.trans_time, 'YYYY-MM-DD') AS original,
                  d.coa_type
                FROM
                  acc_jurnal_details d
                  JOIN acc_jurnals j ON j.id = d.acc_jurnal_id
                WHERE
                  j.is_deleted in(0,2)
                  and j.md_merchant_id = ".$mm->id."
              ) sub ON sub.acc_coa_detail_id = acd.id
            where
            acc.md_merchant_id=".$mm->id."
            and acc.name in (
                'Pendapatan','Pendapatan Usaha', 'Pendapatan Lainnya',
                'Beban Operasional & Usaha', 'Beban Lainnya',
                'Aktiva Tetap', 'Aktiva Tidak Berwujud',
                'Investasi Jangka Pendek', 'Investasi Jangka Panjang',
                'Utang', 'Utang Pajak', 'Piutang',
                'Modal', 'Saldo Laba','Dividen'
              )
            and acd.is_deleted in(0,2)
            and acd.name not in (
                'Diskon Pembelian Barang', 'Utang Konsinyasi','Utang PPN Keluaran'
            )
            and acd.name not like '%Akumulasi%'
            and acd.name not like '%%Utang PPh%'
            union all
            select
              d.acc_coa_detail_id as det_id,
              d.type_coa,
              d.name as det_name,
              aj.md_merchant_id,
              'Pembayaran ke Pemasok' as format_name,
              aj.trans_amount as amount,
              to_char(aj.trans_time, 'YYYY-MM-DD') AS _original,
              d.coa_type,
              aj.is_deleted
            from
              acc_jurnals aj
              join(
                select
                  ajd.acc_jurnal_id,
                  ajd.acc_coa_detail_id,
                  acd.type_coa,
                  acd.name,
                  ajd.coa_type
                from
                  acc_jurnal_details ajd
                  join acc_coa_details acd on ajd.acc_coa_detail_id = acd.id
                where
                  acd.code like '%1.1.01%'
                  or acd.code like '%1.1.02%'
              ) d on d.acc_jurnal_id = aj.id
            where
              aj.is_deleted in(0,2)
              and aj.md_merchant_id = ".$mm->id."
              and substring(aj.ref_code from 0 for 2)='P'
              or substring(aj.ref_code from 0 for 3)='RP'
              or substring(aj.ref_code from 0 for 4)='PVT'
              or substring(aj.ref_code from 0 for 4)='PPO'
              )
            select acr.*,
            case when acr.format_name='Pendapatan' then 'operasional'
            when acr.format_name='Pendapatan Lainnya' then 'operasional'
            when acr.format_name='Beban Operasional & Usaha' then 'operasional'
            when acr.format_name='Beban Lainnya' then 'operasional'
            when acr.format_name='Pembelian/Penjualan Aset' then 'investasi'
            when acr.format_name='Investasi Lainnya' then 'investasi'
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 'pendanaan'
            when acr.format_name='Ekuitas/Modal'
            then 'pendanaan'
            else 'operasional' end as format_type_name,
            case when acr.format_name='Pendapatan' then 1
            when acr.format_name='Pendapatan Lainnya' then 2
            when acr.format_name='Beban Operasional & Usaha' then 3
            when acr.format_name='Beban Lainnya' then 5
            when acr.format_name='Pembelian/Penjualan Aset' then 6
            when acr.format_name='Investasi Lainnya' then 7
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 8
            when acr.format_name='Ekuitas/Modal'
            then 9
            else 4 end as order_number,
            case when acr.format_name='Pendapatan' then 1
            when acr.format_name='Pendapatan Lainnya' then 2
            when acr.format_name='Beban Operasional & Usaha' then 3
            when acr.format_name='Beban Lainnya' then 4
            when acr.format_name='Pembelian/Penjualan Aset' then 5
            when acr.format_name='Investasi Lainnya' then 6
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 7
            when acr.format_name='Ekuitas/Modal'
            then 8
            else 9 end as format_id
            from acc_cashflow_report acr  where acr.md_merchant_id=".$mm->id."
            "));
                $cashflow=[];
                $result2=$data->sortBy('order_number')->where('_original','<=',$lastDate->toDateString());
                foreach ($result2->groupBy('format_type_name') as $key =>$item)
                {
                    $child=[];
                    $sumTotal=0;
                    $sumBefore=0;
                    foreach ($item->groupBy('format_id') as $i)
                    {
                        $subTotal=0;
                        $before=$i->where('_original','<',$firstDate->toDateString())
                            ->reduce(function ($a,$b)use ($subTotal){
                                if($b->coa_type=='Debit' && $b->type_coa=='Debit')
                                {
                                    $subTotal+=$b->amount;
                                }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Debit')
                                {
                                    $subTotal-=$b->amount;

                                }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Kredit')
                                {
                                    $subTotal+=$b->amount;
                                }else{
                                    $subTotal-=$b->amount;
                                }
                                return $a+$subTotal;
                            },0);
                        $total=$i->whereBetween('_original',[$firstDate->toDateString(),$lastDate->toDateString()])
                            ->reduce(function ($a,$b)use ($subTotal){
                                if($b->coa_type=='Debit' && $b->type_coa=='Debit')
                                {
                                    $subTotal+=$b->amount;
                                }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Debit')
                                {
                                    $subTotal-=$b->amount;

                                }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Kredit')
                                {
                                    $subTotal+=$b->amount;
                                }else{
                                    $subTotal-=$b->amount;
                                }
                                return $a+$subTotal;
                            },0);
                        $child[]=(object)[
                            'name'=>$i->first()->format_name,
                            'amount'=>$total,
                            'order_number'=>$i->first()->order_number
                        ];
                        if($key=='operasional'){
                            if(strpos($i->first()->format_name,'Beban')!==false)
                            {
                                $sumBefore-=$before;
                                $sumTotal-=$total;
                            }else{
                                $sumBefore+=$before;
                                $sumTotal+=$total;
                            }
                        }else{
                            $sumBefore+=$before;
                            $sumTotal+=$total;
                        }
                    }
                    if($key=='operasional')
                    {
                        if(count($child)==4)
                        {
                            array_push($child,(object)[
                                'name'=>'Pembayaran ke Pemasok',
                                'amount'=>0,
                                'order_number'=>4
                            ]);
                        }
                    }
                    $cashflow[]=(object)[
                        'key'=>ucwords($key),
                        'child'=>collect($child)->sortBy('order_number'),
                        'sum_all'=>$sumTotal,
                        'sum_before'=>$sumBefore
                    ];
                }



                $result[]=(object)[
                    'merchant_id'=>$mm->id,
                    'merchant_name'=>$mm->name,
                    'data'=>$cashflow
                ];
            }


            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);

            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            foreach ($result as $k =>$i) {
                $objPHPExcel->createSheet();
                $objPHPExcel->setActiveSheetIndex($k);
                $objPHPExcel->getActiveSheet()->setTitle("Laporan Arus kas");
                $objPHPExcel->getActiveSheet()->setCellValue('H2','LAPORAN ARUS KAS '.strtoupper($i->merchant_name).'');
                $objPHPExcel->getActiveSheet()->setCellValue('H3','Periode '.$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y').'');
                $objPHPExcel->getActiveSheet()->mergeCells('H2:J2');
                $objPHPExcel->getActiveSheet()->mergeCells('H3:J3');
                $objPHPExcel->getActiveSheet()->getStyle('H2:J2')->getFont()->setBold( true );
                $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );
                $objPHPExcel->getActiveSheet()->getStyle("H2:J2")->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getStyle("H3:J3")->applyFromArray($style);
                $col = 'H';

                $previousMonth=0;
                $thisMonth=0;
                $num = 4;
                foreach ($i->data as $key => $item) {

                    $dataShow1 = [
                        'Arus kas dari '.$item->key,
                        " ",
                        " "
                    ];

                    $col = 'H';
                    $row = $num + 1;
                    foreach ($dataShow1 as $ds) {
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                        $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':J'.$row);
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                        $col++;
                    }
                    $num++;
                    foreach($item->child as $i =>$j){
                        if($item->key=='Investasi'){
                            $sumT=($j->amount>0)?'('.rupiah($j->amount).')':rupiah(abs($j->amount));
                        }else{
                            if(strpos($j->name,'Beban')===false && strpos($j->name,'Persediaan')===false ){
                                if($j->amount<0){
                                    $sumT='('.rupiah(abs($j->amount)).')';
                                }else{
                                    $sumT=rupiah($j->amount);
                                }
                            }else{
                                if($j->amount<0){
                                    $sumT='('.rupiah(abs($j->amount)).')';
                                }else{
                                    if($j->amount==0){
                                        $sumT=rupiah($j->amount);
                                    }else{
                                        $sumT='('.rupiah($j->amount).')';
                                    }
                                }
                            }
                        }

                        $dataShow2 = [
                            " ",
                            $j->name,
                            $sumT
                        ];
                        $col = 'H';
                        $row = $num + 1;
                        foreach ($dataShow2 as $ds2) {
                            $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                            $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                            $col++;
                        }
                        $num++;
                    }

                    $num++;
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$num,"Kas bersih yang diperoleh dari Aktivitas ".$item->key);
                    $objPHPExcel->getActiveSheet()->mergeCells('H'.$num.':I'.$num);
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$num.':J'.$num)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$num.':J'.$num)->getFont()->setBold( true );
                    if($item->sum_all<0){
                        $sumT='('.rupiah(abs($item->sum_all)).')';
                    }else{
                        if($item->key=='Investasi'){
                            $sumT=($item->sum_all>0)?'('.rupiah($item->sum_all).')':rupiah(abs($item->sum_all));
                        }else{
                            $sumT=rupiah($item->sum_all);
                        }
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue('J'.$num,$sumT);
                    $row++;
                    $num++;
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$num.':J'.$num)->applyFromArray($styleArray);
                    if($item->key=='Investasi'){
                        $previousMonth-=$item->sum_before;
                        $thisMonth-=$item->sum_all;
                    }else{
                        $previousMonth+=$item->sum_before;
                        $thisMonth+=$item->sum_all;
                    }
                }
                $row+=2;
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,"Kenaikan (penurunan) Kas");
                $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':I'.$row);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                if($thisMonth<0){
                    $sumT='('.rupiah(abs($thisMonth)).')';
                }else{
                    $sumT=rupiah($thisMonth);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$row,$sumT);
                $row++;
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,"Kas pada ".$firstDate->subMonth(1)->endOfMonth()->isoFormat('D MMMM Y')."");
                $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':I'.$row);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                if($previousMonth<0){
                    $sumT='('.rupiah(abs($previousMonth)).')';
                }else{
                    $sumT=rupiah($previousMonth);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$row,$sumT);
                $row++;
                if($lastDate->toDateString()>date('Y-m-d')){
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,"Kas pada ".Carbon::parse(date('Y-m-d'))->endOfMonth()->isoFormat('D MMMM Y')."");

                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,"Kas pada ".$lastDate->isoFormat('D MMMM Y')."");

                }
                $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':I'.$row);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                if($thisMonth+$previousMonth<0){
                    $sumT='('.rupiah(abs($thisMonth+$previousMonth)).')';
                }else{
                    $sumT=rupiah($thisMonth+$previousMonth);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$row,$sumT);

            }

            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.uniqid(date('YmdHis').merchant_id()).'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('Laporan-Arus-Kas-Periode-' .$firstDate->isoFormat('D-MMMM-Y').'-sampai-'.$lastDate->isoFormat('D-MMMM-Y').''. '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
        <div id='successExport' class='text-center'>
            <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
        </div>
        <script>
          if(document.querySelector('#successExport')){
            toastForSaveData('Data berhasil diexport!','success',true,'',true);
          }
        </script>";

        }catch (\Exception $e)
        {
            dd($e);
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }

    }


}

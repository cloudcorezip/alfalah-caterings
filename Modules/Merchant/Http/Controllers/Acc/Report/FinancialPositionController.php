<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace Modules\Merchant\Http\Controllers\Acc\Report;


use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\MasterData\Merchant;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PDF;

class FinancialPositionController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('acc/report/financial-position')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', 'Acc\Report\FinancialPositionController@index')
                    ->name('merchant.toko.acc.report.financial-position.index');
                Route::get('/exportPDF', 'Acc\Report\FinancialPositionController@exportPDF')
                    ->name('merchant.toko.acc.report.financial-position.exportPDF');
                Route::post('/exportExcel', 'Acc\Report\FinancialPositionController@exportExcel')
                    ->name('merchant.toko.acc.report.financial-position.exportExcel');
            });
    }


    public function index()
    {
        $startDate = request()->start_date;
        $endDate = request()->end_date;
        $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
        if(is_null($startDate))
        {
            $firstDate=Carbon::now()->firstOfYear();
            $lastDate=Carbon::now()->lastOfYear();

        }else{
            $firstDate=Carbon::parse($startDate);
            $lastDate=Carbon::parse($endDate);

        }
        $titlePeriod=$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y');


        $data2=DB::select("WITH RECURSIVE financial_position AS (
                  SELECT
                    c.*,
                    json '[]',
                    (
                      SELECT
                        json_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          select
                            acd.id,
                            acd.code,
                            acd.name,
                            acd.type_coa,
                            coalesce(sum(s.sum_of_before_debit),0) sum_of_before_debit,
                            coalesce(sum(s.sum_of_before_kredit),0) sum_of_before_kredit,
                            coalesce(sum(s.sum_of_debit),0) sum_of_debit,
                            coalesce(sum(s.sum_of_kredit),0) sum_of_kredit
                          from
                            acc_coa_details acd
                            left join (select ajd.acc_coa_detail_id,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_before_debit,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_before_kredit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_debit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_kredit
                            from acc_jurnal_details ajd
                            join acc_jurnals j
                            on j.id=ajd.acc_jurnal_id
                            where j.md_merchant_id = ".$merchantId."
                            and j.is_deleted in(0, 2)
                            ) s
                            on s.acc_coa_detail_id=acd.id
                          where
                            acd.acc_coa_category_id = c.id
                            and acd.is_deleted in (0,2)
                            group by acd.id
                        ) jd
                    ) as detail
                  FROM
                    acc_coa_categories c
                  WHERE
                    NOT EXISTS(
                      SELECT
                        c.id
                      FROM
                        acc_coa_categories AS hypothetic_child
                      WHERE
                        hypothetic_child.parent_id = c.id
                    )
                    and c.md_merchant_id = ".$merchantId."
                    and c.is_deleted=0
                  UNION ALL
                  SELECT
                    (parent).*,
                    json_agg(child) AS children,
                    (case when (parent).code='3.0.00' then
                    (
                      SELECT
                        json_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          select
                            acd.id,
                            acd.code,
                            acd.name,
                            acd.type_coa,
                            coalesce(sum(s.sum_of_before_debit),0) sum_of_before_debit,
                            coalesce(sum(s.sum_of_before_kredit),0) sum_of_before_kredit,
                            coalesce(sum(s.sum_of_debit),0) sum_of_debit,
                            coalesce(sum(s.sum_of_kredit),0) sum_of_kredit
                          from
                            acc_coa_details acd
                            left join (select ajd.acc_coa_detail_id,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_before_debit,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_before_kredit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_debit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_kredit
                            from acc_jurnal_details ajd
                            join acc_jurnals j
                            on j.id=ajd.acc_jurnal_id
                            where j.md_merchant_id = ".$merchantId."
                            and j.is_deleted in(0, 2)
                            ) s
                            on s.acc_coa_detail_id=acd.id
                          where
                            acd.acc_coa_category_id = (parent).id
                            and acd.is_deleted in (0,2)
                            group by acd.id
                        ) jd
                    ) else
                    json '[]'
                    end)
                  FROM
                    (
                      SELECT
                        parent,
                        child
                      FROM
                        financial_position AS child
                        JOIN acc_coa_categories parent ON parent.id = child.parent_id
                        and parent.md_merchant_id = ".$merchantId."
                    ) branch
                  GROUP BY
                    branch.parent
                )
                SELECT
                  json_agg(t) as data
                FROM
                  financial_position t
                  LEFT JOIN acc_coa_categories AS hypothetic_parent ON(
                    hypothetic_parent.id = t.parent_id and hypothetic_parent.md_merchant_id=".$merchantId."
                  )
                WHERE
                  hypothetic_parent.id IS null;");

        $result=collect(json_decode($data2[0]->data))->whereIn('code',['1.0.00','2.0.00','3.0.00'])->sortBy('id');
        $merchant=Merchant::find($merchantId);
        $params=[
            'title'=>"Laporan Posisi Keuangan ".$merchant->name,
            'sub_title'=>$titlePeriod,
            'data'=>$result,
            'merchant'=>$merchant,
            'startDate'=>$firstDate->toDateString(),
            'endDate'=>$lastDate->toDateString(),
            'merchantId'=>$merchantId
        ];
        return view('merchant::acc.report.financial-position.index',$params);
    }

    public function exportExcel()
    {
        try {
            $startDate = request()->start_date;
            $endDate = request()->end_date;
            $merchantId=is_null(request()->md_merchant_id)?merchant_id():request()->md_merchant_id;
            if(is_null($startDate))
            {
                $firstDate=Carbon::now()->firstOfYear();
                $lastDate=Carbon::now()->lastOfYear();

            }else{
                $firstDate=Carbon::parse($startDate);
                $lastDate=Carbon::parse($endDate);
            }


            $data2=DB::select("WITH RECURSIVE financial_position AS (
                  SELECT
                    c.*,
                    json '[]',
                    (
                      SELECT
                        json_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          select
                            acd.id,
                            acd.code,
                            acd.name,
                            acd.type_coa,
                            coalesce(sum(s.sum_of_before_debit),0) sum_of_before_debit,
                            coalesce(sum(s.sum_of_before_kredit),0) sum_of_before_kredit,
                            coalesce(sum(s.sum_of_debit),0) sum_of_debit,
                            coalesce(sum(s.sum_of_kredit),0) sum_of_kredit
                          from
                            acc_coa_details acd
                            left join (select ajd.acc_coa_detail_id,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_before_debit,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_before_kredit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_debit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_kredit
                            from acc_jurnal_details ajd
                            join acc_jurnals j
                            on j.id=ajd.acc_jurnal_id
                            where j.md_merchant_id = ".$merchantId."
                            and j.is_deleted in(0, 2)
                            ) s
                            on s.acc_coa_detail_id=acd.id
                          where
                            acd.acc_coa_category_id = c.id
                            and acd.is_deleted in (0,2)
                            group by acd.id
                        ) jd
                    ) as detail
                  FROM
                    acc_coa_categories c
                  WHERE
                    NOT EXISTS(
                      SELECT
                        c.id
                      FROM
                        acc_coa_categories AS hypothetic_child
                      WHERE
                        hypothetic_child.parent_id = c.id
                    )
                    and c.md_merchant_id = ".$merchantId."
                    and c.is_deleted=0
                  UNION ALL
                  SELECT
                    (parent).*,
                    json_agg(child) AS children,
                    (case when (parent).code='3.0.00' then
                    (
                      SELECT
                        json_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          select
                            acd.id,
                            acd.code,
                            acd.name,
                            acd.type_coa,
                            coalesce(sum(s.sum_of_before_debit),0) sum_of_before_debit,
                            coalesce(sum(s.sum_of_before_kredit),0) sum_of_before_kredit,
                            coalesce(sum(s.sum_of_debit),0) sum_of_debit,
                            coalesce(sum(s.sum_of_kredit),0) sum_of_kredit
                          from
                            acc_coa_details acd
                            left join (select ajd.acc_coa_detail_id,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_before_debit,
                            (case when j.trans_time::date<'".$firstDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_before_kredit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Debit' then ajd.amount else 0 end) as sum_of_debit,
                            (case when j.trans_time::date between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."' and ajd.coa_type='Kredit' then ajd.amount else 0 end) as sum_of_kredit
                            from acc_jurnal_details ajd
                            join acc_jurnals j
                            on j.id=ajd.acc_jurnal_id
                            where j.md_merchant_id = ".$merchantId."
                            and j.is_deleted in(0, 2)
                            ) s
                            on s.acc_coa_detail_id=acd.id
                          where
                            acd.acc_coa_category_id = (parent).id
                            and acd.is_deleted in (0,2)
                            group by acd.id
                        ) jd
                    ) else
                    json '[]'
                    end)
                  FROM
                    (
                      SELECT
                        parent,
                        child
                      FROM
                        financial_position AS child
                        JOIN acc_coa_categories parent ON parent.id = child.parent_id
                        and parent.md_merchant_id = ".$merchantId."
                    ) branch
                  GROUP BY
                    branch.parent
                )
                SELECT
                  json_agg(t) as data
                FROM
                  financial_position t
                  LEFT JOIN acc_coa_categories AS hypothetic_parent ON(
                    hypothetic_parent.id = t.parent_id and hypothetic_parent.md_merchant_id=".$merchantId."
                  )
                WHERE
                  hypothetic_parent.id IS null;");


            $result=collect(json_decode($data2[0]->data))->whereIn('code',['1.0.00','2.0.00','3.0.00'])->sortBy('id');

            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);
            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            // $objPHPExcel->getActiveSheet()->setCellValue('D1','Laporan Posisi Keuangan Periode '.$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y').'');
            // $objPHPExcel->getActiveSheet()->mergeCells('D1:G1');

            // $header = [
            //     'Tanggal',
            //     'Transaksi',
            //     'Kode Jurnal',
            //     'Debit',
            //     'Kredit',
            //     'Saldo'
            // ];
            $col = 'D';
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            // foreach ($header as $item) {
            //     $objPHPExcel->getActiveSheet()->setCellValue($col . '3', strtoupper($item));
            //     $objPHPExcel->getActiveSheet()->getStyle($col . '3')->getFont()->setBold( true );
            //     $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($style);
            //     $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($styleArray);
            //     $col++;
            // }
            $asset=0;
            $kewajibanAndEkuitas=0;
            $num = 4;
            $data=collect($result);
            $merchant=Merchant::find($merchantId);
            $objPHPExcel->getActiveSheet()->setCellValue('H2','LAPORAN POSISI KEUANGAN '.strtoupper($merchant->name).'');
            $objPHPExcel->getActiveSheet()->setCellValue('H3','Periode '.$firstDate->isoFormat('D MMMM Y').' - '.$lastDate->isoFormat('D MMMM Y').'');
            $objPHPExcel->getActiveSheet()->mergeCells('H2:J2');
            $objPHPExcel->getActiveSheet()->mergeCells('H3:J3');
            $objPHPExcel->getActiveSheet()->getStyle('H2:J2')->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );
            $objPHPExcel->getActiveSheet()->getStyle("H2:J2")->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle("H3:J3")->applyFromArray($style);

            foreach ($data as $key => $item) {

                $dataShow1 = [
                    $item->name,
                    " ",
                    " "
                ];

                $col = 'H';
                $row = $num + 1;
                foreach ($dataShow1 as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':J'.$row);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                    $col++;
                }
                $num++;
                $subTotal=0;
                $child=collect($item->json)->whereNotIn('code',['1.3.00'])->sortBy('id');
                foreach($child as $c){
                    $subTotalDetail=0;
                    $det1=collect($c->detail)->sortBy('id');
                    foreach($det1 as $dd){
                        if($dd->type_coa=='Debit'){
                            $subTotalDetail+=$dd->sum_of_debit-$dd->sum_of_kredit;
                        }else{
                            if(strpos($dd->name,'Akumulasi')!==false)
                            {
                                $subTotalDetail-=$dd->sum_of_kredit-$dd->sum_of_debit;


                            }else{
                                $subTotalDetail+=$dd->sum_of_kredit-$dd->sum_of_debit;
                            }
                        }
                    }
                    $subTotal+=$subTotalDetail;

                    if($subTotalDetail<0){
                        $sumT=rupiah(abs($subTotalDetail));
                    }else{
                        $sumT=rupiah($subTotalDetail);
                    }
                    $dataShow2 = [
                        ucwords($c->name),
                        " ",
                        $sumT
                    ];
                    $col = 'H';
                    $row = $num + 1;
                    foreach ($dataShow2 as $ds2) {
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds2);
                        $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':I'.$row);
                        $col++;
                    }
                    $num++;
                    $child2=collect($c->json)->sortBy('id');
                    if($child2->count()>0){
                        foreach($child2 as $cc){
                            $subTotalDetail=0;
                            $det2=collect($cc->detail)->sortBy('id');
                            foreach($det2 as $d){
                                if($d->type_coa=='Debit'){
                                    $subTotalDetail+=$d->sum_of_debit-$d->sum_of_kredit;
                                }else{
                                    if(strpos($d->name,'Akumulasi')!==false)
                                    {
                                        $subTotalDetail-=$d->sum_of_kredit-$d->sum_of_debit;


                                    }else{
                                        $subTotalDetail+=$d->sum_of_kredit-$d->sum_of_debit;
                                    }
                                }
                            }
                            $subTotal+=$subTotalDetail;

                            if($subTotalDetail<0){
                                $sumT="(".rupiah(abs($subTotalDetail)).")";
                            }else{
                                $sumT=rupiah($subTotalDetail);
                            }
                            $dataShow3 = [
                                " ",
                                ucwords($cc->name),
                                $sumT

                            ];
                            $col = 'H';
                            $row = $num + 1;
                            // dd($cc);
                            foreach ($dataShow3 as $ds3) {
                                $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                                $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds3);
                                $col++;
                            }
                            $num++;

                        }
                    }
                }
                if($item->code=='1.0.00'){
                    $asset+=$subTotal;
                    if($subTotal<0){
                        $sumT="(".rupiah(abs($subTotal)).")";
                    }else{
                        $sumT=rupiah($subTotal);
                    }
                    $dataShow4 = [
                        'TOTAL '.strtoupper($item->name),
                        " ",
                        $sumT
                    ];
                    $col = 'H';
                    $row = $num + 1;
                    // dd($cc);
                    foreach ($dataShow4 as $ds4) {
                        $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                        $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds4);
                        $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':I'.$row);
                        $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
                        $col++;
                    }
                    $num++;
                }else{
                    $kewajibanAndEkuitas+=$subTotal;
                }
                $row++;
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$row,"Saldo Laba Tahun Berjalan");
                $objPHPExcel->getActiveSheet()->getStyle('I'. $row)->applyFromArray($styleArray);
                if($asset-$kewajibanAndEkuitas<0){
                    $tot1='('.rupiah(abs($asset-$kewajibanAndEkuitas)).')';
                }else{
                    $tot1=rupiah($asset-$kewajibanAndEkuitas);
                }
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $tot1);
                $objPHPExcel->getActiveSheet()->getStyle('H'. $row.':J'.$row)->applyFromArray($styleArray);
                $row++;
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$row,"TOTAL KEWAJIBAN & EKUITAS");
                $objPHPExcel->getActiveSheet()->getStyle('H'. $row)->applyFromArray($styleArray);
                $objPHPExcel->getActiveSheet()->mergeCells('H'.$row.':I'.$row);
                if($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas)<0){
                    $tot1='('.rupiah(abs($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas))).')';
                }else{
                    $tot1=rupiah($kewajibanAndEkuitas+($asset-$kewajibanAndEkuitas));
                }
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $tot1);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->applyFromArray($styleArray);

            }
            $objPHPExcel->getActiveSheet()->getStyle('H'.$row.':J'.$row)->getFont()->setBold( true );
            $row++;


            //dd($data);
            foreach(range('A','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/'.uniqid(date('YmdHis').merchant_id()).'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('Laporan-Posisi-Keuangan-Periode-' .$firstDate->isoFormat('D-MMMM-Y').'-sampai-'.$lastDate->isoFormat('D-MMMM-Y').''. '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);

            $fileURL = asset($destinationPath.'/'.$filename);
            return "<script> location.href='$fileURL'; </script>
        <div id='successExport' class='text-center'>
            <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
        </div>
        <script>
          if(document.querySelector('#successExport')){
            toastForSaveData('Data berhasil diexport!','success',true,'',true);
          }
        </script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";


        }




    }
}

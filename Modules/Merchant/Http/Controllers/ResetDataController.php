<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\MasterData\User;

class ResetDataController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('reset-data')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::post('/reset', 'ResetDataController@reset')
                    ->name('merchant.reset-data.reset');
            });
    }

    public function reset(Request $request)
    {
        try{

            $user = User::find(user_id());
            $password = $request->password;


            if($user->is_from_google == 1 && is_null($user->password)){

                $user->password = Hash::make($password);
                $user->save();

                return response()->json([
                    'message'=>'Password berhasil dibuat !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            if(!Hash::check($password, $user->password)){
                return response()->json([
                    'message'=>'Password yang anda masukkan salah !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);

            }



            DB::beginTransaction();
            DB::statement('
                                delete from acc_merchant_ap_details amad where amad.acc_merchant_ap_id in
                                    (select id from acc_merchant_ap where md_merchant_id='.merchant_id().')
                                ');
            DB::statement('
                                delete from acc_merchant_ap where md_merchant_id='.merchant_id().'
                                ');
            DB::statement('
                                delete from acc_merchant_ar_details amad where amad.acc_merchant_ar_id in
                                    (select id from acc_merchant_ar where md_merchant_id='.merchant_id().')
                                ');
            DB::statement('
                                delete from acc_merchant_ar where md_merchant_id='.merchant_id().'
                                ');
            DB::statement('
                                delete from acc_jurnal_details where acc_jurnal_id in(
                                    select aj.id from acc_jurnals aj
                                    where aj.md_merchant_id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                delete from acc_jurnals where md_merchant_id='.merchant_id().'
                                ');
            DB::statement('delete from sc_inv_production_of_good_boms where sc_inv_production_of_good_id
                        in (select id from sc_inv_production_of_goods where md_merchant_id='.merchant_id().')');

            DB::statement('delete from sc_inv_production_of_good_details where sc_inv_production_of_good_id
                        in (select id from sc_inv_production_of_goods where md_merchant_id='.merchant_id().')');

            DB::statement('delete from sc_inv_production_of_goods where md_merchant_id='.merchant_id().'
            ');

            DB::statement('
                                    delete from sc_stock_sale_mappings where sc_stock_inventory_id in(
                                        select ssi.id from sc_stock_inventories ssi
                                        join sc_products sp
                                        on sp.id=ssi.sc_product_id
                                        join md_users u
                                        on u.id=sp.md_user_id
                                        join md_merchants m
                                        on m.md_user_id=u.id
                                        where m.id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                    delete  from sc_stock_inventories where sc_product_id in
                                    (
                                        select sp.id  from sc_products sp join md_users u
                                        on u.id=sp.md_user_id
                                        join md_merchants m
                                        on m.md_user_id=u.id
                                        where m.id='.merchant_id().'
                                    )
                            ');
            DB::statement('
                                delete from sc_retur_sale_order_details where sc_retur_sale_order_id in(
                                    select srso.id from sc_retur_sale_orders srso
                                    join sc_sale_orders sso
                                    on srso.sc_sale_order_id=sso.id
                                    where sso.md_merchant_id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                delete from sc_sale_order_details where sc_sale_order_id in(
                                    select sso.id from sc_sale_orders sso
                                    join md_merchants m
                                    on m.id=sso.md_merchant_id
                                    where m.id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                delete from sc_retur_sale_orders where sc_sale_order_id in(
                                    select sso.id from sc_sale_orders sso
                                    where sso.md_merchant_id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                delete from sc_temp_sale_order_details
                                    where sc_sale_order_id in(
                                    select sso.id from sc_sale_orders sso
                                    where sso.md_merchant_id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                delete from sc_sale_orders sso where sso.md_merchant_id='.merchant_id().'
                                ');
            DB::statement('
                                delete from sc_temp_purchase_order_details where sc_purchase_order_id in (
                                    select sso.id from sc_purchase_orders sso
                                    where sso.md_merchant_id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                delete from sc_retur_purchase_order_details where sc_retur_purchase_order_id in(
                                    select srso.id from sc_retur_purchase_orders srso
                                    join sc_purchase_orders sso
                                    on srso.sc_purchase_order_id=sso.id
                                    where sso.md_merchant_id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                delete from sc_purchase_order_details where sc_purchase_order_id in(
                                    select sso.id from sc_purchase_orders sso
                                    join md_merchants m
                                    on m.id=sso.md_merchant_id
                                    where m.id='.merchant_id().'
                                    )
                                ');
            DB::statement('
                                delete from sc_retur_purchase_orders where sc_purchase_order_id in(
                                    select sso.id from sc_purchase_orders sso
                                    where sso.md_merchant_id='.merchant_id().'
                                    )
                                ');

            DB::statement('
                                delete from sc_purchase_orders sso where sso.md_merchant_id='.merchant_id().'
                                ');
            DB::statement('
                                    delete  from sc_stock_inventories where sc_product_id in
                                    (
                                        select sp.id  from sc_products sp join md_users u
                                        on u.id=sp.md_user_id
                                        join md_merchants m
                                        on m.md_user_id=u.id
                                        where m.id='.merchant_id().'
                                    )
                            ');
            DB::statement('
                                update sc_products set stock=0
                                    where id in(
                                        select sp.id from sc_products sp
                                        join md_users u
                                        on u.id=sp.md_user_id
                                        join md_merchants m
                                        on m.md_user_id=u.id
                                        where m.id='.merchant_id().'
                                    )
                                ');

            DB::statement('
                                delete from acc_merchant_shifts sso where sso.md_merchant_id='.merchant_id().'
                                ');

            DB::statement('
                        delete from acc_asset_depreciation_details where acc_asset_depreciation_id
            in(select id from acc_asset_depreciations where md_merchant_id='.merchant_id().')
                                            ');

            DB::statement('delete from acc_asset_depreciation_payments where acc_asset_depreciation_id
            in(select id from acc_asset_depreciations where md_merchant_id='.merchant_id().')');

            DB::statement('delete  from acc_closing_journal_details where md_merchant_id='.merchant_id().'
            ');
            DB::statement('delete  from acc_closing_journal_distributions where md_merchant_id='.merchant_id().'
            ');

            DB::statement('delete  from acc_asset_depreciations where md_merchant_id='.merchant_id().'
            ');

            DB::statement('delete from acc_merchant_cashflows where md_merchant_id='.merchant_id().'
            ');

            DB::statement('update tp_paypoin_transactions set is_deleted=1 where md_merchant_id='.merchant_id().'');

            DB::statement('delete from clinic_reservations where md_merchant_id='.merchant_id().'');
            DB::statement('delete from clinic_medical_records where md_merchant_id='.merchant_id().'');
            DB::statement('update sc_transfer_stocks set is_deleted=1 where from_merchant_id='.merchant_id().'');
            DB::statement('update sc_stock_opnames set is_deleted=1 where md_merchant_id='.merchant_id().'');
            DB::statement('update sc_consignment_goods set is_deleted=1 where md_merchant_id='.merchant_id().'');




            DB::commit();

            return response()->json([
                'message'=>'Data berhasil direset !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {

            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal dihapus !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);


        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\PaymentInvoice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\VaBank;
use App\Models\SennaToko\PaymentMethod;
use App\Models\SennaToko\PaymentInvoice;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\MasterData\TransactionType;
use App\Models\QRIS\QRISActivation;


class PaymentInvoiceController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('/payment-invoice')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::get('/','PaymentInvoice\PaymentInvoiceController@index')
                ->name('payment-invoice.index');
            Route::post('/save','PaymentInvoice\PaymentInvoiceController@save')
                ->name('payment-invoice.save');
            Route::post('/expire-date','PaymentInvoice\PaymentInvoiceController@expireDate')
                ->name('payment-invoice.expire-date');
        });
    }

    public function index(Request $request)
    {
        $merchantId = merchant_id();

        $paymentInvoice = PaymentInvoice::where('md_merchant_id', $merchantId)->first();
        if(is_null($paymentInvoice)){
            $data = new PaymentInvoice();
            $jsonInvoice = [];
            $expireDate = Carbon::parse(date('Y-m-d H:i:s'))->addDays(1)->isoFormat('D MMMM Y, HH:mm');
        } else {
            $data = PaymentInvoice::where('md_merchant_id', $merchantId)->first();
            $jsonInvoice = json_decode($data->payment_method);
            $expireDate = Carbon::parse(date('Y-m-d H:i:s'))->addDays($data->invoice_duration)->isoFormat('D MMMM Y, HH:mm');
        }

        $vaBank = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'b.name',
            'b.icon'
        ])
            ->join('md_va_banks as vb', 'vb.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->join('md_banks as b', 'b.id', 'vb.md_bank_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.md_merchant_id',$merchantId)
            ->where('md_merchant_payment_methods.transaction_type_id', 5)
            ->get();

        $eWallet = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'tt.name',
            'tt.icon'
        ])
            ->join('md_transaction_types as tt', 'tt.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->whereNotNull('code')
            ->where('tt.code','!=','QRIS')
            ->where('md_merchant_payment_methods.md_merchant_id',$merchantId)
            ->get();

        $qris = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
        ])
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.transaction_type_id', 4)
            ->where('md_merchant_payment_methods.md_merchant_id',$merchantId)
            ->first();
        
        $cash = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'tt.name',
            'tt.icon'
        ])
            ->join('md_transaction_types as tt', 'tt.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.transaction_type_id', 2)
            ->where('md_merchant_payment_methods.md_va_bank_id', 2)
            ->where('md_merchant_payment_methods.md_merchant_id', $merchantId)
            ->first();

        $merchant = Merchant::find($merchantId);
        $currency = Currency::all();
        $params = [
            "title" => "Pengaturan Invoice",
            "data" => $data,
            "vaBank" => $vaBank,
            "eWallet" => $eWallet,
            "qris" => $qris,
            "currency" => $currency,
            "jsonInvoice" => $jsonInvoice,
            "expireDate" => $expireDate,
            "merchant" => $merchant,
            "cash" => $cash
        ];

        return view('merchant::payment-invoice.index', $params);
    }

    public function save(Request $request)
    {
        try {
            DB::beginTransaction();
            $id = $request->id;
            $merchantId = merchant_id();
            $merchant = Merchant::find($merchantId);

            if(is_null($id)){
                $data = new PaymentInvoice();
            } else {
                $data = PaymentInvoice::find($id);
            }


            $paymentMethod = PaymentMethod::where('md_merchant_id', merchant_id())
                                ->where('status', 1)
                                ->get();

            if(count($paymentMethod) == 0){
                return response()->json([
                    'message'=>'Silahkan pilih metode pembayaran terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);
            }

            if(is_null($merchant->form_order_url)){
                $form_order_url = $this->_generateUniqueCode($merchant->id);
                $merchant->form_order_url = $form_order_url;
                $merchant->save();
            }

            $data->md_merchant_id = $merchantId;
            $data->md_language_id = $request->md_language_id;
            $data->md_currency_id = $request->md_currency_id;
            $data->invoice_duration = $request->invoice_duration;
            $data->theme = $request->theme;
            $data->payment_method = json_encode($paymentMethod);
            $data->save();

            DB::commit();

            return response()->json([
                'message'=>'Data berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function expireDate(Request $request)
    {
        try {
            $extended = $request->invoice_duration;
            $expire = Carbon::parse(date('Y-m-d H:i:s'))->addDays($extended)->isoFormat('D MMMM Y, H:mm');
            return $expire;
        } catch(\Exception $e)
        {
            return false;
        }
    }

    protected function _generateUniqueCode($merchantId)
    {
        $code = CodeGenerator::codeGenerator(10);

        $check = Merchant::where('form_order_url', $code)
                    ->where('id','!=', $merchantId)
                    ->first();

        if(is_null($check)){
            return $code;
        } else {
            return $this->_generateUniqueCode($merchantId);
        }
    }

}

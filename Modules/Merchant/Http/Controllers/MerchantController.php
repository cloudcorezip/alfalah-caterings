<?php

namespace Modules\Merchant\Http\Controllers;

use App\Classes\RangeCriteria;
use App\Classes\Singleton\CodeGenerator;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantTempAmount;
use App\Models\MasterData\BusinessCategory;
use App\Models\MasterData\MerchantMenu;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\Plugin\EnableMerchantPlugin;
use App\Models\Plugin\EnableMerchantPluginDetail;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\Sfa\SfaConfig;
use App\Models\SennaToko\StockSaleMapping;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Modules\Merchant\Entities\Acc\MappingEntity;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Entities\HR\LeaveSettingEntity;
use Modules\Merchant\Entities\MenuEntity;
use Modules\Merchant\Entities\MerchantEntity;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\TypeOfBusiness;
use App\Models\MasterData\User;
use App\Models\MasterData\MerchantServiceAvailable;
use App\Models\MasterData\Region\District;
use App\Models\MasterData\InventoryMethod;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;
use App\Models\MasterData\Plugin;
use App\Models\MasterData\VaBank;
use App\Models\SennaToko\PaymentMethod;
use App\Models\MasterData\TransactionType;
use App\Models\QRIS\QRISActivation;
use App\Models\Grab\GrabActivation;
use Modules\Merchant\Entities\Toko\FreeDeliveryEntity;
use Modules\Merchant\Entities\Toko\TimeOperationalEntity;
use Modules\Merchant\Entities\QRIS\QRISActivationEntity;
use Modules\Merchant\Entities\Setting\ShopeeEntity;
use App\Utils\Plugin\PluginUtil;
use Modules\Merchant\Http\Controllers\HR\LeaveSettingController;
use Modules\Merchant\Models\AttendanceSetting;
use Modules\Merchant\Models\ShiftSetting;
use Ramsey\Uuid\Uuid;
use App\Models\Widget\MerchantDashboardWidget;
use App\Models\SennaToko\MerchantCodeConfig;
use Image;


class MerchantController extends Controller
{

    public function routeWeb()
    {

        Route::get('/', 'MerchantController@dashboardAllV3')
            ->name('merchant.dashboard')
            ->middleware('merchant-verification');
        Route::get('/selling', 'MerchantController@index')
            ->name('merchant.selling')
            ->middleware('merchant-verification');
        Route::get('/report-all', 'MerchantController@reportAll')
            ->name('merchant.report-all')
            ->middleware('merchant-verification');
        Route::get('/detail/{id?}','MerchantController@detail')
            ->name('merchant.toko.profile.detail')
            ->middleware('merchant-verification');
        Route::post('/save','MerchantController@save')
            ->name('merchant.toko.profile.save')
            ->middleware('merchant-verification');
        Route::post('/add/{id}','MerchantController@add')
            ->name('merchant.toko.profile.add')
            ->middleware('merchant-verification');
        Route::post('/save-inv','MerchantController@saveInv')
            ->name('merchant.toko.profile.save-inv')
            ->middleware('merchant-verification');
        Route::post('/get-statistik-header','MerchantController@getStatistikHeader')
            ->name('merchant.toko.get-statistik-header')
            ->middleware('merchant-verification');
        Route::post('/laba-rugi','MerchantController@labaRugiQuery')
            ->name('merchant.toko.laba-rugi')
            ->middleware('merchant-verification');
        Route::post('/arus-kas','MerchantController@arusKasQuery')
            ->name('merchant.toko.arus-kas')
            ->middleware('merchant-verification');
        Route::post('/load-transaction','MerchantController@loadTransactionQuery')
            ->name('merchant.toko.load-transaksi')
            ->middleware('merchant-verification');

        Route::post('/get-statistik-header-inv','MerchantController@getStatistikHeaderInventory')
            ->name('merchant.toko.get-statistik-header-inv')
            ->middleware('merchant-verification');

        Route::post('/save-attendance', [static::class, 'saveAttendance'])
            ->name('merchant.toko.setting.save-attendance')->middleware('merchant-verification');
        Route::post('/add-shift', [static::class, 'addShift'])
            ->name('merchant.toko.setting.add-shift')->middleware('merchant-verification');
        Route::post('/save-shift', [static::class, 'saveShift'])
            ->name('merchant.toko.setting.save-shift')->middleware('merchant-verification');
        Route::post('/delete-shift', [static::class, 'deleteShift'])
            ->name('merchant.toko.setting.delete-shift')->middleware('merchant-verification');

        Route::post('/save-sfa', [static::class, 'saveSfaConfig'])
            ->name('merchant.toko.setting.save-sfa-config')->middleware('merchant-verification');

        Route::post('/save-widget', [static::class, 'saveWidget'])
            ->name('merchant.dashboard.save-widget')->middleware('merchant-verification');


    }

    public function saveWidget(Request $request)
    {
        try {
            $merchantId = merchant_id();

            $data = MerchantDashboardWidget::where('md_merchant_id', $merchantId)
                                            ->first();
            if(is_null($data)){
                $data = new MerchantDashboardWidget();
            }

            $data->md_merchant_id = $merchantId;
            $data->widget_list = $request->widget_list;
            $data->save();

            return response()->json([
                'message'=>'Widget berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }
    }

    public function dashboardAllV3(Request $request)
    {
        $merchantId=merchant_id();
        if(is_null($merchantId))
        {
            return view('merchant::not-found');
        }

        $widgetMerchant = MerchantDashboardWidget::where('md_merchant_id', $merchantId)->first();

        if(is_null($widgetMerchant)){
            $widgetMerchantList = [];
            $widgetList = collect(MerchantDashboardWidget::widgetList());

        } else {
            $widgetMerchantList = collect(json_decode($widgetMerchant->widget_list));
            $id=$widgetMerchantList->pluck('element_id');
            $widgetList = collect(MerchantDashboardWidget::widgetList())
                ->whereNotIn('element_id',$id);

        }


        if(count($widgetMerchantList) < 1){
            $selectedWidget = $widgetList->whereIn('name', ['Produk Terlaris', 'Omzet', 'Komisi Karyawan', 'Arus Kas']);
        } else {
            $selectedWidget = $widgetMerchantList;
        }

        $params=[
            'title'=>'Ringkasan Bisnis',
            'widgetList' => $widgetList,
            'selectedWidget' => $selectedWidget,
            'countedWidget'=>count(MerchantDashboardWidget::widgetList())
        ];


        return view('merchant::toko.dashboard.all-v3', $params);
    }

    public function reportAll(Request $request)
    {

        $params=[
            'page'=>$request->page,
        ];

        if(is_null($params['page'])){
            $subTitle='Keuangan';
        }elseif ($params['page']=='sale-order'){
            $subTitle='Penjualan';
        }elseif ($params['page']=='purchase-order'){
            $subTitle='Pembelian';
        }elseif ($params['page']=='inventory'){
            $subTitle='Persediaan';
        }elseif ($params['page']=='employee'){
            $subTitle='Karyawan';
        }
        $params['subTitle']=$subTitle;
        $params['title']=$subTitle;

        return view('merchant::toko.report.all',$params);

    }

    public function save(Request $request)
    {
        try{
            //dd($request->id);
            $id=$request->id;
            if(!is_null($id)){

                $data=Merchant::find($id);
            }else{
                $data=new Merchant();
            }
            $destinationPath = 'public/uploads/merchant/'.user_id().'/image/';

            if($request->hasFile('image'))
            {
                $file=$request->file('image');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('image'))->resize(300,300)->encode($file->getClientOriginalExtension());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, (string) $image);
            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->image;

                }
            }

            if(is_null($data->form_order_url)){
                $form_order_url = $this->_generateUniqueCode($data->id);
                $data->form_order_url = $form_order_url;
            }

            $data->name=$request->name;
            $data->email_merchant=$request->email_merchant;
            $data->description=$request->description;
            $data->phone_merchant=$request->phone_merchant;
            $data->address=$request->address;
            $data->md_district_id=$request->md_district_id;
            $data->md_business_category_id=$request->md_business_category_id;
            $data->image=$fileName;
            $data->save();

            return response()->json([
                'message'=>'Data berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server, data gagal disimpan !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);


        }
    }



    public  function add($id){

        $data=Merchant::find($id);

        $params=[
            'title'=>'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::toko.profile.form',$params);
    }


    public function saveInv(Request $request)
    {
        try{

            $data=Merchant::find(merchant_id());
            $data->md_inventory_method_id=$request->md_inventory_method_id;
            $data->save();

            return response()->json([
                'message'=>'Data berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }
    }


    //detail merchant
    public function detail(Request $request)
    {

        $id=merchant_id();
        if(merchant_id()!=$id){
            return redirect()->route('pages.not-allowed');
        }
        $data=Merchant::with('getDistrict.getCity.getProvince')
            ->with('getCurrency')
            ->with('getInventoryMethod')
            ->orderBy('id','desc')
            ->find($id);
        $params=[
            'title'=>'Pengaturan',
            'page'=>is_null($request->page)?null:$request->page,
            'data'=>$data

        ];
        $params['gcontact']=EnableMerchantPlugin::
        select([
            'enable_merchant_plugins.*',
            'p.id as plugins_id',
            'p.is_multiple',
            'p.name'
        ])
            ->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
            ->with('getChild')
            ->where('enable_merchant_plugins.is_deleted',0)
            ->where('enable_merchant_plugins.status', 1)
            ->where('p.parent_id',Plugin::PARENT_ID_GCONTACT)
            ->where('p.type', Plugin::GOOGLE_CONTACT)
            ->where('enable_merchant_plugins.md_merchant_id', merchant_id())
            ->first();

        $params['ecommerce'] = EnableMerchantPlugin::
        select([
            'enable_merchant_plugins.*',
            'p.id as plugins_id',
            'p.is_multiple',
            'p.name',
            'p.sub_type'
        ])
            ->join('md_plugins as p', 'p.id', 'enable_merchant_plugins.md_plugin_id')
            ->with('getChild')
            ->where('enable_merchant_plugins.is_deleted', 0)
            ->where('enable_merchant_plugins.status', 1)
            ->where('p.type', Plugin::E_COMMERCE)
            ->where('enable_merchant_plugins.md_merchant_id', merchant_id())
            ->get();

        if(is_null($request->page))
        {


            $type=BusinessCategory::whereNull('parent_id')->with('getChild')->get();
            if(!is_null($data->md_district_id))
            {
                $district=District::where('md_city_id', '=', $data->getDistrict->getCity->id)->get();
                $city=City::where('md_province_id', '=', $data->getDistrict->getCity->getProvince->id)->get();

            }else{
                $district=[];
                $city=[];
            }
            $province=Province::All();

            $params['data']=$data;
            $params['type']=$type;
            $params['district']=$district;
            $params['city']=$city;
            $params['province']=$province;

        }elseif($request->page=='role-access')
        {
            $employee=MerchantStaff::select([
                'u.id',
                'u.fullname',
                'u.email',
                'r.name as role_name',
            ])
                ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->where('m.md_user_id',user_id())
                ->where('md_merchant_staff.is_deleted',0)
                ->get();

            $menu=MerchantMenu::whereNull('parent_id')
                ->with(['getChild'=> function ($query){
                    $query->where('is_active',1);
                }])
                ->where('is_active',1)
                ->orderBy('id','ASC')
                ->get();

            $params['menuList']=$menu;
            $params['employee']=$employee;

        }elseif ($request->page=='inventory')
        {
            $inventory=InventoryMethod::All();
            $params['inventory']=$inventory;
        }elseif ($request->page=='digital-payment')
        {
            $row=QRISActivationEntity::where('md_merchant_id',merchant_id())
                ->count();
            $params['row']=$row;
            $params['tableColumns']=QRISActivationEntity::dataTableColumns();

        }elseif($request->page == 'payment-method')
        {
            $row=QRISActivationEntity::where('md_merchant_id',merchant_id())
            ->first();
            $vaBank = VaBank::select([
                            'md_va_banks.id',
                            'md_va_banks.md_bank_id',
                            'md_va_banks.is_active',
                        ])
                        ->where('md_va_banks.is_active', 1)
                        ->join('md_banks as b','b.id','md_va_banks.md_bank_id')
                        ->get();
            $paymentMethod=PaymentMethod::where('md_merchant_id',merchant_id())
                                        ->get();
            $qris=QRISActivation::where('md_merchant_id',merchant_id())
                                ->where('is_active',1)
                                ->join('md_rajabiller_banks as r','r.id','qris_activations.md_rajabiller_bank_id')
                                ->first();
            $eWallet=TransactionType::whereNotNull('code')
                ->where('code','!=','QRIS')
                                ->get();
            $bankTransfer=PaymentMethod::select(['md_merchant_payment_methods.*','b.name as nama_bank'])
                                ->where('md_merchant_id',merchant_id())
                                ->where('transaction_type_id',13)
                                ->join('md_rajabiller_banks as b','b.id','md_merchant_payment_methods.md_va_bank_id')
                                ->get();
            $params['vaBank'] = $vaBank;
            $params['bankTransfer'] = $bankTransfer;
            $params['paymentMethod'] = $paymentMethod;
            $params['qris'] = $qris;
            $params['eWallet'] = $eWallet;
            $params['row']=$row;


        }elseif($request->page=='reset-data')
        {
            $params['user']=User::find(user_id());

        }

        elseif($request->page=='multi-branch'){
            $params['multiBranch']=DB::table('merchant_multi_branch_configs')
                ->where('md_merchant_id',merchant_id())
                ->get();
        }elseif($request->page=='e-commerce')
        {
            if(count($params['ecommerce']) == 0){
                return Redirect::route('merchant.toko.profile.detail');
            }
            $empShopee = EnableMerchantPlugin::
            select([
                'enable_merchant_plugins.*',
                'p.id as plugins_id',
                'p.is_multiple',
                'p.name',
                'p.max_account'
            ])
                ->join('md_plugins as p', 'p.id', 'enable_merchant_plugins.md_plugin_id')
                ->with('getChild')
                ->where('enable_merchant_plugins.is_deleted', 0)
                ->where('enable_merchant_plugins.status', 1)
                ->where('p.type', Plugin::E_COMMERCE)
                ->where('p.sub_type', 'shopee')
                ->where('enable_merchant_plugins.md_merchant_id', merchant_id())
                ->first();

            $empdShopee = EnableMerchantPluginDetail::where('enable_merchant_plugin_id', $empShopee->id)
                                                        ->where('is_deleted', 0)
                                                        ->get();

            $params['tableColumns']=ShopeeEntity::dataTableColumns();
            $params['key_val']=$request->key;
            $params['empShopee']=$empShopee;
            $params['empdShopee']=$empdShopee;

            if(isset($request->code)){
                return Redirect::route('setting.shopee.access-token', ['code'=>$request->code, 'shop_id'=> $request->shop_id, 'emp_id' => $empShopee->id]);
            }
        }

        if($request->page=='attendance')
        {
            $params['presensi']=AttendanceSetting::where('md_merchant_id',MerchantUtil::getHeadBranch(merchant_id()))->first();
        }

        if($request->page=='sfa')
        {
            $params['sfa']=SfaConfig::where('md_merchant_id',MerchantUtil::getHeadBranch(merchant_id()))->first();
        }

        if($request->page=='shift')
        {
            $params['presensi']=ShiftSetting::whereIn('md_merchant_id',MerchantUtil::getBranch(merchant_id(),1))
                ->where('is_deleted',0)
                ->get();
            self::initShiftList(merchant_id(),user_id());

        }

        if($request->page=='leave'){
            LeaveSettingController::generateInitialLeave(merchant_id(),user_id());
            $params['tableColumns']=LeaveSettingEntity::dataTableColumns();
        }

        if($request->page == 'custom-code'){
            $merchantId = merchant_id();
            $codeConfig = DB::select("
            select
                mtc.id,
                coalesce(mtc.parent_id, mtc.id) as parent_id,
                mtcc.type,
                mtcc.sub_type,
                mtc.name,
                mtcc.is_continue_head,
                s.v->>'value' as value
            from
                merchant_transaction_code_configs mtcc
            join lateral
                JSONB_ARRAY_ELEMENTS(mtcc.json_value) as s(v) ON TRUE
            join
                md_transaction_codes mtc ON (s.v->>'id')::text::int = mtc.id
            where
                mtcc.md_merchant_id = $merchantId
            ");
            
            $params["codeConfig"]  = $codeConfig;
            $params["list"] = MerchantCodeConfig::getList();
        }

        $params['sub'] = $request->sub;
        return view('merchant::toko.profile.detail',$params);

    }

    public static function initShiftList($merchantId,$userId)
    {
        if(merchant_detail_multi_branch($merchantId)->is_branch == 0){
            $presensi=ShiftSetting::where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->get();
            if($presensi->count()<1)
            {
                ShiftSetting::insert([
                    'name'=>'Shift Normal',
                    'color'=>'#99C7F1',
                    'md_merchant_id'=>$merchantId,
                    'start_shift'=>'08:00',
                    'end_shift'=>'17:00',
                    'max_late'=>15,
                    'max_allowed_attendance_per_day'=>2,
                    'is_default'=>1,
                    'work_day'=>json_encode([
                        [
                            'day'=>'Senin',
                            'is_selected'=>true,
                        ],
                        [
                            'day'=>'Selasa',
                            'is_selected'=>true,
                        ],
                        [
                            'day'=>'Rabu',
                            'is_selected'=>true,
                        ],
                        [
                            'day'=>'Kamis',
                            'is_selected'=>true,
                        ],
                        [
                            'day'=>"Jum'at",
                            'is_selected'=>true,
                        ],
                        [
                            'day'=>'Sabtu',
                            'is_selected'=>false,
                        ],
                        [
                            'day'=>'Minggu',
                            'is_selected'=>false,
                        ]
                    ]),
                    'created_by'=>$userId
                ]);
            }
        }
        return true;

    }

    protected function _generateUniqueCode($merchantId)
    {
        $code = CodeGenerator::codeGenerator(10);

        $check = Merchant::where('form_order_url', $code)
                    ->where('id','!=', $merchantId)
                    ->first();

        if(is_null($check)){
            return $code;
        } else {
            return $this->_generateUniqueCode($merchantId);
        }
    }



    public function saveAttendance(Request  $request)
    {
        try {

            $presensi=AttendanceSetting::where('md_merchant_id',merchant_id())->first();
            if(!is_null($presensi)){

                $presensi->is_allowed_shift_change=$request->is_allowed_shift_change;
                $presensi->is_allowed_shift_other_time=$request->is_allowed_shift_other_time;
                $presensi->is_active_reminder_status=$request->is_active_reminder_status;
                $presensi->reminder_attendance=$request->reminder_attendance;
                $presensi->is_use_rule_leave=$request->is_use_rule_leave;
                $presensi->is_count_leave_from_date_joining=$request->is_count_leave_form_date_joining;
                $presensi->save();

            }else{
                $presensi = new AttendanceSetting();
                $presensi->is_allowed_shift_change=$request->is_allowed_shift_change;
                $presensi->is_allowed_shift_other_time=$request->is_allowed_shift_other_time;
                $presensi->is_active_reminder_status=$request->is_active_reminder_status;
                $presensi->is_use_rule_leave=$request->is_use_rule_leave;
                $presensi->md_merchant_id=merchant_id();
                $presensi->created_by=user_id();
                $presensi->reminder_attendance=$request->reminder_attendance;
                $presensi->is_count_leave_from_date_joining=$request->is_count_leave_form_date_joining;
                $presensi->save();

            }

            return response()->json([
                'message'=>'Pengaturan presensi berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }
    }

    public function addShift(Request  $request)
    {
        $id=$request->id;
        if(is_null($id)){
            $data = new ShiftSetting();
        }else{
            $data= ShiftSetting::find($id);
        }

        $params=[
            'title'=>is_null($id)?'Tambah Data' :'Edit Data',
            'data'=>$data,
            'days'=>collect([
                [
                    'day'=>'Senin',
                ],
                [
                    'day'=>'Selasa',
                ],
                [
                    'day'=>'Rabu',
                ],
                [
                    'day'=>'Kamis',
                ],
                [
                    'day'=>"Jum'at",
                ],
                [
                    'day'=>'Sabtu',
                ],
                [
                    'day'=>'Minggu',
                ]
            ])
        ];

        return view('merchant::toko.profile.attendance-setting.form',$params);
    }


    public function saveShift(Request  $request)
    {
        try {

            $id=$request->id;
            if(is_null($id)){
                $data = new ShiftSetting();
                $data->md_merchant_id=merchant_id();
                $data->created_by=user_id();
            }else{
                $data= ShiftSetting::find($id);
            }
            $starShift=preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $request->start_shift);
            $endShift=preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $request->end_shift);
            if($request->half_shift=='' || is_null($request->half_shift)){
                $half=null;
            }else{
                $checkHalf=preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $request->half_shift);
                if($checkHalf!=1)
                {
                    return response()->json([
                        'message'=>'Format shift setangah hari tidak sesuai !',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]);
                }
                $half=$request->half_shift;

            }

            if($starShift!=1)
            {
                return response()->json([
                    'message'=>'Format awal shift tidak sesuai !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            if($endShift!=1)
            {
                return response()->json([
                    'message'=>'Format akhir shift tidak sesuai !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            if(empty($request->is_selected_days)){
                return response()->json([
                    'message'=>'Hari Kerja tidak boleh kosong !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }
            $selectedDays=$request->is_selected_days;
            $days=$request->days_name;

            $workDays=[];
            foreach ($selectedDays as $k => $s)
            {
                if($selectedDays[$k]=="null"){
                    $isSelected=false;
                }else{
                    $isSelected=true;
                }
                $workDays[]=[
                    'day'=>$days[$k],
                    'is_selected'=>$isSelected
                ];
            }
            $data->name=$request->name;
            $data->color=$request->color;
            $data->start_shift=$request->start_shift;
            $data->end_shift=$request->end_shift;
            $data->max_late=$request->max_late;
            $data->max_allowed_attendance_per_day=$request->max_allowed_attendance_per_day;
            $data->work_day=json_encode($workDays);
            $data->half_shift=$half;
            $data->save();

            return response()->json([
                'message'=>'Pengaturan shift berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }
    }

    public function deleteShift(Request  $request)
    {
        try{
            $id=$request->id;
            $data=ShiftSetting::find($id);
            $data->is_deleted=1;
            $data->save();

            return response()->json([
                'message'=>'Pengaturan shift berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }


    public function saveSfaConfig(Request  $request)
    {
        try {

            $sfa=SfaConfig::where('md_merchant_id',merchant_id())->first();
            if(!is_null($sfa)){
                $sfa->allow_change_sale_price=$request->allow_change_sale_price;
                $sfa->allow_add_discount=$request->allow_add_discount;
                $sfa->is_use_sale_target=$request->is_use_sale_target;
                $sfa->is_use_visit_schedule=$request->is_use_visit_schedule;
                $sfa->allow_created_po=$request->allow_created_po;
                $sfa->allow_created_invoice=$request->allow_created_invoice;
                $sfa->allow_change_customer=$request->allow_change_customer;
                $sfa->save();

            }else{
                $sfa = new SfaConfig();
                $sfa->allow_change_sale_price=$request->allow_change_sale_price;
                $sfa->allow_add_discount=$request->allow_add_discount;
                $sfa->is_use_sale_target=$request->is_use_sale_target;
                $sfa->is_use_visit_schedule=$request->is_use_visit_schedule;
                $sfa->allow_created_po=$request->allow_created_po;
                $sfa->allow_created_invoice=$request->allow_created_invoice;
                $sfa->allow_change_customer=$request->allow_change_customer;
                $sfa->md_merchant_id=merchant_id();
                $sfa->save();

            }

            return response()->json([
                'message'=>'Pengaturan aplikasi sales berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }
    }

}

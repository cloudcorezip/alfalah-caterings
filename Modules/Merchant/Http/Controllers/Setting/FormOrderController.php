<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\FormOrder;
use App\Models\MasterData\MerchantFormOrder;
use App\Models\SennaToko\PaymentMethod;
use App\Models\SennaToko\Product;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use Modules\Merchant\Entities\Acc\CoaDetailEntity;
use Modules\Merchant\Entities\Acc\CoaCategoryEntity;
use App\Models\MasterData\SennaCashier\Currency;
use App\Utils\Accounting\CoaUtil;
use App\Utils\ExpeditionUtil;


class FormOrderController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('/setting/form-order')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::get('/','Setting\FormOrderController@index')
                ->name('setting.form-order.index');
            Route::post('/save', 'Setting\FormOrderController@save')
                ->name('setting.form-order.save');
            Route::post('/add-coa', 'Setting\FormOrderController@addCoa')
                ->name('setting.form-order.add-coa');
            Route::post('/save-coa', 'Setting\FormOrderController@saveCoa')
                ->name('setting.form-order.save-coa');

            Route::post('/add-coa-income', 'Setting\FormOrderController@addCoaIncome')
                ->name('setting.form-order.add-coa-income');
        });
    }

    public function index(Request $request)
    {
        $merchantId = merchant_id();
        $merchant = Merchant::find($merchantId);
        $data = MerchantFormOrder::where('md_merchant_id', $merchantId)
                                    ->where('is_deleted', 0)
                                    ->first();
        $formTemplate = FormOrder::where('is_deleted', 0)
                    ->orderBy('id', 'ASC')
                    ->get();

        $shippingCategory = ShippingCategory::whereNotNull("parent_id")
                                ->get();

        $formIds = [];
        $productJson = [];
        $shippingJson = [];
        $link = url("form-order");
        if(!is_null($data)){
            foreach(json_decode($data->template_json) as $key => $item){
                foreach($item->value as $k => $i){
                    array_push($formIds, $i->id);
                }
            }

            foreach(json_decode($data->product_json) as $key => $item){
                array_push($productJson, $item);
            }

            foreach(json_decode($data->shipping_categories_json) as $key => $item){
                array_push($shippingJson, $item);
            }

            $link .= "/".$data->unique_code;

        }

        $product = Product::where("md_user_id", $merchant->md_user_id)
                            ->where("is_with_stock", 0)
                            ->where("is_deleted", 0)
                            ->get();

        $vaBank = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'b.name',
            'b.icon'
        ])
            ->join('md_va_banks as vb', 'vb.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->join('md_banks as b', 'b.id', 'vb.md_bank_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.md_merchant_id',$merchantId)
            ->where('md_merchant_payment_methods.transaction_type_id', 5)
            ->get();
        
        $eWallet = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'tt.name',
            'tt.icon'
        ])
            ->join('md_transaction_types as tt', 'tt.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->whereNotNull('code')
            ->where('tt.code','!=','QRIS')
            ->where('md_merchant_payment_methods.md_merchant_id',$merchantId)
            ->get();

        $qris = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
        ])
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.transaction_type_id', 4)
            ->where('md_merchant_payment_methods.md_merchant_id',$merchantId)
            ->first();

        $cash = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'tt.name',
            'tt.icon'
        ])
            ->join('md_transaction_types as tt', 'tt.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.transaction_type_id', 2)
            ->where('md_merchant_payment_methods.md_va_bank_id', 2)
            ->where('md_merchant_payment_methods.md_merchant_id', $merchantId)
            ->first();        
        
        $params = [
            "title" => "Kustom Form Order",
            "data" => $data,
            "form_template" => $formTemplate,
            "form_ids" => $formIds,
            "vaBank" => $vaBank,
            "eWallet" => $eWallet,
            "qris" => $qris,
            "cash" => $cash,
            "product" => $product,
            "product_json" => $productJson,
            "shipping_category" => $shippingCategory,
            "shipping_json" => $shippingJson,
            "code" => is_null($data) ? null:$data->unique_code,
            "link" => $link,
            'coa'=>CoaDetailEntity::getData(),
            'categoryOption'=>CoaCategoryEntity::getData(),
            'shippingMethod'=>ExpeditionUtil::getList($merchantId),
        ];

        return view('merchant::toko.profile.form-order.index', $params);
    }

    public function save(Request $request)
    {
        try {
            $merchantId = merchant_id();
            $formIds = json_decode($request->form_ids);
            $productJson = json_decode($request->product_json);

            $data = MerchantFormOrder::where('md_merchant_id', $merchantId)
                                        ->where('unique_code', $request->unique_code)
                                        ->where('is_deleted', 0)
                                        ->first();
            if(is_null($data)){
                $data = new MerchantFormOrder();
                $code = $this->_generateUniqueCode($merchantId);
            } else {
                $code = $data->unique_code;
            }
            
            if(count($productJson) == 0){
                return response()->json([
                    'message'=>'Silahkan pilih produk terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);    
            }

            $data->md_merchant_id = $merchantId;
            $data->template_json = $request->form_ids;
            $data->product_json = $request->product_json;
            $data->shipping_categories_json = $request->shipping_json;
            $data->coa_custom_json = $request->coa_custom;
            $data->unique_code = $code;
            $data->save();

            return response()->json([
                'message'=>'Data berhasil disimpan!',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    protected function _generateUniqueCode($merchantId)
    {
        $code = CodeGenerator::codeGenerator(8);

        $check = MerchantFormOrder::where('unique_code', $code)
                    ->where('is_deleted', 0)
                    ->where('md_merchant_id','!=', $merchantId)
                    ->first();

        if(is_null($check)){
            return $code;
        } else {
            return $this->_generateUniqueCode($merchantId);
        }
    }


    public function addCoa(Request $request)
    {
        $id=$request->id;
        if(!is_null($id)){

            $data=CoaDetailEntity::find($id);
        }else{
            $data=new CoaDetailEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'categoryOption'=>CoaCategoryEntity::getData(),
            'currencyOption'=>Currency::where('id',1)->get()
        ];

        return view('merchant::toko.profile.form-order.add-coa',$params);
    }

    public function addCoaIncome(Request $request)
    {
        $id=$request->id;
        if(!is_null($id)){

            $data=CoaDetailEntity::find($id);
        }else{
            $data=new CoaDetailEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'categoryOption'=>CoaCategoryEntity::getData(),
            'currencyOption'=>Currency::where('id',1)->get()
        ];

        return view('merchant::toko.profile.form-order.add-coa-income',$params);
    }

    public function saveCoa(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){
                $data=CoaDetailEntity::find($id);
                if($data->acc_coa_category_id!=$request->acc_coa_category_id){
                    $data->code=CoaUtil::generateCoaDetail($request->acc_coa_category_id);
                }
            }else{
                $data=new CoaDetailEntity();
                $data->code=CoaUtil::generateCoaDetail($request->acc_coa_category_id);
            }
            $data->name=$request->name;
            $data->acc_coa_category_id=$request->acc_coa_category_id;
            $data->is_deleted=2;
            $data->md_sc_currency_id=$request->md_sc_currency_id;
            $data->type_coa=$request->type_coa;
            $data->save();


            return response()->json([
                'message'=>'Daftar Akun berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);

        }
    }

}

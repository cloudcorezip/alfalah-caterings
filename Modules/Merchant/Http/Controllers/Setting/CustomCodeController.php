<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Setting;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\SennaToko\MerchantCodeConfig;

class CustomCodeController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('setting/custom-code')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::post('/save',  [static::class, 'save'])
                    ->name('merchant.toko.setting.custom-code.save');
            });
    }

    public function save(Request $request)
    {
        try{
            $merchantId = merchant_id();
            $type = $request->type;
            $subType = $request->sub_type;
            $data = MerchantCodeConfig::where('md_merchant_id', $merchantId)
                                        ->where('type', $type)
                                        ->where('sub_type', $subType)
                                        ->first();
            if(is_null($data))
            {
                $data = new MerchantCodeConfig();
            }

            $decodeCodeConfig = json_decode($request->code_config);
            $kode = array_values(array_filter($decodeCodeConfig, function($item){
                return $item->id == 1;
            }));

            if(count($kode) < 0){
                return response()->json([
                    'message'=>'Pilih kode terlebih dahulu',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            foreach($kode as $key => $item){
                if($item->value == ''){
                    return response()->json([
                        'message'=>'Isi kode terlebih dahulu',
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }

            $strCode = "'".implode("','", array_column($kode, 'value'))."'";
            $checkCode = DB::select("
            select
                *,
                k->>'value' as kode
            from
                merchant_transaction_code_configs mc,
                jsonb_array_elements(mc.json_value) k
            where
                mc.md_merchant_id = $merchantId
                and
                mc.sub_type != '$request->sub_type'
                and
                cast(k ->> 'id' as integer) = 1
                and
                cast(k ->> 'value' as text) in ($strCode)
            ");

            if(count($checkCode) > 0){
                return response()->json([
                    'message'=>'Kode '.$checkCode[0]->kode.' sudah digunakan !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }

            $serialNumber = array_values(array_filter($decodeCodeConfig, function($item){
                return $item->id == 5;
            }));

            if(count($serialNumber) < 1){
                return response()->json([
                    'message'=>'Pilih No Urut terlebih dahulu',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }
            
            $startedValue = $serialNumber[0]->value;

            if(is_null($startedValue)){
                return response()->json([
                    'message'=>'Isi no urut terlebih dahulu',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }
            
            $formula = "";
            foreach($decodeCodeConfig as $key => $item)
            {
                if($item->id == 5){
                    $formula .= "(nextval),";
                } else {
                    $formula .= $item->value .",";
                }
            }

            $data->md_merchant_id = $merchantId;
            $data->type = $type;
            $data->sub_type = $subType;
            $data->formula = rtrim($formula, ",");
            $data->started_value = $startedValue;
            $data->is_continue_head = $request->is_continue_head;
            $data->json_value = json_encode($decodeCodeConfig);
            $data->save();

            return response()->json([
                'message'=>'Data berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }
}

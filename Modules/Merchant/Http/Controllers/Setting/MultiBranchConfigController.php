<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\Setting;


use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Models\QRIS\RajabillerBank;
use App\Models\SennaToko\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\QRIS\QRISActivationEntity;

class MultiBranchConfigController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('setting/multi-branch')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::post('/get-list',  [static::class, 'getList'])
                    ->name('merchant.toko.setting.multi-branch.list');
                Route::post('/update-status',  [static::class, 'updateStatus'])
                    ->name('merchant.toko.setting.multi-branch.update-status');
            });
    }


    public function updateStatus(Request $request)
    {
        try{
            // dd($request->all());
            $data=DB::table('merchant_multi_branch_configs')
                ->where('md_merchant_id',merchant_id())
                ->where('menu_name',$request->menu)
                ->first();
            if(is_null($data)){
                DB::table('merchant_multi_branch_configs')
                    ->insert([
                        'menu_name'=>$request->menu,
                        'is_available_add'=>$request->is_active,
                        'is_available_edit'=>$request->is_active,
                        'is_available_delete'=>$request->is_active,
                        'md_merchant_id'=>merchant_id()
                    ]);
            }else{
                DB::table('merchant_multi_branch_configs')
                    ->where('md_merchant_id',merchant_id())
                    ->where('menu_name',$request->menu)
                    ->update([
                        'is_available_add'=>$request->is_active,
                        'is_available_edit'=>$request->is_active,
                        'is_available_delete'=>$request->is_active,
                    ]);
            }


            return response()->json([
                'message'=>'Akses cabang berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);


        }
    }


    public function getList(Request  $request)
    {

        try{

            $data=DB::table('merchant_multi_branch_configs')
                ->where('md_merchant_id',merchant_id())->get();

            return response()->json([
                'code'=>200,
                'message'=>'Pengambilan data berhasil',
                'data'=>$data
            ]);

        }catch (\Exception $e){
            return response()->json([
                'code'=>500,
                'message'=>'Terjadi kesalahan saat pengambilan data',
                'data'=>[]
            ]);
        }



    }
}

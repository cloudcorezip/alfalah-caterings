<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Merchant\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\Plugin;
use App\Models\Plugin\EnableMerchantPlugin;
use App\Models\Plugin\EnableMerchantPluginDetail;
use App\Models\SennaPayment\Transaction;
use App\Models\SennaPayment\PaymentPlugin;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\SennaPayment\TransactionStatus;
use App\Models\SennaToko\Merk;
use Modules\Merchant\Entities\Toko\MerkEntity;
use Modules\Merchant\Entities\Setting\ShopeeEntity;
use Illuminate\Support\Facades\Redirect;


class EcommerceController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('/setting/ecommerce')
        ->middleware('merchant-verification')
        ->group(function (){
            Route::post('/add', [static::class, 'add'])
                ->name('setting.ecommerce.add');
            Route::post('/save', [static::class, 'save'])
                ->name('setting.ecommerce.save');
            Route::post('/delete', [static::class, 'delete'])
                ->name('setting.ecommerce.delete');
            Route::post('/data-table', [static::class, 'dataTable'])
                ->name('setting.ecommerce.datatable');

        });
    }

    public function add(Request $request)
    {
        $id=$request->id;
        $data=EnableMerchantPluginDetail::find($id);
        $params=[
            'title'=>'Edit Data',
            'data'=>$data,
        ];
        return view('merchant::toko.profile.e-commerce.form',$params);
    }

    public function save(Request $request)
    {
        try {
            $id = $request->id;
            $data = EnableMerchantPluginDetail::find($id);

            $data->is_active = $request->is_active;
            $data->save();

            return response()->json([
                'message'=>'Data berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $id = $request->id;

            $data = EnableMerchantPluginDetail::find($id);
            $data->is_deleted = 1;
            $data->save();
            return response()->json([
                'message'=>'Data berhasil dihapus !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal dihapus !',
                'type'=>'danger',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);
        }
    }

    public function dataTable(Request $request)
    {
        parent::setMenuKey($request->key_val);
        return ShopeeEntity::dataTable($request,parent::getPermission('_edit')
            ,parent::getPermission('_hapus'),
            (get_role()!=3)?true:true);
    }

    public function auth(Request $request)
    {
        try {
            $url = env('NODE_URL').'api/ecommerce/shopee/auth';
            $curl = curl_init($url);
            $userToken = get_user_token();
            $headers = array(
                "Content-type: Application/json",
                "senna-auth: $userToken"
            );
            curl_setopt_array($curl, [
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false
            ]);
            $resp = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($resp);
            if($response->code == 200)
            {
                return Redirect::to($response->data->auth_link);
            } else {
                $message = [
                    "type" => "warning",
                    "message" => "Terjadi kesalahan saat menghubungkan toko !",
                ];
                return Redirect::route('merchant.toko.profile.detail',[
                    "page"=>"e-commerce",
                    "sub"=>"shopee"
                ])->with('status', $message);
            }
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            $message = [
                "type" => "danger",
                "message" => "Terjadi kesalahan di server data gagal disimpan !",
            ];
            return Redirect::route('merchant.toko.profile.detail',[
                "page"=>"e-commerce",
                "sub"=>"shopee"
            ])->with('status', $message);
        }
    }

    public function accessToken(Request $request)
    {
        try {
            $url = env('NODE_URL').'api/ecommerce/shopee/accessToken';
            $code = $request->code;
            $shopId = $request->shop_id;

            $curl = curl_init($url);
            $userToken = get_user_token();
            $headers = array(
                "Content-type: Application/json",
                "senna-auth: $userToken"
            );
            $params = [
                "code" => $code,
                "shop_id" => $shopId
            ];

            curl_setopt_array($curl, [
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_POSTFIELDS => json_encode($params),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false
            ]);

            $resp = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($resp);
            if($response->code == 200)
            {
                $data = EnableMerchantPluginDetail::where('shop_id', $request->shop_id)
                                                    ->where('is_deleted', 0)
                                                    ->first();
                if(is_null($data)){
                    $data = new EnableMerchantPluginDetail();
                }

                $data->enable_merchant_plugin_id = $request->emp_id;
                $data->shop_name = $response->data->store_detail->shop_name;
                $data->shop_id = $request->shop_id;
                $data->code = $request->code;
                $data->is_active = 1;
                $data->refresh_token = $response->data->auth_token->refresh_token;
                $data->access_token = $response->data->auth_token->access_token;
                $data->other_information = json_encode($response->data);
                $data->save();

                $message = [
                    "type" => "success",
                    "message" => "Berhasil menghubungkan Toko !",
                ];
                return Redirect::route('merchant.toko.profile.detail',[
                    "page"=>"e-commerce",
                    "sub"=>"shopee"
                ])->with('status', $message);
            } else {
                $message = [
                    "type" => "warning",
                    "message" => "Terjadi kesalahan saat menghubungkan toko !",
                ];

                return Redirect::route('merchant.toko.profile.detail',[
                    "page"=>"e-commerce",
                    "sub"=>"shopee"
                ])->with('status', $message);
            }

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            $message = [
                "type" => "danger",
                "message" => "Terjadi kesalahan di server data gagal disimpan !",
            ];
            return Redirect::route('merchant.toko.profile.detail',[
                "page"=>"e-commerce",
                "sub"=>"shopee"
            ])->with('status', $message);
        }
    }

}

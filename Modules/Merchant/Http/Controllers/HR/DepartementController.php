<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\HR;


use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Utils\Accounting\CoaUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\HR\DepartementEntity;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Models\Departement;
use Ramsey\Uuid\Uuid;

class DepartementController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('hr/departement')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.hr.departement.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.hr.departement.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.hr.departement.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.hr.departement.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.hr.departement.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Departemen',
            'tableColumns'=>DepartementEntity::dataTableColumns(),
            'add'=>DepartementEntity::add(true),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),

        ];

        return view('merchant::hr.departement.index',$params);


    }

    public function dataTable(Request $request)
    {
        return DepartementEntity::dataTable($request);
    }


    public  function add(Request $request){

        $id=$request->id;
        $noReload = $request->no_reload;
        if(!is_null($id)){

            $data=DepartementEntity::find($id);
        }else{
            $data=new DepartementEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'option'=>DepartementEntity::listOption(),
            'no_reload' => (is_null($noReload))? 0:$noReload
        ];

        return view('merchant::hr.departement.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=DepartementEntity::find($id);
            }else{
                $data=new DepartementEntity();
                $data->md_merchant_id=merchant_id();
                $data->created_by=user_id();

            }
            $data->code=$request->code;
            $data->name=$request->name;
            if($request->parent_id!='-1')
            {
                $data->parent_id=$request->parent_id;
            }
            $data->save();

            return response()->json([
                'message'=>'Data departemen berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,
                'no_reload' => $request->no_reload

            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=DepartementEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            return response()->json([
                'message'=>'Departemen berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }

}

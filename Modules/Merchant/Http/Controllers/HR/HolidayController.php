<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\HR;


use App\Http\Controllers\Controller;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\HR\HolidayEntity;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Models\Holiday;
use Ramsey\Uuid\Uuid;

class HolidayController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('hr/holiday')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.hr.holiday.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.hr.holiday.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.hr.holiday.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.hr.holiday.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.hr.holiday.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Hari Libur',
            'tableColumns'=>HolidayEntity::dataTableColumns(),
            'add'=>HolidayEntity::add(true),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),

        ];

        return view('merchant::hr.holiday.index',$params);

    }


    public static function getHoliday($year,$merchantId,$userId)
    {
        try {
           if(merchant_detail_multi_branch($merchantId)->is_branch == 0){
               $curl=\App\Classes\Singleton\Holiday::getInstance()->_curl($year);
               $holidays=[];
               $colorHoliday='#c82333';
               if($curl['status']==true)
               {
                   foreach ($curl['data'] as $item)
                   {
                       if($item->is_national_holiday==true)
                       {
                           $holidays[]=[
                               'holiday_date'=>$item->holiday_date,
                               'occasion'=>$item->holiday_name,
                               'is_national_holiday'=>1,
                               'is_work'=>0,
                               'is_leave'=>0,
                               'color'=>$colorHoliday,
                               'md_merchant_id'=>$merchantId,
                               'created_by'=>$userId,
                               'created_at'=>date('Y-m-d H:i:s'),
                               'updated_at'=>date('Y-m-d H:i:s'),
                               'is_generate_system'=>1
                           ];
                       }
                   }
               }

               $check=Holiday::where('md_merchant_id',$merchantId)
                   ->whereYear('created_at',$year)
                   ->where('is_generate_system',1)
                   ->first();
               if(is_null($check))
               {
                   Holiday::insert($holidays);
               }
           }
            return true;

        }catch (\Exception $e)
        {
            return false;
        }
    }

    public function dataTable(Request $request)
    {
        return HolidayEntity::dataTable($request);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=HolidayEntity::find($id);
        }else{
            $data=new HolidayEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::hr.holiday.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=HolidayEntity::find($id);
            }else{
                $data=new HolidayEntity();
                $data->md_merchant_id=merchant_id();
                $data->created_by=user_id();

            }
            $data->holiday_date=$request->holiday_date;
            $data->occasion=$request->occasion;
            $data->is_national_holiday=$request->is_national_holidays;
            $data->is_work=$request->is_works;
            $data->is_leave=$request->is_leaves;
            $data->save();

            return response()->json([
                'message'=>'Data hari libur berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=HolidayEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            return response()->json([
                'message'=>'hari libur berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }
}

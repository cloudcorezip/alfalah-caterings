<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\HR;


use App\Http\Controllers\Controller;
use App\Models\MasterData\PermitCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Entities\HR\LeaveSettingEntity;
use Ramsey\Uuid\Uuid;

class LeaveSettingController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('hr/leave-setting')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.hr.leave-setting.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.hr.leave-setting.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.hr.leave-setting.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.hr.leave-setting.delete');
            });
    }


    public function dataTable(Request $request)
    {
        return LeaveSettingEntity::dataTable($request);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=LeaveSettingEntity::find($id);
        }else{
            $data=new LeaveSettingEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::toko.profile.attendance-setting.form-leave',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=LeaveSettingEntity::find($id);
            }else{
                $data=new LeaveSettingEntity();
                $data->md_merchant_id=merchant_id();
                $data->created_by=user_id();

            }
            $data->color=$request->color;
            $data->name=$request->name;
            $data->max_day=$request->max_day;
            $data->is_with_paid=$request->is_with_paid;
            $data->is_per_month=$request->is_per_month;
            $data->type_of_salary_cut=$request->type_of_salary_cut;
            if($request->is_with_paid==0)
            {
                $data->is_with_salary_cut=1;
                if($request->type_of_salary_cut==2){
                    $data->salary_cut_value=$request->salary_cut_value;
                }
            }

            $data->type_of_attendance=$request->type_of_attendance;
            $data->is_for_employee_over_one_year = $request->is_for_employee_over_one_year;
            $data->save();

            return response()->json([
                'message'=>'Data izin/cuti berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=LeaveSettingEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            return response()->json([
                'message'=>'cuti/izin berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }

    public static function generateInitialLeave($merchantId,$userId)
    {
        if(merchant_detail_multi_branch($merchantId)->is_branch==0){
            $check=LeaveSettingEntity::where('md_merchant_id',$merchantId)
                ->count();
            if($check<1)
            {
                $permitCategoryList=PermitCategory::all();
                $insert=[];

                foreach ($permitCategoryList as $l)
                {
                    $insert[]=[
                        'name'=>$l->name,
                        'color'=>'#7B68EE',
                        'md_merchant_id'=>$merchantId,
                        'created_by'=>$userId,
                        'type_of_attendance'=>$l->type_of_attendance
                    ];
                }
                LeaveSettingEntity::insert($insert);
            }
        }
    }

}

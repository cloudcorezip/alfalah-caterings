<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\HR;


use App\Classes\CollectionPagination;
use App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\Attendance\AttendanceController;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantStaff;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\HR\DepartementEntity;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Entities\Toko\StaffEntity;
use Modules\Merchant\Http\Controllers\MerchantController;
use Modules\Merchant\Models\Holiday;
use Modules\Merchant\Models\ShiftRoaster;
use Modules\Merchant\Models\ShiftSetting;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Ramsey\Uuid\Uuid;

class ShiftRoasterController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('hr/shift-roaster')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.hr.shift-roaster.index');
                Route::get('/get-employee', [static::class, 'getEmployee'])
                    ->name('merchant.hr.shift-roaster.employee-list');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.hr.shift-roaster.add');
                Route::post('/add-bulk', [static::class, 'addBulk'])
                    ->name('merchant.hr.shift-roaster.add-bulk');
                Route::post('/save', [static::class, 'saveShift'])
                    ->name('merchant.hr.shift-roaster.save');
                Route::post('/save-bulk', [static::class, 'saveBulk'])
                    ->name('merchant.hr.shift-roaster.save-bulk');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.hr.shift-roaster.delete');
                Route::post('/export-data', [static::class, 'exportData'])
                    ->name('merchant.hr.shift-roaster.export-data');

            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $merchantId=(is_null($request->md_merchant_id))?merchant_id():$request->md_merchant_id;
        HolidayController::getHoliday(date('Y'),$merchantId,user_id());
        MerchantController::initShiftList($merchantId,user_id());
        $startDate=$request->start_date;
        $endDate=$request->end_date;
        if(is_null($startDate) || $startDate==''){
            $startDate=Carbon::now()->startOfMonth()->toDateString();
            $endDate=Carbon::now()->endOfMonth()->toDateString();
        }

        $job=(is_null($request->job_id))?'-1':$request->job_id;
        $departement=is_null($request->departement_id)?'-1':$request->departement_id;

        $params=[
            'title'=>'Jadwal Shift',
            'data'=>$this->_loadShiftRoaster($startDate,$endDate,$merchantId,$departement,$job,true,$request->page),
            'series'=>$this->_loadHeaderDay($startDate,$endDate),
            'shift'=>ShiftSetting::whereIn('md_merchant_id',MerchantUtil::getBranch(merchant_id()))
                ->where('is_deleted',0)
                ->get(),
            'merchantId'=>$merchantId,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'job'=>JobEntity::listOption(),
            'departement'=>DepartementEntity::listOption(),
            'job_id'=>$job,
            'departement_id'=>$departement

        ];

        return view('merchant::hr.shift-roaster.index',$params);
    }

    private function _loadHeaderDay($startDate,$endDate)
    {
        return DB::select("
        select
          to_char(mon.mon, 'YYYY-MM-DD') AS time,
          to_char(mon.mon, 'DD') as day,
          to_char(mon.mon, 'MM') as month
        from
          generate_series(
            '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
            interval '1 day'
          ) as mon(mon)
        ");
    }



    private function _loadShiftRoaster($startDate,$endDate,$merchantId,$departement,$job,$isPagination=true,$page='')
    {
        try {
            $departementId=($departement=='-1' || $departement=='')?"":"and d.id=$departement";
            $jobId=($job=='-1' || $job=='')?"":"and j.id=$job";

            $data=collect(DB::select("
            select
              mms.id as staff_id,
              u.id as staff_user_id,
              u.fullname,
              j.name as job_name,
              mms.md_merchant_id,
              coalesce(
                (
                  select
                    jsonb_agg(jd.*) AS jsonb_agg
                  from
                    (
                      select
                        to_char(mon.mon, 'YYYY-MM-DD') AS time,
                        to_char(mon.mon, 'DD') as day,
                        to_char(mon.mon, 'MM') as month,
                        case when s.type_of_attendance is null then 'A' else s.type_of_attendance end as type_of_attendance,
                        case when h.is_work = 1 then 0 else (
                          case when h.occasion isnull then 0 else 1 end
                        ) end as is_holiday,
                        h.occasion,
                        sr.shift_date,
                        sr.name,
                        sr.color,
                        sr.id,
                        sr.start_shift,
                        sr.end_shift
                      from
                        generate_series(
                          '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                          interval '1 day'
                        ) as mon(mon)
                        left join (
                          select
                            date_trunc('day', mmsa.created_at) as mon,
                            mmsa.type_of_attendance
                          from
                            md_merchant_staff_attendances mmsa
                          where
                            mmsa.md_staff_user_id =u.id
                          group by
                            mon,
                            mmsa.id
                        ) s on mon.mon = s.mon
                        left join(
                          select
                            hmh.holiday_date,
                            hmh.is_work,
                            hmh.occasion
                          from
                            hr_merchant_holidays hmh
                          where
                            hmh.is_deleted = 0
                            and hmh.md_merchant_id = mms.md_merchant_id
                        ) h on h.holiday_date = mon.mon
                        left join (
                          select
                            r.shift_date,
                            ss.name,
                            ss.color,
                            r.id,
                            ss.start_shift,
                            ss.end_shift
                          from
                            hr_merchant_shift_roasters r
                            join hr_merchant_shift_settings ss on ss.id = r.shift_id
                          where
                            r.is_deleted = 0
                            and r.staff_user_id = u.id
                        ) sr on sr.shift_date = mon.mon
                      order by
                        mon.mon asc
                    ) jd
                ),
                '[]'
              ) as roaster_list
            from
              md_merchant_staff mms
              join md_users u on mms.md_user_id = u.id
              left join hr_merchant_job_positions j on j.id = mms.job_position_id
              left join hr_merchant_departements as d on d.id=mms.departement_id
            where
              mms.md_merchant_id = $merchantId
              and mms.is_deleted = 0
              and mms.is_non_employee=0
              $departementId
              $jobId

            "));
            if($isPagination==true)
            {
                return CollectionPagination::paginate($data,10);
            }else{
                return  $data;
            }
        }catch (\Exception $e)
        {
            return collect([]);
        }
    }

    public function add(Request  $request)
    {
        $id=$request->id;
        $staffId=$request->staff_id;
        $userId=$request->user_id;
        $merchantId=$request->md_merchant_id;
        $date=$request->date;

        if(is_null($id))
        {
            $data=new ShiftRoaster();
        }else{
            $data=ShiftRoaster::find($id);
        }
        $params=[
            'title'=>is_null($id)?'Tambah Jadwal Shift':'Edit Jadwal Shift',
            'data'=>$data,
            'staffId'=>$staffId,
            'userId'=>$userId,
            'merchantId'=>$merchantId,
            'date'=>$date,
            'shift'=>ShiftSetting::whereIn('md_merchant_id',MerchantUtil::getBranch(merchant_id()))
                ->where('is_deleted',0)
                ->get()
        ];

        return view('merchant::hr.shift-roaster.form',$params);


    }


    public function addBulk(Request  $request)
    {
        $params=[
            'title'=>'Tambah Jadwal Shift',
            'shift'=>ShiftSetting::whereIn('md_merchant_id',MerchantUtil::getBranch(merchant_id()))
                ->where('is_deleted',0)
                ->get(),
            'departement'=>DepartementEntity::listOption(),
        ];

        return view('merchant::hr.shift-roaster.form-bulk',$params);


    }

    public function saveShift(Request  $request)
    {
        try{
            $id=$request->id;
            $staffId=$request->staff_id;
            $userId=$request->user_id;
            $merchantId=$request->md_merchant_id;
            $date=$request->date;

            if(is_null($id))
            {
                $data=new ShiftRoaster();
            }else{
                $data=ShiftRoaster::find($id);
            }
            $data->shift_date=$date;
            $data->staff_id=$staffId;
            $data->staff_user_id=$userId;
            $data->md_merchant_id=$merchantId;
            $data->shift_id=$request->shift_id;
            $data->timezone=$request->_timezone;
            $data->save();

            return response()->json([
                'message'=>'Data jadwal shift berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);
        }catch (\Exception $e)
        {

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);
        }

    }

    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=ShiftRoaster::find($id);
            $data->is_deleted=1;
            $data->save();

            return response()->json([
                'message'=>'jadwal shift berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }

    public function exportData( Request  $request)
    {
        try {

            $merchantId=$request->md_merchant_id;
            $startDate=$request->start_date;
            $endDate=$request->end_date;
            $job=$request->job_id;
            $departement=$request->departement_id;




            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3600);

            $series=$this->_loadHeaderDay($startDate,$endDate);
            // set data excel
            $fileName = public_path('example/example.xlsx');
            $objPHPExcel = \PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);
            $merchant=Merchant::find($merchantId);
            $objPHPExcel->getActiveSheet()->setCellValue('A1',"Laporan Jadwal Shift ".strtoupper($merchant->name)." Periode $startDate sd $endDate");
            $seriesName=[];
            foreach ($series as $k => $s)
            {
                if($k==0)
                {
                    $seriesName[]='Karyawan';
                }
                $seriesName[]=Carbon::parse($s->time)->isoFormat('D MMMM Y');

            }


            $data=$this->_loadShiftRoaster($startDate,$endDate,$merchantId,$departement,$job,false,'');

            $col = 'A';
            $style = array(
                'alignment' => array(
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                    'vertical' => Alignment::VERTICAL_CENTER
                )
            );
            $styleArray = [
                'borders' => [
                    'outline' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            foreach ($seriesName as $item) {
                $objPHPExcel->getActiveSheet()->setCellValue($col . '3', strtoupper($item));
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->getFont()->setBold( true );
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getStyle($col . '3')->applyFromArray($styleArray);
                $col++;
            }

            $num = 3;
            foreach ($data as $n => $item) {
                $dataShow=[];

                foreach (json_decode($item->roaster_list) as $i  => $r)
                {
                    if($i==0){
                        $dataShow[]=$item->fullname.' '. $item->job_name;
                    }
                    if($r->is_holiday==0)
                    {
                        if($r->type_of_attendance=='S'){
                            $name='Sakit';
                        }elseif ($r->type_of_attendance=='I'){
                            $name='Izin/Cuti';
                        }else{
                            if(is_null($r->id)){
                                $name='Belum Ada Shift';
                            }else{
                                $name=$r->name.' ['.$r->start_shift.' - '.$r->end_shift.']';
                            }
                        }
                    }else{
                        $name=$r->occasion;
                    }
                    $dataShow[]=$name;
                }

                $col = 'A';
                $row = $num + 1;
                foreach ($dataShow as $ds) {
                    $objPHPExcel->getActiveSheet()->setCellValue($col . $row, $ds);
                    $objPHPExcel->getActiveSheet()->getStyle($col . $row)->applyFromArray($styleArray);
                    $col++;
                }
                $num++;

            }

            foreach(range('A','G') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            $destinationPath = 'public/uploads/merchant/shift/'.$merchantId.'/export';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }
            $filename = strtolower('jadwal_shift_' . $merchantId . '.xlsx');
            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($objPHPExcel);
            $writer->save($destinationPath.'/'.$filename);
            $fileURL = asset($destinationPath.'/'.$filename);

            return "<script> location.href='$fileURL'; </script>
            <div id='successExport' class='text-center'>
                <img src='".asset('public/backend/img/loading-horizontal.gif')."' />
            </div>
            <script>
              if(document.querySelector('#successExport')){
                toastForSaveData('Data berhasil diexport!','success',false,'',false);
              }
            </script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal diexport!</div>";

        }
    }

    public function getEmployee(Request  $request)
    {
        try {
            $key=$request->key;
            $merchantId=$request->md_merchant_id;
            $departement=$request->departement_id;
            $employee=MerchantStaff::
            select([
                DB::raw("md_merchant_staff.id"),
                'u.fullname as text',
                DB::raw("md_merchant_staff.id || '_' || u.id as other_id"),
            ])
                ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                ->where('md_merchant_staff.md_merchant_id',$merchantId)
                ->where('md_merchant_staff.is_non_employee',0)
                ->where('md_merchant_staff.is_deleted',0);

            if(!is_null($key) || $key!='')
            {
                $employee->whereRaw("lower(u.fullname) like '%".strtolower($key)."%'");
            }
            if($departement!='-1')
            {
                $employee->where('md_merchant_staff.departement_id',$departement);
            }
            return response()->json($employee->get());
        }catch (\Exception $e)
        {
            return response()->json([]);
        }
    }

    public function saveBulk(Request  $request)
    {
        try {
            DB::beginTransaction();
            $merchantId=$request->md_merchant_id;
            $isByMonth=$request->is_by_month;
            $shiftId=$request->shift_id;
            $staffId=explode(',',$request->staff_id);
            if($isByMonth==0)
            {
                $series=$this->_loadHeaderDay($request->start_date,$request->end_date);
            }else{
                $mergeYearAndMonth=$request->year_id.'-'.$request->month_id;
                $startMonth=Carbon::parse($mergeYearAndMonth)->startOfMonth()->toDateString();
                $endMonth=Carbon::parse($mergeYearAndMonth)->endOfMonth()->toDateString();
                $series=$this->_loadHeaderDay($startMonth,$endMonth);
            }
            $insert=[];
            foreach ($staffId as $key =>$item)
            {
                $staffIds=explode('_',$staffId[$key]);
                foreach ($series as $s)
                {
                    ShiftRoaster::where('shift_date',$s->time)
                        ->where('staff_id',$staffIds[0])->delete();
                    $insert[]=[
                        'shift_date'=>$s->time,
                        'shift_id'=>$shiftId,
                        'md_merchant_id'=>$merchantId,
                        'staff_id'=>$staffIds[0],
                        'staff_user_id'=>$staffIds[1],
                        'timezone'=>$request->_timezone,
                    ];
                }
            }

            ShiftRoaster::insert($insert);
            DB::commit();

            return response()->json([
                'message'=>'Data jadwal shift berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);
        }
    }



}

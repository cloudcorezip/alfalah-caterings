<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\HR;


use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\HR\JobEntity;
use Ramsey\Uuid\Uuid;

class JobController extends Controller
{

    public function routeWeb()
    {
        Route::prefix('hr/job')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.hr.job.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.hr.job.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.hr.job.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.hr.job.save');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.hr.job.delete');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Jabatan',
            'tableColumns'=>JobEntity::dataTableColumns(),
            'add'=>JobEntity::add(true),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'start_date'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'end_date'=>Carbon::now()->lastOfMonth()->toDateString(),

        ];

        return view('merchant::hr.job.index',$params);


    }

    public function dataTable(Request $request)
    {
        return JobEntity::dataTable($request);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=JobEntity::find($id);
        }else{
            $data=new JobEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('merchant::hr.job.form',$params);
    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            if(!is_null($id)){

                $data=JobEntity::find($id);
            }else{
                $data=new JobEntity();
                $data->md_merchant_id=merchant_id();
                $data->created_by=user_id();

            }
            $data->code=$request->code;
            $data->name=$request->name;
            $data->save();

            return response()->json([
                'message'=>'Data jabatan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=JobEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            return response()->json([
                'message'=>'Jabatan berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace Modules\Merchant\Http\Controllers\HR;


use App\Http\Controllers\Controller;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Entities\Asset\AssetEntity;
use Modules\Merchant\Entities\HR\DepartementEntity;
use Modules\Merchant\Entities\HR\JobEntity;
use Modules\Merchant\Entities\HR\ShiftRoasterChangeEntity;
use Modules\Merchant\Models\AttendanceSetting;
use Modules\Merchant\Models\ShiftRoaster;
use Modules\Merchant\Models\ShiftSetting;
use Ramsey\Uuid\Uuid;

class ShiftRoasterChangeController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('hr/shift-roaster-change')
            ->middleware('merchant-verification')
            ->group(function (){
                Route::get('/', [static::class, 'index'])
                    ->name('merchant.hr.shift-roaster-change.index');
                Route::post('/data-table', [static::class, 'dataTable'])
                    ->name('merchant.hr.shift-roaster-change.datatable');
                Route::post('/add', [static::class, 'add'])
                    ->name('merchant.hr.shift-roaster-change.add');
                Route::post('/save', [static::class, 'save'])
                    ->name('merchant.hr.shift-roaster-change.save');
                Route::post('/reload-data', [static::class, 'reloadData'])
                    ->name('merchant.hr.shift-roaster-change.reload-data');
                Route::post('/delete', [static::class, 'delete'])
                    ->name('merchant.hr.shift-roaster-change.delete');
                Route::post('/accept-or-reject', [static::class, 'acceptOrReject'])
                    ->name('merchant.hr.shift-roaster-change.accept-or-reject');
            });
    }


    public function index(Request $request)
    {
        parent::setMenuKey($request->key);
        $params=[
            'title'=>'Pergantian Shift',
            'tableColumns'=>ShiftRoasterChangeEntity::dataTableColumns(),
            'add'=>ShiftRoasterChangeEntity::add(true),
            'searchKey' => md5(Uuid::uuid4()->toString()),
            'startDate'=>Carbon::now()->subMonths(1)->startOfMonth()->toDateString(),
            'endDate'=>Carbon::now()->lastOfMonth()->toDateString(),
            'merchantId'=>[-1],
            'encodeMerchant'=>base64_encode(json_encode(MerchantUtil::getBranch(merchant_id())))
        ];

        return view('merchant::hr.shift-roaster-change.index',$params);


    }

    public function reloadData(Request $request)
    {
        $request['count_data'] = 0;
        $params = [
            'title' => 'Pergantian Shift',
            'tableColumns'=>ShiftRoasterChangeEntity::dataTableColumns(),
            'startDate'=>(is_null($request->startDate))?Carbon::now()->startOfMonth()->toDateString():$request->startDate,
            'endDate'=>(is_null($request->endDate))?Carbon::now()->endOfMonth()->toDateString():$request->endDate,
            'merchantId'=>$request->md_merchant_id,
            'encodeMerchant'=>base64_encode(json_encode($request->md_merchant_id))


        ];
        return view('merchant::hr.shift-roaster-change.list',$params);

    }

    public function dataTable(Request $request)
    {
        return ShiftRoasterChangeEntity::dataTable($request);
    }


    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=ShiftRoasterChangeEntity::find($id);
        }else{
            $data=new ShiftRoasterChangeEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
            'shift'=>ShiftSetting::whereIn('md_merchant_id',MerchantUtil::getBranch(merchant_id()))
                ->where('is_deleted',0)
                ->get(),
            'departement'=>DepartementEntity::listOption(),
        ];

        return view('merchant::hr.shift-roaster-change.form',$params);
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $id=$request->id;

            $checkMerchant=AttendanceSetting::whereIn('md_merchant_id',MerchantUtil::getBranch($request->md_merchant_id))
                ->first();
            if(is_null($checkMerchant))
            {
                $isCheck=false;
            }else{
                if($checkMerchant->is_allowed_shift_change==1)
                {
                    $isCheck=false;
                }else{
                    $isCheck=true;
                }
            }

            if($isCheck==true)
            {
                return response()->json([
                    'message'=>'Pergantian shift tidak diizinkan,silahkan update pengaturan presensi di menu pengaturan',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>true,

                ]);
            }

            if(!is_null($id)){

                $data=ShiftRoasterChangeEntity::find($id);
            }else{
                $data=new ShiftRoasterChangeEntity();
                $data->md_merchant_id=merchant_id();
                $data->created_by=user_id();

            }
            $staff=explode('_',$request->staff_id);
            if($request->to_staff_id!='-1'){
                $toStaff=explode('_',$request->to_staff_id);
                $data->to_staff_id=$toStaff[0];
                $data->to_staff_user_id=$toStaff[1];
                $to=$toStaff[0];
                $toU=$toStaff[1];
            }else{
                $to=null;
                $toU=null;
            }
            $data->shift_change_date=$request->shift_change_date;
            $data->staff_id=$staff[0];
            $data->staff_user_id=$staff[1];
            $data->approval_by=user_id();
            $data->approval_date=date('Y-m-d H:i:s');
            $data->is_accepted=1;
            $data->shift_id=$request->shift_id;
            $data->reason=$request->reason;
            $data->timezone=$request->_timezone;
            $data->save();

            $checkShift=ShiftRoaster::where('staff_id',$staff[0])
                ->where('shift_date',$data->shift_change_date)->first();

            if(is_null($checkShift))
            {
                return response()->json([
                    'message'=>'Karyawan ini belum memiliki shift pada hari '.$data->shift_change_date.',silahkan tambahkan shift terlebih dahulu',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>'',
                    'is_with_datatable'=>true,

                ]);

            }else{

                if($data->is_accepted==1)
                {
                    $data->original_shift_id=$checkShift->shift_id;
                    $data->save();
                    if(!is_null($to))
                    {
                        $checkShiftTo=ShiftRoaster::where('staff_id',$to)
                            ->where('shift_date',$data->shift_change_date)->first();

                        if(is_null($checkShiftTo))
                        {
                            $dataNew=new ShiftRoaster();
                            $dataNew->shift_date=$data->shift_change_date;
                            $dataNew->staff_id=$to;
                            $dataNew->staff_user_id=$toU;
                            $dataNew->md_merchant_id=$data->md_merchant_id;
                            $dataNew->shift_id=$checkShift->shift_id;
                            $dataNew->timezone=$request->_timezone;
                            $dataNew->save();
                        }else{
                            $data->to_original_shift_id=$checkShiftTo->shift_id;
                            $data->save();

                            $checkShiftTo->shift_id=$checkShift->shift_id;
                            $checkShiftTo->save();
                        }

                    }
                    $checkShift->shift_id=$data->shift_id;
                    $checkShift->save();
                }

            }

            DB::commit();

            return response()->json([
                'message'=>'pergantian shift berhasil disimpan',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }


    public function delete(Request $request)
    {
        try{

            DB::beginTransaction();
            $id=$request->id;
            $data=ShiftRoasterChangeEntity::find($id);
            $data->is_deleted=1;
            $data->save();

            ShiftRoaster::where('staff_id',$data->staff_id)
                ->where('shift_date',$data->shift_change_date)->update([
                    'shift_id'=>$data->original_shift_id
                ]);
            ShiftRoaster::where('staff_id',$data->to_staff_id)
                ->where('shift_date',$data->shift_change_date)->update([
                    'shift_id'=>$data->to_original_shift_id
                ]);
            DB::commit();
            return response()->json([
                'message'=>'pergantian shift berhasil dihapus',
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }

    public function acceptOrReject(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);

            DB::beginTransaction();
            $id=explode('_',$request->id);
            $data=ShiftRoasterChangeEntity::find($id[0]);

            if($id[2]=='approval'){

                if($id[1]==0 || $id[1]=='0')
                {
                    $checkMerchant=AttendanceSetting::whereIn('md_merchant_id',MerchantUtil::getBranch($data->md_merchant_id))
                        ->first();
                    if(is_null($checkMerchant))
                    {
                        $isCheck=false;
                    }else{
                        if($checkMerchant->is_allowed_shift_change==1)
                        {
                            $isCheck=false;
                        }else{
                            $isCheck=true;
                        }
                    }

                    if($isCheck==true)
                    {
                        return response()->json([
                            'message'=>'Pergantian shift tidak diizinkan,silahkan update pengaturan presensi di menu pengaturan',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>true,

                        ]);
                    }

                    $checkShift=ShiftRoaster::where('staff_id',$data->staff_id)
                        ->where('shift_date',$data->shift_change_date)->first();

                    if(is_null($checkShift))
                    {
                        return response()->json([
                            'message'=>'Karyawan ini belum memiliki shift pada hari '.$data->shift_change_date.',silahkan tambahkan shift terlebih dahulu',
                            'type'=>'warning',
                            'is_modal'=>true,
                            'redirect_url'=>'',
                            'is_with_datatable'=>true,

                        ]);

                    }else{

                        $data->original_shift_id=$checkShift->shift_id;
                        $data->save();
                        if(!is_null($data->to_staff_id))
                        {
                            $checkShiftTo=ShiftRoaster::where('staff_id',$data->to_staff_id)
                                ->where('shift_date',$data->shift_change_date)->first();

                            if(is_null($checkShiftTo))
                            {
                                $dataNew=new ShiftRoaster();
                                $dataNew->shift_date=$data->shift_change_date;
                                $dataNew->staff_id=$data->to_user_id;
                                $dataNew->staff_user_id=$data->to_staff_user_id;
                                $dataNew->md_merchant_id=$data->md_merchant_id;
                                $dataNew->shift_id=$checkShift->shift_id;
                                $dataNew->timezone=$request->_timezone;
                                $dataNew->save();
                            }else{
                                $data->to_original_shift_id=$checkShiftTo->shift_id;
                                $data->save();

                                $checkShiftTo->shift_id=$checkShift->shift_id;
                                $checkShiftTo->save();
                            }

                        }
                        $checkShift->shift_id=$data->shift_id;
                        $checkShift->save();

                        $data->approval_by=user_id();
                        $data->approval_date=date('Y-m-d H:i:s');
                        $data->is_accepted=1;
                        $data->save();
                    }

                    $message='Berhasil menyetujui pergantian shift';

                }else{
                    $data->approval_by=null;
                    $data->approval_date=null;
                    $data->is_accepted=0;
                    $data->save();

                    ShiftRoaster::where('staff_id',$data->staff_id)
                        ->where('shift_date',$data->shift_change_date)->update([
                            'shift_id'=>$data->original_shift_id
                        ]);
                    ShiftRoaster::where('staff_id',$data->to_staff_id)
                        ->where('shift_date',$data->shift_change_date)->update([
                            'shift_id'=>$data->to_original_shift_id
                        ]);
                    $message='Berhasil membatalkan persetujuan pergantian shift';

                }

            }

            if($id[2]=='reject')
            {
                if($id[1]==0 || $id[1]=='0')
                {

                    $data->is_accepted=2;
                    $data->approval_by=user_id();
                    $data->save();

                    $message='Berhasil menyetujui penolakan pergantian shift';

                }else{
                    $data->is_accepted=0;
                    $data->approval_by=null;
                    $data->save();
                    $message='Berhasil membatalkan persetujuan penolakan pergantian shift';

                }
            }




            DB::commit();
            return response()->json([
                'message'=>$message,
                'type'=>'success',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>true,

            ]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>'',
                'is_with_datatable'=>false,

            ]);


        }
    }



}

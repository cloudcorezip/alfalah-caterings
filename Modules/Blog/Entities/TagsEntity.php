<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Blog\Entities;


use App\Classes\Singleton\DataTable;
use App\Models\Blog\Tags;
use Illuminate\Support\Facades\DB;

class TagsEntity extends Tags
{
    public static function getDataForDataTable()
    {

        try {
            return Tags::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY id)'),
                'id',
                'name'
            ]);

        }catch (\Exception $e)
        {
            
            return [];

        }

    }

    public static function dataTable($request)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForDataTable(),$request,['opsi'],
            ['name'])
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'id',
            'name'
        ];
    }



    public static function getButton($list){
        return "                            <a  onclick='loadModal(this)' target='" . route('blog.tags.add', ['id' => $list->id]) . "' class='btn btn-xs btn-primary btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-danger btn-rounded'>
                                                <span class='fa fa-trash' style='color: white'></span>
                                            </a>

                                          ";
    }
}

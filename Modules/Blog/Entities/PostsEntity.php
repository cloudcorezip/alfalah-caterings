<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Blog\Entities;


use App\Classes\Singleton\DataTable;
use App\Models\Blog\Posts;
use Illuminate\Support\Facades\DB;

class PostsEntity extends Posts
{
    public static function getDataForDataTable()
    {

        try {
            return Posts::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY id)'),
                'id',
                'title',
                'image_preview',
                'is_publish'
            ]);

        }catch (\Exception $e)
        {

            return [];

        }

    }

    public static function dataTable($request)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForDataTable(),$request,['opsi','image_preview','is_publish'])
            ->editColumn('image_preview',function ($list){
                if(is_null($list->image_preview)){
                    return '<img src="'.env('S3_URL').'public/backend-v2/img/no-product-image.png" height="100px" width="100px">';

                }else{
                    return '<img src="'.env('S3_URL').$list->image_preview.'" height="100px" width="100px">';
                }
            })
            ->editColumn('is_publish',function ($list){
                if($list->is_publish==0){
                    return "<div class='text-center py-5'><a  href='".route('blog.posts.publish', ['id' => $list->id])."' class='btn btn-sm btn-success btn-rounded mr-1' title='Publish'>
                            <span class='fa fa-lg fa-bullhorn' style='color: white'></span>
                            </a></div>";

                }else{
                    return "<div class='text-center py-5'><a  href='".route('blog.posts.unpublish', ['id' => $list->id])."' class='btn btn-sm btn-danger btn-rounded mr-1' title='Unpublish'>
                            <span class='fa fa-lg fa-bullhorn' style='color: white'></span>
                            </a></div>";
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'id',
            'title',
            'image_preview',
            'is_publish'
        ];
    }



    public static function getButton($list){
        return "
                                            <div class='d-flex'>
                                            <a   href='" . route('blog.posts.add', ['id' => $list->id]) . "' class='btn btn-sm btn-primary btn-rounded mr-1' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>


                                            <a onclick='deleteData($list->id)' class='btn btn-sm btn-danger btn-rounded mr-1'>
                                                <span class='fa fa-trash' style='color: white'></span>
                                            </a>
                                            </div>
                                          ";
    }
}

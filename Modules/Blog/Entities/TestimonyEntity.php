<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Blog\Entities;


use App\Classes\Singleton\DataTable;
use App\Models\Blog\Testimony;
use Illuminate\Support\Facades\DB;

class TestimonyEntity extends Testimony
{

    public static function getDataForDataTable()
    {

        try {
            return Testimony::
            select([
                DB::raw('ROW_NUMBER () OVER (ORDER BY md_testimony.id)'),
                'md_testimony.id',
                'owner_name',
                'url',
                'm.name as merchant_name',
                'message'
            ])
            ->join('md_merchants as m','m.id','md_testimony.md_merchant_id')
            ;

        }catch (\Exception $e)
        {
            return [];

        }

    }

    public static function dataTable($request)
    {
        return(DataTable::getInstance())->dataTable(self::class,self::getDataForDataTable(),$request,['opsi','url'],
            ['merchant_name'])
            ->editColumn('url',function ($list){
                if(is_null($list->url)){
                    return '<img src="'.asset('public/backend-v2/img/no-product-image.png').'" height="100px" width="100px">';

                }else{
                    return '<img src="'.asset($list->url).'" height="100px" width="100px">';
                }
            })
            ->make(true);
    }


    public static function dataTableColumns()
    {
        return [
            'row_number',
            'opsi',
            'owner_name',
            'foto',
            'merchant_name',
            'message'
        ];
    }

    public static function getButton($list){
        return  "<a  onclick='loadModal(this)' target='".route('blog.testimony.add',['id'=>$list->id])."' class='btn btn-xs btn-primary btn-rounded' title='Lihat Detail'>
                                                <span class='fa fa-edit' style='color: white'></span>
                                            </a>

                                            <a onclick='deleteData($list->id)' class='btn btn-xs btn-danger btn-rounded'>
                                                <span class='fa fa-trash' style='color: white'></span>
                                            </a>
                                          ";
    }


}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(env('APP_DOMAIN')==false)
{
    Route::prefix(config('module_domain.blog_domain'))->group(function() {
        $directory = DIRECTORY_SEPARATOR;
        \App\Classes\RouteConfig::getRouteFromModuleController('Blog',$directory.'Http'.$directory.'Controllers'.$directory);
    });
}else{

    Route::domain(config('module_domain.blog_domain'))->group(function () {
        $directory = DIRECTORY_SEPARATOR;
        \App\Classes\RouteConfig::getRouteFromModuleController('Blog',$directory.'Http'.$directory.'Controllers'.$directory);
    });
}

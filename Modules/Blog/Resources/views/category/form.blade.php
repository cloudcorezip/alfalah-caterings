<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1">Nama Kategori</label>
        <input type="name" class="form-control form-control-sm" name="name" placeholder="Nama Kategori" value="{{$data->name}}" required>
    </div>

    <div class="modal-footer">
        <button class="btn btn-sm btn-primary">Simpan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('blog.category.save')}}", data, '#result-form-konten');
        })
    })


</script>


<style>
    .modal{
        display: block !important; /* I added this to see the modal, you don't need this */
    }

    /* Important part */
    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>

<form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

    <div class="form-group">
        <label for="exampleInputPassword1">Nama Owner</label>
        <input type="text" class="form-control form-control-sm" name="owner_name" placeholder="owner name" value="{{$data->owner_name}}" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Foto</label>
        <input type="file" class="form-control form-control-sm" name="foto" @if ($data->id==null) required @endif >
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Merchant</label>
        <select id="inputState" class="form-control form-control-sm" name="md_merchant_id">
            <option disabled @if ($data->md_merchant_id==null)
                selected
            @endif >--Pilih Merchant--</option>
            @foreach ($merchant as $b)
            <option @if ($b->id==$data->md_merchant_id)
                selected
            @endif value="{{$b->id}}">{{$b->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Message</label>
        <input type="text" class="form-control form-control-sm" name="message" placeholder="Message" value="{{$data->message}}" required>
    </div>

    <div class="modal-footer">
        <button class="btn btn-sm btn-primary">Simpan</button>
        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Tutup</button>
    </div>
    <input type='hidden' name='id' value='{{$data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer("{{route('blog.testimony.save')}}", data, '#result-form-konten');
        })
    })


</script>





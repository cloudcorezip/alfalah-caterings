@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->
    <div class="page-header">
        <div class="container-fluid d-sm-flex justify-content-between">
            <h4>{{$title}}</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{route(''.dashboard_url().'')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
                </ol>
            </nav>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">{{$title}}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="row" style="margin-bottom: 10px">
                        <div class="col-lg-2">
                            <button class="btn btn-success btn-rounded btn-sm btn-block" onclick="loadModal(this)" target="{{route('blog.testimony.add')}}"><i class="fa fa-plus"></i> Tambah</button>
                        </div>

                    </div>
                    <table class="table table-bordered" id="table-data-testimony" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            @foreach($tableColumns as $key =>$item)
                                <th>{{($tableColumns[$key]=="row_number")?"No":str_replace('_',' ',strtoupper($tableColumns[$key]))}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function deleteData(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Konfirmasi", "Apa anda yakin menghapus data ?", function () {
                ajaxTransfer("{{route('blog.testimony.delete')}}", data, "#modal-output");
            })
        }
        $(document).ready(function() {

            ajaxDataTable('#table-data-testimony', 1, "{{route('blog.testimony.datatable')}}", [
                    @foreach($tableColumns as $key =>$item)
                    @if($tableColumns[$key]=='action')
                {
                    data: '{{$tableColumns[$key]}}',
                    name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}'
                },
                @endif
                @endforeach
            ]);
        })
    </script>
@endsection

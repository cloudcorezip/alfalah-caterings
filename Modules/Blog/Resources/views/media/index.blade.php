
@extends('backend-v2.layout.main')
@section('title',$title)
@section('content')
    <!-- Page Heading -->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-danger">{{$title}}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="table-data-media" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        @foreach($tableColumns as $key =>$item)
                            <th>{{($tableColumns[$key]=="row_number")?"No":str_replace('_',' ',strtoupper($tableColumns[$key]))}}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            ajaxDataTable('#table-data-media', 1, "{{route('blog.media.datatable')}}", [
                    @foreach($tableColumns as $key =>$item)
                    @if($tableColumns[$key]=='action')
                {
                    data: '{{$tableColumns[$key]}}',
                    name: '{{$tableColumns[$key]}}',
                    orderable: false,
                    searchable: false
                },
                    @else
                {
                    data: '{{$tableColumns[$key]}}', name: '{{$tableColumns[$key]}}'
                },
                @endif
                @endforeach
            ]);
        })
    </script>
@endsection


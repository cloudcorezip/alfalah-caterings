@extends('backend-v3.layout.main')
@section('title',$title)
@section('content')

<div class="page-header">
    <div class="container-fluid d-sm-flex justify-content-between">
        <h4>{{$title}}</h4>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{route('blog.dashboard.posts')}}">Posts</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
            </ol>
        </nav>
    </div>
</div>

<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">{{$title}}</h6>
        </div>
        <div class="card-body">
        <div id="result-form-konten"></div>
            <form onsubmit="return false;" id="form-konten" class='form-horizontal' backdrop="">

                <div class="form-group">
                    <label >Title</label>
                    <input type="text" class="form-control form-control-sm" name="title" placeholder="Title" value="{{$data->title}}" required>
                </div>
                <div class="form-group">
                    <label >Content</label>
                    <textarea class="form-control form-control-sm" id="konten"  cols="30" rows="10" required>
                        {!! $data->content !!}
                    </textarea>
                    <textarea type="hidden" id="konten-replace" style="display: none"></textarea>
                </div>
                <div class="form-group ">
                    <label for="tags">Tags :</label>
                        @foreach ($tags as $t)
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" name="tags[]" value="{{$t->slug}}"@foreach ($tagsCheck as $c) @if ($t->slug==$c) checked @endif @endforeach>
                                    <label class="form-check-label">{{$t->name}}</label>
                                </div>
                        @endforeach
                </div>
                <div class="form-group">
                    <label >Image</label>
                    <input type="file" class="form-control-file" name="image" placeholder="Image"  @if(is_null($data->id))required @endif>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Category</label>
                    <select class="custom-select" name="category">
                    <option selected disabled>-- Pilih Kategori --</option>
                    @foreach ($category as $p)
                    <option value="{{$p->id}}" @if($p->id==$data->md_bg_category_id) selected @endif>{{$p->name}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-primary">Simpan</button>
                <a href="{{route('blog.dashboard.posts')}}" class="btn btn-sm btn-danger">Kembali</a>
                </div>
                <input type='hidden' name='id' value='{{$data->id }}'>
                <input type='hidden' name='_token' value='{{ csrf_token() }}'>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var editor = CKEDITOR.replace( 'konten' );
        editor.on( 'change', function( evt ) {
            $("#konten-replace").val(evt.editor.getData())
        });
    })
    $('#form-konten').submit(function () {
        var data = getFormData('form-konten');
        data.append('contents',$("#konten-replace").val());
        ajaxTransfer("{{route('blog.posts.save')}}", data, '#result-form-konten');
    })
</script>
@endsection

<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Modules\Blog\Entities\TagsEntity;
use Modules\Blog\Entities\PostsEntity;
use Illuminate\Support\Facades\Validator;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function routeWeb()
    {
        Route::prefix('/dashboard')
            ->middleware('blog-verification')
            ->group(function (){
                Route::get('/tags','TagsController@index')
                    ->name('blog.dashboard.tags');
                Route::post('/tags/data-table', 'TagsController@dataTable')
                    ->name('blog.tags.datatable');
                Route::post('/tags/add', 'TagsController@add')
                    ->name('blog.tags.add');
                Route::post('/tags/save', 'TagsController@save')
                    ->name('blog.tags.save');
                Route::post('/tags/delete', 'TagsController@delete')
                    ->name('blog.tags.delete');
            });
    }

    public function index()
    {
        $params=[
            'title'=>'Tags',
            'tableColumns'=>TagsEntity::dataTableColumns()

        ];

        return view('blog::tags.index',$params);
    }

    public function dataTable(Request $request)
    {
        return TagsEntity::dataTable($request);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=TagsEntity::find($id);

        }else{
            $data=new TagsEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('blog::tags.form',$params);
    }

    public function save(Request $request)
    {
        try{
            $id=$request->id;
            if(!is_null($id)){

                $data=TagsEntity::find($id);
            }else{
                $data=new TagsEntity();
            }

            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return "<div class='alert alert-warning' style='text-align: center'>$error</div> <script>
           reload(1500);";
            }

            $data->name=$request->name;
            $data->slug=Str::slug($request->name);
            $data->save();

            return "<div class='alert alert-success' style='text-align: center'>Data berhasil disimpan!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=TagsEntity::where('id',$id)->delete();
            return "<div class='alert alert-success' style='text-align: center'>Data berhasil dihapus!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal dihapus!</div>";


        }
    }

}

<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Modules\Blog\Entities\PostsEntity;
use Modules\Blog\Entities\CategoriesEntity;
use Modules\Blog\Entities\TagsEntity;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function routeWeb()
    {
        Route::get('/', 'BlogController@index')->name('blog');
        Route::get('/detail/{year}/{month}/{day}/{slug}', 'BlogController@indexDetail')->name('blog.detail');
        Route::get('/search', 'BlogController@search')->name('blog.search');
        Route::get('/category/{slug}', 'BlogController@show')->name('blog.show');
        Route::get('/tags/{slug}', 'BlogController@showByTags')->name('blog.showByTags');

        Route::get('/dashboard','BlogController@dashboard')
            ->name('blog.dashboard')
            ->middleware('blog-verification');
    }

    public function index()
    {
        $data=PostsEntity::where('is_publish',1)
                ->whereNotIn('md_bg_category_id', [8,9,10])  
                ->orderBy('created_at', 'DESC')
                ->paginate(6);
        $content1=PostsEntity::where('is_publish', 1)
            ->whereNotIn('md_bg_category_id', [8,9,10])      
            ->orderBy('created_at', 'DESC')
            ->limit(6)
            ->get();
        $category=CategoriesEntity::whereNotIn('id', [8,9,10])
                    ->limit(5)
                    ->get();

        $params=[
            'title'=>'Blog',
            'data'=>$data,
            'content1'=>$content1,
            'category'=>$category,
            'tags'=>TagsEntity::all(),
        ];
        return view('blog::user.index',$params);
    }

    public function indexDetail($year,$month,$day,$slug)
    {
        $date=implode("-",[$year,$month,$day]);
        $data=PostsEntity::where('slug',$slug)->whereDate('created_at',$date)
        ->first();
        $data->increment('views', 1);
        $category=CategoriesEntity::whereNotIn('id', [8,9,10])
                ->limit(5)
                ->get();
        $content=PostsEntity::orderBy('created_at', 'DESC')
            ->where('is_publish',1)
            ->whereNotIn('md_bg_category_id', [8,9,10])  
            ->take(6)->get();
        $params=[
            'title'=>$data->title,
            'data'=>$data,
            'content'=>$content,
            'category'=>$category,
            'tags'=>TagsEntity::all()
        ];
        return view('blog::user.index-detail',$params);
    }

    public function dashboard()
    {
        $params=[
            'title'=>'Senna | Dashboard'
        ];
        return view('blog::dashboard',$params);
    }


    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('blog::create');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function show($slug)
    {
        $categoryItem=CategoriesEntity::where('slug',$slug)->first();
        $data = PostsEntity::where('md_bg_category_id',$categoryItem->id)
                    ->where('is_publish',1)
                    ->orderBy('created_at', 'DESC')
                    ->paginate(6);
        $content1=PostsEntity::orderBy('created_at', 'DESC')
            ->where('is_publish',1)
            ->whereNotIn('md_bg_category_id', [8,9,10])  
            ->limit(6)
            ->get();
        $category=CategoriesEntity::whereNotIn('id', [8,9,10])
        ->limit(5)
        ->get();
        $params=[
            'title'=>'Posting Dengan Kategori : '.$categoryItem->name,
            'data'=>$data,
            'content1'=>$content1,
            'category'=>$category,
            'categoryItem'=>$categoryItem,
            'tags'=>TagsEntity::all(),
            'slug' => $slug
        ];
        return view('blog::user.show',$params);
    }

    public function showByTags($slug)
    {
        $tags=TagsEntity::where('slug',$slug)->first();
        $data = PostsEntity::where('keyword','like', '%' . $slug . '%')
                    ->whereNotIn('md_bg_category_id', [8,9,10])
                    ->orderBy('created_at', 'DESC')
                    ->paginate(6);
        $content1=PostsEntity::orderBy('created_at', 'DESC')
            ->where('is_publish',1)
            ->whereNotIn('md_bg_category_id', [8,9,10])  
            ->limit(6)
            ->get();
        $category=CategoriesEntity::whereNotIn('id', [8,9,10])
        ->limit(5)
        ->get();
        $params=[
            'title'=>'Posting Dengan Tag : '.$tags->name,
            'data'=>$data,
            'content1'=>$content1,
            'category'=>$category,
            'categoryItem'=>$tags,
            'tags'=>TagsEntity::all(),
            'slug' => $slug
        ];
        return view('blog::user.show',$params);
    }

    public function search(Request $request)
    {
        $search = $request->keyword;
        $data = PostsEntity::where(function($query) use($search){
                    $query->where('title', 'ilike', '%' . $search . '%')
                    ->orWhere('content', 'ilike', '%' . $search . '%')
                    ->orWhere('keyword', 'ilike', '%' . $search . '%');
                })
                ->whereNotIn('md_bg_category_id', [8,9,10])
                ->orderBy('created_at', 'DESC')
                ->paginate(6);
        $contentA=PostsEntity::whereNotIn('md_bg_category_id', [8,9,10])->take(1);
        $content1=PostsEntity::orderBy('created_at', 'DESC')
            ->where('is_publish',1)
            ->whereNotIn('md_bg_category_id', [8,9,10])  
            ->limit(6)
            ->get();
        $category=CategoriesEntity::whereNotIn('id', [8,9,10])
        ->limit(5)
        ->get();
        $params=[
            'title'=>'Pencarian Dengan Kata Kunci : '.$request->keyword,
            'data'=>$data,
            'content1'=>$content1,
            'category'=>$category,
            'tags'=>TagsEntity::all(),
            'keyword'=>$request->keyword
        ];

        return view('blog::user.index',$params);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('blog::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

}

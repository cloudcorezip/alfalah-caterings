<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Modules\Blog\Entities\PostsEntity;
use Modules\Blog\Entities\CategoriesEntity;
use Modules\Blog\Entities\TagsEntity;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Image;
use Carbon\Carbon;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function routeWeb()
    {
        Route::prefix('/dashboard')
            ->middleware('blog-verification')
            ->group(function (){
                Route::get('/posts','PostsController@index')
                    ->name('blog.dashboard.posts');
                Route::post('/posts/data-table', 'PostsController@dataTable')
                    ->name('blog.posts.datatable');
                Route::get('/posts/add', 'PostsController@add')
                    ->name('blog.posts.add');
                Route::post('/posts/save', 'PostsController@save')
                    ->name('blog.posts.save');
                Route::post('/posts/delete', 'PostsController@delete')
                    ->name('blog.posts.delete');
                Route::get('/posts/publish/{id}', 'PostsController@publish')
                    ->name('blog.posts.publish');
                Route::get('/posts/unpublish/{id}', 'PostsController@unPublish')
                    ->name('blog.posts.unpublish');
            });
    }




    public function index()
    {
        $params=[
            'title'=>'Posts',
            'tableColumns'=>PostsEntity::dataTableColumns()

        ];

        return view('blog::posts.index',$params);
    }

    public function dataTable(Request $request)
    {
        return PostsEntity::dataTable($request);
    }
    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('blog::show');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=PostsEntity::find($id);
            $category=CategoriesEntity::all();
            $tags=TagsEntity::all();
            $tagsCheck=explode(',',$data->keyword);
        }else{
            $data=new PostsEntity();
            $category=CategoriesEntity::all();
            $tags=TagsEntity::all();
            $tagsCheck=[];
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Postingan':'Edit Postingan',
            'data'=>$data,'category'=>$category,'tags'=>$tags,'tagsCheck'=>$tagsCheck
        ];

        return view('blog::posts.form',$params);
    }

    public function save(Request $request)
    {
        try{
            $id=$request->id;
            if(!is_null($id)){

                $data=PostsEntity::find($id);

            }else{
                $data=new PostsEntity();
            }


            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return "<div class='alert alert-warning' style='text-align: center'>$error</div> <script>
           reload(1500);";
            }

            $destinationPath = 'public/uploads/blog/'.user_id().'/image/';

            if($request->hasFile('image'))
            {
                $file=$request->file('image');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('image'))->resize(800,300)->encode($file->getClientOriginalExtension());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, (string) $image);


            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->image_preview;

                }
            }

            $t=array_count_values($request->tags);
            if($t>0){
                $tags=implode(",",$request->tags);
            }else{
                $tags=implode($request->tags);
            }
            //dd($request->all());

            $data->title=$request->title;
            $data->content=$request->contents;
            $data->md_bg_category_id=$request->category;
            $data->md_user_id=21;
            $data->keyword=$tags;
            $data->image_preview=$fileName;

            $data->is_publish=0;
            $data->slug=Str::slug($request->title);
            $data->save();

            return "<div class='alert alert-success' style='text-align: center'>Data berhasil disimpan!</div>
            <script>
            (function () {
                setTimeout(function () {
                    history.back();
                }, 1000);
            })();
            </script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            //$data=PostsEntity::where('id',$id)->first();
            //File::delete(public_path('blog/'.$data->image_preview));
            PostsEntity::where('id',$id)->delete();
            return "<div class='alert alert-success' style='text-align: center'>Data berhasil dihapus!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal dihapus!</div>";


        }
    }

    public function publish($id){

        $dateNow=Carbon::now();

        PostsEntity::where('id',$id)->update(['is_publish' => 1,'created_at' => $dateNow]);

        return redirect()->route('blog.dashboard.posts');
    }

    public function unPublish($id){

        PostsEntity::where('id',$id)->update(['is_publish' => 0]);

        return redirect()->route('blog.dashboard.posts');
    }
}

<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use Modules\Blog\Entities\MediasEntity;

class MediasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function routeWeb()
    {
        Route::prefix('/dashboard')
            ->middleware('blog-verification')
            ->group(function (){
                Route::get('/media','MediasController@index')
                    ->name('blog.dashboard.media');
                Route::post('/media/data-table', 'MediasController@dataTable')
                    ->name('blog.media.datatable');
                Route::post('/media/add', 'MediasController@add')
                    ->name('blog.media.add');
                Route::post('/media/save', 'MediasController@save')
                    ->name('blog.media.save');
                Route::post('/media/delete', 'MediasController@delete')
                    ->name('blog.media.delete');
            });
    }

    

    
    public function index()
    {
        $params=[
            'title'=>'Media',
            'tableColumns'=>MediasEntity::dataTableColumns()

        ];

        return view('blog::media.index',$params);
    }

    public function dataTable(Request $request)
    {
        return MediasEntity::dataTable($request);
    }
    
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=MediasEntity::find($id);
            
        }else{
            $data=new MediasEntity();
     
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('blog::media.form',$params);
    }

    public function save(Request $request)
    {
        try{
            $id=$request->id;
            if(!is_null($id)){

                $data=MediasEntity::find($id);
            }else{
                $data=new MediasEntity();
            }

            $data->mediable_id=$request->mediable_id;
            $data->mediable_type=$request->mediable_type;
            $data->file=$request->file;
            $data->save();

            return "<div class='alert alert-success' style='text-align: center'>Data berhasil disimpan!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=MediasEntity::where('id',$id)->delete();
            return "<div class='alert alert-success' style='text-align: center'>Data berhasil dihapus!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal dihapus!</div>";


        }
    }

}

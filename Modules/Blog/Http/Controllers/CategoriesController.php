<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Modules\Blog\Entities\CategoriesEntity;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function routeWeb()
    {
        Route::prefix('/dashboard')
            ->middleware('blog-verification')
            ->group(function (){
                Route::get('/category','CategoriesController@index')
                    ->name('blog.dashboard.category');
                Route::post('/category/data-table', 'CategoriesController@dataTable')
                    ->name('blog.category.datatable');
                Route::post('/category/add', 'CategoriesController@add')
                    ->name('blog.category.add');
                Route::post('/category/save', 'CategoriesController@save')
                    ->name('blog.category.save');
                Route::post('/category/delete', 'CategoriesController@delete')
                    ->name('blog.category.delete');
            });
    }

    public function index()
    {
        $params=[
            'title'=>'Kategori',
            'tableColumns'=>CategoriesEntity::dataTableColumns()

        ];

        return view('blog::category.index',$params);
    }

    public function dataTable(Request $request)
    {
        return CategoriesEntity::dataTable($request);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=CategoriesEntity::find($id);
        }else{
            $data=new CategoriesEntity();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,
        ];

        return view('blog::category.form',$params);
    }

    public function save(Request $request)
    {
        try{
            $id=$request->id;
            if(!is_null($id)){

                $data=CategoriesEntity::find($id);
            }else{
                $data=new CategoriesEntity();
            }

            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return "<div class='alert alert-warning' style='text-align: center'>$error</div> <script>
           reload(1500);";
            }

            $data->name=$request->name;
            $data->slug=Str::slug($request->name);
            $data->save();

            return "<div class='alert alert-success' style='text-align: center'>Data berhasil disimpan!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            $data=CategoriesEntity::where('id',$id)->delete();
            return "<div class='alert alert-success' style='text-align: center'>Data berhasil dihapus!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal dihapus!</div>";


        }
    }

}

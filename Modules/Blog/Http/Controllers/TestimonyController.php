<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace Modules\Blog\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Blog\Entities\TestimonyEntity;
use App\Models\MasterData\Merchant;
use Image;

class TestimonyController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('blog/testimony')
            ->middleware('blog-verification')
            ->group(function (){
                Route::get('/', 'TestimonyController@index')
                    ->name('blog.testimony.index');
                Route::post('/data-table', 'TestimonyController@dataTable')
                    ->name('blog.testimony.datatable');
                Route::post('/add', 'TestimonyController@add')
                    ->name('blog.testimony.add');
                Route::post('/save', 'TestimonyController@save')
                    ->name('blog.testimony.save');
                Route::post('/delete', 'TestimonyController@delete')
                    ->name('blog.testimony.delete');
            });
    }


    public function index()
    {
        $params=[
            'title'=>'Testimoni',
            'tableColumns'=>TestimonyEntity::dataTableColumns()

        ];

        return view('blog::testimony.index',$params);

    }

    public function dataTable(Request $request)
    {
        return TestimonyEntity::dataTable($request);
    }
    public  function add(Request $request){

        $id=$request->id;
        if(!is_null($id)){

            $data=TestimonyEntity::find($id);
            $merchant=Merchant::all();
        }else{
            $data=new TestimonyEntity();
            $merchant=Merchant::all();
        }
        $params=[
            'title'=>(is_null($id))?'Tambah Data':'Edit Data',
            'data'=>$data,'merchant'=>$merchant
        ];

        return view('blog::testimony.form',$params);
    }

    public function save(Request $request)
    {
        try{
            $id=$request->id;
            if(!is_null($id)){

                $data=TestimonyEntity::find($id);
            }else{
                $data=new TestimonyEntity();
            }
            $destinationPath = 'public/uploads/bank/'.user_id().'/image/';
            if(!file_exists($destinationPath)){
                mkdir($destinationPath,0777,true);
            }

            if($request->hasFile('url'))
            {
                $file=$request->file('url');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $resizeImage = Image::make($file->getRealPath());

                $resizeImage->resize(800, 300, function($constraint){
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$fileName);
                //$file->move($destinationPath, $fileName);
                $fileName=$destinationPath.$fileName;
            }else{
                if(is_null($id)){
                    $fileName=null;

                }else{
                    $fileName=$data->url;

                }
            }
            
            $data->owner_name=$request->owner_name;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->message=$request->message;
            $data->foto=$fileName;
            $data->save();

            return "<div class='alert alert-success' style='text-align: center'>Data berhasil disimpan!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }


    public function delete(Request $request)
    {
        try{
            $id=$request->id;
            TestimonyEntity::where('id',$id)->delete();
            return "<div class='alert alert-success' style='text-align: center'>Data berhasil dihapus!</div> <script>
           reload(1500);
</script>";
        }catch (\Exception $e)
        {
            
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal dihapus!</div>";


        }
    }

}

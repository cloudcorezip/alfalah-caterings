<?php

use App\Models\MasterData\MerchantBranch;
use App\Models\MasterData\MerchantMenu;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

/**
 * Senna Apps
 * Copyright (c) 2020.
 */


function convertToRomawi($number)
{
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}
function custom_encrypt($string, $key=5) {
    $result = '';
    for($i=0, $k= strlen($string); $i<$k; $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key))-1, 1);
        $char = chr(ord($char)+ord($keychar));
        $result .= $char;
    }
    return base64_encode($result);
}

function custom_decrypt($string, $key=5) {
    $result = '';
    $string = base64_decode($string);
    for($i=0,$k=strlen($string); $i< $k ; $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key))-1, 1);
        $char = chr(ord($char)-ord($keychar));
        $result.=$char;
    }
    return $result;
}

function star_va_code($number)
{
    $length=strlen($number);
    $middle_string ="";
    if( $length < 3 ){

        return $length == 1 ? "*" : "*". substr($number,  - 1);

    }
    else{
        $part_size = floor( $length / 3 ) ;
        $middle_part_size = $length - ( $part_size * 2 );
        for( $i=0; $i < $middle_part_size ; $i ++ ){
            $middle_string .= "*";
        }

        return substr($number, 0, $part_size ) . $middle_string  . substr($number,  - $part_size );
    }

}

function dashboard_url()
{

    try{
        $cookieRole=decrypt(\Illuminate\Support\Facades\Session::get('role_id'));
        if(!is_null($cookieRole))
        {
            if($cookieRole==5 || $cookieRole==6)
            {
                $route='admin.dashboard';
            }elseif ($cookieRole==2 || $cookieRole== 3 || $cookieRole==4
                ||
                $cookieRole==11 ||
                $cookieRole==12
                || $cookieRole==13
                || $cookieRole==14
                || $cookieRole==15
                || $cookieRole==16
                || $cookieRole==17
                || $cookieRole==18
                || $cookieRole==19
                || $cookieRole==20

            )
            {
                $route='merchant.dashboard';
            }
            else{
                $route='pages.not_allowed';
            }
        }else{
            $route='pages.not_allowed';
        }
    }catch (\Exception $e)
    {
        $route='pages.not_allowed';

    }
    return $route;
}

function user()
{
    try{
        $session=\Illuminate\Support\Facades\Session::get('user_data');
        if(is_null($session))
        {
            $data=\App\Models\MasterData\User::where('id',decrypt(\Illuminate\Support\Facades\Session::get(config('user.user'))))->first();
            \Illuminate\Support\Facades\Session::put('user_data',$data);

        }else{
            $data=$session;
        }
        return $data;
    }catch (\Exception $re)
    {
        null;
    }
}

function user_id()
{
    try {
        $switchBranch=\Illuminate\Support\Facades\Session::get(config('user.switch_branch'));
        if(user()->is_staff==0)
        {
            if(is_null($switchBranch))
            {

                return decrypt(\Illuminate\Support\Facades\Session::get(config('user.user')));

            }elseif ($switchBranch==1)
            {
                return decrypt(\Illuminate\Support\Facades\Session::get(config('user.switch_branch_user_id')));
            }elseif ($switchBranch==0)
            {
                return decrypt(\Illuminate\Support\Facades\Session::get(config('user.user')));

            }else{
                return decrypt(\Illuminate\Support\Facades\Session::get(config('user.user')));

            }

        }else{
            if(is_null($switchBranch))
            {
                return user()->getStaffMerchant->getMerchant->md_user_id;
            }elseif ($switchBranch==1)
            {
                return decrypt(\Illuminate\Support\Facades\Session::get(config('user.switch_branch_user_id')));
            }elseif ($switchBranch==0)
            {
                return user()->getStaffMerchant->getMerchant->md_user_id;
            }else{
                return user()->getStaffMerchant->getMerchant->md_user_id;
            }
        }

    }catch (\Exception $e)
    {
        return null;
    }

}

function merchant_id()
{
    try {
        $switchBranch=\Illuminate\Support\Facades\Session::get(config('user.switch_branch'));
        if(is_null($switchBranch))
        {
            return decrypt(\Illuminate\Support\Facades\Session::get(config('user.merchant')));
        }elseif ($switchBranch==1)
        {
            return decrypt(\Illuminate\Support\Facades\Session::get(config('user.switch_branch_id')));
        }elseif ($switchBranch==0)
        {
            return decrypt(\Illuminate\Support\Facades\Session::get(config('user.merchant')));

        }else{
            return decrypt(\Illuminate\Support\Facades\Session::get(config('user.merchant')));

        }

    }catch (\Exception $e)
    {
        return null;
    }

}


function merchant_detail()
{

    $data=\App\Models\MasterData\Merchant::find(merchant_id());

    return $data;
}


function merchant_detail_multi_branch($merchantId)
{

    $data=\App\Models\MasterData\Merchant::find($merchantId);

    return $data;
}

function get_role()
{
    try{
        return decrypt(\Illuminate\Support\Facades\Session::get(config('user.role')));
    }catch (\Exception $e)
    {
        return null;
    }
}

function get_cabang()
{
    try{

        $merchant_id=decrypt(\Illuminate\Support\Facades\Session::get(config('user.merchant')));

        return collect(\App\Utils\Merchant\MerchantUtil::getBranchList($merchant_id,0));
    }catch (\Exception $e)
    {
        return [];
    }
}

function convertMonth($month)
{
    $array=[
        'January'=>1,
        'February'=>2,
        'March'=>3,
        'April'=>4,
        'May'=>5,
        'June'=>6,
        'July'=>7,
        'August'=>8,
        'September'=>9,
        'October'=>10,
        'November'=>11,
        'December'=>12,
    ];
    return $array[$month];
}

function convertMonthToId($month)
{
    $array=[
        1=>'Januari',
        2=>'Februari',
        3=>'Maret',
        4=>'April',
        5=>'Mei',
        6=>'Juni',
        7=>'Juli',
        8=>'Agustus',
        9=>'September',
        10=>'Oktober',
        11=>'November',
        12=>'Desember'
    ];
    return $array[$month];
}

function convertDayToId($day)
{
    $array=[
        0=>'Minggu',
        1=>'Senin',
        2=>'Selasa',
        3=>'Rabu',
        4=>'Kamis',
        5=>'Jumat',
        6=>'Sabtu'
    ];
    return $array[$day];
}

function arrayMonth()
{
    $array=[
        1=>'Januari',
        2=>'Februari',
        3=>'Maret',
        4=>'April',
        5=>'Mei',
        6=>'Juni',
        7=>'Juli',
        8=>'Agustus',
        9=>'September',
        10=>'Oktober',
        11=>'November',
        12=>'Desember'
    ];
    return $array;
}


function statusPPOB($id)
{
    $array=[
        1=>'PENDING',
        6=>'PENDING',
        2=>'SUCCESS',
        3=>'FAILED',
        4=>'FAILED'
    ];
    return $array[$id];
}

function get_user_profil()
{
    try{
        if(user()->is_staff==0)
        {
            $data=user();
        }else{
            $data=user();
        }
        if(is_null($data))
        {
            return asset('public/backend/img/no-user.png');
        }else{
            if(is_file($data->foto))
            {
                return asset($data->foto);
            }
            return asset('public/backend/img/no-user.png');

        }
    }catch (\Exception $e)
    {
        return asset('public/backend/img/no-user.png');

    }
}

function get_user_name()
{
    try{
        if(user()->is_staff==0)
        {
            $data=user()->fullname;
        }else{
            $data=user()->fullname;
        }
        if(is_null($data))
        {
            return '-';
        }else{
            return $data;
        }
    }catch (\Exception $e)
    {
        return '-';
    }
}

function get_user_token()
{
    try{
        $data=\App\Models\MasterData\User::find(user_id());
        if(is_null($data))
        {
            return '-';
        }else{
            return $data->token;
        }
    }catch (\Exception $e)
    {
        return '-';
    }
}

function login_url($m)
{
    if(env('APP_DOMAIN')==true)
    {
        return "https://senna.co.id/login?m=".$m;
    }else{
        return env('APP_URL').'login?m='.$m;
    }
}

function get_staff_id()
{
    return user()->id;
}

function get_is_staff()
{
    return user()->is_staff;
}

function get_notif()
{
    try{
       $data=\App\Models\MasterData\UserActivityNotification::where('md_user_id',user_id())
           ->where('is_read',0)
            ->orderBy('created_at','DESC')
           ->limit(3)
            ->get();
       return $data;
    }catch (\Exception $e)
    {
        return null;
    }
}

function rupiah($angka){

    $hasil_rupiah = "" . number_format($angka,2,',','.');
    return $hasil_rupiah;

}

function currency($angka){

    $hasil = number_format((float)$angka, 2, '.', '');
    return $hasil;

}

function checkedListMenu($staffId,$id,$action)
{
    return  (\Illuminate\Support\Facades\DB::table('md_merchant_staff_menus')
        ->join('md_merchant_staff_menu_actions as a','md_merchant_staff_menus.id','a.md_merchant_staff_menu_id')
        ->where('md_merchant_staff_menus.md_merchant_staff_id',$staffId)
        ->where('md_merchant_staff_menus.md_merchant_menu_id',$id)
        ->where('a.action',$action)->first())?"checked":"";
}

function menu_show($staffId,$id)
{
    return  (\Illuminate\Support\Facades\DB::table('md_merchant_staff_menus')
        ->join('md_merchant_staff_menu_actions as a','md_merchant_staff_menus.id','a.md_merchant_staff_menu_id')
        ->where('md_merchant_staff_menus.md_merchant_staff_id',$staffId)
        ->where('md_merchant_staff_menus.md_merchant_menu_id',$id)->count()>0)?"show":"";
}

function menu_parent_show($staffId,$id)
{
    return  (\Illuminate\Support\Facades\DB::table('md_merchant_staff_menus')
            ->join('md_merchant_menus as m','m.id','md_merchant_staff_menus.md_merchant_menu_id')
            ->where('md_merchant_staff_menus.md_merchant_staff_id',$staffId)
            ->where('m.id',$id)->count()>0)?"show":"";
}

function getHead(){
    return \Illuminate\Support\Facades\DB::table('md_merchant_branchs')
    ->where('branch_merchant_id',merchant_id())
    ->where('is_accepted',1)->where('is_active',1)
    ->join('md_merchants','md_merchants.id','md_merchant_branchs.head_merchant_id')
    ->first();

}

function removeStringNull($array)
{
    foreach ($array as $key =>$item)
    {
        if($item=="null")
            unset($array[$key]);
    }
    return $array;
}

function searchMenuWithSubscription($idMenu)
{
    $id=(is_null(user()->getMerchant->md_subscription_id))?12:user()->getMerchant->md_subscription_id;
    $menu=MerchantMenu::where("payment_clusters", '@>', '[' . json_encode(['id' => is_null($id)?"1":"".$id.""]) . ']')
    ->where('id',$idMenu)->first();
    return (is_null($menu))?false:true;

}

function arrayFlatten($array) {
    if (!is_array($array)) {
        return FALSE;
    }
    $result = array();
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, arrayFlatten($value));
        }
        else {
            $result[$key] = $value;
        }
    }
    return $result;
}

function get_lang()
{
    $lang=\Illuminate\Support\Facades\Session::get('SENNA_KASIR_PRO_LANGUAGE');
    if(is_null($lang))
    {
        $key='id';
    }else{
        if($lang=='id')
        {
            $key='id';
        }else{
            $key='en';
        }
    }
    return $key;
}

function checkSubscription($merchantId)
{
    try{
        $merchant=merchant_detail();
        if(in_array($merchant->md_subscription_id,[10,11,12]))
        {
            return true;
        }else{
            return false;
        }
    }catch (\Exception $e)
    {
        return false;
    }

}

function subscriptionId($merchantId)
{
    try{
        $merchant=merchant_detail();
        return (is_null($merchant->md_subscription_id))?12: $merchant->md_subscription_id;
    }catch (\Exception $e)
    {
        return 0;

    }

}

function uploadHelper($fileImage, $destinationPath)
{
    try {
        $file = $fileImage;
        $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());

        if($file->getSize() > 0.5 * 1000 * 1000){
            $image = Image::make($file)->resize(300,300, function($constraint){
                $constraint->aspectRatio();
            })->encode($file->getClientOriginalExtension());
        } else {
            $image = Image::make($file)->encode($file->getClientOriginalExtension());
        }

        $fileName=$destinationPath.$fileName;
        Storage::disk('s3')->put($fileName, (string) $image);

        return $fileName;
    }catch(\Exception $e)
    {
        return false;
    }
}


function generateReligion()
{
    $data = [
        "1" => "Islam",
        "2"=>"Katolik",
        "3" => "Kristen",
        "4"=> "Hindu",
        "5"=>"Budha"
    ];

    return $data;
}

function generateMediaInfo()
{
    $data = [
        "1" => "Media Sosial",
        "2"=>"Referensi"
    ];

    return $data;
}

function generateMaritalStatus()
{
    $data = [
        "1" => "Belum Kawin",
        "2" => "Kawin",
        "3" => "Cerai Hidup",
        "4" => "Cerai Mati"
    ];

    return $data;
}

function generateEducation()
{
    $data = [
        "1" => "SD",
        "2" => "SMP",
        "3" => "SMA/Sederajat",
        "4" => "D1",
        "5" => "D2",
        "6" => "D3",
        "7" => "S1",
        "8" => "S2",
        "9" => "S3"
    ];

    return $data;
}

function generateBloodType()
{
    $data = ["A", "B", "AB", "O"];

    return $data;
}




function checkPlugin($merchantId,$type)
{
    if($type==2){
        if(env('APP_ENV')=='production'){
            $check=\App\Models\Plugin\EnableMerchantPlugin::select([
                'enable_merchant_plugins.start_period',
                'enable_merchant_plugins.end_period',
                'p.type'
            ])
                ->where('enable_merchant_plugins.status',1)
                ->where('enable_merchant_plugins.md_merchant_id',$merchantId)
                ->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
                ->whereIn('p.type',['clinic'])->first();
            if(is_null($check)){
                return false;
            }else{
                if(date('Y-m-d H:i:s')>$check->end_period){
                    return false;
                }else{
                    $session=\Illuminate\Support\Facades\Session::get('is_clinic_'.$merchantId);
                    if(is_null($session))
                    {
                        \Illuminate\Support\Facades\Session::put('is_clinic_'.$merchantId,1);
                    }
                    return $check->type;
                }
            }
        }else{
            $check=\App\Models\Plugin\EnableMerchantPlugin::select([
                'enable_merchant_plugins.start_period',
                'enable_merchant_plugins.end_period',
                'p.type'
            ])
                ->where('enable_merchant_plugins.status',1)
                ->where('enable_merchant_plugins.md_merchant_id',$merchantId)
                ->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
                ->whereIn('p.type',['clinic'])->first();
            if(is_null($check)){
                return false;
            }else{
                if(date('Y-m-d H:i:s')>$check->end_period){
                    return false;
                }else{
                    $session=\Illuminate\Support\Facades\Session::get('is_clinic_'.$merchantId);
                    if(is_null($session))
                    {
                        \Illuminate\Support\Facades\Session::put('is_clinic_'.$merchantId,1);
                    }
                    return $check->type;
                }
            }
        }

    }elseif ($type==3){
//        $check=\App\Models\Plugin\EnableMerchantPlugin::select([
//            'enable_merchant_plugins.start_period',
//            'enable_merchant_plugins.end_period',
//        ])
//            ->where('enable_merchant_plugins.status',1)
//            ->where('enable_merchant_plugins.md_merchant_id',$merchantId)->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
//            ->where('p.type','workshop')->first();
//        if(is_null($check)){
//            return false;
//        }else{
//            if(date('Y-m-d H:i:s')>$check->end_period){
//                return false;
//            }else{
//                return true;
//            }
//        }
    }elseif ($type==4){
        if(env('APP_ENV')=='production'){
            $check=\App\Models\Plugin\EnableMerchantPlugin::select([
                'enable_merchant_plugins.start_period',
                'enable_merchant_plugins.end_period',
                'p.type'
            ])
                ->where('enable_merchant_plugins.status',1)
                ->where('enable_merchant_plugins.md_merchant_id',$merchantId)
                ->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
                ->whereIn('p.type',['sfa'])->first();
            if(is_null($check)){
                return false;
            }else{
                if(date('Y-m-d H:i:s')>$check->end_period){
                    return false;
                }else{
                    $session=\Illuminate\Support\Facades\Session::get('is_sfa_'.$merchantId);
                    if(is_null($session))
                    {
                        \Illuminate\Support\Facades\Session::put('is_sfa_'.$merchantId,1);
                    }
                    return 'sfa';
                }
            }
        }else{
            return 'sfa';
        }
    }elseif ($type==5){
//        $check=\App\Models\Plugin\EnableMerchantPlugin::select([
//            'enable_merchant_plugins.start_period',
//            'enable_merchant_plugins.end_period',
//        ])
//            ->where('enable_merchant_plugins.status',1)
//            ->where('enable_merchant_plugins.md_merchant_id',$merchantId)->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
//            ->where('p.type','e_commerce')->first();
//        if(is_null($check)){
//            $checkOther=\App\Models\Plugin\EnableMerchantPlugin::select([
//                'enable_merchant_plugins.start_period',
//                'enable_merchant_plugins.end_period',
//            ])
//                ->where('enable_merchant_plugins.status',1)
//                ->where('enable_merchant_plugins.md_merchant_id',$merchantId)->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
//                ->where('p.type','grab')->first();
//            if(is_null($checkOther)){
//                return  false;
//            }else{
//
//                if(date('Y-m-d H:i:s')>$checkOther->end_period){
//                    return false;
//                }else{
//                    return true;
//                }
//            }
//        }else{
////            if(date('Y-m-d H:i:s')>$check->end_period){
////                return false;
////            }else{
////                return true;
////            }
//        }
    }elseif ($type==6){
//        $check=\App\Models\Plugin\EnableMerchantPlugin::select([
//            'enable_merchant_plugins.start_period',
//            'enable_merchant_plugins.end_period',
//        ])
//            ->where('enable_merchant_plugins.status',1)
//            ->where('enable_merchant_plugins.md_merchant_id',$merchantId)->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
//            ->where('p.type','e_commerce')->first();
//        if(is_null($check)){
//            return false;
//        }else{
//            if(date('Y-m-d H:i:s')>$check->end_period){
//                return false;
//            }else{
//                return true;
//            }
//        }
    }elseif($type==7){
//        $check=\App\Models\Plugin\EnableMerchantPlugin::select([
//            'enable_merchant_plugins.start_period',
//            'enable_merchant_plugins.end_period',
//        ])
//            ->where('enable_merchant_plugins.status',1)
//            ->where('enable_merchant_plugins.md_merchant_id',$merchantId)->join('md_plugins as p','p.id','enable_merchant_plugins.md_plugin_id')
//            ->where('p.type','grab')->first();
//        if(is_null($check)){
//            return false;
//        }else{
//            if(date('Y-m-d H:i:s')>$check->end_period){
//                return false;
//            }else{
//                return true;
//            }
//        }
    }else{
        return true;
   }
}

function getTimeZoneName($timezone){
    // $tz;
    if(is_null($timezone)){
        $tz='';
    }else{
        switch($timezone){
            case 'Asia/Jakarta':
                $tz =  $timezone.' (WIB)';
                break;
            case 'Asia/Makassar':
                $tz =  $timezone.' (WITA)';
                break;
            case 'Asia/Jayapura':
                $tz = $timezone.' (WIT)';
                break;
            default:
                $tz = $timezone;
                break;
        }
    }
    return $tz;
}

function replaceMenuName($type,$name)
{
    if($type=='workshop'){
        $array=[
            'Klinik'=>'Bengkel',
            'Rekam Medis'=>'Riwayat Perawatan',
        ];
        if(!array_key_exists($name,$array)){

            return  $name;
        }else{
            return $array[$name];

        }
    }else{
        return $name;
    }
}

function arrayCustomCode()
{
    return [
        [
            'name'=>'kode',
            'alias'=>'Kode',
            'value'=>''
        ],
        [
            'name'=>'pemisah',
            'alias'=>'Pemisah',
            'value'=>''
        ],
        [
            'name'=>'tanggal',
            'alias'=>'Tanggal',
            'value'=>'Y-m-d'
        ],
        [
            'name'=>'tahun',
            'alias'=>'Tahun',
            'value'=>'Y',
        ],
        [
            'name'=>'no_urut',
            'alias'=>'No Urut',
            'value'=>[
                'dimulai_dari'=>'',
                'panjang_no_urut'=>''
            ]
        ],
    ];
}

function dayOrMonthLocale($name)
{
    $array=[
        'Monday'=>'Senin',
        'Tuesday'=>'Selasa',
        'Wednesday'=>'Rabu',
        'Thursday'=>'Kamis',
        'Friday'=>"Jum'at",
        'Saturday'=>'Sabtu',
        'Sunday'=>'Minggu',
        'January'=>'Januari',
        'February'=>'Februari',
        'March'=>'Maret',
        'April'=>'April',
        'May'=>"Mei",
        'June'=>'Juni',
        'July'=>'Juli',
        'August'=>'Agustus',
        'September'=>'September',
        'October'=>'Oktober',
        'November'=>'November',
        'December'=>'Desember'
    ];

    if(array_key_exists($name,$array)){
        return $array[str_replace(' ','',$name)];

    }else{
        return str_replace(' ','',$name);
    }

}

function getShortSentence($string){

    $words = explode(" ", $string);
    $acronym = "";

    foreach ($words as $w) {
        $acronym .= mb_substr($w, 0, 1);
    }
    return $acronym;
}




<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */


function log_helper($controllerName='',$routeName='',$errorLine='',$errorFile='',$errorMessage='')
{
    \Illuminate\Support\Facades\DB::table('md_app_logs')
        ->insert([
            'log_name'=>'Senna',
            'controller_name'=>$controllerName,
            'route_name'=>$routeName,
            'error_line'=>$errorLine,
            'error_file'=>$errorFile,
            'error_message'=>$errorMessage,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Accounting;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\Accounting\MerchantArDetail;
use App\Models\MasterData\InventoryMethod;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\StockSaleMapping;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


class CoaSaleUtil
{

    public static function coaStockAdjustmentFromSaleV2Mobile($userId,$merchantId,$coaJson,$warehouse,$inventoryId,$purchaseId,$isEdit=0)
    {
        try{
            DB::beginTransaction();
            $merchant=Merchant::find($merchantId);
            $discount=$merchant->coa_transaction_discount_sale_id;
            $tax=$merchant->coa_debt_ppn_out_id;
            $payableAcc=$merchant->coa_ar_sale_id;
            $saleCoa=$merchant->coa_revenue_id;
            $shipping=$merchant->coa_shipping_cost_id;
            $purchase=self::getData($purchaseId)[0];
            $coaTrans=CoaDetail::find($purchase->coa_trans_id);

            $poDelivery=SaleOrder::where('code',$purchase->code)
                ->where('step_type',3)
                ->where('md_merchant_id',$purchase->md_merchant_id)
                ->first();
            if(is_null($poDelivery)){
                $details=collect(json_decode($purchase->get_detail));
                $mappings=collect(json_decode($purchase->get_sale_mapping));

            }else{
                $purchase2=self::getData($poDelivery->id)[0];
                $details=collect(json_decode($purchase2->get_detail));
                $mappings=collect(json_decode($purchase2->get_sale_mapping));
            }
            $code=Jurnal::where('md_merchant_id',$merchantId)
                    ->count()+1;
            if($isEdit==0)
            {
                $data=new  Jurnal();
                $data->trans_code='JRN'.sprintf("%07s", $code);
            }else{
                $data=Jurnal::where('ref_code',$purchase->code)
                    ->where('external_ref_id',$purchase->id)
                    ->first();
                JurnalDetail::where('acc_jurnal_id',$data->id)
                    ->delete();
            }
            $data->ref_code=$purchase->code;
            $data->trans_name='Penjualan Produk '.$purchase->code;
            $data->trans_time=$purchase->created_at;
            $data->trans_note='Penjualan Produk '.$purchase->code;
            $data->trans_purpose='Penjualan Produk '.$purchase->code;
            $data->trans_amount=$purchase->total;
            $data->md_merchant_id=$merchantId;
            $data->md_user_id_created=$userId;
            $data->external_ref_id=$purchase->id;
            $data->timezone=$purchase->timezone;
            $data->flag_name='Penjualan Produk '.$purchase->code;
            $data->save();
            $sale=0;
            $productNotBonus=[];
            $productBonus=[];
            $insert=[];
            $insertHpp=[];
            $insertInv=[];
            $arrayProduct=[];
            $totalProductPacket=0;
            $costOther=[];
            foreach ($details as $d)
            {
                $product=Product::find($d->sc_product_id);
                if(!is_null($d->coa_inv_id))
                {
                    $amountHpp=0;
                    foreach ($mappings as $item)
                    {
                        if($d->sc_product_id==$item->sc_product_id){

                            if($inventoryId==InventoryMethod::FIFO){
                                $amountHpp+=$item->purchase_price*$item->amount;
                            }elseif($inventoryId=InventoryMethod::LIFO)
                            {
                                $amountHpp+=$item->purchase_price*$item->amount;

                            }elseif ($inventoryId==InventoryMethod::AVERAGE)
                            {
                                $amountHpp+=HppUtil::average($item->sc_product_id,$warehouse)*$item->amount;

                            }else{
                                $amountHpp+=$item->purchase_price*$item->amount;

                            }
                        }
                    }

                    if($d->is_bonus==0){
                        $productNotBonus[]=[
                            'sc_product_id'=>$d->sc_product_id
                        ];
                    }
                    if($d->is_bonus==0){
                        array_push($insert,[
                            'acc_coa_detail_id'=>$d->coa_inv_id,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$amountHpp,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>$d->id,
                            'from_trans_type'=>1
                        ]);
                        array_push($insert,[
                            'acc_coa_detail_id'=>$d->coa_hpp_id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$amountHpp,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>$d->id,
                            'from_trans_type'=>1
                        ]);
                    }

                    // foreach($productNotBonus as $key => $i){
                    //     if($d->is_bonus==1 && $d->sc_product_id!=$i['sc_product_id']){
                    //         array_push($insert,[
                    //             'acc_coa_detail_id'=>$d->coa_inv_id,
                    //             'coa_type'=>'Kredit',
                    //             'md_sc_currency_id'=>1,
                    //             'amount'=>$amountHpp,
                    //             'created_at'=>$purchase->created_at,
                    //             'updated_at'=>$purchase->updated_at,
                    //             'acc_jurnal_id'=>$data->id,
                    //             'ref_id'=>$d->id,
                    //             'from_trans_type'=>1
                    //         ]);
                    //         array_push($insert,[
                    //             'acc_coa_detail_id'=>$d->coa_hpp_id,
                    //             'coa_type'=>'Debit',
                    //             'md_sc_currency_id'=>1,
                    //             'amount'=>$amountHpp,
                    //             'created_at'=>$purchase->created_at,
                    //             'updated_at'=>$purchase->updated_at,
                    //             'acc_jurnal_id'=>$data->id,
                    //             'ref_id'=>$d->id,
                    //             'from_trans_type'=>1
                    //         ]);
                    //     }
                    // }

                }
                if($d->is_bonus==1){
                    if(!is_null($d->coa_inv_id)){
                        $productBonus[]=[
                            'sc_product_id'=>$d->sc_product_id,
                            'coa_inv_id'=>$d->coa_inv_id,
                            'ref_id'=>$d->id,
                            'coa_hpp_id'=>$d->coa_hpp_id
                        ];
                    }

                }
                if($d->is_bonus==0){
                    $sale+=$d->sub_total;
                }
                if($product->md_sc_product_type_id==4){
                    if($product->cost_other!=[] && !is_null($product->cost_other)){
                        foreach(json_decode($product->cost_other) as $item){
                            array_push($insert,[
                                'acc_coa_detail_id'=>$item->id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$item->amount,
                                'created_at'=>$purchase->created_at,
                                'updated_at'=>$purchase->updated_at,
                                'acc_jurnal_id'=>$data->id,
                                'ref_id'=>null,
                                'from_trans_type'=>0
                            ]);
                        }
                    }
                    foreach (self::getRawMaterialWithStock($purchaseId,$d->sc_product_id) as $m =>$mm)
                        {
                            $arrayProduct[]=[
                                'id'=>$mm->child_sc_product_id,
                                'total'=>$mm->amount*$mm->purchase_price,
                                'inv_id'=>$mm->inv_id,
                                'hpp_id'=>$mm->hpp_id,
                                'parent_id'=>$d->sc_product_id
                            ];
                        }
                }

            }
            // dd($insert);
            if($productBonus!=[]){
                foreach($productBonus as $key => $i){
                        $amountHppBonus=0;
                        foreach ($mappings as $item)
                        {
                            if($i['sc_product_id']==$item->sc_product_id){

                                if($inventoryId==InventoryMethod::FIFO){
                                    $amountHppBonus+=$item->purchase_price*$item->amount;
                                }elseif($inventoryId=InventoryMethod::LIFO)
                                {
                                    $amountHppBonus+=$item->purchase_price*$item->amount;

                                }elseif ($inventoryId==InventoryMethod::AVERAGE)
                                {
                                    $amountHppBonus+=HppUtil::average($item->sc_product_id,$warehouse)*$item->amount;

                                }else{
                                    $amountHppBonus+=$item->purchase_price*$item->amount;

                                }
                            }
                        }
                        if(!in_array($i['coa_inv_id'],array_column($insert,'acc_coa_detail_id'))){
                            array_push($insert,[
                                'acc_coa_detail_id'=>$i['coa_inv_id'],
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$amountHppBonus,
                                'created_at'=>$purchase->created_at,
                                'updated_at'=>$purchase->updated_at,
                                'acc_jurnal_id'=>$data->id,
                                'ref_id'=>$i['ref_id'],
                                'from_trans_type'=>1
                            ]);
                            array_push($insert,[
                                'acc_coa_detail_id'=>$i['coa_hpp_id'],
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$amountHppBonus,
                                'created_at'=>$purchase->created_at,
                                'updated_at'=>$purchase->updated_at,
                                'acc_jurnal_id'=>$data->id,
                                'ref_id'=>$i['ref_id'],
                                'from_trans_type'=>1
                            ]);
                        }

                }
            }

            if($purchase->cost_other!=0){
                $sale-=$purchase->cost_other;
            }
            if($d->is_bonus==0)
            {
                if(!is_null($d->cost_other))
                {
                    $cost=(object)$d->cost_other;
                    foreach ($cost as $c)
                    {
                        $costOther[]=[
                            'id'=>$c->id,
                            'amount'=>$c->amount*$d->quantity
                        ];
                    }
                }
            }
            if(!empty($arrayProduct))
            {
                $arrayProduct2=collect($arrayProduct)->groupBy('id');
                foreach ($arrayProduct2 as $cc2 =>$ccc2)
                {
                    array_push($insertInv,[
                        'acc_coa_detail_id'=>$ccc2->first()['inv_id'],
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$ccc2->sum('total'),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$ccc2->first()['id'],
                        'from_trans_type'=>1
                    ]);
                    array_push($insertHpp,[
                        'acc_coa_detail_id'=>$ccc2->first()['hpp_id'],
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$ccc2->sum('total'),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$ccc2->first()['id'],
                        'from_trans_type'=>1
                    ]);
                }
            }

            array_push($insert,[

                'acc_coa_detail_id'=>$saleCoa,
                'coa_type'=>'Kredit',
                'md_sc_currency_id'=>1,
                'amount'=>$sale,
                'created_at'=>$purchase->created_at,
                'updated_at'=>$purchase->updated_at,
                'acc_jurnal_id'=>$data->id,
                'ref_id'=>null,
                'from_trans_type'=>0
            ]);

            if($purchase->promo!=0)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$discount,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->promo,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0

                ]);
            }
            if($purchase->tax!=0)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$tax,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->tax,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0

                ]);
            }

            if($purchase->total_cost_other_sale!=0)
            {
                $jsonCost=json_decode($purchase->cost_other_sale);
                foreach($jsonCost as $c){
                    // dd($c->amount);
                    array_push($insert,[
                        'acc_coa_detail_id'=>$c->id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$c->amount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);
                }
            }
            if($purchase->is_debet==0)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$coaTrans->id,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->total,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0
                ]);
            }
            if($purchase->is_debet==1){
                array_push($insert,[
                    'acc_coa_detail_id'=>$payableAcc,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->total,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0
                ]);
            }
            if($purchase->is_with_load_shipping==1 && $purchase->is_debet==0)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$shipping,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->shipping_cost,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0
                ]);

                $dataShipingCost=new  Jurnal();
                $dataShipingCost->trans_code='JRN'.sprintf("%07s", $code+1);
                $dataShipingCost->ref_code=$purchase->code;
                $dataShipingCost->trans_name='Biaya Pengiriman Penjualan Produk '.$purchase->code;
                $dataShipingCost->trans_time=$purchase->created_at;
                $dataShipingCost->trans_note='Biaya Pengiriman Penjualan Produk '.$purchase->code;
                $dataShipingCost->trans_purpose='Penjualan Produk '.$purchase->code;
                $dataShipingCost->trans_amount=$purchase->shipping_cost;
                $dataShipingCost->md_merchant_id=$merchantId;
                $dataShipingCost->md_user_id_created=$userId;
                $dataShipingCost->external_ref_id=$purchase->id;
                $dataShipingCost->flag_name='Biaya Pengiriman Penjualan Produk '.$purchase->code;
                $dataShipingCost->save();
                $insert2=[];
                array_push($insert2,
                    [
                        'acc_coa_detail_id'=>$shipping,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$dataShipingCost->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0

                    ]);

                array_push($insert2,
                    [
                        'acc_coa_detail_id'=>$coaTrans->id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$dataShipingCost->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                ]);

                JurnalDetail::insert($insert2);

            }elseif($purchase->is_with_load_shipping==1 && $purchase->is_debet==1){
                $coaTrans1=$merchant->coa_cash_id;
                array_push($insert,[
                        'acc_coa_detail_id'=>$shipping,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                $dataShipingCost=new  Jurnal();
                $dataShipingCost->trans_code='JRN'.sprintf("%07s", $code+1);
                $dataShipingCost->ref_code=$purchase->code;
                $dataShipingCost->trans_name='Biaya Pengiriman Penjualan Produk '.$purchase->code;
                $dataShipingCost->trans_time=$purchase->created_at;
                $dataShipingCost->trans_note='Biaya Pengiriman Penjualan Produk '.$purchase->code;
                $dataShipingCost->trans_purpose='Penjualan Produk '.$purchase->code;
                $dataShipingCost->trans_amount=$purchase->shipping_cost;
                $dataShipingCost->md_merchant_id=$merchantId;
                $dataShipingCost->md_user_id_created=$userId;
                $dataShipingCost->external_ref_id=$purchase->id;
                $dataShipingCost->flag_name='Biaya Pengiriman Penjualan Produk '.$purchase->code;
                $dataShipingCost->save();
                $insert2=[];
                array_push($insert2,
                    [
                        'acc_coa_detail_id'=>$shipping,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$dataShipingCost->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0

                    ]);

                array_push($insert2,
                    [
                        'acc_coa_detail_id'=>$coaTrans1,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$dataShipingCost->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                JurnalDetail::insert($insert2);
            }

            if($purchase->admin_fee!=0)
            {
                $adminFee=$merchant->coa_administration_bank_id;

                array_push($insert,
                        [
                            'acc_coa_detail_id'=>$adminFee,
                            'coa_type'=>"Debit",
                            'md_sc_currency_id'=>1,
                            'amount'=>$purchase->admin_fee,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>null,
                            'from_trans_type'=>0

                        ]
                );

                array_push($insert,
                        [
                            'acc_coa_detail_id'=>$coaTrans->id,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$purchase->admin_fee,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>null,
                            'from_trans_type'=>0
                        ]
                );

            }
            if(!empty($costOther))
            {
                $totalOther2=0;
                $costOther2=collect($costOther)->groupBy('id');
                $data4=new  Jurnal();
                $data4->trans_code='JRN'.sprintf("%07s", $code+1);
                $data4->ref_code=$purchase->code;
                $data4->trans_name='Pengurangan Biaya Atas Penjualan Produk '.$purchase->code;
                $data4->trans_time=$purchase->created_at;
                $data4->trans_note='Pengurangan Biaya Atas Penjualan Produk '.$purchase->code;
                $data4->trans_purpose='Pengurangan Biaya Atas Penjualan Produk '.$purchase->code;
                $data4->trans_amount=$purchase->shipping_cost;
                $data4->md_merchant_id=$merchantId;
                $data4->md_user_id_created=$userId;
                $data4->external_ref_id=$purchase->id;
                $data4->flag_name='Pengurangan Biaya Atas Penjualan Produk '.$purchase->code;
                $data4->save();
                $arrayCost=[];

                if($coaTrans->name=='Kas' || $coaTrans->name=='Kas Kasir Outlet'){
                    $coaTrans1=$coaTrans->id;
                }else{
                    $coaTrans1=$merchant->coa_cash_id;
                }
                foreach ($costOther2 as $cc =>$ccc)
                {

                    array_push($arrayCost,[
                        'acc_coa_detail_id'=>$cc,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$ccc->sum('amount'),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data4->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                    $totalOther2+=$ccc->sum('amount');
                }

                array_push($arrayCost,
                    [
                        'acc_coa_detail_id'=>$coaTrans1,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$totalOther2,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data4->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);


                JurnalDetail::insert($arrayCost);

            }

            JurnalDetail::insert($insert);
            DB::commit();
            return true;
        }catch (\Exception $e){
            DB::rollBack();
            // dd($e);
            return false;
        }
    }


    public static function coaReturSaleOrderv2Mobile($userId,$merchantId,$coaJson,$retur,$time,$amountOfHpp)
    {
        try {

            DB::beginTransaction();
            $merchant=Merchant::find($merchantId);

            $payableAcc=$merchant->coa_ar_sale_id;
            $saleCoa=$merchant->coa_revenue_id;

            $data=new  Jurnal();
            $data->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);
            $data->ref_code=$retur->code;
            $data->trans_name='Retur Penjualan '.$retur->code.'-'.$retur->getSaleOrder->code;
            $data->trans_time=$time;
            $data->trans_note='Retur Penjualan '.$retur->code.'-'.$retur->getSaleOrder->code;
            $data->trans_purpose='Retur Penjualan '.$retur->code.'-'.$retur->getSaleOrder->code;
            $data->trans_amount=$retur->total;
            $data->md_merchant_id=$merchantId;
            $data->md_user_id_created=$userId;
            $data->external_ref_id=$retur->id;
            $data->timezone=$retur->timezone;
            $data->flag_name='Retur Penjualan '.$retur->code.'-'.$retur->getSaleOrder->code;
            $data->save()
            ;
            $insert=[];

            if($retur->getSaleOrder->is_debet==1)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$payableAcc,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$retur->total,
                    'created_at'=>$time,
                    'updated_at'=>$time,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>2
                ]);
            }

            if($retur->getSaleOrder->is_debet==0)
            {
                $coaPayable=$retur->getSaleOrder->coa_trans_id;

                array_push($insert,[
                    'acc_coa_detail_id'=>$coaPayable,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$retur->total,
                    'created_at'=>$time,
                    'updated_at'=>$time,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>2
                ]);
            }
            array_push($insert,[
                'acc_coa_detail_id'=>$saleCoa,
                'coa_type'=>'Debit',
                'md_sc_currency_id'=>1,
                'amount'=>$retur->total,
                'created_at'=>$time,
                'updated_at'=>$time,
                'acc_jurnal_id'=>$data->id,
                'ref_id'=>null,
                'from_trans_type'=>2
            ]);
            foreach ($retur->getDetail as $item)
            {

                if(!is_null($item->coa_inv_id))
                {
                    $total=0;
                    foreach ($amountOfHpp as $h)
                    {
                        if($item->sc_product_id==$h['id'])
                        {
                            $total+=$h['total'];
                        }
                    }
                    array_push($insert,[
                        'acc_coa_detail_id'=>$item->coa_inv_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$total,
                        'created_at'=>$time,
                        'updated_at'=>$time,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$item->id,
                        'from_trans_type'=>2
                    ]);

                    array_push($insert,[
                        'acc_coa_detail_id'=>$item->coa_hpp_id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$total,
                        'created_at'=>$time,
                        'updated_at'=>$time,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$item->id,
                        'from_trans_type'=>1
                    ]);
                }
            }

            JurnalDetail::insert($insert);
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;

        }
    }


    public static function coaStockAdjustmentFromSaleV2($userId,$merchantId,$coaJson,$warehouse,$inventoryId,$purchaseId,$isEdit=0)
    {
        try{
            DB::beginTransaction();
            //dd($coaJson);
            $merchant=Merchant::find($merchantId);
            $discount=$merchant->coa_transaction_discount_sale_id;
            $tax=$merchant->coa_debt_ppn_out_id;
            $payableAcc=$merchant->coa_ar_sale_id;
            $saleCoa=$merchant->coa_revenue_id;
            $shipping=$merchant->coa_shipping_cost_id;
            $purchase=self::getData($purchaseId)[0];
            $coaTrans=CoaDetail::find($purchase->coa_trans_id);

            $poDelivery=SaleOrder::where('code',$purchase->code)
                ->where('step_type',3)
                ->where('md_merchant_id',$purchase->md_merchant_id)
                ->first();
            if(is_null($poDelivery)){
                $soId=$purchase->id;
                $details=collect(json_decode($purchase->get_detail));
                $mappings=collect(json_decode($purchase->get_sale_mapping));

            }else{
                $purchase2=self::getData($poDelivery->id)[0];
                $soId=$purchase2->id;
                $details=collect(json_decode($purchase2->get_detail));
                $mappings=collect(json_decode($purchase2->get_sale_mapping));
            }

            $code=Jurnal::where('md_merchant_id',$merchantId)
                    ->count()+1;
            if($isEdit==0)
            {
                $data=new  Jurnal();
                $data->trans_code='JRN'.sprintf("%07s", $code);
            }else{
                $data=Jurnal::where('ref_code',$purchase->code)
                    ->where('external_ref_id',$purchase->id)
                    ->first();
                JurnalDetail::where('acc_jurnal_id',$data->id)
                    ->delete();
            }
            $purchaseCode=is_null($purchase->second_code)?$purchase->code:$purchase->second_code;

            $data->ref_code=$purchase->code;
            $data->trans_name='Penjualan Produk '.$purchaseCode;
            $data->trans_time=$purchase->created_at;
            $data->trans_note='Penjualan Produk '.$purchaseCode;
            $data->trans_purpose='Penjualan Produk '.$purchaseCode;
            $data->trans_amount=$purchase->total;
            $data->md_merchant_id=$merchantId;
            $data->md_user_id_created=$userId;
            $data->external_ref_id=$purchase->id;
            $data->flag_name='Penjualan Produk '.$purchaseCode;
            $data->timezone=$purchase->timezone;
            $data->save();
            $sale=0;
            $productNotBonus=[];
            $insert=[];
            $insertHpp=[];
            $insertInv=[];
            $arrayProduct=[];
            $updateDetails="";

            if($purchase->is_debet==0)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$coaTrans->id,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->total,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0
                ]);
            }

            if($purchase->is_debet==1){
                array_push($insert,[
                    'acc_coa_detail_id'=>$payableAcc,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->total,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0
                ]);
            }

            if($purchase->tax!=0)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$tax,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->tax,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0

                ]);
            }

            $costOther=[];
            foreach ($details as $dkey => $d)
            {
                $costAll=0;

                if(!is_null($d->coa_inv_id))
                {
                    $amountHpp=0;
                    foreach ($mappings as $item)
                    {
                        if($d->sc_product_id==$item->sc_product_id){

                            if($inventoryId==InventoryMethod::FIFO){
                                $amountHpp+=$item->purchase_price*$item->amount;
                            }elseif($inventoryId=InventoryMethod::LIFO)
                            {
                                $amountHpp+=$item->purchase_price*$item->amount;

                            }elseif ($inventoryId==InventoryMethod::AVERAGE)
                            {
                                $amountHpp+=HppUtil::average($item->sc_product_id,$warehouse)*$item->amount;

                            }else{
                                $amountHpp+=$item->purchase_price*$item->amount;

                            }
                        }
                    }


                    if($d->is_bonus==0){
                        $productNotBonus[]=[
                            'sc_product_id'=>$d->sc_product_id
                        ];
                    }
                    if($d->is_bonus==0){
                        array_push($insertInv,[
                            'acc_coa_detail_id'=>$d->coa_inv_id,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$amountHpp,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>$d->id,
                            'from_trans_type'=>1
                        ]);
                        array_push($insertHpp,[
                            'acc_coa_detail_id'=>$d->coa_hpp_id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$amountHpp,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>$d->id,
                            'from_trans_type'=>1
                        ]);
                    }

                    foreach($productNotBonus as $key => $i){
                        if($d->is_bonus==1 && $d->sc_product_id!=$i['sc_product_id']){
                            array_push($insertInv,[
                                'acc_coa_detail_id'=>$d->coa_inv_id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$amountHpp,
                                'created_at'=>$purchase->created_at,
                                'updated_at'=>$purchase->updated_at,
                                'acc_jurnal_id'=>$data->id,
                                'ref_id'=>$d->id,
                                'from_trans_type'=>1
                            ]);
                            array_push($insertHpp,[
                                'acc_coa_detail_id'=>$d->coa_hpp_id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>1,
                                'amount'=>$amountHpp,
                                'created_at'=>$purchase->created_at,
                                'updated_at'=>$purchase->updated_at,
                                'acc_jurnal_id'=>$data->id,
                                'ref_id'=>$d->id,
                                'from_trans_type'=>1
                            ]);
                        }
                    }

                    $costAll+=$amountHpp;

                }
                if($d->is_bonus==0){
                    $sale+=$d->sub_total;
                }

                if($d->is_bonus==0)
                {
                    if(!is_null($d->cost_other))
                    {
                        $cost=(object)$d->cost_other;
                        foreach ($cost as $c)
                        {
                            $costOther[]=[
                                'id'=>$c->id,
                                'amount'=>$c->amount*$d->quantity*$d->value_conversation,
                            ];
                            $costAll+=$c->amount*$d->quantity*$d->value_conversation;
                        }
                    }
                }

                if($d->md_sc_product_type_id==4)
                {
                    if($d->is_bonus==0)
                    {
                        foreach (self::getRawMaterialWithStock($soId,$d->sc_product_id) as $m =>$mm)
                        {
                            $arrayProduct[]=[
                                'id'=>$mm->child_sc_product_id,
                                'total'=>$mm->amount*$mm->purchase_price,
                                'inv_id'=>$mm->inv_id,
                                'hpp_id'=>$mm->hpp_id,
                                'parent_id'=>$d->sc_product_id
                            ];
                            $costAll+=$mm->amount*$mm->purchase_price;

                        }

                    }
                }
                $costPerUnit=($costAll==0)?0:$costAll/$d->quantity;
                $updateDetails.=($dkey==0)?"(".$d->id.",".$costAll.",".$costPerUnit.")":",(".$d->id.",".$costAll.",".$costPerUnit.")";
            }


            if($updateDetails!=""){
                DB::statement("
                    update sc_sale_order_details as t set
                            total_cost = c.column_c,
                            cost_per_unit = c.column_e
                        from (values
                            $updateDetails
                        ) as c(column_b,column_c,column_e)
                        where c.column_b = t.id;
                    ");
            }

            if(!empty($arrayProduct))
            {
                $arrayProduct2=collect($arrayProduct)->groupBy('id');
                foreach ($arrayProduct2 as $cc2 =>$ccc2)
                {
                    array_push($insertInv,[
                        'acc_coa_detail_id'=>$ccc2->first()['inv_id'],
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$ccc2->sum('total'),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$ccc2->first()['id'],
                        'from_trans_type'=>1
                    ]);
                    array_push($insertHpp,[
                        'acc_coa_detail_id'=>$ccc2->first()['hpp_id'],
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$ccc2->sum('total'),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$ccc2->first()['id'],
                        'from_trans_type'=>1
                    ]);
                }
            }


            $insertHpp2=[];
            $insertInv2=[];
            if(!empty($insertHpp)){

                $inv2=collect($insertInv)->groupBy('acc_coa_detail_id');
                $hpp2=collect($insertHpp)->groupBy('acc_coa_detail_id');
                foreach ($inv2 as $iv =>$iv2)
                {
                    $first=$iv2->first();
                    array_push($insertInv2,[
                        'acc_coa_detail_id'=>$first['acc_coa_detail_id'],
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$first['amount'],
                        'created_at'=>$first['created_at'],
                        'updated_at'=>$first['updated_at'],
                        'acc_jurnal_id'=>$first['acc_jurnal_id'],
                        'ref_id'=>$first['ref_id'],
                        'from_trans_type'=>1
                    ]);
                }

                foreach ($hpp2 as $hp =>$hp2)
                {
                    $first=$hp2->first();
                    array_push($insertHpp2,[
                        'acc_coa_detail_id'=>$first['acc_coa_detail_id'],
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$first['amount'],
                        'created_at'=>$first['created_at'],
                        'updated_at'=>$first['updated_at'],
                        'acc_jurnal_id'=>$first['acc_jurnal_id'],
                        'ref_id'=>$first['ref_id'],
                        'from_trans_type'=>1
                    ]);
                }

            }

            if($purchase->promo!=0)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$discount,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->promo,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0

                ]);
            }

            $totalOther=0;
//            $insertCost=[];
//            if(!empty($costOther))
//            {
//                $costOther2=collect($costOther)->groupBy('id');
//
//                foreach ($costOther2 as $cc =>$ccc)
//                {
//                    array_push($insertCost,[
//                        'acc_coa_detail_id'=>$cc,
//                        'coa_type'=>'Kredit',
//                        'md_sc_currency_id'=>1,
//                        'amount'=>$ccc->sum('amount'),
//                        'created_at'=>$purchase->created_at,
//                        'updated_at'=>$purchase->updated_at,
//                        'acc_jurnal_id'=>$data->id,
//                        'ref_id'=>null,
//                        'from_trans_type'=>0
//                    ]);
//                    $totalOther+=$ccc->sum('amount');
//                }
//
//            }

            array_push($insert,[

                'acc_coa_detail_id'=>$saleCoa,
                'coa_type'=>'Kredit',
                'md_sc_currency_id'=>1,
                'amount'=>$sale-$totalOther,
                'created_at'=>$purchase->created_at,
                'updated_at'=>$purchase->updated_at,
                'acc_jurnal_id'=>$data->id,
                'ref_id'=>null,
                'from_trans_type'=>0
            ]);



            if($purchase->is_with_load_shipping==1){
                array_push($insert,[
                    'acc_coa_detail_id'=>$shipping,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->shipping_cost,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0
                ]);
            }

            JurnalDetail::insert($insert);
//            JurnalDetail::insert($insertCost);
            JurnalDetail::insert($insertHpp2);
            JurnalDetail::insert($insertInv2);


            if($purchase->is_with_load_shipping==1 && $purchase->is_debet==0){
                $dataShipingCost=new  Jurnal();
                $dataShipingCost->trans_code='JRN'.sprintf("%07s", $code+2);
                $dataShipingCost->ref_code=$purchase->code;
                $dataShipingCost->trans_name='Biaya Pengiriman Penjualan Produk '.$purchaseCode;
                $dataShipingCost->trans_time=$purchase->created_at;
                $dataShipingCost->trans_note='Biaya Pengiriman Penjualan Produk '.$purchaseCode;
                $dataShipingCost->trans_purpose='Penjualan Produk '.$purchaseCode;
                $dataShipingCost->trans_amount=$purchase->shipping_cost;
                $dataShipingCost->md_merchant_id=$merchantId;
                $dataShipingCost->md_user_id_created=$userId;
                $dataShipingCost->external_ref_id=$purchase->id;
                $dataShipingCost->flag_name='Biaya Pengiriman Penjualan Produk '.$purchaseCode;
                $dataShipingCost->save();
                if($coaTrans->name=='Kas' || $coaTrans->name=='Kas Kasir Outlet'){
                    $coaTrans1=$coaTrans->id;
                }else{
                    $coaTrans1=$merchant->coa_cash_id;
                }

                $insert2=[];
                array_push($insert2,
                    [
                        'acc_coa_detail_id'=>$shipping,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$dataShipingCost->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0

                    ]);

                array_push($insert2,
                    [
                        'acc_coa_detail_id'=>$coaTrans1,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$dataShipingCost->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                JurnalDetail::insert($insert2);
            }

            if($purchase->is_with_load_shipping==1 && $purchase->is_debet==1){
                $cash=$merchant->coa_cash_id;
                $dataShipingCost=new  Jurnal();
                $dataShipingCost->trans_code='JRN'.sprintf("%07s", $code+2);
                $dataShipingCost->ref_code=$purchase->code;
                $dataShipingCost->trans_name='Biaya Pengiriman Penjualan Produk '.$purchaseCode;
                $dataShipingCost->trans_time=$purchase->created_at;
                $dataShipingCost->trans_note='Biaya Pengiriman Penjualan Produk '.$purchaseCode;
                $dataShipingCost->trans_purpose='Penjualan Produk '.$purchaseCode;
                $dataShipingCost->trans_amount=$purchase->shipping_cost;
                $dataShipingCost->md_merchant_id=$merchantId;
                $dataShipingCost->md_user_id_created=$userId;
                $dataShipingCost->external_ref_id=$purchase->id;
                $dataShipingCost->flag_name='Biaya Pengiriman Penjualan Produk '.$purchaseCode;
                $dataShipingCost->save();
                $insert2=[];
                array_push($insert2,
                    [
                        'acc_coa_detail_id'=>$shipping,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$dataShipingCost->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0

                    ]);

                array_push($insert2,
                    [
                        'acc_coa_detail_id'=>$cash,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->shipping_cost,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$dataShipingCost->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                JurnalDetail::insert($insert2);
            }

            if(!empty($costOther))
            {
                $totalOther2=0;
                $costOther2=collect($costOther)->groupBy('id');
                $data4=new  Jurnal();
                $data4->trans_code='JRN'.sprintf("%07s", $code+1);
                $data4->ref_code=$purchase->code;
                $data4->trans_name='Biaya Atas Penjualan Produk '.$purchaseCode;
                $data4->trans_time=$purchase->created_at;
                $data4->trans_note='Biaya Atas Penjualan Produk '.$purchaseCode;
                $data4->trans_purpose='Biaya Atas Penjualan Produk '.$purchaseCode;
                $data4->trans_amount=$purchase->shipping_cost;
                $data4->md_merchant_id=$merchantId;
                $data4->md_user_id_created=$userId;
                $data4->external_ref_id=$purchase->id;
                $data4->flag_name='Biaya Atas Penjualan Produk '.$purchaseCode;
                $data4->save();
                $arrayCost=[];

                if($coaTrans->name=='Kas' || $coaTrans->name=='Kas Kasir Outlet'){
                    $coaTrans1=$coaTrans->id;
                }else{
                    $coaTrans1=$merchant->coa_cash_id;
                }
                foreach ($costOther2 as $cc =>$ccc)
                {

                    array_push($arrayCost,[
                        'acc_coa_detail_id'=>$cc,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$ccc->sum('amount'),
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data4->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                    $totalOther2+=$ccc->sum('amount');
                }

                array_push($arrayCost,
                    [
                        'acc_coa_detail_id'=>$coaTrans1,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$totalOther2,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data4->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);


                JurnalDetail::insert($arrayCost);

            }

            DB::commit();
            return true;
        }catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }



    public static function coaReturSaleOrderv2($userId,$merchantId,$coaJson,$retur,$time,$amountOfHpp)
    {
        try {

            DB::beginTransaction();
            $merchant=Merchant::find($merchantId);

            $payableAcc=$merchant->coa_ar_sale_id;
            $saleCoa=$merchant->coa_revenue_id;
            $data=new  Jurnal();
            $purchasrCode=is_null($retur->getSaleOrder->second_code)?$retur->getSaleOrder->code:$retur->getSaleOrder->second_code;
            $returCode=is_null($retur->second_code)?$retur->code:$retur->second_code;
            $data->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);
            $data->ref_code=$retur->code;
            $data->trans_name='Retur Penjualan '.$returCode.'-'.$purchasrCode;
            $data->trans_time=$time;
            $data->trans_note='Retur Penjualan '.$returCode.'-'.$purchasrCode;
            $data->trans_purpose='Retur Penjualan '.$returCode.'-'.$purchasrCode;
            $data->trans_amount=$retur->total;
            $data->md_merchant_id=$merchantId;
            $data->md_user_id_created=$userId;
            $data->external_ref_id=$retur->id;
            $data->timezone=$retur->timezone;
            $data->flag_name='Retur Penjualan '.$returCode.'-'.$purchasrCode;
            $data->save()
            ;
            $insert=[];
            if($retur->getSaleOrder->is_debet==1)
            {
                array_push($insert,[
                    'acc_coa_detail_id'=>$payableAcc,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$retur->total,
                    'created_at'=>$time,
                    'updated_at'=>$time,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>2
                ]);
            }

            if($retur->getSaleOrder->is_debet==0)
            {
                $coaPayable=$retur->getSaleOrder->coa_trans_id;

                array_push($insert,[
                    'acc_coa_detail_id'=>$coaPayable,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$retur->total,
                    'created_at'=>$time,
                    'updated_at'=>$time,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>2
                ]);
            }
            array_push($insert,[
                'acc_coa_detail_id'=>$saleCoa,
                'coa_type'=>'Debit',
                'md_sc_currency_id'=>1,
                'amount'=>$retur->total,
                'created_at'=>$time,
                'updated_at'=>$time,
                'acc_jurnal_id'=>$data->id,
                'ref_id'=>null,
                'from_trans_type'=>2
            ]);

            foreach ($retur->getDetail as $item)
            {

                if(!is_null($item->coa_inv_id))
                {
                    $total=0;
                    foreach ($amountOfHpp as $h)
                    {
                        if($item->sc_product_id==$h['id'])
                        {
                            $total+=$h['total'];
                        }
                    }
                    array_push($insert,[
                        'acc_coa_detail_id'=>$item->coa_inv_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$total,
                        'created_at'=>$time,
                        'updated_at'=>$time,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$item->id,
                        'from_trans_type'=>2
                    ]);

                    array_push($insert,[
                        'acc_coa_detail_id'=>$item->coa_hpp_id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$total,
                        'created_at'=>$time,
                        'updated_at'=>$time,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$item->id,
                        'from_trans_type'=>1
                    ]);
                }
            }

            JurnalDetail::insert($insert);
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;

        }
    }



    public static function getCoaSaleOrder($merchantId)
    {
        return DB::select("select acd.id,acd.code,acd.name,
                case when acd.code='1.1.01.01' then 'kas'
                when acd.code='1.1.03.04' then 'piutang'
                when acd.code='2.1.04.01' then 'ppn'
                when acd.code='4.1.00.01' then 'penjualan'
                when acd.code='6.1.02.06' then 'admin'
                when acd.code='6.1.02.21' then 'pengiriman'
                else 'diskon'
                end as type
                from acc_coa_details acd
                join acc_coa_categories acc
                on acd.acc_coa_category_id=acc.id
                where acc.md_merchant_id=$merchantId
                and acd.code in('6.1.03.01','2.1.04.01','1.1.03.04','4.1.00.01','6.1.02.21','6.1.02.06','1.1.01.01')");
    }

    public static  function coaInvSaleOrder($merchantId)
    {
        return DB::select("select acc.id,acc.code,acc.name,
            case when acc.code='1.1.05' then 'persediaan'
            else 'hpp' end as type
            from acc_coa_categories acc
            where acc.code in('1.1.05','5.0.00') and acc.md_merchant_id=$merchantId");
    }

    private  static function  getData($purchaseId)
    {
        return DB::select("
        SELECT sco.id,
            sco.code,
            sco.second_code,
            sco.ref_code,
            sco.sc_customer_id,
            sco.total,
            sco.md_sc_transaction_status_id,
            sco.created_at,
            sco.updated_at,
            sco.promo,
            sco.admin_fee,
            sco.qr_code,
            sco.is_from_senna_app,
            sco.md_transaction_type_id,
            sco.md_merchant_id,
            sco.latitude,
            sco.longitude,
            sco.resi_code,
            sco.courier_name,
            sco.service_courier_name,
            sco.destination,
            sco.shipping_cost,
            sco.is_approved_shop,
            sco.is_cancel_user,
            sco.is_approved_driver,
            sco.queue_code,
            sco.md_sc_shipping_category_id,
            sco.other_information,
            sco.md_sc_order_status_id,
            sco.is_rating,
            sco.distance,
            sco.weight,
            sco.is_rating_driver,
            sco.md_user_id_driver,
            sco.is_type,
            sco.is_debet,
            sco.paid_nominal,
            sco.created_by,
            sco.change_money,
            sco.is_with_retur,
            sco.is_active_transaction,
            sco.tax,
            sco.is_deleted,
            sco.timezone,
            sco.sync_id,
            sco.tax_percentage,
            sco.promo_percentage,
            sco.assign_to_user_id,
            sco.digital_struck,
            sco.struck_file,
            sco.shortest_link,
            sco.unit_name,
            sco.coa_trans_id,
            sco.is_with_load_shipping,
            sco.is_keep_transaction,
            sco.is_editable,
            sco.cost_other,
            sco.cost_other_sale,
            sco.total_cost_other_sale,
            ( SELECT jsonb_agg(jd.*) AS jsonb_agg
                   FROM ( SELECT spod.*,
                                 sp.cost_other,
                                 sp.md_sc_product_type_id,
                                 sp.is_with_stock
                           FROM sc_sale_order_details spod
                           join sc_products as sp on sp.id=spod.sc_product_id
                          WHERE spod.sc_sale_order_id = sco.id AND  spod.is_deleted = 0) jd)
                AS get_detail,
             ( SELECT jsonb_agg(jd.*) AS jsonb_agg
                   FROM ( SELECT spod.*
                           FROM sc_stock_sale_mappings spod
                          WHERE spod.sc_sale_order_id = sco.id) jd) AS get_sale_mapping
             FROM sc_sale_orders sco
          WHERE sco.id=$purchaseId
        ");
    }

    private static function getRawMaterialWithStock($saleOrderId,$parentId)
    {
        return StockSaleMapping::select([
            'r.child_sc_product_id',
            'r.parent_sc_product_id',
            'sc_stock_sale_mappings.amount',
            'sc_stock_sale_mappings.purchase_price',
            'p.inv_id',
            'p.hpp_id',
        ])
            ->join('sc_product_raw_materials as r','r.child_sc_product_id','sc_stock_sale_mappings.sc_product_id')
            ->join('sc_products as p','p.id','r.child_sc_product_id')
            ->where('sc_stock_sale_mappings.sc_sale_order_id',$saleOrderId)
            ->where('r.is_deleted',0)
            ->where('p.is_with_stock',1)
            ->where('r.parent_sc_product_id',$parentId)
            ->get();
    }





}

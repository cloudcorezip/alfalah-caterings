<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Accounting;


use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\StockInventory;
use Illuminate\Support\Facades\DB;

class CoaProductUtilV2
{
    public static function inventoryAdjustment($userId,$merchantId,$product,$time,$type='initial',$total=0)
    {
        try{
            if($product->stock!=0)
            {

            $fromCoa=$product->inv_id;
            $merchant=Merchant::find($merchantId);
            $toCoa=$merchant->coa_stock_adjustment_id;
            DB::beginTransaction();
                $code=Jurnal::where('md_merchant_id',$merchantId)
                        ->count()+1;
                if($type=='initial')
                {
                    $transName='Inisiasi Awal Produk '.$product->name;
                    $transNote='Inisiasi Produk Awal';
                    $flagName='Inisiasi Awal Produk '.$product->name;
                    $transAmount=$product->stock*$product->purchase_price;
                }elseif($type=="plus")
                {
                    $transName='Penambahan Penyesuaian Stok Produk  '.$product->name;
                    $transNote='Penambahan Penyesuaian Stok Produk ';
                    $flagName='Penambahan Penyesuaian Stok Produk  '.$product->name;
                    $transAmount=$total;

                }else{
                    $transName='Pengurangan Penyesuaian Stok Produk  '.$product->name;
                    $transNote='Pengurangan Penyesuaian Stok Produk ';
                    $flagName='Pengurangan Penyesuaian Stok Produk  '.$product->name;
                    $transAmount=$total;

                }
                $data=new  Jurnal();
                $data->trans_code='JRN'.sprintf("%07s", $code);
                $data->ref_code='INV'.sprintf("%07s", $code);
                $data->trans_name=$transName;
                $data->trans_time=$time;
                $data->trans_note=$transNote;
                $data->trans_purpose=$transNote;
                $data->trans_amount=$transAmount;
                $data->md_merchant_id=$merchantId;
                $data->md_user_id_created=$userId;
                $data->flag_name=$flagName;
                $data->save();

                if($type=='initial')
                {
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$fromCoa,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$product->stock*$product->purchase_price,
                            'created_at'=>$time,
                            'updated_at'=>$time,
                            'acc_jurnal_id'=>$data->id
                        ],
                        [
                            'acc_coa_detail_id'=>$toCoa,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$product->stock*$product->purchase_price,
                            'created_at'=>$time,
                            'updated_at'=>$time,
                            'acc_jurnal_id'=>$data->id
                        ],
                    ];
                }elseif($type=='plus')
                {
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$fromCoa,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$total,
                            'created_at'=>$time,
                            'updated_at'=>$time,
                            'acc_jurnal_id'=>$data->id
                        ],
                        [
                            'acc_coa_detail_id'=>$toCoa,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$total,
                            'created_at'=>$time,
                            'updated_at'=>$time,
                            'acc_jurnal_id'=>$data->id
                        ],
                    ];
                }else{
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$fromCoa,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$total,
                            'created_at'=>$time,
                            'updated_at'=>$time,
                            'acc_jurnal_id'=>$data->id
                        ],
                        [
                            'acc_coa_detail_id'=>$toCoa,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$total,
                            'created_at'=>$time,
                            'updated_at'=>$time,
                            'acc_jurnal_id'=>$data->id
                        ],
                    ];
                }
                JurnalDetail::insert($insert);
            }
            DB::commit();

            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,'Error Subtract',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }
    }




}

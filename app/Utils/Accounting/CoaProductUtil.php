<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Accounting;


use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\SennaToko\StockInventory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class CoaProductUtil
{

    public static function initialCoaProduct($merchantId,$product,$type)
    {
        try{
            $coaInv=collect(CoaSaleUtil::coaInvSaleOrder($merchantId));
            $inventoryAccount=$coaInv->firstWhere('type','persediaan')->id;
            $hppAccount=$coaInv->firstWhere('type','hpp')->id;
            $timestamp=date('Y-m-d H:i:s');
            DB::beginTransaction();

            $invPId=CoaDetail::insertGetId(
                [
                    'code' =>CoaUtil::generateCoaDetail($hppAccount),
                    'name' => 'Harga Pokok Penjualan Produk '.$product->name,
                    'md_sc_currency_id' => 1,
                    'type_coa' => 'Debit',
                    'acc_coa_category_id' =>$hppAccount,
                    'parent_id' => NULL,
                    'created_at' =>$timestamp,
                    'updated_at' => $timestamp,
                    'ref_external_id'=>$product->id,
                    'ref_external_type'=>$type
                ]);

            $coaId=CoaDetail::insertGetId( [
                'code' =>CoaUtil::generateCoaDetail($inventoryAccount),
                'name' => 'Persediaan Produk '.$product->name,
                'md_sc_currency_id' => 1,
                'type_coa' => 'Debit',
                'acc_coa_category_id' =>$inventoryAccount,
                'parent_id' => NULL,
                'created_at' =>$timestamp,
                'updated_at' => $timestamp,
                'ref_external_id'=>$product->id,
                'ref_external_type'=>$type
            ]);
            DB::table('acc_coa_cashflow_format_details')
                ->insert([
                    'acc_coa_cashflow_format_id'=>CoaCashflowFormat::where([
                        'name'=>'Persediaan',
                        'type'=>'operasional',
                        'md_merchant_id'=>$merchantId
                    ])->first()->id,
                    'acc_coa_detail_id'=>$coaId,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
            DB::commit();

            return [
                'inv_id'=>$coaId,
                'hpp_id'=>$invPId
            ];

        }catch (\Exception $e)
        {
            DB::rollBack();
            return  false;

        }

    }
    public static function initialCoaVarian($merchantId,$varianId,$type,$varianName,$productName)
    {
        try{
            $coaInv=collect(CoaSaleUtil::coaInvSaleOrder($merchantId));
            $inventoryAccount=$coaInv->firstWhere('type','persediaan')->id;
            $hppAccount=$coaInv->firstWhere('type','hpp')->id;
            $timestamp=date('Y-m-d H:i:s');
            DB::beginTransaction();
            $invPId=CoaDetail::insertGetId(
                [
                    'code' =>CoaUtil::generateCoaDetail($hppAccount),
                    'name' => 'Harga Pokok Penjualan Produk '.$productName.' '.$varianName,
                    'md_sc_currency_id' => 1,
                    'type_coa' => 'Debit',
                    'acc_coa_category_id' =>$hppAccount,
                    'parent_id' => NULL,
                    'created_at' =>$timestamp,
                    'updated_at' => $timestamp,
                    'ref_external_id'=>$varianId,
                    'ref_external_type'=>$type
                ]);

            $coaId=CoaDetail::insertGetId( [
                'code' =>CoaUtil::generateCoaDetail($inventoryAccount),
                'name' => 'Persediaan Produk '.$productName.' '.$varianName,
                'md_sc_currency_id' => 1,
                'type_coa' => 'Debit',
                'acc_coa_category_id' =>$inventoryAccount,
                'parent_id' => NULL,
                'created_at' =>$timestamp,
                'updated_at' => $timestamp,
                'ref_external_id'=>$varianId,
                'ref_external_type'=>$type
            ]);
            DB::table('acc_coa_cashflow_format_details')
                ->insert([
                    'acc_coa_cashflow_format_id'=>CoaCashflowFormat::where([
                        'name'=>'Persediaan',
                        'type'=>'operasional',
                        'md_merchant_id'=>$merchantId
                    ])->first()->id,
                    'acc_coa_detail_id'=>$coaId,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
            DB::commit();

            return [
                'inv_id'=>$invPId,
                'hpp_id'=>$coaId
            ];

        }catch (\Exception $e)
        {
            DB::rollBack();
            return  false;

        }

    }
    public static function inventoryAdjustment($userId,$merchantId,$product,$time,$type='initial',$amount=0,$total=0)
    {
        try{
            $coaInv=$product->inv_id;
            if($type=='initial' || $type=='plus') {

                $fromCoa=$coaInv;
                $toCoa=CoaDetail::where('acc_coa_details.code','6.1.03.04')
                    ->select([
                        'acc_coa_details.*'
                    ])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$merchantId)->first();;
            }else{

                $toCoa=CoaDetail::where('acc_coa_details.code','6.1.03.04')
                    ->select([
                        'acc_coa_details.*'
                    ])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$merchantId)->first();;;
                $fromCoa=$coaInv;
            }

            DB::beginTransaction();
            $code=Jurnal::where('md_merchant_id',$merchantId)
                    ->count()+1;

            if($type=='initial')
            {
                if($product->stock!=0)
                {
                    $data=new  Jurnal();
                    $data->trans_code='JRN'.sprintf("%07s", $code);
                    $data->ref_code='INV'.sprintf("%07s", $code);
                    $data->trans_name='Inisiasi Awal Produk '.$product->name;
                    $data->trans_time=$time;
                    $data->trans_note='Inisiasi Produk Awal';
                    $data->trans_purpose='Inisiasi Produk Awal';
                    $data->trans_amount=$product->stock*$product->purchase_price;
                    $data->md_merchant_id=$merchantId;
                    $data->md_user_id_created=$userId;
                    $data->flag_name='Inisiasi Awal Produk '.$product->name;
                    $data->save();
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$fromCoa->id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>$fromCoa->md_sc_currency_id,
                            'amount'=>$product->stock*$product->purchase_price,
                            'created_at'=>$time,
                            'updated_at'=>$time,
                            'acc_jurnal_id'=>$data->id
                        ],
                        [
                            'acc_coa_detail_id'=>$toCoa->id,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>$toCoa->md_sc_currency_id,
                            'amount'=>$product->stock*$product->purchase_price,
                            'created_at'=>$time,
                            'updated_at'=>$time,
                            'acc_jurnal_id'=>$data->id
                        ],
                    ];
                    JurnalDetail::insert($insert);

                }
            }elseif ($type=='plus')
            {
                $data=new  Jurnal();
                $data->trans_code='JRN'.sprintf("%07s", $code);
                $data->ref_code='INV'.sprintf("%07s", $code);
                $data->trans_name='Penambahan Penyesuaian Stok Produk '.$product->name;
                $data->trans_time=$time;
                $data->trans_note='Penambahan Penyesuaian Stok Produk';
                $data->trans_purpose='Penambahan Penyesuaian Stok Produk';
                $data->trans_amount=$amount*$total;
                $data->md_merchant_id=$merchantId;
                $data->md_user_id_created=$userId;
                $data->flag_name='Penambahan Penyesuaian Stok Produk '.$product->name;
                $data->save();
                $insert=[
                    [
                        'acc_coa_detail_id'=>$fromCoa->id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>$fromCoa->md_sc_currency_id,
                        'amount'=>$amount*$total,
                        'created_at'=>$time,
                        'updated_at'=>$time,
                        'acc_jurnal_id'=>$data->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa->id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>$toCoa->md_sc_currency_id,
                        'amount'=>$amount*$total,
                        'created_at'=>$time,
                        'updated_at'=>$time,
                        'acc_jurnal_id'=>$data->id
                    ],
                ];
                JurnalDetail::insert($insert);

            }else{
                $data=new  Jurnal();
                $data->trans_code='JRN'.sprintf("%07s", $code);
                $data->ref_code='INV'.sprintf("%07s", $code);
                $data->trans_name='Pengurangan Penyesuaian Stok Produk '.$product->name;
                $data->trans_time=$time;
                $data->trans_note='Pengurangan Penyesuaian Stok Produk';
                $data->trans_purpose='Pengurangan Penyesuaian Stok Produk';
                $data->trans_amount=$amount*$total;
                $data->md_merchant_id=$merchantId;
                $data->md_user_id_created=$userId;
                $data->flag_name='Pengurangan Penyesuaian Stok Produk '.$product->name;
                $data->save();
                $insert=[
                    [
                        'acc_coa_detail_id'=>$fromCoa->id,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>$fromCoa->md_sc_currency_id,
                        'amount'=>$amount*$total,
                        'created_at'=>$time,
                        'updated_at'=>$time,
                        'acc_jurnal_id'=>$data->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa->id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>$toCoa->md_sc_currency_id,
                        'amount'=>$amount*$total,
                        'created_at'=>$time,
                        'updated_at'=>$time,
                        'acc_jurnal_id'=>$data->id
                    ],
                ];
                JurnalDetail::insert($insert);

            }
            DB::commit();

            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return $e;
        }
    }


    public static function checkInitialProductCoa($merchantId,$productId)
    {
        try{
            $coaInv=DB::select("
            select
              acd.*
            from
              acc_coa_details acd
              join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
            where
              acc.code = '1.1.05'
              and acc.md_merchant_id = $merchantId
              and acd.ref_external_type = 1
              and acd.ref_external_id = $productId
            ");
            if(empty($coaInv))
            {
                return false;
            }else{
                return true;
            }
        }catch (\Exception $e)
        {
            return false;
        }

    }

    public static function createStockOpnameBulk($userId,$adjustment,$merchant,$products)
    {
        try{
            $toCoa=$merchant->coa_stock_adjustment_id;

            DB::beginTransaction();
            $code=Jurnal::where('md_merchant_id',$merchant->id)
                    ->count()+1;
            $inserts=[];
            $adjustmentCode=is_null($adjustment->second_code)?$adjustment->code:$adjustment->second_code;

            foreach ($products as $key =>$item)
            {
                $jurnalId=Jurnal::insertGetId([
                    'trans_code'=>'JRN'.sprintf("%07s", $code+1),
                    'ref_code'=>$adjustment->code,
                    'trans_name'=>($item['type']=='plus')?
                        'Penambahan Stok Produk '.$item['name'].' Dari Stok Opname '.$adjustmentCode:
                        'Pengurangan Stok Produk '.$item['name'].' Dari Stok Opname '.$adjustmentCode,
                    'trans_note'=>($item['type']=='plus')?
                        'Penambahan Stok Produk '.$item['name'].' Dari Stok Opname '.$adjustmentCode:
                        'Pengurangan Stok Produk '.$item['name'].' Dari Stok Opname '.$adjustmentCode,
                    'trans_purpose'=>($item['type']=='plus')?
                        'Penambahan Stok Produk '.$item['name'].' Dari Stok Opname '.$adjustmentCode:
                        'Pengurangan Stok Produk '.$item['name'].' Dari Stok Opname '.$adjustmentCode,
                    'flag_name'=>($item['type']=='plus')?
                        'Penambahan Stok Produk '.$item['name'].' Dari Stok Opname '.$adjustmentCode:
                        'Pengurangan Stok Produk '.$item['name'].' Dari Stok Opname '.$adjustmentCode,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'trans_amount'=>$item['amount'],
                    'md_merchant_id'=>$merchant->id,
                    'md_user_id_created'=>$userId,
                    'external_ref_id'=>$adjustment->id,
                    'timezone'=>$adjustment->timezone,
                    'trans_time'=>$adjustment->created_at_by
                ]);

                if($item['type']=='plus'){

                    array_push($inserts,[
                        'acc_coa_detail_id'=>$item['inv_id'],
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item['amount'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnalId
                    ]);
                    array_push($inserts,[
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item['amount'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnalId
                    ]);
                }else{

                    array_push($inserts,[
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item['amount'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnalId
                    ]);

                    array_push($inserts,[
                        'acc_coa_detail_id'=>$item['inv_id'],
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item['amount'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnalId
                    ]);
                }

            }

            JurnalDetail::insert($inserts);

            DB::commit();

            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }


    public static function createStockAdjustmentBulk($userId,$adjustment,$merchant,$products)
    {
        try{
            DB::beginTransaction();
            $code=Jurnal::where('md_merchant_id',$merchant->id)
                    ->count()+1;
            $inserts=[];
            $toCoa=$merchant->coa_stock_adjustment_id;
            $adjustmentCode=is_null($adjustment->second_code)?$adjustment->code:$adjustment->second_code;

            foreach ($products as $key =>$item)
            {
                $jurnalId=Jurnal::insertGetId([
                    'trans_code'=>'JRN'.sprintf("%07s", $code+1),
                    'ref_code'=>$adjustment->code,
                    'trans_name'=>($item['type']=='plus')?
                        'Penambahan Stok Produk '.$item['name'].' Dari Penyesuaian Stok '.$adjustmentCode:
                        'Pengurangan Stok Produk '.$item['name'].' Dari Penyesuaian Stok '.$adjustmentCode,
                    'trans_note'=>($item['type']=='plus')?
                        'Penambahan Stok Produk '.$item['name'].' Dari Penyesuaian Stok '.$adjustmentCode:
                        'Pengurangan Stok Produk '.$item['name'].' Dari Penyesuaian Stok '.$adjustmentCode,
                    'trans_purpose'=>($item['type']=='plus')?
                        'Penambahan Stok Produk '.$item['name'].' Dari Penyesuaian Stok '.$adjustmentCode:
                        'Pengurangan Stok Produk '.$item['name'].' Dari Penyesuaian Stok '.$adjustmentCode,
                    'flag_name'=>($item['type']=='plus')?
                        'Penambahan Stok Produk '.$item['name'].' Dari Penyesuaian Stok '.$adjustmentCode:
                        'Pengurangan Stok Produk '.$item['name'].' Dari Penyesuaian Stok '.$adjustmentCode,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                    'trans_amount'=>$item['amount'],
                    'md_merchant_id'=>$merchant->id,
                    'md_user_id_created'=>$userId,
                    'external_ref_id'=>$adjustment->id,
                    'timezone'=>$adjustment->timezone,
                    'trans_time'=>$adjustment->created_at_by
                ]);

                if($item['type']=='plus'){

                    array_push($inserts,[
                        'acc_coa_detail_id'=>$item['inv_id'],
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item['amount'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnalId
                    ]);
                    array_push($inserts,[
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item['amount'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnalId
                    ]);
                }else{

                    array_push($inserts,[
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item['amount'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnalId
                    ]);

                    array_push($inserts,[
                        'acc_coa_detail_id'=>$item['inv_id'],
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item['amount'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnalId
                    ]);
                }

            }

            JurnalDetail::insert($inserts);

            DB::commit();

            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Accounting;


use App\Models\Accounting\Period;
use Illuminate\Support\Facades\DB;

class AccUtil
{

    public  static function checkClosingJournal($merchantId,$beforeYear)
    {

        try{
            $period=collect(DB::select("

           select
              id
            from
              acc_closing_journal_distributions
            where
              '".$beforeYear."' between start_date :: date
              and end_date :: date
              and md_merchant_id = $merchantId
              and is_deleted = 0
            limit
              1
            "));

            if($period->count()>0)
            {
                return true;
            }else{
                return  false;
            }
        }catch (\Exception $e)
        {
            return false;
        }

    }

}

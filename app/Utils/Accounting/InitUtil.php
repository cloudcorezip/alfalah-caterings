<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\Accounting;


use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaMapping;
use App\Models\MasterData\Merchant;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class InitUtil
{

    public static function initCoaToMerchantDetailUtil($merchantId)
    {
        try {
            $merchant=Merchant::find($merchantId);

            if(is_null($merchant->coa_debt_asset_id))
            {
                $coa=collect(DB::select("
                select acd.id,acd.code,acd.name from acc_coa_details acd
                    join acc_coa_categories acc
                    on acd.acc_coa_category_id=acc.id
                    where acd.code in(
                    '1.1.01.01',
                    '6.1.02.21',
                    '6.1.03.01',
                    '2.1.04.01',
                    '4.1.00.01',
                    '1.1.03.04',
                    '6.1.02.06',
                    '1.1.06.01',
                    '4.3.00.03',
                    '2.1.01.03',
                    '1.3.01.03',
                    '3.2.00.02',
                    '2.1.01.04',
                    '3.0.00.01',
                    '6.1.01.04',
                    '6.1.03.04',
                    '3.1.00.01'
                    ) and acc.md_merchant_id=".$merchant->id."
                    order by acd.code asc
                "));

                if(count($coa)<17)
                {
                    $coa=collect(DB::select("
                        select
                          acd.id,
                          acd.code,
                          acd.name
                        from
                          acc_coa_details acd
                          join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                        where
                          acd.code in(
                            '1.1.01.01', '4.1.00.01', '1.1.03.04',
                            '1.1.06.01', '2.1.01.03', '1.3.01.03',
                            '2.1.01.04', '2.1.02.01', '3.0.00.01',
                            '3.1.00.01', '3.2.00.02', '4.2.00.03',
                            '6.1.00.01', '6.1.00.07', '6.1.00.022',
                            '6.2.00.01', '6.2.00.04'
                          )
                          and acc.md_merchant_id = ".$merchant->id."
                        order by
                          acd.code asc
                        "));
                    $merchant->coa_cash_id=$coa->firstWhere('code','1.1.01.01')->id;
                    $merchant->coa_shipping_cost_id=$coa->firstWhere('code','6.1.00.022')->id;
                    $merchant->coa_administration_bank_id=$coa->firstWhere('code','6.1.00.07')->id;
                    $merchant->coa_debt_ppn_out_id=$coa->firstWhere('code','2.1.02.01')->id;
                    $merchant->coa_transaction_discount_sale_id=$coa->firstWhere('code','6.2.00.01')->id;
                    $merchant->coa_ar_sale_id=$coa->firstWhere('code','1.1.03.04')->id;
                    $merchant->coa_revenue_id=$coa->firstWhere('code','4.1.00.01')->id;
                    $merchant->coa_tax_ppn_in_id=$coa->firstWhere('code','1.1.06.01')->id;
                    $merchant->coa_ap_purchase_id=$coa->firstWhere('code','2.1.01.03')->id;
                    $merchant->coa_transaction_discount_purchase_id=$coa->firstWhere('code','4.2.00.03')->id;
                    $merchant->coa_stock_adjustment_id=$coa->firstWhere('code','6.2.00.04')->id;
                    $merchant->coa_initial_asset_id=$coa->firstWhere('code','1.3.01.03')->id;
                    //laba tahun berjalan
                    $merchant->coa_profit_sale_asset_id=$coa->firstWhere('code','3.2.00.02')->id;
                    $merchant->coa_debt_asset_id=$coa->firstWhere('code','2.1.01.04')->id;
                    $merchant->coa_resume_profit_lost_id=$coa->firstWhere('code','3.0.00.01')->id;
                    $merchant->coa_production_cost_id=$coa->firstWhere('code','6.1.00.01')->id;
                    $merchant->save();
                }else{
                    $merchant->coa_cash_id=$coa->firstWhere('code','1.1.01.01')->id;
                    $merchant->coa_shipping_cost_id=$coa->firstWhere('code','6.1.02.21')->id;
                    $merchant->coa_administration_bank_id=$coa->firstWhere('code','6.1.02.06')->id;
                    $merchant->coa_debt_ppn_out_id=$coa->firstWhere('code','2.1.04.01')->id;
                    $merchant->coa_transaction_discount_sale_id=$coa->firstWhere('code','6.1.03.01')->id;
                    $merchant->coa_ar_sale_id=$coa->firstWhere('code','1.1.03.04')->id;
                    $merchant->coa_revenue_id=$coa->firstWhere('code','4.1.00.01')->id;
                    $merchant->coa_tax_ppn_in_id=$coa->firstWhere('code','1.1.06.01')->id;
                    $merchant->coa_ap_purchase_id=$coa->firstWhere('code','2.1.01.03')->id;
                    $merchant->coa_transaction_discount_purchase_id=$coa->firstWhere('code','4.3.00.03')->id;
                    $merchant->coa_stock_adjustment_id=$coa->firstWhere('code','6.1.03.04')->id;
                    $merchant->coa_initial_asset_id=$coa->firstWhere('code','1.3.01.03')->id;
                    //laba tahun berjalan
                    $merchant->coa_profit_sale_asset_id=$coa->firstWhere('code','3.2.00.02')->id;
                    $merchant->coa_debt_asset_id=$coa->firstWhere('code','2.1.01.04')->id;
                    $merchant->coa_resume_profit_lost_id=$coa->firstWhere('code','3.0.00.01')->id;
                    $merchant->coa_production_cost_id=$coa->firstWhere('code','6.1.01.04')->id;
                    $merchant->save();
                }


            }
            if(is_null($merchant->coa_capital_id))
            {
                $coa=collect(DB::select("
                select acd.id,acd.code,acd.name from acc_coa_details acd
                    join acc_coa_categories acc
                    on acd.acc_coa_category_id=acc.id
                    where acd.code in(
                    '1.1.01.01',
                    '6.1.02.21',
                    '6.1.03.01',
                    '2.1.04.01',
                    '4.1.00.01',
                    '1.1.03.04',
                    '6.1.02.06',
                    '1.1.06.01',
                    '4.3.00.03',
                    '2.1.01.03',
                    '1.3.01.03',
                    '3.2.00.02',
                    '2.1.01.04',
                    '3.0.00.01',
                    '6.1.01.04',
                    '6.1.03.04',
                     '3.1.00.01'
                    ) and acc.md_merchant_id=".$merchant->id."
                    order by acd.code asc
                "));
                if(count($coa)<17){
                    $coa=collect(DB::select("
                        select
                          acd.id,
                          acd.code,
                          acd.name
                        from
                          acc_coa_details acd
                          join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                        where
                          acd.code in(
                            '1.1.01.01', '4.1.00.01', '1.1.03.04',
                            '1.1.06.01', '2.1.01.03', '1.3.01.03',
                            '2.1.01.04', '2.1.02.01', '3.0.00.01',
                            '3.1.00.01', '3.2.00.02', '4.2.00.03',
                            '6.1.00.01', '6.1.00.07', '6.1.00.022',
                            '6.2.00.01', '6.2.00.04'
                          )
                          and acc.md_merchant_id = ".$merchant->id."
                        order by
                          acd.code asc
                        "));
                    $merchant->coa_debt_asset_id=$coa->firstWhere('code','2.1.01.04')->id;
                    $merchant->coa_capital_id=$coa->firstWhere('code','3.1.00.01')->id;
                    $merchant->save();
                }else{
                    $merchant->coa_debt_asset_id=$coa->firstWhere('code','2.1.01.04')->id;
                    $merchant->coa_capital_id=$coa->firstWhere('code','3.1.00.01')->id;
                    $merchant->save();
                }
            }
            return true;
        }catch (\Exception $e)
        {
            log_helper(self::class,'Error',$e->getLine(),$e->getFile(),$e->getMessage());

            return true;
        }
    }

    public  static function generateNewCoa($merchantId)
    {
        try{
            if(CoaCategory::whereNull('parent_code')->where('md_merchant_id',$merchantId)->count()>0){
                self::reinitAllCoa($merchantId);
                $coa=CoaCategory::where('is_deleted',0)
                    ->whereNull('parent_id')
                    ->where('md_merchant_id',$merchantId)
                    ->orderBy('id','asc')
                    ->get();
                foreach ($coa as $key =>$item)
                {
                    $item->code=($key+1).'.0'.'.00';
                    $item->parent_code=($key+1);
                    $item->name=self::replaceStringName($item->name);
                    $item->save();
                    foreach ($item->getChild as $key2 =>$item2)
                    {
                        $item2->code=($key+1).'.'.($key2+1).'.00';
                        $item2->parent_code=($key+1);
                        $item2->name=self::replaceStringName($item2->name);
                        $item2->save();

                        if($item2->getDetail->count()>0)
                        {
                            foreach ($item2->getDetail as $d2 =>$dd2)
                            {
                                $dd2->code=($key+1).'.'.($key2+1).'.00.0'.($d2+1);
                                $dd2->name=self::replaceStringName($dd2->name);
                                $dd2->save();
                            }
                        }

                        foreach ($item2->getChild as $key3 =>$item3)
                        {
                            $item3->code=($key+1).'.'.($key2+1).'.0'.($key3+1);
                            $item3->name=self::replaceStringName($item3->name);
                            $item3->parent_code=($key+1);
                            $item3->save();

                            if($item3->getDetail->count()>0)
                            {
                                foreach ($item3->getDetail as $d3 =>$dd3)
                                {
                                    $dd3->code=($key+1).'.'.($key2+1).'.0'.($key3+1).'.0'.($d3+1);
                                    $dd3->name=self::replaceStringName($dd3->name);
                                    $dd3->save();
                                }
                            }
                        }
                    }
                    if($item->getDetail->count()>0)
                    {
                        foreach ($item->getDetail as $d =>$dd)
                        {
                            $dd->code=($key+1).'.0.00.0'.($d+1);
                            $dd->name=self::replaceStringName($dd->name);
                            $dd->save();
                        }
                    }
                }

                self::generateNewMappingAndCashflowFormat($merchantId);

            }

            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }

    public  static function generateNewCoaMobile($userId)
    {
        try{
            DB::beginTransaction();
            $merchant=Merchant::where('md_user_id',$userId)->first();
            $merchantId=$merchant->id;
            if(CoaCategory::whereNull('parent_code')->where('md_merchant_id',$merchantId)->count()>0){
                self::reinitAllCoa($merchantId);
                $coa=CoaCategory::where('is_deleted',0)
                    ->whereNull('parent_id')
                    ->where('md_merchant_id',$merchantId)
                    ->orderBy('id','asc')
                    ->get();
                foreach ($coa as $key =>$item)
                {
                    $item->code=($key+1).'.0'.'.00';
                    $item->parent_code=($key+1);
                    $item->name=self::replaceStringName($item->name);
                    $item->save();
                    foreach ($item->getChild as $key2 =>$item2)
                    {
                        $item2->code=($key+1).'.'.($key2+1).'.00';
                        $item2->parent_code=($key+1);
                        $item2->name=self::replaceStringName($item2->name);
                        $item2->save();

                        if($item2->getDetail->count()>0)
                        {
                            foreach ($item2->getDetail as $d2 =>$dd2)
                            {
                                $dd2->code=($key+1).'.'.($key2+1).'.00.0'.($d2+1);
                                $dd2->name=self::replaceStringName($dd2->name);
                                $dd2->save();
                            }
                        }

                        foreach ($item2->getChild as $key3 =>$item3)
                        {
                            $item3->code=($key+1).'.'.($key2+1).'.0'.($key3+1);
                            $item3->name=self::replaceStringName($item3->name);
                            $item3->parent_code=($key+1);
                            $item3->save();

                            if($item3->getDetail->count()>0)
                            {
                                foreach ($item3->getDetail as $d3 =>$dd3)
                                {
                                    $dd3->code=($key+1).'.'.($key2+1).'.0'.($key3+1).'.0'.($d3+1);
                                    $dd3->name=self::replaceStringName($dd3->name);
                                    $dd3->save();
                                }
                            }
                        }
                    }
                    if($item->getDetail->count()>0)
                    {
                        foreach ($item->getDetail as $d =>$dd)
                        {
                            $dd->code=($key+1).'.0.00.0'.($d+1);
                            $dd->name=self::replaceStringName($dd->name);
                            $dd->save();
                        }
                    }
                }

                self::generateNewMappingAndCashflowFormat($merchantId);

            }
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

    public static function generateNewMappingAndCashflowFormat($merchantId)
    {
        try{
            DB::beginTransaction();
            $check=CoaCashflowFormat::where('md_merchant_id',$merchantId)->count();
            $timestamp=date('Y-m-d H:i:s');
            if($check<1)
            {
                DB::table('acc_coa_cashflow_formats')
                    ->insert([
                        [
                            'name' => 'Pendapatan',
                            'type' => 'operasional',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp

                        ],
                        [
                            'name' => 'Pendapatan Lainnya',
                            'type' => 'operasional',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'name' => 'Beban Operasional & Usaha',
                            'type' => 'operasional',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp

                        ],
                        [
                            'name' => 'Beban Lainnya',
                            'type' => 'operasional',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'name' => 'Pembelian/Penjualan Aset',
                            'type' => 'investasi',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'name' => 'Aktivitas Investasi Lainnya',
                            'type' => 'investasi',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'name' => 'Pembayaran/Penerimaan Pinjaman',
                            'type' => 'pendanaan',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'name' => 'Modal',
                            'type' => 'pendanaan',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'name' => 'Persediaan',
                            'type' => 'operasional',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                    ]);
            }

            $checkMapping=CoaMapping::where('md_merchant_id',$merchantId)->count();
            if($checkMapping==0)
            {
                $categoryMapping=collect(DB::select("
                select
                  id,
                  name,
                  (
                    case when name = 'Utang'
                    or name = 'Utang Lainnya' then 4 when name = 'Piutang' then 5 when name = 'Pendapatan Lainnya' then 3 when name = 'Beban Operasional & Usaha'
                    or name = 'Beban Lainnya' then 2 else 0 end
                  ) as type_mapping
                from
                  acc_coa_categories
                where
                  md_merchant_id = $merchantId
                  and name in(
                   'Piutang', 'Utang',
                    'Utang Lainnya', 'Pendapatan Lainnya',
                    'Beban Operasional & Usaha', 'Beban Lainnya'
                  )
                order by
                  id asc
                "));

                foreach ($categoryMapping as $key =>$item)
                {
                    DB::statement("
                insert into acc_coa_mappings(name,is_type,md_merchant_id,acc_coa_detail_id)
                select name,".$item->type_mapping." as is_type,$merchantId,id from acc_coa_details where acc_coa_category_id=".$item->id."
                and is_deleted=0 and name not in('Piutang Pembelian','Piutang Penjualan','Utang Saldo Deposit','Utang Pembelian Aset','Utang Pembelian Barang','Utang Direksi/Pemegang Saham','Utang Dividen','Utang Bank Jangka Pendek','Retur Pembelian','Diskon Pembelian Barang','Operasional Produksi','Diskon Penjualan','Beban Penjualan Aset','Penyesuaian Persediaan')
                ");

                }

                DB::statement("

                insert into acc_coa_cashflow_format_details(
                      acc_coa_cashflow_format_id, acc_coa_detail_id
                    )
                    select
                      (
                        select
                          id
                        from
                          acc_coa_cashflow_formats
                        where
                          md_merchant_id = $merchantId
                          and name = 'Pendapatan'
                      ) as cash_code,
                      acd.id
                    from
                      acc_coa_details acd
                      join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                    where
                      acd.name in(
                        'Utang Saldo Deposit', 'Utang PPN Keluaran',
                        'Penjualan', 'Diskon Pembelian Barang'
                      )
                      and acc.md_merchant_id = $merchantId

                ");

                DB::statement("
                insert into acc_coa_cashflow_format_details(
                  acc_coa_cashflow_format_id, acc_coa_detail_id
                )
                select
                  (
                    select
                      id
                    from
                      acc_coa_cashflow_formats
                    where
                      md_merchant_id = $merchantId
                      and name = 'Pendapatan Lainnya'
                  ) as cash_code,
                  acd.id
                from
                  acc_coa_details acd
                  join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                where
                  acc.name='Pendapatan Lainnya'
                  and acc.md_merchant_id = $merchantId
                  and acd.name not in('Diskon Pembelian Barang')

                ");

                DB::statement("
                insert into acc_coa_cashflow_format_details(
                  acc_coa_cashflow_format_id, acc_coa_detail_id
                )
                 select
                  (
                    select
                      id
                    from
                      acc_coa_cashflow_formats
                    where
                      md_merchant_id = $merchantId
                      and name = 'Beban Operasional & Usaha'
                  ) as cash_code,
                  acd.id
                from
                  acc_coa_details acd
                  join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                where
                  acc.name='Beban Operasional & Usaha'
                  and acc.md_merchant_id = $merchantId

                ");

                DB::statement("
                 insert into acc_coa_cashflow_format_details(
                  acc_coa_cashflow_format_id, acc_coa_detail_id
                )
                 select
                  (
                    select
                      id
                    from
                      acc_coa_cashflow_formats
                    where
                      md_merchant_id = $merchantId
                      and name = 'Beban Operasional & Usaha'
                  ) as cash_code,
                  acd.id
                from
                  acc_coa_details acd
                  join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                where
                  acd.name='Utang Pembelian Barang'
                  and acc.md_merchant_id = $merchantId
                ");

                DB::statement("
                 insert into acc_coa_cashflow_format_details(
                  acc_coa_cashflow_format_id, acc_coa_detail_id
                )
                select
                  (
                    select
                      id
                    from
                      acc_coa_cashflow_formats
                    where
                      md_merchant_id = $merchantId
                      and name = 'Beban Lainnya'
                  ) as cash_code,
                  acd.id
                from
                  acc_coa_details acd
                  join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                where
                  acc.name='Beban Lainnya'
                  and acc.md_merchant_id = $merchantId
                ");

                DB::statement("
                 insert into acc_coa_cashflow_format_details(
                  acc_coa_cashflow_format_id, acc_coa_detail_id
                )
                 select
                  (
                    select
                      id
                    from
                      acc_coa_cashflow_formats
                    where
                      md_merchant_id = $merchantId
                      and name = 'Pembelian/Penjualan Aset'
                  ) as cash_code,
                  acd.id
                from
                  acc_coa_details acd
                  join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                where
                  acd.name in('Tanah','Bangunan','Kendaraan','Mesin & Peralatan','Perlengkapan Kantor','Sewa Guna Usaha')
                  and acc.md_merchant_id = $merchantId

                ");

                DB::statement("
                   insert into acc_coa_cashflow_format_details(
                  acc_coa_cashflow_format_id, acc_coa_detail_id
                )
                 select
              (
                select
                  id
                from
                  acc_coa_cashflow_formats
                where
                  md_merchant_id = $merchantId
                  and name = 'Aktivitas Investasi Lainnya'
              ) as cash_code,
              acd.id
            from
              acc_coa_details acd
              join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
            where
              acd.name in('Investasi Jangka Pendek','Investasi Jangka Panjang')
              and acc.md_merchant_id = $merchantId
                ");

                DB::statement("
                 insert into acc_coa_cashflow_format_details(
                  acc_coa_cashflow_format_id, acc_coa_detail_id
                )
                 select
                  (
                    select
                      id
                    from
                      acc_coa_cashflow_formats
                    where
                      md_merchant_id = $merchantId
                      and name = 'Pembayaran/Penerimaan Pinjaman'
                  ) as cash_code,
                  acd.id
                from
                  acc_coa_details acd
                  join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                where
                  acd.name in('Piutang Usaha','Piutang Lainnya','Utang Usaha','Utang Lainnya')
                  and acc.md_merchant_id = $merchantId
                ");

                DB::statement("
                 insert into acc_coa_cashflow_format_details(
                  acc_coa_cashflow_format_id, acc_coa_detail_id
                )
                 select
                  (
                    select
                      id
                    from
                      acc_coa_cashflow_formats
                    where
                      md_merchant_id = $merchantId
                      and name = 'Modal'
                  ) as cash_code,
                  acd.id
                from
                  acc_coa_details acd
                  join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                where
                  acd.name in('Modal Disetor','Modal Saham','Saldo Laba Ditahan','Saldo Laba Tahun Berjalan','Dividen')
                  and acc.md_merchant_id = $merchantId
                ");
            }
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;

        }

    }

    private static function reinitAllCoa($merchantId)
    {
        try {
            DB::beginTransaction();
            DB::statement("delete from acc_coa_mappings where md_merchant_id=$merchantId");
            DB::statement("delete from acc_coa_cashflow_format_details where acc_coa_cashflow_format_id in(select id from acc_coa_cashflow_formats where md_merchant_id=$merchantId)");
            DB::statement("delete from acc_coa_cashflow_formats where md_merchant_id=$merchantId");
            DB::statement("
            update acc_coa_categories
set name='Aktiva Lancar Lainnya'
where name='Pajak Bayar Dimuka'
and md_merchant_id=$merchantId
            ");

            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Biaya Bayar Dimuka')
            ");

            DB::statement("delete from acc_coa_categories
where md_merchant_id=$merchantId
and name='Biaya Bayar Dimuka'");
            DB::statement("delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Biaya Pokok Produksi')
");

            DB::statement("
            delete from acc_coa_categories
where md_merchant_id=$merchantId
and name='Biaya Pokok Produksi'
            ");

            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Persediaan') and name in (
'Persediaan Bahan Baku',
'Persediaan Barang Dalam Proses',
'Persediaan Barang Jadi',
'Persediaan Barang Dalam Transfer',
'Persediaan Barang Dalam Retur')
            ");

            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Aktiva Tidak Berwujud')
            ");

            DB::statement("
            delete from acc_coa_categories
where md_merchant_id=$merchantId
and name='Aktiva Tidak Berwujud'
            ");
            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Pendapatan Diterima Dimuka')
            ");
            DB::statement("
            delete from acc_coa_categories
where md_merchant_id=$merchantId
and name='Pendapatan Diterima Dimuka'");
            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name in('Aktiva Lancar Lainnya','Utang Pajak'))
and name in('Pajak Lebih Bayar','Pajak Kurang Bayar','Utang PPh 4 (2)')
            ");

            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Biaya Terutang')
            ");

            DB::statement("
            delete from acc_coa_categories
where md_merchant_id=$merchantId
and name='Biaya Terutang'
            ");

            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Utang Lainnya')
            ");
            DB::statement("
            delete from acc_coa_categories
where md_merchant_id=$merchantId
and name='Utang Lainnya'
            ");


            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Beban Operasional') and name not in('Operasional Produksi Barang')
            ");

            DB::statement("
            update acc_coa_details
            set acc_coa_category_id=(select id from acc_coa_categories
            where name='Beban Administrasi & Umum' and md_merchant_id=$merchantId
            )
            from acc_coa_categories
            where acc_coa_categories.id=acc_coa_details.acc_coa_category_id
            and acc_coa_categories.md_merchant_id=$merchantId
            and acc_coa_details.name='Operasional Produksi Barang'
            ");

            DB::statement("
            delete from acc_coa_categories
where md_merchant_id=$merchantId
and name='Beban Operasional'
            ");

            DB::statement("
            update acc_coa_details
set acc_coa_category_id=(select id from acc_coa_categories
where name in('Pendapatan Lain-lain','Pendapatan Lainnya') and md_merchant_id=$merchantId
)
from acc_coa_categories
where acc_coa_categories.id=acc_coa_details.acc_coa_category_id
and acc_coa_categories.md_merchant_id=$merchantId
and acc_coa_details.name in('Pendapatan Diluar Usaha','Laba Penjualan Aset')
            ");

            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name in('Pendapatan Lain-lain','Pendapatan Lainnya')) and name='Retur Pembelian Barang'
            ");

            DB::statement("
            update acc_merchant_cashflows
set cashflow_coa_detail_id=(
select acd.id from acc_coa_details acd
join acc_coa_categories acc
on acd.acc_coa_category_id=acc.id
where acd.name in('Pendapatan Lain-Lain','Pendapatan Lainnya') and acc.md_merchant_id=$merchantId
)
from acc_coa_details
where acc_merchant_cashflows.cashflow_coa_detail_id=acc_coa_details.id
and acc_coa_details.name='Pendapatan Diluar Usaha'
and acc_merchant_cashflows.md_merchant_id=$merchantId
            ");

            DB::statement("
            update acc_jurnal_details
set acc_coa_detail_id=(
select acd.id from acc_coa_details acd
join acc_coa_categories acc
on acd.acc_coa_category_id=acc.id
where acd.name in('Pendapatan Lain-Lain','Pendapatan Lainnya') and acc.md_merchant_id=$merchantId
)
from acc_coa_details
where acc_jurnal_details.acc_coa_detail_id=acc_coa_details.id
and acc_jurnal_details.acc_jurnal_id in(
select id from acc_jurnals where md_merchant_id=$merchantId
) and acc_coa_details.name='Pendapatan Diluar Usaha'

            ");

            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name in('Pendapatan Lain-lain','Pendapatan Lainnya')) and name='Pendapatan Diluar Usaha'
            ");

            DB::statement("
            delete from acc_coa_details where acc_coa_category_id in
(select id from acc_coa_categories
where md_merchant_id=$merchantId
and name='Beban Administrasi & Umum') and name='Biaya Amortisasi dari Biaya Pra Operasi'
            ");

            DB::statement("

            delete from acc_coa_categories where name='Pendapatan Diluar Usaha'
and md_merchant_id=$merchantId
            ");

            DB::statement("
            update acc_coa_categories
set parent_id=(select id from acc_coa_categories where md_merchant_id=$merchantId and name='Beban')
where name in('Beban Lainnya','Beban Lain-lain','Beban Lain-Lain','Beban Administrasi & Umum','Beban Operasional & Usaha')
and md_merchant_id=$merchantId
            ");

            DB::statement("delete from acc_coa_categories where md_merchant_id=$merchantId and name='Beban Usaha'");

            DB::statement("delete from acc_coa_categories where md_merchant_id=$merchantId and name='Utang Komisi'");

            $checkAktivaWujud=CoaCategory::where('name','Aktiva Tidak Berwujud')
                ->where('md_merchant_id',$merchantId)->first();
            $parent=CoaCategory::where('name','Aktiva Tidak Lancar')
                ->where('md_merchant_id',$merchantId)->first();
            if(is_null($checkAktivaWujud))
            {
                DB::table('acc_coa_categories')
                    ->insert([
                        [
                            'code' => '1.2.03',
                            'name' => 'Aktiva Tidak Berwujud',
                            'parent_id' => $parent->id,
                            'md_merchant_id' => $merchantId,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]
                    ]);
            }


            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;

        }
    }

    private static function replaceStringName($name)
    {
        $array=[
            'Piutang Transaksi Produk'=>'Piutang Penjualan',
            'Piutang Pembelian Barang'=>'Piutang Pembelian',
            'Pajak PPN Masukan'=>'PPN Masukan',
            'Penjualan Transaksi Produk'=>'Penjualan',
            'Operasional Produksi Barang'=>'Operasional Produksi',
            'Pendapatan Lain-lain'=>'Pendapatan Lainnya',
            'Pendapatan Lain-Lain'=>'Pendapatan Lainnya',
            'Beban Lain-lain'=>'Beban Lainnya',
            'Beban Lain-Lain'=>'Beban Lainnya',
            'Piutang Lain-lain'=>'Piutang Lainnya',
            'Piutang Lain-Lain'=>'Piutang Lainnya',
            'Utang Lain-lain'=>'Utang Lainnya',
            'Utang Lain-Lain'=>'Utang Lainnya',
            'Diskon Transaksi'=>'Diskon Penjualan',
            'Rekening Perantara'=>'Ayat Silang',
            'Rekening Perantara Inisialisasi Persediaan Barang'=>'Ayat Silang Inisialisasi Persediaan Barang',
            'Rekening Perantara Inisialisasi Aset'=>'Ayat Silang Inisialisasi Aset',
            'Beban Administrasi & Umum'=>'Beban Operasional & Usaha',
            'Beban Administrasi & Usaha'=>'Beban Operasional & Usaha',
            'Beban lainnya'=>'Beban Lainnya'
        ];

        if(array_key_exists($name,$array))
        {
            return $array[$name];
        }else{
            return str_replace('Biaya ','',$name);
        }
    }

    public static function initCoaToMerchantDetailUtilMobile($userId)
    {
        try {
            $merchant=Merchant::where('md_user_id',$userId)->first();

            if(!is_null($merchant))
            {
                if(is_null($merchant->coa_debt_asset_id))
                {
                    $coa=collect(DB::select("
                select acd.id,acd.code,acd.name from acc_coa_details acd
                    join acc_coa_categories acc
                    on acd.acc_coa_category_id=acc.id
                    where acd.code in(
                    '1.1.01.01',
                    '6.1.02.21',
                    '6.1.03.01',
                    '2.1.04.01',
                    '4.1.00.01',
                    '1.1.03.04',
                    '6.1.02.06',
                    '1.1.06.01',
                    '4.3.00.03',
                    '2.1.01.03',
                    '1.3.01.03',
                    '3.2.00.02',
                    '2.1.01.04',
                    '3.0.00.01',
                    '6.1.01.04',
                    '6.1.03.04'
                    ) and acc.md_merchant_id=".$merchant->id."
                    order by acd.code asc
                "));

                    if(count($coa)<17)
                    {
                        $coa=collect(DB::select("
                        select
                          acd.id,
                          acd.code,
                          acd.name
                        from
                          acc_coa_details acd
                          join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                        where
                          acd.code in(
                            '1.1.01.01', '4.1.00.01', '1.1.03.04',
                            '1.1.06.01', '2.1.01.03', '1.3.01.03',
                            '2.1.01.04', '2.1.02.01', '3.0.00.01',
                            '3.1.00.01', '3.2.00.02', '4.2.00.03',
                            '6.1.00.01', '6.1.00.07', '6.1.00.022',
                            '6.2.00.01', '6.2.00.04'
                          )
                          and acc.md_merchant_id = ".$merchant->id."
                        order by
                          acd.code asc
                        "));
                        $merchant->coa_cash_id=$coa->firstWhere('code','1.1.01.01')->id;
                        $merchant->coa_shipping_cost_id=$coa->firstWhere('code','6.1.00.022')->id;
                        $merchant->coa_administration_bank_id=$coa->firstWhere('code','6.1.00.07')->id;
                        $merchant->coa_debt_ppn_out_id=$coa->firstWhere('code','2.1.02.01')->id;
                        $merchant->coa_transaction_discount_sale_id=$coa->firstWhere('code','6.2.00.01')->id;
                        $merchant->coa_ar_sale_id=$coa->firstWhere('code','1.1.03.04')->id;
                        $merchant->coa_revenue_id=$coa->firstWhere('code','4.1.00.01')->id;
                        $merchant->coa_tax_ppn_in_id=$coa->firstWhere('code','1.1.06.01')->id;
                        $merchant->coa_ap_purchase_id=$coa->firstWhere('code','2.1.01.03')->id;
                        $merchant->coa_transaction_discount_purchase_id=$coa->firstWhere('code','4.2.00.03')->id;
                        $merchant->coa_stock_adjustment_id=$coa->firstWhere('code','6.2.00.04')->id;
                        $merchant->coa_initial_asset_id=$coa->firstWhere('code','1.3.01.03')->id;
                        //laba tahun berjalan
                        $merchant->coa_profit_sale_asset_id=$coa->firstWhere('code','3.2.00.02')->id;
                        $merchant->coa_debt_asset_id=$coa->firstWhere('code','2.1.01.04')->id;
                        $merchant->coa_resume_profit_lost_id=$coa->firstWhere('code','3.0.00.01')->id;
                        $merchant->coa_production_cost_id=$coa->firstWhere('code','6.1.00.01')->id;
                        $merchant->save();
                    }else{
                        $merchant->coa_cash_id=$coa->firstWhere('code','1.1.01.01')->id;
                        $merchant->coa_shipping_cost_id=$coa->firstWhere('code','6.1.02.21')->id;
                        $merchant->coa_administration_bank_id=$coa->firstWhere('code','6.1.02.06')->id;
                        $merchant->coa_debt_ppn_out_id=$coa->firstWhere('code','2.1.04.01')->id;
                        $merchant->coa_transaction_discount_sale_id=$coa->firstWhere('code','6.1.03.01')->id;
                        $merchant->coa_ar_sale_id=$coa->firstWhere('code','1.1.03.04')->id;
                        $merchant->coa_revenue_id=$coa->firstWhere('code','4.1.00.01')->id;
                        $merchant->coa_tax_ppn_in_id=$coa->firstWhere('code','1.1.06.01')->id;
                        $merchant->coa_ap_purchase_id=$coa->firstWhere('code','2.1.01.03')->id;
                        $merchant->coa_transaction_discount_purchase_id=$coa->firstWhere('code','4.3.00.03')->id;
                        $merchant->coa_stock_adjustment_id=$coa->firstWhere('code','6.1.03.04')->id;
                        $merchant->coa_initial_asset_id=$coa->firstWhere('code','1.3.01.03')->id;
                        //laba tahun berjalan
                        $merchant->coa_profit_sale_asset_id=$coa->firstWhere('code','3.2.00.02')->id;
                        $merchant->coa_debt_asset_id=$coa->firstWhere('code','2.1.01.04')->id;
                        $merchant->coa_resume_profit_lost_id=$coa->firstWhere('code','3.0.00.01')->id;
                        $merchant->coa_production_cost_id=$coa->firstWhere('code','6.1.01.04')->id;
                        $merchant->save();
                    }


                }
                if(is_null($merchant->coa_capital_id))
                {
                    $coa=collect(DB::select("
                select acd.id,acd.code,acd.name from acc_coa_details acd
                    join acc_coa_categories acc
                    on acd.acc_coa_category_id=acc.id
                    where acd.code in(
                    '1.1.01.01',
                    '6.1.02.21',
                    '6.1.03.01',
                    '2.1.04.01',
                    '4.1.00.01',
                    '1.1.03.04',
                    '6.1.02.06',
                    '1.1.06.01',
                    '4.3.00.03',
                    '2.1.01.03',
                    '1.3.01.03',
                    '3.2.00.02',
                    '2.1.01.04',
                    '3.0.00.01',
                    '6.1.01.04',
                    '6.1.03.04',
                     '3.1.00.01'
                    ) and acc.md_merchant_id=".$merchant->id."
                    order by acd.code asc
                "));
                    if(count($coa)<17){
                        $coa=collect(DB::select("
                        select
                          acd.id,
                          acd.code,
                          acd.name
                        from
                          acc_coa_details acd
                          join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
                        where
                          acd.code in(
                            '1.1.01.01', '4.1.00.01', '1.1.03.04',
                            '1.1.06.01', '2.1.01.03', '1.3.01.03',
                            '2.1.01.04', '2.1.02.01', '3.0.00.01',
                            '3.1.00.01', '3.2.00.02', '4.2.00.03',
                            '6.1.00.01', '6.1.00.07', '6.1.00.022',
                            '6.2.00.01', '6.2.00.04'
                          )
                          and acc.md_merchant_id = ".$merchant->id."
                        order by
                          acd.code asc
                        "));
                        $merchant->coa_debt_asset_id=$coa->firstWhere('code','2.1.01.04')->id;
                        $merchant->coa_capital_id=$coa->firstWhere('code','3.1.00.01')->id;
                        $merchant->save();
                    }else{
                        $merchant->coa_debt_asset_id=$coa->firstWhere('code','2.1.01.04')->id;
                        $merchant->coa_capital_id=$coa->firstWhere('code','3.1.00.01')->id;
                        $merchant->save();
                    }
                }
            }

            return true;
        }catch (\Exception $e)
        {
            log_helper(self::class,'Error',$e->getLine(),$e->getFile(),$e->getMessage());

            return true;
        }
    }

}

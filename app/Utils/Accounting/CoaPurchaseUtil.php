<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Accounting;


use App\Classes\Singleton\CodeGenerator;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\ReturPurchaseOrderDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CoaPurchaseUtil
{

    public static function coaReturPurchaseOrderV2($userId,$merchantId,$retur,$time,$coaAp)
    {
        try {
            DB::beginTransaction();
            $purchase=PurchaseOrder::find($retur->sc_purchase_order_id);
            $data=new  Jurnal();
            $data->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);
            $returCode=is_null($retur->second_code)?$retur->code:$retur->second_code;
            $purchaseCode=is_null($purchase->second_code)?$purchase->code:$purchase->second_code;
            $data->ref_code=$retur->code;
            $data->trans_name='Retur Pembelian '.$returCode.'-'.$purchaseCode;
            $data->trans_time=$time;
            $data->trans_note='Retur Pembelian '.$returCode.'-'.$purchaseCode;
            $data->trans_purpose='Retur Pembelian '.$returCode.'-'.$purchaseCode;
            $data->trans_amount=$retur->total;
            $data->md_merchant_id=$merchantId;
            $data->md_user_id_created=$userId;
            $data->external_ref_id=$retur->id;
            $data->timezone=$purchase->timezone;
            $data->flag_name='Retur Pembelian '.$returCode.'-'.$purchaseCode;
            $data->save();

            $returId=$retur->id;
            $detailRetur=collect(DB::select("
            select
                   srpod.*,
                   p.inv_id as coa_inv_retur_id,
                   p.hpp_id as coa_hpp_retur_id
                from
                  sc_retur_purchase_order_details srpod
                  left join sc_products p on p.id=srpod.sc_product_id
                  where
                  srpod.sc_retur_purchase_order_id = $returId
            "));

            $insert=[];

            foreach ($detailRetur as $item)
            {
                //return $detailRetur;
                $insert[]=[
                    'acc_coa_detail_id'=>$item->coa_inv_retur_id,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$item->sub_total,
                    'created_at'=>$time,
                    'updated_at'=>$time,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>$item->id,
                    'from_trans_type'=>2
                ];
            }
            JurnalDetail::insert($insert);

            if($purchase->is_debet==1)
            {
                $coaPayable=$coaAp;

                JurnalDetail::insert([
                    'acc_coa_detail_id'=>$coaPayable,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$retur->total,
                    'created_at'=>$time,
                    'updated_at'=>$time,
                    'acc_jurnal_id'=>$data->id,
                    'from_trans_type'=>2
                ]);

            }

            if($purchase->is_debet==0)
            {
                $coaPayable=$purchase->coa_trans_id;

                JurnalDetail::insert([
                    'acc_coa_detail_id'=>$coaPayable,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$retur->total,
                    'created_at'=>$time,
                    'updated_at'=>$time,
                    'acc_jurnal_id'=>$data->id,
                    'from_trans_type'=>2
                ]);

            }
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;

        }
    }


    public static function coaStockAdjustmentFromPurchasev2($userId,$merchantId,$coaJson,$purchase,$isEdit=0)
    {
        try{
            DB::beginTransaction();
            $merchant=Merchant::find($merchantId);
            $discount=$merchant->coa_transaction_discount_purchase_id;
            $tax=$merchant->coa_tax_ppn_in_id;
            $payableAcc=$merchant->coa_ap_purchase_id;

            $coaTrans=$purchase->coa_trans_id;
            if($isEdit==0)
            {
                $data=new  Jurnal();
                $data->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);

            }else{
                $data=Jurnal::where('ref_code',$purchase->code)
                    ->where('external_ref_id',$purchase->id)
                    ->first();
                JurnalDetail::where('acc_jurnal_id',$data->id)
                    ->delete();

            }
            $poCode=is_null($purchase->second_code)?$purchase->code:$purchase->second_code;

            $data->ref_code=$purchase->code;
            $data->trans_name='Pembelian '.$poCode;
            $data->trans_time=$purchase->created_at;
            $data->trans_note='Pembelian  '.$poCode;
            $data->trans_purpose='Pembelian '.$poCode;
            $data->trans_amount=$purchase->total;
            $data->md_merchant_id=$merchantId;
            $data->md_user_id_created=$userId;
            $data->external_ref_id=$purchase->id;
            $data->flag_name='Pembelian Produk '.$poCode;
            $data->timezone=$purchase->timezone;
            $data->save();
            $insert=[];
            foreach ($purchase->getDetail as $item)
            {
                if($item->is_deleted==0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$item->coa_inv_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item->sub_total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$item->id,
                        'from_trans_type'=>1
                    ]);
                }
            }
            if($purchase->is_debet==0)
            {
                if($purchase->discount!=0 && $purchase->tax!=0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$discount,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->discount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                    array_push($insert,
                        [
                            'acc_coa_detail_id'=>$tax,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$purchase->tax,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>null,
                            'from_trans_type'=>0
                        ]);

                    array_push($insert,[
                        'acc_coa_detail_id'=>$coaTrans,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                }elseif ($purchase->discount==0 && $purchase->tax!=0){
                    array_push($insert,
                        [
                            'acc_coa_detail_id'=>$tax,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$purchase->tax,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>null,
                            'from_trans_type'=>0
                        ]);


                    array_push($insert,[
                        'acc_coa_detail_id'=>$coaTrans,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                }elseif ($purchase->discount!=0 && $purchase->tax==0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$discount,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->discount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                    array_push($insert,[
                        'acc_coa_detail_id'=>$coaTrans,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);
                } else{
                    array_push($insert,[
                        'acc_coa_detail_id'=>$coaTrans,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);
                }
            }else{
                if($purchase->discount!=0 && $purchase->tax!=0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$discount,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->discount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                    array_push($insert,[
                        'acc_coa_detail_id'=>$tax,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->tax,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                }elseif ($purchase->discount==0 && $purchase->tax!=0){
                    array_push($insert,[
                        'acc_coa_detail_id'=>$tax,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->tax,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                }elseif ($purchase->discount!=0 && $purchase->tax==0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$discount,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->discount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);
                }
                array_push($insert,[
                    'acc_coa_detail_id'=>$payableAcc,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->total,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0
                ]);
            }
            JurnalDetail::insert($insert);
            DB::commit();
            return true;
        }catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }


    public static function coaStockAdjustmentFromPurchasev2Mobile($userId,$merchantId,$coaJson,$purchase,$isEdit=0)
    {
        try{
            DB::beginTransaction();
            $merchant=Merchant::find($merchantId);
            $discount=$merchant->coa_transaction_discount_purchase_id;
            $tax=$merchant->coa_tax_ppn_in_id;
            $payableAcc=$merchant->coa_ap_purchase_id;

            $coaTrans=$purchase->coa_trans_id;
            if($isEdit==0)
            {
                $data=new  Jurnal();
                $data->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);

            }else{
                $data=Jurnal::where('ref_code',$purchase->code)
                    ->where('external_ref_id',$purchase->id)
                    ->first();
                JurnalDetail::where('acc_jurnal_id',$data->id)
                    ->delete();

            }
            $data->ref_code=$purchase->code;
            $data->trans_name='Pembelian '.$purchase->code;
            $data->trans_time=$purchase->created_at;
            $data->trans_note='Pembelian  '.$purchase->code;
            $data->trans_purpose='Pembelian '.$purchase->code;
            $data->trans_amount=$purchase->total;
            $data->md_merchant_id=$merchantId;
            $data->md_user_id_created=$userId;
            $data->external_ref_id=$purchase->id;
            $data->flag_name='Pembelian Produk '.$purchase->code;
            $data->timezone=$purchase->timezone;
            $data->save();
            $insert=[];
            foreach ($purchase->getDetail as $item)
            {
                if($item->is_deleted==0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$item->coa_inv_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$item->sub_total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>$item->id,
                        'from_trans_type'=>1
                    ]);
                }
            }
            if($purchase->is_debet==0)
            {
                if($purchase->discount!=0 && $purchase->tax!=0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$discount,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->discount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                    array_push($insert,
                        [
                            'acc_coa_detail_id'=>$tax,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$purchase->tax,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>null,
                            'from_trans_type'=>0
                        ]);

                    array_push($insert,[
                        'acc_coa_detail_id'=>$coaTrans,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                }elseif ($purchase->discount==0 && $purchase->tax!=0){
                    array_push($insert,
                        [
                            'acc_coa_detail_id'=>$tax,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$purchase->tax,
                            'created_at'=>$purchase->created_at,
                            'updated_at'=>$purchase->updated_at,
                            'acc_jurnal_id'=>$data->id,
                            'ref_id'=>null,
                            'from_trans_type'=>0
                        ]);


                    array_push($insert,[
                        'acc_coa_detail_id'=>$coaTrans,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                }elseif ($purchase->discount!=0 && $purchase->tax==0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$discount,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->discount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                    array_push($insert,[
                        'acc_coa_detail_id'=>$coaTrans,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);
                } else{
                    array_push($insert,[
                        'acc_coa_detail_id'=>$coaTrans,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->total,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);
                }
            }else{
                if($purchase->discount!=0 && $purchase->tax!=0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$discount,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->discount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                    array_push($insert,[
                        'acc_coa_detail_id'=>$tax,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->tax,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                }elseif ($purchase->discount==0 && $purchase->tax!=0){
                    array_push($insert,[
                        'acc_coa_detail_id'=>$tax,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->tax,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);

                }elseif ($purchase->discount!=0 && $purchase->tax==0)
                {
                    array_push($insert,[
                        'acc_coa_detail_id'=>$discount,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$purchase->discount,
                        'created_at'=>$purchase->created_at,
                        'updated_at'=>$purchase->updated_at,
                        'acc_jurnal_id'=>$data->id,
                        'ref_id'=>null,
                        'from_trans_type'=>0
                    ]);
                }
                array_push($insert,[
                    'acc_coa_detail_id'=>$payableAcc,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$purchase->total,
                    'created_at'=>$purchase->created_at,
                    'updated_at'=>$purchase->updated_at,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>null,
                    'from_trans_type'=>0
                ]);
            }
            JurnalDetail::insert($insert);
            DB::commit();
            return true;
        }catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }



    public static function getCoaPurchaseOrder($merchantId)
    {
        return DB::select("select acd.id,acd.code,acd.name,
                case when acd.code='4.3.00.03' then 'diskon'
                when acd.code='1.1.06.01' then 'pajak'
                else 'hutang'
                end as type
                from acc_coa_details acd
                join acc_coa_categories acc
                on acd.acc_coa_category_id=acc.id
                where acc.md_merchant_id=$merchantId
                and acd.code in('4.3.00.03','1.1.06.01','2.1.01.03')");
    }

    public static  function coaInvPurchaseOrder($merchantId)
    {
        return DB::select("select acc.id,acc.code,acc.name,
            case when acc.code='1.1.05' then 'persediaan'
            else 'hpp' end as type
            from acc_coa_categories acc
            where acc.code in('1.1.05','5.0.00') and acc.md_merchant_id=$merchantId");
    }



}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\Accounting;


use App\Models\Accounting\Asset\AssetDepreciation;
use App\Models\Accounting\Asset\AssetDepreciationPayment;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\SennaToko\ConsignmentGood;
use App\Models\SennaToko\MerchantCommissionList;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\StockOpname;
use App\Models\SennaToko\TransferStock;

class FindCodeUtil
{
    public static function findCode($code,$merchantId,$type='purchase',$step=0,$id=null)
    {
        if($type=='purchase')
        {
            $check=PurchaseOrder::where('second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('step_type',$step)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }

        }elseif ($type=='sell')
        {
            $check=SaleOrder::where('second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('step_type',$step)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='asset')
        {
            $check=AssetDepreciation::where('second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='asset_payment')
        {
            $check=AssetDepreciationPayment::where('acc_asset_depreciation_payments.second_code',$code)
                ->join('acc_asset_depreciations as a','a.id','acc_asset_depreciation_payments.acc_asset_depreciation_id')
                ->where('a.md_merchant_id',$merchantId);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('acc_asset_depreciation_payments.id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='jurnal')
        {
            $check=Jurnal::where('second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='ap')
        {
            $check=MerchantAp::where('second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }

        }elseif ($type=='ap_detail')
        {
            $check=MerchantApDetail::where('acc_merchant_ap_details.second_code',$code)
                ->join('acc_merchant_ap as ap','ap.id','acc_merchant_ap_details.acc_merchant_ap_id')
                ->where('ap.md_merchant_id',$merchantId)
                ->where('acc_merchant_ap_details.is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('acc_merchant_ap_details.id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }
        elseif ($type=='ar')
        {
            $check=MerchantAr::where('second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }

        }elseif ($type=='ar_detail')
        {
            $check=MerchantArDetail::where('acc_merchant_ar_details.second_code',$code)
                ->join('acc_merchant_ar as ap','ap.id','acc_merchant_ar_details.acc_merchant_ar_id')
                ->where('ap.md_merchant_id',$merchantId)
                ->where('acc_merchant_ar_details.is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('acc_merchant_ar_details.id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='stock_opnames')
        {
            $check=StockOpname::where('second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('is_adjustment_stock',$step)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='transfer_stock')
        {
            $check=TransferStock::where('second_code',$code)
                ->where('from_merchant_id',$merchantId)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='consignment')
        {
            $check=ConsignmentGood::where('second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='asset_sell'){
            $check=AssetDepreciation::where('sell_second_code',$code)
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='po_retur'){
            $check=ReturPurchaseOrder::where('sc_retur_purchase_orders.second_code',$code)
                ->join('sc_purchase_orders as p','p.id','sc_retur_purchase_orders.sc_purchase_order_id')
                ->where('p.md_merchant_id',$merchantId)
                ->where('sc_retur_purchase_orders.is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('sc_retur_purchase_orders.id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='so_retur'){
            $check=ReturSaleOrder::where('sc_retur_sale_orders.second_code',$code)
                ->join('sc_sale_orders as p','p.id','sc_retur_sale_orders.sc_sale_order_id')
                ->where('p.md_merchant_id',$merchantId)
                ->where('sc_retur_sale_orders.is_deleted',0);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('sc_retur_sale_orders.id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }elseif ($type=='commission_code'){
            $check=MerchantCommissionList::where('second_code',$code)->where('md_merchant_id',$merchantId);
            if(is_null($id))
            {
                if(!is_null($check->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }else{
                if(!is_null($check->where('id','!=',$id)->first()))
                {
                    return true;

                }else{
                    return false;
                }
            }
        }
    }
}

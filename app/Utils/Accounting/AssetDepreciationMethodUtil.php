<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Accounting;


class AssetDepreciationMethodUtil
{



    public static function calculate($assetValue,$benefitPeriod,$depreciationValue,$isType)
    {
        if($isType==1)
        {
            return self::straightLine($assetValue,$depreciationValue,$benefitPeriod);
        }else{
            return self::doubleDecliningBalance($assetValue,$benefitPeriod);

        }
    }


    private static function doubleDecliningBalance($value,$benefitPeriod)
    {
        $doubleDecline=[];
        $accDepr=0;
        $ddRate = ((1.0 / $benefitPeriod) * 2) * 100;
        $assetValue=$value;
        $loop=0;
        for ($i=0;$i<$benefitPeriod;$i++)
        {
            $yearlyDepr = $assetValue * ($ddRate / 100);
            $accDepr +=$yearlyDepr;
            for ($j=0;$j<12;$j++)
            {
                $doubleDecline[]=($loop==$benefitPeriod-1)?
                    ($yearlyDepr/12)+($value-$accDepr)/12
                    :$yearlyDepr/12;
            }
            $assetValue -=$yearlyDepr;
            $loop++;
        }

        return $doubleDecline;

    }

    private static function straightLine($value,$valueDepreciation,$benefitPeriod)
    {
        $assetValue=($value*$valueDepreciation)/12;

        $straightLine=[];
        for($i=0;$i<$benefitPeriod*12;$i++)
        {
            $straightLine[]=$assetValue;
        }

        return $straightLine;

    }

}

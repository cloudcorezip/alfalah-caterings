<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Accounting;


use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaMapping;
use App\Models\MasterData\Merchant;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CoaUtil
{

    public static function generateCodeCoaCategory($id)
    {
        try{
            $data=CoaCategory::find($id);
            if($data)
            {
                $coaCode=$data->parent_code.'.'.($data->getChild->count()+1).'.00';

                return $coaCode;
            }else{
                return  false;
            }
        }catch (\Exception $e)
        {
            return false;

        }
    }

    public static function generateCoaDetail($id)
    {
        try{
            $data=CoaCategory::find($id);
            if($data){
                if($data->getDetail->count()>0)
                {
                    $coaCode=$data->code.'.0'.($data->getDetail->count()+1);
                }else{
                    $coaCode=$data->code.'.01';
                }
                return $coaCode;
            }else{
                return false;
            }
        }catch (\Exception $e)
        {
            return false;
        }
    }

    public static function generateFromInitial($id,$incremental)
    {
        try{
            $data=CoaCategory::find($id);
            $coaCode=$data->code.'.0'.$incremental;
            return $coaCode;
        }catch (\Exception $e)
        {
            return false;
        }
    }


    public static function checkAmountOfCoa($trans,$merchantId)
    {
        $firstDate=Carbon::now()->firstOfMonth();
        $lastDate=Carbon::now()->lastOfMonth();
        $fields = [
            'acc_coa_details.*',
            DB::raw("
                coalesce(
                (select sum(amount) from acc_jurnal_details d
                  join acc_jurnals j
                on j.id=d.acc_jurnal_id
                 where d.acc_coa_detail_id=acc_coa_details.id and coa_type='Debit' and j.md_merchant_id=$merchantId
                 and j.is_deleted in(0,2) and j.trans_time::date<'".$firstDate->toDateString()."'
                 ),0
                ) as sum_of_before_debit
            "),
            DB::raw("
                coalesce(
                (select sum(amount) from acc_jurnal_details d
                join acc_jurnals j
                on j.id=d.acc_jurnal_id
                 where d.acc_coa_detail_id=acc_coa_details.id and coa_type='Kredit' and j.md_merchant_id=$merchantId
                 and j.is_deleted in(0,2) and j.trans_time::date<'".$firstDate->toDateString()."'
                 ),0
                ) as sum_of_before_kredit
            "),
            DB::raw("
                coalesce(
                (select sum(amount) from acc_jurnal_details d
                  join acc_jurnals j
                on j.id=d.acc_jurnal_id
                 where d.acc_coa_detail_id=acc_coa_details.id and coa_type='Debit' and j.md_merchant_id=$merchantId
                 and j.is_deleted in(0,2) and j.trans_time::date  between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."'
                 ),0
                ) as sum_of_debit
            "),
            DB::raw("
                coalesce(
                (select sum(amount) from acc_jurnal_details d
                join acc_jurnals j
                on j.id=d.acc_jurnal_id
                 where d.acc_coa_detail_id=acc_coa_details.id and coa_type='Kredit' and j.md_merchant_id=$merchantId
                 and j.is_deleted in(0,2) and j.trans_time::date  between '".$firstDate->toDateString()."'  and '".$lastDate->toDateString()."'
                 ),0
                ) as sum_of_kredit
            "),

        ];

        $data = CoaDetail::select(
            $fields)
            ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
            ->where('c.md_merchant_id', $merchantId)
            ->whereIn('acc_coa_details.is_deleted', [0,2])
            ->orderBy('acc_coa_details.code','asc')
            ->groupBy('acc_coa_details.id');

        if(!is_null($trans) && ($trans=='-1')==false)
        {
            $result=$data->where('acc_coa_details.id',$trans);
        }else{
            $result=$data;
        }



        $sumOfCoaAmount = [
            'sum_of_total_debit' => $result->get()->reduce(function($i, $k) {
                return $i + $k->sum_of_debit+ $k->sum_of_before_debit;
            }, 0),
            'sum_of_total_kredit' => $result->get()->reduce(function($i, $k) {
                return $i + $k->sum_of_kredit+$k->sum_of_before_kredit;
            }, 0)
        ];
        return $sumOfCoaAmount;
    }

    public static function checkOfCoa($merchantId)
    {
        $lastDate=Carbon::now()->lastOfMonth();
        $endYear=Carbon::now()->lastOfYear();

        $fields = [
            'acc_coa_details.*',
            DB::raw("
                coalesce(
                (select sum(amount) from acc_jurnal_details d
                  join acc_jurnals j
                on j.id=d.acc_jurnal_id
                 where d.acc_coa_detail_id=acc_coa_details.id and coa_type='Debit' and j.md_merchant_id=$merchantId
                 and j.is_deleted in(0,2) and j.trans_time::date<='".$endYear->toDateString()."'
                 ),0
                ) as sum_all_debit
            "),
            DB::raw("
                coalesce(
                (select sum(amount) from acc_jurnal_details d
                join acc_jurnals j
                on j.id=d.acc_jurnal_id
                 where d.acc_coa_detail_id=acc_coa_details.id and coa_type='Kredit' and j.md_merchant_id=$merchantId
                 and j.is_deleted in(0,2) and j.trans_time::date<='".$endYear->toDateString()."'
                 ),0
                ) as sum_all_kredit
            "),
            DB::raw("
                coalesce(
                (select sum(amount) from acc_jurnal_details d
                  join acc_jurnals j
                on j.id=d.acc_jurnal_id
                 where d.acc_coa_detail_id=acc_coa_details.id and coa_type='Debit' and j.md_merchant_id=$merchantId
                 and j.is_deleted in(0,2) and j.trans_time::date<='".$lastDate->toDateString()."'
                 ),0
                ) as sum_of_debit
            "),
            DB::raw("
                coalesce(
                (select sum(amount) from acc_jurnal_details d
                join acc_jurnals j
                on j.id=d.acc_jurnal_id
                 where d.acc_coa_detail_id=acc_coa_details.id and coa_type='Kredit' and j.md_merchant_id=$merchantId
                 and j.is_deleted in(0,2) and j.trans_time::date <= '".$lastDate->toDateString()."'
                 ),0
                ) as sum_of_kredit
            "),

        ];

        $result = CoaDetail::select(
            $fields)
            ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
            ->where('c.md_merchant_id', $merchantId)
            ->whereIn('acc_coa_details.is_deleted', [0,2])
            ->whereIn('acc_coa_details.code',['1.1.01.01','1.1.03.01','2.1.01.03','1.1.03.04','2.1.04.01','2.1.01.01'])
            ->orderBy('acc_coa_details.code','asc')
            ->groupBy('acc_coa_details.id')->get();
        $data=[];
        foreach ($result as $key =>$item )
        {
            $data[]=[
                'no'=>$key+1,
                'name'=>$item->name,
                'code'=>$item->code,
                'thisYear'=>($item->type_coa=='Debit')?$item->sum_all_debit-$item->sum_all_kredit:$item->sum_all_kredit-$item->sum_all_debit,
                'thisMonth'=>($item->type_coa=='Debit')?$item->sum_of_debit-$item->sum_of_kredit:$item->sum_of_kredit-$item->sum_of_debit,

            ];
        }

        return $data;

    }

    public static function checkOfCoaArAp($merchantId)
    {
        $coaSale=CoaDetail::select(['acc_coa_details.code','acc_coa_details.id'])
                        ->join('acc_coa_categories','acc_coa_categories.id','acc_coa_details.acc_coa_category_id')
                        ->where('acc_coa_details.name','like','%Piutang Penjualan%')
                        ->where('md_merchant_id',$merchantId)
                        ->first();
        $coaPurchase=CoaDetail::select(['acc_coa_details.code','acc_coa_details.id'])
                        ->join('acc_coa_categories','acc_coa_categories.id','acc_coa_details.acc_coa_category_id')
                        ->where('acc_coa_details.name','like','%Utang Pembelian Barang%')
                        ->where('md_merchant_id',$merchantId)
                        ->first();
        $coaMap=CoaMapping::select(['acc_coa_detail_id as id','name'])->where('md_merchant_id',$merchantId)->whereIn('is_type',[4,5])->get();

        $merchant=Merchant::find($merchantId);
        if(is_null($merchant->coa_ar_sale) || is_null($merchant->coa_ar_other) || is_null($merchant->coa_ap_purchase) || is_null($merchant->coa_ap_other) || is_null($merchant->coa_ar_company) || is_null($merchant->coa_ap_company)){
            $merchant->coa_ar_sale=$coaSale->id;
            $merchant->coa_ar_other=$coaMap->firstWhere('name','Piutang Lainnya')->id;
            $merchant->coa_ar_company=$coaMap->firstWhere('name','Piutang Usaha')->id;
            $merchant->coa_ap_purchase=$coaPurchase->id;
            $merchant->coa_ap_other=$coaMap->firstWhere('name','Utang Lainnya')->id;
            $merchant->coa_ap_company=$coaMap->firstWhere('name','Utang Usaha')->id;
            $merchant->save();
        }

        return $merchant;

    }
}

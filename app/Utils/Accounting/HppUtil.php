<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Accounting;


use App\Models\SennaToko\StockInventory;
use Illuminate\Support\Facades\DB;

class HppUtil
{

    public static function fifo($productId,$warehouseId)
    {
        try{
            $keywordRetur="Retur";
            $hpp=StockInventory::where('sc_product_id',$productId)
                ->where('type','in')
                ->where('residual_stock','!=',0)
                ->whereRaw("transaction_action not like '%".$keywordRetur."%'")
                ->where('inv_warehouse_id',$warehouseId)
                ->where('is_deleted',0)
                ->orderBy('id','asc')
                ->sum(DB::raw('residual_stock*purchase_price'));
            return $hpp;

        }catch (\Exception $e)
        {
            return  false;
        }

    }

    public static function lifo($productId,$warehouseId)
    {
        try{
            $keywordRetur="Retur";
            $hpp=StockInventory::where('sc_product_id',$productId)
                ->where('type','in')
                ->where('residual_stock','!=',0)
                ->whereRaw("transaction_action not like '%".$keywordRetur."%'")
                ->where('inv_warehouse_id',$warehouseId)
                ->where('is_deleted',0)
                ->orderBy('id','desc')
                ->sum(DB::raw('residual_stock*purchase_price'));
            return $hpp;
        }catch (\Exception $e)
        {
             return false;
        }
    }

    public static function average($productId,$warehouseId)
    {
        try{
            $keywordRetur="Retur";
            $hpp=StockInventory::where('sc_product_id',$productId)
                ->select([
                    DB::raw("
                    (sum(residual_stock*purchase_price)/sum(residual_stock)) as average
                    ")
                ])
                ->where('type','in')
                ->where('residual_stock','!=',0)
                ->whereRaw("transaction_action not like '%".$keywordRetur."%'")
                ->where('inv_warehouse_id',$warehouseId)
                ->where('is_deleted',0)
                ->orderBy('id','desc')->first();
            return $hpp[0]->average;
        }catch (\Exception $e)
        {
            return false;

        }

    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Account;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Models\MasterData\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class AccountUtil
{

    private static function getUser()
    {
        return User::class;
    }

    private static function getMessage()
    {
        return Message::getInstance();
    }

    public static function checkEmail($email,$role)
    {
        return (self::getUser()::whereRaw("lower(email)='".strtolower($email)."'")
        ->where(['md_role_id'=>$role])
            ->first())?1:0;
    }

    public static function emailVerification($request)
    {
        try{
            $data=self::getUser()::where('token',$request->key)->first();
            if(is_null($data)){
                return self::getMessage()::getArrayResponse(404,trans('custom.account_not_found'),[]);
            }
            $data->is_active=1;
            $data->is_email_verified=1;
            $data->save();
            return self::getMessage()::getArrayResponse(200,trans('custom.account_is_active_success'),[]);
        }catch (\Exception $e)
        {
            return self::getMessage()::getArrayResponse(500,trans('custom.registration_errors'),[]);
        }
    }

    public static function getVaCode($code)
    {
        return (self::getUser()::where('va_code',$code)->first())?
            self::getUser()::where('va_code',$code)->first():null;
    }
}

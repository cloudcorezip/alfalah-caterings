<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\CRM;


use Illuminate\Support\Facades\DB;

class CommissionUtil
{

    public static function resultCommission($product,$productQuantitys,$userHelpers,$merchantId,$amount,$saleOrderId,$timestamp,$timezone)
    {
        try {
            $productQuantitysObject=collect($productQuantitys);
            $data=collect(DB::select("
           SELECT
              mc.*,
               p->>'id' as product_id,
              s->>'id' as staff_id
            FROM
              merchant_commissions mc,
              jsonb_array_elements(mc.product_list) AS p,
              jsonb_array_elements(mc.staff_list) as s
            where
              cast(p ->> 'id' as integer) in $product
              and cast(s ->> 'id' as text) in $userHelpers
              and mc.is_deleted = 0
              and mc.status = 1
              and mc.assign_to_branch @> '[$merchantId]'
              and mc.min_transaction <= $amount
              and mc.type='product'
            union
            SELECT
              mc.*,
              p->>'id' as product_id,
              s->>'id' as staff_id
            FROM
              merchant_commissions mc,
              jsonb_array_elements(mc.product_list) AS p,
              jsonb_array_elements(mc.staff_list) as s
            where
              cast(p ->> 'id' as integer) in $product
              and cast(s ->> 'id' as text) in $userHelpers
              and mc.is_deleted = 0
              and mc.status = 1
              and mc.min_transaction <= $amount
              and mc.md_merchant_id=$merchantId
              and mc.type='product'
            union
            SELECT
              mc.*,
              '-1' product_id,
              s->>'id' as staff_id
            FROM
              merchant_commissions mc,
              jsonb_array_elements(mc.staff_list) as s
            where
              cast(s ->> 'id' as text) in $userHelpers
              and mc.is_deleted = 0
              and mc.status = 1
              and mc.min_transaction <=$amount
              and mc.type='transaction'
              and mc.md_merchant_id=$merchantId
            union
            SELECT
              mc.*,
              '-1' product_id,
              s->>'id' as staff_id
            FROM
              merchant_commissions mc,
              jsonb_array_elements(mc.staff_list) as s
            where
              cast(s ->> 'id' as text) in $userHelpers
              and mc.is_deleted = 0
              and mc.status = 1
              and mc.min_transaction <=$amount
              and mc.type='transaction'
              and mc.assign_to_branch @> '[$merchantId]'
            "));

            $result=[];
            if($data->count()>0){
                foreach ($data as $key =>$item)
                {
                    if(!is_null($item->value_commissions)){
                        $commissionValue=[];
                        $jsonCommission=json_decode($item->value_commissions);
                        if($item->type_of_value_commissions!='tiered'){
                            foreach ($jsonCommission as $commission){
                                if($item->type=='product'){
                                    foreach ($productQuantitysObject as $p){
                                        if($p->id==$item->product_id){
                                            if($item->value_type=='nominal'){
                                                if($item->type_of_value_commissions=='flat'){
                                                    $commissionValue[]=(object)[
                                                        'id'=>$p->id,
                                                        'commission_amount'=>$commission->value,
                                                        'product_name'=>$p->product_name,
                                                        'qty'=>$p->quantity,
                                                        'origin_qty'=>$p->origin_quantity,
                                                        'unit_name'=>$p->unit_name,
                                                        'origin_unit_name'=>$p->origin_unit_name,
                                                        'price'=>$p->price
                                                    ];

                                                }else{
                                                    $rules=floor($p->quantity/$commission->rules);
                                                    if(floor($rules)!=0)
                                                    {
                                                        $commissionValue[]=(object)[
                                                            'id'=>$p->id,
                                                            'commission_amount'=>$rules*$commission->value,
                                                            'product_name'=>$p->product_name,
                                                            'qty'=>$p->quantity,
                                                            'origin_qty'=>$p->origin_quantity,
                                                            'unit_name'=>$p->unit_name,
                                                            'origin_unit_name'=>$p->origin_unit_name,
                                                            'price'=>$p->price
                                                        ];
                                                    }
                                                }
                                            }else{
                                                if($item->type_of_value_commissions=='flat'){
                                                    $commissionValue[]=(object)[
                                                        'id'=>$p->id,
                                                        'commission_amount'=>$p->price*($commission->value/100),
                                                        'product_name'=>$p->product_name,
                                                        'qty'=>$p->quantity,
                                                        'origin_qty'=>$p->origin_quantity,
                                                        'unit_name'=>$p->unit_name,
                                                        'origin_unit_name'=>$p->origin_unit_name,
                                                        'price'=>$p->price
                                                    ];

                                                }else{
                                                    $rules=floor($p->price/$commission->rules);
                                                    if($rules!=0)
                                                    {
                                                        $commissionValue[]=(object)[
                                                            'id'=>$p->id,
                                                            'commission_amount'=>$p->price*($commission->value/100),
                                                            'product_name'=>$p->product_name,
                                                            'qty'=>$p->quantity,
                                                            'origin_qty'=>$p->origin_quantity,
                                                            'unit_name'=>$p->unit_name,
                                                            'origin_unit_name'=>$p->origin_unit_name,
                                                            'price'=>$p->price
                                                        ];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    if($item->value_type=='nominal'){
                                        if($item->type_of_value_commissions=='flat'){
                                            $commissionValue[]=(object)[
                                                'id'=>'-1',
                                                'commission_amount'=>$commission->value,
                                                'product_name'=>'-1',
                                                'qty'=>'-1',
                                                'origin_qty'=>'-1',
                                                'unit_name'=>'-1',
                                                'origin_unit_name'=>'-1',
                                                'price'=>0
                                            ];

                                        }else{
                                            $rules=floor($amount/$commission->rules);
                                            if($rules!=0){
                                                $commissionValue[]=(object)[
                                                    'id'=>'-1',
                                                    'commission_amount'=>$rules*$commission->value,
                                                    'product_name'=>'-1',
                                                    'qty'=>'-1',
                                                    'origin_qty'=>'-1',
                                                    'unit_name'=>'-1',
                                                    'origin_unit_name'=>'-1',
                                                    'price'=>0
                                                ];
                                            }

                                        }
                                    }else{
                                        if($item->type_of_value_commissions=='flat'){
                                            $commissionValue[]=(object)[
                                                'id'=>'-1',
                                                'commission_amount'=>$amount*($commission->value/100),
                                                'product_name'=>'-1',
                                                'qty'=>'-1',
                                                'origin_qty'=>'-1',
                                                'unit_name'=>'-1',
                                                'origin_unit_name'=>'-1',
                                                'price'=>0
                                            ];
                                        }else{
                                            $rules=floor($amount/$commission->rules);
                                            if($rules!=0){
                                                $commissionValue[]=(object)[
                                                    'id'=>'-1',
                                                    'commission_amount'=>$amount*($commission->value/100),
                                                    'product_name'=>'-1',
                                                    'qty'=>'-1',
                                                    'origin_qty'=>'-1',
                                                    'unit_name'=>'-1',
                                                    'origin_unit_name'=>'-1',
                                                    'price'=>0
                                                ];
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            $jsonCommissionTiered=collect(json_decode($item->value_commissions))->sortBy('rules');
                            $jsonCommissionTieredCount=count($jsonCommissionTiered)-1;
                            if($item->type=='product'){
                                foreach ($productQuantitysObject as $p){
                                    if($p->id==$item->product_id) {
                                        foreach ($jsonCommissionTiered as $key =>$t){
                                            if($key==$jsonCommissionTieredCount){
                                                if($p->quantity>=$jsonCommissionTiered[$key]->rules){
                                                    $commissionValue[]=(object)[
                                                        'id'=>$p->id,
                                                        'commission_amount'=>($item->value_type=='nominal')?$jsonCommissionTiered[$key]->value:($p->price*($jsonCommissionTiered[$key]->value/100)),
                                                        'product_name'=>$p->product_name,
                                                        'qty'=>$p->quantity,
                                                        'origin_qty'=>$p->origin_quantity,
                                                        'unit_name'=>$p->unit_name,
                                                        'origin_unit_name'=>$p->origin_unit_name,
                                                        'price'=>$p->price
                                                    ];
                                                }
                                            }else{
                                                if(($p->quantity>=$jsonCommissionTiered[$key]->rules) && ($p->quantity<$jsonCommissionTiered[$key+1]->rules)){
                                                    $commissionValue[]=(object)[
                                                        'id'=>$p->id,
                                                        'commission_amount'=>($item->value_type=='nominal')?$jsonCommissionTiered[$key]->value:($p->price*($jsonCommissionTiered[$key]->value/100)),
                                                        'product_name'=>$p->product_name,
                                                        'qty'=>$p->quantity,
                                                        'origin_qty'=>$p->origin_quantity,
                                                        'unit_name'=>$p->unit_name,
                                                        'origin_unit_name'=>$p->origin_unit_name,
                                                        'price'=>$p->price
                                                    ];
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                foreach ($jsonCommissionTiered as $keys =>$t){
                                    if($keys==$jsonCommissionTieredCount){
                                        if($amount>=$jsonCommissionTiered[$keys]->rules){
                                            $commissionValue[]=(object)[
                                                'id'=>'-1',
                                                'commission_amount'=>($item->value_type=='nominal')?$jsonCommissionTiered[$keys]->value:($amount*($jsonCommissionTiered[$keys]->value/100)),
                                                'product_name'=>'-1',
                                                'qty'=>'-1',
                                                'origin_qty'=>'-1',
                                                'unit_name'=>'-1',
                                                'origin_unit_name'=>'-1',
                                                'price'=>0
                                            ];
                                        }
                                    }else{
                                        if(($amount>=$jsonCommissionTiered[$keys]->rules) && ($amount<$jsonCommissionTiered[$keys+1]->rules)){
                                            $commissionValue[]=(object)[
                                                'id'=>'-1',
                                                'commission_amount'=>($item->value_type=='nominal')?$jsonCommissionTiered[$keys]->value:($amount*($jsonCommissionTiered[$keys]->value/100)),
                                                'product_name'=>'-1',
                                                'qty'=>'-1',
                                                'origin_qty'=>'-1',
                                                'unit_name'=>'-1',
                                                'origin_unit_name'=>'-1',
                                                'price'=>0
                                            ];
                                        }
                                    }
                                }
                            }

                        }

                        if(!empty($commissionValue)){
                            array_push($result,[
                                'commission_id'=>$item->id,
                                'commission_name'=>$item->name,
                                'staff_id'=>$item->staff_id,
                                'product_id'=>($item->product_id=='-1' || $item->product_id=='-1')?null:$item->product_id,
                                'commission_value'=>json_encode($commissionValue),
                                'sale_amount'=>$amount,
                                'type'=>$item->type,
                                'value_type'=>$item->value_type,
                                'type_of_value_commissions'=>$item->type_of_value_commissions,
                                'detail_commissions'=>json_encode($item),
                                'sc_sale_order_id'=>$saleOrderId,
                                'created_at'=>$timestamp,
                                'updated_at'=>$timestamp,
                                'timezone'=>$timezone,
                                'md_merchant_id'=>$merchantId
                            ]);
                        }
                    }
                }
            }
            return $result;
        }catch (\Exception $e)
        {
            return false;

        }
    }

    public static function calculateCommission($data_commission,$productQuantitys,$merchantId,$amount,$saleOrderId,$timestamp,$timezone)
    {
        try {
            $item=$data_commission;
            $productQuantitysObject=collect($productQuantitys);
            $result=[];
                    if(!is_null($item->value_commissions)){
                        $commissionValue=[];
                        $jsonCommission=json_decode($item->value_commissions);
                        if($item->type_of_value_commissions!='tiered'){
                            foreach ($jsonCommission as $commission){
                                if($item->type=='product'){
                                    foreach ($productQuantitysObject as $p){
                                        if($p->id==$item->product_id){
                                            if($item->value_type=='nominal'){
                                                if($item->type_of_value_commissions=='flat'){
                                                    $commissionValue[]=(object)[
                                                        'id'=>$p->id,
                                                        'commission_amount'=>$commission->value,
                                                        'product_name'=>$p->product_name,
                                                        'qty'=>$p->quantity,
                                                        'origin_qty'=>$p->origin_quantity,
                                                        'unit_name'=>$p->unit_name,
                                                        'origin_unit_name'=>$p->origin_unit_name,
                                                        'price'=>$p->price
                                                    ];

                                                }else{
                                                    $rules=floor($p->quantity/$commission->rules);
                                                    if(floor($rules)!=0)
                                                    {
                                                        $commissionValue[]=(object)[
                                                            'id'=>$p->id,
                                                            'commission_amount'=>$rules*$commission->value,
                                                            'product_name'=>$p->product_name,
                                                            'qty'=>$p->quantity,
                                                            'origin_qty'=>$p->origin_quantity,
                                                            'unit_name'=>$p->unit_name,
                                                            'origin_unit_name'=>$p->origin_unit_name,
                                                            'price'=>$p->price
                                                        ];
                                                    }
                                                }
                                            }else{
                                                if($item->type_of_value_commissions=='flat'){
                                                    $commissionValue[]=(object)[
                                                        'id'=>$p->id,
                                                        'commission_amount'=>$p->price*($commission->value/100),
                                                        'product_name'=>$p->product_name,
                                                        'qty'=>$p->quantity,
                                                        'origin_qty'=>$p->origin_quantity,
                                                        'unit_name'=>$p->unit_name,
                                                        'origin_unit_name'=>$p->origin_unit_name,
                                                        'price'=>$p->price
                                                    ];

                                                }else{
                                                    $rules=floor($p->price/$commission->rules);
                                                    if($rules!=0)
                                                    {
                                                        $commissionValue[]=(object)[
                                                            'id'=>$p->id,
                                                            'commission_amount'=>$p->price*($commission->value/100),
                                                            'product_name'=>$p->product_name,
                                                            'qty'=>$p->quantity,
                                                            'origin_qty'=>$p->origin_quantity,
                                                            'unit_name'=>$p->unit_name,
                                                            'origin_unit_name'=>$p->origin_unit_name,
                                                            'price'=>$p->price
                                                        ];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    if($item->value_type=='nominal'){
                                        if($item->type_of_value_commissions=='flat'){
                                            $commissionValue[]=(object)[
                                                'id'=>'-1',
                                                'commission_amount'=>$commission->value,
                                                'product_name'=>'-1',
                                                'qty'=>'-1',
                                                'origin_qty'=>'-1',
                                                'unit_name'=>'-1',
                                                'origin_unit_name'=>'-1',
                                                'price'=>0
                                            ];

                                        }else{
                                            $rules=floor($amount/$commission->rules);
                                            if($rules!=0){
                                                $commissionValue[]=(object)[
                                                    'id'=>'-1',
                                                    'commission_amount'=>$rules*$commission->value,
                                                    'product_name'=>'-1',
                                                    'qty'=>'-1',
                                                    'origin_qty'=>'-1',
                                                    'unit_name'=>'-1',
                                                    'origin_unit_name'=>'-1',
                                                    'price'=>0
                                                ];
                                            }

                                        }
                                    }else{
                                        if($item->type_of_value_commissions=='flat'){
                                            $commissionValue[]=(object)[
                                                'id'=>'-1',
                                                'commission_amount'=>$amount*($commission->value/100),
                                                'product_name'=>'-1',
                                                'qty'=>'-1',
                                                'origin_qty'=>'-1',
                                                'unit_name'=>'-1',
                                                'origin_unit_name'=>'-1',
                                                'price'=>0
                                            ];
                                        }else{
                                            $rules=floor($amount/$commission->rules);
                                            if($rules!=0){
                                                $commissionValue[]=(object)[
                                                    'id'=>'-1',
                                                    'commission_amount'=>$amount*($commission->value/100),
                                                    'product_name'=>'-1',
                                                    'qty'=>'-1',
                                                    'origin_qty'=>'-1',
                                                    'unit_name'=>'-1',
                                                    'origin_unit_name'=>'-1',
                                                    'price'=>0
                                                ];
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            $jsonCommissionTiered=collect(json_decode($item->value_commissions))->sortBy('rules');
                            $jsonCommissionTieredCount=count($jsonCommissionTiered)-1;
                            if($item->type=='product'){
                                foreach ($productQuantitysObject as $p){
                                    if($p->id==$item->product_id) {
                                        foreach ($jsonCommissionTiered as $key =>$t){
                                            if($key==$jsonCommissionTieredCount){
                                                if($p->quantity>=$jsonCommissionTiered[$key]->rules){
                                                    $commissionValue[]=(object)[
                                                        'id'=>$p->id,
                                                        'commission_amount'=>($item->value_type=='nominal')?$jsonCommissionTiered[$key]->value:($p->price*($jsonCommissionTiered[$key]->value/100)),
                                                        'product_name'=>$p->product_name,
                                                        'qty'=>$p->quantity,
                                                        'origin_qty'=>$p->origin_quantity,
                                                        'unit_name'=>$p->unit_name,
                                                        'origin_unit_name'=>$p->origin_unit_name,
                                                        'price'=>$p->price
                                                    ];
                                                }
                                            }else{
                                                if(($p->quantity>=$jsonCommissionTiered[$key]->rules) && ($p->quantity<$jsonCommissionTiered[$key+1]->rules)){
                                                    $commissionValue[]=(object)[
                                                        'id'=>$p->id,
                                                        'commission_amount'=>($item->value_type=='nominal')?$jsonCommissionTiered[$key]->value:($p->price*($jsonCommissionTiered[$key]->value/100)),
                                                        'product_name'=>$p->product_name,
                                                        'qty'=>$p->quantity,
                                                        'origin_qty'=>$p->origin_quantity,
                                                        'unit_name'=>$p->unit_name,
                                                        'origin_unit_name'=>$p->origin_unit_name,
                                                        'price'=>$p->price
                                                    ];
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                foreach ($jsonCommissionTiered as $keys =>$t){
                                    if($keys==$jsonCommissionTieredCount){
                                        if($amount>=$jsonCommissionTiered[$keys]->rules){
                                            $commissionValue[]=(object)[
                                                'id'=>'-1',
                                                'commission_amount'=>($item->value_type=='nominal')?$jsonCommissionTiered[$keys]->value:($amount*($jsonCommissionTiered[$keys]->value/100)),
                                                'product_name'=>'-1',
                                                'qty'=>'-1',
                                                'origin_qty'=>'-1',
                                                'unit_name'=>'-1',
                                                'origin_unit_name'=>'-1',
                                                'price'=>0
                                            ];
                                        }
                                    }else{
                                        if(($amount>=$jsonCommissionTiered[$keys]->rules) && ($amount<$jsonCommissionTiered[$keys+1]->rules)){
                                            $commissionValue[]=(object)[
                                                'id'=>'-1',
                                                'commission_amount'=>($item->value_type=='nominal')?$jsonCommissionTiered[$keys]->value:($amount*($jsonCommissionTiered[$keys]->value/100)),
                                                'product_name'=>'-1',
                                                'qty'=>'-1',
                                                'origin_qty'=>'-1',
                                                'unit_name'=>'-1',
                                                'origin_unit_name'=>'-1',
                                                'price'=>0
                                            ];
                                        }
                                    }
                                }
                            }

                        }

                        if(!empty($commissionValue)){
                            array_push($result,[
                                'commission_id'=>$item->id,
                                'commission_name'=>$item->name,
                                'staff_id'=>$item->staff_id,
                                'product_id'=>($item->product_id=='-1' || $item->product_id=='-1')?null:$item->product_id,
                                'commission_value'=>json_encode($commissionValue),
                                'sale_amount'=>$amount,
                                'type'=>$item->type,
                                'value_type'=>$item->value_type,
                                'type_of_value_commissions'=>$item->type_of_value_commissions,
                                'detail_commissions'=>json_encode($item),
                                'sc_sale_order_id'=>$saleOrderId,
                                'created_at'=>$timestamp,
                                'updated_at'=>$timestamp,
                                'timezone'=>$timezone,
                                'md_merchant_id'=>$merchantId
                            ]);
                        }
                    }
                
            
            return $result;
        }catch (\Exception $e)
        {
            return false;

        }
    }
}

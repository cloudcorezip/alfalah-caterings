<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Plugin;


use App\Classes\Singleton\CodeGenerator;
use App\Models\Marketing\Promo;
use App\Models\MasterData\Subscription;
use App\Models\MasterData\TransactionType;
use App\Models\Plugin\EnableMerchantPlugin;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PluginUtil
{
    public static function calculate($date,$type,$plus=0)
    {
        $carbon=new Carbon($date);
        if(strtolower($type)=='monthly')
        {
            $extended=(int)$plus+1;
            $endDate=$carbon->addMonths($extended)->toDateTimeString();
        }elseif (strtolower($type)=='threemonth'){
            $extended=(int)$plus+3;
            $endDate=$carbon->addMonths($extended)->toDateTimeString();
        }elseif (strtolower($type)=='sixmonth')
        {
            $extended=(int)$plus+6;
            $endDate=$carbon->addMonths($extended)->toDateTimeString();
        }elseif (strtolower($type)=='yearly')
        {
            $extended=(int)$plus+12;
            $endDate=$carbon->addMonths($extended)->toDateTimeString();
        }

        return $endDate;
    }

    public static function calculatePluginEndDate($merchantPlugin,$plugin)
    {
        try{
            if(is_null($merchantPlugin)){
                $endDate=self::calculate(date('Y-m-d H:i:s'),$plugin->payment_type);
            } else {
                // if(is_null($merchantPlugin->start_period))
                // {
                //     $endDate=self::calculate(date('Y-m-d H:i:s'),$plugin->payment_type);
                // }else{
                //     if($merchantPlugin->start_period > date('Y-m-d H:i:s')){
                //         $plusDate=$merchantPlugin->start_period;
                //     }else{
                //         $plusDate=date('Y-m-d H:i:s');
                //     }
                //     $endDate=self::calculate($plusDate,$plugin->payment_type);
                // }

                if(is_null($merchantPlugin->end_period))
                {
                    $endDate=self::calculate(date('Y-m-d H:i:s'),$plugin->payment_type);
                }else{
                    $plusDate=$merchantPlugin->end_period;
                    $endDate=self::calculate($plusDate,$plugin->payment_type);
                }
            }

            return $endDate;

        }catch (\Exception $e)
        {
            return false;
        }

    }

    public static function checkActivePlugin($merchantId, $type)
    {
        try {
            $data = EnableMerchantPlugin::select([
                'enable_merchant_plugins.id',
                'enable_merchant_plugins.status',
                'enable_merchant_plugins.start_period',
                'enable_merchant_plugins.end_period',
                'enable_merchant_plugins.quota_used',
                'enable_merchant_plugins.quota_available',
                'enable_merchant_plugins.md_merchant_id',
                'enable_merchant_plugins.email',
                'enable_merchant_plugins.client_id',
                'enable_merchant_plugins.client_secret',
                'enable_merchant_plugins.client_token',
                'enable_merchant_plugins.md_plugin_id',
                'mp.id as plugin_id',
                'mp.name',
                'mp.type',
                'mp.payment_type'
            ])
                ->join('md_plugins as mp','mp.id','enable_merchant_plugins.md_plugin_id')
                ->where('enable_merchant_plugins.is_deleted', 0)
                ->where('enable_merchant_plugins.status', 1)
                ->where('enable_merchant_plugins.md_merchant_id',$merchantId)
                ->where('mp.type', $type)
                ->first();
            
            if(!is_null($data)){
                if(strtolower($data->payment_type) != "quota"){ 
                    if(date('Y-m-d H:i:s') > $data->end_period){
                        $data->status = 0;
                        $data->save();
                    }
               } else {
                   if($data->quota_used >= $data->quota_available){
                        $data->status = 0;
                        $data->save();
                    } else {
                        $data->status = 1;
                        $data->save();
                    }
               }
            }
               
            return is_null($data) ? false : $data;
        }catch(\Exception $e)
        {
            return false;
        }
    }

}

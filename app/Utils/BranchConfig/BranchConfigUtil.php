<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\BranchConfig;


use App\Models\MasterData\Merchant;
use Illuminate\Support\Facades\DB;

class BranchConfigUtil
{

    public static function getAccess($menuName,$merchantId)
    {
        $merchant=Merchant::find($merchantId);

        $head=$merchant->getHeadBranch;
        if(is_null($head))
        {
            return true;
        }else{
            $check=DB::table('merchant_multi_branch_configs')
                ->where('md_merchant_id',$head->head_merchant_id)
                ->where('menu_name',$menuName)
                ->first();

            if(is_null($check))
            {
                return false;
            }else{
                if($check->is_available_add==1)
                {
                    return true;
                }else{
                    return false;
                }
            }
        }

    }

    public static function getAccessMobile($menuName,$userId)
    {
        $merchant=Merchant::where('md_user_id',$userId)->first();
        if(is_null($merchant))
        {
            return true;
        }else{
            if($merchant->is_branch==0){
                return true;
            }else{
                $head=$merchant->getHeadBranch;
                if(is_null($head)){
                    return  self::getAccess($menuName,$merchant->id);
                }else{
                    return  self::getAccess($menuName,$head->head_merchant_id);
                }
            }
        }
    }
}

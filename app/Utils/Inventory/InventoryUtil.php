<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Inventory;


use App\Models\MasterData\InventoryMethod;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductionOfGood;
use App\Models\SennaToko\StockInventory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class InventoryUtil
{

    public static function inventoryCode($merchantId,$warehouseId,$productId,$amount,$type,$purchase=null)
    {
        try{
            $inventoryId=(is_null((Merchant::find($merchantId)->md_inventory_method_id)))?null:
                (Merchant::find($merchantId))->md_inventory_method_id;
            $product=Product::find($productId);
            if($inventoryId==InventoryMethod::FIFO)
            {
                if($product->is_with_stock==1){
                    return self::fifoCalculate($warehouseId,$productId,$amount,$type,$purchase);
                }else{
                    if($product->is_with_purchase_price==0)
                    {
                        return 0;
                    }else{
                        return $product->purchase_price;
                    }
                }
            }elseif ($inventoryId==InventoryMethod::AVERAGE)
            {
                if($product->is_with_stock==1){
                    return self::average($warehouseId,$productId,$amount,$type,$purchase);
                }else{
                    if($product->is_with_purchase_price==0)
                    {
                        return 0;
                    }else{
                        return $product->purchase_price;
                    }
                }


            }elseif ($inventoryId==InventoryMethod::LIFO)
            {
                if($product->is_with_stock==1){
                    return self::lifoCalculate($warehouseId,$productId,$amount,$type,$purchase);
                }else{
                    if($product->is_with_purchase_price==0)
                    {
                        return 0;
                    }else{
                        return $product->purchase_price;
                    }
                }
            }else{
                if($product->is_with_stock==1){
                    return self::fifoCalculate($warehouseId,$productId,$amount,$type,$purchase);
                }else{

                    if($product->is_with_purchase_price==0)
                    {
                        return 0;
                    }else{
                        return $product->purchase_price;
                    }
                }
            }
        }catch (\Exception $e)
        {
            log_helper(self::class,'Error Inventory',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }
    }


    public static function inventoryCodev2($inventoryId,$warehouseId,$product,$amount,$type,$purchase=null)
    {
        try{
            if($inventoryId==InventoryMethod::FIFO)
            {
                if($product->is_with_stock==1){
                    return self::fifoCalculate($warehouseId,$product->id,$amount,$type,$purchase);
                }else{
                    if($product->is_with_purchase_price==0)
                    {
                        return 0;
                    }else{
                        return $product->purchase_price;
                    }
                }
            }elseif ($inventoryId==InventoryMethod::AVERAGE)
            {
                if($product->is_with_stock==1){
                    return self::average($warehouseId,$product->id,$amount,$type,$purchase);
                }else{
                    if($product->is_with_purchase_price==0)
                    {
                        return 0;
                    }else{
                        return $product->purchase_price;
                    }
                }


            }elseif ($inventoryId==InventoryMethod::LIFO)
            {
                if($product->is_with_stock==1){
                    return self::lifoCalculate($warehouseId,$product->id,$amount,$type,$purchase);
                }else{
                    if($product->is_with_purchase_price==0)
                    {
                        return 0;
                    }else{
                        return $product->purchase_price;
                    }
                }
            }else{
                if($product->is_with_stock==1){
                    return self::fifoCalculate($warehouseId,$product->id,$amount,$type,$purchase);
                }else{

                    if($product->is_with_purchase_price==0)
                    {
                        return 0;
                    }else{
                        return $product->purchase_price;
                    }
                }
            }
        }catch (\Exception $e)
        {
            log_helper(self::class,'Error Inventory',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }
    }


    private  static function fifoCalculate($warehouseId,$productId,$amount,$type,$purchase=null)
    {
        try{
            DB::beginTransaction();
            if($type=='minus')
            {
                $keywordRetur="Retur";
                if(is_null($purchase))
                {
                    $stock=StockInventory::where('sc_product_id',$productId)
                        ->where('type','in')
                        ->where('residual_stock','!=',0)
                        ->whereRaw("transaction_action not like '%".$keywordRetur."%'")
                        ->where('inv_warehouse_id',$warehouseId)
                        ->where('is_deleted',0)
                        ->orderBy('id','asc')
                        ->get();
                }else{
                    $stock=StockInventory::where('sc_stock_inventories.sc_product_id',$productId)
                        ->select([
                            'sc_stock_inventories.*',
                        ])
                        ->where('sc_stock_inventories.type','in')
                        ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                        ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                        ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                        ->where('m.sc_sale_order_id',$purchase->id)
                        ->where('sc_stock_inventories.is_deleted',0)
                        ->orderBy('sc_stock_inventories.id','asc')
                        ->get();
                }
                $initial=[$amount];
                $subtraction=[];
                foreach ($stock as $item)
                {
                    $subtraction[]=$item->residual_stock;
                }
                $decrementData=self::decrementStock(array_merge($initial,$subtraction));
                $arrayOfPurchase=[];
                if(!empty($decrementData))
                {
                    foreach ($stock as $key => $v)
                    {
                        ProductionOfGood::where('sc_stock_inventory_id',$v->id)
                            ->update(['is_use'=>1]);

                        if($decrementData[$key]['decrement_amount']!=0)
                        {
                            $arrayOfPurchase[]=[
                                'id'=>$v->id,
                                'purchase'=>$v->purchase_price,
                                'amount'=>$decrementData[$key]['decrement_amount'],
                                'residual_amount'=>$decrementData[$key]['residual_amount']
                            ];
                        }
                        StockInventory::where('id',$v->id)
                            ->update([
                                'residual_stock'=>$decrementData[$key]['residual_amount']
                            ]);
                    }
                }
                DB::commit();
                return $arrayOfPurchase;
            }else{
                $keywordRetur="Retur";
                $stock=StockInventory::where('sc_stock_inventories.sc_product_id',$productId)
                    ->select([
                        'sc_stock_inventories.*',
                        'm.amount'
                    ])
                    ->where('sc_stock_inventories.type','in')
                    ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                    ->where('is_deleted',0)
                    ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                    ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                    ->where('m.sc_sale_order_id',$purchase->id)
                    ->orderBy('sc_stock_inventories.id','asc')
                    ->get();
                $subtraction=[];
                foreach ($stock as $item)
                {
                    $subtraction[]=[
                        'stock'=>$item->total,
                        'residual'=>$item->residual_stock,
                        'id'=>$item->id,
                    ];
                }
                $incrementData=self::incrementStock($amount,$subtraction);
                $totalOfRetur=[];
                if(!empty($incrementData))
                {
                    foreach ($incrementData as $key =>$item)
                    {
                        $inv=StockInventory::find($incrementData[$key]['id']);
                        $inv->residual_stock=$incrementData[$key]['stock'];
                        $inv->save();
                        $totalOfRetur[]=[
                            'id'=>$inv->sc_product_id,
                            'total'=>$incrementData[$key]['total_increment']*$inv->purchase_price,
                        ];
                    }
                }
                DB::commit();
                return $totalOfRetur;
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,'FIFO',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }

    }



    private  static function lifoCalculate($warehouseId,$productId,$amount,$type,$purchase=null)
    {
        try{
            DB::beginTransaction();
            if($type=='minus')
            {
                $keywordRetur="Retur";
                if(is_null($purchase))
                {
                    $stock=StockInventory::where('sc_product_id',$productId)
                        ->where('type','in')
                        ->where('residual_stock','!=',0)
                        ->whereRaw("transaction_action not like '%".$keywordRetur."%'")
                        ->where('inv_warehouse_id',$warehouseId)
                        ->where('is_deleted',0)
                        ->orderBy('id','desc')
                        ->get();
                }else{
                    $stock=StockInventory::where('sc_stock_inventories.sc_product_id',$productId)
                        ->select([
                            'sc_stock_inventories.*',
                        ])
                        ->where('sc_stock_inventories.type','in')
                        ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                        ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                        ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                        ->where('m.sc_sale_order_id',$purchase->id)
                        ->where('sc_stock_inventories.is_deleted',0)
                        ->orderBy('sc_stock_inventories.id','desc')
                        ->get();
                }
                $initial=[$amount];
                $subtraction=[];
                foreach ($stock as $item)
                {
                    $subtraction[]=$item->residual_stock;
                }
                $decrementData=self::decrementStock(array_merge($initial,$subtraction));
                $arrayOfPurchase=[];
                if(!empty($decrementData))
                {
                    foreach ($stock as $key => $v)
                    {
                        ProductionOfGood::where('sc_stock_inventory_id',$v->id)
                            ->update(['is_use'=>1]);

                        if($decrementData[$key]['decrement_amount']!=0)
                        {
                            $arrayOfPurchase[]=[
                                'id'=>$v->id,
                                'purchase'=>$v->purchase_price,
                                'amount'=>$decrementData[$key]['decrement_amount'],
                                'residual_amount'=>$decrementData[$key]['residual_amount']
                            ];
                        }
                        StockInventory::where('id',$v->id)
                            ->update([
                                'residual_stock'=>$decrementData[$key]['residual_amount']
                            ]);
                    }
                }
                //return $decrementData;
                DB::commit();
                return $arrayOfPurchase;
            }else{
                $keywordRetur="Retur";
                $stock=StockInventory::where('sc_stock_inventories.sc_product_id',$productId)
                    ->select([
                        'sc_stock_inventories.*',
                        'm.amount'
                    ])
                    ->where('sc_stock_inventories.type','in')
                    ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                    ->where('is_deleted',0)
                    ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                    ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                    ->where('m.sc_sale_order_id',$purchase->id)
                    ->orderBy('sc_stock_inventories.id','desc')
                    ->get();
                $subtraction=[];
                foreach ($stock as $item)
                {
                    $subtraction[]=[
                        'stock'=>$item->total,
                        'residual'=>$item->residual_stock,
                        'id'=>$item->id,
                    ];
                }
                $incrementData=self::incrementStock($amount,$subtraction);
                $totalOfRetur=[];
                if(!empty($incrementData))
                {
                    foreach ($incrementData as $key =>$item)
                    {
                        $inv=StockInventory::find($incrementData[$key]['id']);
                        $inv->residual_stock=$incrementData[$key]['stock'];
                        $inv->save();
                        $totalOfRetur[]=[
                            'id'=>$inv->sc_product_id,
                            'total'=>$incrementData[$key]['total_increment']*$inv->purchase_price,
                        ];
                    }
                }
                DB::commit();
                return $totalOfRetur;
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,'LIFO',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }

    }



    private  static function average($warehouseId,$productId,$amount,$type,$purchase=null)
    {
        try{
            DB::beginTransaction();
            if($type=='minus')
            {
                $keywordRetur="Retur";
                if(is_null($purchase))
                {
                    $stock=StockInventory::where('sc_product_id',$productId)
                        ->where('type','in')
                        ->where('residual_stock','!=',0)
                        ->whereRaw("transaction_action not like '%".$keywordRetur."%'")
                        ->where('inv_warehouse_id',$warehouseId)
                        ->where('is_deleted',0)
                        ->orderBy('id','desc')
                        ->get();
                }else{
                    $stock=StockInventory::where('sc_stock_inventories.sc_product_id',$productId)
                        ->select([
                            'sc_stock_inventories.*',
                        ])
                        ->where('sc_stock_inventories.type','in')
                        ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                        ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                        ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                        ->where('m.sc_sale_order_id',$purchase->id)
                        ->where('sc_stock_inventories.is_deleted',0)
                        ->orderBy('sc_stock_inventories.id','desc')
                        ->get();
                }
                $initial=[$amount];
                $subtraction=[];
                foreach ($stock as $item)
                {
                    $subtraction[]=$item->residual_stock;
                }
                $decrementData=self::decrementStock(array_merge($initial,$subtraction));
                $arrayOfPurchase=[];
                if(!empty($decrementData))
                {
                    foreach ($stock as $key => $v)
                    {
                        ProductionOfGood::where('sc_stock_inventory_id',$v->id)
                            ->update(['is_use'=>1]);

                        if($decrementData[$key]['decrement_amount']!=0)
                        {
                            $arrayOfPurchase[]=[
                                'id'=>$v->id,
                                'purchase'=>$v->purchase_price,
                                'amount'=>$decrementData[$key]['decrement_amount'],
                                'residual_amount'=>$decrementData[$key]['residual_amount']
                            ];
                        }
                        StockInventory::where('id',$v->id)
                            ->update([
                                'residual_stock'=>$decrementData[$key]['residual_amount']
                            ]);
                    }
                }
                DB::commit();
                return $arrayOfPurchase;
            }else{
                $keywordRetur="Retur";
                $stock=StockInventory::where('sc_stock_inventories.sc_product_id',$productId)
                    ->select([
                        'sc_stock_inventories.*',
                        'm.amount'
                    ])
                    ->where('sc_stock_inventories.type','in')
                    ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                    ->where('is_deleted',0)
                    ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                    ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                    ->where('m.sc_sale_order_id',$purchase->id)
                    ->orderBy('sc_stock_inventories.id','desc')
                    ->get();
                $subtraction=[];
                foreach ($stock as $item)
                {
                    $subtraction[]=[
                        'stock'=>$item->total,
                        'residual'=>$item->residual_stock,
                        'id'=>$item->id,
                    ];
                }
                $incrementData=self::incrementStock($amount,$subtraction);
                $totalOfRetur=0;
                if(!empty($incrementData))
                {
                    foreach ($incrementData as $key =>$item)
                    {
                        $inv=StockInventory::find($incrementData[$key]['id']);
                        $inv->residual_stock=$incrementData[$key]['stock'];
                        $inv->save();
                        $totalOfRetur+=$incrementData[$key]['total_increment']*$inv->purchase_price;
                    }
                }
                DB::commit();
                return $totalOfRetur;
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,'LIFO',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }


    }


    private static function decrementStock($array)
    {
        $total=$array[0];
        $data=[];
        for($i=1;$i<count($array);$i++)
        {
            if($total>=$array[$i]){
                $data[]=[
                    'decrement_amount'=>$array[$i],
                    'residual_amount'=>0,
                ];

            }else{
                if($total<0){

                    $data[]=[
                        'decrement_amount'=>0,
                        'residual_amount'=>$array[$i],
                    ];
                }else{

                    $data[]=[
                        'decrement_amount'=>$total,
                        'residual_amount'=>$array[$i]-$total
                    ];
                }

            }

            $total -= $array[$i];
        }

        return $data;

    }

    private static function incrementStock($amount,$array)
    {
        $total=$amount;
        $data=[];
        for($i=0;$i<count($array);$i++)
        {
            if($total>=($array[$i]['stock']-$array[$i]['residual'])){
                $totalIncrement=$array[$i]['stock']-$array[$i]['residual'];
                $data[]=[
                    'stock'=>$array[$i]['stock'],
                    'total_increment'=>$totalIncrement,
                    'id'=>$array[$i]['id']
                ];
                $total-=$totalIncrement;
            }else{
                $data[]=[
                    'stock'=>$array[$i]['residual']+$total,
                    'total_increment'=>$total,
                    'id'=>$array[$i]['id']
                ];
                $total=0;
            }
        }

        return $data;

    }


}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Inventory;


use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\MasterData\InventoryMethod;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\StockSaleMapping;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaProductUtilV2;
use Illuminate\Support\Facades\DB;

class InventoryUtilV2
{

    public static function inventoryCodev2($inventoryId,$warehouseId,$products,$type,$purchase=null)
    {
        try{
            if($inventoryId==InventoryMethod::FIFO)
            {
                return self::fifoCalculateV2($warehouseId,$products,$type,$purchase);
            }elseif ($inventoryId==InventoryMethod::AVERAGE)
            {
                return self::fifoCalculateV2($warehouseId,$products,$type,$purchase);

            }elseif ($inventoryId==InventoryMethod::LIFO)
            {
                return self::lifoCalculateV2($warehouseId,$products,$type,$purchase);
            }else{
                return self::fifoCalculateV2($warehouseId,$products,$type,$purchase);
            }
        }catch (\Exception $e)
        {
            log_helper(self::class,'Error Inventory',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }
    }

    private  static function fifoCalculateV2($warehouseId,$products,$type,$purchase=null)
    {
        try{
            $productId=[];
            foreach ($products as $item)
            {
                if(is_object($item)){
                    $productId[]=$item->id;

                }else{
                    $productId[]=$item['id'];

                }

            }

            if($type=='minus')
            {
                $keywordRetur="Retur";
                if(is_null($purchase))
                {
                    $stock=StockInventory::whereIn('sc_product_id',$productId)
                        ->where('type','in')
                        ->where('residual_stock','!=',0)
                        ->whereRaw("transaction_action not like '%".$keywordRetur."%'")
                        ->where('inv_warehouse_id',$warehouseId)
                        ->where('is_deleted',0)
                        ->orderBy('id','asc')
                        ->get();
                }else{
                    $stock=StockInventory::whereIn('sc_stock_inventories.sc_product_id',$productId)
                        ->select([
                            'sc_stock_inventories.*',
                        ])
                        ->where('sc_stock_inventories.type','in')
                        ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                        ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                        ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                        ->where('m.sc_sale_order_id',$purchase)
                        ->where('sc_stock_inventories.is_deleted',0)
                        ->orderBy('sc_stock_inventories.id','asc')
                        ->get();
                }

                $tempStock=collect($stock->toArray());
                $arrayOfPurchase=[];
                foreach ($products as $p)
                {
                    $stockProduct=$tempStock->where('sc_product_id',(is_object($p))?$p->id:$p['id'])->sortBy('created_at');
                    $subtraction=[];

                    array_push($subtraction,[
                        'id'=>0,
                        'amount'=>(is_object($p))?$p->amount:$p['amount'],
                        'purchase'=>0,
                        'sc_product_id'=>0
                    ]);
                    foreach ($stockProduct as $item)
                    {
                        array_push($subtraction,[
                            'id'=>$item['id'],
                            'amount'=>$item['residual_stock'],
                            'purchase'=>$item['purchase_price'],
                            'sc_product_id'=>$item['sc_product_id']
                        ]);
                    }
                    $decrementData=self::decrementStock($subtraction);
                    if(!empty($decrementData))
                    {
                        foreach ($decrementData as $v)
                        {
                            if($v['decrement_amount']!=0)
                            {
                                $arrayOfPurchase[]=[
                                    'id'=>$v['id'],
                                    'purchase'=>$v['purchase'],
                                    'sc_product_id'=>$v['sc_product_id'],
                                    'amount'=>$v['decrement_amount'],
                                    'residual_amount'=>$v['residual_amount']
                                ];
                            }

                        }
                    }
                }
                return $arrayOfPurchase;

            }else{
                $keywordRetur="Retur";
                $stock=StockInventory::whereIn('sc_stock_inventories.sc_product_id',$productId)
                    ->select([
                        'sc_stock_inventories.*',
                        'm.amount'
                    ])
                    ->where('sc_stock_inventories.type','in')
                    ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                    ->where('is_deleted',0)
                    ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                    ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                    ->where('m.sc_sale_order_id',$purchase)
                    ->orderBy('sc_stock_inventories.id','asc')
                    ->get();
                $tempStockOut=collect($stock->toArray());
                $totalOfRetur=[];

                foreach ($products as $pIn)
                {
                    $subtraction=[];
                    $stockProductIn=$tempStockOut->where('sc_product_id',$pIn['id'])->sortBy('created_at');
                    foreach ($stockProductIn as $v)
                    {
                        $subtraction[]=[
                            'stock'=>$v['total'],
                            'residual'=>$v['residual_stock'],
                            'id'=>$v['id'],
                            'sc_product_id'=>$v['sc_product_id'],
                            'purchase'=>$v['purchase_price']
                        ];
                    }
                    $incrementData=self::incrementStock($pIn['amount'],$subtraction);
                    if(!empty($incrementData))
                    {
                        foreach ($incrementData as $key =>$item)
                        {
                            $totalOfRetur[]=[
                                'id'=>$incrementData[$key]['sc_product_id'],
                                'total'=>$incrementData[$key]['total_increment']*$incrementData[$key]['purchase'],
                                'sc_stock_inventory_id'=>$incrementData[$key]['id'],
                                'total_increment'=>$incrementData[$key]['stock']
                            ];
                        }
                    }
                }
                return $totalOfRetur;
            }

        }catch (\Exception $e)
        {
            log_helper(self::class,'FIFO',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }

    }

    private  static function lifoCalculateV2($warehouseId,$products,$type,$purchase=null)
    {
        try{
            $productId=[];
            foreach ($products as $item)
            {
                if(is_object($item)){
                    $productId[]=$item->id;

                }else{
                    $productId[]=$item['id'];

                }

            }
            if($type=='minus')
            {
                $keywordRetur="Retur";
                if(is_null($purchase))
                {
                    $stock=StockInventory::whereIn('sc_product_id',$productId)
                        ->where('type','in')
                        ->where('residual_stock','!=',0)
                        ->whereRaw("transaction_action not like '%".$keywordRetur."%'")
                        ->where('inv_warehouse_id',$warehouseId)
                        ->where('is_deleted',0)
                        ->orderBy('id','desc')
                        ->get();
                }else{
                    $stock=StockInventory::whereIn('sc_stock_inventories.sc_product_id',$productId)
                        ->select([
                            'sc_stock_inventories.*',
                        ])
                        ->where('sc_stock_inventories.type','in')
                        ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                        ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                        ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                        ->where('m.sc_sale_order_id',$purchase)
                        ->where('sc_stock_inventories.is_deleted',0)
                        ->orderBy('sc_stock_inventories.id','desc')
                        ->get();
                }

                $tempStock=collect($stock->toArray());
                $arrayOfPurchase=[];
                foreach ($products as $p)
                {

                    $stockProduct=$tempStock->where('sc_product_id',(is_object($p))?$p->id:$p['id'])->sortByDesc('created_at');
                    $subtraction=[];

                    array_push($subtraction,[
                        'id'=>0,
                        'amount'=>(is_object($p))?$p->amount:$p['amount'],
                        'purchase'=>0,
                        'sc_product_id'=>0
                    ]);
                    foreach ($stockProduct as $item)
                    {
                        array_push($subtraction,[
                            'id'=>$item['id'],
                            'amount'=>$item['residual_stock'],
                            'purchase'=>$item['purchase_price'],
                            'sc_product_id'=>$item['sc_product_id']
                        ]);
                    }
                    $decrementData=self::decrementStock($subtraction);
                    if(!empty($decrementData))
                    {
                        foreach ($decrementData as $v)
                        {
                            if($v['decrement_amount']!=0)
                            {
                                $arrayOfPurchase[]=[
                                    'id'=>$v['id'],
                                    'purchase'=>$v['purchase'],
                                    'sc_product_id'=>$v['sc_product_id'],
                                    'amount'=>$v['decrement_amount'],
                                    'residual_amount'=>$v['residual_amount']
                                ];
                            }

                        }
                    }
                }
                return $arrayOfPurchase;

            }else{
                $keywordRetur="Retur";
                $stock=StockInventory::whereIn('sc_stock_inventories.sc_product_id',$productId)
                    ->select([
                        'sc_stock_inventories.*',
                        'm.amount'
                    ])
                    ->where('sc_stock_inventories.type','in')
                    ->whereRaw("sc_stock_inventories.transaction_action not like '%".$keywordRetur."%'")
                    ->where('is_deleted',0)
                    ->where('sc_stock_inventories.inv_warehouse_id',$warehouseId)
                    ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                    ->where('m.sc_sale_order_id',$purchase)
                    ->orderBy('sc_stock_inventories.id','desc')
                    ->get();
                $tempStockOut=collect($stock->toArray());
                $totalOfRetur=[];

                foreach ($products as $pIn)
                {
                    $subtraction=[];
                    $stockProductIn=$tempStockOut->where('sc_product_id',$pIn['id'])->sortByDesc('created_at');
                    foreach ($stockProductIn as $v)
                    {
                        $subtraction[]=[
                            'stock'=>$v['total'],
                            'residual'=>$v['residual_stock'],
                            'id'=>$v['id'],
                            'sc_product_id'=>$v['sc_product_id'],
                            'purchase'=>$v['purchase_price']

                        ];
                    }
                    $incrementData=self::incrementStock($pIn['amount'],$subtraction);
                    if(!empty($incrementData))
                    {
                        foreach ($incrementData as $key =>$item)
                        {
                            $totalOfRetur[]=[
                                'id'=>$incrementData[$key]['sc_product_id'],
                                'total'=>$incrementData[$key]['total_increment']*$incrementData[$key]['purchase'],
                                'sc_stock_inventory_id'=>$incrementData[$key]['id'],
                                'total_increment'=>$incrementData[$key]['stock']
                            ];
                        }
                    }
                }
                return $totalOfRetur;

            }

        }catch (\Exception $e)
        {
            log_helper(self::class,'LIFO',$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }

    }

    private static function decrementStock($array)
    {
        $total=$array[0]['amount'];
        $data=[];
        for($i=1;$i<count($array);$i++)
        {
            if($total>=$array[$i]['amount']){
                $data[]=[
                    'decrement_amount'=>$array[$i]['amount'],
                    'residual_amount'=>0,
                    'id'=>$array[$i]['id'],
                    'purchase'=>$array[$i]['purchase'],
                    'sc_product_id'=>$array[$i]['sc_product_id'],

                ];
            }else{
                if($total<0){

                    $data[]=[
                        'decrement_amount'=>0,
                        'residual_amount'=>$array[$i]['amount'],
                        'id'=>$array[$i]['id'],
                        'purchase'=>$array[$i]['purchase'],
                        'sc_product_id'=>$array[$i]['sc_product_id'],
                    ];
                }else{

                    $data[]=[
                        'decrement_amount'=>$total,
                        'residual_amount'=>$array[$i]['amount']-$total,
                        'id'=>$array[$i]['id'],
                        'purchase'=>$array[$i]['purchase'],
                        'sc_product_id'=>$array[$i]['sc_product_id'],
                    ];
                }

            }

            $total -= $array[$i]['amount'];
        }

        return $data;

    }

    private static function incrementStock($amount,$array)
    {
        $total=$amount;
        $data=[];
        for($i=0;$i<count($array);$i++)
        {
            if($total>=($array[$i]['stock']-$array[$i]['residual'])){
                $totalIncrement=$array[$i]['stock']-$array[$i]['residual'];
                $data[]=[
                    'stock'=>$array[$i]['stock'],
                    'total_increment'=>$totalIncrement,
                    'id'=>$array[$i]['id'],
                    'sc_product_id'=>$array[$i]['sc_product_id'],
                    'purchase'=>$array[$i]['purchase']
                ];
                $total-=$totalIncrement;
            }else{
                $data[]=[
                    'stock'=>$array[$i]['residual']+$total,
                    'total_increment'=>$total,
                    'id'=>$array[$i]['id'],
                    'sc_product_id'=>$array[$i]['sc_product_id'],
                    'purchase'=>$array[$i]['purchase']
                ];
                $total=0;
            }
        }

        return $data;
    }

    public static function decrementStockMap($purchaseId,$products)
    {
        try{

            $productId=[];
            foreach ($products as $item)
            {
                $productId[]=$item['id'];
            }
            $stockMap=StockSaleMapping::where('sc_sale_order_id',$purchaseId)
                ->where('amount','>',0)
                ->whereIn('sc_product_id',$productId)->orderBy('id','desc')
                ->get();

            $temp=collect($stockMap->toArray());
            $updateSaleMapping="";
            $p=[];
            foreach ($products as $v)
            {
                $map=$temp->where('sc_product_id',$v['id']);
                $total=$v['amount'];
                foreach ($map as $n => $j)
                {
                    if($total>=$j['amount']){
                        $min2=$j['amount']-$total;
                        $total-=$j['amount'];
                    }else{
                        $min2=$j['amount']-$total;
                        $total=0;
                    }
                    array_push($p,[
                        'id'=>$j['id'],
                        'min'=>$min2
                    ]);
                }
            }
            foreach ($p as $c => $i)
            {
                $updateSaleMapping.=($c==0)?"(".$i['id'].",".$i['min'].")":",(".$i['id'].",".$i['min'].")";

            }
            return $updateSaleMapping;
        }catch (\Exception $e)
        {
            log_helper(self::class,'Error Stock Map',$e->getLine(),$e->getFile(),$e->getMessage());
            return $e;
        }

    }


    public static function subtractionStock($data,$productId,$quantity,$warehouseId)
    {
        try{
            $product=Product::find($productId);
            $stock=StockInventory::where('sc_product_id',$productId)
                ->where('type','in')
                ->whereRaw("transaction_action not like '%Retur%'")
                ->where('is_deleted',0)
                ->where('inv_warehouse_id',$warehouseId)
                ->orderBy('created_at','asc')
                ->get();
            $subtraction=[];
            array_push($subtraction,[
                'id'=>0,
                'amount'=>$quantity,
                'purchase'=>0,
                'sc_product_id'=>0
            ]);
            foreach ($stock as $item)
            {
                array_push($subtraction,[
                    'id'=>$item->id,
                    'amount'=>$item->residual_stock,
                    'purchase'=>$item->purchase_price,
                    'sc_product_id'=>$item->sc_product_id
                ]);
            }
            $decrementData=self::decrementStock($subtraction);
            $updateStockInv="";
            if(!empty($decrementData))
            {
                $stock=0;
                $amount=0;
                foreach ($decrementData as $key =>$v)
                {
                    $isDeleted=($v['residual_amount']==0)?1:0;
                    $updateStockInv.=($key==0)?"(".$v['id'].",".$v['residual_amount'].",".$isDeleted.")":",(".$v['id'].",".$v['residual_amount'].",".$isDeleted.")";
                    $stock+=$v['decrement_amount'];
                    $amount+=$v['decrement_amount']*$v['purchase'];
                }
                if(CoaProductUtilV2::inventoryAdjustment($data->getUser->id,$data->getUser->getMerchant->id,$product,date('Y-m-d H:i:s'),'minus',$amount)==false)
                {
                    return  false;
                }
            }
            return $updateStockInv;
        }catch (\Exception $e)
        {
            log_helper(self::class,'Error Subtract',$e->getLine(),$e->getFile(),$e->getMessage());

            return false;

        }

    }





}

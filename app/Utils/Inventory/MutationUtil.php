<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\Inventory;


use App\Classes\Singleton\CodeGenerator;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Utils\Accounting\CoaUtil;
use Illuminate\Support\Facades\DB;

class MutationUtil
{

    public static function createCoaTransfer($merchantId)
    {
        try{
            $coaTransferStock=CoaCategory::where('md_merchant_id',$merchantId)
                ->where('code','3.1.00')
                ->first();

            $checkCoa=CoaDetail::select([
                'acc_coa_details.*'
            ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)
                ->where('c.code','3.1.00')
                ->where('acc_coa_details.name','Transfer Persediaan Cabang')
                ->first();
            $coaFormat=CoaCashflowFormat::where('name','Modal')
                ->where('type','pendanaan')
                ->where('md_merchant_id',$merchantId)
                ->first();

            if(is_null($checkCoa)){
                $checkCoa=new CoaDetail();
                $checkCoa->code=CoaUtil::generateCoaDetail($coaTransferStock->id);
                $checkCoa->name='Transfer Persediaan Cabang';
                $checkCoa->acc_coa_category_id=$coaTransferStock->id;
                $checkCoa->is_deleted=0;
                $checkCoa->md_sc_currency_id=1;
                $checkCoa->type_coa='Kredit';
                $checkCoa->save();

                if(!is_null($coaFormat))
                {
                    $checkFormat=CoaCashflowFormatDetail::where('acc_coa_detail_id',$checkCoa->id)
                        ->where('acc_coa_cashflow_format_id',$coaFormat->id)
                        ->where('is_deleted',0)
                        ->first();
                    if(is_null($checkFormat))
                    {
                        CoaCashflowFormatDetail::insert([
                            'acc_coa_cashflow_format_id'=>$coaFormat->id,
                            'acc_coa_detail_id'=>$checkCoa->id,
                            'is_deleted'=>0,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);
                    }

                }
                return $checkCoa;
            }else{
                if(!is_null($coaFormat))
                {
                    $checkFormat=CoaCashflowFormatDetail::where('acc_coa_detail_id',$checkCoa->id)
                        ->where('acc_coa_cashflow_format_id',$coaFormat->id)
                        ->where('is_deleted',0)
                        ->first();
                    if(is_null($checkFormat))
                    {
                        CoaCashflowFormatDetail::insert([
                            'acc_coa_cashflow_format_id'=>$coaFormat->id,
                            'acc_coa_detail_id'=>$checkCoa->id,
                            'is_deleted'=>0,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);
                    }

                }
                return  $checkCoa;
            }


        }catch (\Exception $e)
        {
            return false;
        }
    }

    public static function createJurnalMutation($data,$fromMerchant,$toMerchant,$coaTf1,$coaTf2,$jurnalDetailOut,$jurnalDetailIn,$total,$amount)
    {
        try {
            DB::beginTransaction();

            $j=new  Jurnal();
            $mutationCode=(!is_null($data->second_code))?$data->second_code:$data->code;

            $j->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$fromMerchant->id);
            $j->ref_code=$data->code;
            $j->trans_name='Transfer Persediaan Antar Cabang '.$mutationCode;
            $j->trans_time=$data->created_at;
            $j->trans_note='Transfer Persediaan Antar Cabang '.$mutationCode;
            $j->trans_purpose='Transfer Persediaan Antar Cabang '.$mutationCode;
            $j->trans_amount=$total;
            $j->md_merchant_id=$fromMerchant->id;
            $j->md_user_id_created=(get_role()==3)?user_id():get_staff_id();
            $j->external_ref_id=$data->id;
            $j->timezone=$data->timezone;
            $j->flag_name='Transfer Persediaan Antar Cabang '.$mutationCode;
            $j->save();

            $j2=new  Jurnal();
            $j2->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$toMerchant->id);
            $j2->ref_code=$data->code;
            $j2->trans_name='Transfer Persediaan Antar Cabang  '.$mutationCode;
            $j2->trans_time=$data->created_at;
            $j2->trans_note='Transfer Persediaan Antar Cabang '.$mutationCode;
            $j2->trans_purpose='Transfer Persediaan Antar Cabang '.$mutationCode;
            $j2->trans_amount=$total;
            $j2->md_merchant_id=$toMerchant->id;
            $j2->md_user_id_created=(get_role()==3)?user_id():get_staff_id();
            $j2->external_ref_id=$data->id;
            $j2->timezone=$data->timezone;
            $j2->flag_name='Transfer Persediaan Antar Cabang '.$mutationCode;
            $j2->save();

            $detJurnal=[];



            foreach ($jurnalDetailOut as $key =>$item)
            {
                if($key==0)
                {
                    array_push($detJurnal,
                        [
                            'acc_coa_detail_id'=>$coaTf1->id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$total,
                            'created_at'=>$data->created_at,
                            'updated_at'=>$data->updated_at,
                            'acc_jurnal_id'=>$j->id,
                        ]);
                }
                array_push($detJurnal,[
                    'acc_coa_detail_id'=>$item['acc_coa_detail_id'],
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$amount[$key],
                    'created_at'=>$data->created_at,
                    'updated_at'=>$data->updated_at,
                    'acc_jurnal_id'=>$j->id,
                ]);
            }

            foreach ($jurnalDetailIn as $key =>$item)
            {
                array_push($detJurnal,[
                    'acc_coa_detail_id'=>$item['acc_coa_detail_id'],
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$amount[$key],
                    'created_at'=>$data->created_at,
                    'updated_at'=>$data->updated_at,
                    'acc_jurnal_id'=>$j2->id,
                ]);
            }

            array_push($detJurnal,
                [
                    'acc_coa_detail_id'=>$coaTf2->id,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$total,
                    'created_at'=>$data->created_at,
                    'updated_at'=>$data->updated_at,
                    'acc_jurnal_id'=>$j2->id,
                ]);


            JurnalDetail::insert($detJurnal);
            DB::commit();

            return true;

        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }
}

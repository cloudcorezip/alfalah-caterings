<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\Inventory;


use App\Classes\Singleton\CodeGenerator;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Utils\Accounting\CoaUtil;
use Illuminate\Support\Facades\DB;

class ConsignmentUtil
{

    public static function generateCoaConsignment($merchantId)
    {
        try {
            $coaUtang=CoaCategory::where('code','2.1.01')->where('md_merchant_id',$merchantId)->first();
            $checkCoa=CoaDetail::select([
                'acc_coa_details.*'
            ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)
                ->where('c.code','2.1.01')
                ->where('acc_coa_details.name','Utang Konsinyasi')
                ->first();

            if(is_null($checkCoa)){
                $checkCoa=new CoaDetail();
                $checkCoa->code=CoaUtil::generateCoaDetail($coaUtang->id);
                $checkCoa->name='Utang Konsinyasi';
                $checkCoa->acc_coa_category_id=$coaUtang->id;
                $checkCoa->is_deleted=0;
                $checkCoa->md_sc_currency_id=1;
                $checkCoa->type_coa='Kredit';
                $checkCoa->save();

                return $checkCoa;
            }else{
                return  $checkCoa;
            }

        }catch (\Exception $e)
        {
            return false;

        }

    }

    public static function insertConsignmentJurnal($merchant,$data,$coaConsignment,$product,$total)
    {
        try{
            DB::beginTransaction();

            $j=new  Jurnal();
            $consCode=is_null($data->second_code)?$data->code:$data->second_code;
            $j->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$merchant->id);
            $j->ref_code=$data->code;
            $j->trans_name='Penerimaan Konsinyasi '.$consCode;
            $j->trans_time=$data->created_at;
            $j->trans_note='Penerimaan Konsinyasi  '.$consCode;
            $j->trans_purpose='Penerimaan Konsinyasi '.$consCode;
            $j->trans_amount=$total;
            $j->md_merchant_id=$merchant->id;
            $j->md_user_id_created=(get_role()==3)?user_id():get_staff_id();
            $j->external_ref_id=$data->id;
            $j->timezone=$data->timezone;
            $j->flag_name='Penerimaan Konsinyasi '.$consCode;
            $j->save();

            $detJurnal=[];

            foreach ($product as $key =>$item)
            {
                array_push($detJurnal,[
                    'acc_coa_detail_id'=>$item['inv_id'],
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$item['amount'],
                    'created_at'=>$data->created_at,
                    'updated_at'=>$data->updated_at,
                    'acc_jurnal_id'=>$j->id,
                ]);
            }

            array_push($detJurnal,[
                'acc_coa_detail_id'=>$coaConsignment->id,
                'coa_type'=>'Kredit',
                'md_sc_currency_id'=>1,
                'amount'=>$total,
                'created_at'=>$data->created_at,
                'updated_at'=>$data->updated_at,
                'acc_jurnal_id'=>$j->id,
            ]);

            JurnalDetail::insert($detJurnal);
            DB::commit();

            return true;

        }catch (\Exception $e)
        {
            return false;
        }

    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\Inventory;


use App\Models\SennaToko\StockInventory;
use Illuminate\Support\Facades\DB;

class StockAdjustmentUtil
{


    public static function subtraction($productId,$quantity,$warehouse,$isDelete=0)
    {
        try{
            DB::beginTransaction();
            $stock=StockInventory::where('sc_product_id',$productId)
                ->where('type','in')
                ->whereRaw("transaction_action not like '%Retur%'")
                ->where('is_deleted',0)
                ->where('inv_warehouse_id',$warehouse->id)
                ->orderBy('created_at','asc')
                ->get();

            $initial=[$quantity];
            $subtraction=[];
            foreach ($stock as $item)
            {
                $subtraction[]=$item->residual_stock;
            }
            $decrementData=self::decrementStock(array_merge($initial,$subtraction),$isDelete);

            $invs=[];
            $totalAmountDecrement=0;
            if(!empty($decrementData))
            {
                foreach ($stock as $key => $v)
                {
                    $inv=StockInventory::find($v->id);

                    if($v->residual_stock!=$decrementData[$key]['residual_amount'])
                    {
                        $invs[]=[
                            'stock_inventory_id'=>$v->id,
                            'stock_put'=>$decrementData[$key]['decrement_amount']
                        ];


                        $inv->residual_stock=$decrementData[$key]['residual_amount'];
                        if($warehouse->is_default==1)
                        {
                            $v->getProduct->stock-=$decrementData[$key]['decrement_amount'];
                            $v->getProduct->save();
                        }

                        $totalAmountDecrement+=$decrementData[$key]['decrement_amount']*$v->purchase_price;

                        if($decrementData[$key]['residual_amount']==0)
                        {
                            $inv->is_deleted=1;
                        }
                        $inv->save();


                    }

                }
            }

            DB::commit();
            return [
                'totalAmountDecrement'=>$totalAmountDecrement,
                'invs'=>$invs
            ];

        }catch (\Exception $e)
        {

            DB::rollBack();
            return false;

        }

    }

    private static  function decrementStock($array,$isDelete)
    {
        $total=$array[0];
        $data=[];
        for($i=1;$i<count($array);$i++)
        {
            if($total>=$array[$i]){
                $data[]=[
                    'decrement_amount'=>$array[$i],
                    'residual_amount'=>0,
                ];

            }else{
                if($total<0){

                    $data[]=[
                        'decrement_amount'=>0,
                        'residual_amount'=>$array[$i],
                    ];
                }else{

                    $data[]=[
                        'decrement_amount'=>$total,
                        'residual_amount'=>$array[$i]-$total
                    ];
                }

            }

            $total -= $array[$i];
        }

        return $data;

    }



}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Inventory;


use App\Classes\Singleton\CodeGenerator;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use Illuminate\Support\Facades\DB;

class ProductionOfGoodUtil
{

    public static function createProductionOfGood($merchantId,$userId,$pod,$product,$material)
    {
        try {
            DB::beginTransaction();
            $inventoryAccount=CoaCategory::where('code','1.1.05')
                ->where('md_merchant_id',$merchantId)->first()->id;

            $data=new  Jurnal();
            $data->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);
            $data->ref_code=$pod->code;
            $data->trans_name='Produksi Produk '.$pod->code.'-'.$product->code;
            $data->trans_time=$pod->time_of_production;
            $data->trans_note='Produksi Produk  '.$pod->code.'-'.$product->code;
            $data->trans_purpose='Produksi Produk '.$pod->code.'-'.$product->code;
            $data->md_merchant_id=$merchantId;
            $data->md_user_id_created=$userId;
            $data->external_ref_id=$pod->id;
            $data->flag_name='Produksi Produk '.$pod->code.'-'.$product->code;
            $data->save();
            $fromCoa=CoaDetail::where([
                'acc_coa_category_id'=>$inventoryAccount,
                'ref_external_id'=>$pod->sc_product_id,
                'ref_external_type'=>1
            ])->first();


            $insert=[];
            $total=0;
            foreach ($material as $item)
            {
                $toCoa=CoaDetail::where([
                    'acc_coa_category_id'=>$inventoryAccount,
                    'ref_external_id'=>$item->child_sc_product_id,
                    'ref_external_type'=>1
                ])->first();
                $subTotal=$pod->getDetail->where('sc_product_id',$item->child_sc_product_id)
                    ->sum('sub_total');
                $total+=$subTotal;
                $insert[]=[
                    'acc_coa_detail_id'=>$toCoa->id,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$subTotal,
                    'created_at'=>$pod->time_of_production,
                    'updated_at'=>$pod->time_of_production,
                    'acc_jurnal_id'=>$data->id,
                    'ref_id'=>$item->id,
                    'from_trans_type'=>3
                ];
            }
            JurnalDetail::insert([
                'acc_coa_detail_id'=>$fromCoa->id,
                'coa_type'=>'Debit',
                'md_sc_currency_id'=>1,
                'amount'=>$total,
                'created_at'=>$pod->time_of_production,
                'updated_at'=>$pod->time_of_production,
                'acc_jurnal_id'=>$data->id,
                'ref_id'=>$item->id,
                'from_trans_type'=>3
            ]);

            JurnalDetail::insert($insert);
            $data->trans_amount=$total;
            $data->save();
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;

        }
    }

}

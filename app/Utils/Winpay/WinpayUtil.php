<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Winpay;


use App\Models\MasterData\Merchant;
use Carbon\Carbon;

class WinpayUtil
{


    public static function OpenSSLEncrypt($message, $key)
    {
        try{
            $encrypt_method = "AES-256-CBC";
            $secret_key = $key;
            $secret_iv = $key;
            $key = hash('sha256', $secret_key);
            $iv = substr(hash('sha256', $secret_iv), 0, 16);
            $output = openssl_encrypt($message, $encrypt_method, $key, 0, $iv);
            $output = trim(base64_encode($output));
            return $output;
        }catch (\Exception $e)
        {
            return  false;

        }
    }


    public static function generateOpenSSLEncrypt($merchantId,$amount,$data, $token,$ref)
    {
        try{
            $merchant=Merchant::find($merchantId);
            $date=date('Ymd').'235959';
            $merchant_key = env("WINPAY_MERCHANT_KEY");
            $spi_token = env('WINPAY_KEY_1').env('WINPAY_KEY_2');
            $spi_amount = $amount;
            $spi_amount = number_format(doubleval($spi_amount),2,".","");
            $spi_signature = strtoupper(sha1( $spi_token . '|' . $merchant_key . '|' . $ref . '|' . $spi_amount . '|0|0' ));
            $json_string =[
                "cms"=> "WINPAY API",
                "spi_callback"=> route('winpay.listener'),
                "url_listener"=> route('winpay.listener'),
                "spi_currency"=> "IDR",
                "spi_item"=> json_decode($data),
                "spi_amount"=> $amount,
                "spi_signature"=>"$spi_signature",
                "spi_token"=> "$spi_token",
                "spi_merchant_transaction_reff"=> "$ref",
                "spi_billingPhone"=> $merchant->phone_merchant,
                "spi_billingEmail"=> $merchant->getUser->email,
                "spi_billingName"=> $merchant->getUser->fullname,
                "spi_paymentDate"=> $date,
                "spi_qr_type"=> "dynamic",
                "spi_qr_subname"=> $merchant->name
            ];

            $messageEncrypted = self::OpenSSLEncrypt(json_encode($json_string), $token);
            $orderData = substr($messageEncrypted, 0, 10). $token. substr($messageEncrypted, 10);
            return $orderData;
        }catch (\Exception $e)
        {
            return  false;

        }
    }


    private static function generateBasicAuth()
    {
        $privateKey1 = env("WINPAY_KEY_1");
        $privateKey2 = env("WINPAY_KEY_2");
        return base64_encode($privateKey1 . ":" . $privateKey2);
    }

    public static  function getToken()
    {
        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "".env('WINPAY_PROD')."token");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/x-www-form-urlencoded",
                "Authorization: Basic ".self::generateBasicAuth().""
            ));

            $response = curl_exec($ch);
            curl_close($ch);

            $result=json_decode($response);
            return $result->data->token;
        }catch (\Exception $e)
        {
            return $e;

        }


    }


    public static function inquiryQRIS($token,$ref,$data,$amount,$merchant)
    {
        try{
            $date=date('Ymd').'235959';
            $merchant_key = env("WINPAY_MERCHANT_KEY");
            $spi_token = env('WINPAY_KEY_1').env('WINPAY_KEY_2');
            $spi_amount = $amount;
            $spi_amount = number_format(doubleval($spi_amount),2,".","");
            $spi_signature = strtoupper(sha1( $spi_token . '|' . $merchant_key . '|' . $ref . '|' . $spi_amount . '|0|0' ));
            $json_string =[
                "cms"=> "WINPAY API",
                "spi_callback"=> route('winpay.listener'),
                "url_listener"=> route('winpay.listener'),
                "spi_currency"=> "IDR",
                "spi_item"=> $data,
                "spi_amount"=> $amount,
                "spi_signature"=>"$spi_signature",
                "spi_token"=> "$spi_token",
                "spi_merchant_transaction_reff"=> "$ref",
                "spi_billingPhone"=> $merchant->phone_merchant,
                "spi_billingEmail"=> $merchant->getUser->email,
                "spi_billingName"=> $merchant->getUser->fullname,
                "spi_paymentDate"=> $date,
                "spi_qr_type"=> "dynamic",
                "spi_qr_subname"=> $merchant->name
            ];
            $messageEncrypted = self::OpenSSLEncrypt(json_encode($json_string), $token);
            $orderData = substr($messageEncrypted, 0, 10). $token. substr($messageEncrypted, 10);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"".env('WINPAY_PROD')."apiv2/QRISPAY");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_POST, TRUE);

            curl_setopt($ch, CURLOPT_POSTFIELDS, "orderdata=$orderData");

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/x-www-form-urlencoded"
            ));

            $response = curl_exec($ch);
            curl_close($ch);

            $result=json_decode($response);
            return $result;
        }catch (\Exception $e)
        {
            return false;
        }


    }

}

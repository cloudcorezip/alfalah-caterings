<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Utils\Winpay;


class RajaBillerUtil
{
    private static function requestJson($data){
        try{
            $api_url = env('RAJA_BILLER');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $api_url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 0);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 500);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $result = curl_exec($ch);
            return json_decode($result);
        }catch (\Exception $e)
        {
            return false;
        }

    }

    public static function inquiryTransferDana($accountNumber,$amount,$bankCode,$phoneNumber)
    {
        try{
            $params=[
                "method"      => "rajabiller.transferinq",
                "uid"         => env('UID_RAJA_BILLER'),
                "pin"         => env('PIN_RAJA_BILLER'),
                "idpel1"      => $accountNumber,
                "idpel2"      => "",
                "idpel3"    => "",
                "kode_produk" => "BLTRFAG",
                "ref1"        => "",
                "nominal"     => $amount,
                "kodebank"    => $bankCode,
                "nomorhp"   => $phoneNumber
            ];

            $result=self::requestJson($params);
            return $result;

        }catch (\Exception $e)
        {
            return false;
        }

    }

    public static function pay($accountNumber,$amount,$bankCode,$phoneNumber,$transId)
    {
        try {
            $params=[
                "method"      => "rajabiller.transferpay",
                "uid"         => env('UID_RAJA_BILLER'),
                "pin"         => env('PIN_RAJA_BILLER'),
                "idpel1"      => $accountNumber,
                "idpel2"      => "",
                "idpel3"    => "",
                "kode_produk" => "BLTRFAG",
                "ref1"        => "",
                "ref2"        => $transId,
                "nominal"     => $amount,
                "kodebank"    => $bankCode,
                "nomorhp"   => $phoneNumber
            ];
            $result=self::requestJson($params);
            return $result;
        }catch (\Exception $e)
        {
            return false;

        }
    }

    public static function ipCheck()
    {
        try {
            $params=[
                "method"=> "rajabiller.cekip"
            ];

            $result=self::requestJson($params);
            return $result;

        }catch (\Exception $e)
        {
            return false;
        }
    }

    public static function balanceCheck()
    {
        try {
            $params=[
                "method"  => "rajabiller.balance",
                "uid"     => env('UID_RAJA_BILLER'),
                "pin"     => env("PIN_RAJA_BILLER")
            ];
            $result=self::requestJson($params);
            return $result;
        }catch (\Exception $e)
        {
            return false;

        }
    }

    public static function transactionCheck($idTrans,$idProduct,$idPel,$date)
    {
        try{
            $params=[
                "method"      => "rajabiller.datatransaksi",
                "uid"     => env('UID_RAJA_BILLER'),
                "pin"     => env("PIN_RAJA_BILLER"),
                "tgl1"        => $date,
                "tgl2"        => $date,
                "id_transaksi"=> $idTrans,
                "id_produk"   => $idProduct,
                "idpel"       => $idPel,
                "limit"       => "",
                "ref1"        => ""
            ];
            $result=self::requestJson($params);
            return $result;

        }catch (\Exception $e)
        {
            return false;
        }
    }



}

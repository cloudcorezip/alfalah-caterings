<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\SFA;


use App\Models\SennaToko\Sfa\SfaConfig;

class SfaInitUtil
{

    public static function init($merchantId)
    {
        if(merchant_detail_multi_branch($merchantId)->is_branch==0){
            $sfaConfigCheck=SfaConfig::where('md_merchant_id',$merchantId)->first();
            if(is_null($sfaConfigCheck)){
                $newSfaConfig= new SfaConfig();
                $newSfaConfig->md_merchant_id=$merchantId;
                $newSfaConfig->save();
            }
        }
        return true;
    }

}

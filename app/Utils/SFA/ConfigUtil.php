<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils\SFA;


use App\Models\SennaToko\Sfa\SfaConfig;
use App\Utils\Merchant\MerchantUtil;

class ConfigUtil
{

    public static function checkPermission($merchantId,$type='')
    {
        $config=SfaConfig::where('md_merchant_id',MerchantUtil::getHeadBranch($merchantId))->first();
        if(is_null($config)){
            return false;
        }else{
            if($type=='target'){
                if($config->is_use_sale_target==0){
                    return false;
                }else{
                    return true;
                }
            }
            if($type=='visit'){
                if($config->is_use_visit_schedule==0){
                    return false;
                }else{
                    return true;
                }
            }
        }
    }
}

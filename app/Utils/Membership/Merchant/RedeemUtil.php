<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Membership\Merchant;


use App\Classes\Singleton\CodeGenerator;
use App\Models\Marketing\MerchantCommission;
use App\Models\Marketing\MerchantHistoryRedeemPoint;
use App\Models\Marketing\MerchantMembership;
use App\Models\Marketing\MerchantMembershipDetail;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\TransactionStatus;
use App\Utils\Subscription\SubscriptionUtil;
use Illuminate\Support\Facades\DB;

class RedeemUtil
{

    public static function checkReferalCode($ref,$merchantId,$subscriptionId)
    {
        try{
            $check=MerchantMembership::where('refferal_code',$ref)->first();
            if($check)
            {
                $detail=MerchantMembershipDetail::where('sm_merchant_membership_id',$check->id)
                    ->where('member_merchant_id',$merchantId)->first();
                if(is_null($detail))
                {
                    $check->point+=self::calculatePoint($subscriptionId);
                    $check->save();
                    $detail=new MerchantMembershipDetail();
                    $detail->sm_merchant_membership_id=$check->id;
                    $detail->member_merchant_id=$merchantId;
                    $detail->save();
                }
                return true;
            }
            return  true;
        }catch (\Exception $e)
        {
            return false;
        }

    }

    private static function calculatePoint($subscriptionId)
    {
        try{
            $data=MerchantCommission::where('md_subscription_id',$subscriptionId)->first();
            if(is_null($data))
            {
                return 0;
            }else{
                return $data->fee/1000;
            }
        }catch (\Exception $e)
        {
            return 0;
        }
    }

    public static function reducePointBySubscription($user,$merchantId,$redeemPointId,$point,$subscription)
    {
        try{
            DB::beginTransaction();
            $check=MerchantMembership::where('md_merchant_id',$merchantId)->first();
            if($check)
            {
                $check->point-=$point;
                $check->save();
                $endDate=SubscriptionUtil::calculate($check->getMerchant->end_subscription,$subscription->type,$subscription->plus_month);
                $check->getMerchant->end_subscription=$endDate;
                $check->getMerchant->save();
                $history=new MerchantHistoryRedeemPoint();
                $code=CodeGenerator::redeemCode($merchantId);
                $history->code=$code;
                $history->md_merchant_id=$merchantId;
                $history->sm_merchant_redeem_point_id=$redeemPointId;
                $history->point_exchange=$point;
                $history->md_transaction_status_id=\App\Models\MasterData\SennaPayment\TransactionStatus::SUCCESS;
                $history->save();
                $history->transaction()->create([
                    'code'=>$code,
                    'md_user_id'=>$user->id,
                    'md_sp_transaction_type_id'=>TransactionType::SUBSCRIPTION_FEE,
                    'md_sp_transaction_status_id'=> \App\Models\MasterData\SennaPayment\TransactionStatus::SUCCESS,
                    'amount'=>$point,
                    'transaction_from'=>'Penukaran point oleh '.$user->fullname,
                    'transaction_to'=>'Penukaran point dengan menambah berlangganan'
                ]);
                DB::commit();
                return true;
            }
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

    public static function checkPoint($merchantId,$point)
    {
        return ((MerchantMembership::where('md_merchant_id',$merchantId)->first())->point>=$point)?1:0;
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils;


use App\Models\MasterData\SennaCashier\ShippingCategory;

class ExpeditionUtil
{

    public static function getList($merchantId,$type=0)
    {

        $shipping=ShippingCategory::all();

        $noExpedition=(object)[
            'id'=>$shipping->firstWhere('id',27)->id,
            'name'=>$shipping->firstWhere('id',27)->name,
            'is_biteship'=>0,
            'alias'=>$shipping->firstWhere('id',3)->name,
            'key'=>27
        ];

        $inLocation=(object)[
            'id'=>$shipping->firstWhere('id',3)->id,
            'name'=>$shipping->firstWhere('id',3)->name,
            'is_biteship'=>0,
            'alias'=>$shipping->firstWhere('id',3)->alias,
            'key'=>3
        ];

        $internal=(object)[
            'id'=>$shipping->firstWhere('id',28)->id,
            'name'=>$shipping->firstWhere('id',28)->name,
            'is_biteship'=>0,
            'alias'=>$shipping->firstWhere('id',28)->alias,
            'key'=>28
        ];

        $expeditionLabel=[];

        foreach ($shipping->where('parent_id',1)->all() as $item){
            $expeditionLabel[]=(object)[
                'id'=>$item->id,
                'name'=>$item->name,
                'is_biteship'=>0,
                'alias'=>$item->alias,
                'key'=>1
            ];
        }

        return [
           [
               'name'=>'Tidak Memakai Pengiriman',
               'data'=>(object) $noExpedition,
               'is_group'=>0,

           ],
            [
                'name'=>$shipping->firstWhere('id',28)->name,
                'data'=>(object) $internal,
                'is_group'=>0,

            ],
            [
                'name'=>$shipping->firstWhere('id',3)->name,
                'data'=>(object) $inLocation,
                'is_group'=>0,

            ],[
                'name'=>'Pengiriman Lainnya (Hanya Label)',
                'data'=>(object) $expeditionLabel,
                'is_group'=>1,
            ]
        ];


    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\ThirdParty;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\XenditCore;
use App\Models\MasterData\User;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class VirtualAccountUtil
{
    protected $xendit;
    protected $user;

    public function __construct()
    {
        $this->xendit=XenditCore::getInstance();
        $this->user=User::class;
    }

    public function create($userId,$vaCode,$amount,$type,$codeParams='')
    {

        try{
            DB::beginTransaction();
            $user=$this->user::find($userId);

            if(is_null($user))
            {
                return false;
            }
            if($codeParams!=''){
                $code=$codeParams;
            }else{
                $code=$this->generate($userId,$type);

            }
            $bankCode=$this->xendit::bankCode();
            $valueBank=$bankCode[$vaCode];
            $va=(string)rand(config('va.'.$valueBank.'.min'),config('va.'.$valueBank.'.max'));
            if($this->checkVA($valueBank,$userId,$va)==true)
            {
                $va=(string)rand(config('va.'.$valueBank.'.min'),config('va.'.$valueBank.'.max'));
            }
            $params = [
                "external_id" => (string)Uuid::uuid4()->toString(),
                "bank_code" => $valueBank,
                "name" => $user->fullname,
                'virtual_account_number'=>$va,
                'expected_amount'=>(float)$amount,
                'is_closed'=>true
            ];
            $response=$this->xendit::createVirtualAccount($params);
            if($response!=false)
            {
                $response['code']=$code;
                DB::commit();
                return $response;
            }else{
                DB::rollBack();
                return false;
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;

        }

    }

    private function checkVA($bankCode,$userId,$vaNumber)
    {
        return (!is_null($this->user::where('va_'.strtolower($bankCode).'',$vaNumber)
            ->where('id','!=',$userId)
            ->first()))?true:false;
    }

    private function generate($userId,$type)
    {
        return CodeGenerator::generateTransactionCode($userId,$type);
    }




}

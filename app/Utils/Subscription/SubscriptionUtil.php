<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Subscription;


use App\Classes\Singleton\CodeGenerator;
use App\Models\Marketing\Promo;
use App\Models\MasterData\Subscription;
use App\Models\MasterData\TransactionType;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SubscriptionUtil
{

    public static function calculateSubsWithSennaPay($request,$user,$subscription)
    {
        try{
            DB::beginTransaction();
            $user->balance-=$request->amount;
            $user->save();
            if(is_null($user->getMerchant->start_subscription))
            {
                $endDate=self::calculate(date('Y-m-d H:i:s'),$subscription->type,$subscription->plus_month);
                $user->getMerchant->is_active_subscription=1;
                $user->getMerchant->start_subscription=date('Y-m-d H:i:s');
                $user->getMerchant->end_subscription=$endDate;
                $user->getMerchant->save();
                DB::commit();
            }else{
                $endDate=self::calculate(date('Y-m-d H:i:s'),$subscription->type,$subscription->plus_month);
                $user->getMerchant->is_active_subscription=1;
                $user->getMerchant->start_subscription=date('Y-m-d H:i:s');
                $user->getMerchant->end_subscription=$endDate;
                $user->getMerchant->save();
                DB::commit();
            }
            return $endDate;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }

    }


    public static function calculateSubsVAOrQRIS($user,$subscription)
    {
        try{
            if(is_null($user->getMerchant->start_subscription))
            {
                $endDate=self::calculate(date('Y-m-d H:i:s'),$subscription->type,$subscription->plus_month);
            }else{
                if($user->getMerchant->start_subscription>date('Y-m-d H:i:s')){
                    $plusDate=$user->getMerchant->start_subscription;
                }else{
                    $plusDate=date('Y-m-d H:i:s');
                }
                $endDate=self::calculate($plusDate,$subscription->type,$subscription->plus_month);
            }
            return $endDate;
        }catch (\Exception $e)
        {
            return false;
        }

    }

    public static function calculate($date,$type,$plus=0)
    {
        $carbon=new Carbon($date);
        if($type==Subscription::MONTHLY)
        {
            $extended=(int)$plus+1;
            $endDate=$carbon->addMonths($extended)->toDateTimeString();
        }elseif ($type==Subscription::THREEMONTH){
            $extended=(int)$plus+3;
            $endDate=$carbon->addMonths($extended)->toDateTimeString();
        }elseif ($type==Subscription::SIX_MONTH)
        {
            $extended=(int)$plus+6;
            $endDate=$carbon->addMonths($extended)->toDateTimeString();
        }elseif ($type==Subscription::YEARLY)
        {
            $extended=(int)$plus+12;
            $endDate=$carbon->addMonths($extended)->toDateTimeString();
        }

        return $endDate;
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Merchant;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantBranch;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\Subscription;
use App\Models\SennaJasa\OrderService;
use App\Models\SennaPayment\PaymentSubscription;
use App\Models\SennaToko\SaleOrder;
use Carbon\Carbon;

class MerchantUtil
{

    public static function checkMerchant($merchantId)
    {
        return (is_null(Merchant::find($merchantId)))?null:1;
    }

    public static function checkSubscription($merchantId)
    {
        return (is_null((Merchant::find($merchantId))->md_subscription_id))?null:1;

    }

    public static function maxStaff($merchantId)
    {
        $staff=MerchantStaff::where('md_merchant_id',$merchantId)->where('is_deleted',0)
            ->count();
        if(is_null(self::getSubscription($merchantId)))
        {
            if($staff>2)
            {
                return false;
            }else{
                return true;
            }
        }else{
            if(self::getSubscription($merchantId)->max_staff==0)
            {
                return true;
            }else{
                if($staff>self::getSubscription($merchantId)->max_staff)
                {
                    return false;
                }else{
                    return true;
                }
            }

        }



    }

    public static function maxBranch($merchantId)
    {
        $branch=MerchantBranch::where('head_merchant_id',$merchantId)->where('is_active',1)
            ->
            count();
        if(is_null(self::getSubscription($merchantId)))
        {
            if($branch>1)
            {
                return false;
            }else{
                return true;
            }
        }else{
            if(self::getSubscription($merchantId)->max_branch==0)
            {
                return true;
            }else{
                if($branch>self::getSubscription($merchantId)->max_branch)
                {
                    return false;
                }
                return true;
            }

        }



    }

    public static function maxTransaction($merchantId)
    {
        $sale=SaleOrder::where('md_merchant_id',$merchantId)
            ->whereMonth('created_at',date('m'))
            ->count();
        if(is_null(self::getSubscription($merchantId)))
        {
            if($sale>=10000)
            {
                return false;
            }else{
                return  true;
            }
        }else{
            if(self::getSubscription($merchantId)->max_transaction==0)
            {
                return true;
            }else{
                if($sale>=self::getSubscription($merchantId)->max_transaction)
                {
                    return false;
                }else{
                    return true;
                }
            }
        }



    }

    public static function maxTransactionService($merchantId)
    {
        $sale=OrderService::where('md_merchant_id',$merchantId)
            ->count();
        if(is_null(self::getSubscription($merchantId)))
        {
            if($sale>=10000)
            {
                return false;
            }
            return true;
        }else{
            if(self::getSubscription($merchantId)->max_transaction==0)
            {
                return true;
            }else{
                if($sale>=self::getSubscription($merchantId)->max_transaction)
                {
                    return false;
                }else{
                    return true;
                }
            }
        }



    }
    public static function maxOnlineTransaction($merchantId)
    {
        $sale=SaleOrder::where('md_merchant_id',$merchantId)
            ->where('is_from_senna_app',1)
            ->whereDate('created_at',date('Y-m-d'))
            ->count();
        if(is_null(self::getSubscription($merchantId)))
        {
            if($sale>10)
            {
                return false;
            }
            return true;
        }else{
            if(self::getSubscription($merchantId)->max_transaction_online==0)
            {
                return true;
            }else{
                if($sale>self::getSubscription($merchantId)->max_transaction_online)
                {
                    return false;
                }else{
                    return true;
                }
            }

        }

    }


    private static function getSubscription($merchantId)
    {
        $merchant=Merchant::where('id',$merchantId)->first();
        return $merchant->getSubscription;
    }

    public static function maxStaffFree($merchantId)
    {
        $staff=MerchantStaff::where('md_merchant_id',$merchantId)->where('is_deleted',0)
            ->count();
        if($staff==1){
            return false;
        }else{
            return true;
        }
    }




    public  static function getBranch($merchantId,$type=0)
    {
        $result=[];
        if($type==0)
        {
            foreach (self::getAllBranch($merchantId) as $key =>$item){
                $result[]=$item->id;
            }

        }else{

            foreach (self::getAllBranch(self::getTopHeadBranch($merchantId)) as $key =>$item){
                $result[]=$item->id;
            }
        }

        return $result;
    }


    public static function getBranchList($merchantId,$type=0)
    {
        if($type==0)
        {
            return self::getAllBranch($merchantId);

        }else{
            return self::getAllBranch(self::getTopHeadBranch($merchantId));
        }
    }


    private  static  function getAllBranch($merchantId) {

        $merchant=Merchant::find($merchantId);
        $getBranch=$merchant->getBranch;
        $result=[];
        if($getBranch->count()>0)
        {
            array_push($result,(object)[
                'id'=>$merchant->id,
                'nama_cabang'=>$merchant->name,
                'owner_user_id'=>$merchant->md_user_id,
                'head_id'=>$merchant->id,
                'head_name'=>$merchant->name
            ]);

            foreach($getBranch as $item) {
                if($item->is_active==1){
                    array_push($result,(object)[
                        'id'=>$item->getMerchant->id,
                        'nama_cabang'=>$item->getMerchant->name,
                        'owner_user_id'=>$item->getMerchant->md_user_id,
                        'head_id'=>$item->getHead->id,
                        'head_name'=>$item->getHead->name
                    ]);
                    if(!empty(self::itemWithChildren($item->getChildBranch))){
                        foreach (self::itemWithChildren($item->getChildBranch) as $cc => $c){
                            array_push($result,(object)[
                                'id'=>$c['id'],
                                'nama_cabang'=>$c['nama_cabang'],
                                'owner_user_id'=>$c['user_id'],
                                'head_id'=>$item->id,
                                'head_name'=>$item->getMerchant->name
                            ]);
                        }
                    }
                }
            }
        }else{
            array_push($result,(object)[
                'id'=>$merchant->id,
                'nama_cabang'=>$merchant->name,
                'owner_user_id'=>$merchant->md_user_id,
                'head_id'=>$merchant->id,
                'head_name'=>$merchant->name,
            ]);


        }

        return (object)$result;
    }

    private static function itemWithChildren($item) {
        $result = [];
        if($item->count()>0){
            foreach ($item as $child) {
                if($child->is_active==1){
                    $result[]=[
                        'id'=>$child->getMerchant->id,
                        'nama_cabang'=>$child->getMerchant->name,
                        'user_id'=>$child->getMerchant->md_user_id
                    ];
                    self::itemWithChildren($child->getChildBranch);
                }
            }
        }

        return $result;
    }

    private  static  function getTopHeadBranch($merchantId) {

        $activeBranch=MerchantBranch::where('branch_merchant_id',$merchantId)
            ->where('is_active',1)
            ->first();

        if($activeBranch)
        {

            if(is_null($activeBranch->getHeadBranchRecursive)){
                return $activeBranch->head_merchant_id;
            }else{
                $head=self::itemGetHead($activeBranch->getHeadBranchRecursive);
                if(!is_null($head)){
                    return  $head;
                }
            }

        }else{

            return $merchantId;

        }


    }

    private static function itemGetHead($item)
    {

        if(!is_null($item)){
            self::itemGetHead($item->getHeadBranchRecursive);
            return $item->getHead->id;
        }else{
            return  null;
        }
    }

    public static function checkUrl($merchantId){
        try {
            $merchant=merchant_detail_multi_branch($merchantId);

            if(is_null($merchant->md_subscription_id)){
                return [
                    'status'=>true,
                    'message'=>'Mohon kamu belum berlangganan silahkan update pembayaran langgananmu untuk menggunakan fitur ini',
                ];
            }else{
                if(date('Y-m-d')<=Carbon::parse($merchant->end_subscription)->toDateString()){
                    if(strpos($merchant->getSubscription->name,'Nakula')!==false)
                    {
                        return [
                            'status'=>true,
                            'message'=>'Fitur ini hanya berlaku pada paket Sadewa atau Antasena',
                        ];
                    }else{
                        if(strpos($merchant->getSubscription->name,'Sadewa')!==false)
                        {
                            return [
                                'status'=>true,
                                'message'=>'Fitur ini hanya berlaku pada paket Antasena',
                            ];

                        }

                        if(strpos($merchant->getSubscription->name,'Antasena')!==false)
                        {
                            return [
                                'status'=>false,
                                'message'=>'',
                            ];
                        }

                    }
                }else{
                    return [
                        'status'=>true,
                        'message'=>'Mohon masa berlanggananmu telah habis silahkan lakukan  perpanjangan langganan untuk menggunakan fitur ini',
                    ];
                }

            }

        }catch (\Exception $e)
        {
            return [
                'status'=>false,
                'message'=>'',
            ];
        }
    }

    public static function getHeadBranch($merchantId)
    {
       return self::getTopHeadBranch($merchantId);
    }

}

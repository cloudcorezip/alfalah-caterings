<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Merchant;


use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductRedeem;
use App\Models\SennaToko\ProductRedeemDetail;
use App\Models\MasterData\Merchant;
use App\Utils\Accounting\CoaProductUtil;
use App\Models\SennaToko\ProductSellingLevel;
use App\Models\SennaToko\MultiUnit;
use App\Models\SennaToko\MultiVarian;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\ProductionOfGoodDetail;
use App\Models\Accounting\CoaCashflowFormatDetail;
use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaMapping;
use App\Models\SennaToko\SaleOrder;
use App\Utils\Accounting\CoaUtil;
use Illuminate\Support\Facades\DB;


class ProductUtil
{

    public static function checkQuantity($productId,$quantity)
    {
        $data=Product::find($productId);
        if($data->stock>=$quantity)
        {
            return true;
        }
        return false;

    }

    public static function reduceQuantity($productId,$quantity)
    {
        try{
            $data=Product::find($productId);
            $data->stock-=$quantity;
            $data->save();

            return true;
        }catch (\Exception $e)
        {
            return false;
        }


    }

    public static function getCount($userId)
    {
        try{
            return Product::where('md_user_id',$userId)->count();
        }catch (\Exception $e)
        {
            return 0;
        }
    }

    public static function duplicateProduct($branch,$product,$request,$multi_unit,$selling_level){
        try{
            $assign_to_product=[];
            $idNewMulti=[];
            $idNewSL=[];

            foreach($branch as $key => $b){

                $merchant = Merchant::find($b);
                $product = Product::find($product->id);
                $checkProduct=Product::whereRaw("lower(code) iLIKE '%".strtolower($product->code)."%' ")
                                    ->where('md_user_id',$merchant->md_user_id)
                                    ->where('is_deleted',0)
                                    ->first();

                if(is_null($checkProduct)){
                    $newProduct = $product->replicate();
                    $newProduct->md_user_id = $merchant->md_user_id;
                    $newProduct->stock = 0;
                    $newProduct->duplicated_by_id = $product->id;
                    $newProduct->assign_to_branch = null;
                    $newProduct->assign_to_product=null;
                    $newProduct->created_by_merchant=null;
                    if($product->md_sc_product_type_id==4 || $product->is_with_stock==0){
                        if(!is_null($product->cost_other) && $product->cost_other!=[]){
                            $newCostOther=[];
                            foreach(json_decode($product->cost_other) as $key => $c){
                                $coa=CoaDetail::find($c->id);
                                $coa_detail=CoaDetail::select(['acc_coa_details.name','acc_coa_details.id'])
                                                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                                                    ->where('acc_coa_details.name',$coa->name)
                                                    ->where('c.md_merchant_id',$b)
                                                    ->first();
                                if(!$coa_detail){
                                    $coa_detail_id=self::createNewCoa($coa,$b);
                                }else{
                                    $coa_detail_id=$coa_detail->id;
                                }

                                $newCostOther[]=[
                                                    "id"=>$coa_detail_id,
                                                    "amount"=>$c->amount,
                                                    "name"=>$c->name
                                                ];
                            }
                            $newProduct->cost_other=json_encode($newCostOther);
                        }
                    }

                    $newProduct->save();

                    $assign_to_product[]=$newProduct->id;

                    if($product->is_with_stock==1){
                        $coaProduct=CoaProductUtil::initialCoaProduct($b,$newProduct,1);
                        if($coaProduct==false)
                        {
                            return false;
                        }

                        $newProduct->inv_id=$coaProduct['inv_id'];
                        $newProduct->hpp_id=$coaProduct['hpp_id'];
                        $newProduct->save();
                    }

                    if(is_null($request->id) && $multi_unit!=[]){
                        foreach($multi_unit as $key =>$m){
                            $mu = MultiUnit::find($m);
                            $newMultiUnit = $mu->replicate();
                            $newMultiUnit->sc_product_id=$newProduct->id;
                            $newMultiUnit->assign_to=null;
                            $newMultiUnit->save();
                            $idNewMulti[]=$newMultiUnit->id;
                        }
                        $mu->assign_to=$idNewMulti;
                        $mu->save();
                    }

                    if(is_null($request->id) && $selling_level!=[]){

                        foreach($selling_level as $key =>$m){
                            $sl = ProductSellingLevel::find($m);
                            $newSellingLevel = $sl->replicate();
                            $newSellingLevel->sc_product_id=$newProduct->id;
                            $newSellingLevel->assign_to=null;
                            $newSellingLevel->save();
                            $idNewSL[]=$newSellingLevel->id;
                        }
                        $sl->assign_to=$idNewSL;
                        $sl->save();
                    }

                    $raw_material=Material::where('parent_sc_product_id',$product->id)->get();

                    if(is_null($request->id) && count($raw_material)>0){

                        foreach($raw_material as $i =>$r){
                            $rm = Material::find($r->id);
                            $child = Product::find($rm->child_sc_product_id);
                            $checkChild = Product::whereRaw("lower(code) iLIKE '%".strtolower($child->code)."%' ")
                                                ->where('md_user_id',$merchant->md_user_id)
                                                ->where('is_deleted',0)
                                                ->first();
                            if(!is_null($checkChild)){
                                $childId=$checkChild->id;
                            }else{
                                $newChild = $child->replicate();
                                $newChild->md_user_id = $merchant->md_user_id;
                                $newChild->stock = 0;
                                $newChild->duplicated_by_id = $child->id;
                                $newChild->assign_to_branch = null;
                                $newChild->assign_to_product= null;
                                $newChild->save();

                                $coaChild=CoaProductUtil::initialCoaProduct($b,$newChild,1);
                                if($coaChild==false)
                                {
                                    return false;
                                }

                                $newChild->inv_id=$coaChild['inv_id'];
                                $newChild->hpp_id=$coaChild['hpp_id'];
                                $newChild->save();
                                $childId=$newChild->id;
                            }
                            $newRawMaterial = $rm->replicate();
                            $newRawMaterial->parent_sc_product_id=$newProduct->id;
                            $newRawMaterial->child_sc_product_id=$childId;
                            $newRawMaterial->assign_to=null;
                            $newRawMaterial->save();
                            if(is_null($rm->assign_to)){
                                $newAssign=[];
                                $newAssign[]=$newRawMaterial->id;
                                $rm->assign_to=$newAssign;
                            }else{
                                $decodeJson=json_decode($rm->assign_to);
                                array_push($decodeJson,$newRawMaterial->id);
                                $rm->assign_to=json_encode($decodeJson);
                            }

                            $rm->save();
                        }

                    }

                    if(!is_null($request->id)){
                        $checkSellingLevel=ProductSellingLevel::where('sc_product_id',$request->id)->where('is_deleted',0)->get();
                        if($checkSellingLevel->count()>0){
                            foreach($checkSellingLevel as $key => $s){
                                $sl=ProductSellingLevel::find($s->id);
                                $newSellingLevel = $sl->replicate();
                                $newSellingLevel->sc_product_id=$newProduct->id;
                                $newSellingLevel->assign_to=null;
                                $newSellingLevel->save();
                                $idNewSL[]=$newSellingLevel->id;
                            }
                            $sl->assign_to=$idNewSL;
                            $sl->save();
                        }
                        $checkMultiUnit=MultiUnit::where('sc_product_id',$request->id)->where('is_deleted',0)->get();
                        if($checkMultiUnit->count()>0){
                            foreach($checkMultiUnit as $key => $s){
                                $mu=MultiUnit::find($s->id);
                                $newMultiUnit = $mu->replicate();
                                $newMultiUnit->sc_product_id=$newProduct->id;
                                $newMultiUnit->assign_to=null;
                                $newMultiUnit->save();
                                $idNewMulti[]=$newMultiUnit->id;
                            }
                            $mu->assign_to=$idNewMulti;
                            $mu->save();
                        }
                        if($product->md_sc_product_type_id==2){
                            $checkRawMaterial=Material::where('parent_sc_product_id',$product->id)->where('is_deleted',0)->get();
                            if($checkRawMaterial->count()>0){
                                foreach($checkRawMaterial as $key => $s){
                                    $rm = Material::find($s->id);
                                    $child = Product::find($rm->child_sc_product_id);
                                    $checkChild = Product::whereRaw("lower(code) iLIKE '%".strtolower($child->code)."%' ")
                                                        ->where('md_user_id',$merchant->md_user_id)
                                                        ->where('is_deleted',0)
                                                        ->first();
                                    if(!is_null($checkChild)){
                                        $childId=$checkChild->id;
                                    }else{
                                        $newChild = $child->replicate();
                                        $newChild->md_user_id = $merchant->md_user_id;
                                        $newChild->stock = 0;
                                        $newChild->duplicated_by_id = $child->id;
                                        $newChild->assign_to_branch = null;
                                        $newChild->assign_to_product= null;
                                        $newChild->save();

                                        $coaChild=CoaProductUtil::initialCoaProduct($b,$newChild,1);
                                        if($coaChild==false)
                                        {
                                            return false;
                                        }

                                        $newChild->inv_id=$coaChild['inv_id'];
                                        $newChild->hpp_id=$coaChild['hpp_id'];

                                        $newChild->save();
                                        $childId=$newChild->id;
                                    }
                                    $newRawMaterial = $rm->replicate();
                                    $newRawMaterial->parent_sc_product_id=$newProduct->id;
                                    $newRawMaterial->child_sc_product_id=$childId;
                                    $newRawMaterial->assign_to=null;
                                    $newRawMaterial->save();
                                    $idNewMaterial[]=$newRawMaterial->id;
                                }
                                $rm->assign_to=$idNewMaterial;
                                $rm->save();
                            }
                        }

                    }


                }

            }
            $product->assign_to_product=$assign_to_product;
            $product->save();
            return true;
        }catch (\Exception $e)
        {

            return false;
        }

    }

    public static function checkDuplicateProduct($product){
        try{
            $oldProduct=json_decode($product->assign_to_product);
            if(
                PurchaseOrderDetail::whereIn('sc_product_id',$oldProduct)
                            ->join('sc_purchase_orders as s','s.id','sc_purchase_order_details.sc_purchase_order_id')
                            ->where('s.is_deleted',0)
                            ->count()>0 &&
                SaleOrderDetail::whereIn('sc_product_id',$oldProduct)
                                ->join('sc_sale_orders as s','s.id','sc_sale_order_details.sc_sale_order_id')
                                ->join('sc_products as p','p.id','sc_sale_order_details.sc_product_id')
                                ->where('p.is_with_stock',1)
                                ->where('s.is_deleted',0)
                                ->count()>0 &&
                StockInventory::whereIn('sc_product_id',$oldProduct)
                                ->where('is_deleted',0)
                                ->where('type','in')
                                ->where('residual_stock','>',0)
                                ->count()>0 &&
                ProductionOfGoodDetail::whereIn('sc_inv_production_of_good_details.sc_product_id',$oldProduct)
                                ->join('sc_inv_production_of_goods as s','s.id','sc_inv_production_of_good_details.sc_inv_production_of_good_id')->where('s.is_deleted',0)
                                ->count()>0
            ){
                return false;
            }else{
                if($oldProduct!=[]){
                    Product::whereIn('id',$oldProduct)->update(['is_deleted'=>1]);
                    $coa=CoaDetail::whereIn('ref_external_id',$oldProduct)->where('ref_external_type',1)->get();
                    foreach ($coa as $key =>$item)
                    {
                        $item->is_deleted=1;
                        $item->save();
                        $coaCfD=CoaCashflowFormatDetail::where('acc_coa_detail_id',$item->id)->first();
                        if(!is_null($coaCfD))
                        {
                            $coaCfD->is_deleted=1;
                            $coaCfD->save();
                        }
                    }
                    Material::whereIn('id',$oldProduct)->update(['is_deleted'=>1]);

                }
            }

            return true;
        }catch (\Exception $e)
        {
            return false;
        }

    }

    public static function updateDuplicateProduct($idProductBranch,$productCentral){
        try{
        foreach(json_decode($idProductBranch) as $key => $i){
            $product = Product::find($i);
            $product->foto=$productCentral->foto;
            $product->product_photos=$productCentral->product_photos;
            $product->is_with_stock=$productCentral->is_with_stock;
            $product->purchase_price=$productCentral->purchase_price;
            $product->selling_price=$productCentral->selling_price;
            $product->weight=$productCentral->weight;
            $product->sc_merk_id=$productCentral->sc_merk_id;
            $product->description=$productCentral->description;
            $product->md_sc_product_type_id=$productCentral->md_sc_product_type_id;
            $product->long=$productCentral->long;
            $product->wide=$productCentral->wide;
            $product->hight=$productCentral->hight;
            $product->package_contents=$productCentral->package_contents;
            $product->sc_product_category_id=$productCentral->sc_product_category_id;
            $product->profit_prosentase=$productCentral->profit_prosentase;
            $product->md_sc_category_id=$productCentral->md_sc_category_id;
            $product->md_unit_id=$productCentral->md_unit_id;
            $product->is_show_senna_app=$productCentral->is_show_senna_app;
            if($product->md_sc_product_type_id==4 || $product->is_with_stock==0){
                $newCostOther=[];
                $merchant = Merchant::where('md_user_id',$product->md_user_id)->first();
                // dd($productCentral->cost_other);
                if(json_decode($productCentral->cost_other)!=[]){
                    foreach(json_decode($productCentral->cost_other) as $key => $c){
                        $coa=CoaDetail::find($c->id);
                        if($coa){
                            $coa_detail=CoaDetail::select(['acc_coa_details.name','acc_coa_details.id'])
                                            ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                                            ->where('acc_coa_details.name',$coa->name)
                                            ->where('c.md_merchant_id',$merchant->id)
                                            ->first();
                            if(!$coa_detail){
                                $coa_detail_id=self::createNewCoa($coa,$merchant->id);
                            }else{
                                $coa_detail_id=$coa_detail->id;
                            }
                            $newCostOther[]=[
                                                "id"=>$coa_detail_id,
                                                "amount"=>$c->amount,
                                                "name"=>$c->name
                                            ];
                        }

                    }
                    $product->cost_other=json_encode($newCostOther);
                }

            }
            $product->save();
        }
        return true;
    }catch (\Exception $e)
    {
        return false;
    }
    }

    public static function duplicateProductMobile($raw_material,$selling_level,$multi_unit,$merchant){
        try{
            if(!is_null($raw_material)){
                    $branchRawMaterial=[];
                    $child = Product::find($raw_material->child_sc_product_id);
                    $parent = Product::find($raw_material->parent_sc_product_id);

                    $checkChild = Product::whereRaw("lower(code) iLIKE '%".strtolower($child->code)."%' ")
                                        ->where('md_user_id',$merchant->md_user_id)
                                        ->where('is_deleted',0)
                                        ->first();
                    $getParent = Product::whereRaw("lower(code) iLIKE '%".strtolower($parent->code)."%' ")
                                        ->where('md_user_id',$merchant->md_user_id)
                                        ->where('is_deleted',0)
                                        ->first();
                    if(!is_null($checkChild)){
                        $childId=$checkChild->id;
                    }else{
                        $newChild = $child->replicate();
                        $newChild->md_user_id = $merchant->md_user_id;
                        $newChild->save();

                        $coaChild=CoaProductUtil::initialCoaProduct($merchant->id,$newChild,1);
                        if($coaChild==false)
                        {
                            return false;
                        }

                        $newChild->inv_id=$coaChild['inv_id'];
                        $newChild->hpp_id=$coaChild['hpp_id'];

                        $newChild->save();
                        $childId=$newChild->id;
                    }
                    $newRawMaterial = $raw_material->replicate();
                    $newRawMaterial->parent_sc_product_id=$getParent->id;
                    $newRawMaterial->child_sc_product_id=$childId;
                    $newRawMaterial->assign_to=null;
                    $newRawMaterial->save();
                    if(is_null($raw_material->assign_to)){
                        $newAssign=[];
                        $newAssign[]=$newRawMaterial->id;
                        $raw_material->assign_to=$newAssign;
                    }else{
                        $decodeJson=json_decode($raw_material->assign_to);
                        array_push($decodeJson,$newRawMaterial->id);
                        $raw_material->assign_to=json_encode($decodeJson);
                    }

                    $raw_material->save();

            }
            if($selling_level!=[]){
                foreach($selling_level as $m){
                    $sl = ProductSellingLevel::find($m);
                    $getProduct = Product::whereRaw("lower(code) iLIKE '%".strtolower($sl->getProduct->code)."%' ")
                                        ->where('md_user_id',$merchant->md_user_id)
                                        ->where('is_deleted',0)
                                        ->first();
                    $newSellingLevel = $sl->replicate();
                    $newSellingLevel->sc_product_id=$getProduct->id;
                    $newSellingLevel->assign_to=null;
                    $newSellingLevel->save();
                    $idNewSL[]=$newSellingLevel->id;
                }
                $sl->assign_to=$idNewSL;
                $sl->save();
            }
            if($multi_unit!=[]){
                foreach($multi_unit as $s){
                    $mu=MultiUnit::find($s);
                    $getProduct = Product::whereRaw("lower(code) iLIKE '%".strtolower($mu->getProduct->code)."%' ")
                                    ->where('md_user_id',$merchant->md_user_id)
                                    ->where('is_deleted',0)
                                    ->first();
                    $newMultiUnit = $mu->replicate();
                    $newMultiUnit->sc_product_id=$getProduct->id;
                    $newMultiUnit->assign_to=null;
                    $newMultiUnit->save();
                    $idNewMulti[]=$newMultiUnit->id;
                }
                $mu->assign_to=$idNewMulti;
                $mu->save();
            }
            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }

    public static function createNewCoa($coaCentral,$merchantId){
        try{
            $data=new CoaDetail();
            $getCategory=CoaCategory::where('name','Beban Operasional & Usaha')->where('md_merchant_id',$merchantId)->first();
            $data->code=CoaUtil::generateCoaDetail($getCategory->id);
            $data->acc_coa_category_id=$getCategory->id;
            $data->name=$coaCentral->name;
            $data->md_sc_currency_id=1;
            $data->is_deleted=0;
            $data->type_coa="Debit";
            $data->duplicated_by_id=$coaCentral->id;
            $data->save();

            $coa_map = new CoaMapping();
            $coa_map->name=$coaCentral->name;
            $coa_map->is_type=2;
            $coa_map->acc_coa_detail_id=$data->id;
            $coa_map->md_merchant_id=$merchantId;
            $coa_map->save();

            $checkCashflowFormatDetails=CoaCashflowFormatDetail::where([
                'acc_coa_detail_id'=>$data->id,
                'is_deleted'=>0
            ])->first();

            $getCashflowFormatId=CoaCashflowFormat::where('name','Beban Operasional & Usaha')->where('md_merchant_id',$merchantId)->first();
            if(!is_null($checkCashflowFormatDetails))
            {
                $checkCashflowFormatDetails->acc_coa_detail_id=$data->id;
                $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$getCashflowFormatId->id;
                $checkCashflowFormatDetails->save();
            }else{
                $checkCashflowFormatDetails=new CoaCashflowFormatDetail();
                $checkCashflowFormatDetails->acc_coa_detail_id=$data->id;
                $checkCashflowFormatDetails->acc_coa_cashflow_format_id=$getCashflowFormatId->id;
                $checkCashflowFormatDetails->save();
            }
            return $data->id;
        }catch(\Exception $e){
            return false;
        }
    }

    public static function calculatePackageSubs($productId,$quantity,$price,$saleId){
        try{
            $product=Product::find($productId);
            $product->limit_selling-=$quantity;
            $product->save();

            $jsonChild=[];
            foreach($product->getPackageItem as $item){
                $jsonChild[]=[
                    "id"=>$item->child_sc_product_id,
                    "name"=>$item->getChild->name,
                    "code"=>$item->getChild->code,
                    "quantity"=>$item->quantity
                ];
            }

            // dd($jsonChild);

            for ($x = 1; $x <= $quantity; $x++) {
                $redeem=new ProductRedeem();
                $redeem->sc_sale_order_id=$saleId;
                $redeem->sc_product_id=$product->id;
                $redeem->price=$price;
                $redeem->json_product_child=json_encode($jsonChild);
                $redeem->expired_date=$product->expired_date;
                $redeem->amount_redeem=$product->limit_redeem;
                $redeem->limit_redeem=$product->limit_redeem;
                $redeem->save();
            }
            
              return true;
        }catch(\Exception $e){
            // dd($e);
            return false;
        }
    }

    public static function adjustStockPackageSubs($productTemporary,$saleId,$invMethodId,$warehouseId){
        try{

            $stockOut=[];
            $stockInvId=[];
            $saleMapping=[];
            $productCalculate=[];
            $updateStockProduct="";
            $updateStockInv="";
            $key1 = 0;
            $timestamp= date('Y-m-d H:i:s');
            $sale=SaleOrder::find($saleId);
        
                foreach (collect($productTemporary)->groupBy('id') as $item) {
                    $quantity = collect($item)->reduce(function ($i, $k) {
                        return $i + $k['quantity'];
                    }, 0);
    
                    array_push($productCalculate, [
                        'id' => $item->first()['id'],
                        'amount' => $quantity,
                    ]);
    
                    $stock = $item->first()['stock'];
    
                    $stockP = $stock - $quantity;
                    $updateStockProduct .= ($key1 == 0) ? "(" . $item->first()['id'] . "," . $stockP . ")" : ",(" . $item->first()['id'] . "," . $stockP . ")";
    
                    $key1++;
    
                    $stockOut[] = [
                        'sync_id' => $item->first()['id'] . Uuid::uuid4()->toString(),
                        'sc_product_id' => $item->first()['id'],
                        'total' => $quantity,
                        'inv_warehouse_id' => $warehouseId,
                        'record_stock' => $stockP,
                        'created_by' => $sale->created_by,
                        'selling_price' => $item->first()['selling_price'],
                        'purchase_price' => $item->first()['purchase_price'],
                        'residual_stock' => $quantity,
                        'type' => StockInventory::OUT,
                        'transaction_action' => 'Pengurangan Stok ' . $item->first()['name'] . ' Dari Penjualan Dengan Code ' . $sale->id,
                        'stockable_type' => 'App\Models\SennaToko\SaleOrder',
                        'stockable_id' => $sale->id,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ];
                }
            
            // dd($stockOut);
            if (!empty($productCalculate)) {
                $calculateInventory = InventoryUtilV2::inventoryCodev2($invMethodId, $warehouseId, $productCalculate, 'minus');
                if ($calculateInventory == false) {
                    return false;
                }
                foreach ($calculateInventory as $key => $n) {
                    $updateStockInv .= ($key == 0) ? "(" . $n['id'] . "," . $n['residual_amount'] . ")" : ",(" . $n['id'] . "," . $n['residual_amount'] . ")";
                    $stockInvId[] = $n['id'];
                    $saleMapping[] = [
                        'sc_sale_order_id' => $sale->id,
                        'sc_product_id' => $n['sc_product_id'],
                        'sc_stock_inventory_id' => $n['id'],
                        'amount' => $n['amount'],
                        'purchase_price' => $n['purchase'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
    
            if($updateStockInv!=""){
                DB::statement("
                update sc_stock_inventories as t set
                        residual_stock = c.column_a
                    from (values
                        $updateStockInv
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
            }
            if($updateStockProduct!=""){
                DB::statement("
                update sc_products as t set
                        stock = c.column_a
                    from (values
                        $updateStockProduct
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
            }
    
        DB::table('sc_stock_sale_mappings')->insert($saleMapping);
        DB::table('sc_stock_inventories')->insert($stockOut);
        DB::table('sc_inv_production_of_goods')->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);

            return true;
        }catch(\Exception $e){
            // dd($e);
            return false;
        }

    } 
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Merchant;


use App\Models\SennaToko\Product;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\StockInventory;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class RawMaterialUtil
{

    public static function increaseByProduct($productId,$quantity,$userId,$message){
        try{
            $rawMaterial= Material::where('parent_sc_product_id', '=', $productId)
                ->where('is_deleted',0)
                ->get();
            foreach ($rawMaterial as $key => $d) {
                $cStock=$d->quantity*$quantity;
                $product= Product::where('id', '=', $d->child_sc_product_id)->first();
                $product->stock+=$cStock;
                $product->save();
                $product->stock()->create([
                    'sync_id'=>$product->id.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$product->id,
                    'total'=>$cStock,
                    'selling_price'=>$product->selling_price,
                    'purchase_price'=>$product->purchase_price,
                    'residual_stock'=>$cStock,
                    'created_by'=>$userId,
                    'record_stock'=>$product->stock,
                    'type'=>StockInventory::IN,
                    'transaction_action'=>$message,
                ]);
            }
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

    public static function decreaseByProduct($productId,$quantity,$userId,$message){
        try{
            $rawMaterial= Material::where('parent_sc_product_id', '=', $productId)
                ->where('is_deleted',0)
                ->get();
            foreach ($rawMaterial as $key => $d) {
                $cStock=$d->quantity*$quantity;
                $product= Product::where('id', '=', $d->child_sc_product_id)->first();
                $product->stock-=$cStock;
                $product->save();
                $product->stock()->create([
                    'sync_id'=>$product->id.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$product->id,
                    'total'=>$cStock,
                    'selling_price'=>$product->selling_price,
                    'purchase_price'=>$product->purchase_price,
                    'residual_stock'=>$cStock,
                    'created_by'=>$userId,
                    'record_stock'=>$product->stock,
                    'type'=>StockInventory::OUT,
                    'transaction_action'=>$message,
                ]);
            }
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

}

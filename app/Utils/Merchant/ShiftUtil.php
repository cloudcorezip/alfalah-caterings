<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils\Merchant;


use App\Models\MasterData\MerchantShiftCashflow;
use App\Models\MasterData\MerchantShift;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\PurchaseOrder;

class ShiftUtil
{


    public static function saleCashflow($saleId,$code,$amount)
    {
        try{
            $shift=MerchantShift::where('is_active',1)->first();

            if(is_null($shift)){
                return true;
            }

            $data= MerchantShiftCashflow::where('ref_transaction_id',$saleId)
                ->where('md_sc_cash_type_id',2)
                ->first();
            if(!is_null($data)){
                $data->ref_transaction_id=$saleId;
                $data->amount=$amount;
                $data->md_merchant_shift_id=$shift->id;
                $data->type=1;
                $data->note="Transaksi Penjualan dari ".$code;
                $data->md_sc_cash_type_id=1;
                $data->save();
            }else{
                $data=new MerchantShiftCashflow();
                $data->ref_transaction_id=$saleId;
                $data->amount=$amount;
                $data->md_merchant_shift_id=$shift->id;
                $data->type=1;
                $data->note="Transaksi Penjualan dari ".$code;
                $data->md_sc_cash_type_id=1;
                $data->save();
            }
            return true;
        }catch (\Exception $e)
        {
            return false;
        }


    }

    public static function purchaseCashflow($purchaseId,$code,$amount)
    {
        try{
            $shift=MerchantShift::where('is_active',1)->first();

            if(is_null($shift)){
                return true;
            }
            $data= MerchantShiftCashflow::where('ref_transaction_id',$purchaseId)
            ->where('md_sc_cash_type_id',2)
                ->first();
            if(!is_null($data))
            {
                $data->ref_transaction_id=$purchaseId;
                $data->amount=$amount;
                $data->md_merchant_shift_id=$shift->id;
                $data->type=2;
                $data->note="Transaksi Pembelian dari ".$code;
                $data->md_sc_cash_type_id=2;
                $data->save();
            }else{
                $data=new MerchantShiftCashflow();
                $data->ref_transaction_id=$purchaseId;
                $data->amount=$amount;
                $data->md_merchant_shift_id=$shift->id;
                $data->type=2;
                $data->note="Transaksi Pembelian dari ".$code;
                $data->md_sc_cash_type_id=2;
                $data->save();
            }

            return true;
        }catch (\Exception $e)
        {
            return false;
        }


    }

}

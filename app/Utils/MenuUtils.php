<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Utils;


use App\Classes\Singleton\DataTable;
use App\Models\MasterData\MerchantMenu;
use Illuminate\Support\Facades\DB;

class MenuUtils
{

    private static $instance=null;

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new MenuUtils();
        }

        return self::$instance;
    }


    public  function menu($staffId,$key)
    {
        $data=DB::table('md_merchant_staff_menu_actions')
            ->join('md_merchant_staff_menus as m','m.id','md_merchant_staff_menu_actions.md_merchant_staff_menu_id')
            ->join('md_merchant_menus as mm','mm.id','m.md_merchant_menu_id')
            ->where('md_merchant_staff_menu_actions.md_merchant_staff_id',$staffId)
            ->where('mm.unique_code',$key)
            ->get();

        $permission=[];
        foreach ($data as $key =>$item)
        {
            $permission[]= [
                'action'=>$item->action,
                'is_show'=>true
            ];
        }
        return self::actionDefaultPermission($permission);


    }

    private static function actionDefaultPermission($permission)
    {
        $data=[];
        foreach (MerchantMenu::actionList() as $key =>$item)
        {
            $data[]=[
                'action'=>$item,
                'is_show'=>false
            ];

        }

        $data=array_merge($permission,$data);
        $result = [];
        $newArray=[];
        foreach ( $data as $key => $line ) {
            if ( !in_array($line['action'], $result) ) {
                $result[] = $line['action'];
                $newArray[$key] = $line;
            }
        }

        return  array_values($newArray);
    }

}

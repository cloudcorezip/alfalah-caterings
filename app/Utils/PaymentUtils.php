<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Utils;


use App\Models\SennaToko\PaymentMethod;
use App\Models\Accounting\CoaDetail;
use Illuminate\Support\Facades\DB;

class PaymentUtils
{
    public static function getPayment($merchantId,$type=1)
    {
        $checkCash=PaymentMethod::where('transaction_type_id',2)->where('md_merchant_id',$merchantId)->first();
        if(is_null($checkCash)){
            $data=new PaymentMethod();
            $coaTunai=CoaDetail::
            select([
                'acc_coa_details.id',
                'acc_coa_details.code'
            ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)
                ->where('acc_coa_details.code','1.1.01.01')
                ->first();

            $data->acc_coa_id=$coaTunai->id;
            $data->acc_coa_code=$coaTunai->code;
            $data->md_va_bank_id=2;
            $data->md_merchant_id=$merchantId;
            $data->transaction_type_id=2;
            $data->status=1;
            $data->save();
        }

        $digitalPaymentData=PaymentMethod::
        select([
            'md_merchant_payment_methods.acc_coa_id',
            'md_merchant_payment_methods.id as payment_id',
            't.id as transaction_id',
            't.code',
            't.name',
            't.icon',
            'b.code',
            'md_merchant_payment_methods.payment_name',
            'bb.icon as icon_bank'
        ])
            ->where('md_merchant_payment_methods.status',1)
            ->where('md_merchant_payment_methods.md_merchant_id',$merchantId)
            ->leftJoin('md_transaction_types as t','t.id','md_merchant_payment_methods.transaction_type_id')
            ->leftJoin('md_va_banks as b','b.id','md_merchant_payment_methods.md_va_bank_id')
            ->leftJoin('md_banks as bb', 'bb.id', 'b.md_bank_id')
            ->get();

        $qris=[];
        $wallet=[];
        $VA=[];
        $transfer=[];
        $cash=[];
        $other=DB::select("select
              d.id as acc_coa_id,
              t.id as trans_id,
              t.name,
              t.icon
            from
              acc_coa_details d
              join md_transaction_types t on lower(d.name)= lower(t.name)
              join acc_coa_categories c on c.id = d.acc_coa_category_id
              and c.md_merchant_id = $merchantId
              and d.is_deleted in(0, 2)
         ");

        foreach($digitalPaymentData as $key =>$item){
            if($item->transaction_id==4){
                $qris[]=[
                    'acc_coa_id'=>$item->acc_coa_id,
                    'trans_id'=>$item->transaction_id,
                    'name'=>$item->name,
                    'icon'=>$item->icon,
                    'alias'=>'QRIS',
                    'id'=>$item->payment_id
                ];

            }elseif ($item->transaction_id==5){
                $VA[]=[
                    'acc_coa_id'=>$item->acc_coa_id,
                    'trans_id'=>$item->transaction_id,
                    'name'=>'Virtual Account - '.$item->code,
                    'icon'=>$item->icon_bank,
                    'alias'=>$item->code,
                    'id'=>$item->payment_id


                ];
            }elseif ($item->transaction_id==2) {
                $cash[]=(object)[
                    'acc_coa_id'=>$item->acc_coa_id,
                    'trans_id'=>$item->transaction_id,
                    'name'=>$item->name,
                    'icon'=>$item->icon,
                    'alias'=>'-',
                    'id'=>$item->payment_id

                ];
            }elseif ($item->transaction_id==13) {
                $transfer[]=(object)[
                    'acc_coa_id'=>$item->acc_coa_id,
                    'trans_id'=>$item->transaction_id,
                    'name'=>$item->payment_name,
                    'icon'=>$item->icon,
                    'alias'=>'-',
                    'id'=>$item->payment_id


                ];
            } else{
                $wallet[]=[
                    'acc_coa_id'=>$item->acc_coa_id,
                    'trans_id'=>15,
                    'name'=>'E-Wallet - '.$item->name,
                    'icon'=>$item->icon,
                    'alias'=>'ID_'.strtoupper($item->name),
                    'id'=>$item->payment_id

                ];
            }
        }

        if($type==1){
            $payment=[
                [
                    'name'=>'Tunai',
                    'data'=>collect($cash)
                ],
                [
                    'name'=>'Bank Transfer',
                    'data'=>collect($transfer)
                ],
                [
                    'name'=>'Pembayaran Digital',
                    'data'=>[
                        [
                            'name'=>'E-Wallet',
                            'data'=>collect($wallet)
                        ],
                        [
                            'name'=>'QRIS',
                            'data'=>collect($qris)
                        ],
                        [
                            'name'=>'Virtual Account',
                            'data'=>collect($VA)
                        ],
                    ],
                ],
                [
                    'name'=>'Label Pembayaran Lainnya',
                    'data'=>collect($other)
                ],
            ];

        }else{
            $payment=[
                [
                    'name'=>'Tunai',
                    'data'=>collect($cash)
                ],
                [
                    'name'=>'Bank Transfer',
                    'data'=>collect($transfer)
                ],
                [
                    'name'=>'Label Pembayaran Lainnya',
                    'data'=>collect($other)
                ],
            ];
        }

        return $payment;
    }
}

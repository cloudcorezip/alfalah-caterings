<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Mails\Account;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailActivation extends Mailable
{
    use Queueable, SerializesModels;
    public $fullname;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fullname,$link)
    {
        $this->fullname=$fullname;
        $this->link=$link;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'link'=>$this->link,
            'fullname'=>$this->fullname
        ];

        return $this->subject('Senna Aktivasi Akun')->view('emails.account.email-verification', $params);
    }

}

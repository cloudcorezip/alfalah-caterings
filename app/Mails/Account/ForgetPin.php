<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Mails\Account;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgetPin extends Mailable
{
    use Queueable, SerializesModels;
    public $fullname;
    public $pin;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fullname,$pin)
    {
        $this->fullname=$fullname;
        $this->pin=$pin;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'pin'=>$this->pin,
            'fullname'=>$this->fullname
        ];

        return $this->subject('Lupa Pin')->view('emails.account.forgot-pin', $params);
    }


}

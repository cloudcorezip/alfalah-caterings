<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Mails\Account;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgetEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $fullname;
    public $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fullname,$password)
    {
        $this->fullname=$fullname;
        $this->password=$password;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'password'=>$this->password,
            'fullname'=>$this->fullname
        ];

        return $this->subject('Lupa Password')->view('emails.account.forgot-password', $params);
    }

}

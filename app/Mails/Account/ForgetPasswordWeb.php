<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Mails\Account;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgetPasswordWeb extends Mailable
{
    use Queueable, SerializesModels;
    public $fullname;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fullname,$link)
    {
        $this->fullname=$fullname;
        $this->link=$link;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'fullname'=>$this->fullname,
            'link'=>$this->link,
        ];

        return $this->subject('Lupa Password')->view('emails.account.forgot-password-web', $params);
    }

}

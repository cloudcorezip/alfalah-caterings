<?php

namespace App\Console\Commands;

use App\Models\Accounting\MerchantAp;
use App\Utils\Accounting\CoaPurchaseUtil;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\SennaToko\StockInventory;
use Ramsey\Uuid\Uuid;

class CreatePurchaseDelivery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purchase:delivery {--id|purchase_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Purchase Delivery';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $optionPurchaseId=$this->option('purchase_id');
            if(!is_null($optionPurchaseId))
            {
                $data=Session::get('purchase_delivery_'.$optionPurchaseId);
                if(!is_null($data))
                {
                    if($data['updateStockProduct']!=""){
                        DB::statement("
                        update sc_products as t set
                            stock = c.column_a,
                            purchase_price = c.column_c
                        from (values
                            ".$data['updateStockProduct']."
                        ) as c(column_b, column_a, column_c)
                        where c.column_b = t.id;
                            ");
                    }
        
                    StockInventory::insert($data['stock_in']);
                }

                DB::commit();
                Session::forget('purchase_delivery_'.$optionPurchaseId);
 
                return true;

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }
}

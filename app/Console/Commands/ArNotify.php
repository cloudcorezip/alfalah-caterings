<?php

namespace App\Console\Commands;

use App\Classes\Singleton\Firebase;
use App\Utils\Order\SaleOrderUtil;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class ArNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ar:duedate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Untuk memberikan informasi terkait kasbon jatuh tempo pelanggan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data=DB::select("
            select
                  ama.due_date - now() :: date as over_due,
                  mu.fcm_key,
                  mu.id,
                  mu.fullname,
                  sso.code,
                  ama.residual_amount,
                  m.name as merchant_name,
                  ama.due_date
                from
                  acc_merchant_ar ama
                  join sc_sale_orders sso on ama.arable_id = sso.id
                  join sc_customers sc on sc.id = sso.sc_customer_id
                  join sc_user_customers suc on suc.sc_customer_id = sc.id
                  join md_users mu on mu.id = suc.from_user_id
                  join md_merchants m on m.id=sso.md_merchant_id
                where
                  mu.fcm_key notnull
                  and ama.is_paid_off = 0
                  and (ama.due_date - now() :: date) between -100
                  and 4"
        );
        foreach (array_chunk($data,150) as $key =>$j)
        {
            foreach ($j as $num =>$l)
            {
                $item=(object)$l;
                Firebase::sendWithoutCollection(
                    'Kasbon Jatuh Tempo',
                    trans('firebase.ar_due_date',[
                        'code'=>$item->code,
                        'residual_amount'=>$item->residual_amount,
                        'merchant_name'=>$item->merchant_name,
                        'due_date'=>$item->due_date
                    ]),
                    '',
                    $item,
                    self::class,'-',
                    $item,
                    1);
            }

        }
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Accounting\MerchantAp;
use App\Utils\Accounting\CoaPurchaseUtil;
use App\Models\Accounting\CoaDetail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\ReturPurchaseOrderDetail;
use App\Models\SennaToko\TempPurchaseOrderDetail;

use Ramsey\Uuid\Uuid;

class CreatePurchaseReturDeliv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purchase:retur_deliv {--id|purchase_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Purchase Retur Delivery';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $optionPurchaseId=$this->option('purchase_id');
            if(!is_null($optionPurchaseId))
            {
                $data=Session::get('purchase_retur_deliv_'.$optionPurchaseId);

                $updateStockProduct="";
                $updateStockInv="";
                $stockInOut=[];
                $temp=[];

                foreach ($data['arrayOfProduct'] as $key =>$item)
                {
                    $saleOrderLine=$data['purchaseDetail']->firstWhere('id',$data['arrayOfPurchaseDetail'][$key]);
                    $stockAble=$data['stockAbles']->firstWhere('sc_product_id',$data['arrayOfProduct'][$key]);

                    $temp[]=[
                        'sync_id'=>$stockAble->id.Uuid::uuid4()->toString(),
                        'sc_product_id'=>$saleOrderLine->sc_product_id,
                        'quantity'=>$saleOrderLine->quantity,
                        'sub_total'=>$saleOrderLine->sub_total,
                        'price'=>$saleOrderLine->price,
                        'sc_purchase_order_id'=>$data['purchase_id'],
                        'is_current'=>1,
                        'created_at'=>$saleOrderLine->created_at,
                        'updated_at'=>$saleOrderLine->updated_at
                    ];
                    if($data['retur_reason_id']!=1)
                    {
                        if($data['isMultiUnit'][$key]==0){

                            $inv=$stockAble->residual_stock-$data['arrayOfQuantity'][$key];

                        }else{
                            $inv=$stockAble->residual_stock-($data['arrayOfQuantity'][$key]*$data['valueConversation'][$key]);

                        }

                        $updateStockInv.=($key==0)?"(".$stockAble->inv_id.",".$inv.")":",(".$stockAble->inv_id.",".$inv.")";

                        if($data['warehouse_is_default']==1)
                        {
                            if($data['isMultiUnit'][$key]==0){
                                $stockP=$stockAble->stock-$data['arrayOfQuantity'][$key];

                            }else{
                                $stockP=$stockAble->stock-($data['arrayOfQuantity'][$key]*$data['valueConversation'][$key]);
                            }

                            $updateStockProduct.=($key==0)?"(".$stockAble->sc_product_id.",".$stockP.")":",(".$stockAble->sc_product_id.",".$stockP.")";

                        }else{
                            $stockP=$stockAble->stock;
                        }

                    }else{
                        $stockP=$stockAble->stock;
                    }

                    if($data['isMultiUnit'][$key]==0)
                    {
                        if($data['arrayOfQuantity'][$key]!=0)
                        {
                            $stockInOut[]=[
                                'sync_id' => $stockAble->sc_product_id . Uuid::uuid4()->toString(),
                                'sc_product_id' => $stockAble->sc_product_id,
                                'total' => $data['arrayOfQuantity'][$key],
                                'inv_warehouse_id' => $data['warehouse_id'],
                                'record_stock' => $stockP,
                                'created_by' => $data['user_id'],
                                'selling_price' => $stockAble->selling_price,
                                'purchase_price' => $stockAble->purchase_price,
                                'type' => StockInventory::OUT,
                                'transaction_action' => 'Pengurangan Stok ' . $stockAble->product_name_2 . ' Dari  Retur Transaksi ' . $data['retur_code'],
                                'stockable_type'=>'App\Models\SennaToko\ReturPurchaseOrder',
                                'stockable_id'=>$data['retur_id'],
                                'created_at'=>$data['timestamp'],
                                'updated_at'=>$data['timestamp'],
                                'product_name'=>$stockAble->product_name_2.' '.$stockAble->product_code_2,
                                'timezone'=>$data['retur']->timezone,
                                'unit_name'=>$stockAble->unit_name_2

                            ];
                        }
                    }else{
                        if(($data['arrayOfQuantity'][$key]*$data['valueConversation'][$key])!=0)
                        {
                            $stockInOut[]=[
                                'sync_id' => $stockAble->sc_product_id . Uuid::uuid4()->toString(),
                                'sc_product_id' => $stockAble->sc_product_id,
                                'total' => ($data['arrayOfQuantity'][$key]*$data['valueConversation'][$key]),
                                'inv_warehouse_id' => $data['warehouse_id'],
                                'record_stock' => $stockP,
                                'created_by' => $data['user_id'],
                                'selling_price' => $stockAble->selling_price,
                                'purchase_price' => $stockAble->purchase_price,
                                'type' => StockInventory::OUT,
                                'transaction_action' => 'Pengurangan Stok ' . $stockAble->product_name_2 . ' Dari  Retur Transaksi Penerimaan ' . $data['retur_code'],
                                'stockable_type'=>'App\Models\SennaToko\ReturPurchaseOrder',
                                'stockable_id'=>$data['retur_id'],
                                'created_at'=>$data['timestamp'],
                                'updated_at'=>$data['timestamp'],
                                'product_name'=>$stockAble->product_name_2.' '.$stockAble->product_code_2,
                                'timezone'=>$data['retur']->timezone,
                                'unit_name'=>$stockAble->unit_name_2
                            ];
                        }
                    }



                }


                if($updateStockInv!=""){
                    DB::statement("
                        update sc_stock_inventories as t set
                                residual_stock = c.column_a
                            from (values
                                $updateStockInv
                            ) as c(column_b, column_a)
                            where c.column_b = t.id;
                        ");
                }
                if($updateStockProduct!="") {
                    DB::statement("
                        update sc_products as t set
                                stock = c.column_a
                            from (values
                                $updateStockProduct
                            ) as c(column_b, column_a)
                            where c.column_b = t.id;
                        ");
                }

                TempPurchaseOrderDetail::insert($temp);
                StockInventory::insert($stockInOut);

                DB::commit();

                Session::forget('purchase_retur_deliv_'.$optionPurchaseId);

                return true;

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\MasterData\Merchant;
use App\Models\Accounting\MerchantArDetail;
use App\Models\SennaToko\StockInventory;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Inventory\InventoryUtilV2;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Ramsey\Uuid\Uuid;

class SaleOrderMobile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sale:mobile {--id|sale_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $optionSaleId=$this->option('sale_id');
            if(!is_null($optionSaleId))
            {
                $data=Session::get('sale_mobile_'.$optionSaleId);
                $merchant=Merchant::find($data['merchantId']);

                $stockOut=[];
                $stockInvId=[];
                $saleMapping=[];
                $productCalculate=[];
                $updateStockProduct="";
                $updateStockInv="";
                if($data['isKeep']==0)
                {
                    if (!empty($data['productTemporary'])) {
                        $key1 = 0;
                        foreach (collect($data['productTemporary'])->groupBy('id') as $item) {
                            $quantity = collect($item)->reduce(function ($i, $k) {
                                return $i + $k['quantity'];
                            }, 0);

                            array_push($productCalculate, [
                                'id' => $item->first()['id'],
                                'amount' => $quantity,
                            ]);

                            $stock = $item->first()['stock'];

                            $stockP = $stock - $quantity;
                            $updateStockProduct .= ($key1 == 0) ? "(" . $item->first()['id'] . "," . $stockP . ")" : ",(" . $item->first()['id'] . "," . $stockP . ")";

                            $key1++;

                            $stockOut[] = [
                                'sync_id' => $item->first()['id'] . Uuid::uuid4()->toString(),
                                'sc_product_id' => $item->first()['id'],
                                'total' => $quantity,
                                'inv_warehouse_id' => $data['warehouseId'],
                                'record_stock' => $stockP,
                                'created_by' => $data['createdBy'],
                                'selling_price' => $item->first()['selling_price'],
                                'purchase_price' => $item->first()['purchase_price'],
                                'residual_stock' => $quantity,
                                'type' => StockInventory::OUT,
                                'transaction_action' => 'Pengurangan Stok ' . $item->first()['name'] . ' Dari Penjualan Dengan Code ' . $data['code'],
                                'stockable_type' => 'App\Models\SennaToko\SaleOrder',
                                'stockable_id' => $data['id'],
                                'created_at' => $data['timestamp'],
                                'updated_at' => $data['timestamp']
                            ];
                        }
                    }
                    // dd($stockOut);
                    if (!empty($productCalculate)) {
                        $calculateInventory = InventoryUtilV2::inventoryCodev2($data['invMethodId'], $data['warehouseId'], $productCalculate, 'minus');
                        if ($calculateInventory == false) {
                            return false;
                        }
                        foreach ($calculateInventory as $key => $n) {
                            $updateStockInv .= ($key == 0) ? "(" . $n['id'] . "," . $n['residual_amount'] . ")" : ",(" . $n['id'] . "," . $n['residual_amount'] . ")";
                            $stockInvId[] = $n['id'];
                            $saleMapping[] = [
                                'sc_sale_order_id' => $data['id'],
                                'sc_product_id' => $n['sc_product_id'],
                                'sc_stock_inventory_id' => $n['id'],
                                'amount' => $n['amount'],
                                'purchase_price' => $n['purchase'],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ];
                        }
                    }

                    if($updateStockInv!=""){
                        DB::statement("
                        update sc_stock_inventories as t set
                                residual_stock = c.column_a
                            from (values
                                $updateStockInv
                            ) as c(column_b, column_a)
                            where c.column_b = t.id;
                        ");
                    }
                    if($updateStockProduct!=""){
                        DB::statement("
                        update sc_products as t set
                                stock = c.column_a
                            from (values
                                $updateStockProduct
                            ) as c(column_b, column_a)
                            where c.column_b = t.id;
                        ");
                    }

                }

                DB::table('sc_stock_sale_mappings')->insert($saleMapping);
                DB::table('sc_stock_inventories')->insert($stockOut);
                DB::table('sc_inv_production_of_goods')->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);

                if($data['isKeep']==0)
                {
                    if (CoaSaleUtil::coaStockAdjustmentFromSaleV2Mobile($data['userId'],$data['merchantId'],collect($data['coaSale']),$data['warehouseId'],$data['invMethodId'],$data['id']) == false) {
                        return false;
                    }
                }

                if($data['isDebet']==1)
                {
                    $idAr=MerchantAr::insertGetId([
                        'sync_id'=>$data['merchantId'].Uuid::uuid4()->toString(),
                        'md_merchant_id'=>$data['merchantId'],
                        'residual_amount'=>$data['total']-$data['paidNominal'],
                        'paid_nominal'=>$data['paidNominal'],
                        'is_paid_off'=>0,
                        'arable_id'=>$data['id'],
                        'arable_type'=>'App\Models\SennaToko\SaleOrder',
                        'due_date'=>$data['dueDate'],
                        'timezone'=>$data['timezone'],
                        'created_at'=>$data['timestamp'],
                        'updated_at'=>$data['timestamp']
                    ]);
                }
                if($data['isDebet']==1 && $data['paidNominal']>0)
                {
                    $codeJur=Jurnal::where('md_merchant_id',$data['merchantId'])
                            ->count()+1;

                    $data1=new MerchantArDetail();
                    $data1->ar_detail_code='PTR'.sprintf("%07s", $codeJur);
                    $data1->paid_nominal=$data['paidNominal'];
                    $data1->paid_date=$data['timestamp'];
                    $data1->note='Uang Muka Penjualan';
                    $data1->acc_merchant_ar_id=$idAr;
                    $data1->created_by=$data['createdBy'];
                    $data1->timezone=$data['timezone'];
                    $data1->trans_coa_detail_id=$data['trans1'];
                    $data1->save();

                    $jurnal=new Jurnal();
                    $jurnal->ref_code=$data1->ar_detail_code;
                    $jurnal->trans_code='JRN'.sprintf("%07s", $codeJur);;
                    $jurnal->trans_name='Uang Muka  Penjualan '.$data['code'].'-'.$data1->ar_detail_code;
                    $jurnal->trans_time=$data1->paid_date;
                    $jurnal->trans_note='Uang Muka Penjualan '.$data['code'].'-'.$data1->ar_detail_code;
                    $jurnal->trans_purpose='Uang Muka  Penjualan '.$data['code'].'-'.$data1->ar_detail_code;
                    $jurnal->trans_amount=$data1->paid_nominal;
                    $jurnal->md_merchant_id=$data['merchantId'];
                    $jurnal->md_user_id_created=$data['createdBy'];
                    $jurnal->external_ref_id=$data1->id;
                    $jurnal->flag_name='Uang Muka Penjualan '.$data['code'];
                    $jurnal->save();

                    $fromCoa=$merchant->coa_ar_sale_id;
                    $toCoa=$data['trans1'];
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$fromCoa,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data1->paid_nominal,
                            'created_at'=>$data1->paid_date,
                            'updated_at'=>$data1->paid_date,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$toCoa,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data1->paid_nominal,
                            'created_at'=>$data1->paid_date,
                            'updated_at'=>$data1->paid_date,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                    ];
                    JurnalDetail::insert($insert);
                }
            }
            Session::forget('sale_mobile_'.$optionSaleId);
            DB::commit();

            return true;

        }catch (\Exception $e)
        {
            
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return false;
        }
    }
}

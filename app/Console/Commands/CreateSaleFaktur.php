<?php

namespace App\Console\Commands;

use App\Models\Accounting\MerchantAr;
use App\Models\SennaToko\SaleOrder;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Inventory\InventoryUtilV2;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;

class CreateSaleFaktur extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sale:faktur {--id|sale_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Sale Faktur';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $optionSaleId=$this->option('sale_id');
            if(!is_null($optionSaleId))
            {
                $data=Session::get('sale_faktur_'.$optionSaleId);
                if(!empty($data['productCalculate']))
                {
                    $calculateInventory=InventoryUtilV2::inventoryCodev2($data['inv_id'],$data['warehouse_id'],$data['productCalculate'],'minus');
                    if($calculateInventory==false)
                    {
                        return  false;
                    }
                    $updateStockInv="";
                    $stockInvId=[];
                    $saleMapping=[];

                    foreach ($calculateInventory as $key => $n)
                    {
                        $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                        $stockInvId[]=$n['id'];
                        $saleMapping[]=[
                            'sc_sale_order_id'=>$optionSaleId,
                            'sc_product_id'=>$n['sc_product_id'],
                            'sc_stock_inventory_id'=>$n['id'],
                            'amount'=>$n['amount'],
                            'purchase_price'=>$n['purchase'],
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                    if($updateStockInv!=""){
                        DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                    }
                    if($data['updateStockProduct']!=""){
                        DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            ".$data['updateStockProduct']."
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                    }
                    DB::table('sc_stock_sale_mappings')->insert($saleMapping);
                    DB::table('sc_stock_inventories')->insert($data['stock_in_out']);
                    DB::table('sc_inv_production_of_goods')
                        ->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);


                }

                 if($data['is_debet']==1)
                {
                    MerchantAr::insert([
                        'sync_id'=>$data['merchant_id'].Uuid::uuid4()->toString(),
                        'md_merchant_id'=>$data['merchant_id'],
                        'residual_amount'=>($data['is_with_load_shipping']==0)?$data['total']-$data['paid_nominal']:$data['total'],
                        'paid_nominal'=>$data['paid_nominal'],
                        'is_paid_off'=>0,
                        'arable_id'=>$optionSaleId,
                        'arable_type'=>'App\Models\SennaToko\SaleOrder',
                        'due_date'=>$data['due_date'],
                        'timezone'=>$data['timezone'],
                        'created_at'=>$data['timestamp'],
                        'updated_at'=>$data['timestamp']
                    ]);

                }
                DB::commit();
                Session::forget('sale_faktur_'.$optionSaleId);
                if(CoaSaleUtil::coaStockAdjustmentFromSaleV2($data['user_id'], $data['merchant_id'],$data['coaJson'],$data['warehouse_id'],$data['inv_id'], $optionSaleId) == false) {
                    DB::rollBack();
                    return false;
                }
                return true;

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }
}

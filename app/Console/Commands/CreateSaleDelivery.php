<?php

namespace App\Console\Commands;

use App\Utils\Inventory\InventoryUtilV2;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CreateSaleDelivery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sale:delivery {--id|sale_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Sale Delivery';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            DB::beginTransaction();
            $optionSaleId=$this->option('sale_id');
            if(!is_null($optionSaleId))
            {
                $data=Session::get('sale_delivery_'.$optionSaleId);
                if(!is_null($data))
                {
                    $calculateInventory=InventoryUtilV2::inventoryCodev2($data['inv_id'],$data['warehouse_id'],$data['productCalculate'],'minus');
                    if($calculateInventory==false)
                    {
                        return false;
                    }
                    $updateStockInv="";
                    $stockInvId=[];
                    $saleMapping=[];
                    foreach ($calculateInventory as $key => $n)
                    {
                        $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                        $stockInvId[]=$n['id'];
                        $saleMapping[]=[
                            'sc_sale_order_id'=>$optionSaleId,
                            'sc_product_id'=>$n['sc_product_id'],
                            'sc_stock_inventory_id'=>$n['id'],
                            'amount'=>$n['amount'],
                            'purchase_price'=>$n['purchase'],
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                    if($updateStockInv!=""){
                        DB::statement("
                        update sc_stock_inventories as t set
                                residual_stock = c.column_a
                            from (values
                                $updateStockInv
                            ) as c(column_b, column_a)
                            where c.column_b = t.id;
                        ");
                    }

                    if($data['updateStockProduct']!=""){
                        DB::statement("
                        update sc_products as t set
                                stock = c.column_a
                            from (values
                                ".$data['updateStockProduct']."
                            ) as c(column_b, column_a)
                            where c.column_b = t.id;
                        ");
                    }

                    DB::table('sc_stock_sale_mappings')->insert($saleMapping);
                    DB::table('sc_stock_inventories')->insert($data['stock_in_out']);
                    DB::table('sc_inv_production_of_goods')
                        ->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);
                    DB::commit();
                    Session::forget('sale_delivery_'.$optionSaleId);
                }
            }
            return  true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Accounting\MerchantAp;
use App\Utils\Accounting\CoaPurchaseUtil;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\SennaToko\StockInventory;
use App\Utils\Inventory\InventoryUtilTransfer;
use Ramsey\Uuid\Uuid;

class CreateTransferStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer:stock {--id|transfer_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Transfer Stock';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $optionTransferId=$this->option('transfer_id');
            if(!is_null($optionTransferId))
            {
                $data=Session::get('transfer_stock_'.$optionTransferId);
                $stockMutation=[];
                foreach($data['sc_product_id'] as $key =>$item){
                    $purchaseStock=InventoryUtilTransfer::inventoryCode($data['merchantId'],$data['from_warehouse_id'],$data['sc_product_id'][$key],$data['quantity'][$key],'minus');
                    if($purchaseStock==false)
                    {
                        DB::rollBack();
                        return false;
                    }
                    foreach ($purchaseStock as $p)
                    {
                        array_push($stockMutation,[
                            'sync_id'=>$data['sc_product_id'][$key].Uuid::uuid4()->toString(),
                            'sc_product_id'=>$data['sc_product_id'][$key],
                            'total'=>$p['amount'],
                            'inv_warehouse_id'=>$data['to_warehouse_id'],
                            'record_stock'=>$p['amount'],
                            'created_by'=>user_id(),
                            'residual_stock'=>$p['amount'],
                            'type'=>StockInventory::IN,
                            'selling_price'=>$p['selling'],
                            'purchase_price'=>$p['purchase'],
                            'transaction_action'=> 'Penambahan Stok '.$data['product_name'][$key]. ' Dari Transfer Stok Dengan Code '.$data['transfer_code'],
                            'stockable_type'=>'App\Models\SennaToko\TransferStock',
                            'stockable_id'=>$data['transfer_id'],
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'),
                        ]);
                    }
                }

                StockInventory::insert($stockMutation);


                DB::commit();
                Session::forget('transfer_stock_'.$optionTransferId);

                return true;

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }
}

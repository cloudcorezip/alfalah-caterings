<?php

namespace App\Console\Commands;

use App\Mail\EmailForExpiredSubscription;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class NotifyExpiredSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:notify-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Untuk notif pengguna yang akan habis subscriptionnya';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data=DB::select("
                select
                mm.name,
                u.fullname,
                mm.start_subscription,
                mm.end_subscription,
                u.email,
          date_part(
            'day', mm.end_subscription - mm.start_subscription
          ) as interval
        from
          md_merchants mm
          join md_users u on u.id = mm.md_user_id
        where
          u.md_role_id = 3
          and mm.start_subscription notnull
          and date_part(
            'day', mm.end_subscription - mm.start_subscription
          )<= 5
        ");
        if(!empty($data))
        {
            foreach ($data as $key =>$item)
            {
                $email = new EmailForExpiredSubscription($item);
                Mail::to($item->email)
                    ->send($email);
            }

        }

    }
}

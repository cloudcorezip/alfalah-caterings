<?php

namespace App\Console\Commands;

use App\Models\Accounting\MerchantAp;
use App\Utils\Accounting\CoaPurchaseUtil;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\SennaToko\StockInventory;
use Ramsey\Uuid\Uuid;

class CreatePurchaseFaktur extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'purchase:faktur {--id|purchase_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Purchase Faktur';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $optionPurchaseId=$this->option('purchase_id');
            if(!is_null($optionPurchaseId))
            {
                $data=Session::get('purchase_faktur_'.$optionPurchaseId);

                if($data['updateStockProduct']!=""){
                    DB::statement("
                        update sc_products as t set
                                stock = c.column_a,
                                purchase_price = c.column_c
                            from (values
                                ".$data['updateStockProduct']."
                            ) as c(column_b, column_a, column_c)
                            where c.column_b = t.id;
                        ");
                }
                StockInventory::insert($data['stock_in']);

                if($data['is_debet']==1)
                {
                    MerchantAp::insert([
                        'apable_id'=>$optionPurchaseId,
                        'apable_type'=>'App\Models\SennaToko\PurchaseOrder',
                        'sync_id'=>$data['merchant_id'].Uuid::uuid4()->toString(),
                        'md_merchant_id'=>$data['merchant_id'],
                        'residual_amount'=>$data['total']-$data['paid_nominal'],
                        'paid_nominal'=>$data['paid_nominal'],
                        'is_paid_off'=>0,
                        'due_date'=>$data['due_date'],
                        'timezone'=>$data['timezone'],
                        'created_at'=>$data['timestamp'],
                        'updated_at'=>$data['timestamp']
                    ]);
                }
                DB::commit();
                Session::forget('purchase_faktur_'.$optionPurchaseId);
                if(CoaPurchaseUtil::coaStockAdjustmentFromPurchasev2($data['user_id'],$data['merchant_id'],$data['coaJson'],$data['purchase'])==false)
                {
                    DB::rollBack();
                    return false;
    
                }
                return true;

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }
}

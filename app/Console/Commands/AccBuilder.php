<?php

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Product;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaUtil;
use App\Utils\Accounting\InitUtil;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AccBuilder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'acc:build {--M|merchant=30}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Accounting DataBase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            $merchantId=$this->option('merchant');
            $timestamp=date('Y-m-d H:i:s');
            DB::beginTransaction();
            if(DB::table('acc_periods')->where('md_merchant_id',$merchantId)->count()<1) {
                $lastCategoriesId=DB::table('acc_coa_categories')->orderBy('id','desc')->first();
                $lastCoa=DB::table('acc_coa_details')->orderBy('id','desc')->first();

                if(is_null($lastCategoriesId))
                {
                    $i=0;
                }else{
                    $i=$lastCategoriesId->id;
                }
                if(is_null($lastCoa))
                {
                    $i_d=0;
                }else{
                    $i_d=$lastCoa->id;
                }

                DB::table('acc_periods')
                    ->insert([

                        [
                            'name' => 'Periode ' . (date('Y')-1),
                            'start_period' => (date('Y')-1) . '-01-01',
                            'end_period' => (date('Y')-1) . '-12-31',
                            'is_active' => 0,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'name' => 'Periode ' . date('Y'),
                            'start_period' => date('Y') . '-01-01',
                            'end_period' => date('Y') . '-12-31',
                            'is_active' => 1,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ]]);

                DB::table('acc_coa_categories')
                    ->insert([
                        [
                            'id' => $i+1,
                            'code' => '1.0.00',
                            'name' => 'Aktiva',
                            'parent_id' => NUll,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],

                        [
                            'id' =>$i+2,
                            'code' => '2.0.00',
                            'name' => 'Kewajiban',
                            'parent_id' => NUll,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' =>$i+3,
                            'code' => '3.0.00',
                            'name' => 'Ekuitas',
                            'parent_id' => NUll,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+4,
                            'code' => '4.0.00',
                            'name' => 'Pendapatan',
                            'parent_id' => NUll,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+5,
                            'code' => '5.0.00',
                            'parent_id' => NUll,
                            'name' => 'Harga Pokok Penjualan',
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+6,
                            'code' => '6.0.00',
                            'name' => 'Beban',
                            'parent_id' => NUll,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+7,
                            'code' => '1.1.00',
                            'name' => 'Aktiva Lancar',
                            'parent_id' => $i+1,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+8,
                            'code' => '1.2.00',
                            'name' => 'Aktiva Tidak Lancar',
                            'parent_id' => $i+1,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+9,
                            'code' => '1.3.00',
                            'name' => 'Ayat Silang',
                            'parent_id' => $i+1,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],

                        [
                            'id' => $i+10,
                            'code' => '1.1.01',
                            'name' => 'Kas',
                            'parent_id' => $i+7,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+11,
                            'code' => '1.1.02',
                            'name' => 'Bank',
                            'parent_id' => $i+7,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+12,
                            'code' => '1.1.03',
                            'name' => 'Piutang',
                            'parent_id' => $i+7,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+13,
                            'code' => '1.1.04',
                            'name' => 'Investasi Jangka Pendek',
                            'parent_id' => $i+7,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+14,
                            'code' => '1.1.05',
                            'name' => 'Persediaan',
                            'parent_id' => $i+7,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+15,
                            'code' => '1.1.07',
                            'name' => 'Aktiva Lancar Lainnya',
                            'parent_id' => $i+7,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],

                        [
                            'id' => $i+19,
                            'code' => '1.2.01',
                            'name' => 'Aktiva Tetap',
                            'parent_id' =>$i+8,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+20,
                            'code' => '1.2.02',
                            'name' => 'Investasi Jangka Panjang',
                            'parent_id' => $i+8,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+21,
                            'code' => '1.2.03',
                            'name' => 'Aktiva Tidak Berwujud',
                            'parent_id' => $i+8,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' =>$i+22,
                            'code' => '1.3.01',
                            'name' => 'Ayat Silang',
                            'parent_id' => $i+9,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],

                        [
                            'id' => $i+23,
                            'code' => '2.1.00',
                            'name' => 'Kewajiban Lancar',
                            'parent_id' => $i+2,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+24,
                            'code' => '2.2.00',
                            'name' => 'Kewajiban Tidak Lancar',
                            'parent_id' => $i+2,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],

                        [
                            'id' => $i+25,
                            'code' => '2.1.01',
                            'name' => 'Utang',
                            'parent_id' => $i+23,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' =>$i+28,
                            'code' => '2.1.04',
                            'name' => 'Utang Pajak',
                            'parent_id' => $i+23,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],

                        [
                            'id' => $i+31,
                            'code' => '2.2.01',
                            'name' => 'Kewajiban Jangka Panjang',
                            'parent_id' => $i+24,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+32,
                            'code' => '3.1.00',
                            'name' => 'Modal',
                            'parent_id' => $i+3,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+33,
                            'code' => '3.2.00',
                            'name' => 'Saldo Laba',
                            'parent_id' =>$i+3,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+34,
                            'code' => '3.3.00',
                            'name' => 'Dividen',
                            'parent_id' => $i+3,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],

                        [
                            'id' => $i+35,
                            'code' => '4.1.00',
                            'name' => 'Pendapatan',
                            'parent_id' => $i+4,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+37,
                            'code' => '4.3.00',
                            'name' => 'Pendapatan Lainnya',
                            'parent_id' => $i+4,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+39,
                            'code' => '6.1.01',
                            'name' => 'Beban Operasional & Usaha',
                            'parent_id' => $i+6,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                        [
                            'id' => $i+41,
                            'code' => '6.1.03',
                            'name' => 'Beban Lainnya',
                            'parent_id' => $i+6,
                            'md_merchant_id' => $merchantId,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp
                        ],
                    ]);
                DB::table('acc_coa_details')->insert([
                    [
                        //'id' => ,
                        'id'=>++$i_d,'code' => '1.1.01.01',
                        'name' => 'Kas',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+10,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp

                    ],
                    [
                        //'id' => $i_d+3,
                        'id'=>++$i_d,'code' => '1.1.02.01',
                        'name' => 'Gopay',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+11,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => $i_d+4,
                        'id'=>++$i_d,'code' => '1.1.02.02',
                        'name' => 'Ovo',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+11,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => $i_d+5,
                        'id'=>++$i_d,'code' => '1.1.02.03',
                        'name' => 'Dana',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+11,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => $i_d+6,
                        'id'=>++$i_d,'code' => '1.1.02.04',
                        'name' => 'ShopeePay',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+11,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => $i_d+7,
                        'id'=>++$i_d,'code' => '1.1.02.05',
                        'name' => 'Cashlez',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' =>$i+11,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' =>$i_d+8,
                        'id'=>++$i_d,'code' => '1.1.02.06',
                        'name' => 'LinkAja',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+11,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => $i_d+9,
                        'id'=>++$i_d,'code' => '1.1.03.01',
                        'name' => 'Piutang Usaha',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+12,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => $i_d+10,
                        'id'=>++$i_d,'code' => '1.1.03.02',
                        'name' => 'Piutang Pembelian',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+12,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => $i_d+11,
                        'id'=>++$i_d,'code' => '1.1.03.03',
                        'name' => 'Piutang Lainnya',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+12,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => $i_d+12,
                        'id'=>++$i_d,'code' => '1.1.03.04',
                        'name' => 'Piutang Penjualan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+12,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],

                    [
                        //'id' => $i_d+13,
                        'id'=>++$i_d,'code' => '1.1.04.01',
                        'name' => 'Investasi Jangka Pendek',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+13,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],

                    [
                        //'id' => $i_d+19,
                        'id'=>++$i_d,'code' => '1.1.06.01',
                        'name' => 'PPN Masukan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+15,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '14',
                        'id'=>++$i_d,'code' => '1.1.06.02',
                        'name' => 'Pajak Dibayar Dimuka PPh21',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+15,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '15',
                        'id'=>++$i_d,'code' => '1.1.06.03',
                        'name' => 'Pajak Dibayar Dimuka PPh22',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+15,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '16',
                        'id'=>++$i_d,'code' => '1.1.06.04',
                        'name' => 'Pajak Dibayar Dimuka PPh23',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+15,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '17',
                        'id'=>++$i_d,'code' => '1.1.06.05',
                        'name' => 'Pajak Dibayar Dimuka PPh25',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+15,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '23',
                        'id'=>++$i_d,'code' => '1.2.01.01',
                        'name' => 'Tanah',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '24',
                        'id'=>++$i_d,'code' => '1.2.01.02',
                        'name' => 'Bangunan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '25',
                        'id'=>++$i_d,'code' => '1.2.01.03',
                        'name' => 'Kendaraan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '26',
                        'id'=>++$i_d,'code' => '1.2.01.04',
                        'name' => 'Mesin & Peralatan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '27',
                        'id'=>++$i_d,'code' => '1.2.01.05',
                        'name' => 'Perlengkapan Kantor',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '28',
                        'id'=>++$i_d,'code' => '1.2.01.06',
                        'name' => 'Sewa Guna Usaha',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '29',
                        'id'=>++$i_d,'code' => '1.2.01.07',
                        'name' => 'Akumulasi Penyusutan Kendaraan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '30',
                        'id'=>++$i_d,'code' => '1.2.01.08',
                        'name' => 'Akumulasi Penyusutan Mesin & Peralatan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '31',
                        'id'=>++$i_d,'code' => '1.2.01.09',
                        'name' => 'Akumulasi Penyusutan Perlengkapan Kantor',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+19,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],

                    [
                        //'id' => (int)$merchantId . '32',
                        'id'=>++$i_d,'code' => '1.2.02.01',
                        'name' => 'Investasi Jangka Panjang',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+20,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '35',
                        'id'=>++$i_d,'code' => '1.3.01.01',
                        'name' => 'Ayat Silang',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+22,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '36',
                        'id'=>++$i_d,'code' => '1.3.01.02',
                        'name' => 'Ayat Silang Inisialisasi Persediaan Barang',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+22,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '37',
                        'id'=>++$i_d,'code' => '1.3.01.03',
                        'name' => 'Ayat Silang Inisialisasi Aset',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+22,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],

                    [
                        //'id' => (int)$merchantId . '38',
                        'id'=>++$i_d,'code' => '2.1.01.01',
                        'name' => 'Utang Usaha',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+25,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '39',
                        'id'=>++$i_d,'code' => '2.1.01.02',
                        'name' => 'Utang Saldo Deposit',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+25,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '40',
                        'id'=>++$i_d,'code' => '2.1.01.03',
                        'name' => 'Utang Pembelian Barang',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' =>$i+25,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '41',
                        'id'=>++$i_d,'code' => '2.1.01.04',
                        'name' => 'Utang Pembelian Aset',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+25,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '42',
                        'id'=>++$i_d,'code' => '2.1.01.05',
                        'name' => 'Utang Lainnya',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+25,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '43',
                        'id'=>++$i_d,'code' => '2.1.04.01',
                        'name' => 'Utang PPN Keluaran',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+28,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '44',
                        'id'=>++$i_d,'code' => '2.1.04.02',
                        'name' => 'Utang PPh 21',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+28,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '45',
                        'id'=>++$i_d,'code' => '2.1.04.03',
                        'name' => 'Utang PPh 22',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+28,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '46',
                        'id'=>++$i_d,'code' => '2.1.04.04',
                        'name' => 'Utang PPh 23',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+28,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '47',
                        'id'=>++$i_d,'code' => '2.1.04.05',
                        'name' => 'Utang PPh 25',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+28,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '566',
                        'id'=>++$i_d,'code' => '3.0.00.01',
                        'name' => 'Ikhtisar Laba Rugi',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+3,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],

                    [
                        //'id' => (int)$merchantId . '57',
                        'id'=>++$i_d,'code' => '3.1.00.01',
                        'name' => 'Modal Disetor',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+32,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '58',
                        'id'=>++$i_d,'code' => '3.1.00.02',
                        'name' => 'Modal Saham',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' =>$i+32,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],

                    [
                        //'id' => (int)$merchantId . '59',
                        'id'=>++$i_d,'code' => '3.2.00.01',
                        'name' => 'Saldo Laba Ditahan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' =>$i+33,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '60',
                        'id'=>++$i_d,'code' => '3.2.00.02',
                        'name' => 'Saldo Laba Tahun Berjalan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+33,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '61',
                        'id'=>++$i_d,'code' => '3.3.00.01',
                        'name' => 'Dividen',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+34,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '62',
                        'id'=>++$i_d,'code' => '4.1.00.01',
                        'name' => 'Penjualan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+35,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '633',
                        'id'=>++$i_d,'code' => '4.2.00.02',
                        'name' => 'Laba Penjualan Aset',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' =>$i+37,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '64',
                        'id'=>++$i_d,'code' => '4.3.00.01',
                        'name' => 'Pendapatan Lainnya',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+37,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '66',
                        'id'=>++$i_d,'code' => '4.3.00.03',
                        'name' => 'Diskon Pembelian Barang',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Kredit',
                        'acc_coa_category_id' => $i+37,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '70',
                        'id'=>++$i_d,'code' => '6.1.01.01',
                        'name' => 'Operasional Produksi',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '72',
                        'id'=>++$i_d,'code' => '6.1.02.01',
                        'name' => 'Gaji',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '73',
                        'id'=>++$i_d,'code' => '6.1.02.02',
                        'name' => 'PPh 21',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '74',
                        'id'=>++$i_d,'code' => '6.1.02.03',
                        'name' => 'BPJS Ketenagakerjaan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '75',
                        'id'=>++$i_d,'code' => '6.1.02.04',
                        'name' => 'BPJS Kesehatan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '76',
                        'id'=>++$i_d,'code' => '6.1.02.05',
                        'name' => 'THR',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '77',
                        'id'=>++$i_d,'code' => '6.1.02.06',
                        'name' => 'Administrasi Bank',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '78',
                        'id'=>++$i_d,'code' => '6.1.02.07',
                        'name' => 'Insentif dan Bonus',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '79',
                        'id'=>++$i_d,'code' => '6.1.02.08',
                        'name' => 'Tunjangan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '80',
                        'id'=>++$i_d,'code' => '6.1.02.09',
                        'name' => 'Makan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '81',
                        'id'=>++$i_d,'code' => '6.1.02.10',
                        'name' => 'Medis',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],

                    [
                        //'id' => (int)$merchantId . '82',
                        'id'=>++$i_d,'code' => '6.1.02.11',
                        'name' => 'Perjalanan Dinas',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '83',
                        'id'=>++$i_d,'code' => '6.1.02.12',
                        'name' => 'Transportasi, BBM, Toll & Parkir',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '84',
                        'id'=>++$i_d,'code' => '6.1.02.13',
                        'name' => 'Listrik',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '85',
                        'id'=>++$i_d,'code' => '6.1.02.14',
                        'name' => 'Gas',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '86',
                        'id'=>++$i_d,'code' => '6.1.02.15',
                        'name' => 'Air atau PDAM',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '87',
                        'id'=>++$i_d,'code' => '6.1.02.16',
                        'name' => 'Telepon, Fax & Internet',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '88',
                        'id'=>++$i_d,'code' => '6.1.02.17',
                        'name' => 'Keamanan dan Kebersihan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '89',
                        'id'=>++$i_d,'code' => '6.1.02.18',
                        'name' => 'Meterai',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '90',
                        'id'=>++$i_d,'code' => '6.1.02.19',
                        'name' => 'ATK & Fotocopy',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '91',
                        'id'=>++$i_d,'code' => '6.1.02.20',
                        'name' => 'Perlengkapan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '92',
                        'id'=>++$i_d,'code' => '6.1.02.21',
                        'name' => 'Pengiriman',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '93',
                        'id'=>++$i_d,'code' => '6.1.02.22',
                        'name' => 'Pos, Paket, Kurir',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '94',
                        'id'=>++$i_d,'code' => '6.1.02.23',
                        'name' => 'Servis dan Pemeliharaan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '95',
                        'id'=>++$i_d,'code' => '6.1.02.24',
                        'name' => 'Sewa Kendaraan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '96',
                        'id'=>++$i_d,'code' => '6.1.02.25',
                        'name' => 'Entertainment dan Representasi',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '97',
                        'id'=>++$i_d,'code' => '6.1.02.26',
                        'name' => 'Rekrutmen dan Pelatihan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '98',
                        'id'=>++$i_d,'code' => '6.1.02.27',
                        'name' => 'Promosi & Iklan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '99',
                        'id'=>++$i_d,'code' => '6.1.02.28',
                        'name' => 'Asuransi Kendaraan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '100',
                        'id'=>++$i_d,'code' => '6.1.02.29',
                        'name' => 'Sewa Kantor',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '101',
                        'id'=>++$i_d,'code' => '6.1.02.30',
                        'name' => 'Lisensi/Izin',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '102',
                        'id'=>++$i_d,'code' => '6.1.02.31',
                        'name' => 'Legal',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '103',
                        'id'=>++$i_d,'code' => '6.1.02.32',
                        'name' => 'Donasi',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '104',
                        'id'=>++$i_d,'code' => '6.1.02.33',
                        'name' => 'Piutang Tak Tertagih',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '105',
                        'id'=>++$i_d,'code' => '6.1.02.34',
                        'name' => 'Operasional Lainnya',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        //'id' => (int)$merchantId . '106',
                        'id'=>++$i_d,'code' => '6.1.02.35',
                        'name' => 'Penyusutan Bangunan Kantor',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        'id'=>++$i_d,
                        'code' => '6.1.02.36',
                        'name' => 'Penyusutan Kendaraan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        'id'=>++$i_d,
                        'code' => '6.1.02.37',
                        'name' => 'Penyusutan Peralatan Kantor',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+39,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        'id'=>++$i_d,
                        'code' => '6.1.03.01',
                        'name' => 'Diskon Penjualan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+41,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        'id'=>++$i_d,
                        'code' => '6.1.03.02',
                        'name' => 'Beban Lainnya',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+41,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        'id'=>++$i_d,
                        'code' => '6.1.03.03',
                        'name' => 'Beban Penjualan Aset',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+41,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                    [
                        'id'=>++$i_d,
                        'code' => '6.1.03.04',
                        'name' => 'Penyesuaian Persediaan',
                        'md_sc_currency_id' => 1,
                        'type_coa' => 'Debit',
                        'acc_coa_category_id' => $i+41,
                        'parent_id' => NULL,
                        'created_at' => $timestamp,
                        'updated_at' => $timestamp
                    ],
                ]);

                DB::statement("ALTER SEQUENCE acc_coa_categories_id_seq RESTART WITH ".($i+42)."");
                DB::statement("ALTER SEQUENCE acc_coa_details_id_seq RESTART WITH ".($i_d+1)."");
                Controller::getGlobalWarehouse($merchantId);
            }


            InitUtil::initCoaToMerchantDetailUtil($merchantId);
            InitUtil::generateNewCoa($merchantId);
            InitUtil::generateNewMappingAndCashflowFormat($merchantId);
            DB::commit();
            return 1;
        }catch (\Exception $e)
        {
            DB::rollBack();

            return  0;

        }
    }
}

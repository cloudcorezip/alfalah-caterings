<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Builder;


use App\Models\Cfg\MerchantMenu;
use App\Models\Plugin\EnableMerchantPlugin;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;

class SfaInstallation
{

    public static function generate($merchantId)
    {
        try{

            Session::forget(env('APP_MENU_VERSION').'_sfa_'.$merchantId);

            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }
}

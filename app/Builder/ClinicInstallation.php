<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Builder;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Ramsey\Uuid\Uuid;

class ClinicInstallation
{
    public static function generate($merchantId)
    {
        try{
            Session::forget(env('APP_MENU_VERSION').'_clinic_'.$merchantId);
            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }
}

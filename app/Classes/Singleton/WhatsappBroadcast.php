<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Classes\Singleton;


class WhatsappBroadcast
{
    private static $instance=null;

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new WhatsappBroadcast();
        }

        return self::$instance;
    }

    private function _curl($url,$data)
    {
        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "".env('WA_BROADCAST_URL')."$url");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/x-www-form-urlencoded",
            ));
            $response = curl_exec($ch);
            curl_close($ch);

            $result=json_decode($response);
            return $result;
        }catch (\Exception $e)
        {
            return [
                'status'=>false,
                'message'=>$e->getMessage()
            ];

        }
    }

    public function send($data)
    {
        //data['number'=>081228656274,'message'=>'A']
        $send='number='.$data['number'].'&message='.$data['message'];
        return $this->_curl('send-message',$send);
    }
}

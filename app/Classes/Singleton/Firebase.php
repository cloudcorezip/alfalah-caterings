<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Classes\Singleton;


use Illuminate\Support\Facades\DB;
use Kreait\Firebase\Messaging\AndroidConfig;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\WebPushConfig;

class Firebase
{
    private static $instance=null;

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Firebase();
        }

        return self::$instance;
    }
    public static function send($title,$message,$image='',$to,$controller,$route,$data=[],$isActivity=0){
        try{
            DB::table('md_user_activity_notifications')
                ->insert([
                    'title'=>$title,
                    'body'=>$message,
                    'is_read'=>0,
                    'type'=>$isActivity,
                    'other_information'=>json_encode(is_object($data)?$data->toArray():$data),
                    'md_user_id'=>$to->id,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
            if(!is_null($to->fcm_key))
            {
                $messaging =app('firebase.messaging');
                $messages = CloudMessage::withTarget('token', $to->fcm_key)
                    ->withNotification(
                        Notification::create(
                            $title,$message
                        )
                    )->withData(is_object($data)?$data->toArray():$data);
                return $messaging->send($messages);
            }

        }catch (\Exception $e)
        {
            log_helper($controller,$route,$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }


    }

    public static function sendWithoutCollection($title,$message,$image='',$to,$controller,$route,$data=[],$isActivity=0){
        try{

            $messaging =app('firebase.messaging');
            $messages = CloudMessage::withTarget('token', $to->fcm_key)
                ->withNotification(
                    Notification::create(
                        $title,$message
                    )
                )->withData((array)$data);


            DB::table('md_user_activity_notifications')
                ->insert([
                    'title'=>$title,
                    'body'=>$message,
                    'is_read'=>0,
                    'type'=>$isActivity,
                    'other_information'=>json_encode((array)$data),
                    'md_user_id'=>$to->id,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
            return $messaging->send($messages);
        }catch (\Exception $e)
        {
            log_helper($controller,$route,$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }


    }

}

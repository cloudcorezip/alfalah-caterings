<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Classes\Singleton;


class Message
{
    private static $instance=null;

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Message();
        }

        return self::$instance;
    }

    public static function getArrayResponse($code,$message,$data=[],$alert='')
    {
        return [
            'code' => $code,
            'message' => $message,
            'data' => $data,
            'alert'=>$alert
        ];
    }

    public static function getJsonResponse($code,$message,$data=[])
    {
        return response()->json([
            'code'=>$code,
            'message'=>$message,
            'data'=>$data
        ]);

    }

}

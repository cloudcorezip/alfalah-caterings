<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Classes\Singleton;


use Yajra\DataTables\DataTables;

class DataTable
{
    private static $instance=null;

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new DataTable();
        }

        return self::$instance;
    }

    public function dataTable($model,$query,$request,$rawColumns=[],$searchColumns=[])
    {
        $data=$query;
        $dataTables = DataTables::of($data)
            ->addColumn('opsi', function ($list) use ($model) {
                return $model::getButton($list);
            })
            ->filter(function ($query) use ($request,$searchColumns) {
                if ($request->input('search')['value']) {
                    if(!empty($searchColumns)){
                        foreach ($searchColumns as $key =>$item){
                            ($key==0)?$query->where(''.$searchColumns[$key].'', 'like', "%{$request->input('search')['value']}%")
                                :$query->orWhere(''.$searchColumns[$key].'', 'like', "%{$request->input('search')['value']}%");
                        }
                    }
                }
            })
            ->rawColumns($rawColumns)
            ->setRowId('id');

        return $dataTables;
    }

    public function dataTableForMerchant($model,$query,$request,$rawColumns=[],$searchColumns=[],$isEdit,$isDelete,$isHead=true)
    {
        $data=$query;
        $dataTables = DataTables::of($data)
            ->addColumn('opsi', function ($list) use ($model,$isEdit,$isDelete,$isHead) {
                return $model::getButton($list,$isEdit,$isDelete,$isHead);
            })
            ->filter(function ($query) use ($request,$searchColumns) {
                if ($request->input('search')['value']) {
                    $search=strtolower($request->input('search')['value']);
                    if(!empty($searchColumns)){
                        foreach ($searchColumns as $key =>$item){
                            ($key==0)?$query->whereRaw("".strtolower($searchColumns[$key])." like '%".$search."%'")
                                :$query->orWhereRaw("".strtolower($searchColumns[$key])." like '%".$search."%'");
                        }
                    }
                }
            })
            ->rawColumns($rawColumns)
            ->setRowId('id');

        return $dataTables;
    }

    public function dataTableForMerchantV2($model,$query,$rawColumns,$isEdit,$isDelete,$isHead=true)
    {
        $data=$query;
        $dataTables = DataTables::of($data)
            ->addColumn('opsi', function ($list) use ($model,$isEdit,$isDelete,$isHead) {
                return $model::getButton($list,$isEdit,$isDelete,$isHead);
            })
            ->rawColumns($rawColumns)
            ->setRowId('id');

        return $dataTables;
    }

    public function dataTableWithoutButton($model,$query,$request,$rawColumns=[],$searchColumns=[])
    {
        $data=$query;
        $dataTables = DataTables::of($data)
            ->filter(function ($query) use ($request,$searchColumns) {
                if ($request->input('search')['value']) {
                    if(!empty($searchColumns)){
                        foreach ($searchColumns as $key =>$item){
                            ($key==0)?$query->where(''.$searchColumns[$key].'', 'like', "%{$request->input('search')['value']}%")
                                :$query->orWhere(''.$searchColumns[$key].'', 'like', "%{$request->input('search')['value']}%");
                        }
                    }
                }
            })
            ->rawColumns($rawColumns)
            ->setRowId('id');

        return $dataTables;
    }
}

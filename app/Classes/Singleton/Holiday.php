<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Classes\Singleton;


class Holiday
{
    private static $instance=null;

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Holiday();
        }

        return self::$instance;
    }

    public function _curl($year)
    {
        try {
            $ch = curl_init();
            $url="https://api-harilibur.vercel.app/api?year=$year";

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
            ));
            $response = curl_exec($ch);
            curl_close($ch);

            $result=json_decode($response);
            return [
                'status'=>true,
                'message'=>'Success',
                'data'=>$result
            ];
        }catch (\Exception $e)
        {
            return [
                'status'=>false,
                'message'=>$e->getMessage(),
                'data'=>[]
            ];

        }
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Classes\Singleton;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class Crud
{
    private static $instance=null;
    private $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Crud();
        }

        return self::$instance;
    }


    public function create($model,$params,$with='',$currentParams=[],$isMany=false,$isFile=false)
    {
        DB::beginTransaction();
        try{
            unset($params['_token']);
            $modelName=new $model;
            $rules=$modelName->rules;
            if(is_null($params['id'])){
                $validator = Validator::make($params,$rules);

                if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    return $this->message::getArrayResponse(404,'warning', $error, null);

                }
                $checkAsFile=self::actionAsFile($rules,'mimes');
                if($checkAsFile){
                    $params=array_merge($params,self::actionUploadImage($checkAsFile,$rules,$params,$modelName->getTable()));
                }
            }else{
                if($isFile==false){
                    if($modelName->rulesUpdate){
                        $validator = Validator::make($params,$modelName->rulesUpdate);

                        if ($validator->fails()) {
                            $error = $validator->errors()->first();
                            return $this->message::getArrayResponse(404,'warning', $error, null);
                        }
                    }else{

                        $validator = Validator::make($params,$rules);

                        if ($validator->fails()) {
                            $error = $validator->errors()->first();
                            return $this->message::getArrayResponse(404,'warning', $error, null);

                        }

                        $checkAsFile=self::actionAsFile($rules,'mimes');
                        if($checkAsFile){
                            $params=array_merge($params,self::actionUploadImage($checkAsFile,$rules,$params,$modelName->getTable()));
                        }
                    }
                }else{
                    $validator = Validator::make($params,$rules);

                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        return $this->message::getArrayResponse(404,'warning', $error, null);
                    }

                    $checkAsFile=self::actionAsFile($rules,'mimes');
                    if($checkAsFile){
                        $params=array_merge($params,self::actionUploadImage($checkAsFile,$rules,$params,$modelName->getTable()));
                    }
                }

            }


            if(is_null($params['id'])){
                DB::commit();
                if($with==''){
                    $model::create($params);
                }else{
                    if($isMany==false){
                        $create=$modelName::create($params);
                        $create->$with()->create($currentParams);
                    }else{
                        $create=$modelName::create($params);
                        if(count($currentParams)>0){
                            for($i=0;$i<count($currentParams);$i++){
                                $create->$with()->createMany([$currentParams[$i]]);
                            }
                        }
                    }
                }
                return $this->message::getArrayResponse(200,'success', trans('message.200'), null);

            }else{
                DB::commit();
                if($with==''){
                    $update=$model::find($params['id']);
                    //$update->$with()->delete();
                    $update->update($params);
                }else{
                    $update=$model::find($params['id']);
                    if(is_null($update->$with)){
                        $update->$with()->create($currentParams);
                    }else{
                        if($isMany==false){
                            $update->$with()->create($currentParams);
                        }else{
                            $update->$with()->delete();
                            if(count($currentParams)>0){
                                for($i=0;$i<count($currentParams);$i++){
                                    $update->$with()->createMany([$currentParams[$i]]);
                                }
                            }
                        }

                    }
                    $update->update($params);
                }
                return $this->message::getArrayResponse(200,'success', trans('message.302'), null);

            }

        }catch (\Exception $e){
            DB::rollBack();
            log_helper($model,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return $this->message::getArrayResponse(500,'danger', trans('message.500'), null);

        }



    }

    private static function actionAsFile($array,$keyword)
    {
        $data=[];
        foreach($array as $index => $string) {
            if (strpos($string, $keyword) !== FALSE)
                $data[$index]=$string;
        }
        return $data;
    }


    private static function actionUploadImage($data,$rules,$params,$path)
    {
        $fileData=[];
        if(!empty($data)){
            $data=array_intersect($rules,$data);
            foreach ($params as $key =>$value){
                if(array_key_exists($key,$data)){
                    $currentData[$key]=$value;
                }
            }
            foreach ($currentData as $key =>$item){
                if(!is_null($item) || $item!=''){
                    $destinationPath = 'public/uploads/'.$path.'/';
                    if(!file_exists($destinationPath)){
                        mkdir($destinationPath,0777,true);
                    }
                    $fileName = uniqid($key).'_'.date('YmdHis').'_'.$item->getClientOriginalName();
                    $item->move($destinationPath, $fileName);
                    $fileData[$key]=$fileName;
                }
            }

        }
        return $fileData;

    }

    public function delete($model,$id)
    {
        try{
            $model::find($id)->delete();
            $response=$this->message::getArrayResponse(200,'success', trans('message.202'), null);

        }catch (\Exception $e)
        {
            log_helper($model,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            $response=$this->message::getArrayResponse(500,'danger', trans('message.501'), null);
        }

        return $response;

    }

    public function insert($model,$params){

        try{
            $model::insert($params);
            return $this->message::getArrayResponse(200,'success', trans('message.200'), null);
        }catch (\Exception $e){

            log_helper($model,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return $this->message::getArrayResponse(500,'danger', trans('message.500'), null);

        }

    }

}

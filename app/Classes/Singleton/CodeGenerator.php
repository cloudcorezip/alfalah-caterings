<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Classes\Singleton;


use App\Models\Accounting\Jurnal;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantArDetail;
use App\Models\Accounting\MerchantApDetail;
use App\Models\Marketing\MerchantHistoryRedeemPoint;
use App\Models\SennaJasa\OrderService;
use App\Models\SennaPayment\HistoryTransactionIn;
use App\Models\SennaPayment\HistoryTransactionOut;
use App\Models\SennaPayment\Transaction;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\StockOpname;
use App\Models\SennaToko\TransferStock;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Self_;
use Ramsey\Uuid\Uuid;

class CodeGenerator
{
    private static $instance = null;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new CodeGenerator();
        }

        return self::$instance;
    }

    public static function codeGenerator($length = 8)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public static function generatePin($length = 6)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateTransactionCode($userId,$type)
    {

        return self::generalCode('T',$userId);

    }



    public static function generateVirtualCode($length=12)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateSaleOrder($customerId)
    {

        return self::generalCode('S',$customerId);

    }

    public static function generatePurchaseOrder($supplierId)
    {

        return self::generalCode('P',$supplierId);

    }

    public static function generatePurchaseOrderV2($merchantId)
    {
        return self::generalCode('PO',$merchantId);

    }

    public static function returSaleOrder($merchantId)
    {
        //harusnya merchant
        return self::generalCode('RS',$merchantId);


    }

    public static function returPurchaseOrder($merchantId)
    {
        //harusnya merchant
        return self::generalCode('RP',$merchantId);
    }

    public static function redeemCode($merchantId)
    {

        return self::generalCode('RE',$merchantId);

    }

    public static function transferStock($saleOrderId)
    {
        $prefix='TS';
        $merchantId=$saleOrderId;

        return self::generalCode($prefix,$merchantId);

    }

    public static function generalCode($prefix,$merchantId)
    {
        DB::statement("CREATE SEQUENCE  if  not exists unique_".$prefix."_".$merchantId."_".date('Y')."_code_trans_id_seq");

        return DB::select("select '$prefix' || '$merchantId' || '".date('Y')."' || '".self::codeGenerator(3)."' || nextval('unique_".$prefix."_".$merchantId."_".date('Y')."_code_trans_id_seq')::text as code")[0]->code;


    }


    public static function generalCodeV2($prefix,$merchantId)
    {
        DB::statement("CREATE SEQUENCE  if  not exists unique_".$prefix."_".$merchantId."_code_trans_id_seq");

        return DB::select("select '$prefix' || '$merchantId' || lpad(nextval('unique_".$prefix."_".$merchantId."_code_trans_id_seq')::text , 10, '0') as code")[0]->code;

    }

    public static function generateUniqueProduct($userId)
    {
        return $userId.Uuid::uuid1()->toString();
    }

    public static function voucherDoorprize($type,$merchantId)
    {
        return 'V'.$type.$merchantId.self::generatePin(6);
    }

    public static function generateRefCode($type,$merchantId)
    {
        return $type.$merchantId.self::generatePin();
    }

    public static function generateJurnalCode($type,$initial,$merchantId,$increment=0)
    {
        if($type==0){
            $noUrut=Jurnal::where('md_merchant_id',$merchantId)
                    ->count()+1;
        }else{
            $noUrut=Jurnal::where('md_merchant_id',$merchantId)
                    ->where('trans_type',$type)
                    ->count()+1;
        }
        return $initial.sprintf("%07s", $noUrut+$increment);
    }

    public static function generateApCode($type,$initial,$merchantId,$increment=0)
    {

        return self::generalCode($initial,$merchantId);

    }

    public static function generateArCode($type,$initial,$merchantId,$increment=0)
    {

        return self::generalCode($initial,$merchantId);

    }

    public static function generateInitial($initial,$incremental)
    {
        return self::generalCode($initial,$incremental);

    }


    public static function otherCode($prefix,$merchantId,$year,$month,$date)
    {

        DB::statement("CREATE SEQUENCE  if  not exists unique_".$prefix."_".$merchantId."_code_trans_id_seq");

        return DB::select("select '$prefix' || '$merchantId'  || '$year' || '$month' || '$date'  || nextval('unique_".$prefix."_".$merchantId."_code_trans_id_seq')::text as code")[0]->code;

    }



}

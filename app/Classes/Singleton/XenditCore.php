<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Classes\Singleton;
use Illuminate\Support\Facades\Route;
use Xendit\Xendit;

class XenditCore
{
    private static $instance=null;

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new XenditCore();
        }

        return self::$instance;
    }

    public static function bankVA($code)
    {
        $array=[
            'BNI'=>8808,
            "BRI"=>26215,
            "MANDIRI"=>88608,
            "PERMATA"=>8214,
            "BCA"=>10766
        ];

        return $array[$code];
    }

    public static function bankCode()
    {
        $array=[
            "BNI"=>'BNI',
            "BRI"=>"BRI",
            "MANDIRI"=>"MANDIRI",
            "PERMATA"=>"PERMATA",
            "BCA"=>"BCA",
        ];

        return $array;
    }
    public static function createVirtualAccount($params)
    {
        try{
            Xendit::setApiKey(env('XENDIT_KEY'));
            return \Xendit\VirtualAccounts::create($params);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }
    }

    public static function updateVA($id,$params)
    {
        try{
            Xendit::setApiKey(env('XENDIT_KEY'));
            return \Xendit\VirtualAccounts::update($id,$params);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }
    }

    public static function createQRIS($params)
    {
        try{
            Xendit::setApiKey(env('XENDIT_KEY'));
            return \Xendit\QRCode::create($params);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
           
            return [
                'message'=>$e->getMessage()
            ];
        }
    }

    public static function createInvoiceWeb($params)
    {
        try{
            Xendit::setApiKey(env('XENDIT_KEY'));
            return \Xendit\Invoice::create($params);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return false;
        }
    }

    public static function createEWalletCharge($params)
    {
        try{
            Xendit::setApiKey(env('XENDIT_KEY'));
            return \Xendit\EWallets::createEWalletCharge($params);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return [
                'message'=>$e->getMessage()
            ];
        }
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Classes;

class RouteConfig
{
    public static function getRouteFromController($path, $method = 'routeWeb') {
        try{
            $path = app_path() . $path;
            $allFiles = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($path)
            );
            $ctrlFiles = new \RegexIterator($allFiles, '/\.php$/');
            foreach ($ctrlFiles as $pathname => $fileinfo) {
                if (!$fileinfo->isFile()) continue;
                $str = '';
                if (substr($pathname, 0, strlen(app_path())) == app_path()) {
                    $str = substr($pathname, strlen(app_path()));
                }
                if (substr($str, -strlen('.php')) === '.php') {
                    $str = substr($str, 0, -strlen('.php'));
                }
                $str = 'App' . str_replace('/', '\\', $str);

                try {
                    $r = new \ReflectionClass($str);
                    $instance = $r->newInstanceWithoutConstructor();
                    if (method_exists($instance, $method)) {
                        $instance->{$method}();
                    }
                } catch (\Exception $e) {
                    throw $e;
                }
            }
        }catch (\Exception $e)
        {
            throw $e;
        }
    }

    public static function getRouteFromModuleController($module,$path, $method = 'routeWeb') {

        try {
            $path ='Modules/'.$module.$path;
            //dd($path);
            $allFiles = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($path)
            );
            $ctrlFiles = new \RegexIterator($allFiles, '/\.php$/');
            //dd($ctrlFiles);
            foreach ($ctrlFiles as $pathname => $fileinfo) {
                if (!$fileinfo->isFile()) continue;
                $str = '';
                if (substr($pathname, -strlen('.php')) === '.php') {
                    $str = substr($pathname, 0, -strlen('.php'));
                };
                $str=str_replace(str_replace('app/Classes','',dirname(__FILE__)),'',$str);
                $str = str_replace('/', '\\', $str);
                try {
                    $r = new \ReflectionClass($str);
                    $instance = $r->newInstanceWithoutConstructor();
                    if (method_exists($instance, $method)) {
                        $instance->{$method}();
                    }
                } catch (\Exception $e) {
                    throw $e;
                }
            }
        }catch (\Exception $e)
        {
            throw  $e;
        }

    }

}

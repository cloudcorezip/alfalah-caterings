<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Classes;


use Carbon\Carbon;

class RangeCriteria
{
    protected $carbon;

    public function __construct()
    {
        $this->carbon=new Carbon();
    }

    public  function rangeFilter($periodFilter)
    {
        switch (strtolower($periodFilter)) {
            case 'today':
                return $this->carbon->today();
            case 'yesterday':
                return $this->carbon->yesterday();
            case 'thisweek':
                return [
                    $this->carbon->today()->startOfWeek(),
                    $this->carbon->today()->endOfWeek()
                ];
            case 'lastweek':
                return [
                    ($this->carbon->today()->dayOfWeek === 0) ?
                        $this->carbon->today()->previous(0) :
                        $this->carbon->today()->previous(0)->previous(),
                    $this->carbon->today()->previous(6)->addDay()
                ];
            case 'thismonth':
                return [
                    $this->carbon->today()->startOfMonth(),
                    $this->carbon->today()->endOfMonth()
                ];
            case 'lastmonth':
                return [
                    $this->carbon->today()->subMonth()->startOfMonth(),
                    $this->carbon->today()->subMonth()->endOfMonth()
                ];
            case 'lastthreemonth':
                return [
                    $this->carbon->today()->subMonths(3)->startOfMonth(),
                    $this->carbon->today()
                ];
            case 'lastsixmonth':
                return [
                    $this->carbon->today()->subMonths(6)->startOfMonth(),
                    $this->carbon->today()
                ];
            case 'lastninemonth':
                return [
                    $this->carbon->today()->subMonths(9)->startOfMonth(),
                    $this->carbon->today()
                ];
            case 'thisyear':
                return date('Y');
            case 'lastyear':
                return date('Y')-1;
        }
    }


    public  function rangeFilterDashboard($periodFilter)
    {
        switch (strtolower($periodFilter)) {
            case 'today':
                return [$this->carbon->today()->toDateString(),$this->carbon->today()->toDateString()];
            case 'yesterday':
                return [$this->carbon->yesterday()->toDateString(),$this->carbon->yesterday()->toDateString()];
            case 'thisweek':
                return [
                    $this->carbon->today()->startOfWeek()->toDateString(),
                    $this->carbon->today()->endOfWeek()->toDateString()
                ];
            case 'lastweek':
                return [
                    ($this->carbon->today()->dayOfWeek === 0) ?
                        $this->carbon->today()->previous(0)->toDateString() :
                        $this->carbon->today()->previous(0)->previous()->toDateString(),
                    $this->carbon->today()->previous(6)->addDay()->toDateString()
                ];
            case 'thismonth':
                return [
                    $this->carbon->today()->startOfMonth()->toDateString(),
                    $this->carbon->today()->endOfMonth()->toDateString()
                ];
            case 'lastmonth':
                return [
                    $this->carbon->today()->subMonth()->startOfMonth()->toDateString(),
                    $this->carbon->today()->subMonth()->endOfMonth()->toDateString()
                ];
            case 'lastthreemonth':
                return [
                    $this->carbon->today()->subMonths(3)->startOfMonth()->toDateString(),
                    $this->carbon->today()->toDateString()
                ];
            case 'lastfourmonth':
                return [
                    $this->carbon->today()->subMonths(4)->startOfMonth()->toDateString(),
                    $this->carbon->today()->toDateString()
                ];
            case 'lastsixmonth':
                return [
                    $this->carbon->today()->subMonths(6)->startOfMonth()->toDateString(),
                    $this->carbon->today()->toDateString()
                ];
            case 'lasteightmonth':
                return [
                    $this->carbon->today()->subMonths(8)->startOfMonth()->toDateString(),
                    $this->carbon->today()->toDateString()
                ];
            case 'lastninemonth':
                return [
                    $this->carbon->today()->subMonths(9)->startOfMonth()->toDateString(),
                    $this->carbon->today()->toDateString()
                ];
            case 'thisyear':
                return [$this->carbon->today()->startOfYear()->toDateString(),
                    $this->carbon->today()->endOfYear()->toDateString()
                    ];
            case 'lastyear':
                return [$this->carbon->today()->subYear()->startOfYear()->toDateString(),
                    $this->carbon->today()->subYear()->endOfYear()->toDateString()
                ];
        }
    }


    public  function rangeFilterPreviousDashboard($periodFilter,$startDate='')
    {
        switch (strtolower($periodFilter)) {
              case 'today':
                return [$this->carbon->yesterday()->toDateString(),
                    $this->carbon->yesterday()->toDateString(),
                    'yesterday'
                ];
            case 'thisweek':
                return [
                    ($this->carbon->today()->dayOfWeek === 0) ?
                        $this->carbon->today()->previous(0)->toDateString() :
                        $this->carbon->today()->previous(0)->previous()->toDateString(),
                    $this->carbon->today()->previous(6)->addDay()->toDateString(),
                    'lastweek'
                ];
            case 'thismonth':
                return [
                    $this->carbon->today()->subMonth()->startOfMonth()->toDateString(),
                    $this->carbon->today()->subMonth()->endOfMonth()->toDateString(),
                    'lastmonth'
                ];
            case 'lastthreemonth':
                return [
                    Carbon::parse($startDate)->subMonths(3)->startOfMonth()->toDateString(),
                    Carbon::parse($startDate)->subMonth()->endOfMonth()->toDateString(),
                    ''
                ];
            case 'lastfourmonth':
                return [
                    Carbon::parse($startDate)->subMonths(4)->startOfMonth()->toDateString(),
                    Carbon::parse($startDate)->subMonth()->endOfMonth()->toDateString(),
                    ''
                ];
            case 'lastsixmonth':
                return [
                    Carbon::parse($startDate)->subMonths(6)->startOfMonth()->toDateString(),
                    Carbon::parse($startDate)->subMonth()->endOfMonth()->toDateString(),
                    ''
                ];
            case 'lasteightmonth':
                return [
                    Carbon::parse($startDate)->subMonths(8)->startOfMonth()->toDateString(),
                    Carbon::parse($startDate)->subMonth()->endOfMonth()->toDateString(),
                    ''
                ];
            case 'lastninemonth':
                return [
                    Carbon::parse($startDate)->subMonths(9)->startOfMonth()->toDateString(),
                    Carbon::parse($startDate)->subMonth()->endOfMonth()->toDateString(),
                    ''
                ];
            case 'thisyear':
                return [$this->carbon->today()->subYear()->startOfYear()->toDateString(),
                    $this->carbon->today()->subYear()->endOfYear()->toDateString(),
                    ''
                ];
        }
    }



    public function getListRange()
    {
        $array=$this->listRange();
        $result=[];
        foreach ($array as $key =>$item){
            $result[]=[
                'id'=>$key,
                'text'=>$item
            ];
        }
        return $result;
    }

    public function findRangeName($key,$previous=0)
    {
        if($previous==1){
            if($key=='7-month-lastthreemonth'){
                return '3 Bulan Sebelumnya';
            }elseif ($key=='12-month-lastfourmonth'){
                return '4 Bulan Sebelumnya';
            }elseif ($key=='8-month-lastsixmonth'){
                return '6 Bulan Sebelumnya';
            }elseif ($key=='13-month-lasteightmonth'){
                return '8 Bulan Sebelumnya';
            }elseif ($key=='9-month-lastninemonth'){
                return '9 Bulan Sebelumnya';
            }elseif ($key=='1-day-today'){
                return 'Kemarin';
            } else{
                return str_replace('Ini','lalu',$this->listRange()[$key]);
            }
        }else{
            return $this->listRange()[$key];

        }

    }

    private function listRange()
    {
        $array=[
            '1-day-today'=> 'Hari Ini',
            '2-day-yesterday'=> 'Kemarin',
            '3-day-thisweek'=> 'Minggu Ini',
            '4-day-lastweek'=> 'Minggu Lalu',
            '5-month-thismonth' => 'Bulan Ini',
            '6-month-lastmonth' => 'Bulan Lalu',
            '7-month-lastthreemonth' => '3 Bulan Terakhir',
            '12-month-lastfourmonth' => '4 Bulan Terakhir',
            '8-month-lastsixmonth' => '6 Bulan Terakhir',
            '13-month-lasteightmonth' => '8 Bulan Terakhir',
            '9-month-lastninemonth' => '9 Bulan Terakhir',
            '10-month-thisyear' => 'Tahun Ini',
            '11-month-lastyear' => 'Tahun Lalu',
        ];
        return $array;
    }
}

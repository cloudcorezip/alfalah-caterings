<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailForSuccessfulPlugin extends Mailable
{
    use Queueable, SerializesModels;
    protected  $email;
    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$data)
    {
        $this->email=$email;
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params=[
            'email'=>$this->email,
            'data'=>$this->data
        ];
        return $this->
            subject('Pembelian Plugin')->
            view('emails.plugin.successful-plugin',$params);

    }
}

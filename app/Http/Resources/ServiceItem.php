<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id'=>$this->id,
            'name'=>$this->name,
            'banner'=>$this->banner,
            'description'=>$this->description,
            'category_id'=>$this->getCategory->id,
            'category_name'=>$this->getCategory->name,
            'user_id_created'=>$this->getUser->id,
            'user_id_name'=>$this->getUser->fullname,
            'is_with_offer'=>$this->is_with_offer,
            'is_deleted'=>$this->is_deleted,
            'is_published'=>$this->is_published,
            'detail'=>is_null($this->getDetail)?[]:DetailServiceItem::collection($this->getDetail),
            'merchant_name'=>is_null($this->getUser)?'':$this->getUser->getMerchant->name
        ];
    }
}

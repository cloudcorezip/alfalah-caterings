<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailServiceItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'detail_name'=>$this->detail_name,
            'note'=>$this->note,
            'price'=>$this->price,
            'quantity_per_unit'=>$this->quantity_per_unit,
            'unit_id'=>$this->getUnit->id,
            'unit_name'=>$this->getUnit->name,
            'description'=>$this->description,
            'service_name'=>(is_null($this->getService))?'':$this->getService->name,
            'category_id'=>(is_null($this->getService))?'':$this->getService->md_ss_category_service_id,
            'category_name'=>(is_null($this->getService))?'':$this->getService->getCategory->name,
            'banner_service'=>$this->banner,
            'merchant_name'=>(is_null($this->getService))?'':$this->getService->getUser->getMerchant->name,
            'merchant_id'=>(is_null($this->getService))?'':$this->getService->getUser->getMerchant->id,
            'rating'=>0,
            'is_archived'=>$this->is_archived,
            'price_markup'=>$this->price_markup,
            'is_show_senna_app'=>$this->is_show_senna_app,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at
        ];
    }
}

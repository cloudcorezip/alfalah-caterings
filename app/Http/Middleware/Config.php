<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Middleware;


use App\Models\Accounting\CoaCashflowFormat;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaMapping;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantMenu;
use App\Utils\SFA\SfaInitUtil;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use App\Utils\Accounting\InitUtil;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Modules\Merchant\Entities\Acc\CashflowFormatEntity;
use Modules\Merchant\Http\Controllers\Acc\Asset\AssetController;
use Modules\Merchant\Http\Controllers\HR\HolidayController;
use Modules\Merchant\Http\Controllers\HR\LeaveSettingController;
use phpDocumentor\Reflection\Types\Self_;

class Config
{

    public static function ruleLogin($role=[],$request,$next)
    {
        try{
            $cookieActiveUser=decrypt(Session::get(config('user.user')));
            $cookieRole=decrypt(Session::get(config('user.role')));

            if (is_null($cookieRole) && is_null($cookieActiveUser)) {
                return redirect()->route('login');
            } else {
                if(!is_null(merchant_id()))
                {
                    self::_generateMenu(merchant_id());
                    InitUtil::initCoaToMerchantDetailUtil(merchant_id());
                    InitUtil::generateNewCoa(merchant_id());
                    AssetController::monthlyGenerate(merchant_id(),user_id());
                    HolidayController::getHoliday(date('Y'),merchant_id(),user_id());
                    LeaveSettingController::generateInitialLeave(merchant_id(),user_id());
                    SfaInitUtil::init(merchant_id());
                    $token=Session::get('merchant_data'.merchant_id());

                    $checkDoubleAktiva=DB::select("
                        select count(*) as total from acc_coa_categories
                        where md_merchant_id=".merchant_id()." and code='1.1.07'
                        ");

                    if(!empty($checkDoubleAktiva))
                    {
                        DB::statement("update acc_coa_categories set is_deleted=1 where md_merchant_id=".merchant_id()." and code='1.1.07'");
                    }

                    if(is_null($token))
                    {
                        $merchant=Merchant::select([
                            'start_subscription',
                            'end_subscription'
                        ])->where('id',merchant_id())->first();
                        Session::put('merchant_data'.merchant_id(),json_encode($merchant));
                    }else{
                        $merchant=json_decode($token);
                    }

                    if(!is_null($merchant))
                    {
                         return $next($request);
                    }
                }else{
                      return $next($request);

                }

            }
        }catch (\Exception $e)
        {
            return redirect()->route('login');
        }

    }

    private static function _generateMenu($merchantId)
    {
        $session=Session::get(env('APP_MENU_VERSION').'_'.$merchantId);
        if(is_null($session))
        {
            if(user()->is_staff==0){
                $menu=MerchantMenu::whereNull('parent_id')
                    ->with(['getChild'=> function ($query){
                        $query->where('is_active',1);
                    }])
                    ->where('is_active',1)
                    ->orderBy('id','ASC')
                    ->get();

                Session::put(env('APP_MENU_VERSION').'_'.$merchantId,$menu);

            }else{
                $menu=MerchantMenu::
                select([
                    'md_merchant_menus.*'
                ])
                    ->whereNull('md_merchant_menus.parent_id')
                    ->with(['getChild'=> function ($query)use($merchantId) {
                        $query
                            -> select([
                                'md_merchant_menus.*'
                            ])
                            ->with(['getChild'=> function ($query)use($merchantId) {
                                $query
                                    -> select([
                                        'md_merchant_menus.*'
                                    ])
                                    ->join('md_merchant_menu_staffs as s','s.menu_merchant_id','md_merchant_menus.id')
                                    ->where('md_merchant_menus.is_active',1)
                                    ->where('s.md_merchant_id',$merchantId)
                                    ->where('s.staff_user_id',user()->id);
                            }])
                            ->join('md_merchant_menu_staffs as s','s.menu_merchant_id','md_merchant_menus.id')
                            ->where('md_merchant_menus.is_active',1)
                            ->where('s.md_merchant_id',$merchantId)
                            ->where('s.staff_user_id',user()->id);
                    }])
                    ->join('md_merchant_menu_staffs as ss','ss.menu_merchant_id','md_merchant_menus.id')
                    ->where('ss.md_merchant_id',$merchantId)
                    ->where('ss.staff_user_id',user()->id)
                    ->where('md_merchant_menus.is_active',1)
                    ->orderBy('md_merchant_menus.id','ASC')
                    ->get();

                Session::put(env('APP_MENU_VERSION').'_'.$merchantId,$menu);
            }
        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Middleware;

use App\Models\MasterData\Merchant;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class SubscriptionWebVerification
{
    public function handle(\Illuminate\Http\Request $request, \Closure $next)
    {
        try{

            $cookieActiveUser=decrypt(Session::get(config('user.user')));
            $cookieRole=decrypt(Session::get(config('user.role')));

            if (is_null($cookieRole) && is_null($cookieActiveUser)) {
                return redirect()->route('login');
            } else {
                if(!is_null(merchant_id()))
                {
                    $token=Session::get('merchant_data'.merchant_id());
                    if(is_null($token))
                    {
                        $merchant=Merchant::select([
                            'start_subscription',
                            'end_subscription'
                        ])->where('id',merchant_id())->first();
                        Session::put('merchant_data'.merchant_id(),json_encode($merchant));
                    }else{
                        $merchant=json_decode($token);
                    }
                    if(!is_null($merchant))
                    {
                        return $next($request);
                    }
                }else{
                    return redirect()->route('login');

                }
            }
        }catch (\Exception $e)
        {
            return redirect()->route('login');
        }

    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Middleware;


use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class CsVerification
{
    public function handle(\Illuminate\Http\Request $request, \Closure $next)
    {

        return Config::ruleLogin([8],$request,$next);

    }
}

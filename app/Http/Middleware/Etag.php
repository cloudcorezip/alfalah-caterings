<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Middleware;


use Illuminate\Http\Request;

class Etag
{
    public function handle(Request $request, \Closure $next)
    {
        $response = $next($request);

        if ($request->isMethod('get')) {
            $etag = sha1($response->getContent());

            $requestEtag = str_replace('W/"', '', $request->getETags());
            $requestEtag = str_replace('"', '', $requestEtag);

            if ($requestEtag && $requestEtag[0] == $etag) {
                $response->setNotModified();
            }
            $response->setEtag($etag, true);
        }

        return $response;
    }

}

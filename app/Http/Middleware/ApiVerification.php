<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Middleware;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\User;
use App\Utils\Accounting\InitUtil;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;
use Modules\Merchant\Http\Controllers\Acc\Asset\AssetController;

class ApiVerification
{
    public function handle(\Illuminate\Http\Request $request, \Closure $next)
    {
        try {

            $token = $request->header('senna-auth');
            if (is_null($token)) {
                return Message::getJsonResponse(404, 'Parameter Senna-auth tidak ditemukan', []);
            }
            $redisKey=Redis::get($token);
            if(!is_null($redisKey))
            {
                Artisan::call('accmap:build', ['--merchant' =>$redisKey]);
                Controller::generateWarehouseById($redisKey);
                InitUtil::initCoaToMerchantDetailUtilMobile($redisKey);
                InitUtil::generateNewCoaMobile($redisKey);
                return $next($request);
            }else{
                $userCheck = User::select('id','token')
                    ->where(['token' => $token])->first();
                if (is_null($userCheck)) {
                    return Message::getJsonResponse(404, 'Authentifikasi tidak ditemukan', []);
                } else {

                    if(!is_null($userCheck->getMerchant)){
                        Artisan::call('acc:build', ['--merchant' =>$userCheck->getMerchant->id]);
                        AssetController::monthlyGenerate($userCheck->getMerchant->id,$userCheck->id);
                        Controller::generateWarehouseById($userCheck->id);
                        InitUtil::initCoaToMerchantDetailUtilMobile($redisKey);
                        InitUtil::generateNewCoaMobile($redisKey);
                    }
                    Redis::set($token,$userCheck->id);
                    Redis::expire($token, ( 24*3600));
                    return $next($request);
                }
            }
        } catch (\Exception $e) {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return Message::getJsonResponse(500, 'Terjadi kesalahan pada saat authentifikasi', []);
        }
    }
}

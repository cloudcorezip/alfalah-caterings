<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Middleware;


use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class MerchantVerification
{
    public function handle(\Illuminate\Http\Request $request, \Closure $next)
    {
        return Config::ruleLogin([2,3,4,11,12,13,14,15,16,17,18,19,20],$request,$next);

    }

}

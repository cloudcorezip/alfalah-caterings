<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Frontend;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Classes\Singleton\WhatsappBroadcast;
use App\Classes\Singleton\XenditCore;
use App\Http\Controllers\Controller;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\MasterData\TransactionType;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\PaymentMethod;
use App\Models\SennaToko\Product;
// use App\Models\SennaToko\DiscountProduct;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrderDetail;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\ReturSaleOrderDetail;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\StockSaleMapping;
use App\Models\SennaToko\Supplier;
use App\Models\SennaToko\TempPurchaseOrderDetail;
use App\Models\SennaToko\TempSaleOrderDetail;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaPurchaseUtil;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\ExpeditionUtil;
use App\Utils\Inventory\InventoryUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\Merchant\ProductUtil;
use App\Utils\PaymentUtils;
use App\Utils\ThirdParty\VirtualAccountUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\WarehouseEntity;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Image;
use Xendit\Xendit;
use QrCode;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;
use Modules\Merchant\Entities\Toko\CustLevelEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Illuminate\Support\Facades\Crypt;
use App\Models\MasterData\Bank;


class FormOrderController extends Controller
{

    protected $sale;
    protected $message;
    protected $product;
    protected $ar;
    protected $xendit;
    protected $virtualAccount;
    protected $waBroadcast;


    public function __construct()
    {
        $this->sale=SaleOrder::class;
        $this->message=Message::getInstance();
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->xendit=XenditCore::getInstance();
        $this->virtualAccount=new VirtualAccountUtil();
        $this->waBroadcast=WhatsappBroadcast::getInstance();


    }

    public function routeWeb()
    {
        Route::prefix('/form-order')
            ->group(function (){
                Route::get('/{param}', 'Frontend\FormOrderController@index')
                    ->name('form-order.index');
                Route::get('/detail/{id}', 'Frontend\FormOrderController@detail')
                    ->name('form-order.detail');
                Route::post('/save', 'Frontend\FormOrderController@save')
                    ->name('form-order.save');
                Route::post('/add-customer', 'Frontend\FormOrderController@addCustomer')
                    ->name('form-order.add-customer');
                Route::post('/save-customer', 'Frontend\FormOrderController@saveCustomer')
                    ->name('form-order.save-customer');
                Route::post('/get-city', 'Frontend\FormOrderController@getCity')
                    ->name('form-order.get-city');

                Route::get('/trans/{id}', 'Frontend\FormOrderController@trans')
                    ->name('form-order.trans');
            });
    }

    public function index(Request $request){

        $formOrderUrl=$request->param; // form order url
        $merchant = Merchant::where("form_order_url", $formOrderUrl)->first();
        if(is_null($merchant)){
            return abort(404);
        }
        $merchantId = $merchant->id;
        $userId=$merchant->md_user_id;
        $code=CodeGenerator::generateSaleOrder($merchantId);
        $coaInv=CoaPurchaseUtil::coaInvPurchaseOrder($merchantId);
        $data = new SaleOrder();
        $coaJson=CoaSaleUtil::getCoaSaleOrder($merchantId);
        $warehouse=parent::getGlobalWarehouse($merchantId);
        $title = "Form Penjualan";
        $params=[
            'title'=>$title,
            'code'=>$code,
            'data'=>$data,
            'customer'=>Customer::where('md_user_id',$userId)->where('is_deleted',0)->get(),
            'transType'=>PaymentUtils::getPayment($merchantId),
            'product'=>Product::where('md_user_id',$userId)->where('is_deleted',0)->get(),
            'shippingMethod'=>ExpeditionUtil::getList($merchantId),
            'inventory_id'=>(is_null($merchant->md_inventory_method_id))?null:
                $merchant->md_inventory_method_id,
            'coa_json'=>json_encode($coaJson),
            'warehouse'=>json_encode($warehouse),
            'coa_inv'=>collect($coaInv),
            'userId'=> $userId,
            'token'=>$merchant->getUser->token,
            'param'=> $request->param,
            'merchantId'=>$merchantId
        ];

        return view('form-order.index', $params);

    }

    public function detail(Request $request)
    {
        try{
            $id = $request->id;
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')
                ->where('id',$id)->first();

            $merchant = Merchant::find($data->md_merchant_id);

            $page = (is_null($request->page))? "Transaksi": $request->page;
            $coaJson=CoaSaleUtil::getCoaSaleOrder($data->md_merchant_id);
            $bank = Bank::all();
            $params=[
                'title'=>'Detail Penjualan | '.$data->code,
                'data'=>$data,
                'promo_product'=>0,
                'page'=>$page,
                'coaJson'=>json_encode($coaJson),
                'bank' => $bank,
                'param' => $merchant->form_order_url

            ];
            return view('form-order.detail',$params);
        }catch (\Exception $e)
        {
            return abort(404);
        }
    }

    public function trans(Request $request)
    {
        try{
            $id = $request->id;
            $data=SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')
                ->where('id',$id)->first();

            $merchant = Merchant::find($data->md_merchant_id);

            $page = (is_null($request->page))? "Transaksi": $request->page;
            $coaJson=CoaSaleUtil::getCoaSaleOrder($data->md_merchant_id);
            $bank = Bank::all();
            $params=[
                'title'=>'Detail Penjualan | '.$data->code,
                'data'=>$data,
                'promo_product'=>0,
                'page'=>$page,
                'coaJson'=>json_encode($coaJson),
                'bank' => $bank,
                'param' => $merchant->form_order_url

            ];
            return view('form-order.detail-transaksi',$params);
        }catch (\Exception $e)
        {
            return abort(404);
        }
    }

    public function save(Request $request)
    {
        try {
            $formOrderUrl = $request->param; // form order url
            $merchant = Merchant::where('form_order_url', $formOrderUrl)->first();
            $merchantId = $merchant->id;
            $userId = $merchant->md_user_id;
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            $timestamp=$request->created_at;
            $date = new \DateTime($request->created_at);

            if($request->sc_customer_id=='-1')
            {
                return response()->json([
                    'message' => "Silahkan pilih pelanggan terlebih dahulu !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(AccUtil::checkClosingJournal($merchantId,$date->format('Y-m-d'))==true)
            {
                return response()->json([
                    'message' => "Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan kembali dibuka pada tautan berikut : <a href='".\route('merchant.toko.acc.journal.closing.index')."'><b>Pencatatan Jurnal Penutup</b></a>",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            if($request->is_debet==0)
            {
                if ($request->md_transaction_type_id == '-1') {
                    return response()->json([
                        'message' => 'Metode Pembayaran belum dipilih !',
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }else{
                if($request->due_date<$request->created_at){
                    return response()->json([
                        'message' => "Tanggal jatuh tempo tidak boleh kurang dari tanggal transaksi !",
                        'type' => 'warning',
                        'is_modal' => false,
                        'redirect_url' => ''
                    ]);
                }
            }

            if ($request->md_sc_shipping_category_id == '-1') {
                return response()->json([
                    'message' => 'Metode Pengiriman belum dipilih !',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }
            $warehouse=json_decode($request->warehouse);
            $coaJson=collect(json_decode($request->coa_json));
            $inventoryId=$request->inventory_id;

            $trans = null;
            $is_with_load_shipping=0;
            $status=TransactionStatus::UNPAID;
            $adminFee=0;
            $total=$request->total;
            $shipping_cost = 0;

            $purchaseId=SaleOrder::insertGetId([
                'sc_customer_id'=>($request->sc_customer_id!='-1')?$request->sc_customer_id:null,
                'code'=>$request->code,
                'step_type'=>$request->step_type,
                'is_with_load_shipping'=>$is_with_load_shipping,
                'coa_trans_id'=>(!is_null($trans))?$trans[1]:null,
                'md_transaction_type_id'=>(!is_null($trans))?$trans[0]:null,
                'qr_code'=>'-',
                'is_debet'=>$request->is_debet,
                'total'=>$total-$adminFee,
                'md_merchant_id'=>$merchantId,
                'md_sc_transaction_status_id'=>$status,
                'md_sc_shipping_category_id'=>$request->md_sc_shipping_category_id,
                'tax'=>$request->tax,
                'promo'=>$request->discount,
                'promo_percentage'=>$request->discount_percentage,
                'tax_percentage'=>$request->tax_percentage,
                'shipping_cost'=>$shipping_cost,
                'timezone'=>$request->timezone,
                'note_order'=>$request->note,
                'created_by'=>$userId,
                'is_approved_shop'=>1,
                'created_at'=>$timestamp,
                'updated_at'=>$timestamp,
                'admin_fee'=>$adminFee,
                'is_from_form_order' => 1
            ]);

            $sc_product_id = $request->sc_product_id;
            $prices = $request->selling_price;
            $quantity = $request->quantity;
            $coaInvId=$request->coa_inv_id;
            $coHppId=$request->coa_hpp_id;
            $details = [];
            $stockInOut=[];
            if (empty($sc_product_id)) {
                return response()->json([
                    'message' => 'Data item penjualan tidak boleh kosong !',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            $products=$this->product::whereIn('id',$sc_product_id)->get();
            $productCalculate=[];
            $updateStockProduct="";
            foreach ($products as $key => $product) {
                if($product->is_with_stock==1)
                {
                    if($product->stock<$quantity[$key])
                    {
                        return response()->json([
                            'message' => "Stok produk $product->name tidak mencukupi",
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }

                    array_push($productCalculate,[
                        'id'=>$product->id,
                        'amount'=>$quantity[$key],
                    ]);

                    if ($warehouse->is_default == 1) {
                        $stockP=$product->stock-$quantity[$key];
                        $updateStockProduct.=($key==0)?"(".$product->id.",".$stockP.")":",(".$product->id.",".$stockP.")";
                    }else{
                        $stockP=$product->stock;
                    }
                    $stockInOut[]=[
                        'sync_id' => $product->id . Uuid::uuid4()->toString(),
                        'sc_product_id' => $product->id,
                        'total' => $quantity[$key],
                        'inv_warehouse_id' => $warehouse->id,
                        'record_stock' => $stockP,
                        'created_by' => $userId,
                        'selling_price' => $product->selling_price,
                        'purchase_price' => $prices[$key],
                        'residual_stock' => $quantity[$key],
                        'type' => StockInventory::OUT,
                        'transaction_action' => 'Pengurangan Stok ' . $product->name . ' Dari Penjualan Dengan Code ' . $request->code,
                        'stockable_type'=>'App\Models\SennaToko\SaleOrder',
                        'stockable_id'=>$purchaseId,
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp

                    ];

                }

                $details[]=[
                    'sc_product_id'=>$sc_product_id[$key],
                    'quantity'=>$quantity[$key],
                    'is_bonus'=>0,
                    'sub_total'=>$quantity[$key]*($prices[$key]),
                    'sc_sale_order_id'=>$purchaseId,
                    'profit'=>0,
                    'price'=>$prices[$key],
                    'coa_inv_id'=>$coaInvId[$key],
                    'coa_hpp_id'=>$coHppId[$key],
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ];
            }

            $arrayOfSession=[
                'productCalculate'=>$productCalculate,
                'updateStockProduct'=>$updateStockProduct,
                'inv_id'=>$inventoryId,
                'warehouse_id'=>$warehouse->id,
                'user_id'=>$userId,
                'merchant_id'=>$merchantId,
                'coaJson'=>$coaJson,
                'timestamp'=>$timestamp,
                'is_debet'=>$request->is_debet,
                'stock_in_out'=>$stockInOut,
                'is_with_load_shipping'=>$is_with_load_shipping,
                'total'=>$total-$adminFee,
                'paid_nominal'=>$request->paid_nominal,
                'shipping_cost'=>$shipping_cost,
                'due_date'=>$request->due_date,
                'timezone'=>$request->timezone,
                'admin_fee'=>$adminFee
            ];

            DB::table('sc_sale_order_details')->insert($details);

            $updateSale=SaleOrder::find($purchaseId);
            $updateSale->json_order_online=json_encode($arrayOfSession);
            $updateSale->is_payment_gateway=1;
            $updateSale->save();
            $stringProduct="";
            $stringProduct.='%0A*No*  |    *Item Transaksi*    |  *Quantity*  |  *Harga*  |  *Sub Total*%0A';
            foreach ($updateSale->getDetail as $key => $item){
                $unitName=is_null($item->getProduct->getUnit)?'':$item->getProduct->getUnit->name;
                $stringProduct.='%0A*'.($key+1).'.*  |    '.$item->getProduct->name.'    |  '.$item->quantity.' '.$unitName.'  |  '.rupiah($item->price).'  |  *'.rupiah($item->sub_total).'*%0A';
            }

            $customerData=[
                'name'=>is_null($updateSale->getCustomer)?'-':$updateSale->getCustomer->name,
                'phone_number'=>is_null($updateSale->getCustomer)?'-':$updateSale->getCustomer->phone_number,
                'address'=>is_null($updateSale->getCustomer)?'-':$updateSale->getCustomer->address
            ];

            $broadcast=$this->waBroadcast->send([
                'number'=>$updateSale->getMerchant->phone_merchant,
                'message'=>'Hai *'.$customerData['name'].'*,%0ABerikut invoice untuk transaksi nomor *'.$updateSale->code.'*: %0ANama Pelanggan : '.$customerData["name"].'%0ANo.Telp : '.$customerData["phone_number"].'%0AAlamat: '.$customerData["address"].'%0A'.$stringProduct.'%0A*Total* *'.rupiah($updateSale->total).'*%0A%0ADetail Pesanan : '.route('form-order.detail', ['id' => $purchaseId]).'%0A%0ADetail Pesanan Untuk Pelanggan : '.route('form-order.trans', ['id' => $purchaseId]).' %0ALink Pembayaran : '.route('checkout', ['key' => $updateSale->id]).'%0A%0ATerima Kasih'
            ]);

            if($broadcast->status==false){
                DB::rollBack();
                return response()->json([
                    'message' => 'Terjadi kesalahan saat mengirimkan invoice melalui WA',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''

                ]);
            }

            DB::commit();
            return response()->json([
                'message' => 'Data berhasil disimpan!',
                'type' => 'success',
                'is_modal' => false,
                'redirect_url' => route('form-order.detail', ['id' => $purchaseId])

            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message' => 'Terjadi kesalahan di server,data gagal disimpan !',
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function checkout(Request  $request)
    {
        $key=decrypt($request->key);
        $saleOrder=SaleOrder::find($key);

        $data = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.md_merchant_id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'b.name',
            'b.icon',
            'vb.code as bank_code',
            't.icon as icon_wallet',
            't.name as wallet_name'
        ])
            ->leftJoin('md_va_banks as vb', 'vb.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->leftJoin('md_banks as b', 'b.id', 'vb.md_bank_id')
            ->leftJoin('md_transaction_types as t','t.id','md_merchant_payment_methods.transaction_type_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.id', $saleOrder->md_merchant_payment_method_id)
            ->where('md_merchant_payment_methods.md_merchant_id',$saleOrder->md_merchant_id)
            ->first();

        date_default_timezone_set($saleOrder->timezone);

        if(date('Y-m-d H:i:s')>$saleOrder->expired_time){
           return Redirect::route('merchant.toko.transaction.sale-order-ar.checkout-expired',[
                'type'=>'order',
                'id'=>encrypt($saleOrder->id)
            ]);
        }

        $params = [
            "title" => "Checkout Pembayaran - ".$saleOrder->code,
            "data" => $data,
            "merchant" => $saleOrder->getMerchant,
            "saleOrder" => $saleOrder,
            "expireDate" => $saleOrder->expired_time,
            'va'=>json_decode($saleOrder->checkout_response)
        ];

        return view('merchant::toko.transaction.sale-order.checkout-detail', $params);


    }

    public function checkoutExpired(Request  $request){

        $type=$request->type;
        $id=decrypt($request->id);

        if($type=='order'){
            $data=SaleOrder::find($id);
            $code=$data->code;
        }else{
            $data=MerchantArDetail::find($id);
            $code=$data->ar_detail_code;
        }

        $params=[
            'title'=>'Pilih Metode Pembayaran Transaksi '.$code,
            'data'=>$data,
            'paymentOption'=>PaymentUtils::getPayment(merchant_id()),
            'type'=>$type
        ];
        return view('merchant::toko.transaction.sale-order.checkout-expired', $params);

    }

    public function addCustomer(Request $request)
    {
        $param = $request->param; // merchant id
        $isFromSale=is_null($request->is_sale)?0:1;
        $merchant = Merchant::find($param);
        $userId = $merchant->md_user_id;

        $data=new CustomerEntity();

        $params=[
            'title'=>'Tambah Pelanggan Baru',
            'data'=>$data,
            'level'=>CustLevelEntity::where('md_user_id', $userId)
                ->where('is_deleted', 0)
                ->get(),
            'provinces' => Province::all(),
            'isFromSale'=>$isFromSale,
            'userId' => $userId
        ];


        return view('form-order.modal-customer',$params);
    }

    public function saveCustomer(Request $request)
    {
        try {
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);

            $customer_name = $request->name;
            $email = $request->email;
            $phone_number = $request->phone_number;

            if(is_null($customer_name)){
                return response()->json([
                    'message' => "Nama tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(is_null($email)){
                return response()->json([
                    'message' => "Email tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            if(is_null($phone_number)){
                return response()->json([
                    'message' => "No Handphone tidak boleh kosong !",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $customer = CustomerEntity::where("email", $email)
                            ->where("phone_number", $phone_number)
                            ->where("md_user_id", $request->user_id)
                            ->where("is_deleted", 0)
                            ->first();

            if(!is_null($customer)){
                return response()->json([
                    'message' => "Pelanggan sudah ada",
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);
            }

            $data=new CustomerEntity();

            $data->code=$request->code;
            $data->name=$request->name;
            $data->md_user_id=$request->user_id;
            $data->email=$request->email;
            $data->phone_number=$request->phone_number;
            $data->address=$request->address;
            $data->type_address = $request->type_address;
            $data->city_name = $request->city_name;
            $data->region_name = $request->region_name;
            $data->postal_code = $request->postal_code;
            $data->gender = $request->gender;
            $data->sc_customer_level_id=$request->sc_customer_level_id;

            $data->save();
            DB::commit();

            return response()->json([
                'message'=>'Pelanggan berhasil disimpan',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        }catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return response()->json([
                'message' => 'Terjadi kesalahan di server,data gagal disimpan !',
                'type' => 'danger',
                'is_modal' => false,
                'redirect_url' => ''
            ]);
        }
    }

    public function getCity(Request $request){
        try{
            $id = $request->id;
            $data = City::where('md_province_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }
}

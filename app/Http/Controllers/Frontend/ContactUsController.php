<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Utils\Account\AccountUtil;
use Illuminate\Http\Request;
use App\Models\MasterData\Contact;
use Illuminate\Support\Facades\Route;

class ContactUsController extends Controller
{
    public function routeWeb()
    {
        Route::post('contact/store', 'Frontend\ContactUsController@store')
        ->name('contact.store');
    }

    public function store(Request $request)
    {
        try{
            $id=$request->id;
            if(!is_null($id)){

                $data=Contact::find($id);
            }else{
                $data=new Contact();
            }
            
            if($request->fullname == '' || $request->email == '' || $request->message == ''){
                return "<script>
                    swal({
                        title: 'Perhatian !',
                        text: 'Isian form wajib diisi semua !',
                        icon: 'warning',
                        button: {
                            text:'Tutup',
                            className:'btn btn-orange'
                        },
                    });
                </script>";    
            }

            if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                return "<script>
                    swal({
                        title: 'Perhatian !',
                        text: 'Email yang anda masukkan tidak valid !',
                        icon: 'warning',
                        button: {
                            text:'Tutup',
                            className:'btn btn-orange'
                        },
                    });
                </script>";
            }

            $data->fullname=$request->fullname;
            $data->email=$request->email;
            $data->message=$request->message;
            $data->save();

            
            return "<script>
                swal({
                    title: 'Sukses !',
                    text: 'Pesan anda berhasil dikirim !',
                    icon: 'success',
                    button: {
                        text:'Tutup',
                        className:'btn btn-orange'
                    },
                });
                reload(3000);
            </script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }
}
<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\MasterData\Region\City;
use App\Utils\Account\AccountUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Modules\Blog\Entities\TestimonyEntity;
use App\Models\MasterData\SennaCashier\Category;
use App\Utils\Merchant\ShiftUtil;

class HomeController extends Controller
{
    public function routeWeb()
    {
        Route::get('/', 'Frontend\HomeController@index')->name('home');
        Route::get('/language', 'Frontend\HomeController@language')->name('language');
        Route::get('/sitemap.xml', 'Frontend\HomeController@sitemap')->name('sitemap');
        Route::get('/robot.txt', 'Frontend\HomeController@robots')->name('robot');
        Route::get('/contact', 'Frontend\HomeController@contact')->name('contact');
        // Route::get('/demo', 'Frontend\HomeController@demo')->name('demo');
//        Route::get('/service', 'Frontend\HomeController@layanan')->name('service');
        Route::get('/about', 'Frontend\HomeController@about')->name('about');
        Route::get('/package', 'Frontend\HomeController@package')->name('package');
        Route::get('/term-condition', 'Frontend\HomeController@term')->name('term');
        Route::get('/privacy-policy', 'Frontend\HomeController@kebijakan')->name('policy');
        Route::get('/features', 'Frontend\HomeController@features')->name('features');
        Route::get('/solution', 'Frontend\HomeController@solution')->name('solution');
        Route::get('/solution/detail/{page}', 'Frontend\HomeController@solutionDetail')->name('solution.detail');
        Route::get('/BersamaSennaSelangkahMaju', 'Frontend\HomeController@ctalink')->name('ctaLink');
        Route::get('/SennaFund-batch1', 'Frontend\HomeController@batch1')->name('batch1');
        Route::get('/template-proposal-bisnis-batch-1', 'Frontend\HomeController@templateBatch1')->name('template-batch1');

    }

    public function language(Request $request)
    {
        $lang=$request->lang;
        if(!is_null(Session::get('SENNA_KASIR_PRO_LANGUAGE'))){
            Session::forget('SENNA_KASIR_PRO_LANGUAGE');
            Session::put('SENNA_KASIR_PRO_LANGUAGE',$lang);
        }else{
            Session::put('SENNA_KASIR_PRO_LANGUAGE',$lang);
        }
        return redirect()->back();
    }
    public function index(Request $request)
    {
        $testimony=TestimonyEntity::
        select([
            'md_testimony.*',
            'm.name as merchant_name'
        ])
            ->join('md_merchants as m','m.id','md_testimony.md_merchant_id')
            ->orderBy('md_testimony.created_at', 'DESC')
            ->limit(3)
            ->get();
        $params=[
            'title'=>'Senna - Aplikasi Wirausaha Indonesia',
            'data'=>[],
            'testimony'=>$testimony,
            'meta_desc'=>'Aplikasi Senna untuk semua jenis usaha meliputi: persediaan, keuangan, karyawan, loyalty, dan analisis bisnis. Satu solusi pilihan puluhan ribu pebisnis di Indonesia.'
        ];

        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{
            return view('frontend.page.home',$params);

        }


    }

    public function sitemap(Request $request)
    {

        return response()->view('frontend.page.sitemap')->header('Content-Type', 'text/xml');
    }
    public function contact()
    {
        $params=[
            'title'=>(get_lang()=='id')?'Kontak Kami':'Contact Us',
            'data'=>[],
            'meta_desc'=>"Butuh demo? Butuh bantuan teknis? Ada tawaran kerja sama dengan Senna? Atau ada pertanyaan lain? Segera hubungi layanan sales dan support tim Senna!"

        ];

        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{
            return view('frontend.page.contact',$params);

        }

    }
    public function demo()
    {
        $city=City::all();
        $category=Category::all();
        $params=[
            'title'=>(get_lang()=='id')?'Demo':'Demo',
            'data'=>[],'city'=>$city,'category'=>$category,
            'meta_desc'=>"Butuh demo? Segera isi formnya dan tim sales Senna  akan menghubungi kamu!"

        ];

        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{
            return view('frontend.page.demo',$params);
        }


    }
    public function layanan()
    {
        $params=[
            'title'=>(get_lang()=='id')?'Layanan':'Service',
            'data'=>[]
        ];

        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{

            return view('frontend.page.layanan',$params);
        }


    }
    public function about()
    {
        $params=[
            'title'=>(get_lang()=='id')?'Tentang Kami':'About',
            'meta_desc' => "Fitur Senna terdiri dari aplikasi pengelolaan usaha, manajemen persediaan dan akuntansi, absensi dan karyawan, CRM, cek ongkir pengiriman, serta analisis bisnis.",
            'data'=>[]
        ];


        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{

            return view('frontend.page.about',$params);

        }


    }
    public function package()
    {
        $params=[
            'title'=>(get_lang()=='id')?'Paket Layanan':'Service Package',
            'data'=>[],
            'meta_desc'=>"Dapatkan aplikasi Senna lengkap: aplikasi usaha, aplikasi akuntansi, harga terjangkau mulai 100 ribuan dan coba gratis 14 hari"

        ];

        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{

            return view('frontend.page.package',$params);


        }

    }
    public function kebijakan()
    {
        $params=[
            'title'=>(get_lang()=='id')?'Kebijakan & Privasi':'Privacy & Policy',
            'data'=>[]
        ];
        if(get_lang()=='id')
        {
            return view('frontend.page.kebijakan',$params);

        }else{
            return view('frontend.page.kebijakan-en',$params);

        }
    }
    public function term()
    {
        $params=[
            'title'=>(get_lang()=='id')?'Syarat & Ketentuan':'Term & Condition',
            'data'=>[]
        ];
        if(get_lang()=='id')
        {
            return view('frontend.page.term',$params);

        }else{
            return view('frontend.page.term-en',$params);

        }
    }

    public function features()
    {
        $params = [
            'title' => (get_lang()=='id')?'Fitur ':'Features',
            'data' => [],
            'meta_desc'=>"Fitur Senna terdiri dari aplikasi bisnis online, manajemen persediaan dan akuntansi, absensi dan karyawan, CRM, cek ongkir pengiriman, serta analisis bisnis."

        ];

        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{

            return view('frontend.page.features', $params);
        }


    }

    public function solution()
    {
        $params = [
            'title' => 'Solusi Usaha',
            'data' => [],
            'meta_desc' => "Apapun bisnismu bisa menggunakan aplikasi Senna dengan aplikasi wirausaha lengkap untuk kelola bisnismu jadi selangkah lebih maju",
        ];

        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{

            return view('frontend.page.solution', $params);
        }


    }

    public function solutionDetail(Request $request)
    {
        $params = [
            'title' => 'Solusi Usaha - '.ucwords($request->page),
            'meta_desc' => "Apapun bisnismu bisa menggunakan aplikasi Senna dengan aplikasi wirausaha lengkap untuk kelola bisnismu jadi selangkah lebih maju",
            'page' => $request->page
        ];

        if(env('IS_WITH_FRONTEND')==false){
            return Redirect::route('login');
        }else{

            return view('frontend.page.solution-detail', $params);

        }


    }

    public function robots()
    {
        return response()->view('frontend.page.robot')->header('Content-Type', 'text/plain');
    }

    public function ctaLink()
    {
        return Redirect::to("https://play.google.com/store/apps/details?id=com.senna_store");
    }


    public function batch1()
    {
        return Redirect::to("https://forms.gle/bfFMgETaF8Wk6gxW7");
    }


    public function templateBatch1()
    {
        return Redirect::to("https://sennakreasi-prod.s3.ap-southeast-1.amazonaws.com/public/proposal/Template+Proposal+Bisnis.docx");
    }



}

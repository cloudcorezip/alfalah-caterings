<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Models\Blog\BlogHardware;

class HardwareController extends Controller
{
    public function routeWeb()
    {
        Route::get('/hardware', 'Frontend\HardwareController@index')->name('hardware');
        Route::get('/hardware/search/{keyword?}', 'Frontend\HardwareController@search')->name('hardware.search');
    }

    public function index(Request $request)
    {   
       
        $data = BlogHardware::where('is_deleted', 0)
                    ->where('is_bundling', 0)
                    ->orderBy('id', 'ASC')
                    ->get();

        $dataBundling = BlogHardware::where('is_deleted', 0)
                            ->where('is_bundling', 1)
                            ->orderBy('id', 'ASC')
                            ->get();

        $params = [
            'title' => 'Mesin Penunjang Usaha',
            'data' => $data,
            'dataBundling' => $dataBundling,
            'meta_desc'=>"Aplikasi Senna lengkap dengan sistem terbaik untuk kelola bisnismu selangkah maju."
        ];

        return view('frontend.page.hardware', $params);
    }

    public function search(Request $request)
    {
        $keyword = preg_replace("/[^A-Za-z0-9 ]/", ' ', $request->keyword);
        $data = BlogHardware::where('title', 'ilike', '%' . $keyword . '%')->get();
        
        $params = [
            'title' => 'Mesin Penunjang Usaha',
            'data' => $data,
            'keyword' => $keyword,
            'meta_desc'=>"Aplikasi Senna lengkap dengan sistem terbaik untuk kelola bisnismu selangkah maju."
        ];

        return view('frontend.page.hardware', $params);
    }

}

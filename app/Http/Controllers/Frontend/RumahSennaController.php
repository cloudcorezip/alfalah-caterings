<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use App\Models\Blog\Posts;
use App\Models\Blog\CategoryBlog;
use App\Models\Blog\Tags;

class RumahSennaController extends Controller
{
    
    public function routeWeb()
    {
        Route::get('/rumah-senna', 'Frontend\RumahSennaController@index')->name('rumah-senna');
        Route::get('/rumah-senna/detail/{year}/{month}/{day}/{slug}', 'Frontend\RumahSennaController@indexDetail')->name('rumah-senna.detail');
        Route::get('/rumah-senna/search', 'Frontend\RumahSennaController@search')->name('rumah-senna.search');
        Route::get('/rumah-senna/category/{slug}', 'Frontend\RumahSennaController@show')->name('rumah-senna.show');
        Route::get('/rumah-senna/tags/{slug}', 'Frontend\RumahSennaController@showByTags')->name('rumah-senna.showByTags');
    }

    public function index()
    {
        $data=Posts::where('is_publish',1)
                ->whereIn('md_bg_category_id', [8,9,10])
                ->orderBy('created_at', 'DESC')
                ->paginate(6);
        $content1=Posts::where('is_publish', 1)
            ->whereIn('md_bg_category_id', [8,9,10])    
            ->orderBy('created_at', 'DESC')
            ->limit(6)
            ->get();
        $category=CategoryBlog::whereIn('id', [8,9,10])->get();
        $params=[
            'title'=>'Rumah Senna',
            'data'=>$data,
            'content1'=>$content1,
            'category'=>$category,
            'tags'=>Tags::all(),
        ];
        return view('blog.rumah-senna.index',$params);
    }

    public function indexDetail($year,$month,$day,$slug)
    {
        $date=implode("-",[$year,$month,$day]);
        $data=Posts::where('slug',$slug)->whereDate('created_at',$date)
        ->first();
        $data->increment('views', 1);
        $category=CategoryBlog::whereIn('id', [8,9,10])->get();
        $content=Posts::where('is_publish',1)
                ->whereIn('md_bg_category_id', [8,9,10])
                ->orderBy('created_at', 'DESC')
                ->take(6)->get();
        $params=[
            'title'=>$data->title,
            'data'=>$data,
            'content'=>$content,
            'category'=>$category,
            'tags'=>Tags::all()
        ];
        return view('blog.rumah-senna.detail',$params);
    }

    public function show($slug)
    {
        $categoryItem=CategoryBlog::where('slug',$slug)->first();
        $data = Posts::where('md_bg_category_id',$categoryItem->id)
                ->where('is_publish',1)
                ->orderBy('created_at', 'DESC')
                ->paginate(6);
        $content1=Posts::orderBy('created_at', 'DESC')
            ->where('is_publish',1)
            ->whereIn('md_bg_category_id', [8,9,10])
            ->limit(6)
            ->get();
        $category=CategoryBlog::whereIn('id', [8,9,10])->get();
        $params=[
            'title'=>'Posting Dengan Kategori : '.$categoryItem->name,
            'data'=>$data,
            'content1'=>$content1,
            'category'=>$category,
            'categoryItem'=>$categoryItem,
            'tags'=>Tags::all(),
            'slug' => $slug
        ];
        return view('blog.rumah-senna.show',$params);
    }

    public function showByTags($slug)
    {
        $tags=Tags::where('slug',$slug)->first();
        $data = Posts::where('keyword','like', '%' . $slug . '%')
                    ->whereIn('md_bg_category_id', [8,9,10])
                    ->orderBy('created_at', 'DESC')
                    ->paginate(6);
        $content1=Posts::orderBy('created_at', 'DESC')
            ->where('is_publish',1)
            ->whereIn('md_bg_category_id', [8,9,10])
            ->orderBy('created_at', 'DESC')
            ->limit(6)
            ->get();
        $category=CategoryBlog::whereIn('id', [8,9,10])->get();
        $params=[
            'title'=>'Posting Dengan Tag : '.$tags->name,
            'data'=>$data,
            'content1'=>$content1,
            'category'=>$category,
            'categoryItem'=>$tags,
            'tags'=>Tags::all(),
            'slug' => $slug
        ];
        return view('blog.rumah-senna.show',$params);
    }

    public function search(Request $request)
    {
        $search = $request->keyword;
        $data = Posts::where(function($query) use($search){
                    $query->where('title', 'ilike', '%' . $search . '%')
                    ->orWhere('content', 'ilike', '%' . $search . '%')
                    ->orWhere('keyword', 'ilike', '%' . $search . '%');
                })
                ->whereIn('md_bg_category_id', [8,9,10])
                ->orderBy('created_at', 'DESC')
                ->paginate(6);
        $contentA=Posts::all()->take(1);
        $content1=Posts::orderBy('created_at', 'DESC')
            ->where('is_publish',1)
            ->whereIn('md_bg_category_id', [8,9,10])
            ->limit(6)
            ->get();
        $category=CategoryBlog::whereIn('id', [8,9,10])->get();
        $params=[
            'title'=>'Pencarian Dengan Kata Kunci : '.$request->keyword,
            'data'=>$data,
            'content1'=>$content1,
            'category'=>$category,
            'tags'=>Tags::all()
        ];

        return view('blog.rumah-senna.index',$params);
    }

}
<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Utils\Account\AccountUtil;
use Illuminate\Http\Request;
use App\Models\MasterData\Demo;
use Illuminate\Support\Facades\Route;

class DemoController extends Controller
{
    public function routeWeb()
    {
        Route::post('demo/store', 'Frontend\DemoController@store')
        ->name('demo.store');
    }

    public function store(Request $request)
    {
        try{
            $id=$request->id;
            if(!is_null($id)){

                $data=Demo::find($id);
            }else{
                $data=new Demo();
            }
            
            $data->fullname=$request->fullname;
            $data->email=$request->email;
            $data->telp=$request->telp;
            $data->company_name=$request->company_name;
            $data->md_cities_id=$request->md_cities_id;
            $data->md_sc_categories_id=$request->md_sc_categories_id;
            $data->product=$request->product;
            $data->demo_type=$request->demo_type;
            $data->demo_date=$request->demo_date;
            $data->description=$request->description;
            $data->save();

            return "<div class='alert alert-success text-center' role='alert'>
            <h4 class='alert-heading'>Sukses!</h4>
            <p>Terima kasih telah melakukan request demo ke kami, tim kami akan menghubungimu melalui whatsapp atau email untuk proses selanjutnya.</p>
          </div>
            <script>
           reload(1500);
            </script>";
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());

            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server,data gagal disimpan!</div>";


        }
    }
}
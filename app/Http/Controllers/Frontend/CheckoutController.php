<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Frontend;

use App\Classes\Singleton\WhatsappBroadcast;
use App\Http\Controllers\Controller;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\SaleOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;
use App\Classes\Singleton\CodeGenerator;
use App\Models\MasterData\User;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\VaBank;
use App\Models\SennaToko\PaymentMethod;
use App\Models\SennaToko\PaymentInvoice;
use App\Models\MasterData\SennaCashier\Currency;
use App\Models\MasterData\TransactionType;
use App\Models\QRIS\QRISActivation;
use App\Utils\ThirdParty\VirtualAccountUtil;
use App\Utils\PaymentUtils;
use App\Classes\Singleton\XenditCore;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Classes\Singleton\Message;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Artisan;
use App\Utils\Accounting\CoaSaleUtil;
use Illuminate\Support\Facades\Redirect;
use QrCode;
use Image;
use Illuminate\Support\Facades\Storage;
use App\Utils\Inventory\InventoryUtilV2;
use App\Events\PaymentSuccess;
use PDF;
use Modules\Merchant\Entities\Toko\CustomerEntity;


class CheckoutController extends Controller
{

    protected $sale;
    protected $message;
    protected $product;
    protected $ar;
    protected $xendit;
    protected $virtualAccount;
    protected $waBroadcast;



    public function __construct()
    {
        $this->sale=SaleOrder::class;
        $this->message=Message::getInstance();
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->xendit=XenditCore::getInstance();
        $this->virtualAccount=new VirtualAccountUtil();
        $this->waBroadcast=WhatsappBroadcast::getInstance();

    }

    public function routeWeb()
    {
        Route::get('/checkout/{key}', 'Frontend\CheckoutController@index')
            ->name('checkout');

        Route::post('/checkout/create-charge', 'Frontend\CheckoutController@createCharge')
            ->name('checkout.create-charge');

        Route::get('/checkout-detail', 'Frontend\CheckoutController@detail')
            ->name('checkout-detail');

        Route::get('/checkout-notification', 'Frontend\CheckoutController@checkoutNotification')
            ->name('checkout-notification');

        Route::post('/accept-transfer', 'Frontend\CheckoutController@acceptBankTransfer')
            ->name('checkout.accept-transfer');

        Route::get('/export/{key}', 'Frontend\CheckoutController@exportPdf')
            ->name('checkout.export-pdf');

    }


    public function checkoutNotification(Request $request){
        $id = decrypt($request->key);
        $status = $request->status;
        $sale = SaleOrder::find($id);
        if(is_null($sale)){
            $sale = MerchantArDetail::find($id);
            $code = $sale->ar_detail_code;
            $total = $sale->total+$sale->admin_fee;

        }else{
            $code = $sale->code;
            $total = $sale->total+$sale->admin_fee;
        }
        $params=[
            'title'=>($status==2)?'Pembayaran Berhasil':'Pembayaran Gagal',
            'data'=>$sale,
            'status' => $status,
            'code'=>$code,
            'total'=>$total
        ];

        return view('checkout.notification',$params);

    }

    public function index(Request $request)
    {
        $key = $request->key; // saleorder id
        $data = SaleOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getStaff')
                ->with('ar')
                ->with('getMerchant')
                ->where('id',$key)->first();

        $merchantId = $data->getMerchant->id;
        $paymentInvoice = PaymentInvoice::where('md_merchant_id', $merchantId)
                                            ->first();
        date_default_timezone_set($data->timezone);
        if(!is_null($data->md_transaction_type_id) && $data->expired_time > date('Y-m-d H:i:s')){
            if($data->md_sc_transaction_status_id != 4){
                return Redirect::route('checkout-detail',[
                    'key'=>encrypt($data->id)
                ]);
            }
        }

        $merchant = Merchant::find($merchantId);
        $code = $data->code;

        $coaJson=CoaSaleUtil::getCoaSaleOrder($merchantId);
        $params=[
            'title'=>'Pilih Metode Pembayaran Transaksi '.$code,
            'data'=>$data,
            'paymentOption'=>PaymentUtils::getPayment($merchantId),
            'merchant_id' => $merchantId,
            'merchant' => $merchant,
            'coa_json'=>json_encode($coaJson),
            'payment_invoice' => $paymentInvoice
        ];

        return view('checkout.index', $params);
    }

    public function detail(Request $request)
    {
        $key=decrypt($request->key);
        $saleOrder=SaleOrder::find($key);
        $merchantId = $saleOrder->md_merchant_id;

        $paymentInvoice = PaymentInvoice::where('md_merchant_id', $merchantId)
                                            ->first();
        $data = PaymentMethod::select([
            'md_merchant_payment_methods.id',
            'md_merchant_payment_methods.md_merchant_id',
            'md_merchant_payment_methods.transaction_type_id',
            'md_merchant_payment_methods.md_va_bank_id',
            'md_merchant_payment_methods.payment_name',
            'md_merchant_payment_methods.bank_account_name',
            'md_merchant_payment_methods.bank_account_number',
            'b.name',
            'b.icon',
            'vb.code as bank_code',
            't.icon as icon_wallet',
            't.name as wallet_name'
        ])
            ->leftJoin('md_va_banks as vb', 'vb.id', 'md_merchant_payment_methods.md_va_bank_id')
            ->leftJoin('md_banks as b', 'b.id', 'vb.md_bank_id')
            ->leftJoin('md_transaction_types as t','t.id','md_merchant_payment_methods.transaction_type_id')
            ->where('md_merchant_payment_methods.status', 1)
            ->where('md_merchant_payment_methods.id', $saleOrder->md_merchant_payment_method_id)
            ->where('md_merchant_payment_methods.md_merchant_id',$saleOrder->md_merchant_id)
            ->first();

        date_default_timezone_set($saleOrder->timezone);

        if(date('Y-m-d H:i:s')>$saleOrder->expired_time){
           return Redirect::route('checkout',[
                'key'=>$saleOrder->id
            ]);
        }

        $params = [
            "title" => "Checkout Pembayaran - ".$saleOrder->code,
            "data" => $data,
            "merchant" => $saleOrder->getMerchant,
            "saleOrder" => $saleOrder,
            "expireDate" => $saleOrder->expired_time,
            'va'=>json_decode($saleOrder->checkout_response),
            "payment_invoice" => $paymentInvoice
        ];

        return view('checkout.detail', $params);
    }

    public function createCharge(Request $request)
    {
        try {
            $data = SaleOrder::find($request->sale_id);
            $totalSaleOrder = $data->total;
            $trans = explode('_', $request->md_transaction_type_id);
            $merchant = Merchant::find($data->md_merchant_id);
            $userId = $merchant->md_user_id;
            $status=TransactionStatus::UNPAID;

            if($data->md_sc_transaction_status_id == TransactionStatus::PAID){
                return response()->json([
                    'message' => 'Pembayaran Transaksi Penjualan '.$data->code.' Telah Dilakukan',
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' =>route("checkout-detail", ["key" => encrypt($data->id)])
                ]);
            }

            if(is_null($trans)){
                return response()->json([
                    'message' => 'Metode Pembyaran belum dipilih !',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' =>""
                ]);
            }

            DB::beginTransaction();

            if($trans[2]==1){
                if($trans[0]==4){
                    $adminFee = ceil(0.008 * $totalSaleOrder);
                    $total = ceil($adminFee + $totalSaleOrder);
                }elseif ($trans[0]==5){
                    $adminFee = 5000;
                    $total = ceil($adminFee + $totalSaleOrder);
                }else {
                    $adminFee = ceil(0.016 * $totalSaleOrder);
                    $total = ceil($adminFee + $totalSaleOrder);
                }

            }else{
                $adminFee=0;
                $total=$totalSaleOrder;
            }

            if($trans[2]==1){
                $updateSale=SaleOrder::find($data->id);
                $updateSale->is_payment_gateway=1;
                if($trans[0]==4){
                    $paramsQRIS = [
                        'external_id' => (string)Uuid::uuid4()->toString(),
                        'type' => 'DYNAMIC',
                        'callback_url' =>\route('pay.callback.all'),
                        'amount' =>(float)$total,
                    ];

                    if(env('APP_ENV')=='production'){
                        $qris=$this->xendit::createQRIS($paramsQRIS);

                        if (array_key_exists('message', $qris)) {
                            return response()->json([
                                'message' => $qris['message'],
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                    }else{
                        if($merchant->id==4090){
                            $qris=$this->xendit::createQRIS($paramsQRIS);

                            if (array_key_exists('message', $qris)) {
                                return response()->json([
                                    'message' => $qris['message'],
                                    'type' => 'warning',
                                    'is_modal' => false,
                                    'redirect_url' => ''
                                ]);
                            }
                        }else{
                            $qris=[
                                'id' => (string)Uuid::uuid4()->toString(),
                                'external_id' => 'testing_id_123',
                                'nominal' => 1500,
                                'qr_string' => '0002010102##########CO.XENDIT.WWW011893600#######14220002152#####414220010303TTT####015CO.XENDIT.WWW02180000000000000000000TTT52045######ID5911XenditQRIS6007Jakarta6105121606##########3k1mOnF73h11111111#3k1mOnF73h6v53033605401163040BDB',
                                'callback_url' => 'https://yourwebsite.com/callback',
                                'type' => 'DYNAMIC',
                                'status' => 'ACTIVE',
                                'created' => '2020-01-08T18:18:18.661Z',
                                'updated' => '2020-01-08T18:18:18.661Z',
                            ];
                        }

                    }

                    $destinationPath = 'public/uploads/transaction/'.$userId.'/qr/';
                    $qrCodeFile=$destinationPath.$data->code.'.png';
                    $imageQR=QrCode::size(600)
                        ->format('png')
                        ->generate($qris['qr_string']);
                    Storage::disk('s3')->put($qrCodeFile, $imageQR);
                    $updateSale->qr_code=$qrCodeFile;
                    $updateSale->md_merchant_payment_method_id=$trans[4];
                    $updateSale->md_transaction_type_id = $trans[0];
                    $updateSale->expired_time=Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
                    $updateSale->qris_response=json_encode($qris);
                    $updateSale->checkout_response=json_encode($qris);
                    $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                    $updateSale->coa_trans_id = $trans[1];
                    $updateSale->admin_fee = $adminFee;
                    $updateSale->is_debet = 0;
                    $updateSale->save();
                    DB::commit();

                    return response()->json([
                        'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('checkout-detail', [
                            'key' => encrypt($data->id),
                        ])
                    ]);
                }elseif ($trans[0]==5){
                    $response=$this->virtualAccount->create($userId,strtoupper($trans[3]),$total,16,(string)Uuid::uuid4()->toString());

                    if($response == false){
                        return response()->json([
                            'message' => 'Terjadi Kesalahan saat pembuatan VA !',
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }

                    $updateSale->md_merchant_payment_method_id=$trans[4];
                    $updateSale->md_transaction_type_id = $trans[0];
                    $updateSale->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                    $updateSale->checkout_response=json_encode($response);
                    $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                    $updateSale->coa_trans_id = $trans[1];
                    $updateSale->admin_fee = $adminFee;
                    $updateSale->is_debet = 0;
                    $updateSale->save();
                    DB::commit();

                    return response()->json([
                        'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('checkout-detail', [
                            'key' => encrypt($data->id),
                            ])
                    ]);

                }else{
                    $idEWallet=$trans[3].'_'.$trans[4];
                    if($idEWallet=='ID_OVO'){
                        if(is_null($request->mobile_number)){
                            return response()->json([
                                'message' => 'No telpon untuk metode pembayaran OVO wajib diisi',
                                'type' => 'warning',
                                'is_modal' => false,
                                'redirect_url' => ''
                            ]);
                        }
                        $ewallet = [
                            'reference_id' => (string)Uuid::uuid4()->toString(),
                            'currency' => 'IDR',
                            'amount' => $total,
                            'checkout_method' => 'ONE_TIME_PAYMENT',
                            'channel_code' => $idEWallet,
                            'channel_properties' => [
                                'mobile_number'=>$request->mobile_number,
                                'success_redirect_url' => route('checkout-notification', [
                                    'key' => encrypt($data->id),
                                    'status'=>2
                                ]),
                                'failure_redirect_url'=>route('checkout-notification', [
                                    'key' => encrypt($data->id),
                                    'status'=>5

                                ])
                            ],
                        ];
                    }else{
                        $ewallet = [
                            'reference_id' => (string)Uuid::uuid4()->toString(),
                            'currency' => 'IDR',
                            'amount' => $total,
                            'checkout_method' => 'ONE_TIME_PAYMENT',
                            'channel_code' => $idEWallet,
                            'channel_properties' => [
                                'success_redirect_url' => route('checkout-notification', [
                                    'key' => encrypt($data->id),
                                    'status'=>2
                                ]),
                                'failure_redirect_url'=>route('checkout-notification', [
                                    'key' => encrypt($data->id),
                                    'status'=>5

                                ])
                            ],
                        ];

                    }
                    $ewalletRes=$this->xendit::createEWalletCharge($ewallet);

                    if (array_key_exists('message', $ewalletRes)) {
                        return response()->json([
                            'message' => $ewalletRes['message'],
                            'type' => 'warning',
                            'is_modal' => false,
                            'redirect_url' => ''
                        ]);
                    }
                    $updateSale->md_merchant_payment_method_id=$trans[5];
                    $updateSale->md_transaction_type_id = $trans[0];
                    $updateSale->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                    $updateSale->checkout_response=json_encode($ewalletRes);
                    $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                    $updateSale->coa_trans_id = $trans[1];
                    $updateSale->admin_fee = $adminFee;
                    $updateSale->save();
                    DB::commit();

                    return response()->json([
                        'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                        'type' => 'success',
                        'is_modal' => false,
                        'redirect_url' => route('checkout-detail', [
                            'key' => encrypt($data->id),
                        ])
                    ]);
                }

            }else{

                $arrayOfSession = json_decode($data->json_order_online, true);

                $coaJson = collect(json_decode($request->coa_json));
                $arrayOfSession["coaJson"] =  $coaJson;

                $updateSale=SaleOrder::find($data->id);
                $updateSale->admin_fee = 0;
                $updateSale->md_merchant_payment_method_id = $trans[3];
                $updateSale->coa_trans_id = $trans[1];
                $updateSale->md_transaction_type_id = $trans[0];
                $updateSale->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                $updateSale->expired_time=Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                $updateSale->save();

                DB::commit();
                return response()->json([
                    'message' => 'Checkout berhasil, kamu akan diarahkan ke halaman pembayaran!',
                    'type' => 'success',
                    'is_modal' => false,
                    'redirect_url' => route('checkout-detail', [
                        'key' => encrypt($data->id),
                    ])

                ]);
            }
        }catch(\Exception $e){
            DB::rollback();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function acceptBankTransfer(Request $request)
    {
        try {

            $sale = SaleOrder::find($request->sale_id);
            $data = json_decode($sale->json_order_online, true);
            if($request->bank_id == -1){
                return response()->json([
                    'message'=>'Silahkan pilih bank terlebih dahulu !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }
            $coaJson=collect(json_decode($request->coa_json));

            if(!empty($data["productCalculate"])){
                $calculateInventory=InventoryUtilV2::inventoryCodev2($data['inv_id'],$data['warehouse_id'],(array)$data['productCalculate'],'minus');
                if($calculateInventory==false)
                {
                    return  false;
                }
                $updateStockInv="";
                $stockInvId=[];
                $saleMapping=[];

                foreach ($calculateInventory as $key => $n){
                    $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                    $stockInvId[]=$n['id'];
                    $saleMapping[]=[
                        'sc_sale_order_id'=>$sale->id,
                        'sc_product_id'=>$n['sc_product_id'],
                        'sc_stock_inventory_id'=>$n['id'],
                        'amount'=>$n['amount'],
                        'purchase_price'=>$n['purchase'],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ];
                }

                if($updateStockInv!=""){
                    DB::statement("
                        update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                }

                if($data['updateStockProduct']!=""){
                    DB::statement("
                        update sc_products as t set
                            stock = c.column_a
                        from (values
                            ".$data['updateStockProduct']."
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                }

                $stockOut=[];
                foreach ($data['stock_in_out'] as $key =>$i){
                    $stockOut[]=[
                        "type"=> $i["type"],
                        "total"=> $i["total"],
                        "sync_id"=> $i["sync_id"],
                        "created_at"=>  $i["created_at"],
                        "created_by"=>  $i["created_by"],
                        "updated_at"=>  $i["updated_at"],
                        "record_stock"=> $i["record_stock"],
                        "stockable_id"=>  $i["stockable_id"],
                        "sc_product_id"=>  $i["sc_product_id"],
                        "selling_price"=>  $i["selling_price"],
                        "purchase_price"=>  $i["purchase_price"],
                        "residual_stock"=>  $i["residual_stock"],
                        "stockable_type"=>  $i["stockable_type"],
                        "inv_warehouse_id"=>  $i["inv_warehouse_id"],
                        "transaction_action"=>  $i["transaction_action"],
                    ];
                }

                DB::table('sc_stock_sale_mappings')->insert($saleMapping);
                DB::table('sc_stock_inventories')->insert($stockOut);
                DB::table('sc_inv_production_of_goods')
                    ->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);
            }

            $otherPayment = [
                "md_bank_id" => $request->bank_id,
                "bank_name" => $request->bank_name,
                "account_number" => $request->no_rek,
                "total_amount" => $sale->total
            ];

            $sale->md_sc_transaction_status_id=2;
            $sale->other_payment_info = json_encode($otherPayment);
            $sale->save();


            $customerData=[
                'name'=>is_null($sale->getCustomer)?'-':$sale->getCustomer->name,
                'phone_number'=>is_null($sale->getCustomer)?'-':$sale->getCustomer->phone_number,
                'address'=>is_null($sale->getCustomer)?'-':$sale->getCustomer->address
            ];

            $broadcast=$this->waBroadcast->send([
                'number'=>$sale->getMerchant->phone_merchant,
                'message'=>'Hai *'.$sale->getMerchant->name.'*,%0ABerikut invoice untuk transaksi nomor *'.$sale->code.'*: %0ANama Pelanggan : '.$customerData["name"].'%0ANo.Telp : '.$customerData["phone_number"].'%0AAlamat: '.$customerData["address"].'%0A%0ASilahkan unduh invoice disini : '.route('checkout.export-pdf', ['key' => encrypt($sale->id)]).'%0A%0ATerima Kasih...'
            ]);




            DB::commit();
            if(CoaSaleUtil::coaStockAdjustmentFromSaleV2($data['user_id'], $data['merchant_id'],$coaJson,$data['warehouse_id'],$data['inv_id'], $sale->id) == false) {
                DB::rollBack();
                return false;
            }
            if($broadcast->status==false){
                DB::rollBack();
                return response()->json([
                    'message' => 'Terjadi kesalahan saat mengirimkan invoice melalui WA',
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''

                ]);
            }
            event(new PaymentSuccess($sale->id,2));

            return response()->json([
                'message'=>'Pembayaran Transaksi Penjualan '.$sale->code.', Telah Diterima',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>''
            ]);

        } catch(\Exception $e){
            DB::rollback();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function exportPdf(Request $request)
    {
        try {
            $key=decrypt($request->key);
            $saleOrder=SaleOrder::find($key);
            $merchantId = $saleOrder->md_merchant_id;

            if(is_null($saleOrder)){
                return abort(404);
            }

            if($saleOrder->is_from_form_order != 1){
                return abort(404);
            }

            $paymentInvoice = PaymentInvoice::where('md_merchant_id', $merchantId)
                                                ->first();
            $data = PaymentMethod::select([
                'md_merchant_payment_methods.id',
                'md_merchant_payment_methods.md_merchant_id',
                'md_merchant_payment_methods.transaction_type_id',
                'md_merchant_payment_methods.md_va_bank_id',
                'md_merchant_payment_methods.payment_name',
                'md_merchant_payment_methods.bank_account_name',
                'md_merchant_payment_methods.bank_account_number',
                'b.name',
                'b.icon',
                'vb.code as bank_code',
                't.icon as icon_wallet',
                't.name as wallet_name'
            ])
                ->leftJoin('md_va_banks as vb', 'vb.id', 'md_merchant_payment_methods.md_va_bank_id')
                ->leftJoin('md_banks as b', 'b.id', 'vb.md_bank_id')
                ->leftJoin('md_transaction_types as t','t.id','md_merchant_payment_methods.transaction_type_id')
                ->where('md_merchant_payment_methods.status', 1)
                ->where('md_merchant_payment_methods.id', $saleOrder->md_merchant_payment_method_id)
                ->where('md_merchant_payment_methods.md_merchant_id',$saleOrder->md_merchant_id)
                ->first();

            date_default_timezone_set($saleOrder->timezone);

            $customer = CustomerEntity::find($saleOrder->sc_customer_id);

            $params = [
                "title" => "Checkout Pembayaran - ".$saleOrder->code,
                "data" => $data,
                "merchant" => $saleOrder->getMerchant,
                "saleOrder" => $saleOrder,
                "expireDate" => $saleOrder->expired_time,
                'va'=>json_decode($saleOrder->checkout_response),
                "payment_invoice" => $paymentInvoice,
                'promo_product'=>0,
                "created_at" => Carbon::parse($saleOrder->created_at)->isoFormat('D MMMM Y, HH:mm:ss'),
                "customer" => $customer
            ];

            $pdf = PDF::loadview('checkout.export-pdf', $params)->setPaper('a4');
            return $pdf->stream('transaksi-'.$saleOrder->code.'_'.date('Y-m-d').'.pdf');

        }catch(\Exception $e)
        {
            return abort(404);
        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Models\Faq\FaqContent;
use App\Models\Faq\FaqCategory;

class FaqController extends Controller
{
    public function routeWeb()
    {
        Route::get('/faq', 'Frontend\FaqController@index')->name('faq');
        Route::get('/faq/search/{keyword?}', 'Frontend\FaqController@search')->name('faq.search');
        Route::get('/faq/change-view', 'Frontend\FaqController@changeView')->name('faq.change-view');
    }

    public function index(Request $request)
    {
        $categoryActive = FaqCategory::orderBy('id', 'ASC')->first();

        if(is_null($categoryActive)){
            abort(404);
        }
        $data = FaqContent::where('faq_category_id', $categoryActive->id)
                    ->where('is_publish', 1)
                    ->orderBy('id', 'ASC')
                    ->get();

        $params = [
            'title' => 'FAQ',
            'data' => $data,
            'category' => FaqCategory::orderBy('id', 'ASC')->get(),
            'category_active' => $categoryActive,
            'meta_desc' => "Halo, selamat datang di Pusat Informasi dan Bantuan Senna. Disini tersedia jawaban atas kebingungan anda menggunakan Senna. Budayakan membaca sebelum bertanya ya. Apabila anda tidak menemukan jawaban disini, silahkan gunakan fitur Live Chat dengan Admin kami untuk membantu menyelesaikan masalah anda. Terima kasih telah menggunakan Senna"
        ];

        return view('frontend.page.faq', $params);
    }

    public function search(Request $request)
    {
        $keyword = preg_replace("/[^A-Za-z0-9 ]/", ' ', $request->keyword);
        $data = FaqContent::where('title', 'ilike', '%' . $keyword . '%')->get();
        $categoryActive = FaqCategory::orderBy('id', 'ASC')->first();
        $params = [
            'title' => 'FAQ',
            'data' => $data,
            'category' => FaqCategory::all(),
            'category_active' => $categoryActive,
            'keyword' => $keyword,
            'meta_desc' => "Halo, selamat datang di Pusat Informasi dan Bantuan Senna. Disini tersedia jawaban atas kebingunganmu menggunakan Senna. Budayakan membaca sebelum bertanya ya. Apabila kamu tidak menemukan jawaban disini, silahkan gunakan fitur Live Chat dengan Admin kami untuk membantu menyelesaikan masalah kamu. Terima kasih telah menggunakan Senna"
        ];

        return view('frontend.page.faq', $params);
    }

    public function changeView(Request $request)
    {
        $data = FaqContent::where('faq_category_id', $request->category_id)
                    ->where('is_publish', 1)
                    ->orderBy('id', 'ASC')
                    ->get();
        $html = "";

        if(count($data) == 0){
            $html .= '<script>
                        swal({
                            title: "Perhatian !",
                            text: "Data tidak tersedia!",
                            icon: "warning",
                            button: {
                                text:"Tutup",
                                className:"btn btn-orange"
                            },
                        }).then((value) => {
                            window.location.replace("'.route('faq').'");
                        });
                    </script>';
        } else {
            foreach($data as $key => $item){
                $html .= '<div class="w-100 mb-4">
                            <a class="faq-collapse arial-rounded collapsed d-flex align-items-center" data-toggle="collapse" href="#collapse'.$item->id.'" role="button" aria-expanded="false" aria-controls="collapse'.$item->id.'">
                                '.$item->title.' <i class="fa fa-chevron-down"></i>
                            </a>
                            <div class="collapse py-3" id="collapse'.$item->id.'">
                                '.$item->content.'
                            </div>
                        </div>';
            }
        }

        return $html;
    }

}

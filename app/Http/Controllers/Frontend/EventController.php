<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\MasterData\User;
use App\Models\Marketing\Event;
use App\Models\Marketing\EventParticipant;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;
use App\Models\MasterData\Region\District;
use App\Models\MasterData\TypeOfBusiness;
use App\Models\MasterData\SennaCashier\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Image;

class EventController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('event')->group(function (){
            Route::get('/detail/{slug}', 'Frontend\EventController@index')
                ->name('event');
            Route::post('/detail/{slug}/register', 'Frontend\EventController@register')
                ->name('event.register');
            Route::post('/detail/{slug}/save', 'Frontend\EventController@save')
                ->name('event.save');
            Route::get('/get-city', 'Frontend\EventController@getCity')
                ->name('event.get-city');
            Route::get('/get-district', 'Frontend\EventController@getDistrict')
                ->name('event.get-district');
        });
    }

    public function index(Request $request, $slug){
        $step = $request->step;
        $type=TypeOfBusiness::All();
        $categories = Category::All();
        $event = Event::where('slug',$slug)
                        ->where('is_active', 1)
                        ->first();
        $availableEvent = ["status" => true, "message" => ""];

        if(is_null($event)){
            return abort(404);
        }

        if($event->start_event < date('Y-m-d h:i:s')){
            $availableEvent["status"] = false;
            $availableEvent["message"] = "Mohon maaf, pendaftaran untuk event ini sudah berakhir !";
        }

        if($event->end_event < date('Y-m-d h:i:s')){
            $availableEvent["status"] = false;
            $availableEvent["message"] = "Mohon maaf, event ini sudah tidak tersedia !";
        }

        $stepOne = null;
        if(!is_null($step)){
            $stepOne = $request->session()->get('stepOne');
            if(is_null($stepOne) && $step == 2){
                return \Redirect::route('event', [$slug]);
            }
        }

        $params = [
            "title" => "Event - ".ucwords($event->name),
            "step" => $step,
            "stepOne" => $stepOne,
            "type" => $type,
            "categories" => $categories,
            "provinces" => Province::all(),
            "event" => $event,
            "start_event" =>  Carbon::parse($event->start_event)->isoFormat(' D MMMM Y'),
            "availableEvent" => $availableEvent
        ];

        return view('event.index', $params);
    }

    public function register(Request $request, $slug){

        try {

            $userEvent = EventParticipant::where('email_address', '=', $request->email)
                        ->where('mrkt_events_id', '=', $request->event_id)
                        ->first();

            if(!is_null($userEvent)){
                return redirect()->route('event', [$slug])->with('message', "Emailmu sudah terdaftar untuk event ini !");
            }


            $step = $request->step;

            if(is_null($step)){
                $stepOne = [
                    "fullname" => $request->fullname,
                    "email" => $request->email,
                    "phone_number" => $request->phone_number,
                    "address" => $request->address,
                    "gender" => $request->gender,
                    "district" => $request->district,
                    "event_id" => $request->event_id,
                    "slug" => $request->slug
                ];

                 return redirect()->route('event', [$slug, "step" => 2])->with('stepOne', $stepOne);
            }


        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server</div><script>reload(3000);</script>";
        }
    }

    public function save(Request $request, $slug){
        try {

            $data = new EventParticipant();

            $destinationPath = 'public/uploads/event/proof/';

            $data->fullname = $request->fullname;
            $data->email_address = $request->email;
            $data->phone_number = $request->phone_number;
            $data->address = $request->address;
            $data->gender = $request->gender;
            $data->company_name = $request->company_name;
            $data->md_type_of_business_id = $request->type_id;
            $data->business_category = $request->business_category;
            $data->mrkt_events_id = $request->event_id;
            $data->md_transaction_status_id = 3;
            $data->md_district_id = $request->district_id;

            if($request->hasFile('trans_approve'))
            {
                $file=$request->file('trans_approve');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('trans_approve'))->encode($file->getClientOriginalExtension());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, (string) $image);

                $data->transaction_approve = $fileName;
            }

            $data->save();

            return "<div class='alert alert-success' style='text-align: center'>Data berhasil disimpan !</div>
                <script>
                    setTimeout(() => {window.location.href = '".route('event', [$slug, 'step' => 3])."'
                    }, 1000)
                </script>";

        } catch(\Exception $e){
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server</div><script>reload(3000);</script>";
        }
    }

    public function getCity(Request $request){

        try{
            $id = $request->id;
            $data = City::where('md_province_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }

    public function getDistrict(Request $request){
        try{
            $id = $request->id;
            $data = District::where('md_city_id', $id)->get();

            return $data;

        }catch(\Exception $e){
            return [];
        }
    }


}

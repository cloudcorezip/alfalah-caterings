<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Frontend;

use App\Classes\Singleton\CodeGenerator;
use App\Http\Controllers\Controller;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;
use Modules\Merchant\Entities\Toko\ReservationEntity;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\Product;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantBranch;
use App\Models\SennaToko\ClinicMedicalRecord;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PDF;
use QrCode;
use Illuminate\Support\Facades\Storage;
use App\Utils\Merchant\MerchantUtil;

class ReservationController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('/reservation')
            ->group(function (){
                Route::get('/create/{merchant_id}', [static::class, 'create'])
                    ->name('reservation.create');
                Route::get('/add/{merchant_id}', [static::class, 'add'])
                    ->name('reservation.add');

                Route::post('/save-member', [static::class, 'saveMember'])
                    ->name('reservation.save-member');
                Route::post('/redirect-member', [static::class, 'redirectMember'])
                    ->name('reservation.redirect-member');
                Route::post('/save', [static::class, 'save'])
                    ->name('reservation.save');

                Route::get('/detail', [static::class, 'detail'])
                    ->name('reservation.detail');

                Route::post('/getCustomer', [static::class, 'getCustomer'])
                    ->name('reservation.get-customer');

                Route::get('/print/{id}', [static::class, 'printPdf'])
                    ->name('reservation.print-pdf');
                Route::get('/detail/{id}', [static::class, 'detail'])
                    ->name('reservation.detail');

                Route::post('/calculate-age', [static::class, 'calculateAge'])
                    ->name('reservation.calculate-age');
        
        });
    }

    protected function _getHeadAndBranch($merchantId)
    {
        $merchant = Merchant::find($merchantId);
        if($merchant->is_branch == 0){
            $head = $merchant;
        } else {
            $merchantBranch = MerchantBranch::where('branch_merchant_id', $merchant->id)
                                            ->where('is_active', 1)
                                            ->first();
            $head = Merchant::find($merchantBranch->head_merchant_id);
        }
        $data = MerchantBranch::
                select([
                    'md_merchant_branchs.id',
                    'b.name as nama_cabang',
                    'md_merchant_branchs.email_branch as email_cabang',
                    'b.id as branch_merchant_id',
                    'u.id as branch_user_id'
                ])
                    ->join('md_merchants as m','m.id','md_merchant_branchs.head_merchant_id')
                    ->join('md_merchants as b','b.id','md_merchant_branchs.branch_merchant_id')
                    ->join('md_users as u', 'u.id', 'b.md_user_id')
                    ->orderBy('md_merchant_branchs.id','desc')
                    ->where('md_merchant_branchs.is_active', 1)
                    ->where('m.id', $head->id)
                    ->get();

        return collect([
            "head" => $head,
            "data" => $data
        ]);
        
    }

    protected function _generateUniqueCode($userId, $merchantId ,$prefix, $plus)
    {
        $merchant = Merchant::find($merchantId);
        $branch = $this->_getHeadAndBranch($merchantId);
        
        $userIds = [$branch['head']->md_user_id];
        foreach($branch['data'] as $key => $item){
            array_push($userIds, $item->branch_user_id);
        }

        $incremental = CustomerEntity::whereIn('md_user_id', $userIds)->count() + $plus;

        $code = $prefix.sprintf("%07s", $incremental);

        $check = CustomerEntity::where('code', $code)
                    ->whereIn('md_user_id', $userIds)
                    ->first();

        if(is_null($check)){
            return $code;
        } else {
            return $this->_generateUniqueCode($userId, $merchantId, $prefix, $plus + 1);
        }
    }

    protected function _generateQueueCode($merchantId, $rDate, $plus)
    {
        $cDate = date('ymd', strtotime($rDate));
        $total = ClinicReservation::where('md_merchant_id', $merchantId)
                                            ->whereRaw("reservation_date::date between '$rDate' and '$rDate'")
                                            ->get();
        $totalData = ClinicReservation::where('md_merchant_id', $merchantId)
                                    ->get();  
        $rowReservation = count($totalData) + $plus;
        $reservationNumber = $merchantId.'-'.$cDate.'-'.sprintf("%02s", $rowReservation);

        $check = ClinicReservation::where('md_merchant_id', $merchantId)
                                    ->whereRaw("reservation_date::date between '$rDate' and '$rDate'")
                                    ->where('reservation_number', $reservationNumber)
                                    ->first();
        if(is_null($check)){
            return [
                "reservation_number" => $reservationNumber
            ];
        } else {
            return $this->_generateQueueCode($merchantId, $rDate, $plus + 1);
        }
    }

    public function create(Request $request)
    {
        try {
            $merchantId = $request->merchant_id;
            $merchant = Merchant::find($merchantId);
            $id = $request->id;
            $userId = $merchant->md_user_id;
            $page = $request->page;
            $customerId = $request->member;
            $code = $this->_generateUniqueCode($userId, $merchantId,'PS-', 1);

            $acceptedPage = ['cust', 'service', 'print'];

            if(!in_array($page, $acceptedPage)){
                return abort(404);
            }

            if(!is_null($id)){
                $data=ReservationEntity::with(['getMerchant', 'getCustomer', 'getProduct', 'getStaff.getUser'])
                                        ->where('is_deleted', 0)
                                        ->where('id', $id)
                                        ->first();
                if(is_null($data)){
                    return abort(404);
                }
                $customer = CustomerEntity::find($data->getCustomer->id);
            }else{
                $data=new ReservationEntity();
                $customer = is_null($customerId)? new CustomerEntity(): CustomerEntity::where('md_user_id', $userId)
                                                                                        ->where('id', $customerId)
                                                                                        ->where('is_deleted', 0)
                                                                                        ->first();
            }

            if(is_null($customer)){
                return abort(404);
            }

            $service = Product::where('md_user_id', $userId)
                                ->where('is_with_stock', 0)
                                ->where('is_deleted', 0)
                                ->get();
            $staff = MerchantStaff::with(['getUser' => function($query){
                                        $query->where('md_role_id', 23);
                                    }])
                                    ->where('md_merchant_id', $merchantId)
                                    ->where('is_deleted', 0)
                                    ->where('is_active', 1)
                                    ->get();

            $doctor = $staff->filter(function($item){
                return (!is_null($item->getUser));
            });

            $params=[
                'title'=>'FORM RESERVASI',
                'data'=>$data,
                'jobs' => Job::all(),
                'religions' => generateReligion(),
                'mediaInfo' => CustomerEntity::generateMediaInfo(),
                'maritalStatus' => CustomerEntity::generateMaritalStatus(),
                'page' => $page,
                'customerId' => $customerId,
                'service' => $service,
                'doctor' => $doctor,
                'customer' => $customer,
                'code' => $code,
                'merchantId' => $merchantId,
                'userId' => $userId
            ];

            return view('reservation.form-create',$params);
        } catch(\Exception $e)
        {
            return abort(404);
        }
    }

    public function add(Request $request)
    {
        try {
            $merchantId = $request->merchant_id;
            $id = $request->id;
            $merchant = Merchant::find($merchantId);
            $userId = $merchant->md_user_id;
            $page = $request->page;
            $customerId = $request->member;

            $acceptedPage = ['cust', 'service', 'print'];

            if(!in_array($page, $acceptedPage)){
                return abort(404);
            }

            if($page == 'service'){
                if(is_null($customerId)){
                    return abort(404);
                }
            }

            if(!is_null($id)){
                $data=ReservationEntity::find($id);
                if(is_null($data)){
                    return(abort(404));
                }
                $customer = CustomerEntity::find($data->getCustomer->id);
            }else{
                $data=new ReservationEntity();

                $customer = (is_null($customerId))? new CustomerEntity() : CustomerEntity::select([
                                                                                "sc_customers.id",
                                                                                "sc_customers.name as name",
                                                                                "sc_customers.code as code",
                                                                                "sc_customers.gender as gender"
                                                                            ])
                                                                            ->join('md_users as u', 'u.id', 'sc_customers.md_user_id')
                                                                            ->join('md_merchants as m', 'm.md_user_id', 'u.id')
                                                                            ->whereIn('m.id',MerchantUtil::getBranch($merchant->id,1))
                                                                            ->where('sc_customers.is_deleted', 0)
                                                                            ->where('sc_customers.id', $customerId)
                                                                            ->first();
            }

            if(is_null($customer)){
                return abort(404);
            }

            $service = Product::where('md_user_id', $userId)
                                        ->where('is_with_stock', 0)
                                        ->where('is_deleted', 0)
                                        ->get();
            $staff = MerchantStaff::with(['getUser' => function($query){
                                        $query->where('md_role_id', 23);
                                    }])
                                    ->where('md_merchant_id', $merchantId)
                                    ->where('is_deleted', 0)
                                    ->get();
            
            $doctor = $staff->filter(function($item){
                return (!is_null($item->getUser));
            });

            $params=[
                'title'=>'FORM RESERVASI',
                'data'=>$data,
                'service' => $service,
                'doctor' => $doctor,
                'merchant' => $merchant,
                'page' => $page,
                'merchantId' => $merchantId,
                'userId' => $userId,
                'customer' => $customer,
                'customerId' => $customerId
            ];

            return view('reservation.form-add',$params);
        } catch(\Exception $e)
        {
            return abort(404);
        }
    }

    public function saveMember(Request $request)
    {
        try {
            date_default_timezone_set($request->_timezone);
            $customerId=$request->sc_customer_id;
            $merchantId = $request->merchant_id;
            $id = $request->id; // reservation id

            if(!is_null($customerId)){
                $data=CustomerEntity::find($customerId);
            }else{
                $data=new CustomerEntity();
            }

            $data->code=$request->code;
            $data->name=$request->name;
            $data->md_user_id=$request->md_user_id;
            $data->email=$request->email;
            $data->phone_number=$request->phone_number;
            $data->address=$request->address;
            $data->type_address = $request->type_address;
            $data->city_name = $request->city_name;
            $data->region_name = $request->region_name;
            $data->postal_code = $request->postal_code;
            $data->gender = $request->gender;
            $data->sc_customer_level_id=$request->sc_customer_level_id;
            $data->identity_card_number = $request->identity_card_number;
            $data->date_of_birth = $request->date_of_birth;
            $data->md_job_id = $request->md_job_id;
            $data->media_info = $request->media_info;
            $data->religion = $request->religion;
            $data->marital_status = $request->marital_status;
            $data->age = $request->age;

            $data->save();

            if(is_null($id)){
                return response()->json([
                    'message'=>'Member berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=> route('reservation.create', ['merchant_id' => $merchantId,'page'=>'service','member'=>$data->id])
                ]);
            } else {
                return response()->json([
                    'message'=>'Member berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=> route('reservation.create', ['merchant_id' => $merchantId,'id'=>$id,'page'=>'service','member'=>$data->id])
                ]);
            }

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }


    public function redirectMember(Request $request)
    {
        try {
            $id = $request->id; // reservation id
            $merchantId = $request->merchant_id;
            $merchant = Merchant::find($merchantId);
            $customerId = $request->customer_id;
            $userId = $merchant->md_user_id;

            $checkCustomer = CustomerEntity::select([
                "sc_customers.id",
                "sc_customers.name as name",
                "sc_customers.code as code",
                "sc_customers.gender as gender"
            ])
            ->join('md_users as u', 'u.id', 'sc_customers.md_user_id')
            ->join('md_merchants as m', 'm.md_user_id', 'u.id')
            ->whereIn('m.id',MerchantUtil::getBranch($merchant->id,1))
            ->where('sc_customers.is_deleted', 0)
            ->where('sc_customers.id', $customerId)
            ->first();
            
            if(!is_null($id)){
                $data = ReservationEntity::find($id);
                if(is_null($data)){
                    return response()->json([
                        'message'=>'Data reservasi tidak ditemukan !',
                        'type'=>'warning',
                        'is_modal'=>true,
                        'redirect_url'=>''
                    ]);    
                }
            } else {
                $data = new ReservationEntity();
            }

            if(is_null($checkCustomer)){
                return response()->json([
                    'message'=>'Member tidak ditemukan !',
                    'type'=>'warning',
                    'is_modal'=>true,
                    'redirect_url'=>''
                ]);    
            }
            return response()->json([
                'message'=>'Data berhasil disimpan !',
                'type'=>'success',
                'is_modal'=>false,
                'redirect_url'=>route('reservation.add', ['merchant_id' => $merchantId,'id' => $data->id, 'page'=>'service', 'member'=>$checkCustomer->id])
            ]);


        } catch(\Exception $e)
        {   
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function save(Request $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->_timezone);
            $id=$request->id;
            $rDate = $request->reservation_date;
            $cDate = date('ymd', strtotime($rDate));
            $merchantId = $request->merchant_id;

            $merchant = Merchant::find($merchantId);
            $userId = $merchant->md_user_id;
            $code = $this->_generateUniqueCode($userId, $merchantId,'PS-', 1);


            $merchantStaff = MerchantStaff::find($request->md_merchant_staff_id);
            $permit = DB::select("
            select
                id
            from 
                md_merchant_staff_permit_submissions mmsps
            where
                md_staff_user_id = $merchantStaff->md_user_id
                and
                mmsps.start_date < '$request->reservation_date'
                and
                mmsps.end_date > '$request->reservation_date'
            ");

            if(count($permit) > 0){
                return response()->json([
                    'message'=>'Dokter tidak bertugas pada saat tanggal reservasi yang anda pilih !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }


            if(!is_null($id)){
                $data=ClinicReservation::find($id);
                if($data->status != 0){
                    return response()->json([
                        'message'=>'Data reservasi tidak dapat diubah dkarenakan reservasi sedang berjalan / sudah selesai !',
                        'type'=>'warning',
                        'is_modal'=>false,
                        'redirect_url'=>''
                    ]); 
                }
            }else{
                $data=new ClinicReservation();
            }

            $checkReservationDate = DB::select("
            select
                id,
                to_char(reservation_date, 'HH24') as hour
            from 
                clinic_reservations
            where
                md_merchant_id = $merchantId
                and
                reservation_date::date = '$request->reservation_date'
                and
                is_deleted = 0
            ");

            $hour = Carbon::parse($request->reservation_date)->format('H');
            
            if(in_array($hour, array_column($checkReservationDate, 'hour')) && !in_array($data->id, array_column($checkReservationDate, 'id'))){
                return response()->json([
                    'message'=>'Tanggal dan waktu reservasi tidak tersedia !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>''
                ]);
            }
            
            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return response()->json([
                    'message' => $error,
                    'type' => 'warning',
                    'is_modal' => false,
                    'redirect_url' => ''
                ]);

            }
            
            if($request->reservation_date < date('Y-m-d h:i:s')){
                return response()->json([
                    'message'=>'Tanggal reservasi tidak boleh kurang dari sekarang !',
                    'type'=>'warning',
                    'is_modal'=>false,
                    'redirect_url'=>route('reservation.create', ['merchant_id' => $merchantId,'id'=>$data->id,'page'=>'print'])
                ]);
            }
                                          
            if(is_null($id)){
                $queue = $this->_generateQueueCode($merchantId, $rDate, 1);
                $data->reservation_number = $queue["reservation_number"];
            }

            $data->md_merchant_id = $merchantId;
            $data->sc_customer_id = $request->sc_customer_id;
            $data->md_merchant_staff_id = $request->md_merchant_staff_id;
            $data->sc_product_id = $request->sc_product_id;
            $data->reservation_date = $request->reservation_date;
            $data->is_type = 1;
            $data->save();

            if(is_null($id)){
                $destinationPath = 'public/uploads/reservation/';
                $link = route('reservation.detail', ["id" => $data->id]);
                $qrCodeFile = $destinationPath.date('YmdHis').'.png';

                $image=QrCode::format('png')->size(400)->generate($link);
                Storage::disk('s3')->put($qrCodeFile,$image);

                $data->qr_code_file = $qrCodeFile;
                $data->qr_code_url = $link;

                $data->save();

                $medicalRecord = new ClinicMedicalRecord();
                $medicalRecord->md_merchant_id = $merchantId;
                $medicalRecord->clinic_reservation_id = $data->id;
                $medicalRecord->save();
            } else {
                $medicalRecord = ClinicMedicalRecord::where('clinic_reservation_id', $data->id)
                                                    ->first();
            }

            $product = Product::find($data->sc_product_id);

            $prodJson = [
                [
                    "name" => $product->name,
                    "price" => $product->selling_price,
                    "quantity" => 1,
                    "sub_total" => $product->selling_price,
                    "sc_product_id" => $product->id
                ]
            ];

            $medicalRecord->service = json_encode($prodJson);
            $medicalRecord->save();

            $customer = CustomerEntity::find($data->sc_customer_id);

            if(is_null($customer->code) || $customer->code == ""){
                $customer->code = $code;
                $customer->save();
            }

            DB::commit();

            if($request->is_from_create == 1){
                return response()->json([
                    'message'=>'Data reservasi berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>route('reservation.create', ['merchant_id' => $merchantId,'id'=>$data->id,'page'=>'print'])
                ]);
            } else {
                return response()->json([
                    'message'=>'Data reservasi berhasil disimpan !',
                    'type'=>'success',
                    'is_modal'=>false,
                    'redirect_url'=>route('reservation.add', ['merchant_id' => $merchantId,'id'=> $data->id, 'page' => 'print'])
                ]);
            }

        }catch (\Exception $e)
        {   
            DB::rollback();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'message'=>'Terjadi kesalahan di server,data gagal disimpan!',
                'type'=>'danger',
                'is_modal'=>true,
                'redirect_url'=>''
            ]);
        }
    }

    public function getCustomer(Request $request)
    {
        try {
            $data = [];
            $userId = $request->user_id;
            $merchant = Merchant::where('md_user_id', $userId)->first();
            $code = $request->code;

            $customer = CustomerEntity::select([
                "sc_customers.id",
                "sc_customers.name as name",
                "sc_customers.code as code",
                "sc_customers.gender as gender"
            ])
            ->join('md_users as u', 'u.id', 'sc_customers.md_user_id')
            ->join('md_merchants as m', 'm.md_user_id', 'u.id')
            ->where('code', $code)
            ->whereIn('m.id',MerchantUtil::getBranch($merchant->id,1))
            ->where('sc_customers.is_deleted',0)
            ->first();

            if(!is_null($customer)){
                if(strtolower($customer->gender) == 'male'){
                    $gender = 'Laki - Laki';
                } else if(strtolower($customer->gender) == 'female'){
                    $gender = 'Perempuan';
                } else {
                    $gender = '-';
                }

                $data = [
                    "id" => $customer->id,
                    "name"=> $customer->name,
                    "code" => $customer->code,
                    "date_of_birth" => (is_null($customer->date_of_birth))? '-':Carbon::parse($customer->date_of_birth)->isoFormat('D MMMM Y'),
                    "gender" => $gender
                ];

                return collect($data);
            } else {
                return false;
            }

        } catch(\Exception $e)
        {
            return false;
        }
    }


    public function detail(Request $request)
    {
        try {
            $id = $request->id;
            $data=ReservationEntity::with(['getMerchant', 'getCustomer', 'getProduct', 'getStaff.getUser'])
                                    ->where('is_deleted', 0)
                                    ->where('id', $id)
                                    ->first();

            $params=[
                'title'=>'Detail Reservasi',
                'data'=>$data,
                'maritalStatus'=> generateMaritalStatus(),
                'religion' => generateReligion()
            ];

            return view('reservation.detail',$params);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return abort(404);
        }
    }

    public function calculateAge(Request $request)
    {
        try {
            $dateOfBirth = $request->date;
            $today = date("Y-m-d");
            $diff = date_diff(date_create($dateOfBirth), date_create($today));

            return $diff->format('%y');
        
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return 0;
        }
    }


    public function printPdf(Request $request)
    {
        try {
            $id = $request->id;
            $data = ReservationEntity::with(['getMerchant', 'getCustomer', 'getProduct', 'getStaff.getUser'])
                                        ->where('is_deleted', 0)
                                        ->where('id', $id)
                                        ->first();

            $params = [
                "title" => "Reservasi ".$data->reservation_number,
                "data" => $data,
            ];

            $pdf = PDF::loadview('reservation.print-pdf', $params)->setPaper('a5');
            return $pdf->stream('Reservasi_'.$data->reservation_number.'_'.date('Y-m-d h:i:s').'.pdf');
        } catch(\Exception $e)
        {
            return abort(404);
        }
    }
}

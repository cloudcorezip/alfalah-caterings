<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Backend\Confirmation\Account;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\User;
use App\Utils\Account\AccountUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class EmailActivationController extends Controller
{

    public function routeWeb()
    {
        Route::get('account/email-verification', 'Backend\Confirmation\Account\EmailActivationController@emailVerification');

    }


    public function emailVerification(Request $request)
    {
        $params=AccountUtil::emailVerification($request);
        return view('pages.account.email-successfull-confirmation',$params);
    }

}

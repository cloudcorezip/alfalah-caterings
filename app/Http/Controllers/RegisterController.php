<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers;


use App\Mails\Account\EmailActivation;
use App\Models\MasterData\BusinessCategory;
use App\Models\MasterData\MerchantMenu;
use App\Models\MasterData\User;
use App\Models\MasterData\Merchant;
use App\Utils\Account\AccountUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Validator;
use App\Models\MasterData\TypeOfBusiness;
use App\Models\MasterData\SennaCashier\Category;
use App\Models\MasterData\MerchantShopCategory;
use Illuminate\Support\Facades\DB;
use App\Models\MasterData\Region\Province;
use App\Models\MasterData\Region\City;
use App\Models\MasterData\Region\District;
use Ramsey\Uuid\Uuid;

class RegisterController extends Controller
{
    protected $user;
    public function __construct()
    {
        $this->user=User::class;
    }

    public function routeWeb()
    {
        Route::prefix('register')->group(function (){
            Route::get('/','RegisterController@index')->name('register');
            Route::post('/validate-register','RegisterController@validateRegister')
                ->name('register.validate');
            Route::get('/lengkapi-profil/{id}','RegisterController@completeProfile')
                ->name('register.lengkapi-profil');
            Route::post('/save', "RegisterController@save")
                ->name('register.save');
            Route::get('/get-city', 'RegisterController@getCity')
                ->name('register.get-city');
            Route::get('/get-district', 'RegisterController@getDistrict')
                ->name('register.get-district');
            Route::get('/complete-register', 'RegisterController@completeDataRegister')
                ->name('register.complete-data');
        });

    }

    public function index(Request $request)
    {
        $type=TypeOfBusiness::All();
        $categories =BusinessCategory::whereNull('parent_id')->with('getChild')->get();
        $user = null;

        if(!is_null($request->step) && !is_null($request->key)){
            $user = User::where('id',decrypt($request->key))->first();
            if(is_null($user)){
                return redirect('register');
            }
        }

        $params = [
            'title' => 'Register',
            'key'=>config('merchant.toko'),
            'type' => $type,
            "provinces" => Province::all(),
            'step' => is_null($request->step)? null : $request->step,
            'user' => $user,
            'categories' => $categories
        ];
        return view('register.index', $params);
    }

    public function validateRegister(Request $request)
    {
        DB::beginTransaction();
        try{
            $user=$this->user::whereRaw("lower(email)='".strtolower($request->email)."'")
            ->where(['md_role_id'=>3])->first();

            if(!is_null($user) && $request->is_from_google == 0){
                return "<div class='alert alert-warning' style='text-align: center'>Email Sudah Digunakan. Silahkan Login !</div>
                ";
            }

            if($request->is_from_google == 0 && strlen($request->password) < 6){
                return "<div class='alert alert-warning' style='text-align: center'>Panjang password minimal 6 karakter</div>";
            }

            if($request->is_from_google == 0 && strlen($request->password) < 6){
                return "<div class='alert alert-warning' style='text-align: center'>Panjang password minimal 6 karakter</div>";
            }

            if($request->is_from_google == 0){
                $validator = Validator::make($request->all(), [
                    'confirmPassword' => 'same:password'
                ]);

                if($validator->fails()){
                    return "<div class='alert alert-warning' style='text-align: center'>Konfirmasi Password Tidak Sesuai</div>";
                }

            }


            $isFromGoogle=is_null($request->is_from_google)?0:$request->is_from_google;
            $isMerchant=$request->type;
            $data=new $this->user;
            $validator = Validator::make($request->all(),($isFromGoogle==1)?$data->registerWithGmail:$data->registerWithoutGmail);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404,$error,[]);
            }

            if($isMerchant==0)
            {
                $user=$this->user::whereRaw("lower(email)='".strtolower($request->email)."'")
                    ->where(['md_role_id'=>1])->first();
            }else{
                $user=$this->user::whereRaw("lower(email)='".strtolower($request->email)."'")->where('md_role_id',$isMerchant)
                    ->where('is_active',1)
                    ->first();
                if(is_null($user))
                {
                    $user=$this->user::whereRaw("lower(email)='".strtolower($request->email)."'")
                        ->where('md_role_id','>=',11)
                        ->first();
                }

            }
            if(is_null($user))
            {
                if(AccountUtil::checkEmail($request->email,($isMerchant==0)?1:$isMerchant)==1){
                    return "<div class='alert alert-warning' style='text-align: center'>".trans('custom.email_already_use')."</div>";

                }
                $params=$request->all();
                unset($params['type']);
                $params['token']=Hash::make($params['email'].date('YmdHis').Uuid::uuid4()->toString());
                $user=new $this->user;
                $user->fullname=$request->fullname;
                $user->email=strtolower($params['email']);
                $user->token=$params['token'];
                $user->md_role_id=($isMerchant==0)?1:$isMerchant;
                $user->is_merchant=($isMerchant==0)?0:1;

                $user->balance=0;
                if($isFromGoogle==1)
                {
                    $user->is_active=1;
                    $user->is_email_verified=1;
                    $user->gmail_information=json_encode(json_decode($request->gmail_information));
                    $user->is_from_google=1;
                    $user->save();
                }else{
                    $user->password=Hash::make($params['password']);
                    $user->save();
                }

                $merchant=new Merchant();
                $merchant->name = $request->merchant_name;
                $merchant->email_merchant = $user->email;
                $merchant->md_app_service_id = 2;
                $merchant->md_user_id = $user->id;
                $merchant->md_inventory_method_id=2;
                $merchant->md_business_category_id=$request->md_sc_category_id;
                $merchant->start_subscription=
                $merchant->save();
                DB::commit();


                $userEncrypt=encrypt($user->id);
                $roleEncrypt=encrypt($user->md_role_id);

                Session::put(config('user.user'),$userEncrypt);
                Session::put(config('user.role'),$roleEncrypt);
                Artisan::call('acc:build', ['--merchant' =>$merchant->id]);

                $merchantEncrypt=encrypt($merchant->id);
                Session::put(config('user.merchant'),$merchantEncrypt);

                if($isFromGoogle==0)
                {
                    $link=url('/account/email-verification').'?key='.$params['token'];
                    $email=new EmailActivation($user->fullname,$link);
                    Mail::to($user->email)->send($email);
                }
                return "<div class='alert alert-success' style='text-align: center'>".trans('custom.registration_success')."</div> <script>setTimeout(() => {window.location.href = '".route('merchant.dashboard')."'
                    }, 3000)</script>";

            } else {
                DB::rollBack();
                return "<div class='alert alert-warning' style='text-align: center'>".trans('custom.registration_errors')."</div>";

            }

        }catch(\Exception $e){
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-warning' style='text-align: center'>".trans('custom.registration_errors')."</div><script>reload(3000);</script>";
        }
    }

}

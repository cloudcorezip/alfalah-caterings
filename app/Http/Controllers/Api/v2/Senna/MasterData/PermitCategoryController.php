<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\MasterData;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\PermitCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class PermitCategoryController extends Controller
{
    protected $permitCategory;
    protected $message;

    public function __construct()
    {
        $this->permitCategory=PermitCategory::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/master-data/permit-category')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/all', 'Api\v2\Senna\MasterData\PermitCategoryController@getAll');
                Route::get('/one/{id}', 'Api\v2\Senna\MasterData\PermitCategoryController@getOne');
            });
    }

    public function getAll(Request $request)
    {
        try {

            $data=$this->permitCategory::where('is_deleted',0)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {

            $data=$this->permitCategory::find($id);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

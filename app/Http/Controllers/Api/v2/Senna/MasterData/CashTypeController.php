<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\MasterData;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\SennaCashier\CashType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class CashTypeController extends Controller
{
    protected $cashType;
    protected $message;

    public function __construct()
    {
        $this->cashType=CashType::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/master-data/cash-type')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/all', 'Api\v2\Senna\MasterData\CashTypeController@getAll');
                Route::get('/one/{id}', 'Api\v2\Senna\MasterData\CashTypeController@getOne');
            });
    }

    public function getAll(Request $request)
    {
        try {

            $data=$this->cashType::where('is_deleted',0)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {

            $data=$this->cashType::find($id);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\MasterData;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Utils\Merchant\MerchantUtil;


class MerchantController extends Controller
{
    protected $merchant;
    protected $message;

    public function __construct()
    {
        $this->merchant=Merchant::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/master-data/merchant')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/all', 'Api\v2\Senna\MasterData\MerchantController@getAll');
                Route::get('/all-branch/{id}', 'Api\v2\Senna\MasterData\MerchantController@getAllBranch');
            });
    }

    public function getAll(Request $request)
    {
        try {

            $data=$this->merchant::select([
                'md_merchants.name as merchant_name',
                'u.email as email_merchant',
                'md_merchants.phone_merchant',
                'd.name as district_name',
                'md_merchants.created_at as tanggal_pendaftaran',
                DB::raw('(CASE WHEN md_merchants.is_free = 0 AND md_subscription_id IS NULL THEN 0
                               WHEN md_merchants.is_free = 0 AND md_subscription_id IS NOT NULL THEN 1
                                ELSE 2 END) AS status')

            ])
                ->join('md_users as u','md_merchants.md_user_id','u.id')
                ->leftJoin('md_districts as d','md_merchants.md_district_id','d.id')
                ;

            if(!is_null($request->status)){
                if($request->status==0){
                    $data->where(function($query){
                        $query->where('md_merchants.is_free',0)->whereNull('md_subscription_id');
                    });
                }elseif($request->status==1){
                    $data->where(function($query){
                        $query->where('md_merchants.is_free',0)->whereNotNull('md_subscription_id');
                    });
                }else{
                    $data->where(function($query){
                        $query->where('md_merchants.is_free',1)->whereNull('md_subscription_id');
                    });
                }
                
            }

            if(!is_null($request->month)){
                $data->whereMonth('md_merchants.created_at', $request->month);
            }

            if(!is_null($request->year)){
                $data->whereYear('md_merchants.created_at', $request->year);
            }


            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data->get());
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getAllBranch($id){
        try{
            $branch=MerchantUtil::getBranch($id,1);
            // $res=[];
            // if($request->is_only_branch==1){
            //     foreach($branch as $item){
            //         if($id!=$item){
            //             $res[]=$item;
            //         }
            //     }
            // }else{
            //    $res = $branch;
            // }


            return $this->message::getJsonResponse(200,trans('message.201'),$branch);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\Material;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\ProductSellingLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Utils\Merchant\ProductUtil;


class ProductRawMaterialController extends Controller
{
    protected $material;
    protected $merchant;
    protected $product;
    protected $message;

    public function __construct()
    {
        $this->material=Material::class;
        $this->merchant=Merchant::class;
        $this->product=Product::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/product/raw-material')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create/{productId}', 'Api\v2\Senna\Merchant\Cashier\ProductRawMaterialController@create');
                Route::get('/all/{productId}', 'Api\v2\Senna\Merchant\Cashier\ProductRawMaterialController@getAll');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductRawMaterialController@delete');
                Route::post('/delete-v2/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductRawMaterialController@deleteV2');
                Route::post('/update/{productId}', 'Api\v2\Senna\Merchant\Cashier\ProductRawMaterialController@update');

            });
    }

    public function getAll($productId)
    {
        try {
            $data=$this->material::
            select([
                'sc_product_raw_materials.*',
                'p.name as material_product_name',
                'p.purchase_price',
                'p.selling_price',
                'p.is_with_stock'
            ])->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
            ->where('sc_product_raw_materials.parent_sc_product_id',$productId)
                ->where('sc_product_raw_materials.is_deleted',0)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


    public function create(Request  $request,$productId)
    {
        try {
            DB::beginTransaction();
            $timestamp=date('Y-m-d H:i:s');

            $rawMaterial=$request->raw_material;
            $product=$this->product::find($productId);
            //dd($priceType);
            Product::where('id',$productId)->update(['md_sc_product_type_id' => 4]);
            if(!empty($rawMaterial))
            {
                $insert=[];
                $newAssign=[];
                foreach ($rawMaterial as $key =>$item)
                {
                    $checkMaterial=$this->material::where('child_sc_product_id',$item['child_sc_product_id'])->where('parent_sc_product_id',$productId)->first();
                   if(!is_null($checkMaterial)){
                    return $this->message::getJsonResponse(404,'Bahan baku sudah ada!',[]);
                   }
                    $data=new $this->material();
                    $data->unit_name=$item['unit_name'];
                    $data->quantity=$item['quantity'];
                    $data->total=$item['total'];
                    $data->child_sc_product_id=$item['child_sc_product_id'];
                    $data->parent_sc_product_id=$productId;
                    $data->created_at=$timestamp;
                    $data->updated_at=$timestamp;
                    $data->save();
                    
                    $jsonIdBranch=json_decode($product->assign_to_branch);

                    if(!is_null($product->assign_to_branch) || $jsonIdBranch!=[]){
                        foreach($jsonIdBranch as $i){
                            $merchant=$this->merchant::find($i);
                            $duplicate=ProductUtil::duplicateProductMobile($data,[],null,$merchant);
                            // dd($duplicate);
                        }                    
                    }

                    $insert[]=$data;
                }

                DB::commit();  
                return $this->message::getJsonResponse(200,trans('message.200'),[$insert]);
            }else{
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function delete($id)
    {
        try {
            $data=$this->material::find($id);
            $data->is_deleted=1;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function deleteV2($id)
    {
        try {
            DB::beginTransaction();
            $data=$this->material::find($id);
            if(!is_null($data->getParent->created_by_merchant) && !is_null($request->md_merchant_id)){
                if($data->getParent->created_by_merchant!=$request->md_merchant_id){
                    return $this->message::getJsonResponse(404,'Cabang tidak diperkenankan mengubah produk yang dibuat oleh pusat',[]);
                }
            }
            $data->is_deleted=1;
            $data->save();

            $jsonAssign=json_decode($data->assign_to);
            if(!is_null($data->assign_to) || $jsonAssign!=[]){
                $this->material::whereIn('id', $jsonAssign)->update(['is_deleted' => 1]);
                  
            }
            DB::commit(); 
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function update(Request  $request,$productId)
    {
        try {
            DB::beginTransaction();
            $timestamp=date('Y-m-d H:i:s');

            $this->material::where('parent_sc_product_id',$productId)->delete();

            $product=$this->product::find($productId);
            $jsonIdParent=json_decode($product->assign_to_product);

            if(!is_null($product->assign_to_product) || $jsonIdParent!=[]){
                $this->material::whereIn('parent_sc_product_id',$jsonIdParent)->delete();
            }

            $rawMaterial=$request->raw_material;

            if(!empty($rawMaterial))
            {
                $insert=[];
                $newAssign=[];
                foreach ($rawMaterial as $key =>$item)
                {
                    $data=new $this->material();
                    $data->unit_name=$item['unit_name'];
                    $data->quantity=$item['quantity'];
                    $data->total=$item['total'];
                    $data->child_sc_product_id=$item['child_sc_product_id'];
                    $data->parent_sc_product_id=$productId;
                    $data->created_at=$timestamp;
                    $data->updated_at=$timestamp;
                    $data->save();
                    
                    $jsonIdBranch=json_decode($product->assign_to_branch);

                    if(!is_null($product->assign_to_branch) || $jsonIdBranch!=[]){
                        foreach($jsonIdBranch as $i){
                            $merchant=$this->merchant::find($i);
                            $duplicate=ProductUtil::duplicateProductMobile($data,[],null,$merchant);
                            if($duplicate==false){
                                return $this->message::getJsonResponse(404,'Terjadi kesalahan saat duplikasi ke cabang!',[]);
                            }
                        }                    
                    }

                    $insert[]=$data;
                }
                DB::commit();
                return $this->message::getJsonResponse(200,trans('message.200'),[$insert]);
            }else{
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
           

        }
        catch (\Exception $e)
        {
           
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }



}

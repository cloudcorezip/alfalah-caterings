<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantCashdrawer;
use App\Models\SennaToko\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class CashdrawerController extends Controller
{

    protected $message;
    protected $cashdrawer;

    public function __construct()
    {
        $this->cashdrawer=MerchantCashdrawer::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/cashdrawer')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\CashdrawerController@create');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\CashdrawerController@update');
                Route::get('/all/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\CashdrawerController@getAll');
                Route::get('/one/{id}', 'Api\v2\Senna\Merchant\Cashier\CashdrawerController@getOne');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\CashdrawerController@delete');
            });
    }

    public function getAll($merchantId)
    {
        try {
            $data=$this->cashdrawer::where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->get();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {
            $data=$this->cashdrawer::where('id',$id)
                ->where('is_deleted',0)
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


    public function create(Request  $request)
    {
        try{
            $data = new $this->cashdrawer;

            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $data->name=$request->name;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->desc=$request->desc;
            $data->is_active=$request->is_active;
            $data->is_deleted=0;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }
    public function update(Request  $request,$id)
    {
        try{
            $data = $this->cashdrawer::find($id);

            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $data->name=$request->name;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->desc=$request->desc;
            $data->is_active=$request->is_active;
            $data->is_deleted=0;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }

    public function delete($id)
    {
        try {
            $data =$this->cashdrawer::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $data->is_deleted=1;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

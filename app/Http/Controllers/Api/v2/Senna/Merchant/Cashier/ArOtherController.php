<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Utils\Accounting\AccUtil;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Supplier;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Product;
use App\Utils\Accounting\CoaUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class ArOtherController extends Controller
{

    protected $message;
    protected $supplier;
    protected $customer;
    protected $product;
    protected $ar;
    protected $arDetail;

    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->customer=Customer::class;
        $this->supplier=Supplier::class;
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->arDetail=MerchantArDetail::class;

    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/ar-other')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\ArOtherController@create');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\ArOtherController@delete');
                Route::delete('/delete-detail/{id}', 'Api\v2\Senna\Merchant\Cashier\ArOtherController@deleteDetail');

            });
    }

    public function create(Request $request){
        try{
            DB::beginTransaction();

            $code=CodeGenerator::generateArCode(0,'ARC',$request->merchant_id);
            $getCoa=CoaUtil::checkOfCoaArAp($request->merchant_id);
            // dd($getCoa);
            if(is_null($request->payment_method))
            {
                return $this->message::getJsonResponse(404,'Metode pembayaran belum dipilih',[]);
            }
            $trans=explode('_',$request->payment_method);

            if($request->user_type=='supplier'){
                $user=Supplier::find($request->user_id);
                $is_supplier=1;
            }else{
                $user=Customer::find($request->user_id);
                $is_supplier=0;
            }

            $destinationPath = 'public/uploads/merchant/'.$request->merchant_id.'/acc/jurnal/ar/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }

            date_default_timezone_set($request->timezone);

            if(is_null($request->pay_json)){
                    $data = new $this->ar;

                    $data->arable_id=0;
                    $data->arable_type="#";
                    $data->ar_code=$code;
                    $data->ar_name=$request->note;
                    $data->ar_note=$request->note;
                    $data->ar_purpose=$user->name;
                    $data->ar_time=date('Y-m-d H:i:s');
                    $data->ar_acc_coa=$getCoa->coa_ar_other;
                    $data->sync_id=Uuid::uuid4()->toString();
                    $data->md_merchant_id=$request->merchant_id;
                    $data->timezone=$request->timezone;
                    $data->ar_amount=$request->amount;
                    $data->ar_proof=$fileName;
                    $data->residual_amount=$request->amount;
                    $data->due_date=$request->due_date;
                    $data->is_from_supplier=$is_supplier;
                    $data->ref_user_id=$request->user_id;
                    if($is_supplier==0)
                    {
                        $data->sc_customer_id=$request->user_id;
                    }
                    $data->save();

                    $arDetail=new $this->arDetail;
                    $arDetail->acc_merchant_ar_id=$data->id;
                    $arDetail->paid_nominal=$request->amount;
                    $arDetail->paid_date=date('Y-m-d H:i:s');
                    $arDetail->note=$request->note;
                    $arDetail->timezone=$request->timezone;
                    $arDetail->trans_proof=$fileName;
                    $arDetail->created_by=$request->user_id;
                    $arDetail->md_transaction_type_id=$trans[0];
                    $arDetail->ar_detail_code=CodeGenerator::generateJurnalCode(0,'PTR',$request->merchant_id);;
                    $arDetail->type_action=3;
                    $arDetail->save();

                $jurnal=new Jurnal();
                $jurnal->ref_code=$arDetail->ar_detail_code;
                $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$request->merchant_id);
                $jurnal->trans_name='Pemberian Piutang Usaha '.$data->ar_code.'-'.$arDetail->ar_detail_code;
                $jurnal->trans_time=$arDetail->paid_date;
                $jurnal->trans_note='Pemberian Piutang Usaha  '.$data->ar_code.'-'.$arDetail->ar_detail_code;
                $jurnal->trans_purpose='Pemberian Piutang Usaha  '.$data->ar_code.'-'.$arDetail->ar_detail_code;
                $jurnal->trans_amount=$arDetail->paid_nominal;
                $jurnal->trans_proof=$fileName;
                $jurnal->md_merchant_id=$request->merchant_id;
                $jurnal->md_user_id_created=$request->created_by;
                $jurnal->external_ref_id=$arDetail->id;
                $jurnal->flag_name='Pemberian Piutang Usaha  '.$data->ar_code;
                $jurnal->save();
                // dd($trans[1]);
                $toCoa=$trans[1];
                $insert=[
                    [
                        'acc_coa_detail_id'=>$getCoa->coa_ar_other,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$arDetail->paid_nominal,
                        'created_at'=>$arDetail->paid_date,
                        'updated_at'=>$arDetail->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$arDetail->paid_nominal,
                        'created_at'=>$arDetail->paid_date,
                        'updated_at'=>$arDetail->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                ];
                JurnalDetail::insert($insert);
                $result=$data;
            }else{
                $result=[];
                foreach(json_decode($request->pay_json) as $key => $item){
                    $data = $this->ar::find($item->id);
                    if($item->amount>$data->residual_amount)
                    {
                        return $this->message::getJsonResponse(404,'Pembayaran lebih besar dari sisa piutang!',[]);
                    }
                    if($data->arable_id!=0 && $data->arable_type!='#'){
                        if($item->amount>=$data->residual_amount)
                        {
                            $data->is_paid_off=1;
                            $data->arable->md_sc_transaction_status_id=TransactionStatus::PAID;
                            $data->arable->save();
                        }else{
                            $data->arable->md_sc_transaction_status_id=6;
                            $data->arable->save();
                        }
                    }

                    $data->residual_amount-=$item->amount;
                    $data->paid_nominal+=$item->amount;
                    $data->save();

                    $arDetail=new MerchantArDetail();
                    $arDetail->ar_detail_code=CodeGenerator::generateJurnalCode(0,'PTR',$request->merchant_id);
                    $arDetail->paid_nominal=$item->amount;
                    $arDetail->paid_date=date('Y-m-d H:i:s');
                    $arDetail->note=$request->note;
                    $arDetail->acc_merchant_ar_id=$data->id;
                    $arDetail->created_by=$request->created_by;
                    $arDetail->md_transaction_type_id=$trans[0];
                    $arDetail->timezone=$request->timezone;
                    $arDetail->note=$request->note;
                    $arDetail->trans_coa_detail_id=$trans[1];
                    $arDetail->trans_proof=$fileName;
                    if($data->arable_id!=0 && $data->arable_type!='#'){
                        $arDetail->type_action=1;
                    }else{
                        $arDetail->type_action=2;
                    }
                    $arDetail->save();

                    if($data->arable_id!=0 && $data->arable_type!='#'){
                        $coa_id=$getCoa->coa_ar_sale;
                        $desc='Pembayaran Penjualan '.$data->arable->code.'-'.$arDetail->ar_detail_code;
                    }else{
                        $coa_id=$getCoa->coa_ar_other;
                        $desc='Pembayaran Piutang '.$data->ar_code.'-'.$arDetail->ar_detail_code;
                    }

                    $jurnal=new Jurnal();
                    $jurnal->ref_code=$arDetail->ar_detail_code;
                    $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$request->merchant_id);
                    $jurnal->trans_name=$desc;
                    $jurnal->trans_time=$arDetail->paid_date;
                    $jurnal->trans_note=$desc;
                    $jurnal->trans_purpose=$desc;
                    $jurnal->trans_amount=$arDetail->paid_nominal;
                    $jurnal->trans_proof=$fileName;
                    $jurnal->md_merchant_id=$request->merchant_id;
                    $jurnal->md_user_id_created=$request->created_by;
                    $jurnal->external_ref_id=$arDetail->id;
                    $jurnal->flag_name=$desc;
                    $jurnal->save();

                    $toCoa=$trans[1];
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$coa_id,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$arDetail->paid_nominal,
                            'created_at'=>$arDetail->paid_date,
                            'updated_at'=>$arDetail->paid_date,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$toCoa,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$arDetail->paid_nominal,
                            'created_at'=>$arDetail->paid_date,
                            'updated_at'=>$arDetail->paid_date,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                    ];
                    JurnalDetail::insert($insert);

                    $result[]=[
                            "id"=>$data->id,
                            "id_payment"=>$arDetail->id,
                            "residual_amount"=>$data->residual_amount,
                            "payment_nominal"=>$arDetail->paid_nominal,
                            "payment_note"=>$arDetail->note,
                            "payment_code"=>$arDetail->ar_detail_code,
                            "paid_date"=>$arDetail->paid_date,
                            "payment_method"=>$request->payment_name,
                            "payment_proof"=>$arDetail->trans_proof
                    ];
                }

            }


            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$result);


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function delete($id)
    {
        try{
            DB::beginTransaction();

            $data=$this->ar::find($id);
            $date = new \DateTime($data->ar_time);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            $data->is_deleted=1;
            $data->save();
            $this->arDetail::where('acc_merchant_ar_id',$id)->update(['is_deleted'=>1]);
            $arDetail=$this->arDetail::where('acc_merchant_ar_id',$id)->get();
            foreach($arDetail as $key => $item){
                Jurnal::where([
                    'external_ref_id'=>$item->id,
                    'ref_code'=>$item->ar_detail_code
                ])->update(['is_deleted'=>1]);
            }
            $this->arDetail::where('acc_merchant_ar_id',$id)->delete();
            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }


    public function deleteDetail($id)
    {
        try{
            DB::beginTransaction();

            $data=$this->arDetail::find($id);

            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            $ar=$this->ar::find($data->acc_merchant_ar_id);
            $ar->residual_amount+=$data->paid_nominal;
            $ar->paid_nominal-=$data->paid_nominal;
            $ar->save();
            Jurnal::where([
                'external_ref_id'=>$data->id,
                'ref_code'=>$data->ar_detail_code
            ])->update(['is_deleted'=>1]);

            $this->arDetail::where('acc_merchant_ar_id',$id)->delete();
            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }
}

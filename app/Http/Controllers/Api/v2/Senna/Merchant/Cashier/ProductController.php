<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\MultiUnit;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\Merk;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\StockInventory;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Merchant\ProductUtil;
use App\Models\Accounting\CoaDetail;
use App\Utils\BranchConfig\BranchConfigUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\Accounting\CoaMapping;
use Ramsey\Uuid\Uuid;

class ProductController extends Controller
{

    protected $product;
    protected $message;
    protected $inventory;
    protected $material;
    protected $multiunit;
    protected $coamapping;

    public function __construct()
    {
        $this->product=Product::class;
        $this->productCategory=ProductCategory::class;
        $this->merk=Merk::class;
        $this->material=Material::class;
        $this->message=Message::getInstance();
        $this->inventory=StockInventory::class;
        $this->multiunit=MultiUnit::class;
        $this->coamapping=CoaMapping::class;

    }


    public function routeApi()
    {
        Route::prefix('v2/merchant/toko/product')
            ->middleware('etag')

            ->group(function(){
                Route::post('/create/{userId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@create')->middleware('api-verification');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductController@update')->middleware('api-verification');
                Route::post('/all/{userId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@getAll')->middleware('api-verification');
                Route::get('/one/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductController@getOne')->middleware('api-verification');
                Route::get('/all-web/{userId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@getAllWeb')->name('api.product.all');
                Route::get('/one-web/{userId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@getByCode')->name('api.product.one');
                Route::get('/all-unit', 'Api\v2\Senna\Merchant\Cashier\ProductController@getMultiUnit')->name('api.unit.all');
                Route::get('/all-so', 'Api\v2\Senna\Merchant\Cashier\ProductController@getAllSo')->name('api.product.so');
                Route::get('/all-web-po/{userId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@getAllWebPurchase')->name('api.product.all-po');
                Route::get('/all-category/{userId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@getAllCategory')->name('api.product.category');
                Route::get('/all-merk/{userId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@getAllMerk')->name('api.product.merk');
                Route::get('/all-web-cost/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@getCostPackage')->name('api.cost.all');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductController@delete')->middleware('api-verification');
                Route::post('/delete-v2/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductController@deleteV2')->middleware('api-verification');
                Route::post('/stock-log/{productId}', 'Api\v2\Senna\Merchant\Cashier\ProductController@logStock')->middleware('api-verification');
                Route::get('/child', 'Api\v2\Senna\Merchant\Cashier\ProductController@getChildPackage')->name('api.product.child');

            });

    }

    public function getAll($userId,Request $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $data=$this->product::where('md_user_id',$userId)
                ->with(['getMerk','getCategory','getType','getUnit','getGlobalCategory','getProductSellingLevel'])
                ->where('is_deleted',0);


            if($request->type=='string'){
                if($request->search_filter!='-1')
                {
                    $data->whereRaw("upper(name) like '%".strtoupper($request->search_filter)."%' ");
                }

            }else{
                if($request->search_filter!='-1')
                {
                    $data->where('code',$request->search_filter);
                }
            }

            if($request->category_filter!='-1')
            {
                $data->where('sc_product_category_id',$request->category_filter);
            }

            $result=$data->skip($offset)
                ->take($limit)
                ->orderBy('id','desc')
                ->get();

            if($result->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$result);


        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getMultiUnit(Request $request)
    {
        try{

            $data=$this->multiunit::select([
                'sc_product_multi_units.id',
                'm.name as unit_name',
                'price'
            ])
                ->join('md_units as m','m.id','sc_product_multi_units.md_unit_id')
                ->where('sc_product_id',$request->product_id)
                ->where('is_deleted',0)
                ->get();


            $product=$this->product::find($request->product_id);
            $response =[];
            $response[] = [
                "id"=>-1,
                "text"=>"Tanpa Multi Satuan",
                'purchase_price'=>$product->purchase_price,
                "defaultSelected"=>true,
                "selected"=>true,
            ];
            foreach($data as $key => $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->unit_name,
                    'purchase_price'=>$item->price,
                ];
            }


            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);
        }

    }

    public function getByCode($userId,Request $request)
    {
        try{
            $checkCode=$this->product::where('code',$request->code)->where('md_user_id',$userId)->where('is_deleted',0)->get();
            if(count($checkCode)<1){
                return response()->json(0);
            }
            $data=$this->product::select([
                'id',
                'name as text',
                'stock',
                'purchase_price',
                'selling_price',
                'code'
            ])
                ->where('md_user_id',$userId)
                ->where('is_deleted',0)
                ->get();



            $response =[];
            foreach($data as $item){
                if($item->code==$request->code){
                    $response[] = [
                        "id"=>$item->id,
                        "text"=>$item->text.'-'.$item->code,
                        "defaultSelected"=>true,
                        "selected"=>true,
                        'code'=>$item->code,
                        'purchase_price'=>$item->purchase_price,
                        'selling_price'=>$item->selling_price,

                    ];
                }else{
                    $response[] = [
                        "id"=>$item->id,
                        "text"=>$item->text.'-'.$item->code,
                        'code'=>$item->code,
                        'purchase_price'=>$item->purchase_price,
                        'selling_price'=>$item->selling_price
                    ];
                }

            }

            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);
        }

    }

    public function getAllWeb($userId,Request $request)
    {
        try{
            $search = $request->key;

            if($search == ''){
                $data=$this->product::select([
                    'sc_products.id',
                    'sc_products.name as text',
                    'stock',
                    'purchase_price',
                    'selling_price',
                    'code',
                    'inv_id as coa_inv_id',
                    'hpp_id as coa_hpp_id',
                    'u.name as unit_name'
                ])
                    ->join('md_units as u','u.id','sc_products.md_unit_id')
                    ->where('md_user_id',$userId)
                    ->where('is_deleted',0)
                    ->get();
            }else{
                $data=$this->product::select([
                    'sc_products.id',
                    'sc_products.name as text',
                    'stock',
                    'purchase_price',
                    'is_with_stock',
                    'selling_price',
                    'code',
                    'inv_id as coa_inv_id',
                    'hpp_id as coa_hpp_id',
                    'u.name as unit_name'
                ])->join('md_units as u','u.id','sc_products.md_unit_id')
                    ->whereRaw("upper(name) like '%".strtoupper($search)."%' ")
                    ->where('md_user_id',$userId)
                    ->where('is_deleted',0)
                    ->get();
            }

            $response =[];
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->text.'-'.$item->code,
                    'stock'=>$item->stock,
                    'code'=>$item->code,
                    'purchase_price'=>$item->purchase_price,
                    'selling_price'=>$item->selling_price,
                    'coa_inv_id'=>$item->coa_inv_id,
                    'coa_hpp_id'=>$item->coa_hpp_id,
                    'unit_name'=>$item->unit_name
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }

    public function getCostPackage($merchantId,Request $request)
    {
        try{
            $search = $request->key;

            if($search == ''){
                $data=CoaDetail::select([
                    'acc_coa_details.id',
                    'acc_coa_details.name as text'
                ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$merchantId)
                    ->where('c.code','6.1.00')
                    ->whereIn('acc_coa_details.is_deleted',[0,2])
                    ->orderBy('acc_coa_details.id','asc')
                    ->get();

            }else{
                $data=CoaDetail::select([
                    'acc_coa_details.id',
                    'acc_coa_details.name as text'
                ])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$merchantId)
                    ->where('c.code','6.1.00')
                    ->whereRaw("upper(acc.coa_details.name) like '%".strtoupper($search)."%' ")
                    ->whereIn('acc_coa_details.is_deleted',[0,2])
                    ->orderBy('acc_coa_details.id','asc')
                    ->get();
            }


            return response()->json($data);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }

    public function getChildPackage(Request $request)
    {
        try{

                $data=Product::find($request->id);
            if($data->is_with_stock=1){
                $price=$data->purchase_price;
            }else{
                $price=$data->selling_price;
            }
            $response =[];
            $response[] = [
                "id"=>$data->id,
                "purchase_price"=>$price,
                "unit"=>$data->getUnit->name
            ];
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }

    public function getAllSo(Request $request)
    {
        try{
            $userId=$request->userId;
            $warehouse=$request->warehouse;
            $search = strtoupper($request->key);

            if($search == ''){
                $data=DB::select("select
              sp.id,
              sp.code,
              sp.name as text,
              coalesce(s.residual_stock, 0) as real_stock
            from
              sc_products sp
              left join (
                select
                  ssi.sc_product_id,
                  sum(ssi.residual_stock) as residual_stock
                from
                  sc_stock_inventories ssi
                where
                  ssi.inv_warehouse_id = $warehouse
                  and type = 'in'
                  and ssi.transaction_action not like '%Retur%'
                  and ssi.is_deleted = 0
                group by
                  ssi.sc_product_id
              ) as s on s.sc_product_id = sp.id
            where
              sp.md_user_id = $userId
              and sp.is_deleted = 0
        ");
            }else{
                $data=DB::select("select
              sp.id,
              sp.code,
              sp.name as text,
              coalesce(s.residual_stock, 0) as real_stock
            from
              sc_products sp
              left join (
                select
                  ssi.sc_product_id,
                  sum(ssi.residual_stock) as residual_stock
                from
                  sc_stock_inventories ssi
                where
                  ssi.inv_warehouse_id = $warehouse
                  and type = 'in'
                  and ssi.transaction_action not like '%Retur%'
                  and ssi.is_deleted = 0
                group by
                  ssi.sc_product_id
              ) as s on s.sc_product_id = sp.id
            where
              sp.md_user_id = $userId
              and sp.is_deleted = 0
            and upper(sp.name) like '%$search%'
            ");
            }

            $response =[];
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->text.'-'.$item->code,
                    'stock'=>$item->real_stock,
                    'code'=>$item->code,
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);

        }

    }

    public function getAllCategory($userId)
    {
        try{

            $data=$this->productCategory::where('md_user_id',$userId)
                ->where('is_deleted',0)
                ->get();


            $response =[];

            foreach($data as $key => $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->name,
                ];
            }


            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);
        }

    }

    public function getAllMerk($userId)
    {

            $data=$this->merk::where('md_user_id',$userId)
                ->where('is_deleted',0)
                ->get();


            $response =[];

            foreach($data as $key => $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->name,
                ];
            }


            return response()->json($response);

    }

    public function getAllWebPurchase($userId,Request $request)
    {
        $search = $request->key;

        if($search == ''){
            $data=$this->product::select([
                'id',
                'name as text',
                'stock',
                'purchase_price',
                'selling_price',
                'code',
                'inv_id as coa_inv_id',
                'hpp_id as coa_hpp_id',
            ])
                ->where('md_user_id',$userId)
                ->where('is_deleted',0)
                ->where('is_with_stock',1)
                ->where('md_sc_product_type_id','!=',2)
                ->get();
        }else{
            $data=$this->product::select([
                'id',
                'name as text',
                'stock',
                'purchase_price',
                'selling_price',
                'code',
                'inv_id as coa_inv_id',
                'hpp_id as coa_hpp_id',

            ])->whereRaw("upper(name) like '%".strtoupper($search)."%' ")
                ->where('md_user_id',$userId)
                ->where('is_deleted',0)
                ->where('is_with_stock',1)
                ->where('md_sc_product_type_id','!=',2)
                ->get();
        }

        $response =[];
        foreach($data as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->text.'-'.$item->code,
                'stock'=>$item->stock,
                'purchase_price'=>$item->purchase_price,
                'selling_price'=>$item->selling_price,
                'coa_inv_id'=>$item->coa_inv_id,
                'coa_hpp_id'=>$item->coa_hpp_id
            ];
        }
        return response()->json($response);
    }

    public function getOne($id)
    {
        try {

            $data=$this->product::with(['getMerk','getCategory','getType','getUnit','getGlobalCategory',
                'getProductSellingLevel'=>function($q){
                    $q->where('is_deleted','=',0);
                },
                'getMultiUnit'=>function($a){
                    $a->where('is_deleted','=',0)
                    ->orderBy('id', 'ASC');
                }])
                ->find($id);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function create(Request $request,$userId)
    {
        try {
            DB::beginTransaction();
            $data = new $this->product;

            if(BranchConfigUtil::getAccessMobile('master_data',$userId)==false)
            {
                return $this->message::getJsonResponse(404,'Kamu tidak diperkenankan untuk menambah data produk oleh pengaturan multi cabang',[]);

            }

            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $destinationPath = 'public/uploads/merchant/'.$userId.'/product/';

            if($request->hasFile('foto'))
            {
                $file=$request->file('foto');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }
            $checkCode=Product::whereRaw("lower(code) like '".strtolower($request->code)."' ")->where('md_user_id',$userId)->first();
            if(!is_null($checkCode))
            {
                return $this->message::getJsonResponse(404,'Kode produk telah digunakan!',$checkCode->name);
            }
            $data->sync_id=$userId.Uuid::uuid4()->toString();
            $data->name=$request->name;
            $data->foto=$fileName;
            $data->code=$request->code;
            $data->selling_price=$request->selling_price;
            $data->purchase_price=$request->purchase_price;
            $data->weight=(is_null($request->weight))?0:$request->weight;
            $data->stock=0;
            $data->description=$request->description;
            $data->sc_merk_id=$request->sc_merk_id;
            $data->md_sc_product_type_id=$request->md_sc_product_type_id;
            $data->sc_product_category_id=$request->sc_product_category_id;
            $data->md_user_id=$userId;
            $data->is_show_senna_app=$request->is_show_senna_app;
            $data->md_unit_id=$request->md_unit_id;
            $data->md_sc_category_id=$request->md_sc_category_id;
            $data->is_with_stock=$request->is_with_stock;
            $data->is_with_purchase_price=$request->is_with_purchase_price;
            $data->unique_code=$userId.Uuid::uuid4()->toString();
            $data->is_deleted=0;
            $merchant=Merchant::where('md_user_id',$userId)->first();
            $data->created_by_merchant=$merchant->id;
            if($data->is_with_stock==0){
                $data->cost_other=$request->cost_other;
            }
            $data->save();

            if($data->is_with_stock==1)
            {
                $coaProduct=CoaProductUtil::initialCoaProduct($data->getUser->getMerchant->id,$data,1);
                if($coaProduct==false)
                {
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan,inisiasi akun persediaan gagal dibuat!',$data);
                }
                $data->inv_id=$coaProduct['inv_id'];
                $data->hpp_id=$coaProduct['hpp_id'];
                $data->save();
            }


            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }

    }

    public function update($id,Request $request)
    {
        try {
            DB::beginTransaction();

            $data =$this->product::find($id);

            if(!is_null($data->created_by_merchant) && !is_null($request->md_merchant_id)){
                if($data->created_by_merchant!=$request->md_merchant_id){
                    return $this->message::getJsonResponse(404,'Cabang tidak diperkenankan mengubah produk yang dibuat oleh pusat',[]);
                }
            }

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }

            if(BranchConfigUtil::getAccessMobile('master_data',$data->md_user_id)==false)
            {
                return $this->message::getJsonResponse(404,'Kamu tidak diperkenankan untuk menambah data produk oleh pengaturan multi cabang',[]);

            }

            $validator = Validator::make($request->all(), $data->ruleUpdate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $destinationPath = 'public/uploads/merchant/'.$data->md_user_id.'/product/';
            if($request->hasFile('foto'))
            {
                $file=$request->file('foto');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=$data->foto;
            }
            $data->name=$request->name;
            $data->foto=$fileName;
            $data->code=$request->code;
            $data->selling_price=$request->selling_price;
            $data->weight=$request->weight;
            $data->description=$request->description;
            $data->sc_merk_id=$request->sc_merk_id;
            $data->md_sc_product_type_id=$request->md_sc_product_type_id;
            $data->sc_product_category_id=$request->sc_product_category_id;
            $data->is_show_senna_app=$request->is_show_senna_app;
            $data->md_unit_id=$request->md_unit_id;
            $data->md_sc_category_id=$request->md_sc_category_id;
            $data->profit_prosentase=(is_null($request->profit_prosentase))?10:$request->profit_prosentase;
            $data->is_deleted=0;
            if($data->is_with_stock==0){
                $data->cost_other=$request->cost_other;
            }
            $data->save();

            if(!is_null($data->assign_to_branch) || json_decode($data->assign_to_branch)!=[] && json_decode($data->assign_to_branch)!=[""]){
                $updateDuplicateProduct=ProductUtil::updateDuplicateProduct($data->assign_to_product,$data);
                if($updateDuplicateProduct==false){
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan ketika update produk cabang!',[]);
                }
            }

            if($data->is_with_stock==1){
                CoaDetail::where('ref_external_id',$id)->where('ref_external_type',1)->where('name', 'like', '%Persediaan%')->update(['name' => 'Persediaan Produk '.$request->name]);
                CoaDetail::where('ref_external_id',$id)->where('ref_external_type',1)->where('name', 'like', '%Harga Pokok Penjualan%')->update(['name' => 'Harga Pokok Penjualan Produk '.$request->name]);
            }

            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $data =$this->product::find($id);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }

            if(BranchConfigUtil::getAccessMobile('master_data',$data->md_user_id)==false)
            {
                return $this->message::getJsonResponse(404,'Kamu tidak diperkenankan untuk menambah data produk oleh pengaturan multi cabang',[]);

            }

            $data->is_deleted=1;
            $data->save();
            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function deleteV2($id,Request $request)
    {
        try {
            DB::beginTransaction();
            $data =$this->product::find($id);

            if(!is_null($data->created_by_merchant) && !is_null($request->md_merchant_id)){
                if($data->created_by_merchant!=$request->md_merchant_id){
                    return $this->message::getJsonResponse(404,'Cabang tidak diperkenankan menghapus produk yang dibuat oleh pusat',[]);
                }
            }

            $checkDuplicateProduct=ProductUtil::checkDuplicateProduct($data->assign_to_product,$data);
            if($checkDuplicateProduct==false){
                return $this->message::getJsonResponse(404,'Produk pada cabang telah digunakan untuk penjualan tidak dapat dihapus!',[]);
            }

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }

            if(BranchConfigUtil::getAccessMobile('master_data',$data->md_user_id)==false)
            {
                return $this->message::getJsonResponse(404,'Kamu tidak diperkenankan untuk menambah data produk oleh pengaturan multi cabang',[]);

            }

            $data->is_deleted=1;
            $data->save();
            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function logStock($productId,Request $request)
    {
        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $status=($request->status=='in')?'in':$request->status;

            $data=$this->inventory::select([
                'sc_stock_inventories.id',
                'p.name',
                DB::raw("(CASE WHEN sc_stock_inventories.type='initial' THEN 0
                 WHEN sc_stock_inventories.type='in' THEN sc_stock_inventories.total
                 ElSE 0 END
                 ) stock_in"),
                DB::raw("(CASE WHEN sc_stock_inventories.type='initial' THEN 0
                 WHEN sc_stock_inventories.type='out' THEN sc_stock_inventories.total
                 ElSE 0 END
                 ) stock_out"),
                'sc_stock_inventories.purchase_price',
                'p.selling_price',
                'sc_stock_inventories.record_stock',
                'sc_stock_inventories.type',
                'sc_stock_inventories.transaction_action',
                'sc_stock_inventories.created_at',
                'sc_stock_inventories.updated_at',
                'sc_stock_inventories.residual_stock'
            ])->join('sc_products as p','p.id','sc_stock_inventories.sc_product_id')
                ->join('md_merchant_inv_warehouses as w','w.id','sc_stock_inventories.inv_warehouse_id')
                ->where('w.is_default',1)
                ->where('p.id',$productId)
                ->where('sc_stock_inventories.type',$status)
                ->orderBy('sc_stock_inventories.created_at','asc');
            $result=$data->skip($offset)
                ->take($limit)
                ->get();

            if($result->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$result);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

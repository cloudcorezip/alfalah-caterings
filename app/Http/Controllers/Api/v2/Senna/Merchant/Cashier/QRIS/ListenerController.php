<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\QRIS;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\StockSaleMapping;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Inventory\InventoryUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Utils\Winpay\WinpayUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Ramsey\Uuid\Uuid;

class ListenerController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/sandbox/')
            ->group(function(){
                Route::post('/listener', 'Api\v2\Senna\Merchant\Cashier\QRIS\ListenerController@listener')->name('winpay.listener');
                Route::post('/generate', 'Api\v2\Senna\Merchant\Cashier\QRIS\ListenerController@generateKey')->name('winpay.generate');

            });
    }

    public function listener(Request $request)
    {
        try {
            DB::beginTransaction();

            $req=json_decode($request->getContent());
            DB::table('xendit_responses')
                ->insert([
                    'response'=>json_encode($req),
                    'external_code'=>$req->id_transaksi
                ]);
            $saleOrder=SaleOrder::where('code',$req->no_reff)->first();
            $saleOrderId=$saleOrder->id;
            date_default_timezone_set($saleOrder->timezone);

            $warehouse=MerchantWarehouse::where('md_merchant_id',$saleOrder->md_merchant_id)
                ->where('is_default',1)
                ->where('is_deleted',0)
                ->first();

            if($saleOrder->md_sc_transaction_status_id==2)
            {
                return 'ACCEPTED';
            }else{
                $saleOrder->md_sc_transaction_status_id=2;
                $saleOrder->admin_fee=$saleOrder->total*0.007;
                $saleOrder->qris_status='ACCEPTED';
                if($saleOrder->is_keep_transaction==0)
                {
                    $saleMapping=[];
                    $stockOut=[];
                    $productTemporary=[];
                    $productCalculate=[];
                    $updateStockProduct="";

                    $dataDetail=collect(DB::select("select sp.*,ssod.quantity,ssod.price from sc_sale_order_details ssod
                        join sc_products sp
                        on ssod.sc_product_id=sp.id
                        where ssod.is_deleted=0 and ssod.sc_sale_order_id=$saleOrderId
                    "));

                    foreach ($dataDetail as $key =>$product)
                    {
                        if($product->is_with_stock==1)
                        {

                            array_push($productTemporary,[
                                'id'=>$product->id,
                                'name'=>$product->name,
                                'code'=>$product->code,
                                'stock'=>$product->stock,
                                'selling_price'=>$product->price,
                                'purchase_price'=>$product->purchase_price,
                                'quantity'=>$product->quantity
                            ]);

                        }
                    }

                    if(!empty($productTemporary))
                    {
                        $key1=0;
                        foreach (collect($productTemporary)->groupBy('id') as $item)
                        {
                            $quantity=collect($item)->reduce(function($i, $k) {
                                return $i + $k['quantity'];
                            }, 0);

                            array_push($productCalculate,[
                                'id'=>$item->first()['id'],
                                'amount'=>$quantity,
                            ]);

                            $stock=$item->first()['stock'];

                            $stockP=$stock-$quantity;
                            $updateStockProduct.=($key1==0)?"(".$item->first()['id'].",".$stockP.")":",(".$item->first()['id'].",".$stockP.")";
                            $key1++;

                            $stockOut[]=[
                                'sync_id' => $item->first()['id'] . Uuid::uuid4()->toString(),
                                'sc_product_id' => $item->first()['id'],
                                'total' => $quantity,
                                'inv_warehouse_id' => $warehouse->id,
                                'record_stock' => $stockP,
                                'created_by' => $saleOrder->created_by,
                                'selling_price' => $item->first()['selling_price'],
                                'purchase_price' => $item->first()['purchase_price'],
                                'residual_stock' => $quantity,
                                'type' => StockInventory::OUT,
                                'transaction_action' => 'Pengurangan Stok ' . $item->first()['name'] . ' Dari Penjualan Dengan Code ' . $saleOrder->code,
                                'stockable_type'=>'App\Models\SennaToko\SaleOrder',
                                'stockable_id'=>$saleOrderId,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];
                        }
                    }

                    if(!empty($productCalculate))
                    {
                        $calculateInventory=InventoryUtilV2::inventoryCodev2($saleOrder->getMerchant->md_inventory_method_id,$warehouse->id,$productCalculate,'minus');
                        if($calculateInventory==false)
                        {
                            return $this->message::getJsonResponse(404,'Terjadi kesalahan saat pengurangan stok dipersediaan',[]);
                        }
                        $updateStockInv="";

                        foreach ($calculateInventory as $key => $n)
                        {
                            $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                            $stockInvId[]=$n['id'];
                            $saleMapping[]=[
                                'sc_sale_order_id'=>$saleOrder->id,
                                'sc_product_id'=>$n['sc_product_id'],
                                'sc_stock_inventory_id'=>$n['id'],
                                'amount'=>$n['amount'],
                                'purchase_price'=>$n['purchase'],
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];
                        }
                        if($updateStockInv!=""){
                            DB::statement("
                update sc_stock_inventories as t set
                        residual_stock = c.column_a
                    from (values
                        $updateStockInv
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                        }
                        if($updateStockProduct!=""){
                            DB::statement("
                update sc_products as t set
                        stock = c.column_a
                    from (values
                        $updateStockProduct
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                        }
                    }

                    StockInventory::insert($stockOut);
                    StockSaleMapping::insert($saleMapping);

                }else{
                    $saleOrder->is_keep_transaction=0;
                }
                $saleOrder->save();
                $coaSale=collect(CoaSaleUtil::getCoaSaleOrder($saleOrder->md_merchant_id));

                if (CoaSaleUtil::coaStockAdjustmentFromSaleV2($saleOrder->getMerchant->md_user_id,$saleOrder->md_merchant_id,$coaSale,$warehouse->id,$saleOrder->getMerchant->md_inventory_method_id,$saleOrderId) == false) {
                    return $this->message::getJsonResponse(404,trans('Terjadi kesalahan dalam pencatatan jurnal penjualan dengan kode penjualan'),[$saleOrder->code]);
                }

                DB::commit();
                return 'ACCEPTED';

            }
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
            
        }
    }

    public function generateKey(Request  $request)
    {
        try{

            $result=WinpayUtil::generateOpenSSLEncrypt($request->merchant_id,$request->amount,$request->raw,$request->key,$request->ref);
            return $this->message::getJsonResponse(200,trans('message.201'),$result);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\SennaToko\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class WarehouseController extends Controller
{

    protected $message;
    protected $warehouse;

    public function __construct()
    {
        $this->warehouse=MerchantWarehouse::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/warehouse')
            ->middleware('etag')
            ->group(function(){
                Route::get('/getWarehouse/{merchantId})','Api\v2\Senna\Merchant\Cashier\WarehouseController@getWarehouse')
                    ->name('api.getWarehouse');
            });
    }

    public function getWarehouse($merchantId)
    {

            $data=$this->warehouse::select([
                'id',
                'name as text'
            ])
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->get();

        $response =[];
        foreach($data as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->text

            ];
        }
        return response()->json($response);
    }








}

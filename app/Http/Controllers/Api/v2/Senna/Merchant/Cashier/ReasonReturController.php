<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\ArrayOfAccounting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class ReasonReturController extends Controller
{

    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/reason-retur')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/all/{isDebet}', 'Api\v2\Senna\Merchant\Cashier\ReasonReturController@getAll');
            });
    }

    public function getAll($isDebet=0)
    {

        try {
            $data=ArrayOfAccounting::reasonOfRetur($isDebet);
            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\MultiUnit;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\MasterData\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Utils\Merchant\ProductUtil;


class ProductMultiUnitController extends Controller
{
    protected $multiunit;
    protected $message;

    public function __construct()
    {
        $this->multiunit=MultiUnit::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/product/multi-unit')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\ProductMultiUnitController@create');
                Route::get('/all/{productId}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiUnitController@getAll');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiUnitController@delete');
                Route::post('/delete-v2/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiUnitController@deleteV2');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiUnitController@update');
                Route::get('/one/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiUnitController@getOne');

            });
    }

    public function getAll($productId)
    {
        try {
            $data=$this->multiunit::
            select([
                'sc_product_multi_units.*',
                'p.name as product_name',
                'u.name as unit_name'
            ])  ->join('sc_products as p','p.id','sc_product_multi_units.sc_product_id')
                ->join('md_units as u','u.id','sc_product_multi_units.md_unit_id')
                ->where('sc_product_multi_units.is_deleted',0)
                ->where('p.id',$productId)
                ->orderBy('sc_product_multi_units.id', 'ASC')
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {

            $data=$this->multiunit::
            select([
                'sc_product_multi_units.*',
                'p.name as product_name',
                'u.name as unit_name'
            ])  ->join('sc_products as p','p.id','sc_product_multi_units.sc_product_id')
                ->join('md_units as u','u.id','sc_product_multi_units.md_unit_id')
                ->where('sc_product_multi_units.is_deleted',0)
                ->where('sc_product_multi_units.id',$id)
                ->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


    public function create(Request  $request)
    {
        try {
            DB::beginTransaction();
            
            $multiUnit=$request->multi_unit;


            if(!empty($multiUnit))
            {
                $insert=[];
                $idS=[];
                foreach ($multiUnit as $key =>$item)
                {
                    $checkMulti=$this->multiunit::where('m.name',$item['unit_name'])
                                                ->where('sc_product_id',$item['sc_product_id'])
                                                ->where('sc_product_multi_units.is_deleted',0)
                                                ->join('md_units as m','m.id','sc_product_multi_units.md_unit_id')
                                                ->get();
                    
                    if(count($checkMulti)>0){
                        return $this->message::getJsonResponse(404,'Multi satuan sudah ada!',[]);
                    }

                    $unit=Unit::where('name',$item['unit_name'])->first();

                    if(is_null($unit)){
                        $unit=new Unit();
                        $unit->name=$item['unit_name'];
                        $unit->save();
                    }

                    $data = new $this->multiunit;
                    $data->md_unit_id=$unit->id;
                    $data->price=$item['price'];
                    $data->unit_name=$item['unit_name'];
                    $data->sc_product_id=$item['sc_product_id'];
                    $data->quantity=$item['quantity'];
                    $data->created_at=date('Y-m-d H:i:s');
                    $data->updated_at=date('Y-m-d H:i:s');
                    $data->save();

                    $idS[]=$data->id;
                }
                $productId=collect($multiUnit)->first()['sc_product_id'];
                $product=DB::table('sc_products')->find($productId);
                $jsonIdProduct=json_decode($product->assign_to_product);
                $jsonIdBranch=json_decode($product->assign_to_branch);
                
                if(!is_null($product->assign_to_branch) || $jsonIdBranch!=[]){
                    foreach($jsonIdBranch as $i){
                        $merchant=DB::table('md_merchants')->find($i);
                        $duplicate=ProductUtil::duplicateProductMobile(null,[],$idS,$merchant);
                        if($duplicate==false){
                            return $this->message::getJsonResponse(404,'Terjadi kesalahan saat duplikasi ke cabang!',[]);
                        }
                    }
                }
                DB::commit(); 
                return $this->message::getJsonResponse(200,trans('message.200'),[$insert]);
            }else{
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function delete($id)
    {
        try {
            $data =$this->multiunit::find($id);

            $checkSale=SaleOrderDetail::where('multi_unit_id',$data->id)
                                        ->join('sc_sale_orders as s','s.id','sc_sale_order_details.sc_sale_order_id')
                                        ->where('sc_sale_order_details.is_deleted',0)
                                        ->where('s.is_deleted',0)
                                        ->count();
            if($checkSale>0)
            {
                return $this->message::getJsonResponse(404,'data multi satuan telah digunakan pada penjualan!',[]);
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            $data->is_deleted=1;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function deleteV2($id,Request  $request)
    {
        try {
            DB::beginTransaction();
            $data =$this->multiunit::find($id);

            if(!is_null($data->getProduct->created_by_merchant) && !is_null($request->md_merchant_id)){
                if($data->getProduct->created_by_merchant!=$request->md_merchant_id){
                    return $this->message::getJsonResponse(404,'Cabang tidak diperkenankan menghapus produk yang dibuat oleh pusat',[]);
                }
            }

            $checkSale=SaleOrderDetail::where('multi_unit_id',$data->id)
                                        ->join('sc_sale_orders as s','s.id','sc_sale_order_details.sc_sale_order_id')
                                        ->where('sc_sale_order_details.is_deleted',0)
                                        ->where('s.is_deleted',0)
                                        ->count();
            if($checkSale>0)
            {
                return $this->message::getJsonResponse(404,'data multi satuan telah digunakan pada penjualan!',[]);
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            $data->is_deleted=1;
            $data->save();

            $jsonIdBranch=json_decode($data->assign_to);
            if(!is_null($data->assign_to) || json_decode($data->assign_to)!=[]){
                $this->multiunit::whereIn('id', $jsonIdBranch)->update(['is_deleted' => 1]);
            }

            DB::commit(); 
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function update(Request  $request,$id)
    {
        try {
            $data = $this->multiunit::find($id);
            if(!is_null($data->getProduct->created_by_merchant) && !is_null($request->md_merchant_id)){
                if($data->getProduct->created_by_merchant!=$request->md_merchant_id){
                    return $this->message::getJsonResponse(404,'Cabang tidak diperkenankan mengubah produk yang dibuat oleh pusat',[]);
                }
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $unit=Unit::where('name',$request->unit_name)->first();
            if(!is_null($unit)){
                $data->md_unit_id=$unit->id;
            }else{
                $unit=new Unit();
                $unit->name=$request->unit_name;
                $unit->save();
                $data->md_unit_id=$unit->id;
            }
            $data->sc_product_id=$request->sc_product_id;
            $data->unit_name=$request->unit_name;
            $data->quantity=$request->quantity;
            $data->price=$request->price;
            $data->is_deleted=0;
            $data->save();

            $jsonIdBranch=json_decode($data->assign_to);
            if(!is_null($data->assign_to) || json_decode($data->assign_to)!=[]){
                $this->multiunit::whereIn('id', $jsonIdBranch)->update(['unit_name' => $request->unit_name,'quantity' => $request->quantity,'price' => $request->price]);
            }

            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),$e);
        }
    }



}

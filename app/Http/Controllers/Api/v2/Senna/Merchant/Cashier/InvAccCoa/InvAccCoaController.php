<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\InvAccCoa;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Utils\Accounting\CoaPurchaseUtil;
use App\Utils\Accounting\CoaSaleUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class InvAccCoaController extends Controller
{

    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/inv-acc-coa')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/default-warehouse/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\InvAccCoa\InvAccCoaController@getDefaultWarehouse');
                Route::get('/coa-sale/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\InvAccCoa\InvAccCoaController@getCoaSale');
                Route::get('/coa-inv-sale/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\InvAccCoa\InvAccCoaController@getCoaInvSale');
                Route::get('/coa-ar/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\InvAccCoa\InvAccCoaController@getCoaAr');
                Route::get('/coa-ap/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\InvAccCoa\InvAccCoaController@getCoaAp');
                Route::get('/coa-purchase/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\InvAccCoa\InvAccCoaController@getCoaPurchase');


            });
    }

    public function getDefaultWarehouse($merchantId)
    {

        try {

            $data=parent::getGlobalWarehouse($merchantId);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getCoaSale($merchantId)
    {

        try {

            $data=CoaSaleUtil::getCoaSaleOrder($merchantId);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


    public function getCoaPurchase($merchantId)
    {

        try {

            $data=CoaPurchaseUtil::getCoaPurchaseOrder($merchantId);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


    public function getCoaInvSale($merchantId)
    {

        try {

            $data=CoaSaleUtil::coaInvSaleOrder($merchantId);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }



    public function getCoaAr($merchantId)
    {
        try {

            $data=CoaDetail::where('acc_coa_details.code','1.1.03.04')
                ->select([
                    'acc_coa_details.*'
                ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getCoaAp($merchantId)
    {
        try {

            $data=CoaDetail::where('acc_coa_details.code','2.1.01.03')
                ->select([
                    'acc_coa_details.*'
                ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }
}

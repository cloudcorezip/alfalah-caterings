<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\Accounting\Jurnal;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\PurchaseOrderDetail;
use App\Models\SennaToko\ReturPurchaseOrder;
use App\Models\SennaToko\ReturPurchaseOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\TempPurchaseOrderDetail;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaPurchaseUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class ReturPurchaseOrderController extends Controller
{
    protected $returPurchaseOrder;
    protected $returDetailPurchaseOrder;
    protected $message;
    protected $merchant;
    protected $purchaseOrder;
    protected $product;
    protected $purchaseOrderDetail;

    public function __construct()
    {
        $this->returPurchaseOrder=ReturPurchaseOrder::class;
        $this->message=Message::getInstance();
        $this->returDetailPurchaseOrder=ReturPurchaseOrderDetail::class;
        $this->merchant=Merchant::class;
        $this->purchaseOrder=PurchaseOrder::class;
        $this->product=Product::class;
        $this->purchaseOrderDetail=PurchaseOrderDetail::class;
        $this->ArrayOfAccounting=ArrayOfAccounting::class;
    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/toko/retur-purchase-order')
            ->middleware('etag')
            ->group(function()
            {
                Route::post('create/{purchaseOrderId}', 'Api\v2\Senna\Merchant\Cashier\ReturPurchaseOrderController@create')->middleware('api-verification');
                Route::post('list/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\ReturPurchaseOrderController@list')->middleware('api-verification');
                Route::get('one/{orderId}', 'Api\v2\Senna\Merchant\Cashier\ReturPurchaseOrderController@one')->middleware('api-verification');
                Route::delete('delete/{returId}', 'Api\v2\Senna\Merchant\Cashier\ReturPurchaseOrderController@delete')->middleware('api-verification');
                Route::get('/all', 'Api\v2\Senna\Merchant\Cashier\ReturPurchaseOrderController@getPurchaseBySupplier')->name('api.purchase.all');
                Route::post('/detail', 'Api\v2\Senna\Merchant\Cashier\ReturPurchaseOrderController@getPurchaseDetail')->name('api.purchase.detail');
                Route::get('/reason-id', 'Api\v2\Senna\Merchant\Cashier\ReturPurchaseOrderController@getReasonId')->name('api.reason.retur');
            });
    }


    public function getReasonId(Request $request){
        try{
            //dd($request->sc_supplier_id);

            $data=ArrayOfAccounting::reasonOfRetur($request->is_debet);

            $response =[];
            foreach($data as $key => $item){
                //dd($item->id);
                $response[] = [
                    "id"=>$data[0][1],
                    "text"=>$item[$key]
                ];
            }

            return response()->json($data);
        }catch (\Exception $e)
        {
            return response()->json($e);
        }

    }

    public function getPurchaseDetail(Request $request)
    {
        try{
            //dd($request->sc_supplier_id);
            $sumRetur=0;
            $data=$this->purchaseOrderDetail::select([
                'sc_purchase_order_details.id as id_detail',
                'p.id as product_id',
                'p.name as product_name',
                'p.code as product_code',
                'quantity',
                's.is_debet',
                's.step_type',
                'i.name as warehouse',
                's.id as purchase_id',
                'ap.is_paid_off',
                'm.name as unit_name',
                'sc_purchase_order_details.unit_name as unit_name_2',
                'sc_purchase_order_details.product_name as product_name_2',
                'sc_purchase_order_details.is_multi_unit',
                'sc_purchase_order_details.multi_unit_id',
                'sc_purchase_order_details.multi_quantity',
                'sc_purchase_order_details.multi_quantity as origin_multi_quantity',
                'sc_purchase_order_details.value_conversation',
                DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id as original_id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as text,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_purchase_order_details.sc_product_id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '-1'
                ) as json_multi_unit
                ")
            ])
                ->where('sc_purchase_order_id',$request->sc_purchase_order_id)
                ->join('sc_products as p','p.id','sc_purchase_order_details.sc_product_id')
                ->leftJoin('md_units as m','m.id','p.md_unit_id')
                ->join('sc_purchase_orders as s','s.id','sc_purchase_order_details.sc_purchase_order_id')
                ->join('md_merchant_inv_warehouses as i','i.id','s.inv_warehouse_id')
                ->leftjoin('acc_merchant_ap as ap','ap.apable_id','s.id')
                ->where('sc_purchase_order_details.is_deleted',0)
                ->where('s.is_step_close',0)
                ->get();

            foreach($data as $key => $d){
                $sumSubRetur=0;
                $dataRetur= DB::table('sc_retur_purchase_order_details')
                    ->select(['sc_retur_purchase_order_details.*'])
                    ->join('sc_retur_purchase_orders as c','c.id','sc_retur_purchase_order_id')
                    ->where('sc_purchase_order_detail_id',$d->id_detail)
                    ->where('c.step_type',$d->step_type)
                    ->where(function($query){
                        $query->where('c.reason_id',2)
                            ->orWhere('c.reason_id',3);
                    })
                    ->where('c.is_deleted',0)->get();
                foreach ($dataRetur as $dr){
                    if($dr->is_multi_unit==0){
                        $sumSubRetur+=$dr->quantity;
                    }else{
                        if($dr->value_conversation==0){
                            $sumSubRetur+=$dr->multi_quantity;
                        }else{
                            $sumSubRetur+=($dr->quantity*$dr->value_conversation);
                        }
                    }
                }
                if($d->is_multi_unit==0){
                    $d->quantity-=$sumSubRetur;
                }else{
                    $d->quantity-=$sumSubRetur/$d->value_conversation;
                    $d->multi_quantity-=$sumSubRetur;
                }
            }

            return response()->json($data);
        }catch (\Exception $e)
        {
            return response()->json([]);
        }

    }

    public function getPurchaseBySupplier(Request $request)
    {
        try{
            //dd($request->sc_supplier_id);
            $merchantId=$request->md_merchant_id;
            if(is_null($merchantId))
            {
                $merchantId=$request->merchant_id;
            }
            $data=$this->purchaseOrder::select([
                'id',
                'created_at',
                'total',
                'code',
                'second_code'
            ])
                ->where('sc_supplier_id',$request->sc_supplier_id)
                ->where('is_deleted',0)
                ->where('step_type',$request->step_type)
                ->where('is_step_close',0);

            if(!is_null($merchantId)){
                $result=$data->where('md_merchant_id',$merchantId)->get();
            }else{
                $result=$data->get();
            }
            $response =[];

            if(count($result)<1){
                $response[] = [
                    "id"=>-1,
                    "text"=>"Tidak Ada Data Pembelian",
                    "defaultSelected"=>true,
                    "selected"=>true,
                    "disabled"=>true
                ];
                return response()->json($response);
            }


            foreach($result as $item){
                //dd($item->id);
                $code= is_null($item->second_code)?$item->code:$item->second_code;
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$code.' ('.Carbon::parse($item->created_at)->isoFormat('D MMMM Y, HH:mm').')'
                ];
            }

            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);
        }

    }

    public function list($merchantId,Request $request)
    {
        try {
            $data=$this->purchaseOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getSupplier')
                ->with(['getOldStruck'=>function($query){
                    $query->select([
                        'sc_temp_purchase_order_details.*',
                        'p.name as product_name',
                        'p.foto',
                        'p.id as product_id',
                        'p.description',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'u.name as unit_name'
                    ])->join('sc_products as p','p.id','sc_temp_purchase_order_details.sc_product_id')
                        ->join('md_units as u','u.id','p.md_unit_id')
                        ->where('sc_temp_purchase_order_details.is_current',1)
                    ;

                }])
                ->with('ap')
                ->where('sc_purchase_orders.is_with_retur',1)
                ->where('sc_purchase_orders.md_merchant_id',$merchantId)->get()
            ;
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404, trans('message.404'), []);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function create($purchaseOrderId,Request $request)
    {
        try{
            date_default_timezone_set($request->timezone);
            DB::beginTransaction();
            $data=$this->purchaseOrder::find($purchaseOrderId);
            $poDelivery=PurchaseOrder::where('code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->where('step_type',3)
                ->first();

            if(is_null($poDelivery)){
                $purchaseId=$data->id;
            }else{
                $purchaseId=$poDelivery->id;
            }

            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            $merchant=Merchant::find($data->md_merchant_id);
            $merchantId=$merchant->id;

            $warehouseId=$request->inv_warehouse_id;
            if(is_null($warehouseId))
            {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }
            $invMethod=$request->md_inventory_method_id;

            if(is_null($invMethod))
            {
                return $this->message::getJsonResponse(404, 'Parameter metode persediaan belum terisi', []);

            }

            // $coaAp=$request->coa_ap_id;

            // if(is_null($coaAp))
            // {
            //     return $this->message::getJsonResponse(404, 'Parameter coa hutang belum terisi', []);

            // }

            if(is_null($data)){
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }

            if($data->is_editable==0){
                return $this->message::getJsonResponse(404,'Perhatian! Demi perhitungan akurat di akuntansi mulai dari tanggal 8 Maret 2021 Transaksi Penjualan Maupun Pembelian dan Piutang/Hutang tidak dapat diedit, karena ada penambahan modul akuntansi ,apabila kamu ingin mereset data silahkan lakukan di menu pengaturan dan menghapusnya', []);
            }
            $pluck=$data->stock->pluck('id');
            $check=StockInventory::whereIn('sc_stock_inventories.id',$pluck->all())
                ->where('sc_stock_inventories.type','in')
                ->whereRaw("sc_stock_inventories.transaction_action not like '%Retur%'")
                ->join('sc_stock_sale_mappings as m','m.sc_stock_inventory_id','sc_stock_inventories.id')
                ->where('sc_stock_inventories.is_deleted',0)
                ->count();

            if($check==0)
            {
                $isAddRetur=true;
            }else{
                $isAddRetur=false;
            }

            if($isAddRetur==false)
            {
                return $this->message::getJsonResponse(404, 'Persediaan dalam pembelian ini sudah digunakan,kamu tidak bisa membuat retur!', []);

            }
            TempPurchaseOrderDetail::where('sc_purchase_order_id',$data->id)
                ->update(['is_current'=>0]);

            $retur=new $this->returPurchaseOrder;
            $validator = Validator::make($request->all(),$retur->rule);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $retur->sc_purchase_order_id=$purchaseOrderId;
            $retur->code=CodeGenerator::returPurchaseOrder($purchaseOrderId);
            $retur->md_sc_transaction_status_id=TransactionStatus::PAID;
            $retur->total=$request->total;
            $retur->created_by=$request->created_by;
            $retur->is_retur=1;
            $retur->note=$request->note;
            $retur->timezone=$request->timezone;
            $retur->sync_id=$purchaseOrderId.Uuid::uuid4()->toString();
            $retur->reason_id=$request->reason_id;
            $retur->_reason_name=ArrayOfAccounting::searchOfReasonName($data->is_debet,$request->reason_id);
            $retur->save();
            $data->is_with_retur=1;
            $data->save();

            $detail=$request->details;
            if(empty($detail)){
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }
            $timestamp = date('Y-m-d H:i:s');
            $details=[];
            $temp=[];
            $stockInv=[];
            $productId = [];
            $productIds = "";
            foreach ($detail as $n => $i) {
                $productId[] = $i['sc_product_id'];
                $productIds .= ($n == 0) ? $i['sc_product_id'] : "," . $i["sc_product_id"];
            }

            $updateStockProduct="";
            $updateStockInv="";

            $poDetail=collect(DB::select("
            select
              sp.*,
              sp.inv_id as coa_inv_id,
              sp.hpp_id as coa_hpp_id,
              spod.is_multi_unit,
              spod.multi_quantity,
              spod.unit_name,
              spod.id as po_detail_id,
              spod.price as po_price,
              spod.quantity,
              spod.sub_total,
              spod.value_conversation,
              u.name as default_unit_name,
              (
                SELECT
                  jsonb_agg(jd.*) AS jsonb_agg
                FROM
                  (
                    SELECT
                      spmu.*,
                      mu.name
                    FROM
                      sc_product_multi_units spmu
                      join md_units mu on mu.id = spmu.md_unit_id
                    WHERE
                      spmu.sc_product_id = sp.id
                      and spmu.is_deleted = 0
                  ) jd
              ) AS multi_units
            from
              sc_products sp
            join sc_purchase_order_details spod  on spod.sc_product_id = sp.id
            left join md_units u on u.id = sp.md_unit_id
            where
              sp.id in ($productIds)
              and spod.is_deleted=0
            "));

            $stockAbles=StockInventory::
            select([
                'sc_stock_inventories.*',
                'sc_stock_inventories.id as inv_id',
                'p.name as product_name',
                'p.code as product_code',
                'p.stock',
                'm.name as unit_name_2'

                ])
                ->whereIn('sc_stock_inventories.sc_product_id',$productId)
                ->join('sc_products as p','p.id','sc_stock_inventories.sc_product_id')
                ->leftJoin('md_units as m','m.id','p.md_unit_id')
                ->where('sc_stock_inventories.stockable_type','App\Models\SennaToko\PurchaseOrder')
                ->where('sc_stock_inventories.stockable_id',$purchaseId)->get();

            foreach ($detail as $key =>$item)
            {
                $product = $poDetail->firstWhere('id',$item['sc_product_id']);
                if($product->is_with_stock==0){
                    return $this->message::getJsonResponse(404, 'Tidak dapat melakukan pembelian karena produk berupa jasa', []);
                }
                $isMulti=isset($item['is_multi_unit'])?$item['is_multi_unit']:0;
                $MultiId=isset($item['multi_unit_id'])?$item['multi_unit_id']:null;
                $unit_name="";
                $mQuantity=$item['quantity'];
                $multi_quantity=0;
                $convertionValue=1;
                $jsonMulti=null;

                $saleOrderLine=$poDetail->firstWhere('po_detail_id',$item['sc_purchase_order_detail_id']);
                $stockAble=$stockAbles->firstWhere('sc_product_id',$item['sc_product_id']);

                if($isMulti==1 && !is_null($MultiId)){
                    $multi_unit=collect(json_decode($product->multi_units));
                    $unit=$multi_unit->firstWhere('id',$MultiId);
                    $unit_name=$unit->name;
                    $mQuantity=$item['quantity'];
                    $multi_quantity=$unit->quantity*$item['quantity'];
                    $convertionValue=$unit->quantity;
                    $jsonMulti=$multi_unit;
                }else{
                    $unit_name=$product->default_unit_name;

                }



                if(!is_null($saleOrderLine))
                {
                    $temp[]=[
                        'sync_id'=>$product->id.Uuid::uuid4()->toString(),
                        'sc_product_id'=>$saleOrderLine->id,
                        'quantity'=>$saleOrderLine->quantity,
                        'sub_total'=>$saleOrderLine->sub_total,
                        'price'=>$saleOrderLine->po_price,
                        'sc_purchase_order_id'=>$data->id,
                        'is_current'=>1,
                        'created_at'=>$saleOrderLine->created_at,
                        'updated_at'=>$saleOrderLine->updated_at
                    ];

                    if($mQuantity!=0){
                        $details[]=[
                            'sc_product_id'=>$item['sc_product_id'],
                            'quantity'=>$mQuantity,
                            'multi_quantity'=>$multi_quantity,
                            'sub_total'=>$item['sub_total'],
                            'sc_purchase_order_detail_id'=>$item['sc_purchase_order_detail_id'],
                            'sc_retur_purchase_order_id'=>$retur->id,
                            'type'=>$item['type'],
                            'unit_name'=>$unit_name,
                            'created_at'=>$timestamp,
                            'updated_at'=>$timestamp,
                            'coa_inv_id'=>$product->coa_inv_id,
                            'coa_hpp_id'=>$product->coa_hpp_id,
                            'product_name'=>$product->name.' '.$product->code,
                            'multi_unit_id'=>$MultiId,
                            'is_multi_unit'=>$isMulti,
                            'value_conversation'=>$convertionValue,
                            'json_multi_unit'=>$jsonMulti
                        ];
                    }
                    $q=($isMulti==1 && !is_null($MultiId))?$multi_quantity:$mQuantity;
                    if($q>$saleOrderLine->quantity*$saleOrderLine->value_conversation)
                    {
                        return $this->message::getJsonResponse(404, "Jumlah pengembalian produk $product->name melebihi pembelian yang tersedia", []);
                    }

                    if($retur->reason_id!=1)
                    {
                        $inv=$stockAble->residual_stock-($mQuantity*$convertionValue);
                        $updateStockInv.=($key==0)?"(".$stockAble->inv_id.",".$inv.")":",(".$stockAble->inv_id.",".$inv.")";
                        $stockP=$stockAble->stock-($mQuantity*$convertionValue);
                        $updateStockProduct.=($key==0)?"(".$stockAble->sc_product_id.",".$stockP.")":",(".$stockAble->sc_product_id.",".$stockP.")";

                        if($mQuantity>0)
                        {
                            $stockInv[]=[
                                'sync_id'=>$stockAble->sc_product_id.Uuid::uuid4()->toString(),
                                'sc_product_id' => $stockAble->sc_product_id,
                                'total' => $mQuantity*$convertionValue,
                                'record_stock' => $stockP,
                                'created_by' => user_id(),
                                'selling_price' => $stockAble->selling_price,
                                'purchase_price' => $stockAble->purchase_price,
                                'type' => StockInventory::OUT,
                                'inv_warehouse_id'=>$stockAble->inv_warehouse_id,
                                'transaction_action' => 'Pengurangan Stok ' . $stockAble->product_name . ' Dari Retur Transaksi ' . $retur->code,
                                'stockable_type'=>'App\Models\SennaToko\ReturPurchaseOrder',
                                'stockable_id'=>$retur->id,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'timezone'=>$request->timezone,
                                'unit_name'=>$stockAble->unit_name_2,
                                'product_name'=>$stockAble->product_name.' '.$stockAble->product_code
                            ];
                        }
                    }

                }


            }

            if($updateStockInv!=""){
                DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
            }

            if($updateStockProduct!="") {
                DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            $updateStockProduct
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
            }


            if($retur->reason_id==3)
            {
                $data->ap->residual_amount-=$request->total;
                $data->ap->save();
            }
            DB::table('sc_stock_inventories')->insert($stockInv);
            TempPurchaseOrderDetail::insert($temp);
            ReturPurchaseOrderDetail::insert($details);

            if($retur->reason_id!=1)
            {
                if(CoaPurchaseUtil::coaReturPurchaseOrderV2($merchant->md_user_id,$data->md_merchant_id,$retur,$timestamp,$merchant->coa_ap_purchase)==false)
                {
                    return $this->message::getJsonResponse(404,'Pencatatan retur pada jurnal keuangan terjadi kesalahan',$retur);

                }
            }
            DB::commit();
            return $this->message::getJsonResponse(200,trans('ordershop.po_retur'),$retur);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }



    public function one($orderId)
    {
        try{
            $data=$this->purchaseOrder::with('getDetail.getProduct.getUnit')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getSupplier')
                ->with(['getOldStruck'=>function($query){
                    $query->select([
                        'sc_temp_purchase_order_details.*',
                        'p.name as product_name',
                        'p.foto',
                        'p.id as product_id',
                        'p.description',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'u.name as unit_name'
                    ])->join('sc_products as p','p.id','sc_temp_purchase_order_details.sc_product_id')
                        ->join('md_units as u','u.id','p.md_unit_id')
                        ->where('sc_temp_purchase_order_details.is_current',1)
                    ;

                }])
                ->with('ap')
                ->where('id',$orderId)->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404, trans('message.404'), []);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }

    }

    public function delete($id)
    {
        try{
            DB::beginTransaction();
            $data=ReturPurchaseOrder::find($id);
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->getPurchaseOrder->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,'data retur tidak ada',[]);

            }
            $data->is_deleted=1;
            $data->save();
            $purchase=PurchaseOrder::find($data->sc_purchase_order_id);
            $totalBack=0;
            if($data->reason_id!=1)
            {
                foreach ($data->getDetail as $d)
                {
                    $stockAble=$purchase->stock->where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                        ->where('stockable_id',$purchase->id)
                        ->first();
                    $stockAble->residual_stock+=$d->quantity;
                    $stockAble->save();
                    $totalBack+=$d->sub_total;
                    if($purchase->getWarehouse->is_default==1)
                    {
                        $stockAble->getProduct->stock +=$d->quantity;
                        $stockAble->getProduct->save();
                    }
                    $data->stock->where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\ReturPurchaseOrder')
                        ->where('stockable_id',$data->id)
                        ->first()->delete();
                }
                if(!is_null($purchase->ap))
                {
                    $purchase->ap->residual_amount+=$totalBack;
                    $purchase->ap->save();

                    if($purchase->ap->residual_amount==0)
                    {
                        $purchase->ap->is_paid_off=0;
                        $purchase->ap->save();
                        $purchase->md_sc_transaction_status_id=6;
                        $purchase->save();
                    }
                }
                Jurnal::where([
                    'external_ref_id'=>$data->id,
                    'ref_code'=>$data->code
                ])->update([
                    'is_deleted'=>1
                ]);
            }
            DB::commit();
            return $this->message::getJsonResponse(200,'data retur berhasil dihapus',[]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\SennaToko\SaleOrder;
use Illuminate\Support\Facades\Storage;
use Str;
use App\Jobs\StruckMail;

class StruckFileController extends Controller
{

    protected $message;
    protected $shift;

    public function __construct()
    {
        $this->order=SaleOrder::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/struck')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/get/{shortest_link}', 'Api\v2\Senna\Merchant\Cashier\StruckFileController@getFile')->name('get.struck');
                Route::post('/send-struck/{id}', 'Api\v2\Senna\Merchant\Cashier\StruckFileController@sendStruck');
            });
    }


    public function sendStruck(Request  $request,$id)
    {
        try{
            $data = $this->order::find($id);

            $destinationPath = 'public/uploads/merchant/'.$data->md_merchant_id.'/sale-order/';

            if($request->hasFile('struck_file'))
            {
                $file=$request->file('struck_file');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }

            $data->struck_file=$fileName;
            $shortest_link=$data->id.Str::random(6);
            $data->shortest_link=$shortest_link;
            $data->save();
            // $link=route('get.struck',['shortest_link'=>$shortest_link]);
            $link=env('S3_URL').$data->struck_file;
            if($request->is_type==1){
                return $this->message::getJsonResponse(200,trans('message.201'),$link);
            }else{
                return $this->message::getJsonResponse(200,trans('message.201'),$link);
            }
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),$e);
        }

    }
    public function getFile($shortest_link)
    {
        $find = $this->order::where('shortest_link', $shortest_link)->first();
        $link=env('S3_URL').$find->struck_file;
        return redirect($link);
    }
}

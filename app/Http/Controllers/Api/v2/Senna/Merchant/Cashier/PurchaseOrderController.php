<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\StockInventory;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaPurchaseUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class PurchaseOrderController extends Controller
{
    protected $purchase;
    protected $message;
    protected $product;
    protected $ap;

    public function __construct()
    {
        $this->purchase=PurchaseOrder::class;
        $this->message=Message::getInstance();
        $this->product=Product::class;
        $this->ap=MerchantAp::class;

    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/toko/purchase-order')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('list/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\PurchaseOrderController@list');
                Route::post('create/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\PurchaseOrderController@create');
                Route::get('one/{orderId}', 'Api\v2\Senna\Merchant\Cashier\PurchaseOrderController@one');
                Route::delete('void/{orderId}', 'Api\v2\Senna\Merchant\Cashier\PurchaseOrderController@voidTransaction');

            });
    }

    public function create(Request $request,$merchantId)
    {
        try {
            DB::beginTransaction();

            $purchase=new $this->purchase;
            $validator = Validator::make($request->all(),$purchase->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $warehouseId=$request->inv_warehouse_id;
            if(is_null($warehouseId))
            {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }
            $invMethod=$request->md_inventory_method_id;

            if(is_null($invMethod))
            {
                return $this->message::getJsonResponse(404, 'Parameter metode persediaan belum terisi', []);

            }
            //dalam bentuk array
            // $coaPurchase=$request->coa_purchase;

            // if(is_null($coaPurchase))
            // {
            //     return $this->message::getJsonResponse(404, 'Parameter coa persediaan belum terisi', []);

            // }

            // $coaAp=$request->coa_ap_id;

            // if(is_null($coaAp))
            // {
            //     return $this->message::getJsonResponse(404, 'Parameter coa hutang belum terisi', []);

            // }
            if($request->is_debet==0)
            {
                if($request->md_transaction_type_id==0)
                {
                    return $this->message::getJsonResponse(404, 'Metode pembayaran belum dipilih', []);

                }
                $mdTransactionTypeId=$request->md_transaction_type_id;
                $explode=explode("_",$mdTransactionTypeId);
                $coaKasOrBank=$explode[1];
                $transType=$explode[0];

                $coaPayment=$coaKasOrBank;

            }elseif($request->is_debet==1 && $request->paid_nominal>0){
                if($request->md_transaction_type_id==0)
                {
                    return $this->message::getJsonResponse(404, 'Metode pembayaran belum dipilih', []);

                }
                $mdTransactionTypeId=$request->md_transaction_type_id;
                $explode=explode("_",$mdTransactionTypeId);
                $coaKasOrBank=$explode[1];
                $transType=$explode[0];

                $coaPayment=$coaKasOrBank;
            }else{
                $transType=null;
            }
            date_default_timezone_set($request->timezone);
            if(is_null($request->code))
            {
                $code=CodeGenerator::generatePurchaseOrder($request->sc_supplier_id);
                $purchase->code=$code;
            }else{
                $purchase->code=$request->code;
            }
            $purchase->qr_code='-';
            $purchase->is_debet=$request->is_debet;
            $purchase->sc_supplier_id=$request->sc_supplier_id;
            $purchase->total=$request->total;
            $purchase->md_merchant_id=$merchantId;

            if($request->is_debet==1)
            {
                $purchase->md_sc_transaction_status_id=TransactionStatus::UNPAID;
            }
            else{
                $purchase->md_sc_transaction_status_id=TransactionStatus::PAID;
            }
            if(!is_null($transType))
            {
                if($request->is_debet==0)
                {
                    $purchase->md_transaction_type_id=$transType;
                    $purchase->coa_trans_id=$coaPayment;
                }

            }
            $purchase->discount=$request->discount;
            $purchase->change_nominal=$request->change_nominal;
            $purchase->discount_percentage=$request->discount_percentage;
            $purchase->timezone=$request->timezone;
            $purchase->paid_nominal=$request->paid_nominal;
            $purchase->created_by=$request->created_by;
            $purchase->sync_id=$merchantId.Uuid::uuid4()->toString();
            $purchase->inv_warehouse_id=$warehouseId;
            $purchase->note=$request->note;
            $purchase->save();
            $detail=$request->details;
            if(empty($detail)){
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }
            $details=[];
            $stockIn=[];

            $timestamp = date('Y-m-d H:i:s');
            $productId = [];
            $productIds = "";
            foreach ($detail as $n => $i) {
                $productId[] = $i['sc_product_id'];
                $productIds .= ($n == 0) ? $i['sc_product_id'] : "," . $i["sc_product_id"];
            }
            $products=collect(DB::select("
            select
              sp.*,
              sp.inv_id as coa_inv_id,
              sp.hpp_id as coa_hpp_id,
              u.name as default_unit_name,
              (
                SELECT
                  jsonb_agg(jd.*) AS jsonb_agg
                FROM
                  (
                    SELECT
                      spmu.*,
                      mu.name
                    FROM
                      sc_product_multi_units spmu
                      join md_units mu on mu.id = spmu.md_unit_id
                    WHERE
                      spmu.sc_product_id = sp.id
                      and spmu.is_deleted = 0
                  ) jd
              ) AS multi_units
            from
              sc_products sp
            left join md_units u on u.id = sp.md_unit_id
            where
              sp.id in ($productIds)
            "));

            $updateStockProduct="";

            foreach ($detail as $key =>$item)
            {
                $product=$products->firstWhere('id',$item['sc_product_id']);
                if($product->is_with_stock==0){
                    return $this->message::getJsonResponse(404, 'Tidak dapat melakukan pembelian karena produk berupa jasa', []);
                }
                $isMulti=isset($item['is_multi_unit'])?$item['is_multi_unit']:0;
                $MultiId=isset($item['multi_unit_id'])?$item['multi_unit_id']:null;
                $isCustomPrice=isset($item['is_custom_price'])?$item['is_custom_price']:0;

                $unit_name="";
                $mQuantity=$item['quantity'];
                if($mQuantity>9999999999){
                    return $this->message::getJsonResponse(404,'Stok melebihi batas',[]);
                }

                $mPrice=$item['price'];
                $sub_total=$item['sub_total'];
                $multi_quantity=$item['quantity'];
                $convertionValue=1;
                $jsonMulti=null;

                if($isMulti==1 && !is_null($MultiId)){
                    $multi_unit=collect(json_decode($product->multi_units));
                    $unit=$multi_unit->firstWhere('id',$MultiId);
                    $unit_name=$unit->name;
                    $multi_quantity=$unit->quantity*$item['quantity'];
                    $convertionValue=$unit->quantity;
                    $jsonMulti=$multi_unit;
                }else{
                    $unit_name=$product->default_unit_name;
                }

                $noteOrder=isset($item['note_order'])?$item['note_order']:"";
                $details[]=[
                    'sc_product_id'=>$item['sc_product_id'],
                    'quantity'=>$mQuantity,
                    'is_multi_unit'=>$isMulti,
                    'is_custom_price'=>$isCustomPrice,
                    'multi_unit_id'=>$MultiId,
                    'unit_name'=>$unit_name,
                    'multi_quantity'=>$multi_quantity,
                    'sub_total'=>$sub_total,
                    'sc_purchase_order_id'=>$purchase->id,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp,
                    'note_order'=>$noteOrder,
                    'price'=>$mPrice,
                    'coa_inv_id'=>$product->coa_inv_id,
                    'coa_hpp_id'=>$product->coa_hpp_id,
                    'value_conversation'=>$convertionValue,
                    'json_multi_unit'=>$jsonMulti
                ];


                $stockP=$product->stock+$multi_quantity;

                $updateStockProduct.=($key==0)?"(".$product->id.",".$stockP.",".$mPrice/$convertionValue.")":",(".$product->id.",".$stockP.",".$mPrice/$convertionValue.")";

                $stockIn[]=[
                    'sync_id'=>$product->id.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$product->id,
                    'total'=>$multi_quantity,
                    'record_stock'=>$product->stock,
                    'created_by'=>$request->created_by,
                    'selling_price'=>$product->selling_price,
                    'purchase_price'=>$mPrice,
                    'residual_stock'=>$multi_quantity,
                    'type'=>StockInventory::IN,
                    'inv_warehouse_id'=>$warehouseId,
                    'transaction_action'=> 'Penambahan Stok '.$product->name. ' Dari Pembelian Dengan Code '.$purchase->code,
                    'stockable_type'=>'App\Models\SennaToko\PurchaseOrder',
                    'stockable_id'=>$purchase->id,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),
                ];
            }

            if($updateStockProduct!=""){
                DB::statement("
                    update sc_products as t set
                            stock = c.column_a,
                            purchase_price=c.column_c
                        from (values
                            $updateStockProduct
                        ) as c(column_b, column_a,column_c)
                        where c.column_b = t.id;
                    ");
            }
            DB::table('sc_stock_inventories')->insert($stockIn);
            DB::table('sc_purchase_order_details')->insert($details);

            if($purchase->is_debet==1)
            {
                $apId=$purchase->ap()->insertGetId([
                    'sync_id'=>$merchantId.Uuid::uuid4()->toString(),
                    'md_merchant_id'=>$merchantId,
                    'residual_amount'=>$request->total-$request->paid_nominal,
                    'paid_nominal'=>$request->paid_nominal,
                    'is_paid_off'=>0,
                    'apable_id'=>$purchase->id,
                    'apable_type'=>'App\Models\SennaToko\PurchaseOrder',
                    'due_date'=>$request->due_date,
                    'timezone'=>$request->timezone
                ]);
            }

            if(CoaPurchaseUtil::coaStockAdjustmentFromPurchasev2Mobile($purchase->getMerchant->md_user_id,$merchantId,[],$purchase)==false)
            {
                DB::rollBack();
                return $this->message::getJsonResponse(404,"Terjadi kesalahan dalam pencatatan jurnal pembelian dengan kode pembelian $purchase->code",[]);
            }


            if($purchase->is_debet==1 && $purchase->paid_nominal>0)
            {
                $codeJur=Jurnal::where('md_merchant_id',$merchantId)
                        ->count()+1;

                $data=new MerchantApDetail();
                $data->ap_code='PVT'.sprintf("%07s", $codeJur);
                $data->paid_nominal=$purchase->paid_nominal;
                $data->paid_date=$purchase->created_at;
                $data->note='Uang Muka Pembelian';
                $data->acc_merchant_ap_id=$apId;
                $data->created_by=$request->created_by;
                $data->timezone=$request->timezone;
                $data->trans_coa_detail_id=$coaPayment;
                $data->save();

                $jurnal=new Jurnal();
                $jurnal->ref_code=$data->ap_code;
                $jurnal->trans_code='JRN'.sprintf("%07s", $codeJur);
                $jurnal->trans_name='Uang Muka Pembelian  '.$purchase->code.'-'.$data->ap_code;
                $jurnal->trans_time=$data->paid_date;
                $jurnal->trans_note='Uang Muka Pembelian  '.$purchase->code.'-'.$data->ap_code;
                $jurnal->trans_purpose='Uang Muka Pembelian  '.$purchase->code.'-'.$data->ap_code;
                $jurnal->trans_amount=$data->paid_nominal;
                $jurnal->trans_proof='-';
                $jurnal->md_merchant_id=$merchantId;
                $jurnal->md_user_id_created=$data->created_by;
                $jurnal->external_ref_id=$data->id;
                $jurnal->flag_name='Uang Muka Pembelian '.$purchase->code;
                $jurnal->save();

                $fromCoa=Merchant::find($merchantId);

                $insert=[
                    [
                        'acc_coa_detail_id'=>$fromCoa->coa_ap_purchase_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$coaPayment,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->paid_nominal,
                        'created_at'=>$data->paid_date,
                        'updated_at'=>$data->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                ];
                JurnalDetail::insert($insert);

            }
            $resultJson=[
                'purchase_id'=>$purchase->id,
                'purchase_code'=>$purchase->code,
                'md_sc_transaction_status_id'=>$purchase->md_sc_transaction_status_id
            ];
            DB::commit();
            return $this->message::getJsonResponse(200,trans('ordershop.purchase_order_create'),$resultJson);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }

    }

    public function list($merchantId,Request $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $sort=($request->sorting=='asc')?'asc':'desc';

            $data=$this->purchase::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit')
                        ->where('quantity','!=',0);
                }])
                ->with('ap')
                ->with('getStaff')
                ->where('md_merchant_id',$merchantId)
                ->skip($offset)
                ->take($limit)
                ->orderBy('created_at',$sort)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function one($id)
    {
        try {
            $data=$this->purchase::with(['getTransactionType','getTransactionStatus','getSupplier'])
                ->with('ap')
                ->with('getStaff')
                ->with('getAllRetur')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit')
                        ->where('quantity','!=',0);
                }])
                ->where('id',$id)
                ->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function voidTransaction($purchaseId)
    {
        try{
            DB::beginTransaction();
            $data=PurchaseOrder::find($purchaseId);
            $poDelivery=PurchaseOrder::where('code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->where('step_type',3)
                ->first();
            if(is_null($poDelivery))
            {
                $purchaseId=$purchaseId;
            }else{
                $purchaseId=$poDelivery->id;
            }
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            $warehouse=parent::getGlobalWarehouse($data->md_merchant_id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }

            $isUse=0;
            $productId=$data->getDetail->pluck('sc_product_id');
            $stockAble=StockInventory::whereIn('sc_product_id',$productId->all())
                ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                ->where('type','in')
                ->whereRaw("transaction_action not like '%Retur%'")
                ->where('stockable_id',$purchaseId)
                ->get();

            foreach ($stockAble as $map)
            {
                $totalRetur=0;
                foreach ($data->getRetur->where('reason_id','!=',1)->where('is_deleted',0) as $r)
                {
                    foreach ($r->getDetail->where('sc_product_id',$map->sc_product_id) as $d)
                    {
                        $totalRetur+=$d->quantity;
                    }
                }
                if(($map->residual_stock+$totalRetur)<$map->total)
                {
                    $isUse++;
                }
            }

            if($isUse>0)
            {
                return $this->message::getJsonResponse(404,'Mohon maaf data persediaan barang sudah digunakan,kamu tidak dapat melakukan penghapusan',[]);


            }else{
                foreach ($data->getDetail as $d)
                {
                    $stockAble=StockInventory::where('sc_product_id',$d->sc_product_id)
                        ->where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                        ->where('stockable_id',$purchaseId)
                        ->first();
                    if($stockAble->inv_warehouse_id==$warehouse->id)
                    {
                        $product=Product::find($d->sc_product_id);
                        $product->stock-=$stockAble->residual_stock;
                        $product->save();
                    }
                    $stockAble->is_deleted=1;
                    $stockAble->save();
                }
                $data->is_deleted=1;
                $data->save();

                Jurnal::where(['ref_code'=>$data->code,
                    'external_ref_id'=>$purchaseId
                ])
                    ->update(['is_deleted'=>1]);

                StockInventory::where('stockable_type','App\Models\SennaToko\PurchaseOrder')
                    ->where('stockable_id',$purchaseId)
                    ->delete();

                foreach ($data->getRetur as $retur)
                {
                    $retur->is_deleted=1;
                    $retur->save();

                    StockInventory::where('stockable_type','App\Models\SennaToko\ReturPurchaseOrder')
                        ->where('stockable_id',$retur->id)
                        ->delete();

                    Jurnal::where(['ref_code'=>$retur->code,
                        'external_ref_id'=>$retur->id
                    ])
                        ->update(['is_deleted'=>1]);
                }
                if($data->is_debet==1)
                {
                    $data->ap->is_deleted=1;
                    $data->ap->save();
                    foreach ($data->ap->getDetail as $ap)
                    {
                        $ap->is_deleted=1;
                        $ap->save();
                        Jurnal::where(['ref_code'=>$ap->ap_code,
                            'external_ref_id'=>$ap->id
                        ])
                            ->update(['is_deleted'=>1]);
                    }
                }
            }
            if(!is_null($poDelivery))
            {
                $poDelivery->is_deleted=1;
                $poDelivery->save();

                foreach ($poDelivery->getRetur as $retur)
                {
                    $retur->is_deleted=1;
                    $retur->save();

                    StockInventory::where('stockable_type','App\Models\SennaToko\ReturPurchaseOrder')
                        ->where('stockable_id',$retur->id)
                        ->delete();

                    Jurnal::where(['ref_code'=>$retur->code,
                        'external_ref_id'=>$retur->id
                    ])
                        ->update(['is_deleted'=>1]);
                }
            }
            PurchaseOrder::where('code',$data->code)
                ->where('md_merchant_id',$data->md_merchant_id)
                ->where('step_type',2)
                ->update([
                    'is_step_close'=>0
                ]);

            DB::commit();
            return $this->message::getJsonResponse(200, 'Data transaksi pembelian berhasil dihapus,data pembelian yang terhapus tetap bisa terlihat di backoffice', []);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }

    }

}

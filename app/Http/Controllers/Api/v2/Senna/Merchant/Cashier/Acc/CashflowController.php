<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\Acc;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class CashflowController extends  Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/acc/report/cashflow')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('report/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\Acc\CashflowController@getReport');

            });
    }

    public function getReport(Request  $request,$merchantId)
    {

        try{
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():Carbon::parse($request->start_date);
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():Carbon::parse($request->end_date);
            $firstDate=Carbon::parse($startDate)->toDateString();


            $data=collect(DB::select("
            with acc_cashflow_report as (select
             acd.id AS det_id,
              acd.type_coa,
              acd.name AS det_name,
              acc.md_merchant_id,
              case
              when acc.name = 'Pendapatan Usaha' then 'Pendapatan'
              when acd.name = 'Utang Saldo Deposit' then 'Pendapatan'
              when acd.name = 'Utang PPN Keluaran' then 'Pendapatan'
              when acd.name = 'Utang Pembelian Barang' then 'Beban Operasional & Usaha'
              when acc.name = 'Aktiva Tetap' then 'Pembelian/Penjualan Aset'
              when acc.name = 'Aktiva Tidak Berwujud' then 'Pembelian/Penjualan Aset'
              when acc.name = 'Investasi Jangka Pendek' then 'Investasi Lainnya'
              when acc.name = 'Investasi Jangka Panjang' then 'Investasi Lainnya'
              when acc.name = 'Piutang' then 'Pembayaran/Penerimaan Pinjaman'
              when acc.name = 'Utang' then 'Pembayaran/Penerimaan Pinjaman'
              when acc.name = 'Saldo Laba' then 'Ekuitas/Modal'
              when acc.name = 'Dividen' then 'Ekuitas/Modal'
              when acc.name = 'Modal' then 'Ekuitas/Modal'
              else acc.name end as format_name,
              COALESCE(sub.amount, 0) AS amount,
              COALESCE(sub.original, '".$firstDate."') AS _original,
              COALESCE(sub.coa_type, acd.type_coa) AS coa_type,
              0 as is_deleted
            from acc_coa_categories acc
            join acc_coa_details acd
            on acc.id=acd.acc_coa_category_id
            left join (
                select
                  d.acc_coa_detail_id,
                  d.amount,
                  j.trans_time,
                  to_char(j.trans_time, 'YYYY-MM-DD') AS original,
                  d.coa_type
                FROM
                  acc_jurnal_details d
                  JOIN acc_jurnals j ON j.id = d.acc_jurnal_id
                WHERE
                  j.is_deleted in(0,2)
                  and j.md_merchant_id = ".$merchantId."
              ) sub ON sub.acc_coa_detail_id = acd.id
            where
            acc.md_merchant_id=".$merchantId."
            and acc.name in (
                'Pendapatan','Pendapatan Usaha', 'Pendapatan Lainnya',
                'Beban Operasional & Usaha', 'Beban Lainnya',
                'Aktiva Tetap', 'Aktiva Tidak Berwujud',
                'Investasi Jangka Pendek', 'Investasi Jangka Panjang',
                'Utang', 'Utang Pajak', 'Piutang',
                'Modal', 'Saldo Laba','Dividen'
              )
            and acd.is_deleted in(0,2)
            and acd.name not in (
                'Diskon Pembelian Barang', 'Utang Konsinyasi','Utang PPN Keluaran'
            )
            and acd.name not like '%Akumulasi%'
            and acd.name not like '%%Utang PPh%'
            union all
            select
              d.acc_coa_detail_id as det_id,
              d.type_coa,
              d.name as det_name,
              aj.md_merchant_id,
              'Pembayaran ke Pemasok' as format_name,
              aj.trans_amount as amount,
              to_char(aj.trans_time, 'YYYY-MM-DD') AS _original,
              d.coa_type,
              aj.is_deleted
            from
              acc_jurnals aj
              join(
                select
                  ajd.acc_jurnal_id,
                  ajd.acc_coa_detail_id,
                  acd.type_coa,
                  acd.name,
                  ajd.coa_type
                from
                  acc_jurnal_details ajd
                  join acc_coa_details acd on ajd.acc_coa_detail_id = acd.id
                where
                  acd.code like '%1.1.01%'
                  or acd.code like '%1.1.02%'
              ) d on d.acc_jurnal_id = aj.id
            where
              aj.is_deleted in(0,2)
              and aj.md_merchant_id = ".$merchantId."
              and substring(aj.ref_code from 0 for 2)='P'
              or substring(aj.ref_code from 0 for 3)='RP'
              or substring(aj.ref_code from 0 for 4)='PVT'
              or substring(aj.ref_code from 0 for 4)='PPO'
              )
            select acr.*,
            case when acr.format_name='Pendapatan' then 'operasional'
            when acr.format_name='Pendapatan Lainnya' then 'operasional'
            when acr.format_name='Beban Operasional & Usaha' then 'operasional'
            when acr.format_name='Beban Lainnya' then 'operasional'
            when acr.format_name='Pembelian/Penjualan Aset' then 'investasi'
            when acr.format_name='Investasi Lainnya' then 'investasi'
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 'pendanaan'
            when acr.format_name='Ekuitas/Modal'
            then 'pendanaan'
            else 'operasional' end as format_type_name,
            case when acr.format_name='Pendapatan' then 1
            when acr.format_name='Pendapatan Lainnya' then 2
            when acr.format_name='Beban Operasional & Usaha' then 3
            when acr.format_name='Beban Lainnya' then 5
            when acr.format_name='Pembelian/Penjualan Aset' then 6
            when acr.format_name='Investasi Lainnya' then 7
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 8
            when acr.format_name='Ekuitas/Modal'
            then 9
            else 4 end as order_number,
            case when acr.format_name='Pendapatan' then 1
            when acr.format_name='Pendapatan Lainnya' then 2
            when acr.format_name='Beban Operasional & Usaha' then 3
            when acr.format_name='Beban Lainnya' then 4
            when acr.format_name='Pembelian/Penjualan Aset' then 5
            when acr.format_name='Investasi Lainnya' then 6
            when acr.format_name='Pembayaran/Penerimaan Pinjaman' then 7
            when acr.format_name='Ekuitas/Modal'
            then 8
            else 9 end as format_id
            from acc_cashflow_report acr  where acr.md_merchant_id=".$merchantId."
            "));

            $result=$data->sortBy('order_number')->where('_original','<=',$endDate->toDateString());
            $cashflow=[];

            foreach ($result->groupBy('format_type_name') as $key =>$item)
            {
                $child=[];
                $sumTotal=0;
                $sumBefore=0;
                foreach ($item->groupBy('format_id') as $i)
                {
                    $subTotal=0;
                    $before=$i->where('_original','<',$startDate->toDateString())
                        ->reduce(function ($a,$b)use ($subTotal){
                            if($b->coa_type=='Debit' && $b->type_coa=='Debit')
                            {
                                $subTotal+=$b->amount;
                            }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Debit')
                            {
                                $subTotal-=$b->amount;

                            }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Kredit')
                            {
                                $subTotal+=$b->amount;
                            }else{
                                $subTotal-=$b->amount;
                            }
                            return $a+$subTotal;
                        },0);
                    $total=$i->whereBetween('_original',[$startDate->toDateString(),$endDate->toDateString()])
                        ->reduce(function ($a,$b)use ($subTotal){
                            if($b->coa_type=='Debit' && $b->type_coa=='Debit')
                            {
                                $subTotal+=$b->amount;
                            }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Debit')
                            {
                                $subTotal-=$b->amount;

                            }elseif ($b->coa_type=='Kredit' && $b->type_coa=='Kredit')
                            {
                                $subTotal+=$b->amount;
                            }else{
                                $subTotal-=$b->amount;
                            }
                            return $a+$subTotal;
                        },0);
                    $child[]=(object)[
                        'name'=>$i->first()->format_name,
                        'amount'=>$total
                    ];
                    if($key=='operasional'){
                        if(strpos($i->first()->format_name,'Beban')!==false)
                        {
                            $sumBefore-=$before;
                            $sumTotal-=$total;
                        }else{
                            $sumBefore+=$before;
                            $sumTotal+=$total;
                        }
                    }else{
                        $sumBefore+=$before;
                        $sumTotal+=$total;
                    }
                }
                $cashflow[]=(object)[
                    'key'=>ucwords($key),
                    'child'=>$child,
                    'sum_all'=>$sumTotal,
                    'sum_before'=>$sumBefore,
                    'desc'=>'sum_all-sum_before,jika minus diabs kan dan diisi beri (nominal)'
                ];
            }

            $params=[
                'startDate'=>$startDate,
                'endDate'=>$endDate,
                'data'=>$cashflow
            ];
            return $this->message::getJsonResponse(200,'Data report tersedia',$params);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\QRIS;

use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\QRIS\RajabillerBank;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class RajabillerBankController extends Controller
{

    protected $message;
    protected $rajabiller;

    public function __construct()
    {
        $this->rajabiller=RajabillerBank::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/cashier/rajabiller')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/all', 'Api\v2\Senna\Merchant\Cashier\QRIS\RajabillerBankController@getAll');
            });
    }

    public function getAll()
    {
        try {

            $data=$this->rajabiller::all();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

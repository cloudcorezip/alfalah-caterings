<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\Shift;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantShift;
use App\Utils\Accounting\CoaUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class ShiftController extends Controller
{
    protected $message;

    protected $shift;

    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->shift=MerchantShift::class;
    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/toko/shift')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('start/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\Shift\ShiftController@start');
                Route::post('end/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\Shift\ShiftController@end');
                Route::post('active-shift/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\Shift\ShiftController@getActiveShift');
                Route::get('one/{Id}', 'Api\v2\Senna\Merchant\Cashier\Shift\ShiftController@getOne');
                Route::post('report/{Id}', 'Api\v2\Senna\Merchant\Cashier\Shift\ShiftController@getReportCloseCashier');
                Route::post('save-report/{Id}', 'Api\v2\Senna\Merchant\Cashier\Shift\ShiftController@saveReport');
                Route::post('report-all/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\Shift\ShiftController@getReportAll');
                Route::post('report-one/{id}', 'Api\v2\Senna\Merchant\Cashier\Shift\ShiftController@getReportOne');
            });
    }


    public function start(Request $request,$merchantId)
    {
        try {
            DB::beginTransaction();
            $check=$this->shift::where('md_merchant_id',$merchantId)
                ->where('staff_user_id',$request->user_id)
                ->where('is_active',1)
                ->first();
            $data=new $this->shift;
            if($check) {
                return $this->message::getJsonResponse(200,'Mohon maaf ada shift yang belum selesai!',[]);
            }
            date_default_timezone_set($request->timezone);

            $kasirOutlet=CoaDetail::whereRaw("upper(acc_coa_details.name)='KAS KASIR OUTLET'")
                ->select([
                    'acc_coa_details.*'
                ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)->first();
            if(is_null($kasirOutlet))
            {
                $coaCategoryKas=CoaCategory::where('code','1.1.01')
                    ->where('md_merchant_id',$merchantId)->first();

                $kasirOutlet=new CoaDetail();
                $kasirOutlet->code=CoaUtil::generateCoaDetail($coaCategoryKas->id);
                $kasirOutlet->name='Kas Kasir Outlet';
                $kasirOutlet->acc_coa_category_id=$coaCategoryKas->id;
                $kasirOutlet->is_deleted=0;
                $kasirOutlet->md_sc_currency_id=1;
                $kasirOutlet->type_coa='Debit';
                $kasirOutlet->save();
            }

            $data->amount=$request->amount;
            $data->md_merchant_id=$merchantId;
            $perantara=CoaDetail::where("acc_coa_details.code",'1.1.01.01')
                ->select([
                    'acc_coa_details.*'
                ])
                ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                ->where('c.md_merchant_id',$merchantId)->first();
            $payment=$kasirOutlet;
            $data->trans_coa_detail_id=$perantara->id;
            $data->to_coa_detail_id=$payment->id;
            $data->staff_user_id=$request->user_id;
            $data->is_first_shift=1;
            $data->is_active=1;
            $data->is_shift=1;
            $data->save();
            if($request->amount>0){
                $code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);
                $jurnal=new Jurnal();
                $jurnal->trans_amount=$request->amount;
                $jurnal->trans_time=date('Y-m-d H:i:s');
                $jurnal->md_user_id_created=$request->user_id;
                $jurnal->md_merchant_id=$merchantId;
                $jurnal->trans_name='Saldo Buka Kasir -'.Carbon::parse(date('Y-m-d H:i:s'))->isoFormat('D MMMM Y HH:MM:SS');
                $jurnal->trans_purpose='Saldo Buka Kasir -'.Carbon::parse(date('Y-m-d H:i:s'))->isoFormat('D MMMM Y HH:MM:SS');
                $jurnal->trans_code=$code;
                $jurnal->ref_code=$code;
                $jurnal->save();
                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$payment->id;
                $detjurnal->coa_type="Debit";
                $detjurnal->amount=$request->amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=date('Y-m-d H:i:s');
                $detjurnal->updated_at=date('Y-m-d H:i:s');
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$perantara->id;
                $detjurnal->coa_type="Kredit";
                $detjurnal->amount=$request->amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=date('Y-m-d H:i:s');
                $detjurnal->updated_at=date('Y-m-d H:i:s');
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();
            }
            DB::commit();
            return $this->message::getJsonResponse(200,'Shift  berhasil ditambahkan!',$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function end(Request $request,$id)
    {
        try {
            DB::beginTransaction();
            $check=$this->shift::find($id);
            if($check->is_active==0){
                return $this->message::getJsonResponse(404,'Shift ini sudah ditutup',[]);
            }
            date_default_timezone_set($request->timezone);

            $date=new Carbon();

            if($check){
                $check->is_active=0;
                $check->end_date=date('Y-m-d H:i:s');
                $check->actual_amount=$request->actual_amount;
                $check->save();
                DB::commit();
                return $this->message::getJsonResponse(200,'Shift  berhasil diakhiri!',$check);
            }else{
                return $this->message::getJsonResponse(404,'Data shift tidak ditemukan',[]);
            }
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function getActiveShift($merchantId,Request $request)
    {
        try {
            $data=$this->shift::where('md_merchant_id',$merchantId)
                ->where('is_active',1)
                ->where('staff_user_id',$request->user_id)
                ->first();
            if($data){
                return $this->message::getJsonResponse(200,'Data tersedia',$data);
            }else{
                return $this->message::getJsonResponse(404,'Data shift tidak ditemukan',[]);
            }
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function getOne($id)
    {
        try {
            $data=$this->shift::find($id);
            if($data){

                return $this->message::getJsonResponse(200,'Data tersedia',$data);
            }else{

                return $this->message::getJsonResponse(404,'Data shift tidak ditemukan',[]);
            }
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }


    public function getReportCloseCashier($merchantId,Request  $request)
    {
        try{
            $endDate=(is_null($request->end_date))?date('Y-m-d H:i:s'):$request->end_date;
            $shift=$this->shift::find($request->end_id);
            $date=date('Y-m-d', strtotime($shift->created_at));
            $endId=$shift->id;
            $userId=$shift->staff_user_id;
            $data=DB::select("
            select
              u.fullname,
              r.name as role_name,
              s.created_at as start_date,
              s.amount as start_amount,
              s.end_date,
              s.actual_amount,
              coalesce(
                (
                  select
                    sum(sso.total)
                  from
                    sc_sale_orders sso
                  where
                    sso.md_merchant_id = s.md_merchant_id
                    and sso.md_sc_transaction_status_id = 2
                    and sso.updated_at between s.created_at
                    and '$endDate'
                    and sso.md_transaction_type_id = 2
                    and sso.is_deleted = 0
                    and sso.created_by = $userId
                    and sso.is_debet = 0
                ),
                0
              )+ coalesce(
                (
                  select
                    sum(amad.paid_nominal)
                  from
                    acc_merchant_ar_details amad
                    join acc_merchant_ar ar on amad.acc_merchant_ar_id = ar.id
                    join acc_coa_details acd on acd.id = amad.trans_coa_detail_id
                  where
                    amad.created_by = $userId
                    and amad.updated_at between s.created_at
                    and '$endDate'
                    and amad.is_deleted = 0
                    and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                    and ar.is_deleted = 0
                    and ar.md_merchant_id = s.md_merchant_id
                    and upper(acd.name) like '%KAS%'
                ),
                0
              )- coalesce(
                (
                  select
                    sum(srso.total)
                  from
                    sc_retur_sale_orders srso
                  join 
                    sc_sale_orders sso on sso.id = srso.sc_sale_order_id
                  where
                    srso.created_by = $userId
                    and srso.updated_at between s.created_at
                    and '$endDate'
                    and srso.is_deleted = 0
                    and srso.reason_id in(2,3)
                    and sso.md_transaction_type_id = 2
                ),
                0
              )
               cash_sale,
              coalesce(
                (
                  select
                    sum(sso.total)
                  from
                    sc_sale_orders sso
                  where
                    sso.md_merchant_id = s.md_merchant_id
                    and sso.md_sc_transaction_status_id = 2
                    and sso.updated_at between s.created_at
                    and '$endDate'
                    and sso.md_transaction_type_id != 2
                    and sso.is_deleted = 0
                    and sso.created_by = $userId
                    and sso.is_debet = 0
                ),
                0
              )+ coalesce(
                (
                  select
                    sum(amad.paid_nominal)
                  from
                    acc_merchant_ar_details amad
                    join acc_merchant_ar ar on amad.acc_merchant_ar_id = ar.id
                    join acc_coa_details acd on acd.id = amad.trans_coa_detail_id
                  where
                    amad.created_by = $userId
                    and amad.updated_at between s.created_at
                    and '$endDate'
                    and amad.is_deleted = 0
                    and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                    and ar.is_deleted = 0
                    and ar.md_merchant_id = s.md_merchant_id
                    and upper(acd.name) not like '%KAS%'
                ),
                0
              )- coalesce(
                (
                  select
                    sum(srso.total)
                  from
                    sc_retur_sale_orders srso
                  join 
                    sc_sale_orders sso on sso.id = srso.sc_sale_order_id
                  where
                    srso.created_by = $userId
                    and srso.updated_at between s.created_at
                    and '$endDate'
                    and srso.is_deleted = 0
                    and srso.reason_id in(2,3)
                    and sso.md_transaction_type_id != 2
                ),
                0
              ) non_cash_sale,
              coalesce(
                (
                  select
                    count(*)
                  from
                    sc_sale_orders sso
                  where
                    sso.md_merchant_id = s.md_merchant_id
                    and sso.updated_at between s.created_at
                    and '$endDate'
                    and sso.md_sc_transaction_status_id = 2
                    and sso.is_deleted = 0
                    and sso.created_by = $userId
                ),
                0
              ) sale_finish,
              coalesce(
                (
                  select
                    count(*)
                  from
                    sc_sale_orders sso
                  where
                    sso.md_merchant_id = s.md_merchant_id
                    and sso.updated_at between s.created_at
                    and '$endDate'
                    and sso.md_sc_transaction_status_id = 1
                    and sso.is_deleted = 0
                    and sso.created_by = $userId
                ),
                0
              ) sale_not_finish,
              coalesce(
                (
                  select
                    sum(spo.total)
                  from
                    sc_purchase_orders spo
                  where
                    spo.md_merchant_id = s.md_merchant_id
                    and spo.md_sc_transaction_status_id = 2
                    and spo.updated_at between s.created_at
                    and '$endDate'
                    and spo.md_transaction_type_id = 2
                    and spo.is_deleted = 0
                    and spo.created_by = $userId
                    and spo.is_debet = 0
                ),
                0
              )+ coalesce(
                (
                  select
                    sum(amar.paid_nominal)
                  from
                    acc_merchant_ap_details amar
                    join acc_merchant_ap ap on amar.acc_merchant_ap_id = ap.id
                    join acc_coa_details acd on acd.id = amar.trans_coa_detail_id
                  where
                    amar.created_by = $userId
                    and amar.updated_at between s.created_at
                    and '$endDate'
                    and amar.is_deleted = 0
                    and ap.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                    and ap.is_deleted = 0
                    and ap.md_merchant_id = s.md_merchant_id
                    and upper(acd.name) like '%KAS%'
                ),0
              )- coalesce(
                (
                  select
                    sum(srso.total)
                  from
                    sc_retur_purchase_orders srso
                  join 
                    sc_purchase_orders sso on sso.id = srso.sc_purchase_order_id
                  where
                    srso.created_by = $userId
                    and srso.updated_at between s.created_at
                    and '$endDate'
                    and srso.is_deleted = 0
                    and srso.reason_id in(2,3)
                    and sso.md_transaction_type_id = 2
                ),
                0
              ) cash_purchase,
              coalesce(
                (
                  select
                    sum(spo.total)
                  from
                    sc_purchase_orders spo
                  where
                    spo.md_merchant_id = s.md_merchant_id
                    and spo.md_sc_transaction_status_id = 2
                    and spo.updated_at between s.created_at
                    and '$endDate'
                    and spo.md_transaction_type_id != 2
                    and spo.is_deleted = 0
                    and spo.created_by = $userId
                    and spo.is_debet = 0
                ),
                0
              )+ coalesce(
                (
                  select
                    sum(amar.paid_nominal)
                  from
                    acc_merchant_ap_details amar
                    join acc_merchant_ap ap on amar.acc_merchant_ap_id = ap.id
                    join acc_coa_details acd on acd.id = amar.trans_coa_detail_id
                  where
                    amar.created_by = $userId
                    and amar.updated_at between s.created_at
                    and '$endDate'
                    and amar.is_deleted = 0
                    and ap.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                    and ap.is_deleted = 0
                    and ap.md_merchant_id = s.md_merchant_id
                    and upper(acd.name) not like '%KAS%'
                ),0
              )- coalesce(
                (
                  select
                    sum(srso.total)
                  from
                    sc_retur_purchase_orders srso
                  join 
                    sc_purchase_orders sso on sso.id = srso.sc_purchase_order_id
                  where
                    srso.created_by = $userId
                    and srso.updated_at between s.created_at
                    and '$endDate'
                    and srso.is_deleted = 0
                    and srso.reason_id in(2,3)
                    and sso.md_transaction_type_id != 2
                ),
                0
              ) non_cash_purchase,
              coalesce(
                (
                  select
                    sum(amc.amount + amc.admin_fee)
                  from
                    acc_merchant_cashflows amc
                    join acc_coa_details d on d.id = amc.to_cashflow_coa_detail_id
                  where
                    amc.md_merchant_id = s.md_merchant_id
                    and upper(d.name) like '%KAS%'
                    and amc.is_deleted = 0
                    and amc.created_at between s.created_at
                    and '$endDate'
                    and amc.md_sc_cash_type_id = 3
                    and amc.created_by = $userId
                ),
                0
              ) other_revenue,
               coalesce(
                (
                  select
                    sum(amc.amount + amc.admin_fee)
                  from
                    acc_merchant_cashflows amc
                    join acc_coa_details d on d.id = amc.to_cashflow_coa_detail_id
                  where
                    amc.md_merchant_id = s.md_merchant_id
                    and upper(d.name) not like '%KAS%'
                    and amc.is_deleted = 0
                    and amc.created_at between s.created_at
                    and '$endDate'
                    and amc.md_sc_cash_type_id = 3
                    and amc.created_by = $userId
                ),
                0
              ) other_revenue_not_cash,
              coalesce(
                (
                  select
                    sum(amc.amount + amc.admin_fee)
                  from
                    acc_merchant_cashflows amc
                    join acc_coa_details d on d.id = amc.to_cashflow_coa_detail_id
                  where
                    amc.md_merchant_id = s.md_merchant_id
                    and upper(d.name) like '%KAS%'
                    and amc.is_deleted = 0
                    and amc.created_at between s.created_at
                    and '$endDate'
                    and amc.md_sc_cash_type_id = 2
                    and amc.created_by = $userId
                ),
                0
              ) other_cost,
              coalesce(
                (
                  select
                    sum(amc.amount + amc.admin_fee)
                  from
                    acc_merchant_cashflows amc
                    join acc_coa_details d on d.id = amc.to_cashflow_coa_detail_id
                  where
                    amc.md_merchant_id = s.md_merchant_id
                    and upper(d.name) not like '%KAS%'
                    and amc.is_deleted = 0
                    and amc.created_at between s.created_at
                    and '$endDate'
                    and amc.md_sc_cash_type_id = 2
                    and amc.created_by = $userId
                ),
                0
              ) other_cost_not_cash,
              (
                SELECT
                  jsonb_agg(jd.*) AS jsonb_agg
                FROM
                  (
                    select
                      sp.id,
                      sp.name,
                      mu.name as unit_name,
                      sum(dd.quantity) as quantity
                    from
                      sc_products sp
                      left join md_units mu on mu.id = sp.md_unit_id
                      join(
                        select
                          ssod.sc_product_id,
                          ssod.quantity
                        from
                          sc_sale_order_details ssod
                          join sc_sale_orders sso on sso.id = ssod.sc_sale_order_id
                        where
                          sso.is_deleted = 0
                          and sso.is_keep_transaction = 0
                          and ssod.is_bonus = 0
                          and sso.md_merchant_id = s.md_merchant_id
                          and sso.updated_at between s.created_at
                          and '$endDate'
                          and sso.created_by = $userId
                      ) dd on dd.sc_product_id = sp.id
                    group by
                      sp.id,
                      mu.name
                  ) jd
              ) as json_sale_menus
            from
              acc_merchant_shifts s
              left join md_users u on u.id = s.staff_user_id
              left join md_roles r on r.id = u.md_role_id
            where
              s.md_merchant_id = $merchantId
              and s.id = $endId
            ");

            if($data){

                return $this->message::getJsonResponse(200,'Data report tersedia',$data);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }


    public function saveReport(Request $request,$id)
    {
        try {
            DB::beginTransaction();
            $check=$this->shift::find($id);
            if(!is_null($check->json_response)){
                return $this->message::getJsonResponse(404,'Shift ini sudah ditutup',[]);
            }
            if($check){
                date_default_timezone_set($request->timezone);
                $check->json_response=$request->json_response;
                $check->system_amount=$request->system_amount;
                $check->save();

                $kasirOutlet=CoaDetail::whereRaw("upper(acc_coa_details.name)='KAS KASIR OUTLET'")
                    ->select([
                        'acc_coa_details.*'
                    ])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$check->md_merchant_id)->first();


                $perantara=CoaDetail::where("acc_coa_details.code",'1.1.01.01')
                    ->select([
                        'acc_coa_details.*'
                    ])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$check->md_merchant_id)->first();
                $payment=$kasirOutlet;

                $code=CodeGenerator::generateJurnalCode(0,'JRN',$check->md_merchant_id);
                $jurnal=new Jurnal();
                $jurnal->trans_amount=$request->system_amount;
                $jurnal->trans_time=$check->end_date;
                $jurnal->md_user_id_created=$check->staff_user_id;
                $jurnal->md_merchant_id=$check->md_merchant_id;
                $jurnal->trans_name='Saldo Tutup Kasir -'.Carbon::parse($check->end_date)->isoFormat('D MMMM Y HH:MM:SS');
                $jurnal->trans_purpose='Saldo Tutup Kasir -'.Carbon::parse($check->end_date)->isoFormat('D MMMM Y HH:MM:SS');
                $jurnal->trans_code=$code;
                $jurnal->ref_code=$code;
                $jurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$payment->id;
                $detjurnal->coa_type="Kredit";
                $detjurnal->amount=$request->system_amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$check->end_date;
                $detjurnal->updated_at=$check->end_date;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$perantara->id;
                $detjurnal->coa_type="Debit";
                $detjurnal->amount=$request->system_amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=$check->end_date;
                $detjurnal->updated_at=$check->end_date;
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();
                DB::commit();
                return $this->message::getJsonResponse(200,'Shift  berhasil diakhiri!',$check);
            }else{
                return $this->message::getJsonResponse(404,'Data shift tidak ditemukan',[]);
            }
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

//    public function getReportAll(Request  $request,$merchantId)
//    {
//        try {
//            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
//            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
//            $offset=(is_null($request->offset))?0:$request->offset;
//            $limit=(is_null($request->limit))?10:$request->limit;
//
//            $data=DB::select("select
//              u.fullname,
//              r.name as role_name,
//              s.created_at as start_date,
//              s.amount as start_amount,
//              s.end_date,
//              s.actual_amount,
//              s.system_amount,
//              s.end_date,
//              s.is_active,
//              s.json_response
//            from
//              acc_merchant_shifts s
//              left join md_users u on u.id = s.staff_user_id
//              left join md_roles r on r.id = u.md_role_id
//            where
//              s.md_merchant_id = $merchantId and s.is_deleted=0
//              and s.created_at::date between '".$startDate."' and '".$endDate."'
//              order by s.id asc
//              offset $offset limit $limit
//
//              ");
//
//            if($data){
//
//                return $this->message::getJsonResponse(200,'Data report tersedia',$data);
//            }else{
//
//                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
//            }
//
//        }catch (\Exception $e)
//        {
//            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
//            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
//
//        }
//    }


    public function getReportAll(Request  $request,$merchantId)
    {
        try {
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;

            $data=DB::select("
            WITH shiftData_$merchantId AS (
                  select
                    mon.mon :: date AS time,
                    coalesce(
                      (
                        SELECT
                          jsonb_agg(jd.*) AS jsonb_agg
                        FROM
                          (
                            select
                              s.id,
                              u.fullname,
                              r.name as role_name,
                              s.created_at as start_date,
                              s.amount as start_amount,
                              s.end_date,
                              s.actual_amount,
                              s.system_amount,
                              s.end_date,
                              s.is_active,
                              s.json_response
                            from
                              acc_merchant_shifts s
                              left join md_users u on u.id = s.staff_user_id
                              left join md_roles r on r.id = u.md_role_id
                            where
                              s.md_merchant_id = $merchantId
                              and s.is_deleted = 0
                              and s.created_at :: date = mon.mon :: date
                          ) jd
                      ),
                      '[]'
                    ) AS get_detail
                  from
                    generate_series(
                      '$startDate' :: timestamp, '$endDate' :: timestamp,
                      interval '1 day'
                    ) as mon(mon)
                    order by time desc
                )
                select * from shiftData_$merchantId where get_detail!='[]' offset $offset limit $limit;
              ");

            if($data){

                return $this->message::getJsonResponse(200,'Data report tersedia',$data);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function getReportOne(Request  $request,$id)
    {
        try {

            $data=DB::select("
            select 
            s.id as id_shift,
            s.amount,
            s.actual_amount,
            s.system_amount,
            (
                        SELECT
                          jsonb_agg(js.*) AS jsonb_agg
                        FROM
                          (
                            select
                          sso.id,
                          sso.code,
                          sso.total as total,
                          c.name as customer_name,
                          sso.created_at as date
                        from
                          sc_sale_orders sso
                          left join sc_customers c on c.id = sso.sc_customer_id
                        where
                          sso.created_by = s.staff_user_id
                          and sso.is_keep_transaction = 0
                          and sso.is_editable = 1
                          and sso.is_deleted = 0
                          and sso.md_sc_transaction_status_id not in(3, 5)
                          and sso.step_type = 0
                          and sso.created_at between '".$request->start_date."'
                            and '".$request->end_date."'
                          ) js
                      ) AS sale_order,
                      (
                        SELECT
                          jsonb_agg(jp.*) AS jsonb_agg
                        from
                        (
                        select
                        spo.id,
                        spo.code,
                        spo.total as total,
                        sup.name as supplier_name,
                        spo.created_at as date
                      from 
                        sc_purchase_orders spo
                        left join sc_suppliers sup on sup.id = spo.sc_supplier_id
                      where
                        spo.created_by = s.staff_user_id
                        and spo.is_editable = 1
                        and spo.is_deleted = 0
                        and spo.md_sc_transaction_status_id not in(3, 5)
                        and spo.step_type = 0
                        and spo.created_at between '".$request->start_date."'
                          and '".$request->end_date."'
                        ) jp
                       ) AS purchase_order,
                       (
                        SELECT
                          jsonb_agg(km.*) AS jsonb_agg
                        from
                        (
                        select
                        c.id,
                        c.code,
                        c.amount,
                        coa.name as coa_name,
                        c.created_at as date
                      from acc_merchant_cashflows c
                        join acc_coa_details coa on coa.id = c.cashflow_coa_detail_id
                      where c.md_sc_cash_type_id=2
                        and c.is_deleted=0
                        and c.created_by = s.staff_user_id
                        and c.created_at between '".$request->start_date."'
                          and '".$request->end_date."'
                        ) km
                       ) AS kas_keluar,
                       (
                        SELECT
                          jsonb_agg(kk.*) AS jsonb_agg
                        from
                        (
                        select
                        cc.id,
                        cc.code,
                        cc.amount,
                        coa.name as coa_name,
                        cc.created_at as date
                      from acc_merchant_cashflows cc
                        join acc_coa_details coa on coa.id = cc.cashflow_coa_detail_id
                      where cc.md_sc_cash_type_id=3
                        and cc.is_deleted=0
                        and cc.created_by = s.staff_user_id
                        and cc.created_at between '".$request->start_date."'
                          and '".$request->end_date."'
                        ) kk
                       ) AS kas_masuk
                      from
                      acc_merchant_shifts s
                    where
                      s.id = $id    
            ");

            
              if($data){

                return $this->message::getJsonResponse(200,'Data report tersedia',$data);
            }else{

                return $this->message::getJsonResponse(404,'Data laporan tidak ditemukan',[]);
            }

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

}

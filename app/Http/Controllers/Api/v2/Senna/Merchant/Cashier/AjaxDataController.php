<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Http\Controllers\Controller;
use Modules\Merchant\Entities\Toko\CategoryEntity;
use Modules\Merchant\Entities\Toko\MerkEntity;
use App\Models\MasterData\Unit;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class AjaxDataController extends Controller
{

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/ajax-data')
            ->middleware('etag')
            ->group(function(){
                Route::get('/get-product-category', [static::class, 'getCategoryProduct'])->name('get.product.category');
                Route::get('/get-merk', [static::class, 'getMerk'])->name('get.merk');
                Route::get('/get-unit', [static::class, 'getUnit'])->name('get.unit');

            });
    }

    public function getCategoryProduct(Request $request)
    {
        try{
            $search = $request->key;
            $getBranch=MerchantUtil::getBranch($request->merchant_id,1);

            if($search == ''){
                $data=CategoryEntity::getDataForDataTable()->whereIn('m.id',$getBranch)->where('sc_product_categories.is_deleted',0)
                ->get();
            }else{
                $data=CategoryEntity::getDataForDataTable()->whereIn('m.id',$getBranch)->where('sc_product_categories.is_deleted',0)
                ->whereRaw("upper(sc_product_categories.name) like '%".strtoupper($search)."%' ")->get();
            }

            
            $response =[];
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->name
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }

    public function getMerk(Request $request)
    {
        try{
            $search = $request->key;
            $getBranch=MerchantUtil::getBranch($request->merchant_id,1);

            if($search == ''){
                $data=MerkEntity::getDataForDataTable()->whereIn('m.id',$getBranch)->where('sc_merks.is_deleted',0)
                ->get();
            }else{
                $data=MerkEntity::getDataForDataTable()->whereIn('m.id',$getBranch)->where('sc_merks.is_deleted',0)
                ->whereRaw("upper(sc_merks.name) like '%".strtoupper($search)."%' ")->get();
            }

            
            $response =[];
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->name
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }

    public function getUnit(Request $request)
    {
        try{
            $search = $request->key;

            if($search == ''){
                $data=Unit::all();
            }else{
                $data=Unit::whereRaw("upper(name) like '%".strtoupper($search)."%' ")->get();
            }

            
            $response =[];
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->name
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            dd($e);
            return response()->json($e);


        }

    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\DiscountProduct;
use App\Models\SennaToko\DiscountProductDetail;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class DiscountByProductController extends Controller
{

    protected $message;
    protected $discount;
    protected $product;
    protected $details;

    public function __construct()
    {
        $this->product=Product::class;
        $this->discount=DiscountProduct::class;
        $this->details=DiscountProductDetail::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/discount-by-product')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@create');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@update');
                Route::get('/all/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@getAll');
                Route::post('/all/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@getAll');
                Route::post('/active/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@getActive');
                Route::post('/code/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@getByCode');
                Route::get('/one/{id}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@getOne');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@delete');
                Route::get('/all-web/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@getAllWeb')->name('api.discount.all');
                Route::get('/discount-product/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\DiscountByProductController@getDiscountProduct')->name('api.discount.product');
            });
    }

    public function getDiscountProduct($merchantId,Request $request)
    {

            $data=$this->discount::select([
                'd.discount_by_product_id',
                'name as text',
                'bonus_type',
                'bonus_value',
                'is_applies_multiply',
                'promo_type',
                'promo_value'
            ])
                ->join('crm_discount_by_product_details as d','d.discount_by_product_id','crm_discount_by_products.id')
                ->where('start_date','<=',$request->date)
                ->where('end_date','>=',$request->date)
                ->where('d.sc_product_id',$request->product)
                ->where('md_merchant_id',$merchantId)
                ->where('is_type',1)
                ->where('is_deleted',0)
                ->where('is_active',1)
                ->where('day','like','%'.$request->day.'%')
                ->orderBy('crm_discount_by_products.created_at','desc')
                ->get();


        $response =[];
        foreach($data as $key => $item){
            if($key==0){
                $response[] = [
                    "id"=>0,
                    "text"=>"Tanpa Promo",
                    "bonus_type"=>0,
                    "bonus_value"=>0
                ];
            }
            foreach(json_decode($item->bonus_value) as $key => $b){
            if($item->bonus_type==1){
                if($item->is_applies_multiply==1){
                    $name=$item->text."(Diskon Rp.".$b->value.", berlaku kelipatan)";
                }else{
                    $name=$item->text."(Diskon Rp.".$b->value.")";
                }
            }else if($item->bonus_type==2){
                $name=$item->text."(Diskon ".$b->value."%)";
            }else{
                $name=$item->text."(Bonus Produk)";
            }

                if($item->promo_type==1){
                    if($item->promo_value<=$request->subtotal){
                        $response[] = [
                            "id"=>$item->discount_by_product_id,
                            "text"=>$name,
                            "bonus_type"=>$item->bonus_type,
                            "bonus_value"=>$b->value,
                            "is_applies_multiply"=>$item->is_applies_multiply,
                            "promo_type"=>$item->promo_type,
                            "promo_value"=>$item->promo_value
                        ];
                    }
                }else{
                    if($item->promo_value<=$request->quantity){
                        $response[] = [
                            "id"=>$item->discount_by_product_id,
                            "text"=>$name,
                            "bonus_type"=>$item->bonus_type,
                            "bonus_value"=>$b->value,
                            "is_applies_multiply"=>$item->is_applies_multiply,
                            "promo_type"=>$item->promo_type,
                            "promo_value"=>$item->promo_value
                        ];
                    }
                }
            }


        }

        return response()->json($response);
    }

    public function getAllWeb($merchantId,Request $request)
    {

            $data=$this->discount::select([
                'id',
                'name as text',
                'bonus_type',
                'bonus_value',
                'is_applies_multiply',
                'promo_type',
                'promo_value'
            ])
                ->where('start_date','<=',$request->date)
                ->where('end_date','>=',$request->date)
                ->where('md_merchant_id',$merchantId)
                ->where('promo_value','<=',$request->total)
                ->where('is_type',2)
                ->where('is_deleted',0)
                ->where('is_active',1)
                ->where('day','like','%'.$request->day.'%')
                ->get();


        $response =[];
        foreach($data as $key => $item){
            if($key==0){
                $response[] = [
                    "id"=>0,
                    "text"=>"Tanpa Promo",
                    "bonus_type"=>0,
                    "bonus_value"=>0
                ];
            }
            foreach(json_decode($item->bonus_value) as $key => $b){
                if($item->bonus_type==1){
                    if($item->is_applies_multiply==1){
                        $name=$item->text."(Diskon Rp.".$b->value.", berlaku kelipatan)";
                        $value=$b->value;
                    }else{
                        $name=$item->text."(Diskon Rp.".$b->value.")";
                        $value=$b->value;
                    }
                }else if($item->bonus_type==2){
                    $name=$item->text."(Diskon ".$b->value."%)";
                    $value=$b->value;
                }else{
                    $name=$item->text."(Bonus Produk)";
                    $value=$b->value;
                }

            }

            $response[] = [
                "id"=>$item->id,
                "text"=>$name,
                "bonus_type"=>$item->bonus_type,
                "bonus_value"=>$value,
                "is_applies_multiply"=>$item->is_applies_multiply,
                "promo_type"=>$item->promo_type,
                "promo_value"=>$item->promo_value
            ];


        }

        return response()->json($response);
    }

    public function getAll($merchantId,Request $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $order=is_null($request->order)?'asc':$request->order;

                $data=DB::select("
                select
                    cdbp.id,
                    cdbp.name,
                    cdbp.desc,
                    cdbp.code,
                    cdbp.activation_type,
                    cdbp.bonus_type,
                    cdbp.bonus_value,
                    cdbp.promo_type,
                    cdbp.promo_value,
                    cdbp.start_date,
                    cdbp.end_date,
                    cdbp.is_active,
                    cdbp.is_type,
                    cdbp.is_applies_multiply,
                    cdbp.day,
                    cdbp.foto,
                    (case when is_type=1 then
                    (select
                        jsonb_agg(dt)
                        from
                            (
                                select
                                p.id ,
                                p.name
                                from crm_discount_by_product_details det
                                join sc_products p on p.id = det.sc_product_id
                                where det.discount_by_product_id = cdbp.id
                            ) dt)
                    end) detail
                    from crm_discount_by_products cdbp
                    where cdbp.is_deleted=0 and is_type=1 and cdbp.md_merchant_id=$merchantId order by cdbp.id $order limit $limit offset $offset");


            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,'Promo tidak tersedia!',[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getActive($merchantId,Request $request)
    {
        try {
                $data=DB::select("
                select
                    cdbp.id,
                    cdbp.name,
                    cdbp.desc,
                    cdbp.code,
                    cdbp.activation_type,
                    cdbp.bonus_type,
                    cdbp.bonus_value,
                    cdbp.promo_type,
                    cdbp.promo_value,
                    cdbp.start_date,
                    cdbp.end_date,
                    cdbp.is_active,
                    cdbp.is_type,
                    cdbp.is_applies_multiply,
                    cdbp.day,
                    (case when is_type=1 then
                    (select
                        jsonb_agg(dt)
                        from
                            (
                                select
                                p.id ,
                                p.name
                                from crm_discount_by_product_details det
                                join sc_products p on p.id = det.sc_product_id
                                where det.discount_by_product_id = cdbp.id
                            ) dt)
                    end) detail
                    from crm_discount_by_products cdbp
                    where '".$request->date."' between cdbp.start_date and cdbp.end_date
                    and cdbp.is_deleted=0 and cdbp.is_active=1 and md_merchant_id=".$merchantId."
                    and cdbp.day @> '[{".'"hari"'.":".'"'."".$request->day."".'"'."}]'
                ");


            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,'Promo tidak tersedia!',[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getByCode($merchantId,Request $request)
    {
        try {
                $data=DB::select("
                select
                    cdbp.id,
                    cdbp.name,
                    cdbp.desc,
                    cdbp.code,
                    cdbp.activation_type,
                    cdbp.bonus_type,
                    cdbp.bonus_value,
                    cdbp.promo_type,
                    cdbp.promo_value,
                    cdbp.start_date,
                    cdbp.end_date,
                    cdbp.is_active,
                    cdbp.is_type,
                    cdbp.is_applies_multiply,
                    cdbp.day,
                    (case when is_type=1 then
                    (select
                        jsonb_agg(dt)
                        from
                            (
                                select
                                p.id ,
                                p.name
                                from crm_discount_by_product_details det
                                join sc_products p on p.id = det.sc_product_id
                                where det.discount_by_product_id = cdbp.id
                            ) dt)
                    end) detail
                    from crm_discount_by_products cdbp
                    where '".$request->date."' between cdbp.start_date and cdbp.end_date and cdbp.code='".$request->code."'
                    and cdbp.is_deleted=0 and cdbp.is_active=1 and md_merchant_id=".$merchantId."
                    and cdbp.day @> '[{".'"hari"'.":".'"'."".$request->day."".'"'."}]'
                ");


            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,'Promo tidak tersedia!',[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {
            $data=$this->discount::where('id',$id)
                ->where('is_deleted',0)
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function create(Request  $request,$merchantId)
    {
        try{
            DB::beginTransaction();
            $data = new $this->discount;

            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            if($request->is_type==1){
                $code=$this->discount::where('code',$request->code)->where('md_merchant_id',$merchantId)->where('is_type',1)->where('is_deleted',0)->get();
            }else{
                $code=$this->discount::where('code',$request->code)->where('md_merchant_id',$merchantId)->where('is_type',2)->where('is_deleted',0)->get();
            }
            if(count($code)>0){
                return $this->message::getJsonResponse(404,'Kode promo telah digunakan',[]);
            }
            if($request->is_type==1){

                $det=json_decode($request->details);

                foreach($det as $key=> $item){
                    $start=$request->start_date;
                    $end=$request->end_date;
                    $promoActive=$this->discount::join('crm_discount_by_product_details as d','d.discount_by_product_id','crm_discount_by_products.id')
                                                ->where('is_active',1)
                                                ->where('is_type',1)
                                                ->where('is_deleted',0)
                                                ->where('is_coupon',0)
                                                ->where('is_referral_afiliate',0)
                                                ->where('crm_discount_by_products.md_merchant_id',$merchantId)
                                                ->where('d.sc_product_id',$item->sc_product_id)
                                                ->where(function($q) use($start,$end){
                                                    $q->where([['start_date','<=',$start],
                                                                ['end_date','>=',$start]])
                                                        ->orWhere([['start_date','>=',$start],
                                                                    ['start_date','<=',$end]]);
                                                })
                                                ->get();
                    if(count($promoActive)>0){
                        return $this->message::getJsonResponse(404,'Ada promo pada jangka waktu ini',[]);
                    }
                }
            }else{
                $start=$request->start_date;
                $end=$request->end_date;
                    $promoActive=$this->discount::where(function($q) use($start,$end){
                                                    $q->where([['start_date','<=',$start],
                                                                ['end_date','>=',$start]])
                                                        ->orWhere([['start_date','>=',$start],
                                                                    ['start_date','<=',$end]]);
                                                })
                                                ->where('crm_discount_by_products.md_merchant_id',$merchantId)
                                                ->where('is_active',1)
                                                ->where('is_type',2)
                                                ->where('is_coupon',0)
                                                ->where('is_referral_afiliate',0)
                                                ->where('is_deleted',0)
                                                ->get();

                    if(count($promoActive)>0){
                        return $this->message::getJsonResponse(404,'Ada promo pada jangka waktu ini',[]);
                    }
            }


            $destinationPath = 'public/uploads/merchant/'.$merchantId.'/promo/';

            if($request->hasFile('foto'))
            {
                $file=$request->file('foto');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }

            $data->name=$request->name;
            $data->desc=$request->desc;
            $data->code=$request->code;
            $dayName=[];
            foreach(json_decode($request->day) as $key => $item){
                $dayName[]=["hari"=>$item->hari];
            }
            $data->day=json_encode($dayName);
            $data->start_date=$request->start_date;
            $data->end_date=$request->end_date;
            $data->activation_type=$request->activation_type;
            $data->bonus_type=$request->bonus_type;
            $data->is_applies_multiply=$request->is_applies_multiply;
            $bonusValue=[];
            foreach(json_decode($request->bonus_value) as $item){
                if($request->bonus_type==3){
                    $p=$this->product::where('is_deleted',0)->find($item->value);
                    if($p->count()<1){
                        return $this->message::getJsonResponse(404,'Bonus produkmu tidak ada',[]);
                    }
                    $bonusValue[]=[
                        "value"=>$item->value,
                        "name"=>$p->name,
                        "foto"=>$p->foto,
                        "description"=>$p->description,
                        "is_with_stock"=>$p->is_with_stock,
                        "code"=>$p->code,
                        "selling_price"=>$p->selling_price,
                        "quantity"=>$item->quantity
                        ];
                }else{
                    $bonusValue[]=[
                        "value"=>$item->value
                        ];
                }

            }
            $data->bonus_value=json_encode($bonusValue);
            $data->promo_type=$request->promo_type;
            $data->is_type=$request->is_type;
            $data->promo_value=$request->promo_value;
            $data->foto=$fileName;
            $data->md_merchant_id=$merchantId;
            $data->is_active=$request->is_active;
            $data->save();

            if($request->is_type==1){
                $details=[];
                foreach(json_decode($request->details) as $key => $item){
                    $details[]=[
                        'discount_by_product_id'=>$data->id,
                        'sc_product_id'=>$item->sc_product_id,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                        ];
                }
                DB::table('crm_discount_by_product_details')
                ->insert($details);
            }

            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }
    
    public function update(Request  $request,$id)
    {
        try{
            DB::beginTransaction();
            $data = $this->discount::find($id);
            $promoDetail = $this->details::where('discount_by_product_id',$data->id)->get();
            foreach($promoDetail as $item){
                $start=$data->start_date;
                $end=$data->end_date;
                $promoActive=$this->discount::join('crm_discount_by_product_details as d','d.discount_by_product_id','crm_discount_by_products.id')
                                            ->where('is_active',1)
                                            ->where('is_deleted',0)
                                            ->where('crm_discount_by_products.md_merchant_id',$data->md_merchant_id)
                                            ->where('discount_by_product_id','!=',$data->id)
                                            ->where('d.sc_product_id',$item['sc_product_id'])
                                            ->where(function($q) use($start,$end){
                                                $q->where([['start_date','<=',$start],
                                                            ['end_date','>=',$start]])
                                                    ->orWhere([['start_date','>=',$start],
                                                                ['start_date','<=',$end]]);
                                            })
                                            ->get();
                if(count($promoActive)>0){
                    return $this->message::getJsonResponse(404,'Ada promo pada jangka waktu ini',[]);
                }
            }
            $destinationPath = 'public/uploads/merchant/'.$data->md_merchant_id.'/promo/';

            if($request->hasFile('foto'))
            {
                $file=$request->file('foto');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                if(is_null($id)){
                    $fileName=null;
                }else{
                    $fileName=$data->foto;
                }
            }

            $data->foto=$fileName;
            $data->is_active=$request->is_active;
            $data->start_date=$request->start_date;
            $data->end_date=$request->end_date;
            $data->day=$request->day;

            $data->save();
            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $data =$this->discount::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $data->is_deleted=1;
            $data->save();
            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

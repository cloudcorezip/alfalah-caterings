<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\WIthdraw;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\MasterData\Merchant;
use App\Models\QRIS\QRISActivation;
use App\Models\QRIS\QRISWithdrawActivity;
use App\Utils\Winpay\RajaBillerUtil;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Modules\Merchant\Entities\Acc\JournalEntity;

class WithdrawController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/withdraw')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/inquiry', 'Api\v2\Senna\Merchant\Cashier\Withdraw\WithdrawController@inquiry');
                Route::get('/admin-fee/{merchant_id}', 'Api\v2\Senna\Merchant\Cashier\Withdraw\WithdrawController@getAdminFee');
                Route::get('/transaction-check/{code}', 'Api\v2\Senna\Merchant\Cashier\Withdraw\WithdrawController@transactionCheck');
                Route::get('/withdraw-callback/{id}', 'Api\v2\Senna\Merchant\Cashier\Withdraw\WithdrawController@withdrawCallback');



            });
    }

    public  function getAdminFee(Request $request)
    {
        try{
            return $this->message::getJsonResponse(200,trans('message.201'),['admin_fee'=>config('qris_rule.administration_wd'),'min_wd'=>10000]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }
    public function inquiry(Request  $request)
    {
        try {
            DB::beginTransaction();
            $data=new QRISWithdrawActivity();
            $validator = Validator::make($request->all(), $data->ruleCreate);

            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            date_default_timezone_set($request->timezone);

            $activation=QRISActivation::where('md_merchant_id',$request->md_merchant_id)->first();
            if(is_null($activation))
            {
                return $this->message::getJsonResponse(404,'Data aktivasi metode pembayaran tidak ditemukan',[]);

            }
            if($request->amount<(config('qris_rule.min_wd')-config('qris_rule.administration_wd')))
            {
                return $this->message::getJsonResponse(404,'Mohon maaf saldomu belum mencapai batas minimal penarikan yang telah ditentukan',[]);

            }
            $checkSaldo=RajaBillerUtil::balanceCheck();
            if($checkSaldo==false)
            {
                return $this->message::getJsonResponse(404,'Terjadi kesalahan saat melakukan proses pengecekan saldo,cobalah beberapa saat lagi',[]);

            }
            $isFail=false;
            $data->unique_code=CodeGenerator::generalCode('WD',$request->md_merchant_id);
            $data->md_user_id=$request->md_user_id;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->qris_activation_id=$activation->id;
            $data->amount=$request->amount;
            $data->admin_fee=config('qris_rule.administration_wd');
            if($checkSaldo->SALDO<($request->amount+config('qris_rule.administration_wd')))
            {
                $data->transaction_status='PENDING';
                $data->save();

            }else{
                $inquiry=RajaBillerUtil::inquiryTransferDana($activation->bank_account_number,$request->amount,$activation->getBank->bank_code,$activation->phone_number);
                if($inquiry==false)
                {
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan saat melakukan proses inquiry saldo,cobalah beberapa saat lagi',[]);

                }
                $pay=RajaBillerUtil::pay($activation->bank_account_number,$request->amount,$activation->getBank->bank_code,$activation->phone_number,$inquiry->REF2);
                if($pay==false)
                {
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan saat melakukan proses penarikan dana,cobalah beberapa saat lagi',[]);

                }
                if($pay->STATUS=='00')
                {
                    if($pay->KET=='Success')
                    {
                        $data->response_status=json_encode($inquiry);
                        $data->transaction_status='SUCCESS';
                        $data->response_create=json_encode($pay);
                        $data->save();
                    }else{
                        $data->response_status=json_encode($inquiry);
                        $data->transaction_status='PENDING';
                        $data->response_create=json_encode($pay);
                        $data->save();
                    }

                }else{
                    $isFail=true;
                    $data->response_status=json_encode($inquiry);
                    $data->transaction_status='FAILED';
                    $data->response_create=json_encode($pay);
                    $data->save();
                }
            }
            DB::commit();
            if($isFail==true)
            {
                return $this->message::getJsonResponse(404,'Penarikan saldo gagal dilakukan,Mohon cobalah beberapa saat lagi',[]);
            }else{
                return $this->message::getJsonResponse(200,'Penarikan saldo berhasil,silahkan cek akunmu',$data);

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

    public function withdrawCallback($id)
    {
        try {
            DB::beginTransaction();
            $wd=QRISWithdrawActivity::find($id);
            if(is_null($wd))
            {
                return $this->message::getJsonResponse(404,'Data penarikan tidak ditemukan',[]);

            }
            if($this->insertJurnal($wd,$wd->getActivation)==false)
            {
                return $this->message::getJsonResponse(404,'Terjadi kesalahan saat pencatatan jurnal penarikan saldo',[]);

            }
            DB::commit();
            return $this->message::getJsonResponse(200,'Penarikan saldo berhasil,silahkan cek akun bankmu',$data);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }
    public function transactionCheck($code)
    {
        try{
            DB::beginTransaction();
            $wd=QRISWithdrawActivity::whereNotNull('response_create')
                ->where('unique_code',$code)
                ->first();
            if(!is_null($wd))
            {
                $trans=json_decode($wd->response_create);
                $check=RajaBillerUtil::transactionCheck($trans->REF2,$trans->KODE_PRODUK,$trans->IDPEL1,$trans->WAKTU);

                if($wd->transaction_status!='SUCCESS')
                {

                    if($check->KET=='Transaksi berhasil')
                    {
                        $wd->transaction_status='SUCCESS';
                        $wd->save();
                        if($this->insertJurnal($wd,$wd->getActivation)==false)
                        {
                            DB::rollBack();
                            return $this->message::getJsonResponse(404,'Terjadi kesalahan saat melakukan pengecekan transaksi',[]);

                        }
                    }

                    DB::commit();

                    return $this->message::getJsonResponse(200,'Penarikan saldo berhasil,silahkan cek akun bankmu',[
                        'wd'=>$wd,
                        'report'=>$check
                    ]);

                }else{
                    return $this->message::getJsonResponse(200,'Penarikan saldo berhasil,silahkan cek akun bankmu',[
                        'wd'=>$wd,
                        'report'=>$check
                    ]);
                }
            }else{
                return $this->message::getJsonResponse(404,'Data penarikan tidak ditemukan',[]);
            }
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

    private function insertJurnal($data,$activation)
    {
        try{
            $check=JournalEntity::where('ref_code',$data->unique_code)->first();
            if(is_null($check))
            {
                $jurnal=new JournalEntity();
                $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$activation->md_merchant_id);
                $jurnal->ref_code=$data->unique_code;
                $jurnal->trans_name='Penarikan Saldo '.$data->unique_code;
                $jurnal->trans_time=date('Y-m-d H:i:s');
                $jurnal->trans_note='penarikan';
                $jurnal->trans_purpose='Penarikan Saldo '.$data->unique_code;
                $jurnal->trans_amount=$data->amount+config('qris_rule.administration_wd');
                $jurnal->admin_fee=config('qris_rule.administration_wd');
                $jurnal->md_merchant_id=$activation->md_merchant_id;
                $jurnal->md_user_id_created=$activation->md_user_id;
                $jurnal->save();
                $adminFee=Merchant::find($activation->md_merchant_id);
                $fromCoa=$activation->qrisdetail_qris_id;
                $toCoa=$activation->qrisbank_qris_id;
                $insert=[
                    [
                        'acc_coa_detail_id'=>$fromCoa,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->amount+config('qris_rule.administration_wd'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->amount,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$adminFee->coa_administration_bank_id,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>config('qris_rule.administration_wd'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnal->id
                    ],

                ];

                JurnalDetail::insert($insert);
            }
            return true;
        }catch (\Exception $e)
        {
            return false;
        }
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Product;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Classes\Singleton\CodeGenerator;
use Ramsey\Uuid\Uuid;
use App\Models\SennaToko\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class SfaTransactionController extends Controller
{

    protected $message;
    protected $sale;
    protected $product;

    public function __construct()
    {
        $this->sale=SaleOrder::class;
        $this->product=Product::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/sfa-transaction')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\SfaTransactionController@create');
            });
    }

        //Order
        public function create(Request $request)
        {
            try {
    
                DB::beginTransaction();
                date_default_timezone_set($request->timezone);
                $timestamp=date('Y-m-d H:i:s');
                $purchase = new $this->sale;

                $purchase->step_type=2;
                $code = CodeGenerator::generateSaleOrder($request->merchant_id);
                $purchase->code = str_replace('SO-','S-',$code);
                $purchase->qr_code = '-';
                $purchase->is_debet = 0;
                $purchase->is_with_retur = 0;
                $purchase->change_money = 0;    
                $purchase->total = $request->total;
                $purchase->md_merchant_id = $request->merchant_id;
                $purchase->sc_customer_id = $request->customer_id;
                $purchase->paid_nominal = 0;
                $purchase->md_sc_transaction_status_id = ($request->is_debet==0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;

                $purchase->timezone = $request->timezone;
                $purchase->created_by = $request->user_id;
                $purchase->sync_id = $request->merchant_id . Uuid::uuid4()->toString();
                
                $promo_order=$request->discount_percentage/100*$request->total;
                $tax_order=$request->tax_percentage/100*$request->total;

                $purchase->time_to_order=$timestamp;
                $purchase->total_order=$request->total;
                $purchase->promo_order = $promo_order;
                $purchase->promo_percentage_order = $request->discount_percentage;
                $purchase->tax_order = $tax_order;
                $purchase->tax_percentage_order = $request->tax_percentage;
    
                $purchase->is_approved_shop=1;
                $purchase->created_at=$timestamp;
                $purchase->updated_at=$timestamp;
                $purchase->save();

                $details = json_decode($request->details) ;
                $results = [];
                
                if(is_null($details)){
                    return $this->message::getJsonResponse(404, 'Data item produk tidak boleh kosong', []);
                }
                $productId = [];
                $productIds = "";
                foreach ($details as $n => $i) {
                    $productId[] = $i->sc_product_id;
                    $productIds .= ($n == 0) ? $i->sc_product_id : "," . $i->sc_product_id;
                }
                $products = collect(DB::select("
                select
                  sp.*,
                  sp.inv_id as coa_inv_id,
                  sp.hpp_id as coa_hpp_id,
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          spmu.*,
                          mu.name
                        FROM
                          sc_product_multi_units spmu
                          join md_units mu on mu.id = spmu.md_unit_id
                        WHERE
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                      ) jd
                  ) AS multi_units
                from
                  sc_products sp
                where
                  sp.id in ($productIds)
                 "));
                foreach ($details as $key => $item) {
                    // dd($item);
                    $unit_name = "";
                    $mQuantity = $item->quantity;
                    $multi_quantity=0;
                    $product = $products->firstWhere('id', $item->sc_product_id);
                    if($item->is_multi_unit==1){
                      $multi_unit=collect(json_decode($product->multi_units))->firstWhere('id', $item->unit_id);
                      $unit_name = $multi_unit->name;
                      $multi_quantity = $item->quantity;
                      $mQuantity = $multi_unit->quantity * $item->quantity;
                      // dd($unit_name);
                    }
                    $purchaseFromInventory=$product->purchase_price*$item->quantity;
                    $results[]=[
                        'sc_product_id'=>$product->id,
                        'quantity'=>$mQuantity,
                        'multi_quantity'=>$multi_quantity,
                        'is_bonus'=>0,
                        'is_multi_unit' => $item->is_multi_unit,
                        'unit_name' => $unit_name,
                        'sub_total'=>$mQuantity*($item->price),
                        'sc_sale_order_id'=>$purchase->id,
                        'profit'=>($item->quantity*$item->price)-$purchaseFromInventory,
                        'price'=>$item->price,
                        'note_order'=>$item->note,
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'coa_inv_id'=>$product->coa_inv_id,
                        'coa_hpp_id'=>$product->coa_hpp_id,
                    ];
                }
    
                SaleOrderDetail::insert($results);
    
                DB::commit();
                
                return $this->message::getJsonResponse(200,trans('ordershop.so_create'),$purchase);

    
            } catch (\Exception $e) {
                DB::rollBack();
                log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan disistem,cobalah beberapa saat lagi', []);
            }
        }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\QRIS;

use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\QRIS\QRISActivation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class QRISActivationController extends Controller
{

    protected $message;
    protected $qris;

    public function __construct()
    {
        $this->qris=QRISActivation::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/cashier/qris/activation')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISActivationController@create');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISActivationController@update');
                Route::get('/one/{md_merchant_id}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISActivationController@getOne');
                Route::post('/getHistory/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISActivationController@getHistory');
                Route::get('/getBalance/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISActivationController@getBalance');
                Route::get('/getDetailHistory/{code}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISActivationController@getDetailHistory');
            });
    }

    public function getBalance($merchantId)
    {
        try{
            $wd=DB::select("select coalesce(sum(qwa.amount+qwa.admin_fee),0) as saldo from qris_withdraw_activities qwa where qwa.md_merchant_id=$merchantId and qwa.transaction_status='SUCCESS'
")[0]->saldo;
            $qris=DB::select("select coalesce(sum(so.total-so.admin_fee),0) as saldo  from sc_sale_orders so where  so.md_sc_transaction_status_id=2
and so.md_transaction_type_id=4 and so.qris_response notnull and so.qris_status notnull
and so.md_merchant_id=$merchantId")[0]->saldo;

            return $this->message::getJsonResponse(200,trans('message.201'),$qris-$wd);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }
    public function getOne($id)
    {
        try {
            $data=$this->qris::where('md_merchant_id',$id)
                ->with('getBank')
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getHistory($merchantId,Request $request){
        try{
            DB::statement("refresh materialized view mv_wd_table");
            $startDate=(is_null($request->start_date))?date('Y-m-d'):$request->start_date;
            $endDate=(is_null($request->end_date))?date('Y-m-d'):$request->end_date;

            $data =DB::table("mv_wd_table")
                ->where("md_merchant_id",$merchantId)
                ->whereRaw("tanggal::date between '$startDate' and '$endDate'")
                ->orderBy('tanggal','desc')
                ->get();
            ;

            if(count($data)<1){
                return $this->message::getJsonResponse(404,trans('message.404'),$data);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getDetailHistory($code){
        try{

            $data=collect(DB::select("SELECT
                             so.total,
                             'in' as trans_type,
                             so.code,
                             null as bank_name,
                             null as bank_account_number,
                             null as bank_user,
                             so.admin_fee,
                            (so.total-so.admin_fee) as jumlah_total,so.created_at,s.name as status

                             FROM sc_sale_orders so
                            join md_sc_transaction_status s
                            on so.md_sc_transaction_status_id=s.id
                             and so.md_transaction_type_id = 4
                             and so.code='$code'
                             and so.qris_response notnull
                                UNION
                             SELECT
                             amount,
                             'out' as trans_type,
                             unique_code,
                             r.name as bank_name,
                             bank_account_number,
                             bank_account_name,
                             admin_fee,
                            (amount+admin_fee) as jumlah_total,w.created_at,
                             transaction_status as status
                             FROM qris_withdraw_activities as w
                             JOIN qris_activations a ON a.id = w.qris_activation_id
                             JOIN md_rajabiller_banks r ON r.id = a.md_rajabiller_bank_id
                             where unique_code='$code'
            "))->first();

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function create(Request  $request,$merchantId)
    {
        try{
            $qris=QRISActivation::where('md_merchant_id',$merchantId)->first();
            if(!is_null($qris))
            {
                if($qris->is_agree==0){
                    return $this->message::getJsonResponse(404,'Kamu sudah melakukan pengajuan aktivasi QRIS,tunggu tim kami memverifikasi',[]);
                }else if($qris->is_agree==2){
                    $data = QRISActivation::find($qris->id);
                }else if($qris->is_agree==1){
                    return $this->message::getJsonResponse(404,'Kamu sudah memliki akun QRIS yang aktif',[]);
                }
            }else{
                $data = new $this->qris;
            }

            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $destinationPath = 'public/uploads/merchant/'.$merchantId.'/QRISActivation/';

            if($request->hasFile('identity_card'))
            {
                $file=$request->file('identity_card');
                $fileIdentity= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileIdentity=$destinationPath.$fileIdentity;
                Storage::disk('s3')->put($fileIdentity, file_get_contents($file));

            }else{
                $fileIdentity=null;
            }

            if($request->hasFile('account_book'))
            {
                $file=$request->file('account_book');
                $fileAccount= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileAccount=$destinationPath.$fileAccount;
                Storage::disk('s3')->put($fileAccount, file_get_contents($file));

            }else{
                $fileAccount=null;
            }

            $data->code=$merchantId . Uuid::uuid4()->toString();
            $data->md_merchant_id=$merchantId;
            $data->md_user_id=$request->md_user_id;
            $data->identity_card=$fileIdentity;
            $data->account_book=$fileAccount;
            $data->bank_account_name=$request->bank_account_name;
            $data->phone_number=$request->phone_number;
            $data->bank_account_number=$request->bank_account_number;
            $data->balance_amount=0;
            $data->is_active=0;
            $data->is_agree=0;
            $data->md_rajabiller_bank_id=$request->md_rajabiller_bank_id;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }


}

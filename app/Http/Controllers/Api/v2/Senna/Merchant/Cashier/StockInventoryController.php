<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\MultiUnit;
use App\Models\SennaToko\MultiVarian;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\StockInventory;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\Accounting\CoaProductUtilV2;
use App\Utils\BranchConfig\BranchConfigUtil;
use App\Utils\Inventory\InventoryUtilV2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class StockInventoryController extends Controller
{

    protected $product;
    protected $message;
    protected $inventory;

    public function __construct()
    {
        $this->product=Product::class;
        $this->message=Message::getInstance();
        $this->inventory=StockInventory::class;
    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/toko/stock-management')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/add-or-reduce/{productId}', 'Api\v2\Senna\Merchant\Cashier\StockInventoryController@addOrReduceStock');
                Route::post('/add-or-reduce-close-cashier', 'Api\v2\Senna\Merchant\Cashier\StockInventoryController@addOrReduceStockCloseCashier');

            });
    }


    public function addOrReduceStock($productId,Request $request)
    {
        try{
            DB::beginTransaction();
            $data=$this->product::find($productId);
            if($data->md_sc_type_product_id==2){
                return $this->message::getJsonResponse(404,'Stok barang produksi hanya dapat ditambahkan melalui menu produksi produk !',[]);
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }




            if(BranchConfigUtil::getAccessMobile('mutation',$data->md_user_id)==false)
            {
                return $this->message::getJsonResponse(404,'Kamu tidak diperkenankan untuk menambah data produk oleh pengaturan multi cabang',[]);

            }

            $validator = Validator::make($request->all(), $data->ruleStock);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            date_default_timezone_set($request->timezone);


            if(AccUtil::checkClosingJournal($data->getUser->getMerchant->id,date('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);

            }


            $warehouseId=$request->inv_warehouse_id;

            if(is_null($warehouseId))
            {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }

            $isMulti=isset($request->is_multi_unit)?$request->is_multi_unit:0;
            $MultiId=isset($request->multi_unit_id)?$request->multi_unit_id:null;
            // $isVarian=isset($request->is_multi_varian)?$request->is_multi_varian:0;
            // $VarianId=isset($request->multi_varian_id)?$request->multi_varian_id:null;

            $mQuantity=$request->amount;
            $purchase_price=$request->purchase_price;

            if($isMulti==1 && !is_null($MultiId)){
                $unit=MultiUnit::find($MultiId);
                $unit->price=$request->purchase_price;
                $mQuantity=$unit->quantity*$request->amount;
            }

            // if($isVarian==1 && !is_null($VarianId)){
            //     $varian=MultiVarian::find($VarianId);
            // }

            if($request->type!='in')
            {
                if($data->stock<$mQuantity)
                {
                    return $this->message::getJsonResponse(404,'Jumlah yang kamu masukkan lebih banyak daripada stok yang tersedia',[]);

                }
            }

            if($request->type=='in')
            {
                $data->stock+=$mQuantity;
                if($isMulti==1 && !is_null($MultiId)){
                    $priceAfterAdjust=$request->purchase_price*$request->amount;
                    $purchase_price=round($priceAfterAdjust/$mQuantity);
                    $data->purchase_price=$purchase_price;
                }else{
                    $data->purchase_price=$purchase_price;
                }
            }

            $data->save();
            if($request->type=='in'){

                $data->stock()->create([
                    'sync_id'=>$productId.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$data->id,
                    'total'=>$mQuantity,
                    'record_stock'=>$data->stock,
                    'created_by'=>$request->created_by,
                    'selling_price'=>$data->selling_price,
                    'residual_stock'=>$mQuantity,
                    'purchase_price'=>$request->purchase_price,
                    'timezone'=>$request->timezone,
                    'type'=>StockInventory::IN,
                    'inv_warehouse_id'=>$warehouseId,
                    'transaction_action'=>'Penambahan Stok '.$data->name. ' Dari Aplikasi Mobile',
                ]);
                if(CoaProductUtilV2::inventoryAdjustment($data->md_user_id,$data->getUser->getMerchant->id,$data,date('Y-m-d H:i:s'),'plus',$request->purchase_price*$mQuantity)==false)
                {
                    return $this->message::getJsonResponse(404,'Penyesuaian persediaan stok gagal dilakukan',[]);
                }
            }else{
                $updateStockInv=InventoryUtilV2::subtractionStock($data,$productId,$mQuantity,$warehouseId);
                if($updateStockInv==false)
                {
                    return $this->message::getJsonResponse(404,'Penyesuaian persediaan stok gagal dilakukan',[]);

                }
                if($updateStockInv!=""){
                    DB::statement("
                update sc_stock_inventories as t set
                        residual_stock = c.column_a
                    from (values
                        $updateStockInv
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                }
                $data->stock-=$mQuantity;
                $data->save();

                $data->stock()->create([
                    'sync_id'=>$productId.Uuid::uuid4()->toString(),
                    'sc_product_id'=>$data->id,
                    'total'=>$mQuantity,
                    'record_stock'=>$data->stock,
                    'created_by'=>$request->created_by,
                    'selling_price'=>$data->selling_price,
                    'residual_stock'=>$mQuantity,
                    'purchase_price'=>$request->purchase_price,
                    'type'=>StockInventory::OUT,
                    'timezone'=>$request->timezone,
                    'inv_warehouse_id'=>$warehouseId,
                    'transaction_action'=>'Pengurangan Stok '.$data->name. ' Dari Aplikasi Mobile'
                ]);
            }

            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function addOrReduceStockCloseCashier(Request $request)
    {
        try{
            DB::beginTransaction();
            $warehouseId=$request->inv_warehouse_id;

            foreach($request->products as $key => $p){
                $data=$this->product::find($p['sc_product_id']);
                if($data->md_sc_type_product_id==2){
                    return $this->message::getJsonResponse(404,'Stok barang produksi hanya dapat ditambahkan melalui menu produksi produk !',[]);
                }
                if(is_null($data))
                {
                    return $this->message::getJsonResponse(404,trans('message.404'),[]);
                }

                $validator = Validator::make($p, $data->ruleStock);
                if ($validator->fails()) {
                    $error = $validator->errors()->first();
                    return $this->message::getJsonResponse(404, $error, []);
                }
                date_default_timezone_set($p['timezone']);

                if(BranchConfigUtil::getAccessMobile('mutation',$data->md_user_id)==false)
                {
                    return $this->message::getJsonResponse(404,'Kamu tidak diperkenankan untuk menambah data produk oleh pengaturan multi cabang',[]);

                }

                if(is_null($warehouseId))
                {
                    return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);
                }

                $isMulti=isset($p['is_multi_unit'])?$p['is_multi_unit']:0;
                $MultiId=isset($p['multi_unit_id'])?$p['multi_unit_id']:null;

                $mQuantity=$p['amount'];
                $purchase_price=$p['purchase_price'];

                if($isMulti==1 && !is_null($MultiId)){
                    $unit=MultiUnit::find($MultiId);
                    $unit->price=$p['purchase_price'];
                    $mQuantity=$unit->quantity*$p['amount'];
                }

                if($p['type']!='in')
                {
                    if($data->stock<$mQuantity)
                    {
                        return $this->message::getJsonResponse(404,'Jumlah yang kamu masukkan lebih banyak daripada stok yang tersedia',[]);

                    }
                }

                if($p['type']=='in')
                {
                    $data->stock+=$mQuantity;
                    if($isMulti==1 && !is_null($MultiId)){
                        $priceAfterAdjust=$p['purchase_price']*$p['amount'];
                        $purchase_price=round($priceAfterAdjust/$mQuantity);
                        $data->purchase_price=$purchase_price;
                    }else{
                        $data->purchase_price=$purchase_price;
                    }
                }

                $data->save();
                if($p['type']=='in'){

                    $data->stock()->create([
                        'sync_id'=>$p['sc_product_id'].Uuid::uuid4()->toString(),
                        'sc_product_id'=>$data->id,
                        'total'=>$mQuantity,
                        'record_stock'=>$data->stock,
                        'created_by'=>$p['created_by'],
                        'selling_price'=>$data->selling_price,
                        'residual_stock'=>$mQuantity,
                        'purchase_price'=>$p['purchase_price'],
                        'timezone'=>$p['timezone'],
                        'type'=>StockInventory::IN,
                        'inv_warehouse_id'=>$warehouseId,
                        'transaction_action'=>'Penambahan Stok '.$data->name. ' Dari Aplikasi Mobile',
                    ]);
                    if(CoaProductUtilV2::inventoryAdjustment($data->md_user_id,$data->getUser->getMerchant->id,$data,date('Y-m-d H:i:s'),'plus',$p['purchase_price']*$mQuantity)==false)
                    {
                        return $this->message::getJsonResponse(404,'Penyesuaian persediaan stok gagal dilakukan',[]);
                    }
                }else{
                    $updateStockInv=InventoryUtilV2::subtractionStock($data,$p['sc_product_id'],$mQuantity,$warehouseId);
                    if($updateStockInv==false)
                    {
                        return $this->message::getJsonResponse(404,'Penyesuaian persediaan stok gagal dilakukan',[]);

                    }
                    if($updateStockInv!=""){
                        DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                    }
                    $data->stock-=$mQuantity;
                    $data->save();

                    $data->stock()->create([
                        'sync_id'=>$p['sc_product_id'].Uuid::uuid4()->toString(),
                        'sc_product_id'=>$data->id,
                        'total'=>$mQuantity,
                        'record_stock'=>$data->stock,
                        'created_by'=>$p['created_by'],
                        'selling_price'=>$data->selling_price,
                        'residual_stock'=>$mQuantity,
                        'purchase_price'=>$p['purchase_price'],
                        'type'=>StockInventory::OUT,
                        'timezone'=>$p['timezone'],
                        'inv_warehouse_id'=>$warehouseId,
                        'transaction_action'=>'Pengurangan Stok '.$data->name. ' Dari Aplikasi Mobile'
                    ]);
                }
            }


            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\ArrayOfAccounting;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\ReturSaleOrderDetail;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\TempSaleOrderDetail;
use App\Utils\Accounting\AccUtil;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Models\SennaToko\Material;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Ramsey\Uuid\Uuid;

class ReturSaleOrderController extends Controller
{
    protected $returSaleOrder;
    protected $returDetailSaleOrder;
    protected $message;
    protected $merchant;
    protected $saleOrder;
    protected $product;
    protected $saleOrderDetail;

    public function __construct()
    {
        $this->returSaleOrder=ReturSaleOrder::class;
        $this->message=Message::getInstance();
        $this->returDetailSaleOrder=ReturSaleOrderDetail::class;
        $this->merchant=Merchant::class;
        $this->saleOrder=SaleOrder::class;
        $this->product=Product::class;
        $this->saleOrderDetail=SaleOrderDetail::class;
    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/toko/retur-sale-order')
            ->middleware('etag')

            ->group(function()
            {
                Route::post('create/{saleOrderId}', 'Api\v2\Senna\Merchant\Cashier\ReturSaleOrderController@create')->middleware('api-verification');
                Route::post('list/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\ReturSaleOrderController@list')->middleware('api-verification');
                Route::get('one/{orderId}', 'Api\v2\Senna\Merchant\Cashier\ReturSaleOrderController@one')->middleware('api-verification');
                Route::get('/all', 'Api\v2\Senna\Merchant\Cashier\ReturSaleOrderController@getSaleByCustomer')->name('api.sale.all');
                Route::post('/detail', 'Api\v2\Senna\Merchant\Cashier\ReturSaleOrderController@getSaleDetail')->name('api.sale.detail');
                Route::get('/reason-id', 'Api\v2\Senna\Merchant\Cashier\ReturSaleOrderController@getReasonId')->name('api.reason.retur');

            });
    }

    public function getReasonId(Request $request){
        try{
            //dd($request->sc_supplier_id);

            $data=ArrayOfAccounting::reasonOfRetur($request->is_debet);

            $response =[];
            foreach($data as $key => $item){
                //dd($item->id);
                $response[] = [
                    "id"=>$data[0][1],
                    "text"=>$item[$key]
                ];
            }

            return response()->json($data);
        }catch (\Exception $e)
        {
            return response()->json($e);
        }

    }

    public function getSaleDetail(Request $request)
    {
        try{
            //dd($request->sc_supplier_id);
            $sumRetur=0;
            $data=$this->saleOrderDetail::select([
                'sc_sale_order_details.id as id_detail',
                'p.id as product_id',
                'p.name as product_name',
                'p.code as product_code',
                'quantity',
                's.is_debet',
                's.step_type',
                's.id as sale_id',
                'ar.is_paid_off',
                'm.name as unit_name',
                'sc_sale_order_details.unit_name as unit_name_2',
                'sc_sale_order_details.product_name as product_name_2',
                'p.md_sc_product_type_id',
                'sc_sale_order_details.is_multi_unit',
                'sc_sale_order_details.multi_unit_id',
                'sc_sale_order_details.multi_quantity',
                'sc_sale_order_details.multi_quantity as origin_multi_quantity',
                'sc_sale_order_details.value_conversation',
                DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id as original_id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as text,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_sale_order_details.sc_product_id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '-1'
                ) as json_multi_unit
                ")
            ])
                ->where('sc_sale_order_id',$request->sc_sale_order_id)
                ->join('sc_products as p','p.id','sc_sale_order_details.sc_product_id')
                ->leftJoin('md_units as m','m.id','p.md_unit_id')
                ->join('sc_sale_orders as s','s.id','sc_sale_order_details.sc_sale_order_id')
                ->leftjoin('acc_merchant_ar as ar','ar.arable_id','s.id')
                ->where('s.is_deleted',0)
                ->where('s.is_step_close',0)
                ->where('sc_sale_order_details.is_deleted',0)
                ->where('sc_sale_order_details.is_bonus',0)
                ->get();

            foreach($data as $key => $d){
                $sumSubRetur=0;
                $dataRetur= DB::table('sc_retur_sale_order_details')
                    ->select(['sc_retur_sale_order_details.*'])
                    ->join('sc_retur_sale_orders as c','c.id','sc_retur_sale_order_id')
                    ->where('sc_sale_order_detail_id',$d->id_detail)
                    ->where('c.step_type',$d->step_type)
                    ->where(function($query){
                        $query->where('c.reason_id',2)
                            ->orWhere('c.reason_id',3);
                    })
                    ->where('c.is_deleted',0)->get();
                foreach ($dataRetur as $dr){
                    if($dr->is_multi_unit==0){
                        $sumSubRetur+=$dr->quantity;
                    }else{
                        if($dr->value_conversation==0){
                            $sumSubRetur+=$dr->multi_quantity;
                        }else{
                            $sumSubRetur+=($dr->quantity*$dr->value_conversation);
                        }
                    }
                }
                if($d->is_multi_unit==0){
                    $d->quantity-=$sumSubRetur;
                }else{
                    $d->quantity-=$sumSubRetur/$d->value_conversation;
                    $d->multi_quantity-=$sumSubRetur;

                }
            }


            return response()->json($data);
        }catch (\Exception $e)
        {
            return response()->json($e);
        }

    }

    public function getSaleByCustomer(Request $request)
    {
        try{
            //dd($request->sc_supplier_id);
            if($request->sc_customer_id==-1){

                $data=$this->saleOrder::select([
                    'id',
                    'created_at',
                    'total',
                    'code',
                    'second_code',
                ])
                    ->whereNull('sc_customer_id')
                    ->where('is_deleted',0)
                    ->where('md_merchant_id',$request->merchant_id)
                    ->where('step_type',$request->step_type)
                    ->where('is_step_close',0)->get();

            }else{

                $data=$this->saleOrder::select([
                    'id',
                    'created_at',
                    'total',
                    'code',
                    'second_code'
                ])
                    ->where('sc_customer_id',$request->sc_customer_id)
                    ->where('is_deleted',0)
                    ->where('step_type',$request->step_type)
                    ->where('is_step_close',0)
                    ->get();
            }


            $response =[];
            if(count($data)<1){
                $response[] = [
                    "id"=>-1,
                    "text"=>"Tidak Ada Data Penjualan",
                    "defaultSelected"=>true,
                    "selected"=>true,
                    "disabled"=>true
                ];
                return response()->json($response);
            }



            foreach($data as $item){
                //dd($item->id);
                $code=is_null($item->second_code)?$item->code:$item->second_code;
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$code.' ('.Carbon::parse($item->created_at)->isoFormat('D MMMM Y, HH:mm').')'
                ];
            }

            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);
        }

    }

    public function create($saleOrderId,Request $request)
    {
        try{
            date_default_timezone_set($request->timezone);

            DB::beginTransaction();
            $data=SaleOrder::find($saleOrderId);
            $merchantId=$data->md_merchant_id;
            $merchant=$data->getMerchant;
            if($data->md_sc_transaction_status_id==3 || $data->md_sc_transaction_status_id==5){
                return $this->message::getJsonResponse(500, 'Transaksi penjualan tidak dapat diretur, karena transaksi telah dibatalkan/refund', []);
            }
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }


            $warehouseId=$request->inv_warehouse_id;
            if(is_null($warehouseId))
            {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }
            $invMethod=$request->md_inventory_method_id;

            if(is_null($invMethod))
            {
                return $this->message::getJsonResponse(404, 'Parameter metode persediaan belum terisi', []);

            }

            TempSaleOrderDetail::where('sc_sale_order_id',$data->id)
                ->update(['is_current'=>0]);

            if($data->is_editable==0){
                return $this->message::getJsonResponse(404, trans('Perhatian! Demi perhitungan akurat di akuntansi mulai dari tanggal 8 Maret 2021 Transaksi Penjualan Maupun Pembelian dan Piutang/Hutang tidak dapat diedit, karena ada penambahan modul akuntansi ,apabila kamu ingin mereset data silahkan lakukan di menu pengaturan dan menghapusnya.'), []);
            }
            $retur=new ReturSaleOrder();
            $retur->sc_sale_order_id=$saleOrderId;
            $retur->code=CodeGenerator::returSaleOrder($saleOrderId);
            $retur->md_sc_transaction_status_id=TransactionStatus::PAID;
            $retur->is_retur=1;
            $retur->note=$request->note;
            $retur->reason_id=$request->reason_id;
            $retur->save();
            $data->is_with_retur=1;
            $data->save();

            $timestamp = date('Y-m-d H:i:s');
            $arrayOfDetail=$request->details;
            $total=0;
            $amountOfHpp=[];
            $details=[];
            $temp=[];
            $stockInv=[];
            $productIds="";
            $arrayOfPurchaseDetail=[];
            foreach ($arrayOfDetail as $n => $i)
            {
                $arrayOfPurchaseDetail[]=$i['sc_sale_order_detail_id'];
                $productIds.=($n==0)?$i['sc_product_id']:",".$i["sc_product_id"];
            }

            $products=collect(DB::select("
                select
                  sp.*,
                  sp.inv_id as coa_inv_id,
                  sp.hpp_id as coa_hpp_id,
                  u.name as default_unit_name,
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          spmu.id,
                          spmu.sc_product_id,
                          spmu.md_unit_id,
                          spmu.quantity,
                          spmu.price,
                          mu.name
                        FROM
                          sc_product_multi_units spmu
                          join md_units mu on mu.id = spmu.md_unit_id
                        WHERE
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                      ) jd
                  ) AS multi_units
                from
                  sc_products sp
                left join md_units u on u.id = sp.md_unit_id
                where
                  sp.id in ($productIds)
            "));

            $purchaseDetail=SaleOrderDetail::whereIn('id',$arrayOfPurchaseDetail)
                ->where('is_bonus',0)
                ->get();
            $updateStockProduct="";
            $productCalculate=[];

            $j=0;

            foreach ($arrayOfDetail as $key =>$item)
            {

                $product=$products->firstWhere('id',$item['sc_product_id']);
                $saleOrderLine=$purchaseDetail->firstWhere('id',$item['sc_sale_order_detail_id']);
                if($saleOrderLine->is_bonus==0)
                {
                    $isMulti=isset($item['is_multi_unit'])?$item['is_multi_unit']:0;
                    $MultiId=isset($item['multi_unit_id'])?$item['multi_unit_id']:null;
                    $unit_name="";
                    $multi_quantity=$item['quantity'];
                    $mPrice=$item['price'];
                    $mQuantity=$item['quantity'];

                    if($isMulti==1 && !is_null($MultiId)){
                        $multi_unit=collect(json_decode($product->multi_units));
                        $unit=$multi_unit->firstWhere('id',$MultiId);
                        $unit_name=$unit->name;
                        $multi_quantity=$unit->quantity*$item['quantity'];
                        $convertionValue=$unit->quantity;
                        $jsonMulti=$multi_unit;
                    }else{
                        $unit_name=$product->default_unit_name;
                    }

                    if($product->md_sc_product_type_id==4){
                        if($retur->reason_id!=1)
                        {
                            $productChild= Material::where('parent_sc_product_id', '=', $product->id)
                            ->join('sc_products as p','p.id','child_sc_product_id')
                            ->where('sc_product_raw_materials.is_deleted',0)
                            ->get();
                            foreach ($productChild as $p => $c) {
                                array_push($productCalculate,[
                                    'id'=>$c->child_sc_product_id,
                                    'amount'=>$item['quantity']*$c->quantity,
                                ]);
                            }
                        }
                    }

                    if($product->is_with_stock==1)
                    {
                        if($retur->reason_id!=1)
                        {
                                array_push($productCalculate,[
                                    'id'=>$item['sc_product_id'],
                                    'amount'=>$multi_quantity,
                                ]);
                        }
                    }

                    $total+=$multi_quantity*$mPrice;

                    $temp[]=[
                        'sync_id'=>$item['sc_product_id'].Uuid::uuid4()->toString(),
                        'sc_product_id'=>$saleOrderLine->sc_product_id,
                        'quantity'=>$saleOrderLine->quantity,
                        'sub_total'=>$saleOrderLine->sub_total,
                        'price'=>$mPrice,
                        'sc_sale_order_id'=>$data->id,
                        'is_current'=>1,
                        'created_at'=>$saleOrderLine->created_at,
                        'updated_at'=>$saleOrderLine->updated_at
                    ];
                    if($multi_quantity!=0)
                    {
                        if($product->is_with_stock==1){
                                $stockP=$product->stock+$multi_quantity;
                                $updateStockProduct.=($j==0)?"(".$product->id.",".$stockP.")":",(".$product->id.",".$stockP.")";
                                $j++;
                        }

                        if($product->md_sc_product_type_id==4){
                            foreach ($productCalculate as $key => $c) {
                                $pChild=Product::find($c["id"]);
                                $stockP=$pChild->stock+$c["amount"];
                                $updateStockProduct.=($j==0)?"(".$pChild->id.",".$stockP.")":",(".$pChild->id.",".$stockP.")";
                                $j++;
                            }
                        }
                        $details[]=[
                            'sc_product_id'=>$item['sc_product_id'],
                            'quantity'=>$mQuantity,
                            'is_multi_unit'=>$isMulti,
                            'multi_unit_id'=>$MultiId,
                            'unit_name'=>$unit_name,
                            'multi_quantity'=>$multi_quantity,
                            'sub_total'=>$item['sub_total'],
                            'sc_sale_order_detail_id'=>$saleOrderLine->id,
                            'sc_retur_sale_order_id'=>$retur->id,
                            'type'=>'minus',
                            'created_at'=>$timestamp,
                            'updated_at'=>$timestamp,
                            'value_conversation' => ( $isMulti==1 ) ? $convertionValue : 1,
                            'json_multi_unit' => ( $isMulti==1 ) ? $jsonMulti : null,
                        ];
                    }
                    
                    $q=($isMulti==1 && !is_null($MultiId))?$multi_quantity:$mQuantity;
                    if($q>$saleOrderLine->quantity*$saleOrderLine->value_conversation)
                    {
                        return $this->message::getJsonResponse(404, 'Jumlah pengembalian produk melebihi maksimal produk yang dapat dijual', []);
                    }
                    if($retur->reason_id!=1)
                    {
                        if($product->is_with_stock==1)
                        {
                            if($mQuantity!=0)
                            {
                                $stockP2=$product->stock+$multi_quantity;
                                $stockInv[]=[
                                    'sync_id'=>$item['sc_product_id'].Uuid::uuid4()->toString(),
                                    'sc_product_id' =>$item['sc_product_id'],
                                    'total' => $multi_quantity,
                                    'record_stock' => $stockP2,
                                    'created_by' => $request->created_by,
                                    'selling_price' => $product->selling_price,
                                    'purchase_price' => $product->purchase_price,
                                    'type' => StockInventory::IN,
                                    'inv_warehouse_id'=>$warehouseId,
                                    'transaction_action' => 'Penambahan Stok ' . $product->name . ' Dari Retur Transaksi ' . $retur->code,
                                    'stockable_type'=>'App\Models\SennaToko\ReturSaleOrder',
                                    'stockable_id'=>$retur->id,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s'),
                                ];
                            }
                        }

                        if($product->md_sc_product_type_id==4){
                            if($mQuantity!=0)
                            {
                                foreach ($productCalculate as $p => $c) {
                                    $pChild=Product::find($c["id"]);
                                    $stockP2=$pChild->stock+$c["amount"];
                                    $stockInv[]=[
                                        'sync_id'=>$pChild->id.Uuid::uuid4()->toString(),
                                        'sc_product_id' =>$pChild->id,
                                        'total' => $c["amount"],
                                        'record_stock' => $stockP2,
                                        'created_by' => $request->created_by,
                                        'selling_price' => $pChild->selling_price,
                                        'purchase_price' => $pChild->purchase_price,
                                        'type' => StockInventory::IN,
                                        'inv_warehouse_id'=>$warehouseId,
                                        'transaction_action' => 'Penambahan Stok ' . $pChild->name . ' Dari Retur Transaksi ' . $retur->code,
                                        'stockable_type'=>'App\Models\SennaToko\ReturSaleOrder',
                                        'stockable_id'=>$retur->id,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'),
                                    ];
                                }

                            }
                        }
                    }
                }

            }

            $checkFromDelivery=SaleOrder::where('code',$data->code)->where('step_type',3)->first();
            if(!is_null($checkFromDelivery)){
                $idSale=$checkFromDelivery->id;
            }else{
                $idSale=$data->id;
            }

            if(!empty($productCalculate))
            {
                $calculateInventory=InventoryUtilV2::inventoryCodev2($invMethod,$warehouseId,$productCalculate,'plus',$idSale);
                if($calculateInventory==false)
                {
                    return $this->message::getJsonResponse(404, 'Terjadi kesalahan saat penambahan stok dari retur penjualan dipersediaan', []);
                }
                $updateStockInv="";
                foreach ($calculateInventory as $key => $n)
                {
                    $amountOfHpp[]=[
                        'id'=>$n['id'],
                        'total'=>$n['total']
                    ];

                    $updateStockInv.=($key==0)?"(".$n['sc_stock_inventory_id'].",".$n['total_increment'].")":",(".$n['sc_stock_inventory_id'].",".$n['total_increment'].")";
                }

                if($updateStockInv!=""){
                    DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                }

            }
            if($updateStockProduct!="") {
                DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            $updateStockProduct
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
            }
            $retur->total=$total;
            $retur->created_by=$request->created_by;
            $retur->timezone=$request->timezone;
            $retur->sync_id=$saleOrderId.Uuid::uuid4()->toString();
            $retur->save();

            if($retur->reason_id==3)
            {
                $data->ar->residual_amount-=$total;
                $data->ar->save();
            }
            DB::table('sc_stock_inventories')->insert($stockInv);

            TempSaleOrderDetail::insert($temp);
            ReturSaleOrderDetail::insert($details);

            if($retur->reason_id!=1)
            {
                if(CoaSaleUtil::coaReturSaleOrderv2Mobile($merchant->md_user_id,$merchantId,[],$retur,$timestamp,$amountOfHpp)==false)
                {
                    return $this->message::getJsonResponse(404, trans('Pencatatan jurnal untuk retur penjualan terjadi kesalahan'), []);

                }
            }
            DB::commit();

            return $this->message::getJsonResponse(200,trans('ordershop.so_retur'),$retur);

        }catch (\Exception $e)
        {

            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }


    }


    public function list(Request $request,$merchantId)
    {
        try{
            $data=$this->saleOrder::select([
                'sc_sale_orders.*',
                'm.name as merchant_name',
                'm.id as as merchant_id'
            ])->with('getCustomer.getUserCustomer.getUser')
                ->with('getShippingCategory.getParent')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getOrderStatus')
                ->with(['getOldStruck'=>function($query){
                    $query->select([
                        'sc_temp_sale_order_details.*',
                        'p.name as product_name',
                        'p.foto',
                        'p.id as product_id',
                        'p.description',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'u.name as unit_name'
                    ])->join('sc_products as p','p.id','sc_temp_sale_order_details.sc_product_id')
                        ->join('md_units as u','u.id','p.md_unit_id')
                        ->where('sc_temp_sale_order_details.is_current',1)
                    ;

                }])
                ->join('md_merchants as m','m.id','sc_sale_orders.md_merchant_id')
                ->with(['getDetail'=>function($query){
                    $query->select([
                        'sc_sale_order_details.*',
                        'p.name as product_name',
                        'p.foto',
                        'p.id as product_id',
                        'p.description',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'u.name as unit_name'
                    ])->join('sc_products as p','p.id','sc_sale_order_details.sc_product_id')
                        ->join('md_units as u','u.id','p.md_unit_id');

                }])
                ->with('ar')
                ->where('sc_sale_orders.is_from_senna_app',0)
                ->where('sc_sale_orders.md_merchant_id',$merchantId)
                ->where('sc_sale_orders.is_with_retur',1)
                ->orderBy('sc_sale_orders.id','ASC')
                ->get()
            ;
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }

    }

    public function one($orderId)
    {
        try{
            $data=$this->saleOrder::with('getDetail.getProduct.getUnit')
                ->with(['getDetail'=>function($query){
                $query->with('getProduct.getUnit')
                    ->with(['getProduct.getMultiUnit'=>function($q){
                         $q->select(['sc_product_multi_units.*','m.name as unit_name'])
                            ->where('is_deleted','=',0)
                            ->join('md_units as m','m.id','sc_product_multi_units.md_unit_id');
                    }]);
                }])
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getCustomer')
                ->with(['getOldStruck'=>function($query){
                    $query->select([
                        'sc_temp_sale_order_details.*',
                        'p.name as product_name',
                        'p.foto',
                        'p.id as product_id',
                        'p.description',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'u.name as unit_name'
                    ])->join('sc_products as p','p.id','sc_temp_sale_order_details.sc_product_id')
                        ->join('md_units as u','u.id','p.md_unit_id')
                        ->where('sc_temp_sale_order_details.is_current',1)
                    ;

                }])
                ->with('ar')
                ->where('id',$orderId)->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404, trans('message.404'), []);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }

    }


}

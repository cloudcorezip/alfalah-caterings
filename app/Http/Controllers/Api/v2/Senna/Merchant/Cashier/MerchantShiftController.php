<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\Jurnal;
use App\Models\MasterData\MerchantShift;
use App\Models\SennaToko\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Utils\Merchant\ShiftUtil;
use App\Models\MasterData\MerchantShiftCashflow;
use Illuminate\Support\Facades\DB;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaMapping;
use Modules\Merchant\Entities\Acc\JournalEntity;
use App\Models\Accounting\JurnalDetail;
use App\Classes\Singleton\CodeGenerator;
use App\Utils\Accounting\CoaUtil;

class MerchantShiftController extends Controller
{

    protected $message;
    protected $shift;

    public function __construct()
    {
        $this->shift=MerchantShift::class;
        $this->cash=MerchantShiftCashflow::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/shift')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@create');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@update');
                Route::post('/end-shift/{id}', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@endShift');
                Route::get('/active/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@listActive');
                Route::post('/history/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@getHistory');
                Route::get('/report/{id}', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@getReport');
                Route::post('/detail/{id}', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@detail');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@delete');
                Route::get('/getHistory/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\MerchantShiftController@getHistory');
            });
    }



    public function listActive($merchantId,Request  $request)
    {
        try {
            $data=$this->shift::select([
                'md_merchant_shifts.*',
                'u.fullname',
                'm.name as merchant_name',
                'c.name as cashdrawer_name'
            ])->join('md_merchants as m','m.id','md_merchant_shifts.md_merchant_id')
                ->join('md_users as u','u.id','md_merchant_shifts.md_user_id')
                ->leftJoin('md_merchant_cashdrawers as c','c.id','md_merchant_shifts.md_merchant_cashdrawer_id')
                ->where('md_merchant_shifts.is_active',1)
                ->where('md_merchant_shifts.is_deleted',0)
                ->where('md_merchant_shifts.md_merchant_id',$merchantId);

            if($request->segment(7)=='active'){
                $result=$data->first();
                if(is_null($result))
                {
                    return $this->message::getJsonResponse(404,trans('message.404'),[]);

                }
            }else{
                $result=$data
                    ->orderBy('md_merchant_shifts.created_at','DESC')
                    ->get();
                if(is_null($result))
                {
                    return $this->message::getJsonResponse(404,trans('message.404'),[]);

                }
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$result);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function create(Request  $request)
    {
        try{
            DB::beginTransaction();
            $data = new $this->shift;
            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $data->md_merchant_id=$request->md_merchant_id;
            $data->md_user_id=$request->md_user_id;
            $data->role_name=$request->role_name;
            if(!is_null($request->md_merchant_cashdrawer_id) || $request->md_merchant_cashdrawer_id!='')
            {
                $data->md_merchant_cashdrawer_id=$request->md_merchant_cashdrawer_id;
            }
            $data->start_shift=$request->start_shift;
            $data->end_shift=$request->end_shift;
            $data->starting_cash=$request->starting_cash;
            $data->timezone=$request->timezone;
            $data->is_active=1;
            $data->is_deleted=0;
            $data->save();

            $jurnal=new Jurnal();
            $jurnal->trans_name="Saldo Awal Shift ";
            $jurnal->trans_time=date('Y-m-d H:i:s');
            $jurnal->trans_purpose="Mulai Shift";
            $jurnal->trans_type=4;
            $jurnal->trans_amount=$request->starting_cash;
            $jurnal->md_merchant_id=$request->md_merchant_id;
            $jurnal->md_user_id_created=$request->md_user_id;
            $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$request->md_merchant_id);
            $jurnal->ref_code='-';
            $jurnal->ref_id=$data->id;
            $jurnal->save();

                $insert=[
                    [
                        'acc_coa_detail_id'=>2,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'ref_id'=>$data->id,
                        'amount'=>$request->starting_cash,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>65,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'ref_id'=>$data->id,
                        'amount'=>$request->starting_cash,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                ];

            JurnalDetail::insert($insert);
            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $data = $this->shift::find($id);
            if (is_null($data)) {
                return $this->message::getJsonResponse(404, trans('message.404'), []);

            }
            $data->is_deleted = 1;
            $data->save();

            Jurnal::where('ref_id',$id)->update([
                'is_deleted'=>1
            ]);
            DB::commit();
            return $this->message::getJsonResponse(200, trans('message.202'), $data);

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }
    public function endShift(Request  $request,$id)
    {
        try{
            $data = $this->shift::find($id);

            $validator = Validator::make($request->all(), $data->end);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $data->end_cash=$request->end_cash;
            $data->end_shift=$request->end_shift;
            $data->note=$request->note;
            $data->is_active=0;
            $data->is_deleted=0;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }


    public function getReport($id){
        try{
            $data=collect(DB::select("SELECT s.*,u.fullname,
                                coalesce(
                                    (select
                                        sum(amount)
                                    from
                                        md_merchant_shift_cashflows
                                    where
                                        md_sc_cash_type_id = 1 and
                                        md_merchant_shift_id = $id
                                        and is_deleted=0
                                    ),
                                    0
                                ) as penjualan_tunai,
                                coalesce(
                                    (
                                    select
                                        sum(amount)
                                    from
                                        md_merchant_shift_cashflows
                                    where
                                        md_sc_cash_type_id = 2 and
                                        md_merchant_shift_id = $id
                                        and is_deleted=0
                                    ),
                                    0
                                ) as pengeluaran,
                                coalesce(
                                    (
                                    select
                                        sum(amount)
                                    from
                                        md_merchant_shift_cashflows
                                    where
                                        md_sc_cash_type_id = 3 and
                                        md_merchant_shift_id = $id
                                        and is_deleted=0
                                    ),
                                    0
                                ) as pemasukan_lain,
                                coalesce(
                                    (
                                    select
                                        sum(amount)
                                    from
                                        md_merchant_shift_cashflows
                                    where
                                        md_sc_cash_type_id = 4 and
                                        md_merchant_shift_id = $id
                                        and is_deleted=0
                                    ),
                                    0
                                ) as pengeluaran_lain
                                FROM md_merchant_shifts as s
                                JOIN md_users as u on u.id = s.md_user_id
                                WHERE s.id = $id

            "))->first();
            $data->kas_awal=$data->starting_cash;
            $data->penerimaan_sistem=($data->penjualan_tunai+$data->pemasukan_lain+$data->starting_cash)-($data->pengeluaran+$data->pengeluaran_lain);
            $data->penerimaan_aktual=$data->end_cash;
            $data->selisih=$data->end_cash-$data->penerimaan_sistem;
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getHistory($merchantId,Request $request){
        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;

            $data=$this->shift::select([
                'md_merchant_shifts.*',
                'u.fullname'
            ])->leftJoin('md_users as u','u.id','md_merchant_shifts.md_user_id')
                ->where('md_merchant_shifts.is_active',0)
                ->where('md_merchant_shifts.md_merchant_id',$merchantId)
                ->skip($offset)
                ->take($limit)
                ->orderBy('md_merchant_shifts.id','asc')
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function detail($id,Request  $request)
    {

        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $data=$this->cash::select([
                'md_merchant_shift_cashflows.*',
                'c.name as cash_name',
                DB::raw("case when md_merchant_shift_cashflows.type=1 then 'sale' when md_merchant_shift_cashflows.type=2 then 'purchase'  else 'other' end as transaction_type")
            ])->join('md_merchant_shifts as s','s.id','md_merchant_shift_cashflows.md_merchant_shift_id')
                ->join('md_sc_cash_types as c','c.id','md_merchant_shift_cashflows.md_sc_cash_type_id')
                ->where('md_merchant_shift_cashflows.is_deleted',0)
                ->where('s.id',$id)
                ->skip($offset)
                ->take($limit)
                ->orderBy('md_merchant_shift_cashflows.id','asc')
                ->get();


            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {

            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

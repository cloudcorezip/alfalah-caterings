<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\MerchantCommission;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\DiscountProduct;
use App\Models\Accounting\CoaDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\StockSaleMapping;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Utils\Winpay\WinpayUtil;
use App\Utils\CRM\CommissionUtil;
use App\Utils\Accounting\AccUtil;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\DiscountCoupon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Accounting\MerchantShift;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Utils\Merchant\ProductUtil;
use Illuminate\Support\Facades\Storage;
use QrCode;
use Ramsey\Uuid\Uuid;

class SaleOrderController extends Controller
{
    protected $order;
    protected $orderDetail;
    protected $message;
    protected $customer;
    protected $product;
    protected $ar;
    protected $shift;
    protected $coupon;
    protected $discount;
    protected $commission;


    public function __construct()
    {
        $this->order=SaleOrder::class;
        $this->orderDetail=SaleOrderDetail::class;
        $this->message=Message::getInstance();
        $this->customer=Customer::class;
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->shift=MerchantShift::class;
        $this->coupon=DiscountCoupon::class;
        $this->discount=DiscountProduct::class;
        $this->commission=MerchantCommission::class;



    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/toko/sale-order')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::get('test-map', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@testMap');
                Route::post('list/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@list');
                Route::post('create/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@create');
                Route::get('one/{orderId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@one');
                Route::delete('void/{orderId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@voidTransaction');

                Route::get('after-trans/{orderId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@getStruckAfterTransaction');
                Route::post('start-cash/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@startCash');
                Route::get('get-cash/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@getCash');
                Route::get('get-total-keep/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@getTotalTransactionKeep');
                Route::delete('refund/{orderId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@refundTransaction');
                Route::delete('cancel/{orderId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@cancelTransaction');

                //keep transaksi
                Route::post('edit-keep-trans/{orderId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@editKeepTransaction');
                Route::post('pay-keep-trans/{orderId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@payKeepTransaction');

                //QRIS
                Route::post('inquiry-qris/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@inquiryQRIS');
                Route::post('pay-qris-expired/{orderId}', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@payQRISExpired');


                //Store
                Route::post('inquiry-store', 'Api\v2\Senna\Merchant\Cashier\SaleOrderController@inquiryQRISFromStore');


            });
    }


    public function testMap()
    {
        $array=[
            [
                'id'=>4,
                'quantity'=>3,
                'stock'=>10
            ],
            [
                'id'=>1,
                'quantity'=>3,
                'stock'=>10
            ],
            [
                'id'=>1,
                'quantity'=>2,
                'stock'=>10
            ],
            [
                'id'=>2,
                'quantity'=>3,
                'stock'=>10
            ]
        ];
        $d=[];
        foreach (collect($array)->groupBy('id') as $i)
        {
            $quantity=collect($i)->reduce(function($i2, $k) {
                return $i2 + $k['quantity'];
            }, 0);

            $d[]=$quantity;
        }

        dd($d);

    }

    public function getTotalTransactionKeep($merchantId){
        try{
            DB::beginTransaction();
            $data=$this->order::where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->where('is_keep_transaction',1)
                ->whereNotIn('md_sc_transaction_status_id',[3,4,5])
                ->count();

            //$count=count($data);

            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }

    }

    public function getCash($merchantId){
        try{
            DB::beginTransaction();
            $data=$this->shift::select([
                'id',
                'amount',
                'md_merchant_id',
                'to_coa_detail_id',
                'created_at'
            ])
                ->whereDate('created_at', \Carbon\Carbon::today())
                ->where('md_merchant_id',$merchantId)
                ->where('is_deleted',0)
                ->first();
            if(is_null($data)){
                return $this->message::getJsonResponse(404, 'Belum mengatur saldo awal', []);
            }
            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }

    }

    public function startCash(Request $request,$merchantId)
    {
        try {
            $check=$this->shift::where('md_merchant_id',$merchantId)
                ->whereDate('created_at',date('Y-m-d'))
                ->where('is_first_shift',1)
                ->first();

            if($check)
            {
                $isFirst=0;
            }else{
                $isFirst=1;
            }
            $data=new $this->shift;
            $trans = explode('_', $request->md_transaction_type_id);
            if(!is_array($trans))
            {
                return $this->message::getJsonResponse(404,trans('Set saldo awal mengalami kesalahan'),[]);

            }
            $data->is_first_shift=$isFirst;
            $data->amount=$request->amount;
            $data->md_merchant_id=$merchantId;
            $modal=Merchant::find($merchantId);

            $payment=$trans[1];

            $data->trans_coa_detail_id=$modal->coa_capital_id;
            $data->to_coa_detail_id=$payment;
            $data->save();

            if($request->amount>0)
            {
                DB::beginTransaction();
                $code=CodeGenerator::generateJurnalCode(0,'JRN',$merchantId);
                $jurnal=new Jurnal();
                $jurnal->trans_amount=$request->amount;
                $jurnal->trans_time=date('Y-m-d H:i:s');
                $jurnal->md_user_id_created=$request->user_id;
                $jurnal->md_merchant_id=$merchantId;
                $jurnal->trans_name='Saldo Awal -'.Carbon::parse(date('Y-m-d H:i:s'))->isoFormat('D MMMM Y HH:MM:SS');
                $jurnal->trans_purpose='Saldo Awal -'.Carbon::parse(date('Y-m-d H:i:s'))->isoFormat('D MMMM Y HH:MM:SS');
                $jurnal->trans_code=$code;
                $jurnal->ref_code=$code;

                $jurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$payment;
                $detjurnal->coa_type="Debit";
                $detjurnal->amount=$request->amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=date('Y-m-d H:i:s');
                $detjurnal->updated_at=date('Y-m-d H:i:s');
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();

                $detjurnal=new JurnalDetail();
                $detjurnal->acc_coa_detail_id=$modal->coa_capital_id;
                $detjurnal->coa_type="Kredit";
                $detjurnal->amount=$request->amount;
                $detjurnal->acc_jurnal_id=$jurnal->id;
                $detjurnal->created_at=date('Y-m-d H:i:s');
                $detjurnal->updated_at=date('Y-m-d H:i:s');
                $detjurnal->md_sc_currency_id=1;
                $detjurnal->save();
                DB::commit();

            }
            return $this->message::getJsonResponse(200,trans('Saldo berhasil ditambahkan!'),$data);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function list(Request $request,$merchantId)
    {
        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $sort=($request->sorting=='asc')?'asc':'desc';
            $data=$this->order::select([
                'sc_sale_orders.*',
                'm.name as merchant_name',
                'm.id as as merchant_id'
            ])->with('getCustomer.getUserCustomer.getUser')
                ->with('getShippingCategory.getParent')
                ->with('getTransactionStatus')
                ->with('getTransactionType')
                ->with('getOrderStatus')
                ->with(['getOldStruck'=>function($query){
                    $query->select([
                        'sc_temp_sale_order_details.*',
                        'p.name as product_name',
                        'p.foto',
                        'p.id as product_id',
                        'p.description',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'u.name as unit_name'
                    ])->join('sc_products as p','p.id','sc_temp_sale_order_details.sc_product_id')
                        ->join('md_units as u','u.id','p.md_unit_id')
                        ->where('sc_temp_sale_order_details.quantity','!=',0)
                        ->where('sc_temp_sale_order_details.is_current',1);
                }])
                ->with('ar')
                ->with('getStaff')
                ->join('md_merchants as m','m.id','sc_sale_orders.md_merchant_id')
                ->with(['getDetail'=>function($query){
                    $query->select([
                        'sc_sale_order_details.*',
                        'p.name as product_name',
                        'p.foto',
                        'p.id as product_id',
                        'p.description',
                        'p.stock',
                        'p.selling_price',
                        'p.purchase_price',
                        'u.name as unit_name'
                    ])->join('sc_products as p','p.id','sc_sale_order_details.sc_product_id')
                        ->join('md_units as u','u.id','p.md_unit_id')
                        ->where('sc_sale_order_details.quantity','!=',0)
                        ->where('sc_sale_order_details.is_deleted',0);
                }])
                ->where('sc_sale_orders.is_from_senna_app',0)
                ->where('sc_sale_orders.md_merchant_id',$merchantId)
                ->skip($offset)
                ->take($limit)
                ->orderBy('sc_sale_orders.id',$sort)
                ->get()
            ;
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }

    }

    public function one($orderId)
    {
        try {
            $data=$this->order::with(['getTransactionType','getTransactionStatus','getCustomer'])
                ->with('ar')
                ->with('getStaff')
                ->with('getAllRetur')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit')
                        ->with(['getProduct.getMultiUnit'=>function($q){
                            $q->select(['sc_product_multi_units.*','m.name as unit_name'])
                                ->where('is_deleted','=',0)
                                ->join('md_units as m','m.id','sc_product_multi_units.md_unit_id');
                        }])
                        ->with('getDetailPromo')
                        // ->with('getOneMultiUnit')
                        ->where('quantity','!=',0);
                }])
                ->where('id',$orderId)
                ->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function getStruckAfterTransaction($orderId)
    {
        try {
            $data=$this->order::with(['getTransactionStatus','getCustomer'])
                ->with('ar')
                ->with('getStaff')
                ->with(['getDetail'=>function($query){
                    $query->with('getProduct.getUnit')
                        ->where('quantity','!=',0);
                }])
                ->where('id',$orderId)
                ->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function create(Request $request,$merchantId)
    {
        try {
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            $checkDoubleAktiva=DB::select("
                        select count(*) as total from acc_coa_categories
                        where md_merchant_id=$merchantId and code='1.1.07'
                        ");

            if(!empty($checkDoubleAktiva))
            {
                DB::statement("delete from acc_coa_categories where md_merchant_id=$merchantId and code='1.1.07'");
            }

            $isKeep = (is_null($request->is_keep_transaction)) ? 0 : $request->is_keep_transaction;
            if ($request->is_debet == 0) {
                if ($isKeep == 0) {
                    if (is_null($request->md_transaction_type_id)) {
                        return $this->message::getJsonResponse(404, 'Metode Pembayaran belum dipilih', []);
                    }
                }
            }
            if (is_null($request->md_sc_shipping_category_id)) {
                return $this->message::getJsonResponse(404, 'Metode Pengiriman belum dipilih', []);
            }
            if (is_null($request->is_with_load_shipping)) {
                return $this->message::getJsonResponse(404, 'Centang beban pengiriman belum dipilih', []);

            }

            $warehouseId = $request->inv_warehouse_id;
            if (is_null($warehouseId)) {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }
            $invMethod = $request->md_inventory_method_id;

            if (is_null($invMethod)) {
                return $this->message::getJsonResponse(404, 'Parameter metode persediaan belum terisi', []);

            }

            $purchase = new $this->order;
            $validator = Validator::make($request->all(), $purchase->ruleOfflineOrder);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, trans('error'), [$error]);
            }
            if (!is_null($request->sc_customer_id)) {
                $purchase->sc_customer_id = $request->sc_customer_id;
            }
            if ($request->is_debet == 0) {
                if ($isKeep == 0) {
                    $trans = explode('_', $request->md_transaction_type_id);
                }

            } elseif ($request->is_debet == 1 && $request->paid_nominal > 0) {

                $trans = explode('_', $request->md_transaction_type_id);

            } else {

                $trans = null;
            }
            if (is_null($request->code)) {
                $code = CodeGenerator::generateSaleOrder($merchantId);
                $purchase->code = $code;
            } else {
                $code = $request->code;
                $purchase->code = $request->code;
            }

            $purchase->queue_code = $request->queue_code;
            $purchase->note_order = $request->note_order;
            $purchase->courier_name = $request->courier_name;
            $purchase->service_courier_name = $request->service_courier_name;
            $purchase->destination = $request->destination;
            $purchase->distance = $request->distance;
            $purchase->weight = $request->weight;
            $purchase->qr_code = '-';
            $purchase->is_debet = $request->is_debet;
            $purchase->is_with_retur = 0;
            $purchase->change_money = $request->change_money;
            $purchase->sc_customer_id = $request->sc_customer_id;
            if ($request->md_sc_shipping_category_id == 1) {
                $purchase->is_with_load_shipping = 1;
            } else {
                $purchase->is_with_load_shipping = 0;
            }

            $purchase->total = $request->total;
            $purchase->md_merchant_id = $merchantId;
            $purchase->paid_nominal = $request->paid_nominal;
            if ($isKeep == 0) {
                $purchase->md_sc_transaction_status_id = ($request->is_debet == 0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;

            } else {
                $purchase->md_sc_transaction_status_id = TransactionStatus::UNPAID;

            }
            $purchase->md_sc_shipping_category_id = $request->md_sc_shipping_category_id;
            $purchase->tax = $request->tax;
            
            $isReferral=0;
            if ($isKeep == 0 && !is_null($request->ref_coupon_id)) {
                $isReferral=$request->is_referral;
                $flex=($isReferral==1)?'referral':'kupon';

                $coupon=$this->discount::find($request->ref_coupon_id);
                if($coupon->is_limited==1){
                    if($coupon->max_coupon_use<=0){
                        return $this->message::getJsonResponse(404, 'Kuota '.$flex.' telah habis, tidak dapat digunakan!', []);
                    }else if($coupon->max_coupon_use>0){
                        $coupon->max_coupon_use-=1;
                        $coupon->save();
                    }
                }

                if($request->sc_customer_id){
                    $getCouponUsed=$this->order::where('sc_customer_id',$request->sc_customer_id)
                    ->where("ref_coupon_id", $request->ref_coupon_id)
                    ->where("is_keep_transaction", 0)
                    ->where("is_deleted", 0)
                    ->where("step_type", 0)
                    ->count();

                    if($coupon->max_coupon_use_per_customer>0 && $coupon->max_coupon_use_per_customer<$getCouponUsed){
                        return $this->message::getJsonResponse(404, 'Kuota '.$flex.' untuk pelanggan ini telah habis, tidak dapat digunakan!', []);
                    }
                }

                $purchase->ref_coupon_id = $request->ref_coupon_id;
                $purchase->ref_coupon_code = $request->ref_coupon_code;
                $purchase->is_referral = $request->is_referral;
                $purchase->coupon_value = $coupon;
            }

            if ($isKeep == 0 && !is_null($request->ref_discount_id)) {
                $disc_id=[];
                $disc_code=[];
                $discount=$this->discount::find($request->ref_discount_id);

                $disc_id[]=[
                    'discount_id'=>$request->ref_discount_id
                ];
                $disc_code[]=[
                    'discount_code'=>$request->ref_discount_code
                ];

                $purchase->ref_discount_id = json_encode($disc_id);
                $purchase->ref_discount_code = json_encode($disc_code);
                $purchase->discount_value = $discount;
            }
            $purchase->json_amount_promo = ($request->json_amount_promo) ? json_encode($request->json_amount_promo) : null;

            $purchase->discount_primary_id = $request->discount_primary_id;
            $purchase->promo = $request->promo;
            $purchase->promo_percentage = $request->promo_percentage;
            $purchase->tax_percentage = $request->tax_percentage;
            $purchase->shipping_cost = $request->shipping_cost;
            $purchase->timezone = $request->timezone;
            $purchase->created_by = $request->created_by;
            $purchase->sync_id = $merchantId . Uuid::uuid4()->toString();
            if ($request->is_debet == 0) {
                if ($isKeep == 0) {
                    $purchase->coa_trans_id = $trans[1];
                    $purchase->md_transaction_type_id = $trans[0];
                }
            }

            if ($isKeep == 1) {
                $purchase->is_keep_transaction = 1;
            }

            $purchase->is_approved_shop = 1;
            $purchase->created_at = date('Y-m-d H:i:s');
            $purchase->updated_at = date('Y-m-d H:i:s');

            $purchase->assign_to_user_helper = $request->assign_to_user_helper;
            $purchase->mode_cashier = $request->mode_cashier;
            $purchase->receive_time = $request->receive_time;
            $purchase->pick_up_time = $request->pick_up_time;
            $purchase->cost_other_sale = ($request->cost_other_sale) ? json_encode($request->cost_other_sale) : null;
            $purchase->total_cost_other_sale = ($request->total_cost_other_sale) ? $request->total_cost_other_sale : 0;
            $destinationPath = 'public/uploads/merchant/'.$request->created_by.'/cashiermode/';

            if($request->hasFile('guarantee'))
            {
                $file=$request->file('guarantee');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }
            $purchase->guarantee = $fileName;


            $purchase->save();
            $details = [];
            $timestamp = date('Y-m-d H:i:s');
            $detail = $request->details;
            if (empty($detail)) {
                return $this->message::getJsonResponse(404, 'Data item penjualan tidak boleh kosong', []);

            }

            $productId = [];
            $productIds = "";
            foreach ($detail as $n => $i) {
                $productId[] = $i['sc_product_id'];
                $productIds .= ($n == 0) ? $i['sc_product_id'] : "," . $i['sc_product_id'];
            }

            $products = collect(DB::select("
                select
                  sp.*,
                  sp.inv_id as coa_inv_id,
                  sp.hpp_id as coa_hpp_id,
                  u.name as default_unit_name,
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          spmu.*,
                          mu.name
                        FROM
                          sc_product_multi_units spmu
                          join md_units mu on mu.id = spmu.md_unit_id
                        WHERE
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                      ) jd
                  ) AS multi_units
                from
                  sc_products sp
                left join md_units u on u.id = sp.md_unit_id
                where
                  sp.id in ($productIds)
            "));
            $productTemporary = [];
            $productWithQuantitys= [];
            $sumCost=0;
            $is_from_package=0;
            foreach ($detail as $j => $item) {
                $product = $products->firstWhere('id', $item['sc_product_id']);
                $profit=0;
                if( $product->md_sc_product_type_id==4){
                    $is_from_package=1;
                    if($product->is_package_subs==1){
                        if($item['quantity']>$product->limit_selling){
                            return $this->message::getJsonResponse(404, 'Penjualan produk '.$product->name.' telah melebihi batas!', []);
                        }
                        
                        $calculatePackageSubs=ProductUtil::calculatePackageSubs($product->id,$item['quantity'],$item['price'],$purchase->id);
                        // dd($calculatePackageSubs);
                        if($calculatePackageSubs==false){
                            return $this->message::getJsonResponse(404, 'Terjadi kesalahan ketika penambahan langganan produk', []);
                        }
                    }else{
                        $productChild= Material::join('sc_products as p','p.id','child_sc_product_id')
                        ->where('parent_sc_product_id', $product->id)
                        ->where('sc_product_raw_materials.is_deleted',0)
                        ->get();

                        foreach ($productChild as $p => $c) {
                            if($c->is_with_stock==1){
                                if($c->stock<$item['quantity']*$c->quantity){
                                    return $this->message::getJsonResponse(404, 'Stok produk '.$c->name.' tidak mencukupi!', []);
                                }
                                array_push($productTemporary, [
                                    'id' => $c->child_sc_product_id,
                                    'name' => $c->name,
                                    'code' => $c->code,
                                    'stock' => $c->stock,
                                    'selling_price' => $c->selling_price,
                                    'purchase_price' => $c->purchase_price,
                                    'quantity' => $item['quantity']*$c->quantity,
                                    'is_from_package'=>1
                                ]);
                            }
                    }
                    }


                    if(!is_null($product->cost_other) && $product->cost_other!=[]){
                        foreach(json_decode($product->cost_other) as $sum){
                            $sumCost+=($sum->amount*$item['quantity']);
                        }
                    }

                }
                $isMulti = isset($item['is_multi_unit']) ? $item['is_multi_unit'] : 0;
                $MultiId = isset($item['multi_unit_id']) ? $item['multi_unit_id'] : null;

                $multi_quantity = $item['quantity'];
                $mQuantity = $item['quantity'];
                $mPrice = $item['price'];
                $sub_total = $item['sub_total'];
                $convertionValue=1;

                if ($isMulti == 1 && !is_null($MultiId)) {
                    $multi_unit = collect(json_decode($product->multi_units));
                    $unit = $multi_unit->firstWhere('id', $item['multi_unit_id']);
                    $unit_name = $unit->name;
                    $convertionValue=$unit->quantity;
                    $multi_quantity = $convertionValue * $item['quantity'];
                    $priceDefault = $item['price'] * $mQuantity / $unit->quantity;
                    $jsonMulti=$multi_unit;
                }else{
                    $unit_name=$product->default_unit_name;
                }

                if ($isKeep == 0) {
                    if ($product->is_with_stock == 1 && $product->md_sc_product_type_id!=4) {

                        array_push($productTemporary, [
                            'id' => $product->id,
                            'name' => $product->name,
                            'code' => $product->code,
                            'stock' => $product->stock,
                            'selling_price' => ($isMulti == 1) ? $priceDefault : $mPrice,
                            'purchase_price' => $product->purchase_price,
                            'quantity' => $multi_quantity,
                            'is_from_package'=>0
                        ]);

                    }
                }
                $isBonus = isset($item['is_bonus']) ? $item['is_bonus'] : 0;
                $noteOrder = isset($item['note_order']) ? $item['note_order'] : "";
                $startRent = isset($item['start_rent']) ? $item['start_rent'] : null;
                $endRent = isset($item['end_rent']) ? $item['end_rent'] : null;
                $discount=null;

                if(!is_null($item['ref_discount_id']) && $item['ref_discount_id']!=0){
                    $discount=$this->discount::join('crm_discount_by_product_details as d','d.discount_by_product_id','crm_discount_by_products.id')
                                ->where('d.sc_product_id',$item['sc_product_id'])
                                ->where('crm_discount_by_products.id',$item['ref_discount_id'])
                                ->first();
                }

                $productWithQuantitys[]=(object)[
                    'id'=>$item['sc_product_id'],
                    'origin_quantity'=>$item['quantity'],
                    'quantity'=>$item['quantity']*$convertionValue,
                    'price'=>$item['quantity']*$item['price'],
                    'product_name'=>$product->name,
                    'unit_name'=>($isMulti==1 && !is_null($MultiId))?$unit_name:$product->default_unit_name,
                    'origin_unit_name'=>'',
                ];

                $jsonPromo=(isset($item['json_amount_promo']))?json_encode($item['json_amount_promo']):null;

                $details[] = [
                    'sc_product_id' => $item['sc_product_id'],
                    'quantity' => $mQuantity,
                    'multi_quantity' => $multi_quantity,
                    'is_bonus' => $isBonus,
                    'is_multi_unit' => $isMulti,
                    'unit_name' => $unit_name,
                    'start_rent' => $startRent,
                    'end_rent' => $endRent,
                    'multi_unit_id' => $MultiId,
                    'ref_discount_id' => $item['ref_discount_id'],
                    'sub_total' => $sub_total,
                    'sc_sale_order_id' => $purchase->id,
                    'profit' => $profit,
                    'price' => $mPrice,
                    'note_order' => $noteOrder,
                    'discount_value' => $discount,
                    'special_price_id' => ( isset($item['special_price_id']) ) ? $item['special_price_id'] : null ,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp,
                    'coa_inv_id' => ($product->is_with_stock == 0) ? null : $product->coa_inv_id,
                    'coa_hpp_id' => ($product->is_with_stock == 0) ? null : $product->coa_hpp_id,
                    'value_conversation' => ( $isMulti==1 ) ? $convertionValue : 1,
                    'json_multi_unit' => ( $isMulti==1 ) ? $jsonMulti : null,
                    'json_amount_promo' => $jsonPromo

                ];
            }

            if($request->user_helpers){
                $userHelpers=$request->user_helpers;
                $userHelperIds = "";
                foreach($userHelpers as $key => $u){
                    $userHelperIds .= ($key == 0) ? "'".$u['id']."'" : "," . "'".$u['id']."'";
                }

                if(!empty($userHelpers)){
                    $productList="(".$productIds.")";
                    $userHelperList="(".$userHelperIds.")";
                    $checkCommission=CommissionUtil::resultCommission($productList,$productWithQuantitys,$userHelperList,$merchantId,$purchase->total,$purchase->id,$timestamp,$request->timezone);

                    if(!empty($checkCommission)){
                        $purchase->commission_value=json_encode($checkCommission);
                        $purchase->save();
                        DB::table('merchant_commission_lists')->insert($checkCommission);
                    }
                    $purchase->assign_to_user_helper=json_encode($userHelpers);
                    $purchase->save();
                }
            }

            if($isReferral==1){
                $commission=$this->commission::find($coupon->commission_id);
                $calculateCommission=CommissionUtil::calculateCommission($commission,$productWithQuantitys,$merchantId,$purchase->total,$purchase->id,$timestamp,$request->timezone);
                if(!empty($calculateCommission)){
                    $purchase->commission_value=json_encode($calculateCommission);
                    $purchase->save();
                    DB::table('merchant_commission_lists')->insert($calculateCommission);
                }


            }

            if($is_from_package==1){
                $purchase->cost_other=$sumCost;
                $purchase->save();
            }
            DB::table('sc_sale_order_details')->insert($details);
            $productInsert=[
                'productTemporary'=>$productTemporary,
                'isKeep'=>$isKeep,
                'warehouseId'=>$warehouseId,
                'createdBy'=>$request->created_by,
                'timestamp'=>$timestamp,
                'invMethodId'=>$invMethod,
                'code'=>$purchase->code,
                'id'=>$purchase->id,
                'userId'=>$purchase->getMerchant->md_user_id,
                'merchantId'=>$merchantId,
                'coaSale'=>[],
                'paidNominal'=>$request->paid_nominal,
                'timezone'=>$request->timezone,
                'isDebet'=>$purchase->is_debet,
                'total'=>$request->total,
                'dueDate'=>$request->due_date,
                'trans1'=>($request->is_debet == 1 && $request->paid_nominal >0)?$trans[1]:null,
                'coaAr'=>null

            ];
            // dd(CoaSaleUtil::coaStockAdjustmentFromSaleV2Mobile($productInsert['userId'],$productInsert['merchantId'],collect($productInsert['coaSale']),$productInsert['warehouseId'],$productInsert['invMethodId'],$productInsert['id']));
            $resultJson=[
                'sale_id'=>$purchase->id,
                'sale_code'=>$purchase->code,
                'md_sc_transaction_status_id'=>$purchase->md_sc_transaction_status_id
            ];
            DB::commit();
            Session::put('sale_mobile_'.$purchase->id,$productInsert);
            Artisan::call('sale:mobile', ['--sale_id' =>$purchase->id]);
            return $this->message::getJsonResponse(200,trans('ordershop.so_create'),$resultJson);
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, 'Terjadi kesalahan disistem,cobalah beberapa saat lagi', []);

        }
    }

    public function editKeepTransaction($saleOrderId,Request $request)
    {

        try{
            DB::beginTransaction();
            $data=$this->order::find($saleOrderId);
            if(is_null($data)){
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }

            $merchantId=$data->md_merchant_id;

            date_default_timezone_set($request->timezone);
            $data->total=$request->total;
            $data->save();
            $detail=$request->details;
            if(empty($detail)){
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }
            $timestamp = date('Y-m-d H:i:s');
            $new=[];

            $warehouseId=$request->inv_warehouse_id;
            if(is_null($warehouseId))
            {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }
            $invMethod=$request->md_inventory_method_id;

            if(is_null($invMethod))
            {
                return $this->message::getJsonResponse(404, 'Parameter metode persediaan belum terisi', []);

            }

            $productId="";
            foreach ($detail as $j => $v)
            {
                $productId.=($j==0)?"".$v['sc_product_id']."":",".$v['sc_product_id']."";
            }
            $multiunits=collect(DB::select("
                      SELECT
                        spmu.id,
                        spmu.sc_product_id,
                        spmu.md_unit_id,
                        spmu.quantity,
                        spmu.price,

                        mu.name
                      FROM
                        sc_product_multi_units spmu
                        join md_units mu on mu.id = spmu.md_unit_id
                      WHERE
                        spmu.sc_product_id in($productId)
                        and spmu.is_deleted = 0
          "));

            $productQuery=collect(DB::select("
              select
                  sp.*,
                  dd.id as sc_sale_order_detail_id,
                  dd.quantity,
                  sp.inv_id as coa_inv_id,
                  sp.hpp_id as coa_hpp_id,
                  dd.coa_inv_id as coa_inv_id_detail,
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          spmu.id,
                          spmu.sc_product_id,
                          spmu.md_unit_id,
                          spmu.quantity,
                          spmu.price,

                          mu.name
                        FROM
                          sc_product_multi_units spmu
                          join md_units mu on mu.id = spmu.md_unit_id
                        WHERE
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                      ) jd
                  ) AS multi_units
                from
                  sc_products sp
                  left join(
                    select
                      d.sc_product_id,
                      d.id,
                      d.quantity,
                      d.coa_inv_id
                    from
                      sc_sale_order_details d
                      join sc_sale_orders s on s.id = d.sc_sale_order_id
                    where
                      s.id = $saleOrderId
                      and d.is_deleted = 0
                  ) dd on dd.sc_product_id = sp.id
                where
                  sp.id in($productId)

            "));
            $this->orderDetail::where('sc_sale_order_id',$saleOrderId)->delete();

            foreach ($detail as $key =>$item)
            {

                $isMulti=isset($item['is_multi_unit'])?$item['is_multi_unit']:0;
                $MultiId=isset($item['multi_unit_id'])?$item['multi_unit_id']:null;
                $noteOrder=isset($item['note_order'])?$item['note_order']:"";

                $unit_name="";
                $mQuantity=$item['quantity'];
                $mPrice=$item['price'];
                $sub_total=$item['sub_total'];
                $multi_quantity=$item['quantity'];
                $jsonMulti=null;
                $convertionValue=1;

                if($isMulti==1 && !is_null($MultiId) && $item['quantity']>0){
                    $unit=$multiunits->firstWhere('id',$MultiId);
                    $unit_name=$unit->name;
                    $multi_quantity=$unit->quantity*$item['quantity'];
                    $mPrice=$item['quantity']*$item['price']/$unit->quantity;
                    $jsonMulti=json_encode($unit);
                    $convertionValue=$unit->quantity;
                }

                $product=$productQuery->firstWhere('id',$item['sc_product_id']);
                    if($product->is_with_stock==1)
                    {
                        if($product->stock<$mQuantity)
                        {
                            return $this->message::getJsonResponse(404,'Stok produk tidak mencukupi',[$product->name]);
                        }
                    }
                    $new[]=[
                        'sc_product_id'=>$product->id,
                        'quantity'=>$mQuantity,
                        'sub_total'=>$sub_total,
                        'sc_sale_order_id'=>$data->id,
                        'multi_unit_id'=>$MultiId,
                        'is_multi_unit'=>$isMulti,
                        'unit_name'=>$unit_name,
                        'multi_quantity'=>$multi_quantity,
                        'profit'=>0,
                        'price'=>$mPrice,
                        'is_bonus'=>0,
                        'note_order'=>$noteOrder,
                        'created_at'=>$timestamp,
                        'updated_at'=>$timestamp,
                        'coa_inv_id'=>($product->is_with_stock==0)?null:$product->coa_inv_id,
                        'coa_hpp_id'=>($product->is_with_stock==0)?null:$product->coa_hpp_id,
                        'value_conversation'=>$convertionValue,
                        'json_multi_unit'=>$jsonMulti
                    ];
            }
            $result=[
                'id'=>$data->id,
                'code'=>$data->code,
                'total'=>$data->total,
                'detail'=>$new
            ];
            DB::table('sc_sale_order_details')->insert($new);
            DB::commit();
            return $this->message::getJsonResponse(200,'Edit keep berhasil',$result);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), [$e]);
        }
    }


    public function payKeepTransaction(Request $request,$orderId)
    {
        try{
            DB::beginTransaction();
            $data=SaleOrder::find($orderId);
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404, trans('message.404'), []);

            }
            if (is_null($request->md_transaction_type_id)) {
                return $this->message::getJsonResponse(404,'Metode Pembayaran belum dipilih',[]);
            }

            if($data->is_keep_transaction==0)
            {
                return $this->message::getJsonResponse(404,'Transaksi sudah terbayarkan',[]);
            }

            $warehouseId=$request->inv_warehouse_id;
            if(is_null($warehouseId))
            {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }
            $invMethod=$request->md_inventory_method_id;

            if(is_null($invMethod))
            {
                return $this->message::getJsonResponse(404, 'Parameter metode persediaan belum terisi', []);

            }

            $trans = explode('_', $request->md_transaction_type_id);
            $data->sc_customer_id = $request->sc_customer_id;
            $data->queue_code = $request->queue_code;
            $data->created_by = $request->created_by;
            $data->note_order = $request->note_order;
            $data->courier_name = $request->courier_name;
            $data->service_courier_name = $request->service_courier_name;
            $data->destination = $request->destination;
            $data->distance = $request->distance;
            $data->weight = $request->weight;
            $data->qr_code = '-';
            $data->is_debet = $request->is_debet;
            $data->is_with_retur = 0;
            $data->change_money = $request->change_money;
            $data->sc_customer_id = $request->sc_customer_id;
            if ($request->md_sc_shipping_category_id == 1) {
                $data->is_with_load_shipping = 1;

            } else {
                $data->is_with_load_shipping = 0;

            }
            if($request->is_debet==0){
                $data->coa_trans_id = $trans[1];
                $data->md_transaction_type_id = $trans[0];
            }
            $data->total = $request->total;
            $data->paid_nominal = $request->paid_nominal;
            $data->md_sc_transaction_status_id = ($request->is_debet == 0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;

            $data->md_sc_shipping_category_id = $request->md_sc_shipping_category_id;
            $data->tax = $request->tax;

            $isReferral=0;
            if (!is_null($request->ref_coupon_id)) {
                $isReferral=$request->is_referral;
                $flex=($isReferral==1)?'referral':'kupon';

                $coupon=$this->discount::find($request->ref_coupon_id);
                if($coupon->is_limited==1){
                    if($coupon->max_coupon_use<=0){
                        return $this->message::getJsonResponse(404, 'Kuota '.$flex.' telah habis, tidak dapat digunakan!', []);
                    }else if($coupon->max_coupon_use>0){
                        $coupon->max_coupon_use-=1;
                        $coupon->save();
                    }
                }

                if($request->sc_customer_id){
                    $getCouponUsed=$this->order::where('sc_customer_id',$request->sc_customer_id)
                    ->where("ref_coupon_id", $request->ref_coupon_id)
                    ->where("is_keep_transaction", 0)
                    ->where("is_deleted", 0)
                    ->where("step_type", 0)
                    ->count();

                    if($coupon->max_coupon_use_per_customer>0 && $coupon->max_coupon_use_per_customer<$getCouponUsed){
                        return $this->message::getJsonResponse(404, 'Kuota '.$flex.' untuk pelanggan ini telah habis, tidak dapat digunakan!', []);
                    }
                }

                $data->ref_coupon_id = $request->ref_coupon_id;
                $data->ref_coupon_code = $request->ref_coupon_code;
                $data->is_referral = $request->is_referral;
                $data->coupon_value = $coupon;

            }

            if (!is_null($request->ref_discount_id)) {
                $disc_id=[];
                $disc_code=[];
                $discount=$this->discount::find($request->ref_discount_id);

                $disc_id[]=[
                    'discount_id'=>$request->ref_discount_id
                ];
                $disc_code[]=[
                    'discount_code'=>$request->ref_discount_code
                ];

                $data->ref_discount_id = json_encode($disc_id);
                $data->ref_discount_code = json_encode($disc_code);
                $data->discount_value = $discount;

            }
            $data->json_amount_promo = ($request->json_amount_promo) ? json_encode($request->json_amount_promo) : null;

            $data->discount_primary_id = $request->discount_primary_id;
            $data->promo = $request->promo;
            $data->promo_percentage = $request->promo_percentage;
            $data->tax_percentage = $request->tax_percentage;
            $data->shipping_cost = $request->shipping_cost;
            $data->sync_id = $data->md_merchant_id . Uuid::uuid4()->toString();
            $data->is_keep_transaction=0;
            $data->save();

            $detail = $request->details;

            $merchantId=$data->md_merchant_id;
            $new=[];
            $saleMapping=[];
            $stockOut=[];
            $updateStockProduct="";
            $productCalculate=[];
            $timestamp=date('Y-m-d H:i:s');
            $productId="";
            $productIsBonus="";
            $countProduct=0;
            $countBonus=0;

            if($detail){
                foreach ($detail as $b => $d)
                {
                    if($d['is_bonus']==1)
                    {
                        $productIsBonus.=($countBonus==0)?$d['sc_product_id']:",".$d['sc_product_id'];
                        $countBonus++;
                    }
                    $productId.=($countProduct==0)?$d['sc_product_id']:",".$d['sc_product_id'];
                    $countProduct++;

                    $multiunits=collect(DB::select("
                      SELECT
                        spmu.id,
                        spmu.sc_product_id,
                        spmu.md_unit_id,
                        spmu.quantity,
                        spmu.price,

                        mu.name
                      FROM
                        sc_product_multi_units spmu
                        join md_units mu on mu.id = spmu.md_unit_id
                      WHERE
                        spmu.sc_product_id in($productId)
                        and spmu.is_deleted = 0
          "));
                }
            }

            if($productIsBonus!="")
            {
                $productIsBonusQuery = collect(DB::select("
                select
                  sp.*,
                  sp.inv_id as coa_inv_id,
                  sp.hpp_id as coa_hpp_id,
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          spmu.id,
                          spmu.sc_product_id,
                          spmu.md_unit_id,
                          spmu.quantity,
                          spmu.price,

                          mu.name
                        FROM
                          sc_product_multi_units spmu
                          join md_units mu on mu.id = spmu.md_unit_id
                        WHERE
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                      ) jd
                  ) AS multi_units
                from
                  sc_products sp
                where
                  sp.id in ($productIsBonus)"));
            }else{
                $productIsBonusQuery=[];
            }

                $productQuery=collect(DB::select("
                select
                    sp.*,
                    sp.inv_id as coa_inv_id,
                    sp.hpp_id as coa_hpp_id,
                    (
                      SELECT
                        jsonb_agg(jd.*) AS jsonb_agg
                      FROM
                        (
                          SELECT
                            spmu.id,
                            spmu.sc_product_id,
                            spmu.md_unit_id,
                            spmu.quantity,
                            spmu.price,
  
                            mu.name
                          FROM
                            sc_product_multi_units spmu
                            join md_units mu on mu.id = spmu.md_unit_id
                          WHERE
                            spmu.sc_product_id = sp.id
                            and spmu.is_deleted = 0
                        ) jd
                    ) AS multi_units
                  from
                    sc_products sp
                  where
                    sp.id in($productId)
  
              "));
              $discountQuery=collect(DB::select("
              select
                  p.*
                from
                  crm_discount_by_products p
                  join crm_discount_by_product_details d on d.discount_by_product_id = p.id
                where
                  d.sc_product_id in($productId)

            "));
            $productTemporary=[];
            $stockInvId=[];
            $updateStockInv="";
            $sumCost=0;
            $this->orderDetail::where('sc_sale_order_id',$orderId)->delete();

                foreach ($detail as $i =>$v)
                {
                    if($v['is_bonus']==1){
                        $coaInvBonusQuery = $productIsBonusQuery->firstWhere('id', $v['sc_product_id']);

                        if ($coaInvBonusQuery->is_with_stock == 1) {
                            array_push($productTemporary, [
                                'id' => $coaInvBonusQuery->id,
                                'name' => $coaInvBonusQuery->name,
                                'code' => $coaInvBonusQuery->code,
                                'stock' => $coaInvBonusQuery->stock,
                                'selling_price' => $v['price'],
                                'purchase_price' => $coaInvBonusQuery->purchase_price,
                                'quantity' => $v['quantity'],
                                'is_from_package' => 0
                            ]);
                        }
                        $coaInvBonus = ($coaInvBonusQuery->is_with_stock == 0) ? null : $coaInvBonusQuery->coa_inv_id;
                        $coaHppBonus = ($coaInvBonusQuery->is_with_stock == 0) ? null : $coaInvBonusQuery->coa_hpp_id;
                        $new[] = [
                            'sc_product_id' => $v['sc_product_id'],
                            'quantity' => $v['quantity'],
                            'multi_quantity' => 0,
                            'is_bonus' => 1,
                            'is_multi_unit' => 0,
                            'unit_name' => null,
                            'ref_discount_id' => null,
                            'sub_total' => $v['sub_total'],
                            'sc_sale_order_id' => $data->id,
                            'profit' => 0,
                            'price' => $v['price'],
                            'note_order' => null,
                            'created_at' => $timestamp,
                            'updated_at' => $timestamp,
                            'coa_inv_id' => $coaInvBonus,
                            'coa_hpp_id' => $coaHppBonus
                        ];

                    }else{
                        $isMulti=isset($v['is_multi_unit'])?$v['is_multi_unit']:0;
                        $MultiId=isset($v['multi_unit_id'])?$v['multi_unit_id']:null;
                        $refDiscId=isset($v['ref_discount_id'])?$v['ref_discount_id']:null;
                        $noteOrder=isset($v['note_order'])?$v['note_order']:"";
        
                        $unit_name="";
                        $mQuantity=$v['quantity'];
                        $mPrice=$v['price'];
                        $sub_total=$v['sub_total'];
                        $multi_quantity=$v['quantity'];
                        $jsonMulti=null;
                        $convertionValue=1;
        
                        if($isMulti==1 && !is_null($MultiId) && $v['quantity']>0){
                            $unit=$multiunits->firstWhere('id',$MultiId);
                            $unit_name=$unit->name;
                            $multi_quantity=$unit->quantity*$v['quantity'];
                            $mPrice=$v['quantity']*$v['price']/$unit->quantity;
                            $jsonMulti=json_encode($unit);
                            $convertionValue=$unit->quantity;
                        }
        
                                $product=$productQuery->firstWhere('id',$v['sc_product_id']);
                                $promo=$discountQuery->firstWhere('id',$v['ref_discount_id']);
                                $jsonPromo=(isset($v['json_amount_promo']))?json_encode($v['json_amount_promo']):null;
        
                                $new[]=[
                                    'sc_product_id'=>$product->id,
                                    'quantity'=>$mQuantity,
                                    'sub_total'=>$sub_total,
                                    'sc_sale_order_id'=>$data->id,
                                    'multi_unit_id'=>$MultiId,
                                    'is_multi_unit'=>$isMulti,
                                    'unit_name'=>$unit_name,
                                    'multi_quantity'=>$multi_quantity,
                                    'ref_discount_id'=>$refDiscId,
                                    'profit'=>0,
                                    'price'=>$mPrice,
                                    'is_bonus'=>0,
                                    'note_order'=>$noteOrder,
                                    'created_at'=>$timestamp,
                                    'updated_at'=>$timestamp,
                                    'coa_inv_id'=>($product->is_with_stock==0)?null:$product->inv_id,
                                    'coa_hpp_id'=>($product->is_with_stock==0)?null:$product->hpp_id,
                                    'value_conversation'=>$convertionValue,
                                    'json_multi_unit'=>$jsonMulti,
                                    'json_amount_promo'=>$jsonPromo,
                                    'discount_value'=>json_encode($promo)
                                ];

                                //add to json calculated stock
                                if ($product->is_with_stock == 1) {
            
                                    if( $product->md_sc_product_type_id==4){
                                        if($product->is_package_subs==1){
                                            if($v['quantity']>$product->limit_selling){
                                                return $this->message::getJsonResponse(404, 'Penjualan produk '.$product->name.' telah melebihi batas!', []);
                                            }
                                            
                                            $calculatePackageSubs=ProductUtil::calculatePackageSubs($product->id,$v['quantity'],$v['price'],$data->id);
                                            // dd($calculatePackageSubs);
                                            if($calculatePackageSubs==false){
                                                return $this->message::getJsonResponse(404, 'Terjadi kesalahan ketika penambahan langganan produk', []);
                                            }
                                        }else{
                                            $productChild= Material::where('parent_sc_product_id', '=', $product->id)
                                            ->join('sc_products as p','p.id','child_sc_product_id')
                                            ->where('sc_product_raw_materials.is_deleted',0)
                                            ->get();
                                            foreach ($productChild as $p => $c) {
                                                array_push($productTemporary, [
                                                    'id' => $c->child_sc_product_id,
                                                    'name' => $c->name,
                                                    'code' => $c->code,
                                                    'stock' => $c->stock,
                                                    'selling_price' => $c->selling_price,
                                                    'purchase_price' => $c->purchase_price,
                                                    'quantity' => $v['quantity']*$c->quantity,
                                                    'is_from_package' => 1
                                                ]);
                                            }
                                            if(!is_null($product->cost_other)){
                                                foreach(json_decode($product->cost_other) as $sum){
                                                    $sumCost+=$sum->amount;
                                                }
                                            }
                                        }
                                    }else{
                                        array_push($productTemporary, [
                                            'id' => $product->id,
                                            'name' => $product->name,
                                            'code' => $product->code,
                                            'stock' => $product->stock,
                                            'selling_price' => $v['price']*$v['quantity']/$convertionValue,
                                            'purchase_price' => $product->purchase_price,
                                            'quantity' => $v['quantity']*$convertionValue,
                                            'is_from_package' => 0
                                        ]);
                                    }
                                }
            
                                
                            $productWithQuantitys[]=(object)[
                                'id'=>$v['sc_product_id'],
                                'origin_quantity'=>$v['quantity'],
                                'quantity'=>$v['quantity']*$convertionValue,
                                'price'=>$v['quantity']*$v['price'],
                                'product_name'=>$product->name,
                                'unit_name'=>($v['is_multi_unit']==1)?$unit_name:$product->getUnit->name,
                                'origin_unit_name'=>'',
                            ];
        
                    }
 
              

                }

            $productWithQuantitys=[];

            if($request->user_helpers){
                $userHelpers=$request->user_helpers;
                $userHelperIds = "";
                foreach($userHelpers as $key => $u){
                    $userHelperIds .= ($key == 0) ? "'".$u['id']."'" : "," . "'".$u['id']."'";
                }

                if(!empty($userHelpers)){
                    $productList="(".$productId.")";
                    $userHelperList="(".$userHelperIds.")";
                    $checkCommission=CommissionUtil::resultCommission($productList,$productWithQuantitys,$userHelperList,$merchantId,$data->total,$data->id,$timestamp,$request->timezone);

                    if(!empty($checkCommission)){
                        $data->commission_value=json_encode($checkCommission);
                        $data->save();
                        DB::table('merchant_commission_lists')->insert($checkCommission);

                    }
                    $data->assign_to_user_helper=json_encode($userHelpers);
                    $data->save();
                }
            }

            if($isReferral==1){
                $commission=$this->commission::find($coupon->commission_id);
                $calculateCommission=CommissionUtil::calculateCommission($commission,$productWithQuantitys,$merchantId,$data->total,$data->id,$timestamp,$request->timezone);
                if(!empty($calculateCommission)){
                    $data->commission_value=json_encode($calculateCommission);
                    $data->save();
                    DB::table('merchant_commission_lists')->insert($calculateCommission);

                }
            }
            
            $data->cost_other = $sumCost;
            $data->save();

            //dd($new);
            DB::table('sc_sale_order_details')->insert($new);

            $detailQuery=collect(DB::select("
                    select
                      ssod.*,
                      sp.is_with_stock,
                      sp.selling_price,
                      ssod.coa_inv_id as coa_inv_detail,
                      sp.inv_id as coa_inv_id,
                      sp.hpp_id as coa_hpp_id,
                      sp.purchase_price,
                      sp.stock,
                      sp.name,
                      sp.code,
                      (
                        SELECT
                          jsonb_agg(jd.*) AS jsonb_agg
                        FROM
                          (
                            SELECT
                              spmu.id,
                              spmu.sc_product_id,
                              spmu.md_unit_id,
                              spmu.quantity,
                              spmu.price,

                              mu.name
                            FROM
                              sc_product_multi_units spmu
                              join md_units mu on mu.id = spmu.md_unit_id
                            WHERE
                              spmu.sc_product_id = sp.id
                              and spmu.is_deleted = 0
                          ) jd
                      ) AS multi_units
                    from
                      sc_sale_order_details ssod
                      join sc_products sp on ssod.sc_product_id = sp.id
                    where
                      ssod.is_deleted = 0
                      and ssod.sc_sale_order_id = $orderId
                "));
            if($trans[0]==4)
            {
                $dataInquiry=[];
                array_push($dataInquiry,[
                    "name"=> $detailQuery->first()->name,
                    "sku"=> (is_null( $detailQuery->first()->name))?'-':$detailQuery->first()->code,
                    "qty"=> 1,
                    "unitPrice"=> $data->total,
                    "desc"=> $detailQuery->first()->name
                ]);
                $token=WinpayUtil::getToken();
                if($token==false)
                {
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan dalam proses generate QRIS',[]);
                }
                $inquiry=WinpayUtil::inquiryQRIS($token,$data->code,$dataInquiry,$data->total,$data->getMerchant);

                if($inquiry==false)
                {
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan dalam proses menampilkan data QRIS',[]);

                }
                $data->expired_time=Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
                $data->md_sc_transaction_status_id =TransactionStatus::UNPAID;
                $data->qris_response=json_encode($inquiry);
                $data->save();

                $resultJson=[
                    'sale'=>$data,
                    'qris'=>$inquiry
                ];
                return $this->message::getJsonResponse(200,'Silahkan melakukan scan QRIS untuk melanjutkan pembayaran',$resultJson);

            }else{

                if(!empty($productTemporary))
                {
                    $key1=0;
                    foreach (collect($productTemporary)->groupBy('id') as $item)
                    {
                        $quantity=collect($item)->reduce(function($i, $k) {
                            return $i + $k['quantity'];
                        }, 0);

                        array_push($productCalculate,[
                            'id'=>$item->first()['id'],
                            'amount'=>$quantity,
                        ]);
                        //dd($item);
                        $stock=$item->first()['stock'];

                        $stockP=$stock-$quantity;
                        $updateStockProduct.=($key1==0)?"(".$item->first()['id'].",".$stockP.")":",(".$item->first()['id'].",".$stockP.")";
                        $key1++;

                        $stockOut[]=[
                            'sync_id' => $item->first()['id'] . Uuid::uuid4()->toString(),
                            'sc_product_id' => $item->first()['id'],
                            'total' => $quantity,
                            'inv_warehouse_id' => $warehouseId,
                            'record_stock' => $stockP,
                            'created_by' => $request->created_by,
                            'selling_price' => $item->first()['selling_price'],
                            'purchase_price' => $item->first()['purchase_price'],
                            'residual_stock' => $quantity,
                            'type' => StockInventory::OUT,
                            'transaction_action' => 'Pengurangan Stok ' . $item->first()['name'] . ' Dari Penjualan Dengan Code ' . $data->code ,
                            'stockable_type'=>'App\Models\SennaToko\SaleOrder',
                            'stockable_id'=>$data->id,
                            'created_at'=>$timestamp,
                            'updated_at'=>$timestamp
                        ];
                    }
                }

                if(!empty($productCalculate))
                {

                    $calculateInventory=InventoryUtilV2::inventoryCodev2($invMethod,$warehouseId,$productCalculate,'minus');
                    if($calculateInventory==false)
                    {
                        return $this->message::getJsonResponse(404,'Terjadi kesalahan saat pengurangan stok dipersediaan',[]);
                    }

                    foreach ($calculateInventory as $key => $n)
                    {
                        $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                        $stockInvId[]=$n['id'];
                        $saleMapping[]=[
                            'sc_sale_order_id'=>$data->id,
                            'sc_product_id'=>$n['sc_product_id'],
                            'sc_stock_inventory_id'=>$n['id'],
                            'amount'=>$n['amount'],
                            'purchase_price'=>$n['purchase'],
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }

                }

                DB::table('sc_stock_sale_mappings')->insert($saleMapping);
                DB::table('sc_stock_inventories')->insert($stockOut);
                if(!empty($stockInvId)){
                    DB::table('sc_inv_production_of_goods')
                    ->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);
                }


                if($data->is_debet==1)
                {
                    $idAr=$data->ar()->insertGetId([
                        'sync_id'=>$data->md_merchant_id.Uuid::uuid4()->toString(),
                        'md_merchant_id'=>$data->md_merchant_id,
                        'residual_amount'=>$request->total-$request->paid_nominal,
                        'paid_nominal'=>$request->paid_nominal,
                        'is_paid_off'=>0,
                        'arable_id'=>$data->id,
                        'arable_type'=>'App\Models\SennaToko\SaleOrder',
                        'due_date'=>$request->due_date,
                        'timezone'=>$request->timezone
                    ]);
                }

                if($updateStockInv!=""){
                    DB::statement("
                update sc_stock_inventories as t set
                        residual_stock = c.column_a
                    from (values
                        $updateStockInv
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                }

                if($updateStockProduct!=""){
                    DB::statement("
                update sc_products as t set
                        stock = c.column_a
                    from (values
                        $updateStockProduct
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                }


                if($data->is_debet==1 && $data->paid_nominal>0)
                {
                    $codeJur=Jurnal::where('md_merchant_id',$data->md_merchant_id)
                            ->count()+1;

                    $ard=new MerchantArDetail();
                    $ard->ar_detail_code='PTR'.sprintf("%07s", $codeJur);
                    $ard->paid_nominal=$request->paid_nominal;
                    $ard->paid_date=$timestamp;
                    $ard->note='Uang Muka Penjualan';
                    $ard->acc_merchant_ar_id=$idAr;
                    $ard->created_by=$request->created_by;
                    $ard->timezone=$request->timezone;
                    $ard->trans_coa_detail_id=$trans[1];
                    $ard->save();

                    $jurnal=new Jurnal();
                    $jurnal->ref_code=$ard->ar_detail_code;
                    $jurnal->trans_code='JRN'.sprintf("%07s", $codeJur);;
                    $jurnal->trans_name='Uang Muka  Penjualan '.$data->code.'-'.$ard->ar_detail_code;
                    $jurnal->trans_time=$ard->paid_date;
                    $jurnal->trans_note='Uang Muka Penjualan '.$ard->code.'-'.$ard->ar_detail_code;
                    $jurnal->trans_purpose='Uang Muka  Penjualan '.$data->code.'-'.$ard->ar_detail_code;
                    $jurnal->trans_amount=$ard->paid_nominal;
                    $jurnal->md_merchant_id=$data->md_merchant_id;
                    $jurnal->md_user_id_created=$data->created_by;
                    $jurnal->external_ref_id=$ard->id;
                    $jurnal->flag_name='Uang Muka Penjualan '.$data->code;
                    $jurnal->save();

                    if(is_null($data->getMerchant->coa_ar_sale_id)){
                        return $this->message::getJsonResponse(404,trans('Terjadi kesalahan dalam pencatatan jurnal akun akuntansi merchant kosong!'));
                    }

                    $fromCoa=$data->getMerchant->coa_ar_sale_id;
                    $toCoa=$trans[1];
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$fromCoa,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$ard->paid_nominal,
                            'created_at'=>$ard->paid_date,
                            'updated_at'=>$ard->paid_date,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$toCoa,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$ard->paid_nominal,
                            'created_at'=>$ard->paid_date,
                            'updated_at'=>$ard->paid_date,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                    ];
                    JurnalDetail::insert($insert);
                }

                if (CoaSaleUtil::coaStockAdjustmentFromSaleV2Mobile($data->getMerchant->md_user_id,$data->md_merchant_id,[],$warehouseId,$invMethod,$orderId) == false)
                {
                    return $this->message::getJsonResponse(404,trans('Terjadi kesalahan dalam pencatatan jurnal penjualan dengan kode penjualan'),[$data->code]);
                }
                DB::commit();
                return $this->message::getJsonResponse(200, 'Pembayaran transaksi berhasil', $data);

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            // dd($e);
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);



        }
    }

    public function inquiryQRIS(Request $request,$merchantId)
    {
        try {
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            $isKeep=(is_null($request->is_keep_transaction))?0:$request->is_keep_transaction;
            if($request->is_debet==0)
            {
                if($isKeep==0)
                {
                    if (is_null($request->md_transaction_type_id)) {
                        return $this->message::getJsonResponse(404,'Metode Pembayaran belum dipilih',[]);
                    }
                }
            }
            if (is_null($request->md_sc_shipping_category_id)) {
                return $this->message::getJsonResponse(404,'Metode Pengiriman belum dipilih',[]);
            }
            if(is_null($request->is_with_load_shipping))
            {
                return $this->message::getJsonResponse(404,'Centang beban pengiriman belum dipilih',[]);

            }
            $timestamp=date('Y-m-d H:i:s');
            $purchase = new $this->order;
            $validator = Validator::make($request->all(),$purchase->ruleOfflineOrder);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404,trans('error'),[$error]);
            }
            if(!is_null($request->sc_customer_id))
            {
                $purchase->sc_customer_id=$request->sc_customer_id;
            }
            if($request->is_debet==0)
            {
                if($isKeep==0)
                {
                    $trans = explode('_', $request->md_transaction_type_id);
                }

            }elseif ($request->is_debet==1 && $request->paid_nominal>0) {

                $trans = explode('_', $request->md_transaction_type_id);

            }else{

                $trans=null;
            }
            $code=CodeGenerator::generateSaleOrder($merchantId);
            $purchase->code = $code;
            $purchase->qr_code = '-';
            $purchase->is_debet = $request->is_debet;
            $purchase->is_with_retur = 0;
            $purchase->change_money = $request->change_money;
            $purchase->sc_customer_id = $request->sc_customer_id;
            if($request->md_sc_shipping_category_id==1)
            {
                $purchase->is_with_load_shipping=1;

            }else{
                $purchase->is_with_load_shipping=0;

            }

            $purchase->total = $request->total;
            $purchase->md_merchant_id = $merchantId;
            $purchase->paid_nominal = $request->paid_nominal;
            $purchase->md_sc_transaction_status_id = TransactionStatus::UNPAID;
            $purchase->md_sc_shipping_category_id=$request->md_sc_shipping_category_id;
            $purchase->tax = $request->tax;
            if (!is_null($request->ref_coupon_id)) {
                $isReferral=$request->is_referral;
                $flex=($isReferral==1)?'referral':'kupon';

                $coupon=$this->discount::find($request->ref_coupon_id);
                if($coupon->is_limited==1){
                    if($coupon->max_coupon_use<=0){
                        return $this->message::getJsonResponse(404, 'Kuota '.$flex.' telah habis, tidak dapat digunakan!', []);
                    }else if($coupon->max_coupon_use>0){
                        $coupon->max_coupon_use-=1;
                        $coupon->save();
                    }
                }

                if($request->sc_customer_id){
                    $getCouponUsed=$this->order::where('sc_customer_id',$request->sc_customer_id)
                    ->where("ref_coupon_id", $request->ref_coupon_id)
                    ->where("is_keep_transaction", 0)
                    ->where("is_deleted", 0)
                    ->where("step_type", 0)
                    ->count();

                    if($coupon->max_coupon_use_per_customer>0 && $coupon->max_coupon_use_per_customer<$getCouponUsed){
                        return $this->message::getJsonResponse(404, 'Kuota '.$flex.' untuk pelanggan ini telah habis, tidak dapat digunakan!', []);
                    }
                }

                $purchase->ref_coupon_id = $request->ref_coupon_id;
                $purchase->ref_coupon_code = $request->ref_coupon_code;
                $purchase->is_referral = $request->is_referral;
                $purchase->coupon_value = $coupon;

            }


            if (!is_null($request->ref_discount_id)) {
                $disc_id=[];
                $disc_code=[];
                $discount=$this->discount::find($request->ref_discount_id);

                $disc_id[]=[
                    'discount_id'=>$request->ref_discount_id
                ];
                $disc_code[]=[
                    'discount_code'=>$request->ref_discount_code
                ];

                $purchase->ref_discount_id = json_encode($disc_id);
                $purchase->ref_discount_code = json_encode($disc_code);
                $purchase->discount_value = $discount;
            }

            $purchase->json_amount_promo = ($request->json_amount_promo) ? json_encode($request->json_amount_promo) : null;
            $purchase->discount_primary_id = $request->discount_primary_id;
            $purchase->promo = $request->promo;
            $purchase->promo_percentage = $request->promo_percentage;
            $purchase->tax_percentage = $request->tax_percentage;
            $purchase->shipping_cost=$request->shipping_cost;
            $purchase->timezone = $request->timezone;
            $purchase->created_by = $request->created_by;
            $purchase->sync_id = $merchantId . Uuid::uuid4()->toString();
            if($request->is_debet==0)
            {
                if($isKeep==0)
                {
                    $purchase->coa_trans_id = $trans[1];
                    $purchase->md_transaction_type_id = $trans[0];
                }
            }
            if($isKeep==1)
            {
                $purchase->is_keep_transaction=1;
            }
            $purchase->is_approved_shop=1;

            $purchase->assign_to_user_helper = $request->assign_to_user_helper;
            $purchase->mode_cashier = $request->mode_cashier;
            $purchase->receive_time = $request->receive_time;
            $purchase->pick_up_time = $request->pick_up_time;
            $purchase->cost_other_sale = json_encode($request->cost_other_sale);
            $purchase->total_cost_other_sale = $request->total_cost_other_sale;
            $destinationPath = 'public/uploads/merchant/'.$request->created_by.'/cashiermode/';

            if($request->hasFile('guarantee'))
            {
                $file=$request->file('guarantee');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }
            $purchase->guarantee = $fileName;
            $purchase->created_at=date('Y-m-d H:i:s');
            $purchase->updated_at=date('Y-m-d H:i:s');
            $purchase->save();
            $details = [];
            $detail=$request->details;
            if (empty($detail)) {
                return $this->message::getJsonResponse(404,'Data item pembelian tidak boleh kosong',[]);

            }

            $productId=[];
            $productWithQuantitys=[];
            $productIds="";
            foreach ($detail as $n => $i)
            {
                $productId[]=$i['sc_product_id'];
                $productIds.=($n==0)?$i['sc_product_id']:",".$i["sc_product_id"];
            }

            $products=collect(DB::select("
                select
                  sp.*,
                  sp.inv_id as coa_inv_id,
                  sp.hpp_id as coa_hpp_id,
                  u.name as default_unit_name
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          spmu.id,
                          spmu.sc_product_id,
                          spmu.md_unit_id,
                          spmu.quantity,
                          spmu.price,
                          mu.name
                        FROM
                          sc_product_multi_units spmu
                          join md_units mu on mu.id = spmu.md_unit_id
                        WHERE
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                      ) jd
                  ) AS multi_units
                from
                  sc_products sp
                  join md_units u on u.id = sp.md_unit_id
                where
                  sp.id in ($productIds)
            "));
            $dataInquiry=[];

            foreach ($detail as $key => $item) {

                $product=$products->firstWhere('id',$item['sc_product_id']);

                $isMulti=isset($item['is_multi_unit'])?$item['is_multi_unit']:0;
                $MultiId=isset($item['multi_unit_id'])?$item['multi_unit_id']:null;
                $noteOrder=isset($item['note_order'])?$item['note_order']:"";

                $unit_name="";
                $mQuantity=$item['quantity'];
                $mPrice=$item['price'];
                $sub_total=$item['sub_total'];
                $multi_quantity=$item['quantity'];
                $convertionValue=1;

                if($isMulti==1 && !is_null($MultiId)){
                    $multi_unit=collect(json_decode($product->multi_units));
                    $unit=$multi_unit->firstWhere('id',$MultiId);
                    $unit_name=$unit->name;
                    $multi_quantity=$unit->quantity*$item['quantity'];
                    $mQuantity=$item['quantity'];
                    $mPrice=$item['price']*$item['quantity']/$unit->quantity;
                    $convertionValue=$unit->quantity;
                    $jsonMulti=json_encode($multi_unit);
                }

                if($product->is_with_stock==1) {

                    if ($product->stock < $multi_quantity) {
                        return $this->message::getJsonResponse(404, 'Stok produk tidak mencukupi', [$product->name]);
                    }
                }

                $productWithQuantitys[]=(object)[
                    'id'=>$item['sc_product_id'],
                    'origin_quantity'=>$item['quantity'],
                    'quantity'=>$item['quantity']*$convertionValue,
                    'price'=>$item['quantity']*$item['price'],
                    'product_name'=>$product->name,
                    'unit_name'=>($isMulti==1 && !is_null($MultiId))?$unit_name:$product->default_unit_name,
                    'origin_unit_name'=>'',
                ];

                $discount=null;

                if(!is_null($item['ref_discount_id']) && $item['ref_discount_id']!=0){
                    $discount=$this->discount::join('crm_discount_by_product_details as d','d.discount_by_product_id','crm_discount_by_products.id')
                                ->where('d.sc_product_id',$item['sc_product_id'])
                                ->where('crm_discount_by_products.id',$item['ref_discount_id'])
                                ->first();
                }
                $jsonPromo=(isset($item['json_amount_promo']))?json_encode($item['json_amount_promo']):null;

                $isBonus=isset($item['is_bonus'])?$item['is_bonus']:0;
                $details[]=[
                    'sc_product_id'=>$product->id,
                    'quantity'=>$mQuantity,
                    'multi_quantity'=>$multi_quantity,
                    'is_bonus'=>$isBonus,
                    'is_multi_unit'=>$isMulti,
                    'multi_unit_id'=>$MultiId,
                    'unit_name'=>$unit_name,
                    'ref_discount_id'=>$item['ref_discount_id'],
                    'special_price_id'=>$item['special_price_id'],
                    'sub_total'=>$sub_total,
                    'note_order'=>$noteOrder,
                    'discount_value'=>$discount,
                    'sc_sale_order_id'=>$purchase->id,
                    'profit'=>0,
                    'price'=>$mPrice,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp,
                    'coa_inv_id'=>($product->is_with_stock==0)?null:$product->coa_inv_id,
                    'coa_hpp_id'=>($product->is_with_stock==0)?null:$product->coa_hpp_id,
                    'value_conversation' => ( $isMulti==1 ) ? $convertionValue : 1,
                    'json_multi_unit' => ( $isMulti==1 ) ? $jsonMulti : null,
                    'json_amount_promo' => $jsonPromo
                ];


            }

            if($request->user_helpers){
                $userHelpers=$request->user_helpers;
                $userHelperIds = "";
                foreach($userHelpers as $key => $u){
                    $userHelperIds .= ($key == 0) ? "'".$u['id']."'" : "," . "'".$u['id']."'";
                }

                if(!empty($userHelpers)){
                    $productList="(".$productIds.")";
                    $userHelperList="(".$userHelperIds.")";
                    $checkCommission=CommissionUtil::resultCommission($productList,$productWithQuantitys,$userHelperList,$merchantId,$purchase->total,$purchase->id,$timestamp,$request->timezone);

                    if(!empty($checkCommission)){
                        $purchase->commission_value=json_encode($checkCommission);
                        $purchase->save();
                        DB::table('merchant_commission_lists')->insert($checkCommission);
                    }
                    $purchase->assign_to_user_helper=json_encode($userHelpers);
                    $purchase->save();
                }
            }
            
            if($isReferral==1){
                $commission=$this->commission::find($coupon->commission_id);
                $calculateCommission=CommissionUtil::calculateCommission($commission,$productWithQuantitys,$merchantId,$purchase->total,$purchase->id,$timestamp,$request->timezone);
                if(!empty($calculateCommission)){
                    $purchase->commission_value=json_encode($calculateCommission);
                    $purchase->save();
                    DB::table('merchant_commission_lists')->insert($calculateCommission);

                }

            }

            array_push($dataInquiry,[
                "name"=> $products->first()->name,
                "sku"=> (is_null( $products->first()->code))?'-':$products->first()->code,
                "qty"=> 1,
                "unitPrice"=> $purchase->total,
                "desc"=> $products->first()->name
            ]);


            SaleOrderDetail::insert($details);

            $token=WinpayUtil::getToken();
            if($token==false)
            {
                return $this->message::getJsonResponse(404,'Terjadi kesalahan dalam proses generate QRIS',[]);
            }
            $inquiry=WinpayUtil::inquiryQRIS($token,$purchase->code,$dataInquiry,$purchase->total,$purchase->getMerchant);

            if($inquiry==false)
            {
                return $this->message::getJsonResponse(404,'Terjadi kesalahan dalam proses menampilkan data QRIS',[]);

            }
            $purchase->expired_time=Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
            $purchase->qris_response=json_encode($inquiry);
            $purchase->save();
            DB::commit();

            $resultJson=[
                "sale_id"=>$purchase->id,
                "sale_code"=>$purchase->code,
                'md_sc_transaction_status_id'=>$purchase->md_sc_transaction_status_id
            ];
            return $this->message::getJsonResponse(200,'Silahkan melakukan scan QRIS untuk melanjutkan pembayaran',$resultJson);

        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, $e->getMessage(), []);


        }
    }

    public function payQRISExpired($id,Request  $request)
    {
        try {

            DB::beginTransaction();
            $sale=SaleOrder::find($id);
            $date = new \DateTime($sale->created_at);
            if(AccUtil::checkClosingJournal($sale->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            if(is_null($sale))
            {
                return $this->message::getJsonResponse(404, trans('message.404'), []);

            }
            if (is_null($request->md_transaction_type_id)) {
                return $this->message::getJsonResponse(404,'Metode Pembayaran belum dipilih',[]);
            }


            $warehouseId=$request->inv_warehouse_id;
            if(is_null($warehouseId))
            {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }
            $invMethod=$request->md_inventory_method_id;

            if(is_null($invMethod))
            {
                return $this->message::getJsonResponse(404, 'Parameter metode persediaan belum terisi', []);

            }


            $trans = explode('_', $request->md_transaction_type_id);
            $sale->coa_trans_id = $trans[1];
            $sale->md_transaction_type_id = $trans[0];
            $dataInquiry=[];
            $timestamp=date('Y-m-d H:i:s');
            $stockOut=[];
            $saleMapping=[];

            $dataDetail=collect(DB::select("select sp.*,ssod.* from sc_sale_order_details ssod
                join sc_products sp
                on ssod.sc_product_id=sp.id
                where ssod.is_deleted=0 and ssod.sc_sale_order_id=$id
            "));

            $productCalculate=[];
            $updateStockProduct="";
            if($trans[0]==4)
            {
                array_push($dataInquiry,[
                    "name"=> $dataDetail->first()->name,
                    "sku"=> $dataDetail->first()->code,
                    "qty"=> 1,
                    "unitPrice"=> $sale->total,
                    "desc"=> $dataDetail->first()->name
                ]);
            }
            $productTemporary=[];
            $sumCost=0;
            foreach ($dataDetail as $key => $product) {
                if($product->is_with_stock==1) {

                    if ($product->stock < $product->quantity) {
                        return $this->message::getJsonResponse(404, 'Stok produk tidak mencukupi', []);
                    }
                }

                if($trans[0]!=4)
                {
                    if($sale->is_keep_transaction==0)
                    {
                        if($product->is_with_stock==1)
                        {

                            array_push($productTemporary,[
                                'id'=>$product->id,
                                'name'=>$product->name,
                                'code'=>$product->code,
                                'stock'=>$product->stock,
                                'selling_price'=>$product->price/$product->value_conversation,
                                'purchase_price'=>$product->purchase_price,
                                'quantity'=>$product->quantity*$product->value_conversation,
                                'is_from_package'=>0
                            ]);

                        }
                        if( $product->md_sc_product_type_id==4){
                            if($product->is_package_subs==1){
                                if($product->quantity>$product->limit_selling){
                                    return $this->message::getJsonResponse(404, 'Penjualan produk '.$product->name.' telah melebihi batas!', []);
                                }
                                
                                $calculatePackageSubs=ProductUtil::calculatePackageSubs($product->id,$product->quantity,$product->price,$product->sc_sale_order_id);
                                // dd($calculatePackageSubs);
                                if($calculatePackageSubs==false){
                                    return $this->message::getJsonResponse(404, 'Terjadi kesalahan ketika penambahan langganan produk', []);
                                }
                            }else{
                                $productChild= Material::join('sc_products as p','p.id','child_sc_product_id')
                                ->where('parent_sc_product_id', $product->id)
                                ->where('sc_product_raw_materials.is_deleted',0)
                                ->get();
                                foreach ($productChild as $p => $c) {
                                    array_push($productTemporary, [
                                        'id' => $c->child_sc_product_id,
                                        'name' => $c->name,
                                        'code' => $c->code,
                                        'stock' => $c->stock,
                                        'selling_price' => $c->selling_price,
                                        'purchase_price' => $c->purchase_price,
                                        'quantity' => $product->quantity*$c->quantity,
                                        'is_from_package'=>1
                                    ]);
                                }
    
                                if(!is_null($product->cost_other)){
                                    foreach(json_decode($product->cost_other) as $sum){
                                        $sumCost+=$sum->amount;
                                    }
                                }
                            }


                        }
                    }

                }
            }
            $sale->cost_other=$sumCost;
            $sale->save();
            if($trans[0]!=4){
                if(!empty($productTemporary))
                {
                    $key1=0;
                    foreach (collect($productTemporary)->groupBy('id') as $item)
                    {
                        $quantity=collect($item)->reduce(function($i, $k) {
                            return $i + $k['quantity'];
                        }, 0);

                        array_push($productCalculate,[
                            'id'=>$item->first()['id'],
                            'amount'=>$quantity,
                        ]);

                        $stock=$item->first()['stock'];

                        $stockP=$stock-$quantity;
                        $updateStockProduct.=($key1==0)?"(".$item->first()['id'].",".$stockP.")":",(".$item->first()['id'].",".$stockP.")";
                        $key1++;

                        $stockOut[]=[
                            'sync_id' => $item->first()['id'] . Uuid::uuid4()->toString(),
                            'sc_product_id' => $item->first()['id'],
                            'total' => $quantity,
                            'inv_warehouse_id' => $warehouseId,
                            'record_stock' => $stockP,
                            'created_by' => $request->created_by,
                            'selling_price' => $item->first()['selling_price'],
                            'purchase_price' => $item->first()['purchase_price'],
                            'residual_stock' => $quantity,
                            'type' => StockInventory::OUT,
                            'transaction_action' => 'Pengurangan Stok ' . $item->first()['name'] . ' Dari Penjualan Dengan Code ' . $sale->code,
                            'stockable_type'=>'App\Models\SennaToko\SaleOrder',
                            'stockable_id'=>$sale->id,
                            'created_at'=>$timestamp,
                            'updated_at'=>$timestamp
                        ];
                    }
                }

                $updateStockInv="";

                if(!empty($productCalculate))
                {
                    $calculateInventory=InventoryUtilV2::inventoryCodev2($invMethod,$warehouseId,$productCalculate,'minus');
                    if($calculateInventory==false)
                    {
                        return $this->message::getJsonResponse(404,'Terjadi kesalahan saat pengurangan stok dipersediaan',[]);
                    }
                    foreach ($calculateInventory as $key => $n)
                    {
                        $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                        $stockInvId[]=$n['id'];
                        $saleMapping[]=[
                            'sc_sale_order_id'=>$sale->id,
                            'sc_product_id'=>$n['sc_product_id'],
                            'sc_stock_inventory_id'=>$n['id'],
                            'amount'=>$n['amount'],
                            'purchase_price'=>$n['purchase'],
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                }


                if($updateStockInv!=""){
                    DB::statement("
                update sc_stock_inventories as t set
                        residual_stock = c.column_a
                    from (values
                        $updateStockInv
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                }

                if($updateStockProduct!=""){
                    DB::statement("
                update sc_products as t set
                        stock = c.column_a
                    from (values
                        $updateStockProduct
                    ) as c(column_b, column_a)
                    where c.column_b = t.id;
                ");
                }

                DB::table('sc_stock_sale_mappings')->insert($saleMapping);
                DB::table('sc_stock_inventories')->insert($stockOut);
            }

            if($trans[0]==4){

                $token=WinpayUtil::getToken();
                if($token==false)
                {
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan dalam proses generate QRIS',[]);
                }
                $inquiry=WinpayUtil::inquiryQRIS($token,$sale->code,$dataInquiry,$sale->total,$sale->getMerchant);

                if($inquiry==false)
                {
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan dalam proses menampilkan data QRIS',[]);

                }

                $sale->expired_time=Carbon::now()->addDays(3)->format('Y-m-d H:i:s');
                $sale->qris_response=json_encode($inquiry);
                $sale->save();
                DB::commit();
                $resultJson=[
                    "sale"=>$sale,
                    'qris'=>$inquiry,
                ];
                return $this->message::getJsonResponse(200,'Silahkan melakukan scan QRIS untuk melanjutkan pembayaran',$resultJson);

            }else{
                $sale->md_sc_transaction_status_id =TransactionStatus::PAID;
                $sale->is_keep_transaction=0;
                $sale->change_money=$request->change_nominal;
                $sale->paid_nominal=$request->paid_nominal;
                $sale->save();

                // $coaJson=collect($coaSale);
                // $coaInv=collect($coaInvSale);

                $detail=collect(DB::select("
                    select
                      ssod.id,
                      sp.is_with_stock,
                      ssod.coa_inv_id as coa_inv_detail,
                      sp.inv_id as coa_inv_id,
                      sp.hpp_id as coa_hpp_id
                    from
                      sc_sale_order_details ssod
                      join sc_products sp on ssod.sc_product_id = sp.id
                    where
                      ssod.is_deleted = 0
                      and ssod.sc_sale_order_id = $id
                "));

                $updateDetail="";
                foreach ($detail as $key =>$item)
                {
                    if(is_null($item->coa_inv_id_detail))
                    {
                        $updateDetail.=($key==0)?"(".$item->id.",".$item->coa_inv_id.",".$item->coa_hpp_id.")":",(".$item->id.",".$item->coa_inv_id.",".$item->coa_hpp_id.")";
                    }
                }

                if($updateDetail!="")
                {
                    DB::statement("
                update sc_sale_order_details as t set
                        coa_inv_id = c.column_a,
                        coa_hpp_id=c.column_c
                    from (values
                        $updateDetail
                    ) as c(column_b, column_a,column_c)
                    where c.column_b = t.id;
                ");
                }

                if (CoaSaleUtil::coaStockAdjustmentFromSaleV2Mobile($sale->getMerchant->md_user_id,$sale->md_merchant_id,[],$warehouseId,$invMethod,$sale->id) == false)
                {
                    return $this->message::getJsonResponse(404,'Terjadi kesalahan dalam pencatatan jurnal penjualan dengan kode penjualan',[$sale->code]);
                }

                DB::commit();
                return $this->message::getJsonResponse(200,'Pembayaran berhasil disimpan',$sale);

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, $e->getMessage(), []);


        }
    }

    public function voidTransaction($orderId)
    {
        try{
            DB::beginTransaction();
            $data=SaleOrder::find($orderId);
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }
            foreach (StockSaleMapping::where('sc_sale_order_id',$data->id)->get() as $stockMap)
            {
                $totalRetur=0;
                foreach ($data->getRetur->where('reason_id','!=',1)->where('is_deleted',0) as $r)
                {
                    foreach ($r->getDetail->where('sc_product_id',$stockMap->sc_product_id) as $d)
                    {
                        $totalRetur+=$d->quantity;
                    }
                }
                $stockInv=StockInventory::find($stockMap->sc_stock_inventory_id);
                $stockInv->residual_stock+=$stockMap->amount-$totalRetur;
                $stockInv->save();
                $product=Product::find($stockMap->sc_product_id);
                $product->stock+=$stockMap->amount-$totalRetur;
                $product->save();
            }
            $data->is_deleted=1;
            $data->save();
            Jurnal::where(['ref_code'=>$data->code,
                'external_ref_id'=>$data->id
            ])
                ->update(['is_deleted'=>1]);

            StockInventory::where('stockable_type','App\Models\SennaToko\SaleOrder')
                ->where('stockable_id',$data->id)
                ->update(['is_deleted'=>1]);

            foreach ($data->getRetur as $retur)
            {
                $retur->is_deleted=1;
                $retur->save();

                StockInventory::where('stockable_type','App\Models\SennaToko\ReturSaleOrder')
                    ->where('stockable_id',$retur->id)
                    ->update(['is_deleted'=>1]);

                Jurnal::where(['ref_code'=>$retur->code,
                    'external_ref_id'=>$retur->id
                ])
                    ->update(['is_deleted'=>1]);
            }
            if($data->is_debet==1)
            {
                $data->ar->is_deleted=1;
                $data->ar->save();
                foreach ($data->ar->getDetail as $ar)
                {
                    $ar->is_deleted=1;
                    $ar->save();
                    Jurnal::where(['ref_code'=>$ar->ar_detail_code,
                        'external_ref_id'=>$ar->id
                    ])
                        ->update(['is_deleted'=>1]);
                }
            }
            DB::commit();
            return $this->message::getJsonResponse(200, 'Data transaksi penjualan berhasil dihapus,data penjualan yang terhapus tetap bisa terlihat di backoffice', []);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);


        }
    }

    public function cancelTransaction($orderId)
    {
        try{
            DB::beginTransaction();
            $data=SaleOrder::find($orderId);
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }
            foreach (StockSaleMapping::where('sc_sale_order_id',$data->id)->get() as $stockMap)
            {
                $totalRetur=0;
                foreach ($data->getRetur->where('reason_id','!=',1)->where('is_deleted',0) as $r)
                {
                    foreach ($r->getDetail->where('sc_product_id',$stockMap->sc_product_id) as $d)
                    {
                        $totalRetur+=$d->quantity;
                    }
                }
                $stockInv=StockInventory::find($stockMap->sc_stock_inventory_id);
                $stockInv->residual_stock+=$stockMap->amount-$totalRetur;
                $stockInv->save();
                $product=Product::find($stockMap->sc_product_id);
                $product->stock+=$stockMap->amount-$totalRetur;
                $product->save();
            }
            $data->md_sc_transaction_status_id=3;
            $data->save();
            Jurnal::where(['ref_code'=>$data->code,
                'external_ref_id'=>$data->id
            ])
                ->update(['is_deleted'=>1]);

            StockInventory::where('stockable_type','App\Models\SennaToko\SaleOrder')
                ->where('stockable_id',$data->id)
                ->update(['is_deleted'=>1]);

            foreach ($data->getRetur as $retur)
            {
                $retur->is_deleted=1;
                $retur->save();

                StockInventory::where('stockable_type','App\Models\SennaToko\ReturSaleOrder')
                    ->where('stockable_id',$retur->id)
                    ->update(['is_deleted'=>1]);

                Jurnal::where(['ref_code'=>$retur->code,
                    'external_ref_id'=>$retur->id
                ])
                    ->update(['is_deleted'=>1]);
            }
            if($data->is_debet==1)
            {
                $data->ar->is_deleted=1;
                $data->ar->save();
                foreach ($data->ar->getDetail as $ar)
                {
                    $ar->is_deleted=1;
                    $ar->save();
                    Jurnal::where(['ref_code'=>$ar->ar_detail_code,
                        'external_ref_id'=>$ar->id
                    ])
                        ->update(['is_deleted'=>1]);
                }
            }
            DB::commit();
            return $this->message::getJsonResponse(200, 'Data transaksi penjualan berhasil dicancel', []);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);


        }
    }

    public function refundTransaction($orderId)
    {
        try{
            DB::beginTransaction();
            $data=SaleOrder::find($orderId);
            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }
            foreach (StockSaleMapping::where('sc_sale_order_id',$data->id)->get() as $stockMap)
            {
                $totalRetur=0;
                foreach ($data->getRetur->where('reason_id','!=',1)->where('is_deleted',0) as $r)
                {
                    foreach ($r->getDetail->where('sc_product_id',$stockMap->sc_product_id) as $d)
                    {
                        $totalRetur+=$d->quantity;
                    }
                }
                $stockInv=StockInventory::find($stockMap->sc_stock_inventory_id);
                $stockInv->residual_stock+=$stockMap->amount-$totalRetur;
                $stockInv->save();
                $product=Product::find($stockMap->sc_product_id);
                $product->stock+=$stockMap->amount-$totalRetur;
                $product->save();
            }
            $data->md_sc_transaction_status_id=5;
            $data->save();
            Jurnal::where(['ref_code'=>$data->code,
                'external_ref_id'=>$data->id
            ])
                ->update(['is_deleted'=>1]);

            StockInventory::where('stockable_type','App\Models\SennaToko\SaleOrder')
                ->where('stockable_id',$data->id)
                ->update(['is_deleted'=>1]);

            foreach ($data->getRetur as $retur)
            {
                $retur->is_deleted=1;
                $retur->save();

                StockInventory::where('stockable_type','App\Models\SennaToko\ReturSaleOrder')
                    ->where('stockable_id',$retur->id)
                    ->update(['is_deleted'=>1]);

                Jurnal::where(['ref_code'=>$retur->code,
                    'external_ref_id'=>$retur->id
                ])
                    ->update(['is_deleted'=>1]);
            }
            if($data->is_debet==1)
            {
                $data->ar->is_deleted=1;
                $data->ar->save();
                foreach ($data->ar->getDetail as $ar)
                {
                    $ar->is_deleted=1;
                    $ar->save();
                    Jurnal::where(['ref_code'=>$ar->ar_detail_code,
                        'external_ref_id'=>$ar->id
                    ])
                        ->update(['is_deleted'=>1]);
                }
            }
            DB::commit();
            return $this->message::getJsonResponse(200, 'Data transaksi penjualan berhasil direfund', []);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);


        }
    }

    public function inquiryQRISFromStore(Request  $request)
    {
        try{
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            $merchant=Merchant::find($request->md_merchant_id);
            if(!is_null($request->customer_name))
            {
                $customer=Customer::where('name',$request->customer_name)
                    ->where('md_user_id',$merchant->md_user_id)->first();
                if(is_null($customer))
                {
                    $customer=new Customer();
                    $customer->name=$request->customer_name;
                    $customer->md_user_id=$merchant->md_user_id;
                    $customer->save();

                    $custId=$customer->id;
                }else{
                    $custId=NULL;
                }
            }else{
                $custId=NULL;
            }
            $purchase=new SaleOrder();
            $code=CodeGenerator::generateSaleOrder($merchant->id);
            $purchase->code = $code;
            $purchase->qr_code = '-';
            $purchase->change_money = $request->change_money;
            $purchase->sc_customer_id=$custId;
            $purchase->total = $request->total;
            $purchase->queue_code=$request->queue_code;
            $purchase->md_merchant_id = $request->md_merchant_id;
            $purchase->paid_nominal = $request->paid_nominal;
            $purchase->md_sc_transaction_status_id = 1;
            $purchase->md_sc_shipping_category_id=3;
            $purchase->tax = $request->tax;
            $purchase->promo = $request->promo;
            $purchase->promo_percentage = $request->promo_percentage;
            $purchase->tax_percentage = $request->tax_percentage;
            $purchase->timezone = $request->timezone;
            $purchase->created_by = $merchant->md_user_id;
            $purchase->sync_id = $request->unique_key;
            $purchase->is_approved_shop=1;
            $purchase->created_at=date('Y-m-d H:i:s');
            $purchase->updated_at=date('Y-m-d H:i:s');
            $purchase->is_store=1;
            if($request->is_qris==0)
            {
                $purchase->is_keep_transaction=1;
            }
            $purchase->save();
            $details = [];
            $detail=json_decode($request->details);
            if (empty($detail)) {
                return $this->message::getJsonResponse(404,'Data item penjualan tidak boleh kosong',[]);
            }
            $timestamp=date('Y-m-d H:i:s');
            $dataInquiry=[];
            foreach ($detail as $key => $item) {
                $product = $this->product::find($item->sc_product_id);
                $details[]=[
                    'sc_product_id'=>$item->sc_product_id,
                    'quantity'=>$item->quantity,
                    'is_bonus'=>0,
                    'sub_total'=>$item->quantity*$item->price,
                    'sc_sale_order_id'=>$purchase->id,
                    'profit'=>0,
                    'price'=>$item->price,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ];

                $dataInquiry[]=[
                    "name"=> $product->name,
                    "sku"=> $product->code,
                    "qty"=> $item->quantity,
                    "unitPrice"=>$item->price,
                    "desc"=> $product->name
                ];

            }
            SaleOrderDetail::insert($details);
            DB::commit();
            return $this->message::getJsonResponse(200,'Pemesanan berhasil disimpan',[
                'ref_code'=>$purchase->code,
                'data'=>$purchase,
                'amount'=>$purchase->total,
                'json'=>$dataInquiry,
                'merchant_id'=>$merchant->id,
                'unique_key'=>$merchant->unique_key
            ]);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, $e->getMessage(), []);

        }
    }

}

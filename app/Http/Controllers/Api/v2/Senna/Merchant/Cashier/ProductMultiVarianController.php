<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\MultiVarian;
use App\Models\MasterData\Varian;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\StockInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class ProductMultiVarianController extends Controller
{
    protected $multivarian;
    protected $message;

    public function __construct()
    {
        $this->multivarian=MultiVarian::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/product/multi-varian')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\ProductMultiVarianController@create');
                Route::get('/all/{productId}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiVarianController@getAll');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiVarianController@delete');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiVarianController@update');
                Route::get('/one/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductMultiVarianController@getOne');

            });
    }

    public function getAll($productId)
    {
        try {
            $data=$this->multivarian::
            select([
                'sc_product_multi_varians.*',
                'p.name as product_name'
            ])  ->join('sc_products as p','p.id','sc_product_multi_varians.sc_product_id')
                ->join('md_multi_varians as u','u.id','sc_product_multi_varians.md_multi_varian_id')
                ->where('sc_product_multi_varians.is_deleted',0)
                ->where('p.id',$productId)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {

            $data=$this->multivarian::find($id);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


    public function create(Request  $request)
    {
        try {

            
            $multiVarian=$request->multi_varian;


            if(!empty($multiVarian))
            {

                foreach ($multiVarian as $key =>$item)
                {
                    // $checkMulti=$this->multivarian::where('name',$item['varian_name'])->where('sc_product_multi_varians.is_deleted',0)->get();
                    
                    // if(count($checkMulti)>0){
                    //     return $this->message::getJsonResponse(404,trans('Multi varian sudah ada!'),[]);
                    // }

                    $product=Product::find($item['sc_product_id']);
                    $warehouse=parent::getGlobalWarehouse($product->getUser->getMerchant->id);
                    
                    $data = new $this->multivarian;
    
     
                        $multi_varian = [];

                        foreach ($item['multi_varian_value'] as $key => $i) {
                            
                            $multi_varian[] = [
                                            'id' => $i['id'],
                                            'varian_type' => $i['varian_type'],
                                            'varian_name' => $i['varian_name']
                                              ];
                            if($key==0){
                                $label=$i['varian_type']." ".$i['varian_name'];
                            }else{
                                $label.=" ".$i['varian_type']." ".$i['varian_name'];
                            }

                        }
                        $data->multi_varian = json_encode($multi_varian);
                        //dd($data->multi_varian);
                    $productName=$product->name." ".$label;
                    $data->label=$label;
                    $data->selling_price=$item['selling_price'];
                    $data->purchase_price=$item['purchase_price'];
                    $data->sc_product_id=$item['sc_product_id'];
                    $data->stock=0;
                    $data->created_at=date('Y-m-d H:i:s');
                    $data->updated_at=date('Y-m-d H:i:s');
                    $data->save();

                    // $product->multi_varian=;
                    
                    $product->stock()->create([
                        'sc_product_id'=>$product->id,
                        'total'=>$item['stock'],
                        'selling_price'=>$item['selling_price'],
                        'purchase_price'=>$item['purchase_price'],
                        'created_by'=>$product->md_user_id,
                        'record_stock'=>$item['stock'],
                        'type'=>StockInventory::INIT,
                        'inv_warehouse_id'=>$warehouse->id,
                        'transaction_action'=>'Stok Awal '.$productName,
                        'product_multi_varian_id'=>$data->id
                    ]);
                    $product->stock()->create([
                        'sc_product_id'=>$product->id,
                        'total'=>$item['stock'],
                        'selling_price'=>$item['selling_price'],
                        'purchase_price'=>$item['purchase_price'],
                        'created_by'=>$product->md_user_id,
                        'record_stock'=>$item['stock'],
                        'type'=>StockInventory::IN,
                        'residual_stock'=>$item['stock'],
                        'inv_warehouse_id'=>$warehouse->id,
                        'transaction_action'=>'Stok Masuk '.$productName,
                        'product_multi_varian_id'=>$data->id
                    ]);
                }
  
                return $this->message::getJsonResponse(200,trans('message.200'),[$data]);
            }else{
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function delete($id)
    {
        try {
            $data =$this->multivarian::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $data->is_deleted=1;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function update(Request  $request,$id)
    {
        try {
            $data = $this->multivarian::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }            

            $data->sc_product_id=$request->sc_product_id;
            $data->stock=$request->stock;
            $data->selling_price=$request->selling_price;
            $data->purchase_price=$request->purchase_price;
            $data->is_deleted=0;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }



}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\Acc;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\MerchantShift;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class ProfitLostController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/merchant/acc/report/profit-lost')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('report/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\Acc\ProfitLostController@getReport');

            });
    }


    public function getReport(Request  $request,$merchantId)
    {

        try{
            $startDate=(is_null($request->start_date))?Carbon::now()->startOfMonth()->toDateString():$request->start_date;
            $endDate=(is_null($request->end_date))?Carbon::now()->endOfMonth()->toDateString():$request->end_date;

            $data=collect(DB::select("
            select
              acd.id,
              acd.code,
              acd.name,
              acd.type_coa,
              case WHEN acc.code in('4.1.00', '4.2.00', '4.3.00') THEN 'pendapatan' WHEN acc.code = '5.0.00' THEN 'hpp' ELSE 'beban' END AS profit_type,
              case WHEN acc.code = '4.1.00' THEN 'pendapatan_usaha' WHEN acc.code = '4.2.00' THEN 'pendapatan_diluar_usaha' WHEN acc.code = '4.3.00' THEN 'pendapatan_lain-lain' WHEN acc.code = '5.0.00' THEN 'harga_pokok_penjualan' WHEN acc.code = '6.1.01' THEN 'beban_operasional' WHEN acc.code = '6.1.01' THEN 'beban_administrasi_&_umum' ELSE 'beban_lain-lain' END AS flag,
              CASE WHEN acc.code in ('4.1.00', '4.2.00', '4.3.00')
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_revenue,
              CASE WHEN acc.code in('4.1.00', '4.2.00', '4.3.00')
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_revenue_min,
              CASE WHEN acc.code = '5.0.00'
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_hpp,
              CASE WHEN acc.code = '5.0.00'
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_hpp_min,
              CASE WHEN acc.code in ('6.1.01', '6.1.02', '6.1.03')
              AND j.coa_type = 'Debit' THEN sum(j.amount) ELSE 0 END AS amount_cost,
              CASE WHEN acc.code in('6.1.01', '6.1.02', '6.1.03')
              AND j.coa_type = 'Kredit' THEN sum(j.amount) ELSE 0 END AS amount_cost_min
            from
              acc_coa_details acd
              join acc_coa_categories acc on acd.acc_coa_category_id = acc.id
              left join (
                select
                  ajd.acc_coa_detail_id,
                  ajd.amount,
                  ajd.coa_type
                from
                  acc_jurnal_details ajd
                  join acc_jurnals aj on ajd.acc_jurnal_id = aj.id
                WHERE
                  aj.is_deleted in (0, 2)
                  and aj.trans_time :: date  between '".$startDate."' and '".$endDate."' and aj.md_merchant_id=$merchantId
              ) j on j.acc_coa_detail_id = acd.id
            where
              acc.md_merchant_id = $merchantId
              and acc.code in (
                '4.1.00', '4.2.00', '4.3.00', '5.0.00',
                '6.1.01', '6.1.02', '6.1.03'
              )
              and acd.is_deleted != 1
              and acd.code not in ('4.3.00.02','4.3.00.03')
              group by acd.id,acc.code,j.coa_type
              order by acc.code asc

        "));
            $result=[];

            foreach ($data->groupBy('profit_type') as $key =>$item)
            {
                $child=[];

                foreach ($item->groupBy('flag') as $i =>$j)
                {
                    if($key=='hpp')
                    {
                        $amount=$j->sum('amount_hpp')-$j->sum('amount_hpp_min');
                    }elseif ($key=='pendapatan')
                    {
                        $amount=$j->sum('amount_revenue')-$j->sum('amount_revenue_min');
                    }else{
                        $amount=$j->sum('amount_cost')-$j->sum('amount_cost_min');

                    }
                    $child[]=(object)[
                        'name'=>ucwords(str_replace('_',' ',$i)),
                        'amount'=>$amount
                    ];
                }



                $result[]=(object)[
                    'key'=>$key,
                    'name'=>($key=='hpp')?'Harga Pokok Penjualan':ucwords($key),
                    'child'=>collect($child),
                    'total'=>collect($child)->sum('amount')
                ];
            }
            $params=[
                'startDate'=>$startDate,
                'endDate'=>$endDate,
                'data'=>$result
            ];
            return $this->message::getJsonResponse(200,'Data report tersedia',$params);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }
}

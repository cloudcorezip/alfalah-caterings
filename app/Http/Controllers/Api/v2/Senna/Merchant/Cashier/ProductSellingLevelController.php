<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\ProductSellingLevel;
use App\Utils\Merchant\ProductUtil;
use App\Models\SennaToko\Product;
use App\Utils\Account\AccountUtil;
use App\Models\MasterData\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class ProductSellingLevelController extends Controller
{

    protected $product;
    protected $productSellingLevel;
    protected $merchant;
    protected $message;

    public function __construct()
    {        
        $this->product=Product::class;
        $this->productSellingLevel=ProductSellingLevel::class;
        $this->merchant=Merchant::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/product/selling-level')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\ProductSellingLevelController@create');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductSellingLevelController@update');
                Route::get('/all/{productId}', 'Api\v2\Senna\Merchant\Cashier\ProductSellingLevelController@getAll');
                Route::get('/one/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductSellingLevelController@getOne');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductSellingLevelController@delete');
                Route::post('/delete-v2/{id}', 'Api\v2\Senna\Merchant\Cashier\ProductSellingLevelController@deleteV2');

            });
    }

    public function getAll($productId)
    {
        try {
            $data=$this->productSellingLevel::where('sc_product_id',$productId)
                ->where('is_deleted',0)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {

            $data=$this->productSellingLevel::find($id);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function create(Request $request)
    {
        try {
            DB::beginTransaction();
            $timestamp=date('Y-m-d H:i:s');
            $priceType=$request->price_type;
            //dd($priceType);
            
            if(!empty($priceType))
            {
                $insert=[];
                $idS=[];
                foreach ($priceType as $key =>$item)
                {
                    $checkPrice=$this->productSellingLevel::where('level_name',$item['level_name'])
                    ->where('sc_product_id',$item['sc_product_id'])
                    ->where('is_deleted',0)
                    ->get();

                    if(count($checkPrice)>0){
                        return $this->message::getJsonResponse(404,trans('Tipe harga sudah ada!'),[]);
                    }

                    $data=new $this->productSellingLevel();
                    $data->level_name=$item['level_name'];
                    $data->selling_price=$item['selling_price'];
                    $data->sc_product_id=$item['sc_product_id'];
                    $data->created_at=$timestamp;
                    $data->updated_at=$timestamp;
                    $data->save();

                    $idS[]=$data->id;
                }

                $productId=collect($priceType)->first()['sc_product_id'];
                $product=$this->product::find($productId);
                $jsonIdProduct=json_decode($product->assign_to_product);
                $jsonIdBranch=json_decode($product->assign_to_branch);
                
                if(!is_null($product->assign_to_branch) || $jsonIdBranch!=[]){
                    foreach($jsonIdBranch as $i){
                        $merchant=$this->merchant::find($i);
                        $duplicate=ProductUtil::duplicateProductMobile(null,$idS,[],$merchant);
                        if($duplicate==false){
                            return $this->message::getJsonResponse(404,'Terjadi kesalahan saat duplikasi ke cabang!',[]);
                        }
                    }
                }
                DB::commit(); 
                return $this->message::getJsonResponse(200,trans('message.200'),[]);
            }else{
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }

    }

    public function update($id,Request $request)
    {
        try {
            DB::beginTransaction();
            $data =$this->productSellingLevel::find($id);
            if(!is_null($data->getProduct->created_by_merchant) && !is_null($request->md_merchant_id)){
                if($data->getProduct->created_by_merchant!=$request->md_merchant_id){
                    return $this->message::getJsonResponse(404,'Cabang tidak diperkenankan mengubah produk yang dibuat oleh pusat',[]);
                }
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $validator = Validator::make($request->all(), $data->rule);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $data->level_name=$request->level_name;
            $data->selling_price=$request->selling_price;
            $data->sc_product_id=$request->sc_product_id;
            $data->save();

            $jsonIdBranch=json_decode($data->assign_to);
            if(!is_null($data->assign_to) || json_decode($data->assign_to)!=[]){
                $this->productSellingLevel::whereIn('id', $jsonIdBranch)->update(['level_name' => $request->level_name,'selling_price' => $request->selling_price]);
            }
            DB::commit(); 
            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

    public function delete($id)
    {
        try {
            $data =$this->productSellingLevel::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $data->is_deleted=1;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function deleteV2($id)
    {
        try {
            DB::beginTransaction();
            $data =$this->productSellingLevel::find($id);
            if(!is_null($data->getProduct->created_by_merchant) && !is_null($request->md_merchant_id)){
                if($data->getProduct->created_by_merchant!=$request->md_merchant_id){
                    return $this->message::getJsonResponse(404,'Cabang tidak diperkenankan menghapus produk yang dibuat oleh pusat',[]);
                }
            }
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $data->is_deleted=1;
            $data->save();

            $jsonIdBranch=json_decode($data->assign_to);
            if(!is_null($data->assign_to) || json_decode($data->assign_to)!=[]){
                $this->productSellingLevel::whereIn('id', $jsonIdBranch)->update(['is_deleted' => 1]);
            }
            DB::commit(); 
            return $this->message::getJsonResponse(200,trans('message.202'),$data);

        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

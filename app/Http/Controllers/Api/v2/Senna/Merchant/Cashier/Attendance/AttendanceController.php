<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\Attendance;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Attendance\MerchantStaffAttendance;
use App\Models\MasterData\Attendance\MerchantStaffPermitSubmission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AttendanceController extends Controller
{

    protected $attendance;
    protected $message;

    public function __construct()
    {
        $this->attendance = MerchantStaffAttendance::class;
        $this->message = Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/attendance')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function () {
                Route::get('/active/{staffUserId}', 'Api\v2\Senna\Merchant\Cashier\Attendance\AttendanceController@active');
                Route::post('/start', 'Api\v2\Senna\Merchant\Cashier\Attendance\AttendanceController@start');
                Route::post('/end/{id}', 'Api\v2\Senna\Merchant\Cashier\Attendance\AttendanceController@end');
                Route::post('/list/{staffUserId}', 'Api\v2\Senna\Merchant\Cashier\Attendance\AttendanceController@listAttendance');

            });
    }


    public function active($staffUserId)
    {
        try{
            $data=$this->attendance::
            select([
              'md_merchant_staff_attendances.*',
              'u.fullname',
              'r.name as role_name'
            ])
            ->where([
                'md_merchant_staff_attendances.md_staff_user_id'=>$staffUserId,
            ])->whereDate('md_merchant_staff_attendances.created_at',date('Y-m-d'))
                ->join('md_users as u','u.id','md_merchant_staff_attendances.md_staff_user_id')
                ->join('md_roles as r','r.id','u.md_role_id')
                ->orderBy('id','desc')
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function start(Request $request)
    {
        try {

            $data = new $this->attendance;

            $validator = Validator::make($request->all(), $data->startAttendance);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $check=$this->attendance::where('md_staff_user_id',$request->md_staff_user_id)
                ->whereDate('start_work',date('Y-m-d'))
                ->first();
            if($check)
            {
                return $this->message::getJsonResponse(404,'Kamu sudah melakukan absensi hari ini', []);

            }
            $checkPermit=MerchantStaffPermitSubmission::where('md_staff_user_id',$request->md_staff_user_id)
                ->whereDate('created_at',date('Y-m-d'))
                ->where('is_canceled',0)
                ->first();
            if($checkPermit)
            {
                return $this->message::getJsonResponse(404,'Kamu sudah melakukan permintaan izin hari ini', []);
            }
            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/attendance/'.date('YmdHis').'/';

            if($request->hasFile('start_work_file'))
            {
                $file=$request->file('start_work_file');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                $fileName=null;
            }
            $data->md_merchant_id=$request->md_merchant_id;
            $data->md_staff_user_id=$request->md_staff_user_id;
            $data->start_work=$request->start_work;
            $data->start_longitude=$request->start_longitude;
            $data->start_latitude=$request->start_latitude;
            $data->start_work_file=$fileName;
            $data->type_of_attendance='H';
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        } catch (\Exception $e) {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }


    public function end(Request $request, $id)
    {
        try {
            $data=$this->attendance::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $validator = Validator::make($request->all(), $data->endAttendance);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $destinationPath = 'public/uploads/merchant/'.$data->md_merchant_id.'/attendance/'.date('YmdHis').'';


            if($request->hasFile('end_work_file'))
            {
                $file=$request->file('end_work_file');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));
            }else{
                $fileName=null;
            }

            $data->end_work=$request->end_work;
            $data->end_longitude=$request->end_longitude;
            $data->end_latitude=$request->end_latitude;
            $data->end_work_file=$fileName;
            $data->is_end=1;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        } catch (\Exception $e) {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }


    public function listAttendance($staffUserId,Request $request)
    {

        try {
            $month=(is_null($request->month) || $request->month=='-1')?convertMonth(date('F')):$request->month;
            $year=(is_null($request->year) || $request->year=='-1')?date('Y'):$request->year;

            if($month<convertMonth(date('F')) && $year<date('Y'))
            {
                $startDate=Carbon::now()->month($month)->year($year)->startOfMonth()->toDateString();
                $endDate=Carbon::now()->month($month)->year($year)->endOfMonth()->toDateString();
                $isTrue=true;
            }elseif($month==convertMonth(date('F')) && $year==date('Y')){
                $startDate=Carbon::now()->month($month)->year($year)->startOfMonth()->toDateString();
                $endDate=date('Y-m-d');
                $isTrue=true;
            }else{
                $startDate=Carbon::now()->month($month)->year($year)->startOfMonth()->toDateString();
                $endDate=Carbon::now()->month($month)->year($year)->endOfMonth()->toDateString();
                $isTrue=false;
            }


            $data=DB::select("
            select
              to_char(mon.mon, 'YYYY-MM-DD') AS time,
              case when s.type_of_attendance is null then 'A' else s.type_of_attendance end,
              s.start_work,
              s.end_work,
              s.start_latitude,
              s.start_longitude,
              s.end_latitude,
              s.end_longitude,
              s.start_work_file,
              s.end_work_file
            from
              generate_series(
                '".$startDate."' :: timestamp, '".$endDate."' :: timestamp,
                interval '1 day'
              ) as mon(mon)
              left join (
                select
                  date_trunc('day', mmsa.created_at) as mon,
                  mmsa.*
                from
                  md_merchant_staff_attendances mmsa
                where
                  mmsa.md_staff_user_id = $staffUserId
                group by
                  mon,
                  mmsa.id
              ) s on mon.mon = s.mon
            order by
              mon.mon desc;
            ");

            if(count($data)<1 || $isTrue==false)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.200'),$data);


        } catch (\Exception $e) {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }

    }

}

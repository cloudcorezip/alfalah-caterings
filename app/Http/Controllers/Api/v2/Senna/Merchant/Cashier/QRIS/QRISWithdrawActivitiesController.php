<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\QRIS;

use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\QRIS\QRISWithdrawActivity;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class QRISWithdrawActivitiesController extends Controller
{

    protected $message;
    protected $qris;

    public function __construct()
    {
        $this->qris=QRISWithdrawActivity::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/cashier/qris/withdraw')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISWithdrawActivitiesController@create');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISWithdrawActivitiesController@update');
                Route::get('/all/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISWithdrawActivitiesController@getAll');
                Route::post('/all/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISWithdrawActivitiesController@getAll');
                Route::get('/one/{id}', 'Api\v2\Senna\Merchant\Cashier\QRIS\QRISWithdrawActivitiesController@getOne');
            });
    }

    public function getAll($merchantId,Request $request)
    {
        try {
            $offset=is_null($request->offset)?'-1':$request->offset;
            $limit=is_null($request->limit)?'-1':$request->limit;

            $data=$this->qris::where('md_merchant_id',$merchantId)
                ->skip($offset)
                ->take($limit)
                ->get();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {
            $data=$this->qris::where('id',$id)
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


    public function create(Request  $request,$merchantId)
    {
        try{
            $data = new $this->qris;

            $validator = Validator::make($request->all(), $data->ruleCreate);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $data->unique_code=$merchantId . Uuid::uuid4()->toString();
            $data->md_user_id=$request->md_user_id;
            $data->md_merchant_id=$merchantId;
            $data->qris_activation_id=$request->qris_activation_id;
            $data->amount=$request->amount;
            $data->admin_fee=$request->admin_fee;
            $data->transaction_status=$request->transaction_status;
            $data->response_create=$request->response_create;
            $data->response_status=$request->response_status;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }
    public function update(Request  $request,$id)
    {
        try{
            $data = $this->qris::find($id);

            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $data->qris_activation_id=$request->qris_activation_id;
            $data->amount=$request->amount;
            $data->admin_fee=$request->admin_fee;
            $data->transaction_status=$request->transaction_status;
            $data->response_create=$request->response_create;
            $data->response_status=$request->response_status;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }


}

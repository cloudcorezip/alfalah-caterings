<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\MerchantCashflow;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantShiftCashflow;
use App\Models\MasterData\SennaCashier\CashType;
use App\Models\SennaToko\Material;
use App\Utils\Account\AccountUtil;
use App\Utils\Accounting\CoaUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Models\Accounting\CoaCategory;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\CoaMapping;
use Modules\Merchant\Entities\Acc\JournalEntity;
use App\Models\Accounting\JurnalDetail;
use App\Classes\Singleton\CodeGenerator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class MerchantCashflowController extends Controller
{

    protected $message;
    protected $cashflow;

    public function __construct()
    {
        $this->cashflow=MerchantCashflow::class;
        $this->cashType=CashType::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/cashflow')
             ->middleware('etag')
             ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\MerchantCashflowController@create');
                Route::post('/update/{id}', 'Api\v2\Senna\Merchant\Cashier\MerchantCashflowController@update');
                Route::post('/list/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\MerchantCashflowController@list');
                Route::get('/getCoaMapping/{merchantId}/{type}', 'Api\v2\Senna\Merchant\Cashier\MerchantCashflowController@getCoaMapping');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\MerchantCashflowController@delete');
                Route::get('/detail/{id}', 'Api\v2\Senna\Merchant\Cashier\MerchantCashflowController@detail');

            });
    }


    public function create(Request  $request)
    {
        try{
            DB::beginTransaction();
            $data = new $this->cashflow;
            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/administrative-expense/';
            if($request->hasFile('file_proof'))
            {
                $file=$request->file('file_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }
            $transCode=CodeGenerator::generateJurnalCode(0,'JRN',$request->md_merchant_id);
            if($request->md_sc_cash_type_id==3)
            {
                $code=CodeGenerator::generateJurnalCode(3,'RVN',$request->md_merchant_id);

            }else{
                $code=CodeGenerator::generateJurnalCode(4,'EXP',$request->md_merchant_id);

            }
            $to=explode('_',$request->to_cashflow_coa_detail_id);

            // if($request->md_sc_cash_type_id==2)
            // {
            //     $sumOfCoaAmount=CoaUtil::checkAmountOfCoa($to[1],$request->md_merchant_id);
            //     if($sumOfCoaAmount['sum_of_total_debit']-$sumOfCoaAmount['sum_of_total_kredit']<($request->amount+$request->admin_fee))
            //     {
            //         return $this->message::getJsonResponse(404,'Saldo akun tidak cukup untuk melakukan pembayaran pembelian',[]);

            //     }

            // }
            $data->md_sc_cash_type_id=$request->md_sc_cash_type_id;
            $data->name=$request->name;
            $data->amount=$request->amount;
            $data->admin_fee=$request->admin_fee;
            $data->code=$code;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->cashflow_coa_detail_id=$request->cashflow_coa_detail_id;
            $data->to_cashflow_coa_detail_id=$to[1];
            $data->note=$request->note;
            $data->timezone=$request->timezone;
            $data->is_deleted=0;
            $data->created_by=$request->created_by;
            $data->save();

            $coa_Detail=CoaDetail::find($data->cashflow_coa_detail_id);

            $jurnal=new Jurnal();
            $jurnal->trans_code=$transCode;
            $jurnal->trans_name=$data->name;
            $jurnal->trans_time=$data->created_at;
            $jurnal->trans_note=$data->note;
            $jurnal->trans_purpose=$coa_Detail->name;
            $jurnal->trans_proof=$fileName;
            if($data->md_sc_cash_type_id==2){
                $jurnal->trans_type=4;

            }else{
                $jurnal->trans_type=3;

            }
            $jurnal->trans_amount=$data->amount+$data->admin_fee;
            $jurnal->md_merchant_id=$data->md_merchant_id;
            $jurnal->md_user_id_created=$data->created_by;
            $jurnal->ref_code=$data->code;
            $jurnal->external_ref_id=$data->id;
            $jurnal->save();
            if($request->md_sc_cash_type_id==3){
                $insert=[
                    [
                        'acc_coa_detail_id'=>$data->cashflow_coa_detail_id,
                        'coa_type'=>$coa_Detail->type_coa,
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->amount,
                        'created_at'=>$jurnal->created_at,
                        'updated_at'=>$jurnal->created_at,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$data->to_cashflow_coa_detail_id,
                        'coa_type'=>($coa_Detail->type_coa=='Debit')?'Kredit':'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->amount,
                        'created_at'=>$jurnal->created_at,
                        'updated_at'=>$jurnal->created_at,
                        'acc_jurnal_id'=>$jurnal->id
                    ],

                ];
                JurnalDetail::insert(
                    $insert
                );
            }else if($request->md_sc_cash_type_id==2){

                $adminFee=Merchant::find($request->md_merchant_id);
                if($data->admin_fee!=0){
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$data->cashflow_coa_detail_id,
                            'coa_type'=>$coa_Detail->type_coa,
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->amount,
                            'created_at'=>$jurnal->created_at,
                            'updated_at'=>$jurnal->created_at,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$adminFee->coa_administration_bank_id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->admin_fee,
                            'created_at'=>$jurnal->created_at,
                            'updated_at'=>$jurnal->created_at,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$data->to_cashflow_coa_detail_id,
                            'coa_type'=>($coa_Detail->type_coa=='Debit')?'Kredit':'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->amount+$data->admin_fee,
                            'created_at'=>$jurnal->created_at,
                            'updated_at'=>$jurnal->created_at,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                    ];
                }else{
                    $insert=[
                        [
                        'acc_coa_detail_id'=>$data->cashflow_coa_detail_id,
                        'coa_type'=>$coa_Detail->type_coa,
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->amount,
                        'created_at'=>$jurnal->created_at,
                        'updated_at'=>$jurnal->created_at,
                        'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$data->to_cashflow_coa_detail_id,
                            'coa_type'=>($coa_Detail->type_coa=='Debit')?'Kredit':'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->amount+$data->admin_fee,
                            'created_at'=>$jurnal->created_at,
                            'updated_at'=>$jurnal->created_at,
                            'acc_jurnal_id'=>$jurnal->id
                        ],

                    ];
                }

                JurnalDetail::insert(
                    $insert
                );
            }
            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }

    public function update(Request  $request,$id)
    {
        try{
            DB::beginTransaction();
            $data =$this->cashflow::find($id);
            $validator = Validator::make($request->all(), $data->create);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $transCode=CodeGenerator::generateJurnalCode(0,'JRN',$request->md_merchant_id);
            if($request->md_sc_cash_type_id==3 || $request->md_sc_cash_type_id==1)
            {
                $code=CodeGenerator::generateJurnalCode(3,'RVN',$request->md_merchant_id);
                $oldCode=$data->code;
            }else{
                $code=CodeGenerator::generateJurnalCode(4,'EXP',$request->md_merchant_id);
                $oldCode=$data->code;

            }
            // $code=$request->code;
            $to=explode('_',$request->to_cashflow_coa_detail_id);

            $data->md_sc_cash_type_id=$request->md_sc_cash_type_id;
            $data->name=$request->name;
            $data->amount=$request->amount;
            $data->admin_fee=$request->admin_fee;
            $data->code=$code;
            $data->md_merchant_id=$request->md_merchant_id;
            $data->cashflow_coa_detail_id=$request->cashflow_coa_detail_id;
            $data->to_cashflow_coa_detail_id=$to[1];
            $data->note=$request->note;
            $data->is_deleted=0;
            $data->created_by=$request->created_by;
            $data->save();

            $jurnalOld=Jurnal::where('external_ref_id',$data->id)->where('ref_code',$oldCode)->first();
            if(!is_null($jurnalOld)){
                $jurnalOldDetail=JurnalDetail::where('acc_jurnal_id',$jurnalOld->id)->delete();
                $jurnalOld->delete();
            }


            $coa_detail=CoaDetail::find($data->cashflow_coa_detail_id);
            $jurnal=new Jurnal();
            $jurnal->trans_code=$transCode;
            $jurnal->trans_name=$data->name;
            $jurnal->trans_time=$data->created_at;
            $jurnal->trans_note=$data->note;
            $jurnal->trans_purpose=$coa_detail->name;
            $jurnal->trans_proof='-';
            if($data->md_sc_cash_type_id==2){
                $jurnal->trans_type=4;

            }else{
                $jurnal->trans_type=3;

            }
            $jurnal->trans_amount=$data->amount+$data->admin_fee;
            $jurnal->md_merchant_id=$data->md_merchant_id;
            $jurnal->md_user_id_created=$data->created_by;
            $jurnal->ref_code=$data->code;
            $jurnal->external_ref_id=$data->id;
            $jurnal->save();
            if($request->md_sc_cash_type_id==3){
                $insert=[
                    [
                        'acc_coa_detail_id'=>$data->cashflow_coa_detail_id,
                        'coa_type'=>$coa_detail->type_coa,
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->amount,
                        'created_at'=>$jurnal->created_at,
                        'updated_at'=>$jurnal->created_at,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$data->to_cashflow_coa_detail_id,
                        'coa_type'=>($coa_detail->type_coa=='Debit')?'Kredit':'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->amount,
                        'created_at'=>$jurnal->created_at,
                        'updated_at'=>$jurnal->created_at,
                        'acc_jurnal_id'=>$jurnal->id
                    ],

                ];
                JurnalDetail::insert(
                    $insert
                );
            }else if($request->md_sc_cash_type_id==2){

                $adminFee=Merchant::find($request->md_merchant_id);
                if($data->admin_fee!=0){
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$data->cashflow_coa_detail_id,
                            'coa_type'=>$coa_detail->type_coa,
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->amount,
                            'created_at'=>$jurnal->created_at,
                            'updated_at'=>$jurnal->created_at,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$adminFee->coa_administration_bank_id,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->admin_fee,
                            'created_at'=>$jurnal->created_at,
                            'updated_at'=>$jurnal->created_at,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$data->to_cashflow_coa_detail_id,
                            'coa_type'=>($coa_detail->type_coa=='Debit')?'Kredit':'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->amount+$data->admin_fee,
                            'created_at'=>$jurnal->created_at,
                            'updated_at'=>$jurnal->created_at,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                    ];
                }else{
                    $insert=[
                        [
                        'acc_coa_detail_id'=>$data->cashflow_coa_detail_id,
                        'coa_type'=>$coa_detail->type_coa,
                        'md_sc_currency_id'=>1,
                        'amount'=>$data->amount,
                        'created_at'=>$jurnal->created_at,
                        'updated_at'=>$jurnal->created_at,
                        'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$data->to_cashflow_coa_detail_id,
                            'coa_type'=>($coa_detail->type_coa=='Debit')?'Kredit':'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$data->amount+$data->admin_fee,
                            'created_at'=>$jurnal->created_at,
                            'updated_at'=>$jurnal->created_at,
                            'acc_jurnal_id'=>$jurnal->id
                        ],

                    ];
                }

                JurnalDetail::insert(
                    $insert
                );
            }
            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }


    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $data =$this->cashflow::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $data->is_deleted=1;
            $data->save();

            Jurnal::where('external_ref_id',$data->id)
                ->where('ref_code',$data->code)
                ->update([
                    'is_deleted'=>1
                ]);
            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.202'),$data);
        }
        catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function  list($merchantId,Request  $request)
    {
        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $start_date=is_null($request->start_date)?date('Y-m-d'):$request->start_date;
            $end_date=is_null($request->end_date)?date('Y-m-d'):$request->end_date;


            $data=$this->cashflow::select([
                    'acc_merchant_cashflows.id',
                    'acc_merchant_cashflows.code',
                    'acc_merchant_cashflows.amount',
                    'acc_merchant_cashflows.created_at',
                    'acc_merchant_cashflows.md_sc_cash_type_id',
                    'd.name as transaction_name',
                    'd2.name as payment_name',
                    'u.fullname as created_name',
                    't.name as type_cashflow_name',
                    DB::raw("(CASE WHEN m.id is null and d2.code LIKE '1.1.01%' THEN 2 ELSE 13 END) AS trans_type_id"),
                    'd2.id as id_coa_detail',
                    'd2.code as code_coa_detail',
                ])
                    ->join('acc_coa_details  as d','acc_merchant_cashflows.cashflow_coa_detail_id','d.id')
                    ->join('acc_coa_details as d2','acc_merchant_cashflows.to_cashflow_coa_detail_id','d2.id')
                    ->leftjoin('md_transaction_types as m','m.name','d2.name')
                    ->join('md_users as u','u.id','acc_merchant_cashflows.created_by')
                    ->join('md_sc_cash_types as t','t.id','acc_merchant_cashflows.md_sc_cash_type_id')
                    ->where('acc_merchant_cashflows.md_merchant_id',$merchantId)
                    ->where('acc_merchant_cashflows.is_deleted',0)
                    ->whereDate('acc_merchant_cashflows.created_at','>=',$start_date)
                    ->whereDate('acc_merchant_cashflows.created_at','<=',$end_date)
                    ->skip($offset)
                        ->take($limit)
                        ->orderBy('acc_merchant_cashflows.id','desc')
                        ->get();






            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

    public function detail($id)
    {
        try {

            $data=$this->cashflow::select([
                'acc_merchant_cashflows.*',
                'd.name as transaction_name',
                'd2.name as payment_name',
                'u.fullname as created_name',
                't.name as type_cashflow_name',
                DB::raw("(CASE WHEN m.id is null and d2.code LIKE '1.1.01%' THEN 2 ELSE 13 END) AS trans_type_id"),
                'd2.id as id_coa_detail',
                'd2.code as code_coa_detail',
            ])
                ->join('acc_coa_details  as d','acc_merchant_cashflows.cashflow_coa_detail_id','d.id')
                ->join('acc_coa_details as d2','acc_merchant_cashflows.to_cashflow_coa_detail_id','d2.id')
                ->leftjoin('md_transaction_types as m','m.name','d2.name')
                ->join('md_users as u','u.id','acc_merchant_cashflows.created_by')
                ->join('md_sc_cash_types as t','t.id','acc_merchant_cashflows.md_sc_cash_type_id')
                ->find($id);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getCoaMapping($merchantId,$type)
    {
        try{

            if($type==2 || $type==4){
                $data=CoaDetail::
                select([
                    'acc_coa_details.id',
                    'acc_coa_details.name as name'
                ])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$merchantId)
                    ->whereIn('acc_coa_details.is_deleted',[0,2])
                    ->whereIn('c.name',['Beban Operasional & Usaha','Beban Lainnya'])
                    ->whereRaw("acc_coa_details.name not like '%Penyesuaian%'  and acc_coa_details.name not like '%Penyusutan%' and acc_coa_details.name not like '%Produksi%' and acc_coa_details.name not like '%Aset%' and acc_coa_details.name not like '%Diskon Penjualan%' ")
                    ->orderBy('acc_coa_details.id','asc')
                    ->get();
            }else{
                $data=CoaDetail::
                select([
                    'acc_coa_details.id',
                    'acc_coa_details.name'
                ])
                    ->join('acc_coa_categories as c','c.id','acc_coa_details.acc_coa_category_id')
                    ->where('c.md_merchant_id',$merchantId)
                    ->whereIn('acc_coa_details.is_deleted',[0,2])
                    ->whereIn('c.name',['Pendapatan','Pendapatan Lainnya'])
                    ->whereRaw("acc_coa_details.name not like '%Penjualan%'  and acc_coa_details.name not like '%Laba Penjualan Aset%' and acc_coa_details.name not like '%Diskon Pembelian Barang%'")
                    ->orderBy('acc_coa_details.id','asc')
                    ->get();
            }

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

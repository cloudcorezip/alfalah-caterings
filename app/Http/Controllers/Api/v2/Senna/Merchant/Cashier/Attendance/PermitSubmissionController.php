<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\Attendance;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Attendance\MerchantStaffAttendance;
use App\Models\MasterData\Attendance\MerchantStaffPermitSubmission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PermitSubmissionController extends Controller
{

    protected $permit;
    protected $message;
    protected $attendance;

    public function __construct()
    {
        $this->permit = MerchantStaffPermitSubmission::class;
        $this->message = Message::getInstance();
        $this->attendance=MerchantStaffAttendance::class;
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/attendance/permit-submission')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function () {
                Route::post('/submit', 'Api\v2\Senna\Merchant\Cashier\Attendance\PermitSubmissionController@submit');
                Route::get('/cancel/{id}', 'Api\v2\Senna\Merchant\Cashier\Attendance\PermitSubmissionController@cancel');
                Route::get('/one/{id}', 'Api\v2\Senna\Merchant\Cashier\Attendance\PermitSubmissionController@detail');
                Route::post('/approve-or-reject/{id}', 'Api\v2\Senna\Merchant\Cashier\Attendance\PermitSubmissionController@approved');
                Route::post('/list/{staffUserId}', 'Api\v2\Senna\Merchant\Cashier\Attendance\PermitSubmissionController@list');
                Route::post('/list-all/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\Attendance\PermitSubmissionController@listAll');



            });
    }

    public function submit(Request  $request)
    {
        try{


            $data = new $this->permit;

            $validator = Validator::make($request->all(), $data->submit);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/permit-submission/'.date('YmdHis').'/';
            $check=$this->permit::where('md_staff_user_id',$request->md_staff_user_id)
                ->whereDate('created_at',date('Y-m-d'))
                ->where('is_canceled',0)
                ->where('is_approved',0)
                ->first();
            if($check)
            {
                return $this->message::getJsonResponse(404,'Kamu sudah pengajuan izin hari ini', []);

            }

            if($request->hasFile('supporting_file'))
            {
                $file=$request->file('supporting_file');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }

            $data->md_merchant_id=$request->md_merchant_id;
            $data->md_staff_user_id=$request->md_staff_user_id;
            $data->supporting_file=$fileName;
            $data->type_of_attendance=$request->type_of_attendance;
            $data->desc=$request->desc;
            $data->md_permit_category_id=$request->md_permit_category_id;
            $data->save();

            return $this->message::getJsonResponse(200,'Pengajuan izin tidak masuk berhasil dikirim',$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }

    }

    public function cancel($id)
    {
        try{

            $data=$this->permit::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            $data->is_canceled=1;
            $data->save();

            return $this->message::getJsonResponse(200,'Pengajuan izin tidak masuk berhasil dicancel',$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function approved($id,Request  $request)
    {
        try{
            DB::beginTransaction();
            $data=$this->permit::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            $validator = Validator::make($request->all(), $data->approve);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }

            $data->is_approved=$request->is_approved;
            $data->md_user_id_approved=$request->md_user_id_approved;
            $data->save();

            $attendance=new $this->attendance;
            $attendance->md_merchant_id=$data->md_merchant_id;
            $attendance->md_staff_user_id=$data->md_staff_user_id;
            if($request->is_approved==1){
                $attendance->is_permit=1;
            }else{
                $attendance->is_permit=0;
            }
            
            $attendance->type_of_attendance=($request->is_approved==1)?$data->type_of_attendance:'A';
            $attendance->save();

            DB::commit();

            return $this->message::getJsonResponse(200,($request->is_approved==1)?'Pengajuan izin disetujui':'Pengajuan izin tidak disetujui',$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function detail($id)
    {
        try{
            $data=$this->permit::select([
                'md_merchant_staff_permit_submissions.*',
                'c.name as permit_category_name',
                'u.fullname as staff'
            ])->join('md_permit_categories as c','c.id','md_merchant_staff_permit_submissions.md_permit_category_id')
                ->join('md_users as u','u.id','md_merchant_staff_permit_submissions.md_staff_user_id')
                ->where('md_merchant_staff_permit_submissions.id',$id)
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }

    }

    public function list($staffUserId,Request  $request)
    {
        $offset=is_null($request->offset)?0:$request->offset;
        $limit=is_null($request->limit)?10:$request->limit;
        $order=(is_null($request->order) || $request->order=='-1')?'desc':$request->order;
        $month=(is_null($request->month) || $request->month=='-1')?convertMonth(date('F')):$request->month;
        //dd(convertMonth(date('F')));
        $year=(is_null($request->year) || $request->year=='-1')?date('Y'):$request->year;



        try{
            $data=$this->permit::select([
                'md_merchant_staff_permit_submissions.*',
                'c.name as permit_category_name',
                'u.fullname as staff'
            ])->join('md_permit_categories as c','c.id','md_merchant_staff_permit_submissions.md_permit_category_id')
                ->join('md_users as u','u.id','md_merchant_staff_permit_submissions.md_staff_user_id')
                ->where('u.id',$staffUserId)
                ->whereMonth('md_merchant_staff_permit_submissions.created_at',$month)
                ->whereYear('md_merchant_staff_permit_submissions.created_at',$year)
                ->skip($offset)
                ->take($limit)
                ->orderBy('md_merchant_staff_permit_submissions.created_at', 'desc')
                ->get();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function listAll($merchantId,Request  $request)
    {
        $offset=is_null($request->offset)?0:$request->offset;
        $limit=is_null($request->limit)?10:$request->limit;
        $order=(is_null($request->order) || $request->order=='-1')?'desc':$request->order;
        $month=(is_null($request->month) || $request->month=='-1')?convertMonth(date('F')):$request->month;
        //dd(convertMonth(date('F')));
        $year=(is_null($request->year) || $request->year=='-1')?date('Y'):$request->year;


        //dd($month);

        try{
            $data=$this->permit::select([
                'md_merchant_staff_permit_submissions.*',
                'c.name as permit_category_name',
                'u.fullname as staff'
            ])->join('md_permit_categories as c','c.id','md_merchant_staff_permit_submissions.md_permit_category_id')
                ->join('md_users as u','u.id','md_merchant_staff_permit_submissions.md_staff_user_id')
                ->where('md_merchant_staff_permit_submissions.md_merchant_id',$merchantId)
                ->whereMonth('md_merchant_staff_permit_submissions.created_at',$month)
                ->whereYear('md_merchant_staff_permit_submissions.created_at',$year)
                ->skip($offset)
                ->take($limit)
                ->orderBy('md_merchant_staff_permit_submissions.created_at', 'desc')
                ->get();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

}

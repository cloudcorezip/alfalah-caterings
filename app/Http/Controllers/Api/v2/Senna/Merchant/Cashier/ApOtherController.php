<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Utils\Accounting\AccUtil;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Supplier;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Product;
use App\Utils\Accounting\CoaUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class ApOtherController extends Controller
{

    protected $message;
    protected $supplier;
    protected $customer;
    protected $product;
    protected $ap;
    protected $apDetail;

    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->customer=Customer::class;
        $this->supplier=Supplier::class;
        $this->product=Product::class;
        $this->ap=MerchantAp::class;
        $this->apDetail=MerchantApDetail::class;

    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/ap-other')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create', 'Api\v2\Senna\Merchant\Cashier\ApOtherController@create');
                Route::delete('/delete/{id}', 'Api\v2\Senna\Merchant\Cashier\ApOtherController@delete');
                Route::delete('/delete-detail/{id}', 'Api\v2\Senna\Merchant\Cashier\ApOtherController@deleteDetail');

            });
    }

    public function create(Request $request){
        try{
            DB::beginTransaction();

            $code=CodeGenerator::generateApCode(0,'APC',$request->merchant_id);
            $getCoa=CoaUtil::checkOfCoaArAp($request->merchant_id);

            if(is_null($request->payment_method))
            {
                return $this->message::getJsonResponse(404,'Metode pembayaran belum dipilih',[]);
            }
            $trans=explode('_',$request->payment_method);

            if($request->user_type=='customer'){
                $user=Customer::find($request->user_id);
                $is_customer=1;
            }else{
                $user=Supplier::find($request->user_id);
                $is_customer=0;
            }

            $destinationPath = 'public/uploads/merchant/'.$request->merchant_id.'/acc/jurnal/ap/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, file_get_contents($file));

            }else{
                $fileName=null;
            }

            date_default_timezone_set($request->timezone);


            if(is_null($request->pay_json)){
                    $data = new $this->ap;
                    $data->apable_id=0;
                    $data->apable_type="#";
                    $data->ap_code=$code;
                    $data->ap_name=$request->note;
                    $data->ap_note=$request->note;
                    $data->ap_purpose=$user->name;
                    $data->ap_time= date('Y-m-d H:i:s');
                    $data->ap_acc_coa=$getCoa->coa_ap_other;
                    $data->sync_id=Uuid::uuid4()->toString();
                    $data->md_merchant_id=$request->merchant_id;
                    $data->timezone=$request->timezone;
                    $data->ap_amount=$request->amount;
                    $data->ap_proof=$fileName;
                    $data->residual_amount=$request->amount;
                    $data->due_date=$request->due_date;
                    $data->is_from_customer=$is_customer;
                    $data->ref_user_id=$request->user_id;
                    if($is_customer==0)
                    {
                        $data->sc_supplier_id=$request->user_id;
                    }

                    $data->save();

                    $apDetail=new $this->apDetail;
                    $apDetail->acc_merchant_ap_id=$data->id;
                    $apDetail->paid_nominal=$request->amount;
                    $apDetail->paid_date=date('Y-m-d H:i:s');
                    $apDetail->note=$request->note;
                    $apDetail->timezone=$request->timezone;
                    $apDetail->trans_proof=$fileName;
                    $apDetail->created_by=$request->user_id;
                    $apDetail->md_transaction_type_id=$trans[0];
                    $apDetail->ap_detail_code=CodeGenerator::generateJurnalCode(0,'PVT',$request->merchant_id);;
                    $apDetail->type_action=3;
                    $apDetail->save();

                $jurnal=new Jurnal();
                $jurnal->ref_code=$apDetail->ap_detail_code;
                $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$request->merchant_id);
                $jurnal->trans_name='Pemberian Utang Usaha '.$data->ap_code.'-'.$apDetail->ap_detail_code;
                $jurnal->trans_time=$apDetail->paid_date;
                $jurnal->trans_note='Pemberian Utang Usaha  '.$data->ap_code.'-'.$apDetail->ap_detail_code;
                $jurnal->trans_purpose='Pemberian Utang Usaha  '.$data->ap_code.'-'.$apDetail->ap_detail_code;
                $jurnal->trans_amount=$apDetail->paid_nominal;
                $jurnal->trans_proof=$fileName;
                $jurnal->md_merchant_id=$request->merchant_id;
                $jurnal->md_user_id_created=$request->created_by;
                $jurnal->external_ref_id=$apDetail->id;
                $jurnal->flag_name='Pemberian Utang Usaha  '.$data->ap_code;
                $jurnal->save();
                // dd($trans[1]);

                $toCoa=$trans[1];
                $insert=[
                    [
                        'acc_coa_detail_id'=>$getCoa->coa_ap_other,
                        'coa_type'=>'Kredit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$apDetail->paid_nominal,
                        'created_at'=>$apDetail->paid_date,
                        'updated_at'=>$apDetail->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                    [
                        'acc_coa_detail_id'=>$toCoa,
                        'coa_type'=>'Debit',
                        'md_sc_currency_id'=>1,
                        'amount'=>$apDetail->paid_nominal,
                        'created_at'=>$apDetail->paid_date,
                        'updated_at'=>$apDetail->paid_date,
                        'acc_jurnal_id'=>$jurnal->id
                    ],
                ];
                JurnalDetail::insert($insert);
                $result=$data;
            }else{
                $result=[];
                foreach(json_decode($request->pay_json) as $key => $item){
                    $data = $this->ap::find($item->id);

                    if($data->apable_id!=0 && $data->apable_type!='#'){
                        if($item->amount>=$data->residual_amount)
                        {
                            $data->is_paid_off=1;
                            $data->apable->md_sc_transaction_status_id=TransactionStatus::PAID;
                            $data->apable->save();
                        }else{
                            $data->apable->md_sc_transaction_status_id=6;
                            $data->apable->save();
                        }
                    }

                    $data->residual_amount-=$item->amount;
                    $data->paid_nominal+=$item->amount;
                    $data->save();

                    $apDetail=new MerchantApDetail();
                    $apDetail->ap_detail_code=CodeGenerator::generateJurnalCode(0,'PVT',$request->merchant_id);
                    $apDetail->paid_nominal=$item->amount;
                    $apDetail->paid_date=date('Y-m-d H:i:s');
                    $apDetail->note=$request->note;
                    $apDetail->acc_merchant_ap_id=$data->id;
                    $apDetail->created_by=$request->created_by;
                    $apDetail->md_transaction_type_id=$trans[0];
                    $apDetail->timezone=$request->timezone;
                    $apDetail->note=$request->note;
                    $apDetail->trans_coa_detail_id=$trans[1];
                    $apDetail->trans_proof=$fileName;
                    if($data->apable_id!=0 && $data->apable_type!='#'){
                        $apDetail->type_action=1;
                    }else{
                        $apDetail->type_action=2;
                    }
                    $apDetail->save();

                    if($data->apable_id!=0 && $data->apable_type!='#'){
                        $coa_id=$getCoa->coa_ap_purchase;
                        $desc='Pembayaran Penjualan '.$data->apable->code.'-'.$apDetail->ap_detail_code;
                    }else{
                        $coa_id=$getCoa->coa_ap_other;
                        $desc='Pembayaran Hutang '.$data->ap_code.'-'.$apDetail->ap_detail_code;
                    }

                    $jurnal=new Jurnal();
                    $jurnal->ref_code=$apDetail->ap_detail_code;
                    $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$request->merchant_id);
                    $jurnal->trans_name=$desc;
                    $jurnal->trans_time=$apDetail->paid_date;
                    $jurnal->trans_note=$desc;
                    $jurnal->trans_purpose=$desc;
                    $jurnal->trans_amount=$apDetail->paid_nominal;
                    $jurnal->trans_proof=$fileName;
                    $jurnal->md_merchant_id=$request->merchant_id;
                    $jurnal->md_user_id_created=$request->created_by;
                    $jurnal->external_ref_id=$apDetail->id;
                    $jurnal->flag_name=$desc;
                    $jurnal->save();

                    $toCoa=$trans[1];
                    $insert=[
                        [
                            'acc_coa_detail_id'=>$coa_id,
                            'coa_type'=>'Kredit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$apDetail->paid_nominal,
                            'created_at'=>$apDetail->paid_date,
                            'updated_at'=>$apDetail->paid_date,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                        [
                            'acc_coa_detail_id'=>$toCoa,
                            'coa_type'=>'Debit',
                            'md_sc_currency_id'=>1,
                            'amount'=>$apDetail->paid_nominal,
                            'created_at'=>$apDetail->paid_date,
                            'updated_at'=>$apDetail->paid_date,
                            'acc_jurnal_id'=>$jurnal->id
                        ],
                    ];
                    JurnalDetail::insert($insert);
                }
                $result[]=[
                    "id"=>$data->id,
                    "id_payment"=>$apDetail->id,
                    "residual_amount"=>$data->residual_amount,
                    "payment_nominal"=>$apDetail->paid_nominal,
                    "payment_note"=>$apDetail->note,
                    "payment_code"=>$apDetail->ar_detail_code,
                    "paid_date"=>$apDetail->paid_date,
                    "payment_method"=>$request->payment_name,
                    "payment_proof"=>$apDetail->trans_proof
            ];
            }


            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$result);


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function delete(Request $request)
    {
        try{
            DB::beginTransaction();
            $id=$request->id;
            $data=$this->ap::find($id);
            $date = new \DateTime($data->ap_time);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            $data->is_deleted=1;
            $data->save();
            $this->apDetail::where('acc_merchant_ap_id',$id)->update(['is_deleted'=>1]);
            $apDetail=$this->apDetail::where('acc_merchant_ap_id',$id)->get();
            foreach($apDetail as $key => $item){
                Jurnal::where([
                    'external_ref_id'=>$item->id,
                    'ref_code'=>$item->ap_detail_code
                ])->update(['is_deleted'=>1]);
            }
            $this->apDetail::where('acc_merchant_ap_id',$id)->delete();
            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function deleteDetail($id)
    {
        try{
            DB::beginTransaction();

            $data=$this->apDetail::find($id);

            $date = new \DateTime($data->created_at);
            if(AccUtil::checkClosingJournal($data->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }

            $ap=$this->ap::find($data->acc_merchant_ap_id);
            $ap->residual_amount+=$data->paid_nominal;
            $ap->paid_nominal-=$data->paid_nominal;
            $ap->save();

            Jurnal::where([
                'external_ref_id'=>$data->id,
                'ref_code'=>$data->ap_detail_code
            ])->update(['is_deleted'=>1]);

            $this->apDetail::where('acc_merchant_ap_id',$id)->delete();
            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }
}

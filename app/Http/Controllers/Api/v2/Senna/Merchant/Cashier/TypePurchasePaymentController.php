<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Models\MasterData\MerchantCashdrawer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class TypePurchasePaymentController
{

    protected $message;
    protected $cashdrawer;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/purchase-payment-type')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/all/{merchantId}', 'Api\v2\Senna\Merchant\Cashier\TypePurchasePaymentController@getAll');
            });
    }

    public function getAll($merchantId,Request  $request)
    {

        try {

            $transType=collect(DB::select("select t.id,t.name,d.id as coa_detail_id,d.name as coa_name,d.code,c.md_merchant_id,d.is_deleted
                from acc_coa_details d left join md_transaction_types
                t on lower(d.name)=lower(t.name)
                join acc_coa_categories c
                on c.id=d.acc_coa_category_id
                where d.code like '%1.1.01.%' or d.code like '%1.1.02.%'
                and c.md_merchant_id=$merchantId and d.is_deleted in(0,2)
               "));
            
            $filtered = $transType->filter(function ($item)use ($merchantId) {
                return data_get($item, 'md_merchant_id') == $merchantId;
            });

            $results = $filtered->where('is_deleted','!=',1)->sortBy('code')->all();

            $array=[];
            foreach ($results as $b)
            {
                if(substr_count($b->code,"1.1.02")>0){
                    $type=(is_null($b->id))?'Bank-':'';
                    $b_id=(is_null($b->id))?13:$b->id;

                }else{
                    $b_id=(is_null($b->id))?2:$b->id;
                    $type=(is_null($b->id))?'Tunai-':'';

                }
                if(is_null($request->get('is_shift')) || $request->get('is_shift')==0){
                    $array[]=[
                        'id'=>$b_id.'_'.$b->coa_detail_id.'_'.$b->code,
                        'name'=>$type.$b->coa_name
                    ];
                }else{
                    if($b->code!='1.1.01.01'){
                        $array[]=[
                            'id'=>$b_id.'_'.$b->coa_detail_id.'_'.$b->code,
                            'name'=>$type.$b->coa_name
                        ];
                    }
                }

            }
            if(count($array)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$array);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

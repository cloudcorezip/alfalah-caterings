<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Supplier;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantStaff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class DropdownController extends Controller
{

    protected $message;
    protected $customer;
    protected $supplier;

    public function __construct()
    {
        $this->customer=Customer::class;
        $this->supplier=Supplier::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/toko/dropdown')
            ->middleware('etag')
            ->group(function(){
                Route::get('/all-customer', 'Api\v2\Senna\Merchant\Cashier\DropdownController@getAllCustomer')->name('api.customer.all');
                Route::get('/all-supplier', 'Api\v2\Senna\Merchant\Cashier\DropdownController@getAllSupplier')->name('api.supplier.all');
                Route::get('/all-staff', 'Api\v2\Senna\Merchant\Cashier\DropdownController@getAllStaff')->name('api.staff.all');

            });
    }
    public function getAllCustomer(Request $request)
    {
        try{
                $merchant=Merchant::find($request->merchant_id);
                $data=$this->customer::select([
                    'id',
                    'name'
                ])
                    ->where('md_user_id',$merchant->md_user_id)
                    ->where('is_deleted',0)
                    ->get();
            

            $response =[];
            $response[] = [
                "id"=>-1,
                "text"=>'--Pilih Semua Pelanggan--',
                "selected"=>true
            ];
            
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->name
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }
    public function getAllSupplier(Request $request)
    {
        try{
                $merchant=Merchant::find($request->merchant_id);
                $data=$this->supplier::select([
                    'id',
                    'name'
                ])
                    ->where('md_user_id',$merchant->md_user_id)
                    ->where('is_deleted',0)
                    ->get();
            

            $response =[];
            $response[] = [
                "id"=>-1,
                "text"=>'--Pilih Semua Supplier--',
                "selected"=>true
            ];
            
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->name
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }
    public function getAllStaff(Request $request)
    {
        try{
            $merchant=Merchant::find($request->merchant_id);

                $data=MerchantStaff::select([
                    'u.id',
                    'u.fullname',
                    'u.email',
                    'r.name as role_name'
                ])
                    ->join('md_merchants as m','m.id','md_merchant_staff.md_merchant_id')
                    ->join('md_users as u','u.id','md_merchant_staff.md_user_id')
                    ->join('md_roles as r','r.id','u.md_role_id')
                    ->where('m.md_user_id',$merchant->md_user_id)
                    ->where('md_merchant_staff.is_deleted',0)
                    ->where('md_merchant_staff.is_non_employee',0)
                    ->get();
            

            $response =[];
            $response[] = [
                        "id"=>-1,
                        "text"=>'--Pilih Semua Kasir--',
                        "selected"=>true
                ];
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->fullname.'-('.$item->role_name.')'
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }
}

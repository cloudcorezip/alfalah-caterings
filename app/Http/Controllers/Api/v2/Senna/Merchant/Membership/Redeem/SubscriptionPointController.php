<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Membership\Redeem;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Marketing\MerchantCommission;
use App\Models\Marketing\MerchantHistoryRedeemPoint;
use App\Models\Marketing\MerchantMembership;
use App\Models\Marketing\MerchantRedeemPoint;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\Subscription;
use App\Models\MasterData\User;
use App\Utils\Membership\Merchant\RedeemUtil;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class SubscriptionPointController extends Controller
{

    protected $message;
    protected $redeemPoint;
    protected $merchant;
    protected $user;
    protected $merchantHistoryRedeem;

    public function __construct()
    {
        $this->message = Message::getInstance();
        $this->redeemPoint = MerchantRedeemPoint::class;
        $this->merchant=Merchant::class;
        $this->user=User::class;
        $this->merchantHistoryRedeem=MerchantHistoryRedeemPoint::class;
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/membership/redeem/subscription')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/all', 'Api\v2\Senna\Merchant\Membership\Redeem\SubscriptionPointController@all');
                Route::post('/redeem/{merchantId}', 'Api\v2\Senna\Merchant\Membership\Redeem\SubscriptionPointController@redeem');
            });
    }

    public function all(Request  $request)
    {
        try{
            $data=$this->redeemPoint::where('is_type',1)
                ->where('is_active',1)
                ->where('is_deleted',0)
                ->with('redeemable')
                ->orderBy('point','ASC')
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }

    }

    public function redeem(Request  $request,$merchantId)
    {
        try {
            $merchant=$this->merchant::find($merchantId);
            if(is_null($merchant))
            {
                return $this->message::getJsonResponse(404,trans('custom.account_not_found'),[]);
            }
            $validator = Validator::make($request->all(),[
                'redeemable_id'=>'required',
                'point'=>'required',
                'id'=>'required',
                'md_user_id'=>'required',
                'timezone'=>'required'
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404,$error,[]);
            }
            date_default_timezone_set($request->timezone);
            $user=$this->user::find($request->md_user_id);
            if(RedeemUtil::checkPoint($merchantId,$request->point)==0)
            {
                return $this->message::getJsonResponse(404,'Point tidak mencukupi',[]);
            }
            $redeem=RedeemUtil::reducePointBySubscription($user,$merchantId,$request->id,$request->point,Subscription::find($request->redeemable_id));
            if($redeem==false)
            {
                return $this->message::getJsonResponse(404,'Ada kesalahan saat melakukan penukaran point',[]);
            }
            return $this->message::getJsonResponse(200,'Penukaran point berhasil',[]);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }


}

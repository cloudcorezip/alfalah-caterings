<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Membership\Redeem;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Marketing\MerchantHistoryRedeemPoint;
use App\Models\Marketing\MerchantRedeemPoint;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\OrderStatus;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\MasterData\Subscription;
use App\Models\MasterData\TransactionType;
use App\Models\MasterData\User;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\StockInventory;
use App\Utils\Inventory\InventoryUtil;
use App\Utils\Membership\Merchant\RedeemUtil;
use App\Utils\Merchant\ProductUtil;
use App\Utils\Order\SaleOrderUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use QrCode;


class SennaStoreController extends Controller
{
    protected $message;
    protected $redeemPoint;
    protected $merchant;
    protected $user;
    protected $merchantHistoryRedeem;
    protected $order;
    protected $customer;
    protected $detailOrder;
    protected $product;


    public function __construct()
    {
        $this->message = Message::getInstance();
        $this->redeemPoint = MerchantRedeemPoint::class;
        $this->merchant=Merchant::class;
        $this->user=User::class;
        $this->merchantHistoryRedeem=MerchantHistoryRedeemPoint::class;
        $this->order=SaleOrder::class;
        $this->customer=Customer::class;
        $this->detailOrder=SaleOrderDetail::class;
        $this->product=Product::class;

    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/membership/redeem/store')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/all', 'Api\v2\Senna\Merchant\Membership\Redeem\SennaStoreController@all');
                Route::post('/redeem/{merchantId}', 'Api\v2\Senna\Merchant\Membership\Redeem\SennaStoreController@redeem');
            });
    }

    public function all(Request  $request)
    {
        try{
            $data=$this->redeemPoint::where('is_type',2)
                ->where('is_active',1)
                ->where('is_deleted',0)
                ->with('redeemable')
                ->orderBy('point','ASC')
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function  redeem(Request  $request,$merchantId)
    {
//        try {
//            DB::beginTransaction();
//            $merchant=$this->merchant::find($merchantId);
//            if(is_null($merchant))
//            {
//                return $this->message::getJsonResponse(404,trans('custom.account_not_found'),[]);
//            }
//            $validator = Validator::make($request->all(),[
//                'redeemable_id'=>'required',
//                'point'=>'required',
//                'id'=>'required',
//                'md_user_id'=>'required',
//                'shipping_cost'=>'numeric|required',
//                'destination'=>'required',
//                'courier_name'=>'required',
//                'timezone'=>'required',
//                'service_courier_name'=>'required',
//                'weight'=>'required',
//            ]);
//
//            if ($validator->fails()) {
//                $error = $validator->errors()->first();
//                return $this->message::getJsonResponse(404,$error,[]);
//            }
//            date_default_timezone_set($request->timezone);
//            if(RedeemUtil::checkPoint($merchantId,$request->point)==0)
//            {
//                return $this->message::getJsonResponse(404,'Point tidak mencukupi',[]);
//            }
//            $customerCheck=$this->customer::select(['sc_customers.*'])
//                ->join('sc_user_customers as c','c.sc_customer_id','sc_customers.id')
//                ->where('c.from_user_id',$request->md_user_id)
//                ->first();
//            $user=$this->user::find($request->md_user_id);
//            if(is_null($customerCheck))
//            {
//                $customerCheck=new $this->customer;
//                $customerCheck->name=$user->fullname;
//                $customerCheck->email=$user->email;
//                $customerCheck->phone_number=$user->phone_number;
//                $customerCheck->md_user_id=$merchant->md_user_id;
//                $customerCheck->save();
//                DB::table('sc_user_customers')
//                    ->insert([
//                        'from_user_id'=>$request->md_user_id,
//                        'sc_customer_id'=>$customerCheck->id
//                    ]);
//            }
//            $convert=$request->point*config('senna_membership.kurs.senna_kasir');
//            $checkOrder=new $this->order;
//            $code=CodeGenerator::generateSaleOrder($merchantId);
//            $checkOrder->code=$code;
//            $checkOrder->sc_customer_id=$customerCheck->id;
//            $checkOrder->md_merchant_id=$merchantId;
//            $checkOrder->md_sc_shipping_category_id=ShippingCategory::COURIER;
//            $destinationPath = 'public/uploads/users/order/offline/toko/'.custom_encrypt($code).'/qr-code/';
//            if(!file_exists($destinationPath)){
//                mkdir($destinationPath,0777,true);
//            }
//            $checkOrder->md_sc_transaction_status_id=TransactionStatus::PAID;
//            $checkOrder->md_transaction_type_id=6;
//            $encrytion="type=pay&key=".custom_encrypt($code);
//            $qrCodeFile=$destinationPath.date('YmdHis').'.png';
//            QrCode::format('png')->size(400)->generate($encrytion,$qrCodeFile);
//            $checkOrder->sync_id=$merchantId.Uuid::uuid4()->toString();
//            $checkOrder->qr_code=$qrCodeFile;
//            $checkOrder->destination=$request->destination;
//            $checkOrder->promo=0;
//            $checkOrder->promo_percentage=0;
//            $checkOrder->shipping_cost=$request->shipping_cost;
//            $checkOrder->distance=0;
//            $checkOrder->weight=$request->weight;
//            $checkOrder->courier_name=$request->courier_name;
//            $checkOrder->service_courier_name=$request->service_courier_name;
//            $checkOrder->md_sc_order_status_id=OrderStatus::SHOP_CONFIRMATION;
//            $checkOrder->is_debet=0;
//            $checkOrder->queue_code='-';
//            $checkOrder->paid_nominal=0;
//            $checkOrder->is_approved_shop=0;
//            $checkOrder->tax=0;
//            $checkOrder->tax_percentage=0;
//            $checkOrder->is_from_senna_app=1;
//            $checkOrder->change_money=0;
//            $checkOrder->created_by=$request->md_user_id;
//            $checkOrder->timezone=$request->timezone;
//            $checkOrder->total=$convert-$request->shipping_cost;
//            $checkOrder->save();
//            $product = $this->product::find($request->redeemable_id);
//
//            if($product->is_with_stock==1)
//            {
//                $checkQuantity=ProductUtil::checkQuantity($request->redeemable_id,1);
//                if($checkQuantity==false)
//                {
//                    return $this->message::getJsonResponse(404, trans('ordershop.out_of_stock'), []);
//
//                }
//            }
//
//            if($checkOrder->md_sc_transaction_status_id==TransactionStatus::PAID)
//            {
//                if($product->is_with_stock==1)
//                {
//                    ProductUtil::reduceQuantity($request->redeemable_id,1);
//
//                }
//                $purchase=InventoryUtil::inventoryCode($merchantId,$request->redeemable_id,1);
//                $profit=(1*($convert-$request->shipping_cost))-(1*$purchase);
//
//            }else{
//                $profit=0;
//                $purchase=0;
//            }
//            $detailOrder=new  $this->detailOrder;
//            $detailOrder->sc_product_id=$request->redeemable_id;
//            $detailOrder->quantity=1;
//            $detailOrder->sc_sale_order_id=$checkOrder->id;
//            $detailOrder->profit=$profit;
//            $detailOrder->price=$convert-$request->shipping_cost;
//            $detailOrder->sub_total=$convert-$request->shipping_cost;
//            $detailOrder->save();
//            $merchant->getMerchantPoint->point-=$request->point;
//            $merchant->getMerchantPoint->save();
//            $history=new MerchantHistoryRedeemPoint();
//            $codeRedeem=CodeGenerator::redeemCode($merchantId);
//            $history->code=$codeRedeem;
//            $history->md_merchant_id=$merchantId;
//            $history->sm_merchant_redeem_point_id=$request->id;
//            $history->point_exchange=$request->point;
//            $history->ref_id=$code;
//            $history->md_transaction_status_id=\App\Models\MasterData\SennaPayment\TransactionStatus::SUCCESS;
//            $history->save();
//            $checkOrder->transaction()->create([
//                'code'=>$codeRedeem,
//                'md_user_id'=>$user->id,
//                'md_sp_transaction_type_id'=> \App\Models\MasterData\SennaPayment\TransactionType::TF_TOKO,
//                'md_sp_transaction_status_id'=> \App\Models\MasterData\SennaPayment\TransactionStatus::SUCCESS,
//                'amount'=>$request->point,
//                'transaction_from'=>'Penukaran point oleh '.$user->fullname,
//                'transaction_to'=>'Penukaran point dengan pembelian produk '.$product->name."(".$code.")"
//            ]);
//            parent::firebase()::send(
//                'Pesanan Masuk',
//                trans('firebase.order_in',['code'=>$code]),
//                '',
//                $checkOrder->getMerchant->getUser,
//                self::class,Route::currentRouteAction(),
//                SaleOrderUtil::saleOrder($checkOrder->id),
//                1);
//
//            DB::commit();
//            return $this->message::getJsonResponse(200,'Penukaran point berhasil',[]);
//
//        }catch (\Exception $e)
//        {
//            DB::rollBack();
//            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
//            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
//
//        }

        return $this->message::getJsonResponse(404,'Untuk sementara penukaran point dengan hardware belum bisa digunakan',[]);


    }

}

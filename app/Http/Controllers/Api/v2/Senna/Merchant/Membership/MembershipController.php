<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Senna\Merchant\Membership;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Marketing\MerchantCommission;
use App\Models\Marketing\MerchantHistoryRedeemPoint;
use App\Models\Marketing\MerchantMembership;
use App\Models\MasterData\MerchantFollower;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class MembershipController extends Controller
{
    protected $message;
    protected $membership;
    protected $commission;
    protected $merchantHistoryRedeem;


    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->membership=MerchantMembership::class;
        $this->commission=MerchantCommission::class;
        $this->merchantHistoryRedeem=MerchantHistoryRedeemPoint::class;
    }

    public function routeApi()
    {
        Route::prefix('v2/senna/merchant/membership')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/join/{merchantId}', 'Api\v2\Senna\Merchant\Membership\MembershipController@join');
                Route::get('/detail/{merchantId}', 'Api\v2\Senna\Merchant\Membership\MembershipController@getDetail');
                Route::post('/member/{merchantId}', 'Api\v2\Senna\Merchant\Membership\MembershipController@getMember');
                Route::get('/list-commission', 'Api\v2\Senna\Merchant\Membership\MembershipController@listCommission');
                Route::post('/history/{merchantId}', 'Api\v2\Senna\Merchant\Membership\MembershipController@history');

            });
    }
    public function join($merchantId)
    {
        try {
            if(is_null(MerchantUtil::checkMerchant($merchantId)))
            {
                return $this->message::getJsonResponse(404,trans('custom.account_not_found'),[]);

            }

            if(is_null(MerchantUtil::checkSubscription($merchantId)))
            {
                return $this->message::getJsonResponse(404,'Kamu belum berlangganan,silahkan berlangganan terlebih dahulu',[]);

            }
            $check=$this->membership::where('md_merchant_id',$merchantId)->first();
            if($check)
            {
                return $this->message::getJsonResponse(404,'Kamu sudah bergabung menjadi membership',[]);

            }
            $code=CodeGenerator::generateRefCode('M',$merchantId);
            $data=new $this->membership;
            $data->md_merchant_id=$merchantId;
            $data->refferal_code=$code;
            $data->refferal_link=url('/').'/register/?code='.$code;
            $data->is_active=1;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function getDetail($merchantId)
    {
        try {
            $data=$this->membership::
            select([
                'sm_merchant_memberships.*',
                'm.id as merchant_id',
                'm.name as merchant_name',
                'm.description as merchant_description',
                'm.address as address_merchant',
                'u.fullname as owner_name',
                'u.balance',
                DB::raw("(".config('senna_membership.kurs.senna_kasir')." * sm_merchant_memberships.point) convertion_point_to_balance"),
                DB::raw("(Select count(*) from sm_merchant_membership_details as sm where sm.sm_merchant_membership_id=sm_merchant_memberships.id) as sum_merchant")
            ])
                ->join('md_merchants as m','m.id','sm_merchant_memberships.md_merchant_id')
                ->join('md_users as u','u.id','m.md_user_id')
                ->where('sm_merchant_memberships.is_active',1)
                ->where('sm_merchant_memberships.md_merchant_id',$merchantId)
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function getMember($merchantId,Request $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;

            $data=$this->membership::
            select([
                'd.*',
                'm.id as merchant_id',
                'm.name as merchant_name',
                'm.description as merchant_description',
                'm.address as address_merchant'
            ])
                ->join('sm_merchant_membership_details as d','d.sm_merchant_membership_id','sm_merchant_memberships.id')
                ->join('md_merchants as m','m.id','d.member_merchant_id')
                ->where('d.is_deleted',0)
                ->where('sm_merchant_memberships.md_merchant_id',$merchantId)
                ->orderBy('d.id','DESC')
                ->skip($offset)
                ->take($limit)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function listCommission()
    {
        try{
            $data=$this->commission::
            select([
                'sm_merchant_commisions.id',
                's.name',
                's.price',
                'sm_merchant_commisions.fee'
            ])
                ->join('md_subscriptions as s','s.id','sm_merchant_commisions.md_subscription_id')
                ->orderby('sm_merchant_commisions.fee','DESC')
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }
    public function history($merchantId,Request  $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;

            $temp=$this->merchantHistoryRedeem::
            select([
                'sm_merchant_history_redeem_points.*',
                's.name as redeem_for',
                'p.point'
            ])
                ->join('sm_merchant_redeem_points as p','p.id','sm_merchant_history_redeem_points.sm_merchant_redeem_point_id')
                ->join('md_transaction_status as t','t.id','sm_merchant_history_redeem_points.md_transaction_status_id')
                ->join('md_subscriptions as s','s.id','p.redeemable_id')
                ->where('p.redeemable_type','App\Models\MasterData\Subscription')
                ->where('sm_merchant_history_redeem_points.md_merchant_id',$merchantId);

            $data=$this->merchantHistoryRedeem::
            select([
                'sm_merchant_history_redeem_points.*',
                's.name as redeem_for',
                'p.point'
            ])
                ->join('sm_merchant_redeem_points as p','p.id','sm_merchant_history_redeem_points.sm_merchant_redeem_point_id')
                ->join('md_transaction_status as t','t.id','sm_merchant_history_redeem_points.md_transaction_status_id')
                ->join('sc_products as s','s.id','p.redeemable_id')
                ->where('p.redeemable_type','App\Models\SennaToko\Product')
                ->where('sm_merchant_history_redeem_points.md_merchant_id',$merchantId)
                ->union($temp)
                ->orderBy('created_at','DESC')
                ->skip($offset)
                ->take($limit)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

}

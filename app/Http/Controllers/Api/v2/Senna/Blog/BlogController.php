<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Http\Controllers\Api\v2\Senna\Blog;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Blog\CategoryBlog;
use App\Models\Blog\Posts;
use App\Models\MasterData\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class BlogController extends Controller
{


    protected $blog;
    protected $blogCategory;
    protected $message;

    public function __construct()
    {
        $this->blog=Posts::class;
        $this->blogCategory=CategoryBlog::class;
        $this->message=Message::getInstance();
    }


    public function routeApi()
    {
        Route::prefix('v2/blog/')
            ->middleware('etag')
            ->group(function(){
                Route::post('/all', 'Api\v2\Senna\Blog\BlogController@getAll');
                Route::get('/category', 'Api\v2\Senna\Blog\BlogController@getCategory');

            });
    }

    public function getAll(Request  $request)
    {
        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?5:$request->limit;
            $categoryId=$request->category_id;

            $data=$this->blog::where('is_publish',1);

            if(!is_null($categoryId))
            {
                $data->where('md_bg_category_id',$categoryId);
            }
            $result=$data->skip($offset)
                ->take($limit)
                ->orderBy('id','desc')->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            foreach ($result as $key =>$item)
            {
                $item->link=route('blog.detail', [
                    'year'=>$item->created_at->year,
                    'month'=>$item->created_at->month,
                    'day'=>$item->created_at->day,
                    'slug' => $item->slug
                ]);
                $item->image_preview="https://sennakreasi-prod.s3-ap-southeast-1.amazonaws.com/".$item->image_preview;
            }


            return $this->message::getJsonResponse(200,trans('message.201'),$result);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }


    public function getCategory()
    {
        try{
            $data=$this->blogCategory::where('id','>=',6)->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }
}

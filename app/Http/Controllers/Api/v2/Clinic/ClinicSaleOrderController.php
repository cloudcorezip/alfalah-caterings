<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Clinic;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ReturSaleOrder;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\DiscountProduct;
use App\Models\Accounting\CoaDetail;
use App\Models\SennaToko\StockInventory;
use App\Models\SennaToko\StockSaleMapping;
use App\Models\SennaToko\ClinicMedicalRecord;
use App\Models\SennaToko\ClinicReservation;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Utils\Winpay\WinpayUtil;
use App\Utils\Accounting\AccUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Models\Accounting\MerchantShift;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use QrCode;
use Ramsey\Uuid\Uuid;
use App\Http\Controllers\Api\v2\Senna\Merchant\Cashier\InvAccCoa\InvAccCoaController;
use App\Models\SennaToko\Material;

class ClinicSaleOrderController extends Controller
{
    protected $order;
    protected $message;
    protected $customer;
    protected $product;
    protected $ar;
    protected $shift;
    protected $invAccCoa;

    public function __construct()
    {
        $this->order=SaleOrder::class;
        $this->message=Message::getInstance();
        $this->customer=Customer::class;
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->shift=MerchantShift::class;
        $this->invAccCoa = new InvAccCoaController();
    }

    public function routeApi()
    {
        Route::prefix('v2/clinic/sale-order')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('create/{merchantId}', [static::class,'create']);
                Route::post('save', [static::class, 'save']);
            });
    }

    private function _getInvWarehouseId($merchantId)
    {
        $response = $this->invAccCoa->getDefaultWarehouse($merchantId);
        $data = $response->getData();
        
        if($data->code == 200){
            return $data->data->id;
        } else {
            return false;
        }
    }

    private function _getCoaSale($merchantId)
    {
        $response = $this->invAccCoa->getCoaSale($merchantId);
        $data = $response->getData();

        if($data->code == 200){
            $returnData = [];
            foreach($data->data as $key=>$item)
            {
                array_push($returnData, [
                    "id"=>$item->id,
                    "code"=>$item->code,
                    "name"=>$item->name,
                    "type"=>$item->type
                ]);
            }

            return $returnData;
        } else {
            return false;
        }
    }

    private function _getCoaAr($merchantId)
    {
        $response = $this->invAccCoa->getCoaAr($merchantId);
        $data = $response->getData();

        if($data->code == 200){
            return $data->data->id;
        } else {
            return false;
        }
    }


    public function create(Request $request,$merchantId)
    {
        try {
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);

            $checkDoubleAktiva=DB::select("
                        select count(*) as total from acc_coa_categories
                        where md_merchant_id=$merchantId and code='1.1.07'
                        ");

            if(!empty($checkDoubleAktiva))
            {
                DB::statement("delete from acc_coa_categories where md_merchant_id=$merchantId and code='1.1.07'");
            }

            $merchant = Merchant::find($merchantId);
            $medicalRecordId = $request->medical_record_id;
            $medicalRecord = ClinicMedicalRecord::find($medicalRecordId);
            if(is_null($medicalRecord))
            {
                return $this->message::getJsonResponse(404, 'Data rekam medis tidak tersedia !', []);
            }
            $reservation = ClinicReservation::find($medicalRecord->clinic_reservation_id);
            if($reservation->status != 1){
                return $this->message::getJsonResponse(404,trans('Appointment belum disetejui atau sudah selesai. Data tidak dapat disimpan !'),[]);    
            }

            $checkSale = $this->order::find($medicalRecord->sc_sale_order_id);
            if(!is_null($checkSale))
            {
                return $this->message::getJsonResponse(404, 'Data penjualan untuk rekam medis sudah ada !', []);
            }

            $isKeep = (is_null($request->is_keep_transaction)) ? 0 : $request->is_keep_transaction;
            if ($request->is_debet == 0) {
                if ($isKeep == 0) {
                    if (is_null($request->md_transaction_type_id)) {
                        return $this->message::getJsonResponse(404, 'Metode Pembayaran belum dipilih', []);
                    }
                }
            }
            if (is_null($request->md_sc_shipping_category_id)) {
                return $this->message::getJsonResponse(404, 'Metode Pengiriman belum dipilih', []);
            }
            if (is_null($request->is_with_load_shipping)) {
                return $this->message::getJsonResponse(404, 'Centang beban pengiriman belum dipilih', []);

            }
            $warehouseId = $this->_getInvWarehouseId($merchantId);
            if (!$warehouseId) {
                return $this->message::getJsonResponse(404, 'Parameter gudang belum terisi', []);

            }

            $invMethod = $merchant->md_inventory_method_id;
            if (is_null($invMethod)) {
                return $this->message::getJsonResponse(404, 'Parameter metode persediaan belum terisi', []);
            }

            //dalam bentuk array
            $coaSale = $this->_getCoaSale($merchantId);
            if (!$coaSale) {
                return $this->message::getJsonResponse(404, 'Parameter coa penjualan belum terisi', []);
            }
            $coaAr = $this->_getCoaAr($merchantId);
            if (!$coaAr) {
                return $this->message::getJsonResponse(404, 'Parameter coa piutang belum terisi', []);
            }

            $purchase = new $this->order;
            $validator = Validator::make($request->all(), $purchase->ruleOfflineOrder);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, trans('error'), [$error]);
            }
            if (!is_null($request->sc_customer_id)) {
                $purchase->sc_customer_id = $request->sc_customer_id;
            }
            if ($request->is_debet == 0) {
                if ($isKeep == 0) {
                    $trans = explode('_', $request->md_transaction_type_id);
                }

            } elseif ($request->is_debet == 1 && $request->paid_nominal > 0) {

                $trans = explode('_', $request->md_transaction_type_id);

            } else {

                $trans = null;
            }
            if (is_null($request->code)) {
                $code = CodeGenerator::generateSaleOrder($merchantId);
                $purchase->code = $code;
            } else {
                $code = $request->code;
                $purchase->code = $request->code;
            }

            $purchase->queue_code = $request->queue_code;
            $purchase->courier_name = $request->courier_name;
            $purchase->service_courier_name = $request->service_courier_name;
            $purchase->destination = $request->destination;
            $purchase->distance = $request->distance;
            $purchase->weight = $request->weight;
            $purchase->qr_code = '-';
            $purchase->is_debet = $request->is_debet;
            $purchase->is_with_retur = 0;
            $purchase->change_money = $request->change_money;
            $purchase->sc_customer_id = $request->sc_customer_id;
            if ($request->md_sc_shipping_category_id == 1) {
                $purchase->is_with_load_shipping = 1;

            } else {
                $purchase->is_with_load_shipping = 0;

            }

            $purchase->total = $request->total;
            $purchase->md_merchant_id = $merchantId;
            $purchase->paid_nominal = $request->paid_nominal;
            if ($isKeep == 0) {
                $purchase->md_sc_transaction_status_id = ($request->is_debet == 0) ? TransactionStatus::PAID : TransactionStatus::UNPAID;

            } else {
                $purchase->md_sc_transaction_status_id = TransactionStatus::UNPAID;

            }
            $purchase->md_sc_shipping_category_id = $request->md_sc_shipping_category_id;
            $purchase->tax = $request->tax;

            $purchase->discount_primary_id = $request->discount_primary_id;
            $purchase->promo = $request->promo;
            $purchase->promo_percentage = $request->promo_percentage;
            $purchase->tax_percentage = $request->tax_percentage;
            $purchase->shipping_cost = $request->shipping_cost;
            $purchase->timezone = $request->timezone;
            $purchase->created_by = $request->created_by;
            $purchase->assign_to_user_id = $request->assign_to_user_id;
            $purchase->sync_id = $merchantId . Uuid::uuid4()->toString();
            if ($request->is_debet == 0) {
                if ($isKeep == 0) {
                    $purchase->coa_trans_id = $trans[1];
                    $purchase->md_transaction_type_id = $trans[0];
                }
            }

            if ($isKeep == 1) {
                $purchase->is_keep_transaction = 1;
            }

            $purchase->is_approved_shop = 1;
            $purchase->created_at = date('Y-m-d H:i:s');
            $purchase->updated_at = date('Y-m-d H:i:s');
            $purchase->is_from_clinic = 1;
            $purchase->save();
            $details = [];
            $timestamp = date('Y-m-d H:i:s');
            $detail = $request->details;
            if (empty($detail)) {
                return $this->message::getJsonResponse(404, 'Data item penjualan tidak boleh kosong', []);

            }

            $productId = [];
            $productIds = "";
            foreach ($detail as $n => $i) {
                $productId[] = $i['sc_product_id'];
                $productIds .= ($n == 0) ? $i['sc_product_id'] : "," . $i["sc_product_id"];
            }

            $products = collect(DB::select("
                select
                  sp.*,
                  sp.inv_id as coa_inv_id,
                  sp.hpp_id as coa_hpp_id,
                  u.name as default_unit_name,
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          spmu.*,
                          mu.name
                        FROM
                          sc_product_multi_units spmu
                          join md_units mu on mu.id = spmu.md_unit_id
                        WHERE
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                      ) jd
                  ) AS multi_units
                from
                  sc_products sp
                left join md_units u on u.id = sp.md_unit_id
                where
                  sp.id in ($productIds)
            "));
            $productTemporary = [];
            $sumcost = 0;
            $is_from_package = 0;

            foreach ($detail as $j => $item) {
                $product = $products->firstWhere('id', $item['sc_product_id']);
                $profit=0;
                if( $product->md_sc_product_type_id==4){
                    $is_from_package=1;
                    $productChild= Material::join('sc_products as p','p.id','child_sc_product_id')
                    ->where('parent_sc_product_id', $product->id)
                    ->where('sc_product_raw_materials.is_deleted',0)
                    ->get();
                    $sumChild=0;
                    foreach ($productChild as $p => $c) {
                        array_push($productTemporary, [
                            'id' => $c->child_sc_product_id,
                            'name' => $c->name,
                            'code' => $c->code,
                            'stock' => $c->stock,
                            'selling_price' => $c->selling_price,
                            'purchase_price' => $c->purchase_price,
                            'quantity' => $item['quantity']*$c->quantity,
                            'is_from_package'=>1
                        ]);
                        $sumChild+=$c->total;
                    }

                    if(!is_null($product->cost_other) && $product->cost_other!=[]){
                        foreach(json_decode($product->cost_other) as $sum){
                            $sumCost+=($sum->amount*$item['quantity']);
                        }
                    }

                    // $profit=$product->selling_price-$sumCost-$sumChild;
                    // dd($profit);
                }
                $isMulti = isset($item['is_multi_unit']) ? $item['is_multi_unit'] : 0;
                $MultiId = isset($item['multi_unit_id']) ? $item['multi_unit_id'] : null;
                $unit_name = "";
                $mQuantity = $item['quantity'];
                $mPrice = $item['price'];
                $sub_total = $item['sub_total'];
                $multi_quantity = 0;
                if ($isMulti == 1 && !is_null($MultiId)) {
                    $multi_unit = collect(json_decode($product->multi_units));
                    $unit = $multi_unit->firstWhere('id', $item['multi_unit_id']);
                    $unit_name = $unit->name;
                    $multi_quantity = $item['quantity'];
                    $mQuantity = $unit->quantity * $item['quantity'];
                    $priceAfterAdjust = $item['price'] * $item['quantity'];
                    $mPrice = $priceAfterAdjust / $mQuantity;
                }else{
                    $unit_name=$product->default_unit_name;
                }

                if ($isKeep == 0) {
                    if ($product->is_with_stock == 1 && $product->md_sc_product_type_id!=4) {

                        array_push($productTemporary, [
                            'id' => $product->id,
                            'name' => $product->name,
                            'code' => $product->code,
                            'stock' => $product->stock,
                            'selling_price' => $mPrice,
                            'purchase_price' => $product->purchase_price,
                            'quantity' => $mQuantity,
                            'is_from_package'=>0
                        ]);

                    }
                }
                $isBonus = isset($item['is_bonus']) ? $item['is_bonus'] : 0;
                $noteOrder = isset($item['note_order']) ? $item['note_order'] : "";
                $startRent = isset($item['start_rent']) ? $item['start_rent'] : null;
                $endRent = isset($item['end_rent']) ? $item['end_rent'] : null;

                $details[] = [
                    'sc_product_id' => $item['sc_product_id'],
                    'quantity' => $mQuantity,
                    'multi_quantity' => $multi_quantity,
                    'is_bonus' => $isBonus,
                    'is_multi_unit' => $isMulti,
                    'unit_name' => $unit_name,
                    'start_rent' => $startRent,
                    'end_rent' => $endRent,
                    'multi_unit_id' => $MultiId,
                    'ref_discount_id' => $item['ref_discount_id'],
                    'sub_total' => $sub_total,
                    'sc_sale_order_id' => $purchase->id,
                    'profit' => $profit,
                    'price' => $mPrice,
                    'note_order' => $noteOrder,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp,
                    'coa_inv_id' => ($product->is_with_stock == 0) ? null : $product->coa_inv_id,
                    'coa_hpp_id' => ($product->is_with_stock == 0) ? null : $product->coa_hpp_id
                ];
            }


            DB::table('sc_sale_order_details')->insert($details);

            $productInsert=[
                'productTemporary'=>$productTemporary,
                'isKeep'=>$isKeep,
                'warehouseId'=>$warehouseId,
                'createdBy'=>$request->created_by,
                'timestamp'=>$timestamp,
                'invMethodId'=>$invMethod,
                'code'=>$purchase->code,
                'id'=>$purchase->id,
                'userId'=>$purchase->getMerchant->md_user_id,
                'merchantId'=>$merchantId,
                'coaSale'=>$coaSale,
                'paidNominal'=>$request->paid_nominal,
                'timezone'=>$request->timezone,
                'isDebet'=>$purchase->is_debet,
                'total'=>$request->total,
                'dueDate'=>$request->due_date,
                'trans1'=>($request->is_debet == 1 && $request->paid_nominal >0)?$trans[1]:null,
                'coaAr'=>$coaAr
            ];

            $resultJson=[
                'sale_id'=>$purchase->id,
                'sale_code'=>$purchase->code,
                'md_sc_transaction_status_id'=>$purchase->md_sc_transaction_status_id
            ];

            $reservation->status = 2;
            $reservation->save();

            $medicalRecord->status = 1;
            $medicalRecord->sc_sale_order_id = $purchase->id;
            $medicalRecord->save();

            DB::commit();
            Session::put('sale_mobile_'.$purchase->id,$productInsert);
            Artisan::call('sale:mobile', ['--sale_id' =>$purchase->id]);
            return $this->message::getJsonResponse(200,'Data rekam medis berhasil disimpan !',$resultJson);
        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, 'Terjadi kesalahan disistem,cobalah beberapa saat lagi', []);

        }
    }

    public function save(Request $request)
    {
        try {
            DB::beginTransaction();
            $medicalRecordId = $request->medical_record_id;
            $saleOrderId = $request->sc_sale_order_id;

            $data= ClinicMedicalRecord::where("id", $medicalRecordId)->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,'Data rekam medis tidak ditemukan !',[]);    
            }

            $reservation = ClinicReservation::where("id", $data->clinic_reservation_id)->first();

            if(is_null($reservation))
            {
                return $this->message::getJsonResponse(404,'Data reservasi tidak ditemukan !',[]);
            }

            $sale = SaleOrder::where("id", $saleOrderId)->first();

            if(is_null($sale))
            {
                return $this->message::getJsonResponse(404,'Data penjualan tidak ditemukan !',[]);
            }

            $data->sc_sale_order_id = $saleOrderId;
            $data->status = 1;
            $data->save();

            $reservation->status = 2;
            $reservation->save();

            $sale->is_from_clinic = 1;
            $sale->save();

            DB::commit();

            return $this->message::getJsonResponse(200,'Data rekam medis berhasil disimpan !',$data);

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, 'Terjadi kesalahan disistem,cobalah beberapa saat lagi', []);
        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Clinic;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Classes\Singleton\CodeGenerator;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;
use Modules\Merchant\Entities\Toko\ReservationEntity;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ClinicMedicalRecord;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\MerchantBranch;
use Ramsey\Uuid\Uuid;

class ClinicReservationController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/clinic/reservation')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/all',[static::class,'all']);
                Route::post('/detail',[static::class,'detail']);
                Route::post('/update-status',[static::class,'updateStatus']);
        });
    }

    public function all(Request $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $merchantId = $request->merchant_id;
            $userId = $request->user_id;
            $date = $request->date;
            $staff = MerchantStaff::where('md_user_id', $userId)
                                    ->first();
            
            if(is_null($staff))
            {
                return $this->message::getJsonResponse(404,'Data dokter tidak ditemukan !',[]);
            }
            
            $data = DB::select("
            select
                cr.id,
                cr.reservation_number,
                cr.reservation_date,
                cr.queue_number,
                cr.status,
                sc.code as patient_code,
                sc.name as patient_fullname,
                scl.name as patient_category_name,
                sp.name as service_name,
                u.id as user_id,
                u.fullname as doctor_name
            from 
                clinic_reservations cr
            join
                sc_customers sc on sc.id = cr.sc_customer_id
            left join
                sc_customer_level scl on scl.id = sc.sc_customer_level_id
            join
                sc_products sp on sp.id = cr.sc_product_id
            join
                md_merchant_staff mms on mms.id = cr.md_merchant_staff_id
            join
                md_users u on u.id = mms.md_user_id
            where
                cr.is_deleted = 0
                and
                cr.status = 0
                and
                cr.md_merchant_id = $merchantId
                and
                cr.md_merchant_staff_id = $staff->id
                and
                cr.reservation_date::date between '$date' and '$date'
            order by
                cr.reservation_date desc
            offset
                $offset
            limit
                $limit
            "); 

            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function detail(Request $request)
    {
        try {
            $id = $request->id;
            $userId = $request->user_id;
            $staff = MerchantStaff::where('md_user_id', $userId)
                                    ->first();
            
            if(is_null($staff))
            {
                return $this->message::getJsonResponse(404,'Data dokter tidak ditemukan !',[]);
            }
            
            $data = DB::select("
            select
                cr.id,
                cr.reservation_number,
                cr.queue_number as queue_number,
                cr.reservation_date,
                sc.id as patient_id,
                sc.code as patient_code,
                sc.name as patient_fullname,
                scl.name as patient_category_name,
                sp.name as service_name,
                u.fullname as doctor_name,
                cr.status,
                cmr.id as medical_record_id
            from
                clinic_reservations cr
            join
                sc_customers sc on sc.id = cr.sc_customer_id
            left join
                sc_customer_level scl on scl.id = sc.sc_customer_level_id
            join
                sc_products sp on sp.id = cr.sc_product_id
            join
                md_merchant_staff mms on mms.id = cr.md_merchant_staff_id
            join
                md_users u on u.id = mms.md_user_id
            join
                clinic_medical_records cmr on cmr.clinic_reservation_id = cr.id
            where
                cr.id = $id
                and
                cr.md_merchant_staff_id = $staff->id
            ");

            if(count($data) < 1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            
            return $this->message::getJsonResponse(200,trans('message.201'),$data[0]);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function updateStatus(Request $request)
    {
        try {
            $id = $request->id;
            $status = $request->status;

            $data = ClinicReservation::find($id);

            if(is_null($data)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $data->status = $status;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

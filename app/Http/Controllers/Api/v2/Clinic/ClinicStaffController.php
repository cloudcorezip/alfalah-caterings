<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Clinic;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Classes\Singleton\CodeGenerator;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;
use Modules\Merchant\Entities\Toko\ReservationEntity;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ClinicMedicalRecord;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\User;
use App\Models\MasterData\MerchantBranch;
use Ramsey\Uuid\Uuid;
use Image;
use Illuminate\Support\Facades\Storage;

class ClinicStaffController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/clinic/staff')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/detail/{userId}', [static::class, 'detail']);
                Route::post('/total-customer', [static::class, 'totalCustomer']);
                Route::post('/save', [static::class, 'save']);
        });
    }

    public function detail($userId,Request $request)
    {
        try {
            $data = DB::select("
            select
                mms.id,
                mu.id as user_id,
                coalesce(mu.fullname, '-') as fullname,
                coalesce(mms.license_number, '-') as license_number,
                coalesce(mms.code, '-') as code,
                coalesce(mu.phone_number, '-') as phone_number,
                coalesce(mu.email, '-') as email,
                coalesce(mms.address, '-') as address,
                coalesce(mu.birth_place, '-') as birth_place,
                case
                    when
                        mu.birth_date is null
                    then
                        'null'
                    else
                        mu.birth_date::text
                end as birth_date,
                coalesce(mu.gender, 0) as gender,
                coalesce(mms.md_merchant_staff_category_id, 0) as md_merchant_staff_category_id,
                coalesce(mmsc.name, '-') as category_name,
                coalesce(mms.religion, 0) as religion,
                coalesce(mms.blood_type, '-') as blood_type,
                coalesce(mms.last_education, 0) as last_education,
                mms.is_active,
                coalesce(mu.foto, '-') as foto
            from
                md_merchant_staff mms
            join
                md_users mu on mu.id = mms.md_user_id
            left join
                md_merchant_staff_categories mmsc on mmsc.id = mms.md_merchant_staff_category_id
            where
                mu.id = $userId
            ");

            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data[0]);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function totalCustomer(Request $request)
    {
        try {
            $userId = $request->user_id;
            $data = DB::select("
            select
                count(mu.id) as total
            from 
                md_users mu
            join
                md_merchant_staff mms on mms.md_user_id = mu.id
            join
                clinic_reservations cr on cr.md_merchant_staff_id = mms.id
            join
                clinic_medical_records cmr on cmr.clinic_reservation_id = cr.id
            where
                mu.id = $userId
                and
                cmr.status = 1
            ");

            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data[0]);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }

    }

    public function save(Request $request)
    {
        try{
            date_default_timezone_set($request->_timezone);

            DB::beginTransaction();
            $id=$request->id;

            $data=MerchantStaff::find($id);

            if(is_null($data)){
                return $this->message::getJsonResponse(404,'Data dokter tidak ditemukan !',[]);
            }

            $destinationPath = 'public/uploads/user/'.$data->md_user_id.'/image/';

            if($request->hasFile('image'))
            {
                $file=$request->file('image');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('image'))->resize(300,300)->encode($file->getClientOriginalExtension());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, (string) $image);
            }else{
                if(is_null($id)){
                    $fileName=null;
                }else{
                    $fileName=$data->image;
                }
            }

            $user=User::find($data->md_user_id);

            $user->fullname=$request->name;
            $user->gender = $request->gender;
            $user->phone_number = $request->phone_number;
            $user->birth_place = $request->birth_place;
            $user->birth_date = $request->birth_date;
            $user->foto = $fileName;
            $user->save();

            $data->license_number = $request->license_number;
            $data->blood_type = $request->blood_type;
            $data->last_education = $request->last_education;
            $data->is_active = $request->is_active;
            $data->religion = $request->religion;
            $data->address = $request->address;
            $data->is_create_email = 1;
            $data->md_merchant_staff_category_id = $request->md_merchant_staff_category_id;
            $data->save();

            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Clinic;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Classes\Singleton\CodeGenerator;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Toko\ReservationEntity;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ClinicMedicalRecord;
use App\Models\SennaToko\TemplateDiagnosis;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\MerchantBranch;
use Ramsey\Uuid\Uuid;
use Image;
use Illuminate\Support\Facades\Storage;
use App\Models\SennaToko\Material;


class ClinicMedicalRecordController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/clinic/medical-record')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/get-template/{medicalRecordId}',[static::class,'getTemplate']);
                Route::get('/get-product/{merchantId}',[static::class,'getProduct']);
                Route::post('/save-diagnosis',[static::class,'saveDiagnosis']);
                Route::post('/save-product',[static::class,'saveProduct']);
                Route::post('/save-product-with-type',[static::class,'saveProductWithType']);
                Route::post('/history-by-customer', [static::class, 'historyByCustomer']);
                Route::post('/save-signature', [static::class, 'saveSignature']);
                Route::get('/detail/{medicalRecordId}', [static::class, 'detail']);
                Route::get('/detail-item/{medicalRecordId}', [static::class, 'detailItem']);
                Route::post('/delete-item', [static::class, 'deleteItem']);
        
        });
    }

    public function getTemplate($medical_record_id, Request $request)
    {
        try {
            
            $medicalRecord = ClinicMedicalRecord::find($medical_record_id);
            
            if(is_null($medicalRecord)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $merchantId = $medicalRecord->md_merchant_id;

            if(!is_null($medicalRecord->diagnosis)){
                $data = [];
                $tmpData = json_decode($medicalRecord->diagnosis);

                foreach($tmpData as $key => $item)
                {
                    array_push($data, [
                        "name" => $item->name,
                        "type" => $item->type,
                        "value" => ($item->value == 'null')? null:$item->value,
                        "option" => $item->option,
                        "question" => $item->question,
                        "md_merchant_template_diagnosis_id" => $item->md_merchant_template_diagnosis_id
                    ]);
                }
            } else {
                $data = TemplateDiagnosis::
                        select([
                            'name',
                            'type',
                            DB::raw('null as value'),
                            'option',
                            'question',
                            'id as md_merchant_template_diagnosis_id'
                        ])
                        ->where("md_merchant_id", $merchantId)
                        ->where('is_deleted', 0)
                        ->get();
            }

            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getProduct($merchantId, Request $request)
    {
        try {
            $merchant = Merchant::find($merchantId);

            if(is_null($merchant)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $userId = $merchant->md_user_id;

            $data = DB::select("
            select
                sp.id,
                sp.name,
                coalesce(sp.selling_price, 0) as selling_price,
                coalesce(sp.purchase_price, 0) as purchase_price,
                sp.description,
                sp.is_with_stock,
                mu.name as unit_name
            from 
                sc_products sp
            join
                md_units mu on mu.id = sp.md_unit_id
            where
                sp.is_deleted = 0
                and
                sp.md_sc_product_type_id != 3
                and
                sp.md_user_id = $userId
            ");
        
            if(count($data) < 1){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function saveDiagnosis(Request $request)
    {
        try {
            $medicalRecordId = $request->medical_record_id;
            $data = ClinicMedicalRecord::find($medicalRecordId);

            if(is_null($data)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $merchantId = $data->md_merchant_id;
            
            $merchant = Merchant::find($merchantId);

            if(is_null($merchant)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $userId = $merchant->md_user_id;

            $templateDiagnosis = TemplateDiagnosis::where("md_merchant_id", $merchantId)
                                                    ->get();

            $qa = [];
            $patternText = "/text-/i";
            $patternRadio = "/radio-/i";
            $patternFile = "/file-/i";

            foreach($request->all() as $key => $item){
                if(preg_match($patternText, $key)){
                    $template = $templateDiagnosis->filter(function($c) use($key){
                        return $c->name == $key;
                    })->first();

                    $val = [
                        "md_merchant_template_diagnosis_id" => $template->id,
                        "type" => $template->type,
                        "name" => $key,
                        "question" => $template->question,
                        "option" => $template->option,
                        "value" => ($request->$key == 'null')? null:$request->$key,
                    ];
                    array_push($qa, $val);
                }

                if(preg_match($patternRadio, $key)){
                    $template = $templateDiagnosis->filter(function($c) use($key){
                        return $c->name == $key;
                    })->first();

                    $val = [
                        "md_merchant_template_diagnosis_id" => $template->id,
                        "type" => $template->type,
                        "name" => $key,
                        "question" => $template->question,
                        "option" => $template->option,
                        "value" => $request->$key,
                    ];
                    array_push($qa, $val);
                }
            }

            // upload file
            $destinationPath = 'public/uploads/diagnosis/'.$userId.'/image/';
            $requestFile = [];
            

            if(is_null($data->diagnosis)){
                $requestFile = $templateDiagnosis->filter(function($c) use($key, $patternFile){
                    return $c->type == 'file' && $c->is_deleted == 0;
                })->values();
            } else {
                foreach(json_decode($data->diagnosis) as $key => $item){
                    if($item->type == 'file'){
                        array_push($requestFile, $item);
                    }               
                }
            }

            foreach($requestFile as $k => $i){
                if($request->hasFile($i->name))
                {
                    $file=$request->file($i->name);
                    $fileName= Uuid::uuid4()->toString().'_'.str_replace(' ','_',$file->getClientOriginalName());
                    $image = Image::make(request()->file($i->name))->resize(300,300, function($constraint){
                        $constraint->aspectRatio();
                    })->encode($file->getClientOriginalExtension());
                    $fileName=$destinationPath.$fileName;
                    Storage::disk('s3')->put($fileName, (string) $image);

                    $valFile = [
                        "md_merchant_template_diagnosis_id" => (is_null($data->diagnosis))? $i->id : $i->md_merchant_template_diagnosis_id,
                        "type" => $i->type,
                        "name" => $i->name,
                        "question" => $i->question,
                        "option" => $i->option,
                        "value" => $fileName,
                    ];

                }else{

                    $diagnosa = json_decode($data->diagnosis);   
                    if(!is_null($diagnosa)){
                        $fileObj = array_values(
                            array_filter($diagnosa,function($q)use($i){
                                return $q->name == $i->name;
                            })
                        )[0];
                        $fileName = $fileObj->value;
                        
                    } else {
                        $fileName = null;
                    }

                    $valFile = [
                        "md_merchant_template_diagnosis_id" => (is_null($data->diagnosis))? $i->id : $i->md_merchant_template_diagnosis_id,
                        "type" => $i->type,
                        "name" => $i->name,
                        "question" => $i->question,
                        "option" => $i->option,
                        "value" => $fileName,
                    ];
                }

                
                array_push($qa, $valFile);
            }
            // end upload

            $data->md_merchant_id = $merchantId;
            $data->diagnosis = json_encode($qa);
            $data->note = $request->note;

            $data->save();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function saveProduct(Request $request)
    {
        try {
            $medicalRecordId = $request->medical_record_id;

            if(count($request->services) == 0){
                return $this->message::getJsonResponse(404, 'Data item penjualan tidak boleh kosong', []);
            }

            $data = ClinicMedicalRecord::find($medicalRecordId);

            if(is_null($data)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $productServices = $request->services;

            foreach($productServices as $key => $item)
            {
                
                $findProduct = Product::find($item["sc_product_id"]);

                if(!is_null($findProduct)){
                   if($findProduct->md_sc_product_type_id == 4){
                        $material=Material::
                        select([
                            'sc_product_raw_materials.id',
                            'sc_product_raw_materials.quantity',
                            'p.id as product_id',
                            'p.name as product_name',
                            'p.stock',
                            'p.is_with_stock'
                        ])
                            ->where('sc_product_raw_materials.parent_sc_product_id',$findProduct->id)
                            ->join('sc_products as p','p.id','sc_product_raw_materials.child_sc_product_id')
                            ->where('sc_product_raw_materials.is_deleted',0)
                            ->where('p.is_deleted',0)
                            ->get();

                        foreach($material as $i => $v)
                        {
                            if($v->is_with_stock == 1){
                                if($v->stock < ($item["quantity"] * $v->quantity))
                                {
                                    return $this->message::getJsonResponse(404, "Stock product $v->product_name di dalam paket $findProduct->name tidak mencukupi!", []);
                                }
                            }
                        }
                   }
                   
                }
            }

            $arrayRecipe = $request->recipes;
            $productIds = [];

            if(!is_null($request->recipes)){
                foreach($arrayRecipe as $key => $item){
                    array_push($productIds, $item['sc_product_id']);
                }
    
                $products = Product::whereIn('id', $productIds)->get();
                
                if(!is_null($products)){
                    foreach ($products as $key => $product){
                        $filteredRecipe = array_values(
                            array_filter($arrayRecipe, function($var) use($product){
                                return $product->id == $var['sc_product_id'];
                            })
                        )[0];
                        
                        if($filteredRecipe['quantity'] <= 0)
                        {
                            return $this->message::getJsonResponse(404, "Masukkan quantity untuk $product->name", []);
                        }
                        
                        if($product->stock < $filteredRecipe['quantity'])
                        {
                            return $this->message::getJsonResponse(404, "Stok produk $product->name tidak mencukupi", []);
                        }
                    }
                }
                
            }

            $data->service = json_encode($request->services);
            $data->recipe = json_encode($request->recipes);
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function saveProductWithType(Request $request)
    {
        try {
            $medicalRecordId = $request->medical_record_id;
            $isWithStock = $request->is_with_stock;
            $data = ClinicMedicalRecord::find($medicalRecordId);

            if(is_null($data)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            if(is_null($isWithStock)){
                return $this->message::getJsonResponse(404, 'Tipe produk belum dipilih !', []);
            }

            if($isWithStock == 0){
                if(count($request->products) == 0){
                    return $this->message::getJsonResponse(404, 'Data layanan tidak boleh kosong', []);
                }
                $data->service = json_encode($request->products);
                $data->save();
            } 

            if($isWithStock == 1){
                $arrayProducts = $request->products;
                $productIds = [];
                if(!is_null($request->products)){
                    foreach($arrayProducts as $key => $item){
                        array_push($productIds, $item['sc_product_id']);
                    }
        
                    $products = Product::whereIn('id', $productIds)->get();
                    
                    if(!is_null($products)){
                        foreach ($products as $key => $product){
                            $filteredProduct = array_values(
                                array_filter($arrayProducts, function($var) use($product){
                                    return $product->id == $var['sc_product_id'];
                                })
                            )[0];
                            
                            if($filteredProduct['quantity'] <= 0)
                            {
                                return $this->message::getJsonResponse(404, "Masukkan quantity untuk $product->name", []);
                            }
                            
                            if($product->stock < $filteredProduct['quantity'])
                            {
                                return $this->message::getJsonResponse(404, "Stok produk $product->name tidak mencukupi", []);
                            }
                        }
                    }   
                }

                $data->recipe = json_encode($request->products);
                $data->save();
            }

            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function saveSignature(Request $request)
    {
        try {
            $medicalRecordId = $request->medical_record_id;

            $data = ClinicMedicalRecord::find($medicalRecordId);

            if(is_null($data)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $merchantId = $data->md_merchant_id;
            
            $merchant = Merchant::find($merchantId);

            if(is_null($merchant)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $userId = $merchant->md_user_id;

            $destinationPath = 'public/uploads/signature/'.$userId.'/image/';

            if($request->hasFile('image'))
            {
                $file=$request->file('image');
                $fileName= Uuid::uuid4()->toString().'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('image'))->encode($file->getClientOriginalExtension());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, (string) $image);
            }else{
                if(is_null($medicalRecordId)){
                    $fileName=null;

                }else{
                    $fileName=$data->image;

                }
            }

            $data->signature = $fileName;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);
        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function historyByCustomer(Request $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;

            $medicalRecordId = $request->medical_record_id;
            $customerid = $request->sc_customer_id;

            $data = ClinicMedicalRecord::
            select([
                'clinic_medical_records.id',
                'cr.reservation_date',
                'sp.name as service_name',
                'clinic_medical_records.status as medical_record_status',
                'cr.status as reservation_status',
            ])
                ->join('clinic_reservations as cr', 'cr.id', 'clinic_medical_records.clinic_reservation_id')
                ->join('sc_customers as sc', 'sc.id', 'cr.sc_customer_id')
                ->leftJoin('sc_customer_level as scl', 'scl.id', 'sc.sc_customer_level_id')
                ->join('sc_products as sp', 'sp.id', 'cr.sc_product_id')
                ->where('cr.sc_customer_id', $customerid)
                ->where('clinic_medical_records.id', '!=', $medicalRecordId)
                ->where('clinic_medical_records.status', 1)
                ->offset($offset)
                ->limit($limit)
                ->orderBy('cr.reservation_date', 'desc')
                ->get();

            if(count($data) < 1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function detail($medicalRecordId, Request $request)
    {
        try {
            
            $data = DB::select("
            select
                cmr.id,
                sp.name as service_name,
                cr.reservation_date,
                cr.reservation_number,
                queue_number,
                cmr.signature as signature,
                mu.fullname as doctor_name,
                sc.name as patient_name,
                sc.id as patient_id,
                coalesce(
                    (
                        select
                            jsonb_agg(s.*) as service
                        from 
                            (
                                select
                                    s.v->>'sc_product_id' as sc_product_id,
                                    spps.name,
                                    s.v->>'quantity' as qty,
                                    s.v->>'price' as price,
                                    s.v->>'sub_total' as sub_total,
                                    spps.is_with_stock,
                                    spps.md_unit_id,
                                    mu.name as unit_name
                                from 
                                    clinic_medical_records cmrrs
                                join lateral 
                                    JSONB_ARRAY_ELEMENTS(cmrrs.service) as s(v) ON TRUE
                                join 
                                    sc_products spps ON (s.v->>'sc_product_id')::text::int = spps.id
                                join
                                    md_units mu on mu.id = spps.md_unit_id
                                where
                                    cmrrs.id = cmr.id
                            ) as s
                        ), '[]'
                ) as service,
                coalesce(
                    (
                        select
                            jsonb_agg(r.*) as recipe
                        from 
                            (
                                select
                                    r.v->>'sc_product_id' as sc_product_id,
                                    sppr.name,
                                    r.v->>'quantity' as qty,
                                    r.v->>'price' as price,
                                    r.v->>'sub_total' as sub_total,
                                    sppr.is_with_stock,
                                    sppr.md_unit_id,
                                    mu.name as unit_name
                                from 
                                    clinic_medical_records cmrrr
                                join lateral 
                                    JSONB_ARRAY_ELEMENTS(cmrrr.recipe) as r(v) ON TRUE
                                join 
                                    sc_products sppr ON (r.v->>'sc_product_id')::text::int = sppr.id
                                join
                                    md_units mu on mu.id = sppr.md_unit_id
                                where
                                    cmrrr.id = cmr.id
                            ) as r
                        ), '[]'
                ) as recipe,
                coalesce(sso.id, 0) as sale_order_id,
                (CASE
                    WHEN
                        cmr.status = 0
                    THEN
                        coalesce(
                            (
                                select
                                    jsonb_agg(dt.*) as detail
                                from
                                (
                                    select
                                        s.v->>'sc_product_id' as sc_product_id,
                                        sps.name as product_name,
                                        s.v->>'quantity' as quantity,
                                        s.v->>'price' as price,
                                        s.v->>'sub_total' as sub_total,
                                        sps.is_with_stock,
                                        0 as promo
                                    from 
                                        clinic_medical_records cmrs
                                    join lateral 
                                        JSONB_ARRAY_ELEMENTS(cmrs.service) as s(v) ON TRUE
                                    join 
                                        sc_products sps ON (s.v->>'sc_product_id')::text::int = sps.id
                                    where
                                        cmrs.id = cmr.id
                                union
                                    select
                                        r.v->>'sc_product_id' as sc_product_id,
                                        spr.name as product_name,
                                        r.v->>'quantity' as quantity,
                                        r.v->>'price' as price,
                                        r.v->>'sub_total' as sub_total,
                                        spr.is_with_stock,
                                        0 as promo
                                    from 
                                        clinic_medical_records cmrr
                                    join lateral 
                                        JSONB_ARRAY_ELEMENTS(cmrr.recipe) as r(v) ON TRUE
                                    join 
                                        sc_products spr ON (r.v->>'sc_product_id')::text::int = spr.id
                                    where
                                        cmrr.id = cmr.id
                                ) as dt
                            ),'[]'
                        )
                    ELSE
                        coalesce(
                            (
                                select
                                    jsonb_agg(dt.*) AS jsonb_agg
                                from
                                (
                                    select
                                        spp.id as sc_product_id,
                                        spp.name as product_name,
                                        spp.is_with_stock,
                                        (CASE 
                                            WHEN
                                                ssod.multi_quantity != 0
                                            THEN
                                                ssod.multi_quantity
                                            ELSE
                                                ssod.quantity
                                        END) as qty,
                                        ssod.price,
                                        ssod.sub_total,
                                        (CASE 
                                            WHEN
                                                ssod.multi_quantity != 0
                                            THEN
                                                ssod.unit_name
                                            ELSE
                                                mu.name
                                        END) as unit_name,
                                        (CASE
                                            WHEN 
                                                ssod.sub_total != ssod.price * ssod.quantity AND ssod.multi_quantity = 0 
                                            THEN
                                                ssod.price * ssod.quantity - ssod.sub_total
                                            WHEN
                                                ssod.sub_total != ssod.price * ssod.multi_quantity AND ssod.multi_quantity != 0
                                            THEN
                                                ssod.price * ssod.quantity - ssod.sub_total
                                            ELSE
                                                0
                                        END) as promo
                                    from
                                        sc_sale_orders
                                    join
                                        sc_sale_order_details ssod on ssod.sc_sale_order_id = sc_sale_orders.id
                                    join
                                        sc_products spp on spp.id = ssod.sc_product_id
                                    join
                                        md_units mu on mu.id = spp.md_unit_id
                                    where
                                        sc_sale_orders.id = sso.id
                                    order by
                                        ssod.id ASC
                                ) as dt
                            ),'[]'
                        )
                END
                ) as details,
                coalesce(
                    (
                        select 
                            jsonb_agg(hs.*) AS js
                        from
                            (
                                select
                                    muu.id as md_user_id,
                                    muu.fullname
                                from 
                                    sc_sale_orders ssos
                                join lateral 
                                    JSONB_ARRAY_ELEMENTS(ssos.assign_to_user_helper) AS e(usr) ON TRUE
                                join 
                                    md_users muu ON (e.usr->>'id')::text::int = muu.id
                                where
                                    ssos.id = sso.id
                                order by
                                    ssos.id ASC
                            ) as hs
                    ),'[]' 
                ) as helper_staff,
                coalesce(
                    (
                        select
                            jsonb_agg(d.*) as diagnosis
                        from 
                            (
                                select
                                    d.v->>'name' as name,
                                    d.v->>'type' as type,
                                    d.v->>'value' as value,
                                    d.v->>'option' as option,
                                    d.v->>'question' as question,
                                    d.v->>'md_merchant_template_diagnosis_id' as md_merchant_template_diagnosis_id
                                from 
                                    clinic_medical_records cmrr,
                                    JSONB_ARRAY_ELEMENTS(cmrr.diagnosis) as d(v)
                                where
                                    cmrr.id = cmr.id
                            ) as d
                        ), '[]'
                ) as diagnosis,
                cmr.note as note,
                coalesce(sso.promo, 0) as promo,
                coalesce(sso.tax, 0) as tax,
                coalesce(sso.admin_fee, 0) as admin_fee,
                coalesce(sso.total, 0) as total,
                cr.status as status
            from
                clinic_medical_records cmr
            join
                clinic_reservations cr on cr.id = cmr.clinic_reservation_id
            join
                sc_customers sc on sc.id = cr.sc_customer_id
            join
                md_merchant_staff mms on mms.id = cr.md_merchant_staff_id
            join 
                md_users mu on mu.id = mms.md_user_id
            join
                sc_products sp on sp.id = cr.sc_product_id
            left join
                sc_sale_orders sso on sso.id = cmr.sc_sale_order_id
            where
                cmr.id = $medicalRecordId               
            ");
            
            if(count($data) < 1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $decodeDiagnosis = json_decode($data[0]->diagnosis);
            $diagnosis = [];

            foreach($decodeDiagnosis as $key => $item){
                if($item->type == 'text'){
                    array_push($diagnosis, [
                        'name' => $item->name,
                        'type' => $item->type,
                        'value' => ($item->value == 'null')? null:$item->value,
                        'option' => $item->option,
                        'question' => $item->question,
                        'md_merchant_template_diagnosis_id' => $item->md_merchant_template_diagnosis_id
                    ]);
                }

                if($item->type == 'radio'){
                    array_push($diagnosis, [
                        'name' => $item->name,
                        'type' => $item->type,
                        'value' => $item->value,
                        'option' => $item->option,
                        'question' => $item->question,
                        'md_merchant_template_diagnosis_id' => $item->md_merchant_template_diagnosis_id
                    ]);
                }

                if($item->type == 'file'){
                    array_push($diagnosis, [
                        'name' => $item->name,
                        'type' => $item->type,
                        'value' => $item->value,
                        'option' => $item->option,
                        'question' => $item->question,
                        'md_merchant_template_diagnosis_id' => $item->md_merchant_template_diagnosis_id
                    ]);
                }
            }

            $data[0]->diagnosis = $diagnosis;

            return $this->message::getJsonResponse(200, trans('message.201'), $data[0]);
            
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function detailItem($medicalRecordId, Request $request)
    {
        try {
            $data = DB::select("
            select
                cmr.id,
                cr.status,
                (CASE
                    WHEN
                        cmr.status = 0
                    THEN
                        coalesce(
                            (
                                select
                                    jsonb_agg(dt.*) as detail
                                from
                                (
                                    select
                                        s.v->>'sc_product_id' as sc_product_id,
                                        sps.name as product_name,
                                        s.v->>'quantity' as quantity,
                                        s.v->>'price' as price,
                                        s.v->>'sub_total' as sub_total,
                                        sps.is_with_stock,
                                        0 as promo
                                    from 
                                        clinic_medical_records cmrs
                                    join lateral 
                                        JSONB_ARRAY_ELEMENTS(cmrs.service) as s(v) ON TRUE
                                    join 
                                        sc_products sps ON (s.v->>'sc_product_id')::text::int = sps.id
                                    where
                                        cmrs.id = cmr.id
                                union
                                    select
                                        r.v->>'sc_product_id' as sc_product_id,
                                        spr.name as product_name,
                                        r.v->>'quantity' as quantity,
                                        r.v->>'price' as price,
                                        r.v->>'sub_total' as sub_total,
                                        spr.is_with_stock,
                                        0 as promo
                                    from 
                                        clinic_medical_records cmrr
                                    join lateral 
                                        JSONB_ARRAY_ELEMENTS(cmrr.recipe) as r(v) ON TRUE
                                    join 
                                        sc_products spr ON (r.v->>'sc_product_id')::text::int = spr.id
                                    where
                                        cmrr.id = cmr.id
                                ) as dt
                            ),'[]'
                        )
                    ELSE
                        coalesce(
                            (
                                select
                                    jsonb_agg(dt.*) AS jsonb_agg
                                from
                                (
                                    select
                                        spp.id as sc_product_id,
                                        spp.name as product_name,
                                        spp.is_with_stock,
                                        (CASE 
                                            WHEN
                                                ssod.multi_quantity != 0
                                            THEN
                                                ssod.multi_quantity
                                            ELSE
                                                ssod.quantity
                                        END) as quantity,
                                        ssod.price,
                                        ssod.sub_total,
                                        (CASE 
                                            WHEN
                                                ssod.multi_quantity != 0
                                            THEN
                                                ssod.unit_name
                                            ELSE
                                                mu.name
                                        END) as unit_name,
                                        (CASE
                                            WHEN 
                                                ssod.sub_total != ssod.price * ssod.quantity AND ssod.multi_quantity = 0 
                                            THEN
                                                ssod.price * ssod.quantity - ssod.sub_total
                                            WHEN
                                                ssod.sub_total != ssod.price * ssod.multi_quantity AND ssod.multi_quantity != 0
                                            THEN
                                                ssod.price * ssod.quantity - ssod.sub_total
                                            ELSE
                                                0
                                        END) as promo
                                    from
                                        sc_sale_orders
                                    join
                                        sc_sale_order_details ssod on ssod.sc_sale_order_id = sc_sale_orders.id
                                    join
                                        sc_products spp on spp.id = ssod.sc_product_id
                                    join
                                        md_units mu on mu.id = spp.md_unit_id
                                    where
                                        sc_sale_orders.id = sso.id
                                    order by
                                        ssod.id ASC
                                ) as dt
                            ),'[]'
                        )
                END
                ) as details
            from
                clinic_medical_records cmr
            left join
                sc_sale_orders sso on sso.id = cmr.sc_sale_order_id
            join
                clinic_reservations cr on cr.id = cmr.clinic_reservation_id
            where
                cmr.id = $medicalRecordId
            ");

            if(count($data) < 1){
                return $this->message::getJsonResponse(404, trans('message.404'), []);
            }
            return $this->message::getJsonResponse(200, trans('message.201'), $data[0]);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function deleteItem(Request $request)
    {
        try {
            $medicalRecordId = $request->medical_record_id;
            $isWithStock = $request->is_with_stock;
            $scProductId = $request->sc_product_id;

            $data = ClinicMedicalRecord::find($medicalRecordId);

            if(is_null($data)){
                return $this->message::getJsonResponse(404, trans('message.404'),[]);
            }

            if($isWithStock == 1){
                $oldData = collect(json_decode($data->recipe));
                $newData = $oldData->filter(function($c) use($scProductId){
                    return $c->sc_product_id != $scProductId;
                })->values();  

                $data->recipe = json_encode($newData);
                $data->save();

                return $this->message::getJsonResponse(200,trans('message.202'),$data);
            } else {
                $oldData = collect(json_decode($data->service));
                $newData = $oldData->filter(function($c) use($scProductId){
                    return $c->sc_product_id != $scProductId;
                })->values();  

                $data->service = json_encode($newData);
                $data->save();

                return $this->message::getJsonResponse(200,trans('message.202'),$data);
            }

        } catch(\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);            
        }
    }

}

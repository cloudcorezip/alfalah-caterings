<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Clinic;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Classes\Singleton\CodeGenerator;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;
use Modules\Merchant\Entities\Toko\ReservationEntity;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ClinicMedicalRecord;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\User;
use App\Models\MasterData\MerchantBranch;
use Ramsey\Uuid\Uuid;

class ClinicMasterController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/clinic/master')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/product', [static::class, 'product']);
                Route::get('/customer-category/{merchantId}', [static::class, 'customerCategory']);
                Route::post('/staff-category', [static::class, 'staffCategory']);
                Route::get('/education', [static::class, 'getEducation']);
                Route::get('/blood-type', [static::class, 'getBloodType']);
                Route::get('/religion', [static::class, 'getReligion']);
                Route::get('/marital-status', [static::class, 'getMaritalStatus']);
                Route::get('/job', [static::class, 'getJob']);
        });
    }

    public function product(Request $request)
    {
        try {
            $merchantId = $request->merchant_id;
            $merchant = Merchant::find($merchantId);
            $type = $request->type;
            $keyword = str_replace("'", "", $request->keyword);
            $offset = is_null($request->offset)? 0 : $request->offset;
            $limit = is_null($request->limit)? 10 : $request->limit;
            if(is_null($merchant)){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            $userId = $merchant->md_user_id;
            $searchQuery = "";
            $whereQuery = "";
            
            if($type != '-1')
            {
                $whereQuery .= " and sp.is_with_stock = ".$type;
            }

            if($keyword != ""){
                $searchQuery .= " and (
                                    sp.name ilike '%$keyword%'
                                    or
                                    sp.code ilike '%$keyword%'
                                )";
            }

            $data= DB::select("
            select
                sp.id,
                coalesce(sp.name, '-') as name,
                coalesce(sp.selling_price, 0) as selling_price,
                coalesce(sp.purchase_price, 0) as purchase_price,
                coalesce(sp.description, '-') as description,
                sp.is_with_stock,
                coalesce(mu.name, '-') as unit_name
            from 
                sc_products as sp
            join 
                md_units mu on mu.id = sp.md_unit_id
            where
                sp.is_deleted = 0
                and
                sp.md_sc_product_type_id != 3
                and
                sp.md_user_id = $userId
                $whereQuery
                $searchQuery
            limit
                $limit
            offset
                $offset
            ");
        
            if(count($data) < 1){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function customerCategory($merchantId, Request $request)
    {
        try {
            $merchant = Merchant::find($merchantId);
            
            if(is_null($merchant))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $userId = $merchant->md_user_id;

            $data = DB::select("
            select
                scl.id,
                coalesce(scl.name, '-') as name,
                coalesce(scl.code, '-') as code
            from
                sc_customer_level scl
            where
                scl.is_deleted = 0
                and
                scl.md_user_id = $userId
            ");

            if(count($data) < 1){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function staffCategory(Request $request)
    {
        try {
            $merchantId = $request->merchant_id;
            $isSpecialist = $request->is_specialist;
            $merchant = Merchant::find($merchantId);
            
            if(is_null($merchant))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            $userId = $merchant->md_user_id;

            $whereQuery = '';
            
            if($isSpecialist != '-1')
            {
                $whereQuery .= " and mmsc.is_specialist = ".$isSpecialist;
            }

            $data = DB::select("
            select
                mmsc.id,
                coalesce(mmsc.name, '-') as name,
                coalesce(mmsc.code, '-') as code
            from
                md_merchant_staff_categories mmsc
            where
                mmsc.is_deleted = 0
                and
                mmsc.md_user_id = $userId
                $whereQuery
            ");

            if(count($data) < 1){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getEducation(Request $request)
    {
        try {
            $education = generateEducation();

            $data = [];

            foreach($education as $key => $item)
            {
                array_push($data, ["id" => $key, "name"=>$item]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getBloodType(Request $request)
    {
        try {
            $data = generateBloodType();
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getReligion(Request $request)
    {
        try {
            $religion = generateReligion();

            $data = [];
            foreach($religion as $key => $item)
            {
                array_push($data, ["id" => $key, "name"=> $item]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'), $data);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getMaritalStatus(Request $request)
    {
        try {
            $maritalStatus = generateMaritalStatus();

            $data = [];
            foreach($maritalStatus as $key => $item)
            {
                array_push($data, ["id" => $key, "name" => $item]);
            }

            return $this->message::getJsonResponse(200, trans('message.201'), $data);

        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);   
        }
    }

    public function getJob(Request $request)
    {
        try {
            $data = DB::select("
            select
                id,
                name
            from 
                md_jobs
            order by id asc
            ");

            if(count($data) < 1){
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]); 
        }
    }

}

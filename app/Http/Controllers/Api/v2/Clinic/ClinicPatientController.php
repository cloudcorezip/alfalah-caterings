<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v2\Clinic;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Utils\Merchant\MerchantUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Classes\Singleton\CodeGenerator;
use App\Utils\MenuUtils;
use App\Models\MasterData\Job;
use Illuminate\Support\Facades\DB;
use Modules\Merchant\Entities\Toko\CategorySpecialistEntity;
use Modules\Merchant\Entities\Toko\CustomerEntity;
use Modules\Merchant\Entities\Toko\SupplierCategoryEntity;
use Modules\Merchant\Entities\Toko\ReservationEntity;
use App\Models\SennaToko\ClinicReservation;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ClinicMedicalRecord;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\MerchantBranch;
use Ramsey\Uuid\Uuid;

class ClinicPatientController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v2/clinic/patient')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/all',[static::class,'all']);
                Route::post('/history', [static::class, 'history']);
                Route::post('/detail', [static::class, 'detail']);
        });
    }

    protected function getFilterMap()
    {
        return [
            "product_id" => "cr.sc_product_id",
            "sc_customer_level_id" => "sc.sc_customer_level_id",
            "gender" => "sc.gender"
        ];
    }

    public function all(Request $request)
    {
        try {
            
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $merchantId = $request->merchant_id;
            $userId = $request->user_id;
            $customerId = $request->sc_customer_id;
            $keyword = str_replace("'","", $request->keyword);
            $searchQuery = '';
            $filterQuery = '';
            
            foreach(self::getFilterMap() as $key => $item)
            {
                if($request->$key != ''){
                    $filterQuery .= " and ".$item." = '".$request->$key."' ";
                }
            }

            $staff = MerchantStaff::where('md_user_id', $userId)
                                    ->first();
            
            if(is_null($staff))
            {
                return $this->message::getJsonResponse(404,'Data dokter tidak ditemukan !',[]);
            }

            if($keyword != "")
            {
                $searchQuery .= "and (
                                        sc.name ilike '%$keyword%'
                                        or
                                        sc.code ilike '%$keyword%'
                                )";
            }

            $data = DB::select("
            select
                sc.id,
                sc.name as patient_name,
                coalesce(sc.code, '-') as patient_code,
                coalesce(scl.name, '-') as patient_category_name
            from
                sc_customers sc
            join
                clinic_reservations cr on cr.sc_customer_id = sc.id
            left join
                sc_customer_level scl on scl.id = sc.sc_customer_level_id
            where
                cr.md_merchant_staff_id = $staff->id
                $filterQuery
                $searchQuery
            group by
                sc.id, scl.name
            order by
                sc.name asc
            offset
                $offset
            limit
                $limit
            ");

            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function history(Request $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $userId = $request->user_id;
            $customerId = $request->sc_customer_id;
            $staff = MerchantStaff::where('md_user_id', $userId)
                                    ->first(); 
            if(is_null($staff))
            {
                return $this->message::getJsonResponse(404,'Data dokter tidak ditemukan !',[]);
            }

            $interval = $request->interval;
            $whereBetween = '';

            if($interval != '-1'){
                if($interval == "last week"){
                    $startDate = Carbon::now()->subDays(6)->toDateString();
                    $endDate = Carbon::now()->toDateString();
                }
                if($interval == "last month"){
                    $startDate = Carbon::now()->subMonth(1)->toDateString();
                    $endDate = Carbon::now()->toDateString();
                }
                if($interval == "last year"){
                    $startDate = Carbon::now()->subYear(1)->toDateString();
                    $endDate = Carbon::now()->toDateString();
                }

                $whereBetween = " and cr.reservation_date::date between '$startDate' and '$endDate' ";
            }

            
            $data = DB::select("
            select
                cmr.id,
                coalesce(sc.name, '-') as patient_name,
                coalesce(sc.code, '-') as patient_code,
                coalesce(scl.name, '-') as patient_category_name,
                coalesce(sp.name, '-') as product_name,
                cr.reservation_date,
                cr.reservation_number,
                cmr.status as medical_record_status,
                cr.status as clinic_reservation_status
            from
                clinic_medical_records cmr
            join
                clinic_reservations cr on cr.id = cmr.clinic_reservation_id
            join
                sc_products sp on sp.id = cr.sc_product_id
            join
                sc_customers sc on sc.id = cr.sc_customer_id
            left join
                sc_customer_level scl on scl.id = sc.sc_customer_level_id
            join
                md_merchant_staff mms on mms.id = cr.md_merchant_staff_id
            join
                md_users mu on mu.id = mms.md_user_id
            where
                cr.sc_customer_id = $customerId
                and
                cr.md_merchant_staff_id = $staff->id
                and
                cr.is_deleted = 0
                $whereBetween
            order by
                cr.id desc
            offset
                $offset
            limit
                $limit
            ");

            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        } catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function detail(Request $request)
    {
        try {
            $customerId = $request->sc_customer_id;
            $userId = $request->user_id;
            $staff = MerchantStaff::where('md_user_id', $userId)
                                    ->first();
            
            if(is_null($staff))
            {
                return $this->message::getJsonResponse(404,'Data dokter tidak ditemukan !',[]);
            }

            $data = DB::select("
            select
                sc.id,
                coalesce(sc.code, '-') as patient_code,
                coalesce(sc.name, '-') as patient_name,
                coalesce(scl.name, '-') as patient_category_name,
                coalesce(sc.gender, '-') as gender,
                coalesce(sc.religion, 0) as religion,
                coalesce(sc.marital_status, 0) as marital_status,
                coalesce(sc.age, 0) as age,
                coalesce(sc.email, '-') as email,
                coalesce(sc.address, '-') as address,
                coalesce(sc.phone_number, '-') as phone_number,
                coalesce(sc.blood_type,'-') as blood_type,
                coalesce(mj.id, 0) as md_job_id,
                coalesce(mj.name,'-') as job_name,
                max(cr.reservation_date) as last_visit
            from 
                sc_customers sc
            left join
                sc_customer_level scl on scl.id = sc.sc_customer_level_id
            left join
                md_jobs mj on mj.id = sc.md_job_id
            left join 
                clinic_reservations cr on cr.sc_customer_id = sc.id
            where
                sc.id = $customerId
                and
                cr.md_merchant_staff_id = $staff->id
            group by
                sc.id, scl.name, mj.name, mj.id
            ");

            if(count($data)<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            return $this->message::getJsonResponse(200,trans('message.201'),$data[0]);
        }catch(\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

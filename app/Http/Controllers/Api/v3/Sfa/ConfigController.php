<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Http\Controllers\Api\v3\Sfa;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Sfa\SfaConfig;
use App\Utils\Merchant\MerchantUtil;
use App\Utils\SFA\SfaInitUtil;
use Illuminate\Support\Facades\Route;

class ConfigController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v3/sfa/')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/config/{merchant_id}',[static::class,'config']);
            });
    }

    public function config($merchantId)
    {
        try {
            $topBranch=MerchantUtil::getHeadBranch($merchantId);
            SfaInitUtil::init($merchantId);
            $data=SfaConfig::where('md_merchant_id',$topBranch)->first();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }
}

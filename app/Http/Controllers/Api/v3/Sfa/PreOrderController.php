<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Http\Controllers\Api\v3\Sfa;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantShift;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\DiscountCoupon;
use App\Models\SennaToko\DiscountProduct;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\MerchantCommission;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\SaleOrderDetail;
use App\Utils\CRM\CommissionUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class PreOrderController extends Controller
{
    protected $order;
    protected $orderDetail;
    protected $message;
    protected $customer;
    protected $product;
    protected $ar;
    protected $shift;
    protected $coupon;
    protected $discount;
    protected $commission;


    public function __construct()
    {
        $this->order=SaleOrder::class;
        $this->orderDetail=SaleOrderDetail::class;
        $this->message=Message::getInstance();
        $this->customer=Customer::class;
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->shift=MerchantShift::class;
        $this->coupon=DiscountCoupon::class;
        $this->discount=DiscountProduct::class;
        $this->commission=MerchantCommission::class;



    }
    public function routeApi()
    {
        Route::prefix('v3/sfa/pre-order')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create/{merchant_id}',[static::class,'create']);
            });
    }

    public function create(Request $request,$merchantId)
    {
        try {
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);
            $purchase = new $this->order;
            $validator = Validator::make($request->all(), $purchase->rulePreOrder);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, [$error]);
            }

            if (is_null($request->code)) {
                $code = CodeGenerator::generateSaleOrder($merchantId);
                $purchase->code = $code;
            } else {
                $purchase->code = $request->code;
            }

            $purchase->note_order = $request->note_order;
            $purchase->qr_code = '-';
            $purchase->sc_customer_id = $request->sc_customer_id;
            $purchase->total_order = $request->total;
            $purchase->md_merchant_id = $merchantId;
            $purchase->md_sc_transaction_status_id = TransactionStatus::UNPAID;
            $purchase->tax_order = $request->tax;
            $purchase->promo_order = $request->promo;
            $purchase->promo_percentage_order = $request->promo_percentage;
            $purchase->tax_percentage_order = $request->tax_percentage;
            $purchase->timezone = $request->timezone;
            $purchase->created_by = $request->created_by;
            $purchase->sync_id = $merchantId . Uuid::uuid4()->toString();
            $purchase->is_approved_shop = 1;
            $purchase->created_at = date('Y-m-d H:i:s');
            $purchase->updated_at = date('Y-m-d H:i:s');
            $purchase->assign_to_user_helper = json_encode($request->user_helpers);
            $purchase->step_type=2;
            $purchase->is_approve=0;
            $purchase->is_from_app_sales=1;
            $purchase->save();
            $details = [];
            $timestamp = date('Y-m-d H:i:s');
            $detail = $request->details;
            if (empty($detail)) {
                return $this->message::getJsonResponse(404, 'Data item pre order tidak boleh kosong', []);

            }

            $productId = [];
            $productIds = "";
            foreach ($detail as $n => $i) {
                $productId[] = $i['sc_product_id'];
                $productIds .= ($n == 0) ? $i['sc_product_id'] : "," . $i['sc_product_id'];
            }

            $products = collect(DB::select("
                select
                  sp.*,
                  sp.inv_id as coa_inv_id,
                  sp.hpp_id as coa_hpp_id,
                  u.name as default_unit_name,
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        SELECT
                          spmu.*,
                          mu.name
                        FROM
                          sc_product_multi_units spmu
                          join md_units mu on mu.id = spmu.md_unit_id
                        WHERE
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                      ) jd
                  ) AS multi_units
                from
                  sc_products sp
                left join md_units u on u.id = sp.md_unit_id
                where
                  sp.id in ($productIds)
            "));
            foreach ($detail as $j => $item) {
                $product = $products->firstWhere('id', $item['sc_product_id']);
                $isMulti = isset($item['is_multi_unit']) ? $item['is_multi_unit'] : 0;
                $MultiId = isset($item['multi_unit_id']) ? $item['multi_unit_id'] : null;

                $multi_quantity = $item['quantity'];
                $mQuantity = $item['quantity'];
                $mPrice = $item['price'];
                $sub_total = $item['sub_total'];
                $convertionValue=1;
                if ($isMulti == 1 && !is_null($MultiId)) {
                    $multi_unit = collect(json_decode($product->multi_units));
                    $unit = $multi_unit->firstWhere('id', $item['multi_unit_id']);
                    $unit_name = $unit->name;
                    $convertionValue=$unit->quantity;
                    $multi_quantity = $convertionValue * $item['quantity'];
                    $jsonMulti=$multi_unit;
                }else{
                    $unit_name=$product->default_unit_name;
                    $jsonMulti=null;
                }

                $noteOrder = isset($item['note_order']) ? $item['note_order'] : "";
                $startRent =  null;
                $endRent = null;
                $discount=null;


                $details[] = [
                    'sc_product_id' => $item['sc_product_id'],
                    'quantity' => $mQuantity,
                    'multi_quantity' => $multi_quantity,
                    'is_multi_unit' => $isMulti,
                    'unit_name' => $unit_name,
                    'multi_unit_id' => $MultiId,
                    'sub_total' => $sub_total,
                    'sc_sale_order_id' => $purchase->id,
                    'profit' =>0,
                    'price' => $mPrice,
                    'note_order' => $noteOrder,
                    'created_at' => $timestamp,
                    'updated_at' => $timestamp,
                    'coa_inv_id' => ($product->is_with_stock == 0) ? null : $product->coa_inv_id,
                    'coa_hpp_id' => ($product->is_with_stock == 0) ? null : $product->coa_hpp_id,
                    'value_conversation' => ( $isMulti==1 ) ? $convertionValue : 1,
                    'json_multi_unit' => ( $isMulti==1 ) ? $jsonMulti : null,

                ];
            }

            DB::table('sc_sale_order_details')->insert($details);
            $resultJson=[
                'id'=>$purchase->id,
                'code'=>is_null($purchase->second_code)?$purchase->code:$purchase->second_code,
            ];
            DB::commit();
            return $this->message::getJsonResponse(200,'Pre order berhasil disimpan',$resultJson);
        } catch (\Exception $e) {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, 'Terjadi kesalahan disistem,cobalah beberapa saat lagi', []);
        }
    }

}

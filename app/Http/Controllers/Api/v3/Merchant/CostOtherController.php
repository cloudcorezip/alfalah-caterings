<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v3\Merchant;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Product;
use App\Models\Accounting\CoaMapping;
use App\Models\SennaToko\ProductSellingLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class CostOtherController extends Controller
{
    protected $cost;
    protected $message;

    public function __construct()
    {
        $this->cost=CoaMapping::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v3/senna/merchant/toko/cost-other')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/all/{merchantId}', 'Api\v3\Merchant\CostOtherController@getAll');

            });
    }


    public function getAll($merchantId)
    {
        try {
            $data=$this->cost::select(['acc_coa_detail_id as id','name'])
                ->where('md_merchant_id',$merchantId)
                ->whereIn('is_deleted',[0])
                ->where('is_type',2)->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

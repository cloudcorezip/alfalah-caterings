<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v3\Merchant;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductRedeem;
use App\Models\SennaToko\ProductRedeemDetail;
use App\Models\Accounting\CoaMapping;
use App\Models\SennaToko\ProductSellingLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Utils\Merchant\ProductUtil;
use Carbon\Carbon;

class RedeemPackageController extends Controller
{
    protected $redeemDetail;
    protected $redeem;
    protected $message;

    public function __construct()
    {
        $this->redeem=ProductRedeem::class;
        $this->redeemDetail=ProductRedeemDetail::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v3/senna/merchant/toko/redeem-package')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/all/{customerId}', 'Api\v3\Merchant\RedeemPackageController@getAll');
                Route::post('/claim/{id}', 'Api\v3\Merchant\RedeemPackageController@claim');
                Route::get('/detail/{redeemId}', 'Api\v3\Merchant\RedeemPackageController@getDetail');

            });
    }


    public function getAll($customerId)
    {
        try {
            $current=date('Y-m-d H:i:s');

            $data=$this->redeem::select([
                                    'sc_product_redeems.*',
                                    's.code',
                                    'p.name as product_name',
                                    DB::raw("(CASE
                                    WHEN sc_product_redeems.expired_date < '".$current."' THEN 1
                                    ELSE 0
                                    END) as is_expired")
                                    ])
                            ->join('sc_sale_orders as s','s.id','sc_product_redeems.sc_sale_order_id')
                            ->join('sc_products as p', 'p.id', 'sc_product_redeems.sc_product_id')
                            ->where('s.sc_customer_id',$customerId)
                            ->get();


            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
             //dd($e);
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getDetail($redeemId)
    {
        try {
            $current=date('Y-m-d H:i:s');

            $data=$this->redeemDetail::select([
                                    'sc_product_redeem_details.*',
                                    's.code',
                                    'p.name as product_name'
                                    ])
                            ->join('sc_product_redeems as r','r.id','sc_product_redeem_details.sc_redeem_id')
                            ->join('sc_sale_orders as s','s.id','r.sc_sale_order_id')
                            ->join('sc_products as p', 'p.id', 'r.sc_product_id')
                            ->where('sc_redeem_id',$redeemId)
                            ->get();


            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
             dd($e);
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function claim($id,Request $request)
    {
        try {
            DB::beginTransaction();
            date_default_timezone_set($request->timezone);

            $productTemporary=[];
            $details=[];

            $invMethodId=$request->md_inventory_method_id;
            $warehouseId=$request->warehouse_id;

            $data=$this->redeem::find($id);
            
            foreach($request->product as $item){
                $product=Product::find($item['sc_product_id']);
                if($product->is_wtih_stock==1){
                    array_push($productTemporary, [
                        'id' => $product->id,
                        'name' => $product->name,
                        'code' => $product->code,
                        'stock' => $product->stock,
                        'selling_price' => $product->selling_price,
                        'purchase_price' => $product->purchase_price,
                        'quantity' => $item['quantity']
                    ]);
                }

                $details[]=[
                    "sc_redeem_id"=>$data->id,
                    "child_product_id"=>$product->id,
                    'quantity' => $item['quantity'],
                    "created_by"=>$request->created_by,
                    "created_at"=>date('Y-m-d H:i:s')
                ];
            }
            if(!empty($productTemporary)){
                $adjustStockPackageSubs=ProductUtil::adjustStockPackageSubs($productTemporary,$data->sc_sale_order_id,$invMethodId,$warehouseId);
                // dd($adjustStockPackageSubs);
                if($adjustStockPackageSubs==false){
                    return $this->message::getJsonResponse(404, 'Terjadi kesalahan ketika penambahan langganan produk', []);
                }
            }

            DB::table('sc_product_redeem_details')->insert($details);
            DB::commit();
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            DB::rollBack();
             dd($e);
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

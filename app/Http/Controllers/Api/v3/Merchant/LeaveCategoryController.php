<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Http\Controllers\Api\v3\Merchant;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class LeaveCategoryController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v3/attendance/leave-category')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/list',[static::class,'list']);
            });
    }

    public function list(Request $request)
    {
        try {
            $merchantId=$request->md_merchant_id;
            if(is_null($merchantId) || $merchantId==''){
                return $this->message::getJsonResponse(404,'Paramater md_merchant_id tidak boleh kosong',[]);

            }
            $data=DB::select('hr');
        }catch (\Exception $e){

        }
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\VirtualAccount;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Classes\Singleton\WhatsappBroadcast;
use App\Classes\Singleton\XenditCore;
use App\Events\PaymentSuccess;
use App\Events\PaymentSuccessAr;
use App\Http\Controllers\Controller;
use App\Jobs\SendSuccessfulSubscription;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\SennaPayment\PaymentSubscription;
use App\Models\SennaPayment\Transaction;
use App\Models\SennaToko\SaleOrder;
use App\Utils\Accounting\CoaSaleUtil;
use App\Utils\Inventory\InventoryUtilV2;
use App\Utils\Subscription\SubscriptionUtil;
use App\Utils\ThirdParty\VirtualAccountUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use function GuzzleHttp\Psr7\str;

class VirtualAccountController extends Controller
{
    protected $message;
    protected $xendit;
    protected $virtualAccount;
    protected $waBroadcast;


    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->xendit=XenditCore::getInstance();
        $this->virtualAccount=new VirtualAccountUtil();
        $this->waBroadcast=WhatsappBroadcast::getInstance();

    }

    public function routeApi()
    {
        Route::post('v1/virtual-account/callback', 'Api\v1\VirtualAccount\VirtualAccountController@callback')->name('pay.callback.all');
        Route::post('v1/virtual-account/callback-new', 'Api\v1\VirtualAccount\VirtualAccountController@callbackNew');

    }
    public function callback(Request $request)
    {
        try{

            DB::table('xendit_responses')
                ->insert([
                    'response'=>json_encode($request->all()),
                    'external_code'=>$request->external_id
                ]);
            DB::beginTransaction();
            $date=Carbon::parse($request->created_at)->toDateString();
            $transaction=Transaction::where('external_id',$request->external_id)
                ->where('md_sp_transaction_status_id',1)
                ->whereDate('created_at',$date)
                ->where('amount',$request->amount)
                ->first();

            if(!is_null($transaction))
            {
                if($transaction->md_sp_transaction_type_id==TransactionType::SUBSCRIPTION_FEE)
                {
                    $endDate = SubscriptionUtil::calculateSubsVAOrQRIS($transaction->getUser, $transaction->transactionable->getSubscription);
                    if ($endDate == false) {
                        return response()->json([
                            'code'=>404,
                            'message'=>'Terjadi kesalahan saat perpanjangan',
                            'data'=>$endDate
                        ],404);

                    }else{
                        $transaction->md_sp_transaction_status_id = 2;
                        $transaction->save();
                        $transaction->transactionable->getMerchant->is_active_subscription = 1;
                        $transaction->transactionable->getMerchant->end_subscription = $endDate;
                        $transaction->transactionable->getMerchant->md_subscription_id = $transaction->transactionable->md_subscription_id;
                        $transaction->transactionable->getMerchant->save();
                        DB::commit();
                        $merchant=$transaction->transactionable->getMerchant;

                        Session::get('merchant_data'.$merchant->id);
                        Session::put('merchant_data'.$merchant->id,json_encode($merchant));

                        parent::firebase()::send(
                            'Pembayaran Berlangganan Berhasil',
                            trans('custom.pay_subscription_fee', [
                                'end_date' => $endDate
                            ]),
                            '',
                            $transaction->transactionable->getMerchant->getUser,
                            self::class, Route::currentRouteAction(),
                            PaymentSubscription::where('id',$transaction->transactionable_id)->first(),
                            1);

                        return response()->json([
                            'code'=>200,
                            'message'=>'Pembayaran Berhasil Dilakukan',
                            'data'=>$endDate
                        ],200);
                    }

                }
            }else{

                $status="";
                $saleOrder=[];
                if(!is_null($request->data)){
                    $saleOrder=DB::select("select * from sc_sale_orders where
                checkout_response notnull
                and checkout_response ->>'id'= '".$request->data['id']."'");
                    $status=($request->data['status']=='SUCCEEDED')?'COMPLETED':$request->data['status'];
                }

                if(!is_null($request->qr_code)){
                    $saleOrder=DB::select("select * from sc_sale_orders where
                checkout_response notnull
                and checkout_response ->>'id'= '".$request->qr_code['id']."'");
                    $status=($request->status=='COMPLETED')?'COMPLETED':$request->status;
                }

                if(!is_null($request->callback_virtual_account_id)){
                    $saleOrder=DB::select("select * from sc_sale_orders where
                checkout_response notnull
                and checkout_response ->>'external_id'= '".$request->external_id."'");
                $status='COMPLETED';

                }
                if(count($saleOrder)>0){
                    $data=(array)json_decode($saleOrder[0]->json_order_online);
                    $sale=SaleOrder::find($saleOrder[0]->id);

                    if($status!='COMPLETED'){
                        $sale->md_sc_transaction_status_id=4;
                        $sale->save();

                        DB::commit();

                        event(new PaymentSuccess($saleOrder[0]->id,1));

                        return response()->json([
                            'code'=>500,
                            'message'=>'Pembayaran Gagal dilakukan',
                            'data'=>$request->all()
                        ],500);
                    }

                    

                    if($saleOrder[0]->is_from_step_delivery==1){
                        if($saleOrder[0]->md_sc_transaction_status_id==2){

                            return response()->json([
                                'code'=>200,
                                'message'=>'Pembayaran Berhasil Dilakukan',
                                'data'=>[]
                            ],200);
                        }

                        $sale->md_sc_transaction_status_id=2;
                        $sale->save();
                        DB::commit();

                        if(CoaSaleUtil::coaStockAdjustmentFromSaleV2($data['user_id'], $data['merchant_id'],collect($data['coaJson']),$data['warehouse_id'],$data['inv_id'], $saleOrder[0]->id) == false) {
                            DB::rollBack();
                            return false;
                        }
                        event(new PaymentSuccess($saleOrder[0]->id,2));

                        return response()->json([
                            'code'=>200,
                            'message'=>'Pembayaran Transaksi Penjualan '.$saleOrder[0]->code.', Berhasil Dilakukan',
                            'data'=>$saleOrder[0]
                        ],200);
                    }else{

                        if($saleOrder[0]->md_sc_transaction_status_id==2){

                            return response()->json([
                                'code'=>200,
                                'message'=>'Pembayaran Berhasil Dilakukan',
                                'data'=>[]
                            ],200);
                        }

                        if(!empty($data['productCalculate']))
                        {

                            $calculateInventory=InventoryUtilV2::inventoryCodev2($data['inv_id'],$data['warehouse_id'],(array)$data['productCalculate'],'minus');
                            if($calculateInventory==false)
                            {
                                return  false;
                            }
                            $updateStockInv="";
                            $stockInvId=[];
                            $saleMapping=[];

                            foreach ($calculateInventory as $key => $n)
                            {
                                $updateStockInv.=($key==0)?"(".$n['id'].",".$n['residual_amount'].")":",(".$n['id'].",".$n['residual_amount'].")";
                                $stockInvId[]=$n['id'];
                                $saleMapping[]=[
                                    'sc_sale_order_id'=>$saleOrder[0]->id,
                                    'sc_product_id'=>$n['sc_product_id'],
                                    'sc_stock_inventory_id'=>$n['id'],
                                    'amount'=>$n['amount'],
                                    'purchase_price'=>$n['purchase'],
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s')
                                ];
                            }
                            if($updateStockInv!=""){
                                DB::statement("
                    update sc_stock_inventories as t set
                            residual_stock = c.column_a
                        from (values
                            $updateStockInv
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                            }

                            if($data['updateStockProduct']!=""){
                                DB::statement("
                    update sc_products as t set
                            stock = c.column_a
                        from (values
                            ".$data['updateStockProduct']."
                        ) as c(column_b, column_a)
                        where c.column_b = t.id;
                    ");
                            }
                            $stockOut=[];
                            foreach ($data['stock_in_out'] as $key =>$i){
                                $stockOut[]=[
                                    "type"=> $i->type,
                                    "total"=> $i->total,
                                    "sync_id"=> $i->sync_id,
                                    "created_at"=>  $i->created_at,
                                    "created_by"=>  $i->created_by,
                                    "updated_at"=>  $i->updated_at,
                                    "record_stock"=> $i->record_stock,
                                    "stockable_id"=>  $i->stockable_id,
                                    "sc_product_id"=>  $i->sc_product_id,
                                    "selling_price"=>  $i->selling_price,
                                    "purchase_price"=>  $i->purchase_price,
                                    "residual_stock"=>  $i->residual_stock,
                                    "stockable_type"=>  $i->stockable_type,
                                    "inv_warehouse_id"=>  $i->inv_warehouse_id,
                                    "transaction_action"=>  $i->transaction_action,
                                ];
                            }

                            DB::table('sc_stock_sale_mappings')->insert($saleMapping);
                            DB::table('sc_stock_inventories')->insert($stockOut);
                            DB::table('sc_inv_production_of_goods')
                                ->whereIn('sc_stock_inventory_id',$stockInvId)->update(['is_use'=>1]);

                        }
                        $sale->md_sc_transaction_status_id=2;
                        $sale->save();

                        DB::commit();
                        if(CoaSaleUtil::coaStockAdjustmentFromSaleV2($data['user_id'], $data['merchant_id'],collect($data['coaJson']),$data['warehouse_id'],$data['inv_id'], $saleOrder[0]->id) == false) {
                            DB::rollBack();
                            return false;
                        }

                        event(new PaymentSuccess($saleOrder[0]->id,2));

                        return response()->json([
                            'code'=>200,
                            'message'=>'Pembayaran Transaksi Penjualan '.$saleOrder[0]->code.', Berhasil Dilakukan',
                            'data'=>$saleOrder[0]
                        ],200);
                    }


                }else{

                    if(!is_null($request->data)){
                        $ar=DB::select("select * from acc_merchant_ar_details where
                checkout_response notnull
                and checkout_response ->>'id'= '".$request->data['id']."'");
                        $status=($request->data['status']=='SUCCEEDED')?'COMPLETED':$request->data['status'];
                    }
                    if(!is_null($request->qr_code)){
                        $ar=DB::select("select * from acc_merchant_ar_details where
                checkout_response notnull
                and checkout_response ->>'id'= '".$request->qr_code['id']."'");
                        $status=($request->status=='COMPLETED')?'COMPLETED':$request->status;
                    }

                    if(!is_null($request->callback_virtual_account_id)){
                        $ar=DB::select("select * from acc_merchant_ar_details where
                checkout_response notnull
                and checkout_response ->>'external_id'= '".$request->external_id."'");
                     $status='COMPLETED';

                    }



                    if(count($ar)>0){
                        

                        $arData=(array)json_decode($ar[0]->array_insert);
                        $arObject=MerchantArDetail::find($ar[0]->id);

                        if($status!='COMPLETED'){
                            $arObject->md_transaction_status_id=4;
                            $arObject->save();
                            DB::commit();
                            event(new PaymentSuccess($saleOrder[0]->id,1));
    
                            return response()->json([
                                'code'=>500,
                                'message'=>'Pembayaran Gagal dilakukan',
                                'data'=>[]
                            ],500);
                        }

                        date_default_timezone_set($arObject->timezone);


                        if($arData['paid_nominal']>=$arObject->getAr->residual_amount)
                        {
                            $arObject->getAr->is_paid_off=1;
                            $arObject->getAr->arable->md_sc_transaction_status_id=TransactionStatus::PAID;
                            $arObject->getAr->arable->save();
                        }else{
                            $arObject->getAr->arable->md_sc_transaction_status_id=6;
                            $arObject->getAr->arable->save();
                        }
                        $arObject->getAr->residual_amount-=$arData['paid_nominal'];
                        $arObject->getAr->paid_nominal+=$arData['paid_nominal'];
                        $arObject->getAr->save();

                        $arObject->md_transaction_status_id=2;
                        $arObject->paid_date=date('Y-m-d H:i:s');
                        $arObject->save();

                        $jurnal=new Jurnal();
                        $jurnal->ref_code=$arObject->ar_detail_code;
                        $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$arData['merchantId']);
                        $jurnal->trans_name='Pembayaran Penjualan '.$arObject->getAr->arable->code.'-'.$arObject->ar_detail_code;
                        $jurnal->trans_time=date('Y-m-d H:i:s');
                        $jurnal->trans_note='Pembayaran Penjualan  '.$arObject->getAr->arable->code.'-'.$arObject->ar_detail_code;
                        $jurnal->trans_purpose='Pembayaran Penjualan  '.$arObject->getAr->arable->code.'-'.$arObject->ar_detail_code;
                        $jurnal->trans_amount=$arData['total'];
                        $jurnal->trans_proof=$arObject->trans_proof;
                        $jurnal->md_merchant_id=$arData['merchantId'];
                        $jurnal->md_user_id_created=$arData['userId'];
                        $jurnal->external_ref_id=$arObject->id;
                        $jurnal->flag_name='Pembayaran Penjualan  '.$arObject->getAr->arable->code;
                        $jurnal->save();

                        $fromCoa= $arData['from_coa'];
                        $toCoa=$arData['to_coa'];
                        $insert=[
                            [
                                'acc_coa_detail_id'=>$fromCoa->id,
                                'coa_type'=>'Kredit',
                                'md_sc_currency_id'=>$fromCoa->md_sc_currency_id,
                                'amount'=>$arData['total'],
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'acc_jurnal_id'=>$jurnal->id
                            ],
                            [
                                'acc_coa_detail_id'=>$toCoa->id,
                                'coa_type'=>'Debit',
                                'md_sc_currency_id'=>$toCoa->md_sc_currency_id,
                                'amount'=>$arData['total'],
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'acc_jurnal_id'=>$jurnal->id
                            ],
                        ];

                        JurnalDetail::insert($insert);

                        DB::commit();

                        event(new PaymentSuccessAr($ar[0]->id,2));

                        return response()->json([
                            'code'=>200,
                            'message'=>'Pembayaran Transaksi Piutang Penjualan '.$ar[0]->ar_detail_code.', Berhasil Dilakukan',
                            'data'=>$ar[0]
                        ],200);



                    }else{

                        return response()->json([
                            'code'=>404,
                            'message'=>'Data Pembayaran Tidak Ditemukan/Pembayaran sudah dilakukan',
                            'data'=>[]
                        ],404);
                    }

                }

            }


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'code'=>500,
                'message'=>'Terjadi Kesalahan Saat Pembayaran',
                'data'=>$e->getMessage()
            ],500);
        }
    }

    public function callbackNew()
    {
        return response()->json([
            'code'=>200,
            'message'=>'Va Berhasil Diperbaharui',
            'data'=>[]
        ],200);
    }


}

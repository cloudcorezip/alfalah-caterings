<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantApDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\MasterData\Merchant;
use App\Utils\Accounting\AccUtil;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Product;
use App\Utils\Accounting\CoaUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class AccountPayableController extends Controller
{
    protected $message;
    protected $customer;
    protected $product;
    protected $ap;
    protected $apDetail;


    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->customer=Customer::class;
        $this->product=Product::class;
        $this->ap=MerchantAp::class;
        $this->apDetail=MerchantApDetail::class;
    }

    public function routeApi()
    {
        Route::prefix('v1/merchant/toko/account-payable')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('list/{merchantId}', 'Api\v1\Merchant\Cashier\AccountPayableController@list');
                Route::get('one/{id}', 'Api\v1\Merchant\Cashier\AccountPayableController@one');
                Route::post('pay-debt/{id}', 'Api\v1\Merchant\Cashier\AccountPayableController@payDebt');
                Route::delete('delete/{detailId}', 'Api\v1\Merchant\Cashier\AccountPayableController@delete');
                Route::get('detail/{detailId}', 'Api\v1\Merchant\Cashier\AccountPayableController@detail');

            });
    }

    public function list($merchantId,Request $request)
    {
        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $isDebet=($request->is_debet=='asc')?'asc':'desc';
            $sorting=($request->sorting=='asc')?'desc':'asc';

            $data=$this->ap::select([
                'acc_merchant_ap.*',
                'c.name',
                's.total'
            ])
                ->where('acc_merchant_ap.md_merchant_id',$merchantId)
                ->where('acc_merchant_ap.apable_type','App\Models\SennaToko\PurchaseOrder')
                ->join('sc_purchase_orders as s',function ($join){
                    $join->on('s.id',DB::raw('cast(acc_merchant_ap.apable_id as bigint)'));
                })
                ->join('sc_suppliers as c','c.id','s.sc_supplier_id')
                ->with('apable.getDetail')
                ->where('acc_merchant_ap.is_editable',1)
                ->where('acc_merchant_ap.is_deleted',0)
                ->orderBy('acc_merchant_ap.id',$sorting)
                ->orderBy('acc_merchant_ap.is_paid_off',$isDebet)
                ->skip($offset)
                ->take($limit)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function one($id)
    {
        try {
            $data=$this->ap::select([
                'acc_merchant_ap.*',
                'c.name',
                's.total',
            ])
                ->with('apable.getDetail')
                ->where('acc_merchant_ap.apable_type','App\Models\SennaToko\PurchaseOrder')
                ->join('sc_purchase_orders as s',function ($join){
                    $join->on('s.id',DB::raw('cast(acc_merchant_ap.apable_id as bigint)'));
                })
                ->with('getDetail.getCoa')
                ->join('sc_suppliers as c','c.id','s.sc_supplier_id')
                ->where('acc_merchant_ap.id',$id)
                ->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function payDebt($id,Request $request)
    {
        try{
            DB::beginTransaction();
            $ar=$this->ap::find($id);
            $date = new \DateTime($ar->created_at);
            if(AccUtil::checkClosingJournal($ar->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }

            if(is_null($ar))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }

            if($ar->is_editable==0){
                return $this->message::getJsonResponse(404, trans('Perhatian! Demi perhitungan akurat di akuntansi mulai dari tanggal 8 Maret 2021 Transaksi Penjualan Maupun Pembelian dan Piutang/Hutang tidak dapat diedit, karena ada penambahan modul akuntansi ,apabila kamu ingin mereset data silahkan lakukan di menu pengaturan dan menghapusnya.'), []);
            }

            $data=new $this->apDetail;
            $validator = Validator::make($request->all(),$data->rule);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            date_default_timezone_set($request->timezone);

            if($request->paid_nominal>$ar->residual_amount)
            {
                return $this->message::getJsonResponse(404,'Nominal pembayaran pembelian melebihi dari kekurangan pembayaran',[]);

            }
            if(is_null($request->trans_coa_detail_id))
            {
                return $this->message::getJsonResponse(404,'Metode pembayaran belum dipilih',[]);

            }
            $trans=explode('_',$request->trans_coa_detail_id);

            if($request->paid_nominal>=$ar->residual_amount)
            {
                $ar->is_paid_off=1;
                $ar->apable->md_sc_transaction_status_id=TransactionStatus::PAID;
                $ar->apable->save();
            }else{
                $ar->apable->md_sc_transaction_status_id=6;
                $ar->apable->save();
            }
            $ar->residual_amount-=$request->paid_nominal;
            $ar->paid_nominal+=$request->paid_nominal;
            $ar->save();
            $data->ap_code=CodeGenerator::generateJurnalCode(0,'PVT',$ar->md_merchant_id);
            $data->paid_nominal=$request->paid_nominal;
            $data->paid_date=date('Y-m-d H:i:s');
            $data->note=$request->note;
            $data->acc_merchant_ap_id=$id;
            $data->created_by=$request->created_by;
            $data->timezone=$request->timezone;
            $data->trans_coa_detail_id=$trans[1];
            $data->save();

            $jurnal=new Jurnal();
            $jurnal->ref_code=$data->ap_code;
            $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$ar->md_merchant_id);
            $jurnal->trans_name='Pembayaran Pembelian  '.$ar->apable->code.'-'.$data->ap_code;
            $jurnal->trans_time=$data->paid_date;
            $jurnal->trans_note='Pembayaran Pembelian  '.$ar->apable->code.'-'.$data->ap_code;
            $jurnal->trans_purpose='Pembayaran Pembelian  '.$ar->apable->code.'-'.$data->ap_code;
            $jurnal->trans_amount=$data->paid_nominal;
            $jurnal->trans_proof='-';
            $jurnal->md_merchant_id=$ar->md_merchant_id;
            $jurnal->md_user_id_created=$data->created_by;
            $jurnal->external_ref_id=$data->id;
            $jurnal->flag_name='Pembayaran Pembelian  '.$ar->apable->code;
            $jurnal->save();

            $fromCoa=Merchant::find($ar->md_merchant_id);

            $toCoa=$trans[1];
            $insert=[
                [
                    'acc_coa_detail_id'=>$fromCoa->coa_ap_purchase_id,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$data->paid_nominal,
                    'created_at'=>$data->paid_date,
                    'updated_at'=>$data->paid_date,
                    'acc_jurnal_id'=>$jurnal->id
                ],
                [
                    'acc_coa_detail_id'=>$toCoa,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$data->paid_nominal,
                    'created_at'=>$data->paid_date,
                    'updated_at'=>$data->paid_date,
                    'acc_jurnal_id'=>$jurnal->id
                ],
            ];
            JurnalDetail::insert($insert);

            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function delete($detailId)
    {
        try {
            DB::beginTransaction();
            $data=MerchantApDetail::find($detailId);
            $ar=MerchantAp::find($data->acc_merchant_ap_id);
            $date = new \DateTime($ar->created_at);
            if(AccUtil::checkClosingJournal($ar->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            $ar->residual_amount+=$data->paid_nominal;
            $ar->paid_nominal-=$data->paid_nominal;
            $ar->is_paid_off=0;
            $ar->save();
            $ar->apable->md_sc_transaction_status_id=6;
            if($ar->apable->paid_nominal>0)
            {
                if($ar->apable->paid_nominal==$data->paid_nominal)
                {
                    $ar->apable->paid_nominal-=$data->paid_nominal;
                }
            }
            $ar->apable->save();

            Jurnal::where([
                'external_ref_id'=>$data->id,
                'ref_code'=>$data->ap_code
            ])->update([
                'is_deleted'=>1
            ]);
            $data->delete();
            DB::commit();
            return $this->message::getJsonResponse(200,'Data pembayaran pelunasan hutang berhasil dihapus',[]);
        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function detail($detailId)
    {
        try {
            $data=$this->apDetail::find($detailId);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.200'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

}

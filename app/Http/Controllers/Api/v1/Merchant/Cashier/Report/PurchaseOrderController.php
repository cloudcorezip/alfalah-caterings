<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Merchant\Cashier\Report;


use App\Classes\RangeCriteria;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class PurchaseOrderController extends Controller
{
    protected $message;
    protected $rangeCriteria;
    protected $merchant;

    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->rangeCriteria=new RangeCriteria();
        $this->merchant=Merchant::class;

    }
    public function routeApi()
    {
        Route::prefix('v1/merchant/toko/report/purchase-order')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('general/{merchantId}', 'Api\v1\Merchant\Cashier\Report\PurchaseOrderController@general')
                ->name('toko.report.purchase-order.general');
                ;
                Route::post('list/{merchantId}', 'Api\v1\Merchant\Cashier\Report\PurchaseOrderController@list')
                    ->name('toko.report.purchase-order.list')
                ;
            });
    }


    public function general($merchantId,Request $request)
    {
        try{
            $periodFilter=(is_null($request->period_filter))?'today':$request->period_filter;
            $dateCustom=(is_null($request->date_custom))?date('Y-m-d'):$request->date_custom;
            $roleFilter=(is_null($request->role_filter))?0:$request->role_filter;
            if($periodFilter=='custom')
            {
                $date=date('Y-m-d',strtotime($dateCustom));

            }else{
                $date=$this->rangeCriteria->rangeFilter($periodFilter);
            }
            if($roleFilter==1)
            {
                $user=$this->merchant::find($merchantId);
                $queryRole="and spo.created_by=$user->md_user_id";
            }else{
                $queryRole="";
            }
            if($periodFilter=='today' || $periodFilter=='yesterday')
            {
                $query="'".$date->format('Y-m-d')."' :: timestamp, '".$date->format('Y-m-d')."' :: timestamp";
                $queryTotal="='".$date->format('Y-m-d')."'";
                $format='YYYY-MM-DD';
                $interval='1 day';
                $dateTrunch='day';
            }elseif($periodFilter=='custom'){
                $query="'".$date."' :: timestamp, '".$date."' :: timestamp";
                $queryTotal="='".$date."'";
                $format='YYYY-MM-DD';
                $interval='1 day';
                $dateTrunch='day';
            }
            elseif ($periodFilter=='thisyear')
            {
                $query="'".date('Y')."-01-01' :: timestamp, '".(date('Y'))."-12-31 23:59:00'";
                $queryTotal="between '".date('Y')."-01-01' and '".(date('Y'))."-12-31'";
                $format='Month';
                $interval='1 Month';
                $dateTrunch='Month';



            }
            elseif ($periodFilter=='lastyear')
            {
                $query="'".(date('Y')-1)."-01-01' :: timestamp, '".(date('Y')-1)."-12-31 23:59:00'";
                $queryTotal="between '".(date('Y')-1)."-01-01' and '".(date('Y')-1)."-12-31'";
                $format='Month';
                $interval='1 Month';
                $dateTrunch='Month';

            }
            else{
                $query="'".$date[0]->format('Y-m-d')."' :: timestamp, '".$date[1]->format('Y-m-d')."' :: timestamp";
                $queryTotal="between '".$date[0]->format('Y-m-d')."' and '".$date[1]->format('Y-m-d')."'";
                $format='YYYY-MM-DD';
                $interval='1 day';
                $dateTrunch='day';

            };
            $data=DB::select("
               select
                  count(*) as transaction_amount,
                  coalesce (sum(spo.total),0) as spending_amount,
                  coalesce(
                    sum(
                      (
                        select
                          sum(amp.residual_amount)
                        from
                          acc_merchant_ap amp
                        where
                          amp.apable_id = spo.id
                          and amp.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                      )
                    ),
                    0
                  ) debt
                from
                  sc_purchase_orders spo
                where
                  spo.is_deleted = 0
                  and spo.is_editable=1
                  and spo.step_type=0
                  and spo.md_merchant_id = $merchantId
                  and spo.created_at::date $queryTotal
            ");

            if($periodFilter=='custom' || $periodFilter=='today' || $periodFilter=='yesterday')
            {
                $chart=DB::select("
                select
                  count(*) as transaction_amount,
                  to_char(
                    (
                      date_trunc(
                        'minute', spo.created_at + interval '1' day
                      )
                    ),
                    'HH24:MI'
                  ) as time,
                  coalesce(sum(spo.total),0) as total,
                  coalesce(
                    sum(
                      coalesce(
                        (
                          select
                            sum(amp.residual_amount)
                          from
                            acc_merchant_ap amp
                          where
                            amp.apable_id = spo.id
                            and spo.is_debet = 1
                            and amp.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                        ),
                        0
                      )
                    ),
                    0
                  ) as debt
                from
                  sc_purchase_orders spo
                where
                  spo.is_deleted = 0
                  and spo.step_type=0
                  and spo.is_editable=1
                  and spo.md_merchant_id = $merchantId $queryRole
                  and spo.created_at::date='$date'
                group by
                  to_char(
                    (
                      date_trunc(
                        'minute', spo.created_at + interval '1' day
                      )
                    ),
                    'HH24:MI'
                  ),
                  spo.total
                order by
                  to_char(
                    (
                      date_trunc(
                        'minute', spo.created_at + interval '1' day
                      )
                    ),
                    'HH24:MI'
                  ) asc
                ");

            } else{
                $chart=DB::select("
                select
                  coalesce(s.transaction_amount, 0) as transaction_amount,
                  to_char(mon.mon, '$format') AS time,
                  coalesce(s.total, 0) as total,
                  coalesce(s.debt, 0) as debt
                from
                  generate_series(
                    $query,
                    interval '$interval'
                  ) as mon(mon)
                  left join (
                    select
                      date_trunc('$dateTrunch', created_at) as mon,
                      sum(spo.total) as total,
                      count(*) as transaction_amount,
                      coalesce(
                        sum(
                          (
                            select
                              sum(amp.residual_amount)
                            from
                              acc_merchant_ap amp
                            where
                              amp.apable_id = spo.id
                              and spo.is_debet = 1
                              and amp.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                          )
                        ),
                        0
                      ) as debt
                    from
                      sc_purchase_orders spo
                    where
                      spo.is_deleted = 0
                      and spo.is_editable=1
                      and spo.step_type=0
                      and spo.md_merchant_id = $merchantId
                    group by
                      mon
                  ) s on mon.mon = s.mon;
                ");
            }

            $params=[
                'dashboard'=>$data,
                'chart'=>$chart
            ];
            return $this->message::getJsonResponse(200,trans('message.201'),$params);

        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }

    }

    public function list($merchantId,Request $request)
    {
        try{
            $periodFilter=(is_null($request->period_filter))?'today':$request->period_filter;
            $dateCustom=(is_null($request->date_custom))?date('Y-m-d'):$request->date_custom;
            $roleFilter=(is_null($request->role_filter))?0:$request->role_filter;
            $orderFilter=(is_null($request->order_filter))?'desc':$request->order_filter;
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;
            if($periodFilter=='custom')
            {
                $date=date('Y-m-d',strtotime($dateCustom));

            }else{
                $date=$this->rangeCriteria->rangeFilter($periodFilter);
            }
            if($roleFilter==1)
            {
                $user=$this->merchant::find($merchantId);
                $queryRole="and spo.created_by=$user->md_user_id";
            }else{
                $queryRole="";
            }
            if($periodFilter=='today' || $periodFilter=='yesterday')
            {
                $queryTotal="='".$date->format('Y-m-d')."'";
            }elseif($periodFilter=='custom'){
                $queryTotal="='".$date."'";
            }
            elseif ($periodFilter=='thisyear')
            {
                $queryTotal="between '".date('Y')."-01-01' and '".(date('Y'))."-12-31'";


            }
            elseif ($periodFilter=='lastyear')
            {
                $queryTotal="between '".(date('Y')-1)."-01-01' and '".(date('Y')-1)."-12-31'";

            }
            else{
                $queryTotal="between '".$date[0]->format('Y-m-d')."' and '".$date[1]->format('Y-m-d')."'";
            };

            $data=DB::select("
            select
              spo.id,
              spo.code,
              spo.is_debet,
              spo.created_at,
              spo.created_by,
              coalesce(ss.name, '-') as supplier_name,
              spo.total,
              coalesce(
                          (
                            select
                              sum(amp.residual_amount)
                            from
                              acc_merchant_ap amp
                            where
                              amp.apable_id = spo.id
                              and spo.is_debet = 1
                              and amp.apable_type = 'App\Models\SennaToko\PurchaseOrder'
                          ),
                        0
                      ) as debt
            from
              sc_purchase_orders spo
              left join sc_suppliers ss on spo.sc_supplier_id = ss.id
            where
              spo.is_deleted = 0
              and spo.is_editable=1
              and spo.step_type=0
              and spo.md_merchant_id = $merchantId
              and spo.created_at::date $queryTotal
              order by spo.id $orderFilter
              offset $offset
              limit $limit
            ");
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

}

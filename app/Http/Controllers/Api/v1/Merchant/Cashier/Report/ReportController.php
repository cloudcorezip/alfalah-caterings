<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Merchant\Cashier\Report;


use App\Classes\RangeCriteria;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\ProductCategory;
use App\Models\SennaToko\SaleOrder;
use App\Models\SennaToko\StockInventory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class ReportController extends Controller
{

    protected $message;
    protected $saleOrder;
    protected $stockInventory;
    protected $rangeCriteria;
    protected $merchant;

    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->saleOrder=SaleOrder::class;
        $this->stockInventory=StockInventory::class;
        $this->rangeCriteria=new RangeCriteria();
        $this->merchant=Merchant::class;
    }

    public function routeApi()
    {
        Route::prefix('v1/merchant/toko/report')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('general/{merchantId}', 'Api\v1\Merchant\Cashier\Report\ReportController@general')->name('toko.report.general');
                Route::post('stock-flow/{merchantId}', 'Api\v1\Merchant\Cashier\Report\ReportController@stockFlow');
                Route::post('tax-report/{merchantId}', 'Api\v1\Merchant\Cashier\Report\ReportController@taxReport');
                Route::prefix('inventory')
                    ->middleware('etag')
                    ->group(function(){
                        Route::post('by-product/{merchantId}', 'Api\v1\Merchant\Cashier\Report\ReportController@inventoryByProduct');
                        Route::post('by-category/{merchantId}', 'Api\v1\Merchant\Cashier\Report\ReportController@inventoryByCategory');
                    });

            });
    }

    public function general(Request $request,$merchantId)
    {
        try{
            $periodFilter=(is_null($request->period_filter))?'today':$request->period_filter;
            $dateCustom=(is_null($request->date_custom))?date('Y-m-d'):$request->date_custom;
            $warehouse=(parent::getGlobalWarehouse($merchantId))->id;

            if($periodFilter=='custom')
            {
                $date=date('Y-m-d',strtotime($dateCustom));

            }else{
                $date=$this->rangeCriteria->rangeFilter($periodFilter);
            }
            if($periodFilter=='today' || $periodFilter=='yesterday')
            {
                $query="'".$date->format('Y-m-d')."' :: timestamp, '".$date->format('Y-m-d')."' :: timestamp";
                $queryTotal="='".$date->format('Y-m-d')."'";

            }elseif($periodFilter=='custom'){
                $query="'".$date."' :: timestamp, '".$date."' :: timestamp";
                $queryTotal="='".$date."'";


            }
            elseif ($periodFilter=='thisyear')
            {
                $query="'".date('Y')."-01-01' :: timestamp, '".(date('Y'))."-12-31 23:59:00'";
                $queryTotal="between '".date('Y')."-01-01' and '".(date('Y'))."-12-31'";


            }
            elseif ($periodFilter=='lastyear')
            {
                $query="'".(date('Y')-1)."-01-01' :: timestamp, '".(date('Y')-1)."-12-31 23:59:00'";
                $queryTotal="between '".(date('Y')-1)."-01-01' and '".(date('Y')-1)."-12-31'";

            }
            else{
                $query="'".$date[0]->format('Y-m-d')."' :: timestamp, '".$date[1]->format('Y-m-d')."' :: timestamp";
                $queryTotal="between '".$date[0]->format('Y-m-d')."' and '".$date[1]->format('Y-m-d')."'";

            };
            $data=DB::select("
                           select
                  count(*) as transaction_amount,
                  coalesce (
                    sum(so.tax),
                    0
                  ) as tax,
                  coalesce(
                    sum(r.residual_amount),
                    0
                  ) as acc_receivable,
                  COALESCE(
                    sum(r.paid_nominal),
                    0
                  ) as income_from_ar,
                  COALESCE(
                    sum(
                      so.total - so.admin_fee -coalesce((

                      select sum(total) from sc_retur_sale_orders srso
                      where srso.sc_sale_order_id=so.id
                      and srso.is_editable=1
                      and srso.is_deleted=0
                      and srso.reason_id!=1
                      ),0)
                    ),
                    0
                  )-COALESCE(
                    sum(r.residual_amount),
                    0
                  )
                   as income_paid_off
                from
                  sc_sale_orders so
                  left join (
                    select
                      sum(ar.paid_nominal) as paid_nominal,
                      sum(ar.residual_amount) as residual_amount,
                      ar.arable_id
                    from
                      acc_merchant_ar as ar
                    where
                      ar.arable_type = 'App\Models\SennaToko\SaleOrder'

                    group by
                      ar.arable_id
                  ) r on r.arable_id = so.id
                where
                  so.md_merchant_id = $merchantId
                  and so.is_editable=1
                  and so.is_keep_transaction=0
                  and so.is_approved_shop = 1
                  and so.is_cancel_user = 0
                  and so.is_deleted = 0
                  and so.step_type=0
                  and so.created_at :: date $queryTotal
                  and so.md_sc_transaction_status_id not in(3, 5)

            ");
            if($periodFilter=='custom' || $periodFilter=='today' || $periodFilter=='yesterday')
            {
                $chart=DB::select("
                select
                      count(*) as transaction_amount,
                      to_char(
                        (
                          date_trunc(
                            'minute', so.created_at + interval '1' day
                          )
                        ),
                        'HH24:MI'
                      ) as time,
                      (
                        case when so.is_debet = 0 then COALESCE(
                          (
                            sum(so.total - so.admin_fee)- coalesce(sum((select sum(total) from sc_retur_sale_orders where reason_id!=1
                            and is_deleted=0 and is_editable=1 and sc_sale_order_id=so.id
                            )),0)
                          ),0)
                         else COALESCE(
                          (
                            sum(
                              (
                                select
                                  sum(ar.paid_nominal)
                                from
                                  sc_sale_orders as so_sub
                                  join acc_merchant_ar as ar on ar.arable_id = so_sub.id
                                  where ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                                  and so_sub.id = so.id
                                  and so_sub.is_debet = 1
                              )
                            )
                          ),
                          0
                        ) end
                      ) total
                    from
                      sc_sale_orders as so
                    where

                      so.is_approved_shop = 1
                      and so.is_editable=1
                      and so.is_deleted=0
                      and so.is_cancel_user = 0
                      and so.step_type=0
                      and so.is_keep_transaction=0
                      and so.md_merchant_id = $merchantId
                      and so.created_at :: date = '$date'
                      and so.md_sc_transaction_status_id not in(3, 5)
                    group by
                      to_char(
                        (
                          date_trunc(
                            'minute', so.created_at + interval '1' day
                          )
                        ),
                        'HH24:MI'
                      ),
                      so.total,
                      so.is_debet
                    order by
                      to_char(
                        (
                          date_trunc(
                            'minute', so.created_at + interval '1' day
                          )
                        ),
                        'HH24:MI'
                      ) asc
                 ")
                ;
            }
            elseif($periodFilter=='thisyear' || $periodFilter=='lastyear'){

                $chart=DB::select("
                select
                  coalesce(s.id, 0) as transaction_amount,
                  TO_CHAR(mon.mon, 'Month') AS time,
                  coalesce(s.total, 0) as total
                from
                  generate_series(
                    $query,
                    interval '1 month'
                  ) as mon(mon)
                  left join (
                    select
                      date_trunc('Month', created_at) as mon,
                      sum(
                        (
                          case when so.is_debet = 0 then COALESCE(
                            (
                              select
                                sum(so_sub.total-so_sub.admin_fee)-sum(coalesce(
                                (
                                 select sum(srso.total) from sc_retur_sale_orders srso
                                where srso.sc_sale_order_id=so_sub.id
                               and srso.is_deleted=0
                               and srso.is_editable=1
                               and srso.reason_id!=1
                                )
                                ,0))
                              from
                                sc_sale_orders as so_sub
                              where
                                so_sub.id = so.id

                            ),
                            0
                          ) else COALESCE(
                            (
                              select
                                sum(ar.paid_nominal)
                              from
                                sc_sale_orders as so_sub
                                join acc_merchant_ar as ar on ar.arable_id = so_sub.id
                                and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                              where
                                so_sub.id = so.id

                            ),
                            0
                          ) end
                        )
                      ) as total,
                      count(so.id) as id
                    from
                      sc_sale_orders as so
                    where
                   so.is_approved_shop = 1
                   and so.is_editable=1
                   and so.is_deleted=0
                   and so.is_keep_transaction=0
                  and so.is_cancel_user = 0
                  and so.step_type=0
                  and so.md_merchant_id=$merchantId
                  and so.md_sc_transaction_status_id not in(3, 5)
                    group by
                      mon
                  ) s on mon.mon = s.mon
                  order by mon.mon asc
                  ;

                ");
            }else{

                $chart=DB::select("
                select
                  coalesce(s.id, 0) as transaction_amount,
                  to_char(mon.mon, 'YYYY-MM-DD') AS time,
                  coalesce(s.total, 0) as total
                from
                  generate_series(
                    '".$date[0]->format('Y-m-d')."' :: timestamp, '".$date[1]->format('Y-m-d')."' :: timestamp,
                    interval '1 day'
                  ) as mon(mon)
                  left join (
                    select
                      date_trunc('day', created_at) as mon,
                      sum(
                        (
                          case when so.is_debet = 0 then COALESCE(
                            (
                                so.total-so.admin_fee-coalesce((select sum(total) from sc_retur_sale_orders where reason_id!=1
                            and is_deleted=0 and sc_sale_order_id=so.id
                            and is_editable=1
                            ),0)
                            ),
                            0
                          ) else COALESCE(
                            (
                              select
                                sum(ar.paid_nominal)
                              from
                                sc_sale_orders as so_sub
                                join acc_merchant_ar as ar on ar.arable_id = so_sub.id
                                and ar.arable_type = 'App\Models\SennaToko\SaleOrder'
                              where
                                so_sub.id = so.id

                            ),
                            0
                          ) end
                        )
                      ) as total,
                      count(so.id) as id
                    from
                      sc_sale_orders as so
                    where
                    so.md_merchant_id=$merchantId 
                    and so.is_approved_shop=1 
                    and so.is_cancel_user=0 
                    and so.is_deleted=0
                    and so.is_editable=1 
                    and so.step_type=0
                    and so.is_keep_transaction=0
                    and so.md_sc_transaction_status_id not in(3, 5)
                    group by
                      mon
                  ) s on mon.mon = s.mon
                    order by mon.mon asc
                  ;

                ");
            }
            $residual=DB::select("
            select
              coalesce(
                sum(
                  ssi.purchase_price * ssi.residual_stock
                ),
                0
              ) as residual_amount
            from
              sc_stock_inventories ssi
              join sc_products sp on sp.id = ssi.sc_product_id
              join md_merchants m on m.md_user_id = sp.md_user_id
            where
              m.id = $merchantId
              and sp.is_with_stock=1
              and ssi.is_deleted = 0
              and sp.is_deleted = 0
              and ssi.inv_warehouse_id=$warehouse
                and ssi.type='in'
                and ssi.transaction_action not like '%Retur%'
            ");

            $params=[
                'residual'=>$residual,
                'dashboard'=>$data,
                'chart'=>$chart
            ];
            return $this->message::getJsonResponse(200,trans('message.201'),$params);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }

    }

    public function stockFlow(Request $request,$merchantId)
    {
        try{
            $periodFilter=(is_null($request->period_filter))?'today':$request->period_filter;
            $dateCustom=(is_null($request->date_custom))?date('Y-m-d'):$request->date_custom;
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;
            $warehouse=(parent::getGlobalWarehouse($merchantId))->id;

            if($periodFilter=='custom')
            {
                $date=date('Y-m-d',strtotime($dateCustom));

            }else{
                $date=$this->rangeCriteria->rangeFilter($periodFilter);
            }
            if($periodFilter=='today' || $periodFilter=='yesterday'){
              $q="<='".$date->format('Y-m-d')."'";
            }elseif($periodFilter=='custom'){
              $q="='".$date."'";


          }
          elseif ($periodFilter=='thisyear')
          {
              $q="between '".date('Y')."-01-01' and '".(date('Y'))."-12-31'";

          }
          elseif ($periodFilter=='lastyear')
          {
              $q="between '".(date('Y')-1)."-01-01' and '".(date('Y')-1)."-12-31'";

          }
          else{
              $q="between '".$date[0]->format('Y-m-d')."' and '".$date[1]->format('Y-m-d')."'";

          };

            if($periodFilter=='today' || $periodFilter=='yesterday')
            {
                $query="='".$date->format('Y-m-d')."'";

            }elseif($periodFilter=='custom'){
                $query="='".$date."'";


            }
            elseif ($periodFilter=='thisyear')
            {
                $query="between '".date('Y')."-01-01' and '".(date('Y'))."-12-31'";

            }
            elseif ($periodFilter=='lastyear')
            {
                $query="between '".(date('Y')-1)."-01-01' and '".(date('Y')-1)."-12-31'";

            }
            else{
                $query="between '".$date[0]->format('Y-m-d')."' and '".$date[1]->format('Y-m-d')."'";

            };

            $data=DB::select("
            select
              sp.id,
              sp.name,
              sp.code,
              u.name as unit_name,
              coalesce(
                (
                  select
                    sum(ssi.total)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sp.id
                    and type='in' and transaction_action not like '%Retur%'
                     and ssi.created_at :: date $query
                     and ssi.is_deleted = 0
                     and ssi.inv_warehouse_id=$warehouse
                ),
                0
              ) as stock_in,
              coalesce(
                (
                  select
                    sum(ssi.total)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sp.id
                    and type = 'out'
                    and ssi.is_deleted = 0
                    and ssi.inv_warehouse_id=$warehouse
                     and ssi.created_at :: date $query
                ),
                0
              )-coalesce(
                (
                  select
                    sum(ssi.total)
                  from
                    sc_stock_inventories as ssi
                  where
                    ssi.sc_product_id = sp.id
                    and type = 'in'
                    and transaction_action like '%Retur%'
                    and ssi.inv_warehouse_id=$warehouse
                    and ssi.is_deleted = 0
                    and ssi.created_at :: date $query
                ),
                0
              ) as stock_out,
              coalesce ((
                select
                  sum(ssi.residual_stock)
                from
                  sc_stock_inventories ssi
                where
                  ssi.sc_product_id = sp.id
                  and ssi.is_deleted = 0
                  and ssi.type = 'in' and ssi.transaction_action not like '%Retur%'
                  and ssi.inv_warehouse_id=$warehouse
                  and ssi.created_at :: date $q
              ),0) as residual_stock
            from
              sc_products as sp
              join md_merchants as m on m.md_user_id = sp.md_user_id
              join md_units as u on u.id = sp.md_unit_id
            where
              m.id = $merchantId
              and sp.is_with_stock=1
              and sp.is_deleted = 0
              order by sp.id asc
              offset $offset
            limit
              $limit
            ");

            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function  taxReport($merchantId,Request $request)
    {
        try{
            $periodFilter=(is_null($request->period_filter))?'today':$request->period_filter;
            $dateCustom=(is_null($request->date_custom))?date('Y-m-d'):$request->date_custom;
            if($periodFilter=='custom')
            {
                $date=date('Y-m-d',strtotime($dateCustom));

            }else{
                $date=$this->rangeCriteria->rangeFilter($periodFilter);
            }
            if($periodFilter=='today' || $periodFilter=='yesterday')
            {
                $query="'".$date->format('Y-m-d')."' :: timestamp, '".$date->format('Y-m-d')."' :: timestamp";
                $format='YYYY-MM-DD';
                $interval='1 day';
                $dateTrunch='day';

            }elseif($periodFilter=='custom'){
                $query="'".$date."' :: timestamp, '".$date."' :: timestamp";
                $format='YYYY-MM-DD';
                $interval='1 day';
                $dateTrunch='day';

            }
            elseif ($periodFilter=='thisyear')
            {
                $query="'".date('Y')."-01-01' :: timestamp, '".(date('Y'))."-12-31 23:59:00'";
                $format='Month';
                $interval='1 Month';
                $dateTrunch='Month';


            }
            elseif ($periodFilter=='lastyear')
            {
                $query="'".(date('Y')-1)."-01-01' :: timestamp, '".(date('Y')-1)."-12-31 23:59:00'";
                $format='Month';
                $interval='1 Month';
                $dateTrunch='Month';


            }
            else{
                $query="'".$date[0]->format('Y-m-d')."' :: timestamp, '".$date[1]->format('Y-m-d')."' :: timestamp";
                $format='YYYY-MM-DD';
                $interval='1 day';
                $dateTrunch='day';

            };

            $data=DB::select("
               select
                  to_char(mon.mon, '$format') AS time,
                  coalesce(s.tax, 0) as tax
                from
                  generate_series(
                    $query,
                    interval '$interval'
                  ) as mon(mon)
                  left join (
                    select
                      date_trunc('$dateTrunch', created_at) as mon,
                      sum(so.tax) as tax
                    from
                      sc_sale_orders as so
                    where
                      so.md_sc_transaction_status_id = 2  and so.md_merchant_id=$merchantId
                      and so.is_approved_shop=1 and so.is_cancel_user=0
                      and so.is_deleted=0
                      and so.is_editable=1
                      and so.step_type=0
                    group by
                      mon
                  ) s on mon.mon = s.mon;

            ");
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function inventoryByProduct($merchantId,Request $request)
    {
        try{
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;
            $warehouse=(parent::getGlobalWarehouse($merchantId))->id;

            $data=DB::select("
            select
              sp.id,
              sp.name,
              u.name as unit_name,
              coalesce ((
                select
                  sum(
                    ssi.purchase_price * ssi.residual_stock
                  )
                from
                  sc_stock_inventories ssi
                where
                  ssi.sc_product_id = sp.id
                  and ssi.is_deleted = 0
                  and ssi.type = 'in'
                  and ssi.inv_warehouse_id=$warehouse
                  and ssi.transaction_action not like '%Retur%'
              ),0) as residual_amount,
              coalesce ((
                select
                  sum(ssi.residual_stock)
                from
                  sc_stock_inventories ssi
                where
                  ssi.sc_product_id = sp.id
                  and ssi.is_deleted = 0
                  and ssi.type = 'in'
                and ssi.inv_warehouse_id=$warehouse
                  and ssi.transaction_action not like '%Retur%'
              ),0) as residual_stock
            from
              sc_products sp
              join md_merchants m on sp.md_user_id = m.md_user_id
              join md_units as u on u.id = sp.md_unit_id
            where
              m.id = $merchantId
              and sp.is_with_stock=1
              and sp.is_deleted = 0
              order by sp.id asc
              offset $offset limit $limit
            ");
            $residual=DB::select("
            select
              coalesce(
                sum(
                  ssi.purchase_price * ssi.residual_stock
                ),
                0
              ) as residual_amount
            from
              sc_stock_inventories ssi
              join sc_products sp on sp.id = ssi.sc_product_id
              join md_merchants m on m.md_user_id = sp.md_user_id
            where
              m.id = $merchantId
              and ssi.is_deleted = 0
              and sp.is_deleted = 0
              and ssi.type='in'
              and ssi.inv_warehouse_id=$warehouse
            and ssi.transaction_action not like '%Retur%'
            ");
            $params=[
                'offset'=>$offset,
                'limit'=>$limit,
                'countData'=>Product::join('md_users as u','u.id','sc_products.md_user_id')
                    ->join('md_merchants as m','m.md_user_id','u.id')
                    ->where('m.id',$merchantId)
                    ->where('sc_products.is_deleted',0)
                    ->count(),
                'residual_total'=>$residual,
                'residual'=>$data
            ];
            return $this->message::getJsonResponse(200,trans('message.201'),$params);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function inventoryByCategory($merchantId,Request $request)
    {
        try{
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;
            $warehouse=(parent::getGlobalWarehouse($merchantId))->id;
            $data=DB::select("
            select
              spc.id,
              spc.name,
              coalesce (sum(
                (
                  select
                    sum(
                      ssi.purchase_price * ssi.residual_stock
                    )
                  from
                    sc_stock_inventories ssi
                  where
                    ssi.sc_product_id = sp.id
                    and ssi.is_deleted = 0
                    and ssi.type = 'in'
                     and ssi.inv_warehouse_id=$warehouse
                    and ssi.transaction_action not like '%Retur%'
                )
              ),0)as residual_amount,
              coalesce (sum(
                (
                  select
                    sum(ssi.residual_stock)
                  from
                    sc_stock_inventories ssi
                  where
                    ssi.sc_product_id = sp.id
                    and ssi.is_deleted = 0
                    and ssi.type = 'in'
                     and ssi.inv_warehouse_id=$warehouse
                    and ssi.transaction_action not like '%Retur%'
                )
              ),0) as residual_stock
            from
              sc_product_categories spc
              join sc_products sp on sp.sc_product_category_id = spc.id
              join md_merchants m on sp.md_user_id = m.md_user_id
              join md_units as u on u.id = sp.md_unit_id
            where
              m.id = $merchantId
              and sp.is_with_stock=1
              and spc.is_deleted = 0 and sp.is_deleted=0
            group by
              spc.id
              offset $offset
              limit $limit
           ");
            $residual=DB::select("
            select
              coalesce(
                sum(
                  ssi.purchase_price * ssi.residual_stock
                ),
                0
              ) as residual_amount
            from
              sc_stock_inventories ssi
              join sc_products sp on sp.id = ssi.sc_product_id
              join md_merchants m on m.md_user_id = sp.md_user_id
            where
              m.id = $merchantId
              and sp.is_with_stock=1
              and ssi.is_deleted = 0
              and ssi.type='in'
              and ssi.inv_warehouse_id=$warehouse
              and ssi.transaction_action not like '%Retur%'
              and sp.is_deleted = 0
            ");
            $params=[
                'offset'=>$offset,
                'limit'=>$limit,
                'countData'=>ProductCategory::join('md_users as u','u.id','sc_product_categories.md_user_id')
                    ->join('md_merchants as m','m.md_user_id','u.id')
                    ->where('m.id',$merchantId)
                    ->where('sc_product_categories.is_deleted',0)
                    ->count(),
                'residual_total'=>$residual,
                'residual'=>$data
            ];
            return $this->message::getJsonResponse(200,trans('message.201'),$params);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

}

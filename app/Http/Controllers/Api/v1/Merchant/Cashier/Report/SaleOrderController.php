<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Merchant\Cashier\Report;


use App\Classes\RangeCriteria;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class SaleOrderController extends Controller
{
    protected $message;
    protected $rangeCriteria;

    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->rangeCriteria=new RangeCriteria();

    }

    public function routeApi()
    {
        Route::prefix('v1/merchant/toko/report/sale-order')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('by-product/{merchantId}', 'Api\v1\Merchant\Cashier\Report\SaleOrderController@byProduct')->name('toko.report.sale-order.by-product');
                Route::post('by-category/{merchantId}', 'Api\v1\Merchant\Cashier\Report\SaleOrderController@byCategory')->name('toko.report.sale-order.by-category');

            });
    }

    public function byProduct($merchantId,Request $request)
    {
        try{
            $periodFilter=(is_null($request->period_filter))?'today':$request->period_filter;
            $dateCustom=(is_null($request->date_custom))?date('Y-m-d'):$request->date_custom;
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;
            if($periodFilter=='custom')
            {
                $date=date('Y-m-d',strtotime($dateCustom));

            }else{
                $date=$this->rangeCriteria->rangeFilter($periodFilter);
            }
            if($periodFilter=='today' || $periodFilter=='yesterday')
            {
                $query="='".$date->format('Y-m-d')."'";

            }elseif($periodFilter=='custom'){
                $query="='".$date."'";


            }
            elseif ($periodFilter=='thisyear')
            {
                $query="between '".date('Y')."-01-01' and '".(date('Y'))."-12-31'";

            }
            elseif ($periodFilter=='lastyear')
            {
                $query="between '".(date('Y')-1)."-01-01' and '".(date('Y')-1)."-12-31'";

            }
            else{
                $query="between '".$date[0]->format('Y-m-d')."' and '".$date[1]->format('Y-m-d')."'";

            };

            $data=DB::select("
            select
              sp.id,
              sp.name,
              u.name as unit_name,
              coalesce(
                (
                  select
                    sum(ssod.quantity * ssod.price)
                  from
                    sc_sale_orders sso
                    join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                  where
                    sso.is_deleted = 0
                    and sso.is_editable=1
                    and sso.is_keep_transaction=0
                    and ssod.sc_product_id = sp.id
                    and ssod.is_bonus = 0
                    and sso.md_sc_transaction_status_id not in(3, 5)
                    and sso.step_type in (0, 3)
                    and sso.created_at :: date $query
                ),
                0
              ) as income,
              coalesce(
                (
                  select
                    sum(ssod.profit)
                  from
                    sc_sale_orders sso
                    join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                  where
                    sso.is_deleted = 0
                    and sso.is_editable=1
                    and sso.is_keep_transaction=0
                    and sso.step_type in (0, 3)
                    and ssod.sc_product_id = sp.id
                    and ssod.is_bonus = 0
                    and sso.md_sc_transaction_status_id not in(3, 5)
                    and sso.created_at :: date $query
                ),
                0
              ) as profit,
              coalesce(
                (
                  select
                    sum(ssod.quantity)
                  from
                    sc_sale_orders sso
                    join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                  where
                    sso.is_deleted = 0
                    and sso.is_editable=1
                    and sso.is_keep_transaction=0
                    and ssod.sc_product_id = sp.id
                    and ssod.is_bonus = 0
                    and sso.md_sc_transaction_status_id not in(3, 5)
                    and sso.step_type in (0, 3)
                    and sso.created_at :: date $query
                ),
                0
              ) as quantity
            from
              sc_products sp
              join md_merchants m on m.md_user_id = sp.md_user_id
              join md_units as u on u.id = sp.md_unit_id
            where
              m.id = $merchantId
              and sp.is_deleted = 0
              order by quantity desc
                offset $offset
            limit
              $limit
            ");

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function byCategory($merchantId,Request $request)
    {
        try{
            $periodFilter=(is_null($request->period_filter))?'today':$request->period_filter;
            $dateCustom=(is_null($request->date_custom))?date('Y-m-d'):$request->date_custom;
            $offset=(is_null($request->offset))?0:$request->offset;
            $limit=(is_null($request->limit))?10:$request->limit;
            if($periodFilter=='custom')
            {
                $date=date('Y-m-d',strtotime($dateCustom));

            }else{
                $date=$this->rangeCriteria->rangeFilter($periodFilter);
            }
            if($periodFilter=='today' || $periodFilter=='yesterday')
            {
                $query="='".$date->format('Y-m-d')."'";

            }elseif($periodFilter=='custom'){
                $query="='".$date."'";


            }
            elseif ($periodFilter=='thisyear')
            {
                $query="between '".date('Y')."-01-01' and '".(date('Y'))."-12-31'";

            }
            elseif ($periodFilter=='lastyear')
            {
                $query="between '".(date('Y')-1)."-01-01' and '".(date('Y')-1)."-12-31'";

            }
            else{
                $query="between '".$date[0]->format('Y-m-d')."' and '".$date[1]->format('Y-m-d')."'";

            };

            $data=DB::select("
            select
              spc.id,
              spc.name,
              coalesce(
                (
                  select
                    sum(ssod.quantity * ssod.price)
                  from
                    sc_sale_orders sso
                    join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                    join sc_products sp on sp.id=ssod.sc_product_id
                  where
                    sso.is_deleted = 0 and sp.is_deleted=0 and sso.is_editable=1
                    and sso.is_keep_transaction=0
                    and sso.step_type=0
                    and sp.sc_product_category_id=spc.id and sso.created_at :: date $query
                ),
                0
              ) as income,
              coalesce(
                (
                  select
                    sum(ssod.profit)
                  from
                    sc_sale_orders sso
                    join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                    join sc_products sp on sp.id=ssod.sc_product_id
                  where
                    sso.is_deleted = 0 and sp.is_deleted=0 and sso.is_editable=1
                    and sso.is_keep_transaction=0
                    and sso.step_type=0
                    and sp.sc_product_category_id=spc.id and sso.created_at :: date $query
                ),
                0
              ) as profit,
              coalesce(
                (
                  select
                    sum(ssod.quantity)
                  from
                    sc_sale_orders sso
                    join sc_sale_order_details ssod on sso.id = ssod.sc_sale_order_id
                    join sc_products sp on sp.id=ssod.sc_product_id
                  where
                    sso.is_deleted = 0 and sp.is_deleted=0 and sso.is_editable=1 and sso.step_type=0
                    and sso.is_keep_transaction=0 and sp.sc_product_category_id=spc.id and sso.created_at :: date $query
                ),
                0
              ) as quantity
            from
              sc_product_categories spc
              join sc_products sp on sp.sc_product_category_id = spc.id
              join md_merchants m on sp.md_user_id = m.md_user_id
              join md_units as u on u.id = sp.md_unit_id
            where
              m.id = $merchantId
              and spc.is_deleted = 0
              and sp.is_deleted = 0
            group by
              spc.id
              order by quantity desc
              offset $offset
              limit $limit
            ");

            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }catch (\Exception $e)
        {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }



}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\Accounting\CoaDetail;
use App\Models\Accounting\Jurnal;
use App\Models\Accounting\JurnalDetail;
use App\Models\Accounting\MerchantAr;
use App\Models\Accounting\MerchantArDetail;
use App\Models\MasterData\Merchant;
use App\Utils\Accounting\AccUtil;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\SennaToko\Customer;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\SaleOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Image;
use Illuminate\Support\Facades\Storage;

class AccountReceivableController extends Controller
{
    protected $message;
    protected $customer;
    protected $product;
    protected $ar;
    protected $arDetail;


    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->customer=Customer::class;
        $this->product=Product::class;
        $this->ar=MerchantAr::class;
        $this->arDetail=MerchantArDetail::class;
    }

    public function routeApi()
    {
        Route::prefix('v1/merchant/toko/account-receivable')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function()
            {
                Route::post('list/{merchantId}', 'Api\v1\Merchant\Cashier\AccountReceivableController@list');
                Route::get('one/{id}', 'Api\v1\Merchant\Cashier\AccountReceivableController@one');
                Route::post('pay-debt/{id}', 'Api\v1\Merchant\Cashier\AccountReceivableController@payDebt');
                Route::delete('delete/{detailId}', 'Api\v1\Merchant\Cashier\AccountReceivableController@delete');
                Route::get('detail/{detailId}', 'Api\v1\Merchant\Cashier\AccountReceivableController@detail');






            });
    }

    public function list($merchantId,Request $request)
    {
        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $isDebet=($request->is_debet=='asc')?'asc':'desc';
            $sorting=($request->sorting=='asc')?'desc':'asc';

            $data=$this->ar::select([
                'acc_merchant_ar.*',
                'c.name',
                's.total'
            ])
                ->where('acc_merchant_ar.md_merchant_id',$merchantId)
                ->where('acc_merchant_ar.arable_type','App\Models\SennaToko\SaleOrder')
                ->join('sc_sale_orders as s',function ($join){
                    $join->on('s.id',DB::raw('cast(acc_merchant_ar.arable_id as bigint)'));
                })
                ->with('arable.getDetail')
                ->join('sc_customers as c','c.id','s.sc_customer_id')
                ->with('getDetail.getCoa')
                ->where('acc_merchant_ar.is_editable',1)
                ->where('acc_merchant_ar.is_deleted',0)
                ->orderBy('acc_merchant_ar.id',$sorting)
                ->orderBy('acc_merchant_ar.is_paid_off',$isDebet)
                ->skip($offset)
                ->take($limit)
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function one($id)
    {
        try {
            $data=$this->ar::select([
                'acc_merchant_ar.*',
                'c.name',
                's.total'
            ])
                ->with('arable.getDetail')
                ->where('acc_merchant_ar.arable_type','App\Models\SennaToko\SaleOrder')
                ->join('sc_sale_orders as s',function ($join){
                    $join->on('s.id',DB::raw('cast(acc_merchant_ar.arable_id as bigint)'));
                })
                ->with('getDetail.getCoa')
                ->join('sc_customers as c','c.id','s.sc_customer_id')
                ->where('acc_merchant_ar.id',$id)
                ->first();

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {

            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function payDebt($id,Request $request)
    {
        try{
            DB::beginTransaction();
            $data=new MerchantArDetail();
            $ar=MerchantAr::find($id);
            $date = new \DateTime($ar->created_at);
            if(AccUtil::checkClosingJournal($ar->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            if($ar->is_editable==0){
                return $this->message::getJsonResponse(404, trans('Perhatian! Demi perhitungan akurat di akuntansi mulai dari tanggal 8 Maret 2021 Transaksi Penjualan Maupun Pembelian dan Piutang/Hutang tidak dapat diedit, karena ada penambahan modul akuntansi ,apabila kamu ingin mereset data silahkan lakukan di menu pengaturan dan menghapusnya.'), []);
            }
            if($request->paid_nominal>$ar->residual_amount)
            {
                return $this->message::getJsonResponse(404,trans('Nominal pembayaran melebihi kekurangan yang harus dibayar'),[]);
            }
            if(is_null($request->trans_coa_detail_id))
            {
                return $this->message::getJsonResponse(404,'Metode pembayaran belum dipilih',[]);

            }

            if($request->paid_nominal>=$ar->residual_amount)
            {
                $ar->is_paid_off=1;
                $ar->arable->md_sc_transaction_status_id=TransactionStatus::PAID;
                $ar->arable->save();
            }else{
                $ar->arable->md_sc_transaction_status_id=6;
                $ar->arable->save();
            }
            $ar->residual_amount-=$request->paid_nominal;
            $ar->paid_nominal+=$request->paid_nominal;
            $ar->save();

            $destinationPath = 'public/uploads/merchant/'.$request->md_merchant_id.'/acc/jurnal/ar/';
            if($request->hasFile('trans_proof'))
            {
                $file=$request->file('trans_proof');
                $fileName= uniqid(rand(1,1000000)).'_'.date('YmdHis').'_'.str_replace(' ','_',$file->getClientOriginalName());
                $image = Image::make(request()->file('trans_proof'))->resize(300,300)->encode($file->getClientOriginalExtension());
                $fileName=$destinationPath.$fileName;
                Storage::disk('s3')->put($fileName, (string) $image);

            }else{
                $fileName=null;
            }
            $trans=explode('_',$request->trans_coa_detail_id);

            $data->ar_detail_code=CodeGenerator::generateJurnalCode(0,'PTR',$request->md_merchant_id);
            $data->paid_nominal=$request->paid_nominal;
            $data->paid_date=date('Y-m-d H:i:s');
            $data->note=$request->note;
            $data->acc_merchant_ar_id=$ar->id;
            $data->created_by=$request->created_by;
            $data->timezone=$request->timezone;
            $data->note=$request->note;
            $data->trans_coa_detail_id=$trans[1];
            $data->trans_proof=$fileName;
            $data->save();

            $jurnal=new Jurnal();
            $jurnal->ref_code=$data->ar_detail_code;
            $jurnal->trans_code=CodeGenerator::generateJurnalCode(0,'JRN',$request->md_merchant_id);
            $jurnal->trans_name='Pembayaran Penjualan '.$ar->arable->code.'-'.$data->ar_detail_code;
            $jurnal->trans_time=$data->paid_date;
            $jurnal->trans_note='Pembayaran Penjualan  '.$ar->arable->code.'-'.$data->ar_detail_code;
            $jurnal->trans_purpose='Pembayaran Penjualan  '.$ar->arable->code.'-'.$data->ar_detail_code;
            $jurnal->trans_amount=$data->paid_nominal;
            $jurnal->trans_proof=$fileName;
            $jurnal->md_merchant_id=$request->md_merchant_id;
            $jurnal->md_user_id_created=$request->created_by;
            $jurnal->external_ref_id=$data->id;
            $jurnal->flag_name='Pembayaran Penjualan  '.$ar->arable->code;
            $jurnal->save();


            $fromCoa=Merchant::find($request->md_merchant_id);
            $toCoa=$trans[1];
            $insert=[
                [
                    'acc_coa_detail_id'=>$fromCoa->coa_ar_sale_id,
                    'coa_type'=>'Kredit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$data->paid_nominal,
                    'created_at'=>$data->paid_date,
                    'updated_at'=>$data->paid_date,
                    'acc_jurnal_id'=>$jurnal->id
                ],
                [
                    'acc_coa_detail_id'=>$toCoa,
                    'coa_type'=>'Debit',
                    'md_sc_currency_id'=>1,
                    'amount'=>$data->paid_nominal,
                    'created_at'=>$data->paid_date,
                    'updated_at'=>$data->paid_date,
                    'acc_jurnal_id'=>$jurnal->id
                ],
            ];
            JurnalDetail::insert($insert);

            DB::commit();

            return $this->message::getJsonResponse(200,trans('message.200'),$data);


        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);

        }
    }

    public function delete($detailId)
    {
        try {
            $data=MerchantArDetail::find($detailId);
            $ar=MerchantAr::find($data->acc_merchant_ar_id);
            $date = new \DateTime($ar->created_at);
            if(AccUtil::checkClosingJournal($ar->md_merchant_id,$date->format('Y-m-d'))==true)
            {
                return $this->message::getJsonResponse(500, 'Terjadi kesalahan! Transaksi tidak dapat diproses dikarenakan periode pelaporan keuangan dari transaksi yang dilakukan telah ditutup. Transaksi dapat diproses apabila jurnal penutup dari periode pelaporan keuangan.', []);
            }
            $ar->residual_amount+=$data->paid_nominal;
            $ar->paid_nominal-=$data->paid_nominal;
            $ar->is_paid_off=0;
            $ar->save();
            if($ar->arable->paid_nominal>0)
            {
                if($ar->arable->paid_nominal==$data->paid_nominal)
                {
                    $ar->arable->paid_nominal-=$data->paid_nominal;
                }
            }
            $ar->arable->save();

            Jurnal::where([
                'external_ref_id'=>$data->id,
                'ref_code'=>$data->ar_detail_code
            ])->update([
                'is_deleted'=>1
            ]);
            $data->delete();
            return $this->message::getJsonResponse(200,trans('message.202'),[]);
        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

    public function detail($detailId)
    {
        try {
            $data=$this->arDetail::find($detailId);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.200'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class, Route::currentRouteAction(), $e->getLine(), $e->getFile(), $e->getMessage());
            return $this->message::getJsonResponse(500, trans('custom.errors'), []);
        }
    }

}

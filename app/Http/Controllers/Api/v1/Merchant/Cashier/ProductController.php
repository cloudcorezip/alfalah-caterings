<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Merchant\Cashier;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\SennaToko\Material;
use App\Models\SennaToko\Merk;
use App\Models\SennaToko\Product;
use App\Models\SennaToko\StockInventory;
use App\Utils\Account\AccountUtil;
use App\Utils\Accounting\CoaProductUtil;
use App\Utils\App\AppUtil;
use App\Utils\Merchant\ProductUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Models\SennaToko\SaleOrderDetail;
use App\Models\SennaToko\PurchaseOrderDetail;
use Ramsey\Uuid\Uuid;

class ProductController extends Controller
{

    protected $product;
    protected $message;
    protected $inventory;
    protected $material;

    public function __construct()
    {
        $this->product=Product::class;
        $this->material=Material::class;
        $this->message=Message::getInstance();
        $this->inventory=StockInventory::class;
    }


    public function routeApi()
    {
        Route::prefix('v1/merchant/toko/product')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/one/{id}', 'Api\v1\Merchant\Cashier\ProductController@getOne');
                Route::get('/all-web-v2', 'Api\v1\Merchant\Cashier\ProductController@getAllWebV2')->name('api.product.all-v2');
                Route::get('/one-web-v2', 'Api\v1\Merchant\Cashier\ProductController@getByCodeV2')->name('api.product.one-v2');
                Route::get('/all-non-package', 'Api\v1\Merchant\Cashier\ProductController@getAllNonPackage')->name('api.product.non-package');
                Route::get('/all-stock-adjusment', 'Api\v1\Merchant\Cashier\ProductController@getAllStockAdjustment')->name('api.product.stockadjustment');
                Route::get('/all-web-po/{userId}', 'Api\v1\Merchant\Cashier\ProductController@getAllWebPurchase')->name('api.product.all-po');
                Route::get('/all-web-po-v2', 'Api\v1\Merchant\Cashier\ProductController@getAllWebPurchaseV2')->name('api.product.all-po-v3');
            });

    }

    public function getAllWebV2(Request $request)
    {
        $search = $request->key;
        $merchantId=$request->md_merchant_id;

        if($search == ''){
            $data=$this->product::select([
                'sc_products.id',
                'sc_products.name as text',
                'sc_products.stock',
                'sc_products.purchase_price',
                'sc_products.selling_price',
                'sc_products.code',
                'sc_products.inv_id as coa_inv_id',
                'sc_products.hpp_id as coa_hpp_id',
                DB::raw("coalesce(u.name,'') as satuan"),
                'u.id as satuan_id',
                DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_products.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
                ")

            ])
                ->join('md_merchants as m','m.md_user_id','sc_products.md_user_id')
                ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->where('m.id',$merchantId)
                ->where('sc_products.is_deleted',0)
                ->get();
        }else{
            $data=$this->product::select([
                'sc_products.id',
                'sc_products.name as text',
                'sc_products.stock',
                'sc_products.purchase_price',
                'sc_products.selling_price',
                'sc_products.code',
                'sc_products.inv_id as coa_inv_id',
                'sc_products.hpp_id as coa_hpp_id',
                DB::raw("coalesce(u.name,'') as satuan"),
                'u.id as satuan_id',
                DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_products.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
                ")


            ])
                ->join('md_merchants as m','m.md_user_id','sc_products.md_user_id')
                ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->where('m.id',$merchantId)
                ->whereRaw("upper(sc_products.name) like '%".strtoupper($search)."%' ")
                ->where('sc_products.is_deleted',0)
                ->get();
        }

        $response =[];
        foreach($data as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->text.'-'.$item->code,
                'stock'=>$item->stock,
                'code'=>$item->code,
                'purchase_price'=>$item->purchase_price,
                'selling_price'=>$item->selling_price,
                'coa_inv_id'=>$item->coa_inv_id,
                'coa_hpp_id'=>$item->coa_hpp_id,
                'satuan'=>$item->satuan,
                'satuan_id'=>$item->satuan_id,
                'multi_unit'=>$item->multi_unit
            ];
        }
        return response()->json($response);
    }

    public function getByCodeV2(Request $request)
    {
        $merchantId=$request->md_merchant_id;
        $checkCode=$this->product::
        select([
            'sc_products.id',
            'sc_products.name as text',
            'sc_products.stock',
            'sc_products.purchase_price',
            'sc_products.selling_price',
            'sc_products.code',
            'sc_products.inv_id as coa_inv_id',
            'sc_products.hpp_id as coa_hpp_id',
            DB::raw("coalesce(u.name,'') as satuan"),
            'u.id as satuan_id',
            DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_products.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
                ")

        ])
            ->join('md_merchants as m','m.md_user_id','sc_products.md_user_id')
            ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
        ->where('sc_products.code',$request->code)
            ->where('m.id',$merchantId)
            ->where('sc_products.is_deleted',0)
            ->first();
        if(is_null($checkCode)){
            return response()->json(0);
        }else{
            $response=[
                "id"=>$checkCode->id,
                "text"=>$checkCode->text.' '.$checkCode->code,
                'code'=>$checkCode->code,
                'stock'=>$checkCode->stock,
                "defaultSelected"=>true,
                "selected"=>true,
                'purchase_price'=>$checkCode->purchase_price,
                'selling_price'=>$checkCode->selling_price,
                'coa_inv_id'=>$checkCode->coa_inv_id,
                'coa_hpp_id'=>$checkCode->coa_hpp_id,
                'satuan'=>$checkCode->satuan,
                'satuan_id'=>$checkCode->satuan_id,
                'multi_unit'=>$checkCode->multi_unit
            ];
            return response()->json($response);
        }

    }

    public function getAllNonPackage(Request $request)
    {
        try{
            $search = $request->key;
            $userId=$request->userId;
            if($search == ''){
                $data=$this->product::select([
                    'sc_products.id',
                    'sc_products.name as text',
                    'stock',
                    'purchase_price',
                    'selling_price',
                    'code',
                    'inv_id as coa_inv_id',
                    'hpp_id as coa_hpp_id',
                    'u.name as unit_name',
                    'u.id as unit_id',
                    DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_products.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
                ")
                ])
                    ->join('md_units as u','u.id','sc_products.md_unit_id')
                    ->where('md_user_id',$userId)
                    ->where('is_deleted',0)
                    ->where('md_sc_product_type_id','!=',4)
                    ->get();
            }else{
                $data=$this->product::select([
                    'sc_products.id',
                    'sc_products.name as text',
                    'stock',
                    'purchase_price',
                    'selling_price',
                    'code',
                    'inv_id as coa_inv_id',
                    'hpp_id as coa_hpp_id',
                    'u.name as unit_name',
                    'u.id as unit_id',
                    DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_products.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
                ")
                ])
                    ->join('md_units as u','u.id','sc_products.md_unit_id')
                    ->where('md_user_id',$userId)
                    ->whereRaw("upper(sc_products.name) like '%".strtoupper($search)."%' ")
                    ->where('is_deleted',0)
                    ->where('md_sc_product_type_id','!=',4)
                    ->get();
            }


            $response =[];
            foreach($data as $item){
                $response[] = [
                    "id"=>$item->id,
                    "text"=>$item->text.'-'.$item->code,
                    'stock'=>$item->stock,
                    'code'=>$item->code,
                    'purchase_price'=>$item->purchase_price,
                    'selling_price'=>$item->selling_price,
                    'coa_inv_id'=>$item->coa_inv_id,
                    'coa_hpp_id'=>$item->coa_hpp_id,
                    'unit_name'=>$item->unit_name,
                    'unit_id'=>$item->unit_id,
                    'multi_unit'=>$item->multi_unit
                ];
            }
            return response()->json($response);
        }catch (\Exception $e)
        {
            return response()->json($e);


        }

    }

    public function getAllStockAdjustment(Request $request)
    {
        $merchantId=$request->merchant_id;
        $warehouse=$request->warehouse;
        $search = strtoupper($request->key);
        $isConsignment=is_null($request->is_consignment)?0:$request->is_consignment;

        if($search == ''){
            $data=DB::select("select
              sp.id,
              sp.code,
              sp.name as text,
              sp.unique_code,
              sp.inv_id,
              sp.hpp_id,
              mu.name as unit_name,
              sp.selling_price,
              sp.purchase_price,
              coalesce(s.residual_stock, 0) as real_stock,
              mu.id as unit_id,
              coalesce(
                (
                SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                    (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
            from
              sc_products sp
              left join (
                select
                  ssi.sc_product_id,
                  sum(ssi.residual_stock) as residual_stock
                from
                  sc_stock_inventories ssi
                where
                  ssi.inv_warehouse_id = $warehouse
                  and type = 'in'
                  and ssi.transaction_action not like '%Retur%'
                  and ssi.is_deleted = 0
                group by
                  ssi.sc_product_id
              ) as s on s.sc_product_id = sp.id
              join md_merchants  m on m.md_user_id=sp.md_user_id
              left join md_units mu on mu.id=sp.md_unit_id
            where
              m.id=$merchantId
              and sp.is_deleted = 0
              and sp.is_with_stock=1
              and sp.is_consignment=$isConsignment

            ");
        }else{
            $data=DB::select("select
              sp.id,
              sp.code,
              sp.name as text,
              sp.unique_code,
              sp.inv_id,
              sp.hpp_id,
              mu.name as unit_name,
              sp.selling_price,
              sp.purchase_price,
              coalesce(s.residual_stock, 0) as real_stock,
              mu.id as unit_id,
              coalesce(
                (
                SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                    (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sp.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
            from
              sc_products sp
              left join (
                select
                  ssi.sc_product_id,
                  sum(ssi.residual_stock) as residual_stock
                from
                  sc_stock_inventories ssi
                where
                  ssi.inv_warehouse_id = $warehouse
                  and type = 'in'
                  and ssi.transaction_action not like '%Retur%'
                  and ssi.is_deleted = 0
                group by
                  ssi.sc_product_id
              ) as s on s.sc_product_id = sp.id
            join md_merchants  m on m.md_user_id=sp.md_user_id
              left join md_units mu on mu.id=sp.md_unit_id

            where
              m.id=$merchantId
              and sp.is_deleted = 0
            and sp.is_with_stock=1
            and sp.is_consignment=$isConsignment
            and upper(sp.name) like '%$search%'
            ");
        }

        $response =[];
        foreach($data as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->text.'-'.$item->code,
                'stock'=>$item->real_stock,
                'code'=>$item->code,
                'unique_code'=>$item->unique_code,
                'inv_id'=>$item->inv_id,
                'hpp_id'=>$item->hpp_id,
                'unit_name'=>$item->unit_name,
                'unit_id'=>$item->unit_id,
                'multi_unit'=>$item->multi_unit,
                'selling_price'=>$item->selling_price,
                'purchase_price'=>$item->purchase_price
            ];
        }
        return response()->json($response);
    }

    public function getAllWebPurchaseV2(Request $request)
    {
        $search = $request->key;
        $merchantId=$request->md_merchant_id;

        if($search == ''){
            $data=$this->product::select([
                'sc_products.id',
                'sc_products.name as text',
                'sc_products.stock',
                'sc_products.purchase_price',
                'sc_products.selling_price',
                'sc_products.code',
                'sc_products.unique_code',
                'sc_products.inv_id as coa_inv_id',
                'sc_products.hpp_id as coa_hpp_id',
                DB::raw("coalesce(u.name,'') as satuan"),
                'u.id as satuan_id',
                DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_products.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
                ")

            ])
                ->join('md_merchants as m','m.md_user_id','sc_products.md_user_id')
                ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->where('sc_products.is_deleted',0)
                ->where('sc_products.is_with_stock',1)
                ->where('sc_products.md_sc_product_type_id','!=',2)
                ->where('m.id',$merchantId)
                ->get();
        }else{
            $data=$this->product::select([
                'sc_products.id',
                'sc_products.name as text',
                'sc_products.stock',
                'sc_products.purchase_price',
                'sc_products.selling_price',
                'sc_products.code',
                'sc_products.unique_code',
                'sc_products.inv_id as coa_inv_id',
                'sc_products.hpp_id as coa_hpp_id',
                DB::raw("coalesce(u.name,'') as satuan"),
                'u.id as satuan_id',
                DB::raw("coalesce(
                  (
                    SELECT
                      jsonb_agg(jd.*) AS jsonb_agg
                    FROM
                      (
                        select
                          spmu.id,
                          spmu.quantity as nilai_konversi,
                          u2.name as konversi_dari,
                          u.name as konversi_ke,
                          spmu.price as harga_jual,
                          spmu.sc_product_id,
                          spmu.md_unit_id as unit_id,
                          p.md_unit_id as product_unit_id
                        from
                          sc_product_multi_units spmu
                          join sc_products as p on p.id = spmu.sc_product_id
                          left join md_units as u on u.id = spmu.md_unit_id
                          left join md_units as u2 on u2.id = p.md_unit_id
                        where
                          spmu.sc_product_id = sc_products.id
                          and spmu.is_deleted = 0
                        order by
                          spmu.id asc
                      ) jd
                  ),
                  '[]'
                ) as multi_unit
                ")

            ])
                ->join('md_merchants as m','m.md_user_id','sc_products.md_user_id')
                ->leftJoin('md_units as u','u.id','sc_products.md_unit_id')
                ->whereRaw("upper(sc_products.name) like '%".strtoupper($search)."%' ")
                ->where('sc_products.is_deleted',0)
                ->where('sc_products.is_with_stock',1)
                ->where('sc_products.md_sc_product_type_id','!=',2)
                ->where('m.id',$merchantId)
                ->get();
        }

        $response =[];
        foreach($data as $item){
            $response[] = [
                "id"=>$item->id,
                "text"=>$item->text.' '.$item->code,
                'stock'=>$item->stock,
                'purchase_price'=>$item->purchase_price,
                'selling_price'=>$item->selling_price,
                'coa_inv_id'=>$item->coa_inv_id,
                'coa_hpp_id'=>$item->coa_hpp_id,
                'satuan'=>$item->satuan,
                'satuan_id'=>$item->satuan_id,
                'multi_unit'=>$item->multi_unit
            ];
        }
        return response()->json($response);
    }







}

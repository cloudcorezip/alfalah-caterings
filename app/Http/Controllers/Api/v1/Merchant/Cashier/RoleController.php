<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Merchant\Cashier;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\AvailableBank;
use App\Models\MasterData\Bank;
use App\Models\MasterData\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class RoleController extends Controller
{

    protected $role;
    protected $message;

    public function __construct()
    {
        $this->role=Role::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v1/merchant/toko/role')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
            Route::get('/all', 'Api\v1\Merchant\Cashier\RoleController@getAll');
        });
    }

    public function getAll()
    {
        try{
            $data=$this->role::where('id','>=',11)
            ->orderBy('id','ASC')->get();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Merchant;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\MerchantStaffMenuMobile;
use App\Models\MasterData\User;
use App\Models\SennaToko\MerchantDiscount;
use App\Utils\Merchant\MerchantUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class MenuController extends Controller
{
    protected $menu;
    protected $user;
    protected $message;

    public function __construct()
    {
        $this->menu=MerchantStaffMenuMobile::class;
        $this->user=User::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v1/merchant/menu')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::post('/create/{merchantId}', 'Api\v1\Merchant\MenuController@create');
                Route::post('/update/{id}', 'Api\v1\Merchant\MenuController@update');
                Route::get('/by-staff/{staffId}', 'Api\v1\Merchant\MenuController@getOne');
                Route::delete('/delete/{id}', 'Api\v1\Merchant\MenuController@delete');


            });
    }

    public function create($merchantId,Request $request)
    {
        try {
            if(is_null(MerchantUtil::checkMerchant($merchantId)))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            $data = new $this->menu;
            $validator = Validator::make($request->all(), $data->rule);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $data->md_user_id_staff=$request->md_user_id_staff;
            $data->menus=$request->menus;
            $data->sync_id=$merchantId.Uuid::uuid4()->toString();
            $data->md_merchant_id=$merchantId;
            $data->save();
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function update($id,Request $request)
    {
        try {

            $data = $this->menu::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $validator = Validator::make($request->all(), $data->rule);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $data->menus=$request->menus;
            $data->md_user_id_staff=$request->md_user_id_staff;
            $data->save();

            $user = $this->user::find($request->md_user_id_staff);
            $user->is_update=1;
            $user->save();
            
            return $this->message::getJsonResponse(200,trans('message.200'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


    public function getOne($staffId)
    {
        try {

            $data=$this->menu::where('md_user_id_staff',$staffId)
            ->where('is_deleted',0)
            ->first()
            ;

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }
    public function delete($id)
    {
        try {

            $this->menu::where('id',$id)
                ->update([
                    'is_deleted'=>1
                ]);
            ;
            return $this->message::getJsonResponse(200,trans('message.202'),[]);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\Notification;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\UserActivityNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class UserActivityNotificationController extends Controller
{


    protected $userNotification;
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
        $this->userNotification=UserActivityNotification::class;
    }

    public function routeApi()
    {
        Route::prefix('v1/notification/activity')
            ->middleware('api-verification')
            ->group(function () {
            Route::post('list/{userId}', 'Api\v1\Notification\UserActivityNotificationController@list');
            Route::get('read/{id}', 'Api\v1\Notification\UserActivityNotificationController@read');
            Route::get('one/{id}', 'Api\v1\Notification\UserActivityNotificationController@one');
            Route::get('count/{userId}', 'Api\v1\Notification\UserActivityNotificationController@count');

        });
    }

    public function count($userId)
    {
        try{

            $data=$this->userNotification::where('md_user_id',$userId)
                ->where('is_read',0)->count();
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }

    }
    public function list($userId,Request $request)
    {
        try {
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;
            $data=$this->userNotification::where('md_user_id',$userId)
                ->skip($offset)
                ->take($limit)
                ->orderBy('created_at','DESC')
                ->get();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }

    }

    public function read($id)
    {
        try {
            $data=$this->userNotification::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            $data->is_read=1;
            $data->save();
            return $this->message::getJsonResponse(200,trans('notification.success'),[]);


        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function one($id)
    {
        try {
            $data=$this->userNotification::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

}

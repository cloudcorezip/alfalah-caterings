<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\MasterData;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Subscription;
use App\Models\MasterData\TransactionType;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;

class SubscriptionController extends Controller
{
    protected $subscription;
    protected $message;

    public function __construct()
    {
        $this->subscription=Subscription::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v1/master-data/subscription')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/all', 'Api\v1\MasterData\SubscriptionController@getAll');
            });
    }

    public function getAll()
    {
        try {

            $data=$this->subscription::where('is_deleted',0)
                ->orderBy('price','desc')
                ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            $result=[];
            foreach ($data as $key =>$item)
            {
                $result[]=[
                    "id"=>$item->id,
                    "price"=> $item->price,
                    "created_at"=>$item->created_at,
                    "updated_at"=>$item->updated_at,
                    "name"=>$item->name,
                    "description"=>$item->description,
                    "is_deleted"=> $item->is_deleted,
                    "max_staff"=> $item->max_branch,
                    "max_branch"=> $item->max_staff,
                    "max_transaction"=> $item->max_transaction,
                    "max_transaction_online"=> $item->max_transaction_online,
                    "banner"=> $item->banner,
                    "plus_month"=> $item->plus_month,
                    "type"=> $item->type,
                    "discount_percentage"=> $item->discount_percentage
                ];
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$result);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

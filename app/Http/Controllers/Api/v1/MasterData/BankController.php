<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\MasterData;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\AvailableBank;
use App\Models\MasterData\Bank;
use App\Models\MasterData\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BankController extends Controller
{

    protected $bank;
    protected $availableBank;
    protected $message;

    public function __construct()
    {
        $this->bank=Bank::class;
        $this->availableBank=AvailableBank::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v1/master-data/bank')
            ->middleware('api-verification')
            ->group(function(){
            Route::get('/all', 'Api\v1\MasterData\BankController@getAll');
            Route::get('/available-bank', 'Api\v1\MasterData\BankController@getAvailableBank');
            Route::get('/va-available-bank', 'Api\v1\MasterData\BankController@getVAAvailableBank');
            });
    }

    public function getAll(Request $request)
    {
        try{
            $search=$request->search;
            if(is_null($search))
            {
                $data=$this->bank::orderBy('id','ASC')->get();
            }else{
                $data=$this->bank::where(DB::raw('upper(name)'), 'LIKE', '%'.strtoupper($search).'%')
                    ->orderBy('id','ASC')->get()
                    ->get();
            }

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }


    public function getAvailableBank(Request $request)
    {
        try{
            $search=$request->search;
            if(is_null($search))
            {
                $data=$this->availableBank::
                select([
                    'md_banks.id',
                    'md_available_banks.bank_account_name',
                    'md_available_banks.bank_no_rek',
                    'md_banks.code',
                    'md_banks.name',
                    'md_banks.icon'
                ])->join('md_banks','md_banks.id','md_available_banks.md_bank_id')
                ->where('md_available_banks.is_active',1)
                    ->get();
                ;
            }else{
                $data=
                    $this->availableBank::
                select([
                    'md_banks.id',
                    'md_available_banks.bank_account_name',
                    'md_available_banks.bank_no_rek',
                    'md_banks.code',
                    'md_banks.name',
                        'md_banks.icon'
                ])->join('md_banks','md_banks.id','md_available_banks.md_bank_id')
                    ->where('md_available_banks.is_active',1)
                        ->where(DB::raw('upper(name)'), 'LIKE', '%'.strtoupper($search).'%')
                    ->get();

            }

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }


    public function getVAAvailableBank()
    {
        try{


            $data=
                DB::table('md_va_banks')
                ->select([
                    'md_va_banks.id',
                    'md_va_banks.code as bank_code',
                    'md_banks.name',
                    'md_banks.icon',
                    'md_va_banks.description',
                    'md_va_banks.plus_amount'
                ])->join('md_banks','md_banks.id','md_va_banks.md_bank_id')
                    ->where('md_va_banks.is_active',1)
                    ->get();

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

}

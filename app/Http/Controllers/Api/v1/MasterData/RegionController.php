<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\MasterData;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\Region\City;
use App\Models\MasterData\Region\District;
use App\Models\MasterData\Region\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class RegionController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v1/master-data/region')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/city', 'Api\v1\MasterData\RegionController@getCity')->name('api.city.get');
                Route::get('/district', 'Api\v1\MasterData\RegionController@getDistrict')->name('api.district.get');

            });
    }
    public function getCity(Request $request)
    {

        $data=City::select([
            'md_cities.id',
            'md_cities.name as text'
        ])
            ->join('md_provinces as n','n.id','md_cities.md_province_id')
            ->where('md_province_id',$request->province)
            ->get();

        $response =[];
        $response[] = [
            "id"=>-1,
            "text"=>"--Pilih Kabupaten--",
            "disabled"=>true,
            "selected"=>true
        ];
        foreach($data as $key => $item){

            $response[] = [
                "id"=>$item->id,
                "text"=>$item->text
            ];


        }

        return response()->json($response);
    }

    public function getDistrict(Request $request)
    {

        $data=District::select([
            'md_districts.id',
            'md_districts.name as text'
        ])
            ->join('md_cities as n','n.id','md_districts.md_city_id')
            ->where('md_city_id',$request->city)
            ->get();

        $response =[];
        $response[] = [
            "id"=>-1,
            "text"=>"--Pilih Kecamatan--",
            "disabled"=>true,
            "selected"=>true
        ];
        foreach($data as $key => $item){

            $response[] = [
                "id"=>$item->id,
                "text"=>$item->text
            ];


        }

        return response()->json($response);
    }


}

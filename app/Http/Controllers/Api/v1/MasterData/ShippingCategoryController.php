<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Http\Controllers\Api\v1\MasterData;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\SennaCashier\Category;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ShippingCategoryController extends Controller
{

    protected $shippingCategory;
    protected $message;

    public function __construct()
    {
        $this->shippingCategory=ShippingCategory::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::prefix('v1/master-data/senna-toko/shipping-category')
            ->middleware('etag')
            ->middleware('api-verification')
            ->group(function(){
                Route::get('/parent-category', 'Api\v1\MasterData\ShippingCategoryController@getParent');
                Route::get('/child-category/{parentId}', 'Api\v1\MasterData\ShippingCategoryController@getChild');
                Route::get('/one/{id}', 'Api\v1\MasterData\ShippingCategoryController@getOne');
                Route::get('/category-group','Api\v1\MasterData\ShippingCategoryController@getGroup');
            });
    }

    public function getParent()
    {
        try {
            $data=$this->shippingCategory::whereNull('parent_id')
                ->isActive(1)
                ->orderBy('order_number','ASC')
                ->get()
            ;

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getGroup()
    {
        try {
            $redisKey=Redis::get('shippingCategoryGroup');
            if(!is_null($redisKey))
            {
                return $this->message::getJsonResponse(200,trans('message.201'),json_decode($redisKey));

            }else{
                $data=$this->shippingCategory::with('getChild')
                    ->whereNull('parent_id')
                    ->isActive(1)
                    ->orderBy('order_number','ASC')
                    ->get()
                ;

                if($data->count()<1)
                {
                    return $this->message::getJsonResponse(404,trans('message.404'),[]);

                }
                Redis::set('shippingCategoryGroup',json_encode($data));
                Redis::expire('shippingCategoryGroup', ( 24*3600));
                return $this->message::getJsonResponse(200,trans('message.201'),$data);
            }

        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getChild($parentId)
    {
        try {
            $data=$this->shippingCategory::
            where('parent_id',$parentId)
                ->isActive(1)
                ->orderBy('order_number','ASC')
                ->get()
            ;

            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function getOne($id)
    {
        try {

            $data=$this->shippingCategory::find($id);

            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);
        }
        catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }


}

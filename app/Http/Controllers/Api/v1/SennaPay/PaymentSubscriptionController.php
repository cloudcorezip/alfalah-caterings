<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\SennaPay;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Classes\Singleton\XenditCore;
use App\Http\Controllers\Controller;
use App\Jobs\SendSuccessfulSubscription;
use App\Mail\EmailForSuccessfulPlugin;
use App\Mail\EmailForSuccessfulSubscription;
use App\Mails\Account\ForgetEmail;
use App\Models\Marketing\Promo;
use App\Models\MasterData\SennaPayment\TransactionStatus;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\Subscription;
use App\Models\MasterData\User;
use App\Models\Plugin\EnableMerchantPlugin;
use App\Models\SennaPayment\PaymentPlugin;
use App\Models\SennaPayment\PaymentSubscription;
use App\Models\SennaPayment\Transaction;
use App\Utils\Account\AccountUtil;
use App\Utils\Membership\Merchant\RedeemUtil;
use App\Utils\Order\SaleOrderUtil;
use App\Utils\Plugin\PluginUtil;
use App\Utils\Subscription\SubscriptionUtil;
use App\Utils\ThirdParty\VirtualAccountUtil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use QrCode;

class PaymentSubscriptionController extends Controller
{
    protected $user;
    protected $message;
    protected $payment;
    protected $codeGenerator;
    protected $xendit;
    protected  $virtualAccount;

    public function __construct()
    {
        $this->user=User::class;
        $this->message=Message::getInstance();
        $this->payment=PaymentSubscription::class;
        $this->codeGenerator=CodeGenerator::getInstance();
        $this->xendit=XenditCore::getInstance();
        $this->virtualAccount=new VirtualAccountUtil();

    }

    public function routeApi()
    {
        Route::prefix('v1/senna-pay/payment-subscription')
            ->group(function()
            {
                Route::post('/pay/{userId}', 'Api\v1\SennaPay\PaymentSubscriptionController@payment') ->middleware('api-verification');
                Route::post('callback','Api\v1\SennaPay\PaymentSubscriptionController@callbackSubscription')->name('pay.subscription.qris');
                Route::post('callback-transaction','Api\v1\SennaPay\PaymentSubscriptionController@callbackPaySubs')->name('pay.subscription.callback')
                    ->middleware('api-verification');
                Route::post('list/{userId}','Api\v1\SennaPay\PaymentSubscriptionController@list')
                    ->middleware('api-verification');
                Route::post('callback-web','Api\v1\SennaPay\PaymentSubscriptionController@callbackSubscriptionWeb')->name('pay.subscription.web');
            });
    }

    public function payment(Request $request,$userId)
    {
        try {
            date_default_timezone_set((is_null($request->timezone))?'Asia/Jakarta':$request->timezone);
            DB::table('xendit_responses')
                ->insert([
                    'response'=>json_encode($request->all()),
                    'external_code'=>'-'
                ]);

            DB::beginTransaction();
            $user=$this->user::find($userId);
            if(is_null($user))
            {
                return $this->message::getJsonResponse(404,trans('custom.account_not_found'),[]);
            }
            $data=new $this->payment;
            $validator = Validator::make($request->all(),$data->rule);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404,$error,[]);
            }
            if(is_null($request->md_transaction_type_id)|| $request->md_transaction_type_id==\App\Models\MasterData\TransactionType::SENNA_PAY)
            {
                if(AccountUtil::checkBalance($userId,$request->amount)==0)
                {
                    return $this->message::getJsonResponse(404,trans('custom.scan_qr_error'),[]);
                }
            }
            $subscription=Subscription::find($request->md_subscription_id);
            $code=$this->codeGenerator::generateTransactionCode($userId,TransactionType::SUBSCRIPTION_FEE);
            $data->md_merchant_id=$request->md_merchant_id;
            $data->md_subscription_id=$request->md_subscription_id;
            $data->md_transaction_type_id=$request->md_transaction_type_id;
            $data->refferal_code=$request->refferal_code;
            $data->save();
            RedeemUtil::checkReferalCode($request->refferal_code,$request->md_merchant_id,$subscription->id);
            DB::commit();

            if(is_null($request->md_transaction_type_id)|| $request->md_transaction_type_id==\App\Models\MasterData\TransactionType::SENNA_PAY)
            {
                $endDate=SubscriptionUtil::calculateSubsWithSennaPay($request,$user,$subscription);
                if($endDate==false)
                {
                    return $this->message::getJsonResponse(404,'Pembayaran gagal dilakukan,cobalah beberapa saat lagi',[]);

                }
            }else{
                $endDate=SubscriptionUtil::calculateSubsVAOrQRIS($user,$subscription);
                if($endDate==false)
                {
                    return $this->message::getJsonResponse(404,'Pembayaran gagal dilakukan,cobalah beberapa saat lagi',[]);
                }
            }

            $promo=DB::select("select * from mrkt_promos mp where mp.content @>'[{".'"id"'.":".'"'."".$subscription->id."".'"'."}]'
            and '".date('Y-m-d')."' between mp.start_period::date and mp.end_period::date
            and mp.code='".$request->refferal_code."' limit 1");

            $promoPercentage=false;

            if(count($promo)>0)
            {
                if($promo[0]->value==0)
                {
                    $valuePromo=$promo[0]->value_percentage;
                    $promoPercentage=true;

                }else{
                    $valuePromo=$promo[0]->value;
                    $promoPercentage=false;
                }
            }else{
                $valuePromo=0;
            }


            if(is_null($request->md_transaction_type_id) || $request->md_transaction_type_id==\App\Models\MasterData\TransactionType::SENNA_PAY)
            {

                $data->transaction()->create([
                    'code'=>$code,
                    'md_user_id'=>$userId,
                    'md_sp_transaction_type_id'=>TransactionType::SUBSCRIPTION_FEE,
                    'md_sp_transaction_status_id'=>TransactionStatus::SUCCESS,
                    'amount'=>$request->amount,
                    'transaction_from'=>$user->fullname,
                    'transaction_to'=>'Pembayaran Langganan Senna dengan SennaPay'
                ]);
                $data->getMerchant->md_subscription_id=$subscription->id;
                $data->getMerchant->save();
                $forgot=new EmailForSuccessfulSubscription($user->email,$data);
                Mail::to($user->email)->send($forgot);
                DB::commit();
                return $this->message::getJsonResponse(200,trans('custom.pay_subscription_fee',[
                    'end_date'=>$endDate
                ]),[
                    'is_qris'=>0,
                    'is_sennapay'=>1,
                    'is_va'=>0,
                    'is_wallet'=>0,
                    'is_cc'=>0

                ]);

            }elseif($request->md_transaction_type_id==\App\Models\MasterData\TransactionType::QRIS){
                $price = ceil($data->getSubscription->price - ($data->getSubscription->price * ($data->getSubscription->discount_percentage/100)));
                if($promoPercentage==true)
                {
                    $tmpPrice=ceil($price - ($price*($valuePromo/100)));
                }else{
                    $tmpPrice=ceil($price - $valuePromo);
                }

                $adminFee = ceil(0.007 * $tmpPrice);
                $amount = ceil($adminFee + $tmpPrice);
                $discount=$data->getSubscription->price-$price;
                $params = [
                    'external_id' => $code,
                    'type' => 'DYNAMIC',
                    'callback_url' =>\route('pay.subscription.qris'),
                    'amount' =>(float)$amount,
                ];

                $qris=$this->xendit::createQRIS($params);
                if($qris==false)
                {
                    DB::rollBack();
                    return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
                }else{
                    $destinationPath = 'public/uploads/qris/subscription/'.$userId.'/qr/';
                    $qrCodeFile=$destinationPath.$code.'.png';
                    $imageQR=QrCode::size(600)
                        ->format('png')
                        ->generate($qris['qr_string']);
                    Storage::disk('s3')->put($qrCodeFile, $imageQR);
                    $qrisData=$qris;
                    $qrisData['qr_png']=$qrCodeFile;
                    $data->qr=$qrCodeFile;
                    $data->content_resp=json_encode($qris);
                    $data->save();
                    DB::commit();
                }
                $data->transaction()->create([
                    'code'=>$code,
                    'md_user_id'=>$userId,
                    'md_sp_transaction_type_id'=>TransactionType::SUBSCRIPTION_FEE,
                    'md_sp_transaction_status_id'=>TransactionStatus::PENDING,
                    'amount'=>$amount,
                    'transaction_from'=>'Dari QRIS',
                    'transaction_to'=>'Pembayaran berlangganan dengan QRIS',
                    'external_id'=>$endDate,
                    'admin_fee'=>$adminFee,
                    'discount'=>$discount
                ]);
                DB::commit();
                $output=[
                    'qr_code'=>$data->qr,
                    'admin_fee'=>$adminFee,
                    'subscription_name'=>$subscription->name,
                    'discount'=>$discount,
                    'is_qris'=>1,
                    'is_sennapay'=>0,
                    'is_va'=>0,
                    'is_wallet'=>0,
                    'is_cc'=>0,
                    'amount'=>$amount,
                    'qr_string'=>$qrisData['qr_string'],
                    'code'=>$code,
                    'transaction_status'=>'PENDING',
                    'callback_transaction'=>\route('pay.subscription.callback')
                ];
                return $this->message::getJsonResponse(200,trans('message.201'),$output);
            }elseif($request->md_transaction_type_id==\App\Models\MasterData\TransactionType::VA)
            {
                $price = ceil($data->getSubscription->price - ($data->getSubscription->price * ($data->getSubscription->discount_percentage/100)));
                if($promoPercentage==true)
                {
                    $tmpPrice=ceil($price - ($price*($valuePromo/100)));
                }else{
                    $tmpPrice=ceil($price - $valuePromo);
                }

                $adminFee = 4500;
                $amount = ceil($adminFee + $tmpPrice);
                $discount=$data->getSubscription->price-$price;
                $dt = new \DateTime(date('Y-m-d H:i:s'));
                $dt->modify('+ 6 hour');
                $date=$dt->format('Y-m-d H:i:s');
                $response=$this->virtualAccount->create($userId,strtoupper($request->bank_code),$amount,16);
                if($response==false)
                {
                    return $this->message::getJsonResponse(404,'Pembuatan VA gagal dilakukan,cobalah beberapa saat lagi',[]);
                }
                $vaData=$response;

                $data->transaction()->create([
                    'code'=>$code,
                    'md_user_id'=>$userId,
                    'md_sp_transaction_type_id'=>TransactionType::SUBSCRIPTION_FEE,
                    'md_sp_transaction_status_id'=>TransactionStatus::PENDING,
                    'amount'=>$amount,
                    'transaction_to'=>'Pembayaran berlangganan dengan VA ' . $request->bank_code,
                    'transaction_from'=>$vaData['account_number'] . '_' . $request->bank_code,
                    'external_id'=>$vaData['external_id'],
                    'expiration_date'=>$date,
                    'admin_fee'=>$adminFee,
                    'discount'=>$discount
                ]);
                DB::commit();
                $output=[
                    'account_number'=>$vaData['account_number'],
                    'expiration_date'=>$date,
                    'admin_fee'=>$adminFee,
                    'subscription_name'=>$subscription->name,
                    'discount'=>$discount,
                    'is_qris'=>0,
                    'is_sennapay'=>0,
                    'is_va'=>1,
                    'is_wallet'=>0,
                    'is_cc'=>0,
                    'amount'=>$amount,
                    'bank_code'=>$request->bank_code,
                    'transaction_status'=>'PENDING',
                    'code'=>$code,
                    'callback_transaction'=>\route('pay.subscription.callback')
                ];

                return $this->message::getJsonResponse(200,trans('message.201'),$output);

            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function callbackSubscriptionWeb(Request  $request)
    {
        try{
            $json=$request->all();
            DB::table('xendit_responses')
                ->insert([
                    'response'=>json_encode($request->all()),
                    'external_code'=>$json['external_id']
                ]);
            $transaction=Transaction::where([
                'code'=>$json['external_id'],
                'md_sp_transaction_status_id'=>TransactionStatus::PENDING
            ])->first();

            if($transaction->md_sp_transaction_type_id==TransactionType::SUBSCRIPTION_FEE)
            {
                return $this->_paySubscription($transaction,$json);
            }else{

                return  $this->_payPlugin($transaction,$json);
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'code'=>500,
                'message'=>'Terjadi Kesalahan Saat Pembayaran Berlangganan',
                'data'=>[]
            ],500);
        }
    }

    private function _paySubscription($transaction,$json)
    {
        if(!is_null($transaction) && $json['status']=='EXPIRED')
        {
            DB::beginTransaction();
            $transaction->transactionable->is_expired=1;
            $transaction->transactionable->save();
            $transaction->md_sp_transaction_status_id=TransactionStatus::FAILED;
            $transaction->save();

            $data=PaymentSubscription::where('id',$transaction->transactionable_id)->first();
            DB::commit();
            parent::firebase()::send(
                'Pembayaran Berlangganan Gagal',
                'Mohon maaf pembayaran gagal dilakukan karena invoice sudah kadaluarsa',
                '',
                $transaction->getUser,
                self::class,Route::currentRouteAction(),
                $data,
                1);
            return response()->json([
                'code'=>200,
                'message'=>'Pembayaran Gagal  Dilakukan',
                'data'=>[]
            ],200);
        }

        if(!is_null($transaction) && $json['status']=='PAID')
        {
            DB::beginTransaction();
            $endDate=SubscriptionUtil::calculateSubsVAOrQRIS($transaction->getUser,$transaction->transactionable->getSubscription);
            if($endDate==false)
            {
                return response()->json([
                    'code'=>400,
                    'message'=>'Terjadi Kesalahan Saat Melakukan Kalkulasi Berlangganan',
                    'data'=>[]
                ],500);
            }
            $transaction->transactionable->getMerchant->is_active_subscription=1;
            $transaction->transactionable->getMerchant->end_subscription=$endDate;
            $transaction->transactionable->getMerchant->md_subscription_id=$transaction->transactionable->md_subscription_id;
            $transaction->transactionable->getMerchant->save();
            $transaction->transactionable->md_transaction_type_id=($json['payment_method']=='BANK_TRANSFER')?5:4;
            $transaction->transactionable->save();
            $transaction->md_sp_transaction_status_id=TransactionStatus::SUCCESS;
            $transaction->md_sp_transaction_type_id=16;
            if($json['payment_method']=='BANK_TRANSFER')
            {
                $transaction->transaction_to='Pembayaran berlangganan dengan VA.'.$json['payment_channel'];
                $transaction->transaction_from=$json['payment_method'].' '.$json['payment_channel'];
            }else{
                $transaction->transaction_to='Pembayaran berlangganan dengan QRIS';
                $transaction->transaction_from='Dari QRIS';
            }
            $transaction->save();
            DB::commit();
            $data=PaymentSubscription::where('id',$transaction->transactionable_id)->first();
            parent::firebase()::send(
                'Pembayaran Berlangganan Berhasil',
                trans('custom.pay_subscription_fee',[
                    'end_date'=>$endDate
                ]),
                '',
                $transaction->getUser,
                self::class,Route::currentRouteAction(),
                $data,
                1);

            $forgot=new EmailForSuccessfulSubscription($transaction->getUser->email,$data);
            Mail::to($transaction->getUser->email)->send($forgot);

            return response()->json([
                'code'=>200,
                'message'=>'Pembayaran Berhasil Dilakukan',
                'data'=>[]
            ],200);
        }else{
            return response()->json([
                'code'=>404,
                'message'=>'Kamu Telah Melakukan Pembayaran Berlangganan',
                'data'=>[]
            ],404);
        }
    }

    private function _payPlugin($transaction,$json)
    {
        if(!is_null($transaction) && $json['status']=='EXPIRED')
        {
            DB::beginTransaction();
            $transaction->transactionable->is_expired=1;
            $transaction->transactionable->save();
            $transaction->md_sp_transaction_status_id=TransactionStatus::FAILED;
            $transaction->save();

            $data=PaymentPlugin::where('id',$transaction->transactionable_id)->first();
            DB::commit();
            parent::firebase()::send(
                'Pembayaran Plugin Gagal',
                'Mohon maaf pembayaran gagal dilakukan karena invoice sudah kadaluarsa',
                '',
                $transaction->getUser,
                self::class,Route::currentRouteAction(),
                $data,
                1);
            return response()->json([
                'code'=>200,
                'message'=>'Pembayaran Gagal  Dilakukan',
                'data'=>[]
            ],200);
        }

        if(!is_null($transaction) && $json['status']=='PAID')
        {
            DB::beginTransaction();

            $merchantPlugin = EnableMerchantPlugin::where('md_merchant_id', $transaction->transactionable->getMerchant->id)
                ->where('md_plugin_id', $transaction->transactionable->md_plugin_id)
                ->first();

            if(strtolower($transaction->transactionable->getPlugin->payment_type) != 'quota'){
                $endDate = PluginUtil::calculatePluginEndDate($merchantPlugin, $transaction->transactionable->getPlugin);
            } else {
                $endDate = "batas ".$transaction->transactionable->getPlugin->quota_range_max." kuota";
            }

            if($endDate==false)
            {
                return response()->json([
                    'code'=>400,
                    'message'=>'Terjadi Kesalahan Saat Melakukan Kalkulasi Plugin',
                    'data'=>[]
                ],500);
            }


            $emp = is_null($merchantPlugin) ? new EnableMerchantPlugin() : EnableMerchantPlugin::where('md_merchant_id', $transaction->transactionable->getMerchant->id)
                ->where('md_plugin_id', $transaction->transactionable->md_plugin_id)
                ->first();

            $emp->md_merchant_id = $transaction->transactionable->getMerchant->id;
            $emp->md_plugin_id = $transaction->transactionable->md_plugin_id;

            if(strtolower($transaction->transactionable->getPlugin->payment_type) != 'quota'){
                $emp->start_period = date('Y-m-d H:i:s');
                $emp->end_period = $endDate;
            } else {
                $emp->quota_available = $emp->quota_available + $transaction->transactionable->getPlugin->quota_range_max;
            }

            $emp->status = 1;
            $emp->save();

            $transaction->transactionable->md_transaction_type_id=($json['payment_method']=='BANK_TRANSFER')?5:4;
            $transaction->transactionable->save();
            $transaction->md_sp_transaction_status_id=TransactionStatus::SUCCESS;
            $transaction->md_sp_transaction_type_id=17;
            if($json['payment_method']=='BANK_TRANSFER')
            {
                $transaction->transaction_to='Pembayaran plugin dengan VA.'.$json['payment_channel'];
                $transaction->transaction_from=$json['payment_method'].' '.$json['payment_channel'];
            }else{
                $transaction->transaction_to='Pembayaran plugin dengan QRIS';
                $transaction->transaction_from='Dari QRIS';
            }
            $transaction->save();
            DB::commit();

            $data = PaymentPlugin::where('id',$transaction->transactionable_id)->first();

            parent::firebase()::send(
                'Pembayaran Plugin Berhasil',
                'Selamat,pembayaran plugin sudah berhasil,kamu berlangganan sampai dengan '.$endDate.' ,nikmati fitur yang telah kami berikan',
                '',
                $transaction->getUser,
                self::class,Route::currentRouteAction(),
                $data,
                1);


            $forgot=new EmailForSuccessfulPlugin($transaction->getUser->email,$data);
            Mail::to($transaction->getUser->email)->send($forgot);

            return response()->json([
                'code'=>200,
                'message'=>'Pembayaran Berhasil Dilakukan',
                'data'=>[]
            ],200);
        }else{
            return response()->json([
                'code'=>404,
                'message'=>'Kamu Telah Melakukan Pembayaran Berlangganan',
                'data'=>[]
            ],404);
        }
    }


    public function callbackSubscription(Request $request)
    {
        try{
            $json=$request->all();
            DB::table('xendit_responses')
                ->insert([
                    'response'=>json_encode($request->all()),
                    'external_code'=>$json['qr_code']['external_id']
                ]);
            $transaction=Transaction::where([
                'code'=>$json['qr_code']['external_id'],
                'md_sp_transaction_status_id'=>TransactionStatus::PENDING
            ])->first();
            if(!is_null($transaction) && $json['status']=='COMPLETED')
            {
                DB::beginTransaction();
                $transaction->transactionable->getMerchant->is_active_subscription=1;
                $transaction->transactionable->getMerchant->end_subscription=$transaction->external_id;
                $transaction->transactionable->getMerchant->md_subscription_id=$transaction->transactionable->md_subscription_id;
                $transaction->transactionable->getMerchant->save();
                $transaction->md_sp_transaction_status_id=TransactionStatus::SUCCESS;
                $transaction->save();
                DB::commit();
                if($transaction->transactionable->getMerchant->is_man!=1){
                    $data=PaymentSubscription::where('id',$transaction->transactionable_id)->first();
                    parent::firebase()::send(
                        'Pembayaran Berlangganan Berhasil',
                        trans('custom.pay_subscription_fee',[
                            'end_date'=>$transaction->external_id
                        ]),
                        '',
                        $transaction->transactionable->getMerchant->getUser,
                        self::class,Route::currentRouteAction(),
                        $data,
                        1);

                    $forgot=new EmailForSuccessfulSubscription($transaction->getUser->email,$data);
                    Mail::to($transaction->getUser->email)->send($forgot);
                }
                return response()->json([
                    'code'=>200,
                    'message'=>'Pembayaran Berhasil Dilakukan',
                    'data'=>[]
                ],200);
            }else{
                return response()->json([
                    'code'=>404,
                    'message'=>'Kamu Telah Melakukan Pembayaran Berlangganan',
                    'data'=>[]
                ],404);
            }

        }catch (\Exception $e)
        {
            DB::rollBack();
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return response()->json([
                'code'=>500,
                'message'=>'Terjadi Kesalahan Saat Pembayaran QRIS',
                'data'=>[]
            ],500);
        }
    }

    public function callbackPaySubs(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'code'=>'required',
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404, $error, []);
            }
            $data=Transaction::where('code',$request->code)
                ->with(['getTransactionType','getTransactionStatus'])
                ->with('transactionable')
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);

            }else{

                return $this->message::getJsonResponse(200,trans('message.201'),$data);

            }

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }

    public function list($userId,Request $request)
    {
        try{
            $offset=is_null($request->offset)?0:$request->offset;
            $limit=is_null($request->limit)?10:$request->limit;

            $data=$this->payment::
            select([
                'sp_payment_subscriptions.*',
                's.code',
                'ts.name as transaction_status',
                'tt.name as transaction_type',
                's.amount',
                's.transaction_to',
                's.transaction_from',
                'sub.name as subscription_name',
                's.admin_fee',
                's.discount'

            ])
                ->join('sp_transactions as s','s.transactionable_id','sp_payment_subscriptions.id')
                ->join('md_sp_transaction_status as ts','ts.id','s.md_sp_transaction_status_id')
                ->join('md_sp_transaction_types as tt','tt.id','s.md_sp_transaction_type_id')
                ->join('md_subscriptions as sub','sub.id','sp_payment_subscriptions.md_subscription_id')
                ->where('s.transactionable_type','App\Models\SennaPayment\PaymentSubscription')
                ->where('s.md_user_id',$userId)
                ->skip($offset)
                ->take($limit)
                ->orderBy('sp_payment_subscriptions.created_at','desc')
                ->get();
            if($data->count()<1)
            {
                return $this->message::getJsonResponse(404,trans('message.404'),[]);
            }
            return $this->message::getJsonResponse(200,trans('message.201'),$data);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);
        }
    }





}

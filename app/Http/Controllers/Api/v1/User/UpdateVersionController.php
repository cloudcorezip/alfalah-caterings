<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\User;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class UpdateVersionController extends Controller
{
    protected $user;
    protected $message;

    public function __construct()
    {
        $this->user=User::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::post('v1/user/update-version/{userId}', 'Api\v1\User\UpdateVersionController@update') ->middleware('api-verification');
    }


    public function update(Request $request,$userId)
    {
        try{
            $data=$this->user::find($userId);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('custom.email_not_found'),[]);
            }

            $validator = Validator::make($request->all(),[
                'app_mobile_version'=>'required'
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404,$error,[]);
            }
            if($data->app_mobile_version!=$request->app_mobile_version)
            {
                $data->app_mobile_version=$request->app_mobile_version;
                $data->save();

            }
            return $this->message::getJsonResponse(200,trans('message.200'),[]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\User;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Mails\Account\EmailActivation;
use App\Models\MasterData\User;
use App\Utils\Account\AccountUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;


class LoginController extends Controller
{
    protected $user;
    protected $message;

    public function __construct()
    {
        $this->user=User::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::post('v1/user/update-fcm-key/{id}', 'Api\v1\User\LoginController@updateFcmKey')
        ->middleware('api-verification')
        ;
    }
    public function updateFcmKey($id,Request $request)
    {
        try{
            $data=$this->user::find($id);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('custom.account_not_found'),[]);
            }
            $data->fcm_key=$request->fcm_key;
            $data->save();
            return $this->message::getJsonResponse(200,trans('custom.add_success'),[]);
        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }
}

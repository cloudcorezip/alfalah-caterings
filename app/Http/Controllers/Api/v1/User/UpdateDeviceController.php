<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\User;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class UpdateDeviceController extends Controller
{
    protected $user;
    protected $message;

    public function __construct()
    {
        $this->user=User::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::post('v1/user/update-device/{userId}', 'Api\v1\User\UpdateDeviceController@update') ->middleware('api-verification');
    }


    public function update(Request $request,$userId)
    {
        try{
            $data=$this->user::find($userId);
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('custom.email_not_found'),[]);
            }

            $validator = Validator::make($request->all(),[
                'device_id'=>'required'
            ]);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                return $this->message::getJsonResponse(404,$error,[]);
            }
            $data->device_id=$request->device_id;
            $data->save();

            return $this->message::getJsonResponse(200,trans('message.200'),[]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }
}

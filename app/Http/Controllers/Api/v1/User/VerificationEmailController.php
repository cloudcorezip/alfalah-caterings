<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\User;


use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Models\MasterData\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mails\Account\EmailActivation;

class VerificationEmailController extends Controller
{
    protected $user;
    protected $message;

    public function __construct()
    {
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::post('v1/user/send-verification/{email}', 'Api\v1\User\VerificationEmailController@send');
    }


    public function send(Request $request,$email)
    {
        try{

            $token=Hash::make($email.date('YmdHis').Uuid::uuid4()->toString());

            $user=User::where('email',$email)->first();
            if(is_null($user))
            {
                return $this->message::getJsonResponse(404,'Akun tidak ditemukan',[]);

            }
            $user->token=$token;
            $user->save();
            
            $link=url('/account/email-verification').'?key='.$token;
            
            $content=new EmailActivation($request->fullname,$link);
                    
            Mail::to($email)->send($content);

            return $this->message::getJsonResponse(200,trans('message.200'),[]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }
}

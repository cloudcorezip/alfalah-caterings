<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers\Api\v1\User;


use App\Classes\Singleton\CodeGenerator;
use App\Classes\Singleton\Message;
use App\Http\Controllers\Controller;
use App\Jobs\JobForgetEmail;
use App\Jobs\MailActivation;
use App\Mails\Account\EmailActivation;
use App\Mails\Account\ForgetEmail;
use App\Mails\Account\ForgetPin;
use App\Models\MasterData\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class ForgotPasswordController extends Controller
{

    protected $user;
    protected $message;

    public function __construct()
    {
        $this->user=User::class;
        $this->message=Message::getInstance();
    }

    public function routeApi()
    {
        Route::post('v1/user/forgot-password', 'Api\v1\User\ForgotPasswordController@forgotPassword');
    }


    public function forgotPassword(Request $request)
    {
        try{
            $data=$this->user::whereRaw("lower(email)='".strtolower($request->email)."'")
                ->whereIn('md_role_id','>=',3)
                ->first();
            if(is_null($data))
            {
                return $this->message::getJsonResponse(404,trans('custom.email_not_found'),[]);
            }
            $password=(CodeGenerator::getInstance())::codeGenerator(8);
            $data->password=Hash::make($password);
            $data->save();
            $forgot=new ForgetEmail($data->fullname,$password);
            Mail::to($data->email)->send($forgot);
            return $this->message::getJsonResponse(200,trans('custom.forgot_password_success'),[]);

        }catch (\Exception $e)
        {
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return $this->message::getJsonResponse(500,trans('custom.errors'),[]);

        }
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Route;

class PageController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('pages')->group(function (){
            Route::get('/not-allowed','PageController@notAllowed')->name('pages.not-allowed');
            Route::get('/for-free','PageController@forFree')->name('pages.for-free');
            Route::get('/for-expired','PageController@forExpired')->name('pages.for-expired');



        });

    }

    public function notAllowed()
    {
        return view('pages.not_allowed');
    }
    public function forFree()
    {
        return view('pages.pages-for-free');
    }

    public function forExpired()
    {
        return view('pages.pages-for-expired');
    }
}

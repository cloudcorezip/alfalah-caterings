<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers;


use App\Models\MasterData\MerchantMenu;
use App\Models\MasterData\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Ramsey\Uuid\Uuid;

class LoginController extends Controller
{
    protected $user;
    public function __construct()
    {
        $this->user=User::class;
    }

    public function routeWeb()
    {
        Route::prefix('login')->group(function (){
            Route::get('/','LoginController@index')->name('login');
            Route::post('/validate-login','LoginController@validateLogin')->name('login.validate');
            Route::get('/logout','LoginController@logout')->name('logout');
            Route::get('/redirect-login','LoginController@redirectMerchantLogin')->name('redirect-login');
        });

    }

    public function index(Request $request)
    {
        try{
            $cookieRole=decrypt(Session::get(config('user.role')));
            if(!is_null($cookieRole))
            {
                return redirect()->route('merchant.dashboard');

            }else{
                $params=[
                    'title'=>'Login',
                    'data'=>[],
                    'key'=>config('merchant.toko')
                ];
                return view('login.index',$params);
            }
        }catch (\Exception $e)
        {
            $params=[
                'title'=>'Login',
                'data'=>[],
                'key'=>config('merchant.toko')
            ];
            return view('login.index',$params);
        }
    }

    public function validateLogin(Request $request)
    {
        try{
            $m=$request->key;
            if(is_null($m))
            {
                $activeUser=$this->user::whereRaw("lower(email)='".strtolower($request->email)."'")
                    ->where('is_active',1)
                    ->first();
            }else{
                if($m==config('merchant.toko'))
                {
                    $where=[
                        'md_role_id'=>3
                    ];
                }elseif ($m==config('merchant.jasa'))
                {
                    $where=[
                        'md_role_id'=>2
                    ];
                }
                $activeUser=$this->user::
                whereRaw("lower(email)='".strtolower($request->email)."'")
                ->where($where)->first();
                if(is_null($activeUser)){
                    $activeUser=$this->user::
                    whereRaw("lower(email)='".strtolower($request->email)."'")->where('is_active',1)
                        ->where('is_staff',1)
                        ->first();
                }
            }
            if($request->is_gmail==0)
            {
                if(is_null($activeUser))
                {
                    return "<div class='alert alert-warning' style='text-align: center'>".trans('custom.account_not_found')."</div>";
                }else{
                    if(!Hash::check($request->password,$activeUser->password))
                    {
                        return "<div class='alert alert-warning' style='text-align: center'>".trans('passwords.password_not_match')."</div>";

                    } elseif ($activeUser->md_role_id==1) {
                        return "<div class='alert alert-warning' style='text-align: center'>" . trans('custom.not_allowed') . "</div>";
                    }else{
                        $userEncrypt=encrypt($activeUser->id);
                        $roleEncrypt=encrypt($activeUser->md_role_id);

                        Session::put(config('user.user'),$userEncrypt);
                        Session::put(config('user.role'),$roleEncrypt);

                        if(!is_null($activeUser->getMerchant)){
                            Artisan::call('acc:build', ['--merchant' =>$activeUser->getMerchant->id]);

                            $merchantEncrypt=encrypt($activeUser->getMerchant->id);
                            Session::put(config('user.merchant'),$merchantEncrypt);

                        }
                        if($activeUser->is_staff==1)
                        {
                            $merchantEncrypt=encrypt($activeUser->getStaffMerchant->md_merchant_id);
                            Session::put(config('user.merchant'),$merchantEncrypt);
                        }

                        return "<div class='alert alert-success' style='text-align: center'>".trans('custom.login_success')."</div><script>reload(1000);</script>";

                    }
                }

            }else{
                if(is_null($activeUser))
                {
                    return response()->json([
                        'status'=>false,
                        "dashboard"=>\route('login')."?key=".$request->key.""
                    ]);
                }else{
                    $userEncrypt=encrypt($activeUser->id);
                    $roleEncrypt=encrypt($activeUser->md_role_id);

                    Session::put(config('user.user'),$userEncrypt);
                    Session::put(config('user.role'),$roleEncrypt);

                    if(!is_null($activeUser->getMerchant)){
                        Artisan::call('acc:build', ['--merchant' =>$activeUser->getMerchant->id]);
                        $merchantEncrypt=encrypt($activeUser->getMerchant->id);

                        Session::put(config('user.merchant'),$merchantEncrypt);


                    }
                    if($activeUser->is_staff==1)
                    {
                        $merchantEncrypt=encrypt($activeUser->getStaffMerchant->md_merchant_id);
                        Session::put(config('user.merchant'),$merchantEncrypt);

                    }
                    return response()->json([
                        'status'=>true,
                        'dashboard'=>\route('merchant.dashboard'),

                    ]);
                }
            }

        }catch (\Exception $e)
        {
            return "<div class='alert alert-warning' style='text-align: center'>".trans('custom.not_allowed')."</div>";

        }


    }


    public function redirectMerchantLogin(Request $request)
    {
        try{
            $activeUser=$this->user::find($request->key);
            $userEncrypt=encrypt($activeUser->id);
            $roleEncrypt=encrypt($activeUser->md_role_id);

            Session::put(config('user.user'),$userEncrypt);
            Session::put(config('user.role'),$roleEncrypt);

            if(!is_null($activeUser->getMerchant)){
                $merchantEncrypt=encrypt($activeUser->getMerchant->id);
                Session::put(config('user.merchant'),$merchantEncrypt);

            }
            return redirect()->route('login');

        }catch (\Exception $e)
        {
            return redirect()->route('home');

        }


    }

    public function logout(Request $request)
    {
        $menu=env('APP_MENU_VERSION').'_'.merchant_id();
        Session::forget(config('user.user'));
        Session::forget(config('user.role'));
        Session::forget(config('user.merchant'));
        Session::forget(config('user.switch_branch'));
        Session::forget(config('user.switch_branch_id'));
        Session::forget(config('user.switch_branch_user_id'));
        Session::forget(config('user.menu_merchant'));
        Session::forget(env('APP_MENU_VERSION').'_'.merchant_id());
        Session::flush();
        $request->session()->flush();
        return redirect()->route('login',['is_destroy'=>Uuid::uuid4()->toString()]);

    }




}

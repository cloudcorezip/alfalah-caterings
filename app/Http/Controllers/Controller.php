<?php

namespace App\Http\Controllers;

use App\Classes\Singleton\Firebase;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\MasterData\User;
use App\Models\SennaToko\PurchaseOrder;
use App\Models\SennaToko\StockInventory;
use App\Utils\MenuUtils;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected $menuKey;
    protected $permission;


    /**
     * @return mixed
     */
    public function getPermission($permission)
    {
        try{
            if(count($this->getMenuKey())>0)
            {
                foreach ($this->getMenuKey() as $item)
                {
                    if($item['action']==$permission)
                    {
                        return $item['is_show'];
                        break;
                    }
                }
            }
        }catch (\Exception $e)
        {
           return true;
        }

    }

    /**
     * @return mixed
     */
    public function getMenuKey()
    {
        return  (MenuUtils::getInstance())->menu(user()->getStaffMerchant->id,$this->menuKey);
    }

    /**
     * @param mixed $menuKey
     */
    public function setMenuKey($menuKey): void
    {
        $this->menuKey = $menuKey;
    }



    public  static function firebase()
    {
        return Firebase::getInstance();
    }
    public static function getPrefixRedis()
    {
        return (env('APP_ENV')!='production')?'dev':'prod';
    }

    public static function generateWarehouse($merchantId)
    {
        try{
            $check=MerchantWarehouse::where('md_merchant_id',$merchantId)
                ->where('is_default',1)
                ->first();
            if(is_null($check))
            {
                MerchantWarehouse::insert([
                    'name'=>'Gudang Utama',
                    'md_merchant_id'=>$merchantId,
                    'desc'=>'Gudang utama tidak bisa dihapus',
                    'is_default'=>1,
                    'is_deleted'=>0
                ]);
            }
            return true;
        }catch (\Exception $e)
        {
            return false;

        }
    }

    public static function generateWarehouseById($userId)
    {
        try{
            DB::beginTransaction();
            $user=User::find($userId);
            if(!is_null($user->getMerchant))
            {
                $check=MerchantWarehouse::where('md_merchant_id',$user->getMerchant->id)
                    ->where('is_default',1)
                    ->first();
                $merchantId=$user->getMerchant->id;
                if(is_null($check))
                {
                    $id=MerchantWarehouse::insertGetId([
                        'name'=>'Gudang Utama',
                        'md_merchant_id'=>$user->getMerchant->id,
                        'desc'=>'Gudang utama tidak bisa dihapus',
                        'is_default'=>1,
                        'is_deleted'=>0
                    ]);
                    DB::statement("UPDATE sc_purchase_orders set inv_warehouse_id=$id where md_merchant_id=$merchantId");
                    DB::statement("UPDATE sc_stock_opnames set inv_warehouse_id=$id where md_merchant_id=$merchantId");
                    DB::statement("UPDATE sc_transfer_stocks set inv_warehouse_id=$id where from_merchant_id=$merchantId");
                    DB::statement("update sc_stock_inventories ssi
                        set inv_warehouse_id=$id
                        from sc_products sp
                        join md_users u
                        on sp.md_user_id=u.id
                        join md_merchants m
                        on m.md_user_id=u.id
                        where ssi.sc_product_id=sp.id
                        and m.id=$merchantId");
                }else{
                    $idCek=$check->id;
                    $check2=PurchaseOrder::where('md_merchant_id',$merchantId)
                    ->whereNull('inv_warehouse_id')->count();
                    if($check2>0){
                        DB::statement("UPDATE sc_purchase_orders set inv_warehouse_id=$idCek where md_merchant_id=$merchantId");
                        DB::statement("UPDATE sc_stock_opnames set inv_warehouse_id=$idCek where md_merchant_id=$merchantId");
                        DB::statement("UPDATE sc_transfer_stocks set inv_warehouse_id=$idCek where from_merchant_id=$merchantId");
                        DB::statement("update sc_stock_inventories ssi
                        set inv_warehouse_id=$idCek
                        from sc_products sp
                        join md_users u
                        on sp.md_user_id=u.id
                        join md_merchants m
                        on m.md_user_id=u.id
                        where ssi.sc_product_id=sp.id
                        and m.id=$merchantId");
                    }
                }

            }
            DB::commit();
            return true;
        }catch (\Exception $e)
        {
            DB::rollBack();
            return false;

        }
    }

    public static function getGlobalWarehouse($merchantId)
    {
        try{
            $check=MerchantWarehouse::where('md_merchant_id',$merchantId)
                ->where('is_default',1)
                ->first();
            if(is_null($check))
            {
                self::generateWarehouse($merchantId);
                return MerchantWarehouse::where('md_merchant_id',$merchantId)
                    ->where('is_default',1)
                    ->first();
            }else{
                return $check;
            }
        }catch (\Exception $e)
        {
            return false;

        }
    }

}

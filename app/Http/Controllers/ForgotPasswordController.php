<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Http\Controllers;

use App\Models\MasterData\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use App\Mails\Account\ForgetPasswordWeb;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;

class ForgotPasswordController extends Controller
{
    public function routeWeb()
    {
        Route::prefix('forgot-password')->group(function (){
            Route::get('/','ForgotPasswordController@index')
                ->name('forgot.password');
            Route::post('/send-link', 'ForgotPasswordController@sendLink')
                ->name('forgot.password.send');
            Route::get('/reset', 'ForgotPasswordController@reset')
                ->name('forgot.password.reset');
            Route::post('/save', 'ForgotPasswordController@save')
                ->name('forgot.password.save');
        });

    }

    public function index(){
        $params = [
            "title" => "Lupa Password",
            'data'=>[],
            'key'=>config('merchant.toko')
        ];
        return view('forgot-password.index', $params);
    }

    public function sendLink(Request $request){
        try {
            $data = User::where('email', $request->email)
                    ->where('md_role_id', 3)
                    ->where('is_active', 1)
                    ->first();
            if(is_null($data)){
                return "<div class='alert alert-warning' style='text-align: center'>Email tidak ditemukan !</div>
                        <script>reload(1500)</script>
                ";
            }

            $link = url('/forgot-password/reset')."?key=".$data->token."&p=".encrypt($data->email);

            $email = new ForgetPasswordWeb($data->fullname, $link);
            Mail::to($request->email)->send($email);

            return "<div class='alert alert-success' style='text-align: center'>Kami sudah mengirimkan link untuk mereset passwordmu. Silahkan cek emailmu !</div>
                    <script>reload(3000)</script>";
        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server, cobalah beberapa saat lagi!</div>";
        }
    }

    public function reset(Request $request){
        try {
            // $request->key = $user->token
            // $request->p = decrypt($user->email);
            if(is_null($request->key) || is_null($request->p)){
                return redirect('page-not-found');
            }

            $token = $request->key;
            $email = decrypt($request->p);

            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                return redirect('page-not-found');
            }

            $data = User::where('token', $token)
                    ->where('email', $email)
                    ->where('md_role_id', 3)
                    ->where('is_active', 1)
                    ->first();

            if(is_null($data)){
                return redirect('/login');
            }

            $params = [
                "title" => "Lupa Password",
                "data" => $data,
                "key"=>config('merchant.toko')
            ];

            return view('forgot-password.form', $params);

        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return abort(404);
        }
    }

    public function save(Request $request){
        try {
            if(strlen($request->password) < 6){
                return "<div class='alert alert-warning' style='text-align: center'>Panjang password minimal 6 karakter</div><script>reload(3000);</script>";
            }

            if($request->password !== $request->confirm_password){
                return "<div class='alert alert-warning' style='text-align: center'>Konfirmasi password tidak sesuai !</div>
                        <script>reload(3000)</script>
                ";
            }
            
            $token = $request->key;
            $email = decrypt($request->p);

            $data = User::where('token', $token)
                    ->where('email', $email)
                    ->where('md_role_id', 3)
                    ->where('is_active', 1)
                    ->first();
            if(is_null($data)){
                return "<div class='alert alert-warning' style='text-align: center'>Akun tidak ditemukan !</div>
                    <script>reload(3000)</script>";
            }

            $data->password = Hash::make($request->password);
            $data->token = Hash::make($email.date('YmdHis').Uuid::uuid4()->toString());
            $data->save();

            return "<div class='alert alert-success' style='text-align: center'>Password berhasil di reset, silahkan login!</div>
                    <script>
                    setTimeout(() => {window.location.href = '".url('/login')."'
                    }, 3000)
                    </script>
            ";


        } catch(\Exception $e){
            log_helper(self::class,Route::currentRouteAction(),$e->getLine(),$e->getFile(),$e->getMessage());
            return "<div class='alert alert-danger' style='text-align: center'>Terjadi kesalahan di server, cobalah beberapa saat lagi!</div>";
        }
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Inventory;


class WarehouseMutation extends BaseInventoryAccModel
{
    protected $table='inv_warehouse_stock_mutations';
    protected $primaryKey='id';

    public function getDetail()
    {
        return $this->hasMany(WarehouseMutation::class,'id','inv_warehouse_stock_mutation_id');
    }
}


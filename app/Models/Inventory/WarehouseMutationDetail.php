<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Inventory;


class WarehouseMutationDetail extends BaseInventoryAccModel
{
    protected $table='inv_warehouse_stock_mutation_details';
    protected $primaryKey='id';

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Plugin;

use App\Models\MasterData\Merchant;
use App\Models\MasterData\Plugin;

class EnableMerchantPlugin extends BasePluginModel
{
    protected $table='enable_merchant_plugins';
    protected $primaryKey='id';

    public $ruleCreate=[
        'md_merchant_id'=>'required',
        'md_plugin_id' => 'required'
    ];


    public function getPlugin()
    {
        return $this->hasOne(Plugin::class, 'id', 'md_plugin_id');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class, 'id', 'md_merchant_id');
    }
    public function getChild()
    {
        return $this->hasMany(EnableMerchantPlugin::class, 'parent_id', 'id');

    }
}

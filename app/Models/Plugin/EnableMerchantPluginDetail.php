<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Plugin;

use App\Models\MasterData\Merchant;
use App\Models\MasterData\Plugin;

class EnableMerchantPluginDetail extends BasePluginModel
{
    protected $table='enable_merchant_plugin_details';
    protected $primaryKey='id';
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Blog;


class CategoryBlog extends BaseBlogModel
{
    protected $table='md_bg_categories';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleCreate=[
        'name'=>'required'
    ];
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Blog;


class AppLogs extends BaseBlogModel
{
    protected $table='md_app_logs';
    protected $primaryKey='id';
    public $timestamps=true;


}

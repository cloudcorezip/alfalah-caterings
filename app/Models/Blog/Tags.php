<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Blog;


class Tags extends BaseBlogModel
{
    protected $table='md_bg_tags';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleCreate=[
        'name'=>'required'
    ];
}

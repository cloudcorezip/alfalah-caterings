<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Blog;


class BlogHardware extends BaseBlogModel
{

    protected $table='blog_hardware';
    protected $primaryKey='id';

    public $timestamps=true;

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Blog;
use App\Models\Blog\CategoryBlog;
use App\Models\MasterData\User;

class Posts extends BaseBlogModel
{
    protected $table='md_bg_posts';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleCreate=[
        'title'=>'required',
        'contents'=>'required',
        'tags'=>'required',
        'category'=>'required',
        'image'=>'file|image|mimes:jpg,jpeg,bmp,png'

    ];

    public function getCategory()
    {
        return $this->hasOne(CategoryBlog::class,'id','md_bg_category_id');
    }
    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }


}

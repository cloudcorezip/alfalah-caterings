<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Blog;
use App\Models\MasterData\Merchant;

class Testimony extends BaseBlogModel
{
    protected $table='md_testimony';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

}

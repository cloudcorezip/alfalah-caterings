<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\CRM;


use App\Models\BaseModel;

class Configuration extends BaseModel
{
    protected $table='crm_message_configurations';
    protected $primaryKey='id';

}

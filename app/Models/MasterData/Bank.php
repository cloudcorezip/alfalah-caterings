<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class Bank extends BaseMasterDataModel
{
    protected $table='md_banks';
    protected $primaryKey='id';
    public $timestamps=true;


    public  $ruleUpdate=[
        'code'=>'required',
        'name'=>'required',
        'is_active'=>'required',
        'icon'=>'required',
    ];


    public  $ruleCreate=[
        'code'=>'required',
        'name'=>'required',
        'is_active'=>'required',
        'icon'=>'file|image|mimes:jpg,jpeg,bmp,png',
    ];

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class FormOrder extends BaseMasterDataModel
{
    protected $table='md_form_orders';
    protected $primaryKey='id';
    public $timestamps=true;

}

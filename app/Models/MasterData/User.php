<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use App\Models\MasterData\SennaDriver\UserDriverDetail;
use Illuminate\Notifications\Notifiable;

class User extends BaseMasterDataModel
{
    use Notifiable;

    protected $table='md_users';
    protected $primaryKey='id';
    public $timestamps=true;
    protected $hidden=['password'];

    public $fillable=[
        'fullname','email','birth_date','password','birth_place','gender','balance'
    ];
    public $registerWithoutGmail=[
        'fullname'=>'required',
        'email'=>"required",
        'password'=>'required'
    ];

    public $registerWithGmail=[
        'fullname'=>'required',
        'email'=>"required",
        'gmail_information'=>'required',
        'is_from_google'=>'required'
    ];

    public $login=[
        'email'=>"required",
        'password'=>'required'
    ];

    public $loginWithGoogle=[
        'email'=>"required",
        'is_from_google'=>'required',
        'gmail_information'=>'required'
    ];

    public $updateProfile=[
        'fullname'=>'required',
        'birth_date'=>'nullable',
        'birth_place'=>'nullable',
        'gender'=>'required'
    ];

    public $editPassword=[
        'old_password'=>'required',
        'new_password'=>'required'
    ];
    public $editPasswordWithoutOld=[
        'new_password'=>'required'
    ];

    public $editPin=[
        'old_pin'=>'required',
        'new_pin'=>'required'
    ];

    public $upgradeSennaPay=[
        'identity_card'=>'required|file|image|mimes:jpg,jpeg,bmp,png',
        'identity_card_with_body'=>'required|file|image|mimes:jpg,jpeg,bmp,png'
    ];

    public $checkPin=[
        'pin'=>'required',
    ];

    public $updateFoto=[
        "foto" => 'required|file|image|mimes:jpg,jpeg,bmp,png',
    ];

    public $rulePin=[
        'pin'=>'required'
    ];
    public $rulePhone=[
        'phone_number'=>'required'
    ];

    public $location=[
        'latitude'=>'required',
        'longitude'=>'required'
    ];

    public $ruleSearch=[
        'code'=>'required',
        'from_user_id'=>'required'
    ];

    protected $casts = [
        'gmail_information' => 'array',
    ];

    public function getGmailInformationAttribute($value) {
        return json_decode($value);
    }
    public function getRole()
    {
        return $this->hasOne(Role::class,'id','md_role_id');
    }

    public function getUserBank()
    {
        return $this->hasOne(UserBank::class,'md_user_id','id');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'md_user_id','id');
    }

    public function getUserDriver()
    {
        return $this->hasOne(UserDriverDetail::class,'md_user_id_driver','id');
    }
    public function getStaffMerchant()
    {
        return $this->hasOne(MerchantStaff::class,'md_user_id','id');
    }



}

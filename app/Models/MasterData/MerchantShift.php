<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantShift extends BaseMasterDataModel
{

    protected $table='md_merchant_shifts';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
      'md_user_id'=>'required|exists:md_users,id',
      'start_shift'=>'required',
      'starting_cash'=>'numeric|required',
      'timezone'=>'required',
      'md_merchant_id'=>'required|exists:md_merchants,id'
    ];

    public $end=[
      'end_shift'=>'required',
      'note'=>'required',
        'end_cash'=>'numeric|required'
    ];

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');

    }
    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');

    }

}

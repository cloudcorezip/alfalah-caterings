<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use App\Models\MasterData\Region\City;
use App\Models\MasterData\Region\Province;

class MerchantRangeOfBusiness extends BaseMasterDataModel
{
    protected $table='md_merchant_range_business';
    protected $primaryKey='id';
    public $timestamps=true;


    public function getProvince()
    {
        return $this->hasOne(Province::class,'id','md_province_id');

    }

    public function getCity()
    {
        return $this->hasOne(City::class,'id','md_city_id');
    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantFormOrder extends BaseMasterDataModel
{
    protected $table='md_merchant_form_order_template';
    protected $primaryKey='id';
    public $timestamps=true;

}

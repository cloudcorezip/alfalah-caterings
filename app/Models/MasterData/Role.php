<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class Role extends BaseMasterDataModel
{
    protected $table='md_roles';
    protected $primaryKey='id';
    public $timestamps=true;

}

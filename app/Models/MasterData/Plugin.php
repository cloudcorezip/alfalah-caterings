<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\MasterData;
use App\Models\Plugin\EnableMerchantPlugin;

class Plugin extends BaseMasterDataModel
{
    protected $table='md_plugins';
    protected $primaryKey='id';
    public $timestamps=true;

    // plugin type
    const GOOGLE_CONTACT = 'google_contact';
    const SFA = 'sfa';
    const E_COMMERCE = 'e_commerce';

    // parent id type
    const PARENT_ID_GCONTACT = 1;

    public function getParent()
    {
        return $this->hasOne(Plugin::class,'id','parent_id');
    }

    public function getEnableMerchantPlugin($merchantId,$pluginId)
    {
        return EnableMerchantPlugin::where('md_merchant_id',$merchantId)->where('md_plugin_id',$pluginId)->first();
    }

    public function getChild()
    {
        return $this->hasMany(Plugin::class,'parent_id','id');
    }
}
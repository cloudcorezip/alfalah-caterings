<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantStaff extends BaseMasterDataModel
{
    protected $table='md_merchant_staff';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'email'=>'required',
        'password'=>'required',
        'name'=>'required',
        'md_role_id'=>'required|exists:md_roles,id'
    ];

    public $ruleUpdate=[
        'email'=>'required',
        'password'=>'nullable',
        'name'=>'required',
        'md_role_id'=>'required|exists:md_roles,id'
    ];
    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }

}

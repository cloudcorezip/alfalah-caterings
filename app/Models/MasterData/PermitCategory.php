<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\MasterData;


class PermitCategory extends BaseMasterDataModel
{
    protected $table='md_permit_categories';
    protected $primaryKey='id';
    public $timestamps=true;


}

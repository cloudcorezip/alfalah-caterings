<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaCashier;


use App\Models\MasterData\BaseMasterDataModel;

class ShippingCategory extends BaseMasterDataModel
{
    protected $table='md_sc_shipping_categories';
    protected $primaryKey='id';
    public $timestamps=true;


    const COURIER=1;
    const SENNA_DRIVER=2;
    const PICK_IN_STORE=3;
    const FREE_DELIVERY=27;

    public $ruleCreate=[
        'name'=>'required',
        'icons'=>'required|image|mimes:jpg,jpeg,bmp,png',
        'alias'=>'nullable',
        'is_active'=>'required',
        'is_selected'=>'required',
        'parent_id'=>'nullable',
        'order_number'=>'required|numeric',
    ];

    public $ruleUpdate=[
        'name'=>'required',
        'alias'=>'nullable',
        'is_active'=>'required',
        'is_selected'=>'required',
        'parent_id'=>'nullable',
        'order_number'=>'required|numeric',
    ];


    public function scopeIsSelected($query,$status)
    {
        return $query->where('is_selected',$status);
    }

    public function scopeIsActive($query,$status)
    {
        return $query->where('is_active',$status);
    }

    public function getChild()
    {
        return $this->hasMany(ShippingCategory::class,'parent_id','id')->with('getChild');
    }

    public function getParent()
    {
        return $this->hasOne(ShippingCategory::class,'id','parent_id');
    }
}

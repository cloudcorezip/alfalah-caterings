<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaCashier;


use App\Models\MasterData\BaseMasterDataModel;
use App\Models\MasterData\SennaJasa\CategoryService;

class Category extends BaseMasterDataModel
{
    protected $table='md_sc_categories';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleCreate=[
        'name'=>'required',
        'image'=>'required|image|mimes:jpg,jpeg,bmp,png',
        'is_active'=>'required',
        'parent_id'=>'nullable',
        'order_number'=>'required|numeric',
    ];


    public $ruleUpdate=[
        'name'=>'required',
        'image'=>'required',
        'is_active'=>'required',
        'parent_id'=>'nullable',
        'order_number'=>'required|numeric',
    ];

    public function getChild()
    {
        return $this->hasMany(Category::class,'parent_id','id')->with('getChild');
    }

}

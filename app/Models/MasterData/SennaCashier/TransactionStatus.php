<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaCashier;


use App\Models\MasterData\BaseMasterDataModel;

class TransactionStatus extends BaseMasterDataModel
{
    protected $table='md_sc_transaction_status';
    protected $primaryKey='id';
    public $timestamps=true;

    const UNPAID=1;
    const FAILED=4;
    const PAID=2;
    const PENDING=1;
    const CANCELED=3;
    const REFUND=5;


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaCashier;


use App\Models\MasterData\BaseMasterDataModel;

class Currency extends BaseMasterDataModel
{
    protected $table='md_sc_currencies';
    protected $primaryKey='id';
    public $timestamps=true;


}

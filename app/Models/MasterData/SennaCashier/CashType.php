<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaCashier;


use App\Models\MasterData\BaseMasterDataModel;

class CashType extends BaseMasterDataModel
{

    protected $table='md_sc_cash_types';
    protected $primaryKey='id';
    public $timestamps=true;

}

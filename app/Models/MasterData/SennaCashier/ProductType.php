<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaCashier;


use App\Models\MasterData\BaseMasterDataModel;

class ProductType extends BaseMasterDataModel
{

    protected $table='md_sc_product_types';
    protected $primaryKey='id';
    public $timestamps=true;

    const RAW_MATERIAL=2;


    public $rule=[
        'name'=>'required'
    ];


}

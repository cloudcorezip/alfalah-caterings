<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaCashier;


use App\Models\MasterData\BaseMasterDataModel;

class OrderStatus extends BaseMasterDataModel
{
    protected $table='md_sc_order_status';
    protected $primaryKey='id';
    public $timestamps=true;


    const SHOP_CONFIRMATION=1;
    const  ORDERING_IS_PACKING=2;
    const  ORDER_SENT=3;
    const ORDER_ARRIVED=4;
    const ORDER_PACKING=5;
    const GET_THE_DRIVER=6;
    const DRIVER_TO_SHOP=7;
    const DRIVER_HAS_BEEN=8;
    const ORDER_IS_PACKING_SELLER=9;
    const ORDER_WAITING_FOR_TAKE=10;
    const ORDER_HAS_BEEN_TAKEN=11;
    const ORDER_DONE=12;
    const ORDER_CANCELED_BY_SHOP=13;
    const ORDER_CANCELED_BY_USER=14;




}

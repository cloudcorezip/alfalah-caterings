<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use App\Models\MasterData\SennaCashier\Currency;

class MerchantCurrency extends BaseMasterDataModel
{

    protected $table='md_merchant_currencies';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getCurrency()
    {
        return $this->hasOne(Currency::class,'id','md_sc_currency_id');
    }

}

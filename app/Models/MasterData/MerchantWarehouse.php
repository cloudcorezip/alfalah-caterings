<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\MasterData;


class MerchantWarehouse extends BaseMasterDataModel
{
    protected $table='md_merchant_inv_warehouses';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
        'name'=>'required',
        'md_merchant_id'=>'required|exists:md_merchants,id',
        'is_default'=>'required'
    ];

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class Contact extends BaseMasterDataModel
{
    protected $table='md_contacts';
    protected $primaryKey='id';
    public $timestamps=true;



}

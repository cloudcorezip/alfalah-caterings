<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use App\Models\Marketing\MerchantRedeemPoint;
use App\Models\SennaPayment\Transaction;

class Subscription extends BaseMasterDataModel
{
    protected $table='md_subscriptions';
    protected $primaryKey='id';
    public $timestamps=true;

    const MONTHLY='monthly';
    const YEARLY='yearly';
    const SIX_MONTH='sixmonth';
    const THREEMONTH='threemonth';

    public function redeem()
    {
        return $this->morphOne(MerchantRedeemPoint::class,'redeemable');
    }



}

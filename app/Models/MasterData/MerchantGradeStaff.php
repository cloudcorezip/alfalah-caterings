<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\MasterData;


use App\Models\BaseModel;

class MerchantGradeStaff extends BaseModel
{
    protected $table='merchant_grade_staffs';
    protected $primaryKey='id';
    public $timestamps=true;

}

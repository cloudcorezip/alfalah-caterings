<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use App\Models\MasterData\BaseMasterDataModel;

class TypeOfBusiness extends BaseMasterDataModel
{
    protected $table='md_type_of_business';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required'
    ];


}

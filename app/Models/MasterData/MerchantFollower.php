<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantFollower extends BaseMasterDataModel
{
    protected $table='md_merchant_followers';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantBranch extends BaseMasterDataModel
{
    protected $table='md_merchant_branchs';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'email_branch'=>'required',
    ];

    public function getHead()
    {
        return $this->hasOne(Merchant::class,'id','head_merchant_id');
    }


    public function getHeadBranchRecursive()
    {
        return $this->hasOne(MerchantBranch::class,'branch_merchant_id','head_merchant_id');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','branch_merchant_id');
    }

    public function getChildBranch()
    {
        return $this->hasMany(MerchantBranch::class,'head_merchant_id','branch_merchant_id');
    }

}

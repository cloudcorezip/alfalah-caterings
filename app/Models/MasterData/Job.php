<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\MasterData;

class Job extends BaseMasterDataModel
{
    protected $table='md_jobs';
    protected $primaryKey='id';
    public $timestamps=true;
}
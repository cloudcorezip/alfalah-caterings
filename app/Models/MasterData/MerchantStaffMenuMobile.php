<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantStaffMenuMobile extends BaseMasterDataModel
{
    protected $table='md_merchant_menu_staff_mobiles';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'menus'=>'required',
        'md_user_id_staff'=>'required|exists:md_users,id'
    ];

}

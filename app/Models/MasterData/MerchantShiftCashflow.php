<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;

use App\Models\MasterData\SennaCashier\CashType;

class MerchantShiftCashflow extends BaseMasterDataModel
{
    protected $table='md_merchant_shift_cashflows';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
      'md_sc_cash_type_id'=>'required|exists:md_sc_cash_types,id',
      'amount'=>'numeric|required',
      'note'=>'required'
    ];

    public function getCashType()
    {
        return $this->hasOne(CashType::class,'id','md_sc_cash_type_id');

    }
    public function getShift()
    {
        return $this->hasOne(MerchantShift::class,'id','md_merchant_shift_id');

    }

}

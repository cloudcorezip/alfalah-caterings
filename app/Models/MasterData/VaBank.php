<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class VaBank extends BaseMasterDataModel
{
    protected $table='md_va_banks';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getBank()
    {
        return $this->hasOne(Bank::class,'id','md_bank_id');
    }



}

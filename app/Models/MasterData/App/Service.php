<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\App;


use App\Models\MasterData\BaseMasterDataModel;

class Service extends BaseMasterDataModel
{
    protected $table='md_app_services';
    protected $primaryKey='id';
    public $timestamps=true;

    const JASA=1;
    const TOKO=2;
    const TOKO_OFFLINE=3;


}

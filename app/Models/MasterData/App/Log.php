<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\App;


use App\Models\MasterData\BaseMasterDataModel;

class Log extends BaseMasterDataModel
{
    protected $table='md_app_logs';
    protected $primaryKey='id';
    public $timestamps=true;

}

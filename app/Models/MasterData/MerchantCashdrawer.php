<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantCashdrawer extends BaseMasterDataModel
{
    protected $table='md_merchant_cashdrawers';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
        'name'=>'required',
        'md_merchant_id'=>'required|exists:md_merchants,id',
        'is_active'=>'required'
    ];


}

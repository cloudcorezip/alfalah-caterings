<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class TransactionStatus extends BaseMasterDataModel
{
    protected $table='md_transaction_status';
    protected $primaryKey='id';
    public $timestamps=true;

    const UNPAID=1;
    const PAID=2;
    const PENDING=3;
    const CANCELED=4;


}

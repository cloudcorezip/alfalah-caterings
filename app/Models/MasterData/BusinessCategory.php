<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\MasterData;


class BusinessCategory extends BaseMasterDataModel
{

    protected $table='md_business_categories';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getChild()
    {
        return $this->hasMany(self::class,'parent_id','id');
    }

}

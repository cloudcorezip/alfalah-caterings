<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantStaffCategory extends BaseMasterDataModel
{
    protected $table='md_merchant_staff_categories';
    protected $primaryKey='id';
    public $timestamps=true;

}

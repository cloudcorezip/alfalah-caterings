<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class UserAddress extends BaseMasterDataModel
{

    protected $table='md_user_address';
    protected $primaryKey='id';
    public $timestamps=true;

    public $fillable=[
        'latitude',
        'longitude',
        'md_user_id',
        'address',
        'is_primary'
    ];

    public  $rule=[
        'address'=>'required',
        'md_user_id'=>'required|exists:md_users,id',
        'latitude'=>'required',
        'longitude'=>'required',
        'is_primary'=>'required'
    ];

}

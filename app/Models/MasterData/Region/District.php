<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\Region;


use App\Models\MasterData\BaseMasterDataModel;

class District extends BaseMasterDataModel
{
    protected $table='md_districts';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getCity()
    {
        return $this->hasOne(City::class,'id','md_city_id');
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\Region;


use App\Models\MasterData\BaseMasterDataModel;

class Province extends BaseMasterDataModel
{
    protected $table='md_provinces';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getCity()
    {
        return $this->hasMany(City::class,'md_province_id','id');
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\Region;


use App\Models\MasterData\BaseMasterDataModel;

class Village extends BaseMasterDataModel
{
    protected $table='md_villages';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getDistrict()
    {
        return $this->hasOne(District::class,'id','md_district_id');
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\Region;


use App\Models\MasterData\BaseMasterDataModel;

class City extends BaseMasterDataModel
{
    protected $table='md_cities';
    protected $primaryKey='id';
    public $timestamps=true;


    public function getProvince()
    {
        return $this->hasOne(Province::class,'id','md_province_id');
    }

}

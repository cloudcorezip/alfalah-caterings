<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class UserActivityNotification extends BaseMasterDataModel
{
    protected $table='md_user_activity_notifications';
    protected $primaryKey='id';
    public $timestamps=true;
    protected $casts = [
        'other_information' => 'array',
    ];

    public function getOtherInformationAttribute($value) {
        return json_decode($value);
    }
    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class UserBank extends BaseMasterDataModel
{
    protected $table='md_user_banks';
    protected $primaryKey='id';
    public $timestamps=true;

    public $fillable=[
        'bank_account_name','no_rek','md_bank_id','md_user_id'
    ];

    public  $ruleCreate=[
        'bank_account_name'=>'required',
        'no_rek'=>'required',
        'md_bank_id'=>'required|exists:md_banks,id',
        'md_user_id'=>'required|exists:md_users,id'
    ];
    public  $ruleUpdate=[
        'bank_account_name'=>'required',
        'no_rek'=>'required',
        'md_bank_id'=>'required|exists:md_banks,id',
    ];

    public function getBank()
    {
        return $this->hasOne(Bank::class,'id','md_bank_id');
    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class CashierTransactionStatus extends BaseMasterDataModel
{
    protected $table='md_sc_transaction_status';
    protected $primaryKey='id';
    public $timestamps=true;



}

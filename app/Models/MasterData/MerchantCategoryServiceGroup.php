<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use App\Models\MasterData\SennaJasa\CategoryService;

class MerchantCategoryServiceGroup extends BaseMasterDataModel
{
    protected $table='md_merchant_category_groups';
    protected $primaryKey='id';
    public $timestamps=true;


    public function getCategory()
    {
        return $this->hasOne(CategoryService::class,'id','md_ss_category_service_id');
    }
}

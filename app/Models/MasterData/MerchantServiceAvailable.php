<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantServiceAvailable extends BaseMasterDataModel
{
    protected $table='md_merchant_service_availables';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleCreateOrUpdate=[
        'time_available'=>'required',
    ];
    protected $casts = [
        'service_time' => 'array',
    ];

    public function getSeriveTimeAttribute($value) {
        return json_decode($value);
    }
}

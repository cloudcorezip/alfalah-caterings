<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use App\Models\Marketing\MerchantMembership;
use App\Models\MasterData\App\Service;
use App\Models\MasterData\TypeOfBusiness;
use App\Models\MasterData\Region\District;
use App\Models\SennaJasa\Service as MerchantService;
use App\Models\SennaToko\MerchantFreeDelivery;
use App\Models\SennaToko\Product;
use Illuminate\Support\Facades\DB;

class Merchant extends BaseMasterDataModel
{
    protected $table='md_merchants';
    protected $primaryKey='id';
    public $timestamps=true;


    public $ruleCompanyCreateJasa=[
        'name'=>'required',
        'description'=>'nullable',
        'address'=>'nullable',
        'email_merchant'=>'nullable',
        'phone_merchant'=>'nullable',
        'md_district_id'=>'required|exists:md_districts,id',
        'md_type_of_business_id'=>'required|exists:md_type_of_business,id',
        'range_of_business'=>'required',
        'md_app_service_id'=>'required',
        'categories'=>'required',
        'image'=>'required|file|image|mimes:jpg,jpeg,bmp,png',
  ];

    public $ruleCompanyCreateToko=[
        'name'=>'required',
        'description'=>'nullable',
        'address'=>'nullable',
        'email_merchant'=>'nullable',
        'phone_merchant'=>'nullable',
        'md_district_id'=>'required|exists:md_districts,id',
        'md_type_of_business_id'=>'required|exists:md_type_of_business,id',
        'md_app_service_id'=>'required',
        'md_sc_currency_id'=>'required',
        'categories'=>'required',
        'image'=>'required|file|image|mimes:jpg,jpeg,bmp,png',
    ];

    public $ruleCompanyUpdateJasa=[
        'name'=>'required',
        'description'=>'nullable',
        'address'=>'nullable',
        'email_merchant'=>'nullable',
        'phone_merchant'=>'nullable',
        'md_district_id'=>'required|exists:md_districts,id',
        'md_type_of_business_id'=>'required|exists:md_type_of_business,id',
        'md_app_service_id'=>'required',
        'categories'=>'required',
    ];

    public $ruleCompanyUpdateToko=[
        'name'=>'required',
        'description'=>'nullable',
        'address'=>'nullable',
        'email_merchant'=>'nullable',
        'phone_merchant'=>'nullable',
        'md_district_id'=>'required|exists:md_districts,id',
        'md_type_of_business_id'=>'required|exists:md_type_of_business,id',
        'md_app_service_id'=>'required',
        'md_sc_currency_id'=>'required',
        'categories'=>'required',
    ];

    public $ruleImage=[
        'image'=>'required|file|image|mimes:jpg,jpeg,bmp,png'
    ];
    public $ruleLogo=[
        'logo'=>'required|file|image|mimes:jpg,jpeg,bmp,png'
    ];

    public $ruleCoordinat=[
        'latitude'=>'required',
        'longitude'=>'required'
    ];

    public function getService()
    {
        return $this->hasOne(Service::class,'id','md_app_service_id');
    }

    public function getCategoryService()
    {
        return $this->hasMany(MerchantCategoryServiceGroup::class,'md_merchant_id','id');

    }

    public function getRangeOfBusiness()
    {
        return $this->hasMany(MerchantRangeOfBusiness::class,'md_merchant_id','id');
    }

    public function getDistrict()
    {
        return $this->hasOne(District::class,'id','md_district_id');
    }

    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }


    public function getServiceAvailable()
    {
        return $this->hasMany(MerchantServiceAvailable::class,'md_merchant_id','id');
    }
    public function getCurrency()
    {
        return $this->hasOne(MerchantCurrency::class,'md_merchant_id','id');
    }

    public function getCategory()
    {
        return $this->hasMany(MerchantShopCategory::class,'md_merchant_id','id');
    }
    public function getServiceDetail($userId)
    {
        return MerchantService::select([
            'ss_services.id',
            'ss_services.name',
            'ss_services.banner',
            'ss_services.description',
            DB::raw('coalesce((SELECT (sum(sd.rating)/count(*)) FROM ss_review_detail_order_services sd join ss_detail_services d
                 on sd.ss_detail_service_id=d.id
                 join ss_services s
                 on s.id=d.ss_service_id
                 where s.id=ss_services.id
                 ),0)as rating'),
        ])->with(['getDetail'=>function($query){
            $query->select([
                'ss_detail_services.id',
                'ss_detail_services.detail_name',
                'ss_detail_services.ss_service_id',
                'ss_detail_services.banner',
                'ss_detail_services.description',
                'ss_detail_services.md_unit_id',
                'ss_detail_services.price_markup',
                'md_units.name as unit_name',
                DB::raw('coalesce((SELECT (sum(sd.rating)/count(*)) FROM ss_review_detail_order_services sd join ss_detail_services d
                 on sd.ss_detail_service_id=d.id
                where d.id=ss_detail_services.id
                 ),0)as rating'),
                DB::raw("
                (CASE  WHEN
                (SELECT price_after_promo FROM ss_promo_detail_services WHERE ss_detail_service_id=ss_detail_services.id and is_active=1) IS NULL
                THEN 0
                ELSE                 (SELECT price_after_promo FROM ss_promo_detail_services WHERE ss_detail_service_id=ss_detail_services.id and is_active=1)
                END
                ) as price_after_promo
                "),
            ])
            ->join('md_units','md_units.id','ss_detail_services.md_unit_id');
            ;
        }])
            ->where('ss_services.md_user_id_created',$userId)
            ->where('ss_services.is_published',1)
            ->get();
    }

    public function getProduct($userId)
    {
        $fields=[
            'sc_products.id',
            'sc_products.name',
            'sc_products.foto',
            'sc_products.stock',
            'sc_products.selling_price as price_markup',
            'sc_products.purchase_price',
            'un.name as unit_name',
            'm.name as merchant_name',
            'm.id as merchant_id',
            'sc_products.md_sc_category_id',
            DB::raw("
                (CASE  WHEN
                (SELECT price_after_promo FROM sc_promo_products WHERE sc_product_id=sc_products.id and is_active=1) IS NULL
                THEN 0
                ELSE                 (SELECT price_after_promo FROM sc_promo_products WHERE sc_product_id=sc_products.id and is_active=1)
                END
                ) as price_after_promo
                "),
            DB::raw("(4.5) as rating"),
        ];
        $data=Product::select(
            $fields
        )
            ->with('getGlobalCategory')
            ->join('md_users as u','u.id','sc_products.md_user_id')
            ->join('md_merchants as m','m.md_user_id','u.id')
            ->join('md_units as un','un.id','sc_products.md_unit_id')
            ->where('sc_products.is_show_senna_app',1)
            ->where('sc_products.is_deleted',0)
            ->where('u.id',$userId)
            ->orderBy('price_after_promo','ASC')
            ->get();

        return $data;
    }

    public function getFreeDelivery()
    {
        return $this->hasOne(MerchantFreeDelivery::class,'md_merchant_id','id');
    }
    public function getTypeOfBusiness()
    {
        return $this->hasOne(TypeOfBusiness::class,'id','md_type_of_business_id');
    }

    public function getSubscription()
    {
        return $this->hasOne(Subscription::class,'id','md_subscription_id');
    }

    public function getBranch()
    {
        return $this->hasMany(MerchantBranch::class,'head_merchant_id','id')->where('is_active',1);
    }

    public function getInventoryMethod()
    {
        return $this->hasOne(InventoryMethod::class,'id','md_inventory_method_id');
    }

    public function getMerchantPoint()
    {
        return $this->hasOne(MerchantMembership::class,'md_merchant_id','id');
    }

    public function getWarehouseDefault()
    {
        return $this->hasOne(MerchantWarehouse::class,'md_merchant_id','id')->where('is_default',1);
    }

    public function getHeadBranch()
    {
        return $this->hasOne(MerchantBranch::class,'branch_merchant_id','id')->where('is_active',1);

    }

}

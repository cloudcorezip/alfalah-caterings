<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class InventoryMethod extends BaseMasterDataModel
{

    protected $table='md_inventory_methods';
    protected $primaryKey='id';
    public $timestamps=true;

    const FIFO=2;
    const LIFO=3;
    const AVERAGE=4;

}

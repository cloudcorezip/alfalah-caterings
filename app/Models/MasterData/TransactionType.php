<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class TransactionType extends BaseMasterDataModel
{
    protected $table='md_transaction_types';
    protected $primaryKey='id';
    public $timestamps=true;

    const SENNA_PAY=1;
    const COD=2;
    const CICIL=3;
    const QRIS=4;
    const VA=5;
    const EWALLET=15;

}

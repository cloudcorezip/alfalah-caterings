<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use Illuminate\Support\Facades\DB;

class MerchantMenu extends BaseMasterDataModel
{
    protected $table='md_merchant_menus';
    protected $primaryKey='id';
    public $timestamps=true;


    public static function actionList()
    {
        return [
            '_edit',
            '_hapus',
            '_tambah',
            '_impor',
            '_ekspor',
            '_detail',
        ];
    }


    public function getParent()
    {
        return $this->hasOne(MerchantMenu::class,'id','parent_id');
    }

    public function getChild()
    {
        return $this->hasMany(MerchantMenu::class,'parent_id','id')->with('getChild');
    }

}

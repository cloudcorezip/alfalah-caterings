<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\MasterData;


use App\Models\BaseModel;

class MerchantWarehouseType extends BaseModel
{
    protected $table='merchant_inv_warehouse_types';
    protected $primaryKey='id';
    public $timestamps=true;


    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }
}

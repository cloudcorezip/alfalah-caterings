<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


use App\Models\MasterData\SennaCashier\Category;

class MerchantShopCategory extends BaseMasterDataModel
{
    protected $table='md_merchant_shop_categories';
    protected $primaryKey='id';
    public $timestamps=true;


    public function getCategory()
    {
        return $this->hasOne(Category::class,'id','md_sc_category_id');
    }


}

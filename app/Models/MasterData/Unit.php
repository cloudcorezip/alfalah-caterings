<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class Unit extends BaseMasterDataModel
{
    protected $table='md_units';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required',
        'note'=>'nullable'
    ];

}

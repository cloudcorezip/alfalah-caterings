<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class MerchantApproval extends BaseMasterDataModel
{
    protected $table='md_merchant_approval_settings';
    protected $primaryKey='id';
    public $timestamps=true;



}

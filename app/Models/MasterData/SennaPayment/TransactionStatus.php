<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaPayment;


use App\Models\MasterData\BaseMasterDataModel;

class TransactionStatus extends BaseMasterDataModel
{
    protected $table='md_sp_transaction_status';
    protected $primaryKey='id';
    public $timestamps=true;

    const PENDING=1;
    const SUCCESS=2;
    const CANCELED=3;
    const FAILED=4;
    const REFUND=5;

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaPayment;


use App\Models\MasterData\BaseMasterDataModel;

class GuideCategory extends BaseMasterDataModel
{
    protected $table='md_sp_guide_categories';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required',
    ];
}

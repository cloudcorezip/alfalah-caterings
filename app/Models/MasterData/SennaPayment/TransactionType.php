<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaPayment;


use App\Models\MasterData\BaseMasterDataModel;

class TransactionType extends BaseMasterDataModel
{
    protected $table='md_sp_transaction_types';
    protected $primaryKey='id';
    public $timestamps=true;

    const BETWEEN=3;
    const TOPUP_MANUAL=8;
    const TOPUP_VA=9;
    const WD_MANUAL=10;
    const WD_ONLINE=11;
    const TF_JASA=12;
    const TF_TOKO=13;
    const TF_EVENT=14;
    const TF_DRIVER=15;
    const SUBSCRIPTION_FEE=16;
    const PLUGIN_FEE=17;


    public function getParent()
    {
        return $this->hasOne(TransactionType::class,'id','parent_id');
    }

    public function getChild()
    {
        return $this->hasMany(TransactionType::class,'parent_id','id')->with('getChild');
    }
}

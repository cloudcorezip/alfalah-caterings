<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData\SennaPayment;


use App\Models\MasterData\BaseMasterDataModel;

class Guide extends BaseMasterDataModel
{
    protected $table='md_sp_guides';
    protected $primaryKey='id';
    public $timestamps=true;

    public  $ruleUpdate=[
        'title'=>'required',
        'description'=>'nullable',
        'image'=>'required',
        'order_number'=>'numeric|required',
        'md_sp_guide_category_id'=>'required',
    ];


    public  $ruleCreate=[
        'title'=>'required',
        'description'=>'nullable',
        'image'=>'file|image|mimes:jpg,jpeg,bmp,png',
        'order_number'=>'numeric|required',
        'md_sp_guide_category_id'=>'required',
        ];
}

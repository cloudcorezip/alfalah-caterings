<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\MasterData;


class Varian extends BaseMasterDataModel
{
    protected $table='md_multi_varians';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required'
    ];

}
<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\MasterData\Attendance;


use App\Models\MasterData\BaseMasterDataModel;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\PermitCategory;
use App\Models\MasterData\User;

class MerchantStaffPermitSubmission extends BaseMasterDataModel
{
    protected $table='md_merchant_staff_permit_submissions';
    protected $primaryKey='id';

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getUserStaff()
    {
        return $this->hasOne(User::class,'id','md_staff_user_id');
    }

    public function getUserApproved()
    {
        return $this->hasOne(User::class,'id','md_user_id_approved');
    }

    public function getPermitCategory()
    {
        return $this->hasOne(PermitCategory::class,'id','md_permit_category_id');
    }

    public $submit=[
        'md_merchant_id'=>'required|exists:md_merchants,id',
        'supporting_file'=>'required|file|image|mimes:jpg,jpeg,bmp,png',
        'md_staff_user_id'=>'required|exists:md_users,id',
        'md_permit_category_id'=>'required|exists:md_permit_categories,id',
        'type_of_attendance'=>'required'
    ];
    public $approve=[
        'md_user_id_approved'=>'required|exists:md_users,id',
        'is_approved'=>'required|numeric'
    ];

}

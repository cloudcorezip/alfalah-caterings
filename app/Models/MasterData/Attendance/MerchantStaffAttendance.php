<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\MasterData\Attendance;


use App\Models\MasterData\BaseMasterDataModel;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\User;

class MerchantStaffAttendance extends BaseMasterDataModel
{
    protected $table='md_merchant_staff_attendances';
    protected $primaryKey='id';

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getUserStaff()
    {
        return $this->hasOne(User::class,'id','md_staff_user_id');
    }

    public $fileStartValidate=[
        'start_work_file'=>'required|file|image|mimes:jpg,jpeg,bmp,png'
        ];

    public $startAttendance=[
        'md_merchant_id'=>'required|exists:md_merchants,id',
        'start_work'=>'required',
        'start_longitude'=>'required',
        'start_latitude'=>'required',
        'start_work_file'=>'required|file|image|mimes:jpg,jpeg,bmp,png',
        'md_staff_user_id'=>'required|exists:md_users,id'
    ];
    public $endAttendance=[
        'end_work'=>'required',
        'end_longitude'=>'required',
        'end_latitude'=>'required',
        'end_work_file'=>'required|file|image|mimes:jpg,jpeg,bmp,png'

    ];
}

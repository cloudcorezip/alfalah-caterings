<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\QRIS;


class QRISActivation extends BaseQRISModel
{
    protected $table='qris_activations';
    protected $primaryKey='id';


    public $ruleCreate=[
        'md_user_id'=>'required|exists:md_users,id',
        'identity_card'=>'file|image|mimes:jpg,jpeg,bmp,png',
        'account_book'=>'file|image|mimes:jpg,jpeg,bmp,png',
        'bank_account_name'=>'required',
        'bank_account_number'=>'required|numeric',
        'phone_number'=>'required',
        'md_rajabiller_bank_id'=>'required|exists:md_rajabiller_banks,id'
    ];


    public  static  function  getWithdrawPeriod()
    {
        $array=[
            1=>'3 hari sekali',
            2=>'7 hari sekali',
            3=>'14 hari sekali',
            4=>'1 bulan sekali'
        ];

        return $array;
    }

    public function getXendit()
    {
        return $this->hasOne(XenditAvailableBank::class,'id','md_xendit_available_bank_id');

    }
    public function getBank()
    {
        return $this->hasOne(RajabillerBank::class,'id','md_rajabiller_bank_id');

    }
}

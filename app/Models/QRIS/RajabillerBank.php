<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\QRIS;


class RajabillerBank extends BaseQRISModel
{

    protected $table='md_rajabiller_banks';
    protected $primaryKey='id';

}

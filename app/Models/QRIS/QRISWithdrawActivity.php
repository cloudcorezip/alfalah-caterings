<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\QRIS;


class QRISWithdrawActivity extends BaseQRISModel
{

    protected $table='qris_withdraw_activities';
    protected $primaryKey='id';

    public $ruleCreate=[
        'md_user_id'=>'required|exists:md_users,id',
        'admin_fee'=>'required|numeric',
        'amount'=>'required|numeric',
        'md_merchant_id'=>"required|exists:md_merchants,id",
        'timezone'=>'required'
    ];

    public function getActivation()
    {
        return $this->hasOne(QRISActivation::class,'id','qris_activation_id');
    }
}

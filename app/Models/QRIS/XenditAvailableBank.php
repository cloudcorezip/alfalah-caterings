<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\QRIS;


class XenditAvailableBank extends BaseQRISModel
{

    protected $table='md_xendit_available_banks';
    protected $primaryKey='id';

}

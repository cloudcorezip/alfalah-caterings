<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Widget;

class MerchantDashboardWidget extends BaseWidgetModel
{
    protected $table='merchant_dashboard_widgets';
    protected $primaryKey='id';

    public static function widgetList()
    {
        $data = [
            (object)[
                "id" =>1,
                "element_id" => "dw-cash-flow",
                "name" =>"Arus Kas",
                "file" => "cash-flow",
                "function"=>"loadCashFlow()"
            ],
            (object)[
                "id" =>2,
                "element_id" => "dw-best-selling-product",
                "name" =>"Produk Terlaris",
                "file" => "best-selling-product",
                "function"=>"loadBestSellingProduct()"
            ],
            (object)[
                "id" =>3,
                "element_id" => "dw-omzet",
                "name" =>"Omzet",
                "file" => "omzet",
                "function"=>"loadOmzet()"
            ],
            (object)[
                "id" =>4,
                "element_id" => "dw-commission",
                "name" =>"Komisi Karyawan",
                "file" => "commission",
                "function"=>"loadCommission()"
            ],
            (object)[
                "id" =>5,
                "element_id" => "dw-ap",
                "name" =>"Hutang",
                "file"=> "ap",
                "function"=>"loadAp()"
            ],
            (object)[
                "id" =>6,
                "element_id" => "dw-ar",
                "name" =>"Piutang",
                "file" => "ar",
                "function"=>"loadAr()"
            ],
            (object)[
                "id" =>7,
                "element_id" => "dw-cost",
                "name" =>"Distribusi Pengeluaran",
                "file" => "cost",
                "function"=>"loadCost()"
            ],
            (object)[
                "id" =>8,
                "element_id" => "dw-product-consignment",
                "name" =>"Produk Konsinyasi",
                "file" => "consignment",
                "function"=>"loadConsignment()"
            ],
            (object)[
                "id" =>9,
                "element_id" => "dw-stock",
                "name" =>"Stok Hampir Habis",
                "file" => "stock",
                "function"=>"loadStock()"
            ],
            (object)[
                "id" =>10,
                "element_id" => "dw-profit-lost",
                "name" =>"Laba Rugi",
                "file" => "profit-lost",
                "function"=>"loadProfitLost()"
            ],
            (object)[
                "id" =>11,
                "element_id" => "dw-best-selling-service",
                "name" =>"Jasa Terlaris",
                "file" => "best-selling-service",
                "function"=>"loadBestSellingService()"
            ],
            (object)[
                "id" =>12,
                "element_id" => "dw-consumable-product-stock",
                "name" =>"Stok Produk Habis Pakai",
                "file" => "consumable-product-stock",
                "function"=>"loadConsumableProductStock()"
            ]

        ];

        return collect($data);
    }

}

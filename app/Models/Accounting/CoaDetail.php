<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantCurrency;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoaDetail extends BaseSennaAccModel
{
    use SoftDeletes;
    protected $table='acc_coa_details';
    protected $primaryKey='id';


    public function getCurrency()
    {
        return $this->hasOne(MerchantCurrency::class,'id','md_merchant_currency_id');
    }

    public function getJurnalDetail()
    {
        return $this->hasMany(JurnalDetail::class,'acc_coa_detail_id','id');
    }

    public function getCategory()
    {
        return $this->hasOne(CoaCategory::class,'id','acc_coa_category_id');
    }

    public function getAmountCoaDistribution($id,$refCode){

        $amount=JurnalDetail::
          select(['j.trans_amount'])
        ->where('acc_jurnal_details.acc_coa_detail_id',$id)
            ->join('acc_jurnals as j','j.id','acc_jurnal_details.acc_jurnal_id')
            ->where('j.ref_code',$refCode)
            ->where('j.ref_id',$id)
            ->where('j.is_deleted',0)
            ->first();
        if(is_null($amount)){

            return 0;
        }else{
            return $amount->trans_amount;
        }
    }

}

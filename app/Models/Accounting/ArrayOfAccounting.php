<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


class ArrayOfAccounting
{

    public static function generateTypeOfMapping($acceptNull = false)
    {
        $option = $acceptNull ? [ '-' => '- Pilih Data -'] : [];
        $data = [
            1=>'Kas',
            2=>'Pengeluaran',
            3=>'Pendapatan',
            4=>'Utang',
            5=>'Piutang'
        ];

        foreach ($data as $key => $value) {
            $option[$key] = $value;
        }
        return $option;
    }

    public static function reasonOfRetur($isDebet)
    {
        if($isDebet==0)
        {
            $data = [
                1=>'Tukar Barang(Mengganti barang yang rusak dengan yang baru)',
                2=>'Retur Dengan Pembatalan Item Transaksi',
            ];
        }else{
            $data = [
                1=>'Tukar Barang(Mengganti barang yang rusak dengan yang baru)',
                3=>'Retur Dengan Mengurangi Hutang',
            ];
        }


        foreach ($data as $key => $value) {
            $option[$key] = $value;
        }
        return $option;
    }

    public static function reasonOfReturSale($isDebet)
    {
        if($isDebet==0)
        {
            $data = [
                1=>'Tukar Barang(Mengganti barang yang rusak dengan yang baru)',
                2=>'Retur Dengan Pembatalan Item Transaksi',
            ];
        }else{
            $data = [
                1=>'Tukar Barang(Mengganti barang yang rusak dengan yang baru)',
                3=>'Retur Dengan Mengurangi Piutang',
            ];
        }


        foreach ($data as $key => $value) {
            $option[$key] = $value;
        }
        return $option;
    }

    public static  function  searchReason($v)
    {
        $reason= [
            1=>'Tukar Barang(Mengganti barang yang rusak dengan yang baru)',
            2=>'Retur Dengan Pembatalan Item Transaksi',
            3=>'Retur Dengan Mengurangi Piutang',
        ];
        foreach ($reason as $key =>$item)
        {
            if($key==$v)
            {
                return $item;
                break;
            }
        }

    }

    public static function generateApType($acceptNull = false)
    {
        $option = $acceptNull ? [ '-' => '- Pilih Data -'] : [];
        $data = [
            1=>'Tunai',
            2=>'Transfer Antar Bank/Payment Gateway'
        ];

        foreach ($data as $key => $value) {
            $option[$key] = $value;
        }
        return $option;
    }

    public static function generateTypeOfCashflow($acceptNull=false)
    {
        $option = $acceptNull ? [ '-' => '- Pilih Data -'] : [];
        $data = [
            'operasional'=>'Operasional',
            'investasi'=>'Investasi',
            'pendanaan'=>'Pendanaan',
        ];

        foreach ($data as $key => $value) {
            $option[$key] = $value;
        }
        return $option;
    }


    public static function generateActivityCash($acceptNull=false)
    {
        $option = $acceptNull ? [ '-' => '- Pilih Data -'] : [];
        $data = [
            1=>'Transfer Antar Akun Bank',
            2=>'Transfer Antar Kas',
            3=>'Penarikan Akun Bank Ke Kas',
            4=>'Setoran Kas Ke Akun Bank',
        ];

        foreach ($data as $key => $value) {
            $option[$key] = $value;
        }
        return $option;
    }

    public static function searchOfMapping($value)
    {
        foreach (self::generateTypeOfMapping() as $key=>$item)
        {
            if($item==$value)
            {
                return $key;
                break;
            }
        }
    }

    public static function searchOfActivity($key)
    {
        foreach (self::generateActivityCash() as $num=>$item)
        {
            if($key==$num)
            {
                return $item;
                break;
            }
        }

    }

    public static function searchOfActivityByName($key)
    {
        foreach (self::generateActivityCash() as $num=>$item)
        {
            if($key==$item)
            {
                return $num;
                break;
            }
        }

    }
    public static function searchOfReasonName($isDebet,$key)
    {
        foreach (self::reasonOfRetur($isDebet) as $num=>$item)
        {
            if($key==$num)
            {
                return $item;
                break;
            }
        }

    }



}

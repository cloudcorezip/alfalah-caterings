<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;

class Jurnal extends BaseSennaAccModel
{
    protected $table='acc_jurnals';
    protected $primaryKey='id';

    public function getDetail()
    {
        return $this->hasMany(JurnalDetail::class,'acc_jurnal_id','id');
    }

    public function getCashBank()
    {
        return $this->hasMany(JurnalCashBank::class,'acc_jurnal_id','id');
    }

    public function getCoa()
    {
        return $this->hasOne(CoaMapping::class,'acc_coa_detail_id','ref_type');
    }

    public function ap()
    {
        return $this->morphOne(MerchantAp::class,'apable');
    }

    public function ar()
    {
        return $this->morphOne(MerchantAr::class,'arable');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

}


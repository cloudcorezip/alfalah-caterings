<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;

class CoaMapping extends BaseSennaAccModel
{

    protected $table='acc_coa_mappings';
    protected $primaryKey='id';

    public function getCoaDetail()
    {
        return $this->hasOne(CoaDetail::class,'id','acc_coa_detail_id');
    }
}

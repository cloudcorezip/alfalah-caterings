<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Accounting;


class MerchantArDetail extends BaseSennaAccModel
{
    protected $table='acc_merchant_ar_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'paid_nominal'=>'numeric|required',
        'paid_date'=>'required',
        'note'=>'nullable',
        'timezone'=>'required'
    ];


    public function getAr()
    {
        return $this->hasOne(MerchantAr::class,'id','acc_merchant_ar_id');
    }
    public function getCoa()
    {
        return $this->hasOne(CoaDetail::class,'id','trans_coa_detail_id');
    }


}

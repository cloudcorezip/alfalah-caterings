<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;

class MerchantAp extends BaseSennaAccModel
{
    protected $table='acc_merchant_ap';
    protected $primaryKey='id';
    public $timestamps=true;

    public $fillable=[
        'md_merchant_id',
        'residual_amount',
        'paid_nominal',
        'is_paid_off',
        'due_date',
        'sync_id'
    ];

    public $fileValidate=[
        'trans_proof'=>'file|image|mimes:jpg,jpeg,bmp,png,docx,xlsx,pdf,zip'
    ];

    public function apable()
    {
        return $this->morphTo();
    }

    public function getDetail()
    {
        return $this->hasMany(MerchantApDetail::class,'acc_merchant_ap_id','id');
    }

    public function getCoa()
    {
        return $this->hasOne(CoaMapping::class,'acc_coa_detail_id','ap_acc_coa');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


class MerchantTempAmount extends BaseSennaAccModel
{
    protected $table='acc_merchant_temp_amounts';
    protected $primaryKey='id';

}

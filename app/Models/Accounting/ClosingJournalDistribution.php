<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;

class ClosingJournalDistribution extends BaseSennaAccModel
{
    protected $table='acc_closing_journal_distributions';
    protected $primaryKey='id';

    public function getDetail()
    {
        return $this->hasMany(ClosingJournalDetail::class,'acc_distribution_id','id');
    }
    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

}

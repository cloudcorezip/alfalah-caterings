<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;

class CoaCashflowFormatDetail extends BaseSennaAccModel
{
    protected $table='acc_coa_cashflow_format_details';
    protected $primaryKey='id';


    public function getCoa()
    {
        return $this->hasOne(CoaDetail::class,'id','acc_coa_detail_id');
    }

    public function getCashflowFormat()
    {
        return $this->hasOne(CoaCashflowFormat::class,'id','acc_coa_cashflow_format_id');
    }


}

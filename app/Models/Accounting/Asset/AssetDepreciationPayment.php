<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting\Asset;


use App\Models\Accounting\BaseSennaAccModel;
use App\Models\Accounting\CoaDetail;

class AssetDepreciationPayment extends BaseSennaAccModel
{
    protected $table='acc_asset_depreciation_payments';
    protected $primaryKey='id';

    public function getAssetDepreciation()
    {
        return $this->hasOne(AssetDepreciation::class,'id','acc_asset_depreciation_id');
    }

    public function getCoa()
    {
        return $this->hasOne(CoaDetail::class,'id','acc_payment_id');
    }

}

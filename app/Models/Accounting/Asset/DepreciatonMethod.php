<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting\Asset;


use App\Models\Accounting\BaseSennaAccModel;

class DepreciatonMethod extends BaseSennaAccModel
{
    protected $table='acc_md_depreciation_methods';
    protected $primaryKey='id';
}

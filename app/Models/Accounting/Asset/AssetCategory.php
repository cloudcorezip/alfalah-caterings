<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting\Asset;


use App\Models\Accounting\BaseSennaAccModel;

class AssetCategory extends BaseSennaAccModel
{
    protected $table='acc_asset_categories';
    protected $primaryKey='id';
}

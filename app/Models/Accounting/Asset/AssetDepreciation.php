<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting\Asset;


use App\Models\Accounting\BaseSennaAccModel;
use App\Models\Accounting\CoaDetail;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Supplier;

class AssetDepreciation extends BaseSennaAccModel
{
    protected $table='acc_asset_depreciations';
    protected $primaryKey='id';
    public function getProperty()
    {
        return $this->hasOne(PropertyGroup::class,'id','acc_md_property_group_id');
    }

    public function getCategory()
    {
        return $this->hasOne(AssetCategory::class,'id','acc_asset_category_id');

    }
    public function getDepreciationMethod()
    {
        return $this->hasOne(DepreciatonMethod::class,'id','acc_md_depreciation_method_id');
    }

    public function getCoaAsset()
    {
        return $this->hasOne(CoaDetail::class,'id','acc_coa_asset_id');
    }

    public function getCoaDepreciation()
    {
        return $this->hasOne(CoaDetail::class,'id','account_depreciation_id');
    }

    public function getCoaAccumulation()
    {
        return $this->hasOne(CoaDetail::class,'id','account_accumulation_id');
    }

    public function getPayment()
    {
        return $this->hasMany(AssetDepreciationPayment::class,'acc_asset_depreciation_id','id')->orderBy('id','asc');
    }

    public function getDetail()
    {
        return $this->hasMany(AssetDepreciationDetail::class,'acc_asset_depreciation_id','id')->orderBy('id','asc');
    }

    public function getSupplier()
    {
        return $this->hasOne(Supplier::class,'id','sc_supplier_id');

    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting\Asset;


use App\Models\Accounting\BaseSennaAccModel;

class PropertyGroup extends BaseSennaAccModel
{
    protected $table='acc_md_property_groups';
    protected $primaryKey='id';

    public function getChild()
    {
        return $this->hasMany(PropertyGroup::class,'parent_id','id');
    }
    public function getParent()
    {
        return $this->hasOne(PropertyGroup::class,'id','parent_id');

    }
}

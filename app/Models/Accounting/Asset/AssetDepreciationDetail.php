<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting\Asset;


use App\Models\Accounting\BaseSennaAccModel;

class AssetDepreciationDetail extends BaseSennaAccModel
{
    protected $table='acc_asset_depreciation_details';
    protected $primaryKey='id';

}

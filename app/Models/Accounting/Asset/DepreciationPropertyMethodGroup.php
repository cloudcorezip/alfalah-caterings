<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting\Asset;


use App\Models\Accounting\BaseSennaAccModel;

class DepreciationPropertyMethodGroup extends BaseSennaAccModel
{
    protected $table='acc_md_property_depreciation_groups';
    protected $primaryKey='id';


    public function getProperty()
    {
        return $this->hasOne(PropertyGroup::class,'id','acc_md_property_group_id');
    }

    public function getDepreciationMethod()
    {
        return $this->hasOne(DepreciatonMethod::class,'id','acc_md_depreciation_method_id');
    }
}

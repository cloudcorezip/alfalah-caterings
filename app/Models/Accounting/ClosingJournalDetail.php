<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


class ClosingJournalDetail extends BaseSennaAccModel
{
    protected $table='acc_closing_journal_details';
    protected $primaryKey='id';

    public function getFromCoa()
    {
        return $this->hasOne(CoaDetail::class,'id','from_acc_coa_detail_id');
    }
    public function toCoa()
    {
        return $this->hasOne(CoaDetail::class,'id','to_acc_coa_detail_id');

    }

}

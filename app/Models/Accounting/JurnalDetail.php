<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


class JurnalDetail extends BaseSennaAccModel
{
    protected $table='acc_jurnal_details';
    protected $primaryKey='id';


    public function getCoa()
    {
        return $this->hasOne(CoaDetail::class,'id','acc_coa_detail_id');
    }

    public function getJurnal()
    {
        return $this->hasOne(Jurnal::class,'id','acc_jurnal_id');
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;

class CoaCashflowFormat extends BaseSennaAccModel
{
    protected $table='acc_coa_cashflow_formats';
    protected $primaryKey='id';

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');

    }

    public function getDetail()
    {
        return $this->hasMany(CoaCashflowFormatDetail::class,'acc_coa_cashflow_format_id','id');
    }

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\SennaCashier\CashType;

class MerchantCashflow extends BaseSennaAccModel
{
    protected $table='acc_merchant_cashflows';
    protected $primaryKey='id';

    public $create=[
        'name'=>'required',
        'md_sc_cash_type_id'=>'required|exists:md_sc_cash_types,id',
        'amount'=>'numeric|required',
        'cashflow_coa_detail_id'=>'required|exists:acc_coa_details,id',
        'to_cashflow_coa_detail_id'=>'required',
        'md_merchant_id'=>'required|exists:md_merchants,id',
        'admin_fee'=>'numeric|required',
        'created_by'=>'required'

    ];

    public function getCashType()
    {
        return $this->hasOne(CashType::class,'id','md_sc_cash_type_id');

    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;

class Period extends BaseSennaAccModel
{
    protected $table='acc_periods';
    protected $primaryKey='id';

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');

    }

}

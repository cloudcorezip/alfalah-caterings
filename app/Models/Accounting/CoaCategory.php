<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantMenu;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoaCategory extends BaseSennaAccModel
{
    use SoftDeletes;
    protected $table='acc_coa_categories';
    protected $primaryKey='id';

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getDetail()
    {
        return $this->hasMany(CoaDetail::class,'acc_coa_category_id','id')
            ->whereIn('is_deleted',[0,2])
            ->orderBy('id','asc');
    }

    public function getParent()
    {
        return $this->hasOne(CoaCategory::class,'id','parent_id');
    }

    public function getChild()
    {
        return $this->hasMany(CoaCategory::class,'parent_id','id')
            ->whereIn('is_deleted',[0,2])
            ->orderBy('id','asc');
    }
}

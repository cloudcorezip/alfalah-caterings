<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Accounting;


use App\Models\MasterData\Merchant;

class MerchantAr extends BaseSennaAccModel
{
    protected $table='acc_merchant_ar';
    protected $primaryKey='id';
    public $timestamps=true;

    public $fillable=[
        'md_merchant_id',
        'residual_amount',
        'paid_nominal',
        'is_paid_off',
        'due_date',
        'sync_id'
    ];
    public function arable()
    {
        return $this->morphTo();
    }

    public function getDetail()
    {
        return $this->hasMany(MerchantArDetail::class,'acc_merchant_ar_id','id');
    }

    public function getCoa()
    {
        return $this->hasOne(CoaMapping::class,'acc_coa_detail_id','ar_acc_coa');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

}

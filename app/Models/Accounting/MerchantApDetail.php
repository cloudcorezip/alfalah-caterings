<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Accounting;


class MerchantApDetail extends BaseSennaAccModel
{
    protected $table='acc_merchant_ap_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'paid_nominal'=>'numeric|required',
        'paid_date'=>'required',
        'note'=>'nullable',
        'timezone'=>'required',
        'trans_coa_detail_id'=>'required'
    ];

    public function getAp()
    {
        return $this->hasOne(MerchantAp::class,'id','acc_merchant_ap_id');
    }

    public function getCoa()
    {
        return $this->hasOne(CoaDetail::class,'id','trans_coa_detail_id');
    }


}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Accounting;


class MerchantShift extends BaseSennaAccModel
{
    protected $table='acc_merchant_shifts';
    protected $primaryKey='id';

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\Paypoin;


use App\Models\BaseModel;

class Transaction extends BaseModel
{
    protected $table='tp_paypoin_transactions';
    protected $primaryKey='id';

}

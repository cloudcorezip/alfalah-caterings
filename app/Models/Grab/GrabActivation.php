<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Grab;


class GrabActivation extends BaseGrabModel
{

    protected $table='md_merchant_grab_activations';
    protected $primaryKey='id';

}

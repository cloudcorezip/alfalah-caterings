<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Faq;


class FaqContent extends BaseFaqModel
{

    protected $table='faq_contents';
    protected $primaryKey='id';

    public $timestamps=true;

}
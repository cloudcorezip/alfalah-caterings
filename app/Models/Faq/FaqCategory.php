<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\Faq;


class FaqCategory extends BaseFaqModel
{

    protected $table='faq_categories';
    protected $primaryKey='id';

    public $timestamps=true;

}
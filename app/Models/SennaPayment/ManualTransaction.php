<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


use App\Models\MasterData\AvailableBank;
use App\Models\MasterData\User;

class ManualTransaction extends BaseSennaPaymentModel
{
    protected $table='sp_manual_transactions';
    protected $primaryKey='id';
    public $timestamps=true;


    public function transaction()
    {
        return $this->morphOne(Transaction::class,'transactionable');
    }

    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }

    public function getAvailableBank()
    {
        return $this->hasOne(AvailableBank::class,'md_bank_id','md_bank_id');
    }

}

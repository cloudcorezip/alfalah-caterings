<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


use App\Models\MasterData\SennaPayment\TransactionStatus;
use App\Models\MasterData\SennaPayment\TransactionType;
use App\Models\MasterData\User;

class Transaction extends BaseSennaPaymentModel
{

    protected $table='sp_transactions';
    protected $primaryKey='id';
    public $timestamps=true;

    public $fillable=['code','md_user_id','md_sp_transaction_type_id','md_sp_transaction_status_id','amount'
    ,'transaction_from','transaction_to',
        'admin_fee',
        'external_id',
        'expiration_date',
        'discount'
    ];

    const IN='in';
    const OUT='out';


    public $ruleManualIn=[
        'amount'=>'numeric|required',
        'bank_account_name'=>'required',
        'bank_name'=>'required',
        'no_rek'=>'required',
        'to_available_bank_id'=>'required|exists:md_banks,id',
        "evidence_of_transfer" => 'required|file|image|mimes:jpg,jpeg,bmp,png',
    ];

    public $ruleScan=[
        'key'=>'required',
        'type'=>'required',
        'from_user_id'=>'required',
    ];
    public $rulePay=[
        'key'=>'required',
        'type'=>'required',
        'from_user_id'=>'required',
        'amount'=>'numeric|required'
    ];

    public $ruleAcceptedManualIn=[
        'md_user_id_approval'=>'required',
        'manual_transfer_id'=>'required'
    ];


    public $ruleManualOut=[
        'amount'=>'numeric|required',
        'bank_account_name'=>'required',
        'bank_name'=>'required',
        'no_rek'=>'required',
        'md_bank_id'=>'required|exists:md_banks,id',
    ];

    public $ruleAcceptedManualOut=[
        'md_user_id_approval'=>'required',
        'manual_transfer_id'=>'required'
    ];




    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }

    public function transactionable()
    {
        return $this->morphTo();
    }

    public function getSettlement()
    {
        return $this->hasOne(SettlementTransaction::class,'sp_transaction_id','id');
    }

    public function scopeTransactionType($query,$transactionType)
    {
        $query->where('md_sp_transaction_type_id',$transactionType);
    }

    public function scopeTransactionStatus($query,$transctionStatus)
    {
        $query->where('md_sp_transaction_status_id',$transctionStatus);
    }

    public function getTransactionStatus()
    {
        return $this->hasOne(TransactionStatus::class,'id','md_sp_transaction_status_id');
    }

    public function getTransactionType()
    {
        return $this->hasOne(TransactionType::class,'id','md_sp_transaction_type_id');
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


use App\Models\MasterData\User;

class HistoryTransactionOut extends BaseSennaPaymentModel
{
    protected $table='sp_history_transaction_outs';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleManual=[
        'amount'=>'numeric|required',
        'bank_account_name'=>'required',
        'bank_name'=>'required',
        'no_rek'=>'required',
        'md_bank_id'=>'required|exists:md_banks,id',
    ];

    public $ruleAccepted=[
        'md_user_id_approval'=>'required',
        'manual_transfer_id'=>'required'
    ];

    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }

}

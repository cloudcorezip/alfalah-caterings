<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class BetweenTransaction extends BaseSennaPaymentModel
{
    protected $table='sp_between_transactions';
    protected $primaryKey='id';
    public $timestamps=true;


    public function transaction()
    {
        return $this->morphOne(Transaction::class,'transactionable');
    }

}

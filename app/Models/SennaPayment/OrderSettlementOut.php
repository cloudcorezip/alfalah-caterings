<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class OrderSettlementOut extends BaseSennaPaymentModel
{
    protected $table='sp_order_settlement_outs';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getOrderSettlement()
    {
        return $this->hasOne(OrderSettlement::class,'id','ss_order_settlement_id');
    }

    public function getHistoryTransactionIn()
    {
        return $this->hasOne(HistoryTransactionOut::class,'id','sp_history_transaction_out_id');
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class HistoryTransactionOutFromMerchant extends BaseSennaPaymentModel
{
    protected $table='sp_history_transaction_out_from_merchants';
    protected $primaryKey='id';
    public $timestamps=true;
}

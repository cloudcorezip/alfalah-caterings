<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\Plugin;

class PaymentPlugin extends BaseSennaPaymentModel
{
    protected $table='sp_payment_plugins';
    protected $primaryKey='id';
    public $timestamps=true;


    public $rule=[
        'md_merchant_id'=>'required|exists:md_merchants,id',
        'md_subscription_id'=>'required|exists:md_subscriptions,id',
        'md_transaction_type_id'=>'nullable'
    ];

    public function transaction()
    {
        return $this->morphOne(Transaction::class,'transactionable');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getPlugin()
    {
        return $this->hasOne(Plugin::class,'id','md_plugin_id');
    }


}

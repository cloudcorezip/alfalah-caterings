<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class OrderSettlementIn extends BaseSennaPaymentModel
{
    protected $table='sp_order_settlement_ins';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getOrderSettlement()
    {
        return $this->hasOne(OrderSettlement::class,'id','ss_order_settlement_id');
    }

    public function getHistoryTransactionIn()
    {
        return $this->hasOne(HistoryTransactionIn::class,'id','sp_history_transaction_in_id');
    }


}

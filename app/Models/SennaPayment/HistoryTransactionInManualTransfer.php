<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class HistoryTransactionInManualTransfer extends BaseSennaPaymentModel
{
    protected $table='sp_history_transaction_in_manual_transfer';
    protected $primaryKey='id';
    public $timestamps=true;
}

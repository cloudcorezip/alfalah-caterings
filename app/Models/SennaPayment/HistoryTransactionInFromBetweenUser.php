<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class HistoryTransactionInFromBetweenUser extends BaseSennaPaymentModel
{
    protected $table='sp_history_transaction_in_from_between_users';
    protected $primaryKey='id';
    public $timestamps=true;
}

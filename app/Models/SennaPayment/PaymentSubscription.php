<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\Subscription;

class PaymentSubscription extends BaseSennaPaymentModel
{
    protected $table='sp_payment_subscriptions';
    protected $primaryKey='id';
    public $timestamps=true;


    public $rule=[
        'md_merchant_id'=>'required|exists:md_merchants,id',
        'md_subscription_id'=>'required|exists:md_subscriptions,id',
        'md_transaction_type_id'=>'nullable',
        'amount'=>'numeric|required'
    ];

    public function transaction()
    {
        return $this->morphOne(Transaction::class,'transactionable');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getSubscription()
    {
        return $this->hasOne(Subscription::class,'id','md_subscription_id');
    }


}

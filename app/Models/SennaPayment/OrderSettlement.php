<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class OrderSettlement extends BaseSennaPaymentModel
{
    protected $table='sp_order_settlements';
    protected $primaryKey='id';
    public $timestamps=true;

    const STATUS_PENDING='pending';
    const STATUS_SUCCESS='success';
    const STATUS_REFUND='refund';
    const STATUS_CANCELED='canceled';

    public function orderable()
    {
        return $this->morphTo();
    }

}

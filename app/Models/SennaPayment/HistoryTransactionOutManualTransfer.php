<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class HistoryTransactionOutManualTransfer extends BaseSennaPaymentModel
{
    protected $table='sp_history_transaction_out_manual_transfer';
    protected $primaryKey='id';
    public $timestamps=true;

}

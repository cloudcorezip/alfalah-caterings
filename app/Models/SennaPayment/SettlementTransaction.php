<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


class SettlementTransaction extends BaseSennaPaymentModel
{
    protected $table='sp_settlement_transactions';
    protected $primaryKey='id';
    public $timestamps=true;

    const STATUS_PENDING='pending';
    const STATUS_SUCCESS='success';
    const STATUS_REFUND='refund';
    const STATUS_CANCELED='canceled';

    public function transaction()
    {
        return $this->morphOne(Transaction::class,'transactionable');
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaPayment;


use App\Models\MasterData\User;

class HistoryTransactionIn extends BaseSennaPaymentModel
{
    protected $table='sp_history_transaction_ins';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleManual=[
        'amount'=>'numeric|required',
        'bank_account_name'=>'required',
        'bank_name'=>'required',
        'no_rek'=>'required',
        'to_available_bank_id'=>'required|exists:md_banks,id',
        "evidence_of_transfer" => 'required|file|image|mimes:jpg,jpeg,bmp,png',
    ];

    public $ruleScan=[
        'key'=>'required',
        'type'=>'required',
        'from_user_id'=>'required',
    ];
    public $rulePay=[
        'key'=>'required',
        'type'=>'required',
        'from_user_id'=>'required',
        'amount'=>'numeric|required'
    ];

    public $ruleAccepted=[
        'md_user_id_approval'=>'required',
        'manual_transfer_id'=>'required'
    ];

    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }
}

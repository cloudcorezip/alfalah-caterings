<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaCashier;


class SupplierCategories extends BaseSennaCashierModel
{
    protected $table='sc_supplier_categories';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required'
    ];

}

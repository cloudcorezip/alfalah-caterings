<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaCashier;


class CashDrawer extends BaseSennaCashierModel
{
    protected $table='md_merchant_cashdrawers';
    protected $primaryKey='id';
    public $timestamps=true;


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaCashier;


class CustomerLevel extends BaseSennaCashierModel
{
    protected $table='sc_customer_level';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required'
    ];

}

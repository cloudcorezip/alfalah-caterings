<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ReviewSaleOrder extends BaseSennaTokoModel
{
    protected $table='sc_review_sale_orders';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleReview=[
        'rating'=>'numeric|required',
        'suggestions'=>'nullable'
    ];

    public function getSaleOrder()
    {
        return $this->hasOne(SaleOrder::class,'id','sc_sale_orders');
    }

}

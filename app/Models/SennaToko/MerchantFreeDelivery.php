<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class MerchantFreeDelivery extends BaseSennaTokoModel
{
    protected $table='sc_merchant_fd';
    protected $primaryKey='id';
    public $timestamps=true;


    public $create=[
        'start_available'=>'required',
        'end_available'=>'required',
        'max_distance'=>'numeric|required',
        'is_active'=>'required'
    ];


}

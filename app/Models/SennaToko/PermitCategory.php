<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class PermitCategory extends BaseSennaTokoModel
{
    protected $table='md_permit_categories';
    protected $primaryKey='id';
    public $timestamps=true;


}

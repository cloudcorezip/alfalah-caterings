<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\Accounting\MerchantAp;
use App\Models\Accounting\MerchantAr;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\MasterData\TransactionType;
use App\Models\MasterData\User;
use App\Models\SennaPayment\Transaction;
use Carbon\Carbon;

class PurchaseOrder extends BaseSennaTokoModel
{
    protected $table='sc_purchase_orders';
    protected $primaryKey='id';
    public $timestamps=true;


    public $ruleCreate=[
        'sc_supplier_id'=>'required',
        'total'=>'numeric|required',
        'paid_nominal'=>'numeric|required',
        'is_debet'=>'numeric|required',
        //'md_transaction_type_id'=>'required',
        //'tax'=>'numeric|required',
        // 'discount'=>'numeric|required',
        'timezone'=>'required',
        'change_nominal'=>'numeric|required'
    ];


    public $ruleCreateOffer=[
        'sc_supplier_id'=>'required',
        'total_offer'=>'numeric|required',
        'timezone'=>'required',
    ];


    public $ruleOfflineOrderDetail=[
        'sc_product_id'=>'required',
        'quantity'=>'numeric|required',
        'price'=>'numeric|required',
    ];



    public function stock()
    {
        return $this->morphMany(StockInventory::class,'stockable');
    }


    public function stockSingle()
    {
        return $this->morphOne(StockInventory::class,'stockablePurchase');
    }


    public function getDetail()
    {
        return $this->hasMany(PurchaseOrderDetail::class,'sc_purchase_order_id','id');
    }

    public function ap()
    {
        return $this->morphOne(MerchantAp::class,'apable');
    }

    public function getTransactionType()
    {
        return $this->hasOne(TransactionType::class,'id','md_transaction_type_id');
    }

    public function getTransactionStatus()
    {
        return $this->hasOne(TransactionStatus::class,'id','md_sc_transaction_status_id');
    }

    public function getSupplier()
    {
        return $this->hasOne(Supplier::class,'id','sc_supplier_id');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getRetur()
    {
        return $this->hasMany(ReturPurchaseOrder::class,'sc_purchase_order_id','id');
    }

    public function getWarehouse()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','inv_warehouse_id');
    }


    public function getOldStruck()
    {
        return $this->hasMany(TempPurchaseOrderDetail::class,'sc_purchase_order_id','id');
    }

    public function getAllRetur()
    {
        return $this->hasMany(ReturPurchaseOrder::class,'sc_purchase_order_id','id')
            ->where('is_retur',1)
            ->where('is_deleted',0)
            ->with(['getDetail'=>function($query){
                $query->select([
                    'sc_retur_purchase_order_details.*',
                    'p.name as product_name',
                    'p.foto',
                    'p.id as product_id',
                    'p.description',
                    'p.stock',
                    'p.selling_price',
                    'p.purchase_price',
                    'u.name as unit_name'
                ])->join('sc_products as p','p.id','sc_retur_purchase_order_details.sc_product_id')
                    ->join('md_units as u','u.id','p.md_unit_id');

            }])
            ;
    }

    public function getStaff()
    {
        return $this->hasOne(User::class,'id','created_by');
    }



}

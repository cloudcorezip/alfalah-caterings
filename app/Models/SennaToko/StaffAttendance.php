<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\User;

class StaffAttendance extends BaseSennaTokoModel
{
    protected $table='md_merchant_staff_attendances';
    protected $primaryKey='id';
    public $timestamps=true;


    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');

    }
    public function getStaff()
    {
        return $this->hasOne(User::class,'id','md_staff_user_id');
    }



}

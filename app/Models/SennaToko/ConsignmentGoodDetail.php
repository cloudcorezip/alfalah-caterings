<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko;


use App\Models\BaseModel;

class ConsignmentGoodDetail extends BaseModel
{
    protected $table='sc_consignment_good_details';
    protected $primaryKey='id';

    public function getConsignment()
    {
        return $this->hasOne(ConsignmentGood::class,'id','sc_consignment_good_id');
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko;


class TemplateDiagnosis extends BaseSennaTokoModel
{
    protected $table='md_merchant_template_diagnosis';
    protected $primaryKey='id';
    public $timestamps=true;

}

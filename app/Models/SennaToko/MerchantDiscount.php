<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class MerchantDiscount extends BaseSennaTokoModel
{
    protected $table='sc_merchant_discounts';
    protected $primaryKey='id';
    public $timestamps=true;


    public $create=[
        'name'=>'required',
        'percentage'=>'numeric|required',
    ];

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko;


use App\Models\BaseModel;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\MasterData\User;

class ConsignmentGood extends BaseModel
{
    protected $table='sc_consignment_goods';
    protected $primaryKey='id';

    public function getSupplier()
    {
        return $this->hasOne(Supplier::class,'id','sc_supplier_id');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getWarehouse()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','inv_warehouse_id');
    }

    public function getDetail()
    {
        return $this->hasMany(ConsignmentGoodDetail::class,'sc_consignment_good_id','id');
    }

    public function getCreated()
    {
        return $this->hasOne(User::class,'id','created_by');
    }
}

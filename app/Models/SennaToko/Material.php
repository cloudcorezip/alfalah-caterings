<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class Material extends BaseSennaTokoModel
{
    protected $table='sc_product_raw_materials';
    protected $primaryKey='id';

    public function getParent()
    {
        return $this->hasOne(Product::class,'id','parent_sc_product_id');

    }
    public function getChild()
    {
        return $this->hasOne(Product::class,'id','child_sc_product_id');

    }


}

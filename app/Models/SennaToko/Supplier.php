<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;

use App\Models\SennaCashier\SupplierCategories;

class Supplier extends BaseSennaTokoModel
{
    protected $table='sc_suppliers';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required',
        'email'=>'nullable',
        'phone_number'=>'required',
        'address'=>'nullable',
    ];

    public function getSupplierCategory()
    {
        return $this->hasOne(SupplierCategories::class,'id','sc_supplier_categories_id');
    }


}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\MerchantWarehouse;

class StockInventory extends BaseSennaTokoModel
{

    protected $table='sc_stock_inventories';
    protected $primaryKey='id';
    public $timestamps=true;

    public $fillable=[
        'sc_product_id',
        'total',
        'selling_price',
        'purchase_price',
        'type',
        'transaction_action',
        'record_stock',
        'created_by',
        'timezone',
        'residual_stock',
        'inv_warehouse_id',
        'sync_id',
        'created_at',
        'updated_at',
    ];

    public $rule=[
        'amount'=>'numeric|required',
        'created_at'=>'required',
        'type'=>'required',
        'timezone'=>'required'
    ];

    const IN='in';
    const OUT='out';
    const INIT='initial';

    public function stockable()
    {
        return $this->morphTo();
    }

    public function stockablePurchase()
    {
        return $this->morphTo();
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getWarehouse()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','inv_warehouse_id');
    }


}

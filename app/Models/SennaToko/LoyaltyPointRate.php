<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\User;

class LoyaltyPointRate extends BaseSennaTokoModel
{
    protected $table='crm_loyalty_point_rate_config';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
        "value_per_point" => "required"
    ];

}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\SennaToko;


class ProductionOfGoodDetail extends BaseSennaTokoModel
{
    protected $table='sc_inv_production_of_good_details';
    protected $primaryKey='id';

}

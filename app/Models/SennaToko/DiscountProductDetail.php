<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\User;

class DiscountProductDetail extends BaseSennaTokoModel
{
    protected $table='crm_discount_by_product_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
        'sc_product_id'=>'required',
    ];

    public function getProduct()
    {
       return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getDiscount()
    {
       return $this->hasOne(DiscountProduct::class,'id','discount_by_product_id');
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ReturSaleOrderDetail extends BaseSennaTokoModel
{
    protected $table='sc_retur_sale_order_details';
    protected $primaryKey='id';
    public $timestamps=true;


    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getPurchaseOrderDetail()
    {
        return $this->hasOne(SaleOrderDetail::class,'id','sc_sale_order_detail_id');
    }

    public function getSaleOrderDetail()
    {
        return $this->hasOne(SaleOrderDetail::class,'id','sc_sale_order_detail_id');
    }


}

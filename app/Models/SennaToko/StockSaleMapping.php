<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\SennaToko;


class StockSaleMapping extends BaseSennaTokoModel
{

    protected $table='sc_stock_sale_mappings';
    protected $primaryKey='id';

    public function getStockInventory()
    {
        return $this->hasOne(StockInventory::class,'id','sc_stock_inventory_id');
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }


}

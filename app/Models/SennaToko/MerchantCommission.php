<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko;


class MerchantCommission extends BaseSennaTokoModel
{
    protected $table='merchant_commissions';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
        "name" => "required",
        "type" => "required",
        "value_type" => "required",
        "value" => "required",
        "status" => "required",
        "min_transaction"=>"required",
        "type_of_value_commissions"=>'required',
    ];

    public $update=[
        "name" => "required",
        "type" => "required",
        "value_type" => "required",
        "status" => "required",
        "min_transaction"=>"required",
        "type_of_value_commissions"=>'required',
    ];

}

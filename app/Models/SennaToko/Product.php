<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\SennaCashier\Category;
use App\Models\MasterData\SennaCashier\ProductType;
use App\Models\MasterData\Unit;
use App\Models\MasterData\User;

class Product extends BaseSennaTokoModel
{
    protected $table='sc_products';
    protected $primaryKey='id';
    public $timestamps=true;


    public $ruleCreate=[
        'name'=>'required',
        'foto'=>'nullable|file|image|mimes:jpg,jpeg,bmp,png',
        'code'=>'required',
        'selling_price'=>'numeric|required',
        'purchase_price'=>'numeric|required',
        'stock'=>'numeric|required',
        'description'=>'nullable',
        'sc_merk_id'=>'nullable',
        'md_sc_product_type_id'=>'required',
        'sc_product_category_id'=>'required',
        //'is_show_senna_app'=>'required',
        'md_unit_id'=>'required',
        // 'md_sc_category_id'=>'required|exists:md_sc_categories,id',
        'is_with_stock'=>'required',
        'is_with_purchase_price'=>'required'
    ];
    public $ruleCreateWeb=[
        'name'=>'required',
        'foto'=>'nullable|file|image|mimes:jpg,jpeg,bmp,png',
        'code'=>'required',
        'selling_price'=>'required',
        'purchase_price'=>'required',
        //'stock'=>'numeric|required',
        'description'=>'nullable',
        'sc_merk_id'=>'nullable',
        'md_sc_product_type_id'=>'required',
        'sc_product_category_id'=>'required',
        //'is_show_senna_app'=>'required',
        'md_unit_id'=>'required',
        // 'md_sc_category_id'=>'required|exists:md_sc_categories,id',
        'is_with_stock'=>'required',
        //'is_with_purchase_price'=>'required'
    ];
    public $ruleStock=[
        'amount'=>'numeric|required',
        'type'=>'required',
        'timezone'=>'required'
    ];

    public $ruleStockWeb=[
        'amount'=>'numeric|required',
        'type'=>'required',
    ];

    public $ruleUpdate=[
        'name'=>'required',
        'code'=>'required',
        'selling_price'=>'numeric|required',
        //'purchase_price'=>'numeric|required',
        //'stock'=>'numeric|required',
        'description'=>'nullable',
        'sc_merk_id'=>'nullable',
        'md_sc_product_type_id'=>'required',
        'sc_product_category_id'=>'required',
        //'is_show_senna_app'=>'required',
        'md_unit_id'=>'required|exists:md_units,id',
        // 'md_sc_category_id'=>'required|exists:md_sc_categories,id',

    ];

    public $ruleUpdateJasa=[
        'name'=>'required',
        'code'=>'required',
        'selling_price'=>'numeric|required',
        'purchase_price'=>'numeric|required',
        'description'=>'nullable',
        'sc_merk_id'=>'nullable',
        'md_sc_product_type_id'=>'required',
        'sc_product_category_id'=>'required',
        //'is_show_senna_app'=>'required',
        'md_unit_id'=>'required|exists:md_units,id',
        'md_sc_category_id'=>'required|exists:md_sc_categories,id',

    ];

    public $ruleUpdateWeb=[
        'name'=>'required',
        'code'=>'required',
        'selling_price'=>'required',
        'description'=>'nullable',
        'sc_merk_id'=>'nullable',
        'md_sc_product_type_id'=>'required',
        'sc_product_category_id'=>'required',
        //'is_show_senna_app'=>'required',
        // 'md_sc_category_id'=>'required|exists:md_sc_categories,id',

    ];

    public function getMerk()
    {
        return $this->hasOne(Merk::class,'id','sc_merk_id');

    }

    public function getCategory()
    {
        return $this->hasOne(ProductCategory::class,'id','sc_product_category_id');
    }

    public function getGlobalCategory()
    {
        return $this->hasOne(Category::class,'id','md_sc_category_id');


    }

    public function getType()
    {
        return $this->hasOne(ProductType::class,'id','md_sc_product_type_id');
    }

    public function getUnit()
    {
        return $this->hasOne(Unit::class,'id','md_unit_id');
    }

    public function getPromoProduct()
    {
        return $this->hasOne(PromoProduct::class,'sc_product_id','id');
    }

    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }

    public function stock()
    {
        return $this->morphOne(StockInventory::class,'stockable');
    }

    public function getProductSellingLevel()
    {
        return $this->hasMany(ProductSellingLevel::class,'sc_product_id','id');
    }

    public function getMultiUnit()
    {
        return $this->hasMany(MultiUnit::class,'sc_product_id','id');
    }

    public function getPackageItem()
    {
        return $this->hasMany(Material::class,'parent_sc_product_id','id')->where('sc_product_raw_materials.is_deleted',0);
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;

use App\Models\SennaCashier\CustomerLevel;


class Customer extends BaseSennaTokoModel
{
    protected $table='sc_customers';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required',
        'email'=>'nullable',
        'phone_number'=>'required',
        'address'=>'nullable',
        'point'=>'numeric|required'
    ];

    public static function generateCustomerReligion()
    {
        $data = [
            "1" => "Islam",
            "2"=>"Katolik",
            "3" => "Kristen"
        ];

        return $data;
    }

    public static function generateMediaInfo()
    {
        $data = [
            "1" => "Media Sosial",
            "2"=>"Referensi"
        ];

        return $data;
    }

    public static function generateMaritalStatus()
    {
        $data = [
            "1" => "Belum Kawin",
            "2" => "Kawin",
            "3" => "Cerai Hidup",
            "4" => "Cerai Mati"
        ];

        return $data;
    }

    public function getUserCustomer()
    {
        return $this->hasOne(UserCustomer::class,'sc_customer_id','id');
    }

    public function getCustLevel()
    {
        return $this->hasOne(CustomerLevel::class,'id','sc_customer_level_id');
    }
}

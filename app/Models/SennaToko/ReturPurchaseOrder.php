<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ReturPurchaseOrder extends BaseSennaTokoModel
{
    protected $table='sc_retur_purchase_orders';
    protected $primaryKey='id';
    public $timestamps=true;


    public  $rule=[
        'created_by'=>'required',
        'note'=>'nullable',
        'total'=>'numeric|required',
        'details'=>'required|array',
        'timezone'=>'required',
        'reason_id'=>'required'

    ];

    public $ruleDetail=[
        'sc_product_id'=>'required|exists:sc_products,id',
        'sc_purchase_order_detail_id'=>'required|exists:sc_purchase_order_details,id',
        'type'=>'required',
        'quantity'=>'numeric|required',
        'sub_total'=>'numeric|required',
        'price'=>'numeric|required'
    ];

    public $ruleDetailAdd=[
        'sc_product_id'=>'required|exists:sc_products,id',
        'quantity'=>'numeric|required',
        'sub_total'=>'numeric|required',
        'price'=>'numeric|required'
    ];

    public function getPurchaseOrder()
    {
        return $this->hasOne(PurchaseOrder::class,'id','sc_purchase_order_id');
    }

    public function getDetail()
    {
        return $this->hasMany(ReturPurchaseOrderDetail::class,'sc_retur_purchase_order_id','id');
    }

    public function stock()
    {
        return $this->morphOne(StockInventory::class,'stockable');
    }


}

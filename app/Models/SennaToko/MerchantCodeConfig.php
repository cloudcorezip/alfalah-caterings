<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko;


class MerchantCodeConfig extends BaseSennaTokoModel
{
    protected $table='merchant_transaction_code_configs';
    protected $primaryKey='id';
    public $timestamps=true;

    public static function getList()
    {
        $data = [
            // penjualan
            (object)[
                "id" => "penawaran-penjualan",
                "title" => "Penawaran Penjualan",
                "type" => "sale",
                "sub_type" => "sale-offer"
            ],
            (object)[
                "id" => "pemesanan-penjualan",
                "title" => "Pemesanan Penjualan",
                "type" => "sale",
                "sub_type" => "sale-order"
            ],
            (object)[
                "id" => "pengiriman-penjualan",
                "title" => "Pengiriman Penjualan",
                "type" => "sale",
                "sub_type" => "sale-delivery"
            ],
            (object)[
                "id" => "pembayaran-penjualan",
                "title" => "Pembayaran Penjualan",
                "type" => "sale",
                "sub_type" => "sale-faktur"
            ],
            (object)[
                "id" => "retur-penjualan",
                "title" => "Retur Penjualan",
                "type" => "sale",
                "sub_type" => "sale-retur"
            ],
            (object)[
                "id" => "pembayaran-piutang-penjualan",
                "title" => "Pembayaran Piutang Penjualan",
                "type" => "sale",
                "sub_type" => "sale-pay-ar"
            ],

            // pembelian
            (object)[
                "id" => "penawaran-pembelian",
                "title" => "Penawaran Pembelian",
                "type" => "purchase",
                "sub_type" => "purchase-offer"
            ],
            (object)[
                "id" => "pemesanan-pembelian",
                "title" => "Pemesanan Pembelian",
                "type" => "purchase",
                "sub_type" => "purchase-order"
            ],
            (object)[
                "id" => "pengiriman-pembelian",
                "title" => "Pengiriman Pembelian",
                "type" => "purchase",
                "sub_type" => "purchase-delivery"
            ],
            (object)[
                "id" => "pembayaran-pembelian",
                "title" => "Pembayaran Pembelian",
                "type" => "purchase",
                "sub_type" => "purchase-faktur"
            ],
            (object)[
                "id" => "retur-pembelian",
                "title" => "Retur Pembelian",
                "type" => "purchase",
                "sub_type" => "purchase-retur"
            ],
            (object)[
                "id" => "pembayaran-hutang-pembelian",
                "title" => "Pembayaran Hutang Pembelian",
                "type" => "purchase",
                "sub_type" => "purchase-pay-ap"
            ],

            // stock
            (object)[
                "id" => "opname-stock",
                "title" => "Stock Opname",
                "type" => "stock",
                "sub_type" => "stock-opname"
            ],
            (object)[
                "id" => "adjustment-stock",
                "title" => "Penyesuaian Stock",
                "type" => "stock",
                "sub_type" => "stock-adjustment"
            ],
            (object)[
                "id" => "mutation-stock",
                "title" => "Mutasi Stok",
                "type" => "stock",
                "sub_type" => "stock-mutation"
            ],
            (object)[
                "id" => "production-product-stock",
                "title" => "Produksi Produk",
                "type" => "stock",
                "sub_type" => "stock-production-product"
            ],
            (object)[
                "id" => "consignment-stock",
                "title" => "Penerimaan Barang Konsinyasi",
                "type" => "stock",
                "sub_type" => "stock-consignment"
            ],

            // master data
            (object)[
                "id" => "product-master-data",
                "title" => "Produk",
                "type" => "master-data",
                "sub_type" => "master-data-product"
            ],
            (object)[
                "id" => "customer-master-data",
                "title" => "Pelanggan",
                "type" => "master-data",
                "sub_type" => "master-data-customer"
            ],
            (object)[
                "id" => "supplier-master-data",
                "title" => "Supplier",
                "type" => "master-data",
                "sub_type" => "master-data-supplier"
            ],
            (object)[
                "id" => "staff-master-data",
                "title" => "Karyawan",
                "type" => "master-data",
                "sub_type" => "master-data-staff"
            ],

            // asset
            (object)[
                "id" => "asset",
                "title" => "Aset",
                "type" => "asset",
                "sub_type" => "asset"
            ],
            (object)[
                "id" => "sale-asset",
                "title" => "Penjualan",
                "type" => "asset",
                "sub_type" => "asset-sale"
            ],

            // acc
            (object)[
                "id" => "journal-acc",
                "title" => "Jurnal Umum",
                "type" => "acc",
                "sub_type" => "acc-journal"
            ],
            (object)[
                "id" => "cash-bank-acc",
                "title" => "Kas & Bank",
                "type" => "acc",
                "sub_type" => "acc-cash-bank"
            ],
            (object)[
                "id" => "income-acc",
                "title" => "Pendapatan",
                "type" => "acc",
                "sub_type" => "acc-income"
            ],
            (object)[
                "id" => "cost-acc",
                "title" => "Biaya",
                "type" => "acc",
                "sub_type" => "acc-cost"
            ],
            (object)[
                "id" => "ap-acc",
                "title" => "Utang",
                "type" => "acc",
                "sub_type" => "acc-ap"
            ],
            (object)[
                "id" => "ar-acc",
                "title" => "Piutang",
                "type" => "acc",
                "sub_type" => "acc-ar"
            ],
            (object)[
                "id" => "closing-journal-acc",
                "title" => "Jurnal Penutup",
                "type" => "acc",
                "sub_type" => "acc-closing-journal"
            ]
        ];
        
        return collect($data);
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class Merk extends BaseSennaTokoModel
{
    protected $table='sc_merks';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required'
    ];

}

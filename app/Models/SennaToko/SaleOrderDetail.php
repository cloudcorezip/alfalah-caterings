<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use Illuminate\Support\Facades\DB;

class SaleOrderDetail extends BaseSennaTokoModel
{
    protected $table='sc_sale_order_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public $ruleReduceOrAdd=[
        'quantity'=>'numeric|required',
    ];
    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getOrder()
    {
        return $this->hasOne(SaleOrder::class,'id','sc_sale_order_id');
    }

    public function getDetailPromo()
    {
        return $this->hasOne(DiscountProduct::class,'id','ref_discount_id');
    }

    public function getOneMultiUnit()
    {
        return $this->hasOne(MultiUnit::class,'id','multi_unit_id');
    }


    public function  getReturAmount($stepType,$d)
    {
        try{
            $dataRetur=DB::table('sc_retur_sale_order_details')
                ->join('sc_retur_sale_orders as c','c.id','sc_retur_sale_order_id')
                ->where('sc_sale_order_detail_id',$d->id)
                ->where('c.step_type',$stepType)
                ->where(function($query){
                    $query->where('c.reason_id',2)
                        ->orWhere('c.reason_id',3);
                })
                ->where('c.is_deleted',0)->get();

            $sumSubRetur=0;

            foreach ($dataRetur as $dr){
                if($dr->is_multi_unit==0){
                    $sumSubRetur+=$dr->quantity;
                }else{
                    if($dr->value_conversation==0){
                        $sumSubRetur+=$dr->multi_quantity;
                    }else{
                        $sumSubRetur+=($dr->quantity*$dr->value_conversation);
                    }
                }
            }
            if($d->is_multi_unit==0){
                return $sumSubRetur;
            }else{
                return $sumSubRetur/$d->value_conversation;
            }



        }catch (\Exception $e)
        {
            return 0;
        }

    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class TransferStockDetail extends BaseSennaTokoModel
{
    protected $table='sc_transfer_stock_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ReturSaleOrder extends BaseSennaTokoModel
{
    protected $table='sc_retur_sale_orders';
    protected $primaryKey='id';
    public $timestamps=true;

    public  $rule=[
        'created_by'=>'required',
        'note'=>'nullable',
        'total'=>'numeric|required',
        'details'=>'required|array',
        'timezone'=>'required'
    ];

    public $ruleDetail=[
        'sc_product_id'=>'required|exists:sc_products,id',
        'sc_sale_order_detail_id'=>'required|exists:sc_sale_order_details,id',
        'type'=>'required',
        'quantity'=>'numeric|required',
        'sub_total'=>'numeric|required',
        'price'=>'numeric|required'
    ];

    public $ruleDetailAdd=[
        'sc_product_id'=>'required|exists:sc_products,id',
        'quantity'=>'numeric|required',
        'sub_total'=>'numeric|required',
        'price'=>'numeric|required'
    ];

    public function getSaleOrder()
    {
        return $this->hasOne(SaleOrder::class,'id','sc_sale_order_id');
    }

    public function stock()
    {
        return $this->morphOne(StockInventory::class,'stockable');
    }

    public function getDetail()
    {
        return $this->hasMany(ReturSaleOrderDetail::class,'sc_retur_sale_order_id','id');
    }

}

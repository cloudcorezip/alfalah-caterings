<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;
use App\Models\SennaToko\Customer;
use App\Models\MasterData\MerchantStaff;
use App\Models\MasterData\Merchant;
use App\Models\SennaToko\Product;


class ClinicReservation extends BaseSennaTokoModel
{
    protected $table='clinic_reservations';
    protected $primaryKey='id';

    public $ruleCreate=[
        'sc_customer_id'=>'required',
        'md_merchant_staff_id'=>'required',
        'sc_product_id'=>'required',
        'reservation_date'=>'required'
    ];

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id', 'md_merchant_id');
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::class,'id', 'sc_customer_id');
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getStaff()
    {
        return $this->hasOne(MerchantStaff::class,'id','md_merchant_staff_id');
    }
}

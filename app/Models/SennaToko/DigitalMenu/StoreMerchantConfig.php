<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\SennaToko\DigitalMenu;


use App\Models\SennaToko\BaseSennaTokoModel;

class StoreMerchantConfig extends BaseSennaTokoModel
{
    protected $table='store_merchant_configs';
    protected $primaryKey='id';
    public $timestamps=true;
}

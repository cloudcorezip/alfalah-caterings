<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\SennaToko\DigitalMenu;


use App\Models\SennaToko\BaseSennaTokoModel;

class StoreMerchantDetail extends BaseSennaTokoModel
{
    protected $table='store_merchant_details';
    protected $primaryKey='id';
    public $timestamps=true;
}

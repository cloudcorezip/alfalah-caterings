<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;
use App\Models\MasterData\Unit;

class MultiUnit extends BaseSennaTokoModel
{
    protected $table='sc_product_multi_units';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getUnit()
    {
        return $this->hasOne(Unit::class,'id','md_unit_id');
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\User;

class DiscountCoupon extends BaseSennaTokoModel
{
    protected $table='crm_discount_coupons';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
        'name'=>'required',
        'code'=>'required',
        'bonus_type'=>'required',
        'bonus_value'=>'required'
    ];

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');

    }

}

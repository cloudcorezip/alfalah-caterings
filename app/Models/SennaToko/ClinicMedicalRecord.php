<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;
use App\Models\SennaToko\ClinicReservation;


class ClinicMedicalRecord extends BaseSennaTokoModel
{
    protected $table='clinic_medical_records';
    protected $primaryKey='id';

    public function getReservation()
    {
        return $this->hasOne(ClinicReservation::class,'id','clinic_reservation_id');
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;
use App\Models\MasterData\Varian;

class MultiVarian extends BaseSennaTokoModel
{
    protected $table='sc_product_multi_varians';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

}
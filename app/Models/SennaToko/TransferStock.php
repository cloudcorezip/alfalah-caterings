<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantWarehouse;

class TransferStock extends BaseSennaTokoModel
{
    protected $table='sc_transfer_stocks';
    protected $primaryKey='id';
    public $timestamps=true;


    public $rule=[
        'to_merchant_id'=>'required|exists:md_merchants,id',
        'total'=>'numeric|required',
        'details'=>'required|array',
        'timezone'=>'required'


    ];

    public $ruleCreate=[
        'sc_product_id'=>'required|exists:sc_products,id',
        'quantity'=>'numeric|required',
        'purchase_price'=>'numeric|required',
        'sub_total'=>'numeric|required'
    ];

    public function getHeadMerchant()
    {
        return $this->hasOne(Merchant::class,'id','from_merchant_id');

    }

    public function getBranchMerchant()
    {
        return $this->hasOne(Merchant::class,'id','to_merchant_id');
    }

    public function getFromWarehouse()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','inv_warehouse_id');
    }
    public function getToWarehouse()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','to_inv_warehouse_id');
    }
    public function stock()
    {
        return $this->morphOne(StockInventory::class,'stockable');
    }

    public function getDetail()
    {
        return $this->hasMany(TransferStockDetail::class,'sc_transfer_stock_id','id');
    }
}

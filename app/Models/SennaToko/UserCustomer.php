<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\User;

class UserCustomer extends BaseSennaTokoModel
{
    protected $table='sc_user_customers';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getUser()
    {
        return $this->hasOne(User::class,'id','from_user_id');
    }


}

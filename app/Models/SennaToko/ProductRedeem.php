<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ProductRedeem extends BaseSennaTokoModel
{
    protected $table='sc_product_redeems';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }
    
    public function getSale()
    {
        return $this->hasOne(SaleOrder::class,'id','sc_sale_order_id');
    }

    public function getDetail()
    {
        return $this->hasMany(ProductRedeemDetail::class,'sc_redeem_id','id');
    }
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\MerchantWarehouse;

class ProductionOfGood extends BaseSennaTokoModel
{
    protected $table='sc_inv_production_of_goods';
    protected $primaryKey='id';

    public $ruleProduction=[
        'sc_product_id'=>'required',
        'inv_warehouse_id'=>'required',
        'time_of_production'=>'required',
        'amount'=>'required'
    ];

    public $ruleCreateProductFirst=[
        'inv_warehouse_id'=>'required',
        'time_of_production'=>'required',
        'amount'=>'required'
    ];

    public function getDetail()
    {
        return $this->hasMany(ProductionOfGoodDetail::class,'sc_inv_production_of_good_id','id');
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getWarehouse()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','inv_warehouse_id');
    }

    public function getBom()
    {
        return $this->hasMany(ProductionOfGoodBom::class,'sc_inv_production_of_good_id','id');
    }

}

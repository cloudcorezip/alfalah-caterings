<?php
/*
 * Senna Apps
 * Copyright (c) 2021.
 */

namespace App\Models\SennaToko;


use App\Models\Accounting\CoaDetail;

class ProductionOfGoodBom extends BaseSennaTokoModel
{
    protected $table='sc_inv_production_of_good_boms';
    protected $primaryKey='id';

    public function getCoa()
    {
        return $this->hasOne(CoaDetail::class,'id','trans_coa_detail_id');
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ProductSpecialPrice extends BaseSennaTokoModel
{
    protected $table='sc_product_special_prices';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
        'name'=>'required'
    ];
}

<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko\Sfa;


use Illuminate\Database\Eloquent\Model;

class SfaConfig extends Model
{
    protected $table='sfa_merchant_configs';
    protected $primaryKey='id';
    public $timestamps=true;
}

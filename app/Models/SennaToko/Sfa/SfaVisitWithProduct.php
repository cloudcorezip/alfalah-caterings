<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko\Sfa;


use App\Models\BaseModel;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\SennaToko\Product;

class SfaVisitWithProduct extends BaseModel
{

    protected $table='sfa_visit_with_products';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getWarehouseOut()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','out_warehouse_id');
    }

    public function getWarehouseIn()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','in_warehouse_id');
    }


}

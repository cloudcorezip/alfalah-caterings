<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko\Sfa;


use App\Models\BaseModel;
use App\Models\MasterData\User;

class SfaVisit extends BaseModel
{
    protected $table='sfa_kpi_visits';
    protected $primaryKey='id';
    public $timestamps=true;

    public function  getStaffUser()
    {
        return $this->hasOne(User::class,'id','staff_user_id');
    }
}

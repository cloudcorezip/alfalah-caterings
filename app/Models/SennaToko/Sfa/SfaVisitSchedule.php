<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko\Sfa;


use App\Models\BaseModel;
use App\Models\MasterData\User;
use App\Models\SennaToko\Customer;

class SfaVisitSchedule extends BaseModel
{
    protected $table='sfa_visit_schedules';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getCustomer()
    {
        return $this->hasOne(Customer::class,'id','sc_customer_id');
    }

    public function  getStaffUser()
    {
        return $this->hasOne(User::class,'id','staff_user_id');
    }
}

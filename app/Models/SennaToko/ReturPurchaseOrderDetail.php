<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ReturPurchaseOrderDetail extends BaseSennaTokoModel
{
    protected $table='sc_retur_purchase_order_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public function getPurchaseOrderDetail()
    {
        return $this->hasOne(PurchaseOrderDetail::class,'id','sc_purchase_order_detail_id');
    }
}

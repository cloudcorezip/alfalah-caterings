<?php
/*
 * Senna Apps
 * Copyright (c) 2022.
 */

namespace App\Models\SennaToko;


use Illuminate\Database\Eloquent\Model;

class MerchantCommissionList extends Model
{
    protected $table='merchant_commission_lists';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getSaleOrder()
    {
        return $this->hasOne(SaleOrder::class,'id','sc_sale_order_id');
    }

}

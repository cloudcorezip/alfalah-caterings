<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\Accounting\MerchantAr;
use App\Models\MasterData\Merchant;
use App\Models\MasterData\SennaCashier\OrderStatus;
use App\Models\MasterData\SennaCashier\ShippingCategory;
use App\Models\MasterData\SennaCashier\TransactionStatus;
use App\Models\MasterData\TransactionType;
use App\Models\MasterData\User;
use App\Models\SennaDriver\ReviewOrder;
use App\Models\SennaPayment\Transaction;
use App\Utils\Order\ShippingCategoryUtil;
use Illuminate\Support\Facades\DB;

class SaleOrder extends BaseSennaTokoModel
{
    protected $table='sc_sale_orders';
    protected $primaryKey='id';

    const APPPROVED_SHOP=1;
    const CANCELED_SHOP=2;
    const CANCELED_USER=1;
    const APPROVED_DRIVER=1;
    protected $casts = [
        'other_information' => 'array',
    ];

    public $ruleOrder=[
        'sc_product_id'=>'required|exists:sc_products,id',
        'quantity'=>'numeric|required',
        'merchant_id'=>'required|exists:md_merchants,id',
        'is_type'=>'required'
    ];

    public $ruleDriver=[
        'md_user_id_driver'=>'required|exists:md_users,id'
    ];



    public $ruleOfflineOrder=[
        'sc_customer_id'=>'nullable',
        'md_sc_shipping_category_id'=>'required',
        'total'=>'numeric|required',
        'destination'=>'nullable',
        'weight'=>'numeric|required',
        'shipping_cost'=>'numeric|required',
        'details'=>'required',
        'promo'=>'numeric|required',
        'distance'=>'numeric|required',
        'courier_name'=>'nullable',
        'service_courier_name'=>'nullable',
        'queue_code'=>'nullable',
        'is_debet'=>'numeric|required',
        'paid_nominal'=>'numeric|required',
        'due_date'=>'nullable',
        'tax'=>'numeric|required',
        'timezone'=>'required'

    ];

    public $rulePreOrder=[
        'sc_customer_id'=>'required',
        'total'=>'numeric|required',
        'details'=>'required',
        'promo'=>'numeric|required',
        'tax'=>'numeric|required',
        'promo_percentage'=>'numeric|required',
        'tax_percentage'=>'numeric|required',
        'timezone'=>'required',
        'user_helpers'=>'required'
    ];

    public $ruleInvoiceSales=[
        'sc_customer_id'=>'required',
        'total'=>'numeric|required',
        'details'=>'required',
        'promo'=>'numeric|required',
        'tax'=>'numeric|required',
        'promo_percentage'=>'numeric|required',
        'tax_percentage'=>'numeric|required',
        'timezone'=>'required',
        'user_helpers'=>'required'
    ];

    public $ruleOfflineOrderDetail=[
        'sc_product_id'=>'required|exists:sc_products,id',
        'quantity'=>'numeric|required',
        'price'=>'numeric|required',
    ];


    public $resiNumber=[
        'resi_code'=>'required'
    ];

    public $rulePay=[
        'md_sc_shipping_category_id'=>'required',
        'md_transaction_type_id'=>'required',
        'latitude'=>'required',
        'longitude'=>'required',
        'total'=>'numeric|required',
        'destination'=>'required',
        'weight'=>'required',
        'shipping_cost'=>'numeric|required',
        'distance'=>'numeric|required',
        'timezone'=>'required'
    ];

    public $ruleCourier=[
        'courier_name'=>'required',
        'service_courier_name'=>'required',
    ];
    public function stock()
    {
        return $this->morphOne(StockInventory::class,'stockable');
    }
    public function getDetail()
    {
        return $this->hasMany(SaleOrderDetail::class,'sc_sale_order_id','id');

    }

    public function getTransactionStatus()
    {
        return $this->hasOne(TransactionStatus::class,'id','md_sc_transaction_status_id');
    }

    public function getTransactionType()
    {
        return $this->hasOne(TransactionType::class,'id','md_transaction_type_id');
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::class,'id','sc_customer_id');
    }


    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function transaction()
    {
        return $this->morphOne(Transaction::class,'transactionable');
    }

    public function ar()
    {
        return $this->morphOne(MerchantAr::class,'arable');
    }

    public function orderable()
    {
        return $this->morphOne(ReviewOrder::class,'orderable');
    }

    public function getShippingCategory()
    {
        return $this->hasOne(ShippingCategory::class,'id','md_sc_shipping_category_id');
    }

    public function getOrderStatus()
    {
        return $this->hasOne(OrderStatus::class,'id','md_sc_order_status_id');
    }

    public function scopeOrderStatus($query,$status)
    {
        $query->where('md_sc_order_status_id',$status);

    }
    public function scopeIsRating($query,$isRating)
    {
        $query->where('is_rating',$isRating);

    }

    public function scopeIsFromSennaApp($query,$isFromSennaApp)
    {
        $query->where('is_from_senna_app',$isFromSennaApp);

    }

    public function getUserDriver()
    {
        return $this->hasOne(User::class,'id','md_user_id_driver');
    }

    public static function getListOrderStatus($orderStatusId,$shippingCategoryId)
    {
        $data=
            OrderStatus::whereNull('md_sc_shipping_category_id')
            ->orWhere('md_sc_shipping_category_id',ShippingCategoryUtil::checkShippingCategory($shippingCategoryId))
            ->orderBy('id','ASC')
            ->get();
        $status=[];

        foreach ($data as $key =>$item)
        {
            if($orderStatusId>OrderStatus::ORDER_DONE)
            {
                $status[]=[
                    'id'=>$item->id,
                    'name'=>$item->name,
                    'status'=>($item->id==$orderStatusId)?true:false
                ];
            }else{
                $status[]=[
                    'id'=>$item->id,
                    'name'=>$item->name,
                    'status'=>($item->id<=$orderStatusId)?true:false
                ];
            }

        }

        return $status;

    }

    public function getDriverDetail($orderId)
    {
        return SaleOrder::select([
            'u.id',
            'u.fullname',
            'u.foto',
            'd.police_number',
            'd.merk',
            DB::raw("coalesce((SELECT (sum(dr.rating)/count(*))  FROM dv_review_orders dr where dr.md_user_id_driver=u.id),0) as rating")
        ])
        ->join('md_users as u','u.id','sc_sale_orders.md_user_id_driver')
            ->join('md_user_driver_details as d','u.id','d.md_user_id_driver')
            ->where('sc_sale_orders.id',$orderId)
            ->first();
    }

    public function getOldStruck()
    {
        return $this->hasMany(TempSaleOrderDetail::class,'sc_sale_order_id','id');
    }

    public function getAllRetur()
    {
        return $this->hasMany(ReturSaleOrder::class,'sc_sale_order_id','id')
            ->where('is_retur',1)
            ->where('is_deleted',0)
            ->with(['getDetail'=>function($query){
                $query->select([
                    'sc_retur_sale_order_details.*',
                    'p.name as product_name',
                    'p.foto',
                    'p.id as product_id',
                    'p.description',
                    'p.stock',
                    'p.selling_price',
                    'p.purchase_price',
                    'u.name as unit_name'
                ])
                    ->join('sc_products as p','p.id','sc_retur_sale_order_details.sc_product_id')
                    ->join('md_units as u','u.id','p.md_unit_id');

            }])
            ;
    }

    public function getStaff()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

    public function getStockMapping()
    {
        return $this->hasMany(StockSaleMapping::class,'sc_sale_order_id','id');
    }
    public function getRetur()
    {
        return $this->hasMany(ReturSaleOrder::class,'sc_sale_order_id','id');
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\MerchantWarehouse;
use App\Models\MasterData\User;

class StockOpname extends BaseSennaTokoModel
{
    protected $table='sc_stock_opnames';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getDetail()
    {
        return $this->hasMany(StockOpnameDetail::class,'sc_stock_opname_id','id');
    }

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getUserCreated()
    {
        return $this->hasOne(User::class,'id','md_user_id_created');
    }

    public function stock()
    {
        return $this->morphOne(StockInventory::class,'stockable');
    }

    public function getWarehouse()
    {
        return $this->hasOne(MerchantWarehouse::class,'id','inv_warehouse_id');
    }


}

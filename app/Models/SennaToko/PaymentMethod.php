<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class PaymentMethod extends BaseSennaTokoModel
{
    protected $table='md_merchant_payment_methods';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'name'=>'required'
    ];

}

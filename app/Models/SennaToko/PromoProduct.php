<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class PromoProduct extends BaseSennaTokoModel
{
    protected $table='sc_promo_products';
    protected $primaryKey='id';
    public $timestamps=true;


    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

    public  $ruleCreate=[
        'sc_product_id'=>'required|exists:sc_products,id',
        'promo_percentage'=>'numeric|required',
        'price_after_promo'=>'numeric|required',
        'start_date'=>'date|required',
        'end_date'=>'date|required',
        'is_active'=>'required',

    ];


}

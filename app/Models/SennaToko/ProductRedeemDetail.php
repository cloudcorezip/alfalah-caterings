<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ProductRedeemDetail extends BaseSennaTokoModel
{
    protected $table='sc_product_redeem_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getRedeem()
    {
        return $this->hasOne(ProductRedeem::class,'id','sc_redeem_id');
    }
    
    
}

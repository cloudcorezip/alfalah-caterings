<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class TempSaleOrderDetail extends BaseSennaTokoModel
{
    protected $table='sc_temp_sale_order_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getOrder()
    {
        return $this->hasOne(SaleOrder::class,'id','sc_sale_order_id');
    }

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class TempPurchaseOrderDetail extends BaseSennaTokoModel
{
    protected $table='sc_temp_purchase_order_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getOrder()
    {
        return $this->hasOne(PurchaseOrder::class,'id','sc_purchase_order_id');
    }
}

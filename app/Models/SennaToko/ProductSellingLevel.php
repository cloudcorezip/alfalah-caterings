<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class ProductSellingLevel extends BaseSennaTokoModel
{
    protected $table='sc_product_selling_levels';
    protected $primaryKey='id';
    public $timestamps=true;

    public $rule=[
        'level_name'=>'required',
        'sc_product_id'=>'required|exists:sc_products,id',
        'selling_price'=>'numeric|required'
    ];
    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }


}

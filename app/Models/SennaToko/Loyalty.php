<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\User;

class Loyalty extends BaseSennaTokoModel
{
    protected $table='crm_loyalty_points';
    protected $primaryKey='id';
    public $timestamps=true;

    public $createMethodOne=[
        'get_point_method'=>'required',
        'min_transaction' => 'required',
        'point_earned' => 'required'
    ];

    public $createMethodTwo=[
        'get_point_method'=>'required',
        'sc_product_id' => 'required',
        'point_earned' => 'required'
    ];

    public function getProduct()
    {
        return $this->hasOne(Product::class,'id','sc_product_id');
    }

}

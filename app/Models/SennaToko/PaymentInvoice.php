<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


class PaymentInvoice extends BaseSennaTokoModel
{
    protected $table='md_merchant_payment_invoices';
    protected $primaryKey='id';
    public $timestamps=true;

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\SennaToko;


use App\Models\MasterData\Merchant;
use App\Models\MasterData\User;

class DiscountProduct extends BaseSennaTokoModel
{
    protected $table='crm_discount_by_products';
    protected $primaryKey='id';
    public $timestamps=true;

    public $create=[
        'name'=>'required',
        'day'=>'required',
        'activation_type'=>'required',
        'bonus_type'=>'required',
        'bonus_value'=>'required',
        'promo_type'=>'required',
        'promo_value'=>'required',
    ];

    public $createReferral=[
        'code'=>'required',
        'activation_type'=>'required'
    ];

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');

    }
    public function getDetail()
    {
        return $this->hasMany(DiscountProductDetail::class,'discount_by_product_id','id');

    }

    public function getAffiliator()
    {
        return $this->hasOne(User::class,'id','staff_user_id');
    }

    public function getPromo()
    {
        return $this->hasOne(DiscountProduct::class,'id','promo_id');
    }

    public function getCommission()
    {
        return $this->hasOne(MerchantCommission::class,'id','commission_id');
    }



}

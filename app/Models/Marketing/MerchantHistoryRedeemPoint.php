<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


use App\Models\SennaPayment\Transaction;

class MerchantHistoryRedeemPoint extends BaseMarketingModel
{
    protected $table='sm_merchant_history_redeem_points';
    protected $primaryKey='id';
    public $timestamps=true;

    public function transaction()
    {
        return $this->morphOne(Transaction::class,'transactionable');
    }

}

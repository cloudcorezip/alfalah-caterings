<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


class MerchantRedeemPoint extends BaseMarketingModel
{
    protected $table='sm_merchant_redeem_points';
    protected $primaryKey='id';
    public $timestamps=true;

    public $fillable=[

    ];


    public function redeemable()
    {
        return $this->morphTo();
    }
}

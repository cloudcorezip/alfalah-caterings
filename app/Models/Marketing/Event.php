<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


class Event extends BaseMarketingModel
{
    protected $table='mrkt_events';
    protected $primaryKey='id';
    public $timestamps=true;
    
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


use App\Models\MasterData\Merchant;

class MerchantMembershipDetail extends BaseMarketingModel
{
    protected $table='sm_merchant_membership_details';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getMemberMerchant()
    {
        return $this->hasOne(Merchant::class,'id','member_merchant_id');
    }

}

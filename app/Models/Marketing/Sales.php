<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


use App\Models\MasterData\Region\City;
use App\Models\MasterData\User;

class Sales extends BaseMarketingModel
{
    protected $table='sm_sales';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getCity()
    {
        return $this->hasOne(City::class,'id','regional_city_id');
    }

    public function getUser()
    {
        return $this->hasOne(User::class,'id','md_user_id');
    }

}

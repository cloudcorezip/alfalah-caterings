<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


use App\Models\MasterData\Subscription;
use Illuminate\Database\Eloquent\Model;

class MerchantCommission extends BaseMarketingModel
{
    protected $table='sm_merchant_commisions';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getSubscription()
    {
        return $this->hasOne(Subscription::class,'id','md_subscription_id');
    }


}

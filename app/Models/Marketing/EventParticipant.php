<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


class EventParticipant extends BaseMarketingModel
{
    protected $table='mrkt_event_participants';
    protected $primaryKey='id';
    public $timestamps=true;
    
}
<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


class MerchantPromo extends BaseMarketingModel
{
    protected $table='mrkt_promo_merchants';
    protected $primaryKey='id';
    public $timestamps=true;

    protected $casts = [
        'content' => 'array',
    ];

}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


class Promo extends BaseMarketingModel
{
    protected $table='mrkt_promos';
    protected $primaryKey='id';
    public $timestamps=true;

    protected $casts = [
        'content' => 'array',
    ];

    const DOORPRIZE=1;
    const DISCOUNT_SUBSCRIPTION=2;


    public static function getListKey($key)
    {
        $array=[
            1=>'Doorprize',
            2=>'Diskon Berlangganan'
        ];

        return $array[$key];
    }

    public function getMerchantPromo()
    {
        return $this->hasMany(MerchantPromo::class,'mrkt_promo_id','id');
    }
}

<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Models\Marketing;


use App\Models\MasterData\Merchant;

class MerchantMembership extends BaseMarketingModel
{
    protected $table='sm_merchant_memberships';
    protected $primaryKey='id';
    public $timestamps=true;

    public function getMerchant()
    {
        return $this->hasOne(Merchant::class,'id','md_merchant_id');
    }

    public function getDetail()
    {
        return $this->hasMany(MerchantMembershipDetail::class,'sm_merchant_membership_id');
    }


}

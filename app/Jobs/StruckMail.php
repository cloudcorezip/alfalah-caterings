<?php

namespace App\Jobs;

use App\Mail\EmailForSuccessfulStruck;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class StruckMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $email;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    public function __construct($email,$data)
    {
        $this->email=$email;
        $this->data=$data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new EmailForSuccessfulStruck($this->email,$this->data);
        Mail::to($this->email)->send($email);
    }
}

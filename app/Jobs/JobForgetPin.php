<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Jobs;


use App\Mails\Account\EmailActivation;
use App\Mails\Account\ForgetPin;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class JobForgetPin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $pin;
    protected $fullname;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;
    public function __construct($fullname,$email,$pin)
    {
        $this->fullname=$fullname;
        $this->email=$email;
        $this->pin=$pin;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new ForgetPin($this->fullname,$this->pin);
        Mail::to($this->email)->send($email);
    }
}

<?php

namespace App\Jobs;

use App\Mails\Account\EmailActivation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class MailActivation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $link;
    protected $fullname;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;
    public function __construct($fullname,$email,$link)
    {
        $this->fullname=$fullname;
        $this->email=$email;
        $this->link=$link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new EmailActivation($this->fullname,$this->link);
        Mail::to($this->email)->send($email);
    }
}

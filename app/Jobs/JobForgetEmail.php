<?php
/**
 * Senna Apps
 * Copyright (c) 2020.
 */

namespace App\Jobs;


use App\Mails\Account\EmailActivation;
use App\Mails\Account\ForgetEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class JobForgetEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $fullname;
    public $password;
    public $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;
    public function __construct($fullname,$email,$password)
    {
        $this->fullname=$fullname;
        $this->email=$email;
        $this->password=$password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new ForgetEmail($this->fullname,$this->password);
        Mail::to($this->email)->send($email);
    }

}

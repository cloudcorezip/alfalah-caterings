<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SpTransactionStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('md_sp_transaction_status')
            ->insert([[
                'id'=>1,
                'name'=>'Pending'
            ],
                [
                    'id'=>2,
                    'name'=>'Success'
                ],
                [
                    'id'=>3,
                    'name'=>'Canceled'
                ],
                [
                    'id'=>4,
                    'name'=>'Failed'
                ]
            ]);
        DB::table('md_sp_transaction_types')
            ->insert([[
                'id'=>1,
                'name'=>'Manual'
            ],
                [
                    'id'=>2,
                    'name'=>'Online'
                ],
                [
                    'id'=>3,
                    'name'=>'From Merchant'
                ],
                [
                    'id'=>4,
                    'name'=>'Between User'
                ]
            ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class BankAvailableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('md_available_banks')
            ->insert([[
                'bank_account_name'=>'PT Senna Indonesia',
                'bank_no_rek'=>'4330000434',
                'is_active'=>1,
                'md_bank_id'=>4
                ],
                [
                    'bank_account_name'=>'PT Senna Indonesia',
                    'bank_no_rek'=>'002901002737300',
                    'is_active'=>1,
                    'md_bank_id'=>1
                ],
                [
                'bank_account_name'=>'PT Senna Indonesia',
                'bank_no_rek'=>'1370039500000',
                'is_active'=>1,
                'md_bank_id'=>3
                ],
                [
                    'bank_account_name'=>'PT Senna Indonesia',
                    'bank_no_rek'=>'7315099595',
                    'is_active'=>1,
                    'md_bank_id'=>7
                ],
            ]);
    }
}

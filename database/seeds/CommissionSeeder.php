<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sm_merchant_commisions')
            ->insert([
                [
                    'md_subscription_id'=>4,
                    'fee'=>17500,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ],
                [
                    'md_subscription_id'=>8,
                    'fee'=>20000,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ],
                [
                    'md_subscription_id'=>12,
                    'fee'=>25000,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ],
                [
                    'md_subscription_id'=>3,
                    'fee'=>27500,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ],
                [
                    'md_subscription_id'=>7,
                    'fee'=>30000,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ],
                [
                    'md_subscription_id'=>11,
                    'fee'=>35000,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ],
                [
                    'md_subscription_id'=>2,
                    'fee'=>37500,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ],
                [
                    'md_subscription_id'=>6,
                    'fee'=>40000,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ],
                [
                    'md_subscription_id'=>10,
                    'fee'=>45000,
                    'is_active'=>1,
                    'is_deleted'=>0,
                ]
            ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class MarketingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('md_users')
            ->insert([
                [
                    'fullname'=>'Marketing Senna',
                    'email'=>'marketing@senna.co.id',
                    'password'=>bcrypt('marketing@2022!!'),
                    'is_active'=>1,
                    'is_email_verified'=>1,
                    'md_role_id'=>9,
                    'balance'=>0
                ]
            ]);
    }
}

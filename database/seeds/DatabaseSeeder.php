<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(AdminSeeder::class);
        //$this->call(MarketingSeeder::class);
        //$this->call(UserSeeder::class);
        //$this->call(RoleSeeder::class);
        //$this->call(SpTransactionStatusSeeder::class);
        //$this->call(BankAvailableSeeder::class);
        //$this->call(TypeOfBusinessSeeder::class);
        //$this->call(VASeeder::class);
        //$this->call(CommissionSeeder::class);
        //$this->call(MdScApprovalSettingSeeder::class);
        //$this->call(AssetSeeder::class);
        $this->call(TransactionCodeSeeder::class);
    }
}

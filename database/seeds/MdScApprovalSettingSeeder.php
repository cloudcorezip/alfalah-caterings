<?php

use Illuminate\Database\Seeder;

class MdScApprovalSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('md_sc_approval_settings')
            ->insert([
                [
                    'code'=>'UBAH_STOK',
                    'approval_name'=>'Perubahan Stok',
                    'is_active'=>1
                ],
                [
                    'code'=>'UBAH_TIPE_HARGA',
                    'approval_name'=>'Perubahan Tipe Harga',
                    'is_active'=>1
                ],
                [
                    'code'=>'EDIT_RETUR_PENJUALAN',
                    'approval_name'=>'Perubahan Transaksi Penjualan',
                    'is_active'=>1
                ],
                [
                    'code'=>'EDIT_RETUR_PEMBELIAN',
                    'approval_name'=>'Perubahan Transaksi Pembelian',
                    'is_active'=>1
                ],

            ]);
    }
}

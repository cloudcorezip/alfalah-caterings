<?php

use Illuminate\Database\Seeder;

class AssetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp=date('Y-m-d H:i:s');
        \Illuminate\Support\Facades\DB::table('acc_md_property_groups')
            ->insert([
                [
                    'id'=>1,
                    'name'=>'Bukan Bangunan',
                    'parent_id'=>NULL,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>2,
                    'name'=>'Bangunan',
                    'parent_id'=>NULL,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>3,
                    'name'=>'Kelompok 1',
                    'parent_id'=>1,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>4,
                    'name'=>'Kelompok 2',
                    'parent_id'=>1,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>5,
                    'name'=>'Kelompok 3',
                    'parent_id'=>1,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>6,
                    'name'=>'Kelompok 4',
                    'parent_id'=>1,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>7,
                    'name'=>'Permanen',
                    'parent_id'=>2,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>8,
                    'name'=>'Tidak Permanen',
                    'parent_id'=>2,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ]
            ]);

        \Illuminate\Support\Facades\DB::table('acc_md_depreciation_methods')
            ->insert([
                [
                    'id'=>1,
                    'name'=>'Garis Lurus (Straight Line)',
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>2,
                    'name'=>'Saldo Menurun (Double Declining Balance)',
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
            ]);

        \Illuminate\Support\Facades\DB::table('acc_md_property_depreciation_groups')
            ->insert([
                [
                    'id'=>1,
                    'acc_md_property_group_id'=>3,
                    'acc_md_depreciation_method_id'=>1,
                    'benefit_period'=>4,
                    'depreciation_value'=>0.25,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
                [
                    'id'=>2,
                    'acc_md_property_group_id'=>3,
                    'acc_md_depreciation_method_id'=>2,
                    'benefit_period'=>4,
                    'depreciation_value'=>0.5,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
                [
                    'id'=>3,
                    'acc_md_property_group_id'=>4,
                    'acc_md_depreciation_method_id'=>1,
                    'benefit_period'=>8,
                    'depreciation_value'=>0.125,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
                [
                    'id'=>4,
                    'acc_md_property_group_id'=>4,
                    'acc_md_depreciation_method_id'=>2,
                    'benefit_period'=>8,
                    'depreciation_value'=>0.25,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
                [
                    'id'=>5,
                    'acc_md_property_group_id'=>5,
                    'acc_md_depreciation_method_id'=>1,
                    'benefit_period'=>16,
                    'depreciation_value'=>0.0625,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
                [
                    'id'=>6,
                    'acc_md_property_group_id'=>5,
                    'acc_md_depreciation_method_id'=>2,
                    'benefit_period'=>16,
                    'depreciation_value'=>0.125,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
                [
                    'id'=>7,
                    'acc_md_property_group_id'=>6,
                    'acc_md_depreciation_method_id'=>1,
                    'benefit_period'=>20,
                    'depreciation_value'=>0.05,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
                [
                    'id'=>8,
                    'acc_md_property_group_id'=>6,
                    'acc_md_depreciation_method_id'=>2,
                    'benefit_period'=>20,
                    'depreciation_value'=>0.1,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp
                ],
                [
                    'id'=>9,
                    'acc_md_property_group_id'=>7,
                    'acc_md_depreciation_method_id'=>1,
                    'benefit_period'=>20,
                    'depreciation_value'=>0.05,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
                [
                    'id'=>10,
                    'acc_md_property_group_id'=>8,
                    'acc_md_depreciation_method_id'=>1,
                    'benefit_period'=>10,
                    'depreciation_value'=>0.1,
                    'created_at'=>$timestamp,
                    'updated_at'=>$timestamp

                ],
            ]);
    }
}

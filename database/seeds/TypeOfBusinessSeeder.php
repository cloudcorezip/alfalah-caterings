<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TypeOfBusinessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('md_type_of_business')
            ->insert([
                [
                    'id'=>1,
                    'name'=>'PT (Perseroan Terbatas)'
                ],
                [
                    'id'=>2,
                    'name'=>'CV (Persekutuan Komanditer)'
                ],
                [
                    'id'=>3,
                    'name'=>'Firma'
                ],
                [
                    'id'=>4,
                    'name'=>'Perseorangan'
                ],
                [
                    'id'=>5,
                    'name'=>'Koperasi'
                ],
            ]);
    }
}

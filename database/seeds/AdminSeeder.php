<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('md_roles')
//            ->insert([
//                [
//                    'id'=>3,
//                    'name'=>'Merchant Event'
//                ],
//                [
//                    'id'=>4,
//                    'name'=>'Merchant Toko'
//                ],
//                [
//                    'id'=>5,
//                    'name'=>'SuperAdmin'
//                ],
//                [
//                    'id'=>6,
//                    'name'=>'Admin'
//                ],
//                [
//                    'id'=>7,
//                    'name'=>'Admin Blog'
//                ],
//                [
//                    'id'=>8,
//                    'name'=>'CS'
//                ],
//                [
//                    'id'=>9,
//                    'name'=>'Marketing'
//                ],
//            ]);

        DB::table('md_users')
            ->insert([
                [
                    'id'=>20,
                    'fullname'=>'Superadmin Senna',
                    'email'=>'superadmin@senna.co.id',
                    'password'=>bcrypt('superadmin@2022!!'),
                    'is_active'=>1,
                    'is_email_verified'=>1,
                    'md_role_id'=>5,
                    'balance'=>0
                ],
                [
                    'id'=>21,
                    'fullname'=>'Admin Blog Senna',
                    'email'=>'adminblog@senna.co.id',
                    'password'=>bcrypt('adminblog@2022!!'),
                    'is_active'=>1,
                    'is_email_verified'=>1,
                    'md_role_id'=>7,
                    'balance'=>0
                ]
            ]);

    }

}

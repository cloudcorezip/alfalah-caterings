<?php

use Illuminate\Database\Seeder;

class VASeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('md_va_banks')
            ->insert([
                [
                    'code'=>'BNI',
                    'is_active'=>1,
                    'md_bank_id'=>4,
                    'plus_amount'=>4500
                ],
                [
                    'code'=>'BRI',
                    'is_active'=>1,
                    'md_bank_id'=>1,
                    'plus_amount'=>4500
                ],
                [
                    'code'=>'MANDIRI',
                    'is_active'=>1,
                    'md_bank_id'=>3,
                    'plus_amount'=>4500
                ],
                [
                    'code'=>'BCA',
                    'is_active'=>0,
                    'md_bank_id'=>7,
                    'plus_amount'=>4500
                ],
                [
                'code'=>'PERMATA',
                'is_active'=>1,
                'md_bank_id'=>6,
                    'plus_amount'=>4500
                    ]
            ]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreMerchantDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_merchant_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_product_id');
            $table->unsignedBigInteger('sc_product_category_id');
            $table->smallInteger('is_show')->default(1);
            $table->double('stock',20,3)->default(0);
            $table->unsignedBigInteger('store_merchant_config_id');
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('sc_product_category_id')
                ->references('id')
                ->on('sc_product_categories');
            $table->foreign('store_merchant_config_id')
                ->references('id')
                ->on('store_merchant_configs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_merchant_details');
    }
}

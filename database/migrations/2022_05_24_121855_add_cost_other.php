<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCostOther extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_products', 'cost_other')) {
                $table->jsonb('cost_other')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            //
        });
    }
}

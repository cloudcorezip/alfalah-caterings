<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableHrMerchantShiftRoasterChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_merchant_shift_roaster_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('staff_id');
            $table->unsignedBigInteger('staff_user_id');
            $table->date('shift_change_date');
            $table->unsignedBigInteger('shift_id');
            $table->string('reason')->nullable();
            $table->unsignedBigInteger('to_staff_id')->nullable();
            $table->unsignedBigInteger('to_staff_user_id')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->string('timezone')->nullable();
            $table->smallInteger('is_accepted')->default(0);
            $table->unsignedBigInteger('approval_by')->nullable();
            $table->dateTime('approval_date')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->smallInteger('is_from_mobile')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('original_shift_id')->nullable();
            $table->unsignedBigInteger('to_original_shift_id')->nullable();
            $table->foreign('staff_id')
                ->references('id')
                ->on('md_merchant_staff');
            $table->foreign('staff_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('to_staff_id')
                ->references('id')
                ->on('md_merchant_staff');
            $table->foreign('to_staff_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('shift_id')->references('id')->on('hr_merchant_shift_settings');
            $table->foreign('md_merchant_id')->references('id')->on('md_merchants');
            $table->foreign('approval_by')->references('id')->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_merchant_shift_roaster_changes');
    }
}

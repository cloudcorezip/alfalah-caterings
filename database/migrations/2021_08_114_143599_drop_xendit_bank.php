<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropXenditBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {                
        Schema::table('qris_activations', function (Blueprint $table) {

            if(Schema::hasColumn('qris_activations','md_xendit_available_bank_id'))
            {
                $table->dropColumn('md_xendit_available_bank_id');
            }
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qris_activations', function (Blueprint $table) {
            //
        });
    }
}

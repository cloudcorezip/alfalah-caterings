<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnableMerchantPluginDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enable_merchant_plugin_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('enable_merchant_plugin_id');
            $table->foreign('enable_merchant_plugin_id')
                ->references('id')
                ->on('enable_merchant_plugins');
            $table->string('shop_id')->nullable();
            $table->string('code')->nullable();
            $table->string('shop_name')->nullable();
            $table->jsonb('other_information')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enable_merchant_plugin_details');
    }
}

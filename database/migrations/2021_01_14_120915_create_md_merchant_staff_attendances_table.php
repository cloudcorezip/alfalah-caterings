<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantStaffAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_staff_attendances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_staff_user_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->dateTime('start_work');
            $table->dateTime('end_work')->nullable();
            $table->string('start_longitude');
            $table->string('start_latitude');
            $table->string('end_longitude')->nullable();
            $table->string('end_latitude')->nullable();
            $table->string('start_work_file');
            $table->string('end_work_file')->nullable();
            $table->foreign('md_staff_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_staff_attendances');
    }
}

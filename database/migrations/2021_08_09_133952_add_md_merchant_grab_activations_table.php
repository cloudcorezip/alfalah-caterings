<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdMerchantGrabActivationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_grab_activations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->string('registrant_name');
            $table->string('registrant_email');
            $table->string('registrant_phone_number');
            $table->string('business_type_name')->nullable();
            $table->string('owner_name')->nullable();
            $table->text('owner_address')->nullable();
            $table->string('owner_identity_card_number')->nullable();
            $table->string('owner_phone_number')->nullable();
            $table->string('owner_email')->nullable();
            $table->date('owner_birthdate')->nullable();
            $table->string('owner_citizenship')->nullable();
            $table->string('job_level')->nullable();
            $table->jsonb('owner_file')->nullable();
            $table->string('business_email')->nullable();
            $table->string('business_phone_number')->nullable();
            $table->string('grab_food_name')->nullable();
            $table->string('business_category')->nullable();
            $table->string('business_scalable')->nullable();
            $table->string('business_revenue_average')->nullable();
            $table->text('business_address')->nullable();
            $table->string('google_map_link')->nullable();
            $table->string('countries_name')->nullable();
            $table->string('province_name')->nullable();
            $table->string('city_name')->nullable();
            $table->string('district_name')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('location_type')->nullable();
            $table->string('location_benchmark')->nullable();
            $table->jsonb('outlet_foto')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_number')->nullable();
            $table->string('bank_account_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('bank_file')->nullable();
            $table->smallInteger('is_same_bank_name')->default(0);
            $table->string('letter_of_attorney_file')->nullable();
            $table->jsonb('outlet_type_food')->nullable();
            $table->jsonb('outlet_tag_food')->nullable();
            $table->string('food_classification')->nullable();
            $table->smallInteger('is_with_tax')->default(0);
            $table->text('outlet_detail')->nullable();
            $table->string('time_pickup')->nullable();
            $table->string('outlet_logo')->nullable();
            $table->jsonb('menu_list')->nullable();
            $table->jsonb('food_list')->nullable();
            $table->jsonb('time_operational')->nullable();
            $table->string('npwp_number')->nullable();
            $table->string('npwp_file')->nullable();
            $table->smallInteger('is_approve')->default(0);
            $table->smallInteger('registration_status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_grab_activations');
    }
}

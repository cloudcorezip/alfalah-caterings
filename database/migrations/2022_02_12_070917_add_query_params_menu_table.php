<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQueryParamsMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_menus', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_menus','query_params'))
            {
                $table->string('query_params')->nullable();
            }
        });

        Schema::table('cfg_menu_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('cfg_menu_merchants','query_params'))
            {
                $table->string('query_params')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_menus', function (Blueprint $table) {
            //
        });
    }
}

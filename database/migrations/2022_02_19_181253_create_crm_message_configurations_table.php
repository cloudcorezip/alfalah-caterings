<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmMessageConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_message_configurations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_key');
            $table->string('string_id')->nullable();
            $table->string('password')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('type')->default(0);
            $table->string('key_url')->nullable();
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_message_configurations');
    }
}

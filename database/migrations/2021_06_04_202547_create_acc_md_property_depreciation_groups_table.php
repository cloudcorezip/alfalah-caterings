<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccMdPropertyDepreciationGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_md_property_depreciation_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('acc_md_property_group_id');
            $table->unsignedBigInteger('acc_md_depreciation_method_id');
            $table->double('benefit_period',20,3)->default(0);
            $table->double('depreciation_value',20,3)->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('acc_md_property_group_id')
                ->references('id')
                ->on('acc_md_property_groups');
            $table->foreign('acc_md_depreciation_method_id')
                ->references('id')
                ->on('acc_md_depreciation_methods');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_md_property_depreciation_groups');
    }
}

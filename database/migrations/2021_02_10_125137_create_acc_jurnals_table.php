<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccJurnalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_jurnals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trans_code');
            $table->string('trans_name');
            $table->dateTime('trans_time');
            $table->string('trans_proof')->nullable();
            $table->longText('trans_note')->nullable();
            $table->string('trans_purpose')->nullable();
            $table->smallInteger('trans_type')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->double('trans_amount',20,3)->default(0);
            $table->double('admin_fee',20,3)->default(0);
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('md_user_id_created');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('md_user_id_created')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_jurnals');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminFeeToSpTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sp_transactions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_transactions','admin_fee'))
            {
                $table->double('admin_fee',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sp_transactions', function (Blueprint $table) {
            //
        });
    }
}

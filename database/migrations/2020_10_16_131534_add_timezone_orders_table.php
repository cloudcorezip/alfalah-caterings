<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('sc_transfer_stocks', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_transfer_stocks','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });
        Schema::table('sc_retur_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_orders','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_orders','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('ss_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_order_services','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

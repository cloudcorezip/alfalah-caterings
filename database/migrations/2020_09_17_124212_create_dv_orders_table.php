<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDvOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dv_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('from_latitude');
            $table->string('from_longitude');
            $table->string('from_desc')->nullable();
            $table->string('destination_latitude');
            $table->string('destination_longitude');
            $table->string('destination_desc')->nullable();
            $table->double('total',20,3)->default(0);
            $table->double('promo',20,3)->default(0);
            $table->double('admin_fee',20,3)->default(0);
            $table->double('distance',20,3)->default(0);
            $table->unsignedBigInteger('md_user_id_order');
            $table->unsignedBigInteger('md_user_id_driver')->nullable();
            $table->unsignedBigInteger('dv_status_order_id');
            $table->smallInteger('is_approved_driver')->default(0);
            $table->smallInteger('is_canceled_user')->default(0);
            $table->foreign('dv_status_order_id')
                ->references('id')
                ->on('dv_status_orders');
            $table->foreign('md_user_id_order')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_user_id_driver')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dv_orders');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfgMerchantMenuStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfg_merchant_menu_staffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('staff_user_id');
            $table->unsignedBigInteger('cfg_menu_merchant_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_active')->default(0);
            $table->foreign('staff_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('cfg_menu_merchant_id')
                ->references('id_')
                ->on('cfg_menu_merchants');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfg_merchant_menu_staffs');
    }
}

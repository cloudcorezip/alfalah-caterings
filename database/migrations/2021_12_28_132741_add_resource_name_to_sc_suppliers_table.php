<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResourceNameToScSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_suppliers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_suppliers','resource_name'))
            {
               $table->string('resource_name')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_suppliers', function (Blueprint $table) {
            //
        });
    }
}

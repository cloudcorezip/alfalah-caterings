<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoaCustomJsonToMdMerchantFormOrderTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_form_order_template', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_form_order_template','coa_custom_json')){
                $table->jsonb('coa_custom_json')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_form_order_template', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsFromCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','is_from_supplier')){
                $table->smallInteger('is_from_supplier')->default(0);
                $table->unsignedBigInteger('ref_user_id')->nullable();
            }
        });
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','is_from_customer')){
                $table->smallInteger('is_from_customer')->default(0);
                $table->unsignedBigInteger('ref_user_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            //
        });
    }
}

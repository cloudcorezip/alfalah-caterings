<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScPromoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_promo_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('sc_product_id');
            $table->double('promo_percentage',20,3)->default(0);
            $table->double('price_after_promo',20,3)->default(0);
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->smallInteger('is_active')->default(0);
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_promo_products');
    }
}

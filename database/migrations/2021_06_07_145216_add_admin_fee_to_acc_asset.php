<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminFeeToAccAsset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_asset_depreciation_payments', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciation_payments','admin_fee'))
            {
                $table->double('admin_fee',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_asset_depreciation_payments', function (Blueprint $table) {
            //
        });
    }
}

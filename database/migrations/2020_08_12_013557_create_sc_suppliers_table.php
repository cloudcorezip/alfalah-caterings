<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('code')->nullable();
            $table->unsignedBigInteger('sc_customer_level_id')->nullable();
            $table->string('address')->nullable();
            $table->unsignedBigInteger('md_user_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('sc_supplier_categories_id')
                ->references('id')
                ->on('sc_supplier_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_suppliers');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductResultToCdcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_coupons', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_coupons','product_result'))
            {
                $table->jsonb('product_result')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_coupons', function (Blueprint $table) {
            //
        });
    }
}

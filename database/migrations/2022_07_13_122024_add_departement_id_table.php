<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDepartementIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff','departement_id'))
            {
                $table->bigInteger('departement_id')->nullable();
                $table->foreign('departement_id')->references('id')
                    ->on('hr_merchant_departements');
                $table->bigInteger('job_position_id')->nullable();
                $table->foreign('job_position_id')->references('id')
                    ->on('hr_merchant_job_positions');
                $table->date('join_date')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff', function (Blueprint $table) {
            //
        });
    }
}

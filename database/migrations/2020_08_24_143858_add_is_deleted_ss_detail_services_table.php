<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDeletedSsDetailServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_detail_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_services','is_archived')){
                $table->smallInteger('is_archived')->default(0);
                $table->double('quantity_per_unit',20,2)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_detail_services', function (Blueprint $table) {
            //
        });
    }
}

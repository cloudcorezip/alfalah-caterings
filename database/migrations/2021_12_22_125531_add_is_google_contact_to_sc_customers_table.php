<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsGoogleContactToScCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_customers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_customers','is_google_contact'))
            {
               $table->string('google_contact_id')->nullable();
               $table->string('etag')->nullable();
               $table->smallInteger('is_google_contact')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_customers', function (Blueprint $table) {
            //
        });
    }
}

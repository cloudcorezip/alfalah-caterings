<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCheckinCheckoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfa_visit_schedules', function (Blueprint $table) {
            if(!Schema::hasColumn('sfa_visit_schedules','visit_time_checkout'))
            {
                $table->dateTime('visit_time_checkout')->nullable();
                $table->string('supporting_file_checkout')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfa_visit_schedules', function (Blueprint $table) {
            //
        });
    }
}

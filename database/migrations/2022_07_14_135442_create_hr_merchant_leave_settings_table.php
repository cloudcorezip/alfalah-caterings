<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrMerchantLeaveSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_merchant_leave_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('color')->nullable();
            $table->smallInteger('is_with_paid')->default(0);
            $table->smallInteger('is_with_salary_cut')->default(0);
            $table->smallInteger('type_of_salary_cut')->default(0);
            $table->smallInteger('is_per_month')->default(0);
            $table->integer('max_day')->default(0);
            $table->double('salary_cut_value',20,3)->default(0);
            $table->string('type_of_attendance')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_to_all_branch')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('md_merchant_id')->references('id')
                ->on('md_merchants');
            $table->foreign('created_by')->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_merchant_leave_settings');
    }
}

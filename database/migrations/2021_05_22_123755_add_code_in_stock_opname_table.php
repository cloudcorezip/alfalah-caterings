<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCodeInStockOpnameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_stock_opnames','code'))
            {
                $table->string('code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            //
        });
    }
}

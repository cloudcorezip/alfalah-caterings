<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('address')->nullable();
            $table->string('code')->nullable();
            $table->unsignedBigInteger('sc_customer_level_id')->nullable();
            $table->double('point',20,2)->default(0);
            $table->unsignedBigInteger('md_user_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('sc_customer_level_id')
                ->references('id')
                ->on('sc_customer_level');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_customers');
    }
}

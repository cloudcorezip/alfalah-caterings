<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmMerchantRedeemPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_merchant_redeem_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('redeemable_id');
            $table->string('redeemable_type');
            $table->smallInteger('is_type')->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->double('point',20,3)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_merchant_redeem_points');
    }
}

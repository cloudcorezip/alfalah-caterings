<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScTempSaleOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_temp_sale_order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_product_id');
            $table->double('quantity',20,3)->default(0);
            $table->double('sub_total',20,3)->default(0);
            $table->double('price',20,3)->default(0);

            $table->unsignedBigInteger('sc_sale_order_id');
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('sc_sale_order_id')
                ->references('id')
                ->on('sc_sale_orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_temp_sale_order_details');
    }
}

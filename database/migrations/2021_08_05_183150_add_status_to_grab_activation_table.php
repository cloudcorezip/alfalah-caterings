<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToGrabActivationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_grab_activations', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_grab_activations','status'))
            {
                $table->string('owner_citizenship')->nullable();
                $table->string('google_map_link')->nullable();
                $table->string('province_name')->nullable();
                $table->string('district_name')->nullable();
                $table->string('postal_code')->nullable();
                $table->string('business_category')->nullable();
                $table->string('letter_of_attorney_file')->nullable();
                $table->smallInteger('status')->default(0);
                
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_grab_activations', function (Blueprint $table) {
            //
        });
    }
}

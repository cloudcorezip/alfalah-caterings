<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_user_address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address')->nullable();
            $table->smallInteger('is_primary')->default(0);
            $table->unsignedInteger('md_user_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });

        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(Schema::hasColumn('sc_sale_orders','rajaongkir_response'))
            {
                $table->dropColumn('rajaongkir_response');
                $table->longText('other_information')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_user_address');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsSellToMarketplaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_products','is_sell_to_ecommerce')){
                $table->smallInteger('is_sell_to_ecommerce')->default(0);
                $table->smallInteger('is_draft')->default(0);
                $table->jsonb('product_photos')->nullable();
                $table->bigInteger('ecommerce_product_category_id')->nullable();
                $table->double('hight',20,3)->default(0);
                $table->double('long',20,3)->default(0);
                $table->double('wide',20,3)->default(0);
                $table->text('package_contents')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            //
        });
    }
}

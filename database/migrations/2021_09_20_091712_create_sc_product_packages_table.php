<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScProductPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_product_packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_product_id_child');
            $table->smallInteger('is_deleted')->default(0);
            $table->unsignedBigInteger('sc_product_id');
            $table->double('quantity',20,3)->default(0);
            $table->unsignedBigInteger('md_unit_id');
            $table->foreign('sc_product_id_child')
                ->references('id')
                ->on('sc_products');
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('md_unit_id')
                ->references('id')
                ->on('md_units');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_product_packages');
    }
}

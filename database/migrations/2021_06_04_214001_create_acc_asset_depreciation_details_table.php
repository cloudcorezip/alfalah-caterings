<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccAssetDepreciationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_asset_depreciation_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->dateTime('time');
            $table->double('value_depreciation',20,3)->default(0);
            $table->smallInteger('is_record_to_journal')->default(0);
            $table->dateTime('record_time_to_journal')->nullable();
            $table->unsignedBigInteger('acc_asset_depreciation_id');
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('acc_asset_depreciation_id')
                ->references('id')
                ->on('acc_asset_depreciations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_asset_depreciation_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordStock5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_retur_sale_orders', function (Blueprint $table) {
            if(Schema::hasColumn('sc_retur_sale_orders','type'))
            {
               $table->dropColumn('type');
            }
        });

        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            if(Schema::hasColumn('sc_retur_purchase_orders','type'))
            {
                $table->dropColumn('type');
            }
        });

        Schema::table('sc_retur_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_order_details','type'))
            {
                $table->enum('type',['plus','minus']);
            }
        });

        Schema::table('sc_retur_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_order_details','type'))
            {
                $table->enum('type',['plus','minus']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            //
        });
    }
}

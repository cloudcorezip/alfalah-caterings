<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusInMerchantServiceAvailablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_service_availables', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_service_availables','status')){
                $table->boolean('status')->default(false);
            }
        });

        Schema::table('ss_detail_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_services','banner')){
                $table->string('banner')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_service_availables', function (Blueprint $table) {
            //
        });
    }
}

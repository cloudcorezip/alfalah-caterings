<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScProductSpecialPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_product_special_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('md_merchant_id');
            $table->timestamp('start_date');
            $table->timestamp('end_date')->nullable();
            $table->smallInteger('is_daily')->default(0);
            $table->time('start_hour')->nullable();
            $table->time('end_hour')->nullable();
            $table->jsonb('days')->nullable();
            $table->jsonb('customer_criteria')->nullable();
            $table->smallInteger('is_all_customer')->default(1);
            $table->jsonb('assign_to_branch')->nullable();
            $table->smallInteger('is_all_branch')->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_product_special_prices');
    }
}

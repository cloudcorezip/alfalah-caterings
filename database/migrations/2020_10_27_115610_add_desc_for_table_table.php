<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescForTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_inventory_methods', function (Blueprint $table) {
            if(!Schema::hasColumn('md_inventory_methods','description'))
            {
                $table->longText('description')->nullable();
            }
        });

        Schema::table('md_subscriptions', function (Blueprint $table) {
            if(!Schema::hasColumn('md_subscriptions','banner'))
            {
                $table->string('banner')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_inventory_methods', function (Blueprint $table) {
            //
        });
    }
}

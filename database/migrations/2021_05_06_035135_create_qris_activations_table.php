<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQrisActivationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qris_activations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->bigInteger('md_user_id')->unsigned();
            $table->bigInteger('md_merchant_id')->unsigned();
            $table->string('identity_card');
            $table->string('account_book');
            $table->bigInteger('md_bank_id')->unsigned();
            $table->string('bank_account_name');
            $table->string('bank_account_number');
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_agree')->default(0);
            $table->smallInteger('withdraw_period')->default(0);
            $table->double('balance_amount',20,3)->default(0);
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('md_bank_id')
                ->references('id')
                ->on('md_banks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qris_activations');
    }
}

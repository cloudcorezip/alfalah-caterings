<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableRedeemProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_product_redeems', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sc_product_id');
            $table->unsignedBigInteger('sc_sale_order_id');
            $table->double('amount_redeem')->default(0);
            $table->dateTime('expired_date')->nullable();
            $table->timestamps();
        });

        Schema::create('sc_product_redeem_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('child_product_id');
            $table->double('quantity')->default(0);
            $table->unsignedBigInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_redeem_product');
    }
}

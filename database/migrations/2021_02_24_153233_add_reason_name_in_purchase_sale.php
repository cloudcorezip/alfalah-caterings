<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReasonNameInPurchaseSale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_orders','_reason_name'))
            {
                $table->string('_reason_name')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            //
        });
    }
}

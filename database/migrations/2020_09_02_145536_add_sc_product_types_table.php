<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddScProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','md_sc_product_type_id')){
                $table->unsignedBigInteger('md_sc_product_type_id');
                $table->foreign('md_sc_product_type_id')
                    ->references('id')
                    ->on('md_sc_product_types');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            //
        });
    }
}

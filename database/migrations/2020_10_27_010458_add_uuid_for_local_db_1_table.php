<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUuidForLocalDb1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });
        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar_details','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });
        Schema::table('acc_merchant_ap_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap_details','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_branchs', function (Blueprint $table) {
            //
        });
    }
}

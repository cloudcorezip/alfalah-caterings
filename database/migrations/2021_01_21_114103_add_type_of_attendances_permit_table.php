<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeOfAttendancesPermitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_permit_categories', function (Blueprint $table) {
            if(!Schema::hasColumn('md_permit_categories','type_of_attendance'))
            {
                $table->enum('type_of_attendance',['H','I','S','A'])->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_permit_categories', function (Blueprint $table) {
            //
        });
    }
}

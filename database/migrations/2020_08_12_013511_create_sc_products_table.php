<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('foto');
            $table->string('code');
            $table->double('selling_price',20,2)->default(0);
            $table->double('purchase_price',20,2)->default(0);
            $table->unsignedBigInteger('stock')->default(0);
            $table->string('description')->nullable();
            $table->unsignedBigInteger('sc_merk_id');
            $table->unsignedBigInteger('sc_product_type_id');
            $table->unsignedBigInteger('sc_product_category_id');
            $table->unsignedBigInteger('md_user_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('sc_merk_id')
                ->references('id')
                ->on('sc_merks');
            $table->foreign('sc_product_type_id')
                ->references('id')
                ->on('sc_product_types');
            $table->foreign('sc_product_category_id')
                ->references('id')
                ->on('sc_product_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_products');
    }
}

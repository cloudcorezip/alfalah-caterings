<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccCoaCashflowFormatDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_coa_cashflow_format_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('acc_coa_cashflow_format_id');
            $table->unsignedBigInteger('acc_coa_detail_id');
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('acc_coa_cashflow_format_id')
                ->references('id')
                ->on('acc_coa_cashflow_formats');
            $table->foreign('acc_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_coa_cashflow_format_details');
    }
}

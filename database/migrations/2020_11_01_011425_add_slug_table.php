<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_bg_categories', function (Blueprint $table) {
            if(!Schema::hasColumn('md_bg_categories','slug'))
            {
                $table->string('slug')->nullable();
            }
        });

        Schema::table('md_bg_tags', function (Blueprint $table) {
            if(!Schema::hasColumn('md_bg_tags','slug'))
            {
                $table->string('slug')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_bg_categories', function (Blueprint $table) {
            //
        });
    }
}

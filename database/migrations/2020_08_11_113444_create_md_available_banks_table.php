<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdAvailableBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_available_banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bank_account_name')->nullable();
            $table->string('bank_no_rek')->nullable();
            $table->smallInteger('is_active')->default(0);
            $table->unsignedBigInteger('md_bank_id');
            $table->foreign('md_bank_id')
                ->references('id')
                ->on('md_banks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_available_banks');
    }
}

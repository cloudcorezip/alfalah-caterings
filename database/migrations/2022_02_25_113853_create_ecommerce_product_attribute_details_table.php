<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcommerceProductAttributeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_product_attribute_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('value_id')->nullable();
            $table->string('value_name')->nullable();
            $table->string('blibli_val_disp_name')->nullable();
            $table->string('bukalapak_val_disp_name')->nullable();
            $table->string('elevenia_val_disp_name')->nullable();
            $table->jsonb('have_options')->nullable();
            $table->string('lazada_val_disp_name')->nullable();
            $table->string('shopee_val_disp_name')->nullable();
            $table->string('tokopedia_val_disp_name')->nullable();
            $table->string('zalora_val_disp_name')->nullable();
            $table->string('zilingo_val_disp_name')->nullable();
            $table->string('attribute_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_product_attribute_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCostCoaIdToPogbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_inv_production_of_good_boms', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_inv_production_of_good_boms','cost_coa_id'))
            {
                $table->unsignedBigInteger('cost_coa_id')->nullable();
                $table->unsignedBigInteger('md_transaction_type_id')->nullable();
                $table->double('admin_fee',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_inv_production_of_good_boms', function (Blueprint $table) {
            //
        });
    }
}

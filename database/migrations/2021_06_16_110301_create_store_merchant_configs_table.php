<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreMerchantConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_merchant_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_code');
            $table->string('qr_code');
            $table->string('url');
            $table->smallInteger('order_type')->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->jsonb('payment_type')->nullable();
            $table->string('template_qr')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_merchant_configs');
    }
}

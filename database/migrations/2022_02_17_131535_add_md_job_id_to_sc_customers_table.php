<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdJobIdToScCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_customers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_customers','created_by'))
            {
                $table->string('identity_card_number')->nullable();
                $table->smallInteger('media_info')->nullable();
                $table->smallInteger('religion')->nullable();
                $table->smallInteger('marital_status')->nullable();
                $table->bigInteger('age')->nullable();
                $table->unsignedBigInteger('md_job_id')->nullable();
                $table->timestamp('date_of_birth')->nullable();
                $table->unsignedBigInteger('created_by')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_customers', function (Blueprint $table) {
            //
        });
    }
}

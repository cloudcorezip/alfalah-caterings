<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsReviewDetailOrderServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_review_detail_order_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('rating',10,2)->default(0);
            $table->string('suggestions')->nullable();
            $table->unsignedBigInteger('user_id_review');
            $table->unsignedBigInteger('ss_detail_order_service_id');
            $table->unsignedBigInteger('ss_detail_service_id');
            $table->foreign('user_id_review')
                ->references('id')
                ->on('md_users');
            $table->foreign('ss_detail_order_service_id')
                ->references('id')
                ->on('ss_detail_order_services');
            $table->foreign('ss_detail_service_id')
                ->references('id')
                ->on('ss_detail_services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_review_detail_order_services');
    }
}

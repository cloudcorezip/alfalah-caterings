<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrMerchantStaffAmountLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_merchant_staff_amount_leave', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('leave_id');
            $table->unsignedBigInteger('staff_id');
            $table->unsignedBigInteger('staff_user_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('permit_id')->nullable();
            $table->smallInteger('is_approve')->default(0);
            $table->double('total',20,3)->default(1);
            $table->date('date');
            $table->string('type');
            $table->string('type_of_attendance')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('staff_id')->references('id')->on('md_merchant_staff');
            $table->foreign('staff_user_id')->references('id')->on('md_users');
            $table->foreign('md_merchant_id')->references('id')->on('md_merchants');
            $table->string('timezone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_merchant_staff_amount_leave');
    }
}

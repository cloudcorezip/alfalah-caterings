<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDvUserCancleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dv_user_cancel_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_user_id_driver');
            $table->unsignedBigInteger('dv_order_id');
            $table->string('reason_cancelation')->nullable();
            $table->foreign('md_user_id_driver')
                ->references('id')
                ->on('md_users');
            $table->foreign('dv_order_id')
                ->references('id')
                ->on('dv_orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dv_user_cancel_orders');
    }
}

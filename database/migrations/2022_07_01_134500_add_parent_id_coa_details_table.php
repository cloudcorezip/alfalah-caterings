<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentIdCoaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_coa_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_coa_details','parent_id'))
            {
                $table->bigInteger('parent_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_coa_details', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJsonVaAndCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_users', function (Blueprint $table) {
            if(Schema::hasColumn('md_users','json_resp'))
            {
                $table->dropColumn('json_resp');
                $table->dropColumn('is_generate_va');
            }

            if(!Schema::hasColumn('md_users','json_va_mandiri'))
            {
                $table->jsonb('json_va_mandiri')->nullable();
                $table->jsonb('json_va_bca')->nullable();
                $table->jsonb('json_va_bni')->nullable();
                $table->jsonb('json_va_bri')->nullable();
                $table->jsonb('json_va_permata')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_users', function (Blueprint $table) {
            //
        });
    }
}

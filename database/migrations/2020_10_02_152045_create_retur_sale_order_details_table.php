<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReturSaleOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_retur_sale_order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_product_id');
            $table->unsignedBigInteger('sc_sale_order_detail_id');
            $table->unsignedBigInteger('sc_retur_sale_order_id');
            $table->double('quantity',20,3)->default(0);
            $table->double('sub_total',20,3)->default(0);
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('sc_sale_order_detail_id')
                ->references('id')
                ->on('sc_sale_order_details');
            $table->foreign('sc_retur_sale_order_id')
                ->references('id')
                ->on('sc_retur_sale_orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_retur_sale_order_details');
    }
}

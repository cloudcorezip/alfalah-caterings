<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsPromoDetailServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_promo_detail_services', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ss_detail_service_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->double('promo_percentage',20,3)->default(0);
            $table->double('price_after_promo',20,3)->default(0);
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->smallInteger('is_active')->default(0);
            $table->foreign('ss_detail_service_id')
                ->references('id')
                ->on('ss_detail_services');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_promo_detail_services');
    }
}

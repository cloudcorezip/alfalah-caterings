<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMinTransactionToMerchantCommisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_commissions', function (Blueprint $table) {
            if(!Schema::hasColumn('merchant_commissions','min_transaction'))
            {
                $table->double('min_transaction',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_commisions', function (Blueprint $table) {
            //
        });
    }
}

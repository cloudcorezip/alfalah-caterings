<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCodeToMdMerchantStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff','code')){
                $table->string('code')->nullable();
                $table->smallInteger('marital_status')->nullable();
                $table->smallInteger('religion')->nullable();
                $table->text('address')->nullable();
                $table->string('blood_type')->nullable();
                $table->smallInteger('last_education')->nullable();
                $table->smallInteger('is_active')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff', function (Blueprint $table) {
            //
        });
    }
}

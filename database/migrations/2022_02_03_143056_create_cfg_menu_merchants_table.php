<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCfgMenuMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfg_menu_merchants', function (Blueprint $table) {
            $table->bigIncrements('id_');
            $table->string('name');
            $table->string('icons');
            $table->string('unique_code');
            $table->string('description');
            $table->string('url');
            $table->smallInteger('is_type')->default(0);
            $table->bigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->bigInteger('id');
            $table->jsonb('payment_clusters')->nullable();
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfg_menu_merchants');
    }
}

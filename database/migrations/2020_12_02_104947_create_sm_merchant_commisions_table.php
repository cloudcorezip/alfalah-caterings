<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmMerchantCommisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_merchant_commisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_subscription_id');
            $table->double('fee',20,3)->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('md_subscription_id')
                ->references('id')
                ->on('md_subscriptions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_merchant_commisions');
    }
}

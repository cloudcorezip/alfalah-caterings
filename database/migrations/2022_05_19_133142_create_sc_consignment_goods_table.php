<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScConsignmentGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_consignment_goods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->unsignedBigInteger('sc_supplier_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('inv_warehouse_id');
            $table->dateTime('create_time');
            $table->dateTime('time_received');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->string('attachment_file')->nullable();
            $table->double('total',20,3)->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->longText('note')->nullable();
            $table->string('timezone');
            $table->foreign('sc_supplier_id')
                ->references('id')
                ->on('sc_suppliers');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('inv_warehouse_id')
                ->references('id')
                ->on('md_merchant_inv_warehouses');
            $table->foreign('created_by')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_consignment_goods');
    }
}

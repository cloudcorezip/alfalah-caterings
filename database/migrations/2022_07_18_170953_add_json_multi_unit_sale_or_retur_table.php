<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJsonMultiUnitSaleOrReturTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_order_details','json_multi_unit')){
                $table->jsonb('json_multi_unit')->nullable();
                $table->double('value_conversation',20,3)->default(1);
            }
        });

        Schema::table('sc_retur_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_order_details','json_multi_unit')){
                $table->jsonb('json_multi_unit')->nullable();
                $table->double('value_conversation',20,3)->default(1);
                $table->unsignedBigInteger('multi_unit_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            //
        });
    }
}

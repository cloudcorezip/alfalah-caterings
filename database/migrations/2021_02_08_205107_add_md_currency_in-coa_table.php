<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdCurrencyInCoaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_coa_details', function (Blueprint $table) {
            if(Schema::hasColumn('acc_coa_details','md_merchant_currency_id'))
            {
                $table->dropColumn('md_merchant_currency_id');
            }
            if(!Schema::hasColumn('acc_coa_details','md_sc_currency_id'))
            {
                $table->unsignedBigInteger('md_sc_currency_id');
                $table->foreign('md_sc_currency_id')
                    ->references('id')
                    ->on('md_sc_currencies');
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_coa_details', function (Blueprint $table) {
            //
        });
    }
}

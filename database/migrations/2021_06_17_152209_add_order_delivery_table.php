<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','total_delivery'))
            {
                $table->double('total_delivery',20,3)->default(0);
                $table->double('tax_delivery',20,3)->default(0);
                $table->double('tax_percentage_delivery',20,3)->default(0);
                $table->dateTime('time_to_delivery',)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            //
        });
    }
}

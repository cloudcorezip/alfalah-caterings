<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScProductSellingLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_product_selling_levels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('level_name');
            $table->double('selling_price',20,3)->default(0);
            $table->unsignedBigInteger('sc_product_id');
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_product_selling_levels');
    }
}

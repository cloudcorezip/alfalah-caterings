<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPurchaseStepDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','promo_offer'))
            {
                $table->double('promo_order',20,3)->default(0);
                $table->double('promo_percentage_order',20,3)->default(0);

                $table->double('promo_delivery',20,3)->default(0);
                $table->double('promo_percentage_delivery',20,3)->default(0);

                $table->double('promo_offer',20,3)->default(0);
                $table->double('promo_percentage_offer',20,3)->default(0);
            }
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_purchase_order_details', function (Blueprint $table) {
            //
        });
    }
}

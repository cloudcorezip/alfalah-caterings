<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccJurnalOtherIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_jurnal_other_incomes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('acc_coa_detail_id');
            $table->unsignedBigInteger('sc_supplier_id')->nullable();
            $table->unsignedBigInteger('payment_acc_coa_detail_id');
            $table->unsignedBigInteger('md_sc_currency_id');
            $table->double('amount',20,3)->default(0);
            $table->unsignedBigInteger('acc_jurnal_id');
            $table->foreign('payment_acc_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('acc_jurnal_id')
                ->references('id')
                ->on('acc_jurnals');
            $table->foreign('sc_supplier_id')
                ->references('id')
                ->on('sc_suppliers');
            $table->foreign('md_sc_currency_id')
                ->references('id')
                ->on('md_sc_currencies');
            $table->foreign('acc_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_jurnal_other_incomes');
    }
}

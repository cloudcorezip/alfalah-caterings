<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefreshTokenToEmpdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enable_merchant_plugin_details', function (Blueprint $table) {
            if(!Schema::hasColumn('enable_merchant_plugin_details','refresh_token'))
            {
                $table->string('refresh_token')->nullable();
                $table->string('access_token')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enable_merchant_plugin_details', function (Blueprint $table) {
            //
        });
    }
}

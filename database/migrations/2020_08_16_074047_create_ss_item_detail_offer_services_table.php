<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsItemDetailOfferServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_item_detail_offer_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('choice_name')->nullable();
            $table->smallInteger('is_other_choice')->default(0);
            $table->unsignedBigInteger('ss_detail_offer_service_id');
            $table->foreign('ss_detail_offer_service_id')
                ->references('id')
                ->on('ss_detail_offer_services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_item_detail_offer_services');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccJurnalCashBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_jurnal_cash_banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('activity_type')->default(0);
            $table->unsignedBigInteger('from_acc_coa_detail_id');
            $table->unsignedBigInteger('to_acc_coa_detail_id');
            $table->double('amount',20,3)->default(0);
            $table->double('admin_fee',20,3)->default(0);
            $table->unsignedBigInteger('acc_jurnal_id');
            $table->foreign('from_acc_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('to_acc_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('acc_jurnal_id')
                ->references('id')
                ->on('acc_jurnals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_jurnal_cash_banks');
    }
}

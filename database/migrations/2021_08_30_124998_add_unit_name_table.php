<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitNameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_product_multi_units', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_product_multi_units','unit_name'))
            {
                $table->string('unit_name')->nullable();
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_product_multi_units', function (Blueprint $table) {
            //
        });
    }
}

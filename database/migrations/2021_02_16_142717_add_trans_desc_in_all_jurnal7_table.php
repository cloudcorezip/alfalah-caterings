<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal7Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_jurnal_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnal_details','ref_id'))
            {
                $table->unsignedBigInteger('ref_id')->nullable();
                $table->smallInteger('from_trans_type')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_jurnal_details', function (Blueprint $table) {
            //
        });
    }
}

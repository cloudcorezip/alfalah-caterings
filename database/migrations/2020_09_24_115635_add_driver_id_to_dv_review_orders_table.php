<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDriverIdToDvReviewOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dv_review_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('dv_review_orders','md_user_id_driver'))
            {
                $table->unsignedBigInteger('md_user_id_driver');
                $table->foreign('md_user_id_driver')
                    ->references('id')
                    ->on('md_users');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dv_review_orders', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdBgTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_bg_tags', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            //$table->unsignedBigInteger('name');
            //$table->unsignedBigInteger('md_bg_post_id');
            //$table->foreign('md_bg_post_id')
               // ->references('id')
               // ->on('md_bg_posts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_bg_tags');
    }
}

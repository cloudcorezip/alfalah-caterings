<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScInvProductionOfGoodBomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_inv_production_of_good_boms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_inv_production_of_good_id');
            $table->string('code');
            $table->string('name');
            $table->double('amount_of_bom',20,3)->default(0);
            $table->unsignedBigInteger('trans_coa_detail_id');
            $table->string('payment_to')->nullable();
            $table->string('payment_proof')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('sc_inv_production_of_good_id')
            ->references('id')
                ->on('sc_inv_production_of_goods');
            $table->foreign('trans_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_inv_production_of_good_boms');
    }
}

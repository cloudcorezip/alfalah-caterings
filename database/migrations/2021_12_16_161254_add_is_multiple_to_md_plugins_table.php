<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsMultipleToMdPluginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_plugins', function (Blueprint $table) {
            if(!Schema::hasColumn('md_plugins','is_multiple'))
            {
                // multiple akun
                $table->smallInteger('is_multiple')->nullable();
                $table->double('max_account',20,3)->nullable();

                // unlimited quota maupun periode
                $table->smallInteger('is_unlimited')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_plugins', function (Blueprint $table) {
            //
        });
    }
}

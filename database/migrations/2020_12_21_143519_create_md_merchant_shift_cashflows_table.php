<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantShiftCashflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_shift_cashflows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_sc_cash_type_id');
            $table->unsignedBigInteger('md_merchant_shift_id');
            $table->double('amount',20,3)->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->longText('note');
            $table->foreign('md_sc_cash_type_id')
                ->references('id')
                ->on('md_sc_cash_types');
            $table->foreign('md_merchant_shift_id')
                ->references('id')
                ->on('md_merchant_shifts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_shift_cashflows');
    }
}

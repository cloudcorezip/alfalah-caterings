<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScStockSaleMappings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_stock_sale_mappings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sc_sale_order_id');
            $table->unsignedBigInteger('sc_product_id');
            $table->unsignedBigInteger('sc_stock_inventory_id');
            $table->double('amount',20,3)->default(0);
            $table->double('purchase_price',20,3)->default(0);
            $table->foreign('sc_sale_order_id')
                ->references('id')
                ->on('sc_sale_orders');
            $table->foreign('sc_stock_inventory_id')
                ->references('id')
                ->on('sc_stock_inventories');
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_stock_sale_mappings');
    }
}

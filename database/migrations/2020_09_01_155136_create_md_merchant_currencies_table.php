<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_currencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('md_sc_currency_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_active')->default(0);
            $table->foreign('md_sc_currency_id')
                ->references('id')
                ->on('md_sc_currencies');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
        Schema::dropIfExists('sc_currencies');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_currencies');
    }
}

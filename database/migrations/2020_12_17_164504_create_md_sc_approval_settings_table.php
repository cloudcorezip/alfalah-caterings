<?php
/*
 * Senna Apps
 * Copyright (c) 2020.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdScApprovalSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_sc_approval_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('approval_name');
            $table->smallInteger('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_sc_approval_settings');
    }
}

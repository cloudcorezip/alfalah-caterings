<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShippingCategoriesJsonToMdMerchantFormOrderTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_form_order_template', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_form_order_template','shipping_categories_json')){
                $table->jsonb('shipping_categories_json')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_form_order_template', function (Blueprint $table) {
            //
        });
    }
}

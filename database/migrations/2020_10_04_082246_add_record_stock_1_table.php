<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordStock1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_retur_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_orders','type'))
            {
                $table->enum('type',['plus','minus']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneCashflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_cashflows', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_cashflows','timezone')){
                $table->string('timezone')->nullable();            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_cashflows', function (Blueprint $table) {
            //
        });
    }
}

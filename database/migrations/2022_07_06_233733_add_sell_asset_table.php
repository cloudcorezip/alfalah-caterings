<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSellAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciations','is_sell'))
            {
                $table->smallInteger('is_sell')->default(0);
                $table->double('sell_price',20,3)->default(0);
                $table->double('sell_ppn_amount',20,3)->default(0);
                $table->dateTime('sell_date')->nullable();
                $table->string('sell_code')->nullable();
                $table->string('sell_second_code')->nullable();
                $table->string('sell_trans_proof')->nullable();
                $table->bigInteger('sell_coa_detail_id')->nullable();
                $table->bigInteger('sell_coa_ppn_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBirthDateToMdUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_users', function (Blueprint $table) {
            if(!Schema::hasColumn('md_users','birth_date')){
                $table->date('birth_date')->nullable();
                $table->string('birth_place')->nullable();
                $table->smallInteger('gender')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_users', function (Blueprint $table) {
            //
        });
    }
}

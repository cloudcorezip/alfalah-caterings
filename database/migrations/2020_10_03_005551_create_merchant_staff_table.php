<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_staff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('md_user_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });

        Schema::table('md_merchants',function (Blueprint $table){
           if(Schema::hasColumn('md_merchants','is_sub_merchant')){
               $table->smallInteger('is_sub_merchant')->default(0);
           }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_staff');
    }
}

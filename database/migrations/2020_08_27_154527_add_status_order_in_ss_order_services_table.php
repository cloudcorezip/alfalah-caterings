<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusOrderInSsOrderServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_order_services','md_ss_order_status_id')){
                $table->unsignedBigInteger('md_ss_order_status_id');
                $table->unsignedBigInteger('md_transaction_status_id');
                $table->foreign('md_ss_order_status_id')
                    ->references('id')
                    ->on('md_ss_order_status');
                $table->foreign('md_transaction_status_id')
                    ->references('id')
                    ->on('md_transaction_status');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            //
        });
    }
}

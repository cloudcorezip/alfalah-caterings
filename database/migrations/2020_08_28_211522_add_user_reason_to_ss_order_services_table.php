<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserReasonToSsOrderServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_order_services','user_reason_cancellation')){
                $table->string('user_reason_cancellation')->nullable();
                $table->string('provider_reason_cancellation')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            //
        });
    }
}

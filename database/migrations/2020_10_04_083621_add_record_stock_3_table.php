<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordStock3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_orders','type'))
            {
                $table->enum('type',['plus','minus']);
            }
            if(!Schema::hasColumn('sc_retur_purchase_orders','is_retur'))
            {
                $table->smallInteger('is_retur')->default(0);
            }
        });

        Schema::table('sc_retur_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_orders','created_by'))
            {
                $table->unsignedBigInteger('created_by')->nullable();
            }
        });
        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_orders','created_by'))
            {
                $table->unsignedBigInteger('created_by')->nullable();
            }
        });

        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','created_by'))
            {
                $table->unsignedBigInteger('created_by')->nullable();
            }
        });

        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','created_by'))
            {
                $table->unsignedBigInteger('created_by')->nullable();
            }
        });

        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_inventories','created_by'))
            {
                $table->unsignedBigInteger('created_by')->nullable();
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            //
        });
    }
}

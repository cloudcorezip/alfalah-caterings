<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdVaBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_va_banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_bank_id');
            $table->longText('description')->nullable();
            $table->foreign('md_bank_id')
                ->references('id')
                ->on('md_banks');
            $table->timestamps();
        });

        Schema::table('sp_transactions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_transactions','external_id'))
            {
                $table->string('external_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_va_banks');
    }
}

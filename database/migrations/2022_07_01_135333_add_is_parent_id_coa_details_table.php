<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsParentIdCoaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_coa_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_coa_details','is_parent'))
            {
                $table->smallInteger('is_parent')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_coa_details', function (Blueprint $table) {
            //
        });
    }
}

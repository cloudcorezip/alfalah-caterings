<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVotesStartDateEndDateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_closing_journal_distributions', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_closing_journal_distributions','start_date')){
                $table->dateTime('start_date')->nullable();
                $table->dateTime('end_date')->nullable();
                $table->bigInteger('created_by')->nullable();
                $table->smallInteger('is_deleted')->default(0);
                $table->double('amount_distribution',20,3)->default(0);
            }
        });

        Schema::table('acc_closing_journal_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_closing_journal_details','acc_distribution_id')){
                $table->bigInteger('acc_distribution_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_closing_journal_distributions', function (Blueprint $table) {
            //
        });
    }
}

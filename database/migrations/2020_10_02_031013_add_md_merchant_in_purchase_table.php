<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdMerchantInPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(Schema::hasColumn('sc_purchase_orders','md_user_id'))
            {
                $table->dropColumn('md_user_id');
                $table->unsignedBigInteger('md_merchant_id');
                $table->foreign('md_merchant_id')
                    ->references('id')
                    ->on('md_merchants');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaxPromoValueToCdbpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_by_products','max_promo_value'))
            {
                $table->double('max_promo_value',20,3)->default(0);
                $table->smallInteger('select_product_by')->default(0);
                $table->smallInteger('is_not_know_end')->default(0);
                $table->smallInteger('is_daily')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            //
        });
    }
}

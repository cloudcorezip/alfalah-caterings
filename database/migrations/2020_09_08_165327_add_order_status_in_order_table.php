<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderStatusInOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_sale_orders','md_sc_order_status_id'))
            {
                $table->unsignedInteger('md_sc_order_status_id')->nullable();
                $table->foreign('md_sc_order_status_id')
                    ->references('id')
                    ->on('md_sc_order_status');

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

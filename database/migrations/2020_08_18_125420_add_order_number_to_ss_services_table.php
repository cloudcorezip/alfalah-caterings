<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderNumberToSsServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_ss_category_services', function (Blueprint $table) {
            if(!Schema::hasColumn('md_ss_category_services','order_number'))
            {
              $table->unsignedInteger('order_number')->default(0);
            }
        });

        Schema::table('md_users', function (Blueprint $table) {
            if(!Schema::hasColumn('md_users','is_active_merchant'))
            {
               $table->smallInteger('is_active_merchant')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_ss_category_services', function (Blueprint $table) {
            //
        });
    }
}

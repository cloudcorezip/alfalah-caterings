<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsForEmployeeOverOneYearToHmlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hr_merchant_leave_settings', function (Blueprint $table) {
            if(!Schema::hasColumn('hr_merchant_leave_settings','is_for_employee_over_one_year')){
                $table->smallInteger('is_for_employee_over_one_year')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hr_merchant_leave_settings', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalOffer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','total_offer'))
            {
                $table->double('total_offer',20,3)->default(0);
            }
        });

        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','tax_offer'))
            {
                $table->double('tax_offer',20,3)->default(0);
            }
        });

        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','tax_percentage_offer'))
            {
                $table->double('tax_percentage_offer',20,3)->default(0);
            }
        });

        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','step_type'))
            {
                $table->smallInteger('step_type')->default(0);
            }
        });

        Schema::table('sc_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_order_details','step_type'))
            {
                $table->smallInteger('step_type')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

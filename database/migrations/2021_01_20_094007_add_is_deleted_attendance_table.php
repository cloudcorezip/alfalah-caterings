<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDeletedAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_attendances','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
                $table->smallInteger('is_end')->default(0);
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            //
        });
    }
}

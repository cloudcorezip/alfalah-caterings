<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsAdjustmentStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_opnames','is_adjustment_stock'))
            {
                $table->smallInteger('is_adjustment_stock')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            //
        });
    }
}

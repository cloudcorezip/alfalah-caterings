<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScInvProductionOfGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_inv_production_of_goods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->unsignedBigInteger('sc_product_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('created_by');
            $table->string('timezone');
            $table->unsignedBigInteger('inv_warehouse_id');
            $table->double('amount',20,3)->default(0);
            $table->double('selling_price',20,3)->default(0);
            $table->smallInteger('is_use')->default(0);
            $table->dateTime('time_of_production');
            $table->longText('note')->nullable();
            $table->double('total',20,3)->default(0);
            $table->unsignedBigInteger('sc_stock_inventory_id')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('inv_warehouse_id')
                ->references('id')
                ->on('md_merchant_inv_warehouses');
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_inv_production_of_goods');
    }
}

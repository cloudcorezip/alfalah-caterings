<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTpPaypoinProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tp_paypoin_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parent_group')->nullable();
            $table->string('icons')->nullable();
            $table->string('sub_group')->nullable();
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->double('original_price',20,3)->default(0);
            $table->double('discount_percentage',20,3)->default(0);
            $table->double('price_markup',20,3)->default(0);
            $table->double('reduce_price',20,3)->default(0);
            $table->smallInteger('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tp_paypoin_products');
    }
}

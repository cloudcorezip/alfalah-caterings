<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsShowInSennaToScProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_products','is_show_senna_app')){
                $table->smallInteger('is_show_senna_app')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDistrictToDetailOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_order_services','md_district_id')){
                $table->unsignedBigInteger('md_district_id')->nullable();
                $table->foreign('md_district_id')
                    ->references('id')
                    ->on('md_districts');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_jurnals', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnals','trans_desc'))
            {
                $table->longText('trans_file')->nullable();
            }
        });
        Schema::table('acc_jurnal_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnal_details','trans_desc'))
            {
                $table->longText('trans_desc')->nullable();
            }
        });
        Schema::table('acc_jurnal_other_incomes', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnal_other_incomes','trans_desc'))
            {
                $table->longText('trans_desc')->nullable();
            }
        });
        Schema::table('acc_jurnal_cash_banks', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnal_cash_banks','trans_desc'))
            {
                $table->longText('trans_desc')->nullable();
            }
        });
        Schema::table('acc_jurnal_administrative_expenses', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnal_administrative_expenses','trans_desc'))
            {
                $table->longText('trans_desc')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_jurnal_details', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIncrementMonthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_subscriptions', function (Blueprint $table) {
            if(!Schema::hasColumn('md_subscriptions','plus_month'))
            {
                $table->bigInteger('plus_month')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_subscriptions', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLatitudeToMdMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','latitude'))
            {
                $table->string('latitude')->nullable();
                $table->string('longitude')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

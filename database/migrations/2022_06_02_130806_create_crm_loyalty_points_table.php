<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmLoyaltyPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_loyalty_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('get_point_method');
            $table->unsignedBigInteger('sc_product_id')->nullable();
            $table->double('min_transaction',20,3)->default(0);
            $table->double('point_earned',20,3)->default(0);
            $table->jsonb('customer_criteria')->nullable();
            $table->smallInteger('is_apply_multiple')->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_all_branch')->default(0);
            $table->jsonb('assign_to_branch')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_loyalty_points');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCodeInStockOpnameDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_stock_opname_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_opname_details','average_price'))
            {
                $table->double('average_price',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_opname_details', function (Blueprint $table) {
            //
        });
    }
}

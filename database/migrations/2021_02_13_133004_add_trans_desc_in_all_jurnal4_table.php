<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_jurnals', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnals','ref_code'))
            {
                $table->string('ref_code')->nullable();
                $table->unsignedBigInteger('sc_supplier_id')->nullable();
                $table->smallInteger('ref_type')->default(0);
            }
        });
        Schema::dropIfExists('acc_jurnal_cash_banks');
        Schema::dropIfExists('acc_jurnal_other_incomes');
        Schema::dropIfExists('acc_jurnal_administrative_expenses');

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_jurnals', function (Blueprint $table) {
            //
        });
    }
}

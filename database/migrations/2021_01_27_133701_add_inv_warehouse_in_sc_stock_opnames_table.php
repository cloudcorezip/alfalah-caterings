<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInvWarehouseInScStockOpnamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_opnames','inv_warehouse_id'))
            {
                $table->unsignedBigInteger('inv_warehouse_id')->nullable();
                $table->foreign('inv_warehouse_id')
                    ->references('id')
                    ->on('md_merchant_inv_warehouses');
            }
        });

        Schema::table('sc_transfer_stocks', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_transfer_stocks','inv_warehouse_id'))
            {
                $table->unsignedBigInteger('inv_warehouse_id')->nullable();
                $table->foreign('inv_warehouse_id')
                    ->references('id')
                    ->on('md_merchant_inv_warehouses');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            //
        });
    }
}

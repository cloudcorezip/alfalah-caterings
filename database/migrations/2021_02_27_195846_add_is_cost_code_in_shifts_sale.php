<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsCostCodeInShiftsSale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_shift_cashflows', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_shift_cashflows','shift_code'))
            {
                $table->string('shift_code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_shift_cashflows', function (Blueprint $table) {
            //
        });
    }
}

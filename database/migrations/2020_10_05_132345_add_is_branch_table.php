<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsBranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','is_branch'))
            {
                $table->smallInteger('is_branch')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

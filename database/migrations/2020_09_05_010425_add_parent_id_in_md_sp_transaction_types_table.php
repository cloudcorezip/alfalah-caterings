<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentIdInMdSpTransactionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_sp_transaction_types', function (Blueprint $table) {
            if(!Schema::hasColumn('md_cp_transaction_types','parent_id')){
                $table->unsignedBigInteger('parent_id')->nullable();
                $table->smallInteger('is_selected')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_sp_transaction_types', function (Blueprint $table) {
            //
        });
    }
}

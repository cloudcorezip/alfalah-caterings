<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRajabillerProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rajabiller_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('icons')->nullable();
            $table->string('name');
            $table->string('code')->nullable();
            $table->double('price',20,3)->default(0);
            $table->double('discount_percentage',20,3)->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_single')->default(0);
            $table->bigInteger('parent_id')->nullable();
            $table->double('admin_fee',20,3)->default(0);
            $table->double('partner_commission',20,3)->default(0);
            $table->string('sub_type')->nullable();
            $table->string('source_type')->nullable();
            $table->string('group_type')->nullable();
            $table->integer('order_number')->default(0);
            $table->string('desc')->nullable();
            $table->timestamps();
        });

        Schema::table('sp_transactions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_transactions','detail'))
            {
               $table->jsonb('detail')->nullable();
               
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rajabiller_products');
    }
}

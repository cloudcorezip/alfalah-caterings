<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdTestimonyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_testimony', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('message');
            $table->string('owner_name');
            $table->string('url');
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_deleted')->default(0);
            $table->timestamps();
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_testimony');
    }
}

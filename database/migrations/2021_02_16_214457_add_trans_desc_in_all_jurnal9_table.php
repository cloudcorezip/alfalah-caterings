<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal9Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_coa_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_coa_details','ref_external_id'))
            {
                $table->unsignedBigInteger('ref_external_id')->nullable();
                $table->smallInteger('ref_external_type')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_coa_details', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsSpecificCustomerToCdbpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_by_products','is_specific_customer'))
            {
                $table->smallInteger("is_specific_customer")->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmMerchantMembershipDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_merchant_membership_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sm_merchant_membership_id');
            $table->unsignedBigInteger('member_merchant_id');
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('sm_merchant_membership_id')
                ->references('id')
                ->on('sm_merchant_memberships');
            $table->foreign('member_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_merchant_membership_details');
    }
}

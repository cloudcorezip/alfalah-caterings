<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpOrderSettlementInsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_order_settlement_ins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sp_order_settlement_id');
            $table->unsignedBigInteger('sp_history_transaction_in_id');
            $table->foreign('sp_order_settlement_id')
                ->references('id')
                ->on('sp_order_settlements');
            $table->foreign('sp_history_transaction_in_id')
                ->references('id')
                ->on('sp_history_transaction_ins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_order_settlement_ins');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsAcceptedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_transfer_stocks', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_transfer_stocks','is_accepted'))
            {
                $table->smallInteger('is_accepted')->default(0);
                $table->unsignedBigInteger('accepted_by')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_transfer_stocks', function (Blueprint $table) {
            //
        });
    }
}

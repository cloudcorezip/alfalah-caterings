<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdUserIdStaff2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            if(Schema::hasColumn('ss_order_services','md_user_id_staff'))
            {
                $table->dropColumn('md_user_id_staff');
                $table->dropColumn('is_approved_staff');
            }
        });
        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_order_services','md_user_id_staff'))
            {
                $table->unsignedBigInteger('md_user_id_staff')->nullable();
                $table->smallInteger('is_approved_staff')->default(0);
                $table->foreign('md_user_id_staff')
                    ->references('id')
                    ->on('md_users');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            //
        });
    }
}

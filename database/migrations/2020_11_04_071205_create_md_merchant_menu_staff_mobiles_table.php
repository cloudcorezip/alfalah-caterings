<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantMenuStaffMobilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_menu_staff_mobiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('md_user_id_staff');
            $table->longText('menus');
            $table->smallInteger('is_deleted')->default(0);
            $table->string('sync_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('md_user_id_staff')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_menu_staff_mobiles');
    }
}

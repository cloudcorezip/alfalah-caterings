<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_grade_staffs', function (Blueprint $table) {
            if(!Schema::hasColumn('merchant_grade_staffs','parent_id'))
            {
                $table->bigInteger('parent_id')->nullable();
            }
        });

        Schema::table('hr_merchant_departements', function (Blueprint $table) {
            if(!Schema::hasColumn('hr_merchant_departements','parent_id'))
            {
                $table->bigInteger('parent_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_grade_staffs', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQrisBankId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {                
        Schema::table('qris_activations', function (Blueprint $table) {
            if(!Schema::hasColumn('qris_activations','qrisdetail_qris_id'))
            {
                $table->unsignedBigInteger('qrisdetail_qris_id')->nullable();
                $table->foreign('qrisdetail_qris_id')
                ->references('id')
                ->on('acc_coa_details');
                $table->unsignedBigInteger('qrisbank_qris_id')->nullable();
                $table->foreign('qrisbank_qris_id')
                ->references('id')
                ->on('acc_coa_details');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qris_activations', function (Blueprint $table) {
            //
        });
    }
}

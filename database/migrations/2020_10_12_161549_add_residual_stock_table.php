<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResidualStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_inventories','residual_stock'))
            {
                $table->unsignedBigInteger('residual_stock')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            //
        });
    }
}

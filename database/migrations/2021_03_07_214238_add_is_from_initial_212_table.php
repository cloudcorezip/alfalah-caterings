<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsFromInitial212Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','is_editable'))
            {
                $table->smallInteger('is_editable')->default(1);
            }
        });
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','is_editable'))
            {
                $table->smallInteger('is_editable')->default(1);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            //
        });
    }
}

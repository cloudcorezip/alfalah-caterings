<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccMerchantTempAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('acc_merchant_temp_amounts'))
        {
            Schema::create('acc_merchant_temp_amounts', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('md_merchant_id');
                $table->smallInteger('type')->default(0);
                $table->date('time');
                $table->double('amount',20,3)->default(0);
                $table->smallInteger('is_deleted',)->default(0);
                $table->foreign('md_merchant_id')
                    ->references('id')
                    ->on('md_merchants');
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_merchant_temp_amounts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShiftRoasterIdToMdMerchantStaffAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_attendances','shift_roaster_id'))
            {
                $table->unsignedBigInteger('shift_roaster_id')->nullable();
                $table->text('note')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            //
        });
    }
}

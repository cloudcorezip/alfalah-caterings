<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLatitudeMdUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_user_address', function (Blueprint $table) {
            if(!Schema::hasColumn('md_user_address','latitude'))
            {
                $table->string('latitude');
                $table->string('longitude');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_user_address', function (Blueprint $table) {
            //
        });
    }
}

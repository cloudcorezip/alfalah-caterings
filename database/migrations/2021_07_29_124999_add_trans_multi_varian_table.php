<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransMultiVarianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_order_details','product_multi_varian_id'))
            {
                $table->bigInteger('is_multi_varian')->default(0);
                $table->string('varian_name')->nullable();
                
            }
        });
        Schema::table('sc_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_order_details','product_multi_varian_id'))
            {
                $table->bigInteger('is_multi_varian')->default(0);
                $table->string('varian_name')->nullable();
            }
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccMerchantApTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_merchant_ap', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('apable_id');
            $table->string('apable_type');
            $table->unsignedBigInteger('md_merchant_id');
            $table->double('residual_amount',20,3)->default(0);
            $table->double('paid_nominal',20,3)->default(0);
            $table->smallInteger('is_paid_off')->default(0);
            $table->date('due_date')->nullable();
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_merchant_ap');
    }
}

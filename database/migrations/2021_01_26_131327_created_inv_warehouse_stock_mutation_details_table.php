<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatedInvWarehouseStockMutationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_warehouse_stock_mutation_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('quantity',20,3)->default(0);
            $table->double('selling_price',20,3)->default(0);
            $table->double('purchase_price',20,3)->default(0);
            $table->double('sub_total',20,3)->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->unsignedBigInteger('sc_product_id');
            $table->unsignedBigInteger('inv_warehouse_stock_mutation_id');
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('inv_warehouse_stock_mutation_id')
                ->references('id')
                ->on('inv_warehouse_stock_mutations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_warehouse_stock_mutation_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsRatingToTableDetailOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_order_services','is_rating'))
            {
                $table->smallInteger('is_rating')->default(0);
            }
        });

        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_order_details','is_rating'))
            {
                $table->smallInteger('is_rating')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            //
        });
    }
}

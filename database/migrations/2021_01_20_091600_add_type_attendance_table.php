<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_attendances','type_of_attendance'))
            {
                $table->enum('type_of_attendance',['H','I','S','A']);
            }
        });

        Schema::table('md_merchant_staff_permit_submissions', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_permit_submissions','type_of_attendance'))
            {
                $table->enum('type_of_attendance',['H','I','S','A']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantCashdrawersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_cashdrawers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('desc')->nullable();
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->unsignedBigInteger('md_merchant_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_cashdrawers');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdBusinessCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_business_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('parent_id')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->timestamps();
        });
        Schema::table('md_merchants', function (Blueprint $table) {
            
            if (!Schema::hasColumn('md_merchants', 'md_business_category_id')) {

                $table->bigInteger('md_business_category_id')->nullable();
            }  
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_business_categories');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScPurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->unsignedBigInteger('sc_supplier_id');
            $table->double('total',20,2)->default(0);
            $table->unsignedBigInteger('md_user_id');
            $table->unsignedBigInteger('md_sc_transaction_status_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('sc_supplier_id')
                ->references('id')
                ->on('sc_suppliers');
            $table->foreign('md_sc_transaction_status_id')
                ->references('id')
                ->on('md_sc_transaction_status');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_purchase_orders');
    }
}

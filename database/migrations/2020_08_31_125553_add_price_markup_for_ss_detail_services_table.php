<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriceMarkupForSsDetailServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_detail_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_services','price_markup'))
            {
                $table->double('price_markup',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_detail_services', function (Blueprint $table) {
            //
        });
    }
}

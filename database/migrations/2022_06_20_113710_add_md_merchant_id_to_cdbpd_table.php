<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdMerchantIdToCdbpdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_by_product_details', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_by_product_details','md_merchant_id'))
            {
                $table->unsignedBigInteger('md_merchant_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_by_product_details', function (Blueprint $table) {
            //
        });
    }
}

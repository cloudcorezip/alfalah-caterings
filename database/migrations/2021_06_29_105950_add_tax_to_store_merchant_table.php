<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaxToStoreMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_merchant_configs', function (Blueprint $table) {
            if(!Schema::hasColumn('store_merchant_configs','tax_percentage'))
            {
                $table->double('tax_percentage',20,3)->default(0);
            }
        });

        Schema::table('store_merchant_details', function (Blueprint $table) {
            if(!Schema::hasColumn('store_merchant_details','discount_percentage'))
            {
                $table->double('discount_percentage',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_merchant_configs', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneOrders2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

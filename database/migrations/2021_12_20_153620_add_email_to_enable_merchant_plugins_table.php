<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailToEnableMerchantPluginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enable_merchant_plugins', function (Blueprint $table) {
            if(!Schema::hasColumn('enable_merchant_plugins','email'))
            {
                $table->string('email')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enable_merchant_plugins', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcommerceProductVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_product_variations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('blibli_variation_name')->nullable();
            $table->string('elevenia_variation_name')->nullable();
            $table->string('bukalapak_variation_name')->nullable();
            $table->string('shopee_variation_name')->nullable();
            $table->string('lazada_variation_name')->nullable();
            $table->string('tiktok_variation_name')->nullable();
            $table->string('tokopedia_variation_name')->nullable();
            $table->string('zalora_variation_name')->nullable();
            $table->string('zilingo_variation_name')->nullable();
            $table->string('variation_id')->nullable();
            $table->string('variation_name')->nullable();
            $table->string('category_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_product_variations');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDrafTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_opnames','is_draf'))
            {
                $table->smallInteger('is_draf')->default(0);
            }
        });

        Schema::table('sc_stock_opname_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_opname_details','is_damage'))
            {
                $table->smallInteger('is_damage')->default(0);
                $table->smallInteger('is_expired')->default(0);
                $table->double('qty_damage',20,3)->default(0);
                $table->double('qty_expired',20,3)->default(0);

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderInMdSpGuidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_sp_guides', function (Blueprint $table) {
            if(!Schema::hasColumn('md_sp_guides','order_number')){
                $table->integer('order_number')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_sp_guides', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTpPaypoinTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tp_paypoin_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('trans_name');
            $table->unsignedBigInteger('tp_paypoin_product_id');
            $table->string('tp_paypoin_product_code');
            $table->string('number_id');
            $table->double('capital_price',20,3)->default(0);
            $table->double('selling_price',20,3)->default(0);
            $table->double('profit_price',20,3)->default(0);
            $table->double('admin_fee',20,3)->default(0);
            $table->double('discount_price',20,3)->default(0);
            $table->string('unique_id');
            $table->string('status')->nullable();
            $table->jsonb('inq_response')->nullable();
            $table->jsonb('pay_response')->nullable();
            $table->jsonb('xendit_checkout_response')->nullable();
            $table->jsonb('xendit_pay_response')->nullable();
            $table->timestamp('trans_time')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->unsignedBigInteger('transaction_type_id')->nullable();
            $table->string('transaction_type_name')->nullable();
            $table->foreign('tp_paypoin_product_id')
                ->references('id')
                ->on('tp_paypoin_products');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tp_paypoin_transactions');
    }
}

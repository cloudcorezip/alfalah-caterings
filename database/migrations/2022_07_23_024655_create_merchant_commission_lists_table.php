<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantCommissionListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_commission_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('commission_id');
            $table->string('commission_name');
            $table->unsignedBigInteger('staff_id');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->jsonb('commission_value')->nullable();
            $table->double('sale_amount',20,3)->default(0);
            $table->string('type');
            $table->string('value_type');
            $table->string('type_of_value_commissions');
            $table->jsonb('detail_commissions')->nullable();
            $table->unsignedBigInteger('sc_sale_order_id')->nullable();
            $table->string('timezone');
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_paid')->default(0);
            $table->unsignedBigInteger('payment_paid_id')->nullable();
            $table->dateTime('payment_paid_date')->nullable();
            $table->unsignedBigInteger('payment_user_created')->nullable();
            $table->string('payment_timezone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_commission_lists');
    }
}

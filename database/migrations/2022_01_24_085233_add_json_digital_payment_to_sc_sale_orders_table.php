<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJsonDigitalPaymentToScSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','json_order_online')){
                $table->jsonb('json_order_online')->nullable();
                $table->bigInteger('md_merchant_payment_method_id')->nullable();
                $table->string('url_checkout')->nullable();
                $table->jsonb('checkout_response')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

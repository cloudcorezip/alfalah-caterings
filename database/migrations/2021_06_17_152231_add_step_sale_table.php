<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStepSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','step_type'))
            {
                $table->smallInteger('step_type')->default(0);
                $table->smallInteger('is_step_close')->default(0);
                
                $table->double('total_order',20,3)->default(0);
                $table->double('promo_order',20,3)->default(0);
                $table->double('promo_percentage_order',20,3)->default(0);
                $table->double('tax_order',20,3)->default(0);
                $table->double('tax_percentage_order',20,3)->default(0);
                $table->dateTime('time_to_order',)->nullable();
                
                $table->double('total_delivery',20,3)->default(0);
                $table->double('promo_delivery',20,3)->default(0);
                $table->double('promo_percentage_delivery',20,3)->default(0);
                $table->double('tax_delivery',20,3)->default(0);
                $table->double('tax_percentage_delivery',20,3)->default(0);
                $table->dateTime('time_to_delivery',)->nullable();
                
                $table->double('total_offer',20,3)->default(0);
                $table->double('promo_offer',20,3)->default(0);
                $table->double('promo_percentage_offer',20,3)->default(0);
                $table->double('tax_offer',20,3)->default(0);
                $table->double('tax_percentage_offer',20,3)->default(0);
                $table->dateTime('time_to_offer',)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

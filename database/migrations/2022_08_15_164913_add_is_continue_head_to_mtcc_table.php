<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsContinueHeadToMtccTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_transaction_code_configs', function (Blueprint $table) {
            if(!Schema::hasColumn('merchant_transaction_code_configs','is_continue_head')){
                $table->smallInteger('is_continue_head')->default(1);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_transaction_code_configs', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUuidForLocalDbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('md_merchant_staff', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('md_merchant_branchs', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_branchs','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });
        Schema::table('md_merchant_category_groups', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_category_groups','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('md_merchant_currencies', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_currencies','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('md_merchant_range_business', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_range_business','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('md_merchant_service_availables', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_service_availables','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('md_merchant_shop_categories', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_shop_categories','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_customers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_customers','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_merchant_discounts', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_merchant_discounts','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_merchant_fd', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_merchant_fd','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_merks', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_merks','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_product_categories', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_product_categories','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_products', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_products','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });
        Schema::table('sc_promo_products', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_promo_products','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_retur_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_orders','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_orders','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_inventories','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_suppliers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_suppliers','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_temp_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_temp_purchase_order_details','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });

        Schema::table('sc_temp_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_temp_sale_order_details','sync_id'))
            {
                $table->string('sync_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_branchs', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuantityToCdbpdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_by_product_details', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_by_product_details','quantity'))
            {
                $table->double('quantity',20,3)->default(1);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_by_product_details', function (Blueprint $table) {
            //
        });
    }
}

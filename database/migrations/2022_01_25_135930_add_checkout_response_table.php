<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCheckoutResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar_details','checkout_response')){
                $table->jsonb('checkout_response')->nullable();
                $table->string('qr_code')->nullable();
                $table->smallInteger('is_payment_gateway')->default(0);
                $table->jsonb('array_insert')->nullable();
                $table->dateTime('expired_time')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            //
        });
    }
}

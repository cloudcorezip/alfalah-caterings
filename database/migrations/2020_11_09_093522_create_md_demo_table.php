<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdDemoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_demo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname');
            $table->string('email');
            $table->string('telp');
            $table->string('company_name');
            $table->unsignedBigInteger('md_cities_id');
            $table->unsignedBigInteger('md_sc_categories_id');
            $table->string('product');
            $table->string('demo_type');
            $table->string('demo_date');
            $table->string('description');
            $table->foreign('md_sc_categories_id')
                ->references('id')
                ->on('md_sc_categories');
            $table->foreign('md_cities_id')
                ->references('id')
                ->on('md_cities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_demo');
    }
}

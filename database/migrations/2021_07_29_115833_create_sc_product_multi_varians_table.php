<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScProductMultiVariansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_product_multi_varians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_product_id');
            $table->double('purchase_price',20,3)->default(0);
            $table->double('selling_price',20,3)->default(0);
            $table->double('stock',20,3)->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_product_multi_varians');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStartClockToCdbpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_by_products','start_clock')){
                $table->time('start_clock')->nullable();
                $table->time('end_clock')->nullable();
                $table->jsonb('select_product_by_value')->nullable();
                $table->jsonb('customer_terms')->nullable();
                $table->jsonb('assign_to_branch')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccClosingJournalDistributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_closing_journal_distributions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->double('debit_amount',20,3)->default(0);
            $table->double('kredit_amount',20,3)->default(0);
            $table->double('end_amount',20,3)->default(0);
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('acc_period_id');
            $table->smallInteger('is_distribution')->default(0);
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('acc_period_id')
                ->references('id')
                ->on('acc_periods');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_closing_journal_distributions');
    }
}

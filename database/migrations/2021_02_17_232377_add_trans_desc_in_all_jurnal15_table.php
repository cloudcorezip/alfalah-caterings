<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal15Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','ap_name'))
            {
                $table->string('ap_name')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            //
        });
    }
}

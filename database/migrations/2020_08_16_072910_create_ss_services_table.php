<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('md_user_id_created');
            $table->string('description')->nullable();
            $table->smallInteger('is_single')->default(0);
            $table->smallInteger('is_with_offer')->default(0);
            $table->unsignedBigInteger('md_ss_category_service_id');
            $table->foreign('md_user_id_created')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_ss_category_service_id')
                ->references('id')
                ->on('md_ss_category_services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_services');
    }
}

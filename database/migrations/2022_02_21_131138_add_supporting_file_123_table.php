<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSupportingFile123Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sfa_visit_schedules', function (Blueprint $table) {
            if(!Schema::hasColumn('sfa_visit_schedules','supporting_file_2')){
                $table->string('supporting_file_2')->nullable();
                $table->string('supporting_file_3')->nullable();
                $table->string('supporting_file_checkout_2')->nullable();
                $table->string('supporting_file_checkout_3')->nullable();


            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sfa_visit_schedules', function (Blueprint $table) {
            //
        });
    }
}

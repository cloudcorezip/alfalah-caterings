<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccCoaDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_coa_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name');
            $table->enum('type',['Debit','Kredit','-']);
            $table->unsignedBigInteger('md_merchant_currency_id');
            $table->unsignedBigInteger('acc_coa_category_id');
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('md_merchant_currency_id')
                ->references('id')
                ->on('md_merchant_currencies');
            $table->foreign('acc_coa_category_id')
                ->references('id')
                ->on('acc_coa_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_coa_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminFeeInOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sp_history_transaction_outs', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_history_transaction_outs','admin_fee'))
            {
                $table->double('admin_fee',20,2)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sp_history_transaction_outs', function (Blueprint $table) {
            //
        });
    }
}

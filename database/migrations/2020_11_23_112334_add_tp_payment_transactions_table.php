<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTpPaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sp_payment_subscriptions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_payment_subscriptions','md_transaction_type_id'))
            {
                $table->unsignedBigInteger('md_transaction_type_id')->nullable();
                $table->foreign('md_transaction_type_id')
                    ->references('id')
                    ->on('md_transaction_types');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sp_payment_transactions', function (Blueprint $table) {
            //
        });
    }
}

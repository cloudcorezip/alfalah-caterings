<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdTransactionTypeIdInPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','md_transaction_type_id'))
            {
                $table->string('qr_code')->nullable();
                $table->double('paid_nominal',20,3)->default(0);
                $table->unsignedBigInteger('md_transaction_type_id');
                $table->foreign('md_transaction_type_id')
                    ->references('id')
                    ->on('md_transaction_types');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            //
        });
    }
}

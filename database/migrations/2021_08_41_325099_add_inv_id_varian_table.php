<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInvIdVarianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_product_multi_varians', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_product_multi_varians','inv_id'))
            {
                $table->unsignedBigInteger('inv_id')->nullable();
                $table->unsignedBigInteger('hpp_id')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_product_multi_varians', function (Blueprint $table) {
            //
        });
    }
}

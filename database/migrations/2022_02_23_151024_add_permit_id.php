<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPermitId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_attendances','permit_id')){
                $table->unsignedBigInteger('permit_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpHistoryTransactionOutBetweenUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_history_transaction_out_between_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('to_md_user_id');
            $table->unsignedBigInteger('sp_history_transaction_out_id');
            $table->foreign('to_md_user_id','to_user_id_foreign')
                ->references('id')
                ->on('md_users');
            $table->foreign('sp_history_transaction_out_id','tf_out_from_between_user_id_foreign')
                ->references('id')
                ->on('sp_history_transaction_outs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_history_transaction_out_between_users');
    }
}

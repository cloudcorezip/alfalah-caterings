<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSecondCodeAllTableTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_jurnals', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnals','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciations','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('acc_asset_depreciation_payments', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciation_payments','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('sc_stock_opnames', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_opnames','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('sc_transfer_stocks', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_transfer_stocks','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('acc_merchant_ap_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap_details','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar_details','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('sc_consignment_goods', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_consignment_goods','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('sc_inv_production_of_goods', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_inv_production_of_goods','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });


        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('sc_retur_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_orders','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });

        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_orders','second_code'))
            {
                $table->string('second_code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_jurnals', function (Blueprint $table) {
            //
        });
    }
}

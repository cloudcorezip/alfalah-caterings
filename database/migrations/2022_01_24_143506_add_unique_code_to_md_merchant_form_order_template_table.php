<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueCodeToMdMerchantFormOrderTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_form_order_template', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_form_order_template','unique_code')){
                $table->string('unique_code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_form_order_template', function (Blueprint $table) {
            //
        });
    }
}

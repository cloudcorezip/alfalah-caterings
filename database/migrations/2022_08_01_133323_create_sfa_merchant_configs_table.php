<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSfaMerchantConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sfa_merchant_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('allow_change_sale_price')->default(0);
            $table->smallInteger('allow_add_discount')->default(0);
            $table->smallInteger('is_use_sale_target')->default(0);
            $table->smallInteger('is_use_visit_schedule')->default(0);
            $table->smallInteger('allow_created_po')->default(1);
            $table->smallInteger('allow_created_invoice')->default(0);
            $table->smallInteger('allow_change_customer')->default(0);
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sfa_merchant_configs');
    }
}

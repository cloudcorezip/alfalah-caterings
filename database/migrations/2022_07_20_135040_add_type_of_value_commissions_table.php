<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeOfValueCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_commissions', function (Blueprint $table) {
            if(!Schema::hasColumn('merchant_commissions','type_of_value_commissions')){
                $table->string('type_of_value_commissions')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_commissions', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminFeeConfigForServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_app_services', function (Blueprint $table) {
            if(!Schema::hasColumn('md_app_services','admin_fee_config')){
                $table->double('admin_fee_config',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_app_services', function (Blueprint $table) {
            //
        });
    }
}

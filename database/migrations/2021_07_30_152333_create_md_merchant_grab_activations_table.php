<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantGrabActivationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_grab_activations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->string('business_email');
            $table->string('business_phone_number');
            $table->string('grab_food_name');
            $table->string('business_type_name');
            $table->string('business_scalable');
            $table->string('business_revenue_average');
            $table->string('business_address');
            $table->string('owner_name');
            $table->string('owner_email');
            $table->string('owner_phone_number');
            $table->string('business_legal_type');
            $table->string('owner_name_1');
            $table->string('owner_address');
            $table->string('owner_identity_card_number');
            $table->string('owner_phone_number_1');
            $table->date('owner_birthdate');
            $table->string('countries_name');
            $table->string('job_level');
            $table->jsonb('owner_file');
            $table->string('bank_name');
            $table->string('bank_number');
            $table->string('bank_account_name');
            $table->string('bank_branch');
            $table->string('bank_file');
            $table->smallInteger('is_same_banK_name')->default(0);
            $table->string('city_name');
            $table->string('location_type');
            $table->string('location_benchmark');
            $table->jsonb('outlet_foto');
            $table->jsonb('outlet_type_food');
            $table->jsonb('outlet_tag_food');
            $table->string('food_classification');
            $table->smallInteger('is_with_tax')->default(0);
            $table->string('outlet_detail');
            $table->string('time_pickup')->nullable();
            $table->jsonb('menu_list');
            $table->jsonb('food_list');
            $table->jsonb('time_operational');
            $table->string('npwp_number')->nullable();
            $table->string('npwp_file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_grab_activations');
    }
}

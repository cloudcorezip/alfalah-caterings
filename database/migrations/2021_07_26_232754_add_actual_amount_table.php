<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActualAmountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_shifts', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_shifts','actual_amount'))
            {
                $table->double('actual_amount',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_shifts', function (Blueprint $table) {
            //
        });
    }
}

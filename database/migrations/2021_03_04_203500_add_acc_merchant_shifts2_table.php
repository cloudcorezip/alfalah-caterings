<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccMerchantShifts2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_shifts', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_shifts','trans_coa_detail_id'))
            {
                $table->unsignedBigInteger('trans_coa_detail_id')->nullable();
            }
        });
        Schema::table('acc_merchant_shifts', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_shifts','to_coa_detail_id'))
            {
                $table->unsignedBigInteger('to_coa_detail_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_merchant_shifts');
    }
}

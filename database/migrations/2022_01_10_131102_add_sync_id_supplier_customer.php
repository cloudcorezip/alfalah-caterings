<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSyncIdSupplierCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_customers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_customers','sync_id')){
                $table->string('sync_id')->nullable();
            }
        });
        Schema::table('sc_suppliers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_suppliers','sync_id')){
                $table->string('sync_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_customers', function (Blueprint $table) {
            //
        });
    }
}

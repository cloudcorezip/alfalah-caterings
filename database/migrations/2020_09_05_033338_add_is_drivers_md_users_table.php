<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDriversMdUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_users', function (Blueprint $table) {
            if(!Schema::hasColumn('md_users','is_driver'))
            {
                $table->smallInteger('is_driver')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_users', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcommerceProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_product_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category_id');
            $table->string('full_category_name');
            $table->string('bukalapak_category_id')->nullable();
            $table->string('bukalapak_category_name')->nullable();
            $table->string('bukalapak_full_category_name')->nullable();
            $table->string('lazada_category_id')->nullable();
            $table->string('lazada_category_name')->nullable();
            $table->string('lazada_full_category_name')->nullable();
            $table->string('zalora_category_id')->nullable();
            $table->string('zalora_category_name')->nullable();
            $table->string('zalora_full_category_name')->nullable();
            $table->string('elevenia_category_id')->nullable();
            $table->string('elevenia_category_name')->nullable();
            $table->string('elevenia_full_category_name')->nullable();
            $table->string('blibli_category_id')->nullable();
            $table->string('blibli_category_name')->nullable();
            $table->string('blibli_full_category_name')->nullable();
            $table->string('shopee_category_id')->nullable();
            $table->string('shopee_category_name')->nullable();
            $table->string('shopee_full_category_name')->nullable();
            $table->string('tokopedia_category_id')->nullable();
            $table->string('tokopedia_category_name')->nullable();
            $table->string('tokopedia_full_category_name')->nullable();
            $table->string('blanja_category_id')->nullable();
            $table->string('blanja_category_name')->nullable();
            $table->string('blanja_full_category_name')->nullable();
            $table->string('zilingo_category_id')->nullable();
            $table->string('zilingo_category_name')->nullable();
            $table->string('zilingo_full_category_name')->nullable();
            $table->string('akulaku_category_id')->nullable();
            $table->string('akulaku_category_name')->nullable();
            $table->string('akulaku_full_category_name')->nullable();
            $table->string('tiktok_category_id')->nullable();
            $table->string('tiktok_category_name')->nullable();
            $table->string('tiktok_full_category_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_product_categories');
    }
}

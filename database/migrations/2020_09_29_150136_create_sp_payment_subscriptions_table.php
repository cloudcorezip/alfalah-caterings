<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpPaymentSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_payment_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('md_subscription_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('md_subscription_id')
                ->references('id')
                ->on('md_subscriptions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_payment_subscriptions');
    }
}

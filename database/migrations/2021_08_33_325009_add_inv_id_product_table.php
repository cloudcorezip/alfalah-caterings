<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInvIdProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_products','inv_id'))
            {
                $table->unsignedBigInteger('inv_id')->nullable();
                $table->unsignedBigInteger('hpp_id')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            //
        });
    }
}

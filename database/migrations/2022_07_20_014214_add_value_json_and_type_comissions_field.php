<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddValueJsonAndTypeComissionsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_commissions', function (Blueprint $table) {
            if(!Schema::hasColumn('merchant_commissions','value_commissions')){
                $table->jsonb('value_commissions')->nullable();
                $table->smallInteger('is_for_employee')->default(1);
                $table->smallInteger('is_multiple')->default(0);
            }
        });

        Schema::table('md_merchant_staff', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff','is_non_employee')){
                $table->smallInteger('is_non_employee')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_commissions', function (Blueprint $table) {
            //
        });
    }
}

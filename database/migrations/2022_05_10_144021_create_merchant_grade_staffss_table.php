<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantGradeStaffssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_grade_staffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('grade_name');
            $table->longText('desc')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->jsonb('md_merchant_id')->nullable();
            $table->double('basic_salary',20,3)->default(0);
            $table->unsignedBigInteger('md_user_id');
            $table->foreign('md_user_id')->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_grade_staffs');
    }
}

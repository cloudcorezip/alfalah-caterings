<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDistrictToMdMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(Schema::hasColumn('md_merchants','subdistrict_id_from_raja_ongkir'))
            {
                $table->dropColumn('subdistrict_id_from_raja_ongkir');
                $table->dropColumn('city_id_from_raja_ongkir');
                $table->dropColumn('province_id_from_raja_ongkir');
                $table->dropColumn('subdistrict_name');
                $table->dropColumn('city_name');
                $table->dropColumn('province_name');
            }

            if(!Schema::hasColumn('md_merchants','md_district_id')){
                $table->unsignedBigInteger('md_district_id')->nullable();
                $table->foreign('md_district_id')
                    ->references('id')
                    ->on('md_districts');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

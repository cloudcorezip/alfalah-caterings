<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_jurnal_other_incomes', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnal_other_incomes','trans_code'))
            {
                $table->longText('trans_code')->nullable();
            }
        });
        Schema::table('acc_jurnal_cash_banks', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnal_cash_banks','trans_code'))
            {
                $table->longText('trans_code')->nullable();
            }
        });
        Schema::table('acc_jurnal_administrative_expenses', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnal_administrative_expenses','trans_code'))
            {
                $table->longText('trans_code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_jurnal_details', function (Blueprint $table) {
            //
        });
    }
}

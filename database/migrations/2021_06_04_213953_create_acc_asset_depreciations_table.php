<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccAssetDepreciationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_asset_depreciations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('asset_name');
            $table->unsignedBigInteger('acc_asset_category_id');
            $table->unsignedBigInteger('acc_coa_asset_id');
            $table->unsignedBigInteger('sc_supplier_id')->nullable();
            $table->dateTime('record_time');
            $table->double('asset_value',20,3)->default(0);
            $table->string('trans_proof')->nullable();
            $table->longText('desc')->nullable();
            $table->smallInteger('is_depreciation')->default(0);
            $table->unsignedBigInteger('acc_md_property_group_id')->nullable();
            $table->unsignedBigInteger('acc_md_depreciation_method_id')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->double('benefit_period',20,3)->default(0);
            $table->double('value_depreciation',20,3)->default(0);
            $table->unsignedBigInteger('account_depreciation_id')->nullable();
            $table->unsignedBigInteger('account_accumulation_id')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->smallInteger('is_from_initial')->default(0);
            $table->foreign('acc_asset_category_id')
                ->references('id')
                ->on('acc_asset_categories');
            $table->foreign('acc_coa_asset_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('sc_supplier_id')
                ->references('id')
                ->on('sc_suppliers');
            $table->foreign('acc_md_property_group_id')
                ->references('id')
                ->on('acc_md_property_groups');
            $table->foreign('acc_md_depreciation_method_id')
                ->references('id')
                ->on('acc_md_depreciation_methods');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('account_depreciation_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('account_accumulation_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_asset_depreciations');
    }
}

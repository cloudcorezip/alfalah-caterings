<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultiUnitQuantity1InOpnamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_stock_opname_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_opname_details','multi_quantity')){
                $table->double('multi_quantity',20,3)->default(0);
                $table->string('unit_name_origin_display')->nullable();
            }
        });

        Schema::table('sc_transfer_stock_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_transfer_stock_details','multi_quantity')){
                $table->double('multi_quantity',20,3)->default(0);
                $table->string('unit_name_origin_display')->nullable();

            }
        });

        Schema::table('sc_consignment_good_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_consignment_good_details','multi_quantity')){
                $table->double('multi_quantity',20,3)->default(0);
                $table->string('unit_name_origin_display')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_opname_details', function (Blueprint $table) {
            //
        });
    }
}

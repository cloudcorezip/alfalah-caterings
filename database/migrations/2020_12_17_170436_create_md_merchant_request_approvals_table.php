<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantRequestApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_request_approvals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_staff_user_id');
            $table->unsignedBigInteger('md_merchant_approval_setting_id');
            $table->longText('request_desc')->nullable();
            $table->string('ref_id');
            $table->smallInteger('is_approval')->default(0);
            $table->smallInteger('staff_action')->default(0);
            $table->foreign('md_staff_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_merchant_approval_setting_id')
                ->references('id')
                ->on('md_merchant_approval_settings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_request_approvals');
    }
}

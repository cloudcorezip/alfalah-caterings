<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdSpShippingCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_sc_shipping_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('icons')->nullable();
            $table->string('alias')->nullable();
            $table->integer('order_number')->default(0);
            $table->smallInteger('is_active')->default(1);
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->smallInteger('is_selected')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_sc_shipping_categories');
    }
}

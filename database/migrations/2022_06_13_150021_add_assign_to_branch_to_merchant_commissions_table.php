<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssignToBranchToMerchantCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_commissions', function (Blueprint $table) {
            if(!Schema::hasColumn('merchant_commissions','assign_to_branch'))
            {
                $table->jsonb('assign_to_branch')->nullable();
                $table->smallInteger('is_all_branch')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_commissions', function (Blueprint $table) {
            //
        });
    }
}

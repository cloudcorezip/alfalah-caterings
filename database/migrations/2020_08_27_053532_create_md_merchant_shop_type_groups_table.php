<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantShopTypeGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_shop_type_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('md_shop_type_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('md_shop_type_id')
                ->references('id')
                ->on('md_shop_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_shop_type_groups');
    }
}

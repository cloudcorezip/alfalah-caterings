<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmDiscountCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_discount_coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code');
            $table->unsignedBigInteger('md_merchant_id');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->smallInteger('is_daily')->nullable();
            $table->time('start_hour')->nullable();
            $table->time('end_hour')->nullable();
            $table->double('min_transaction',20,3)->default(0);
            $table->smallInteger('bonus_type')->nullable();
            $table->double('bonus_value',20,3)->default(0);
            $table->smallInteger('is_limited')->default(0);
            $table->double('max_coupon_use',20,3)->default(0);
            $table->double('max_coupon_use_per_customer',20,3)->default(0);
            $table->jsonb('customer_criteria')->nullable();
            $table->jsonb('product_criteria')->nullable();
            $table->jsonb('assign_to_branch')->nullable();
            $table->smallInteger('is_all_branch')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_discount_coupons');
    }
}

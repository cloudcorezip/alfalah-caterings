<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPricePurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_transfer_stock_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_transfer_stock_details','purchase_price'))
            {
                $table->double('purchase_price',20,3)->default(0);
                $table->double('selling_price',20,3)->default(0);
                $table->dropColumn('price');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_transfer_stock_details', function (Blueprint $table) {
            //
        });
    }
}

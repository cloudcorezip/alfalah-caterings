<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinic_reservations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('reservation_number');
            $table->unsignedBigInteger('md_merchant_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');

            $table->unsignedBigInteger('sc_customer_id');
            $table->foreign('sc_customer_id')
                    ->references('id')
                    ->on('sc_customers');

            $table->unsignedBigInteger('md_merchant_staff_id');
            $table->foreign('md_merchant_staff_id')
                    ->references('id')
                    ->on('md_merchant_staff');

            $table->unsignedBigInteger('sc_product_id');
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            
            $table->timestamp('reservation_date');
            $table->smallInteger('status')->default(0);
            $table->smallInteger('is_deleted')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_reservations');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantStaffMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_staff_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_menu_id');
            $table->unsignedBigInteger('md_merchant_staff_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->foreign('md_merchant_menu_id')
                ->references('id')
                ->on('md_merchant_menus');
            $table->foreign('md_merchant_staff_id')
                ->references('id')
                ->on('md_merchant_staff');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_staff_menus');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal19Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','ar_name'))
            {
                $table->string('ar_name')->nullable();
            }
        });
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','ar_purpose'))
            {
                $table->string('ar_purpose')->nullable();
            }
        });
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','ar_time'))
            {
                $table->dateTime('ar_time')->nullable();
            }
        });
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','ar_proof'))
            {
                $table->string('ar_proof')->nullable();
            }
        });
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','ar_acc_coa'))
            {
                $table->unsignedBigInteger('ar_acc_coa')->nullable();
            }
        });
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','ar_amount'))
            {
                $table->double('ar_amount')->nullable();
            }
        });
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','is_deleted'))
            {
                $table->smallInteger('is_deleted')->nullable();
            }
        });
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar','ar_code'))
            {
                $table->string('ar_code')->nullable();
            }
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ar', function (Blueprint $table) {
            //
        });
    }
}

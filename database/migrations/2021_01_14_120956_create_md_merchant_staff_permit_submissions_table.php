<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantStaffPermitSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_staff_permit_submissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_staff_user_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedSmallInteger('md_permit_category_id');
            $table->longText('desc')->nullable();
            $table->smallInteger('is_canceled')->default(0);
            $table->smallInteger('is_approved')->default(0);
            $table->string('supporting_file');
            $table->unsignedBigInteger('md_user_id_approved')->nullable();
            $table->foreign('md_permit_category_id')
                ->references('id')
                ->on('md_permit_categories');
            $table->foreign('md_staff_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_staff_permit_submissions');
    }
}

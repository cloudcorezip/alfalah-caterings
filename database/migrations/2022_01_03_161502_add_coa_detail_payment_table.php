<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoaDetailPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_payment_methods', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_payment_methods','acc_coa_id'))
            {
               $table->unsignedBigInteger('acc_coa_id')->nullable();
               $table->string('acc_coa_code')->nullable();
            
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_payment_methods', function (Blueprint $table) {
            //
        });
    }
}

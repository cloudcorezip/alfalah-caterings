<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoaInvIdToReturSaleOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_retur_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_order_details','coa_inv_id'))
            {
                $table->bigInteger('coa_inv_id')->nullable();
                $table->bigInteger('coa_hpp_id')->nullable();
            }
        });

        Schema::table('sc_retur_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_order_details','coa_inv_id'))
            {
                $table->bigInteger('coa_inv_id')->nullable();
                $table->bigInteger('coa_hpp_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_retur_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

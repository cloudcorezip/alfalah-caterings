<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsPackageSubs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_products','is_package_subs')){
                $table->smallInteger('is_package_subs')->default(0);
                $table->dateTime('expired_date')->nullable();
                $table->double('limit_selling')->default(0);
                $table->double('limit_redeem')->default(0);
            }
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            
        });
    }
}

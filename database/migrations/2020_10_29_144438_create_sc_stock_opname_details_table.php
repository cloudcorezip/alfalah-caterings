<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScStockOpnameDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_stock_opname_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('physical_count',20,3)->default(0);
            $table->double('record_stock',20,3)->default(0);
            $table->double('difference_amount',20,3)->default(0);
            $table->unsignedInteger('sc_product_id');
            $table->unsignedBigInteger('sc_stock_opname_id');
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('sc_stock_opname_id')
                ->references('id')
                ->on('sc_stock_opnames');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_stock_opname_details');
    }
}

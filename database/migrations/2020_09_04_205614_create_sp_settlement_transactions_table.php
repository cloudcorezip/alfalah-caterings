<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpSettlementTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_settlement_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('amount',20,3)->default(0);
            $table->enum('status',['pending','canceled','refund','success','failed']);
            $table->unsignedBigInteger('sp_transaction_id');
            $table->foreign('sp_transaction_id')
                ->references('id')
                ->on('sp_transactions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_settlement_transactions');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccAssetDepreciationPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_asset_depreciation_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->dateTime('paid_date');
            $table->double('paid_nominal',20,3)->default(0);
            $table->unsignedBigInteger('acc_payment_id');
            $table->smallInteger('type_payment')->default(0);
            $table->string('account_bank_name')->nullable();
            $table->string('trans_proof')->nullable();
            $table->longText('desc')->nullable();
            $table->unsignedBigInteger('acc_asset_depreciation_id');
            $table->foreign('acc_payment_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('acc_asset_depreciation_id')
                ->references('id')
                ->on('acc_asset_depreciations');
            $table->timestamps();
        });

        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciations','lack_of_payment'))
            {
                $table->double('lack_of_payment',20,3)->default(0);
                $table->smallInteger('is_paid')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_asset_depreciation_payments');
    }
}

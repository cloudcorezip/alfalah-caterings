<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsFromFormOrderToScSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','is_from_form_order')){
                $table->smallInteger('is_from_form_order')->default(0);
                $table->jsonb('other_field')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

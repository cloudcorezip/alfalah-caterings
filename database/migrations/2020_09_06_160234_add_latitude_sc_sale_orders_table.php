<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLatitudeScSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','latitude'))
            {
                $table->string('latitude')->nullable();
                $table->string('longitude')->nullable();
                $table->string('resi_code')->nullable();
                $table->string('courier_name')->nullable();
                $table->string('service_courier_name')->nullable();
                $table->string('destination')->nullable();
                $table->double('shipping_cost',20,3)->default(0);
                $table->smallInteger('is_approved_shop')->default(0);
                $table->smallInteger('is_cancel_user')->default(0);
                $table->smallInteger('is_approved_driver')->default(0);
                $table->string('queue_code')->nullable();
                $table->unsignedBigInteger('md_sc_shipping_category_id')->nullable();
                $table->foreign('md_sc_shipping_category_id')
                    ->references('id')
                    ->on('md_sc_shipping_categories');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

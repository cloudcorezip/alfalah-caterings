<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefDiscount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {                
        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_order_details','ref_discount_id'))
            {
                $table->smallInteger('ref_discount_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            //
        });
    }
}

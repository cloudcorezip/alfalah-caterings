<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeAddressToScSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_suppliers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_suppliers','type_address'))
            {
               $table->string('type_address')->nullable();
               $table->string('city_name')->nullable();
               $table->string('region_name')->nullable();
               $table->string('postal_code')->nullable();
               $table->string('gender')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_suppliers', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneToClosingAndDepreciation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciations','_timezone'))
            {
                $table->string('_timezone')->nullable();
            }
        });

        Schema::table('acc_closing_journal_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_closing_journal_details','_timezone'))
            {
                $table->string('_timezone')->nullable();
            }
        });

        Schema::table('acc_closing_journal_distributions', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_closing_journal_distributions','_timezone'))
            {
                $table->string('_timezone')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            //
        });
    }
}

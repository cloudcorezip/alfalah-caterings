<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitIdToRawTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_product_raw_materials', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_product_raw_materials','unit_id')){
                $table->unsignedBigInteger('unit_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_product_raw_materials', function (Blueprint $table) {
            //
        });
    }
}

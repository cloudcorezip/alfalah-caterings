<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneToMmspsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff_permit_submissions', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_permit_submissions','timezone')){
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('md_merchant_staff_attendances', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_attendances','timezone')){
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('hr_merchant_staff_amount_leave', function (Blueprint $table) {
            if(Schema::hasColumn('hr_merchant_staff_amount_leave','timezone')){
                DB::statement('ALTER TABLE hr_merchant_staff_amount_leave ALTER COLUMN timezone DROP NOT NULL');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff_permit_submissions', function (Blueprint $table) {
            //
        });
    }
}

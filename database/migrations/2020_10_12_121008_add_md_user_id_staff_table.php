<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdUserIdStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_order_services','md_user_id_staff'))
            {
                // $table->unsignedBigInteger('md_user_id_staff')->nullable();
                // $table->unsignedBigInteger('created_by')->nullable();
                // $table->smallInteger('is_approved_staff')->default(0);
                // $table->foreign('md_user_id_staff')
                //     ->references('id')
                //     ->on('md_users');
                // $table->foreign('created_by')
                //     ->references('id')
                //     ->on('md_users');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcommerceProductVariationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_product_variation_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('value_id')->nullable();
            $table->string('value_name')->nullable();
            $table->string('blibli_value_name')->nullable();
            $table->string('bukalapak_value_name')->nullable();
            $table->string('elevenia_value_name')->nullable();
            $table->jsonb('have_options')->nullable();
            $table->string('lazada_value_name')->nullable();
            $table->string('shopee_value_name')->nullable();
            $table->string('tokopedia_value_name')->nullable();
            $table->string('zalora_value_name')->nullable();
            $table->string('zilingo_value_name')->nullable();
            $table->string('variation_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_product_variation_details');
    }
}

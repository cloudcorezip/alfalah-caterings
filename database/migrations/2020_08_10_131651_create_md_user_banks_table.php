<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdUserBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_user_banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bank_account_name');
            $table->string('no_rek');
            $table->smallInteger('is_primary')->default(0);
            $table->unsignedBigInteger('md_bank_id');
            $table->unsignedBigInteger('md_user_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_bank_id')
                ->references('id')
                ->on('md_banks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_user_banks');
    }
}

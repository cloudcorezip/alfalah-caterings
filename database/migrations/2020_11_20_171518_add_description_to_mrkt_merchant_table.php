<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionToMrktMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mrkt_promo_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('mrkt_promo_merchants','description'))
            {
                $table->longText('description')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mrkt_merchant_promos', function (Blueprint $table) {
            //
        });
    }
}

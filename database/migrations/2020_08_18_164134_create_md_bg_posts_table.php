<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdBgPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_bg_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('content')->nullable();
            $table->string('image_preview')->nullable();
            $table->string('keyword')->nullable();
            $table->string('slug')->nullable();
            $table->smallInteger('is_publish')->default(0);
            $table->dateTime('publish_date')->nullable();
            $table->unsignedBigInteger('md_user_id');
            $table->unsignedBigInteger('md_bg_category_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_bg_category_id')
                ->references('id')
                ->on('md_bg_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_bg_posts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStartEndDatePermit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff_permit_submissions', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_permit_submissions','start_date')){
                $table->dateTime('start_date')->nullable();
                $table->dateTime('end_date')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff_permit_submissions', function (Blueprint $table) {
            //
        });
    }
}

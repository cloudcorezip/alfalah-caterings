<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneInAssetDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_asset_depreciation_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciation_details','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('acc_asset_depreciation_payments', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciation_payments','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_asset_depreciation_details', function (Blueprint $table) {
            //
        });
    }
}

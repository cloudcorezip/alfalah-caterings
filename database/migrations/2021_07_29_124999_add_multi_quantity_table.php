<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultiQuantityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_retur_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_order_details','multi_quantity'))
            {
                $table->smallInteger('is_multi_unit')->default(0);
                $table->string('unit_name')->nullable();
                $table->bigInteger('multi_quantity')->default(0);
            }
        });

        Schema::table('sc_retur_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_order_details','multi_quantity'))
            {
                $table->smallInteger('is_multi_unit')->default(0);
                $table->string('unit_name')->nullable();
                $table->bigInteger('multi_quantity')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_retur_sale_order_details', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdUserDriverDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_user_driver_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('identity_card');
            $table->string('identity_card_with_body');
            $table->string('stnk_file');
            $table->string('police_number');
            $table->string('year');
            $table->string('merk')->nullable();
            $table->string('description')->nullable();
            $table->string('is_verified')->default(0);
            $table->unsignedBigInteger('md_user_id_driver');
            $table->unsignedBigInteger('md_user_id_verified')->nullable();
            $table->unsignedBigInteger('md_vehicle_type_id');
            $table->foreign('md_vehicle_type_id')
                ->references('id')
                ->on('md_vehicle_types');
            $table->foreign('md_user_id_verified')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_user_id_driver')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_user_driver_details');
    }
}

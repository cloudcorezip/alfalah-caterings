<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsPercentageToDiscounts2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_merchant_discounts', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_merchant_discounts','is_percentage'))
            {
                $table->smallInteger('is_percentage')->default(0);
                $table->double('discount_price',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_merchant_discounts', function (Blueprint $table) {
            //
        });
    }
}

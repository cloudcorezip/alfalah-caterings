<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal17Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','ap_amount'))
            {
                $table->double('ap_amount')->nullable();
            }
        });
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','is_deleted'))
            {
                $table->smallInteger('is_deleted')->nullable();
            }
        });
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap','ap_code'))
            {
                $table->string('ap_code')->nullable();
            }
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ap', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccMerchantArDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_merchant_ar_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('acc_merchant_ar_id');
            $table->double('paid_nominal',2)->default(0);
            $table->date('paid_date')->nullable();
            $table->string('note')->nullable();
            $table->foreign('acc_merchant_ar_id')
                ->references('id')
                ->on('acc_merchant_ar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_merchant_ar_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitToSsDetailServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_detail_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_services','md_unit_id'))
            {
                $table->unsignedBigInteger('md_unit_id');
                $table->foreign('md_unit_id')
                    ->references('id')
                    ->on('md_units');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_detail_services', function (Blueprint $table) {
            //
        });
    }
}

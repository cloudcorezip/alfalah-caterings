<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoaArAp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if (!Schema::hasColumn('md_merchants', 'coa_ar_sale')) {
                $table->bigInteger('coa_ar_sale')->nullable();
                $table->bigInteger('coa_ar_other')->nullable();
                $table->bigInteger('coa_ap_purchase')->nullable();
                $table->bigInteger('coa_ap_other')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

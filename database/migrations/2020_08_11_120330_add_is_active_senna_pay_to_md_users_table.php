<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsActiveSennaPayToMdUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_users', function (Blueprint $table) {
            if(!Schema::hasColumn('md_users','is_active_senna_pay')){
                $table->smallInteger('is_active_senna_pay')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_users', function (Blueprint $table) {
            //
        });
    }
}

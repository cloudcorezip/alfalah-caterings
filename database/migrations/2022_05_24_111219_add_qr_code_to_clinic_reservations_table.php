<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQrCodeToClinicReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_reservations', function (Blueprint $table) {
            if(!Schema::hasColumn('clinic_reservations','qr_code')){
                $table->string('qr_code_file')->nullable();
                $table->string('qr_code_url')->nullable();
                $table->smallInteger('is_type')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_reservations', function (Blueprint $table) {
            //
        });
    }
}

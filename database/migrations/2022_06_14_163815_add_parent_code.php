<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_coa_categories', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_coa_categories','parent_code')) {
                $table->integer('parent_code')->nullable();
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_coa_categories', function (Blueprint $table) {

        });
    }
}

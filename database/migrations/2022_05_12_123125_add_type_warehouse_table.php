<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_inv_warehouses', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_inv_warehouses','type_id'))
            {
                $table->bigInteger('type_id')->nullable();
                $table->foreign('type_id')
                    ->references('id')
                    ->on('merchant_inv_warehouse_types');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_inv_warehouses', function (Blueprint $table) {
            //
        });
    }
}

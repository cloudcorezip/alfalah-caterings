<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultiUnitIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_order_details','multi_unit_id'))
            {
                $table->unsignedBigInteger('multi_unit_id')->nullable();
            }
        });
        Schema::table('sc_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_order_details','multi_unit_id'))
            {
                $table->unsignedBigInteger('multi_unit_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            //
        });
    }
}

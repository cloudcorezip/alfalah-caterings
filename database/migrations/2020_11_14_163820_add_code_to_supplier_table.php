<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCodeToSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_suppliers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_suppliers','code'))
            {
                $table->string('code')->nullable();
            }
        });
        Schema::table('sc_suppliers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_suppliers','sc_supplier_categories_id'))
            {
                $table->unsignedBigInteger('sc_supplier_categories_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_suppliers', function (Blueprint $table) {
            //
        });
    }
}

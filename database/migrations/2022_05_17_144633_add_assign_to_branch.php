<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssignToBranch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_product_selling_levels', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_product_selling_levels', 'assign_to')) {
                $table->jsonb('assign_to')->nullable();
            }
        });
        Schema::table('sc_product_multi_units', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_product_multi_units', 'assign_to')) {
                $table->jsonb('assign_to')->nullable();
            }
        });
        Schema::table('sc_product_raw_materials', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_product_raw_materials', 'assign_to')) {
                $table->jsonb('assign_to')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_product_selling_levels', function (Blueprint $table) {
            //
        });
    }
}

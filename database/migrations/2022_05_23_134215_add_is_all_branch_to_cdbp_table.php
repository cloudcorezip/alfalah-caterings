<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsAllBranchToCdbpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_by_products','is_all_branch')){
                $table->smallInteger('is_all_branch')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            //
        });
    }
}

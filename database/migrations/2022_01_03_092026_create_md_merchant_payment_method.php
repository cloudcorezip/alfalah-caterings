<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantPaymentMethod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_type_id');
            $table->unsignedBigInteger('md_va_bank_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('status')->default(0);
            $table->foreign('md_merchant_id')
            ->references('id')
            ->on('md_merchants');
            $table->foreign('transaction_type_id')
            ->references('id')
            ->on('md_transaction_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_payment_methods');
    }
}

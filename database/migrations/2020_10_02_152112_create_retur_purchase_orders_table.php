<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReturPurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_retur_purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('qr_code')->nullable();
            $table->unsignedBigInteger('sc_purchase_order_id');
            $table->string('note')->nullable();
            $table->double('total',20,3)->default(0);
            $table->unsignedBigInteger('md_sc_transaction_status_id');
            $table->foreign('sc_purchase_order_id')
                ->references('id')
                ->on('sc_purchase_orders');
            $table->foreign('md_sc_transaction_status_id')
                ->references('id')
                ->on('md_sc_transaction_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_retur_purchase_orders');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsAllProductToCdcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_coupons', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_coupons','is_all_product'))
            {
                $table->smallInteger('is_all_product')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_coupons', function (Blueprint $table) {
            //
        });
    }
}

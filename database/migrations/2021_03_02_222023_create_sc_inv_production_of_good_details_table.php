<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScInvProductionOfGoodDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_inv_production_of_good_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_product_id');
            $table->unsignedBigInteger('sc_stock_inventory_id');
            $table->double('amount',20,3)->default(0);
            $table->double('purchase_price',20,3)->default(0);
            $table->double('sub_total',20,3)->default(0);
            $table->unsignedBigInteger('sc_inv_production_of_good_id');
            $table->smallInteger('is_return')->default(0);
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('sc_stock_inventory_id')
                ->references('id')
                ->on('sc_stock_inventories');
            $table->foreign('sc_inv_production_of_good_id')
                ->references('id')
                ->on('sc_inv_production_of_goods');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_inv_production_of_good_details');
    }
}

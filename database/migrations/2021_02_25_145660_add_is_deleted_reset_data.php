<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDeletedResetData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ap_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap_details','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);

            }
        });
        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar_details','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ap_details', function (Blueprint $table) {
            //
        });
    }
}

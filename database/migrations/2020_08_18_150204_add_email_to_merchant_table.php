<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailToMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','email_merchant'))
            {
                $table->string('image')->nullable();
                $table->string('address')->nullable();
                $table->string('email_merchant')->nullable();
                $table->string('phone_merchant')->nullable();
                $table->unsignedBigInteger('md_village_id');
                $table->foreign('md_village_id')
                    ->references('id')
                    ->on('md_villages');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

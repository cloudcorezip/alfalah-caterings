<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrMerchantShiftSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_merchant_shift_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('color')->nullable();
            $table->time('start_shift');
            $table->time('end_shift');
            $table->time('half_shift')->nullable();
            $table->double('max_late',20,3)->default(0);
            $table->double('max_allowed_attendance_per_day',20,3)->default(0);
            $table->smallInteger('is_default')->default(0);
            $table->jsonb('work_day')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_to_all_branch')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('md_merchant_id')->references('id')
                ->on('md_merchants');
            $table->foreign('created_by')->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_merchant_shift_settings');
    }
}

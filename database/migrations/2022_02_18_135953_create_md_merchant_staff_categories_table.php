<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantStaffCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_staff_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->unsignedBigInteger('md_user_id');
            $table->smallInteger('is_deleted')->default(0);
            $table->smallInteger('is_specialist')->default(0);
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_staff_categories');
    }
}

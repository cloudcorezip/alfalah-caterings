<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScStockInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_stock_inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stockable_id');
            $table->string('stockable_type');
            $table->unsignedBigInteger('sc_product_id');
            $table->double('total',20,3)->default(0);
            $table->double('selling_price',20,3)->default(0);
            $table->double('purchase_price',20,3)->default(0);
            $table->enum('type',['in','out','initial']);
            $table->string('transaction_action')->nullable();
            $table->foreign('sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_stock_inventories');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('va_code')->nullable();
            $table->longText('barcode')->nullable();
            $table->string('fullname')->nullable();
            $table->string('email');
            $table->string('password');
            $table->string('phone_number')->nullable();
            $table->string('foto')->nullable();
            $table->string('token')->nullable();
            $table->string('fcm_key')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->smallInteger('is_active')->default(0);
            $table->smallInteger('is_email_verified')->default(0);
            $table->smallInteger('is_phone_verified')->default(0);
            $table->smallInteger('is_merchant')->default(0);
            $table->unsignedBigInteger('md_role_id');
            $table->foreign('md_role_id')
                ->references('id')
                ->on('md_roles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_users');
    }
}

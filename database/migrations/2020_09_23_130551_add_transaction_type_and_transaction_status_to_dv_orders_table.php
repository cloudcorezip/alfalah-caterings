<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransactionTypeAndTransactionStatusToDvOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dv_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('dv_orders','md_transaction_type_id'))
            {
                $table->unsignedBigInteger('md_transaction_type_id');
                $table->unsignedBigInteger('md_transaction_status_id');
                $table->foreign('md_transaction_type_id')
                    ->references('id')
                    ->on('md_transaction_types');
                $table->foreign('md_transaction_status_id')
                    ->references('id')
                    ->on('md_transaction_status');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dv_orders', function (Blueprint $table) {
            //
        });
    }
}

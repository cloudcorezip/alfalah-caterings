<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsExpiredToSpPaymentSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sp_payment_subscriptions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_payment_subscriptions','is_expired'))
            {
                $table->smallInteger('is_expired')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sp_payment_subscriptions', function (Blueprint $table) {
            //
        });
    }
}

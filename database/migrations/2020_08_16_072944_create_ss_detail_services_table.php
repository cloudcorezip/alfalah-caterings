<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsDetailServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_detail_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('detail_name')->nullable();
            $table->float('price',20,2)->default(0);
            $table->string('description')->nullable();
            $table->string('note')->nullable();
            $table->bigInteger('parent_id')->nullable();
            $table->smallInteger('is_additional')->default(0);
            $table->unsignedBigInteger('ss_service_id');
            $table->foreign('ss_service_id')
                ->references('id')
                ->on('ss_services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_detail_services');
    }
}

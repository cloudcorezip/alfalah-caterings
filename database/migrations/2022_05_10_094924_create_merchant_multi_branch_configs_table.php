<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantMultiBranchConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_multi_branch_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('menu_name');
            $table->smallInteger('is_available_add')->default(0);
            $table->smallInteger('is_available_edit')->default(0);
            $table->smallInteger('is_available_delete')->default(0);
            $table->unsignedBigInteger('md_merchant_id');
            $table->foreign('md_merchant_id')->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_multi_branch_configs');
    }
}

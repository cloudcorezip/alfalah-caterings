<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDistrictFromRajaOngkirToMdMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(Schema::hasColumn('md_merchants','md_village_id')){
                $table->dropColumn('md_village_id');
            }
            if(!Schema::hasColumn('md_merchants','subdistrict_id_from_raja_ongkir'))
            {
                $table->unsignedBigInteger('subdistrict_id_from_raja_ongkir')->default(0);
                $table->unsignedBigInteger('city_id_from_raja_ongkir')->default(0);
                $table->unsignedBigInteger('province_id_from_raja_ongkir')->default(0);
                $table->string('subdistrict_name')->nullable();
                $table->string('city_name')->nullable();
                $table->string('province_name')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

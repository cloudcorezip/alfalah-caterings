<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScMerchantFdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_merchant_fd', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->time('start_available');
            $table->time('end_available');
            $table->double('max_distance',20,3)->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->unsignedBigInteger('md_merchant_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_merchant_fd');
    }
}

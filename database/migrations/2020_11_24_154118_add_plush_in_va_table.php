<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPlushInVaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_va_banks', function (Blueprint $table) {
            if(!Schema::hasColumn('md_va_banks','plus_amount'))
            {
                $table->float('plus_amount',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_va_banks', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal20Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar_details','ar_detail_code'))
            {
                $table->string('ar_detail_code')->nullable();
            }
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            //
        });
    }
}

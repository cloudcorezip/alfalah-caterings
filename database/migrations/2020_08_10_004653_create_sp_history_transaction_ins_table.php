<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpHistoryTransactionInsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_history_transaction_ins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->unsignedBigInteger('md_user_id');
            $table->float('amount',20,2)->default(0);
            $table->string('description')->nullable();
            $table->smallInteger('is_topup')->default(0);
            $table->unsignedBigInteger('md_sp_transaction_type_id');
            $table->unsignedBigInteger('md_sp_transaction_status_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_sp_transaction_type_id')
                ->references('id')
                ->on('md_sp_transaction_types');
            $table->foreign('md_sp_transaction_status_id')
                ->references('id')
                ->on('md_sp_transaction_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_history_transaction_ins');
    }
}

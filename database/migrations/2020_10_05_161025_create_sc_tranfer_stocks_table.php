<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScTranferStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_transfer_stocks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->unsignedBigInteger('head_merchant_id');
            $table->unsignedBigInteger('branch_merchant_id');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->double('total',20,3)->default(0);
            $table->foreign('head_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('branch_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_transfer_stocks');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal13Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_jurnals', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnals','external_ref_id'))
            {
                $table->unsignedBigInteger('external_ref_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaxBonusValueToCdcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_coupons', function (Blueprint $table) {
            if (!Schema::hasColumn('crm_discount_coupons', 'max_bonus_value')) {
                $table->double('max_bonus_value',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_coupons', function (Blueprint $table) {
            //
        });
    }
}

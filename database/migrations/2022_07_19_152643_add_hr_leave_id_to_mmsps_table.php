<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHrLeaveIdToMmspsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff_permit_submissions', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff_permit_submissions','hr_leave_id')){
                $table->unsignedBigInteger('hr_leave_id')->nullable();
                DB::statement('ALTER TABLE md_merchant_staff_permit_submissions ALTER COLUMN md_permit_category_id DROP NOT NULL');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff_permit_submissions', function (Blueprint $table) {
            //
        });
    }
}

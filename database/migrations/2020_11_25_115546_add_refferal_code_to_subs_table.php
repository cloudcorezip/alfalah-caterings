<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefferalCodeToSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sp_payment_subscriptions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_payment_subscriptions','refferal_code'))
            {
                $table->string('refferal_code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sp_payment_subscriptions', function (Blueprint $table) {
            //
        });
    }
}

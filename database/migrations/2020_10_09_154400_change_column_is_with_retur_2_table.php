<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnIsWithRetur2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_temp_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_temp_sale_order_details','is_current'))
            {
                $table->smallInteger('is_current')->default(1);
            }
        });

        Schema::table('sc_temp_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_temp_purchase_order_details','is_current'))
            {
                $table->smallInteger('is_current')->default(1);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

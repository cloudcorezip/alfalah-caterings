<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpManualTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_manual_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bank_account_name');
            $table->string('bank_name');
            $table->string('no_rek');
            $table->bigInteger('md_user_id_approval')->nullable();
            $table->string('evidence_of_transfer')->nullable();
            $table->smallInteger('is_approved')->default(0);
            $table->enum('type',['in','out']);
            $table->smallInteger('is_topup')->default(0);
            $table->unsignedBigInteger('md_bank_id');
            $table->unsignedBigInteger('md_user_id');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_bank_id')
                ->references('id')
                ->on('md_banks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_manual_transactions');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeTransactionArTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar_details','is_type_payment_gataway')){
                $table->smallInteger('is_type_payment_gateway')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            //
        });
    }
}

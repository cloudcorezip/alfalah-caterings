<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScMerchantApTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_merchant_ap', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_purchase_order_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->double('residual_amount',20,3)->default(0);
            $table->double('paid_nominal',20,3)->default(0);
            $table->smallInteger('is_paid_off')->default(0);
            $table->date('due_date')->nullable();
            $table->foreign('sc_purchase_order_id')
                ->references('id')
                ->on('sc_purchase_orders');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_merchant_ap');
    }
}

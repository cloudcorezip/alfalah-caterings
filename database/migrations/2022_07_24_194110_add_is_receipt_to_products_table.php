<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsReceiptToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_products','is_receipt')){
                $table->smallInteger('is_receipt')->default(0);
            }
        });

        Schema::table('hr_merchant_attendance_settings', function (Blueprint $table) {
            if(!Schema::hasColumn('hr_merchant_attendance_settings','is_use_rule_leave')){
                $table->smallInteger('is_use_rule_leave')->default(0);
            }
        });
        Schema::table('sc_product_raw_materials', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_product_raw_materials','value_conversion')){
                $table->double('value_conversion',20,3)->default(1);
                $table->smallInteger('is_multi_unit')->default(0);
                $table->double('multi_quantity',20,3)->default(0);
                $table->string('multi_unit_name')->nullable();
                $table->unsignedBigInteger('multi_unit_id')->nullable();
                $table->jsonb('json_multi_unit')->nullable();
            }
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_products', function (Blueprint $table) {
            //
        });
    }
}

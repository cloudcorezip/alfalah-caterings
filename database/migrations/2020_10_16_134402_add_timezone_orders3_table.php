<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneOrders3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ap_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap_details','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar_details','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });

        Schema::table('sp_transactions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_transactions','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

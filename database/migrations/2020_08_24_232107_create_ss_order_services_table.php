<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsOrderServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_order_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->double('total_price',20,3)->default(0);
            $table->double('promo_price',20,3)->default(0);
            $table->double('admin_fee',20,3)->default(0);
            $table->unsignedBigInteger('ss_service_id');
            $table->unsignedBigInteger('user_order_id');
            $table->unsignedBigInteger('md_transaction_type_id');
            $table->unsignedBigInteger('md_ss_order_status_id');
            $table->smallInteger('is_approved_provider')->default(0);
            $table->smallInteger('is_canceled_user')->default(0);
            $table->foreign('ss_service_id')
                ->references('id')
                ->on('ss_services');
            $table->foreign('user_order_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_transaction_type_id')
                ->references('id')
                ->on('md_transaction_types');
            $table->foreign('md_ss_order_status_id')
                ->references('id')
                ->on('md_ss_order_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_order_services');
    }
}

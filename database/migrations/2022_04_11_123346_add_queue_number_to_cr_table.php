<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQueueNumberToCrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_reservations', function (Blueprint $table) {
            if (!Schema::hasColumn('clinic_reservations', 'queue_number')) {
                $table->string('queue_number')->nullable();
                $table->string('counter_code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_reservations', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantBranchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_branchs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('head_merchant_id');
            $table->unsignedBigInteger('branch_merchant_id');
            $table->string('email_branch');
            $table->smallInteger('is_accepted')->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->foreign('head_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('branch_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_branchs');
    }
}

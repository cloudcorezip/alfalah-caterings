<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRangelStartToMdVehicleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_vehicle_types', function (Blueprint $table) {
            if(!Schema::hasColumn('md_vehicle_types','initial_rates'))
            {
                $table->double('initial_rates',20,3)->default(0);
                $table->double('rates_based_on_kilometer',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_vehicle_types', function (Blueprint $table) {
            //
        });
    }
}

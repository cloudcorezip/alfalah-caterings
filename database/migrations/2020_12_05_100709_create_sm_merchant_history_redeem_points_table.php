<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmMerchantHistoryRedeemPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_merchant_history_redeem_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('sm_merchant_redeem_point_id');
            $table->string('ref_id')->nullable();
            $table->double('point_exchange',20,3)->default(0);
            $table->unsignedBigInteger('md_transaction_status_id');
            $table->jsonb('description')->nullable();
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('sm_merchant_redeem_point_id')
                ->references('id')
                ->on('sm_merchant_redeem_points');
            $table->foreign('md_transaction_status_id')
                ->references('id')
                ->on('md_transaction_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_merchant_history_redeem_points');
    }
}

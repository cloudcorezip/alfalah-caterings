<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentAdminCurrentAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciations','admin_fee'))
            {
                $table->double('admin_fee',20,3)->default(0);
                $table->double('current_asset_value',20,3)->default(0);
                $table->bigInteger('payment_coa_detail_id')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGmailInformation2ToMdUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_users', function (Blueprint $table) {
            if(Schema::hasColumn('md_users','gmail_information'))
            {
                $table->dropColumn('gmail_information');
            }
        });

        Schema::table('md_users', function (Blueprint $table) {
            if(!Schema::hasColumn('md_users','gmail_information'))
            {
                $table->json('gmail_information')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_users', function (Blueprint $table) {
            //
        });
    }
}

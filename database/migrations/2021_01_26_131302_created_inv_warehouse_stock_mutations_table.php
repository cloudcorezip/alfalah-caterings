<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatedInvWarehouseStockMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_warehouse_stock_mutations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->longText('desc')->nullable();
            $table->unsignedBigInteger('md_merchant_id');
            $table->string('attachment_file')->nullable();
            $table->unsignedBigInteger('from_warehouse_id');
            $table->unsignedBigInteger('to_warehouse_id');
            $table->double('total',20,3)->default(0);
            $table->unsignedBigInteger('md_user_id_created')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('from_warehouse_id')
                ->references('id')
                ->on('md_merchant_inv_warehouses');
            $table->foreign('to_warehouse_id')
                ->references('id')
                ->on('md_merchant_inv_warehouses');
            $table->foreign('md_user_id_created')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inv_warehouse_stock_mutations');
    }
}

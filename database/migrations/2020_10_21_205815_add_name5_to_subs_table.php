<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddName5ToSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_subscriptions', function (Blueprint $table) {
            if(!Schema::hasColumn('md_subscriptions','max_transaction_online'))
            {
                $table->unsignedInteger('max_transaction_online')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_subscriptions', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccClosingJournalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_closing_journal_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->unsignedBigInteger('from_acc_coa_detail_id');
            $table->unsignedBigInteger('to_acc_coa_detail_id');
            $table->string('from_coa_type');
            $table->string('to_coa_type');
            $table->smallInteger('is_reject')->default(0);
            $table->dateTime('time');
            $table->double('amount',20,3)->default(0);
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('acc_period_id');
            $table->foreign('from_acc_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('to_acc_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('acc_period_id')
                ->references('id')
                ->on('acc_periods');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_closing_journal_details');
    }
}

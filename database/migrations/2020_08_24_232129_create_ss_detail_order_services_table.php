<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsDetailOrderServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_detail_order_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('price',20,3)->default(0);
            $table->double('sub_total',20,3)->default(0);
            $table->double('admin_fee',20,3)->default(0);
            $table->double('promo_price',20,3)->default(0);
            $table->timestamp('desired_time')->nullable();
            $table->double('amount_services',20,3)->default(0);
            $table->string('description_location')->nullable();
            $table->unsignedBigInteger('md_village_id');
            $table->unsignedBigInteger('ss_order_service_id');
            $table->unsignedBigInteger('ss_detail_service_id');
            $table->smallInteger('is_cancel_user')->default(0);
            $table->foreign('md_village_id')
                ->references('id')
                ->on('md_villages');
            $table->foreign('ss_order_service_id')
                ->references('id')
                ->on('ss_order_services');
            $table->foreign('ss_detail_service_id')
                ->references('id')
                ->on('ss_detail_services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_detail_order_services');
    }
}

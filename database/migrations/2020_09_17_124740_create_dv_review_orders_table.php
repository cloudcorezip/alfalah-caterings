<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDvReviewOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dv_review_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('orderable_id');
            $table->string('orderable_type');
            $table->float('rating',10,2)->default(0);
            $table->string('suggestions')->nullable();
            $table->smallInteger('is_rating')->default(0);
            $table->unsignedBigInteger('user_id_review');
            $table->foreign('user_id_review')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dv_review_orders');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMerchantGradeIdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff','merchant_grade_id'))
            {
                $table->unsignedBigInteger('merchant_grade_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff', function (Blueprint $table) {
            //
        });
    }
}

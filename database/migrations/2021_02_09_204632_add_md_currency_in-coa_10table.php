<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMdCurrencyInCoa10table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_coa_categories', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_coa_categories','deleted_at'))
            {
                $table->softDeletes();
            }
        });

        Schema::table('acc_coa_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_coa_details','deleted_at'))
            {
                $table->softDeletes();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_coa_categories', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoaHppAndInvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_consignment_good_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_consignment_good_details','coa_hpp_id')){
                $table->unsignedBigInteger('coa_hpp_id')->nullable();
                $table->unsignedBigInteger('coa_inv_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_consignment_good_details', function (Blueprint $table) {
            //
        });
    }
}

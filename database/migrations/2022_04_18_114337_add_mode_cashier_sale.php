<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddModeCashierSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_sale_orders', 'receive_time')) {
                $table->date('receive_time')->nullable();
                $table->date('pick_up_time')->nullable();
                $table->string('guarantee')->nullable();

            }
        });

        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_sale_order_details', 'start_rent')) {
                $table->date('start_rent')->nullable();
                $table->date('end_rent')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExpirationToSpTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sp_transactions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_transactions','expiration_date'))
            {
                $table->dateTime('expiration_date')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sp_transactions', function (Blueprint $table) {
            //
        });
    }
}

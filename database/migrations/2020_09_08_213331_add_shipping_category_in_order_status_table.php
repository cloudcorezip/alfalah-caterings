<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShippingCategoryInOrderStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_sc_order_status', function (Blueprint $table) {
            if(!Schema::hasColumn('md_sc_order_status','md_sc_shipping_category_id'))
            {
                $table->unsignedBigInteger('md_sc_shipping_category_id')->nullable();
                $table->foreign('md_sc_shipping_category_id')
                    ->references('id')
                    ->on('md_sc_shipping_categories');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_sc_order_status', function (Blueprint $table) {
            //
        });
    }
}

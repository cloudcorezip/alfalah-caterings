<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantPaymentInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_payment_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_language_id');
            $table->unsignedBigInteger('md_currency_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->bigInteger('invoice_duration')->default(0);
            $table->text('theme');
            $table->jsonb('payment_method');
            $table->foreign('md_merchant_id')
            ->references('id')
            ->on('md_merchants');
            $table->foreign('md_currency_id')
            ->references('id')
            ->on('md_sc_currencies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_payment_invoices');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsWithInstallToMdPluginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_plugins', function (Blueprint $table) {
            if(!Schema::hasColumn('md_plugins','is_with_install')){
                $table->smallInteger('is_with_install')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_plugins', function (Blueprint $table) {
            //
        });
    }
}

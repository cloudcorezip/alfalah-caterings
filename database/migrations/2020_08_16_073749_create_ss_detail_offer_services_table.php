<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSsDetailOfferServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ss_detail_offer_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('question_name');
            $table->unsignedBigInteger('ss_offer_service_id');
            $table->foreign('ss_offer_service_id')
                ->references('id')
                ->on('ss_offer_services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ss_detail_offer_services');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsCreateEmailToMdMerchantStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_staff', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_staff','is_create_email')){
                $table->smallInteger('is_create_email')->default(0);
                DB::statement('ALTER TABLE md_merchant_staff ALTER COLUMN email DROP NOT NULL');
                DB::statement('ALTER TABLE md_users ALTER COLUMN email DROP NOT NULL');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_staff', function (Blueprint $table) {
            //
        });
    }
}

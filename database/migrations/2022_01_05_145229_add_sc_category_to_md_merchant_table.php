<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddScCategoryToMdMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','md_sc_category_id'))
            {
               $table->unsignedBigInteger('md_sc_category_id')->nullable();
               $table->string('fb')->nullable();
               $table->string('twitter')->nullable();
               $table->string('ig')->nullable();
               $table->string('web')->nullable();
               DB::statement('ALTER TABLE md_merchants ALTER COLUMN md_type_of_business_id DROP NOT NULL');
            
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

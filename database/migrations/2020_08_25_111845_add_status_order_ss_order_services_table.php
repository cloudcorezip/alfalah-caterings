<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusOrderSsOrderServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            if(Schema::hasColumn('ss_order_services','ss_service_id')){
                $table->dropColumn('ss_service_id');
                $table->unsignedBigInteger('md_merchant_id');
                $table->foreign('md_merchant_id')
                    ->references('id')
                    ->on('md_merchants');
            }
        });

        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_order_services','ss_status_order_detail_id')){
                $table->unsignedBigInteger('ss_status_order_detail_id');
                $table->foreign('ss_status_order_detail_id')
                    ->references('id')
                    ->on('ss_status_order_details');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_order_services', function (Blueprint $table) {
            //
        });
    }
}

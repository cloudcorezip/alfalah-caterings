<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveTableOldSennaPayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::dropIfExists('sp_history_transaction_in_from_between_users');
       Schema::dropIfExists('sp_history_transaction_in_from_merchants');
       Schema::dropIfExists('sp_history_transaction_in_manual_transfer');
        Schema::dropIfExists('sp_history_transaction_out_between_users');
        Schema::dropIfExists('sp_history_transaction_out_from_merchants');
        Schema::dropIfExists('sp_history_transaction_out_manual_transfer');
       Schema::dropIfExists('sp_order_settlement_ins');
       Schema::dropIfExists('sp_order_settlement_outs');
       Schema::dropIfExists('sp_order_settlements');
       Schema::dropIfExists('sp_history_transaction_ins');
       Schema::dropIfExists('sp_history_transaction_outs');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sp_history_transaction_ins', function (Blueprint $table) {
            //
        });
    }
}

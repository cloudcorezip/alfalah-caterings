<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQrisWithdrawActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qris_withdraw_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_code');
            $table->bigInteger('md_user_id')->unsigned();
            $table->bigInteger('qris_activation_id')->unsigned();
            $table->bigInteger('md_merchant_id')->unsigned();
            $table->double('amount',20,3)->default(0);
            $table->double('admin_fee',20,3)->default(0);
            $table->string('transaction_status');
            $table->jsonb('response_create')->nullable();
            $table->jsonb('response_status')->nullable();
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('qris_activation_id')
                ->references('id')
                ->on('qris_activations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qris_withdraw_activities');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRenameColumnInMdSpGuidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_sp_guides', function (Blueprint $table) {
            if(Schema::hasColumn('md_sp_guides','icon'))
            {
                $table->dropColumn('icon');
            }
            $table->string('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_sp_guides', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefCodePoSoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_purchase_orders','ref_code'))
            {
                $table->string('ref_code')->nullable();
            }
        });

        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_sale_orders','ref_code'))
            {
                $table->string('ref_code')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            //
        });
    }
}

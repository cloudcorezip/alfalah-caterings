<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccMerchantShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_shifts', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_shifts','md_transaction_type_id'))
            {
                $table->unsignedBigInteger('md_transaction_type_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_merchant_shifts');
    }
}

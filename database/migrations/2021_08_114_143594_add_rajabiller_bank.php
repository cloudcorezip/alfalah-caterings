<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRajabillerBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {                
        Schema::table('qris_activations', function (Blueprint $table) {

                $table->unsignedBigInteger('md_rajabiller_bank_id');
                $table->foreign('md_rajabiller_bank_id')
                ->references('id')
                ->on('md_rajabiller_banks');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qris_activations', function (Blueprint $table) {
            //
        });
    }
}

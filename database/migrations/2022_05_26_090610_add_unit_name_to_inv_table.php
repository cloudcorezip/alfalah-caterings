<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitNameToInvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_transfer_stock_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_transfer_stock_details','unit_name')){
                $table->string('product_name')->nullable();
                $table->string('unit_name')->nullable();
            }
        });

        Schema::table('sc_stock_opname_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_opname_details','unit_name')){
                $table->string('product_name')->nullable();
                $table->string('unit_name')->nullable();
            }
        });

        Schema::table('sc_consignment_good_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_consignment_good_details','unit_name')){
                $table->string('product_name')->nullable();
                $table->string('unit_name')->nullable();
            }
        });

        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_inventories','unit_name')){
                $table->string('product_name')->nullable();
                $table->string('unit_name')->nullable();
            }
        });

        Schema::table('sc_inv_production_of_good_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_inv_production_of_good_details','unit_name')){
                $table->string('product_name')->nullable();
                $table->string('unit_name')->nullable();
            }
        });


        Schema::table('sc_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_order_details','product_name')){
                $table->string('product_name')->nullable();
            }
        });

        Schema::table('sc_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_order_details','product_name')){
                $table->string('product_name')->nullable();
            }
        });

        Schema::table('sc_retur_sale_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_order_details','product_name')){
                $table->string('product_name')->nullable();
            }
        });

        Schema::table('sc_retur_purchase_order_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_order_details','product_name')){
                $table->string('product_name')->nullable();
            }
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_transfer_stock_details', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnableMerchantPluginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enable_merchant_plugins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');

            $table->unsignedBigInteger('md_plugin_id');
            $table->foreign('md_plugin_id')
                ->references('id')
                ->on('md_plugins');

            $table->smallInteger('status')->nullable();
            $table->dateTime('start_period')->nullable();
            $table->dateTime('end_period')->nullable();
            $table->bigInteger('quota_used')->nullable();
            $table->bigInteger('quota_available')->nullable();
            $table->string('client_id')->nullable();
            $table->string('client_secret')->nullable();
            $table->string('client_token')->nullable();
            $table->smallInteger('is_deleted')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enable_merchant_plugins');
    }
}

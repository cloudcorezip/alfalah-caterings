<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsShowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_transaction_types', function (Blueprint $table) {
            if(!Schema::hasColumn('md_transaction_types','is_show'))
            {
                $table->smallInteger('is_show')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_transaction_types', function (Blueprint $table) {
            //
        });
    }
}

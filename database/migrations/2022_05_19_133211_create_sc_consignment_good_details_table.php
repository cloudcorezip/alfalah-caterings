<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScConsignmentGoodDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_consignment_good_details', function (Blueprint $table) {
            $table->bigIncrements('is');
            $table->unsignedBigInteger('sc_product_id');
            $table->double('quantity',20,3)->default(0);
            $table->double('price',20,3)->default(0);
            $table->double('sub_total',20,3)->default(0);
            $table->unsignedBigInteger('sc_consignment_good_id');
            $table->foreign('sc_consignment_good_id')->references('id')
                ->on('sc_consignment_goods');
            $table->timestamps();
        });

        Schema::table('sc_products',function (Blueprint $table){
            if(!Schema::hasColumn('sc_products','is_consignment')){

                $table->smallInteger('is_consignment')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_consignment_good_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordStock4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ap_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap_details','created_by'))
            {
                $table->unsignedBigInteger('created_by')->nullable();
            }
        });

        Schema::table('acc_merchant_ar_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ar_details','created_by'))
            {
                $table->unsignedBigInteger('created_by')->nullable();
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_inventories', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScReviewSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_review_sale_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('rating',10,2)->default(0);
            $table->string('suggestions')->nullable();
            $table->unsignedBigInteger('user_id_review');
            $table->unsignedBigInteger('sc_sale_order_id');
            $table->foreign('user_id_review')
                ->references('id')
                ->on('md_users');
            $table->foreign('sc_sale_order_id')
                ->references('id')
                ->on('sc_sale_orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_review_sale_orders');
    }
}

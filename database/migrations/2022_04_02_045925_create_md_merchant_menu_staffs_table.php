<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantMenuStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_menu_staffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('staff_user_id');
            $table->unsignedBigInteger('menu_merchant_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_active')->default(0);
            $table->foreign('staff_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('menu_merchant_id')
                ->references('id')
                ->on('md_merchant_menus');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });

        Schema::table('md_merchant_menus', function (Blueprint $table) {
            if (!Schema::hasColumn('md_merchant_menus', 'is_active')) {
                $table->smallInteger('is_active')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_menu_staffs');
    }
}

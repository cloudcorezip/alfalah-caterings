<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCapitalCoaToMdMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','coa_capital_id')) {
                $table->unsignedBigInteger('coa_capital_id')->nullable();
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

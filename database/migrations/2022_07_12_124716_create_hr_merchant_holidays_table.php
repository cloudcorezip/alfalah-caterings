<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrMerchantHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_merchant_holidays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('holiday_date');
            $table->string('occasion');
            $table->string('color')->nullable();
            $table->smallInteger('is_generate_system')->default(0);
            $table->smallInteger('is_national_holiday')->default(0);
            $table->smallInteger('is_work')->default(0);
            $table->smallInteger('is_leave')->default(0);
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_to_all_branch')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('md_merchant_id')->references('id')
                ->on('md_merchants');
            $table->foreign('created_by')->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_merchant_holidays');
    }
}

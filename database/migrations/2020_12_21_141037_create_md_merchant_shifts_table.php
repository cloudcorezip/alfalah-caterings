<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('md_user_id');
            $table->string('role_name')->nullable();
            $table->unsignedBigInteger('md_merchant_cashdrawer_id');
            $table->dateTime('start_shift');
            $table->dateTime('end_shift')->nullable();
            $table->double('starting_cash',20,3)->default(0);
            $table->smallInteger('is_active')->default(0);
            $table->string('timezone');
            $table->smallInteger('is_deleted')->default(0);
            $table->longText('note')->nullable();
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_merchant_cashdrawer_id')
                ->references('id')
                ->on('md_merchant_cashdrawers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_shifts');
    }
}

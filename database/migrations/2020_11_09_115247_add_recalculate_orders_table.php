<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecalculateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_sc_categories', function (Blueprint $table) {
            if(!Schema::hasColumn('md_sc_categories','is_recalculate'))
            {
                $table->smallInteger('is_recalculate')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_sc_categories', function (Blueprint $table) {
            //
        });
    }
}

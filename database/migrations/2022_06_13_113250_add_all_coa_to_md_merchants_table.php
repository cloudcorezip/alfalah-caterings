<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAllCoaToMdMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','coa_cash_outlet_id'))
            {
                //general
                $table->unsignedBigInteger('coa_cash_outlet_id')->nullable();
                $table->unsignedBigInteger('coa_shipping_cost_id')->nullable();
                $table->unsignedBigInteger('coa_administration_bank_id')->nullable();

                //sale
                $table->unsignedBigInteger('coa_debt_ppn_out_id')->nullable();
                $table->unsignedBigInteger('coa_transaction_discount_sale_id')->nullable();
                $table->unsignedBigInteger('coa_ar_sale_id')->nullable();
                //purchase
                $table->unsignedBigInteger('coa_tax_ppn_in_id')->nullable();
                $table->unsignedBigInteger('coa_ap_purchase_id')->nullable();
                $table->unsignedBigInteger('coa_transaction_discount_purchase_id')->nullable();
                //stock
                $table->unsignedBigInteger('coa_stock_adjustment_id')->nullable();
                //asset
                $table->unsignedBigInteger('coa_initial_asset_id')->nullable();
                $table->unsignedBigInteger('coa_profit_sale_asset_id')->nullable();
                $table->unsignedBigInteger('coa_debt_asset_id')->nullable();

                //$closing
                $table->unsignedBigInteger('coa_resume_profit_lost_id')->nullable();

                //production
                $table->unsignedBigInteger('coa_production_cost_id')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

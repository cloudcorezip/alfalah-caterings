<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcommerceProductAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecommerce_product_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('attribute_id')->nullable();
            $table->string('attribute_name')->nullable();
            $table->string('blibli_attr_disp_name')->nullable();
            $table->string('bukalapak_attr_disp_name')->nullable();
            $table->string('elevenia_attr_disp_name')->nullable();
            $table->string('lazada_attr_disp_name')->nullable();
            $table->string('shopee_attr_disp_name')->nullable();
            $table->string('tiktok_attr_disp_name')->nullable();
            $table->string('tokopedia_attr_disp_name')->nullable();
            $table->string('zalora_attr_disp_name')->nullable();
            $table->string('zilingo_attr_disp_name')->nullable();
            $table->string('category_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecommerce_product_attributes');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNpwpNumberCustTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_customers', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_customers', 'npwp_number')) {
                $table->string('npwp_number')->nullable();
                $table->string('shop_name')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_customers', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdAppLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_app_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('log_name');
            $table->string('controller_name');
            $table->string('route_name');
            $table->longText('error_line');
            $table->longText('error_file');
            $table->longText('error_message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_app_logs');
    }
}

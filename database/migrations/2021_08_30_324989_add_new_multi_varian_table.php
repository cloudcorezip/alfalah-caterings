<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewMultiVarianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_product_multi_varians', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_product_multi_varians','multi_varian'))
            {
                $table->jsonb('multi_varian')->nullable();
                $table->string('label')->nullable();

            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_product_multi_varians', function (Blueprint $table) {
            //
        });
    }
}

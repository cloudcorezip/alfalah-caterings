<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransDescInAllJurnal21Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_ap_details', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_ap_details','ap_detail_code'))
            {
                $table->string('ap_detail_code')->nullable();
            }
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_ap_details', function (Blueprint $table) {
            //
        });
    }
}

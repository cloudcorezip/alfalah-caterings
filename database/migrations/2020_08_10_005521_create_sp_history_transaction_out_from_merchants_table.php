<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpHistoryTransactionOutFromMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_history_transaction_out_from_merchants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_user_merchant_id');
            $table->unsignedBigInteger('sp_history_transaction_out_id');
            $table->foreign('md_user_merchant_id','user_merchant_outs_id_foreign')
                ->references('id')
                ->on('md_users');
            $table->foreign('sp_history_transaction_out_id','tf_out_from_merchants_id_foreign')
                ->references('id')
                ->on('sp_history_transaction_outs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_history_transaction_out_from_merchants');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoaCommissionInCommisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_commission_lists', function (Blueprint $table) {
            if(!Schema::hasColumn('merchant_commission_lists','coa_commission_id')){
                $table->unsignedBigInteger('coa_commission_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_commission_lists', function (Blueprint $table) {
            //
        });
    }
}

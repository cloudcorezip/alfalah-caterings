<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddScreenshotToGrabActivationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_grab_activations', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_grab_activations','screenshot_file'))
            {
                $table->smallInteger('is_have_account')->default(0);
                $table->string('screenshot_file')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_grab_activations', function (Blueprint $table) {
            //
        });
    }
}

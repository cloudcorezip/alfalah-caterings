<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('price',20,3)->default(0);
            $table->enum('type',['monthly','yearly']);
            $table->timestamps();
        });

        Schema::table('md_merchants',function (Blueprint $table){
            if(!Schema::hasColumn('md_merchants','md_subscription_id'))
            {
                $table->unsignedBigInteger('md_subscription_id')->nullable();
                $table->smallInteger('is_active_subscription')->default(0);
                $table->timestamp('start_subscription')->nullable();
                $table->timestamp('end_subscription')->nullable();
                $table->foreign('md_subscription_id')->references('id')
                    ->on('md_subscriptions');
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_subscriptions');
    }
}

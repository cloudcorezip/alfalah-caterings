<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdPluginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_plugins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('name');
            $table->string('icon')->nullable();
            $table->double('price',20,3)->default(0);
            $table->double('discount',20,3)->default(0);
            $table->double('discount_percentage',20,3)->default(0);
            $table->text('description')->nullable();
            $table->string('payment_type')->nullable();
            $table->bigInteger('quota_range_min')->default(0);
            $table->bigInteger('quota_range_max')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_plugins');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrMerchantShiftRoastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_merchant_shift_roasters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('staff_id');
            $table->unsignedBigInteger('staff_user_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->unsignedBigInteger('shift_id');
            $table->smallInteger('is_change')->default(0);
            $table->smallInteger('is_deleted',0)->default(0);
            $table->date('shift_date');
            $table->foreign('staff_id')->references('id')->on('md_merchant_staff');
            $table->foreign('staff_user_id')->references('id')->on('md_users');
            $table->foreign('md_merchant_id')->references('id')->on('md_merchants');
            $table->foreign('shift_id')->references('id')->on('hr_merchant_shift_settings');
            $table->string('timezone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_merchant_shift_roasters');
    }
}

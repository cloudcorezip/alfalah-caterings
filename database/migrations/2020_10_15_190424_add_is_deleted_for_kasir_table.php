<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDeletedForKasirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_sale_orders','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });

        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });

        Schema::table('sc_suppliers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_suppliers','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });

        Schema::table('sc_customers', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_customers','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });

        Schema::table('sc_transfer_stocks', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_transfer_stocks','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });
        Schema::table('sc_retur_sale_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_sale_orders','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });

        Schema::table('sc_retur_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_retur_purchase_orders','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });

        Schema::table('sc_product_categories', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_product_categories','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });

        Schema::table('sc_merks', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_merks','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_sale_orders', function (Blueprint $table) {
            //
        });
    }
}

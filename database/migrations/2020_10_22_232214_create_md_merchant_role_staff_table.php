<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMdMerchantRoleStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('md_merchant_role_staff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('md_merchant_staff_id');
            $table->unsignedBigInteger('md_user_staff_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->longText('staff_config')->nullable();
            $table->foreign('md_merchant_staff_id')
                ->references('id')
                ->on('md_merchant_staff');
            $table->foreign('md_user_staff_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('md_merchant_role_staff');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRedeemId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_product_redeem_details', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_product_redeem_details','sc_redeem_id')){
                $table->unsignedBigInteger('sc_redeem_id');
            }
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_product_redeem_details', function (Blueprint $table) {
            
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSystemAmountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_merchant_shifts', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_shifts','system_amount'))
            {
                $table->double('system_amount',20,3)->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_merchant_shifts', function (Blueprint $table) {
            //
        });
    }
}

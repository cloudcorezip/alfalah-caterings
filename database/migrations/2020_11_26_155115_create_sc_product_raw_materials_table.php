<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScProductRawMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_product_raw_materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_sc_product_id');
            $table->unsignedBigInteger('child_sc_product_id');
            $table->smallInteger('is_deleted')->default(0);
            $table->double('quantity',20,3)->default(0);
            $table->foreign('parent_sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->foreign('child_sc_product_id')
                ->references('id')
                ->on('sc_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_product_raw_materials');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccMerchantCashflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_merchant_cashflows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name');
            $table->unsignedBigInteger('cashflow_coa_detail_id');
            $table->unsignedBigInteger('to_cashflow_coa_detail_id');
            $table->unsignedBigInteger('md_sc_cash_type_id');
            $table->unsignedBigInteger('md_merchant_id');
            $table->double('amount',20,3)->default(0);
            $table->double('admin_fee')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->longText('note')->nullable();
            $table->foreign('cashflow_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('to_cashflow_coa_detail_id')
                ->references('id')
                ->on('acc_coa_details');
            $table->foreign('md_sc_cash_type_id')
                ->references('id')
                ->on('md_sc_cash_types');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            $table->timestamps();
        });
        Schema::dropIfExists('md_merchant_shift_cashflows');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_merchant_cashflows');
    }
}

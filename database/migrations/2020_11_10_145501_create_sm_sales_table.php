<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sm_sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname');
            $table->string('email');
            $table->string('address')->nullable();
            $table->unsignedBigInteger('regional_city_id');
            $table->unsignedBigInteger('md_user_id');
            $table->unsignedBigInteger('head_id')->nullable();
            $table->smallInteger('is_deleted')->default(0);
            $table->foreign('regional_city_id')
                ->references('id')
                ->on('md_cities');
            $table->foreign('md_user_id')
                ->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sm_sales');
    }
}

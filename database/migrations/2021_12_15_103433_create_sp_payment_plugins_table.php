<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpPaymentPluginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_payment_plugins', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('md_merchant_id');
            $table->foreign('md_merchant_id')
                ->references('id')
                ->on('md_merchants');
            
            $table->unsignedBigInteger('md_plugin_id');
            $table->foreign('md_plugin_id')
                ->references('id')
                ->on('md_plugins');

            $table->unsignedBigInteger('md_transaction_type_id')->nullable();
            $table->foreign('md_transaction_type_id')
                ->references('id')
                ->on('md_transaction_types');
            
            $table->string('refferal_code')->nullable();
            $table->jsonb('content_resp')->nullable();
            $table->jsonb('content_resp_web')->nullable();
            $table->string('qr')->nullable();
            $table->smallInteger('is_from_web')->default(0);
            $table->smallInteger('is_expired')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_payment_plugins');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInventoryMethodsToMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','md_inventory_method_id'))
            {
                $table->unsignedBigInteger('md_inventory_method_id')->nullable();
                $table->bigInteger('stock_notification')->default(0);
                $table->foreign('md_inventory_method_id')
                    ->references('id')
                    ->on('md_inventory_methods');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

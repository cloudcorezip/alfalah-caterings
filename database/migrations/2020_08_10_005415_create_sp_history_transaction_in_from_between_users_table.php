<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpHistoryTransactionInFromBetweenUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_history_transaction_in_from_between_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('from_md_user_id');
            $table->unsignedBigInteger('sp_history_transaction_in_id');
            $table->foreign('from_md_user_id','from_user_id_foreign')
                ->references('id')
                ->on('md_users');
            $table->foreign('sp_history_transaction_in_id','tf_in_from_between_user_id_foreign')
                ->references('id')
                ->on('sp_history_transaction_ins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_history_transaction_in_from_between_users');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsIntangibleAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_asset_depreciations','is_intangible_asset'))
            {
                $table->smallInteger('is_intangible_asset')->default(0);
                $table->string('file_asset')->nullable();

            }
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_asset_depreciations', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsLimitedAndMergeCrmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            if(!Schema::hasColumn('crm_discount_by_products','is_coupon')){
                $table->smallInteger('is_coupon')->default(0);
                $table->smallInteger('is_limited')->default(0);
                $table->double('max_coupon_use')->default(0);
                $table->double('max_coupon_use_per_customer')->default(0);

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crm_discount_by_products', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFcmSchedulerTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fcm_scheduler_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('message');
            $table->string('image');
            $table->smallInteger('is_sended')->default(0);
            $table->jsonb('data')->nullable();
            $table->dateTime('time_scheduler');
            $table->integer('period')->default(0);
            //one time
            //daily
            //weekly
            //monthly
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fcm_scheduler_tasks');
    }
}

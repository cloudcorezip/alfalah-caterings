<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentProductIdInSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_stock_sale_mappings', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_stock_sale_mappings','parent_sc_product_id'))
            {
                $table->unsignedBigInteger('parent_sc_product_id')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_stock_sale_mappings', function (Blueprint $table) {
            //
        });
    }
}

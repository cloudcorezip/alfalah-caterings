<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInvWarehouseInScPurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            if(!Schema::hasColumn('sc_purchase_orders','inv_warehouse_id'))
            {
                $table->unsignedBigInteger('inv_warehouse_id')->nullable();
                $table->foreign('inv_warehouse_id')
                    ->references('id')
                    ->on('md_merchant_inv_warehouses');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_purchase_orders', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJsonTypeToMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchant_menus', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchant_menus','payment_clusters'))
            {
                $table->jsonb('payment_clusters')->nullable();
                //$table->longText('description')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchant_menus', function (Blueprint $table) {
            //
        });
    }
}

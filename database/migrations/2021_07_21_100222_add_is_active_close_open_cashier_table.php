<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsActiveCloseOpenCashierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            if(!Schema::hasColumn('md_merchants','is_active_close_open_cashier'))
            {
                $table->smallInteger('is_active_close_open_cashier')->default(0);
            }
        });

        Schema::table('acc_merchant_shifts', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_merchant_shifts','end_date'))
            {
                $table->dateTime('end_date')->nullable();
                $table->smallInteger('is_shift')->default(0);
                $table->bigInteger('staff_user_id')->nullable();
                $table->smallInteger('is_first_shift')->default(0);
            }
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_merchants', function (Blueprint $table) {
            //
        });
    }
}

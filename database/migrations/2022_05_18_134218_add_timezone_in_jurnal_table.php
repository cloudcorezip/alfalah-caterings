<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimezoneInJurnalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acc_jurnals', function (Blueprint $table) {
            if(!Schema::hasColumn('acc_jurnals','timezone'))
            {
                $table->string('timezone')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('acc_jurnals', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContentRespWebToSpPaymentSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sp_payment_subscriptions', function (Blueprint $table) {
            if(!Schema::hasColumn('sp_payment_subscriptions','content_resp_web'))
            {
                $table->jsonb('content_resp_web')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sp_payment_subscriptions', function (Blueprint $table) {
            //
        });
    }
}

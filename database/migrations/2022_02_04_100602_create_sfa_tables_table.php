<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSfaTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('sfa_kpi_visits')){
            Schema::create('sfa_kpi_visits', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name')->nullable();
                $table->unsignedBigInteger('md_merchant_id');
                $table->unsignedBigInteger('staff_user_id');
                $table->unsignedBigInteger('created_by')->nullable();
                $table->bigInteger('visit_count')->default(0);
                $table->bigInteger('min_sale')->default(0);
                $table->bigInteger('min_new_customer')->default(0);
                $table->dateTime('start_date')->nullable();
                $table->dateTime('end_date')->nullable();
                $table->smallInteger('is_deleted')->default(0);
                $table->timestamps();
                $table->foreign('md_merchant_id')
                    ->references('id')
                    ->on('md_merchants');
                $table->foreign('staff_user_id')
                    ->references('id')
                    ->on('md_users');
                $table->foreign('created_by')
                    ->references('id')
                    ->on('md_users');
            });

            Schema::create('sfa_visit_schedules', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('md_merchant_id');
                $table->unsignedBigInteger('staff_user_id');
                $table->unsignedBigInteger('sc_customer_id');
                $table->bigInteger('created_by')->nullable();
                $table->dateTime('visit_schedule');
                $table->smallInteger('is_visit')->default(0);
                $table->dateTime('visit_time')->nullable();
                $table->string('loc_latitude')->nullable();
                $table->string('loc_longitude')->nullable();
                $table->string('supporting_file')->nullable();
                $table->smallInteger('is_deleted')->default(0);
                $table->string('unique_code')->nullable();
                $table->timestamps();
                $table->foreign('md_merchant_id')
                    ->references('id')
                    ->on('md_merchants');
                $table->foreign('staff_user_id')
                    ->references('id')
                    ->on('md_users');
                $table->foreign('created_by')
                    ->references('id')
                    ->on('md_users');
                $table->foreign('sc_customer_id')
                    ->references('id')
                    ->on('sc_customers');
            });
            Schema::create('sfa_visit_with_products',function (Blueprint $table){
                $table->bigIncrements('id');
                $table->unsignedBigInteger('md_merchant_id');
                $table->unsignedBigInteger('staff_user_id');
                $table->unsignedBigInteger('sc_product_id');
                $table->bigInteger('created_by')->nullable();
                $table->timestamp('start_date')->nullable();
                $table->timestamp('end_date')->nullable();
                $table->double('stock_carried',20,3)->default(0);
                $table->double('remaining_stock',20,3)->default(0);
                $table->double('yesterday_stock',20,3)->default(0);
                $table->bigInteger('out_warehouse_id');
                $table->bigInteger('in_warehouse_id')->nullable();
                $table->dateTime('visit_time')->nullable();
                $table->smallInteger('is_return_to_warehouse')->default(0);
                $table->smallInteger('is_deleted')->default(0);
                $table->timestamps();
                $table->foreign('md_merchant_id')
                    ->references('id')
                    ->on('md_merchants');
                $table->foreign('staff_user_id')
                    ->references('id')
                    ->on('md_users');
                $table->foreign('created_by')
                    ->references('id')
                    ->on('md_users');
                $table->foreign('sc_product_id')
                    ->references('id')
                    ->on('sc_products');
                $table->foreign('out_warehouse_id')
                    ->references('id')
                    ->on('md_merchant_inv_warehouses');
                $table->foreign('in_warehouse_id')
                    ->references('id')
                    ->on('md_merchant_inv_warehouses');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sfa_kpi_visits');
        Schema::dropIfExists('sfa_visit_schedules');
        Schema::dropIfExists('sfa_visit_with_products');
    }
}

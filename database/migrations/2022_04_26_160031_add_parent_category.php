<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParentCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sc_product_categories', function (Blueprint $table) {
            if (!Schema::hasColumn('sc_product_categories', 'parent_id')) {
                $table->unsignedBigInteger('parent_id')->nullable();
                $table->jsonb('assign_to_branch')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sc_product_categories', function (Blueprint $table) {
            //
        });
    }
}

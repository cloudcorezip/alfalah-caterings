<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScUserCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sc_user_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sc_customer_id');
            $table->unsignedBigInteger('from_user_id');
            $table->foreign('from_user_id')
                ->references('id')
                ->on('md_users');
            $table->foreign('sc_customer_id')
                ->references('id')
                ->on('sc_customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sc_user_customers');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefJurnalIdInCommisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('merchant_commission_lists', function (Blueprint $table) {
            if(!Schema::hasColumn('merchant_commission_lists','ref_jurnal_id')){
                $table->unsignedBigInteger('ref_jurnal_id')->nullable();
                $table->double('admin_fee',20,3)->default(0);
                $table->string('second_code')->nullable();
                $table->string('payment_trans_proof')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('merchant_commission_lists', function (Blueprint $table) {
            //
        });
    }
}

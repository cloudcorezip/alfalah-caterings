<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddName3ToSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('md_subscriptions', function (Blueprint $table) {
            if(!Schema::hasColumn('md_subscriptions','is_deleted'))
            {
                $table->smallInteger('is_deleted')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('md_subscriptions', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrMerchantAttendanceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_merchant_attendance_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('is_allowed_shift_change')->default(0);
            $table->smallInteger('is_allowed_shift_other_time')->default(0);
            $table->smallInteger('is_active_reminder_status')->default(0);
            $table->smallInteger('is_count_leave_from_date_joining')->default(0);
            $table->double('reminder_attendance',20,3)->default(0);
            $table->unsignedBigInteger('md_merchant_id');
            $table->smallInteger('is_to_all_branch')->default(0);
            $table->smallInteger('is_deleted')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('md_merchant_id')->references('id')
                ->on('md_merchants');
            $table->foreign('created_by')->references('id')
                ->on('md_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_merchant_attendance_settings');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLatitudeToSsDetailOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            if(!Schema::hasColumn('ss_detail_order_services','latitude'))
            {
                $table->string('latitude')->nullable();
                $table->string('longitude')->nullable();

            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ss_detail_order_services', function (Blueprint $table) {
            //
        });
    }
}
